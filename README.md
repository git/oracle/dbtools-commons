# Build Instructions


## Install Maven

 [Maven Download Page](https://maven.apache.org/download.cgi)
 
 Maven 3.5.2 or later is required

 Add `${MAVEN_HOME}/bin` to your `${PATH}`
 
## Install Oracle SQLcl
 
 1. Download SQLcl from [OTN](http://www.oracle.com/technetwork/developer-tools/sqlcl/downloads/index.html)
 2. Unzip the archive and cd to the lib dir.
 3. In the lib dir, run `mvn validate`.  This will install the jars in the lib dir to your local maven repository in `~/.m2`. We do this because there are dependencies which are not on public repositories.


## Maven Configuration

This project is set up so that the only dependencies, other than  those from `maven.org` will be installed from Oracle SQLcl install.  There should be no other maven configuration needed.


# Initial Build

```
cd ~/work/dbtools-commons
mvn -U clean install
```

 - `-U` forces all dependencies to be updated. 
 - `clean` erases any existing build artefacts.
 - `install` installs each compiled Maven module (jar) to the local repository

 When this completes, you should see this
 
```
...
-----------------------------------------------------------------------
[INFO] Reactor Summary:
[INFO] 
[INFO] DBTools Resource Generation maven plugin ... SUCCESS [  2.378 s]
[INFO] DBTools Common Project Parent POM .......... SUCCESS [  0.292 s]
[INFO] DBTools Common Library ..................... SUCCESS [ 16.331 s]
[INFO] DBTools HTTP Library ....................... SUCCESS [  0.378 s]
[INFO] DBTools REST JDBC Driver ................... SUCCESS [  2.002 s]
[INFO] DBTools SQLDeveloper SQLcl Library ........  SUCCESS [  8.762 s]
[INFO] DBTools SQLcl Plugin for Hudson ............ SUCCESS [03:31 min]
[INFO] ----------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ----------------------------------------------------------------
 ```
   
