![](src/main/icons/toolbag.png)

# Database Tools Common library

The database tools common library contain APIs and implementation for
  
  - Connecting to an Oracle database
  - Connecting to another Database source
  - Parsing & Running SQL, PLSQL and SQL*Plus Scripts
  - TaskManager
  - Formatter
  - Datatype manipulation
  - export generators
  - REST enablement of schema
