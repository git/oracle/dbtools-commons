/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import oracle.dbtools.app.Format.Breaks;
import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.Program;
import oracle.dbtools.arbori.SqlProgram;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Pair;
import oracle.dbtools.util.Service;

public class Dependencies {
    public static void main( String[] args ) throws Exception {
        String input =  Service.readFile(SqlEarley.class, "test.sql");
        //input =  Service.readFile(SqlEarley.class, "FND_STATS.pkgbdy");
        //input =  Service.readFile(SqlEarley.class, "MSC_CL_PRE_PROCESS.pkgbdy");
        //input =  Service.readFile("/temp/privoolk.sql");
        input =  Service.readFile("/temp/test_pack.pkb");
        Program.timing=10000 < input.length();
		//Service.profile(500000, 1000); 

        List<LexerToken>  src = LexerToken.parse(input);
        List<Pair<ParseNode,ParseNode>> dependencies = new Dependencies().calculate(input, src);
        for( Pair<ParseNode,ParseNode> dependency : dependencies ) {
			System.out.println(dependency.first().content(src)+"--->"+dependency.second().content(src));
		}
    }
    
    public List<Pair<ParseNode,ParseNode>> calculate( String input, List<LexerToken> src ) throws SyntaxError, IOException {
        Parsed target = null;
        if( programInstance == null )
            programInstance = new SqlProgram(Service.readFile(Format.class, path+"dependency.arbori"));
            if( /*ignoreSyntaxError=*/ false ) {
                SqlEarley earley = SqlEarley.getInstance();
                Matrix matrix = new Matrix(earley);
                earley.parse(src, matrix); 
                ParseNode root = earley.forest(src, matrix);
                target = new Parsed(input, src, root);
            } else
            	target = new Parsed(  // this is PL/SQL source parsed into a tree which arbori program is applied to
            			input, 
            			src,
            			SqlEarley.getInstance(),
            			new String[]{"sql_statements","subprg_body","expr","sql_stmt", "basic_d"} //$NON-NLS-1$
            			);
            
            dependencies = new LinkedList<Pair<ParseNode,ParseNode>>();
            cu = null;
            programInstance.eval(target, this);
            return dependencies;
    }

    static final String path = "/oracle/dbtools/app/"; //$NON-NLS-1$
    static SqlProgram programInstance = null;
    
    //////////////////////////// Callbacks: ////////////////////////////
    
    List<Pair<ParseNode,ParseNode>> dependencies = new LinkedList<Pair<ParseNode,ParseNode>>();  
    void dependencies( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(tuple.toString());
        // Less verbose:
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
    	
    	ParseNode aname = tuple.get("aname");
    	ParseNode dname = tuple.get("dname");
    	dependencies.add(new Pair(aname,dname));
    }
    
    ParseNode cu = null;
    void compilation_unit( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(tuple.toString());
        // Less verbose:
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
    	
    	cu = tuple.get("aname");
    }

    void compilation_unit_dependencies( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(tuple.toString());
        // Less verbose:
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
    	
    	ParseNode dname = tuple.get("dname");
    	dependencies.add(new Pair(cu,dname));
    }


}
