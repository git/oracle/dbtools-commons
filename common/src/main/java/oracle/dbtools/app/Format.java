/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.Program;
import oracle.dbtools.arbori.SqlProgram;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.Yelrae;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Service;

public class Format {

    public static void main( String[] args ) throws Exception {
        String input =  Service.readFile(SqlEarley.class, "test.sql");
        input =  Service.readFile(Format.class, "format_test.sql");
        //input =  Service.readFile(SqlEarley.class, "FND_STATS.pkgbdy");        
        //input =  Service.readFile(SqlEarley.class, "MSC_CL_PRE_PROCESS.pkgbdy");
        
        /*final Thread mainThread = Thread.currentThread();
        //Service.profile(60000, 1000, 20);
        new Thread() {
			@Override
			public void run() {				
			    try {
			        Thread.sleep(55000);
			        mainThread.interrupt();
			    } catch( InterruptedException e ) {
			    }			    
			}       	
        }.start();*/
        
        //input =  Service.readFile("/Temp/privoolk.sql");
        Program.timing=10000 < input.length();
		//Service.profile(500000, 1000); 
        SqlEarley.visualize = false;
        SqlEarley.main(new String[]{input});
        final Format format = new Format() {
        	// test reflection on derived instance
        };
        format.rethrowSyntaxError = true;
        
        //options.put(alignEquality, true);
        //options.put(alignNamedArgs, true);
        //options.put(alignRight, true);
        //options.put(breakAfterWhile, true);        
        //options.put(breakAfterWhen, true);        
        //options.put(breakAfterIf, true);        
        //options.put(breaksAfterSelect, false);        
        //options.put(breaksComma, Breaks.Before);
        //options.put(breaksProcArgs, true);
        //options.put(breaksConcat, Breaks.None);
        //options.put(breakOnSubqueries,false);
        //options.put(breaksAroundLogicalConjunctions,Breaks.BeforeAndAfter);
        //options.put(breakAnsiiJoin, true);
        //options.put(breakParenCondition, true); 
        //options.put(extraLinesAfterSignificantStatements,BreaksX2.Keep);
        //options.put(forceLinebreaksBeforeComment,true);
        //options.put(identSpaces, 8);
        //options.put(singleLineComments,InlineComments.None);        
        //options.put(spaceAroundOperators,false);        
        //options.put(spaceAfterCommas,true);        

        //options.put(formatProgramURL, "/temp/format.prg");
        
        String output = format.format(input);
        //System.out.println(format.nodeDepths.toString());
        if( input.length() < 100000 ) {
            System.out.println("----------------- output: ------------------");
            System.out.println(output);
        }
        long t1 = System.currentTimeMillis();
        output = format.format(input);
        long t2 = System.currentTimeMillis();
        System.out.println("Second fmt time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
    }
    
    public static String singleLineComments = "singleLineComments";
    public static String kwCase = "kwCase";
    public static String idCase = "idCase";
    public static String formatThreshold = "formatThreshold";
    
    public static String alignTypeDecl = "alignTypeDecl"; 
    public static String alignNamedArgs = "alignNamedArgs"; 
    public static String alignAssignments = "alignAssignments"; 
    public static String alignEquality = "alignEquality"; 
    public static String alignRight = "alignRight"; 
    
    public static String identSpaces = "identSpaces";
    public static String useTab = "useTab";  
    
    public static String breaksComma = "breaksComma";
    public static String breaksProcArgs = "breaksProcArgs";
    public static String breaksConcat = "breaksConcat";
    public static String breaksAroundLogicalConjunctions = "breaksAroundLogicalConjunctions";
    public static String breaksAfterSelect = "breaksAfterSelect";
    public static String commasPerLine = "commasPerLine";
    public static String breakOnSubqueries = "breakOnSubqueries";
    public static String breakAnsiiJoin = "breakAnsiiJoin";
    public static String breakParenCondition = "breakParenCondition";
    public static String maxCharLineSize = "maxCharLineSize";
    public static String forceLinebreaksBeforeComment = "forceLinebreaksBeforeComment";
    public static String extraLinesAfterSignificantStatements = "extraLinesAfterSignificantStatements";
    public static String breakAfterCase = "breakAfterCase";
    public static String breakAfterWhen = "breakAfterWhen";
    public static String breakAfterThen = "breakAfterThen";
    public static String breakAfterElse = "breakAfterElse";
    public static String breakAfterIf = "breakAfterIf";
    public static String breakAfterElseif = "breakAfterElsif";
    public static String breakAfterWhile = "breakAfterWhile";

    
    public static String spaceAroundOperators = "spaceAroundOperators";
    public static String spaceAfterCommas = "spaceAfterCommas";
    public static String spaceAroundBrackets = "spaceAroundBrackets";
    
    public static String formatProgramURL = "formatProgramURL";
    
    public static Map<String,Object> options = new HashMap<String,Object>() {
    	/**
    	 * Advanced format via custom program change
    	 */
		@Override
		public Object put( String key, Object value ) {
			programInstance = null;     
			return super.put(key, value);
		}    	
    };
    static {
        setDefaultOptions();         
    }
	public static void setDefaultOptions() {
		options.put(singleLineComments, InlineComments.CommentsUnchanged); 
        options.put(kwCase, Case.UPPER);
        options.put(idCase, Case.lower);
        options.put(formatThreshold, 1);   // number of words
        
        options.put(alignTypeDecl, true);
        options.put(alignNamedArgs, true);
        options.put(alignEquality, false);
        options.put(alignAssignments, false);
        options.put(alignRight, false);
        
        options.put(identSpaces, 4);
        options.put(useTab, false);  
        
        options.put(breaksComma, Breaks.After);
        options.put(breaksProcArgs, false);
        options.put(breaksConcat, Breaks.Before);
        options.put(breaksAroundLogicalConjunctions, Breaks.Before);
        options.put(breaksAfterSelect, true);        
        options.put(commasPerLine, 1);        
        options.put(breakOnSubqueries, true);        
        options.put(breakAnsiiJoin, false);        
        options.put(breakParenCondition, false);        
        options.put(maxCharLineSize, 128);
        options.put(forceLinebreaksBeforeComment, false);        
        options.put(extraLinesAfterSignificantStatements, BreaksX2.X2);
        options.put(breakAfterCase, false);        
        options.put(breakAfterWhen, false);        
        options.put(breakAfterThen, false); 
        options.put(breakAfterElse, false); 
        options.put(breakAfterIf, false); 
        options.put(breakAfterElseif, false); 
        options.put(breakAfterWhile, false); 
        
        options.put(spaceAroundOperators, true); 
        options.put(spaceAfterCommas, false); 
        options.put(spaceAroundBrackets, Space.NoSpace);
        
        options.put(formatProgramURL, "default");
	}
    public static void setOptions( Map<String,Object> changed ) {
        for( String key : changed.keySet() )
            options.put(key, changed.get(key));
    }

    /* boolean option derivatives */
    public boolean breaksAfterComma() { return options.get(breaksComma) == Breaks.After; }
    public boolean breaksBeforeComma() { return options.get(breaksComma) == Breaks.Before; }
    public boolean breaksAfterLogicalConjunction() { return options.get(breaksAroundLogicalConjunctions) == Breaks.After ||  options.get(breaksAroundLogicalConjunctions) == Breaks.BeforeAndAfter; }
    public boolean breaksBeforeLogicalConjunction() { return options.get(breaksAroundLogicalConjunctions) == Breaks.Before ||  options.get(breaksAroundLogicalConjunctions) == Breaks.BeforeAndAfter; }
    public boolean breaksAfterConcat() { return options.get(breaksConcat) == Breaks.After ||  options.get(breaksConcat) == Breaks.BeforeAndAfter; }
    public boolean breaksBeforeConcat() { return options.get(breaksConcat) == Breaks.Before ||  options.get(breaksConcat) == Breaks.BeforeAndAfter; }
    public boolean breaksAfterSelectFromWhere() { return (Boolean)options.get(breaksAfterSelect); }
    
    // to facilitate cursor placement 
    public int inputPos = -1;
    public int outputPos = -1;
    
    static final String path = "/oracle/dbtools/app/"; //$NON-NLS-1$
    static SqlProgram programInstance = null;
    
    public static void resetProgramInstance( String prg ) {
        options.put(formatProgramURL, prg);
        programInstance = null;
    }
    public static void resetProgramInstance() {
        programInstance = null;
    }
    
	public String getActiveFormatProgram()  throws IOException {
		String ret = Service.readFile(Format.class, path+"format.prg");
        String customURL = (String)options.get(formatProgramURL);
        if( !"default".equals(customURL) ) {
        	try {
        		ret = Service.readFile(customURL);
			} catch( IOException e ) {
				System.out.println("Failed to read custom formatting program "+customURL);
				System.out.println(e.getMessage());
			}
        }
        return ret;
	}
	
    final int unary_add_op = SqlEarley.getInstance().getSymbol("unary_add_op"); //$NON-NLS-1$
    final int compound_expression822 = SqlEarley.getInstance().getSymbol("compound_expression[8,22)");
    final int sql_statement = SqlEarley.getInstance().getSymbol("sql_statement");
    final int numeric_literal = SqlEarley.getInstance().getSymbol("numeric_literal");
    final int exp = SqlEarley.getInstance().getSymbol("\".exp.\"");
    
	public boolean rethrowSyntaxError = false;
    /**
     * Preprocess input and delegate work to formatFragment
     * @param input
     * @param ignoreSyntaxError    format syntactically valid fragment only
     * @return
     * @throws IOException
     */
    public synchronized String format( String input ) throws IOException {
    	initCallbackScaffolds();
    	
        StringBuilder output = new StringBuilder();
             
        outputPos = -1;
        
        Parsed target = null;
        if( programInstance == null ) {
            String formaterArboriCode = getActiveFormatProgram();
			programInstance = new SqlProgram(formaterArboriCode) {
				private Map<String,Boolean> binds = null;
				@Override
				public Boolean getBoolBindVar( String name ) {
					if( binds == null ) {
						binds = new HashMap<String,Boolean>();
						for( String option : options.keySet() ) {
							Object value = options.get(option);
							if( value instanceof Boolean )
								binds.put(option, (boolean) options.get(option));
						}
						Method[] methods = Format.class.getMethods();
						for( int i = 0; i < methods.length; i++ ) {
							if( 0 < methods[i].getGenericParameterTypes().length )
								continue;
							if( methods[i].getReturnType() != boolean.class )
								continue;
							try {
								binds.put(methods[i].getName(), (boolean)methods[i].invoke(Format.this));
							} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
								e.printStackTrace();
							}			
						}
					}
					return binds.get(name);
				}
				
			};

        }
        try {
            //programInstance.run(input, null, new String[]{"sql_statements","subprg_body","expr","sql_stmt", "basic_d"}, this);            
            List<LexerToken>  src = LexerToken.parse(input);
            String rootPayload = "sql_statements";
            if( !rethrowSyntaxError )
            	rootPayload = Yelrae.PARSE_WITH_ERRORS;
            target = new Parsed(  // this is PL/SQL source parsed into a tree which arbori program is applied to
            		input, 
            		src,
            		SqlEarley.getInstance(),
            		new String[]{ rootPayload } //$NON-NLS-1$
            );
            if( rethrowSyntaxError && target.getSyntaxError() != null )
            	throw target.getSyntaxError();
            
            programInstance.eval(target, this);
            if( inputPos < 1 )
                outputPos = output.length();
            
        } catch( SyntaxError e ) {
        	outputPos = e.end;
        	if( rethrowSyntaxError )
        		throw e;
        	//System.out.println(e.getMessage());
            return input;
        }
        
        
        List<LexerToken> fullCode = LexerToken.parse(input,true);
        int pos = -1; // in src (which matches parse tree)
        String priorIdent = null;
        LexerToken prior = null;
        int cumulativeChars = 0;
        int lastNewLine = -1;
        ParseNode root = target.getRoot();
        if( root.children().size() == 0 )
        	return input;
        
        for( LexerToken t : fullCode ) {       	
            if( outputPos < 0 && inputPos <= t.begin )
                outputPos = t.begin;
            
        	if( root.topLevel != null && root.coveredByOnTopLevel(pos+1) == null ) {
        		output.append(t.content);
                if( t.type != Token.WS && t.type != Token.COMMENT  && t.type != Token.LINE_COMMENT 
                 && t.type != Token.MACRO_SKIP && t.type != Token.SQLPLUSLINECONTINUE_SKIP  ) 
                	pos++;
        		continue;
        	}
        	
            if( t.type == Token.WS ) {
                if( "\n".equals(t.content) ) {
                	if( prior != null && prior.end < lastNewLine && lastNewLine < t.begin && options.get(extraLinesAfterSignificantStatements) == BreaksX2.Keep ) {
                		output.append("\n");
                	}
                    lastNewLine = t.end;
                }
                continue;
            }
            if( t.type == Token.COMMENT || t.type == Token.LINE_COMMENT || t.type == Token.MACRO_SKIP || t.type == Token.SQLPLUSLINECONTINUE_SKIP ) {
                if(  (Boolean)options.get(forceLinebreaksBeforeComment) ) {
                    if( prior != null && lastNewLine < prior.end )
                        output.append("\n    ");
                }
                if( prior != null )
                    output.append(input.substring(prior.end,t.begin));
                
                if( t.content.endsWith("\r") ) // really weird cases in doc
                    t.content = t.content.substring(0,t.content.length()-1)+'\n';
                String pureContent = t.content;
                boolean endsWNL = false;
                if( pureContent.endsWith("\n") ) {
                    pureContent = pureContent.substring(0, pureContent.length()-1);
                    endsWNL = true;
                }
                if( pureContent.startsWith("--") )
                    pureContent = pureContent.substring(2);
                else if( pureContent.startsWith("/*") && pureContent.endsWith("*/"))
                    pureContent = pureContent.substring(2, pureContent.length()-2);
                if( !pureContent.contains("\n") ) {
                    if( options.get(singleLineComments)==InlineComments.MultiLine ) {
                        output.append("/*"+pureContent+"*/");
                        if( endsWNL )
                            output.append("\n");
                    } else if( options.get(singleLineComments)==InlineComments.SingleLine ) {
                        output.append("--"+pureContent);
                        output.append("\n");
                    } else
                        output.append(t.content);
                } else
                    output.append(t.content);
                
                prior = t;
                cumulativeChars = 0;
                continue;
            }
            pos++;
                                    
            ParseNode node = target.getRoot().leafAtPos(pos);
            String ident = newlinePositions.get(pos);
            if( ident != null ) {
                output.append(ident);
                priorIdent = ident;
                cumulativeChars = 0;
            } else {
                if( prior != null && prior.type == Token.LINE_COMMENT ) {   // p(aa, -- comment1
                    if( priorIdent != null )                                //   bb, -- comment2
                        output.append(priorIdent);                          // 
                    else
                    	output.append("\n");
                }
                final String scope = ids2scope.get(pos);
                if( null != scope ) {
                    int maxLen = maxIdLengthInScope.get(scope);
                    LexerToken startToken = target.getSrc().get(ids2interval.get(pos));
					int idLength = prior.end-startToken.begin; 
                    int pad = maxLen+3-idLength;  
                                  //^^ not the same as padln("   ",... !
                    if( pad < 1 )
                    	pad = 1;
                    output.append(Service.padln("",pad));
                } else {
                    String separator = decideSpace(target, pos);
                    if( "/".equals(t.content) && node.parent() != null && node.parent().contains(sql_statement) ) {
                        separator = "\n";
                        cumulativeChars = 0;               
                    }
                    if( (Integer)options.get(maxCharLineSize) < cumulativeChars ) {
                    	separator = "\n";
                        cumulativeChars = 0;
                    }
                    output.append(separator);
                }
            }

            
            String word = t.content; 
            if( t.type == Token.IDENTIFIER ) {
                word = adjustCase(word,options.get(kwCase));
                word = padAlign(word);
            }
            if( node != null && casedIds.containsKey(node.interval()) )
                word = casedIds.get(node.interval());

            output.append(word);
            cumulativeChars += word.length();            
                        
            prior = t;
        }
        if( pos < (Integer)options.get(formatThreshold) )
            return input;
        
        String ret = output.toString();
        if( ret.startsWith("\n") )
            ret = ret.substring(1);
        
        
        ret = ret.replace(((Boolean)options.get(spaceAfterCommas)?" ":"")+"  ,", " ,"); // leading ,

        if( options.get(extraLinesAfterSignificantStatements) != BreaksX2.Keep )
        	ret = ret.replace("\n\n\n", "\n\n");
        	
        //Unreliable:  ret = ret.replace(";/", ";\n/");    // will process not only statement end but comments
        //             ret = ret.replace(";\n/*", ";/*");  // rollback those 
        int index = -1;
        while( true ) {
        	index = ret.indexOf(";/", index+1);
        	if( index < 0 )
        		break;
        	if( ret.indexOf("*", index+1)==index+2 )
        		continue;
        	ret = ret.substring(0,index)+";\n/"+ret.substring(index+2);
        }
        inputPos = -1;
        return ret;
    }
    
    
	private String decideSpace( Parsed target, int pos ) {
		LexerToken t = target.getSrc().get(pos);
		LexerToken prior = null;
		if( 0 < pos )
			prior = target.getSrc().get(pos-1);
		ParseNode node = target.getRoot().leafAtPos(pos);
		if( node == null )
			return " ";
		if( node.contains("ext_tbl_string_literal[24,29)") )
			return "";
		String ret = "";
		if( node != null && prior != null &&  
		        (prior.type != Token.OPERATION && !",".equals(t.content) && !";".equals(t.content) && !".".equals(t.content) && !"%".equals(t.content)  && !"@".equals(t.content)
		      || prior.type == Token.OPERATION && !".".equals(prior.content) && !":".equals(prior.content) && !"%".equals(prior.content) && !"@".equals(prior.content) && t.type != Token.OPERATION  ) ) {
		    if(  (!isOpenParen(prior) || 0<pos && !notPaddedParenthesis.contains(pos-1) || options.get(spaceAroundBrackets)==Space.Inside ) //  prior '(' -> exists entry in padded list 
		      && (!isCloseParen(t)    || !notPaddedParenthesis.contains(pos) || options.get(spaceAroundBrackets)==Space.Inside ) //  current ')' -> exists entry in padded list 
		      && (!isOpenParen(t)     || !notPaddedParenthesis.contains(pos) || options.get(spaceAroundBrackets)==Space.Outside ) //  current '(' -> exists entry in padded list 
		            ) {
				ParseNode priorLeaf = target.getRoot().leafAtPos(pos-1);
				ParseNode parent = node.parent();
				if( 0<pos 
				    && ( priorLeaf == null ||
				            !priorLeaf.contains(unary_add_op) //, boolean isOpen
		                 && !(priorLeaf.contains(compound_expression822)&&priorLeaf.contains("'+'"))  
		                 && !(priorLeaf.contains(compound_expression822)&&priorLeaf.contains("'-'")) 
		            ) && ( !parent.contains(numeric_literal)||parent.from==node.from ) 
				    && !parent.contains(exp)
				    && ( !",".equals(prior.content) || (Boolean)options.get(spaceAfterCommas) == true )
				)
		            ret = " ";
			}
		} else if( prior != null && prior.type == Token.OPERATION && isOpenParen(t)   // IF sales >( quota + 200 ) THEN
		        || prior != null && isCloseParen(prior) && t.type == Token.OPERATION && !";".equals(t.content) && !".".equals(t.content) && !",".equals(t.content) ) // bonus := ( sales - quota )/ 4;
		    ret = " ";
		return ret;
	}
	
	private String adjustCase( String word, Object changeCase ) {
        if( !word.startsWith("\"") && changeCase == Case.lower )
            word = word.toLowerCase();
        if( !word.startsWith("\"") && changeCase == Case.UPPER )
            word = word.toUpperCase();
        if( !word.startsWith("\"") && changeCase == Case.InitCap ) {
            char[] converted = new char[word.length()];
            char prior1 = '%';
            for( int i = 0; i < converted.length; i++ ) {
                char current = word.charAt(i);
                if( i == 0 || prior1 == '_' )
                    converted[i] = Character.toUpperCase(current);
                else
                    converted[i] = Character.toLowerCase(current);
                prior1 = current;
            }
            word = new String(converted);
        }
        return word;
    }
            
    private static boolean isOpenParen( LexerToken t ) {
            return "(".equals(t.content)||"[".equals(t.content);
    }
    private static boolean isCloseParen( LexerToken t ) {
            return ")".equals(t.content)||"]".equals(t.content);
    }

    /**
     * utility to indent a formatted line
     */
    String spaceSequence( int level ) {
        String output = "";
        if( (Boolean)options.get(useTab) )
            for( int i = 0; i < level; i++ ) {
                output = output + "\t";
            }
        else
            for( int i = 0; i < level*(Integer)options.get(identSpaces); i++ ) {
                output = output + " ";
            }
        return output;
    }

    private String padAlign( String kw ) {
    	if( !(Boolean)options.get(alignRight) )
    		return kw;
        StringBuilder padding = new StringBuilder();
    	String lkw = kw.toLowerCase();
    	if( "select".equals(lkw) 
    	 || "into".equals(lkw) 
    	 || "from".equals(lkw) 
    	 || "join".equals(lkw)
    	 || "where".equals(lkw)
    	 || "or".equals(lkw)
    	 || "and".equals(lkw)
    	 || "group".equals(lkw)
    	 || "having".equals(lkw)
    	 || "order".equals(lkw)

    	 //NOOP: || "update".equals(lkw)
    	 || "set".equals(lkw)
    			) {
            int delta = "select".length() - lkw.length();
            for( int i = 0; i < delta; i++) 
            	padding.append(' ');
    	}
    	return padding.toString() + kw;
    }

    
    // Enum values are expected to be DISINCT as expected by Persist2XML 
    public enum Case {
        UPPER,lower,InitCap,NoCaseChange
    }; 
    public enum Space {
        /*Default,*/Inside,Outside,NoSpace
    }; 
    public enum Breaks {
        Before,After,None, BeforeAndAfter
    }; 
    public enum BreaksX2 {
        X1,X2,Keep
    }; 
    public enum InlineComments {
        CommentsUnchanged, SingleLine, MultiLine
    }; 
    
    
    //////////////////////////// Callbacks: ////////////////////////////
    Map<Integer,Integer> posDepths = new HashMap<Integer,Integer>();  // pos->node.interval()->ancestor count
    int commasCount = 0;
    Map<Integer, String> newlinePositions = new HashMap<Integer, String>(); 
    Map<String,String> casedIds = new HashMap<String,String>();  // node.interval()->cased id
    Map<String,Integer> maxIdLengthInScope = new HashMap<String,Integer>();  // node.interval()->max id length
    Map<Integer,String> ids2scope = new HashMap<Integer,String>();  // node.to->scope.interval()
    Map<Integer,Integer> ids2interval = new HashMap<Integer,Integer>();  // node.to->node.from
    Set<Integer> notPaddedParenthesis = new HashSet<Integer>();  
    private void initCallbackScaffolds() {
    	posDepths = new HashMap<Integer,Integer>();
    	commasCount = 0;
    	newlinePositions = new HashMap<Integer, String>(); 
    	casedIds = new HashMap<String,String>();
        maxIdLengthInScope = new HashMap<String,Integer>();  
        ids2scope = new HashMap<Integer,String>();  
        ids2interval = new HashMap<Integer,Integer>();  
        notPaddedParenthesis = new HashSet<Integer>();  
    }
    
    // debug 
    public void closestPrecursorDescendant( Parsed target, Map<String,ParseNode> tuple ) {
        final String m = new Object(){}.getClass().getEnclosingMethod().getName();
        System.out.println(m+".  "+MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
    }
    public void ancestorDescendant( Parsed target, Map<String,ParseNode> tuple ) {
        final String m = new Object(){}.getClass().getEnclosingMethod().getName();
        System.out.println(m+".  "+MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
    }
    // end debug
    
    public void indentedNodes1( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(tuple.toString());
        // Less verbose:
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
    	
    	ParseNode node = tuple.get("node");
    	if( node == null )
    		throw new AssertionError("node == null");
    	for( int i = node.from; i < node.to; i++ ) {
    		Integer posDepth = posDepths.get(i);
            if( posDepth == null )
                posDepth = 0;
            posDepth++;
            posDepths.put(i,posDepth);
		}
    }
    
    /**
     * Called after nodeDepths have been calculated
     * Sets positions with corresponding indentations strings
     * @param target
     * @param tuple
     */
    public void indentedNodes2( Parsed target, Map<String,ParseNode> tuple ) {        
        //System.out.println(posDepths.toString());
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        
        ParseNode node = tuple.get("node");
        int pos = node.from;
        Integer depth = depth(pos);
                
        int priorDepth = -1;
        if( 0 < pos ) 
        	priorDepth = depth(pos-1);
        if( priorDepth != depth )
        	newlinePositions.put(pos, "\n"+spaceSequence(depth));
        if( target.getSrc().size() <= node.to )
            return;
        
        pos = node.to;
        //if( depth(pos) != depth )
        	newlinePositions.put(pos, "\n"+spaceSequence(depth(pos)));        
    }
        
    private int depth( int pos ) {
    	Integer ret = posDepths.get(pos);
    	if( ret == null )
    		return 0;
    	return ret;
    }
        
    /**
     * Finds all identifiers (which are not keywords) and case them
     * @param target
     * @param tuple
     */
    public void identifiers( Parsed target, Map<String,ParseNode> tuple ) {
        ParseNode node = tuple.get("identifier");
        String id = target.getSrc().get(node.from).content;
        if( "TO_CHAR".equals(id.toUpperCase())  // not in grammar
         || "TO_DATE".equals(id.toUpperCase())
         || "DECODE".equals(id.toUpperCase())
         || "SYSDATE".equals(id.toUpperCase())
         || "REF".equals(id.toUpperCase())
         || "VARCHAR".equals(id.toUpperCase())
         || "NULL".equals(id.toUpperCase())
         || "RAISE".equals(id.toUpperCase())
         || "EXIT".equals(id.toUpperCase())
        ) return;
        id = adjustCase(id,options.get(idCase));
        casedIds.put(node.interval(), id);
    }

    /**
     * processing paddedIdsInScope[properties id]
     * @param target
     * @param tuple
     */
    public void paddedIdsInScope( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        ParseNode properties = tuple.get("scope");
        Integer maxLen = maxIdLengthInScope.get(properties.interval());
        if( maxLen == null )
            maxLen = 0;
        ParseNode id = tuple.get("id");
        ParseNode id1 = tuple.get("id+1");
        String name = target.getInput().substring(target.getSrc().get(id.from).begin,target.getSrc().get(id.to-1).begin);
        if( 0 < name.indexOf('\n') )
        	return;
        int idLen = 0;
        for( int i = id.from; i < id.to; i++ ) {
			idLen += target.getSrc().get(i).content.length();
		}
        if( maxLen < idLen  )
            maxLen = idLen;
        maxIdLengthInScope.put(properties.interval(), maxLen);
        ids2scope.put(id.to,properties.interval());
        ids2interval.put(id.to,id.from);
    }
    
    public void pairwiseAlignments( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        ParseNode node = tuple.get("node");
        if( newlinePositions.get(node.from) == null )
        	return;
        ParseNode predecessor = tuple.get("predecessor");
    	String ident = offset(predecessor.from, target);
    	newlinePositions.put(node.from, ident);
    }
    
    /**
     * Genuine offset of a node within already formatted code
     * WHERE r.last_name IN ( SELECT c.last_name
     * -----------------------^ offset of the node 'SELECT' from the beginning of the line
     */
    private String offset( int pos, Parsed target ) {
    	String ret = "\n";
    	for( int i = pos; pos-50 < i & 0 <= i; i--) {
    		String tmp = newlinePositions.get(i);
    		if( tmp != null && tmp.startsWith("\n\n") )
    			tmp = tmp.substring(1);
    		if( i == 0 )
    			tmp = "\n";
			if( tmp != null ) {
				ret = tmp;
				for( int j = i; j < pos; j++) {
					for( int k = 0; k < padAlign(target.getSrc().get(j).content).length(); k++ ) {
						ret += ' ';
					}
					ret += decideSpace(target, j+1);
				}
				break;
			}				
		}
    	return ret;
    }
    
    
    /**
     * Line breaks which are not part of indentations
     */
    public void extraBrkBefore( Parsed target, Map<String,ParseNode> tuple ) {
    	//System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        ParseNode node = tuple.get("node");
        String padding = newlinePositions.get(node.from);
        if( padding == null ) {
        	int depth = depth(node.from);
        	newlinePositions.put(node.from, "\n"+spaceSequence(depth));
        }
    }
    public void extraBrkAfter( Parsed target, Map<String,ParseNode> tuple ) {
    	//System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        ParseNode node = tuple.get("node");
        String padding = newlinePositions.get(node.to);
        if( padding == null ) {
        	int depth = depth(node.to);
        	newlinePositions.put(node.to, "\n"+spaceSequence(depth));
        }
    }

    /**
     * Separate sql_statements with blank line
     */
    public void brkX2( Parsed target, Map<String,ParseNode> tuple ) {
    	//System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        if( options.get(extraLinesAfterSignificantStatements) != BreaksX2.X2 )
            return;
        ParseNode node = tuple.get("node");
        String padding = newlinePositions.get(node.to);
        if( padding == null )
            newlinePositions.put(node.to, "\n\n");
        else
            newlinePositions.put(node.to, "\n"+padding);
    }
    
    public void ignoreLineBreaksBeforeNode( Parsed target, Map<String,ParseNode> tuple ) {
    	ParseNode node = tuple.get("node");
    	newlinePositions.remove(node.from);
    }

    public void ignoreLineBreaksAfterNode( Parsed target, Map<String,ParseNode> tuple ) {
    	ParseNode node = tuple.get("node");
    	newlinePositions.remove(node.to);
    }



    
    /**
     * all parenthesis which don't want to spacer
     * @param target
     * @param tuple
     */
    public void notPaddedParenthesis( Parsed target, Map<String,ParseNode> tuple ) {  
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        ParseNode paren = tuple.get("paren");
        notPaddedParenthesis.add(paren.from);
    }

}
