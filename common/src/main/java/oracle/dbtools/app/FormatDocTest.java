/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.io.IOException;
import java.util.List;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.plsql.UnitTest;
import oracle.dbtools.util.Service;

public class FormatDocTest {

    /**
     * @param args
     * @throws IOException 
     */
    public static void main( String[] args ) throws Exception {
        test("../parser/plsql/doc/doc_sql.test");
        test("../parser/plsql/doc/doc_plsql.test");
    }

	private static void test( String file ) throws Exception {
        String input = Service.readFile(FormatDocTest.class, file); //$NON-NLS-1$
		List<LexerToken> src =  LexerToken.parse(input, "`"); 
        Parsed parsed = new Parsed(input,src,UnitTest.testParser,"atest");
        ParseNode root = parsed.getRoot();
        test(root, parsed.getSrc(), input);
	}

    final static int offset = 0;
    final static int start = 0;
    static int cnt = offset;
    static void test( ParseNode root, List<LexerToken> src, String input ) throws Exception {        
        //if( 20+start < cnt )
            //return;
        
        if( cnt%19 != 0 ) {
            cnt++;
            return;
        }
        
        if( root.contains(UnitTest.query) ) {
            String sql = query(root,src,input);
            if( sql.length() < 200 )
                return;
            cnt++;
            if( cnt < start ) 
                return;
            
            String output = new Format().format(sql);
            System.out.println("============================================# "+cnt);
            //System.out.println(sql);
            //System.out.println("----------------- Arbori formatter: ------------------");
            System.out.println(output);
            /*System.out.println("----------------- compare with SQLinForm_200: ------------------");
            // borrow class from sqlcli
            final Class<?> sqlPlusFormatterClass = Class.forName("oracle.dbtools.raptor.proformatter.SQLPlusFormatter");
            final Constructor<?> ctr = sqlPlusFormatterClass.getConstructor();
            Object cmp = ctr.newInstance();
            Method m = sqlPlusFormatterClass.getDeclaredMethod("format", ICodingStyleSQLOptions.class, String.class);
            System.out.println(m.invoke(cmp, null, sql));
            return;*/
        }
        
        //if( root.contains(comment) )
           //comment(root,src,input);

        for( ParseNode child : root.children() ) {
            test(child,src, input);   
        }
    }
    
    private static String query( ParseNode root, List<LexerToken> src, String input ) {      
        String ret = input.substring(src.get(root.from).begin, src.get(root.to-1).end);
        if( ret.charAt(0)=='"' )
            ret = ret.substring(1,ret.length()-1);
        if( ret.charAt(0)=='`' )
            ret = ret.substring(1,ret.length()-1);
        return " "+ret+" ";
    }

}
