/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import oracle.dbtools.app.Format.Breaks;
import oracle.dbtools.app.Format.Case;
import oracle.dbtools.app.Format.InlineComments;
import oracle.dbtools.app.Format.Space;
import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.SqlProgram;
import oracle.dbtools.arbori.Tuple;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Service;

public class FormattingPreferences {
	public static boolean debug = false;
    public static void invokeAllGuessMethods( String sample ) {
		List<LexerToken> src = LexerToken.parse(sample);
		SqlEarley earley = SqlEarley.getInstance();
		Matrix matrix = new Matrix(earley);
		ParseNode root = earley.parse(src);
        Parsed target = null;
        if( Format.programInstance == null )
			try {
				Format.programInstance = new SqlProgram(Service.readFile(Format.class, Format.path+"format.prg"));
			} catch( IOException e1 ) {
				e1.printStackTrace();
			}
    	target = new Parsed(sample, src, root);
    	Map<String, MaterializedPredicate> rels = Format.programInstance.eval(target);

    	for( Method m : FormattingPreferences.class.getDeclaredMethods() ){
    		if( !m.getName().startsWith("guess") )
    			continue;
    		if( !Modifier.isPublic(m.getModifiers()) )
    			continue;
    		try {
				m.invoke(null, sample, src, root, rels);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
    	}
    	
    }
		
    public static void guessSingleLineComments( String sample, List<LexerToken> srcDummy, ParseNode rootDummy, Map<String, MaterializedPredicate> rels ) {
    	int slComments = 0;
    	int mlComments = 0;
		List<LexerToken> src = LexerToken.parse(sample,true);
		for( LexerToken t : src ) {
			if( t.type == Token.COMMENT )
				mlComments++;
			else if( t.type == Token.LINE_COMMENT )
				slComments++;				
		}
    	if( debug ) {
    		System.out.println( "==================SingleLineComments==================" );
    		System.out.println("slComments="+slComments);
    		System.out.println("mlComments="+mlComments);
    	}

    	if( 3 < slComments && mlComments*20 < slComments )
    		Format.options.put(Format.singleLineComments, InlineComments.SingleLine);
    	else if( 3 < mlComments && slComments*20 < mlComments )
    		Format.options.put(Format.singleLineComments, InlineComments.MultiLine);
    	else	
    		Format.options.put(Format.singleLineComments, InlineComments.CommentsUnchanged);
    }
    
    public static void guessKwCase( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	guessCase(sample, src, root, true);
    }
    public static void guessIdCase( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	guessCase(sample, src, root, false);
    }
    private static void guessCase( String sample, List<LexerToken> src, ParseNode root, boolean isKw ) {   	
    	int cntUpper = 0;
    	int cntLower = 0;
    	int cntInitcap = 0;
    	for( ParseNode desc : root.descendants() ) {
			if( desc.from+1 != desc.to )
				continue;
			if( desc.contains(SqlEarley.getInstance().identifier) == isKw )
				continue;
			String word = src.get(desc.from).content;
			if( !Character.isLetter(word.charAt(0)) )
				continue;
			if( word.length() < 2 )
				continue;
			if( Character.isUpperCase(word.charAt(0)) && Character.isUpperCase(word.charAt(1)) ) {
				cntUpper++;
				continue;
			}
			if( Character.isUpperCase(word.charAt(0)) && Character.isLowerCase(word.charAt(1)) ) {
				cntInitcap++;
				continue;
			}
			if( Character.isLowerCase(word.charAt(0)) && Character.isLowerCase(word.charAt(1)) ) {
				cntLower++;
				continue;
			}
		}
    	Format.Case ret = Format.Case.NoCaseChange;
    	if( cntLower*9 < cntUpper && cntInitcap*10 < cntUpper )
    		ret = Format.Case.UPPER;
    	if( cntUpper*9 < cntLower && cntInitcap*10 < cntLower )
    		ret = Format.Case.lower;
    	if( cntLower*9 < cntInitcap && cntUpper*10 < cntInitcap )
    		ret = Format.Case.InitCap;
    	if( debug ) {
    		System.out.println( "=================="+(isKw?"keywords":"identifiers")+"==================" );
    		System.out.println("Upper="+cntUpper);
    		System.out.println("Lower="+cntLower);
    		System.out.println("Initcap="+cntInitcap);
    	}
    	Format.options.put(isKw? Format.kwCase: Format.idCase, ret);
    }
    
    public static void guessSpaces( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) {    	
    	int[] idents = new int[200];
        StringTokenizer st = new StringTokenizer(sample, "\n");
        int priorFullIndent = -1;
        LinkedList<Integer> priorRelativeIndents = new LinkedList<Integer>();
        while( st.hasMoreTokens() ) {
        	String token = st.nextToken();
        	token = token.replace("\r", "");
        	token = token.replace("\t", "");
        	if( token.length() == 0 )
        		continue;
        	int ident = 0;
        	for( ; ident < token.length() && token.charAt(ident)==' '; ident++ ) 
				;
        	if( priorFullIndent == -1 ) {
        		priorFullIndent = ident;
        		priorRelativeIndents.add(ident);
        		continue;
        	}
        	int index = ident - priorFullIndent;
        	if( index == 0 ) {
        		Integer tmp = priorRelativeIndents.getLast();
        		if( tmp != 0 )
        			index = tmp;
        	}
        	if( index < 0 ) {
        		index = -index;
        		priorRelativeIndents.removeLast();
        	} else {
        		if( priorFullIndent < ident)
        			priorRelativeIndents.add(index);
        	}
        	if( index != 0 )
        		idents[index]++;
    		priorFullIndent = ident;
        }
     	if( debug ) {
    		System.out.println( "=================="+"spaces"+"==================" );
    		for( int i = 0; i < idents.length; i++ ) {
				if( idents[i] != 0 )
					System.out.println("idents["+i+"]="+idents[i]);
			}
    	}
       	int ret = 1;
		for( int i = 1; i < idents.length; i++ ) {
			if( idents[ret] < idents[i] )
				ret = i;
		}
    	Format.options.put(Format.identSpaces, ret);
    }
    
    //public static String formatThreshold = "formatThreshold";
    
    private static void guessAlign( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels, String idSymbol, String id1Symbol, String scopeSymbol, String formatOpt ) { 
    	int aligned = 0;
    	int misaligned = 0;
    	
    	Map<ParseNode, List<Integer>> offsets = new TreeMap<ParseNode, List<Integer>>();
    	
    	MaterializedPredicate idsInTheScope = rels.get("paddedIdsInScope");
		for( Tuple t : idsInTheScope.getTuples() ) {
			ParseNode id = idsInTheScope.getAttribute(t, "id");
			ParseNode id1 = idsInTheScope.getAttribute(t, "id+1");
			ParseNode scope = idsInTheScope.getAttribute(t, "scope");
			if( idSymbol!=null && !id.contains(idSymbol) )
				continue;
			if( id1Symbol!=null && !id1.contains(id1Symbol) )
				continue;
			if( scopeSymbol!=null && !scope.contains(scopeSymbol))
				continue;
			int nlPos =  sample.substring(0,src.get(id.from).begin).lastIndexOf('\n');
			List<Integer> localOffsets = offsets.get(scope);
			if( localOffsets == null ) {
				localOffsets = new LinkedList<Integer>();
				offsets.put(scope,localOffsets);
			}
			localOffsets.add(src.get(id1.from).begin-nlPos);
		}
		for( ParseNode scope : offsets.keySet() ) {
			List<Integer> localOffsets = offsets.get(scope);
			Integer prior = null;
			for( int i : localOffsets ) {
				if( prior == null ) {
					prior = i;
					continue;
				}
				if( prior == i )
					aligned++;
				else
					misaligned++;
				prior = i;
			}
		}
     	if( debug ) {
    		System.out.println( "=================="+formatOpt +"==================" );
    		//System.out.println(offsets);
    		System.out.println("aligned="+aligned);
    		System.out.println("misaligned="+misaligned);
    	}
     	if( 0 < aligned || 0 < misaligned )
     		Format.options.put(formatOpt , 2 < aligned && misaligned*5 < aligned );
    }
    
    public static void guessAlignTypeDecl( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	guessAlign(sample, src, root, rels, null, "datatype", null, Format.alignTypeDecl);
    	guessAlign(sample, src, root, rels, "decl_id", null, null, Format.alignTypeDecl);
    }
    public static void guessAlignNamedArgs( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	guessAlign(sample, src, root, rels, "sim_expr", "'='", "paren_expr_list", Format.alignNamedArgs);
    }
    public static void guessAlignAssignments( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	guessAlign(sample, src, root, rels, "name", "':'", null, Format.alignAssignments);
    }
    public static void guessAlignEquality( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	guessAlign(sample, src, root, rels, "column", "'='", "where_clause", Format.alignEquality);
    }
    
    public static void guessUseTab( String sample, List<LexerToken> srcDummy, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	int tabNo = 0;
    	int spaceNo = 0;
        StringTokenizer st = new StringTokenizer(sample, "\n");
        while( st.hasMoreTokens() ) {
        	String token = st.nextToken();
        	token = token.replace("\r", "");
        	if( token.startsWith("\t") )
        		tabNo++;
        	if( token.startsWith(" ") )
        		spaceNo++;
        }
    	if( debug ) {
    		System.out.println( "==================UseTab==================" );
    		System.out.println("tabNo="+tabNo);
    		System.out.println("spaceNo="+spaceNo);
    	}
    	Format.options.put(Format.useTab, spaceNo < tabNo );    	
    }
    
    private static void guessBreaks( String sample, String symbol, String fmtOption ) { 
    	int before = 0;
    	int after = 0;
    	int noBreak = 0;
        StringTokenizer st = new StringTokenizer(sample, "\n");
        while( st.hasMoreTokens() ) {
        	String token = st.nextToken();
        	token = token.replace("\r", "");
        	token = token.replace("\t", "");
        	token = token.replace(" ", "");
        	if( token.startsWith(symbol) )
        		before++;
        	if( token.endsWith(symbol) )
        		after++;
        	if( token.indexOf(symbol) != token.lastIndexOf(symbol) )
        		noBreak++;
        }
    	if( debug ) {
    		System.out.println( "=================="+fmtOption+"==================" );
    		System.out.println("before="+before);
    		System.out.println("after="+after);
    		System.out.println("noBreak="+noBreak);
    	}
    	if( before > after && before > noBreak)
    		Format.options.put(Format.breaksComma, Breaks.Before); 
    	else if( before < after && noBreak < after )
    		Format.options.put(Format.breaksComma, Breaks.After); 
    	else if( before < noBreak && after < noBreak )
    		Format.options.put(Format.breaksComma, Breaks.None);    	
    }    
    public static void guessBreaksComma( String sample, List<LexerToken> srcDummy, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	guessBreaks(sample, ",", Format.breaksComma);
    }
    public static void guessBreaksConcat( String sample, List<LexerToken> srcDummy, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	guessBreaks(sample, "||", Format.breaksConcat);
    }
    
    private static boolean matchesSyntax( ParseNode node, String syntax ){
    	if( node == null )
    		return false;
    	if( node.contains(syntax) )
    		return true;
    	ParseNode child = node.parent();
    	return matchesSyntax(child, syntax);
    }    
    private static long balanceOfBreaksAfterToken( String sample, List<LexerToken> src, ParseNode root, String symbol, String syntax ) {
		return balanceOfBreaksAroundToken(sample, src, root, symbol, syntax, true );
	}
    private static long balanceOfBreaksAroundToken( String sample, List<LexerToken> src, ParseNode root, String symbol, String syntax, boolean after ) {
    	int total = 0;
    	int matches = 0;
    	int pos = -1;
		LexerToken prior = null;
    	for( LexerToken t : src ) {
    		pos++;
			if( symbol.equalsIgnoreCase(t.content) ) {
    			if( sample.length() < t.end+2 )
    				break;
    			if( !matchesSyntax(root.leafAtPos(pos), syntax) )
    				continue;
    			total++; 
    			String tmp = sample.substring(t.end, t.end+2);
    			if( !after ) {
    				if( prior == null )
    					continue;    				
    				tmp = sample.substring(prior.end, prior.end+2);
    			}
    			if( tmp.startsWith("\n") )
    				matches++;
    			else if( tmp.startsWith("\r\n") )
    				matches++;
    			
    		}
    		prior = t;
    	}
    	if( debug ) {
    		System.out.println( "==================balanceOfBreaks "+(after?"After":"Before")+" Token "+symbol+"==================" );
    		System.out.println("matches="+matches);
    		System.out.println("total="+total);
    	}
    	return Service.lPair(matches, total);
    }
    public static void guessBreaksAroundLogicalConjunctions( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long pair = balanceOfBreaksAfterToken(sample, src, root, "OR", "condition");
    	int matches = Service.lX(pair);
    	int total = Service.lY(pair);
    	pair = balanceOfBreaksAfterToken(sample, src, root, "AND", "condition");
    	matches += Service.lX(pair);
    	total += Service.lY(pair);
    	pair = balanceOfBreaksAfterToken(sample, src, root, "OR", "pls_expr");
    	matches += Service.lX(pair);
    	total += Service.lY(pair);
    	pair = balanceOfBreaksAfterToken(sample, src, root, "AND", "pls_expr");
    	matches += Service.lX(pair);
    	total += Service.lY(pair);
    	final int matchesAfter = matches;
    	final int totalAfter = total;
    	
    	pair = balanceOfBreaksAroundToken(sample, src, root, "OR", "condition", false);
        matches = Service.lX(pair);
    	total = Service.lY(pair);
    	pair = balanceOfBreaksAroundToken(sample, src, root, "AND", "condition", false);
    	matches += Service.lX(pair);
    	total += Service.lY(pair);
    	pair = balanceOfBreaksAroundToken(sample, src, root, "OR", "pls_expr", false);
    	matches += Service.lX(pair);
    	total += Service.lY(pair);
    	pair = balanceOfBreaksAroundToken(sample, src, root, "AND", "pls_expr", false);
    	matches += Service.lX(pair);
    	total += Service.lY(pair);

    	if( 0 < totalAfter & 0 < total )
    		if( totalAfter < matchesAfter*2 & total < matches*2 ) {
    			Format.options.put(Format.breaksAroundLogicalConjunctions, Breaks.BeforeAndAfter);  
    			return;
    		}
    	if( 0 < totalAfter  )
    		if( totalAfter < matchesAfter*2  ) {
    			Format.options.put(Format.breaksAroundLogicalConjunctions, Breaks.After); 
    			return;
    		}
    	if( 0 < total )
    		if( total < matches*2 ) {
    			Format.options.put(Format.breaksAroundLogicalConjunctions, Breaks.Before); 
    			return;
    		}
    	if( 0 < totalAfter & 0 < total )
    		Format.options.put(Format.breaksAroundLogicalConjunctions, Breaks.None); 

    }
    public static void guessBreaksAfterSelect( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long pair = balanceOfBreaksAfterToken(sample, src, root, "SELECT", "query_block");
    	int matches = Service.lX(pair);
    	int total = Service.lY(pair);
    	if( 0 < total )
    		Format.options.put(Format.breaksAfterSelect, 0 < matches);  
    }

	public static void guessBreakAfterCase( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long pair = balanceOfBreaksAfterToken(sample, src, root, "CASE", "case_expression");
    	int matches = Service.lX(pair);
    	int total = Service.lY(pair);
    	pair = balanceOfBreaksAfterToken(sample, src, root, "CASE", "case_expr");
    	matches += Service.lX(pair);
    	total = +Service.lY(pair);
    	if( 0 < total )
    		Format.options.put(Format.breakAfterCase, 0 < matches);  
    }
    public static void guessBreakAfterWhen( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long pair =  balanceOfBreaksAfterToken(sample, src, root, "WHEN", "case_expression");
    	int matches = Service.lX(pair);
    	int total = Service.lY(pair);
    	pair = balanceOfBreaksAfterToken(sample, src, root, "WHEN", "case_expr");
    	matches += Service.lX(pair);
    	total = +Service.lY(pair);
    	if( 0 < total )
    		Format.options.put(Format.breakAfterWhen, 0 < matches);  
    }
    public static void guessBreakAfterThen( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long pair = balanceOfBreaksAfterToken(sample, src, root, "THEN", "case_expression");
    	int matches = Service.lX(pair);
    	int total = Service.lY(pair);
    	pair = balanceOfBreaksAfterToken(sample, src, root, "THEN", "case_expr");
    	matches += Service.lX(pair);
    	total = +Service.lY(pair);
    	if( 0 < total )
    		Format.options.put(Format.breakAfterThen, 0 < matches);  
    }
    public static void guessBreakAfterElse( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long pair =  balanceOfBreaksAfterToken(sample, src, root, "ELSE", "case_expression");
    	int matches = Service.lX(pair);
    	int total = Service.lY(pair);
    	pair = balanceOfBreaksAfterToken(sample, src, root, "ELSE", "case_expr");
    	matches += Service.lX(pair);
    	total = +Service.lY(pair);
    	if( 0 < total )
    		Format.options.put(Format.breakAfterElse, 0 < matches);  
    }
    public static void guessBreakAfterIf( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long pair =  balanceOfBreaksAfterToken(sample, src, root, "IF", "if_stmt");
    	int matches = Service.lX(pair);
    	int total = Service.lY(pair);
    	if( 0 < total )
    		Format.options.put(Format.breakAfterIf, 0 < matches);  
    }
    public static void guessBreakAfterElseif( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long pair =  balanceOfBreaksAfterToken(sample, src, root, "ELSIF", "if_stmt");
    	int matches = Service.lX(pair);
    	int total = Service.lY(pair);
    	if( 0 < total )
    		Format.options.put(Format.breakAfterElseif, 0 < matches);  
    }
    public static void guessBreakAfterWhile( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long pair =  balanceOfBreaksAfterToken(sample, src, root, "WHILE", "iteration_scheme");
    	int matches = Service.lX(pair);
    	int total = Service.lY(pair);
    	if( 0 < total )
    		Format.options.put(Format.breakAfterWhile, 0 < matches);  
    }

    public static void guessCommasPerLine( String sample, List<LexerToken> srcDummy, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	int lines = 0;
    	int commas = 0;
    	boolean foundComma = false;
        StringTokenizer st = new StringTokenizer(sample, "\n,", true);
        while( st.hasMoreTokens() ) {
        	String token = st.nextToken();
        	if( "\n".equals(token) ) {
        		if( foundComma )
        			lines++;
        		foundComma = false;
        		continue;
        	}
        	if( ",".equals(token) ) {
        		foundComma = true;
        		commas++;
        		continue;
        	}
        }
    	if( debug ) {
    		System.out.println( "==================commasPerLine==================" );
    		System.out.println("lines="+lines);
    		System.out.println("commas="+commas);
    	}
    	Format.options.put(Format.commasPerLine, (commas+1)/(lines+1));    	
    }
    
    public static void guessMaxCharLineSize( String sample, List<LexerToken> srcDummy, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	int maxLen = 0;
        StringTokenizer st = new StringTokenizer(sample, "\n,", true);
        while( st.hasMoreTokens() ) {
        	String token = st.nextToken();
        	if( maxLen < token.length() )
        		 maxLen = token.length();       	
        }
    	if( debug ) {
    		System.out.println( "==================MaxCharLineSize==================" );
    		System.out.println("maxLen="+maxLen);
    	}
    	if( maxLen > 128 )
    		Format.options.put(Format.maxCharLineSize, maxLen);    	
    }
    
    public static String breakOnSubqueries = "breakOnSubqueries";
    public static String breakAnsiiJoin = "breakAnsiiJoin";
    public static String breakParenCondition = "breakParenCondition";
    public static void guessBreakOnSubqueries( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	decideBreakOn(sample, src, new String[]{"subquery",}, new String[]{}, rels, Format.breakOnSubqueries);
    }    
    public static void guessBreakAnsiiJoin( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	decideBreakOn(sample, src, new String[]{"table_reference","condition",}, new String[]{"on_using_condition","\"inner_cross_join_clause\"","cross_outer_apply_clause",}, rels, Format.breakAnsiiJoin);
    }    
    public static void guessBreakParenCondition( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	decideBreakOn(sample, src, new String[]{"'('",}, new String[]{"compound_condition",}, rels, Format.breakParenCondition);
    }    
    private static void decideBreakOn( String sample, List<LexerToken> src, String[] nodeConditions, String[] parentConditions, Map<String, MaterializedPredicate> rels, String param ) {
    	int yes = 0;
    	int no = 0;
    	MaterializedPredicate formattedNodes = rels.get("indentedNodes1");
		for( Tuple t : formattedNodes.getTuples() ) {
			ParseNode node = formattedNodes.getAttribute(t, "node");
			boolean proceed = nodeConditions.length==0;
			for( String nodeCondition : nodeConditions )
				if( node.contains(nodeCondition) ) {
					proceed = true;
					break;
				}
			if( !proceed )
				continue;
			proceed = parentConditions.length==0;
			for( String parentCondition : parentConditions )
				if( node.parent() != null)
					if( node.parent().contains(parentCondition) ) {
						proceed = true;
						break;
					}
			if( !proceed )
				continue;
			/*proceed = grandParentConditions.length==0;
			for( String grandParentCondition : grandParentConditions )
				if( node.parent() != null && node.parent().parent() != null )
					if( node.parent().parent().contains(grandParentCondition) ) {
						proceed = true;
						break;
					}
			if( !proceed )
				continue;*/
			if( node.from == 0 )
				continue;
			int from = src.get(node.from-1).end;
			int to = src.get(node.from).begin;
			String padding = sample.substring(from,to);
			if( 0 < padding.indexOf('\n')  )
				yes++;
			else
				no++;
			
		}
    	if( debug ) {
    		System.out.println( "==================BreakOn "+nodeConditions[0]+"==================" );
    		System.out.println("yes="+yes);
    		System.out.println("no="+no);
    	}
		if( 0 < yes || no < 0)
			Format.options.put(param, no < yes);

    }
    
    public static void guessForceLinebreaksBeforeComment( String sample, List<LexerToken> srcDummy, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	int yes = 0;
    	int no = 0;
		List<LexerToken> src = LexerToken.parse(sample,true);
    	LexerToken prior = null;
    	for( LexerToken t : src ) {
    		if( prior != null && (t.type == Token.COMMENT || t.type == Token.LINE_COMMENT) ) {
				//System.out.println(t.content);
				int from = prior.end;
				int to = t.begin;
				String padding = sample.substring(from,to);
				if( padding.indexOf('\n') != padding.lastIndexOf('\n') )
					yes++;
				else
					no++;
				continue;
    		}
    		if( /*t.type != Token.COMMENT && t.type != Token.LINE_COMMENT &&*/ t.type != Token.WS )
    			prior = t;
    	}
    	if( debug ) {
    		System.out.println( "==================ForceLinebreaksBeforeComment==================" );
    		System.out.println("yes="+yes);
    		System.out.println("no="+no);
    	}
    	if( 0 < yes || no < 0 )
    		Format.options.put(Format.forceLinebreaksBeforeComment,  no < yes);  
    }
    
    public static void guessExtraLinesAfterSignificantStatements( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	int yes = 0;
    	int no = 0;
    	MaterializedPredicate extraLines = rels.get("extraLines");
		for( Tuple t : extraLines.getTuples() ) {
			ParseNode node = extraLines.getAttribute(t, "node");
			if( node.to < src.size() ) {
				//System.out.println(sample.substring(src.get(node.from).begin,src.get(node.to-1).end));
				int from = src.get(node.to-1).end;
				int to = src.get(node.to).begin;
				String padding = sample.substring(from,to);
				if( padding.indexOf('\n') != padding.lastIndexOf('\n') )
					yes++;
				else
					no++;
			} else
				no++;
		}
    	if( debug ) {
    		System.out.println( "==================ExtraLinesAfterSignificantStatements==================" );
    		System.out.println("yes="+yes);
    		System.out.println("no="+no);
    	}
    	if( 0 < yes || no < 0 )
    		Format.options.put(Format.extraLinesAfterSignificantStatements,  no < yes? Format.BreaksX2.X2: Format.BreaksX2.X1);
    }
   
    private static long spaceAroundToken( String sample, List<LexerToken> src, ParseNode root, String symbol ) {
    	int before = 0;
    	int after = 0;
    	int cnt = 0;
    	for( ParseNode node : root.descendants() ) {
    		if( !node.contains(symbol) )
    			continue;
    		if( src.get(node.to-1).end < sample.length() && sample.charAt(src.get(node.to-1).end) == '\n' )
    			continue;
    		if( src.get(node.to-1).end < sample.length() && sample.charAt(src.get(node.to-1).end) == '\r' )
    			continue;
    		if( 0 < src.get(node.from).begin && sample.charAt(src.get(node.from).begin-1) == ' ' )
    			before++;
    		if( src.get(node.to-1).end < sample.length() && sample.charAt(src.get(node.to-1).end) == ' ' )
    			after++;
    		cnt++;
    	}
    	if( debug ) {
    		System.out.println( "================== spaceAfterToken "+symbol+" ==================" );
    		System.out.println("before="+before);
    		System.out.println("after="+after);
    		System.out.println("cnt="+cnt);
    	}
    	return Service.lPair(cnt, Service.pair(before,after));
    }
    public static void guessSpaceAroundOperators( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long cba = spaceAroundToken( sample, src, root, "relal_op" );
    	int cnt = Service.lX(cba);
    	int ba = Service.lY(cba);
    	int before = Service.X(ba);
    	int after = Service.Y(ba);
    	cba = spaceAroundToken( sample, src, root, "cmp_op" );
    	cnt += Service.lX(cba);
    	ba = Service.lY(cba);
    	before += Service.X(ba);
    	after += Service.Y(ba);
    	if( 0 < cnt )
    		Format.options.put(Format.spaceAroundOperators,  before*3 >= cnt*2 && after*3 >= cnt*2);
    }
    public static void guessSpaceAfterCommas( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long cba = spaceAroundToken( sample, src, root, "','" );
    	int cnt = Service.lX(cba);
    	int ba = Service.lY(cba);
    	//int before = Service.X(ba);
    	int after = Service.Y(ba);
    	if( 0 < cnt )
    		Format.options.put(Format.spaceAfterCommas,  after*3 >= cnt*2);
    }
    public static void guessSpaceAroundBrackets( String sample, List<LexerToken> src, ParseNode root, Map<String, MaterializedPredicate> rels ) { 
    	long cba1 = spaceAroundToken( sample, src, root, "'('" );
    	int cnt1 = Service.lX(cba1);
    	int ba1 = Service.lY(cba1);
    	int before1 = Service.X(ba1);
    	int after1 = Service.Y(ba1);
    	long cba2 = spaceAroundToken( sample, src, root, "')'" );
    	int cnt2 = Service.lX(cba2);
    	int ba2 = Service.lY(cba2);
    	int before2 = Service.X(ba2);
    	int after2 = Service.Y(ba2);
    	Object o = Space.NoSpace;
    	if( (before1+after2)*3 >= (cnt1+cnt2)*2 )
    		o = Space.Outside;
    	if( (before2+after1)*3 >= (cnt1+cnt2)*2 )
    		o = Space.Inside;
    	if( 0 < (cnt1+cnt2) )
    		Format.options.put(Format.spaceAroundBrackets, o);
    }

    public static void main( String[] args ) throws Exception {
    	FormattingPreferences.debug = true;
		String input = Service.readFile(FormattingPreferences.class, "usergrantshelpertest1_b.testsql");
		input = Service.readFile(FormattingPreferences.class, "test.sql");
		//input = Service.readFile(SqlEarley.class, "FND_STATS.pkgbdy");
		invokeAllGuessMethods(input);
	}
}
