/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.Program;
import oracle.dbtools.arbori.SqlProgram;
import oracle.dbtools.arbori.Tuple;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.raptor.query.Parser;
import oracle.dbtools.util.Service;

public class Literals2Binds {
    public final static String prefix = "AUXSQLDBIND";
	
    private String sql;
    private Map<String,String> binds;
    public String getSql() {
        return sql;
    }
    public Map<String,String> getBinds() {
        return binds;
    }

    private Literals2Binds( String sql, Map<String,String> binds ) {
        this.sql = sql;
        this.binds = binds;
    }
    
    public static Literals2Binds Literals2BindsNull(String sql) {
    	return new Literals2Binds(sql, new HashMap<String,String>() );
    }

    /**
     * Recognizes literals within **syntactically correct** anonymous pl/sql block
     * See example usage in main()
     * @param origSql
     * @return
     */
    public static Literals2Binds bindAid( String origSql ) {
        final ArrayList<String> existingBinds = Parser.getInstance().getBindNames(origSql);
        int maxBind = 0;
        for( String eb : existingBinds ) {
            try {
                int candidate = Integer.parseInt(eb);
                if( maxBind < candidate )
                    maxBind = candidate; 
            } catch( NumberFormatException e ) {}            
        }
        List<LexerToken> src =  LexerToken.parse(origSql);
        String sql = origSql;
        Map<String,String> tmp = new HashMap<String,String>();
        //inlined Map<String,Set<Long>> output = o.recognize(input, src, targetSymbol);
        try {
            Program.debug = false;
            String recognitionPrg = Service.readFile(Literals2Binds.class, "/oracle/dbtools/app/literals2binds.prg");
            Map<String,MaterializedPredicate> predicateVectors = new SqlProgram(recognitionPrg).run(origSql);
            
            MaterializedPredicate pragmas = predicateVectors.get("pragmas"); //$NON-NLS-1$
            if( 0 < pragmas.cardinality() )
            	return new Literals2Binds(sql,tmp);
            
            MaterializedPredicate symbols = predicateVectors.get("\"procedureCall\""); //$NON-NLS-1$
            Map<Integer, String> pCall = new TreeMap<Integer,String>();
            for( Tuple t : symbols.getTuples() ) {              
                ParseNode pc = symbols.getAttribute(t, "procedureCall"); //$NON-NLS-1$
                LexerToken f = src.get(pc.from);
                LexerToken t_1 = src.get(pc.to-1);
                pCall.put(f.begin, origSql.substring(f.begin, t_1.end));
            }

            symbols = predicateVectors.get("\"arg\""); //$NON-NLS-1$
            Map<Integer, String> args = new TreeMap<Integer,String>();
            for( Tuple t : symbols.getTuples() ) {              
                ParseNode pc = symbols.getAttribute(t, "arg"); //$NON-NLS-1$
                LexerToken f = src.get(pc.from);
                LexerToken t_1 = src.get(pc.to-1);
                args.put(f.begin, origSql.substring(f.begin, t_1.end));
            }

            int pos = maxBind;
            int adjust = 0; // after insertion
            for( int key : args.keySet() ) {
            	// 23040251 - exclude certain modules
            	boolean skip = false;
                for( int pKey : pCall.keySet() ) {
                	String call = pCall.get(pKey).toLowerCase();
    				if( pKey < key && key < pKey+call.length() ) {
                		if( call.startsWith("dbms_sql") 
                		 || call.startsWith("userenv")
                   		 || call.startsWith("xmltype")
               		     //|| call.startsWith("sys_context(")
                		 //|| call.startsWith("sys_guid(")
                		 //|| call.startsWith("sys_typeid(")
                		 //|| call.startsWith("sys_connect_by_path(")
                		 // generalized:
                		 || call.startsWith("sys_")
                  		)
                			skip = true;
                	}
                }
                if( skip )
                	continue;
            	
                pos++;
                String literal = args.get(key);
                if( 1 < literal.length() && 0 <= literal.substring(1,literal.length()-1).indexOf('\'') )
                	continue;
                boolean isStr = false;
                if( literal.startsWith("'") && literal.endsWith("'") )
                	isStr = true;
                String bind = "TO_NUMBER(:"+prefix+pos+")";
                if( isStr )
                	bind = "TO_CHAR(:"+prefix+pos+")";
                sql = sql.substring(0,key+adjust) + bind + sql.substring(key+literal.length()+adjust);
                adjust += bind.length() - literal.length();
                if( isStr )
                    literal = literal.substring(1, literal.length()-1);
                tmp.put(prefix+pos,literal);
            }            
            
        } catch( SyntaxError e ) {
            throw e;
        } catch( AssertionError e ) {
            System.err.println(e.getMessage());
            return null;
        } catch( Exception e ) {
            e.printStackTrace();
            return null;
        } finally {
            return new Literals2Binds(sql,tmp);
        }
       
    }

    
    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder(sql+"\n");
        for( String bind : binds.keySet()) {
            ret.append(bind+"->"+binds.get(bind)+"\n");            
        }
        return ret.toString();
    }

    public static void test( String plsqlBlock ) throws Exception {
    	DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
    	final Connection c = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe", "hr", "hr");
    	 
    	CallableStatement cs = null;
    	try {
    		cs=c.prepareCall("begin dbms_output.enable(1000000); end;");
    		cs.execute();
    	} finally {
    		if( cs!=null ) {
    			try {
    				cs.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}

    	if( false )
    		cs= c.prepareCall(plsqlBlock);
    	else {
    		Literals2Binds bp = bindAid(plsqlBlock);
            System.out.println(bp.toString());
    		cs= c.prepareCall(bp.getSql());
            Map<String,String> bindsMap = bp.getBinds();
            int pos = 1;
            for( String bindName : bindsMap.keySet() ) {
            	cs.setString(pos++, bindsMap.get(bindName));
            }
    	}
    	try {
			cs.execute();
			System.out.println("No Exception");
    		SQLWarning sw=null;
			sw = cs.getWarnings();

			while (sw !=null) {
				String w = sw.getMessage();
				System.out.println("ZZ: "+w);
				sw=sw.getNextWarning();
			}
			System.out.println("OP: "+getDBMSOUTPUT(c));
    	} finally {

    		if (cs !=null) 
     			cs.close();
    		if (c !=null) 
    			c.close();
    	}
    }

    public static String getDBMSOUTPUT( Connection c ) throws Exception {

    	StringBuilder sb = new StringBuilder();
    	CallableStatement stmt = null;
    	try {
    		//Query q = getXMLQueries().getQuery("GET_DBMSOUTPUT", m_conn); //$NON-NLS-1$
    		stmt = c.prepareCall(
    				"declare\n"
    						+"l_line varchar2(32767);\n"
    						+"l_done number;\n"
    						+"l_buffer varchar2(32767) := '';\n"
    						+"l_lengthbuffer number := 0;\n"
    						+"l_lengthline number := 0;\n"
    						+"begin \n"
    						+"loop \n"
    						+"	dbms_output.get_line( l_line, l_done ); \n"
    						+"	if (l_buffer is null) then \n"
    						+"	  l_lengthbuffer := 0; \n"
    						+"	else \n"
    						+"	  l_lengthbuffer := length(l_buffer); \n"
    						+"	end if; \n"
    						+"	if (l_line is null) then \n"
    						+"	  l_lengthline := 0; \n"
    						+"	else \n"
    						+"	  l_lengthline := length(l_line); \n"
    						+"	end if; \n"
    						+"exit when l_lengthbuffer + l_lengthline > :maxbytes OR l_lengthbuffer + l_lengthline > 32767 OR l_done = 1; \n"
    						+"l_buffer := l_buffer || l_line || chr(10); \n"
    						+"end loop; \n"
    						+":done := l_done; \n" 
    						+":buffer := l_buffer; \n"
    						+":line := l_line; \n"
    						+"end;"

    				);
    		boolean useLargeBuffer = true;//or false...
    		if (useLargeBuffer) {
    			stmt.registerOutParameter(2, java.sql.Types.INTEGER);
    			stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
    			stmt.registerOutParameter(4, java.sql.Types.VARCHAR);
    		} else {
    			stmt.registerOutParameter(2, java.sql.Types.INTEGER);
    			stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
    		}
    		for (;;) {

    			stmt.setInt(1, 1000);

    			stmt.executeUpdate();
    			if (useLargeBuffer) {
    				String got = stmt.getString(3);
    				if (got != null) {
    					sb.append(got);
    				}
    				got = stmt.getString(4);
    				if (got != null) {
    					sb.append(got + "\n"); //$NON-NLS-1$
    				} else {
    					sb.append("\n"); //$NON-NLS-1$
    				}
    			} else {
    				sb.append(stmt.getString(3));
    			}

    			if (stmt.getInt(2) == 1) break;
    		}

    	} finally {
			if (stmt != null) stmt.close();
    	}

    	return sb.toString();
    }
    

    public static void main( String[] args ) throws Exception {
        String[] inputs = {
        		"with   \n" + 
        		        "tab ( parent, child ) as (   \n" + 
        				"  select null, 1 from dual union all   \n" + 
        				"  select 1, 2 from dual   \n" + 
        				")   \n" + 
        				"select ltrim(sys_connect_by_path(child, '/'), '/') as path   \n" + 
        				"from tab   \n" + 
        				"start with parent is null   \n" + 
        				"connect by prior child = parent",       		
        		"declare \n"
						+ "  x integer; \n"
						+ "begin \n"
						+ "  select 1 into x from dual where ABS(11)=11; \n"
						+ "end;",
        		"begin DBMS_LOCK.SLEEP(10);dbms_output.put_line('x');dbms_output.put_line('HELLO'||systimestamp); end;",
        		"begin \n"
        				+ "add_job_history(1,:1,sysdate,:2,1); \n"
        				+ "DBMS_OUTPUT.PUT_LINE('a''b',q'[&USER_ACCOUNT]','Command: ' || sql_text); \n"
        				+ "DBMS_SQL.PARSE(odmruser_cursor, "
        				+ "'select grantee as \"USER\" from dba_role_privs where granted_role = ''ODMRUSER''', DBMS_SQL.NATIVE);  \n"
        				+ "bye(42,'42'); \n"
        				+ " end;\n",
        		Service.readFile(Literals2Binds.class, "usergrantshelpertest1_b.testsql"),
        		Service.readFile(Literals2Binds.class, "test.sql"),
        		//Service.readFile("/temp/createxmlschemaindexestest.sql"),
        };
    	for ( int i = 0; i < inputs.length; i++ ) {
    		System.out.println(">>>>>>>>> "+i);
            test(inputs[i]);
        }  			
	}
}
