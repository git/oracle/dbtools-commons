/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.util.Base64;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.Program;
import oracle.dbtools.arbori.SqlProgram;
import oracle.dbtools.arbori.Tuple;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.plsql.LazyNode;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Service;


public class Obfuscator extends Rewrite { 
    
    private int varCnt = 0; 
    private int changeCnt = 0; 
    
    public Obfuscator( Set<String> fileExtensions ) {
        super(fileExtensions);
    }

    static SecretKey desKey = null;
    static Cipher desCipher = null;
    static {
        try {
            KeyGenerator keygen = KeyGenerator.getInstance("DES");
            desKey = keygen.generateKey();
            desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
        } catch ( Exception e ) {
        }
    }
    
    private static String rename( String input ) {
        try {
            desCipher.init(Cipher.ENCRYPT_MODE, desKey);
            byte[] cleartext = input.getBytes();
            byte[] ciphertext = desCipher.doFinal(cleartext);
            String output64 = Base64.getEncoder().encodeToString(ciphertext);
            if( 30 < output64.length() ) {
                char last = output64.charAt(output64.length()-1);
                output64 = last + output64.substring(1,30);
            }
            return "\""+output64+"\"";
        } catch ( Exception e ) {
            throw new AssertionError(e);
        }
    }
    
    public static void main( String[] args ) throws Exception {
       
        String input = 
                Service.readFile(Obfuscator.class, "test.sql") //$NON-NLS-1$
            ;
        List<LexerToken> src =  LexerToken.parse(input);
        Obfuscator o = new Obfuscator(null);
        
        String output = o.transform(input, src, null);
        
        System.out.println(output);

    }

    static final String path = "/oracle/dbtools/app/"; //$NON-NLS-1$
    static SqlProgram programInstance = null;
    @Override
    public String transform( String input, List<LexerToken> src, LazyNode notUsed ) {
        try {
            if( programInstance == null ) { 
                programInstance = new SqlProgram(Service.readFile(Obfuscator.class, path+"vars.prg"));
                Program.debug = false;
            }
            Map<String,MaterializedPredicate> predicateVectors = programInstance.run(input,null);
            
            Map<Long, String> substitutions = new TreeMap<Long,String>();

            MaterializedPredicate symbols = predicateVectors.get("\"var entry in scope\""); //$NON-NLS-1$
            changeCnt += symbols.cardinality();
            Set<String> variables = new HashSet<String>();
            for( Tuple tu : symbols.getTuples() ) {              
                ParseNode scope = symbols.getAttribute(tu, "scope"); //$NON-NLS-1$
                ParseNode entry =  symbols.getAttribute(tu, "entry"); //$NON-NLS-1$
                LexerToken t = src.get(entry.from);
                String scopedName = t.content+scope.from+','+scope.to;
                variables.add(scopedName);
                substitutions.put(Service.lPair(t.begin, t.end), rename(scopedName));
            }
            varCnt += variables.size();
            
            StringBuilder output = new StringBuilder();
            int priorLexPos = 0;
            
            //int shift = 0;
            for( long xy : substitutions.keySet() ) {
                int x = Service.lX(xy);
                int y = Service.lY(xy);
                String newName = substitutions.get(xy);
                
                output.append(input.substring(priorLexPos,x));
                priorLexPos = y;
                output.append(newName);
            } 
            output.append(input.substring(priorLexPos));
            return output.toString();

        } catch( SyntaxError e ) {
            throw e;
        } catch( AssertionError e ) {
            System.err.println(e.getMessage());
            return null;
        } catch( Exception e ) {
            e.printStackTrace();
            return null;
        } finally {
        }
        
    }

    @Override
    protected String found() {
        return "found "+varCnt+" symbols, "; //$NON-NLS-1$
    }

    @Override
    protected String changed() {
        return "obfuscated "+changeCnt+" instances";
    }

}
