/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import oracle.dbtools.app.Format.Breaks;
import oracle.dbtools.app.Format.BreaksX2;
import oracle.dbtools.app.Format.Case;
import oracle.dbtools.app.Format.InlineComments;
import oracle.dbtools.app.Format.Space;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.util.Pair;

public class Persist2XML extends XML2Table {

    static public void main( String[] args ) throws Exception {
        /*LinkedList list = new LinkedList();
        list.add(new Pair("A","a"));
        list.add(new Pair("B",11));
        list.add(new Pair("C",true));*/
        HashMap tmp = new HashMap();
        tmp.put("A", "a"); //$NON-NLS-1$
        tmp.put("B", 11); //$NON-NLS-1$
        tmp.put("C", true); //$NON-NLS-1$
        tmp.put("D", Case.lower); //$NON-NLS-1$
        tmp.put("E", "C:\\temp\\format.prg"); //$NON-NLS-1$
        tmp.put("F", "/temp/format.prg"); //$NON-NLS-1$
        URL file = Paths.get("/temp","persist.xml").toUri().toURL(); //$NON-NLS-1$
        write(file,tmp);
        System.out.println(read(file));
    }
    
    public Persist2XML() throws IOException {
        super();
    }
    
    public static void write( URL url, Object what ) throws IOException {
        FileOutputStream fos = new FileOutputStream(url.getFile());
        String xml = "<options>"+dumpXML(what)+"</options>";   //$NON-NLS-1$
        fos.write(xml.getBytes()); 
        fos.close();
    }

    private static String dumpXML( Object what ) {
        if( what instanceof Pair ) {
            Pair pair = (Pair) what;
            return "<"+pair.first().toString()+">"+pair.second().toString()+"</"+pair.first().toString()+">\n";
        }
        if( what instanceof Map ) {
            Map map = (Map) what;
            StringBuilder ret = new StringBuilder();
            for( Object key : map.keySet() ) {
                ret.append("<"+key.toString()+">"+map.get(key).toString()+"</"+key.toString()+">\n");               
            }
            return ret.toString();
        }
        if( what instanceof Object ) {
            StringBuilder ret = new StringBuilder();
            for( Object elem : (Collection)what )
                ret.append(dumpXML(elem));
            return ret.toString();
        }
        throw new AssertionError("what instanceof "+what.getClass().getName());
    }

    public static Map<String,Object> read( URL url ) throws IOException {
        FileInputStream fis = new FileInputStream(url.getFile());
        byte[] bytes = new byte[ 4096 ];
        StringBuffer tmp = new StringBuffer();
        BufferedInputStream bin = new BufferedInputStream(fis);
        int bytesRead = bin.read(bytes, 0, bytes.length);
        while (bytesRead != -1) {
            tmp.append(new String(bytes).substring(0, bytesRead));
            bytesRead = bin.read(bytes, 0, bytes.length);
        }
        Persist2XML xmlProgram = new Persist2XML();
        //xmlProgram.debug = true;
        String input = tmp.toString();
        xmlProgram.run(input, null, xmlProgram);
        return xmlProgram.parameters;
    }
    
    //////////////////////////// Callbacks: ////////////////////////////
    HashMap<String,Object> parameters = new HashMap<String,Object>();
    /**
     * Collecting all tags in a callback
     * @param target
     * @param tuple
     */
    void tupleNodes( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(tuple.toString());
        // Less verbose:
        //System.out.println("**tupleNodes(): **"+MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
    }

    void values( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(tuple.toString());
        // Less verbose:
        //System.out.println("**values(): **"+MaterializedPredicate.tupleMnemonics(tuple,target.getSrc(),true));
        
        //final String value = target.getSrc().get(tuple.get("value").from).content;
        ParseNode node = tuple.get("value");
        final String value = target.getInput().substring(
        		target.getSrc().get(node.from).begin,
        		target.getSrc().get(node.to-1).end		
        );
        Object obj = value;
        if( value.equalsIgnoreCase("false") )
            obj = false;
        if( value.equalsIgnoreCase("true") )
            obj = true;
        try {
            obj = Integer.parseInt(value);
        } catch( NumberFormatException e ) {}
        Object tmp = decodeEnum(Case.values(),value);
        if( tmp != null )
        	obj = tmp;
        tmp = decodeEnum(InlineComments.values(),value);
        if( tmp != null )
        	obj = tmp;
        tmp = decodeEnum(Breaks.values(),value);
        if( tmp != null )
        	obj = tmp;
        tmp = decodeEnum(BreaksX2.values(),value);
        if( tmp != null )
        	obj = tmp;
        tmp = decodeEnum(Space.values(),value);
        if( tmp != null )
        	obj = tmp;
        parameters.put(target.getSrc().get(tuple.get("attribute").from).content, 
                       obj);
    }
    
    private Object decodeEnum( Object[] values, String cmp ) {
    	for( Object o : values )
    		if( cmp.equalsIgnoreCase(o.toString()) )
				return o;
    	return null;
    }
}
