/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.StringTokenizer;

import oracle.dbtools.util.Service;

public class Profiler {
    public static void main( String[] args ) throws Exception {
    	DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
        Connection c = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe","hr", "hr"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        c.setAutoCommit(true);
        PreparedStatement stmt = c.prepareStatement("insert into function_sequences1(stack_id,id,name)values(?,?,?)");
        String input = Service.readFile("C:\\Users\\Dim\\Downloads\\apex_5.1.1_en\\apex\\log.txt"); //$NON-NLS-1$
        StringTokenizer st = new StringTokenizer(input,"\n");
        int stackId = 0;
        int id = 0;
        while( st.hasMoreTokens() ) {
        	String line = st.nextToken()/*.intern()*/;
        	if( line.contains("-------") ) {        		
        		stackId++;
        		id = 0;
        		continue;
        	}
        	stmt.setInt(1,stackId);
        	stmt.setInt(2,id);
            StringTokenizer st1 = new StringTokenizer(line,".",true);
            StringBuilder abbr = new StringBuilder();
            while( st1.hasMoreTokens() ) {
            	String chunk = st1.nextToken()/*.intern()*/;
            	if( 0 < chunk.indexOf('(') || 0 < chunk.indexOf(':') )
            		abbr.append(chunk);
            	else
            		abbr.append(chunk.charAt(0));
            }
        	stmt.setString(3,abbr.toString());
        	stmt.execute();
        	id++;
        }
        c.commit();
 
    }
}
