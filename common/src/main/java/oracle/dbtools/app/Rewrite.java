/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.plsql.LazyNode;
import oracle.dbtools.parser.plsql.StackParser;
import oracle.dbtools.util.Service;

/**
 * Transform content of a file
 * Example 1: SqlId
 * Exabple 2: Obfuscate
 * @author Dim
 *
 */
public abstract class Rewrite {

    public int fileCnt = 0; 
    
    private Set<String> fileExtensions;
    
    public Rewrite( Set<String> fileExtensions ) {
        this.fileExtensions = fileExtensions;
    }

    public void walkDir( File file ) throws FileNotFoundException, Exception {
        if( file.isDirectory() )
            for( File child : file.listFiles() ) {
                //System.out.println(fileName);
                walkDir(child);
            }
        else
            rewrite(file);       
    }
    
    public void rewrite( File file ) throws FileNotFoundException, Exception {
        String fileName = file.getName();
        if( fileName.length() > 4 ) {
            int dotPos = fileName.lastIndexOf('.');
            if( dotPos >= 0 && dotPos < fileName.length()-3 ) {
                String suffix = "*"+fileName.substring(dotPos).toLowerCase();
                if( 
                        fileExtensions.contains("*.*")  
                    ||  fileExtensions.contains(suffix) 
                ) {
                    String input = Service.readFile(new FileInputStream(file));
                    //System.out.println(file);
                    List<LexerToken> src =  LexerToken.parse(input);

                    LazyNode root = StackParser.getInstance().parse(src);
                    
                    String output = transform(input, src, root);
                    
                    if( output != null ) {                   
                        FileWriter fstream = new FileWriter(file);
                        BufferedWriter out = new BufferedWriter(fstream);
                        out.write(output.toString());
                        out.close();
                    }
                    
                    fileCnt++;
                }
            }
        }
    }
    
    abstract public String transform(String input, List<LexerToken> src, LazyNode root);
    
    abstract protected String found();
    abstract protected String changed();

    public String log() {
        return "Processed "+fileCnt+" files, " + found() + changed(); //$NON-NLS-1$
    }
    
    public static void main( String[] args ) throws Exception {
        String usage = "Usage: java -jar <utils-nodeps.jar> \n" + //$NON-NLS-1$
                "<command> (e.g. \"sqlid\", \"obfuscate\") \n" + //$NON-NLS-1$
                "<directory> <sqlid_prefix>) \n" + //$NON-NLS-1$
                "e.g. \n" + //$NON-NLS-1$
                "java -jar C:\\sqld\\sqldeveloper\\lib\\oracle.sqldeveloper.utils-nodeps.jar\n" + //$NON-NLS-1$
                "obfuscate . \n" + //$NON-NLS-1$
                "java -jar C:\\sqld\\sqldeveloper\\lib\\oracle.sqldeveloper.utils-nodeps.jar\n" + //$NON-NLS-1$
                "sqlid C:\\Apex_code APEX";
        File dir;
        switch( args.length ) {
            default :
                //File dir = new File("C:/Documents and Settings/dim");
                //walkDir(dir);
                System.out.println(usage); //$NON-NLS-1$
                return;
            case 2: case 3:
                dir = new File(args[1]);
            HashSet<String> fileExts = new HashSet<String>();
            fileExts.add("*.plb"); //$NON-NLS-1$
            fileExts.add("*.pls"); //$NON-NLS-1$
            fileExts.add("*.pkb"); //$NON-NLS-1$
            if( !dir.exists() )
                System.out.println("Specified directory does not exist."); //$NON-NLS-1$
            Rewrite instance = null;
            if( "sqlid".equalsIgnoreCase(args[0]) && args.length == 3 )
                instance = new SqlId(args[2],fileExts);
            else if( "obfuscate".equalsIgnoreCase(args[0]) && args.length == 2 )
                instance = new Obfuscator(fileExts);
            else {
                System.out.println(usage); //$NON-NLS-1$
                return;
            }
            instance.walkDir(dir);
            System.out.println(instance.log()); 
            return;
        }
    }


}
