/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLRecoverableException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import oracle.dbtools.app.CompletionItem.Type;
import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.SqlProgram;
import oracle.dbtools.arbori.Tuple;
import oracle.dbtools.parser.Cell;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.parser.plsql.doc.HarvestDoc;
import oracle.dbtools.raptor.newscriptrunner.commands.HiddenParameters;
import oracle.dbtools.raptor.newscriptrunner.commands.net.NetEntries;
import oracle.dbtools.util.Service;

/**
 * Code completion to use in sqlcl and sqldevweb
 * @author Dim
 */
public class SqlCompleter  {
    
    private static final String color32 = "\u001B[32m";

    private static final String color1 = "\u001B[1m";

    private static final String color0 = "\u001B[0m";

    SqlEarley earley = null; 
    
    static int approximateIndex = -1; // if no token at cursor
    public static void main( String[] args ) throws Exception {
        Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe","hr","hr"); 
        //conn = DriverManager.getConnection("jdbc:oracle:thin:@llg00hon.uk.oracle.com:1521/DB12201","hr","hr"); 
        SqlCompleter c = new SqlCompleter(conn);
        String input = 
            Service.readFile(SqlCompleter.class, "test.sql") //$NON-NLS-1$
        ;
        input = "select * from employees  where !";
        int pos = input.indexOf("!");
        input = input.replace("!", "");
        System.out.println("pos="+pos);
        CompletionList candidates = c.complete( input, pos );
        System.out.println(candidates.toString());
    }
    
    static IProgram programInstance = null;
    
    public SqlCompleter( Connection conn ) {
        this.conn = conn;
    }
    
    private Connection conn = null;
    public Connection getConnection() throws SQLException {
        return conn;
    }
    
    public Set<Long> predict( int pos, Matrix matrix ) {
        Set<Long> predictions = new HashSet<Long>();
        for( int x = 0; x <= pos; x++ ) {
            Cell cell = matrix.get(x,pos);
            if( cell != null ) {
                for( int i = 0; i < cell.size(); i++ ) {
                    int rule = cell.getRule(i);
                    int p = cell.getPosition(i);
                    if( p < earley.rules[rule].rhs.length ) {
                        String e = earley.allSymbols[earley.rules[rule].rhs[p]];
                        //if( !e.startsWith("xml") ) {
                        //    predictions.add(e);
                        //}
                        predictions.add(Service.lPair(earley.rules[rule].rhs[p], earley.rules[rule].head));
                    }
                }
            }           
        }
        return predictions;
    }
    public Set<String> topKWsuggestions( Set<Long> suggestions, LexerToken prefix ) {
        Map<Long,Integer> topN = new TreeMap<Long,Integer>();  // symbol -> frequency
        final int N = 7;
        for( long entry : suggestions ) {
            String prefixKW = "'";
            if( prefix != null )
                prefixKW = "'"+prefix.content;
            String sym = earley.allSymbols[Service.lX(entry)];
            if( !sym.startsWith(prefixKW.toUpperCase()) ) 
                continue;            
            String parent= earley.allSymbols[Service.lY(entry)];
            if( parent.endsWith("]\"") )
                if( 2+sym.length() < parent.length() )  // e.g. "DESC[RIBE]": 'DESCRI'
                    continue;
            
            long minVar = -1;
            int minVal = Integer.MAX_VALUE;
            for( long s : topN.keySet() ) {
                int tmp = topN.get(s);
                if( tmp < minVal ) {
                    minVar = s;
                    minVal = tmp;
                }
            }
            long suggestedVar = entry;
            Integer suggestedVal = HarvestDoc.getFrequencies().get(entry);
//System.out.println(earley.allSymbols[Service.lX(suggestedVar)]+"="+suggestedVal);
            if( suggestedVal == null )
                suggestedVal = 0;
            if( topN.size() == N ) {
                if( suggestedVal != null && minVal < suggestedVal ) {
                    topN.remove(minVar);
                    topN.put(suggestedVar,suggestedVal);
                }
            } else
                topN.put(suggestedVar,suggestedVal);
        }
        
        // http://stackoverflow.com/questions/13015699/representing-binary-relation-in-java
        List<Entry<Long, Integer>> myList = new ArrayList<Entry<Long, Integer>>();
        for (Entry<Long, Integer> e : topN.entrySet())
              myList.add(e);

        Collections.sort( myList, new Comparator<Entry<Long, Integer>>(){
            public int compare( Entry a, Entry b ){
                // compare b to a to get reverse order
                return ((Integer) b.getValue()).compareTo((Integer)a.getValue());
            }
        } );

        //myList = myList.sublist(0, 3);
        int div = 0;
        if( prefix != null )
            div = prefix.content.length();
        
        Set<String> ret = new TreeSet<String>();
        int i = 0;
        for( Entry<Long, Integer> e : myList ) {
            if( N < i )
                break;
            String sugg = earley.allSymbols[Service.lX(e.getKey())];
            sugg = sugg.substring(1,sugg.length()-1);
            if( "true".equals(HiddenParameters.parameters.get("coloredComplete")) ) {
                String pre = sugg.substring(0,div);
                String post = sugg.substring(div);
                ret.add(color0+pre+color32+post+color0);
            } else
                ret.add(sugg);
            i++;
        }
        return ret;
    }

    
    private static boolean contains( List<CharSequence> candidates, String entry ) {
        for( CharSequence tmp : candidates ) {
            if( ((String)tmp).toUpperCase().trim().equals(entry.toUpperCase().trim()) )
                return true;
        }
        return false;
    }

    static class IProgram extends SqlProgram {
        public IProgram( String arboriProgram ) throws IOException {
            super(arboriProgram);
        }

        int offset = -1;
    };
    /**
     * Completes the following:
     * 1. Keywords
     * 2. Table list (prefixed and not)
     * 3. Column list in where clause (prefixed and not)
     */
    public CompletionList complete( String input, int pos ) {
		CompletionList ret = new CompletionList();
        try {

            List<LexerToken> src = LexerToken.parse(input);
            if( 0 == src.size() ) 
				return ret;
			
            // fix incomplete query
            earley = SqlEarley.partialRecognizer();
            Matrix matrix = new Matrix(earley);
            earley.parse(src, matrix); 
            SyntaxError error = SyntaxError.checkSyntax(input, new String[]{"sql_statement","select","insert","update","delete","merge"}, src, earley, matrix, "^^^", "SyntaxError_DetailedMessage");
			if( error != null ) {
//System.out.println("fixing syntax! " + error.getDetailedMessage());
            	int select = -1;
            	int from = -1;
            	for( LexerToken t : src ) {
            		if( "select".equalsIgnoreCase(t.content) && t.end < pos ) 
            			select = t.end;
            		if( 0 <= select && "from".equalsIgnoreCase(t.content) ) {
            			if( pos < t.begin ) {
            				from = t.begin;
            				break;
            			}
            			select = -1; // wrong select; continue scanning
            		}
            	}
            	if( 0 <= select && 0 <= from ) {
            		input = input.substring(0,select) + " * " + input.substring(from);
            		src = LexerToken.parse(input); 
            		pos = select + 1;
            	}
            }

            final Parsed target = new Parsed(
                                             input, 
                                             src,
                                             earley,
                                             (String)null//"sql_statements" //$NON-NLS-1$
                    );
            //if( 0 < src.size() && src.size() < 4 )
                //lookupHistory(candidates, codeMinusLastLine + lastLine);
            
            
            approximateIndex = LexerToken.char2lex(src, pos);
            
            //LexerToken prefix = null;
            if( approximateIndex < src.size() && src.get(approximateIndex).begin < pos && pos <= src.get(approximateIndex).end  ) 
                ret.prefix = src.get(approximateIndex);
            if( ret.prefix != null 
            && ret.prefix.type == Token.OPERATION 
            && !ret.prefix.content.equals(":") 
            && !ret.prefix.content.equals("!") 
            && !ret.prefix.content.equals("^") 
            ) { 
                ret.prefix = null;
                approximateIndex++;
            }
            
            if( "*".equals(ret.prefix) ) {
                ret.prefix = null;
                approximateIndex--;
            }
            
            // Turlock's Net
            if( src.get(0).content.equalsIgnoreCase("connect") /*&& 0 < prefix.content.length()*/) { //$NON-NLS-1$
                String pref = "";
                if( ret.prefix != null )
                    pref = ret.prefix.content;
                for( String sugg : NetEntries.getMatching(pref) )
                     ret.add(new CompletionItem(sugg, CompletionItem.Type.TURLNET));
                
                return ret;
            }
            
            if( 4 <= src.get(0).content.length() 
             && src.get(0).content.substring(0, 4).equalsIgnoreCase("desc") /*&& 0 < prefix.content.length()*/
             || 3 <= src.get(0).content.length() 
             && src.get(0).content.substring(0, 3).equalsIgnoreCase("ddl") /*&& 0 < prefix.content.length()*/
             || 4 <= src.get(0).content.length() 
             && src.get(0).content.substring(0, 4).equalsIgnoreCase("info") /*&& 0 < prefix.content.length()*/
             || 4 <= src.get(0).content.length() 
             && src.get(0).content.substring(0, 4).equalsIgnoreCase("ctas") /*&& 0 < prefix.content.length()*/
            ) { //$NON-NLS-1$
                String pref = "";
                if( ret.prefix != null )
                    pref = ret.prefix.content;
                String owner = null;
                String obj = pref;
                if( 2 < src.size() && ".".equals(src.get(2).content) )
                    owner = src.get(1).content.toUpperCase();
                for( CompletionItem sugg: queryObjects(owner, obj.toUpperCase(),"") ) {
                    ret.add(sugg);
                } 
                ret.pos = pos-pref.length();
                return ret;
            }
            
            int div = 0;
            if( ret.prefix != null )
                div = ret.prefix.content.length();
            
            //Set<Long> predicted = predict(prefix==null?approximateIndex+1:approximateIndex,target.getMatrix());
            Set<Long> predicted = predict(approximateIndex,target.getMatrix());
            Set<String> predictions = new TreeSet<String>();
            for( long key: predicted )
                predictions.add(SqlEarley.getInstance().allSymbols[Service.lX(key)]);
            
            
            Map<String,String> alias2table = new HashMap<String,String>();
            
            if( !predictions.contains("table_reference") 
             && !predictions.contains("dml_table_expression_clause")
            ) {
                if( programInstance == null ) {
                    programInstance = new IProgram(Service.readFile(SqlCompleter.class, "completion.prg"));
                    //programInstance.debug = true;
                } 
                programInstance.offset = approximateIndex;
                    

                Map<String,MaterializedPredicate> output = programInstance.eval(target);
                //MaterializedPredicate dbg = output.get("query_blocks_at_offset"); //$NON-NLS-1$
                //System.out.println(dbg.toString());
            	MaterializedPredicate tanqb = output.get("\"table_alias in narrowest qb\""); //$NON-NLS-1$
            	for( Tuple t : tanqb.getTuples() ) {
            		ParseNode table = tanqb.getAttribute(t, "table");
            		ParseNode alias = tanqb.getAttribute(t, "alias"); 
            		if( table.from+1==table.to && alias.from+1==alias.to)
            			alias2table.put(src.get(alias.from).content.toUpperCase(), src.get(table.from).content.toUpperCase());
            		// alias can be keyword confused as alias in incomplete sql;
            		// double check:
            		{
            			String _alias = "'"+src.get(alias.from).content.toUpperCase()+"'";
            			List<LexerToken> _src = src.subList(0, alias.from); 
            	        Matrix _matrix = new Matrix(earley);
             	        earley.parse(_src, _matrix); 
                        Set<Long> _predicted = predict(alias.from,_matrix);
                        Set<String> _predictions = new TreeSet<String>();
                        for( long key: _predicted )
                            _predictions.add(SqlEarley.getInstance().allSymbols[Service.lX(key)]);
            			if( _predictions.contains(_alias) )
            				alias = table;
            		}

            		int begin = src.get(table.from).begin;
            		int end = src.get(table.to-1).end;
            		String owner = null;
            		String tab = null;
            		if( ret.prefix != null) 
            			tab = ret.prefix.content;
            		String tableExpr = target.getInput().substring(begin, end);
            		if( 0 < tableExpr.indexOf('.') ) {
            			owner = tableExpr.substring(0, tableExpr.indexOf('.'));
            			tab = tableExpr.substring(tableExpr.indexOf('.')+1);                    
            		} else {
            			owner = null;
            			tab = tableExpr;
            		}

            		if( table == alias && table.to < src.size() && ".".equals(src.get(table.to).content) ) { // select * from hr. -- "." is missing from the parse tree                
            			owner = tableExpr;
            			tab = "";
            			ret.prefix = null;
            		}


            		String aliased = src.get(alias.from).content + ".";
            		if( predictions.contains("column") ) { // predict "select * from employees WHERE "
            			String prefix = null;
            			if( ret.prefix != null ) {
            				if( alias == table )      // test#13
            					prefix = ret.prefix.content;       
            				if( 0 < approximateIndex && ".".equals(src.get(approximateIndex-1).content) )  // test#15
            					prefix = ret.prefix.content;
            			}
            			for( String sugg: queryColumns(owner, tab, prefix) ) {
            				if( 1 < tanqb.cardinality()  ) {
            					sugg = aliased + sugg;
            					div += aliased.length();
            				}
            				ret.add(new CompletionItem(sugg,CompletionItem.Type.COLUMN));
            			}
            		}
            		boolean predictedWhereClause = false;
            		for( String symbol : predictions) {
            			if( symbol.startsWith("where_clause") ) {
            				predictedWhereClause = true;
            				break;
            			}
            		}
            		for( String symbol : predictions) {
            			if(  symbol.startsWith("query_table_expression")  // predict "select * from a"
            			 && !predictedWhereClause  
            			) {
            				if( tableExpr.equals(tab) || owner != null ) { 
            					if( table != alias )
            						continue;
            					for( CompletionItem sugg: queryObjects(owner, tab.toUpperCase(), "object_type in ('TABLE','VIEW','SYNONYM') and") ) {
            						ret.add(sugg);
            					} 
            				}                
            				break;
            			}
            		}
            	} 
            } else /*if( predictions.contains("table_reference") )*/ {  // predict "select * from "
            	String prefix = "";
            	if( ret.prefix != null )
            		prefix = ret.prefix.content;
            	for( CompletionItem sugg: queryObjects(null, prefix, "object_type in ('TABLE','VIEW','SYNONYM') and") ) {
            		ret.add(sugg);
            	}
            }  
            
            if( predictions.contains("condition") ) {
            	for( CompletionItem item : joinConditions(ret.entries,alias2table) )
            		ret.add(item);
            }
            
            if( ret.size() == 0 /*&& prefix != null*/ )
                for( String sugg: topKWsuggestions(predicted, ret.prefix) ) {
               	
                    //if( !ret.contains(sugg) )
                        ret.add(new CompletionItem(sugg, CompletionItem.Type.KEYWORD));
                }

            
            if( ret.size() == 1 && 0 < ret.first().toString().indexOf(' ') ) // history completion
                return ret;
            int p = pos;
            if( ret.prefix != null )
                p = pos-ret.prefix.content.length();
            ret.pos = p;
            return ret;
        } catch (SQLRecoverableException e) {
        	//Eat this if the connection dies.
        	//We wont want to do anything on this thread, rather the amin one
        	return ret;
        } catch( Throwable t ) {
            t.printStackTrace();
            return ret;
        }
    }

    public static String discolor( String cleaned ) {
        cleaned = cleaned.replace(color0, "");
        cleaned = cleaned.replace(color1, "");
        cleaned = cleaned.replace(color32, "");
        return cleaned;
    }    
    
    /**
     * 
     * @param owner
     * @param prefix
     * @param typesClause   postfixed(!)  e.g. "object_type in ('TABLE','VIEW','SYNONYM') and"
     * @return
     * @throws SQLException
     */
    protected List<CompletionItem> queryObjects( String owner, String prefix, String typesClause ) throws SQLException {
        final int maxUsers = 20;
        String query = "select /*distinct*/ object_name, object_type from all_objects \n" +
        		"where "+typesClause+" owner=user \n" +
        		"and object_name like :1 ESCAPE '\\' and rownum < 50\n" +
                "union all \n" +
                "select username||'.', 'USER' from all_users where username like :2 and rownum <= "+maxUsers+" \n" +
                "order by object_name";
        if( owner != null )
            query = "select object_name, object_type from all_objects \n" +
            		"where "+typesClause+" owner = :1 and object_name like :2 ESCAPE '\\' \n" +
            		"and rownum < 50 order by object_name";
        Connection conn = getConnection();
        if( conn == null )
            return new LinkedList<CompletionItem>();
        if( 3 < prefix.length() )
            query = query.replace("owner=user", "owner in ('PUBLIC','SYS',user)");
        PreparedStatement stmt = conn.prepareStatement(query);
        int pos = 1;
        if( owner != null )
            stmt.setObject(pos++,owner.toUpperCase());
        else
            stmt.setObject(pos++,prefix.toUpperCase().replace("_", "\\_")+'%');
        stmt.setObject(pos++,prefix.toUpperCase().replace("_", "\\_")+'%');
        ResultSet rs = stmt.executeQuery();
        
        try {
            List<CompletionItem> users = new LinkedList<CompletionItem>();
            List<CompletionItem> ret = new LinkedList<CompletionItem>();
            while( rs.next() ) {
                String candidate = rs.getString(1);
                String type = rs.getString(2);
                if( candidate.endsWith(".") )
                    users.add(new CompletionItem(candidate, CompletionItem.Type.USER));
                else if( "TABLE".equalsIgnoreCase(type) )
                    ret.add(new CompletionItem(candidate, CompletionItem.Type.TABLE));
                else if( "VIEW".equalsIgnoreCase(type) )
                    ret.add(new CompletionItem(candidate, CompletionItem.Type.VIEW));
                else if( "SYNONYM".equalsIgnoreCase(type) )
                    ret.add(new CompletionItem(candidate, CompletionItem.Type.SYNONYM));
                else if( "PROCEDURE".equalsIgnoreCase(type) )
                    ret.add(new CompletionItem(candidate, CompletionItem.Type.PROCEDURE));
                else if( "FUNCTION".equalsIgnoreCase(type) )
                    ret.add(new CompletionItem(candidate, CompletionItem.Type.FUNCTION));
                else if( "PACKAGE".equalsIgnoreCase(type) )
                    ret.add(new CompletionItem(candidate, CompletionItem.Type.PACKAGE));
                else if( "PACKAGE BODY".equalsIgnoreCase(type) )
                    ret.add(new CompletionItem(candidate, CompletionItem.Type.PACKAGE_BODY));
                else 
                    ret.add(new CompletionItem(candidate, null));
            }
            if( users.size() < maxUsers )
                ret.addAll(users);
            return ret;
        } finally {
            if( rs != null ) try {
                rs.close();
            } catch (Exception ex) {}
            if( stmt != null ) try {
                stmt.close();
            } catch (Exception ex) {}
        }
    }

    protected List<String> queryColumns( String owner, String table, String prefix ) throws SQLException {
        String query = "select column_name from all_tab_columns where table_name = :1 and owner in ('PUBLIC','SYS',user) \n" +
                "and column_name like :2 and rownum < 50 order by column_name\n";
        if( owner != null )
            query = "select column_name from all_tab_columns where owner = :1 and table_name = :2 \n" +
                "and column_name like :2 and rownum < 50 order by column_name\n";
        Connection conn = getConnection();
        if( conn == null )
            return new LinkedList<String>();
        PreparedStatement stmt = conn.prepareStatement(query);
        int pos = 1;
        if( owner != null )
            stmt.setObject(pos++,owner.toUpperCase());
        stmt.setObject(pos++,table.toUpperCase());
        if( prefix != null )
            stmt.setObject(pos++,prefix.toUpperCase()+'%');
        else
            stmt.setObject(pos++,"%");
        ResultSet rs = stmt.executeQuery();
        
        try {
            List<String> ret = new LinkedList<String>();
            while( rs.next() ) 
                ret.add(rs.getString(1));           
            return ret;
        } finally {
            if( rs != null ) try {
                rs.close();
            } catch (Exception ex) {}
            if( stmt != null ) try {
                stmt.close();
            } catch (Exception ex) {}
        }
    }

    /**
     * generate equijoin condition, e.g. "select * from all_indexes i, all_objects o where" 
     * (generated) "i.index_name = o.object_name and i.owner = o.owner"
     * @param items
     * @param oracle
     * @return
     */
    private List<CompletionItem> joinConditions( List<CompletionItem> items, Map<String,String> alias2table ) {
        List<CompletionItem> ret = new LinkedList<CompletionItem>();
        int cnt = 0;
        ret:
            for( CompletionItem first : items ) {
                String firstName = first.entry;
                int fi = firstName.indexOf('.');
                if( fi > 0 && Type.COLUMN.equals(first.type) /*&& !"DATE".equals(first.columnType)*/  ) { //$NON-NLS-1$ //$NON-NLS-2$
                    for( CompletionItem second : items ) {
                        String secondName = second.entry;
                        int si = secondName.indexOf('.');
                        if( si <= 0 || firstName.compareTo(secondName) <= 0 )
                            continue;
                        if( Type.COLUMN.equals(first.type) /*&& second.columnType.equals(first.columnType)*/ && colNamesMatch(firstName, secondName, alias2table)  ) { //$NON-NLS-1$
                            ret.add(new CompletionItem(firstName+" = "+secondName, CompletionItem.Type.CONDITION)); //$NON-NLS-1$ //$NON-NLS-2$
                            if( ++cnt > 50 )
                                break ret;
                        }
                    }
                }
            }
        return ret; 
    }
    private boolean colNamesMatch( String firstCol, String secondCol, Map<String,String> alias2table ) {
        int fi = firstCol.indexOf('.');
        int si = secondCol.indexOf('.');
        String firstNAME = firstCol.substring(fi+1).toUpperCase();
        String secondNAME = secondCol.substring(si+1).toUpperCase(); 
        if( firstNAME == null ) // Not sure how 9310304 arrived to this condition: no SQL posted!
            return false;
        if( firstNAME.equals(secondNAME) ) // e.g. ALL_INDEXES.OWNER = ALL_OBJECTS.OWNER
            return true;
        String firstTABLE = alias2table.get(firstCol.substring(0,fi).toUpperCase()).toUpperCase();
        String secondTABLE = alias2table.get(secondCol.substring(0,si).toUpperCase()).toUpperCase();
        //was: if( firstTABLE.equals(secondTABLE) )
        //return false;
        // counterexample: select * from all_constraints c1, all_constraints c2
        //                 where c1.r_constraint_name = c1.constraint_name
        if(
                // CUSTOMERS.CUSTOMER = ORDERS.CUSTOMER_ID
                firstNAME.startsWith(firstTABLE) && secondNAME.equals(firstNAME+"_ID") //$NON-NLS-1$
                || firstNAME.startsWith(firstTABLE) && secondNAME.equals(firstNAME+"_NAME") //$NON-NLS-1$
                ||	secondNAME.startsWith(secondTABLE) && firstNAME.equals(secondNAME+"_ID") //$NON-NLS-1$
                || secondNAME.startsWith(secondTABLE) && firstNAME.equals(secondNAME+"_NAME") //$NON-NLS-1$
                // ALL_INDEXES.INDEX_NAME = ALL_OBJECTS.OBJECT_NAME
                ||	tabColNamesMatch(firstNAME,firstTABLE) && firstNAME.endsWith("_ID") && tabColNamesMatch(secondNAME,secondTABLE) && secondNAME.endsWith("_ID") //$NON-NLS-1$ //$NON-NLS-2$
                || tabColNamesMatch(firstNAME,firstTABLE) && firstNAME.endsWith("_NAME") && tabColNamesMatch(secondNAME,secondTABLE) && secondNAME.endsWith("_NAME") //$NON-NLS-1$ //$NON-NLS-2$
                ) return true;
        return false;
    }
    private boolean tabColNamesMatch( String column, String table ) {
        if( table.length() < 5 || column.length() < 5 )
            return false;
        for( int i = 0; i < table.length()-5; i++ ) {
            String tab = table.substring(i, i+5);
            if( column.contains(tab) )
                return true;
        }
        return false;
    }
    


}
