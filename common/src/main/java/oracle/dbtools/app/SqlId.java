/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.plsql.LazyNode;

/**
 * Application which is looking for a better package home
 * 
     **** file abc.plb ***
     ...
        plsql code
     ...
     for c1 in (select username from ...

     would get changed to:

     **** file abc.plb ***
     ...
     plsql code
     ...
*/
//   for c1 in (select /*OracleAPEX tR5!~1*/  username from ...
/**
     where "tR5!~1" is a random string, ideally 5 or 6 characters to improve readability, 
     unique enough is good enough, so a random number between 1 and billion would seem unique enough to me

     sqlid OracleAPEX *.plb

     this could rewrite all files in the current directory with a plb extension using the identified prefix
 */
public class SqlId extends Rewrite {

    private String prefix = " ";
    private int sqlCnt = 0; 
    private int changeCnt = 0; 
    
    
	public SqlId( String prefix, Set<String> fileExtensions ) {
	    super(fileExtensions);
		this.prefix = prefix;
	}    

    private static List<Integer> getInsertionsPoints( LazyNode node, List<LexerToken> src ) {
        List<Integer> ret = new LinkedList<Integer>();
        if( 
                "insert".equalsIgnoreCase(node.startToken) || //$NON-NLS-1$
                "update".equalsIgnoreCase(node.startToken) || //$NON-NLS-1$
                "delete".equalsIgnoreCase(node.startToken) || //$NON-NLS-1$
                "select".equalsIgnoreCase(node.startToken) || //$NON-NLS-1$
                "with".equalsIgnoreCase(node.startToken)   || //$NON-NLS-1$
                "merge".equalsIgnoreCase(node.startToken)  //$NON-NLS-1$
        )
            ret.add(node.from);
        else if( 
                "cursor".equalsIgnoreCase(node.startToken) || //$NON-NLS-1$
                "for".equalsIgnoreCase(node.startToken)  //$NON-NLS-1$
        ) {
            int pos = -1;
            int start = node.from;
            int end = node.to;
            if( "for".equalsIgnoreCase(node.startToken ) ) { //$NON-NLS-1$)
            	int i = -1;
            	for( LazyNode child : node.shallowChildren() ) {
            		i++;
            		if( i == 0 ) { // the first child of the "for" node is "(" that contains the query 
            			start = child.from;
            			end = child.to;
            		} else // the body of the for loop
                    	ret.addAll(getInsertionsPoints(child, src));
            	}
            }
            for( LexerToken t : src ) {
                pos++;
                if( pos >= end )
                    break;
                if( pos < start )
                    continue;
                if( "select".equalsIgnoreCase(t.content) ) {
                    ret.add(pos);
                    break;
                }
            }
        } else for( LazyNode child : node.shallowChildren() )
            ret.addAll(getInsertionsPoints(child, src));
        return ret;
    }


    @Override
    public String transform( String input, List<LexerToken> src, LazyNode root ) {
        final String fullPrefix = " /* "+prefix;
        List<Integer> insertionPoints = getInsertionsPoints(root, src);
        int pos = -1;
        int priorLexPos = 0;
        StringBuilder output = new StringBuilder();
        for( LexerToken t : src ) {
            pos++;
            if( insertionPoints.contains(pos) ) {
                sqlCnt++;
                if( !fullPrefix.equals(input.substring(t.end, t.end+fullPrefix.length()))) {
                    output.append(input.substring(priorLexPos,t.end));
                    priorLexPos = t.end;
                    output.append(fullPrefix + UUID.randomUUID().toString().substring(0,6) + " */ ");
                    changeCnt++;
                }
            }
        }
        output.append(input.substring(priorLexPos));
        return output.toString();
    }

    @Override
    protected String found() {
        return "found "+sqlCnt+" statements, "; //$NON-NLS-1$
    }

    @Override
    protected String changed() {
        return "stamped "+changeCnt+" of them";
    }


}
