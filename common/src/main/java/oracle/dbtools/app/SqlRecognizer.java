/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.Program;
import oracle.dbtools.arbori.SqlProgram;
import oracle.dbtools.arbori.Tuple;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Service;

/**
 * @author Dim
 */
public class SqlRecognizer {
    
    public static void main( String[] args ) throws Exception {
        final String input = 
                Service.readFile(SqlRecognizer.class, "test.sql") //$NON-NLS-1$
                //Service.readFile("/EclipseWorkspace/common/common/src/oracle/dbtools/parser/plsql/test.sql");
        ;
        /*List<LexerToken> src =  LexerToken.parse(input);
        SqlRecognizer o = new SqlRecognizer();
        
        Map<String,Set<Long>> output = o.recognize(input, src);
        for( String symbol : output.keySet() ) {
            System.out.println(symbol+" recognized at");
            for( Long interval : output.get(symbol)) {
                int from = Service.lX(interval);
                int to = Service.lY(interval);
                System.out.println("["+from+","+to+")   "+input.substring(from, to));               
            }
        }*/
        /*for ( int i = 0; i < 10; i++ ) {
            new Thread() {
                @Override
                public void run() {
                    test(input);
                }            
            }.start();
        }*/
        test(input);
    }
    private static void test(String input) {
        //System.out.println(getColumns(input));
        System.out.println(getTables(input));
        //System.out.println(getPredicates(input));
        //System.out.println(getOrderBy(input));
        //System.out.println(getUserOrRole(input));
        //System.out.println(SqlRecognizer.getStatements(input));*/
    	//System.out.println(getGroupBy(input));

        //System.out.println(SqlRecognizer.getAssignedBinds(input));
    }
    
    static final String path = "/oracle/dbtools/app/"; //$NON-NLS-1$
    public Map<String,Set<Long>> recognize( String input, List<LexerToken> src ) {
    	return recognize(input, src, null);
    }
    /**
     * Recognize grammar symbols as queried in recognize.prg
     * @param input
     * @param src
     * @return each symbol mapped to list of intervals 
     * (pair of ints compactified into long; see Service.lPair, Service.lX, Service.lY
     */
    public Map<String,Set<Long>> recognize( String input, List<LexerToken> src, String extraSymbol ) {
        try {
            Program.debug = false;
            String recognitionPrg = Service.readFile(Obfuscator.class, path+"recognize.prg");
            if( extraSymbol != null ) {
            	List<LexerToken> tmp = LexerToken.parse(recognitionPrg);
            	boolean found = false;
            	for( LexerToken t: tmp )
            		if( t.content.equals(extraSymbol) ) {
            			found = true;
            			break;
            		}
            	if( !found )
            		recognitionPrg = recognitionPrg + "\n\""+extraSymbol+"\": ["+extraSymbol+") "+extraSymbol+";";
            }
            Map<String,MaterializedPredicate> predicateVectors = new SqlProgram(recognitionPrg).run(input);
            //System.out.println(predicateVectors.toString());           
            
            Map<String,Set<Long>> ret = new TreeMap<String,Set<Long>>();

            for( String key : predicateVectors.keySet() ) {
            	if( !key.startsWith("\"") ) // auxiliary predicate
            		continue;
                String unquotedPredicateName = key.substring(1,key.length()-1); // strip double quotes 
                fetch(unquotedPredicateName, src, predicateVectors, ret );
            }
             
            return ret;

        } catch( SyntaxError e ) {
            throw e;
        } catch( AssertionError e ) {
            System.err.println(e.getMessage());
            return null;
        } catch( Exception e ) {
            e.printStackTrace();
            return null;
        } finally {
        }
        
    }
    private void fetch( String predicate, List<LexerToken> src, Map<String, MaterializedPredicate> predicateVectors, Map<String, Set<Long>> ret ) {
        MaterializedPredicate symbols = predicateVectors.get("\""+predicate+"\""); //$NON-NLS-1$
        Set<Long> intervals = new HashSet<Long>();
        if( null == symbols.getAttribute(predicate) )  // auxiliary predicate 
            return;
        ret.put(predicate, intervals);
        for( Tuple t : symbols.getTuples() ) {              
            ParseNode pc = symbols.getAttribute(t, predicate); //$NON-NLS-1$
            LexerToken f = src.get(pc.from);
            LexerToken t_1 = src.get(pc.to-1);
            intervals.add(Service.lPair(f.begin, t_1.end));
        }
    }
    
    /**
     * Get all symbols that match targetSymbol rule
     * @param input  -- sql text to parse
     * @param targetSymbol  -- named query against parse tree, e.g.
     * "tableFrom": [tableFrom) table_reference 
                  & [from_clause) from_clause
                  & from_clause < tableFrom
       ;   
     * @return all found sql text fragments indexed by positions (in character offset units) 
     */
    public static Map<Integer,String> fragmentAtLocation( String input, String targetSymbol ) {
        List<LexerToken> src =  LexerToken.parse(input);
        SqlRecognizer o = new SqlRecognizer();
        Map<Integer,String> ret = new TreeMap<Integer,String>();
        Map<String,Set<Long>> output = o.recognize(input, src, targetSymbol);
        for( String symbol : output.keySet() ) {
            if( targetSymbol.equals(symbol) )
                for( Long interval : output.get(symbol)) {
                    int from = Service.lX(interval);
                    int to = Service.lY(interval);
                    //System.out.println("["+from+","+to+")   "+input.substring(from, to));   
                    ret.put(from,input.substring(from, to));
                }
        }
        return ret;
    }
    /**
     * A version of fragmentAtLocation which returns fragments ordered 
     * (with offset info lost) 
     */
    public static List<String> getList( String input, String targetSymbol ) {
        Map<Integer,String> tmp = fragmentAtLocation(input, targetSymbol);
        List<String> ret = new LinkedList<String>();
        int lastPos = -1;
        for( int pos : tmp.keySet() ) { 
            if( pos <= lastPos )  // in case if Map is not ordered
                throw new AssertionError("pos <= lastPos");
            ret.add(tmp.get(pos));
            lastPos = pos;
        }
        return ret;
    }
    
    /**
     * See "columnSelect" definition in recognize.prg
     */
    public static List<String> getColumns( String input ) {
        return getList(input, "columnSelect");
    }
    /**
     * See "tableFrom" definition in recognize.prg
     */
    public static List<String> getTables( String input ) {
        return getList(input, "tableFrom");
    }
    /**
     * See "whereClause" definition in recognize.prg
     */
    public static List<String> getPredicates( String input ) {
        return getList(input, "predicateWhere");
    }
    /**
     * See "orderBy" definition in recognize.prg
     */
    public static List<String> getOrderBy( String input ) {
        return getList(input, "orderBy");
    }
    
    /**
     * See "groupBy" definition in recognize.prg
     */
    public static List<String> getGroupBy( String input ) {
        return getList(input, "groupBy");
    }
    
    public static List<String> getStatements( String input ) {
    	input = massage(input);
        return getList(input, "sql_statement");
    }
    
    public static List<String> getAssignedBinds( String input ) {
    	input = massage(input);
        return getList(input, "assignedBind");
    }
    
	private static String massage(String input) {
    	input = input.trim();
    	if( 2<input.length() && input.startsWith("{") && input.endsWith("}") )
    		input = input.substring(1, input.length()-1);
		return input;
	}
    

    
   /**
     * See "user" and "role" definition in recognize.prg
     */
    public static Map<Integer, String> getUserOrRole( String input ) {
        Map<Integer, String> ret = fragmentAtLocation(input,"role");
        if( 0 < ret.size() )
            return ret;
        else
            return fragmentAtLocation(input,"user");
    }

}
