/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.lang.reflect.Method;
import java.util.LinkedList;
//import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.Grammar;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parseable;
import oracle.dbtools.parser.RuleTuple;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.Visual;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Service;

/**
 * Catches regression in parse tree shapes due to SQL grammar and parser code evolutions
 * @author Dim
 */
public class SqlRecognizerTest {

    static int testNo = 0;
    private static TreeSet<Integer> failedTests = new TreeSet<Integer>();
    
    private static int output = -1;
    private static int recognized = -1;
    private static int list;
    private static int digits = -1;
    private static int assertion = -1;
    private static int identifier = -1;
    private static int query = -1;
    private static int atest = -1;
    private static int sql_fragment = -1;
    private static int comment = -1;
    
    public static Earley testParser = null;
    static {
        try {
            testParser = new Earley(getRules()) {
                @Override
                protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
                    LexerToken token = src.get(y);
                    return 
                          symbol == identifier && token.type == Token.IDENTIFIER 
                      ||  symbol == identifier && token.type == Token.DQUOTED_STRING
                      ||  symbol == identifier && token.type == Token.BQUOTED_STRING
                    ;
                }
            };
        } catch ( Exception e ) {
            e.printStackTrace(); // (authorized)
        }        
        //RuleTuple.printRules(rules);
        output = testParser.symbolIndexes.get("output");
        list = testParser.symbolIndexes.get("list");
        assertion = testParser.symbolIndexes.get("assertion");
        identifier = testParser.symbolIndexes.get("identifier");
        query = testParser.symbolIndexes.get("query");
        atest = testParser.symbolIndexes.get("atest");
        recognized = testParser.symbolIndexes.get("recognized");
        digits = testParser.symbolIndexes.get("digits");
        sql_fragment = testParser.symbolIndexes.get("sql_fragment");
        comment = testParser.symbolIndexes.get("comment");
    }

    
    public static void main( String[] args ) throws Exception {
                        
        String testFile = "recognizer.test";
        
        test(testFile);
    }
    
    static Random random = new Random();
    static protected boolean introduceRandomError = false;
    
    public static void test( String testFile ) throws Exception {
        String input = Service.readFile(SqlRecognizerTest.class, testFile); //$NON-NLS-1$
        List<LexerToken> src =  LexerToken.parse(input, "`"); 
        
        Visual visual = null;
        //visual = new Visual(src, earley)
        Matrix matrix = new Matrix(testParser);
        testParser.parse(src, matrix); 
         
        SyntaxError s = SyntaxError.checkSyntax(input, new String[]{"atest"}, src, testParser, matrix);
        if( s != null ) { //$NON-NLS-1$ //$NON-NLS-2$
        	if( visual != null )
        		visual.draw(matrix);
            System.err.println("Syntax Error");
            System.err.println("at line#"+s.line);
            System.err.println(s.code);
            System.err.println(s.marker);
            System.err.println("Expected:  ");
                for( String tmp : s.getSuggestions() )
                    System.out.print(tmp+',');
            throw new Exception(">>>> syntactically invalid code fragment <<<<"); //$NON-NLS-1$
        }
        
        ParseNode root = testParser.forest(src, matrix);
        atest(root, src, input);
        
        if( failedTests.size() == 0 )
            System.out.println("*** ALL "+testNo+" TESTS are OK *** ---> "); //$NON-NLS-1$
        else 
            System.err.println("*** FAILED "+failedTests.size()+" tests *** ");        //$NON-NLS-1$
        int cnt = -1;
        for( int testNo : failedTests ) {
            cnt++;
            if( cnt < 10 )
                System.err.print(testNo+",");
            else if( cnt == 10 )
                System.err.print(" ... ");
            else if( failedTests.size() - 10 < cnt  )
                System.err.print(","+testNo);
        }
        if( 0 < failedTests.size() )
            System.exit(2);
    }

    private static Set<RuleTuple> getRules() throws Exception {
        String input = Service.readFile(SqlRecognizerTest.class, "recognizerTest.grammar"); //$NON-NLS-1$
        List<LexerToken> src = LexerToken.parse(input, false, LexerToken.QuotedStrings/*+SqlPlusComments*/); 
        ParseNode root = Grammar.parseGrammarFile(src, input);
        Set<RuleTuple> ret = new TreeSet<RuleTuple>();
        Grammar.grammar(root, src, ret);
        return ret;
    }
    
    /**
     * @param root
     * @param src
     * @return node that violates assertion
     * @throws Exception
     */
    private static void atest( ParseNode root, List<LexerToken> src, String input ) throws Exception {
        if( root.contains(assertion) ) {
            System.out.println("TEST#"+testNo); //$NON-NLS-1$ //$NON-NLS-2$
            assertion(root,src,input);
            return;
        }
    	
        if( root.contains(output) ) {
        	for( ParseNode child : root.children() ) {
                //if( child.contains(query) ) {
        		System.out.println("TEST#"+testNo+":  ---> "); //$NON-NLS-1$ //$NON-NLS-2$
        		String sql = sql_fragment(child,src,input);
        		final Method[] methods = SqlRecognizer.class.getDeclaredMethods();
        		for( int i = 0; i < methods.length; i++ ) {
        			String name = methods[i].getName();
        			if( !name.startsWith("get") )
        				continue;
        			final Class<?>[] argTypes = methods[i].getParameterTypes();
        			if( argTypes.length != 1 )
        				continue;
        			if( argTypes[0] != String.class )
        				continue;
        			final Object ret = methods[i].invoke(null, sql);
        			System.out.println(name+"  "+ret.toString());
				}
        	    return; 
        	}
        	return;
        }
        
        if( root.contains(comment) )
           comment(root,src,input);

        for( ParseNode child : root.children() ) {
            atest(child,src, input);   
        }
    }

    
    private static ParseNode comment(ParseNode root, List<LexerToken> src, String input) throws Exception {
        String testNum = input.substring(src.get(root.from+2).begin,src.get(root.to-1).end-1);
        testNo = Integer.parseInt(testNum);
        return null;
    }

    
	private static String sql_fragment( ParseNode root, List<LexerToken> src, String input ) {	    
        String ret = input.substring(src.get(root.from).begin, src.get(root.to-1).end);
        if( ret.charAt(0)=='"' )
            ret = ret.substring(1,ret.length()-1);
        if( ret.charAt(0)=='`' )
            ret = ret.substring(1,ret.length()-1);
        return " "+ret+" ";
	}
	
    private static ParseNode assertion( ParseNode root, List<LexerToken> src, String input ) throws Exception {
    	String sql = null;
    	for( ParseNode child : root.children() ) {
        	if( child.contains(query) ) {
        		sql = sql_fragment(child,src,input);
        		continue;
        	}
        	if( child.contains(recognized) ) {
        		recognized(child,src,input,sql);
        		continue;
        	}
        }
        return null;
    }
    
    private static ParseNode recognized( ParseNode root, List<LexerToken> src, String input, String sql ) throws Exception {
    	String id = null;
    	List<String> ret = null;
    	for( ParseNode child : root.children() ) {
        	if( child.contains(recognized) ) {
        		recognized(child,src,input,sql);
        		continue;
        	}
        	if( child.contains(list) ) {
        		List<String> cmp = list(child, src, input);
        		        		
        		String diff = diff(cmp, ret, id);
                if( diff != null && 0 < diff.length() ) {
                    failedTests.add(testNo);
                    System.out.println("TEST#"+testNo+":  broken"); //$NON-NLS-1$ //$NON-NLS-2$
                    System.out.println(diff);
                }
        		return null;
        	}
        	if( child.contains(identifier) ) {
        		id = child.content(src);
        		final Method m = SqlRecognizer.class.getDeclaredMethod(id,String.class);
        		ret = (List<String>) m.invoke(null, sql);
        		continue;
        	}
        }
        if( 0 < ret.size() ) {
            failedTests.add(testNo);
            System.out.println("TEST#"+testNo+":  broken"); //$NON-NLS-1$ //$NON-NLS-2$
            System.out.println("method "+id+" returns unexpected "+ret.get(0).toString());
        }
        return null;
    }


	private static List<String> list( ParseNode root, List<LexerToken> src, String input ) {
		List<String> ret = new LinkedList<String>();
		if( root.contains(identifier) ) {
			String id = root.content(src);
			if( id.charAt(0)!= '`' && id.charAt(0)!= '"' )
				id = id.toUpperCase();
			ret.add(id);
			return ret;
		}
    	for( ParseNode child : root.children() ) 
    		ret.addAll(list(child,src,input));
		return ret;
	}
	
	private static String diff( List<String> cmp1, List<String> notNormalized, String method ) {
		StringBuilder ret = new StringBuilder();
		List<String> cmp2 = new LinkedList<>();
		for( String c : notNormalized ) {
			if( c.charAt(0)!= '`' && c.charAt(0)!= '"' )
				c = c.toUpperCase();
			cmp2.add(c);
		}
		for( String c : cmp1 ) {
			if( !cmp2.contains(c) ) {
				ret.append("   expected "+c+" in "+method+" return");
				break;
			}
		}
		for( String c : cmp2 ) {
			if( !cmp1.contains(c) ) {
				ret.append("   extra "+c+" in "+method+" return");
				break;
			}
		}
		return ret.toString();
	}


}
