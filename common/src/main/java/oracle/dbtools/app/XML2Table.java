/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.Program;
import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.Grammar;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.RuleTuple;
import oracle.dbtools.parser.Token;
import oracle.dbtools.util.Service;

public class XML2Table extends Program {
    
    static String currentDir = "/oracle/dbtools/app/";
    public static void main( String[] args ) throws Exception {
        String program = Service.readFile(XML2Table.class, currentDir+"xml2table.prg");
        XML2Table xmlProgram = new XML2Table(program);
        String input = Service.readFile(XML2Table.class, currentDir+"xml.txt");
        xmlProgram.run(input, null, xmlProgram);
        //xmlProgram.getParsedTarget().getRoot().printTree();
    }
    
    private static Earley xmlParser = null;
    private static String dir = "/oracle/dbtools/parser/";
    public static Earley xmlParser() throws IOException {
        if( xmlParser != null )
            return xmlParser; 
        Parsed xmlGrammar = new Parsed(
                                          Service.readFile(Grammar.class, dir+"xml.grammar"), //$NON-NLS-1$
                                          Grammar.bnfParser(),
                                          "grammar" 
        );   
        Set<RuleTuple> rules = new TreeSet<RuleTuple>();
        Grammar.grammar(xmlGrammar.getRoot(), xmlGrammar.getSrc(), rules);
        xmlParser = new Earley(rules) {
            @Override
            protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {                
                if( symbol != identifier )
                    return false;                
                LexerToken token = src.get(y);
                if( token.type == Token.DQUOTED_STRING )
                    return true;
                if( token.type == Token.IDENTIFIER )
                    return true;
                return false;
            }
            
        };
        return xmlParser;
    }

    public XML2Table( String arboriProgram ) throws IOException {
        super(xmlParser());
        compile(arboriProgram);
    }
    
    public XML2Table() throws IOException {
        this(Service.readFile(XML2Table.class, currentDir+"xml2table.prg"));
    }
    
    private Parsed prg;
    
    /*
     * fails in concurrent environment
     * private Parsed target;
    public Parsed getParsedTarget() {
        return target;
    }*/
    
    boolean debug = false;
    /**
     * @param input - text to process
     * @param src   - lexed text (perform lexical analysis if null)
     * @param callback - an object to execute a method (via reflection) for each predicate tuple
     * @return
     * @throws IOException
     */
    public Map<String, MaterializedPredicate> run( String input, List<LexerToken> src, 
                                                                 Object callback ) throws IOException {
        if( src == null )
            src = LexerToken.parse(input);
        Parsed target = new Parsed(  // this is PL/SQL source parsed into a tree which arbori program is applied to
                                           input, 
                                           src,
                                           xmlParser(),
                                           (String)null //$NON-NLS-1$
                );
        if( debug )
            target.getRoot().printTree();
        final Map<String,MaterializedPredicate> predicateVectors = eval(target, callback);
        return predicateVectors;
    }

    
    //////////////////////////// Callbacks: ////////////////////////////
    
    /**
     * Collecting all tags in a callback
     * @param target
     * @param tuple
     */
    void tupleNodes( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(tuple.toString());
        // Less verbose:
        System.out.println("**tupleNodes(): **"+MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
    }

    void values( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(tuple.toString());
        // Less verbose:
        System.out.println("**values(): **"+MaterializedPredicate.tupleMnemonics(tuple,target.getSrc(),true));
    }
}
