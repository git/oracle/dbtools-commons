/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app.injection;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.app.CompleterTest;
import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.Grammar;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.RuleTuple;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.Visual;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Service;

public class RegressionTest {
    static Integer singleTest = null; // null run everything
    
    
    private static int maxTestNo = 0;
    
    private static Set<Integer> failedTests = new TreeSet<Integer>();
    
    
    static Set<RuleTuple> rules = injectionRules();        
    static Earley earley = new Earley(rules) {
        @Override
        protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
    		LexerToken token = src.get(y);
            return 
                  symbol == identifier && token.type == Token.IDENTIFIER 
              ||  symbol == identifier && token.type == Token.DQUOTED_STRING
              ||  symbol == identifier && token.type == Token.BQUOTED_STRING
            ;
        }
    };
    
    private static int output = earley.symbolIndexes.get("output");
    private static int assertion = earley.symbolIndexes.get("assertion");
    private static int query = earley.symbolIndexes.get("query");
    private static int comment = earley.symbolIndexes.get("comment");
    private static int cue = earley.symbolIndexes.get("cue");
    private static int path = earley.symbolIndexes.get("path");
    	
    public static void main( String[] args ) throws Exception {
        String input = Service.readFile(RegressionTest.class, "injection.test"); //$NON-NLS-1$
        List<LexerToken> src =  LexerToken.parse(input,"`");
        //LexerToken.print(src);
        Matrix matrix = new Matrix(earley);
        Visual visual = null;
        if( src.size() < 100 )
            visual = new Visual(src, earley);
        earley.parse(src, matrix);
        //visual.draw(matrix);       
        //if( visual != null )
            //visual.visited = null;
        SyntaxError s = SyntaxError.checkSyntax(input, new String[]{"atest"}, src, earley, matrix);
        if( s != null ) { //$NON-NLS-1$ //$NON-NLS-2$
            if( visual != null )
                visual.draw(matrix);       
            System.err.println("Syntax Error");
            System.err.println("at line#"+s.line);
            System.err.println(s.code);
            System.err.println(s.marker);
            System.err.println("Expected:  ");
                for( String tmp : s.getSuggestions() )
                    System.err.print(tmp+',');
            throw new Exception(">>>> syntactically invalid code fragment <<<<"); //$NON-NLS-1$
        }
        ParseNode root = earley.forest(src, matrix);
        //root.printTree();
        long t1 = System.currentTimeMillis();
        atest(root, src, input);
        long t2 = System.currentTimeMillis();
        System.out.println("Total test time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
        
        if( failedTests.size() == 0 )
            System.out.println("*** ALL "+maxTestNo+" TESTS are OK *** ---> "); //$NON-NLS-1$
        else {
            System.err.println("*** TEST FAILED! *** ---> "+failedTests.toString());        //$NON-NLS-1$
            System.exit(2);
        }
    }
    
    private static Set<RuleTuple> injectionRules() {
        try {
            String input = Service.readFile(RegressionTest.class, "injectionTest.grammar");
            List<LexerToken> src =  LexerToken.parse(input, "`");  
            ParseNode root = Grammar.parseGrammarFile(src, input);
            Set<RuleTuple> ret = new TreeSet<RuleTuple>();
            Grammar.grammar(root, src, ret);
            return ret;
        } catch( Exception e ) {
            throw new AssertionError("VT: failed to init grammar for injection test");
        } 
    }

    private static void atest( ParseNode root, List<LexerToken> src, String input ) throws Exception {
        if( root.contains(assertion) ) {
        	if( singleTest != null && (int)singleTest != maxTestNo )
        		return;
        	
            System.out.print("TEST#"+maxTestNo+" -> ");
            if( assertion(root,src,input, maxTestNo) )
            	System.out.println("OK \n");
            else
            	System.out.println("*** Failed ***\n");
            return;
        }
    	
        if( root.contains(output) ) {
        	for( ParseNode child : root.children() ) {
                //if( child.contains(query) ) {        	    
        	    System.out.println("TEST#"+maxTestNo+" ->\n"+ query(child,src,input) );
        	    return; 
        	}
        	return;
        }
        
        if( root.contains(comment) ) {
            comment(root,src,input);
            return;
        }

        for( ParseNode child : root.children() ) {
            atest(child,src, input);   
        }
    }

    private static boolean assertion( ParseNode root, List<LexerToken> src, String input, int testNum ) throws SQLException {
        List<String> output = null;
        Map<String,Boolean> cmp = null;
    
        for( ParseNode child : root.children() ) {
            if( child.contains(query) ) 
                output = query(child,src,input);
            else if( child.contains(cue) ) 
                cmp = cue(child,src,input);
        }
        
        for( String c : cmp.keySet() ) {
            boolean isPositive = cmp.get(c);
            String cC = c;
            if( cC.charAt(0)!='"' )
                cC = cC.toLowerCase();
            else
            	cC = cC.substring(1,cC.length()-1);
            if( cC.charAt(0)=='%' )
            	cC = cC.substring(1,cC.length()-1);
            boolean matched = false;
            for( String o : output ) {
                String oO = o;
                if( oO.charAt(0)!='"' )
                    oO = oO.toLowerCase();
                else
                	oO = oO.substring(1,oO.length()-1);
                cC = cC.replace(" ", "");
                oO = oO.replace(" ", "");
                if( cC.equals(oO) 
                 ||	!isPositive && oO.contains(cC)	 
                ) {
                    matched = true;
                    break;
                } 
            }
            if( !matched && isPositive
              || matched && !isPositive
            ) {
            	System.out.println((isPositive?"no":"unexpected")+" match for "+ c);
            	failedTests.add(testNum);
            	return false;
            }
        }
        return true;
    }

    private static Map<String, Boolean> cue( ParseNode node, List<LexerToken> src, String input ) {        
        Map<String, Boolean> output = new HashMap<String, Boolean>();
        
        if( node.contains(path) ) { 
            String current = node.content(src); 
            output.put(current,true);
            return output;
        }       
        
        String prior = null;
        for( ParseNode child : node.children() ) {
        	if( child.contains(cue)  ) {
        		output.putAll(cue(child,src,input ));
        		continue;
        	}
            String current = child.content(src);
            if( "-".equals(prior) )
                output.put(current,false);
            else if( !"-".equals(current) )
                output.put(current,true);
            prior = current;
        }
        
        return output;
    }

    private static List<String> query( ParseNode root, List<LexerToken> src, String input ) throws SQLException {            	
    	String sql = sql_fragment(root,src,input);
		return output(sql); //$NON-NLS-1$ //$NON-NLS-2$
    }

	private static List<String> output( String input ) throws SQLException {
        List<String> output = new LinkedList<String>();
		SqlInjection inject = SqlInjection.findDependencies(input);
        for( int p : inject.params ) {
         	for( int e : inject.execs ) {
        		ParseNode ss = inject.parseNode2Scope.get(p); 
        		ParseNode se = inject.parseNode2Scope.get(e);
        		if( ss != se )
        			continue;

        		String vars = inject.dependencyStr(p, e);
        		if( vars != null )
        			output.add(vars);
         	}
        }      
        long t2 = System.currentTimeMillis();
		return output;
	}

	private static String sql_fragment( ParseNode root, List<LexerToken> src, String input ) {
	    String ret = input.substring(src.get(root.from).begin,src.get(root.to-1).end);
	    if( ret.startsWith("`") || ret.startsWith("\"") )
	    	ret = ret.substring(1,ret.length()-1);
	    return ret;
	}
	
    private static ParseNode comment(ParseNode root, List<LexerToken> src, String input) throws Exception {
        final String txt = input.substring(src.get(root.from+1).begin,src.get(root.to-1).end-1);
        int testNo = Integer.parseInt(txt);
        if( maxTestNo < testNo )
        	maxTestNo = testNo;
    	if( singleTest != null && (int)singleTest != testNo )
    		return null;
		//System.out.println("Test# "+txt);
        return null;
    }

}
