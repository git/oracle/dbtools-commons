/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app.injection;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.Program;
import oracle.dbtools.arbori.SqlProgram;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.Yelrae;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Service;

public class SqlInjection {
	
    public static void main( String[] args ) throws Exception {
        String input = Service.readFile(SqlInjection.class, "test.sql"); //$NON-NLS-1$
        //input = Service.readFile(SqlEarley.class, "MSC_CL_PRE_PROCESS.pkgbdy"); //$NON-NLS-1$        
        final boolean isBig = 10000 < input.length();
        
        SqlEarley.visualize = false;
        if( !isBig )
        	SqlEarley.main(new String[]{input});   // parse tree     
        
		Program.timing=isBig;
        //Program.debug = true;
        //Service.profile(300000, 20, 15);
        
        long t1 = System.currentTimeMillis();
        
        SqlInjection inject = findDependencies(input);
        
        //inject.printDependencies();
        
        for( int p : inject.params ) {
    		for( int e : inject.execs ) {
    			ParseNode ss = inject.parseNode2Scope.get(p); 
    			ParseNode se = inject.parseNode2Scope.get(e);
    			if( ss != se )
    				continue;

    			String vars = inject.dependencyStr(p, e);
    			if( vars == null )
    				continue;
    			System.out.println("SQL Injection dependency: "+vars);
    			LinkedList<Integer> dependencyChain = inject.dependencyChain(p,e);
    			System.out.println("-------------------------------------------");
    			int from = inject.target.getSrc().get(dependencyChain.getLast()).begin-50;
    			if( from < 0 )
    				from = 0;
    			int to = inject.target.getSrc().get(dependencyChain.getLast()).begin+50;
    			if( input.length() < to )
    				to = input.length();
    			System.out.println(input.substring(from,to));
    			System.out.println("===========================================");
    		}
        }      
        long t2 = System.currentTimeMillis();
        System.out.println("Exec time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
    }


	public static SqlInjection findDependencies( final String input ) throws SyntaxError {
        return findDependencies(input, null, null);		
	}
	public static SqlInjection findDependencies( final String input, List<LexerToken> src, final ParseNode root ) throws SyntaxError {
		final SqlInjection inject = new SqlInjection() {
        	// test reflection on derived instance
        };
        
        try {
             if( programInstance == null ) {
            	String injectionPrg = Service.readFile(SqlInjection.class, "sqlinjection.arbori");
    			programInstance = new SqlProgram(injectionPrg) {
    				
    			};
            }
            if( src != null )
            	inject.target = new Parsed(input, src, root);
            else {
            	src = LexerToken.parse(input);
                inject.target = new Parsed(  
                		input, 
                		src,
                		SqlEarley.getInstance(),
                		new String[]{ Yelrae.PARSE_WITH_ERRORS } //$NON-NLS-1$
                );            
            }
            programInstance.eval(inject.target, inject);
                        
        } catch( SyntaxError e ) {
            throw e;
        } catch( AssertionError e ) {
            System.err.println(e.getMessage());
        } catch( Exception e ) {
            e.printStackTrace();
        } finally {
        }
        
        return inject;
	}
    
    static SqlProgram programInstance = null;
    
    public Parsed target = null;
    
    
    /**
     * Incrementally grow dependency chain
     * @param value    the assigned variable
     * @param key      one of arguments
     */
    void put( int key, int value, ParseNode scope, Parsed target ) {
    	dependencyLinks.put(key, value);    	
    	parseNode2Scope.put(key, scope);
    	parseNode2Scope.put(value, scope);
    }

    public LinkedList<Integer> dependencyChain( int startPos ) {
		for( int e : execs ) {
			ParseNode ss = parseNode2Scope.get(startPos); 
			ParseNode se = parseNode2Scope.get(e);
			if( ss != se )
				continue;
			LinkedList<Integer> ret = dependencyChain(startPos, e);
			if( ret != null )
				return ret;
		}
    	return null;
    }
    
    public LinkedList<Integer> dependencyChain( int startPos, int endPos ) {
    	List<LexerToken> src = target.getSrc();
    	LinkedList<Integer> chain = new LinkedList<Integer>();
    	chain.add(startPos);
    	if( tryDependency(startPos, endPos, chain) )
    		return chain;
        return null;    	 
    }
    
    private boolean tryDependency( int startPos, int endPos, LinkedList<Integer> chain ) {
    	List<LexerToken> src = target.getSrc();
    	int curPos = chain.getLast();
		if( src.get(endPos).content.equals(src.get(curPos).content) ) {
			if( endPos != curPos )
				chain.add(endPos);
			return true;
		}
		for( int k : dependencyLinks.keySet() ) {
			ParseNode sk = parseNode2Scope.get(k); 
			ParseNode csk = parseNode2Scope.get(curPos);
			if( sk != csk )
				continue;
			if( ! src.get(k).content.equals(src.get(curPos).content) )
				continue;
			if( chain.contains(k) )
				continue;
			int value = dependencyLinks.get(k);
			chain.add(value);
			boolean ret = tryDependency(startPos, endPos, chain);
			if( ret )
				return true;
			chain.removeLast();
		}
		return false;
    }
    
    public String dependencyStr( int startPos ) {
		for( int e : execs ) {
			String ret = dependencyStr(startPos, e);
			if( ret != null )
				return ret;
		}
    	return null;
    }
    
	public String dependencyStr( int startPos, int endPos ) {
		List<Integer> dependencyChain = dependencyChain(startPos, endPos);
		if( dependencyChain == null )
			return null;
		StringBuilder vars = new StringBuilder();
		int pos = 0;
		for( int curPos : dependencyChain ) {
    		final String varName = target.getSrc().get(curPos).content;
			final int lineNo = Service.charPos2LineNo(target.getInput(), target.getSrc().get(curPos).begin);

			if( 0 < pos++ )
				vars.append(" -> ");
			vars.append(varName+"@line#"+lineNo);
		}
		return vars.toString();
	}

    
	public void printDependencies() {
        System.out.println("+++++++++++++");
		for( int k : params ) 
        	System.out.println(dependencyStr(k));
	}


    
    ///////////////////////// callbacks & data structures
    // Functional dependency is specified as
    //     value := f(key)
    // e.g.
    //     l_theQuery := l_theQuery || DBMS_ASSERT.ENQUOTE_NAME(l_column_name);    
    //     ^^^^^^^^^^    ^^^^^^^^^^                             ^^^^^^^^^^^^^
    //     pos1          pos2                                   pos3
    // dependencyMap.put(pos2,pos1);         
    // dependencyMap.put(pos3,pos1);         
    public Map<Integer,Integer> dependencyLinks = new TreeMap<Integer,Integer>();  // not processed, raw links
    //public Map<Integer,Integer> filteredDependencyLinks = null; // not used anymore
    public Map<Integer,ParseNode> parseNode2Scope = new TreeMap<Integer,ParseNode>();     // to match variables in the scope
    
    public void assignments( Parsed target, Map<String,ParseNode> tuple ) {
    	//System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        ParseNode key = tuple.get("key");
        ParseNode value = tuple.get("value");
        ParseNode scope = tuple.get("scope");
        put(key.from,value.from,scope,target);
    }
    
    public void declarations( Parsed target, Map<String,ParseNode> tuple ) {
    	//System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        ParseNode key = tuple.get("key");
        ParseNode value = tuple.get("value");
        ParseNode scope = tuple.get("scope");
        put(key.from,value.from,scope,target);
    }
    
    public List<Integer> params = new LinkedList<Integer>();
    public void parametersInScope( Parsed target, Map<String,ParseNode> tuple ) {
    	//System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        ParseNode scope = tuple.get("scope");
        ParseNode an = tuple.get("arg_name");
        params.add(an.from);
        parseNode2Scope.put(an.from, scope);
    }
    
    public List<Integer> execs = new LinkedList<Integer>();
    public void executionsInScope( Parsed target, Map<String,ParseNode> tuple ) {
    	//System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        ParseNode scope = tuple.get("scope");
        ParseNode sv = tuple.get("sqlVar");
        execs.add(sv.from);
        parseNode2Scope.put(sv.from, scope);
    }



    

}
