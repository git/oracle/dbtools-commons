/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.arbori;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.arbori.AncestorDescendantNodes.Type;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;

/**
 * View table as column store database
 * Evaluate all unary predicates first
 * IndependentAttribute is such an unary relation
 * @author Dim
 *
 */
public class IndependentAttribute extends Attribute {
    Map<String, Predicate> db;
    private MaterializedPredicate content;
    public IndependentAttribute( String name, Map<String, Predicate> db ) {
        this.name = name;
        this.db = db;
    }
    
    public MaterializedPredicate getContent(){
        return content;
    }
    
    /**
     * Initialize an "unary" relation (with independent attributes and all its dependences)
     * @param root
     * @param src
     * @param varDefs
     * @param predVar
     */
    public void initContent( ParseNode root, List<LexerToken> src, Map<String, Attribute> varDefs, String predVar ) {
        ArrayList<String> attributes = new ArrayList<String>();
        //ArrayList<String> header = new ArrayList<String>();
        attributes.add(name);
        //header.add(name);
        
        for( String candidate : varDefs.keySet() ) {
            Attribute attr = varDefs.get(candidate);
            if( attr == null )
                attr = null;
            if( !attributes.contains(candidate) && attr.isDependent(name, varDefs) ) {
                attributes.add(candidate);
                //if( signature.contains(candidate) )
                    //header.add(candidate);
            }
        }
                
        content = new MaterializedPredicate(attributes/*header*/, src, null);
        Predicate fullPredicate = db.get(predVar);
        
        OUTER: for( ParseNode node : root.descendants() ) {
            if( node.from==7 && node.to==12 )
                node.from=7;
            Set<Tuple> candidates = new TreeSet<Tuple>();
            ParseNode[] t = new ParseNode[content.arity()];
            candidates.add(new Tuple(t));
            t[content.getAttribute(name)] = node;
            if( !unaryFilter(fullPredicate,name).eval(content.attributePositions, t, src) )
            	continue OUTER;
            for( String attr : attributes ) {
                candidates = content.assignDependencyChain(attr,candidates,varDefs,root);
                if( candidates.size() == 0 )
                    continue OUTER;
            }
            
            candidates = filterUnaryPredicates(varDefs,predVar,content.attributePositions,candidates,root,src);
            candidates = filterClosestAncestorDescendants(varDefs,predVar,content.attributePositions,candidates,root,src);
            
            for( Tuple tmp : candidates ) 
                content.tuples.add(tmp);                            
        }
    }

    int getLimits() {
        return content.cardinality();
    }
    
    // optimization 1
    private Predicate failfastFilter = null;
    void putFilter( Predicate filter ) {
        failfastFilter = filter;        
    }
    
    private Set<Tuple> filterClosestAncestorDescendants( Map<String, Attribute> varDefs, String predVar, Map<String, Integer> attributePositions, Set<Tuple> candidates, ParseNode root, List<LexerToken> src ) {
    	if( candidates.size() < 2 )
    		return candidates;
    	
    	for( String a : varDefs.keySet() ) {
    		Attribute attr = varDefs.get(a);
    		if( ! (attr instanceof AncestorExpr) )
    			continue;
    		AncestorExpr ancExpr = (AncestorExpr) attr;
     		if( ancExpr.type != Type.CLOSEST )
    			continue;
 			int iDesc = attributePositions.get(ancExpr.def);
 			int iAnc = attributePositions.get(ancExpr.name);
    		
     		Set<Tuple> delete = new TreeSet<Tuple>();
     		for( Tuple t1 : candidates )
         		for( Tuple t2 : candidates ) {
         			if( t1.compareTo(t2) <= 0 )
         				continue;
					ParseNode desc1 = t1.values[iDesc];
					ParseNode anc1 = t1.values[iAnc];
					ParseNode desc2 = t2.values[iDesc];
					ParseNode anc2= t2.values[iAnc];
					if( desc1 == desc2 )
						if( anc1.from <= anc2.from && anc2.to <= anc1.to ) {
							delete.add(t1);
							continue;
						} else if( anc2.from <= anc1.from && anc1.to <= anc2.to ) {
							delete.add(t2);
							continue;
						}
					if( anc1 == anc2 )
						if( desc1.from <= desc2.from && desc2.to <= desc1.to ) {
							delete.add(t2);
							continue;
						} else if( desc2.from <= desc1.from && desc1.to <= desc2.to ) {
							delete.add(t1);
							continue;
						}
         		}
     		
     		for( Tuple t : delete )
     			candidates.remove(t);
    	}
    	
        return candidates;
    }   
    
    private Set<Tuple> filterUnaryPredicates( Map<String, Attribute> varDefs, String predVar, Map<String, Integer> attributePositions, Set<Tuple> candidates, ParseNode root, List<LexerToken> src ) {
        if( failfastFilter == null ) {
            Predicate fullPredicate = db.get(predVar);
            failfastFilter = unaryFilter(varDefs, fullPredicate);
        }
        Set<Tuple> ret = new TreeSet<Tuple>();
        int i = -1;
        for( Tuple t: candidates ) {
        	i++;
        	if( 0 == i%1000 )
           		if( Thread.interrupted() )
        			throw new AssertionError("Interrupted");
            if( failfastFilter.eval(attributePositions, t.values, src) )
                ret.add(t);
        }
        return ret;
    }   
    
    /**
     * Unary predicate extract (for all attrinbutes)
     * @param varDefs
     * @param fullPredicate
     * @return
     */
    private Predicate unaryFilter( Map<String, Attribute> varDefs, Predicate fullPredicate ) {
        List<Predicate> tmp = new LinkedList<Predicate>();
        for( String s : varDefs.keySet() ) {
            Attribute attr = varDefs.get(s);
            if( attr.isDependent(name, varDefs) )
                tmp.add(unaryFilter(fullPredicate, attr.name));
        }
        
        Predicate ret = new True();
        for( Predicate extra : tmp )
            ret = appendProposition(extra, ret);
        return ret;
    }


    @Override
    Set<Tuple> eval(Map<String, Integer> attributePositions, Set<Tuple> candidates, ParseNode root) {
        throw new AssertionError("or should just return candidates?");
    }

    @Override
    Attribute referredTo(Map<String, Attribute> varDefs) {
        return null;
    }   
}


