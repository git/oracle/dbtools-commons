/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.arbori;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.arbori.Head;
import oracle.dbtools.arbori.Tail;
import oracle.dbtools.app.Format;
import oracle.dbtools.arbori.BindVar;
import oracle.dbtools.arbori.Position;
import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.Grammar;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.RuleTuple;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Array;
import oracle.dbtools.util.Pair;
import oracle.dbtools.util.Service;

/**
 * Arbori engine, see arbori.odt
 * 
 * Version 2.0
 * 
 * @author Dim
 *
 */
public class Program {
    
    public static boolean debug = false;
    public static boolean timing = false;
    
    
    protected Earley parser;
	public Program( Earley parser ) {
		this.parser = parser;
	}
	
	public static void main( String[] args ) throws Exception {
		
        //String input = "SELECT * FROM EMPLOYEES where FIRST_NAME not like 'Jeff%';";
        final String input = Service.readFile(Program.class, "test.sql");
        final List<LexerToken> src = LexerToken.parse(input);
        
        SqlProgram r = new SqlProgram(
        		/*"extraBrkBefore: \r\n" + 
        		"  ( _extraBrkBefore - commasInProc)\r\n" + 
        		"| :breaksBeforeComma & :breaksProcArgs & commasInProc \r\n" + 
        		"->;"
                Service.readFile(Format.class, "format.prg") //$NON-NLS-1$
                */
        		Service.readFile(Program.class, "test.arbori") //$NON-NLS-1$
         ) {
        	//int cursor = target.getInput().indexOf("p(pl_n)"); //<----------
            int caret = input.indexOf("p; --")/*+"p;".length()*/; 
            // 2 "extension" variables extracted by reflection
        	//int offset = LexerToken.scanner2parserOffset(target.getSrc(), cursor)+1;
            int offset = LexerToken.scanner2parserOffset(src, caret);
        	String newName = "-new name-";
			public Boolean getBoolBindVar( String name ) {
				return true;
			}

        };
        //System.out.println(r.dependency);

        //target.getRoot(); // to get better idea of timing
		//Service.profile(10000, 100, 5);
        //r.debug = true;
        Map<String,MaterializedPredicate> output = r.run(                
               input, src/*or null*/ //$NON-NLS-1$
        );
        for( String p : output.keySet() )
            System.out.println(p+"="+output.get(p).toString(p.length()+1));

	}

    private static int attribute = -1;
    private static int backslash = -1;
    private static int closePar = -1;
    private static int closeBr = -1;
    private static int col = -1;
    private static int conj = -1;
    private static int digits = -1;
    private static int disj = -1;
    private static int dot = -1;
    private static int eq = -1;
    private static int excl = -1;
    private static int identifier = -1;
    private static int header = -1;
    private static int lt = -1;
    private static int minus = -1;
    private static int node_parent = -1;
    private static int node_predecessor = -1;
    private static int node_position = -1;
    private static int node_successor = -1;
    private static int node = -1;
    private static int openBr = -1;
    private static int openPar = -1;
    private static int plus = -1;
    private static int predicate = -1;
    private static int referenced_node = -1;
    private static int rule = -1;
    private static int srcPtr = -1;
    private static int semicol = -1;
    private static int sharp = -1;
    private static int slash = -1;
    private static int statement = -1;
    private static int string_literal = -1;
	public static Earley getArboriParser() throws IOException  {
		Set<RuleTuple> rules = getRules();
        //RuleTuple.printRules(rules);
        Earley testParser = new Earley(rules) {
        	@Override
        	protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
        		LexerToken token = src.get(y);
        		return 
        		symbol == identifier && token.type == Token.IDENTIFIER 
        		||  symbol == identifier && token.type == Token.DQUOTED_STRING
        		;
        	}
        };
        attribute = testParser.symbolIndexes.get("attribute"); //$NON-NLS-1$
        backslash = testParser.symbolIndexes.get("'\\'"); //$NON-NLS-1$
        closePar = testParser.symbolIndexes.get("')'"); //$NON-NLS-1$
        closeBr = testParser.symbolIndexes.get("']'"); //$NON-NLS-1$
        col = testParser.symbolIndexes.get("':'"); //$NON-NLS-1$
        conj = testParser.symbolIndexes.get("'&'"); //$NON-NLS-1$
        digits = testParser.symbolIndexes.get("digits"); //$NON-NLS-1$
        disj = testParser.symbolIndexes.get("'|'"); //$NON-NLS-1$ 
        dot = testParser.symbolIndexes.get("'.'"); //$NON-NLS-1$
        eq = testParser.symbolIndexes.get("'='"); //$NON-NLS-1$
        excl = testParser.symbolIndexes.get("'!'"); //$NON-NLS-1$ 
        identifier = testParser.symbolIndexes.get("identifier"); //$NON-NLS-1$ 
        header = testParser.symbolIndexes.get("header"); //$NON-NLS-1$ 
        lt = testParser.symbolIndexes.get("'<'"); //$NON-NLS-1$ 
        minus = testParser.symbolIndexes.get("'-'"); //$NON-NLS-1$
        node_parent = testParser.symbolIndexes.get("node_parent"); //$NON-NLS-1$
        node_position = testParser.symbolIndexes.get("node_position"); //$NON-NLS-1$
        node_predecessor = testParser.symbolIndexes.get("node_predecessor"); //$NON-NLS-1$
        node_successor = testParser.symbolIndexes.get("node_successor"); //$NON-NLS-1$
        node = testParser.symbolIndexes.get("node"); //$NON-NLS-1$
        openBr = testParser.symbolIndexes.get("'['"); //$NON-NLS-1$
        openPar = testParser.symbolIndexes.get("'('"); //$NON-NLS-1$
        plus = testParser.symbolIndexes.get("'+'"); //$NON-NLS-1$
        predicate = testParser.symbolIndexes.get("predicate"); //$NON-NLS-1$
        referenced_node = testParser.symbolIndexes.get("referenced_node"); //$NON-NLS-1$
        rule = testParser.symbolIndexes.get("rule"); //$NON-NLS-1$
        semicol = testParser.symbolIndexes.get("';'"); //$NON-NLS-1$
        sharp = testParser.symbolIndexes.get("'#'"); //$NON-NLS-1$
        slash = testParser.symbolIndexes.get("'/'"); //$NON-NLS-1$
        srcPtr = testParser.symbolIndexes.get("'?'"); //$NON-NLS-1$
        statement = testParser.symbolIndexes.get("statement"); //$NON-NLS-1$
        string_literal = testParser.symbolIndexes.get("string_literal"); //$NON-NLS-1$
		return testParser;
	}
	private static Set<RuleTuple> getRules() throws IOException  {
        String input = Service.readFile(Program.class, "arbori.grammar"); //$NON-NLS-1$
        List<LexerToken> src = LexerToken.parse(input, false, LexerToken.QuotedStrings/*+SqlPlusComments*/); 
        ParseNode root = Grammar.parseGrammarFile(src, input);
        Set<RuleTuple> ret = new TreeSet<RuleTuple>();
        Grammar.grammar(root, src, ret);
        return ret;
    }

	private LinkedList<String> execOrder = new LinkedList<String>();
	/**
	 * @return List of predicates/queries in the order they are written in arbori program
	 *         Useful in situation when there is no callback marker identifying the latest query
	 */
	public LinkedList<String> querySequence() {
		return execOrder;
	}
	
	Map<String, Predicate> namedPredicates = new HashMap<String, Predicate>();
	// namedPredicates is mutating, so restore it for subsequent executions from symbolicPredicates
    //Map<String,Predicate> symbolicPredicates = new HashMap<String,Predicate>();
	
    private PredicateDependency dependency = new PredicateDependency();
    
    private Set<String> outputRelations = new HashSet<String>();
    
    private void copyState( Program source ) {
        //execOrder.clear();
        //execOrder.addAll(source.execOrder);
        execOrder = source.execOrder;
        namedPredicates.clear();
        //namedPredicates.putAll(source.namedPredicates);
        for( String key : source.namedPredicates.keySet() ) {
            Predicate value = source.namedPredicates.get(key);
            Predicate clone = value.copy(this);
            namedPredicates.put(key,clone);
        }
        /*symbolicPredicates.clear();
        //symbolicPredicates.putAll(source.symbolicPredicates);
        for( String key : source.symbolicPredicates.keySet() ) {
            Predicate value = source.symbolicPredicates.get(key);
            Predicate clone = value.copy(this);
            symbolicPredicates.put(key,clone);
        }*/
        dependency = source.dependency;
        outputRelations = source.outputRelations;
        parser = source.parser;
        bindsInstance = source.bindsInstance;
    }
    
    private Program compiledInstance = null;
    static protected HashMap<String,Program> compiledPrograms = new HashMap<String,Program>();
    private HashMap<String,Boolean> bindsInstance = null; // recompile the program every time bind changes
    /**
     * Compiles Arbori program.
     * Does it only once (for the same programText).
     * @param programText
     * @throws IOException
     */
    public void compile( String programText ) throws IOException {
        compiledInstance = compiledPrograms.get(programText);
        // recompile if boolean bind values changed
        if( compiledInstance != null && compiledInstance.bindsInstance != null ) {
        	for( String bindVar : compiledInstance.bindsInstance.keySet() ) {
				Boolean bindValue = compiledInstance.bindsInstance.get(bindVar);
				if( !bindValue.equals(getBoolBindVar(bindVar)) ) {
					compiledInstance = null;
					break;
				}
			}
        } else {
        	compiledInstance = null;
        }        	
        if( compiledInstance == null ) {
            Parsed prg = new Parsed(
                         programText, //$NON-NLS-1$
                         getArboriParser(),
                         "program" //$NON-NLS-1$
                );
            final ParseNode root = prg.getRoot();
            final SyntaxError error = prg.getSyntaxError();
            if( error != null )
            	System.out.println(error.getDetailedMessage());
			program(root, prg.getSrc(), prg.getInput());
            compiledInstance = new Program(parser);
            compiledInstance.copyState(this);
            compiledPrograms.put(programText,compiledInstance);
            compiledInstance.bindsInstance = new HashMap<String,Boolean>();
            Set<String> bindVars = listBindVariables(root, prg.getSrc());
            for( String bindVar : bindVars ) {
            	compiledInstance.bindsInstance.put(bindVar, getBoolBindVar(bindVar));
            }
        } else 
            copyState(compiledInstance);
    }
    private void program( ParseNode root, List<LexerToken> src, String input ) {
    	//root.printTree();
    	if( root.contains(statement) ) {
    		statement(root, src, input);
            //copyPredicates();
    		return;
    	}
		for( ParseNode child : root.children() )
			program(child, src, input);
		
		//copyPredicates();
	}

    /*private void copyPredicates() {
        for( String key : namedPredicates.keySet() )
		    symbolicPredicates.put(key, namedPredicates.get(key));
    }*/

    private void statement( ParseNode root, List<LexerToken> src, String input ) {
        if( root.contains(rule) )
            rule(root, src, input);
    }
    
    /**
     * Processes arbori syntax: 
     * 
       rule:
        identifier ':' predicate ';'
       ;
     */
	private void rule( ParseNode root, List<LexerToken> src, String input ) {
		String first = null;
		boolean legitimateOper = false;
		//ParseNode second = null;
		for( ParseNode child : root.children() ) {
			if( first == null ) {
				if( child.from+1==child.to )
					first = child.content(src);
				else
					return;
				continue;
			}
			
			if( !legitimateOper ) {
				if( child.contains(col) )
					legitimateOper = true;
				else
					return;
				continue;
			}
			
	    	if( child.contains(predicate) ) {
	    		Predicate p = predicate(child, src, input);
	            /*if( debug ) {
	                System.out.println("***predicate***="+first); //$NON-NLS-1$
	        		final Set<String> allVariables = new HashSet<String>();
	        		p.variables(allVariables);
	                System.out.println("symbolTable="+allVariables.toString()); //$NON-NLS-1$
	                System.out.println("nodeFilter="+p.toString()); //$NON-NLS-1$
	            }*/
	    		int x =0;
	    		if( p instanceof NodeContent && "sql_stmt".equals(((NodeContent)p).nodeVar) )
	    		    x++;
	    		Map<String, Boolean> dependencies = p.dependencies();
                for( String s : dependencies.keySet() )
	    		    dependency.addDependency(s, first, dependencies.get(s));
	            execOrder.add(first);
	            namedPredicates.put(first, p);

	    		//return;
	            continue;
	    	}
	    	
	    	// trailing "->"
	        if( child.contains(minus) ) {
	            outputRelations.add(first);
	            return;
	        } else
	            return;

		}
	}	

    
	private Predicate predicate( ParseNode root, List<LexerToken> src, String input ) {
	    if( root.contains(identifier) ) 
            return new PredRef(root.content(src),this);
	    
        Predicate ret = isBrackets(root, src, input);
        if( ret != null )
            return ret;
		ret = isParenthesis(root, src, input);
		if( ret != null )
			return ret;
		ret = isConjunction(root, src, input);
		if( ret != null )
			return ret;
		ret = isDisjunction(root, src, input);
		if( ret != null ) 
			return ret;
        ret = isDifference(root, src, input);
        if( ret != null ) 
            return ret;
		ret = isAtomicPredicate(root, src, input);
		if( ret != null )
			return ret;
		throw new AssertionError("unexpected case for: "+root.content(src));
	}

	/**
	 * Processes arbori syntax: predicate & predicate
	 */
	private Predicate isConjunction( ParseNode root, List<LexerToken> src, String input ) {
		ParseNode first = null;
		boolean legitimateOper = false;
		//ParseNode second = null;
		for( ParseNode child : root.children() ) {
			if( first == null ) {
				first = child;
				continue;
			}
			
			if( !legitimateOper ) {
				if( child.contains(conj) )
					legitimateOper = true;
				else
					return null;
				continue;
			}
			
			Predicate lft = predicate(first, src, input);
			Predicate rgt = predicate(child, src, input);
			return new CompositeExpr(lft, rgt, Oper.CONJUNCTION);
		}
		return null;
	}
	
    /**
     * Processes arbori syntax: predicate | predicate
     */
	private Predicate isDisjunction( ParseNode root, List<LexerToken> src, String input) {
		ParseNode first = null;
		boolean legitimateOper = false;
		//ParseNode second = null;
		for( ParseNode child : root.children() ) {
			if( first == null ) {
				first = child;
				continue;
			}
			
			if( !legitimateOper ) {
				if( child.contains(disj) )
					legitimateOper = true;
				else
					return null;
				continue;
			}
			
			Predicate lft = predicate(first, src, input);
			Predicate rgt = predicate(child, src, input);
			return new CompositeExpr(lft, rgt, Oper.DISJUNCTION);
		}
		return null;
	}

	   private Predicate isDifference( ParseNode root, List<LexerToken> src, String input) {
	        ParseNode first = null;
	        boolean isLegit = false;
	        //ParseNode second = null;
	        for( ParseNode child : root.children() ) {
	            if( first == null ) {
	                first = child;
	                continue;
	            }
	            
	            if( !isLegit ) {
	                if( child.contains(Program.minus) )
	                    isLegit = true;
	                else
	                    return null;
	                continue;
	            }
	            
	            Predicate lft = predicate(first, src, input);
	            Predicate rgt = predicate(child, src, input);
	            return new CompositeExpr(lft, rgt, Oper.DIFFERENCE);
	        }
	        return null;
	    }
	   
   /**
     * Processes arbori syntax: ( predicate )
     */
	private Predicate isParenthesis( ParseNode root, List<LexerToken> src, String input) {
		boolean isOpenParen = false;
		for( ParseNode child : root.children() ) {
			if( !isOpenParen ) {
				if( child.contains(openPar ) ) 
					isOpenParen = true;
				else
					return null;
				continue;
			}
			
			if( isOpenParen && child.contains(predicate) ) {
				return predicate(child, src, input);
			} else
				return null;
		}
		return null;
	}

	   /**
     * Processes arbori syntax: ( predicate )
     */
    private Predicate isBrackets( ParseNode root, List<LexerToken> src, String input) {
        boolean isOpenBr = false;
        for( ParseNode child : root.children() ) {
            if( !isOpenBr ) {
                if( child.contains(openBr ) ) 
                    isOpenBr = true;
                else
                    return null;
                continue;
            }
            
            if( isOpenBr ) {
                if( child.contains(header) )
                    return header(child, src, input);
                else if( child.contains(closeBr) )
                    return new MaterializedPredicate(new ArrayList<String>(),src,"[]");
            } else
                return null;
        }
        return null;
    }

    private MaterializedPredicate header( ParseNode root, List<LexerToken> src, String input) {
        if( root.contains(attribute) ) {
            ArrayList<String> hdr = new ArrayList<String>();
            hdr.add(root.content(src));
            return new MaterializedPredicate(hdr,src,"["+root.content(src)+"]");
        }
        MaterializedPredicate ret = null;
        for( ParseNode child : root.children() ) {
            if( ret == null )
                ret = header(child, src, input);
            else
                ret = MaterializedPredicate.join(ret, header(child, src, input));
        }
        return ret;
    }
	
	private Predicate isAtomicPredicate( ParseNode root, List<LexerToken> src, String input ) {
		Predicate ret = isExclamation(root, src, input);
		if( ret != null )
			return ret;
		ret = isNodeContent(root, src, input);
		if( ret != null )
			return ret;
		ret = isNodeMatchingSrc(root, src, input);
		if( ret != null )
			return ret;		
		/*ret = isNotCoveredByVectorNodes(root, src, input);
		if( ret != null )
			return ret;*/
		// 
		ret = isSameNode(root, src, input);
		if( ret != null )
			return ret;
		ret = isNodeAncestorDescendant(root, src, input);
		if( ret != null )
			return ret;
        ret = isAggregate(root, src, input);
        if( ret != null )
            return ret;
		//
		ret = isBoolBindVar(root, src, input);
		if( ret != null )
			return ret;
		ret = isChildNumRelation(root, src, input);
		if( ret != null )
			return ret;
		ret = isPositionalRelation(root, src, input);
		if( ret != null )
			return ret;
		return null;
	}

	private Predicate isAggregate(ParseNode root, List<LexerToken> src, String input) {
        Boolean slash1 = null;
        Boolean slash2 = null;
        ParseNode attribute = null;
        boolean seenOpenParen = false;
        ParseNode p = null;
        for( ParseNode child : root.children() ) {
            if( slash1 == null ) {
                if( child.contains(slash) ) 
                    slash1 = true;
                else if( child.contains(backslash) ) 
                    slash1 = false;
                else
                    return null;
                continue;
            }
            if( slash2 == null ) {
                if( child.contains(slash) ) 
                    slash2 = true;
                else if( child.contains(backslash) ) 
                    slash2 = false;
                else
                    return null;
                continue;
            }
            if( attribute == null ) {
                attribute = child;
                continue;
            }
            if( !seenOpenParen ) {
                if( child.contains(openPar) ) {
                    seenOpenParen = true;
                    continue;
                } else
                    throw new AssertionError("Syntax error not caught by parsing?");
            }
            p = child;
            break;
        }
        Predicate predicate = predicate(p, src, input);
        return new AggregatePredicate(attribute.content(src),predicate,slash1,slash2);
    }

    /**
     * Processes arbori syntax: ! predicate
     */
	private  Predicate isExclamation( ParseNode root,List<LexerToken> src, String input )  {
		boolean isExcl = false;
		for( ParseNode child : root.children() ) {
			if( !isExcl ) {
				if( child.contains(excl ) ) 
					isExcl = true;
				else
					return null;
				continue;
			}
			
			if( isExcl ) {
				return new CompositeExpr(predicate(child, src, input),null, Oper.NEGATION);
			} 
		}
		return null;
	}
	
    /**
     * Processes arbori syntax: [ node ) content
     */
	private Predicate isNodeContent( ParseNode root, List<LexerToken> src, String input ) {
		boolean openBrace = false;
		String first = null;
		boolean legitimateOper = false;
		String second = null;
		for( ParseNode child : root.children() ) {
			if( !openBrace ) {
				if( child.contains(openBr) )
					openBrace = true;
				else
					return null;
				continue;
			}
			
			if( first == null && child.contains(node) ) {
				if( child.from+1==child.to 
				 || child.contains(node_parent) 
				 || child.contains(node_predecessor) 
				 || child.contains(node_successor) 
                 || child.contains(referenced_node) 
				)
					first = child.content(src);
				else
					return null;
				continue;
			}
			
			if( !legitimateOper ) {
				if( child.contains(closePar) )
					legitimateOper = true;
				else
					return null;
				continue;
			}
			
			if( second == null  ) {
				second = child.content(src);
				continue;
			}
				
			throw new AssertionError("unexpected case");
		}
		Integer symbol = parser.symbolIndexes.get(second);
		if( symbol == null )
			throw new AssertionError("Symbol '"+second+"' not found");
		return new NodeContent(first, symbol);
	}

	public Boolean addWsDividers = null;  // customize it for your Program instance
	
    /**
     * Processes arbori syntax: ?node = ?node
     */
	private Predicate isNodeMatchingSrc( ParseNode root, List<LexerToken> src, String input ) {
		boolean legitimateAt1 = false;
		String first = null;
		boolean legitimateOper = false;
		boolean legitimateAt2 = false;
		String second = null;
		for( ParseNode child : root.children() ) {
			if( !legitimateAt1 ) {
				if( child.contains(srcPtr) )
					legitimateAt1 = true;
				else
					return null;
				continue;
			}
			
			if( first == null ) {
				if( /*child.from+1==child.to &&*/ child.contains(node) )
					first = child.content(src);
				else
					return null;
				continue;
			}

			if( !legitimateOper ) {
				if( child.contains(eq) )
					legitimateOper = true;
				else
					return null;
				continue;
			}

			if( !legitimateAt2 ) {
				if( child.contains(srcPtr) )
					legitimateAt2 = true;
				else if( child.contains(string_literal) ) {
					return new NodeMatchingSrc(first, child.content(src), addWsDividers);
				} else
					return null;
				continue;
			}
			
			if( second == null  ) {
				if( /*child.from+1==child.to ||*/ child.contains(node) )
					second = child.content(src);
				else
					return null;
				continue;
			}

			throw new AssertionError("unexpected case");
		}
		return new NodesWMatchingSrc(first, second);
	}
	

	private Predicate isNodeAncestorDescendant( ParseNode root, List<LexerToken> src, String input ) {
		AncestorDescendantNodes.Type type = AncestorDescendantNodes.Type.CLOSEST;
		Pair<String,String> p = binaryPredicateNames(root, src, lt);
		if( p == null ) {
	        p = binaryPredicateNames(root, src, lt,eq);
	        type = AncestorDescendantNodes.Type.TRANSITIVE;
			if( p == null ) {
		        p = binaryPredicateNames(root, src, lt,lt);
		        type = AncestorDescendantNodes.Type.TRANSITIVE;
		        if( p == null )
		            return null; //investigate other relationships, do not throw new AssertionError("Unrecognized ancestor-descendant relationship");
			}
		}
		return new AncestorDescendantNodes(p.first(),p.second(),type);
	}
	private Predicate isSameNode( ParseNode root, List<LexerToken> src, String input ) {
		Pair<String,String> p = binaryPredicateNames(root, src, eq);
		if( p == null )
			return null;
		return new SameNodes(p.first(),p.second());
	}

	public Pair<String,String> binaryPredicateNames( ParseNode root, List<LexerToken> src, final int oper) throws AssertionError {
		return binaryPredicateNames(root, src, oper, -1);
	}	
    public Pair<String,String> binaryPredicateNames( ParseNode root, List<LexerToken> src, final int oper1, final int oper2) throws AssertionError {
        String first = null;
        boolean legitimateOper1 = false;
        boolean legitimateOper2 = (-1 == oper2);
        String second = null;
        for( ParseNode child : root.children() ) {
            if( first == null ) {
                if( child.contains(node) ) {
                    first = child.content(src);                 
                    if( namedPredicates.containsKey(first) )
                        throw new AssertionError("Error: "+first+" is a predicate, not predicate attribute within binary operation");
                } else
                    return null;
                continue;
            }
            
            if( !legitimateOper1 ) {
                if( child.contains(oper1) )
                    legitimateOper1 = true;
                else
                    return null;
                continue;
            }
            if( !legitimateOper2 ) {
                if( child.contains(oper2) )
                    legitimateOper2 = true;
                else
                    return null;
                continue;
            }
            
            if( second == null ) {
                if( child.contains(node) ) {
                    second = child.content(src);                    
                    if( namedPredicates.containsKey(second) )
                        throw new AssertionError("Error: "+second+" is a predicate, not predicate attribute within binary operation");
                } else
                    return null;
                continue;
            }                
            
            if( !child.contains(semicol) )
                throw new AssertionError("unexpected case for: "+root.content(src));
        }
        return new Pair<String,String>(first,second);
    }   
		
	private Predicate isPositionalRelation( ParseNode root, List<LexerToken> src, String input ) {
	    //LexerToken.print(src, root.from, root.to);
	    Position first = null;
        boolean isGT = false;
		boolean isReflexive = false;
		Position second = null;
		for( ParseNode child : root.children() ) {
			if( first == null ) {				
				first = nodeRelativePosition(child,src,input);
				if( first == null )
					return null;
				continue;
			}
			
            if( child.contains(lt) ){
                isGT = true;           
                continue;
            }
            if( child.contains(eq) ) {
                isReflexive = true;           
                continue;
            }

			if( second == null  ) {
				second = nodeRelativePosition(child,src,input);
				if( second == null )
					return null;
				continue;
			}
			
			throw new AssertionError("unexpected case");
		}
		return new PositionalRelation(first, second, isReflexive, isGT, this);
	}
	private Position nodeRelativePosition( ParseNode root, List<LexerToken> src, String input ) {
		String name = null;
		Position t = null;
		int num = -1;
		for( ParseNode child : root.children() ) {			
			if( name == null && child.contains(attribute) || t instanceof BindVar ) {
				name = child.content(src);
				continue;
			} else if( child.contains(digits) ) {
                num = Integer.decode(child.content(src));
                if( t != null )
                    ((Composite)t).addendum = num;
                continue;
            } else if( t == null ) {
				if( child.contains(openBr) ){
					t = new Head(name);
					continue;
				} else if( child.contains(closePar) ) {
					t = new Tail(name);				
					continue;
				} else if( child.contains(col) ) {
					t = new BindVar(name);				
					continue;
				}  else if( child.contains(plus) ) {
                    continue;
                } else if( child.contains(node_position) ) {
                    Position tmp = nodeRelativePosition(child, src, input);
                    name = tmp.name; 
                    t = new Composite(tmp,num);
                    continue;
				} else
					throw new AssertionError();
			}
		}
		if( name == null )
			throw new AssertionError("name == null");
		if( t == null )
			throw new AssertionError("t == null");
		t.setName(name);
		return t;
	}

	private Predicate isChildNumRelation( ParseNode root, List<LexerToken> src, String input ) {
	    //LexerToken.print(src, root.from, root.to);
	    Integer first = null;
        boolean isGT = false;
		boolean isReflexive = false;
		String second = null;
		for( ParseNode child : root.children() ) {
			if( first == null ) {
				try {
					first = Integer.parseInt(src.get(child.from).content);
				} catch( Exception e ) {
					return null;
				}
				if( first == null )
					return null;
				continue;
			}
			
            if( child.contains(lt) ){
                isGT = true;           
                continue;
            }
            if( child.contains(eq) ) {
                isReflexive = true;           
                continue;
            }
            if( child.contains(sharp) ) {
                 continue;
            }

			if( second == null  ) {
				second = src.get(child.from).content;
				if( second == null )
					return null;
				continue;
			}
			
			throw new AssertionError("unexpected case");
		}
		return new ChildNumRelation(first, second, isReflexive, isGT, this);
	}

	public Boolean getBoolBindVar( String name ) {
		return null;
	}
    private Predicate isBoolBindVar( ParseNode root, List<LexerToken> src, String input ) {
		if( !root.contains("bind_var") )
			return null;
		String name = src.get(root.from+1).content;
		Boolean b = getBoolBindVar(name);
        if( b == null )
        	throw new AssertionError("Bind var '"+name+"' not found.");
        if( b )
        	return new True();
        else
        	return new False();
	}
    private Set<String> listBindVariables( ParseNode root, List<LexerToken> src ) {
    	Set<String> ret = new HashSet<String>();
    	if( root.contains("bind_var") 
    	 && root.contains("atomic_predicate") // boolean bind var
    	) {
    		ret.add(src.get(root.from+1).content);
    		return ret;
    	}
    	for( ParseNode child : root.children() )
    		ret.addAll(listBindVariables(child,src));
    	return ret;
    }


	
	/**
	 *  major runtime method 
	 */
	public Map<String,MaterializedPredicate> eval( Parsed target )  {
	    return eval(target, null);
	}

	/**
	 * @param target
	 * @param action -- callback object
	 * @return
	 */
	public Map<String,MaterializedPredicate> eval( Parsed target, Object action )  {
	    final ParseNode root = target.getRoot();
	    if( timing ) {
	        System.out.println("tree depth ="+root.treeDepth());
	        System.out.println("#tokens ="+target.getSrc().size());
	    }
        long t1 = System.currentTimeMillis();
        
	    Map<String,MaterializedPredicate> ret = new HashMap<String,MaterializedPredicate>();
	    
	    for( String predVar : execOrder ) {
	        long t11 = System.currentTimeMillis();
	        if( debug )
	            System.out.println(">=================================<     "+predVar);
		    MaterializedPredicate table = new MaterializedPredicate(predVar,_eval(target, predVar));
            if( timing ) {
                System.out.print(predVar+" eval time = "+(System.currentTimeMillis()-t11)); // (authorized) //$NON-NLS-1$
                System.out.println("       (cardinality="+table.cardinality()+")");         // (authorized) //$NON-NLS-1$
            }

            table.name = predVar; 
            ret.put(predVar,table);
		    namedPredicates.put(predVar, table);
		    table.trimAttributes();
	        if( debug /*|| outputRelations.contains(predVar)*/ )
	            System.out.println(predVar+"="+table);         

		}
	    
	    namedPredicates = /*symbolicPredicates.clone()*/ new HashMap<String,Predicate>();
        for( String key : compiledInstance.namedPredicates.keySet() )
            namedPredicates.put(key, compiledInstance.namedPredicates.get(key).copy(this) );
        
        long t2 = System.currentTimeMillis();
        if( debug || timing )
            System.out.println("eval time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
               
        if( action != null ) { // callback class
            if( outputRelations.size() == 0 )   
            	throw new AssertionError("Did you forget to mark output relations?");
            for( String predVar : execOrder ) {
                if( !outputRelations.contains(predVar)  )
                    continue;
                if( debug )
                    System.out.println("-------->>>   "+predVar);
                Class c = action.getClass();
                try {
                	Method callback = null;
                	try {
                		callback = c.getDeclaredMethod(predVar, Parsed.class, Map.class);
                	} catch ( NoSuchMethodException | SecurityException  | IllegalArgumentException e ) {
                		callback = c.getMethod(predVar, Parsed.class, Map.class);
                    }
                    callback.setAccessible(true);
                    MaterializedPredicate mp = ret.get(predVar); 
                    for( Tuple t : mp.tuples ) {
                        Map<String,ParseNode> tuple = new HashMap<String,ParseNode>();
                        for ( int j = 0; j < mp.arity(); j++ ) {
                            String colName = mp.getAttribute(j);
                            ParseNode node = mp.getAttribute(t, colName); 
                            tuple.put(colName, node);
                        }
                        callback.invoke(action, target, tuple);
                    }
                } catch ( NoSuchMethodException | SecurityException | IllegalAccessException 
                        | IllegalArgumentException | InvocationTargetException e ) {
                    System.err.println(predVar +" callback: "+ e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        if( debug || timing )
            System.out.println("callback time = "+(System.currentTimeMillis()-t2)); // (authorized) //$NON-NLS-1$


		return ret;
	}
		
    // Map<String,MaterializedPredicate> materializedPredicates = new HashMap<String,MaterializedPredicate>();
	// Now Predicate mutate into MaterializedPredicate, so keep them in namedPredicates 
	private MaterializedPredicate _eval( Parsed target, String predVar )  {		    
		final Predicate evaluatedPredicate = namedPredicates.get(predVar);
		MaterializedPredicate ret = evaluatedPredicate.eval(target);
		if( ret != null )
		    return ret; // predicate is defined in terms of predicate variables alone
				
        AttributeDefinitions varDefs = new AttributeDefinitions(predVar, namedPredicates);
        varDefs.evalDimensions(target,false);
        //if( limits[0] == 0 )  redundant: if one dimension is 0, then it would be outer nested loop iterated 0 times
            //return new MaterializedPredicate(varDefs.getHeader(), target.getSrc(), null);
        boolean firstTime = true;
        if( debug ) {
        	for( String varDef : varDefs.listDimensions() ) {
        		System.out.print(" "+varDef);
        	}
        	System.out.println();
        	System.out.print("Eval space = ");
        	for( String varDef : varDefs.listDimensions() ) {
				System.out.print((firstTime?"":"x")+varDefs.getDimensionContent(varDef).cardinality());
				firstTime = false;
			}
        	System.out.println();
        }
        firstTime = true;
        
        /////////////// Try evaluating better than through full dimensional cartesian product /////////////
        String firstDimension = null;
        for( String dim : varDefs.listDimensions() ) {
            if( firstDimension == null || varDefs.getDimensionContent(dim).cardinality() < varDefs.getDimensionContent(firstDimension).cardinality() ) {
                firstDimension = dim;
            }
        }
        Set<String> joined = new HashSet<String>();
        joined.add(firstDimension);
        Attribute firstAttr = varDefs.get(firstDimension); 
        ret = ((IndependentAttribute)firstAttr).getContent();
        while( joined.size() < varDefs.listDimensions().size() ) {
            String current = varDefs.minimalRelatedDimension(joined, ret.cardinality());
            if( current == null ) // failed to find candidate for joining
                throw new AssertionError("Cartesian product evaluation: failed to find attribute joined to "+joined);
            joined.add(current);
            Attribute second = varDefs.get(current);
            
            MaterializedPredicate pred2 = ((IndependentAttribute)second).getContent();
            Predicate filter = new True();
            if( joined.size() != varDefs.listDimensions().size() ) {
                for( String a : ret.attributes ) {
                    if( ret.name != null )
                        a = ret.name + "." + a;
                    for( String b : pred2.attributes ) {
                        if( pred2.name != null )
                            b = pred2.name + "." + b;
                        Predicate rel = evaluatedPredicate.isRelated(a, b, varDefs);
                        if( rel != null ) {
                            if( filter instanceof True )
                                filter = rel;
                            else {
                                if( filter != rel )
                                    filter = new CompositeExpr(filter, rel, Oper.CONJUNCTION);
                            }
                        }
                    }
                }
                if( filter instanceof True ) {
                	if( 1 < varDefs.getDimensionContent(current).cardinality()  
                	 &&	1 < ret.cardinality()	
                			)
                		throw new AssertionError("Cartesian product evaluation; check for missing binary predicates");
                }
            } else
                filter = evaluatedPredicate;

            ret = MaterializedPredicate.filteredCartesianProduct(ret, pred2,filter,varDefs,target.getRoot());
            if( debug )
                System.out.println("dim#"+joined.size()+",cardinality="+ret.cardinality());
        }
        /*if( varDefs.listDimensions().size() == 1 ) filteredCartesianProduct does not disambiguate
              // e.g.   "pc+fml":  pc.id+1=fml_part & [fml_part) fml_part ;
              // Here independent var "pc" is defined somewhere before this rule 
              // Need one more attribute "fml_part" to add to the result and couple predicates to evaluate
              // This happens because MaterializedPredicate.filteredCartesianProduct() is called only when joining
             */
            ret = MaterializedPredicate.filter(ret, evaluatedPredicate,varDefs,target.getRoot());        
        if( joined.size() == varDefs.listDimensions().size() )
            return ret;
        
        throw new AssertionError("Missing dyadic predicate in predvar "+predVar+"; won't evaluate cartesian product");        
	}

    
    /*private boolean skipEmptyRelationEval( String predVar ) {
        for( String target : outputRelations )
            if( dependency.isDependent(predVar, target) ) {
                Map<String, Boolean> backRefs = dependency.backward.get(target);
                if( backRefs == null ) // empty dependencies for NodeContent
                    return false;
                boolean hasNullifier = false;
                for( String src : backRefs.keySet() ) {
                    if( Boolean.FALSE == backRefs.get(src) )
                        continue;
                    Predicate sp = namedPredicates.get(src);
                    if( !(sp instanceof MaterializedPredicate) )
                        continue;
                    if( ((MaterializedPredicate)sp).tuples.size()==0 ) {
                        hasNullifier = true;
                        break;
                    }
                }
                if( !hasNullifier ) {
                    if( debug )
                        System.out.println("No nullifier for "+target);
                    return false;
                }
            }
        return true;
    }*/


	


}
