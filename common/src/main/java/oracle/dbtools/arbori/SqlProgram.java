/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.arbori;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.dbtools.app.Obfuscator;
import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.util.Service;

public class SqlProgram extends Program {
    
    public SqlProgram( Class<?> dir, String file ) throws IOException {
        this(Service.readFile(dir, file));
    }
    /**
     * @param programText
     * @throws IOException
     */
	public SqlProgram( String programText ) throws IOException {
	    super(SqlEarley.getInstance());
	    compile(programText);
	}

	/**
	 * @param input - text to process
	 * @param src   - lexed text (perform lexical analysis if null)
     * @param callback - an object to execute a method (via reflection) for each predicate tuple
	 * @return
	 * @throws IOException
	 */
	public Map<String, MaterializedPredicate> run( String input, List<LexerToken> src, 
	                                                             Object callback ) throws IOException {
	    return run(input, src, null, callback);
	}
	
	public Map<String, MaterializedPredicate> run( String input, List<LexerToken> src, String[] rootSyntax,
	                                               Object callback ) throws IOException {
	    return run(input, src, rootSyntax, callback, SqlEarley.getInstance());
	}

	
	public Map<String, MaterializedPredicate> run( String input, List<LexerToken> src, String[] rootSyntax,
			Object callback, Earley parser ) throws IOException {
		if( src == null )
			src = LexerToken.parse(input);
		Parsed target = new Parsed(  // this is PL/SQL source parsed into a tree which arbori program is applied to
				input, 
				src,
				parser,
				rootSyntax //$NON-NLS-1$
				);
		final Map<String,MaterializedPredicate> predicateVectors = eval(target, callback);
		return predicateVectors;
	}

	
	public Map<String, MaterializedPredicate> run( String input, final List<LexerToken> src  ) throws IOException {
	    
	    return run(input, src, null);
	}

    public Map<String, MaterializedPredicate> run( String input, Object callback  ) throws IOException {
        
        return run(input, null, callback);
    }

    public Map<String, MaterializedPredicate> run( String input ) throws IOException {
        
        return run(input, null, null);
    }
    
    public static void main( String[] args ) throws IOException {
    	String input  = "p: [id) identifier | [x^) where_clause & ?x = 'where';";
    	final Earley arboriParser = getArboriParser();
        final boolean T = true;   // less typing
        final boolean F = false;
        arboriParser.printOrderedRules("predicate", /*headOnly=*/T, /*exactmatch=*/T);
		Parsed prg = new Parsed(
                input, //$NON-NLS-1$
                arboriParser,
                "program" //$NON-NLS-1$
       );
       prg.getRoot().printTree();

    }
}
