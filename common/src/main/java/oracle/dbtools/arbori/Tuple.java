/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.arbori;

import java.util.HashMap;
import java.util.Map;

import oracle.dbtools.parser.ParseNode;

public class Tuple implements Comparable<Tuple> {
	ParseNode[] values;

	public Tuple( ParseNode[] attributes ) {
		this.values = attributes;
	}

	@Override
	public int compareTo( Tuple o ) {
		for( int i = 0; i < values.length; i++ ) {
			if( values[i] == null && o.values[i] == null )
				continue;
			if( values[i] == null )
				return -1;
			if( o.values[i] == null )
				return 1;
			int cmp = values[i].compareTo(o.values[i]);
			if( cmp != 0 )
				return cmp;
		}
		return 0;
	}
	
	/**
	 * Utility method to convert a object representtion into a map 
	 * This is useful because arbori callback methods represent tuple as Map<String,ParseNode>
	 * While explicit iteration MaterializedPredicate.getTuples() returns collection of Tuples 
	 * @param container
	 * @return
	 */
	public Map<String,ParseNode> decode( MaterializedPredicate container ) {
		Map<String,ParseNode> tuple = new HashMap<String,ParseNode>();
		for ( int j = 0; j < container.arity(); j++ ) {
			String colName = container.getAttribute(j);
			ParseNode node = container.getAttribute(this, colName); 
			tuple.put(colName, node);
		}
		return tuple;
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder("<");
		for ( int i = 0; i < values.length; i++ ) {
			if( i > 0 )
				ret.append(",");
			ret.append(values[i].interval());
		}
		return ret.toString()+">";
	}

	
	
}
