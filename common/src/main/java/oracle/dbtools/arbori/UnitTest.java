/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.arbori;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.Grammar;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.RuleTuple;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.Visual;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Pair;
import oracle.dbtools.util.Service;

/**
 * Catches regression in parse tree shapes due to SQL grammar and parser code evolutions
 * 
 * for test grammar see unitTest.grammar
 * 
 * the test cases are in unit.test
 * 
 * @author Dim
 */
public class UnitTest {
    static Integer singleTest = null; // null run everything
    
    static boolean debug = false;
    static boolean printDiffs = true;

    static int testNo = 0;
    static boolean unitTestIsOK = true;
    
    private static int assertion = -1;
    private static int atest = -1;
    private static int attribute = -1;
    private static int closePar = -1;
    private static int comment = -1;
    private static int col = -1;
    private static int content = -1;
    private static int header = -1;
    private static int identifier = -1;
    private static int marker = -1;
    private static int openPar = -1;
    private static int output = -1;
    private static int program_code_or_filename = -1;
    private static int program_with_input = -1;
    private static int sql_prefix = -1;
    private static int sql_postfix = -1;
    private static int table = -1;
    private static int tables = -1;
    private static int value = -1;
    
    public static void main( String[] args ) throws Exception {
        Set<RuleTuple> rules = getRules();
        //RuleTuple.printRules(rules);
        Earley testParser = new Earley(rules) {
            @Override
            protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
        		LexerToken token = src.get(y);
                return 
                      symbol == identifier && token.type == Token.IDENTIFIER 
                  ||  symbol == identifier && token.type == Token.DQUOTED_STRING
                  ||  symbol == identifier && token.type == Token.BQUOTED_STRING
                ;
            }
        };
        identifier = testParser.identifier;
        assertion = testParser.symbolIndexes.get("assertion");
        atest = testParser.symbolIndexes.get("atest");
        attribute = testParser.symbolIndexes.get("attribute");
        closePar = testParser.symbolIndexes.get("')'");
        col = testParser.symbolIndexes.get("':'");
        comment = testParser.symbolIndexes.get("comment");
        content = testParser.symbolIndexes.get("content");
        header = testParser.symbolIndexes.get("header");
        output = testParser.symbolIndexes.get("output");
        marker = testParser.symbolIndexes.get("marker");
        openPar = testParser.symbolIndexes.get("'('");
        program_with_input = testParser.symbolIndexes.get("program_with_input");
        program_code_or_filename = testParser.symbolIndexes.get("program_code_or_filename");
        sql_prefix = testParser.symbolIndexes.get("\"sql prefix\"");
        sql_postfix = testParser.symbolIndexes.get("\"sql postfix\"");
        table = testParser.symbolIndexes.get("table");
        tables = testParser.symbolIndexes.get("tables");
        value = testParser.symbolIndexes.get("value");
                
        String input = Service.readFile(UnitTest.class, "unit.test"); //$NON-NLS-1$
        //String input =  Service.readFile("/EclipseWorkspace/common/common/src/oracle/dbtools/arbori/unit.test");
        List<LexerToken> src =  LexerToken.parse(input,"`");        
        Visual visual = null;
        //visual = new Visual(src, testParser);
        Matrix matrix = new Matrix(testParser);
        testParser.parse(src, matrix); 
         
        SyntaxError s = SyntaxError.checkSyntax(input, new String[]{"atest"}, src, testParser, matrix);
        if( s != null ) { //$NON-NLS-1$ //$NON-NLS-2$
        	if( visual != null )
        		visual.draw(matrix);
            System.err.println("Syntax Error");
            System.err.println("at line#"+s.line);
            System.err.println(s.code);
            System.err.println(s.marker);
            System.err.println("Expected:  ");
                for( String tmp : s.getSuggestions() )
                    System.out.print(tmp+',');
            throw new Exception(">>>> syntactically invalid code fragment <<<<"); //$NON-NLS-1$
        }
        
        ParseNode root = testParser.forest(src, matrix);
        
        long t1 = System.currentTimeMillis();
        atest(root, src, input);
        
        System.out.println("time= "+(System.currentTimeMillis()-t1)+" ms (compared to 550 ms)"); //$NON-NLS-1$
        if( unitTestIsOK )
            System.out.println("*** ALL TESTS are OK *** ---> "); //$NON-NLS-1$
        else {
            System.err.println("*** TEST FAILED! *** ---> ");        //$NON-NLS-1$
            System.exit(2);
        }
    }

    private static Set<RuleTuple> getRules() throws Exception {
        String input = Service.readFile(UnitTest.class, "unitTest.grammar"); //$NON-NLS-1$
        List<LexerToken> src = LexerToken.parse(input, false, LexerToken.QuotedStrings/*+SqlPlusComments*/); 
        ParseNode root = Grammar.parseGrammarFile(src, input);
        Set<RuleTuple> ret = new TreeSet<RuleTuple>();
        Grammar.grammar(root, src, ret);
        return ret;
    }
    
    /**
     * @param root
     * @param src
     * @return node that violates assertion
     * @throws Exception
     */
    private static void atest( ParseNode root, List<LexerToken> src, String input ) throws Exception {    	
        if( root.contains(assertion) ) {
        	if( singleTest != null && (int)singleTest != testNo ) {
        		testNo++;
        		return;
        	}
            assertion(root,src,input);
            return;
        }
    	
        if( root.contains(output) ) {
        	if( singleTest != null && (int)singleTest != testNo ) {
        		testNo++;
        		return;
        	}
        	output(root,src,input);
        	return;
        }
        
        if( root.contains(comment) )
            comment(root,src,input);

        for( ParseNode child : root.children() ) {
            atest(child,src, input);   
        }
    }

    private static void output( ParseNode root, List<LexerToken> src, String input ) throws Exception {            	
    	for( ParseNode child : root.children() ) {
    		Map<String, MaterializedPredicate> output = runProgram(child,src,input);
    		for( String p : output.keySet() ) {
    			MaterializedPredicate pred = output.get(p);
    			if( pred == null )
                    System.err.println("!!! missing predicate "+p); //$NON-NLS-1$ 
    			else
    			    System.out.println(p+"="+pred.toString(p.length()+1)); //$NON-NLS-1$ 
    		}
        	System.out.println("TEST#"+(testNo++)+":  ^^^^^ "); //$NON-NLS-1$ //$NON-NLS-2$
    	    return; 
    	}
    }
    
    static class UTProgram extends SqlProgram {
		public UTProgram(String arboriProgram) throws IOException {
            super( arboriProgram );
         }
        int offset = -1;
        String newName = "-new name-";
    };
    static Map<String, Parsed> parsed = new HashMap<String, Parsed>();
    static Map<String, UTProgram> programs = new HashMap<String, UTProgram>();    
    private static Map<String, MaterializedPredicate> runProgram( ParseNode root, List<LexerToken> src, String input ) throws Exception {     	
    	UTProgram program = null;
    	boolean sawCol = false;
    	String prefix = null;
    	int markerPos = -1;
    	String postfix = null;
    	for( ParseNode child : root.children() ) {
        	if( child.contains(program_code_or_filename) ) {
            	program = program(child,src);
        		continue;
        	}
        	if( !sawCol && child.contains(col) ) {
        		sawCol = true;
        		continue;
        	}
        	if( prefix == null && child.contains(sql_prefix) ) {
            	prefix = input.substring(src.get(child.from).begin, src.get(child.to-1).end);
            	markerPos = child.to - child.from;
        		continue;
        	}
        	if( child.contains(marker) ) {
        		continue;
        	}
        	if( postfix == null && child.contains(sql_postfix) ) {
        		postfix = input.substring(src.get(child.from).begin, src.get(child.to-1).end);
        		continue;
        	}
        	throw new AssertionError("Unexpected case");
        }

        Program.debug = debug;
        program.offset = markerPos; 
        //Map<String,MaterializedPredicate> predicateVectors = program.eval(target);
        Map<String,MaterializedPredicate> predicateVectors = program.run(
                    postfix == null ? prefix : prefix+' '+postfix, null
        );

        Map<String, MaterializedPredicate> output = new HashMap<String, MaterializedPredicate>();
        for( String p : program.namedPredicates.keySet() ) {
        	output.put(p, predicateVectors.get(p));
        }
    	return output;
    }
    
    private static UTProgram program( ParseNode root, List<LexerToken> src ) throws IOException {
    	String str = root.content(src);
    	if( str.charAt(0) == '\"' )
    		str = str.substring(1,str.length()-1);
    	else if( str.charAt(0) == '`' )
    		str = str.substring(1,str.length()-1);
    	
    	UTProgram ret = programs.get(str);
    	if( ret != null )
    		return ret;
    	
    	List<LexerToken> lexed = LexerToken.parse(str);
    	final ParseNode tmp = UTProgram.getArboriParser().parse(lexed);
    	
    	String code = str;
    	if( !tmp.contains(UTProgram.getArboriParser().getSymbol("program")) ) {
    		code = 0 < str.indexOf('/')||0 < str.indexOf('\\') ? Service.readFile(str) :
                Service.readFile(UnitTest.class, str);
    	}
    	
    	ret = new UTProgram(code);
    	programs.put(str, ret);
    	return ret;
    }  

    
    
    private static void comment(ParseNode root, List<LexerToken> src, String input)  {
        System.out.println(input.substring(src.get(root.from+2).begin,src.get(root.to-1).end-1));
    }

    private static void assertion( ParseNode root, List<LexerToken> src, String input ) throws Exception {
    	Map<String, MaterializedPredicate> output = null;
    	Map<String, MaterializedPredicate> cmp = null;
     	boolean sawDash = false;
     	boolean sawGt = false;
    	for( ParseNode child : root.children() ) {
        	if( output == null && child.contains(program_with_input) ) {
        		output = runProgram(child,src,input);
        		//output.printTree();
        		continue;
        	}
            if( !sawDash && child.from+1 == child.to )
                if( "-".equals(child.content(src)) 
                ) { 
                	sawDash = true;
                	continue; 
            }
            if( !sawGt && child.from+1 == child.to )
                if( ">".equals(child.content(src)) 
                ) { 
                	sawGt = true;
                	continue; 
            }
        	if( cmp == null && child.contains(tables) ) {
        		cmp = new HashMap<String, MaterializedPredicate>();
        		tables(cmp,child,src,input);
        		continue;
        	}
            if( child.from+1 == child.to )
                if( ";".equals(child.content(src)) ) 
                	continue; 
        	throw new AssertionError("Unexpected case");
        }
        boolean test = areMatching(output,cmp);
        if( !test ) {
            unitTestIsOK = false;
        }
        if( test )
            System.out.println("TEST#"+(testNo++)+":  ---> "+test); //$NON-NLS-1$ //$NON-NLS-2$
        else
            System.err.println("TEST#"+(testNo++)+":  ---> "+test); //$NON-NLS-1$ //$NON-NLS-2$
        //System.out.println("/* #"+(testNo++)+" */"); //$NON-NLS-1$ //$NON-NLS-2$
        //output.printTree();
    }

	private static boolean areMatching( Map<String, MaterializedPredicate> cmp1, Map<String, MaterializedPredicate> cmp2 ) {
	    boolean ret = true;
		for( String key : cmp2.keySet() ) {
			MaterializedPredicate vd1 = cmp1.get(key);
			MaterializedPredicate vd2 = cmp2.get(key);
			if( vd1==null || vd2==null || !areMatching(vd1,vd2) ) {
			    if( printDiffs ) {
			        System.err.println(vd1.toString());
                    System.err.println(vd2.toString());
			    }
				ret = false; 
			}
		}
		return ret;
	}

	private static boolean areMatching( MaterializedPredicate vd1, MaterializedPredicate vd2 ) {
		if( vd1.arity() != vd2.arity() )
			return false;
		if( vd1.cardinality() != vd2.cardinality() )
			return false;
		/*for( int i = 0; i < vd1.arity(); i++) {
			String attr1 = vd1.getAttribute(i);
			String attr2 = vd2.getAttribute(i);
			if( !attr1.equals(attr2) )
				return false;
		}*/
		for( String attr1 : vd1.attributes ) {
		    if( null == vd2.getAttribute(attr1) )
		        return false;
		}
		
		Tuple[] t1 = new Tuple[vd1.tuples.size()];
		int pos = 0;
		for( Tuple t : vd1.tuples ) 
			t1[pos++] = t;
		Tuple[] t2 = new Tuple[vd2.tuples.size()];
		pos = 0;
		for( Tuple t : vd2.tuples ) 
			t2[pos++] = t;
		for( int i = 0; i < t1.length; i++) {
	        for( String attr1 : vd1.attributes ) {
	            ParseNode node1 = vd1.getAttribute(t1[i], attr1);
                ParseNode node2 = vd2.getAttribute(t2[i], attr1);
                if( node1.from!=node2.from || node1.to!=node2.to )
                    return false;               
	        }
		}
		return true;
	}

	private static void tables( Map<String, MaterializedPredicate> ret, ParseNode root, List<LexerToken> src, String input ) {
		if( root.contains(table) ) {
            table(ret, root, src, input);
            return;
		}
        for( ParseNode child : root.children() ) 
        	tables(ret, child, src, input);         
	}
	
	private static void table( Map<String, MaterializedPredicate> ret, ParseNode root, List<LexerToken> src, String input ) {
		String name = null;
		boolean sawEq = false;
		boolean sawOpenBr = false;
		boolean sawCloseBr = false;
		MaterializedPredicate vd = null;
		for( ParseNode child : root.children() ) {
			if( name == null && child.from+1==child.to ) {
				name = child.content(src);
				continue;
			}
            if( !sawEq && child.from+1 == child.to )
                if( "=".equals(child.content(src)) 
                ) { 
                	sawEq = true;
                	continue; 
                }
            if( !sawOpenBr && child.from+1 == child.to )
                if( "[".equals(child.content(src)) 
                ) { 
                	sawOpenBr = true;
                	continue; 
                }
			if( vd == null && child.contains(header) ) {                   
                vd = new MaterializedPredicate(values(child, src),src, null);
            	continue; 
			}
            if( !sawCloseBr && child.from+1 == child.to )
                if( "]".equals(child.content(src)) 
                ) { 
                	sawCloseBr = true;
                	continue; 
                }
            if( child.contains(content) ) {
                addContent(vd,child, src);
            	continue; 
            }
            throw new AssertionError("unexpected case");
        } 
		ret.put(name, vd);
	}

    private static ArrayList<String> values( ParseNode root, List<LexerToken> src ) {
    	ArrayList<String> ret = new ArrayList<String>();
        if( root.from + 1 == root.to && 
        		(src.get(root.from).type == Token.IDENTIFIER || src.get(root.from).type == Token.DQUOTED_STRING)
         || root.contains(value)
         || root.contains(attribute)
        ) 
            ret.add(root.content(src));
        else
            for( ParseNode child : root.children() )
                ret.addAll(values(child,src));
        return ret;
    }  
    
    private static void addContent( MaterializedPredicate ret, ParseNode root, List<LexerToken> src ) {
        int i = 0;
        ParseNode[] t = new ParseNode[ret.arity()];
        for( String elem : values(root, src) ) {
			int comma = elem.indexOf(',');
			int clPar = elem.indexOf(')');
    	    int from = Integer.parseInt(elem.substring(1,comma));
    		int to = Integer.parseInt(elem.substring(comma+1, clPar));
            t[i%ret.arity()] = new ParseNode(from, to, -1, -1, null);
    		if( i%ret.arity() == ret.arity()-1 ) {
        		ret.addContent(t);
        		t = new ParseNode[ret.arity()];
            }
            i++;
        }
    }



}
