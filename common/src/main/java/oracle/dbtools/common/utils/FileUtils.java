/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.common.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.newscriptrunner.SQLPLUS;
import oracle.dbtools.raptor.newscriptrunner.SQLPlusProviderForSQLPATH;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.util.Logger;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=FileUtils.java"
 *         >Barry McGillin</a>
 *
 */
public class FileUtils {

	public static URLConnection getFile(String filename) {
		return getFile(null, filename);
	}

	public static URLConnection getFile(ScriptRunnerContext ctx, String filename) {

		if (getFile(ctx,filename,true)!=null)
			try {
				return getFile(ctx,filename,true).openConnection();
			} catch (IOException e) {
				return null;
			}
		return null;
	}
	
	public static Path getFilePath(ScriptRunnerContext ctx, String filename) {
		URL u = getFile(ctx, filename, true);
		if (u!=null ) {
			try {
				return Paths.get(u.toURI()).toRealPath();
			} catch (Exception e) {
				Logger.fine(FileUtils.class, e);
			}
		}
		return null;
	}
	
	//
	// This will expand any file/paths that have enviroment variables in the form of : ${ENV}
	//
  private static String expandENVvars(String text) {
    Map<String, String> envMap = System.getenv();
    String pattern = "\\$\\{([A-Za-z0-9]+)\\}";
    Pattern expr = Pattern.compile(pattern);
    Matcher matcher = expr.matcher(text);
    while (matcher.find()) {
        String envValue = envMap.get(matcher.group(1).toUpperCase());
        if (envValue == null) {
            envValue = "";
        } else {
            envValue = envValue.replace("\\", "\\\\");
        }
        Pattern subexpr = Pattern.compile(Pattern.quote(matcher.group(0)));
        text = subexpr.matcher(text).replaceAll(envValue);
    }
    return text;
  }

  
	public static URL getFile(ScriptRunnerContext ctx, String filename, boolean once	) {
		String cdfilename = null;
		String toRun = null;
		if (ctx == null)
			ctx = new ScriptRunnerContext();
		if (filename != null && filename.length() > 0) {
		  
		  // expand any ${ENV} variables
		  filename = expandENVvars(filename);

			try {
				if (!(startsWithHttpOrFtp(filename) && haveIBytes(filename, ctx) || haveIBytesRaw(filename))) {
					cdfilename = ctx.prependCD(filename);
					String pathGiven = null;
					if (cdfilename.equals(filename)) {
						// If not in a cd path, then check sqlpath for something
						// to append.
						if (SQLPLUS.getSqlpathProvider() != null) {
							pathGiven = SQLPLUS.getSqlpathProvider().getSQLPATHsetting();
							if (pathGiven.trim().equals(".")) //$NON-NLS-1$
								pathGiven = System.getProperty("user.dir"); //$NON-NLS-1$
						}
					}

					boolean prependDirectory = false;
					if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
						if (!((filename.length() > 3) && (((filename.charAt(1) == ':')
								&& ((filename.charAt(2) == '/') || (filename.charAt(2) == '\\')))
								|| ((filename.charAt(0) == '/') && (filename.charAt(2) == ':')
										&& (filename.charAt(3) == '/'))))) { // $NON-NLS-1$
							prependDirectory = true;
						}
					} else {
						if (!((filename.startsWith("/")) || (filename.startsWith("\\")))) { //$NON-NLS-1$ //$NON-NLS-2$
							prependDirectory = true;
						}
					}

					if ((pathGiven != null) && (prependDirectory)) { // $NON-NLS-1$
						String[] paths = FileUtils.getSQLPathString(ctx).split(File.pathSeparator);
						for (String possible : paths) {// $NON-NLS-1$
							if (possible == null) {
								continue;
							}

							if ((possible.endsWith("/")) || (possible.endsWith("\\"))) { //$NON-NLS-1$ //$NON-NLS-2$
								possible += filename;
							} else {
								possible += File.separator + filename;
							}
							// does not check http://xxxx.xxx
							if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
								possible = possible.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
							} else {
								possible = possible.replace("\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$
							}
							if ((new File(possible).exists()) && (new File(possible).canRead())) {
								toRun = new File(possible).getAbsolutePath();
								break;
							}
						}
					} else { // Check for a full path given
						if (!new File(filename).isAbsolute() && cdfilename.equals(filename) && prependDirectory) {
							toRun = System.getProperty("user.home") //$NON-NLS-1$
									+ File.separator + filename;
						} else {
							toRun = cdfilename;
						}
					}
					if (toRun == null) {
						// If we cant find a file, reset to default filename
						toRun = cdfilename;
					}
					if (startsWithHttpOrFtp(toRun)) {
						// We can get here if we do not find the file on a URL
						// or its not readable. Need to send back the URL so we
						// can fail cleanly on it.
						return new URL(toRun);
					} else

					if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
						toRun = "/" + toRun.replace("\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$
					}
					return new URL("file://" + toRun); //$NON-NLS-1$
				} else {

					toRun = filename;
					return new URL(toRun);
				}
			} catch (MalformedURLException e) {
				Logger.info(FileUtils.class, e);
			} 
		}
		return null;
	}
	


	public static boolean haveIBytes(String base, ScriptRunnerContext ctx) {
		if (startsWithHttpOrFtp(base) == false) {
			return false;
		}
		InputStream is = null;
		URLConnection c = null;
		Proxy proxy = null;
		try {
			String lower = base.toLowerCase();
			if (containsFile(lower)) { // $NON-NLS-1$
				base = base.substring(lower.indexOf("file:")); //$NON-NLS-1$
			}
			try {
				if (ctx.getProperty(ScriptRunnerContext.HTTP_PROXY) != null) {
					proxy = new Proxy(Proxy.Type.HTTP,
							new InetSocketAddress(ctx.getProperty(ScriptRunnerContext.HTTP_PROXY_HOST).toString(),
									Integer.parseInt(ctx.getProperty(ScriptRunnerContext.HTTP_PROXY_PORT).toString())));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (System.getProperties().get("http.proxyHost") != null //$NON-NLS-1$
					&& System.getProperties().get("http.proxyPort") != null) { //$NON-NLS-1$
				proxy = new Proxy(Proxy.Type.HTTP,
						new InetSocketAddress(System.getProperties().get("http.proxyHost").toString(), //$NON-NLS-1$
								Integer.parseInt(System.getProperty("http.proxyPort")))); //$NON-NLS-1$

			}
			if (proxy != null) {
				c = new URL(base).openConnection(proxy);
			} else {
				c = new URL(base.replaceAll("\\\\", "/")).openConnection(); //$NON-NLS-1$ //$NON-NLS-2$
			}
			is = c.getInputStream();
			if (is.read() != -1) {
				return true;
			}
		} catch (MalformedURLException e1) {
			return false;
		} catch (IOException e1) {
			return false;
		} catch (Exception e2) {// was getting illegal argument exception on
								// !"XYZ$% file input
			return false;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					// ignore.
				}
			}
		}
		return false;
	}

	public static boolean startsWithHttpOrFtp(String base) {
		if (base == null) {
			return false;
		}
		String lower = base.toLowerCase();
		if (!((lower.startsWith("http://") //$NON-NLS-1$
				|| lower.startsWith("https://") //$NON-NLS-1$
				|| lower.startsWith("http:\\\\") //$NON-NLS-1$
				|| lower.startsWith("https:\\\\") //$NON-NLS-1$
				|| containsFile(lower) || lower.startsWith("ftp:\\\\") //$NON-NLS-1$
				|| lower.startsWith("ftps:\\\\") //$NON-NLS-1$
				|| lower.startsWith("ftps://") //$NON-NLS-1$
				|| lower.startsWith("ftp://")))) { //$NON-NLS-1$
			return false;
		}
		return true;
	}

	public static boolean containsFile(String inFile) {
		// can sometimes be .*file:
		boolean retVal = false;
		if (inFile.indexOf(":") > 1) { //$NON-NLS-1$
			String stub = inFile.substring(0, inFile.indexOf(":")); //$NON-NLS-1$
			if (stub.endsWith("file") && (stub.indexOf("/") == -1) && (stub.indexOf("\\") == -1)) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				retVal = true;
			}
		}
		return retVal;
	}

	/**
	 * try \ in case its a microsoft file:\\
	 * 
	 * @param base
	 * @return
	 */
	public static boolean haveIBytesRaw(String base) {
		if (startsWithHttpOrFtp(base) == false) {
			return false;
		}
		InputStream is = null;
		URLConnection c = null;
		try {
			String lower = base.toLowerCase();
			if (containsFile(lower)) { // $NON-NLS-1$
				base = base.substring(lower.indexOf("file:")); //$NON-NLS-1$
			}
			if (base.indexOf("\\") != -1) { //$NON-NLS-1$
				base.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
				c = new URL(base).openConnection(); // $NON-NLS-1$ //$NON-NLS-2$

				is = c.getInputStream();
				if (is.read() != -1) {
					return true;
				}
			}
		} catch (MalformedURLException e1) {
			return false;
		} catch (IOException e1) {
			return false;
		} catch (Exception e2) {// was getting illegal argument exception on
								// !"XYZ$% file input
			return false;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					// ignore.
				}
			}
		}
		return false;
	}


	public static String getSQLPathString(ScriptRunnerContext ctx) {
		String provider = null;
		if (SQLPLUS.getSqlpathProvider() != null) {
			provider = SQLPLUS.getSqlpathProvider().getSQLPATHsetting();
		}
		String pathGiven = ""; //$NON-NLS-1$
		Object[] sources = new Object[] { (String) ctx.getProperty(ScriptRunnerContext.CDPATH),
				getParent((URL) ctx.getProperty(ScriptRunnerContext.BASE_URL)), System.getenv("SQLDEV_STARTUP"),
				getParent((URL) ctx.getProperty(ScriptRunnerContext.TOP_BASE_URL)),
				getParent(useIfNotDirectoryOrNotFile(ctx.getLastNodeForDirNameURL())),
				useIfDirectoryOrNotFile(ctx.getLastNodeForDirNameURL()),
				getParent(useIfNotDirectoryOrNotFile(ctx.getLastDirNameURL())),
				useIfDirectoryOrNotFile(ctx.getLastDirNameURL()),
				ctx.getProperty(ScriptRunnerContext.DBCONFIG_DEFAULT_PATH), provider
				// , "." , iff in sqldev otherwise in sasql it is in provider.
		};
		for (Object source : sources) {
			if (source != null) {
				if (source instanceof String) {
					String val = (String) source;
					if (!(val.equals(""))) { //$NON-NLS-1$
						pathGiven += File.pathSeparator + (String) source;
					}
				} else if (source instanceof URL) {
					String toString = ((URL) source).toString();
					toString = decodeIfFile(toString);
					pathGiven += File.pathSeparator + (toString);
				}
			}
		}
		if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING) == null) {
			pathGiven += File.pathSeparator + "."; //$NON-NLS-1$
		}
		if (pathGiven.length() > 1) {
			pathGiven = pathGiven.substring(1);
		}
		return pathGiven;
	}

	public static String[] getSQLPath(ScriptRunnerContext ctx) {
		return getSQLPathString(ctx).split(File.pathSeparator);
	}

	public static String getCWD(ScriptRunnerContext ctx) {
		String[] paths = FileUtils.getSQLPath(ctx);
		if (paths.length > 0) {
			return paths[0];
		} else {
			return Paths.get(".").toAbsolutePath().normalize().toString();
		}
	}

//	public static Path getFilePath(ScriptRunnerContext ctx, String filename, boolean one) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	/**
	 * Get the first instance of a file on the sqlpath.
	 * @param ctx
	 * @param filename
	 * @return URL Connection to the file.
	 */
	public static Path getFileOnSQLPATH(ScriptRunnerContext ctx, String filename, boolean firstFileFound) {
		if (ctx == null)
			ctx = new ScriptRunnerContext();
		if (filename != null && filename.length() > 0) {
			GetFilenameVisitor gfnv = populateSQLPATHFileList(ctx, filename, firstFileFound);
			if (gfnv.hasFiles()) {
				return gfnv.getFiles().get(0);
			}
		}
		return null;
	}
	

	/**
	 * Get the all instances of a file on the sqlpath.
	 * @param ctx
	 * @param filename
	 * @return URL Connection to the file.
	 */
	public static Path[] getFilesOnSQLPATH(ScriptRunnerContext ctx, String filename) {
		if (ctx == null)
			ctx = new ScriptRunnerContext();
		if (filename != null && filename.length() > 0) {
			GetFilenameVisitor gfnv = populateSQLPATHFileList(ctx, filename, false);
			if (gfnv.hasFiles()) {
				HashSet<Path> e = new HashSet<>();
				e.addAll(gfnv.getFiles());
				ArrayList<Path> alp= new ArrayList<>();
				alp.addAll(e);
				return alp.toArray(new Path[alp.size()]);
			}
		}
		return null;
	}

	private static GetFilenameVisitor populateSQLPATHFileList(ScriptRunnerContext ctx, String filename, boolean firstFileFound) {
		GetFilenameVisitor gfnv = new GetFilenameVisitor(filename, firstFileFound);
		ArrayList<Path> pathlist = new ArrayList<Path>();
		if (findFileOnSQLPATH(ctx, filename, firstFileFound)) {
			String[] paths = getSQLPath(ctx);
			gfnv.getFiles().clear();
			for (String path : paths) {
				// Check if the directory exists and get rid of . on the path
				if (!path.trim().equalsIgnoreCase(".")) {
					try {
						Files.walkFileTree(Paths.get(path), gfnv);
						ArrayList<Path> p = new ArrayList<Path>(gfnv.getFiles());
						pathlist.addAll(p);
						gfnv.getFiles().clear();
					} catch (IOException e) {
						//Cant find it, dont care, roll on
					}
				}
			}
			gfnv.setFiles(pathlist);
		}
		return gfnv;
	}
/**
 * Find files on SQLPATH.  ON SQLPATH we need to check all the directories in the SQLPATH.
 * This implementation does not follow links
 * @param ctx
 * @param filename
 * @return
 */
	public static boolean findFileOnSQLPATH(ScriptRunnerContext ctx, String filename, boolean firstFileFound) {
		String[] paths = getSQLPath(ctx);
		GetFilenameVisitor gfnv = new GetFilenameVisitor(filename, firstFileFound);
		gfnv.getFiles().clear();
		for (String path : paths) {
			// Check if the directory exists
			try {
				if (!path.trim().equalsIgnoreCase(".")) {
					Files.walkFileTree(Paths.get(path), gfnv);
				}
			} catch (IOException e) {
				//Cant find it, dont care, roll on
			}
		}
		if (gfnv.hasFiles()) {
			return true;
		} else {
			return false;
		}
	}

	public static URL getParent(URL inURL) {
		URL retVal = null;
		if (inURL == null) {
			return null;
		}
		try {
			URI uri = inURL.toURI();
			String resolve = "."; //$NON-NLS-1$
			if (uri.getPath().endsWith("/") || uri.getPath().endsWith("\\")) { //$NON-NLS-1$ //$NON-NLS-2$
				resolve = ".."; //$NON-NLS-1$
			}
			URI parent = uri.resolve(resolve);
			retVal = parent.toURL();
		} catch (MalformedURLException e) {
		} catch (URISyntaxException e) {
		}
		return retVal;
	}

	public static String getDirFromFileURL(URL inURL) throws IOException {
		if (inURL == null) {
			return null;
		}
		String pathname = null;
		File file = null;
		try {
			file = new File(inURL.getFile());
			pathname = file.getCanonicalPath();
			if (!(file.isDirectory())) {
				if (!file.exists() || pathname.indexOf(File.separator) == -1) {
					throw new IOException(Messages.getString("ScriptRunnerContext.14") + pathname); //$NON-NLS-1$
				} else {
					pathname = pathname.substring(0, ((pathname.lastIndexOf(File.separator))));
				}
			}
		} catch (Exception e) {
			throw new IOException(e.getMessage());
		}
		return pathname;
	}

	static URL useIfDirectoryOrNotFile(URL inURL) {
		if (inURL == null) {
			return null;
		}
		if (!((((URL) inURL).getProtocol() != null) && ((URL) inURL).getProtocol().toLowerCase().endsWith("file"))) { //$NON-NLS-1$
			return inURL;
		}
		try {
			if (new File(inURL.getFile()).isDirectory()) {
				return inURL;
			} else {
				return null;
			}
		} catch (NullPointerException e) {
			return null;
		}
	}

	public static URL useIfNotDirectoryOrNotFile(URL inURL) {
		if (inURL == null) {
			return null;
		}
		if (!((((URL) inURL).getProtocol() != null) && ((URL) inURL).getProtocol().toLowerCase().endsWith("file"))) { //$NON-NLS-1$
			return inURL;
		}
		try {
			if (new File(inURL.getFile()).isDirectory()) {
				return null;
			} else {
				return inURL;
			}
		} catch (NullPointerException e) {
			return null;
		}
	}

	public static String decodeIfFile(String sourceRef) {
		if ((sourceRef != null) && (!(sourceRef.equals("")))) { //$NON-NLS-1$
			if (FileUtils.containsFile(sourceRef)) {
				try {
					String toString = null;
					try {
						toString = new URL(sourceRef).toURI().getPath();// $NON-NLS-1$
					} catch (URISyntaxException e) {
						oracle.dbtools.util.Logger.fine(FileUtils.class, e);
					} catch (MalformedURLException e) {
						oracle.dbtools.util.Logger.fine(FileUtils.class, e);
					}
					if (toString == null) {// path starts with \ or exception
						toString = URLDecoder.decode(sourceRef, ScriptRunnerContext.ALWAYSUTF8);//$NON-NLS-1$
						if (toString != null) {
							int theIndex = toString.indexOf(":"); //$NON-NLS-1$

							if ((theIndex != -1) && (toString.length() > (theIndex + 1))) {
								toString = toString.substring(theIndex + 1);
							} else {
								toString = null;// error if no:
							}
						}
					}

					if (toString != null) {
						if ((toString.replace('\\', '/').startsWith("//")) && (toString.length() > 2)) { //$NON-NLS-1$
							toString = toString.substring(2);
						}
						if (((System.getProperty("os.name").startsWith("Windows")) //$NON-NLS-1$ //$NON-NLS-2$
								&& ((toString.startsWith("/") || (toString.startsWith("\\"))) && (toString.length() > 3) //$NON-NLS-1$ //$NON-NLS-2$
										&& (toString.substring(2, 3).equals(":"))))) { //$NON-NLS-1$
							// sometimes \c: as well as /c:
							toString = toString.substring(1).replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
						}
						sourceRef = toString;
					}
				} catch (UnsupportedEncodingException e) {
					oracle.dbtools.util.Logger.warn(FileUtils.class, e);
				}
			}
		}
		return sourceRef;
	}

	public static String getUserHome() {
		String storage = System.getProperty("user.home") + File.separator + ".sqlcl"; //$NON-NLS-1$ //$NON-NLS-2$
		if (PlatformUtils.isWindows()) {
			String appData = System.getenv("APPDATA"); //$NON-NLS-1$
			if (appData != null) {
				storage = appData + File.separator + "sqlcl"; //$NON-NLS-1$
			} else {
				storage = null;
			}
		}
		return storage;
	}

	static class GetFilenameVisitor extends SimpleFileVisitor<Path> {
	
		private String _filename = "";
		private ArrayList<Path> files = new ArrayList<Path>();
		private boolean _onlyOne = false;

		public GetFilenameVisitor(String filename, boolean onefile) {
			_filename = filename;
			_onlyOne = onefile;
		}

		public void setFiles(ArrayList<Path> pathlist) {
			files = pathlist;
		}

		public ArrayList<Path> getFiles() {
			return files;
		}

		// Print information about
		// each type of file.
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
			if (attr.isDirectory())
				return FileVisitResult.CONTINUE;
			if (attr.isSymbolicLink()) {
				return FileVisitResult.CONTINUE;
			}
			if (attr.isRegularFile() && file.getFileName().toString().equalsIgnoreCase(_filename)) {
				
				files.add(file);
				if (_onlyOne ) 
					return FileVisitResult.TERMINATE;
			}
			return FileVisitResult.CONTINUE;
		}

		// Print each directory visited.
		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
			return FileVisitResult.CONTINUE;
		}

		// If there is some error accessing
		// the file, let the user know.
		// If you don't override this method
		// and an error occurs, an IOException
		// is thrown.
		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc) {

			return FileVisitResult.CONTINUE;
		}

		public boolean hasFiles() {
			return files != null && files.size() > 0;
		}
		public void reverseFileOrder() {
			Collections.reverse(files);
		}
	}
	/**
	 * Check is a file on the current working directory.
	 * @param file to check
	 * @param ctx context to get the cwd from.
	 * @return Boolean
	 */
	public static boolean isFileOnCWD(File file, ScriptRunnerContext ctx) {
		String cwd = getCWD(ctx);
		String filedir;
		
			filedir = file.getParentFile().getAbsolutePath();
			if (filedir.indexOf('.')!=-1) {
				filedir = filedir.replaceAll("\\.+$", "");
			}
			if (filedir.equals(cwd)) {
				return true;
			} else {
				return false;
			}
	}

	/**
	 * Check is a file on the current working directory.
	 * @param file to check
	 * @param ctx context to get the cwd from.
	 * @return Boolean
	 * @throws Exception 
	 */
	public static boolean isFileOnSQLPATH(File file, ScriptRunnerContext ctx)  {
		String fileDir="";

		try {
		if (file==null) {
			return false;
		}
		if (file.isFile()) {
			fileDir = file.getParentFile().getCanonicalFile().getAbsolutePath();
		} else {
			fileDir = file.getCanonicalFile().getAbsolutePath();
		}
		} catch (Exception e) {
			return false;
		}
		String sqlpath = SQLPLUS.getSqlpathProvider().getSQLPATHsetting();
		if (sqlpath!=null) {
			String[] paths= sqlpath.split(File.pathSeparator);
			for (String path:paths) {
				if (path.equals(".")) {
					continue;
				}
				File fileDirfile = new File(fileDir);
				File pathDir = new File(path);
				if (pathDir.getAbsolutePath().equals(fileDirfile.getAbsolutePath())) {
					return true;
				}
			}
		}
		return false;

	}



public static boolean isCWDonSQLPATH(ScriptRunnerContext ctx) {
	try {
	File afile = new File(getCWD(ctx));
	String cwd = afile.getCanonicalFile().getAbsolutePath();
	
	String[] sqlpath = getSQLPath(ctx);
	for (String s : sqlpath) {
		if (s.equals(".")) {
			continue;
		}
		File a = new File(s);
		String s2 = a.getCanonicalFile().getAbsolutePath();
		if (s2.equals(cwd)) {
			return true;
		} else {
			return false;
		}
	}
	return false;
	} catch (Exception e) {
		return false;
	}
}
public static boolean isSQLPATHSet() {
	if (SQLPLUS.getSqlpathProvider()!=null) {
		if (SQLPLUS.getSqlpathProvider().getSQLPATHsetting()!=null) {
			return true;
		} else {
			return false;
		}
	} else {
		if (System.getenv("SQLPATH")!=null) {
			return true;
		} else {
			return false;
		}
	}
}
	public static void main(String[] args) {
		SQLPLUS.setSqlpathProvider(new SQLPlusProviderForSQLPATH());
		System.out.println(isSQLPATHSet());
		System.out.println("CWD:" + getCWD(new ScriptRunnerContext()));
		System.out.println("CWD:" + new File(getCWD(new ScriptRunnerContext())).getAbsolutePath());
		System.out.println("SQLPATH:"+getSQLPathString(new ScriptRunnerContext()));
		System.out.println(isCWDonSQLPATH(new ScriptRunnerContext()));
		File file= new File("/private/tmp/login.sql");
		System.out.println("isFileOnCWD:"+isFileOnCWD(file, new ScriptRunnerContext()));
		System.out.println("isFileOnSQLPATH:"+isFileOnSQLPATH(file, new ScriptRunnerContext()));
	}

}
