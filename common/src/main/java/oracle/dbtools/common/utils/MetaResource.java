/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.common.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Provides delayed loading for resources.
 * 
 * This is a utility class for storing resource and classloader pairs.
 * It can be used to store paths to resources without looking them up as in the 
 * case of images and properties file. It can also be used in cases where a 
 * given resource, e.g. an xml file, contains class references that should all 
 * resolve with the given classloader.
 * <p>
 * Copied from javax.ide.util.MetaResource
 * 
 * @author svassile
 * @author jmcginni
 * 
 */
public final class MetaResource {
    private final String m_resourcePath;
    private final ClassLoader m_classLoader;
    private URL m_resourceURL;
    
    /**
     * Creates an MetaResource instance. Does not try to access the resource.
     * 
     * @param classLoader the classloader that should load the resource
     * @param path the relative path to the resource e.g. "images/foo.png"
     * @throws NullPointerException when either the classloader or the path are 
     * <code>null</code>.
     */
    public MetaResource(ClassLoader classLoader, String path) {
        if (classLoader == null)
        {
            throw new NullPointerException( "null classLoader" );     //$NON-NLS-1$
        }
        if (path == null)
        {
            throw new NullPointerException( "null resource path" );         //$NON-NLS-1$
        }
        m_classLoader = classLoader;
        m_resourcePath = path;
    }
    
    /**
     * Looks up a resource, starting at the classloader passed in. This method 
     * does not initialize the extension.
     * 
     * @return the url of the resource if it exists or <code>null</code> if it 
     * does not
     */
    public URL toURL() {
        if (m_resourceURL == null)
        {
            m_resourceURL = m_classLoader.getResource(m_resourcePath);
        }
        return m_resourceURL;
    }
    
    /**
     * Looks up a resource, and opens a resource stream on it starting at the 
     * classloader passed in. This method does not initialize the extension.
     * 
     * @return opens and returns an {@code InputStream} of the resource if it 
     * exists or <code>null</code> if it does not
     * 
     * @throws IOException if the openStream failed for whatever reason
     */
    public InputStream getInputStream() throws IOException {
        final URL url = toURL();
        if (url != null)
        {
            return url.openStream();
        }
        return null;
        
    }

    /**
     * Gets the resource path.
     * 
     * @return the resource path
     */
    public String getResourcePath() {
        return m_resourcePath;
    }

    /**
     * Gets the classloader.
     * 
     * @return the classloader
     */
    public ClassLoader getClassLoader() {
        return m_classLoader;
    }
    
    public void setUrl(URL url) {
    	m_resourceURL=url;
    }
}
