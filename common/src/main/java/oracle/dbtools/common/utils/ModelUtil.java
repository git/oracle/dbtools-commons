/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.common.utils;

/**
 * Common utility methods.
 * <p>
 * @author jmcginni
 */
public final class ModelUtil {
	/**
	 * This is a utility method that compares two objects when one or both of
	 * the objects might be <CODE>null</CODE> The result of this method is
	 * determined as follows:
	 * <OL>
	 * <LI>If <CODE>o1</CODE> and <CODE>o2</CODE> are the same object according
	 * to the <CODE>==</CODE> operator, return <CODE>true</CODE>.
	 * <LI>Otherwise, if either <CODE>o1</CODE> or <CODE>o2</CODE> is
	 * <CODE>null</CODE>, return <CODE>false</CODE>.
	 * <LI>Otherwise, return <CODE>o1.equals(o2)</CODE>.
	 * </OL>
	 * 
	 * This method produces the exact logically inverted result as the
	 * {@link #areDifferent(Object, Object)} method.
	 * <P>
	 * 
	 * For array types, one of the <CODE>equals</CODE> methods in
	 * {@link java.util.Arrays} should be used instead of this method. Note that
	 * arrays with more than one dimension will require some custom code in
	 * order to implement <CODE>equals</CODE> properly.
	 */
	public static final boolean areEqual(Object o1, Object o2) {
		if (o1 == o2) {
			return true;
		} else if (o1 == null || o2 == null) {
			return false;
		} else {
			return o1.equals(o2);
		}
	}

	/**
	 * This is a utility method that compares two objects when one or both of
	 * the objects might be <CODE>null</CODE>. The result returned by this
	 * method is determined as follows:
	 * <OL>
	 * <LI>If <CODE>o1</CODE> and <CODE>o2</CODE> are the same object according
	 * to the <CODE>==</CODE> operator, return <CODE>false</CODE>.
	 * <LI>Otherwise, if either <CODE>o1</CODE> or <CODE>o2</CODE> is
	 * <CODE>null</CODE>, return <CODE>true</CODE>.
	 * <LI>Otherwise, return <CODE>!o1.equals(o2)</CODE>.
	 * </OL>
	 * 
	 * This method produces the exact logically inverted result as the
	 * {@link #areEqual(Object, Object)} method.
	 * <P>
	 * 
	 * For array types, one of the <CODE>equals</CODE> methods in
	 * {@link java.util.Arrays} should be used instead of this method. Note that
	 * arrays with more than one dimension will require some custom code in
	 * order to implement <CODE>equals</CODE> properly.
	 */
	public static final boolean areDifferent(Object o1, Object o2) {
		return !areEqual(o1, o2);
	}

	/**
	 * Returns <CODE>true</CODE> if the specified array is not null and contains
	 * a non-null element. Returns <CODE>false</CODE> if the array is null or if
	 * all the array elements are null.
	 */
	public static final boolean hasNonNullElement(Object[] array) {
		if (array != null) {
			final int n = array.length;
			for (int i = 0; i < n; i++) {
				if (array[i] != null) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns a single string that is the concatenation of all the strings in
	 * the specified string array. A single space is put between each string
	 * array element. Null array elements are skipped. If the array itself is
	 * null, the empty string is returned. This method is guaranteed to return a
	 * non-null value, if no exceptions are thrown.
	 */
	public static final String concat(String[] strs) {
		return concat(strs, " "); // NOTRANS
	}

	/**
	 * Returns a single string that is the concatenation of all the strings in
	 * the specified string array. The strings are separated by the specified
	 * delimiter. Null array elements are skipped. If the array itself is null,
	 * the empty string is returned. This method is guaranteed to return a
	 * non-null value, if no exceptions are thrown.
	 */
	public static final String concat(String[] strs, String delim) {
		if (strs != null) {
			final StringBuilder bld = new StringBuilder();
			final int n = strs.length;
			for (int i = 0; i < n; i++) {
				final String str = strs[i];
				if (str != null) {
					bld.append(str).append(delim);
				}
			}
			final int length = bld.length();
			if (length > 0) {
				// Trim trailing space.
				bld.setLength(length - 1);
			}
			return bld.toString();
		} else {
			return ""; // NOTRANS
		}
	}

	/**
	 * @see #hasLength(CharSequence)
	 */
	public static final boolean hasLength(String s) {
		return hasLength((CharSequence) s);
	}

	/**
	 * Returns the hash code for the specified object, or 0 if it is null.
	 * 
	 * @param o
	 *            an object, which may be null.
	 * @return the object's hashCode() or 0.
	 */
	public static int hashCodeFor(Object o) {
		return o != null ? o.hashCode() : 0;
	}

	/**
	 * Returns <CODE>true</CODE> if the specified {@link String} is not
	 * <CODE>null</CODE> and has a length greater than zero. This is a very
	 * frequently occurring check.
	 */
	public static final boolean hasLength(CharSequence cs) {
		return (cs != null && cs.length() > 0);
	}

	/**
	 * Returns <CODE>null</CODE> if the specified string is empty or
	 * <CODE>null</CODE>. Otherwise the string itself is returned.
	 */
	public static final String nullifyIfEmpty(String s) {
		return ModelUtil.hasLength(s) ? s : null;
	}

	/**
	 * Casts the object to the given class, if it is an instance, or returns
	 * null otherwise.
	 */
	public static <T> T as(Object o, Class<T> clazz) {
		if (clazz.isInstance(o))
			return clazz.cast(o);
		else
			return null;
	}

	private ModelUtil() {
		// NOP
	}
}
