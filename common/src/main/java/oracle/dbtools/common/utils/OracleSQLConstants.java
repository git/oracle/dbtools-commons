/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.common.utils;

import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class OracleSQLConstants.
 */
public class OracleSQLConstants {
	/*
	 * List of SQL and PL/SQL reserved words (PL/SQL keyword not included).
	 */
	//http://yourmachine.yourdomain/12/121/appdev.121/e17622/reservewords.htm#LNPLS019
	/** The Constant ORACLE_RESERVED_WORDS. */
	private static final ArrayList<String> ORACLE_RESERVED_WORDS;
	
	static {
		ORACLE_RESERVED_WORDS = new ArrayList<String>();
		ORACLE_RESERVED_WORDS.add("ACCESS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ADD"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ALL"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ALTER"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("AND"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ANY"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ARRAY"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ARROW"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("AS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ASC"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("AT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("AUDIT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("BEGIN"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("BETWEEN"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("BY"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("CASE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("CHAR"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("CHECK"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("CLUSTERS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("CLUSTER"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("COLAUTH"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("COLUMN"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("COLUMNS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("COMMENT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("COMPRESS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("CONNECT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("CRASH"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("CREATE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("CURRENT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("DATE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("DECIMAL"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("DECLARE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("DEFAULT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("DELETE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("DESC"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("DISTINCT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("DROP"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ELSE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("END"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("EXCEPT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("EXCEPTION"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("EXCLUSIVE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("EXISTS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("FETCH"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("FILE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("FLOAT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("FOR"); //$NON-NLS-1$
		//ORACLE_RESERVED_WORDS.add("FORM"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("FROM"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("GOTO"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("GRANT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("GROUP"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("HAVING"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("IDENTIFIED"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("IF"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("IMMEDIATE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("IN"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("INCREMENT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("INDEXES"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("INDEX"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("INITIAL"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("INSERT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("INTEGER"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("INTERSECT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("INTO"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("IS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("LEVEL"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("LIKE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("LOCK"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("LONG"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("MAXEXTENTS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("MINUS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("MLSLABEL"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("MODE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("MODIFY"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("NOCOMPRESS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("NOT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("NOWAIT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("NULL"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("NUMBER"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("OF"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("OFFLINE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ON"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ONLINE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("OPTION"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("OR"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ORDER"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("OVERLAPS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("PCTFREE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("PRIOR"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("PRIVILEGES"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("PROCEDURE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("PUBLIC"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("RAW"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("RENAME"); //$NON-NLS-1$
		//ORACLE_RESERVED_WORDS.add("RANGE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("RECORD"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("RESOURCE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("REVOKE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ROW"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ROWID"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ROWNUM"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("ROWS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("SELECT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("SESSION"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("SET"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("SHARE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("SIZE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("SMALLINT"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("SQL"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("START"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("SUCCESSFUL"); //$NON-NLS-1$
		//ORACLE_RESERVED_WORDS.add("SUBTYPE"); //$NON-NLS-1$ //bug 9500204. Hmm we need to have 2 lists , 1 plsql and 1 sql
		ORACLE_RESERVED_WORDS.add("SYNONYM"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("SYSDATE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("TABAUTH"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("TABLE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("THEN"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("TO"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("TRIGGER"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("UID"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("UNION"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("UNIQUE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("UPDATE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("USE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("USER"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("VALIDATE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("VALUES"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("VARCHAR"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("VARCHAR2"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("VIEW"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("VIEWS"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("WHEN"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("WHERE"); //$NON-NLS-1$
		ORACLE_RESERVED_WORDS.add("WITH"); //$NON-NLS-1$
	};
	
	/** The Constant ORACLE_SIGNIFICANT_WORDS. */
	private static final ArrayList<String> ORACLE_SIGNIFICANT_WORDS;
	
	static  {
		ORACLE_SIGNIFICANT_WORDS = new ArrayList<String>();
		ORACLE_SIGNIFICANT_WORDS.add("CONNECT_BY_ISCYCLE"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("CONNECT_BY_ISLEAF"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("LEVEL"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("VERSIONS_STARTTIME"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("VERSIONS_STARTSCN"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("VERSIONS_ENDTIME"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("VERSIONS_ENDSCN"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("VERSIONS_XID"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("VERSIONS_OPERATION"); //$NON-NLS-1$
		//ORACLE_SIGNIFICANT_WORDS.add("COLUMN_VALUE"); //$NON-NLS-1$ Bug 16963966
		//ORACLE_SIGNIFICANT_WORDS.add("OBJECT_ID"); //$NON-NLS-1$ Bug 21114366
		ORACLE_SIGNIFICANT_WORDS.add("OBJECT_VALUE"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("ORA_ROWSCN"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("ROWID"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("ROWNUM"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("XMLDATA"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("CURRENT_DATE"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("DBTIMEZONE"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("SESSIONTIMEZONE"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("SYSDATE"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("SYSTIMESTAMP"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("UID"); //$NON-NLS-1$
		ORACLE_SIGNIFICANT_WORDS.add("USER"); //$NON-NLS-1$
	}
	
	/**
	 * Checks if is rserved word.
	 *
	 * @param value the value
	 * @return true, if is rserved word
	 */
	public static boolean isRservedWord(final String value) {
		return ORACLE_RESERVED_WORDS.contains(value.toUpperCase());
	}
	
	/**
	 * Checks if is significant word.
	 *
	 * @param value the value
	 * @return true, if is significant word
	 */
	public static boolean isSignificantWord(String value) {
		return ORACLE_SIGNIFICANT_WORDS.contains(value.toUpperCase());
	}
}
