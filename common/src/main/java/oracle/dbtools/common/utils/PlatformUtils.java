/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.common.utils;

import java.io.File;
import java.io.IOException;

/**
 * <code>PlatformUtils</code> provides a common location from which a component
 * can identify the current platform, without the need for each component to
 * perform its own test of the os.name or os.version properties.
 * 
 * @author <a href="mailto:mike.maughmer@oracle.com">Mike Maughmer</a>
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=PlatformUtils.java"
 *         >Barry McGillin</a>
 * 
 */
public final class PlatformUtils {
	private static String _osName;
	private static String _osVersion;

	public static final int OS_UNKNOWN = 0;
	public static final int OS_LINUX = 1 + OS_UNKNOWN;
	public static final int OS_MAC = 1 + OS_LINUX;
	public static final int OS_MAC_PANTHER = 1 + OS_MAC;
	public static final int OS_MAC_TIGER = 1 + OS_MAC_PANTHER;
	public static final int OS_MAC_LEOPARD = 1 + OS_MAC_TIGER;

	public static final int OS_MAC_SNOW_LEOPARD = 1 + OS_MAC_LEOPARD;
	public static final int OS_MAC_LION = 1 + OS_MAC_SNOW_LEOPARD;
	public static final int OS_MAC_MOUNTAIN_LION = 1 + OS_MAC_LION;
	public static final int OS_MAC_MAVERICKS = 1 + OS_MAC_MOUNTAIN_LION;
	public static final int OS_WINDOWS = 1 + OS_MAC_MAVERICKS;

	public static final int OS_WINDOWS_NT = 1 + OS_WINDOWS;
	public static final int OS_WINDOWS_2K = 1 + OS_WINDOWS_NT;
	public static final int OS_WINDOWS_XP = 1 + OS_WINDOWS_2K;
	private static final int OS_WINDOWS_VISTA = 1 + OS_WINDOWS_XP;
	private static final int OS_WINDOWS_7 = 1 + OS_WINDOWS_VISTA;
	private static final int OS_WINDOWS_8 = 1 + OS_WINDOWS_7;
	private static int _platform = OS_UNKNOWN;

	private static boolean caseSensitiveFileSystem;

	static {
		_osName = System.getProperty("os.name");
		if (_osName == null)
			_osName = "";

		_osVersion = System.getProperty("os.version");
		if (_osVersion == null)
			_osVersion = "";

		final String lower = _osName.toLowerCase();

		if (lower.startsWith("linux")) {
			_platform = OS_LINUX;
		} else if (lower.startsWith("mac os x")) {
			if (_osVersion.startsWith("10.9"))
				_platform = OS_MAC_MAVERICKS;
			else if (_osVersion.startsWith("10.8"))
				_platform = OS_MAC_MOUNTAIN_LION;
			else if (_osVersion.startsWith("10.7"))
				_platform = OS_MAC_LION;
			else if (_osVersion.startsWith("10.6"))
				_platform = OS_MAC_SNOW_LEOPARD;
			else if (_osVersion.startsWith("10.5"))
				_platform = OS_MAC_LEOPARD;
			else if (_osVersion.startsWith("10.4"))
				_platform = OS_MAC_TIGER;
			else if (_osVersion.startsWith("10.3"))
				_platform = OS_MAC_PANTHER;
			else
				_platform = OS_MAC;
		} else if (lower.startsWith("windows")) {
			if (lower.startsWith("windows xp"))
				_platform = OS_WINDOWS_XP;
			else if (lower.startsWith("windows 2000"))
				_platform = OS_WINDOWS_2K;
			else if (lower.startsWith("windows nt"))
				_platform = OS_WINDOWS_NT;
			else if (lower.startsWith("windows vista"))
				_platform = OS_WINDOWS_VISTA;
			else if (lower.startsWith("windows 7"))
				_platform = OS_WINDOWS_7;
			else if (lower.startsWith("windows 8"))
				_platform = OS_WINDOWS_8;
			else
				_platform = OS_WINDOWS;
		}

		// Need to actually create a file to determine case sensitivity of the
		// file system, just comparing new File("A) with new File('a') is not
		// enough
		try {
			File tempFile = File.createTempFile("ABC", null);
			tempFile.deleteOnExit();
			caseSensitiveFileSystem = !new File(tempFile.getPath().toLowerCase()).exists();
		} catch (IOException ioe) {
			// Fall back on the next best thing we can do
			caseSensitiveFileSystem = !(new File("A").equals(new File("a")));
		}
	}

	/**
	 * Returns the same as <tt>System.getProperty( "os.name" )</tt>.
	 * 
	 * @return the value of the <tt>os.name</tt> system property.
	 */
	public static String osName() {
		return _osName;
	}

	/**
	 * Returns the same as <tt>System.getProperty( "os.version" )</tt>.
	 * 
	 * @return the value of the <tt>os.version</tt> system property.
	 */
	public static String osVersion() {
		return _osVersion;
	}

	/**
	 * Gets a PlatformUtils identifier representing the current platform.
	 * 
	 * @return int value representing the current platform.
	 */
	public static int getPlatform() {
		return _platform;
	}

	/**
	 * Determine if the local file system of the current platform is case
	 * sensitive
	 * 
	 * @return true if the local file system is case sensitive
	 */
	public static boolean isCaseSensitiveFileSystem() {
		return caseSensitiveFileSystem;
	}

	/**
	 * Determines whether the current platform is a known platform.
	 * 
	 * @return <tt>true</tt> if the platform is known, <tt>false</tt> otherwise.
	 */
	public static boolean isKnown() {
		return _platform != OS_UNKNOWN;
	}

	// Linux...

	/**
	 * Determines whether the current platform is Linux.
	 * 
	 * @return true if Linux, false otherwise.
	 */
	public static boolean isLinux() {
		return _platform == OS_LINUX;
	}

	// Mac OS X...

	/**
	 * Determines whether the current platform is Mac OS X.
	 * <p>
	 * 
	 * This method will return true regardless of which version of Mac OS X
	 * you're using. To determine whether the current platform is a specific
	 * version of Mac OS X (Panther, Tiger, etc.), please use one of the
	 * specific isMac calls below.
	 * 
	 * @return true if Mac OS X, false otherwise.
	 */
	public static boolean isMac() {
		return _platform >= OS_MAC && _platform < OS_WINDOWS;
	}

	/**
	 * Determines whether the current platform is Mac OS X Panther.
	 * 
	 * @return true if Mac OS X Panther, false otherwise.
	 */
	public static boolean isMacPanther() {
		return _platform == OS_MAC_PANTHER;
	}

	/**
	 * Determines whether the current platform is Mac OS X Tiger.
	 * 
	 * @return true if Mac OS X Tiger, false otherwise.
	 */
	public static boolean isMacTiger() {
		return _platform == OS_MAC_TIGER;
	}

	/**
	 * Determines whether the current platform is Mac OS X Leopard.
	 * 
	 * @return true if Mac OS X Leopard, false otherwise.
	 */
	public static boolean isMacLeopard() {
		return _platform == OS_MAC_LEOPARD;
	}

	/**
	 * Determines whether the current platform is Mac OS X Snow Leopard.
	 * 
	 * @return true if Mac OS X Snow Leopard, false otherwise.
	 */
	public static boolean isMacSnowLeopard() {
		return _platform == OS_MAC_SNOW_LEOPARD;
	}

	/**
	 * Determines whether the current platform is Mac OS X Lion.
	 * 
	 * @return true if Mac OS X Lion, false otherwise.
	 */
	public static boolean isMacLion() {
		return _platform == OS_MAC_LION;
	}

	/**
	 * Determines whether the current platform is Mac OS X MountainLion.
	 * 
	 * @return true if Mac OS X MountainLion, false otherwise.
	 */
	public static boolean isMacMountainLion() {
		return _platform == OS_MAC_MOUNTAIN_LION;
	}

	/**
	 * Determines whether the current platform is Mac OS X Mavericks.
	 * 
	 * @return true if Mac OS X Mavericks, false otherwise.
	 */
	public static boolean isMacMavericks() {
		return _platform == OS_MAC_MAVERICKS;
	}

	// Windows...

	/**
	 * Determines whether the current platform is Windows.
	 * <p>
	 * 
	 * Note that this method will return true regardless of which version of
	 * Windows you're using. To determine whether the current platform is a
	 * specific version of Windows (XP, 2K, NT, etc.), please use one of the
	 * specific isWindows calls below.
	 * 
	 * @return true if Windows, false otherwise.
	 */
	public static boolean isWindows() {
		return _platform >= OS_WINDOWS;
	}

	/**
	 * Determines whether the current platform is Windows NT.
	 * 
	 * @return true if Windows NT, false otherwise.
	 */
	public static boolean isWindowsNT() {
		return _platform == OS_WINDOWS_NT;
	}

	/**
	 * Determines whether the current platform is Windows 2K.
	 * 
	 * @return true if Windows 2K, false otherwise.
	 */
	public static boolean isWindows2K() {
		return _platform == OS_WINDOWS_2K;
	}

	/**
	 * Determines whether the current platform is Windows XP.
	 * 
	 * @return true if Windows XP, false otherwise.
	 */
	public static boolean isWindowsXP() {
		return _platform == OS_WINDOWS_XP;
	}

	/**
	 * Determines whether the current platform is Windows Vista.
	 * 
	 * @return true if Windows Vista, false otherwise.
	 */
	public static boolean isWindowsVista() {
		return _platform == OS_WINDOWS_VISTA;
	}

	/**
	 * Determines whether the current platform is Windows 7.
	 * 
	 * @return {@code true} if Windows 7, {@code false} otherwise.
	 */
	public static boolean isWindows7() {
		return _platform == OS_WINDOWS_7;
	}

	/**
	 * Determines whether the current platform is Windows 8.
	 * 
	 * @return {@code true} if Windows 8, {@code false} otherwise.
	 */
	public static boolean isWindows8() {
		return _platform == OS_WINDOWS_8;
	}

	// --------------------------------------------------------------------------
	// Version-specific methods...
	// --------------------------------------------------------------------------

	// Mac OS X...

	/**
	 * Determines whether the current platform is at least Mac OS X Panther.
	 * 
	 * @return true if at least Mac OS X Panther, false otherwise.
	 */
	public static boolean isAtLeastMacPanther() {
		return _platform >= OS_MAC_PANTHER && _platform < OS_WINDOWS;
	}

	/**
	 * Determines whether the current platform is at least Mac OS X Tiger.
	 * 
	 * @return true if at least Mac OS X Tiger, false otherwise.
	 */
	public static boolean isAtLeastMacTiger() {
		return _platform >= OS_MAC_TIGER && _platform < OS_WINDOWS;
	}

	// Windows...

	/**
	 * Called to determine whether the current platform is at least Windows NT.
	 * 
	 * @return true if at least Windows NT, false otherwise.
	 */
	public static boolean isAtLeastWindowsNT() {
		return _platform >= OS_WINDOWS_NT;
	}

	/**
	 * Determines whether the current platform is at least Windows 2K.
	 * 
	 * @return true if at least Windows 2K, false otherwise.
	 */
	public static boolean isAtLeastWindows2K() {
		return _platform >= OS_WINDOWS_2K;
	}

	/**
	 * Determines whether the current platform is at least Windows XP.
	 * 
	 * @return true if at least Windows XP, false otherwise.
	 */
	public static boolean isAtLeastWindowsXP() {
		return _platform >= OS_WINDOWS_XP;
	}

	/**
	 * Determines whether the current platform is at least Windows Vista.
	 * 
	 * @return true if at least Windows Vista, false otherwise.
	 */
	public static boolean isAtLeastWindowsVista() {
		return _platform >= OS_WINDOWS_VISTA;
	}

	/**
	 * Determines whether the current platform is at least Windows 7.
	 * 
	 * @return {@code true} if at least Windows 7, {@code false} otherwise.
	 */
	public static boolean isAtLeastWindows7() {
		return _platform >= OS_WINDOWS_7;
	}

	/**
	 * Determines whether the current platform is at least Windows 8.
	 * 
	 * @return {@code true} if at least Windows 8, {@code false} otherwise.
	 */
	public static boolean isAtLeastWindows8() {
		return _platform >= OS_WINDOWS_8;
	}

}
