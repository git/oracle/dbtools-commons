/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.common.utils;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=StringUtils.java">Barry McGillin</a>
 *
 */
public final class StringUtils {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s = "           This is a #weird String         ";
		System.out.println(s);
		System.out.println("ltrim >" + StringUtils.ltrim(s) + "<");
		System.out.println("rtrim >" + StringUtils.rtrim(s) + "<");
		System.out.println("nonalpha >" + StringUtils.getFirstNonAlpha(s) + "<");
		System.out.println(initCapSingle("tYPE bODY"));
		System.out.println(initCap("tYPE bODY"));
		System.out.println(initCapSingle("tYPE bODY"));
	}

	public static String ltrim(String s) {
		int i = 0;
		while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
			i++;
		}
		return s.substring(i);
	}

	public static String rtrim(String s) {
		int i = s.length() - 1;
		while (i >= 0 && Character.isWhitespace(s.charAt(i))) {
			i--;
		}
		return s.substring(0, i + 1);
	}

	/**
	 * Get the first Non alpha character in a String.
	 * 
	 * @param str
	 *            String to check
	 * @return the character or null
	 */
	public static String getFirstNonAlpha(String str) {
		for (int i = 0, n = str.length(); i < n; i++) {
			char c = str.charAt(i);
			if (!(Character.isAlphabetic(c) || Character.isDigit(c) || Character.isSpaceChar(c))) {
				return Character.toString(c);
			}
		}
		return null;
	}
	
	public static String initCap(String line) {
		String[] arr = line.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String word: arr) {
        	word = word.toUpperCase().replace(word.substring(1), word.substring(1).toLowerCase());
        	sb.append(word + " ");
        }
        return sb.toString().trim();
	}


	public static Object initCapSingle(String objectType) {
		
		if (objectType.length()>1) {
			return objectType.toUpperCase().substring(0,1)+objectType.toLowerCase().substring(1);
		}
		else { 
			return objectType.toUpperCase();
		}
	}
	
}
