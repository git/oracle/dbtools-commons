/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.common.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Represents the version number of an extension or component. A version number
 * is an immutable ordered sequence of integer values.
 * <p>
 * Version numbers can be represented in two forms. When you construct a new
 * Version object using a String representation of the version number, this
 * String representation is stored and returned by the {@link #toString()}
 * method. This form of the version number will retain any redundant leading
 * zeros in any of the integer values which make up the version number.
 * <p>
 * Conversely, in the <i>canonical</i> form, a version number does not retain
 * leading zeros. The canonical form of <tt>new Version( "1.02.03" )</tt> would
 * be <tt>"1.2.3"</tt>. You can retrieve the canonical form of a Version object
 * using the {@link #toCanonicalString()} method.
 * <p>
 * @author jmcginni
 */
public final class Version implements Comparable<Version> {
	private static final Map<String, int[]> _convertCache = new HashMap<String, int[]>(
			100);
	private final int[] _numbers;
	private final String _versionLabel;

	/**
	 * Constructs a Version object from a String representation.
	 * 
	 * @param versionLabel
	 *            a String representation of a version number. This must start
	 *            with an integer and contain only integers and periods.
	 * @throws NumberFormatException
	 *             if the specified version label contains non-numeric
	 *             characters other than periods.
	 */
	public Version(String versionLabel) throws NumberFormatException {
		_versionLabel = versionLabel;
		_numbers = convert(versionLabel);
	}

	/**
	 * Constructs a Version object from a number representation.
	 * 
	 * @param numbers
	 *            a number representation of a version number.
	 */
	public Version(int[] numbers) {
		_numbers = numbers;
		StringBuffer versionLabelBuffer = new StringBuffer();
		for (int i = 0; i < numbers.length; i++) {
			if (i > 0)
				versionLabelBuffer.append('.');

			versionLabelBuffer.append(String.valueOf(numbers[i]));
		}
		_versionLabel = versionLabelBuffer.toString();
	}

	/**
	 * Converts this Version to an int array.
	 * 
	 * @return a newly allocated int array whose length is equal to the number
	 *         of integer values in this version object and whose contents are
	 *         the integer values represented by this version.
	 */
	public int[] toIntArray() {
		int[] numbers = new int[_numbers.length];
		System.arraycopy(_numbers, 0, numbers, 0, _numbers.length);

		return numbers;
	}

	// --------------------------------------------------------------------------
	// Comparable interface.
	// --------------------------------------------------------------------------

	/**
	 * Compare this version object with another version object. Comparison of
	 * two version numbers applies to the canonical form of the version number.
	 * For example, the result of evaluating the expression
	 * <tt>new Version( "1.05.07" ).compareTo( new Version( "1.5.0006" ) )</tt>
	 * is a positive integer indicating that version 1.5.7 is greater than
	 * 1.5.6.
	 * 
	 * @param other
	 *            another instance of Version.
	 * @return true if this version is equal to the other version.
	 */
	public int compareTo(Version other) {
		if (this == other) {
			return 0;
		}

		int[] otherNumbers = ((Version) other)._numbers;
		int len1 = _numbers.length;
		int len2 = otherNumbers.length;
		int max = Math.max(len1, len2);

		for (int i = 0; i < max; i++) {
			int d1 = (i < len1 ? _numbers[i] : 0);
			int d2 = (i < len2 ? otherNumbers[i] : 0);

			if (d1 != d2) {
				return d1 - d2;
			}
		}
		return 0;
	}

	// --------------------------------------------------------------------------
	// Object overrides.
	// --------------------------------------------------------------------------

	/**
	 * Get this version number as a string. The string returned is equal to the
	 * string provided in the constructor. For example,
	 * <tt>new Version( "1.05.06" ).toString();</tt> would return
	 * <tt>"1.05.06"</tt>.
	 * 
	 * @return the string representation of this version.
	 * @see #toCanonicalString()
	 */
	public String toString() {
		return _versionLabel;
	}

	/**
	 * Get this version number as a canonicalized string. Leading zeros will be
	 * removed from all numerical components of the version. For example,
	 * <tt>new Version( "1.05.06" ).toString();</tt> would return
	 * <tt>"1.5.6"</tt>.
	 * 
	 * @return a canonical string representation of this version.
	 * @see #toString()
	 */
	public String toCanonicalString() {
		final int len = _numbers.length;
		final StringBuffer rtn = new StringBuffer(len * 3).append(_numbers[0]);

		for (int i = 1; i < len; i++) {
			rtn.append('.'); // NORES
			rtn.append(_numbers[i]);
		}

		return rtn.toString();
	}

	/**
	 * Compare this version object with another version object for equality.
	 * Equality of two version numbers applies to the canonical form of the
	 * version number. For example, the result of evaluating the expression
	 * <tt>new Version( "1.05.06" ).equals( new Version( "1.5.0006" ) )</tt> is
	 * <tt>true</tt>.
	 * 
	 * @param other
	 *            another instance of Version.
	 * @return true if this version is equal to the other version.
	 */
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if (!(other instanceof Version)) {
			return false;
		}

		final Version version = (Version) other;
		final int len = _numbers.length;

		if (len != version._numbers.length) {
			return false;
		}

		for (int i = 0; i < len; i++) {
			if (_numbers[i] != version._numbers[i]) {
				return false;
			}
		}

		return true;
	}

	public int hashCode() {
		int hash = 925295;
		final int len = _numbers.length;

		for (int i = 0; i < len; i++) {
			hash = 37 * hash + i;
		}

		return hash;
	}

	public int[] convert(String versionLabel) throws NumberFormatException {
		int[] numbers = (int[]) _convertCache.get(versionLabel);

		if (numbers != null) {
			return numbers;
		}

		final StringTokenizer tokenizer = new StringTokenizer(versionLabel,
				".", true); // NORES
		final int count = tokenizer.countTokens() + 1;

		if (count % 2 != 0) {
			throw new NumberFormatException(
					" Malformed version specification: `" // NORES
							+ versionLabel + "`."); // NORES
		}

		numbers = new int[count / 2];
		boolean expectingNumber = true;
		int i = 0;

		while (tokenizer.hasMoreTokens()) {
			final String token = tokenizer.nextToken();

			if (expectingNumber) {
				expectingNumber = false;
				int piece = Integer.parseInt(token);

				if (piece < 0) {
					throw new NumberFormatException(
							"Malformed version specification: `" // NORES
									+ piece + "`." // NORES
									+ "Version number must be > 0."); // NORES
				}
				numbers[i++] = piece;
			} else {
				expectingNumber = true;
			}
		}

		return numbers;
	}

}
