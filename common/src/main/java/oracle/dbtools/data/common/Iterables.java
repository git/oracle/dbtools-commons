/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Various utility methods for manipulating {@link Iterable} instances
 * 
 * @author cdivilly
 */
public class Iterables {
  private Iterables() {
    super();
  }

  public static <T> void add(final Collection<T> collection,
      final Iterable<? extends T> items) {
    add(collection, items, false);
  }

  public static <T> void add(final Collection<T> collection,
      final Iterable<? extends T> items, boolean ignoreNulls) {
    Iterators.add(collection, items == null ? null : items.iterator(),
        ignoreNulls);
  }

  /**
   * Render an Iterable to string representation
   * 
   * @param items
   * @return
   */
  public static String asString(final Iterable<?> items) {
    StringBuilder b = new StringBuilder("[");
    b.append(join(items, ", "));
    b.append("]");
    return b.toString();
  }

  public static <T extends Object & Comparable<? super T>> int compare(
      final Iterable<? extends T> i1, final Iterable<? extends T> i2) {
    return Iterators.compare(i1.iterator(), i2.iterator());
  }

  public static <T> int compare(final Iterable<T> i1, final Iterable<T> i2,
      Comparator<T> comparator) {
    return Iterators.compare(i1.iterator(), i2.iterator(), comparator);
  }

  public static <T> Iterable<T> empty() {
    return Collections.<T> emptyList();
  }

  public static <T> boolean equals(final Iterable<? extends T> i1,
      final Iterable<? extends T> i2) {
    return Iterators.equals(i1.iterator(), i2.iterator());
  }

  /**
   * Return the first item if any
   * 
   * @param <T>
   * @param items
   * @return The first item or null if the {@link Iterable} is empty
   */
  public static final <T> T first(final Iterable<T> items) {
    if (items == null) {
      return null;
    }
    return Iterators.nextOrNull(items.iterator());
  }

  /**
   * Form an iterable from the specified {@link Iterator} of items. Note this
   * method will greedily consume all the contents of the {@link Iterator}.
   * 
   * Null values will be preserved.
   * 
   * @param <T>
   * @param items
   * @return an {@link Iterable} containing the specified items
   */
  public static final <T> Iterable<T> iterable(final Iterator<? extends T> items) {
    final List<T> values = new ArrayList<T>();
    Iterators.add(values, items);
    return values;
  }

  /**
   * Form an iterable from the specified array of items. Note use of this method
   * with parameterized types will result in a warning message which can be
   * safely ignored
   * 
   * @param <T>
   * @param items
   * @return an {@link Iterable} containing the specified items
   */
  public static final <T> Iterable<T> iterable(final T... items) {
    return new ArrayIterable<T>(items);
  }

  /**
   * Concatenate each of the specified items into String form, delimiting each
   * with the specified separator
   * 
   * @param items
   *          items to concatenate
   * @param separator
   *          the character to insert between items
   * @return a String containing the delimited items
   */
  public static String join(final Iterable<?> items, final String separator) {
    return Iterators.join(items.iterator(), separator);
  }

  /**
   * Merge two iterable sets, appending the differences to the end. Any
   * duplicates will be eliminated in the resultant set
   * 
   * @param existingItems
   * @param items
   * @return
   */
  public static <T> Iterable<T> merge(Iterable<? extends T> existingItems,
      Iterable<? extends T> items) {
    final Collection<T> newItems = new ArrayList<T>();
    Iterables.add(newItems, items);
    final Collection<T> mergedItems = new LinkedHashSet<T>();
    Iterables.add(mergedItems, existingItems);

    for (T existing : existingItems) {
      Iterator<T> n = newItems.iterator();
      while (n.hasNext()) {
        T newItem = n.next();
        boolean matched = existing == newItem || existing.equals(newItem);
        if (matched) {
          n.remove();
        }
      }
    }
    mergedItems.addAll(newItems);
    return mergedItems;
  }

   /**
   * Determine the size of an {@link Iterable}. Optimized to retrieve the size
   * of {@link Collection} and {@link HasSize} backed implementations
   * 
   * @param items
   * @return
   */
  public static int size(Iterable<?> items) {
    if (items instanceof Collection) {
      return ((Collection<?>) items).size();
    }
    if (items instanceof HasSize) {
      return ((HasSize) items).size();
    }
    int count = 0;

    for (@SuppressWarnings("unused")
    Object item : items) {
      count++;
    }
    return count;
  }

  /**
   * Sort the specified {@link Iterable} using the specified {@link Comparator}
   * 
   * @param items
   *          The items to sort
   * @param sortOrder
   *          The sorting {@link Comparator}
   * @return The sorted items
   */
  public static final <T> Iterable<T> sort(final Iterable<T> items,
      Comparator<T> sortOrder) {
    List<T> tosort = new ArrayList<T>();
    add(tosort, items);
    Collections.sort(tosort, sortOrder);
    return tosort;
  }

  /**
   * Return an Iterable containing all but the first item in the original
   * Iterable
   * 
   */
  private static final class ArrayIterable<T> implements Iterable<T>, HasSize {
    private ArrayIterable(final T[] items) {
      this.items = items;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof Iterable<?>) {
        @SuppressWarnings("unchecked")
        Iterable<T> other = (Iterable<T>) obj;
        return Iterables.equals(this, other);
      }
      return super.equals(obj);
    }

    @Override
    public int hashCode() {
      return Arrays.hashCode(items);
    }

    @Override
    public boolean isEmpty() {
      return size() == 0;
    }

    @Override
    public Iterator<T> iterator() {
      return Iterators.iterator(items);
    }

    @Override
    public int size() {
      return items.length;
    }

    @Override
    public String toString() {
      return Arrays.toString(items);
    }

    private final T[] items;
  }

}
