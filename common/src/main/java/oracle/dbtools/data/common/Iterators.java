/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.common;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;

/**
 * Various utility methods for forming and manipulating {@link Iterator}s
 * Extracted from Iterators authored by cdivilly.
 * 
 * A simple class for handling iterators.  Support for closeables removed.
 * 
 * 
 * @author joyce.scapicchio
 */
public final class Iterators {
  private Iterators() {
  }

  /**
   * Add items from the specified iterator to the specified collections
   * 
   * @param <T>
   * @param c
   *          collection to add to
   * @param items
   *          items to add
   */
  public static <T> void add(final Collection<T> c,
      final Iterator<? extends T> items) {
    add(c, items, false);
  }

  /**
   * Add items from the specified iterator to the specified collections
   * 
   * @param <T>
   * @param c
   *          collection to add to
   * @param items
   *          items to add
   * @param ignoreNulls
   *          TODO
   */
  public static <T> void add(final Collection<T> c,
      final Iterator<? extends T> items, boolean ignoreNulls) {
    if (c != null && items != null) {
      while (items.hasNext()) {
        final T item = items.next();
        if (!ignore(item, ignoreNulls)) {
          c.add(item);
        }
      }
    }
  }

 
  /**
   * Check if the contents of two iterators are identical
   * 
   * @param <T>
   * @param i1
   *          the first iterator
   * @param i2
   *          the second iterator
   * @return true if the iterators are identical false if not
   */
  public static <T extends Object & Comparable<? super T>> int compare(
      final Iterator<? extends T> i1, final Iterator<? extends T> i2) {
    int c = 0;
    while (i1.hasNext()) {
      final T v1 = i1.next();
      if (!i2.hasNext()) {
        c = 1;
        break;
      }
      final T v2 = i2.next();
      c = v1.compareTo(v2);
      if (c != 0) {
        break;
      }
    }
    if (i2.hasNext()) {
      c = -1;
    }
    return c;
  }

  /**
   * Check if the contents of two iterators are identical
   * 
   * @param <T>
   * @param i1
   *          the first iterator
   * @param i2
   *          the second iterator
   * @return true if the iterators are identical false if not
   */
  public static <T> int compare(final Iterator<T> i1, final Iterator<T> i2,
      Comparator<T> comparator) {
    int c = 0;
    while (i1.hasNext()) {
      final T v1 = i1.next();
      if (!i2.hasNext()) {
        c = 1;
        break;
      }
      final T v2 = i2.next();
      c = comparator.compare(v1, v2);
      if (c != 0) {
        break;
      }
    }
    if (i2.hasNext()) {
      c = -1;
    }
    return c;
  }

   /**
   * Return an empty iterator
   * 
   * @param <T>
   * @return an empty iterator
   */
  @SuppressWarnings("unchecked")
  public static final <T> Iterator<T> empty() {
    return EMPTY;
  }

  public static final <T> Enumeration<T> enumeration(final Iterator<T> items) {
    return new Enumeration<T>() {

      @Override
      public boolean hasMoreElements() {
        return items.hasNext();
      }

      @Override
      public T nextElement() {
        return items.next();
      }

      @Override
      public String toString() {
        return items.toString();
      }
    };
  }

  public static <T> boolean equals(final Iterator<? extends T> i1,
      final Iterator<? extends T> i2) {
    while (i1.hasNext()) {
      final T v1 = i1.next();
      if (!i2.hasNext()) {
        return false;
      }
      final T v2 = i2.next();
      boolean equal = v1 == v2 || v1.equals(v2);
      if (!equal) {
        return false;
      }
    }
    if (i2.hasNext()) {
      return false;
    }
    return true;
  }

  /**
   * Form an iterator from the specified enumeration .
   * 
   * @param <T>
   * @param items
   * @return
   */
  public static final <T> Iterator<T> iterator(final Enumeration<T> items) {
    return new Iterator<T>() {
      @Override
      public boolean hasNext() {
        return items.hasMoreElements();
      }

      @Override
      public T next() {
        return items.nextElement();
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }

      @Override
      public String toString() {
        return items.toString();
      }
    };
  }

  /**
   * It can be challenging debugging iterators backed by collections, as it is
   * not easy at first glance to see what the contents backing the iterator are.
   * This method overrides the {@link Iterator#toString()} method to provide the
   * contents of the collection backing the iterator
   * 
   * @param <T>
   * @param src
   * @param iter
   * @return
   */
  public static <T> Iterator<T> iterator(final Iterable<? extends T> src) {
    if (src == null) {
      return null;
    }
    final Iterator<? extends T> iter = src.iterator();
    return new Iterator<T>() {

      @Override
      public boolean hasNext() {
        return iter.hasNext();
      }

      @Override
      public T next() {
        return iter.next();
      }

      @Override
      public void remove() {
        iter.remove();
      }

      @Override
      public String toString() {
        return src.toString();
      }
    };
  }

  /**
   * Form an iterator from the specified array of items. Note use of this method
   * with parameterized types will result in a warning message which can be
   * safely ignored
   * 
   * @param <T>
   * @param items
   * @return an iterator containing the specified items
   */
  public static final <T> Iterator<T> iterator(final T... items) {
    if (items == null) {
      return null;
    }
    return new ArrayIterator<T>(items);
  }

  public static String join(final Iterator<?> iter, final String separator) {
    final StringBuilder b = new StringBuilder();
    while (iter.hasNext()) {
      final Object i = iter.next();
      b.append(i);
      if (iter.hasNext()) {
        b.append(separator);
      }
    }
    return b.toString();
  }

  /**
   * Find the greatest item in an iterator of comparable items
   * 
   * @param <T>
   * @param i
   *          the iterator containing the items to compare
   * @return the greatest item
   */
  public static <T extends Object & Comparable<? super T>> T max(
      final Iterator<? extends T> i) {
    T candidate = i.next();
    while (i.hasNext()) {
      final T next = i.next();
      if (next.compareTo(candidate) > 0) {
        candidate = next;
      }
    }
    return candidate;
  }

  /**
   * Returns the next item in the iterator
   * 
   * @param <T>
   * @param items
   * @return the next item
   * @throws IllegalStateException
   *           if the iterator has no more items
   */
  public static final <T> T next(final Iterator<? extends T> items) {
    final T next = nextOrNull(items);
    if (next == null) {
      throw new IllegalStateException("no more items");
    }
    return next;
  }

  /**
   * Returns the next item in the iterator
   * 
   * @param <T>
   * @param items
   * @return the next item or null if the iterator has no more items
   */
  public static final <T> T nextOrNull(final Iterator<? extends T> items) {
    if (items.hasNext()) {
      return items.next();
    } else {
      return null;
    }
  }


 
  private static <T> boolean ignore(T item, boolean ignoreNulls) {
    return ignoreNulls && item == null;
  }


  private static final class ArrayIterator<T> implements Iterator<T> {
    private ArrayIterator(final T... items) {
      this.items = items;
    }

    @Override
    public boolean hasNext() {
      return pos < items.length;
    }

    @Override
    public T next() {
      final T item = items[pos];
      pos++;
      return item;
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
      return Arrays.toString(items);
    }

    private final T[] items;

    private int       pos = 0;
  }


  @SuppressWarnings("rawtypes")
  private static final Iterator           EMPTY            = iterator();
  
}
