/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.common;

import java.util.Locale;

import oracle.dbtools.data.common.Iterables;

/**
 * Translatable Message is used to encapsulate all necessary information to
 * allow retrieval of a translated message at a later point in execution. This
 * allows selection of different translations depending upon a list of preferred
 * locales. Usage:
 * 
 * <pre>
 * new TranslatableMessage(CommonMessages.class,
 *     CommonMessages.NotAvailableException_1, &quot;{0} is not available&quot;, type)
 * </pre>
 * 
 * @author rhcooper
 * @author cdivilly
 */
public class TranslatableMessage implements Translatable {

  /**
   * Construct a translatable message
   * 
   * @param baseClass
   *          Resource bundle class
   * @param messageId
   *          ID of message
   * @param defaultTemplate
   *          A default message in case message cannot be retrieved from the
   *          resource bundle
   * @param messageArguments
   *          Optional list of arguments for the message
   */
  public TranslatableMessage(Class<?> baseClass, String messageId,
      String defaultTemplate, Object... messageArguments) {
    this(new TranslatableTemplate(baseClass, messageId, defaultTemplate),
        messageArguments);
  }

  private TranslatableMessage(TranslatableTemplate template,
      Object... messageArguments) {
    this.template = template;
    if (messageArguments == null) {
      this.messageArguments = NO_ARGS;
    } else {
      this.messageArguments = messageArguments;
    }

  }

  /**
   * Constructor for messages that don't have translations such as messages from
   * third party Exceptions.
   * 
   * @param msg
   *          Message without translations
   */
  public TranslatableMessage(String msg) {
    this(NOT_TRANSLATABLE, msg);
  }

  @Override
  public String toString() {
    return toString(Locale.getDefault());
  }

  @Override
  public String toString(Iterable<Locale> localePreference) {
    return Translatables.format(template, localePreference, messageArguments);
  }

  public String toString(Locale locale) {
    return toString(Iterables.iterable(locale));
  }

  public static String toString(Translatable msg,
      Iterable<Locale> localePreference) {
    if (msg == null) {
      return null;
    } else {
      return msg.toString(localePreference);
    }
  }

  private Object[]                          messageArguments;

  private final TranslatableTemplate        template;

  public static final Translatable          EMPTY_MESSAGE    = new TranslatableMessage(
                                                                 TranslatableMessage.class,
                                                                 "", "") {
                                                               @Override
                                                               public String toString() {
                                                                 return "";
                                                               }

                                                               @Override
                                                               public String toString(
                                                                   Locale locale) {
                                                                 return "";
                                                               }
                                                             };

  private static final Object[]             NO_ARGS          = new Object[0];
  private static final TranslatableTemplate NOT_TRANSLATABLE = new TranslatableTemplate(
                                                                 null, null,
                                                                 "{0}");
}
