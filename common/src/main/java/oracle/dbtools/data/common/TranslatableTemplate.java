/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.common;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import oracle.dbtools.data.common.Iterables;

/**
 * A translatable message that may contain parameters as defined by
 * {@link MessageFormat}.
 * 
 * Localize to {@link String} using
 * {@link Translatables#format(Translatable, Iterable, Object...)}
 * 
 * @see MessageFormat
 * @author cdivilly
 * 
 */
class TranslatableTemplate implements Translatable {

  TranslatableTemplate(Class<?> baseClass, String messageId,
      String defaultTemplate) {
    super();
    this.baseClass = baseClass;
    this.messageId = messageId;
    this.defaultTemplate = defaultTemplate;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    TranslatableTemplate other = (TranslatableTemplate) obj;
    if (baseClass == null) {
      if (other.baseClass != null)
        return false;
    } else if (!baseClass.getName().equals(other.baseClass.getName()))
      return false;
    if (defaultTemplate == null) {
      if (other.defaultTemplate != null)
        return false;
    } else if (!defaultTemplate.equals(other.defaultTemplate))
      return false;
    if (messageId == null) {
      if (other.messageId != null)
        return false;
    } else if (!messageId.equals(other.messageId))
      return false;
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result
        + ((baseClass == null) ? 0 : baseClass.getName().hashCode());
    result = prime * result
        + ((defaultTemplate == null) ? 0 : defaultTemplate.hashCode());
    result = prime * result + ((messageId == null) ? 0 : messageId.hashCode());
    return result;
  }

  @Override
  public String toString() {
    return toString(Iterables.iterable(Locale.getDefault()));
  }

  @Override
  public String toString(Iterable<Locale> localePreference) {

    String template = defaultTemplate;
    if (baseClass != null) {
      final ResourceBundle bundle = Translatables.getBundle(baseClass,
          localePreference);
      if (bundle != null) {
        final String value = bundle.getString(messageId);
        if (value != null) {
          template = value;
        }
      }
    }
    return template;
  }

  private final Class<?> baseClass;

  private final String   defaultTemplate;

  private final String   messageId;

}