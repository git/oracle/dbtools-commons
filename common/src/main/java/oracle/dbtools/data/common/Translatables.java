/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.common;

import java.text.MessageFormat;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

import oracle.dbtools.data.common.Iterables;
import oracle.dbtools.data.common.NullOrEmpty;

/**
 * Utility methods for working with {@link Translatable}s
 * 
 * @author cdivilly
 * 
 */
public abstract class Translatables {

  private Translatables() {
  }

  /**
   * Chooses the best match to a client's locale preference from a set of
   * available locales
   * 
   * @param localePreference
   *          The set of preferred locales ordered from most preferred to least
   * @param availableLocales
   *          The set of locales that are actually available
   * @return
   */
  public static Locale choose(Iterable<Locale> localePreference,
      Iterable<Locale> availableLocales) {
    Set<Locale> available = new LinkedHashSet<Locale>();
    Iterables.add(available, availableLocales);
    for (Locale locale : localePreference) {
      Locale candidate = locale;
      while (candidate != Locale.ROOT) {
        // If the candidate equals: 'en' then return it, because 'en' is the
        // default localization and always available
        if (DEFAULT_LOCALIZAITON.equals(candidate)) {
          return DEFAULT_LOCALIZAITON;
        }
        if (available.contains(candidate)) {
          return candidate;
        }
        candidate = parent(candidate);
      }
    }
    // Fall back to the root locale
    return Locale.ROOT;
  }

  /**
   * Localize a message while substituting in the specified arguments
   * 
   * @param message
   *          The message to localize
   * @param localePreference
   *          The locale preference
   * @param messageArguments
   *          The arguments to be substituted into the message
   * @return The localized and substituted messages
   * @see MessageFormat
   */
  public static String format(Translatable message,
      Iterable<Locale> localePreference, Object... messageArguments) {
    String translation = message.toString(localePreference);
    return MessageFormat.format(translation, messageArguments);
  }

  /**
   * Finds the best {@link ResourceBundle} match for the specified set of
   * preferred locales.
   * 
   * This method should be used instead of
   * {@link ResourceBundle#getBundle(Class, Locale)}.
   * 
   * @param baseClass
   *          the base name of the resource bundle class
   * @param localePreference
   *          Ordered Set of preferred locales from most to least preferred
   * @exception NullPointerException
   *              if <code>baseClass</code> or <code>localePreference</code> is
   *              <code>null</code>
   * @exception MissingResourceException
   *              if no resource bundle for the specified base name can be found
   * @return a resource bundle for the given base name and locale preference
   */
  public static ResourceBundle getBundle(Class<?> baseClass,
      Iterable<Locale> localePreference) {
    return getBundle(baseClass.getName(), localePreference);
  }

  /**
   * Finds the best {@link ResourceBundle} match for the specified set of
   * preferred locales.
   * 
   * This method should be used instead of
   * {@link ResourceBundle#getBundle(String, Locale)}.
   * 
   * @param baseName
   *          the base name of the resource bundle, a fully qualified class name
   * @param localePreference
   *          Ordered Set of preferred locales from most to least preferred
   * @exception NullPointerException
   *              if <code>baseName</code> or <code>localePreference</code> is
   *              <code>null</code>
   * @exception MissingResourceException
   *              if no resource bundle for the specified base name can be found
   * @return a resource bundle for the given base name and locale preference
   */
  public static ResourceBundle getBundle(String baseName,
      Iterable<Locale> localePreference) {
    Set<Locale> available = new LinkedHashSet<Locale>();
    for (Locale locale : localePreference) {
      ResourceBundle bundle = ResourceBundle.getBundle(baseName, locale);
      Locale bundleLocale = bundle.getLocale();
      if (NullOrEmpty.nullOrEmpty(bundleLocale.getLanguage())) {
        bundleLocale = Locale.ROOT;
      }
      available.add(bundleLocale);
    }
    Locale bestMatch = choose(localePreference, available);
    return ResourceBundle.getBundle(baseName, bestMatch);
  }

  /**
   * Wrap a non-translatable value in a {@link Translatable} wrapper. Useful
   * whenever there is a need to mix some non-translatable values in with
   * possibly translatable values
   * 
   * @param text
   * @return
   */
  public static Translatable notTranslatable(final CharSequence text) {
    if (text == null) {
      return null;
    }
    return new NotTranslated(text);
  }

  /**
   * Return the parent locale of a locale. The parent of <code>th_TH_TH</code>
   * is <code>th_TH</code>, the parent of <code>en_US</code> is <code>en</code>.
   * 
   * @param locale
   * @return The parent locale or null if the locale has no parent
   */
  private static Locale parent(Locale locale) {
    if (!NullOrEmpty.nullOrEmpty(locale.getVariant())) {
      return new Locale(locale.getLanguage(), locale.getCountry());
    }
    if (!NullOrEmpty.nullOrEmpty(locale.getCountry())) {
      return new Locale(locale.getLanguage());
    }
    return Locale.ROOT;
  }

  private static final Locale DEFAULT_LOCALIZAITON = new Locale("en");

  private static final class NotTranslated implements Translatable {
    private NotTranslated(CharSequence text) {
      this.text = text.toString();
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      NotTranslated other = (NotTranslated) obj;
      if (text == null) {
        if (other.text != null)
          return false;
      } else if (!text.equals(other.text))
        return false;
      return true;
    }

    @Override
    public int hashCode() {
      return text.hashCode();
    }

    @Override
    public String toString() {
      return text;
    }

    @Override
    public String toString(Iterable<Locale> localePreference) {
      return toString();
    }

    private final String text;
  }
}