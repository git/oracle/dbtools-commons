/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.formattors;

import java.sql.Connection;

import java.util.Locale;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.data.model.Column;
/**
 * @author Joyce Scapicchio
 */

public abstract class BaseFormattor implements ITypeFormattor {
	protected Locale _l;
	boolean m_isPrecisionAllowed;
	boolean m_isScaleAllowed;
	boolean m_isScaleImported;
	boolean m_isAddDoubleQuote=false;
	Connection m_connection;
	public abstract String formatColumn(Column col) ;
	public abstract String formatData(Object data, Column c);
	// requires locale and connection to be set
	public Object formatDataForJava(Object data, Column c) throws Exception {
	  return formatData(data,c);  //same b/c numbers aren't quoted
	}  
  	public abstract int getSqlType(String type); 
  
	public void setLocale(Locale l) {
		_l = l;
	}
	public Locale getLocale() {
		return _l;
	}
	public void setConnection(Connection conn) {
		m_connection = conn;
	}
	public Connection getConnection() {
		return m_connection;
	}	
	public boolean isPrecisionAllowed(){
		return m_isPrecisionAllowed;
	}
	public void isPrecisionAllowed(boolean isPrecisionAllowed){
		m_isPrecisionAllowed=isPrecisionAllowed;
	}
	public boolean isScaleAllowed(){
		return m_isScaleAllowed;
	}
	public void isScaleAllowed(boolean isScaleAllowed){
		m_isScaleAllowed=isScaleAllowed;
	}
	public boolean isScaleImported(){
		return m_isScaleImported;
	}
	public void isScaleImported(boolean isScaleImported){
		m_isScaleImported=isScaleImported;
	}
	public boolean isAddDoubleQuote(){
		return m_isAddDoubleQuote;
	}
	public void isAddDoubleQuote(boolean isAddDoubleQuote){
		m_isAddDoubleQuote=isAddDoubleQuote;
	}
    public String addDoubleQuote(String name){
    	if (m_isAddDoubleQuote){
    		return DBUtil.addDoubleQuote(name);
    	} 
    	return name;
    }
}
