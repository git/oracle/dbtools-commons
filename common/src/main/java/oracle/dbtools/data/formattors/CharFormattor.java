/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.formattors;

import oracle.dbtools.data.model.Column;
/**
 * @author Barry McGillin
 *
 */
public class CharFormattor extends BaseFormattor
{
  public String formatColumn(Column col)
  {	  
    String result = null;
    // Bug 8841693 - IMPORT DATA SHOULD NOT GENERATE 
    // CREATE TABLE WITH QUOTED COLUMN NAMES 
    //result = DBUtil.addDoubleQuote(col.getName()) + " " + col.getType();	 //$NON-NLS-1$
    result = addDoubleQuote(col.getName()) + " " + col.getType();	 //$NON-NLS-1$

    if (col.getName() != null)
    {
      if (col.getPrecision() > 0)
        result +=  "(" + col.getPrecision() + ")";      //$NON-NLS-1$ //$NON-NLS-2$
    }
    
    if (col.getDefault() != null && col.getDefault().trim().length() > 0)
    	result += " DEFAULT " +  col.getDefault();     //$NON-NLS-1$
    
    if (!col.isNullable())
      result +=" NOT NULL"; //$NON-NLS-1$
        
    
    return result;
  }
  
  public String formatData(Object data,  Column c){
	  return formatData(data,c,true);
  }
  
  public Object formatDataForJava(Object data, Column c) throws Exception {
	  //return formatData(data,c,false);
	  return (data!=null)?data.toString():"NULL"; //$NON-NLS-1$
  }
  
  public String formatData(Object data,  Column c, boolean addQuotes )
  {
    String result = "NULL"; //$NON-NLS-1$
    if (data != null)
    {
      String val = data.toString();
      //Fix Bug: 7041371
      val = val.replaceAll("'", "''"); //$NON-NLS-1$ //$NON-NLS-2$
    	  
      if (addQuotes){
    	  if (c.getType().toUpperCase().equals("NCHAR") || c.getType().toUpperCase().equals("NVARCHAR2"))
    		  result = "N'" + val + "'"; //$NON-NLS-1$ //$NON-NLS-2$ //Fix Bug: 8712225 - SQLDEV2.1:NLS:IMPORTED MULTIBYTE DATA IN NCHAR/NVARCHARS ARE GARBAGE 

    	  else
    		  result = "'" + val + "'"; //$NON-NLS-1$ //$NON-NLS-2$
      }
      else {
    	  result = val;
      }
    }
    return result;
  }
  
  public int getSqlType(String type) {
	  if (type.equals("VARCHAR2")){
		  return java.sql.Types.VARCHAR;
	  }
	  else if (type.equals("CHAR")){
		  return java.sql.Types.CHAR;
	  }
	  else if (type.equals("NCHAR")){
		  return java.sql.Types.NCHAR;
	  }
	  else if (type.equals("NVARCHAR2")){
		  return java.sql.Types.NVARCHAR;
	  }
	  else return java.sql.Types.NULL; //?
  }

 }   