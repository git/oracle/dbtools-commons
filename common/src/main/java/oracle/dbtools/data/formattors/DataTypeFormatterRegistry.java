/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.formattors;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * A dataformat registery used by import generator.    If the datattype is registered, caller can get a
 * formatter which identifies the data characteristics of the data type.
 * 
 * 
 */

public class DataTypeFormatterRegistry 
{
  //private  HashMap<String, Class<? extends ITypeFormattor>> _fmtMap = new HashMap<String, Class<? extends ITypeFormattor>>();
  private  HashMap<String, DataTypeDef> _fmtMap = new HashMap<String, DataTypeDef>();
  private static DataTypeFormatterRegistry INSTANCE;
  
  
   synchronized public  void registerFormattor(String type, Class<? extends ITypeFormattor> formater, boolean isPrecisionAllowed, boolean isScaleAllowed, boolean isScaleImported)
   {
       if ( _fmtMap == null )
    	   //_fmtMap = new HashMap<String,Class<? extends ITypeFormattor>>();
    	   _fmtMap=new HashMap<String, DataTypeDef>();
       //_fmtMap.put(type, formater);	   
       DataTypeDef dataType = new DataTypeDef(formater,isPrecisionAllowed,isScaleAllowed,isScaleImported);
       _fmtMap.put(type,dataType);
   }
   
   synchronized public  void unregisterFormattor(String type)
   {
       if ( _fmtMap != null ){
    	   _fmtMap.remove(type);
       }	   
   }
   
   /* 
    * This method returns an uninitialized formattor use the set methods to initialize it before calling the 
    * format methods
    * 
    * Returns null if the datatype does not have a registered formatter.
    * 
    */
   public ITypeFormattor getFormattor(String type){
       if ( _fmtMap != null && type != null ){
           try {
               //return _fmtMap.get(type).newInstance();
        	   DataTypeDef dataType = _fmtMap.get(type);
        	   if (dataType == null){
        		   return null;
        	   }
        	   //return dataType.getHandler().newInstance();
        	   ITypeFormattor formattor = dataType.getHandler().newInstance();
        	   formattor.isPrecisionAllowed(dataType.isPrecisionAllowed());
        	   formattor.isScaleAllowed(dataType.isScaleAllowed());
        	   formattor.isScaleImported(dataType.isScaleImported());
        	   //return dataType.getHandler().newInstance();
        	   return formattor;
           }
           catch (Exception e) {
        	   Logger.getLogger(getClass().getName()).log(Level.WARNING,
						e.getStackTrace()[0].toString(),
						e);
           }
       }
       return null;
   }   
   
 private DataTypeFormatterRegistry() {
       registerFormattor("NUMBER", NumberFormattor.class,true,true,true);  //$NON-NLS-1$
       registerFormattor("INTEGER", NumberFormattor.class,false,false,false); //$NON-NLS-1$
       registerFormattor("LONG", NumberFormattor.class,false,false,false); //$NON-NLS-1$
       registerFormattor("FLOAT", NumberFormattor.class,false,false,true);     //$NON-NLS-1$
       registerFormattor("CHAR" , CharFormattor.class,true,false,false); //$NON-NLS-1$
       registerFormattor("NCHAR" , CharFormattor.class,true,false,false); //$NON-NLS-1$
       registerFormattor("VARCHAR2" , CharFormattor.class,true,false,false); //$NON-NLS-1$
       registerFormattor("NVARCHAR2" , CharFormattor.class,true,false,false);      //$NON-NLS-1$
       registerFormattor("DATE" , DateFormattor.class,false,false,false);    //$NON-NLS-1$
       registerFormattor("TIMESTAMP WITH TIME ZONE" , DateFormattor.class,true,false,false); //$NON-NLS-1$
       registerFormattor("TIMESTAMP WITH LOCAL TIME ZONE" , DateFormattor.class,true,false,false); //$NON-NLS-1$
       registerFormattor("TIMESTAMP" , DateFormattor.class,true,false,false);      //$NON-NLS-1$
  }

public static DataTypeFormatterRegistry  getInstance() {
     if (INSTANCE == null) {
       INSTANCE = new DataTypeFormatterRegistry();
     }
     return INSTANCE;
  }

public class DataTypeDef {
	boolean m_isPrecisionAllowed; 
	boolean m_isScaleAllowed;
	boolean m_isScaleImported;
	Class<? extends ITypeFormattor> m_handler;
	
	public DataTypeDef(Class<? extends ITypeFormattor> handler, boolean isPrecisionAllowed, boolean isScaleAllowed, boolean isScaleImported) {
		m_handler=handler;
		m_isPrecisionAllowed=isPrecisionAllowed;
		m_isScaleAllowed=isScaleAllowed;
		m_isScaleImported=isScaleImported;
	}
	
	Class<? extends ITypeFormattor> getHandler(){
		return m_handler;
	}
	public boolean isPrecisionAllowed(){
		return m_isPrecisionAllowed;
	}
	public void isPrecisionAllowed(boolean isPrecisionAllowed){
		m_isPrecisionAllowed=isPrecisionAllowed;
	}
	public boolean isScaleAllowed(){
		return m_isScaleAllowed;
	}
	public void isScaleAllowed(boolean isScaleAllowed){
		m_isScaleAllowed=isScaleAllowed;
	}
	public boolean isScaleImported(){
		return m_isScaleImported;
	}
	public void isScaleImported(boolean isScaleImported){
		m_isScaleImported=isScaleImported;
	}

}

}
