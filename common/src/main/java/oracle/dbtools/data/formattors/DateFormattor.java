/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.formattors;

import java.text.ParsePosition;
import java.text.ParseException;

import oracle.dbtools.data.model.Column;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.dbtools.raptor.nls.OraDATEFormat;
import oracle.dbtools.raptor.nls.OraTIMESTAMPFormat;
import oracle.dbtools.raptor.nls.OraTIMESTAMPLTZFormat;
import oracle.dbtools.raptor.nls.OraTIMESTAMPTZFormat;
import oracle.dbtools.raptor.nls.OracleNLSProvider;
import oracle.i18n.util.OraLocaleInfo;
import oracle.jdbc.OracleTypes;
//import oracle.sql.TIMESTAMP;
//import oracle.sql.TIMESTAMPTZ;
/**
 * @author Barry McGillin
 */
public class DateFormattor extends BaseFormattor {
  

	public String formatColumn(Column col) {
	  
	String retVal = null;
	
    // Bug 8841693 - IMPORT DATA SHOULD NOT GENERATE 
    // CREATE TABLE WITH QUOTED COLUMN NAMES 
	// retVal = DBUtil.addDoubleQuote(col.getName()) + " " + col.getType(); //$NON-NLS-1$
	retVal = addDoubleQuote(col.getName()) + " " + col.getType(); //$NON-NLS-1$
	
    if (col.getDefault() != null && col.getDefault().trim().length() > 0)
    	retVal += " DEFAULT " +  col.getDefault();     //$NON-NLS-1$
	
	if (!col.isNullable())
      retVal += " NOT NULL"; //$NON-NLS-1$
    
    
    return retVal;
  }

  public String formatData(Object data, Column c) {
	  String result = "NULL"; //$NON-NLS-1$
	  String fmt = c.getFormat();
	  String fmtParm = "";
	  if (fmt != null && fmt.length() > 0){
	  	fmtParm = "', '" + fmt; //$NON-NLS-1$ //$NON-NLS-2$
	  	// Forum issue - double quotes being stripped from date mask.  This code has been
		// in nearly from the beginning of import and I cannot think why they would be
		// stripped even if it is an edge case to embed quotes in the mask.
	  	//fmtParm = "', '" + fmt.replaceAll("\"", ""); //$NON-NLS-1$ //$NON-NLS-2$
	  }		  
	  if (data != null) {
		  if (c.getType().equalsIgnoreCase("timestamp")) { //$NON-NLS-1$

			  //if (fmt != null)
			  //{
			  //	  result = "to_timestamp('" + data.toString() + "', '" + fmt + "')";  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			  //}  
			  result = "to_timestamp('" + data.toString() + fmtParm + "')";  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		  }
		  else if(c.getType().equalsIgnoreCase("timestamp with time zone") ||
				  c.getType().equalsIgnoreCase("timestamp with local time zone"))
		  {
			  //result = "to_timestamp_tz('" + data.toString() + "', '" + fmt + "')"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			  result = "to_timestamp_tz('" + data.toString() + fmtParm + "')"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		  }
		  else {
			  //result = "to_date('" + data.toString() + "', '" + fmt + "')"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			  result = "to_date('" + data.toString() + fmtParm + "')"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		  }
	  }
	  return result;
  }

  public Object formatDataForJava(Object data, Column c) throws Exception {
	  //return formatData(data,c);  //TODO test to see how to handle
	  Object result = "NULL"; //$NON-NLS-1$
	  String fmt = c.getFormat();

	  if (fmt != null)
		  fmt = fmt.replaceAll("\"", ""); //$NON-NLS-1$ //$NON-NLS-2$

	  if (data != null) {
		  String dataStr = data.toString();
		  
          // A point to here for DATE, TIMESTAMP, TIMESTAMPTZ, and TIMESTAMPLTZ. 
		  // See OraTIMESTAMPFormat.java for details on oracle time stamps.
		  // Note that we should not be using of the format() methods of OraTIMESTAMPFormat 
		  // or OraTIMESTAMPTZFormat as they call the globalization methods and tend to 
		  // translate the months and years in the native languages and that creates
		  // conflict with the nls_preferences of the database.
		  //
		  // To resolve the problem that we would like to first see if we can first use
		  // the database timestamp preferences before using OraTIMESTAMPFormat &
		  // OraTIMESTAMPFormat classes. 

		  if (dataStr.length() > 0 && !dataStr.equals("NULL")){
			  if (c.getType().equalsIgnoreCase("TIMESTAMP")) { //$NON-NLS-1$
				  OraTIMESTAMPFormat format;
				  
				  if (fmt == null || fmt.length()==0) {
						  format = ((OracleNLSProvider)NLSProvider.getProvider(m_connection)).getOraTIMESTAMPFormat();
				  }	
				  else {
						  format = new OraTIMESTAMPFormat(fmt,OraLocaleInfo.getInstance(getLocale()));
				  }
				  oracle.sql.TIMESTAMP tst  = format.parse(dataStr, new ParsePosition(0));
				  result = tst;				   
			  }
			  else if(c.getType().equalsIgnoreCase("TIMESTAMP WITH TIME ZONE")) {

				  OraTIMESTAMPTZFormat format;
				  if (fmt == null || fmt.length()==0){
					  format = ((OracleNLSProvider)NLSProvider.getProvider(m_connection)).getOraTIMESTAMPTZFormat();
				  }	
				  else {
					  format = new OraTIMESTAMPTZFormat(fmt,OraLocaleInfo.getInstance(getLocale()));
				  }
				  oracle.sql.TIMESTAMPTZ tstz = format.parse(dataStr, new ParsePosition(0));
				  result = tstz;				  
			  }
			  else if (c.getType().equalsIgnoreCase("TIMESTAMP WITH LOCAL TIME ZONE")) { //$NON-NLS-1$
				  OraTIMESTAMPLTZFormat format;
				  OracleNLSProvider nlsProvider = (OracleNLSProvider)NLSProvider.getProvider(m_connection);
				  if (fmt == null || fmt.length()==0) {
					  format = nlsProvider.getOraTIMESTAMPLTZFormat();
				  }	
				  else {
					  format = new OraTIMESTAMPLTZFormat(fmt, OraLocaleInfo.getInstance(getLocale()), nlsProvider.getTimeZone(nlsProvider.getDBTimeZone()), nlsProvider.getSessionTimeZone());
				  }
				  oracle.sql.TIMESTAMPLTZ tst  = format.parse(dataStr, new ParsePosition(0));
				  result = tst;
			  }
			  else {
				  OraDATEFormat format;
				  if (fmt == null || fmt.length()==0){
                      try {
						  format = ((OracleNLSProvider)NLSProvider.getProvider(m_connection)).getOraDATEFormat();
                      } catch (ParseException pe) {
						  format = null;
                      }
				  }	
				  else {
					  format = new OraDATEFormat(fmt,OraLocaleInfo.getInstance(getLocale()));
				  }
				  oracle.sql.DATE tst  = format.parse(dataStr, new ParsePosition(0));
				  return tst;
			  }
		  }
	  }

	  return result;
  }
  
  public int getSqlType(String type) {
	  
	  //"DATE" $NON-NLS-1$
      //"TIMESTAMP WITH TIME ZONE" $NON-NLS-1$
      //"TIMESTAMP WITH LOCAL TIME ZONE"$NON-NLS-1$
      //"TIMESTAMP" NON-NLS-1$
	  
	  if (type.equals("DATE")){
		  //return java.sql.Types.DATE;
		  return OracleTypes.DATE;
	  }
	  else if (type.equals("TIMESTAMP WITH TIME ZONE")){
		  return OracleTypes.TIMESTAMPTZ;
	  }
	  else if (type.equals("TIMESTAMP WITH LOCAL TIME ZONE")){
		  // return OracleTypes.TIMESTAMPLTZ; 
		  // Some how we not have support for timestamp with local zone.
		  // i.e., oraTIMESTAMPLTZ.java is missing! -- Shounak
		  // It is in fact wrapped as timestamp. For now it is timestamp.
		  return OracleTypes.TIMESTAMP;
	  }
	  else {
		  //return java.sql.Types.TIMESTAMP;
		  return OracleTypes.TIMESTAMP;
	  }
  }	  
  
}