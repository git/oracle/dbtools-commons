/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.formattors;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;

import oracle.dbtools.data.model.Column;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.dbtools.raptor.nls.OracleNLSProvider;
/**
 * @author Barry McGillin
 */

public class NumberFormattor extends BaseFormattor {
  public String formatColumn(Column col) {
    String result = null;

    // Bug 8841693 - IMPORT DATA SHOULD NOT GENERATE 
    // CREATE TABLE WITH QUOTED COLUMN NAMES 
  	// result = DBUtil.addDoubleQuote(col.getName()) + " " + col.getType(); //$NON-NLS-1$
  	result = addDoubleQuote(col.getName()) + " " + col.getType(); //$NON-NLS-1$
    
    if (col.getName() != null) {
      if (isPrecisionAllowed() && col.getPrecision() > 0) {
        if (col.getScale() > 0)
          result += "(" + col.getPrecision() + //$NON-NLS-1$
                        ", " + col.getScale() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
        else
          result +=  "(" + col.getPrecision() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
      }
    }

    if (col.getDefault() != null && col.getDefault().trim().length() > 0)
    	result += " DEFAULT " +  col.getDefault();     //$NON-NLS-1$
    
    if (!col.isNullable())
      result += " NOT NULL"; //$NON-NLS-1$


    return result;
  }

  public String formatData(Object data, Column c) {
	  String result = "NULL"; //$NON-NLS-1$
	  if (data != null)
	  {
		  String dataStr = data.toString();
		  NumberFormat fmt = null;
		  ParsePosition pp = new ParsePosition(0);

		  if (_l != null)
			  fmt = NumberFormat.getInstance(_l);
		  else
			  fmt = NumberFormat.getInstance();

		  try
		  {
			  if (c.getPrecision()>0 && c.getScale() > 0 || c.getPrecision() == 0 && c.getScale() == 0
					  || isScaleImported())
				  result = "" + fmt.parse(dataStr, pp).doubleValue(); //$NON-NLS-1$
			  else
				  result = "" + fmt.parse(dataStr, pp).longValue(); //$NON-NLS-1$
		  }
		  catch(Exception e)
		  {
			  result = dataStr; // Let the server complain, we'll just pick the value
		  }

		  // parse may not parse the entire string and it is not handled
		  // as an exception, so if a number was found but there were more
		  // characters, just give up on it as a number. (eg. pnone number like 555-555-5555
		  // is detected as a number... shorter than you would expect!
		  if (pp.getIndex() < dataStr.length()){
			  return dataStr;
		  }
	  }

	  if (result.length()==0)
		  return "NULL"; //$NON-NLS-1$
	  else
		  return result;
  }
  public Object formatDataForJava(Object data, Column c) throws Exception{
	  String num = formatData(data,c);  //same b/c numbers aren't quoted
	  									// but need to do the format to handle groups
	  if (m_connection!=null && num != null){
		  String decChar = ""+((OracleNLSProvider)NLSProvider.getProvider(m_connection)).getDecimalSeparator();
		  num=num.replace(".", decChar);
	  }	  
	  return num;
	  //return formatData(data,c);  //same b/c numbers aren't quoted
	  //if (data != null){			// but need to do the format to handle groups
	  //	  return data.toString();
	  //} 
	  //return "NULL"; //$NON-NLS-1$
  }  
  public int getSqlType(String type) {
	  if (type.equals("NUMBER")){
		  return java.sql.Types.NUMERIC;
	  }
	  else if (type.equals("INTEGER")){
		  return java.sql.Types.INTEGER;
	  }
	  else if (type.equals("LONG")){
		  return java.sql.Types.DOUBLE;
	  }
	  else if (type.equals("FLOAT")){
		  return java.sql.Types.FLOAT;
	  }
	  else return java.sql.Types.NUMERIC;
  }
 
  
}
