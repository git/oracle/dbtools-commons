/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.loadservice;


import java.util.Locale;

import oracle.dbtools.data.common.TranslatableMessage;

	/**
	 * ExitCode contains all valid the exit codes from a load.  Use toStringTranslated()
	 * to get a translated message for the exit code.
	 * @author jscapicc
	 *
	 */
	public enum ExitCode {
		EXIT_SUCCESS(0), 
		EXIT_WARNING(1),
		EXIT_ERROR(2),
		EXIT_SEVERE(3);
		
		private int _severity;
		private String _detailMessage="";
		private String _shortMessage="";
		
		ExitCode(int severity){
			_severity=severity;
		}
		
		/**
		 * toTranslatedString returns a translated message string for the type
		 * @param - iterable of ordered locales
		 * @return - translated string for the status.  If locales is null,
		 * 	the default locale is used.
		 */
		public String toStringTranslated(){
    		return LoadResources.getString(name());
		}
		
		/**
		 * toTranslatedString returns a translated message string for the type
		 * @param - iterable of ordered locales
		 * @return - translated string for the status.  If locales is null,
		 * 	the default locale is used.
		 */
		public String toStringTranslated(Iterable <Locale> locales){
			if (locales == null){
	    		return LoadResources.getString(name());
	    	}
			return (new TranslatableMessage(LoadResources.class, name(), 
					LoadResources.getString(name()))) //$NON-NLS-1$
						.toString(locales); 
		}
		/**
		 * Return the int representing the severity of the exit code.
		 * Higher value represents a greater severity.
		 * @return
		 */
		public int getSeverity(){
			return _severity;
		}
		
		/**
		 * Return the (optional) detailed message associated with this exitCode.
		 * @return
		 */
		public String getMessage(){
			return _detailMessage;
		}
		
		/**
		 * Set the (optional) detailed message to associate with this exitCode.
		 * @return
		 */
		public void setMessage(String detailMessage){
			_detailMessage=detailMessage;
		}
		
		/**
		 * Return the (optional) short message associated with this exitCode.
		 * @return
		 */
		public String getShortMessage(){
			return _shortMessage;
		}
		
		/**
		 * Set the (optional) short message to associate with this exitCode		  
		 * @return
		 */
		public void setShortMessage(String shortMessage){
			_shortMessage=shortMessage;
		}
	}
	