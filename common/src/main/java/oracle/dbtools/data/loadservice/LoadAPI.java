/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.loadservice;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.data.common.TranslatableMessage;
import oracle.dbtools.data.loadservice.LoadParmsAPI.SERVICE_METHOD;
import oracle.dbtools.data.readservice.ReadParmsDelimitedAPI;
import oracle.dbtools.data.readservice.ReadParmsAPI;

/**
 * LoadAPI class for data loading.
 * 
 * Basic requirements:
 * 	1.  A connection
 *  2.  Schema name
 *  3   Table name
 *  4.  An input stream for reading the data.  A stream reader is used to allow encoding.
 *  5.  An outout stream for writing the response.  A stream writer is used to allow encoding
 *  
 *  Optional
 *  1.  Load parameters identifying the loader and specifying the options for it (default for jdbc provided)
 *  	Class must extend LoadParmsAPI.  
 *  2.  Reader parameters identifying the reader and specifying the options for it (default for csv provided)
 *  3.  A log stream for handling java log messages
 * 
 * This API uses the Builder pattern for construction.
 * 
 * Use the internal Builder class to generate the builder for the constructor (see constructor comments).  
 * 
 * 
 * @author Joyce Scapicchio
 * 
 */
public class LoadAPI {
	
	private static enum Parms {
		CONNECTION,
		SCHEMA_NAME,
		TABLE_NAME,
		INPUT_STREAM,
		RESPONSE_STREAM,
		LOAD_PARMS,
		READER_PARMS,
		LOGGER
	}
	
	// default logger
	private static final Logger LOGGER = Logger.getLogger(LoadAPI.class.getName());
	
	// required parameter values
	private Connection _conn;
	private String _schemaName;
	private String _tableName;
	private InputStream _input;
	private OutputStream _response;  
	// optional parameter values
	private LoadParmsAPI _loadParms;
	private ReadParmsAPI _readParms;
	private final Logger _logger;
	
    private LoadService _service = null;
    
   
    /**
	 * Builder class for constructing and validating LoadAPI.   
	 * @author jscapicc
	 *
	 */        
    public static class Builder {
     	// required parameters
    	private Connection _conn;
    	private String _schemaName;
    	private String _tableName;
    	private InputStream _input;
    	private OutputStream _response;  
    	// optional parameters
    	private LoadParmsAPI _loadParms;
    	private ReadParmsAPI _readParms;
    	private Logger _logger;
    
    	/**
    	 * Builder constructor takes the required parameters.
    	 * Use other public methods as needed to set optional parameters.
    	 * 
     	 * LoadAPI constructor will use this object on build()
     	 * 
     	 * @param conn identifies an active connection for the load
     	 * @param schemaName identifies the table owner
     	 * @param tableName identifies the table to load
     	 * @param input identifies the input stream used for reading the data to load
     	 * @param response identifies the output stream used for messages and bad data
    	 */
    	
    	public Builder (Connection conn, 
    		String schemaName, String tableName,
    		InputStream input, OutputStream response){
    		_conn=conn;
    		_schemaName = schemaName;
    		_tableName=tableName;
    		_input=input;
    		_response=response;
    	}
    	/**
    	 * Set the loadParms for this load which specifies the load options to 
    	 * be used for the load (eg, rows per commit).  See LoadParmsAPI.
    	 * 
    	 * If loadParms is omitted, the all defaults for the loadparms are used.
    	 * 
    	 * @param loadParms loadParmsAPI object
    	 * @return Builder
    	 */
    	public Builder loadParms(LoadParmsAPI loadParms){
    		_loadParms=loadParms;
    		return this;
    	}
    	/**
    	 * set the readParms for this load which specifies the input type and 
    	 * read options to be used for the load (eg, delimiter).  See ReadParmsAPI.
    	 * 
    	 * If readParms is omitted, the all defaults for the readParms are used.
    	 * 
    	 * @param readParms readParmsAPI object
    	 * @return Builder
    	 */
    	public Builder readParms(ReadParmsAPI readParms){
    		_readParms=readParms;
    		return this;
    	}
    	/**
    	 * Set the logger for this load which identifies a service logger 
    	 * be used for the load. It indicates that the load is being done as part 
    	 * of a larger product (e.g., rest) which uses the specified logger.
    	 * 
    	 * If logger is omitted, the class based naming is used for the logger.
    	 * 
    	 * @param logger java.util.logging.Logger object
    	 * @return Builder
    	 */
    	public Builder logger(Logger logger){
    		_logger=logger;
    		return this;
    	}
    	/**
    	 * Build LoadAPI using options specified in this object.
    	 * @return LoadAPI object
    	 * 
    	 * Throws illegalArgumentExeption if the parameters are invalid and the object
    	 * cannot be constructed.
    	 */
    	public LoadAPI build(){
    		return new LoadAPI(this);
    	}
    	
    }
    
    /**
     * Single constructor taking Builder
     * 
     * Example:  Using Builder constructor with only the required parameters then build:
     * new LoadAPI.Builder(conn,schema,table,_input,_output).build();
     * 
     * Example:  Using Builder constructor with a single optional parameter:
     * new LoadAPI.Builder(conn,schema,table,_input,_output).loadParms(loadParms).build();
     * 
     * Example:  Using builder constructor with a all optional parameter:
     * new LoadAPI.Builder(conn,schema,table,_input,_output).loadParms(loadParms).readerParms(readerParms).logStream(logStream).build();
     */
    
    private LoadAPI(Builder builder){
    	_conn=builder._conn;
		_schemaName = builder._schemaName;
		_tableName=builder._tableName;
		_input=builder._input;
		_response=builder._response;
		_loadParms=builder._loadParms;
		_readParms=builder._readParms;
		_logger=builder._logger!=null?builder._logger:LOGGER;
		
		// validate required parameters
		validateParm(Parms.CONNECTION, _conn);
//		validateParm(Parms.SCHEMA_NAME, _schemaName);
		validateParm(Parms.TABLE_NAME, _tableName);
		validateParm(Parms.INPUT_STREAM, _input);
		validateParm(Parms.RESPONSE_STREAM, _response);
			
		if (_readParms==null){	// default is delimited
			_readParms = new ReadParmsDelimitedAPI.Builder().build();
		} 
		
		if (_loadParms==null){	// default is jdbc batch
			_loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
				.build();	
		}
		
		if (LoadServicesRegistry.isServiceRegistered(_loadParms.getMethod().toString())){
			
			_service = LoadServicesRegistry.getService(_loadParms.getMethod().toString());
			
			ExitCode exitCode = _service.start(_conn,
		    		_schemaName, _tableName,
		    		_input, _response,
		    		_loadParms, _readParms,
		    		_logger);
			if (exitCode == ExitCode.EXIT_SEVERE){
				String msg = LoadResources.getString(LoadResources.LOAD_START_ERROR);
				_logger.log(Level.SEVERE,msg);
				throw new IllegalStateException(msg);
			}
		} else {
			String msg = LoadResources.format(LoadResources.INVALID_LOAD_PARM,new Object[] {Parms.LOAD_PARMS.toString()}) + " "
			+ LoadResources.getString(LoadResources.LOAD_START_ERROR);
			_logger.log(Level.SEVERE,msg);
			throw new IllegalStateException(msg);
		}	
		
    }
    
    private void validateParm(Enum<Parms> parm, Object parmValue){
    	if (parmValue == null || 
    			(parmValue instanceof String && ((String)parmValue).length() == 0)){
    		String msg = LoadResources.format(LoadResources.INVALID_LOAD_PARM,new Object[] {parm.toString()}) + " "
			+ LoadResources.getString(LoadResources.LOAD_START_ERROR);
			_logger.log(Level.SEVERE,msg);
    		throw new IllegalStateException(msg);
     	}
    }
    
    // TODO ... make it possible for batch loads to be interruptable
    // and report progress possibly with a load provider

    /**
     * Loads the data from the input stream into the table.  
     * 
     * Writes messages and bad data to the response stream
     * 
     * @return ExitCode indicates the summary status of the load
     */
    public ExitCode load(){
    	return _service!=null?_service.load():ExitCode.EXIT_SEVERE;
    }
 

}
