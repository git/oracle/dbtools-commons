/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.loadservice;

import java.util.Locale;
import java.util.Map;



/**
* Values for the options to be used for a load done using the LoadAPI.
* An optional LoadParmsAPI object can be created to customize the load.
*/ 


public class LoadParmsAPI {
	
	public final static int DEFAULT_ERRORS = 50;
    public final static String DEFAULT_ENCODING = "UTF8"; //$NON-NLS-1$;
	public final static int UNLIMITED = -1;
	public static enum RESPONSE_FORMAT{RESPONSE_RAW, RESPONSE_SQL};
	public static enum DELETE_ROWS{FALSE,TRUE,TRUNCATE};
	public static int DEFAULT_BATCH_ROWS = 50;
	public static int DEFAULT_BATCHES_PER_COMMIT = 10;
	public static enum SERVICE_METHOD{JDBC_BATCH/*,SCRIPT,SQLLOADER,STAGING_EXTERNAL_TABLE*/};
	// Convenience date formats
	public static final String LOAD_TIMESTAMP_TZ_FORMAT = "YYYY-MM-DD HH24:MI:SS.FF TZH:TZM";
	public static final String LOAD_TIMESTAMP_TZR_FORMAT = "YYYY-MM-DD HH24:MI:SS.FF TZR";
	public static final String LOAD_TIMESTAMP_FORMAT = "YYYY-MM-DD HH24:MI:SS.FF";
	public static final String LOAD_DATE_FORMAT = "YYYY-MM-DD";

	private static enum PARMS {
		SERVICE_METHOD,
		DELETE_ROWS,
		UNKNOWN_COLUMNS_FAIL,
		MAP_COLUMN_NAMES,
		ERRORS,
		ERRORS_MAX,
		BATCH_ROWS,
		BATCH_MAX,
		BATCHES_PER_COMMIT,
		DO_COMMIT,
		RESPONSE_FORMAT,
		RESPONSE_ENCODING,
		RESPONSE_LOCALES,
		LOCALE,
		DATE_FORMAT,
		TIMESTAMP_FORMAT,
		TIMESTAMPTZ_FORMAT
	}
	
	
	private SERVICE_METHOD _serviceMethod;
	private DELETE_ROWS _deleteRows;
	private boolean _isUnknownColumnsFail;
	private Map <String,String> _aliasToColumnMap;
	private int _errors;
	private int _errorsMax;
	private int _batchRows;
	@SuppressWarnings("unused")
	private int _batchMax;
	private int _batchesPerCommit;
	private boolean _isDoCommit;
	private RESPONSE_FORMAT _responseFormat;
	private Iterable<Locale> _responseLocales;
	private String _responseEncoding;
	private Locale _locale;
	private String _dateFormat;
	private String _timestampFormat;
	private String _timestampTZFormat;
	
	/**
	 * Builder class for constructing and validating LoadParmsAPI.   
	 * @author jscapicc
	 *
	 */
	public static class Builder {
    	// required parameters
		private SERVICE_METHOD _serviceMethod;
    	// optional parameters
    	private boolean _isUnknownColumnsFail=true;
    	private Map <String,String> _aliasToColumnMap;
    	private DELETE_ROWS _deleteRows=DELETE_ROWS.FALSE;
    	private int _errors=DEFAULT_ERRORS;
    	private int _errorsMax=UNLIMITED;
    	private int _batchRows=DEFAULT_BATCH_ROWS;
    	private int _batchMax=UNLIMITED;
    	private int _batchesPerCommit=DEFAULT_BATCHES_PER_COMMIT;
    	private boolean _isDoCommit=true;
    	private RESPONSE_FORMAT _responseFormat = RESPONSE_FORMAT.RESPONSE_RAW;
    	private String _responseEncoding=DEFAULT_ENCODING;
    	private Iterable<Locale> _responseLocales;
    	private Locale _locale;
    	private String _dateFormat=""; //LOAD_DATE_FORMAT;
    	private String _timestampFormat=""; //LOAD_TIMESTAMP_FORMAT;
    	private String _timestampTZFormat=""; //LOAD_TIMESTAMP_TZR_FORMAT;//LOAD_TIMESTAMP_TZ_FORMAT;
    
    	/**
    	 * Builder constructor takes the required parameters.
    	 * Use other public methods as needed to set optional parameters.
    	 * 
    	 * LoadParmsAPI constructor will use this object on build()
    	 */
    	public Builder (SERVICE_METHOD serviceMethod){
    		_serviceMethod=serviceMethod;
    	}
    	/**
    	 *  Indicates if table data should have rows deleted before the load.
    	 *  False (default) indicates that the table will not be deleted before the load.
		 *  True indicates the table data will be deleted with DELETE sql/
		 *  Truncate indicates the table data will be deleted with TRUNCATE sql. 
    	 *  @param truncate true indicates truncate should be done fefore the load.
    	 *  @return Builder  
		 */
    	public Builder deleteRows(DELETE_ROWS deleteRows){
    		_deleteRows=deleteRows;
    		return this;
    	}
    	
    	/**
    	 *  Set action for the condition where columns are found in the input stream that
    	 *  are not defined on the table.
    	 *    
    	 *  The action can be to proceed with the load, but ignore these columns or
    	 *   to terminate the load. 
    	 *   
    	 *  The default is to terminate the load when corresponding columns are not found for
    	 *  the table.
    	 *  
    	 *  @param unknownColumnsFail true indicates that the load wil be terminate, false
    	 *  	indicates that the unknown column data will be ignored.
    	 *  @return Builder
		 */
    	public Builder unknownColumnsFail(boolean isUnknownColumnsFail){
    		_isUnknownColumnsFail=isUnknownColumnsFail;
    		return this;
    	}
    	
    	/**
    	 *  Save the map for mapping aliases to actual column names.
    	 *    
    	 *  The feature handles the mapping of aliases (for REST which are usually
    	 *  in lowercase) to the actual column names which are usually, but now always,
    	 *  in upper case.
    	 *  
    	 *  If mapColumnNames is specified, the column names in the header row must be
    	 *  be the alias name which must also be the key for an entry in the map.
    	 *   
    	 *  After the header row is read, the alias to actual column name is
    	 *  done. 
 		 *  
 		 *  The unknownColumnsFail option directs the handling of cases where
 		 *  the alias is not found in the aliasToColumnMap or the column name value
 		 *  is not defined for the column.
    	 *  
    	 *  @param aliasToColumnMap a map containing the columns included
    	 *  	in the batch load.  The alias is the key for the elements.
    	 *  @return Builder
		 */
    	public Builder mapColumnNames(Map <String,String> aliasToColumnMap){
    		_aliasToColumnMap = aliasToColumnMap;
    		return this;
    	}
    	
       	/**
    	 *  Sets the user option used to limit the number of errors.  
    	 *  If the number of errors exceeds the value specified for errorsMax (the service option)
    	 *   or by errors (the user option), then the load is terminated. 
    	 *   
    	 *   To permit no errors at all, set errors=0. 
    	 *   To indicate that all errors be allowed (up to errorsMax value), specify UNLIMITED ( -1) for errors.
		 *   
		 *   @param errors indicates the number of errors to allow before terminating the load
		 *  @return Builder
		 */
		
    	public Builder errors(int errors){
    		_errors=errors;
    		return this;
    	}
    	
    	/**
    	 *  ErrorsMax is a service option used to limit the number of errors allowed by users.  It intended
    	 *  as an option for the service provider and not to be exposed as a user option.
    	 *  If the number of errors exceeds the value specified for errorsMax (the service option)
    	 *  or by errors (the user option), then the load is terminated. 
    	 *   
    	 *  To permit no errors at all, set errorsMax=0. To indicate that all errors be allowed, specify UNLIMITED ( -1) for errorsMax.
    	 *  
    	 *  @param errorsMax indicates the absolute max for the number of errors allowed
    	 *  @return Builder
		 */
    	public Builder errorsMax(int errorsMax){
    		_errorsMax=errorsMax;
    		return this;
    	}
    	
    	/**
    	 * Set the number of rows in each batch to send to the database. 
    	 * to the database.  
    	 * 
    	 * The default is 50 rows per batch.
     	 * 
     	 * @param batchRows indicates the number of rows in each batch.
    	 * @return Builder
    	 */
    	public Builder batchRows(int batchRows){
    		_batchRows=batchRows;
    		return this;
    	}
    	
    	/**
    	 *  batchMax is a service option used to limit the rows allowed in a batch.  It is intended
    	 *  as an option for the service provider and not to be exposed as a user option.
    	 *  If the batchRows exceeds the value specified for batchMax (the service option),
    	 *  then the batchRows is set to batchMax. 
    	 *   
    	 *  To allow any number of rows in a batch, specify UNLIMITED ( -1) for batchMax.
    	 *  
    	 *  @param batchMax indicates the absolute max for the number of rows per batch
    	 *  @return Builder
		 */
    	public Builder batchMax(int batchMax){
    		_batchMax=batchMax;
    		return this;
    	}
    	/**
    	 * Set the frequency for commits. Optional commit points can be set after a batch is sent 
    	 * to the database.  
    	 * 
    	 * The default is every 10 batches. 0 indicates commit deferred to the end of the load.
     	 * 
     	 * @param batchesPerCommit indicates the number of batches processed before a commit is done.
    	 * @return Builder
    	 */
    	public Builder batchesPerCommit(int batchesPerCommit){
    		_batchesPerCommit=batchesPerCommit;
    		return this;
    	}
    	
    	/**
    	 *  Set flag for committing.
    	 *    
    	 *  DoCommit must be set to true for any commit to be done during the load.   
    	 *   
    	 *  The default is doCommit = true;
    	 *  
    	 *  If doCommit = true, commits will be issued according to the batchesPerCommit parameter
    	 *  
    	 *  @param autocommit true indicates that the load will issue commits, false
    	 *  	indicates that no commits will be done during the load.
    	 *  @return Builder
		 */
    	public Builder doCommit(boolean isDoCommit){
    		_isDoCommit=isDoCommit;
    		return this;
    	}
    	/**
    	 * Set the format for response stream.  This format determines how messages and bad
    	 * data will be formatted.    
    	 * @param responseFormat specifies the RESPONSE_FORMAT for the load.
    	 * @return Builder
    	 */
    	public Builder responseFormat(RESPONSE_FORMAT responseFormat){
    		_responseFormat=responseFormat;
    		return this;
    	}
    	/**
    	 * Set the encoding for the response stream.  
    	 * @param responseEncoding
    	 * @return Builder
    	 */
    	public Builder responseEncoding(String responseEncoding){
    		_responseEncoding=responseEncoding;
    		return this;
    	}
    	/**
    	 * Set the list of preferred locales for messages translations written to the response stream.
    	 * If no list is provided the default locale is used.
    	 * @param responseLocales specifies the list of locales for translating response messages.
    	 * @return Builder
    	 */
    	public Builder responseLocales(Iterable <Locale> responseLocales){
    		_responseLocales=responseLocales;
    		return this;
    	}
    	
    	/**
    	 * Set the locale for locale sensitive datatypes.  This format is used when
    	 * converting input data to columns of type data.
    	 * @param locale
    	 * @return Builder
    	 */
    	public Builder locale(Locale locale){
    		_locale=locale;
    		return this;
    	}
       	/**
    	 * Set the format mask for the date datatype.  This format is used when
    	 * converting input data to columns of type date.
    	 * @param dateFormat
    	 * @return Builder
    	 */
    	public Builder dateFormat(String dateFormat){
    		_dateFormat=dateFormat;
    		return this;
    	}
       	/**
    	 * Set the format mask for the time stamp datatype.  This format is used when
    	 * converting input data to columns of type time stamp.
    	 * @param timestampFormat
    	 * @return Builder
    	 */
    	public Builder timestampFormat(String timestampFormat){
    		_timestampFormat=timestampFormat;
    		return this;
    	}
    	/**
    	 * Set the format mask for the time stamp time zone datatype.  This format is used when
    	 * converting input data to columns of type time stamp time zone.
    	 * @param timestampTZFormat
    	 * @return Builder
    	 */
    	public Builder timestampTZFormat(String timestampTZFormat){
    		_timestampTZFormat=timestampTZFormat;
    		return this;
    	}
    	/**
    	 * Build LoadParmsAPI using options specified in this object.
    	 * @return LoadParmsAPI object
    	 */
    	public LoadParmsAPI build(){
    		return new LoadParmsAPI(this);
    	}
    	
    }
	
	/**
	 * Single Constructor for the LoadParms API taking only the Builder
	 * 
	 * Throws illegalArgumentExeption if the parameters are invalid and the object
	 * cannot be constructed.
	 */
	public LoadParmsAPI(Builder builder) {
		_serviceMethod=builder._serviceMethod;
		if (!LoadServicesRegistry.isServiceRegistered(_serviceMethod.toString())){
			throw new IllegalArgumentException();
		}
		_deleteRows=builder._deleteRows;
		_isUnknownColumnsFail=builder._isUnknownColumnsFail;
		_aliasToColumnMap=builder._aliasToColumnMap;
		_errors=builder._errors;
		_errorsMax=builder._errorsMax;
		_batchRows=builder._batchMax > 0 && builder._batchRows>builder._batchMax?builder._batchMax:builder._batchRows;
		_batchesPerCommit=builder._batchesPerCommit;
		_isDoCommit=builder._isDoCommit;
		_responseFormat=builder._responseFormat;
		_responseEncoding=builder._responseEncoding;
		_responseLocales=builder._responseLocales;  // if null, standard message handling
		_locale=builder._locale;
		_dateFormat=notNull(builder._dateFormat);
		_timestampFormat=notNull(builder._timestampFormat);
		_timestampTZFormat=notNull(builder._timestampTZFormat);
		
		// validate required parameters
		validateParm(PARMS.SERVICE_METHOD, _serviceMethod);
		
	}

	private void validateParm(Enum<PARMS> parm, Object parmValue){
    	if (parmValue == null || 
    			(parmValue instanceof String && ((String)parmValue).length() == 0)){
    		throw new IllegalStateException(parm.toString());
    	}
    }
	
	private String notNull(String string){
		return (string != null)? string: "";
	}
	
	public SERVICE_METHOD getMethod(){
		return _serviceMethod;
	}
	
	/**
	 * getDeleteRows
	 * @return enum indicating if and how rows should be deleted before the load.  
	 * 			FALSE indicates rows will not be deleted before the load
	 * 	        TRUE indicates rows will be deleted before the load using DELETE sql
	 *          TRUNCATE indicates rows will be deleted before the load using TRUNCATE sql
	 */
	public DELETE_ROWS getDeleteRows (){
		return _deleteRows;
	}

	/**
	 * isUnknownColumnsFail
	 * If any columns in the stream are not defined for the table, 
	 * the load will be terminated if unknown columns fail is true.  
	 * Otherewise, they will be excluded from the load.
	 *
	 * @return Returns true if the load is terminated when unknown columns are found
	 */
	public boolean isUnknownColumnsFail (){
		return _isUnknownColumnsFail;
	}
	
	/**
	 * getAliasToColumnMap
	 * get the map containing the alias to column name mappings. 
	 * The Key of the element is the alias for the column..  
	 * When alias to column name mapping is sed, the map contains elements
	 * whose key is the alias and value is the real column name.
	 * 
	 *
	 * @return Returns the alias to column name map or 
	 * 			null if aliases are not used.
	 */
	public Map <String,String> getAliasToColumnMap (){
		return _aliasToColumnMap;
	}
	
	
	/**
	 * getErrors
	 * @return The user specified number of errors to allow before aborting. 
	 */
	public int getErrors (){
		return _errors;
	}

	/**
	 * getErrorsMax
	 * @return The service provider specified number of errors to allow before aborting. 
	 */
	public int getErrorsMax (){
		return _errorsMax;
	}
	
	/**
	 * isErrorsLimited
	 * If Errors or ErrorsMax is greater than 0, the number of errors allowed
	 * To permitting no errors at all, ERRORS=0. UNLIMITED(-1) indicates no limit.
	*/
	public boolean isErrorsLimited(){
		return _errors > 0 || _errorsMax >0;
	}
	
	/**
	 * getErrorsLimit
	 * Return the lesser of Errors or ErrorsMax.
	*/
	public int getErrorsLimit(){
		return _errors<_errorsMax || _errorsMax == UNLIMITED?_errors:_errorsMax;
	}
	
	/**
	 * getBatchRows
	 * @return The number of rows in each insert batch to send to the database.
	 */
	public final int getBatchRows (){
		return _batchRows;
	}

	/**
	 * getBatchesPerCommit
	 * @return The number of rows in each insert batch to send to the database.
	 */
	public int getBatchesPerCommit (){
		return _batchesPerCommit;
	}
	
	/**
	 * isDoCommit
	 * Returns the do commit flag.  True indicates commits will be executed by the load service 
	 * according the the rule specified by batchesPerCommit.
	*/
	public boolean isDoCommit(){
		return _isDoCommit;
	}
	
	/**
	 * getResponseFormat
	 * @return The format that is used for the response.
	 */
	public RESPONSE_FORMAT getResponseFormat (){
		return _responseFormat;
	}
	/**
	 * isResponseFormatRaw
	 * @return true if the Response Format it RAW;
	 */
	public boolean isResponseFormatRaw(){
		return _responseFormat==RESPONSE_FORMAT.RESPONSE_RAW;
	}
	/**
	 * isResponseFormatSql
	 * @return true if the Response Format it SQL;
	 */
	public boolean isResponseFormatSql(){
		return _responseFormat==RESPONSE_FORMAT.RESPONSE_SQL;
	}
	
	/**
	 * getResponseEncoding
	 * @return the encoding selected
	 */
	public String getResponseEncoding(){
		return _responseEncoding;
	}
	
	/**
	 * getResponseLocales
	 * @return the list of preferred locales for messages written to the response stream
	 * 
	 */
	public Iterable<Locale> getResponseLocales(){
		return _responseLocales;
	}

	/**
	 * getLocale
	 * @return the Locale
	 */
	public Locale getLocale(){
		
		return _locale;
	}
	
	/**
	 * getDateFormat
	 * @return the date format
	 */
	public String getDataFormat(){
		
		return _dateFormat;
	}
	
	/**
	 * getTimestampFormat
	 * @return the timestamp format
	 */
	public String getTimestampFormat(){
		
		return _timestampFormat;
	}
	
	/**
	 * getTimestampTZFormat
	 * @return the timestampTZ format
	 */
	public String getTimestampTZFormat(){
		
		return _timestampTZFormat;
	}
	
}
