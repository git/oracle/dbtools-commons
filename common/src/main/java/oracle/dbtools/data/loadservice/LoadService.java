/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.loadservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.data.common.TranslatableMessage;
import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.data.model.GenericTable;
import oracle.dbtools.data.model.Table;
import oracle.dbtools.data.readservice.ReadParmsAPI;
import oracle.dbtools.data.readservice.ReadParmsDelimitedAPI;
import oracle.dbtools.data.readservice.ReadService;
import oracle.dbtools.data.readservice.ReadServiceDelimited;
import oracle.dbtools.data.loadservice.ExitCode;

/**
 * The LoadService class is the service class for supporting the LoadAPI.  
 * 
 * It is an abstract class containing the Service Provider Interface for data load. 
 * 
 * To implement a load service provider, extend this class and register provider with LoadServicesRegistry.  
 * See LoadJdbcBatch.
 * 
 * @author Joyce Scapicchio
 * 
 */
public abstract class LoadService {
	
    protected final String ENCODING = "UTF-8"; //$NON-NLS-1$
    protected final String LINE_TERM = "\n"; //$NON-NLS-1$;

	private final String INFO_PREFIX_SQL="--";
	private final String ERROR_PREFIX_SQL="--";
	private String INFO_PREFIX_RAW;  
	private String ERROR_PREFIX_RAW;
   
	// input parameters
	private Connection _conn;
	private String _schemaName;
	private String _tableName;
	private OutputStreamWriter _responseWriter;  
	private LoadParmsAPI _loadParms;
	private ReadParmsAPI _readerParms;
	private Logger LOGGER = Logger.getLogger(getClass().getName()); // default logger
    
	private boolean CANCELLED;
	private boolean _connLocked=false;
	private boolean _isConnAutoCommit;  // is the connection set to automatically commit;
											  // if true, we must shut this off.	
	private boolean _isDoCommit;			  	  // Does the caller want the load service to do commits:

	private ReadService _reader;
    // Our model:
	private Table _table;

	private int _errorRowsCount;
	private int _processedRowsCount;
	
	private ExitCode _exitCode = ExitCode.EXIT_SUCCESS;
	private static final long START_TIME = System.nanoTime();

	private static String formatInterval(final long l)
    {
        final long hr = TimeUnit.NANOSECONDS.toHours(l);
        final long min = TimeUnit.NANOSECONDS.toMinutes(l - TimeUnit.HOURS.toNanos(hr));
        final long sec = TimeUnit.NANOSECONDS.toSeconds(l - TimeUnit.HOURS.toNanos(hr) - TimeUnit.MINUTES.toNanos(min));
        final long ms = TimeUnit.NANOSECONDS.toMillis(l - TimeUnit.HOURS.toNanos(hr) - TimeUnit.MINUTES.toNanos(min) - TimeUnit.SECONDS.toNanos(sec));
        return String.format("%02d:%02d:%02d.%03d", hr, min, sec, ms);
    }

	ExitCode start(Connection conn, 
			String schemaName, String tableName,
			InputStream input, OutputStream response,
			LoadParmsAPI loadParms, ReadParmsAPI readerParms,
			final Logger logger){


		_conn=conn;
		_schemaName=schemaName;
		_tableName=tableName;
		_loadParms=loadParms;
		_readerParms=readerParms;
		LOGGER=logger;
		
		INFO_PREFIX_RAW="#" + translate(LoadResources.INFO) + " ";
		ERROR_PREFIX_RAW="#" + translate(LoadResources.ERROR) + " ";

		LOGGER.log(Level.INFO,LoadResources.getString(LoadResources.LOAD_START));
		
		_isDoCommit = getLoadParms().isDoCommit();

		try {
			_responseWriter= new OutputStreamWriter(response, _loadParms.getResponseEncoding());

			if (readerParms != null && readerParms instanceof ReadParmsDelimitedAPI){
				_reader = new ReadServiceDelimited(input, (ReadParmsDelimitedAPI)readerParms,loadParms.getResponseLocales());
			} else {
				// TODO move this check to api
				String msg = translate(LoadResources.READER_START_ERR,new Object[] {readerParms.getServiceType().toString()});
				throw new DataLoadException (msg, ExitCode.EXIT_SEVERE);
			}

			initConnection();

			// Creating the Generic Table using this constructor
			// ensures that the table exists and if so then builds the Columns.
			// Build a Column for each with column name, datatype info and nullable 
			// column in the table.  This is the datamodel for the load.
			GenericTable genericTable = new GenericTable(getConnection(), getSchemaName(), getTableName());
			_table = genericTable.getTable();
			if (_table == null){
				// error "Table does not exist or is not accessible with the connection.", 
				// ExitCode.EXIT_SEVERE);

				String t = (_schemaName == null || _schemaName.length()==0 )? _tableName:_schemaName + "." + _tableName;
				String msg = translate(LoadResources.TABLE_NOT_EXIST,new Object[] { t});
				throw new DataLoadException(msg,ExitCode.EXIT_SEVERE);
			}	

		} catch (UnsupportedEncodingException e){  // responseWriter
			String msg = translate(LoadResources.RESPONSE_START_ERR);
			responseErrorMsg(new String[] {msg, e.getMessage()});
			LOGGER.log(Level.SEVERE,LoadResources.getString(LoadResources.RESPONSE_START_ERR) + " " + e.getMessage());
			_exitCode=ExitCode.EXIT_SEVERE;
		} catch (SQLException e){  // connection error
			String msg = translate(LoadResources.CONNECTION_ERR);
			responseErrorMsg(new String[] {msg, e.getMessage()});
			LOGGER.log(Level.SEVERE,msg + " " + e.getMessage());
			_exitCode=ExitCode.EXIT_SEVERE;
		} catch (DataLoadException e){  // readparms or table error
			responseErrorMsg(new String[] {e.getMessage()});
			LOGGER.log(Level.SEVERE,e.getMessage());
			_exitCode=e.getExitCode();
		}		

		if (_exitCode.equals(ExitCode.EXIT_SEVERE)){	// Cannot proceed with load
			signalCancel();
			finish();
		}
		return _exitCode;

	}
    
    abstract String getType();
    

    ExitCode load(){
    	
		doLoad();
		
		
		// Write the final stats/statistics
		//String[] lines = new String[3];
		String[] lines = new String[2];
		lines[0] = translate(LoadResources.STAT_ROWS_PROCESSED,  new Object[] { _processedRowsCount});
		lines[1] = translate(LoadResources.STAT_ROWS_ERROR, new Object[] { _errorRowsCount});
		// TODO, formatInterval is not correct.
		//		lines[2] = translate(LoadResources.STAT_ELAPSED_TIME, new Object[] { formatInterval(getElapsedTime()), getElapsedTime()});
		responseInfoMsg(lines);
		
    	finish();
    	return _exitCode;
    }
    
    // Main load processing which must be implemented by extenders.
    protected abstract void doLoad();
    
    // Methods to support extenders
    
    // get the connection
    protected final Connection getConnection(){
    	return _conn;
    } 
    
    /**
     * Translate and format message written to response stream.
     * If responseLocales were provided, these are used in order for obtaining the translated string
     * If responseLocales is null, standard message format is used.
     * 
     * Currently expecting all keys in LoadResources.properties.
     * @param messageId
     * @param args
     * @return
     */
    protected final String translate(String messageId, Object[] args){
    	if (_loadParms.getResponseLocales() == null){
    		return LoadResources.format(messageId, args);
    	}
		return (new TranslatableMessage(LoadResources.class, messageId, 
					LoadResources.format(messageId, args),  // default message, will use defaultLocale 
					args)) //$NON-NLS-1$
					.toString(_loadParms.getResponseLocales()); 
    }
    
    /**
     * Get message to be written to response stream.
     * If responseLocales were provided, these are used in order for obtaining the translated string
     * If responseLocales is null, standard message getString is used.
     * 
     * Currently expecting all keys in LoadResources.properties.
     * @param messageId
     * @param args
     * @return
     */
    protected final String translate(String messageId){
    	if (_loadParms.getResponseLocales() == null){
    		return LoadResources.getString(messageId);
    	}
		return (new TranslatableMessage(LoadResources.class, messageId, 
				LoadResources.getString(messageId))) //$NON-NLS-1$
					.toString(_loadParms.getResponseLocales()); 
    }
    
 // get the logger
    protected final Logger getLogger(){
    	return LOGGER;
    } 
    
    protected final String getSchemaName(){
    	return _schemaName;
    }
    
    protected final String getTableName(){
    	return _tableName;
    }
    
    // get the Table that is the table data model for the load
    protected final Table getTable(){
    	return _table;
    }
    
    // get the reader for the delimited stream
    protected final ReadService getReader(){
    	return _reader;
    }
    
    // get the reader parms 
    protected final ReadParmsAPI getReaderParms(){
    	return _readerParms;
    }
    
    // get the load parms
    protected final LoadParmsAPI getLoadParms(){
    	return _loadParms;
    }
    
    /**
     * Map a potential alias name to a column name
     * @param aliasOrColumnName
     * @return
     */
    protected String getColumnName(String aliasOrColumnName){
    	if (_loadParms.getAliasToColumnMap()==null || _loadParms.getAliasToColumnMap().size()==0){
    		return aliasOrColumnName;
    	}
    	String name = _loadParms.getAliasToColumnMap().get(aliasOrColumnName);
    	return name == null||name.length()==0?aliasOrColumnName:name;
    }
    
    /**
     * Get the elapsed time since start of Load Service in Milliseconds.
     * @return
     */
    protected long getElapsedTime(){
    	return (System.nanoTime() - START_TIME)/1000000;
    }

    // write a line to the response.  This appends a LINE_TERM character  
    protected final void responseWrite(String content){
    	try {
    		if (_responseWriter != null) {
    			_responseWriter.write(content+LINE_TERM);
    		}
    	} catch (IOException e){
    		LOGGER.log(Level.SEVERE, e.getMessage());	
    	}
    }
    
 // Add to the processed count
    protected final void addRowsProcessed(){
    	++_processedRowsCount;
    }
    
    // Add to the error count
    protected final void addRowsError(){
    	++_errorRowsCount;
    }
    
    protected void responseInfoMsg(String [] lines){
    	
    	String prefix = getLoadParms().isResponseFormatRaw()?
    			INFO_PREFIX_RAW:
    				INFO_PREFIX_SQL;

    	for (int i=0; i < lines.length; ++i){
    		responseWrite(prefix + lines[i]);
     	}
   	
    }
    
    protected void responseErrorMsg(String [] lines){
    	
    	String prefix = getLoadParms().isResponseFormatRaw()?
    			ERROR_PREFIX_RAW:
    				ERROR_PREFIX_SQL;
    	
    	for (int i=0; i < lines.length; ++i){
    		responseWrite(prefix + lines[i]);
     	}
   	
    }
       
    protected final boolean checkCanProceed(){
    	
    	if (_loadParms.isErrorsLimited() && 
    			_errorRowsCount > _loadParms.getErrorsLimit()){
     		CANCELLED = true;
     		_exitCode = ExitCode.EXIT_ERROR;
     	}
    	return !CANCELLED;
    	
    }
    
    protected final void setExitCodeHighest(ExitCode exitCode){
    	if (exitCode.getSeverity()>_exitCode.getSeverity()){
    		_exitCode=exitCode;
    	}	
    }
      
    protected final boolean isCancelled(){
    	return CANCELLED;
    }

    /**
     * cancel
     * 
     * @see oracle.dbtools.raptor.data.core.Cancelable#cancel()
     */
    public void signalCancel() {
    	CANCELLED = true;
    }
  
    // private methods
    
    private void initConnection() throws SQLException {

    	_isConnAutoCommit= _conn.getAutoCommit();

    	_connLocked = LockManager.lock(_conn);

    	if (!_connLocked) {
    		_exitCode = ExitCode.EXIT_SEVERE;
    		signalCancel();
    	} else {
    		// if got the lock and doing insert exec 
    		// suspend autocommit during import
    		if (_isConnAutoCommit){
    			_conn.setAutoCommit(false);
    		}	
    	}
    	//_connUser = _conn.getMetaData().getUserName();


    }
    
    protected void finish(){
    	responseWrite(_exitCode.toStringTranslated(_loadParms.getResponseLocales()));
    	
    	if (CANCELLED){
    		rollbackConn();
    	} else {
    		commitConn();
    	}
    	
    	resetConn();  // don't close it, but reset the autocommit if necessary

// We just read or write, caller creates and closes the stream.    	
    	if (_reader != null){
    		_reader.close();
    	}
    	try {
    		if (_responseWriter != null){
    			_responseWriter.close();
    		} 
    	} catch (Exception e) {  
    		LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
    	}
    	
    	
    }
 
    private void rollbackConn(){
    	
    	try {
    		if (!_conn.isClosed() && _connLocked){
    			_conn.rollback();
     		}	
    	} catch (SQLException e){
    		LOGGER.log(Level.WARNING,
    				e.getStackTrace()[0].toString(),
    				e);
    		_exitCode=ExitCode.EXIT_SEVERE;
    	}

    	signalCancel();
    }

    protected void commitConn(){
    	try {	
    		if (_connLocked && _isDoCommit){
    			_conn.commit();
    		}	
    	}	catch (Exception e) {
    		//TODO Log error
    		//_msg = _taskFullName + ". ";
    		//_msg+= BaseLoadersArb.getString(BaseLoadersArb.TASK_FAILED);
    		LOGGER.log(Level.WARNING,
    				e.getStackTrace()[0].toString(),
    				e);
    		_exitCode=ExitCode.EXIT_SEVERE;	
    		signalCancel();
    	}
    }

    private void resetConn(){
    	try {
    		if (_isConnAutoCommit && _connLocked){
    			_conn.setAutoCommit(_isConnAutoCommit); // restore autocommit setting
    			_connLocked=false;
    			LockManager.unlock(_conn);
    		} else if (_connLocked){
    			LockManager.unlock(_conn);
    		}
    	} catch (SQLException e){
    		LOGGER.log(Level.WARNING,
    				e.getStackTrace()[0].toString(),
    				e);
    		_exitCode=ExitCode.EXIT_SEVERE;
    		signalCancel();
    	}
    }	

 
    

 }
