/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.loadservice;


import java.io.IOException;
import java.sql.BatchUpdateException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import oracle.dbtools.data.model.Column;
import oracle.dbtools.data.formattors.DataTypeFormatterRegistry;
import oracle.dbtools.data.formattors.ITypeFormattor;
import oracle.dbtools.data.readservice.DataFormatException;
import oracle.dbtools.data.loadservice.LoadResources;
import oracle.dbtools.data.loadservice.ExitCode;
import oracle.dbtools.data.loadservice.LoadParmsAPI.RESPONSE_FORMAT;
import oracle.jdbc.OraclePreparedStatement;

/**
 * The JdbcBatchLoad class handles loading data via batch insert sql.
 * 
 * @author Joyce Scapicchio
 * 
 */
public class LoadServiceJdbcBatch extends LoadService {
	
	// The SERVICE_TYPE in the Parms used must match the SERVICE_TYPE in the service.
	public static final String SERVICE_TYPE = "JDBC_BATCH"; //$NON-NLS-1$  
	private String _insertForErrSql;

    Column[] _loadCols;
    boolean _batchErrInit = false;
    
   
    public String getType(){
    	return SERVICE_TYPE;
    }
    
    protected void doLoad(){
    	getLogger().log(Level.INFO,"load start");

    	Object[] row;
    	int batchSize= (getLoadParms()).getBatchRows();
    	ArrayList<Object> batchRowsList = new ArrayList<Object>(batchSize);

    	int[] updateCounts;
    	int commitBatch=getLoadParms().getBatchesPerCommit();
    	
    	int batchCount = 0;
    	int fromRow = 1;
    	int toRow=0;
    	StringBuffer exceptionMessages = null;
    	boolean isStatementValid=false;


    	// Read the first row of the file and build the LoadFields.
    	// Ensure that the load fields are defined in the table. Reference
    	// the columns in the LoadFields.

    	// TODO _loadCols needs to be set, or maybe these methods should
    	// take the LoadFile instead...



    	try {

    		// set up to get the insert statements needed for the load
    		LoadSql loadSql = new LoadSql(getSchemaName(),getTableName(),getLoadParms().getLocale());
    		loadSql.useQuotedNames(true);
    		getReader().start();

    		if (!getReaderParms().isHeader()){
    			/*
    			// for now the header is required. 
    			getLogger().log(Level.WARNING,
    					LoadResources.getString(LoadResources.LOAD_CANCELLED + " " + LoadResources.getString(LoadResources.HEADER_REQUIRED)));
    			throw new DataLoadException(ExitCode.EXIT_SEVERE);
    			 */

    			row = new String[getTable().getColumnCount()];


    		} else {


    			// handle the header row
    			try {
    				//row = getReader().readline();
    				row = getReader().getColumnNames();
    			} catch (DataFormatException e) {
    				throw new DataLoadException(e.getMessage(), ExitCode.EXIT_SEVERE);
    			} 

    			if (row == null || row.length ==0){
    				throw new DataLoadException(translate(LoadResources.HEADER_NOT_FOUND), ExitCode.EXIT_SEVERE);
    			}
    		}	

    		_loadCols = new Column[row.length];

    		// Scan the header row and build the File Model.  There will be one
    		// LoadField built for each field in the file that we are able to load.

    		// Fields may be excluded if there is not a matching column on the database or
    		// if loading this datatype is not supported, but here we just handle
    		// the first case.

    		//    		LoadFile loadFile = new LoadFile();


    		for (int i=0; i<row.length;++i) {
    			//    			LoadField field = new LoadField();
    			//    			field.setName((String)row[i]);

    			
    			// When a header is present, this ensures that the column name in the file matches a real column.
    			// Or that it matches an expected alias as defined in the aliasToColumn map.
    			// When a header is not present, this ensures that the size of the row does not
    			// exceed the number of columns, unless isUnknownColumnsFail is not set
    			Column column = getReaderParms().isHeader()?
    					getTable().getColumnByName(getColumnName((String)row[i])):
    						getTable().getColumnByIndex(i);

    			if (column == null){
    				if (getLoadParms().isUnknownColumnsFail()){
    					throw new DataLoadException(translate(LoadResources.UNKNOWN_COL, new Object[] { (String)row[i]}),ExitCode.EXIT_SEVERE);
    				}
    				//    				field.isLoad(false);
    			} else {
    				column.setSourceColID(i);
    				if (column.getType().equals("DATE")){
    					column.setFormat(getLoadParms().getDataFormat());
    				} else if (column.getType().equals("TIMESTAMP")){
    					column.setFormat(getLoadParms().getTimestampFormat());
    				} else if (column.getType().equals("TIMESTAMP WITH TIME ZONE") ||
    						column.getType().equals("TIMESTAMP WITH LOCAL TIME ZONE")){
    					column.setFormat(getLoadParms().getTimestampTZFormat());
    				}	
    				//    				field.setName((String)row[i]);
    				//    				field.setColumn(column);
    				//    				field.isLoad(true);
    				//    				loadFile.addField(field);
    			}
    		}

    		
    		// Build a customized column load list (columns not being loaded removed.
    		int i=0;
    		for (Column col: getTable().getColumns()){
    			if (col.getSourceColID() != -1){
    				_loadCols[i++] = col;
    			} else {
    				if (!col.isNullable()){
    					throw new DataLoadException(translate(LoadResources.OMITTED_COLS), ExitCode.EXIT_SEVERE);
    				}
    			}
    		}

    		String sql="";
    		if (getLoadParms().getDeleteRows() != LoadParmsAPI.DELETE_ROWS.FALSE ){
    			if (getLoadParms().getDeleteRows() == LoadParmsAPI.DELETE_ROWS.TRUE ){
    				sql = loadSql.getDeleteStatement();
    			} else {
    				sql = loadSql.getTruncateStatement();
    			}	
    			PreparedStatement pstmt =  getConnection().prepareStatement(sql);			
    			pstmt.executeUpdate();
    		}
    		
    		// Create a prepared statement for the insert

    		sql = loadSql.getInsertStatement(_loadCols, row,LoadSql.INSERT_FORMAT.PREPARE);

    		if (getLoadParms().getResponseFormat() == RESPONSE_FORMAT.RESPONSE_SQL){
    			_insertForErrSql = loadSql.getInsertStatement(_loadCols, row,LoadSql.INSERT_FORMAT.ERROR);
    		}
    		
    		OraclePreparedStatement pstmt = (OraclePreparedStatement) getConnection().prepareStatement(sql);			
    		
    		while (checkCanProceed() && getReader().hasMoreRows() /*&& 
    				(!getLoadParms().isLimitRows() || rowsProcessed < getLoadParms().getLimitRows())*/) {
    			exceptionMessages = new StringBuffer();
    			StringBuffer rowData = new StringBuffer();	
    			try {
    				if (isCancelled()) {
    					break;
    				}
    				try{
    					row = getReader().readline();

    				}catch (DataFormatException e) {
    					exceptionMessages.append("--" + e.getMessage()+"\n"); //$NON-NLS-1$
    					row=e.getColumns();
    				}  

    				if (row != null) {// process only non empty rows
    					//		checkCanProceed();
    					//exceptionMessages.append(getBatchForInsert(_tableName, _loadCols, row, rowData, pstmt));
    					isStatementValid = getBatchForInsert(getTableName(), _loadCols, row, rowData, pstmt,exceptionMessages);
    					if (isStatementValid){
    						batchRowsList.add(rowData.toString()); 
    						++batchCount;
    						addRowsProcessed();
    					}	
    				}
    				
    				// If we had a statement exception handled by us
    				// Process the batch, then process our statement exception
    				if (batchCount == batchSize || (exceptionMessages.length()>0 && batchCount >0)) {
    					// Batch Processing starts here:
    					toRow = fromRow+batchCount-1;
    					//toRow = exceptionMessages.length()>0 && batchCount>2?
    					//		fromRow+batchCount-2:fromRow+batchCount-1;
    	    			// Debugging...
    					//String msg = LoadResources.format(LoadResources.LOAD_PROGRESS_INS2, new Object[] { ""+fromRow, ""+toRow});
    					//if (fromRow == toRow){
    					//	msg = LoadResources.format(LoadResources.LOAD_PROGRESS_INS, new Object[] { ""+fromRow});
    					//}

//    					checkCanProceed();
    					updateCounts = pstmt.executeBatch();
    					processUpdateCounts(updateCounts, batchCount, fromRow, batchRowsList);
    					batchCount=0;
    					fromRow=toRow+1;
    					toRow=fromRow;
    					batchRowsList.clear();
    					if (--commitBatch <1){
    	    				commitConn();
    	    				commitBatch = getLoadParms().getBatchesPerCommit();
    	    			}
    				}
    				if (exceptionMessages.length()>0){
    					batchRowsList.add(rowData.toString()); 
    					toRow=fromRow;
    					handleBatchUpdateException(new BatchUpdateException(exceptionMessages.toString(),null),1,fromRow,toRow,batchRowsList);
    					batchRowsList.clear();
    					toRow=++fromRow;
    				}
    			} catch (BatchUpdateException e) {
    				// Not all of the statements were successfully executed
    				// Come here b/c the pstm.executeBatch threw the exception.	
    				handleBatchUpdateException(e,batchCount,fromRow, toRow, batchRowsList);
    				batchCount=0;
    				fromRow=toRow+1;
    				batchRowsList.clear();
    				if (exceptionMessages!=null && exceptionMessages.length()>0){
    					batchRowsList.add(rowData.toString()); 
    					toRow=fromRow;
    					handleBatchUpdateException(new BatchUpdateException(exceptionMessages.toString(),null),1,fromRow,toRow,batchRowsList);
    					batchRowsList.clear();
    					toRow=++fromRow;
    				}
    			} 
    			
    		}  // end for loop

    		// do last batch
    		if (batchCount !=0){
    			toRow = fromRow+batchCount-1;
    			// Debugging...
    			//String msg = LoadResources.format(LoadResources.LOAD_PROGRESS_INS2, new Object[] { ""+fromRow, ""+toRow});
    			//if (batchCount ==1){
    			//	msg = LoadResources.format(LoadResources.LOAD_PROGRESS_INS, new Object[] { ""+toRow});
    			//} 
    			//    			super.checkCanProceed();  
    			//    			super.setMessage(msg);			        		
    			//    			outLine(msg);

    			//    			super.checkCanProceed();
    			updateCounts = pstmt.executeBatch();
    			processUpdateCounts(updateCounts, batchCount, fromRow, batchRowsList);
    		}
   		
    	} catch (BatchUpdateException e) {
    		// Not all of the statements were successfully executed
    		handleBatchUpdateException(e,batchCount,fromRow,toRow, batchRowsList);
    	} catch (SQLException e) {
    		setExitCodeHighest(ExitCode.EXIT_SEVERE);
    		getLogger().log(Level.WARNING, e.getMessage());
    		signalCancel();
    	} catch (DataLoadException e){
    		if (e.getMessage()!= null & e.getMessage().length()>0){
    			try {
    				responseErrorMsg(new String[] {e.getMessage()});
    			} catch (Exception e2){
    				
    			}
    		}
    		getLogger().log(Level.WARNING, e.getMessage());
    		setExitCodeHighest(e.getExitCode());
    		signalCancel();
    	} catch (Exception e){
    		getLogger().log(Level.WARNING, e.getMessage());
    		setExitCodeHighest(ExitCode.EXIT_SEVERE);
    		signalCancel();
    	}


    }

    //TODO need to convert DataLoadException to only use exit code

    
    private void handleBatchUpdateException(BatchUpdateException e, int batchCount, int fromRow, int toRow, ArrayList <Object> batchRowsList)
    {

    	//      	if (_createTableSql != null && _createTableSql.length()>0){
    	//      		outBad(_createTableSql.toString());
    	//      		_createTableSql=null;
    	//      	}
    	
    	setExitCodeHighest(ExitCode.EXIT_WARNING);
    	
    	try {
    		int[] updateCounts = e.getUpdateCounts();
    		// Either commit the successfully executed statements 
    		// or rollback the entire batch.  Let user decide.
    		

    		String[] lines = new String[2];

    		//String eMsg = e.getMessage().startsWith("--")?e.getMessage():"--"+e.getMessage(); //$NON-NLS-1$ $NON-NLS-1$
    		///lines[0] = LoadResources.format(LoadResources.LOAD_PROGRESS_INS_FAIL2, new Object[] { " "+fromRow+" ", " "+toRow+" "}); // + eMsg;
    		lines[0] = translate(LoadResources.LOAD_PROGRESS_INS_FAIL2, new Object[] { " "+fromRow+" ", " "+toRow+" "}); // + eMsg;
    		if (batchCount==1){
    			///lines[0] = LoadResources.format(LoadResources.LOAD_PROGRESS_INS_FAIL, new Object[] { " "+fromRow+" "}); // + eMsg;
    			lines[0] = translate(LoadResources.LOAD_PROGRESS_INS_FAIL, new Object[] { " "+fromRow+" "}); // + eMsg;
    		}
    		//lines[1] = eMsg.substring(0,eMsg.length()-1);  // get rid of unwanted /n at end of e.getMessage string
    		lines[1] = e.getMessage().substring(0,e.getMessage().length()-1);  // get rid of unwanted /n at end of e.getMessage string
    		
    		responseErrorMsg(lines);

    		getLogger().log(Level.INFO, lines[0]);
    		getLogger().log(Level.INFO, lines[1]);
    		// processUpdate Counts will write out the details of the data row errors
    		//
    		// Some databases will continue to execute after one fails.
    		// If so, updateCounts.length will equal the number of batched statements.
    		// If not, updateCounts.length will equal the number of successfully executed statements
    		// Give more details, if possible via processUpdateCounts
    		processUpdateCounts(updateCounts, batchCount, fromRow, batchRowsList);


    		//    	if (!_ignoreAllErrors){
    		//    		try {
    		//    			askContinueOnError(msgNoComments+"\n\n"+DataImportArb.getString(DataImportArb.TASK_PROGRESS_CONTINUE));
    		//    		} catch (Throwable e1) {
    		//    			Logger.getLogger(getClass().getName()).log(Level.WARNING,
    		//    					e1.getStackTrace()[0].toString(),
    		//    					e1);
    		//    			rollbackTask();
    		//    			requestCancel();
    		//   		} 


    		if (!checkCanProceed()){
    			signalCancel();
    		}	
    	
    	} catch (IOException ioe) {
    		getLogger().log(Level.WARNING, ioe.getStackTrace()[ 0 ].toString(), ioe);
    		setExitCodeHighest(ExitCode.EXIT_SEVERE);
    		signalCancel();
    	}
    	
    }

    private void processUpdateCounts(final int[] updateCounts, int batchCount, int fromRow, ArrayList <Object> batchRowsList) throws IOException {

    	// If the driver continues processing commands, 
    	// the array returned by this method will have as many 
    	// elements as there are commands in the batch; 
    	//  otherwise, it will contain an update count for 
    	// each command that executed successfully before 
    	// the BatchUpdateException was thrown. 
    	// However, if there are no no update counts 
    	// assume that the driver did not continue or we detected a format error 
    	// and write all the rows to the bad file.
    	
    	

    	if (updateCounts == null || updateCounts.length < 1 ) {
    		for (int i=0;i<batchCount;++i, ++fromRow){
    			responseWriteErrRow(fromRow,(String)batchRowsList.get(i));
    		}
    		return; 
    	}
    	
    	// here we are assuming that the driver may have continued and 
    	// only some may have been inserted.

    	boolean error = false;
    	for (int i = 0; i < updateCounts.length; i++, fromRow++) {
    		if (updateCounts[i] >= 0) {
    			// Successfully executed; the number represents number of affected rows
    			//  System.out.println("Statement inserted " + updateCounts[i] + " rows");
    		} else if (updateCounts[i] == Statement.SUCCESS_NO_INFO) {
    			// Successfully executed; number of affected rows not available
    			//	count++;
    		} else if (updateCounts[i] == Statement.EXECUTE_FAILED) {
    			// Failed to execute
    			if (!error){
    				error=true;
    			}
    			responseWriteErrRow(fromRow, (String)batchRowsList.get(i));
    		}
    	}
     	
    	// here we are assuming that the driver continued, 
    	// but the update rows array is only up to the point of the error...
    	// so all the remaining inserts should be put into the bad file.
    	if (updateCounts.length < batchCount ){

    		for (int i=updateCounts.length; i < batchCount; ++i, fromRow++){
    			responseWriteErrRow(fromRow, (String)batchRowsList.get(i));
    		}

    		return;
    	}
    	
    }
    
    
    private void responseWriteErrRow(int rowNumber, String row) throws IOException{
    	addRowsError();  // keeping count of the number of errors.
    	///String msg = LoadResources.format(LoadResources.ROW, new Object[] {rowNumber}); 
    	String msg = translate(LoadResources.ROW, new Object[] {rowNumber}); 
    	if (getLoadParms().isResponseFormatSql()){
    		responseWrite("--"+msg);
			responseWrite(_insertForErrSql+row);
		} else if (getLoadParms().isResponseFormatRaw()){
			///responseWrite("#"+LoadResources.getString(LoadResources.ERROR) + " " + msg);
			responseWrite("#"+translate(LoadResources.ERROR) + " " + msg);
			// should probably check for embedded ' and double them
			responseWrite(row.substring(1,row.length()-2));
		}
    }
    
    // returns true if statement successfully added to batch.
    private boolean getBatchForInsert
    (String tableName, Column[] cols,
    		Object[] row, StringBuffer rowData,
    		OraclePreparedStatement pstmt, StringBuffer exceptionMessages) 
    /*throws SQLException*/{
    	ArrayList<Object> orderedRowList = new ArrayList<Object>();

    	for (int i = 0; i < cols.length; i++) {
    		if (i < row.length) { // handle omitted fields at end of record  
    			orderedRowList.add(row[ cols[ i ].getSourceColID() ]);
    		}
    		else {
    			orderedRowList.add("");
    		}
    	}       
    	Object[] orderedRow = orderedRowList.toArray();
    	String delim="(";  // ResponseFormatSQL, not used for RAW

    	ITypeFormattor fmtor = null;
    	//StringBuffer exceptionMessages=new StringBuffer();

    	for (int i = 0; i < orderedRow.length; i++) {
    		fmtor = DataTypeFormatterRegistry.getInstance().getFormattor(cols[ i ].getType());

    		if (fmtor == null) // Assume character as default. This is ugly :(
    			fmtor = DataTypeFormatterRegistry.getInstance().getFormattor("VARCHAR2"); //$NON-NLS-1$

    		fmtor.setLocale(getLoadParms().getLocale());
    		fmtor.setConnection(getConnection());  // we may have cloned this, but base is ok for getting date fmt
    		try {
    			// Append the data for error line first for best chance at getting it
    				rowData.append(delim);
    				rowData.append(getLoadParms().isResponseFormatRaw()?orderedRow[ i ].toString():fmtor.formatData(orderedRow[ i ], cols[ i ]));

    			Object colData = fmtor.formatDataForJava(orderedRow[ i ], cols[ i ]);

    			// For most datatypes, colData is returned as a string.

    			if (colData != null && colData instanceof String) {
    				String colString = (String) colData;
    				if (colString == null || colString.length() ==0 || 
    						colString.equals("''") || colString.equals("NULL")){
    					//rowData.append("NULL");
    					pstmt.setNull(i+1, fmtor.getSqlType(cols[ i ].getType()));
    				}
    				else {
    					if (cols[i].getType().equals("NCHAR") || cols[i].getType().equals("NVARCHAR2")){
    						pstmt.setFormOfUse(i+1, OraclePreparedStatement.FORM_NCHAR);
    					}
    					pstmt.setString(i+1, colString);

    				}
    			}
    			// However, for some datatypes (DATE, TIMESTAMP, etc) it is
    			// returned as an object and a different format set is needed
    			// Even so, we always build the insert values in case of error
    			else {
    				//pstmt.setDate(i+1, (Date)colData);
    				// if colData is null, the insert will which is
    				// find b/c we'll generate the insert in the error file.
    				//rowData.append(fmtor.formatData(orderedRow[ i ], cols[ i ]));

    				pstmt.setObject(i+1, colData, fmtor.getSqlType(cols[ i ].getType()));

    				//String fmt = cols[i].getFormat();
    				//String data = (orderedRow[i]!=null) ? orderedRow[i].toString() : "NULL";
    				//rowData.append("to_date('" + orderedRow[i].toString() + "', '" + fmt + "')");

    			}
    		} catch (Exception e){
    			// we need to keep building the r0owData so that we can generate
    			// the insert in the error file
    			String details = e.getMessage()==null || e.getMessage().equals("null")?
    					LoadResources.getString(LoadResources.FMT_ERR):
    						e.getMessage();
     			exceptionMessages.append(cols[ i ].getName() + ": " + 
    					details + " ");
    		}


    		/*
            	else if (colData instanceof Date) {
            		//pstmt.setDate(i+1, (Date)colData);
            		pstmt.setObject(i+1, colData, fmtor.getSqlType(cols[ i ].getType()));
            		String fmt = cols[i].getFormat();
            		String data = (orderedRow[i]!=null) ? orderedRow[i].toString() : "NULL";
            		rowData.append("to_date('" + orderedRow[i].toString() + "', '" + fmt + "')");
            	}
            	else if (colData instanceof Timestamp) {
            		pstmt.setTimestamp(i+1, (Timestamp) colData);
            		String fmt = cols[i].getFormat();
            		String data = (orderedRow[i]!=null) ? orderedRow[i].toString() : "NULL";
            		rowData.append("to_timestamp('" + orderedRow[i].toString() + "', '" + fmt + "')");
            	}
    		 */
    		delim=",";
    	}
    	// Append the data for error line first for best chance at getting it
			rowData.append(");"); 
   	
    	if (exceptionMessages.length()==0){
    		try {
    			pstmt.addBatch();
    		} catch (Exception e){
    			// Pick up the final error for the row.
    			exceptionMessages.append(e.getMessage());
    		}	
    	}
    	return exceptionMessages.length()==0;
    }
 
}
