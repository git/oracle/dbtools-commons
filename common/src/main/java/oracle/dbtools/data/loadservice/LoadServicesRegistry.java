/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.loadservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Registry of data load service providers
 * @author Joyce.Scapicchio
 */
public class LoadServicesRegistry {
  private static Map<String, Class<? extends LoadService>> s_registry = new HashMap<String, Class<? extends LoadService>>();

  /**
   * Register a load service if you want to. We could do this from a third party extension.
   * @param clazz of Reader to register.
   */
  
  synchronized public static void registerService(final LoadService clazz) {
    s_registry.put(clazz.getType().toLowerCase(), clazz.getClass());
  }

  synchronized public static void unregisterService(final LoadService clazz) {
    s_registry.remove(clazz.getType().toLowerCase());
  }

  synchronized public static LoadService getService(final String type) {
    try {
      return (s_registry.get(type.toLowerCase())).newInstance();      
    } catch (InstantiationException e) {
      Logger.getLogger(LoadServicesRegistry.class.getName()).log(Level.SEVERE, "Unable to instantiate the Data Reader"); //$NON-NLS-1$
    } catch (IllegalAccessException e) {
      Logger.getLogger(LoadServicesRegistry.class.getName()).log(Level.SEVERE, "Unable to instantiate the Data Reader"); //$NON-NLS-1$
    }
    return null;
  }

  /**
   * Get the types supported.
   * @return the list of types supported.
   */
  synchronized public static List<String> getTypes() {
    List<String> types = new ArrayList<String>();
    types.addAll(s_registry.keySet());
    return types;
  }

  /**
   * Registered Load Service by default includes only <code>LoadJdbcBatch</code>
   * 
   */
  static {
    registerService(new LoadServiceJdbcBatch());
  }

  /**
   * Is a data reader for this type of data registered?
   * If it is not, we cant read it so, we will bail...
    */
  public static boolean isServiceRegistered(final String type) {
    if (s_registry.get(type.toLowerCase()) != null) {
      return true;
    } else {
      return false;
    }

  }
}
