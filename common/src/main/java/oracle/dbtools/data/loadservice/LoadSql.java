/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.loadservice;

import java.util.ArrayList;
import java.util.Locale;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.data.model.Column;
import oracle.dbtools.data.formattors.DataTypeFormatterRegistry;
import oracle.dbtools.data.formattors.ITypeFormattor;

/**
 * This class handles generation of truncate and insert sql in the various formats needed
 * by the data load services.
 * 
 * It generates simple sql using a StringBuffer. 
 * 
 * @author Joyce Scapicchio
 * 
 */
public class LoadSql {
	public static enum INSERT_FORMAT{SCRIPT,PREPARE,ERROR,COMMENT};
	
    private Locale _locale;
    private boolean _useQuotedNames = true;
    private String _schemaName;
    private String _tableName;
    
    
    public LoadSql(String schemaName, String tableName){
    	this(schemaName,tableName,null);
    }
    public LoadSql(String schemaName, String tableName, Locale locale){
    	_schemaName=schemaName;
    	_tableName=tableName;
    	_locale=locale;
    }
    
    /**
     * Indicate whethere names are to be generated with double quotes.  True indicates
     * double quotes will be added.
     * @param useQuotedNames
     */
    public void useQuotedNames(boolean useQuotedNames){
    	_useQuotedNames=useQuotedNames;
    }
    public void setSchemaName(String schemaName){
    	_schemaName=schemaName;
    }
    public void setTableName(String tableName){
    	_tableName=tableName;
    }
     
    public String getDeleteStatement(){
       	return ("delete from " + addUser(addTableName(_tableName)));  //$NON-NLS-1$
    }
    
    public String getTruncateStatement(){
    	return ("truncate table " + addUser(addTableName(_tableName)));  //$NON-NLS-1$
    }

    /**
     * getInsertStatement generates an insert statement using columns and row data in the
     * format specified.
     * 
     * @param cols
     * @param row
     * @param insertFormat
     * @return a string containing the generated Insert statement.
     */
    public String getInsertStatement(Column[] cols, Object[] row, INSERT_FORMAT insertFormat) {
    	boolean isPrepare=false;
    	boolean isError=false;
    	StringBuffer insert = new StringBuffer();
    	if (insertFormat==INSERT_FORMAT.SCRIPT) {
    	}
    	else if (insertFormat==INSERT_FORMAT.PREPARE) {
    		isPrepare=true;
    	}
    	else if (insertFormat==INSERT_FORMAT.ERROR) {
    		isError=true;
    	}
    	else {
    		insert.append("--" ); //$NON-NLS-1$
    	}

     	insert.append("INSERT INTO "); //$NON-NLS-1$
    	insert.append(addUser(addTableName(_tableName)));
    	insert.append( "("); //$NON-NLS-1$
    	
    	ArrayList<Object> orderedRowList = new ArrayList<Object>();

    	for (int i = 0; i < cols.length; i++) {
    		if (i < row.length) { // handle omitted fields at end of record  
    			orderedRowList.add(row[ cols[ i ].getSourceColID() ]);
    		}
    		else {
    			orderedRowList.add("");
    		}
    	}       
    	Object[] orderedRow = orderedRowList.toArray();

    	ITypeFormattor fmtor = null;
    	for (int i = 0; i < cols.length; i++){
    		if (i!=0){
    			insert.append(","); //$NON-NLS-1$
    		}
    		// Bug 8841693 - RC1:IMPORT DATA SHOULD NOT GENERATE 
    		//   CREATE TABLE WITH QUOTED COLUMN NAMES 
    		// For existing tables, generate quotes to reference actual name
    		// otherwise the user is required to specify the quotes on the column
    		// name for new tables and they should already be there.
    		if (_useQuotedNames){
    			insert.append(DBUtil.addDoubleQuote(cols[ i ].getName()));
    		}
    		else {
    			insert.append(cols[ i ].getName());
    		}
    	}
    	
    	if (insertFormat == INSERT_FORMAT.COMMENT){
    		insert.append(")" + "\n" + "-- VALUES "); //$NON-NLS-1$
    	} else if (isPrepare){
    		insert.append(") VALUES "); //$NON-NLS-1$
    	} else {
    		insert.append(")" + "\n" + "VALUES "); //$NON-NLS-1$
    	} 	
    	
    	if (!isError){  // defer value gen until error encountered
    		
    		insert.append( "("); //$NON-NLS-1$
    		
    		for (int i = 0; i < orderedRow.length; i++) {
    			if (i!=0){
        			insert.append(","); //$NON-NLS-1$
        		}
    			
    			if (isPrepare){
    				insert.append("?"); //$NON-NLS-1$
    			}
    			else {
    				fmtor = DataTypeFormatterRegistry.getInstance().getFormattor(cols[ i ].getType());

    				if (fmtor == null) // Assume character as default. This is ugly :(
    					fmtor = DataTypeFormatterRegistry.getInstance().getFormattor("VARCHAR2"); //$NON-NLS-1$

    				// set Locale
    				fmtor.setLocale((Locale) _locale);

    				insert.append(fmtor.formatData(orderedRow[ i ], cols[ i ]));
    			}
    		}
    	}

    	if (!isError){
    		insert.append(")"); //$NON-NLS-1$
    	}
    	if (!isError && !isPrepare){
    		insert.append(";");
    	}
    	return insert.toString();
    }

    // Always add user....   
    private String addUser(String name){
    		return _schemaName!=null &&_schemaName.length()>0?DBUtil.addDoubleQuote(_schemaName)+"."+name:name;
     		//return DBUtil.addDoubleQuote(_schemaName)+"."+name; //$NON-NLS-1$
    }
    
    // Adds double quotes to name and db_link if specified 
    protected String addTableName(String name){
    	if (_useQuotedNames){
    		String[] qName = name.split("@");
        	String tName = DBUtil.addDoubleQuote(qName[0]);
        	if (qName.length>1){
        		tName += "@"  + qName[1];
        	}
    		return tName;
    	} 
    	return name;
    }
    
    // Adds double quotes to names use quoted names options is specified 
    protected String addDoubleQuote(String name){
    	if (_useQuotedNames){
    		return DBUtil.addDoubleQuote(name);
    	} 
    	return name;
    }
    
}
