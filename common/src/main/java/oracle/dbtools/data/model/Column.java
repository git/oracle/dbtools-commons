/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.model;

/**
 * @author Barry McGillin
 * Defines a column in a data file.  This will include type, precision, scale
 * and null availability.
 */
public class Column { 
  private String m_defaultName;
  private String m_name;
  // TODO Deprecate. 
  private String sourceColumnName="";
  private String m_type;
  private int m_precision;
  private int m_scale;
  private String m_format;
  private String m_comment;
  private boolean m_nullable;
  private int sourceColID=-1;
  private String defaultString;
    /**
   * @return the comment
   */
  public String getComment() {
    return m_comment;
  }
  /**
   * @param comment the comment to set
   */
  public void setComment(final String comment) {
    m_comment = comment;
  }
  /**
   * @return the Name
   */
  public String getName() {
    return m_name;
  }
  public String getDefaultName() {
	    return m_defaultName;
	  }
  /**
   * @param name the Name to set
   */
  public void setName(final String name) {
    m_name = name;
    if (m_defaultName == null){
    	m_defaultName=name;
    }
  }
  /**
   * @return the m_type
   */
  public String getType() {
    return m_type;
  }
  /**
   * @param type the m_type to set
   */
  public void setType(final String type) {
    m_type = type;
  }
  /**
   * @return the m_precision
   */
  public int getPrecision() {
    return m_precision;
  }
  /**
   * @param precision the precision to set
   */
  public void setPrecision(final int precision) {
    m_precision = precision;
  }
  /**
   * @return the Scale
   */
  public int getScale() {
    return m_scale;
  }
  /**
   * @param scale the Scale to set
   */
  public void setScale(final int scale) {
    m_scale = scale;
  }
  /**
   * @return the Format
   */
  public String getFormat() {
    return m_format;
  }
  /**
   * @param format the Format to set
   */
  public void setFormat(final String format) {
    m_format = format;
  }
  /**
   * @return the Nullable
   */
  public boolean isNullable() {
    return m_nullable;
  }
  /**
   * @param nullable the Nullable to set
   */
  public void setNullable(final boolean nullable) {
    m_nullable = nullable;
  }
  /**
   * getSourceColID getter method
   * @return the sourceColID
   */
  public int getSourceColID() {
    return sourceColID;
  }
  /**
   * setSourceColID setter method.
   * @param sourceColID the sourceColID to set
   */
  public void setSourceColID(final int id) {
    sourceColID = id;
  }
  /**
   * Default String
   * @return the string
   */
  public String getDefault() {
    return defaultString;
  }
  /**
   * setDefaultString setter method.
   * @param defaultid the column default to set
   */
  public void setDefault(final String defaultid) {
    defaultString = defaultid;
  }
   /**
   * getSourceColumnName getter method
   * @return the sourceColumnName
   */
  public String getSourceColumnName() {
    return sourceColumnName;
  }
  /**
   * setSourceColumnName setter method.
   * @param sourceColumnName the sourceColumnName to set
   */
  public void setSourceColumnName(final String name) {
    this.sourceColumnName = name;
  }
  /**
   * @param column
   */
  public void refreshData(final Column column) 
  {
	if (column != null)
	{
	    setName(column.getName());
	    setType(column.getType());
	    setScale(column.getScale());
	    setPrecision(column.getPrecision());
	    setNullable(column.isNullable());
	    setDefault(column.getDefault());
	    setFormat(column.getFormat());
	    setComment(column.getComment());
	    setFormat(column.getFormat());
    }
  }
}