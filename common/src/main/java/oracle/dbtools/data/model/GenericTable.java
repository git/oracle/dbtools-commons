/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.jdbc.OraclePreparedStatement;

/**
 * @author Barry McGillin
 */
public class GenericTable implements Baseable {
 

protected Table _table;
protected String _schemaName;  //the actual schema name from the metadata for the table.


public GenericTable(Connection conn, String schemaName, String tableName){
	populateTable(conn, schemaName,tableName);
}
public GenericTable(final Table inTable) {
  _table = inTable;
}
public GenericTable() {
  _table = new Table();
}

/**
 * Populate table is used to create a table object by querying the database.
 * The column properties will be populated from direct queries and from
 * using getMetaData from the connection. 
 * 
 * It will:
 * 	- will ensure that the table exists.  If it does, it will:
 *  - create a Table object and set in the generic table
 *  - select the column name and nullable columns for it and create the Column objects
 * 
 * @param conn
 * @param schema name
 * @param table name
 */
private void populateTable(Connection conn, String schemaName, String tableName) {
	
	// Ensure that the table exists before trying to build model and letting load proceed.
/*
	boolean exists = isExistsTable(conn, false, schemaName, tableName);
	if (!exists){
		exists = isExistsTable(conn, true, schemaName, tableName);
	}
	if (exists){
		Table table = new Table();
		table.setTablename(tableName);  
		table.setColumns(getColumns(conn, schemaName, tableName));
		_table = table;
	}
	*/
	_table = getMetaData(conn, schemaName, tableName);

}

private Table getMetaData(Connection conn, String schemaName, String tableName) {
	
	Table table = null;
	// Use the prepared statement to get the rest of the metadata for the columns.
	// Doing it this way instead of from the all_tab_cols so that we don't have to intepret
	// the column values for precision, length, scale and datataype.
	ArrayList<Column> columns = new ArrayList<Column>();
	if (LockManager.lock(conn)) {
        try {
        	
        	String sql="";
        	
        	String[] qName = tableName.split("@");
        	String tName = DBUtil.addDoubleQuote(qName[0]);
        	if (qName.length>1){
        		tName += "@"  + qName[1];
        	}
        	
        	if (schemaName != null && schemaName.length()>0) {
        		sql = "select * from " + DBUtil.addDoubleQuote(schemaName) + "." + tName + " where 1 = 2"; 
        	} else {
        		sql = "select * from " + tName + " where 1 = 2"; 
        	}	
        	PreparedStatement pstmt = getPreparedStatement(conn,sql);
        	
        	ResultSetMetaData m_meta = pstmt.getMetaData();
        	
        	if (m_meta != null){
        		int colCount = m_meta.getColumnCount();
        		
        		for (int i = 0; i < colCount; i++){
        			Column column = new Column();
        			column.setName(m_meta.getColumnName(i+1));
        			column.setNullable(m_meta.isNullable(i+1)==1);
        			column.setType(m_meta.getColumnTypeName(i+1));
        			column.setPrecision(m_meta.getPrecision(i+1));
        			column.setScale(m_meta.getScale(i+1));
        			columns.add(column);
        		}
        		
        		table = new Table();
        		table.setTablename(tableName); 
        		table.setColumns(columns);
        	}
        } catch (Exception e) {
        	// table does not exist....
        	int i = 1;
        } finally {
        	LockManager.unlock(conn);
        }
	} 
	return table;
}

/**
 * Override this method if prepraring against a nonOracle database.
 */
protected PreparedStatement getPreparedStatement(Connection conn, String sql) throws SQLException{
	return (PreparedStatement) (OraclePreparedStatement) conn.prepareStatement(sql);
}



  /**
   * @param args
   */
  public static void main(String[] args) {
    // TODO Auto-generated method stub
  }
  /**
   * addColumn 
   * @see oracle.dbtools.raptor.data.core.Baseable#addColumn(oracle.dbtools.raptor.data.model.Column)
   * @param column
   */
  public void addColumn(Column column) {
    _table.addColumn(column);
  }
  /**
   * getColumn 
   * @see oracle.dbtools.raptor.data.core.Baseable#getColumn()
   * @return
   */
  public Column getColumn(final int number) {
    return getColumns()[number];
  }
  /**
   * getColumns 
   * @see oracle.dbtools.raptor.data.core.Baseable#getColumns()
   * @return the columns in the table list.
   */
  public Column[] getColumns() {
    return _table.getColumns().toArray(new Column[_table.getColumns().size()]);
  }
  /**
   * getDataType 
   * @see oracle.dbtools.raptor.data.core.Baseable#getDataType(oracle.dbtools.raptor.data.model.Column)
   * @param column
   * @return
   */
  public String getDataType(final Column column) {
    return column.getType();
  }
  /**
   * getTable 
   * @see oracle.dbtools.raptor.data.core.Baseable#getTable()
   * @return the base table object.
   */
  public Table getTable() {
    return _table;
  }
  /**
   * setColumns 
   * @see oracle.dbtools.raptor.data.core.Baseable#setColumns(oracle.dbtools.raptor.data.model.Column[])
   * @param columns
   */
  public void setColumns(final Column[] columns) {
    
    List<Column> cols = _table.getColumns();
    if (cols != null)
    {
	    cols.clear();
	    for ( int i=0; i<columns.length; i++ ) {
	      cols.add(columns[i]);
	    }
	    _table.setColumns(cols.toArray(new Column[cols.size()]));
    }
  }
  
  /**
   * setTableName 
   * @see oracle.dbtools.raptor.data.core.Baseable#setTableName(java.lang.String)
   * @param name
   */
  public void setTableName(String name) {
    _table.setTablename(name);
  }
  
  
  private boolean isExistsTable(Connection conn, boolean isDba, String owner, String name) {
	  
	  if (conn == null){
			return false;
		}
	  
		Map<String, String> binds = new HashMap<String, String>();
		//String[] object = objectName.split("@");

		String object="";
		//String query = "select count(1) from :OBJECT where rownum =0";
		//if (objectOwner == null || objectOwner.length()==0) {
		//	object = objectName;
		//} else {
		//	object=objectOwner + "." + objectName;
		//}	

		//binds.put("OBJECT", object); //$NON-NLS-1$	
		
		String query = "select count(1) from ";
		if (owner != null && owner.length()>0) {
			//	object = objectName;
			//} else {
			//	object=objectOwner + "." + objectName;		
			query += owner+".";
		}	
		query += name + " where rownum =0";
		
		String ret = DBUtil.getInstance(conn).executeOracleReturnOneCol(query, binds);
		if (ret == null){
			return false;
		}
		return true;

	}
  
  public static boolean isExistsTableSave(Connection conn, boolean isDba, String owner, String name) {
		
		if (conn == null){
			return false;
		}
		
		String EXIST_OBJECT = " select 1 from " + (isDba?"dba_objects":"all_objects") + 
			" where owner = :OWNER " +
			" and object_type = 'TABLE' " +
			" and object_name = :NAME " ;
		

		Map<String, String> binds = new HashMap<String, String>();
		ResultSet rset = null;
		binds.put("OWNER", owner); //$NON-NLS-1$
		binds.put("NAME", name); //$NON-NLS-1$
		if (LockManager.lock(conn)) {
          try {
              rset = DBUtil.getInstance(conn).executeQuery(EXIST_OBJECT, binds);
              while (rset.next()) {
                  return true;
              }
          } catch (Exception e) {

          } finally {
        	  DBUtil.closeResultSet(rset);
              LockManager.unlock(conn);
          }
      }
      return false;
  }
  
  
	private ArrayList<Column> getColumnsSave(Connection conn, String owner, String tableName) {
    	ArrayList<Column> columns = new ArrayList<Column>();
        Map<String, String> binds = new HashMap<String, String>();
        ResultSet rset = null;
        binds.put("OWNER", owner);
        binds.put("TAB", tableName); //$NON-NLS-1$
        
        String COLUMN_OBJECT = "select column_name, nullable from all_tab_cols where table_name = :TAB" +
        					  " and owner = :OWNER" +
        					  " ORDER BY COLUMN_ID";

        if (LockManager.lock(conn)) {
            try {
            	rset = DBUtil.getInstance(conn).executeQuery(COLUMN_OBJECT, binds);
                while (rset.next()) {
                	
                	Column column = new Column();
                	column.setName(rset.getString(1));
                	column.setNullable(rset.getString(2).equals("Y"));
                    columns.add(column);
                }
            } catch (Exception e) {

            } finally {
        		DBUtil.closeResultSet(rset);
                LockManager.unlock(conn);
            }
        }
        return columns;
    }	
}
