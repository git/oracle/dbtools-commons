/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.model;

/**
 * @author joyce.scapicchio@oracle.com
 * Defines a field in the physical data file... not to be confused with the column
 * in a table that the field is mapped to.  This will include mapped column, data
 * format, null value, default value
 */
public class LoadField { 
  private String m_defaultName;
  private String m_name;
  private Column m_column;
  private int m_length;  // The number of bytes for this field in the record
  private String _detectedType;
  private int _detectedPrecision;
  private int _detectedScale;
  private String m_format;
  private String m_nullWhen;
  private String m_defaultWhen;
  private boolean m_isLoad;
  private boolean m_isChecked; // the field has been checked (by user) 
  private boolean m_isCompatible;  // not going as far as determining if valid or not  
 
  /**
   * @return the Name
   */
  public String getName() {
    return m_name;
  }
  public String getDefaultName() {
	    return m_defaultName;
	  }
  /**
   * @param name the Name to set
   */
  public void setName(final String name) {
    m_name = name;
    if (m_defaultName == null){
    	m_defaultName=name;
    }
  }
  
  /**
   * @return the length
   */
  public int getLength() {
    return m_length;
  }
  /**
   * @param the length to set
   */
  public void setLength(final int length) {
    m_length = length;
  }
  /**
   * @return the Format
   */
  public String getFormat() {
    return m_format;
  }
  
  /*
	 * Get the datatype that was detected based on the contents of the data.
	 */
	public String getDetectedType() {
		return _detectedType;
	}
	/*
	 * Get the datatype that was detected based on the contents of the data.
	 */
	public void setDetectedType(String value){
		_detectedType=value;
	}
	
	/*
	 * Get the precision that was detected based on the contents of the data.
	 */
	public int getDetectedPrecision() {
		return _detectedPrecision;
	}
	/*
	 * Get the datatype that was detected based on the contents of the data.
	 */
	public void setDetectedPrecision(int value){
		_detectedPrecision=value;
	}
	
	/*
	 * Get the scale that was detected based on the contents of the data.
	 */
	public int getDetectedScale() {
		return _detectedScale;
	}
	/*
	 * Get the datatype that was detected based on the contents of the data.
	 */
	public void setDetectedScale(int value){
		_detectedScale=value;
	}
  /**
   * @param format the Format to set
   */
  public void setFormat(final String format) {
    m_format = format;
  }
  
  /**
   * @param set the Source Column for this field. 
   */
  public void setColumn(Column column) {
	  m_column = column;
  }
  
  /**
   * @return the Source Column mapped to the field
   */
  public Column getColumn() {
	  return m_column;
  }
  
  /**
   * @param set the Null When string for this field. 
   * If the column is nullable and the value of the data
   * is equal to the string the column will be set to null
   * during the load.
   */
  public void setNullWhen(String nullWhen) {
	  m_nullWhen = nullWhen;
  }
  /**
   * @return the string that should be recognized as the null value
   */
  public String getNullWhen() {
	  return m_nullWhen;
  }
  
  /**
   * @param set the Default When string for this field. 
   * If the column supports default and the value of the
   * data is equal to the string the column will be set 
   * to the default value during the load.
   */
  public void setDefaultWhen(String defaultWhen) {
	  m_defaultWhen = defaultWhen;
  }
  /**
   * @return the string that should be recognized as default value
   */
  public String getDefaultWhen() {
	  return m_defaultWhen;
  }
  
  /**
   * @param set boolean indicating if field should be loaded.
   */
  public void isLoad(boolean isLoad) {
	  m_isLoad = isLoad;
  }
  /**
   * @return the boolean indicating if field should be loaded.
   */
  public boolean isLoad() {
	  return m_isLoad;
  }
  
  /**
   * @param set boolean indicates the field has been checked (in some way)
   * 			possibly only that the user has looked at the field
   */
  public void isChecked(boolean isChecked) {
	  m_isChecked = isChecked;
  }
  /**
   * @return the boolean indicating if field has been checked.
   */
  public boolean isChecked() {
	  return m_isChecked;
  }
  
  /**
   * @param set boolean indicated if the field has been found to be
   * 			compatible with the column def
   */
  public void isCompatible(boolean isCompatible) {
	  m_isCompatible = isCompatible;
  }
  /**
   * @return the boolean indicating if field has been found to be compatible with the table column def.
   */
  public boolean isCompatible() {
	  return m_isCompatible;
  }
 
}