/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.model;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author joyce.scapicchio@oracle.com
 * Defines a file (the physical data file)... not to be confused with the
 * table that the data is mapped to.  
 */
public class LoadFile {
  private List<LoadField> _fields;
  private String _name;
  private String _shortName;
  private String _canonicalName;
  private String _type;
  
  public LoadFile() {
    _fields = new ArrayList<LoadField>();
    _type="csv";
  }
  
  public LoadFile(String name) {
	  setName(name);  // also sets up type and canonical name/
	    _fields = new ArrayList<LoadField>();
	  }
  /**
   * Add a field to the list of fields.
   * @param field to add
   */
  public void addField(final LoadField field) {
	  if (field != null) //add only valid fields
		  _fields.add(field);
  }
  /**
   * @return the fields
   */
  public List<LoadField> getFields() {
    return _fields;
  }
  
  /**
   * @return the number of fields
   */
  public int getFieldCount() {
    return _fields.size();
  }
  /**
   * @param fields the fields to set
   */
  private void setFields(final List<LoadField> fields) {
    _fields = fields;
  }
  /**
   * @param fieldss the fields to set
   */
  public void setFields(LoadField[] fields) {
    List<LoadField> list = new ArrayList<LoadField>();
    for (int i=0; i<fields.length; i++) {
    	if (fields[i] != null) //add only valid fields
    		list.add(fields[i]);
    }
    setFields(list);  
  }
  /**
   * @return the filename
   */
  public String getName() {
    return _name;
  }
  
  /**
   * @return the URL
   */
  public URL getURL() {
	try {  
		return new File(_name).toURI().toURL();
	} catch (Exception e){
    	Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);	
    }
	return null;
  }
  
  /**
   * @return the filename
   */
  public String getShortName() {
	  return _shortName;
  }
  
  /**
   * @return the canonical filename
   */
  public String getCanonicalName() {
    return _canonicalName;
  }
  
  /**
   * @param filename the filename to set
   */
  public void setName(final String fileName) {
    _name = fileName;
    try {
    	_canonicalName = new File(_name).getCanonicalPath();
    	_type = _name.substring(_name.lastIndexOf(".", _name.length()) + 1); //$NON-NLS-1$
    	
    	_shortName  = new File(_name).getName();
    	if (_shortName.lastIndexOf(".") > -1) { 
    		_shortName = _shortName.substring(0, _shortName.lastIndexOf(".")); //$NON-NLS-1$  
    	}	
        
    } catch (Exception e){
    	Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);	
    }
  }
  
  /**
   * @return the type of file (csv, xls...)
   */
  public String getType() {
    return _type;
  }
  
  /**
   * Return the Field based on its position in the list.	
   * @param name
   * @return
   */
  public LoadField getField(final int fieldIndex) {
	if (fieldIndex < _fields.size()){
		return _fields.get(fieldIndex);
	}
    return null;
  }
  /**
   * @param name
   * @return
   */
  public LoadField getFieldByName(final String name) {
    Iterator<LoadField> it = _fields.iterator();
    while (it.hasNext()){
    	LoadField field = it.next();
      if (name.trim().equals(field.getName().trim())) {
        return field;
      }
    }
    return null;
  }
  /**
   * @return the first Field
   */
  public LoadField getFirstField() {
    Iterator<LoadField> it = _fields.iterator();
    while (it.hasNext()){
    	LoadField field = it.next();
      return field;
    }
    return null;
  }

  /**
   * @param name
   * @return
   */
 /*
  public Field getSrcColName(final String name) {
    Iterator<Field> it = _fields.iterator();
    while (it.hasNext()){
      Field field = it.next();
      if (name.equals(field.getSourceFieldName())) {
        return field;
      }
    }
    return null;
  }
  /*
  /**
   * @param col
   */
/*  public void updateField(Field field) {
    Iterator<Field> it = _fields.iterator();
    while (it.hasNext()){
      Field c = it.next();
      if (col.getName().equals(c.getName())) {
        c.setType(col.getType());
        c.setNullable(col.isNullable());
        c.setComment(col.getComment());
        c.setScale(col.getScale());
        c.setPrecision(col.getPrecision());
        c.setDefault(col.getDefault());
        ;
      }
    }
      
  }
*/  

}
