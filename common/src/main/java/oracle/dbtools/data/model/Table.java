/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Barry McGillin
 *
 */
public class Table {
  private List<Column> columns;
  private String tablename;
  
  public Table() {
    columns = new ArrayList<Column>();
  }
  /**
   * Add a column to the list of columns.
   * @param column to add
   */
  public void addColumn(final Column column) {
	  if (column != null) //add only valid columns
		  columns.add(column);
  }
  /**
   * @return the columns
   */
  public List<Column> getColumns() {
    return columns;
  }
  /**
   * @return the number of columns
   */
  public int getColumnCount() {
    return columns.size();
  }
  /**
   * @param columns the columns to set
   */
  public void setColumns(final List<Column> columns) {
    this.columns = columns;
  }
  /**
   * @param columns the columns to set
   */
  public void setColumns(Column[] columns) {
    List<Column> list = new ArrayList<Column>();
    for (int i=0; i<columns.length; i++) {
    	if (columns[i] != null) //add only valid columns
    		list.add(columns[i]);
    }
    setColumns(list);  
  }
  /**
   * @return the tablename
   */
  public String getTablename() {
    return tablename;
  }
  /**
   * @param tablename the tablename to set
   */
  public void setTablename(final String tablename) {
    this.tablename = tablename;
  }
  /**
   * @param name
   * @return
   */
  public Column getColumnByIndex(final int index) {
	if (columns.size()>index){
		return columns.get(index);
	}  
    
    return null;
  }
  
  /**
   * @param name
   * @return
   */
  public Column getColumnByName(final String name) {
    Iterator<Column> it = columns.iterator();
    while (it.hasNext()){
      Column col = it.next();
      if (name.trim().equals(col.getName().trim())) {
        return col;
      }
    }
    return null;
  }
  
  /**
   * @param name
   * @return
   */
  public int getColumnIndexByName(final String name) {
    Iterator<Column> it = columns.iterator();
    int i=0;
    while (it.hasNext()){
      Column col = it.next();
      if (name.trim().equals(col.getName().trim())) {
        return i;
      }
      ++i;
    }
    return -1;
  }
  
  /**
   * @return the first Column
   */
  public Column getFirstColumn() {
    Iterator<Column> it = columns.iterator();
    while (it.hasNext()){
      Column col = it.next();
      return col;
    }
    return null;
  }
  /**
   * @param name
   * @return
   */
  public Column getSrcColName(final String name) {
    Iterator<Column> it = columns.iterator();
    while (it.hasNext()){
      Column col = it.next();
      if (name.equals(col.getSourceColumnName())) {
        return col;
      }
    }
    return null;
  }
  /**
   * @param col
   */
  public void updateColumn(Column col) {
    Iterator<Column> it = columns.iterator();
    while (it.hasNext()){
      Column c = it.next();
      if (col.getName().equals(c.getName())) {
        c.setType(col.getType());
        c.setNullable(col.isNullable());
        c.setComment(col.getComment());
        c.setScale(col.getScale());
        c.setPrecision(col.getPrecision());
        c.setDefault(col.getDefault());
        ;
      }
    }
    
  }

}
