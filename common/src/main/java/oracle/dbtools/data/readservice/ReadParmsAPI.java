/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.readservice;


public abstract class ReadParmsAPI {
	public static int UNLIMITED=-1;
	// generic reader properties
	protected boolean _header = true;
	protected boolean _headerBeforeSkip=true;
	private int _skipRows=0;
	private int _limitRows=UNLIMITED;
		
	public static enum SERVICE_TYPE{CSV,DSV,XLS,XLSX};
		
	public ReadParmsAPI(boolean header, boolean headerBeforeSkip, int skipRows, int limitRows) {
		_header=header;
		_headerBeforeSkip=headerBeforeSkip;
		_skipRows=skipRows;
		_limitRows=limitRows;
	}

	abstract public SERVICE_TYPE getServiceType();
	/**
	 * isHeader
	 * @return true if header is selected
	 */
	public boolean isHeader(){
		return _header;
	}
	
	/**
	 * isHeader - sets header selected
	 * @param valeu = true, header is selected
	 */
	public void isHeader(boolean value){
		_header = value;
	}
	
	/**
	 * isHeader
	 * @return true if header befroe skip is selected
	 */
	public boolean isHeaderBeforeSkip(){
		return _headerBeforeSkip;
	}
	
	/**
	 * isHeader - sets header before skip selected
	 * @param valeu = true, header is selected
	 */
	public void isHeaderBeforeSkip(boolean value){
		_headerBeforeSkip = value;
	}
	
	/**
	 * setSkipRows
	 * @param Indicate the rows to skip before processing.  This does not include the header row.
	 *  
	 * 			Note, SQLDev Importer allows the skip to before or after the header row, 
	 * 			allowing processing files with embedded DDL for example. 
	 * 			This option is not included in this feature.
	 */
	public void setSkipRows (int value){
		_skipRows=value;
	}
	
	
	/**
	 * getSkipRows
	 * @return The rows to skip before processing.  This does not include the header row.
	 */
	public int getSkipRows (){
		return _skipRows;
	}
	

	/**
	 * limitRows
	 * @param Limit the rows being processed for load. 
	 * 			To specify all rows are to be loaded, specify UNLIMITED(-1).
	 * 			Note, this is data rows read and processed, not necessarily successfully loaded.
	 */
	public void setLimitRows (int value){
		_limitRows=value;
	}
	/**
	 * getLimitRows
	 * @return The number of errors to process for load.
	 */
	public int getLimitRows (){
		return _limitRows;
	}
	/**
	 * isLimitRows
	 * @return Returns true if number of errors to != UNLIMITED
	 * 			(UNLIMITED=-1)
	 */
	public boolean isLimitRows (){
		return _limitRows != UNLIMITED;
	}
	
}
