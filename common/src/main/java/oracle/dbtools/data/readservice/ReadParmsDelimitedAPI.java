/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.readservice;

/**
* Values for the options to be used for reading a delimited file using the DelimitedReader.
* A BatchReaderOptions object is created to customize reading the delimited file such as  
* the load encoding, delimiter, enclosures, and line end characters.
*/ 
public class ReadParmsDelimitedAPI extends ReadParmsAPI {
	
    public static String DEFAULT_ENCODING = "UTF8"; //$NON-NLS-1$;
	public static String DEFAULT_DELIMITER = ","; //$NON-NLS-1$;
	public static String DEFAULT_ENCLOSURE="\""; //$NON-NLS-1$
	public static int UNLIMITED=-1;
	// Line Terminator CRLF - windows
	public static final String EOL_CRLF = "\r\n"; //$NON-NLS-1$
	// Line Terminator LF - Java / Unix
	public static final String EOL_LF = "\n"; //$NON-NLS-1$
	// Line Terminator CR - Mac
	public static final String EOL_CR = "\r"; //$NON-NLS-1$

	private static enum PARMS {
		SERVICE_TYPE,
		HEADER,
		SKIP_BEFORE_HEADER,
		SKIP_ROWS,
		LIMIT_ROWS,
		ENCODING,
		DELIMITER,
		LEFT_ENCLOSURE,
		RIGHT_ENCLOSURE,
		RIGHT_ENCLOSURE_DOUBLE,
		LINE_END,
		MAX_LENGTH
	}
	
	// delimiter format properties
	private SERVICE_TYPE _serviceType;
	private String _encoding=DEFAULT_ENCODING;
	private String _delimiter = DEFAULT_DELIMITER;
	private String _enclosureLeft= DEFAULT_ENCLOSURE;
	private String _enclosureRight= DEFAULT_ENCLOSURE;
	private boolean _enclosureRightDouble;
	
	private String _lineEnd; // default is null indicating any standard line term recognized
	private int _lineMax;
	
  	/** 
  	 * Builder class for constructing and validating ReadParmsDelimitedAPI
     */   
	public static class Builder {
    	// optional parameters
		private String _format="CSV";
		private SERVICE_TYPE _serviceType;
    	// optional parameters
		private boolean _header = true;
		private boolean _headerBeforeSkip=true;
		private int _skipRows=0;
		private int _limitRows=UNLIMITED;
		private String _encoding=DEFAULT_ENCODING;
		private String _delimiter = DEFAULT_DELIMITER;
		private String _enclosureLeft= DEFAULT_ENCLOSURE;
		private String _enclosureRight= DEFAULT_ENCLOSURE;
		private boolean _enclosureRightDouble;
		
		private String _lineEnd; // default is null indicating any standard line term recognized
		private int _lineMax = UNLIMITED; ; 
    
		/**
    	 * Builder constructor takes the required parameters.
    	 * Use other public methods as needed to set optional parameters.
    	 * 
    	 * ReadParmsDelimitedAPI constructor will use this object on build()
    	 */
    	public Builder (){
    		_serviceType=SERVICE_TYPE.CSV;
    	}
    	
    	/**
    	 *  Indicate presence of a header sent in the data.
    	 *  The default is that a header is present.

    	 *  @param header true indicates a header is present.
    	 *  @return Builder  
		 */    	
    	public Builder header(boolean header){
    		_header=header;
    		return this;
    	}
    	
       	/**
    	 *  Identify the location of the header in relation to skip rows.
    	 *  The default is that a header the header is before the skip rows.

    	 *  @param headerBeforeSkip true indicates header is before skip rows.
		 *         false indicates a header is after the skip rows. 
    	 *  @return Builder  
		 */   
    	public Builder headerBeforeSkip(boolean headerBeforeSkip){
    		_headerBeforeSkip=headerBeforeSkip;
    		return this;
    	}
    	
    	/**
    	 *  Specify the optional number of rows to skip.
    	 *  The default is 0.

    	 *  @param skipRows indicates the number rows
    	 *  @return Builder  
		 */   
    	public Builder skipRows (int skipRows){
    		_skipRows=skipRows;
    		return this;
    	}

    	/**
    	 *  Identify a maximum number of rows to process.
    	 *  The default is no limit.
    	 *  
    	 *  @param limitRows indicates the limit for the number of rows to process.
    	 *  @return Builder  
		 */   
    	public Builder limitRows (int limitRows){
    		_limitRows=limitRows;
    		return this;
    	}
    	/**
    	 *  Set the encoding of the input stream.
    	 *  The default is UTF8.

    	 *  @param encoding identifies the encoding of the input stream.
    	 *  @return Builder  
		 */      	    	
    	public Builder encoding(String encoding){
    		_encoding=encoding==null?DEFAULT_ENCODING:encoding;
    		return this;
    	}
    	/**
    	 * Set the field delimiter for the fields in the file
    	 * The default is comma (,).
    	 * 
    	 * @param string containing the delimiter that separates fields in a row
    	 * @return Builder
    	 */
    	public Builder delimiter(String delimiter){
    		_delimiter=delimiter;
    		return this;
    	}
    	
    	/**
    	 * setEnclosures - allow 0, 1 or 2 enclosures.  If 1 is specified, sets
    	 * left and right enclosure to this value.  If 2 or more are specified
    	 * sets the left to the first character, the right to the second character and
    	 * ignores the remaining characters.  If null or empty, sets both enclosures to
    	 * empty which means the field is not enclosed 
    	 * 
    	 * Use enclosureLeft and enclosureRight to set multiple character enclosures
    	 * 
    	 * @param string containing the enclosure(s)
    	 * @return Builder
    	 */
    	public Builder enclosures(String enclosures) {
    		if (enclosures == null || enclosures.length() == 0) {
    			_enclosureLeft="";
    			_enclosureRight="";
    		} else if (enclosures.length()==1){
    			_enclosureLeft=enclosures;
    			_enclosureRight=enclosures;
    		} else {
    			_enclosureLeft=enclosures.substring(0,0);
    			_enclosureRight=enclosures.substring(1,1);
    		}
    		return this;
    	}
    	
    	/**
    	 * enclosureLeft - set the left enclosure.  If right enclosure is not specified,
    	 * left enclosure is used for both enclosures.
		 * Null of empty indicates no enclosures.
		 * 
		 * @param enclosureLeft sting containing the left enclosure value.
		 * @return Builder
    	 */ 
    	public Builder enclosureLeft(String enclosureLeft){
    		_enclosureLeft=enclosureLeft;
    		return this;
    	}
    	
    	/**
    	 * enclosureRight - set the right enclosure.  
    	 * 
		 * @param enclosureRight sting containing the right enclosure value.
		 * @return Builder
    	 */ 
    	public Builder enclosureRight(String enclosureRight){
    		_enclosureRight=enclosureRight;
    		return this;
    	}
    	/**
    	 * enclosureRightDouble - indicates that when right enclosures exist in
    	 * the data for a field enclosed with a left enclosure,  they are doubled.  
    	 * A single right enclosure character in the data for a field with a left 
    	 * enclosure will always be interpreted as a right enclosure.
    	 * 
		 * True indicates that two consecutive right enclosures are to be
		 * interpreted as a right enclosure in the data for a field with a left enclosure.
		 * 
		 * False indicates that two consecutive right enclosures for fields with left
		 * enclosures are in error.
		 * 
		 * @param enclosureRightDouble true indicates that enclosed fields have data
		 * 	containing the left enclosure doubled. 
		 * @return Builder
    	 */ 
    	public Builder enclosureRightDouble(boolean enclosureRightDouble){
    		_enclosureRightDouble=enclosureRightDouble;
    		return this;
    	}
    	
    	/**
    	 * lineEnd sets the line end (terminator).  
    	 * 
    	 * If the file contains standard line end characters (\r. \r\n or \n), the
    	 * lineEnd does not need to be specified. 
    	 * 
     	 * @param lineEnd
     	 * @return Builder
    	 */
     	
    	public Builder lineEnd(String lineEnd){
    		if (lineEnd.equals("\\r")){ //$NON-NLS-1$
    			_lineEnd = "\r"; //$NON-NLS-1$
    		} else if (lineEnd.equals("\\r\\n")){ //$NON-NLS-1$
    			_lineEnd = "\r\n"; //$NON-NLS-1$
    		} else if (lineEnd.equals("\\n")){ //$NON-NLS-1$
    			_lineEnd = "\n"; //$NON-NLS-1$
    		} 
    		_lineEnd = lineEnd;
    		return this;
    	}
    	
    	/**
    	 * lineMax sets a maximum line length for identifying lines/rows in the datastream.
    	 * A lineMax will prevent reading an entire stream as a single line when the
    	 * incorrect lineEnd character is being used.
    	 * 
    	 * The default is unlimited.
    	 * 
    	 * @param lineMax
    	 * @return
    	 */
    	public Builder lineMax(int lineMax){
    		_lineMax=lineMax;
    		return this;
    	}
    	
    	/**
    	 * Build LoadParmsAPI using options specified in this object.
    	 * @return LoadParmsAPI object
    	 */
    	public ReadParmsDelimitedAPI build(){
    		return new ReadParmsDelimitedAPI(this);
    	}
	}
	
	
	private ReadParmsDelimitedAPI(Builder builder) {
		super(builder._header, builder._headerBeforeSkip, builder._skipRows, builder._limitRows);
		_serviceType=builder._serviceType;
		_encoding=notNull(builder._encoding);
		_delimiter=notNull(builder._delimiter);
		_enclosureLeft=notNull(builder._enclosureLeft);
		_enclosureRight=notNull(builder._enclosureRight);
		_enclosureRightDouble=builder._enclosureRightDouble;
		_lineEnd=builder._lineEnd;
		_lineMax=builder._lineMax;
		
		validateParm(PARMS.SERVICE_TYPE, _serviceType);
		validateParm(PARMS.DELIMITER, _delimiter);
		
		if (_enclosureLeft.length()> 0 && _enclosureRight.length()==0){
			_enclosureRight = _enclosureLeft;
		}
		if (_lineMax < 1){
			_lineMax=UNLIMITED;
		}
		if (getSkipRows()<0){
			throw new IllegalStateException(PARMS.SKIP_ROWS.toString());
		}
		
		
	}
	
	private void validateParm(Enum<PARMS> parm, Object parmValue){
    	if (parmValue == null || 
    			(parmValue instanceof String && ((String)parmValue).length() == 0)){
    		throw new IllegalStateException(parm.toString());
    	}
    }
	
	private String notNull(String string){
		return (string != null)? string: "";
	}
	
	/**
	 * get the enum for service type for this reader
	 */
	public SERVICE_TYPE getServiceType(){
		return _serviceType;
	}
	
	/**
	 * getEncoding
	 * @return the encoding selected
	 */
	public String getEncoding(){
		return _encoding;
	}
		
	/**
	 * getDelimiter
	 * @return - the selected delimiter
	 */
	public String getDelimiter() {
		return _delimiter;
	}
		
	/**
	 * isDelimiterWhiteSpace
	 * @param delimiter
	 * @return true if the delimiter is whitespace value (two spaces)
	 */	
	public static boolean isDelimiterWhiteSpace(String delimiter){
		return delimiter.length() > 1 && delimiter.equals("  ")? true : false;
	}
	public boolean isDelimiterWhiteSpace(){
		return _delimiter.length() > 1 && _delimiter.equals("  ")? true : false;
	}
	
	/**
	 * getDelimiterValue
	 * @return - returns the value for the delimiter,
	 */
	public String getDelimiterValue() {
		String delimChars = getDelimiter();
		if (delimChars!=null && delimChars.length()>0){
			if (delimChars.equals("TAB")){ //$NON-NLS-1$
				delimChars = "\t"; //$NON-NLS-1$
			} else if (delimChars.equals("WHITESPACE")){ //$NON-NLS-1$
				delimChars = "  "; //$NON-NLS-1$
			} else if (delimChars.equals("SPACE")){ //$NON-NLS-1$
				delimChars = " "; //$NON-NLS-1$
			} 
		}
		return delimChars;
	}

	/**
	 * getDelimiterString
	 * @param delim
	 * @return - get the External String for the delimiter value
	 */	
	public static String getDelimiterString(String delim) {
		String delimChars=delim;
		if (delimChars.equals("TAB")){
			//delimChars = "\\t";
			delimChars = "X'09'"; // not sure about this with encoding
		} else if (delimChars.equals("WHITESPACE")){
			delimChars = "WHITESPACE";
		} else if (delimChars.equals("SPACE")){
			delimChars = " ";
		}
		if (delimChars.equals("\t")){ //$NON-NLS-1$
			return "\\t"; //$NON-NLS-1$
		}
		return delimChars;
	}

	/**
	 * isEnclosed()
	 * @return - true if enclosures are selected
	 */
	public boolean isEnclosed(){
		if (getEnclosureLeft().length()==0 || getEnclosureRight().length()==0){
			return false;
		}
		return true;
	}
		
	/**
	 * getEnclosureLeft
	 * 
	 * @return - enclosure left or "" 
	 */
	public String getEnclosureLeft() {
		return _enclosureLeft;
	}

	
	public String getEnclosureRight() {
		return _enclosureRight;
	}
	
		
	/**
	 * isEnclosureRightDouble
	 * @return - true if enclosure right double
	 */
	public boolean isEnclosureRightDouble(){
		return _enclosureRightDouble;
	}
		
	/**
	 * getEnvTerminatorValue
	 * 
	 * @return - String containing the environment default terminator 
	 */
	public static String getEnvTerminatorValue() {
		return System.getProperty("line.separator"); //$NON-NLS-1$
	}
	
	/**
	 * getTerminator gets the line end terminator 
	 * @return string containing the line end terminator
	 */
	public String getTerminator(){
		return _lineEnd;
	}

	/**
	 * isStandardTerminator convenience method for determining if the terminator
	 * is a standard terminator
	 * @return true if the terminator is CR, CRLF or LF
	 */
	public boolean isStandardTerminator(){
		return (_lineEnd == null || _lineEnd.equalsIgnoreCase(EOL_CRLF) ||
        	_lineEnd.equals(EOL_CR) ||
        	_lineEnd.equals(EOL_LF));
	}
	
	/**
	 * getUseMaxLength check if a maximum line length should be used
	 * 
	 * @return true indicates a miximum line length should be used
	 */
	public boolean getUseMaxLength(){
		return _lineMax>0;
	}
	
	/**
	 * getMaxLength 
	 * @returns the maximum line length
	 */
	public int getMaxLength() {
		return _lineMax;
	}
	
	
}
