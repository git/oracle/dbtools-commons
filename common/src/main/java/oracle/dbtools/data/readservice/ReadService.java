/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.readservice;

import java.util.Locale;
import java.util.logging.Logger;

import oracle.dbtools.data.common.TranslatableMessage;
import oracle.dbtools.data.loadservice.LoadResources;

/**
 * @author Joyce Scapicchio
 *
 */
public abstract class ReadService {
	
	private Logger LOGGER = Logger.getLogger(getClass().getName()); // default logger
	private Iterable<Locale> _responseLocales;
	/**
	 * What type of file is processed by the reader?
	 * @return type
	 */
	public abstract String getType();
	/**
	 * What is the format of the file?
	 * @return format
	 */
	public abstract String getFormat();
		
   /**
   * get the items in the header row (default row 1).
   * @return row 1 as an array of Strings
   */
  public abstract String[] getColumnNames() throws DataFormatException;

  /**
   * Open the reader.
   */
  public abstract void open();
  /**
   * Sets the reader to the start of the data.
   * start()
   * 
   */
  public abstract void start();
  /**
   * Indicates if there are more rows to read when using
   * readline()
   * 
   * @returns true if there are more rows, otherwise returns false
   * Replaces getRowCount which required pre-reading the entire file
   * 	      in some cases
   */
  public abstract boolean hasMoreRows();
  /**
   * Read the next line in file
   * @return array of objects.
   */
  public abstract Object[] readline() throws DataFormatException;
  
     
  /**
   * Close the reader.
   */
  public abstract void close();
 
  /*
   * getResponseLocales
   * @return the list of locales to be used for translating responses
   * 
   */
  protected Iterable<Locale> getResponseLocales(){
	  return _responseLocales;
  }
  
  /**
   * setResponseLocales
   * @param = the list of locales to be used for translating responses
   * 
   */
  protected void setResponseLocales(Iterable <Locale> responseLocales){
	  _responseLocales = responseLocales;
  }
  
  /**
  * setLogger Allows overriding logger with the logger used by service 
  *   
  */
  protected void setLogger(Logger logger){
	  LOGGER = logger;
  }
  
  /**
   * Translate and format message written to response stream.
   * If responseLocales were provided, these are used in order for obtaining the translated string
   * If responseLocales is null, standard message format is used.
   * 
   * Currently expecting all keys in LoadResources.properties.
   * @param messageId
   * @param args
   * @return
   */
  protected final String translate(String messageId, Object[] args){
  	if (_responseLocales == null){
  		return ReadResources.format(messageId, args);
  	}
		return (new TranslatableMessage(LoadResources.class, messageId, 
					ReadResources.format(messageId, args), 
					args)) //$NON-NLS-1$
					.toString(_responseLocales); 
  }
  
  /**
   * Get message to be written to response stream.
   * If responseLocales were provided, these are used in order for obtaining the translated string
   * If responseLocales is null, standard message getString is used.
   * 
   * Currently expecting all keys in LoadResources.properties.
   * @param messageId
   * @param args
   * @return
   */
  protected final String translate(String messageId){
  	if (_responseLocales == null){
  		return ReadResources.getString(messageId);
  	}
		return (new TranslatableMessage(ReadResources.class, messageId, 
				ReadResources.getString(messageId))) //$NON-NLS-1$
					.toString(_responseLocales); 
  }
  
  
  
}
