/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.readservice;
 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Joyce Scapicchio
 * 
 */

/**
 * The ReadServiceDelimited class handles reading delimited input streams.
 * ReadParmsDelimitedAPI object encapsulates the options for this reader.
 * The default format is comma separated value (csv) format, optionally
 * enclosed with double quotes, standard line end terminators. 
 * 
 * @author Joyce Scapicchio
 * 
 */
public class ReadServiceDelimited extends ReadService{
    public static final String NAME = "Delimited"; //$NON-NLS-1$
    public static final String FORMAT = "delimited"; //$NON-NLS-1$
    public static final String EXT = "dsv"; //$NON-NLS-1$
    public static final int BUFFER_SIZE=1024;  // read this many characters unless user sets pref
    public static final int BUFFER_KSIZE=1;

    private BufferedReader _reader;
    private InputStream _inputStream;
    private ReadParmsDelimitedAPI _parms;
    
	private Logger LOGGER = Logger.getLogger(getClass().getName()); // default logger
    
    private String[] m_headerRowColumnNames;
    private int m_columnCount = -1;
    private String m_currentRow = null;
    private boolean m_currentRowRead=true;
    private String[] m_firstRow=null;  
    private int m_rowsRead=0;
    
    /**
     * Constructor for stream based usage
     */
    public ReadServiceDelimited(InputStream inputStream, ReadParmsDelimitedAPI parms) {
    	this(inputStream,parms,null);
    }
    
    /**
     * Constructor for stream based usage with multi locale support for responses
     */
    public ReadServiceDelimited(InputStream inputStream, ReadParmsDelimitedAPI parms, Iterable<Locale> responseLocales) {
    	_inputStream=inputStream;
    	_parms=parms==null?new ReadParmsDelimitedAPI.Builder().build():parms;
    	super.setResponseLocales(responseLocales);
    }
    /**
     * File Name
     * 
     * @see oracle.dbtools.raptor.data.readers.IDataReader#getType()
     */
    public String getName() {
        return NAME;
    }
    /**
     * File Extension.
     * 
     * @see oracle.dbtools.raptor.data.readers.IDataReader#getType()
     */
    public String getType() {
        return EXT;
    }
    /**
     * File Format
     * 
     * @see oracle.dbtools.raptor.data.readers.IDataReader#getFormat()
     */
    public String getFormat() {
        return FORMAT;
    }
    
    /**
     * setLogger Allows overriding logger with the logger used by service 
     *   
     */
     protected void setLogger(Logger logger){
   	  LOGGER = logger;
   	  super.setLogger(logger);
     }
     
    /*
     * (non-Javadoc)
     * 
     * @see oracle.dbtools.raptor.data.core.DataReader#getColumnNames()
     */
    public String[] getColumnNames() throws DataFormatException {
    	try{

    		HashMap<Object, Integer> map = new HashMap<Object, Integer>();
    		List<String> names = new ArrayList<String>();


    		if (_parms.isHeaderBeforeSkip()){
    			for (int i=0; i < _parms.getSkipRows();++i)
    				if (!hasMoreRows()){
    					readLine(_reader);  // spin past the skip rows
    				}
    		}	

    		// This is supposed to be the header row
    		if (hasMoreRows()){
    			m_firstRow = (String[])readline();
    		}
    		
    		// First non-empty row should be header row...
    		while (hasMoreRows() && ((m_firstRow != null &&
    				(m_firstRow.length == 0 || (m_firstRow.length==1 && m_firstRow[0].length()==0))))){
    			m_firstRow = (String[])readline();
    		}

    		if (m_firstRow != null && m_firstRow.length > 0){
    			for (int i = 0; i < m_firstRow.length; i++) {
    				if (m_firstRow[i] != null) {
    					if (map.containsKey(m_firstRow[i])) {
    						map.put(m_firstRow[i], map.get(m_firstRow[i])+ 1);
    						names.add(m_firstRow[i].toString() + map.get(m_firstRow[i]));
    					} else {
    						map.put(m_firstRow[i], 1);
    						names.add(m_firstRow[i]);
    					} 
    				} else {       		        	
    					// error, column name missing
    				}
    			}
    			m_headerRowColumnNames = names.toArray(new String[ names.size() ]);
    		}
    		m_columnCount = m_headerRowColumnNames==null?0:m_headerRowColumnNames.length;
    		
    		// Run through the skip after header rows here
    		
    		if (!_parms.isHeaderBeforeSkip()){
    			for (int i=0; i < _parms.getSkipRows();++i)
    				if (!hasMoreRows()){
    					readLine(_reader);  // spin past the skip rows
    				}
    		}	

    	} catch (IOException e){
			LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
    	} 

    	return m_headerRowColumnNames;

    }

    private String[] splitRow(String line) throws DataFormatException {

    	List<String> columns = new ArrayList<String>();

    	String delimValue = _parms.getDelimiterValue();
    	int delimValueLength=delimValue.length();
		int errorOffset = -1; 	// -1 indicates no error

    	if (delimValueLength!=0){
    		int col      = 0;
    		int state    = 0;
    		StringBuilder token = new StringBuilder();

    		// If the delimiter is more than 1 space..., then use "white space" as the delimiter
    		if ( _parms.isDelimiterWhiteSpace()) {
    			delimValue = "\t";
    			delimValueLength=1;
    		}

    		String eLeftValue = _parms.getEnclosureLeft();
    		String eRightValue = _parms.getEnclosureRight();
    		//boolean isEnclosed = dataFormat.getEnclosureLeft().length()>0? true : false;
    		boolean isEnclosed = _parms.isEnclosed();
    		// push thru the state machine below to parse delimited fields.
    		if ( state == 2 ) state = 0;
    		char c;
    		for (int lineIndex=0; lineIndex<line.length();) {
    			c = line.charAt(lineIndex);
    			switch (state) {
    			case 0:
    				if ( isEnclosed && line.startsWith(eLeftValue, lineIndex) ) {
    					lineIndex+=eLeftValue.length();
    					state = 1;
    				}
    				else if ( isThisDelimiter(line,lineIndex,delimValue)) {
    					// marks the end of a column, so add it
    					columns.add(token.toString());
    					col++;
    					token = new StringBuilder();
    					lineIndex+=delimValueLength;
    					if ( _parms.isDelimiterWhiteSpace() ) {
    						state = 2;
    					}
    				}                                                                                 
    				else {
    					token.append(c);
    					++lineIndex;
    				}
    				break;

    			case 1:  // handling enclosed fields and previously encountered left encl
    				if (isEnclosed && line.startsWith(eRightValue, lineIndex)) {
    					// embedded right enclosure
    					lineIndex+=eRightValue.length();
    					state = _parms.isEnclosureRightDouble()?3:4;
    				}  
    				else {
    					token.append(c);
    					++lineIndex;
    				}
    				break;

    			case 2:
    				// Delimiter whitespace.  Handle consecutive delimiters.
    				if ( !isThisDelimiter(line,lineIndex,delimValue) ) {
    					if ( isEnclosed && line.startsWith(eLeftValue, lineIndex )) {
    						state = 1;
    						token = new StringBuilder();
    						//                  ++recIndex;
    						lineIndex+=eLeftValue.length();

    					}
    					else {
    						state = 0;
    						++lineIndex;
    						token.append(c);
    					}
    				}
    				else{  // Eat any consecutive delimiters
    					lineIndex+=delimValueLength;  
    				}
    				break;
    			case 3: 
    				// The previous char was right encl.  There are two supported ways of 
    				// handling embedded right enclosures.  
    				// Case 3 is the default where embedded
    				// right enclosures were be doubled.  SQL*Loader and external tables expects this.
    				if ( isThisDelimiter(line,lineIndex,delimValue) ) {
    					// marks the end of a column, so add it 
    					columns.add(token.toString());
    					col++;
    					token = new StringBuilder();
    					lineIndex+=delimValueLength;
    					if ( _parms.isDelimiterWhiteSpace() ) {
    						state = 2;
    					} else {
    						state = 0;
    					}
    				} else if (line.startsWith(eRightValue, lineIndex)) {
    					token.append(c);  // Only show 1 (the second) right enclosure 
    					++lineIndex;
    					state = 1;   // And back to normal processing for enclosed
    				}
    				else { // bad format enclosed field.
    					// Remember to warn user and ask (?if want to switch to other enclosed option).  
    					// Show the right enclosure which is kind of bad b/c
    					// the doubled enclosure is shown as single enclosure too.
    					if (errorOffset == -1){
    						errorOffset = lineIndex;
    					}	
    					token.append(eRightValue+c);
    					++lineIndex;
    					state = 5;
    					
    				}
    				break;
    			case 4: 
    				// The previous char was right encl.  There are two supported ways of 
    				// handling embedded right enclosures.  
    				// Case 4 handles the 2nd option where they are not doubled.  This means that delimiters and record
    				// terminators cannot be reliably embedded.     
    				//
    				// If this char is a delimiter, accept the previous
    				// character as an enclosure.  Otherwise, accept the previous char as part of the token.
    				if ( isThisDelimiter(line,lineIndex,delimValue) ) {
    					// marks the end of a column, so add it 
    					columns.add(token.toString());
    					col++;
    					token = new StringBuilder();
    					lineIndex+=delimValueLength;
    					if ( _parms.isDelimiterWhiteSpace() ) {
    						state = 2;
    					} else {
    						state = 0;
    					}
    				} else if (line.startsWith(eRightValue, lineIndex)) {
    					token.append(eRightValue);
    					++lineIndex;
    					// keep state = 3;
    				}
    				else { // Use the right enclosure as part of the token because the character following
    					// it was not a delimiter.
    					token.append(eRightValue+c);
    					++lineIndex;
    					state = 1;
    				}
    				break;
    			
    			case 5:  // Error encountered.  No right answer here, just 
    					// add the rest of the row to the column
    				token.append(c);
    				++lineIndex;
    				break;
    			}
    		}

    		if ( state == 0 ) {
    			columns.add(token.toString());  
    			//row++;
    			col = 0;
    			token = new StringBuilder();
    		}
    		else if (state == 1){	// left enclosure without right enclosure, show left enclosure part of token
    			columns.add(eLeftValue+token.toString());
    			if (errorOffset == -1){
    				errorOffset = line.length()-1;
    			}
    			
    		}

    		else if ( state != 0 ) {
    			columns.add(token.toString());
    		}
    	} else {
    		columns.add(line);
    	}
    	if (errorOffset != -1){
    		//String msg = DataImportArb.format(DataImportArb.DATA_FORMAT_EXCEPTION, new Object[] {errorOffset+1});
    		String msg = translate(ReadResources.ENCLOSURE_ERR) + " " + (errorOffset+1);
    		throw new DataFormatException(msg,errorOffset+1,line, columns.toArray(new String[columns.size()]));
    	}
    	return  columns.toArray(new String[columns.size()]);
    }
      
      private boolean isThisDelimiter(String record, int lineIndex, String delimValue){
        if (_parms.isDelimiterWhiteSpace()){
          if (record.startsWith(delimValue, lineIndex ) || 
              record.startsWith(" ", lineIndex)){
            return true;
          }
          else return false;
        }
        else if (record.startsWith(delimValue, lineIndex)){
            return true;
        }
        return false;
      }

     
     /*
     * (non-Javadoc)
     * 
     * @see oracle.dbtools.raptor.data.core.DataReader#getRowCount()
     */
    public boolean hasMoreRows() {
    	// Some housekeeping first to keep track of rows actually processed.
    	boolean currentRowRead=m_currentRowRead;
    	m_currentRowRead=false;
    	try {
    		if (!currentRowRead){  // has the last row checked been "read" by caller?
    			return true; // No, this check provides support for multiple hasMoreRows calls
    		}
    		if (!_parms.isLimitRows() || m_rowsRead < _parms.getLimitRows()){

    			//Just read the line and remember, a subsequent to call to realine() by the
    			// user will use this line, split it into columns and return as an array.
    			m_currentRow = readLine(_reader);  

    			if (m_currentRow!=null) {
    				return true;
    			}
    		}	
    	} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
			m_currentRow=null;
		}
		return false;
    }	
    
    /**
     * Open the reader.  In the case of a sequential reader like this,
     * open and start are the same, so defer to start.
     * 
      */
    public void open() {
    	
    }
    
    
    /**
     * Sets the reader to the start of the file.
     * start()
     * 
     */
    public void start(){
    	try {
    		if (_reader!=null){
    			_reader.close();
    		}
    		_reader = getReader();
    		
    	} catch (IOException e) {
    		LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
    	}
    	m_currentRow = null;   	
    }
    
    /**
     * Get a buffered reader
     * 
     * @return the reader.
     */
    private BufferedReader getReader() {
        BufferedReader tmpReader = null;
        m_currentRow=null;
        try {
            tmpReader = new BufferedReader (new InputStreamReader(_inputStream, _parms.getEncoding())); 
        } catch (Exception e) {
        }
        return tmpReader;
    }
    
    /**
     * Close the reader
     * 

     */
 
    public void close(){
    	try {
    		if (_reader!=null){
    			_reader.close();
    		}
        } catch (IOException e) {
    		LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
    	}
    	m_currentRow = null;
    	_reader = null;
    }

   
 
    /**
     * Main
     * 
     * @param args
     */
    public static void main(String[] args) {
    	try {
    		ReadServiceDelimited reader = new ReadServiceDelimited(new FileInputStream(new File("d:/family.csv")),null); //$NON-NLS-1$
    		//System.out.println("col count:" + reader.getColumnCount()); //$NON-NLS-1$
    		//for (int i = 0; i < reader.getColumnCount(); i++) {
    			//           System.out.println("col " + i + "width :" + reader.getWidth(i)); //$NON-NLS-1$ //$NON-NLS-2$
    		//}
    	}catch (Exception e){
    		System.out.println(e.getMessage());
    	}
    }

    /**
     * readline
     * 
     * @return
     */
    public Object[] readline() throws DataFormatException {  
        // Note hasMoreRows did the read into m_currentRow.
    	Object[] colArray = splitRow(m_currentRow);
    	m_currentRowRead=true;
        return colArray;
    }

    private String readLine(BufferedReader reader) throws IOException {
    	if (!_parms.isStandardTerminator()){         
    		String recordTerminator = _parms.getTerminator();
    		char[] buffer = new char[1];
    		int num_read = reader.read(buffer);
    		// If we're at the end of the file, then return null
    		if ( num_read == -1 ) {
    			return null;
    		}
    		++m_rowsRead;
    		int totalChars=num_read;
    		StringBuffer phys_record = new StringBuffer();
    		phys_record.append(buffer);
    		String phys_record_end="";
    		while (true) {
    			num_read = reader.read(buffer);
    			totalChars+=num_read;  
    			if (num_read != -1){ 
    				
    				phys_record.append(buffer);
    				if (phys_record.length() >=recordTerminator.length() && recordTerminator.length()>0){
    					phys_record_end=phys_record.substring(phys_record.length()-recordTerminator.length(),phys_record.length());
    					if (phys_record_end.equals( recordTerminator )){
    						return phys_record.substring(0, phys_record.length()-recordTerminator.length());	
     					}
    				}	
				
    			}
    			
    			else return (phys_record.length() == 0) ? null : phys_record.toString(); 
    		}
    	} else {  
    		return reader.readLine();
    	}  	
    }
    
 
}