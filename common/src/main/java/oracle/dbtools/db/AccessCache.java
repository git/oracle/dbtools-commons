/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import oracle.dbtools.common.utils.TriState;
import oracle.dbtools.util.Logger;

public final class AccessCache {
	private static final Map<ConnectionIdentifier.Key, AccessCache> caches = new HashMap<ConnectionIdentifier.Key,AccessCache>();
	
	private synchronized static AccessCache getAccessCache(ConnectionIdentifier id) {
		AccessCache instance = caches.get(id.getIdentifierKey());
		if ( instance == null ) {
			instance = new AccessCache(id);
			caches.put(id.getIdentifierKey(), instance);
		}
		return instance;
	}
	
	public static TriState hasAccessCached(ConnectionIdentifier id, String s) {
		return getAccessCache(id).hasAccessCached(s);
	}
    public static boolean hasAccess(ConnectionIdentifier id, String s) {
    	return getAccessCache(id).hasAccess(s);
    }
    public static boolean checkAccess(ConnectionIdentifier id, String s) {
    	return getAccessCache(id).checkAccess(s);
    }
    
    private  Map<String, Boolean> m_accessCache = new HashMap<String, Boolean>();
    private final ConnectionIdentifier conn;
    
    private AccessCache(ConnectionIdentifier conn) {
    	this.conn = conn;
    }
    
    private TriState hasAccessCached(String s) {
    	String key = s.toLowerCase();
        if ( m_accessCache.containsKey(key)){
            if ( m_accessCache.get(key) == Boolean.TRUE ){
            	return TriState.TRUE;
            }
            return TriState.FALSE;
        }
        else return TriState.UNDEF;
    }    

    private final boolean hasAccess(String s) {
    	String key = s.toLowerCase();
    	if ( m_accessCache.containsKey(key)){
    		return m_accessCache.get(key);
    	} else {
    		boolean hasAccess = false;
    		if ( conn != null ) {
    			hasAccess = checkAccess(key);
    			m_accessCache.put(key, Boolean.valueOf(hasAccess));
    		}
    		return hasAccess;
    	}

    }
    
    private boolean checkAccess(String s) {
    	return DBUtil.getInstance(conn).checkAccess(s);
    }
}
