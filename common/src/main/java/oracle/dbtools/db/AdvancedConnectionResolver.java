/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * The Class AdvancedConnectionResolver.
 */
public class AdvancedConnectionResolver extends DefaultConnectionResolver {
		/** The _loaded connections. */
	private ArrayList<ConnectionDetails> _loadedConnections = new ArrayList<ConnectionDetails>();

	/* (non-Javadoc)
	 * @see oracle.dbtools.db.DefaultConnectionResolver#getConnectionType(java.sql.Connection)
	 */
	@Override
	protected String getConnectionTypeImpl(Connection conn) {
		for(ConnectionDetails connDetails:_loadedConnections){
			if(connDetails.getConn()==conn){
				return connDetails.getType();
			}
		}
		return ORACLE_TYPE;
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.db.DefaultConnectionResolver#getConnection(java.lang.String)
	 */
	@Override
	protected Connection getConnectionImpl(String name) throws Exception {
		if (name.startsWith("\"")) { //this is not a connection name, rather its a URL
			return getConnectionFromURL(strip(name,'"'));
		} else {
			return super.getConnection(name);
		}
	}

	/**
	 * Strip.
	 *
	 * @param connStr the conn str
	 * @param stripChar the strip char
	 * @return the string
	 */
	private String strip(String connStr,char stripChar) {
		connStr = connStr.trim();
		if (connStr.charAt(0) == stripChar && connStr.charAt(connStr.length() - 1) == stripChar) {
			return connStr.substring(1, connStr.length() - 1);
		}
		return connStr;
	}

	/**
	 * Gets the connection from url.
	 *
	 * @param url the url
	 * @return the connection from url
	 */
	private Connection getConnectionFromURL(String url) {
		Connection conn = null;
		ConnectionDetails currentConnDetails = new ConnectionDetails(url);
		for (ConnectionDetails connectionDetails : _loadedConnections) {
			if (currentConnDetails.getUrl().equals(connectionDetails.getUrl())) {
				//connection loaded/open already, return the same one
				conn = connectionDetails.getConn();
				break;
			}
		}
		if (conn == null) {
			_loadedConnections.add(currentConnDetails);
			conn = currentConnDetails.getConn();
		}
		return conn;
	}
}
