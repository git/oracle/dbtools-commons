/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Properties;

// TODO: Auto-generated Javadoc
/**
 * The Class ConnectionDetails.
 */
public class ConnectionDetails {
	
	/** The _type. */
	private String _type = null;
	
	/** The _url. */
	private String _url = null;
	
	/** The _driver. */
	private String _driver =null;
	
	/** The _conn. */
	private Connection _conn = null;
	
	private Properties _properties = new Properties();
	
	/** The _type driver map. */
	private static HashMap<String,String> _typeDriverMap = new HashMap<String,String>(); 
	
	{
		registerDriver("jdbc:jtds:sqlserver","net.sourceforge.jtds.jdbc.Driver");
		registerDriver("jdbc:jtds:sybase","net.sourceforge.jtds.jdbc.Driver");
		registerDriver("jdbc:as400:","com.ibm.db2.jcc.DB2Driver");
		registerDriver("jdbc:db2:","com.ibm.db2.jcc.DB2Driver");
		registerDriver("jdbc:informix","com.ibm.informix.Driver");
		registerDriver("jdbc:mysql:","com.mysql.jdbc.Driver");
		registerDriver("jdbc:postgresql:","org.postgresql.Driver");
		registerDriver("jdbc:teradata:","com.teradata.jdbc.TeraDriver");
		registerDriver("jdbc:oracle:thin:","oracle.jdbc.OracleDriver");
		registerDriver("jdbc:redshift:","com.amazon.redshift.jdbc.Driver");
		registerDriver("jdbc:oracle:orest:","oracle.dbtools.jdbc.orest.Driver");
		registerDriver("http:","oracle.dbtools.jdbc.Driver");
		registerDriver("https:","oracle.dbtools.jdbc.Driver");
	}
	
	/**
	 * Instantiates a new connection details.
	 *
	 * @param url the url
	 */
	public ConnectionDetails(String url) {
		setUrl(url);
		for(String type:_typeDriverMap.keySet()){
			if(getUrl().startsWith(type)){
				setType(type);
				setDriver(_typeDriverMap.get(type));
				break;
			}
		}
	}
	
	/**
	 * Register driver.
	 *
	 * @param type the type
	 * @param driver the driver
	 */
	private static void registerDriver(String type, String driver) {
		_typeDriverMap.put(type,driver);
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return _type;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		_type = type;
	}
	
	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return _url;
	}
	
	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		if(url.startsWith("[")){
			url = processURL(url);
		}
		url=resolveHashAmpersand(url);
		_url = url;
	}
	
	private String processURL(String url) {
		int endIndex = url.indexOf("]");
		String argStr = url.substring(1,endIndex);
		processArgs(argStr);
		url=url.substring(endIndex+1);
		return url;
	}

	private void processArgs(String argStr) {
		String[] args = argStr.split(",");
		for(String arg:args){
			String[] argItems = arg.split("=");
			if (argItems.length>1)
			_properties.put(argItems[0],argItems[1]);
		}
	}

	/**
	 * Resolve hash ampersand.
	 *
	 * @param url the url
	 * @return the string
	 */
	private String resolveHashAmpersand(String url) {
		return url.replace('#', '&');
	}
	
	/**
	 * Gets the driver.
	 *
	 * @return the driver
	 */
	public String getDriver() {
		return _driver;
	}
	
	/**
	 * Sets the driver.
	 *
	 * @param driver the new driver
	 */
	public void setDriver(String driver) {
		_driver = driver;
	}
	
	/**
	 * Gets the conn.
	 *
	 * @return the conn
	 */
	public Connection getConn() {
		if(_conn == null){
			try{
				Class drivercls= Class.forName(getDriver());
				Driver driver = (Driver)drivercls.newInstance();
				DriverManager.registerDriver(driver);
				//setup the connection with explicit USERNAME and PASSWORD if appropriate
				if(_properties.size() == 2 && _properties.containsKey("USERNAME") && _properties.containsKey("PASSWORD") ){
					_conn = DriverManager.getConnection(getUrl(),_properties.getProperty("USERNAME"),_properties.getProperty("PASSWORD"));
				} else if(_properties.size() >0 ){
					_conn = DriverManager.getConnection(getUrl(),_properties);
				} else {
					_conn = DriverManager.getConnection(getUrl());
				}
			} catch(Exception e) {
				//ignore exceptions just return  null. This is expected behavior
			}
		}
		return _conn;
	}
	
	/**
	 * Sets the conn.
	 *
	 * @param conn the new conn
	 */
	public void setConn(Connection conn) {
		_conn = conn;
	}
}
