/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.sql.Connection;

/**
 * Identifier that can be used identify a session or connection.
 * <p>
 * Certain subsystems made need to cache information about a session or connection. In some cases, there is a direct relationship between
 * a {@link Connection} and a session. However, it other cases, a Connection might be used in conjuction with proxy users to represent multiple
 * sessions over time.
 * <p>
 * @author jmcginni
 *
 */
public interface ConnectionIdentifier {
	public abstract static class Key {
		public abstract int hashCode();
		public abstract boolean equals(Object o);
	}
	
	/**
	 * A default implementation of the Connection id key.
	 * @author jmcginni
	 *
	 */
	public static final class DefaultKey extends Key {
		private final Object delegate;
		
		public DefaultKey(Object delegate) {
			this.delegate = delegate;
		}

		@Override
		public int hashCode() {
			return delegate.hashCode();
		}

		@Override
		public boolean equals(Object o) {
			return (o instanceof DefaultKey) && delegate.equals(((DefaultKey)o).delegate);
		}
	}
	/**
	 * Retrieve the Connection object associated with the current identifier instance. Consumers of ConnectionIdentifiers can use the Connection to refresh
	 * a cache or other operation on the session, but will not directly persist the Connection.
	 * @return the Connection
	 */
	Connection getConnection();
	
	/**
	 * The key that can be used to identify the session. In some implementations, this may be the underlying Connection. In other implementations, it may be some other
	 * object type. The key should implement hashCode() and equals() in a way that allows for use in maps and caches.
	 * @return
	 */
	Key getIdentifierKey();
}
