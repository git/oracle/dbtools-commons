/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.sql.Connection;
import java.util.Properties;

/**
 * Utility class for managing the mapping of Connection objects, names, and types.
 * @author jmcginni
 *
 */
public abstract class ConnectionResolver
{
    public static final String ORACLE_TYPE = "Oracle"; //$NON-NLS-1$
    public static final String TIMES_TEN_TYPE = "TimesTen"; //$NON-NLS-1$
    public static final String GENERIC_TYPE = "Generic"; //$NON-NLS-1$
    
    /**
     * Lazy initializer for the connection resolver. 
     * @author jmcginni
     *
     */
    private static class Holder {
    	private static ConnectionResolver sInstance = new DefaultConnectionResolver();
    }
    
    // Package local - called by ConnectionSupport to update the active resolver
    static void setConnectionResolver(ConnectionResolver impl) {
    	Holder.sInstance = impl;
    }
    
    /**
     * Retrieve the Connection associated to the given name.
     * @param name
     * @return
     * @throws Exception
     */
    public static Connection getConnection(String name) throws Exception {
    	return Holder.sInstance.getConnectionImpl(name);
    }
    
    /**
     * Retrieve the name associated with the specified Connection instance.
     * @param conn
     * @return
     */
    public static String getConnectionName(Connection conn) {
    	return Holder.sInstance.getConnectionNameImpl(conn);
    }
    
    /**
     * Retrieve a list of all known connection names
     * @return
     */
    public static String[] getConnectionNames() {
    	return Holder.sInstance.getConnNamesImpl();
    }
    
    /**
     * Retrieve the details for the specified connection name
     * @param name
     * @return
     */
    public static Properties getConnectionInfo(String name) {
    	return Holder.sInstance.getConnectionInfoImpl(name);
    }
    
    /**
     * Whether the specified Connection is to an Oracle instance
     * @param conn
     * @return
     */
    public static boolean isOracle(Connection conn) {
    	return Holder.sInstance.isOracleImpl(conn);
    }
    
    /**
     * Whether the specified Connection is to a TimesTen instance
     * @param conn
     * @return
     */
    public static boolean isTimesTen(Connection conn) {
    	return Holder.sInstance.isTimesTenImpl(conn);
    }
    
    /**
     * Retrieve the connection type for the Connection
     * @param conn
     * @return
     */
    public static String getConnectionType(Connection conn) {
    	return Holder.sInstance.getConnectionTypeImpl(conn);
    }
    
    /** 
     * Retrieve the qualified connection name using just the short name
     * @param name
     * @return
     */
    public static String getQualifiedConnectionName(String name){
    	return Holder.sInstance.getQualifiedName(name);
    }
    
    /**
     * Retrieve a unique connection (cloned connection) using a connection name
     * @param name the qualified name of the connection
     * @return
     * @throws Exception
     */
    public static Connection getUniqueConnection(String name) throws Exception {
			return Holder.sInstance.getUniqueConnectionImpl(name);
		}
    
    protected abstract Connection getConnectionImpl( String name ) throws Exception;
    
    protected abstract String getConnectionNameImpl( Connection conn );
 
    protected abstract String[] getConnNamesImpl();
    
    protected abstract Properties getConnectionInfoImpl( String name );
    
    protected abstract boolean isOracleImpl(Connection conn);
    
    protected abstract boolean isTimesTenImpl(Connection conn);
    
    protected abstract String getConnectionTypeImpl(Connection conn);
    
	  protected abstract String getQualifiedName(String name) ;
	  
	  protected abstract Connection getUniqueConnectionImpl( String name ) throws Exception;

}
