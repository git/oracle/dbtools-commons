/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.lang.reflect.Constructor;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.common.utils.Version;
import oracle.dbtools.raptor.query.Bind;
import oracle.dbtools.raptor.query.Parser;
import oracle.dbtools.raptor.utils.ExceptionHandler;
import oracle.dbtools.raptor.utils.MessageLogging;
import oracle.dbtools.util.Service;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.CLOB;

// import oracle.dbtools.raptor.utils.DBObject;

/**
 * DBUtil instances can be used to execute SQL against a database Connection. Services provided include assisting with extraction of bind variable names from queries, preparing
 * statements for execution, quoting identifier names, and extracting query results into several different formats for consumption. A DBUtil instance is associated with a specific
 * Connection instance, and is acquired using a {@link ConnectionIdentifier}. For legacy support, DBUtil instances can also be create with either a Connection instance or a fully 
 * qualified name of the Connection.
 * <p>
 * DBUtil utilizes a very specific connection locking policy. Instance methods that access a Connection can be divided into two groups. Group one consists of methods that entirely
 * encapsulate access to the Connection. For these methods, DBUtil will internally acquire and release the Connection lock. Group two consists of methods that either take as an
 * argument some object that requires the Connection lock to create/acquire or returns a lock protected Object; as an example, the method executeQuery( String, Map ) returns a
 * ResultSet, which requires holding the Connection lock to access the data. For methods in group 2, the caller is responsible for acquiring and releasing the Connection lock.
 * Internally, DBUtil may audit the proper lock usage. Methods requiring callers to hold the Connection lock will be explicitly documented as such.
 * <p>
 * Different databases can have individual implementations of DBUtil. New implementations are registered via the ServiceLoader framework. The interface 
 * {@link DBUtilImplFactory} provides the Service for creating new DBUtil implementations.
 * <p>
 * @author klrice
 * @author jmcginni
 */
public abstract class DBUtil {
    protected static final String VERSION_10G_STRING = "10.1"; //$NON-NLS-1$
    protected static final String EMPTY_STRING = ""; //$NON-NLS-1$
    private static final String QUOTE_DELIMITER = "\""; //$NON-NLS-1$
    protected static final Logger LOGGER = Logger.getLogger(DBUtil.class.getName());
    public static final Object NULL_VALUE = new Object();
    public static final String SUBSTITUTION = "substitution"; //$NON-NLS-1$

    public static final String PREFERRED_FETCH_SIZE = "PreferfedFetchSize"; //$NON-NLS-1$
    

    private static Map<Thread, Statement> s_execThreadMap = new HashMap<Thread, Statement>();

    protected static String s_prodName;

    /**
     * Sets the nane of the current product. DBUtil may use this name when populating certain data dictionary views with information about active sessions.
     * 
     * @param prodName
     *            The current product name.
     */
    public static void setProductName(String prodName) {
        s_prodName = prodName;
    }

    // private static int _currentErrorNum;

    private static String m_currentErrorString;

    private static List<DBUtilImplFactory> s_staticFactories = new ArrayList<DBUtilImplFactory>();
    private static ServiceLoader<DBUtilImplFactory> s_loader = ServiceLoader.load(DBUtilImplFactory.class);
    
    /**
     * Register a new DBUtilImplFactory for creating new DBUtil implementations.
     * @param factory
     */
    public static void registerImplFactory(DBUtilImplFactory factory) {
        s_staticFactories.add(factory);
    }
    
    /**
     * Retrieves the DBUtil instance for the specified connection
     * 
     * @param conn
     *            the Connection
     * @return the DBUtil instance associated with the Connection.
     */
    public static DBUtil getInstance(Connection conn) {
        return getInstance(null, conn);
    }

    /**
     * Retrieves the DBUtil instance for the specified connection
     * 
     * @param cName
     *            the name of the Connection
     * @return the DBUtul instance associated with the Connection
     */
    public static DBUtil getInstance(String cName) {
        DBUtil instance = null;
        try {
        	Connection conn = ConnectionResolver.getConnection(cName);
        	if ( conn != null ) {
        		instance = getInstance(cName, conn);
        	}
        } catch (Exception ex) {
            // TODO: Handle Exception
        }
        return instance;
    }
    
    /**
     * Create a new DBUtil implementation
     * @param cName the connection name
     * @param conn the Connection
     * @return
     */
    private static DBUtil createImpl(ConnectionIdentifier id) {
        String cType = ConnectionResolver.getConnectionType(id.getConnection());

        DBUtil impl = null;
        
        synchronized(s_loader) {
            // First, check with the ServiceLoader for a DBUtilImplFactory that can create the implementation. We need
            // to synchronize access to the ServiceLoader, as it is not thread-safe
            for ( DBUtilImplFactory factory : s_loader ) {
                impl = factory.createImpl(cType, id);
                if ( impl != null ) {
                    break;
                }
            }
        }
        
        if ( impl == null ) {
            // look for legacy registrations
            for ( DBUtilImplFactory factory : s_staticFactories ) {
                impl = factory.createImpl(cType, id);
                if ( impl != null ) {
                    break;
                }
            }
        }
        
        // Fallback is the ThirdPartyUtil
        if ( impl == null ) {
            impl = new ThirdPartyUtil(id);
        }
        return impl;
    }

    /**
     * Get the appropriate DBUtil instance for a given connection. 
     * @param cName The name of the connection
     * @param conn the Connection
     * @return the appropriate DBUtil instance
     */
    private static DBUtil getInstance(String cName, Connection conn) {
        assert conn != null;
        
        ConnectionIdentifier id = DefaultConnectionIdentifier.createIdentifier(cName, conn);
        return getInstance(id);
    }
    
    /**
     * Get the DBUtil instance for the given connection.
     * 
     * @param id The identifier of the connection
     * @return the DBUtil instance
     */
    public static DBUtil getInstance(ConnectionIdentifier id) {
    	return createImpl(id);
    }

    public static boolean cancelExecution(Thread t) {
        synchronized (s_execThreadMap) {
            Statement stmt = s_execThreadMap.get(t);
            if (stmt != null) {
                try {
                    stmt.cancel();
                    return true;
                } catch (SQLException e) {
                    // Ignore the exception on the cancel request. If a real error occurs, the
                    // exec will throw
                }
            }
        }
        return false;
    }

    protected final Connection m_conn;
    protected final ConnectionIdentifier mID;
    private final String mName;

    private boolean m_raiseError;
    private ThreadLocal<SQLException> m_lastException = new ThreadLocal<SQLException>();


    /*
     * Only allow instance creation via our static factory methods.
     */
    protected DBUtil(ConnectionIdentifier id) {
    	m_conn = id.getConnection();
    	mName = id.toString();
    	mID = id;
    }
    
    /**
     * Retrieve the Connection identifier for this DBUtil instance
     * @return the connection id
     */
    public final ConnectionIdentifier getConnectionIdentifier() {
    	return mID;
    }

    private synchronized void setCurrentStatement(Statement stmt) {
        synchronized (s_execThreadMap) {
            s_execThreadMap.put(Thread.currentThread(), stmt);
        }
    }

    private synchronized void clearCurrentStatement() {
        synchronized (s_execThreadMap) {
            s_execThreadMap.remove(Thread.currentThread());
        }
    }

    /**
     * Sets the name of action in v$session via dbms_application_info
     * 
     * @param action
     *            the name of the action to set in v$session
     */
    public void setDBAction(String action) {
        throw new UnsupportedOperationException("setDBAction"); //$NON-NLS-1$
    }

    //
    // set module and action in v$session
    //
    /**
     * Sets the name of action and module in v$session via dbms_application_info
     * 
     * @param action
     *            the name of the action to set in v$session
     */
    public void setDBModuleAction(String action) {
        throw new UnsupportedOperationException("setDBModuleAction"); //$NON-NLS-1$
    }

    /**
     * Executes a query, returning as a result the value from the first row and column. Both Oracle and non-Oracle queries are supported. In general, executeReturnOneCol should
     * only be used when the result is known to be a single value; however, this is not a requirement and any additional values will silently be ignored.
     * 
     * @param query
     *            the SQL query requiring execution
     * @param binds
     *            a map of names and values to be used as bind variables and values
     * @return the value of the first column of the first row of the query
     */
    public String executeReturnOneCol(String query, Map<String, ? extends Object> binds) {
        return executeNonOracleReturnOneCol(query, binds);
    }
    
    /**
     * Executes a query, returning as a result the value from the first row and column. Both Oracle and non-Oracle queries are supported. In general, executeReturnOneCol should
     * only be used when the result is known to be a single value; however, this is not a requirement and any additional values will silently be ignored.
     * 
     * @param query
     *            the SQL query requiring execution
     * @return the value of the first column of the first row of the query
     */
    public String executeReturnOneCol(String query) {
        return executeNonOracleReturnOneCol(query, null);
        
    }

    /**
     * Executes a query, returning as a result the value from the first row and column. Non-Oracle bind syntax is assumed for the Connection type. The SQL query will be parsed for
     * bind names (in the form <code>:&lt;name&gt;</code>). Bind names will be replaced in the SQL with <code>?</code> and the value associated with the specified name in the
     * specified map of bind values will be bound, by position, into the statement. In general, executeReturnOneCol should only be used when the result is known to be a single
     * value; however, this is not a requirement and any additional values will silently be ignored.
     * 
     * @param query
     *            the SQL query requiring execution
     * @param binds
     *            a map of names and values to be used as bind variables and values
     * @return the value of the first column of the first row of the query
     */
    private String executeNonOracleReturnOneCol(final String query, final Map<String, ?> binds) {
        LockableOperation<String> callable = new OperImpl<String>() {
            public String call() throws SQLException {
                // have to bind by position
                PrepareResult prep = prepareNonOracleSql(query, binds);
                String sql = prep.getSQL();
                List<?> bindList = prep.getBinds();
                sql = sql.replaceAll("\n", " "); //$NON-NLS-1$ //$NON-NLS-2$
                PreparedStatement stmt = null;
                ResultSet rset = null;
                try {
                    stmt = m_conn.prepareStatement(sql);
                    // LOGGER.info(Messages.getString("DBUtil.11") + sql); //$NON-NLS-1$ //RJW
                    bind(stmt, bindList);
                    rset = stmt.executeQuery();
                    rset.next();
                    return rset.getString(1);
                } finally {
                    cleanup(stmt, rset);
                }
            }
            
            public String getSQL() {
                return query;
            }
        };

        return lockForOperation(callable, null);
    }

    /**
     * Executes a query, returning as a result the value from the first row and column. The Connection is assumed to support Oracle bind name syntax. In general,
     * executeReturnOneCol should only be used when the result is known to be a single value; however, this is not a requirement and any additional values will silently be ignored.
     * 
     * @param query
     *            the SQL query requiring execution
     * @param binds
     *            a map of names and values to be used as bind variables and values
     * @return the value of the first column of the first row of the query
     */
    public final String executeOracleReturnOneCol(final String query, final Map<String, ?> binds) {
        LockableOperation<String> callable = new OperImpl<String>() {
            public String getSQL() {
                return query;
            }
            public String call() throws SQLException {
                PreparedStatement statement = null;
                ResultSet rset = null;
                try {

                    statement = m_conn.prepareCall(query);
                    // LOGGER.info(Messages.getString("DBUtil.12") + query); //$NON-NLS-1$ //RJW
                    bind(query, statement, binds);
                    rset = statement.executeQuery();
                    if (rset.next()) return rset.getString(1);
                } finally {
                    cleanup(statement, rset);
                }
                return null;
            }
        };

        return lockForOperation(callable, null);
    }

    /**
     * Executes a query, returning as a result the value from the first row and column. Binds will be executed by position and the SQL query is assumed to be properly formatted for
     * positional binds. In general, executeReturnOneCol should only be used when the result is known to be a single value; however, this is not a requirement and any additional
     * values will silently be ignored.
     * 
     * @param query
     *            the SQL query requiring execution
     * @param binds
     *            a List of bind values
     * @return the value of the first column of the first row of the query
     */
    public final String executeReturnOneCol(final String query, final List<?> binds) {
        LockableOperation<String> callable = new OperImpl<String>() {
            
            public String getSQL() {
                return query;
            }
            public String call() throws SQLException {
                PreparedStatement statement = null;
                ResultSet rset = null;
                try {
                    statement = m_conn.prepareCall(query);
                    // LOGGER.info(Messages.getString("DBUtil.13") + query); //$NON-NLS-1$ //RJW
                    bind(statement, binds);
                    rset = statement.executeQuery();
                    if (rset.next()) {
                        return rset.getString(1);
                    }
                } finally {
                    cleanup(statement, rset);
                }
                return null;
            }
        };

        return lockForOperation(callable, null);
    }

    /**
     * Executes an update statement, with positional binds.
     * 
     * @param query
     *            the String containing the update statement
     * @param binds
     *            a List of bind values
     * @return the number of rows affected
     * @see Statement.executeUpdate
     */
    public final int executeUpdate(final String query, final List<?> binds) {
        LockableOperation<Integer> callable = new OperImpl<Integer>() {
            
            public String getSQL() {
                return query;
            }
            public Integer call() throws SQLException {
                PreparedStatement statement = null;
                try {
                    statement = m_conn.prepareCall(query);
                    // LOGGER.info(Messages.getString("DBUtil.14") + query); //$NON-NLS-1$ //RJW
                    if (binds != null) {
                        bind(statement, binds);
                    }
                    return statement.executeUpdate();

                } finally {
                    cleanup(statement);
                }
            }
        };

        return lockForOperation(callable, 0);
    }

    /**
     * Executes an update statement, with named binds.
     * 
     * @param query
     *            the String containing the update statement
     * @param binds
     *            a Map of bind names and values
     * @return the number of rows affected
     * @see Statement.executeUpdate
     */
    public final int executeUpdate(final String query, final Map<String, ?> binds) {
        LockableOperation<Integer> callable = new OperImpl<Integer>() {
            
            public String getSQL() {
                return query;
            }
            public Integer call() throws SQLException {
                PreparedStatement statement = null;
                try {
                    statement = prepareExecute(query, binds);
                    return statement.executeUpdate();

                } finally {
                	cleanup(statement);
                }
            }
        };

        return lockForOperation(callable, 0);
    }
    public final boolean execute(String query) {
        return execute(query, null, null);
    }
    /**
     * Executes an arbitrary SQL statement with positional binds.
     * 
     * @param query
     *            the String containing the SQL statement
     * @param binds
     *            a List of bind values
     * @return the result of the execute
     * @see Statement.execute
     */
    public final boolean execute(String query, List<?> binds) {
        return execute(query, binds, null);
    }

    /**
     * Executes an arbitrary SQL statement with positional binds.
     * 
     * @param query
     *            the String containing the SQL statement
     * @param binds
     *            a List of bind values
     * @param msg
     * @return the result of the execute
     * @see Statement.execute
     */
    public final boolean execute(final String query, final List<?> binds, String msg) {
        LockableOperation<Boolean> callable = new OperImpl<Boolean>() {
            
            public String getSQL() {
                return query;
            }
            public Boolean call() throws SQLException {
                PreparedStatement statement = null;
                try {
                    statement = m_conn.prepareCall(query);
                    // LOGGER.info(Messages.getString("DBUtil.16") + query); //$NON-NLS-1$ //RJW
                    if (binds != null) bind(statement, binds);
                    return statement.execute();
                } finally {
                    cleanup(statement);
                }

            }
        };

        return lockForOperation(callable, Boolean.FALSE);
    }

    /**
     * Prepares a statement for execution. A {@link PreparedStatement} will be created, and the specified Map will be used to bind in values to the bind names in the SQL statement.
     * <p>
     * This method requires that callers hold a lock on the Connection.
     * 
     * @param sql
     *            a String containing the raw SQL to be executed
     * @param binds
     *            a Map of bind names and values
     * @return a PreparedStatement for the specified SQL with the values bound in
     */
    public final PreparedStatement prepareExecute(final String sql, final Map<String, ?> binds) {
        PreparedStatement statement = null;
        try {
            statement = assertLock(new OperImpl<PreparedStatement>() {
                
                public String getSQL() {
                    return sql;
                }
                public PreparedStatement call() throws Exception {
                    PreparedStatement stmt = prepareExecuteImpl(sql, binds);
                    return stmt;
                }
            });
            clearLastException();
        } catch (SQLException e) {
            cleanup(statement);
            handleException(e, sql);
            statement = null;
        } catch (Exception e) {
            // On an exception cleanup the statement
            cleanup(statement);

            reportException(e);
            statement = null;
        }
        return statement;
    }

	private void reportException(Exception e) {
		if (m_raiseError) {
			String cName = ConnectionResolver.getConnectionName(m_conn);
			if ( cName != null ) {
				ExceptionHandler.handleException(e, cName);
			} else {
				ExceptionHandler.handleException(e);
			}
		} else {
		    LOGGER.severe(e.getMessage());
		}
	}

    protected PreparedStatement prepareExecuteImpl(final String sql, final Map<String, ?> binds) throws SQLException {
        PrepareResult prep = prepareNonOracleSql(sql, binds);
        String prepSql = prep.getSQL();
        PreparedStatement stmt = m_conn.prepareCall(prepSql);
        // LOGGER.info(Messages.getString("DBUtil.17") + prepSql); //$NON-NLS-1$ //RJW
        List<?> bindList = prep.getBinds();
        if (bindList.size() > 0) bind(stmt, bindList);
        return stmt;
    }

    /**
     * Executes an arbitrary SQL statement with named binds.
     * 
     * @param query
     *            the String containing the SQL statement
     * @param binds
     *            a Map of bind name/value pairs
     * @return the result of the execute
     * @see Statement.execute
     */
    public final boolean execute(final String sql, final Map<String, ?> binds) {
        LockableOperation<Boolean> callable = new OperImpl<Boolean>() {
            public Boolean call() {
                PreparedStatement statement = prepareExecute(sql, binds);
                // LOGGER.info(Messages.getString("DBUtil.18") + sql); //$NON-NLS-1$ //RJW
                return execute(statement);
            }
        };

        return lockForOperation(callable, false);
    }

    private boolean execute(final PreparedStatement statement) {
        boolean result = false;
        LockableOperation<Boolean> callable = new OperImpl<Boolean>() {
            public Boolean call() throws Exception {
                try {
                    statement.execute();
                    return true;
                } finally {
                    cleanup(statement);
                }
            }
        };

        try {
            result = assertLock(callable);
            clearLastException();
        } catch (SQLException e) {
            handleException(e, null);
        } catch (Exception e) {
            reportException(e);
        }
        return result;
    }

    /**
     * Executes a query using Oracle named bind syntax.
     * <p>
     * This method requires that callers hold a lock on the Connection.
     * 
     * @param query
     *            a String with the text of the SQL statement
     * @param binds
     *            a Map of bind names and associated values
     * @return a ResultSet describing the results of the query
     */
    public final ResultSet executeOracleQuery(final String query, final Map<String, ?> binds) {
        ResultSet wrapper = null;
        LockableOperation<ResultSet> callable = new OperImpl<ResultSet>() {
            public ResultSet call() throws SQLException {
                PreparedStatement statement = null;
                try {
                    statement = m_conn.prepareStatement(query);
                    // LOGGER.info(Messages.getString("DBUtil.19") + query); //$NON-NLS-1$ //RJW
                    if (binds != null) bind(query, statement, binds);
                    setCurrentStatement(statement);
                    ResultSet rs;
                    try {
                        rs = statement.executeQuery();
                    } catch (SQLException e) {
                        throw e;
                    } finally {
                        clearCurrentStatement();
                    }
                    return rs;
                } catch (SQLException e) {
                    cleanup(statement);
                    throw e;
                }

            }
        };

        try {
            wrapper = assertLock(callable);
            clearLastException();
        } catch (SQLException e) {
            handleException(e, query);
        } catch (Exception ex) {
            reportException(ex);
        }
        return wrapper;
    }

    /**
     * Executes a query. If the Connection is not an {@link OracleConnection}, the SQL query will be parsed for bind names (in the form <code>:&lt;name&gt;</code>). Bind names will
     * be replaced in the SQL with <code>?</code> and the value associated with the specified name in the specified map of bind values will be bound, by position, into the
     * statement. OracleConnections will use bind by name syntax.
     * <p>
     * This method requires that callers hold a lock on the Connection.
     * 
     * @param query
     *            a String with the text of the SQL statement
     * @param binds
     *            a Map of bind names and associated values
     * @return a ResultSet describing the results of the query
     */
    public ResultSet executeQuery(String query, Map<String, ?> binds) {
        // Don't bother asserting the lock here, as the method we are delegating to 
        // will do so
        return executeNonOracleQuery(query, binds);
    }

    /**
     * Executes a query against a non Oracle connection. A message formatter is used to convert the statement and its associated bind values into a single string for execution.
     * This method should only be used when we executing a statement that cannot be handled with a {@link PreparedStatement} (for example, MySQL's "SHOW" calls).
     * <p>
     * This method requires that callers hold a lock on the Connection.
     * 
     * @param query
     *            a String with the text of the SQL statement
     * @param binds
     *            a Map of bind names and associated values
     * @return a ResultSet describing the results of the query
     */
    private String getQueryNonOracleQueryUsingMessageFormat(String query, Map<String, ?> binds) {
        /*
         * for(Bind b : parserdBinds){ bindList.add(binds.get(b.getName())); sql = sql.replaceFirst(":"+b.getName(), Integer.toString(index++)); } String formatSql =
         * MessageFormat.format(sql, bindList.toArray());
         */
        PrepareResult pr = prepareNonOracleSqlMsgFormat(query, binds);
        String formatSql = pr.getSQL();
        return formatSql;
    }

    /**
     * Executes a query. The SQL query will be parsed for bind names (in the form <code>:&lt;name&gt;</code>). Bind names will be replaced in the SQL with <code>?</code> and the
     * value associated with the specified name in the specified map of bind values will be bound, by position, into the statement.
     * <p>
     * This method requires that callers hold a lock on the Connection.
     * 
     * @param query
     *            a String with the text of the SQL statement
     * @param binds
     *            a Map of bind names and associated values
     * @return a ResultSet describing the results of the query
     */
    private ResultSet executeNonOracleQuery(String query, Map<String, ?> binds) {
    	ResultSet wrapper = null;
        // have to bind by position
        if (query.contains("{:")) { //$NON-NLS-1$
            query = getQueryNonOracleQueryUsingMessageFormat(query, binds);
        }

        List<Bind> parserdBinds = Parser.getInstance().getBinds(query, true);
        final List<Object> bindList = new ArrayList<Object>();
        String sql = query;
        for (Bind b: parserdBinds) {
            bindList.add(binds.get(b.getName()));
            sql = sql.replaceFirst(":" + b.getName(), "?"); //$NON-NLS-1$ //$NON-NLS-2$
            // sql.replaceAll(b.getName(), "?");
        }

        final String mungedSql = sql;
        LockableOperation<ResultSet> callable = new OperImpl<ResultSet>() {
            public ResultSet call() throws SQLException {
                PreparedStatement stmt = null;
                try {
                    stmt = m_conn.prepareStatement(mungedSql);
                    // LOGGER.info(Messages.getString("DBUtil.24") + mungedSql); //$NON-NLS-1$ //RJW
                    bind(stmt, bindList);
                    ResultSet rs = stmt.executeQuery();
                    return rs;
                } catch (SQLException e) {
                    cleanup(stmt);
                    throw e;
                }
            }
        };

        try {
            wrapper = assertLock(callable);
            clearLastException();
        } catch (SQLException e) {
            handleException(e, mungedSql);
        } catch (Exception e) {
        	reportException(e);
        }
        return wrapper;
    }

    // /* (non-Javadoc)
    // * @see executeQuery(Connection, String, DBObject)
    // */
    // public ResultSet executeQuery(String conn, String query, DBObject
    // dbObject) {
    //
    // return executeQuery(conn, query, dbObject.getObjectBinds(query));
    // }

    /**
     * Executes a query. If the Connection is not an {@link OracleConnection}, the SQL query will be parsed for bind names (in the form <code>:&lt;name&gt;</code>). Bind names will
     * be replaced in the SQL with <code>?</code>. Bind values will be bound by position for both Oracle and non-Oracle Connections.
     * <p>
     * This method requires that callers hold a lock on the Connection.
     * 
     * @param query
     *            a String with the text of the SQL statement
     * @param binds
     *            a List of bind values
     * @return a ResultSet describing the results of the query Executes a query.
     */
    public final ResultSet executeQuery(final String query, final List<?> binds) {
    	ResultSet rs = null;
        LockableOperation<ResultSet> callable = new OperImpl<ResultSet>() {
            public ResultSet call() throws SQLException {
                PreparedStatement statement = null;
                try {
                    statement = prepareQuery(query);
                    // LOGGER.info(Messages.getString("DBUtil.27") + query); //$NON-NLS-1$ //RJW
                    if (binds != null) bind(statement, binds);
                    ResultSet rs = statement.executeQuery();
                    return rs;
                } catch (SQLException e) {
                    cleanup(statement);
                    throw e;
                }

            }
        };

        try {
            rs = assertLock(callable);
            clearLastException();
        } catch (SQLException e) {
            handleException(e, query);
        } catch (Exception e) {
        	reportException(e);
        }
        return rs;
    }
    /**
     * Executes a queries and returns the results as a List of rows. Each row is a List of values. If the Connection is not an {@link OracleConnection}, the SQL
     * query will be parsed for bind names (in the form <code>:&lt;name&gt;</code>). Bind names will be replaced in the SQL with <code>?</code> and the value associated with the
     * specified name in the specified map of bind values will be bound, by position, into the statement. OracleConnections will use bind by name syntax.
     * 
     * @param sql
     *            a String with the text of the SQL statement
     * @param binds
     *            a Map of bind names and values
     * @return a List of List containing the array zero element being the column named and the rest being column values
     */
    public final List<List<?>> executeReturnListofList( final String sql, final Map<String, ?> binds ) {
        LockableOperation<List<List<?>>> callable = new OperImpl<List<List<?>>>() {
            public List<List<?>> call() throws SQLException {
              ResultSet rs = executeQuery(sql, binds);
                try {
                  List<List<?>> ret = new ArrayList<List<?>>();
                    List< Object> row = null;
                    final String[] colNames = new String[ rs.getMetaData().getColumnCount() ];
                    row = new ArrayList<Object>();
                    for (int i = 0; i < colNames.length; i++) {
                        row.add(rs.getMetaData().getColumnName(i + 1));
                    }
                    ret.add(row);
                    while( rs.next() ) {
                        row = new ArrayList<Object>();
                        for (int i = 0; i < colNames.length; i++) {
                            row.add(rs.getObject(i + 1));
                        }
                        ret.add(row);
                    }

                    return ret;
                } finally {
                    try {
                        cleanup(rs);
                    } catch (Exception e) {
                    }
                }
            }
        };

        return lockForOperation(callable, null);
    }
    /**
     * Executes a queries and returns the results as a List of rows. Each row is a Map of column names and values. If the Connection is not an {@link OracleConnection}, the SQL
     * query will be parsed for bind names (in the form <code>:&lt;name&gt;</code>). Bind names will be replaced in the SQL with <code>?</code> and the value associated with the
     * specified name in the specified map of bind values will be bound, by position, into the statement. OracleConnections will use bind by name syntax.
     * 
     * @param sql
     *            a String with the text of the SQL statement
     * @param binds
     *            a Map of bind names and values
     * @return a List of Maps containing the column names and values
     */
    public final List<Map<String, ?>> executeReturnList( final String sql, final Map<String, ?> binds ) {
        LockableOperation<List<Map<String, ?>>> callable = new OperImpl<List<Map<String, ?>>>() {
            public List<Map<String, ?>> call() throws SQLException {
            	ResultSet rs = executeQuery(sql, binds);
                try {
                    List<Map<String, ?>> ret = new ArrayList<Map<String, ?>>();
                    Map<String, Object> row = null;
                    final String[] colNames = new String[ rs.getMetaData().getColumnCount() ];
                    for (int i = 0; i < colNames.length; i++) {
                        colNames[ i ] = rs.getMetaData().getColumnName(i + 1);
                    }
                    while( rs.next() ) {
                        row = new HashMap<String, Object>();
                        for (int i = 0; i < colNames.length; i++) {
                            row.put(colNames[ i ], rs.getObject(i + 1));
                        }
                        ret.add(row);
                    }

                    return ret;
                } finally {
                    try {
                        cleanup(rs);
                    } catch (Exception e) {
                    }
                }
            }
        };

        List<Map<String, ?>> l = Collections.emptyList();
        return lockForOperation(callable, l);
    }

    /**
     * Executes a queries and returns the results as Map of values. Each row is a Map of column names and values. If the Connection is not an {@link OracleConnection}, the SQL
     * query will be parsed for bind names (in the form <code>:&lt;name&gt;</code>). Bind names will be replaced in the SQL with <code>?</code> and the value associated with the
     * specified name in the specified map of bind values will be bound, by position, into the statement. OracleConnections will use bind by name syntax.
     * 
     * @param sql
     * @param binds
     * @return
     */
    @SuppressWarnings({ "cast", "unchecked" })
    public final Map<String, ?> executeReturnMap(final String sql, final Map<String, ?> binds) {
        LockableOperation<Map<String, ?>> callable = new OperImpl<Map<String, ?>>() {
            public Map<String, ?> call() throws SQLException {
                Map<String, Object> ret = new HashMap<String, Object>();
                ResultSet rs = executeQuery(sql, binds);
                try {
                    final String[] colNames = new String[ rs.getMetaData().getColumnCount() ];
                    for (int i = 0; i < colNames.length; i++) {
                        colNames[ i ] = rs.getMetaData().getColumnName(i + 1);
                    }

                    while (rs.next()) {
                        int x = 0;
                        for (int i = 0; i < colNames.length; i++) {
                            ret.put(x + "." + colNames[ i ], rs.getObject(i + 1)); //$NON-NLS-1$
                        }
                        x++;
                    }
                    return ret;
                } finally {
                    cleanup(rs);
                }
            }
        };

        return lockForOperation(callable, (Map<String, ?>)Collections.EMPTY_MAP);
    }

    /**
     * Binds values into a statement. For Oracle Connections, the values will be bound by name. Calling this method with a {@link PreparedStatement} that does not support binding
     * by name is unsupported and may result in incorrect operation if more than one bind value is specified.
     * <p>
     * This method requires that callers hold a lock on the Connection.
     * 
     * @param stmt
     *            the PreparedStatement requiring values to be bound in
     * @param map
     *            a Map of names and values to bind to the statement
     * @throws SQLException
     *             if any errors are encountered performing the binding
     */
    public static void bind(final PreparedStatement stmt, final Map<String, ?> map) throws SQLException {
        if (map.size() == 0) {
            // Nothing to bind, so return right away
            return;
        }
        try {
            assertLock(new StatementOperImpl<Void>(stmt) {
                public Void call() throws SQLException {
                    if (stmt instanceof OraclePreparedStatement) {
                        OraclePreparedStatement ostmt = (OraclePreparedStatement) stmt;
                        Object bindValue = null;

                        for (String bindName: map.keySet()) {
                            bindValue = map.get(bindName);
                            // logger.info("Bind:"+bindName+":"+bindValue);
                            // Debug.debug(bindName);
                            try {
                                if (bindValue == NULL_VALUE) {
                                    ostmt.setNullAtName(bindName, Types.VARCHAR);
                                } else if (bindValue instanceof String) {
                                    ostmt.setStringAtName(bindName, (String) bindValue);
                                } else if (bindValue instanceof BigDecimal) {
                                    ostmt.setBigDecimalAtName(bindName, (BigDecimal) bindValue);
                                } else if (bindValue instanceof CLOB) {
                                    ostmt.setCLOBAtName(bindName, (CLOB) bindValue);
                                } else {
                                    ostmt.setObjectAtName(bindName, bindValue);
                                }
                            } catch (Exception e) {
                                // ignore if bind does not exist
                            }
                        }
                    } else {
                        logStateException(new IllegalStateException("Non Oracle statement with bind names"), EMPTY_STRING //$NON-NLS-1$
                                + "{0} called with a non Oracle statement; {1} called {2}"); //$NON-NLS-1$

                        // Ok, this is almost certainly wrong. The values are
                        // going to be returned
                        // in the order they are stored in the map; this does
                        // not have any connection
                        // to the order they were placed in the map. Only by
                        // blind luck will a Map with
                        // two or more entries return the values in the correct
                        // order for binding
                        Iterator<?> iter = map.values().iterator();
                        List<Object> bindList = new ArrayList<Object>(16);
                        while (iter.hasNext()) {
                            bindList.add(iter.next());
                        }
                        bind(stmt, bindList);
                    }
                    return null;
                }
            });
        } catch (SQLException ex) {
            throw ex;
        } catch (Exception ex) {

        }
    }

    /**
     * Binds values into a statement. The values will be bound by position in the statement.
     * <p>
     * This method requires that callers hold a lock on the Connection. \ *
     * 
     * @param stmt
     *            the PreparedStatement requiring values to be bound
     * @param binds
     *            a List of bind values
     * @throws SQLException
     *             if any errors are encountered performing the binding
     */
    public static void bind(final PreparedStatement stmt, final List<?> binds) throws SQLException {
        LockableOperation<Void> callable = new StatementOperImpl<Void>(stmt) {
            public Void call() throws SQLException {
                // oracle starts at 1 others start at 0
                int bindIdx = 1;
                for (Object bind : binds) {
                    // Debug.debug(bind);
                    // logger.info("Bind:"+i+":"+bind);

                    if ((bind == NULL_VALUE) || ((bind instanceof String) && (((String) bind).equals(EMPTY_STRING)))) { 
                        stmt.setNull(bindIdx, Types.VARCHAR);
                    } else if (bind instanceof String) {
                        stmt.setString(bindIdx, (String) bind);
                    } else if (bind instanceof BigDecimal) {
                        stmt.setBigDecimal(bindIdx, (BigDecimal) bind);
                    } else if (bind instanceof Date) {
                        stmt.setDate(bindIdx, (Date) bind);
                    } else if (bind instanceof Integer) {
                        stmt.setInt(bindIdx, ((Integer) bind).intValue());
                    } else if (bind instanceof java.sql.Array) {
                        stmt.setArray(bindIdx, (java.sql.Array)bind);
                    } else if (bind instanceof java.sql.Blob) {
                        stmt.setBlob(bindIdx, (java.sql.Blob)bind);
                    } else if (bind instanceof Boolean) {
                        stmt.setBoolean(bindIdx, (Boolean)bind);
                    } else if (bind instanceof Byte) {
                        stmt.setByte(bindIdx, (Byte)bind);
                    } else if (bind instanceof byte[]) {
                        stmt.setBytes(bindIdx, (byte[])bind);
                    } else if (bind instanceof java.sql.Clob) {
                        stmt.setClob(bindIdx, (java.sql.Clob)bind);
                    } else if (bind instanceof Double) {
                        stmt.setDouble(bindIdx, (Double)bind);
                    } else if (bind instanceof Float) {
                        stmt.setFloat(bindIdx, (Float)bind);
                    } else if (bind instanceof Integer) {
                        stmt.setInt(bindIdx, (Integer)bind);
                    } else if (bind instanceof Long) {
                        stmt.setLong(bindIdx, (Long)bind);
                    } else if (bind instanceof java.sql.NClob) {
                        stmt.setNClob(bindIdx, (java.sql.NClob)bind);
                    } else if (bind instanceof String) {
                        stmt.setNString(bindIdx, (String)bind);
                    } else if (bind instanceof java.sql.Ref) {
                        stmt.setRef(bindIdx, (java.sql.Ref)bind);
                    } else if (bind instanceof java.sql.RowId) {
                        stmt.setRowId(bindIdx, (java.sql.RowId)bind);
                    } else if (bind instanceof Short) {
                        stmt.setShort(bindIdx, (Short)bind);
                    } else if (bind instanceof String) {
                        stmt.setString(bindIdx, (String)bind);
                    } else if (bind instanceof java.sql.SQLXML) {
                        stmt.setSQLXML(bindIdx, (java.sql.SQLXML)bind);
                    } else if (bind instanceof java.sql.Time) {
                        stmt.setTime(bindIdx, (java.sql.Time)bind);
                    } else if (bind instanceof java.sql.Timestamp) {
                        stmt.setTimestamp(bindIdx, (java.sql.Timestamp)bind);
                    } else if (bind instanceof java.net.URL) {
                        stmt.setURL(bindIdx, (java.net.URL)bind);
                    } else {
                        stmt.setObject(bindIdx, bind);
                    }
                    bindIdx++;
                }
                return null;
            }
        };

        try {
            assertLock(callable);
        } catch (SQLException e) {
            throw e;
        } catch (Exception ex) {

        }
    }

    /**
     * Retrieves the current contents of the DBMS_OUTPUT buffer. Requires an Oracle connection.
     * 
     * @return a String containing all of the current data in the DBMS_OUTPUT buffer.
     */
    public String getDBMSOUTPUT() {
        throw new UnsupportedOperationException("getDBMSOUTPUT"); //$NON-NLS-1$
    }
    
    /**
     * A standardized mechanism for logging an exception to the Log.
     * 
     * @param e
     *            the Exception being logged
     */
    public void handleException(Exception e) {
        String err = e.getMessage();
        MessageLogging.getInstance().log(err);
    }

    // return true to include
    // // return true to include
    // public static boolean checkVersion(DBObject dbObject, Version maxver,
    // Version minver) {
    // return checkVersion(dbObject.getConnection(), maxver, minver);
    // }

    private static final String VERSION_PATTERN = "[0-9]+\\.[0-9.]*[0-9]+"; //$NON-NLS-1$
    private static final Pattern s_pattern = Pattern.compile(VERSION_PATTERN);

    public static final Version ORACLE8_VERSION = new Version("8"); //$NON-NLS-1$
    public static final Version ORACLE8i_VERSION = new Version("8.1"); //$NON-NLS-1$
    public static final Version ORACLE9i_VERSION = new Version("9"); //$NON-NLS-1$
    public static final Version ORACLE9iR2_VERSION = new Version("9.2"); //$NON-NLS-1$
    public static final Version ORACLE10g_VERSION = new Version(VERSION_10G_STRING);
    public static final Version ORACLE10gR2_VERSION = new Version("10.2"); //$NON-NLS-1$
    public static final Version ORACLE11g_VERSION = new Version("11"); //$NON-NLS-1$
    public static final Version ORACLE12c_VERSION = new Version("12"); //$NON-NLS-1$

    final Version fetchDbVersion() {
        LockableOperation<String> oper = new OperImpl<String>() {

            @Override
            public String call() throws Exception {
                return fetchDbVersionImpl();
            }
        };
        
        String vString = lockForOperation(oper, VERSION_10G_STRING);
        Matcher matcher = s_pattern.matcher(vString);
        String num = matcher.find() ? matcher.group() : "1.0.0"; //$NON-NLS-1$
        
        return new Version(num);
    }

    protected String fetchDbVersionImpl() throws SQLException, ThreadDeath {
        String result;
        DatabaseMetaData dmd = null;
        dmd = m_conn.getMetaData();
        try {
            result = dmd.getDatabaseMajorVersion() + "." //$NON-NLS-1$
                    + dmd.getDatabaseMinorVersion() + ".0"; //$NON-NLS-1$
        } catch (Throwable t) {
            if (t instanceof ThreadDeath) {
                throw (ThreadDeath) t;
            }
            // If third parties dont support these
            // above, then
            // we need to manage the expectation of a
            // version
            try {
                result = dmd.getDatabaseProductVersion();
            } catch (UnsupportedOperationException upe2) {
                result = "1.0.0"; //$NON-NLS-1$
            }
        }
        return result;
    }

    /**
     * Get the current error. This method gets the current error from the DBUTIL facility. It is only updated today for the executeUpdate method.
     * 
     * @return Error String
     */
    public final String getCurrentError() {
        return m_currentErrorString != null ? m_currentErrorString : EMPTY_STRING; 
    }

    /**
     * Determines the location of an error occurring as a result of the executing of a SQL statement.
     * 
     * @param sql
     *            a String containing the statement that encountered the error
     * @return the location of the error in the String
     */
    public int getErrorOffset(final String sql) {
        throw new UnsupportedOperationException("getErrorOffset"); //$NON-NLS-1$
    }

    /*
     * vasriniv: Rolling back the change as this impl is introducing a dependency that will break the build public static String getSchemaQualifiedObjectName(ObjectNode objN)
     * throws SQLException { String retVal = null;
     * 
     * if (objN != null) { DBObject dbo = new DBObject(objN.getURL()); String schema = dbo.getSchemaName(); Connection c = dbo.getConnection(); if ( c != null && schema != null &&
     * !schema.equals(c.getMetaData().getUserName())) { retVal = dbo.getSchemaName() + objN.getObjectName(); } else { retVal = objN.getObjectName(); } } return retVal; }
     */

    /**
     * Quotes a string with double quote char.
     * 
     * @param s
     *            the String to quote
     * @return the quoted String
     * @see quoteIdentifier
     */
    public static String addDoubleQuote(String s) {
    	return quoteIdentifier(s, '"');
//        // We want to check 3 things here:
//        // 1) The string is a valid identifier (starts with a letter, contains only valid characters, isn't a reserved word
//        // 2) The string is all upper case. By policy, we will quote any lower or mixed cased strings
//        // 3) the string is not currently quoted. We will only check the first character for a quote. A quote appearing anywhere
//        //    else or a starting quote w/o an ending quote is going to make the string invalid no matter what (quotes cannot
//        //    be escaped in Oracle identifiers).
//        boolean quote = false;
//        // Let's start with 3) first - it's the easiest check
//        if ( !s.startsWith("\"")) { //$NON-NLS-1$
//            // Next, check to see if it is a valid identifier
//            quote = !OracleDatabaseDescriptor.isValidOracleIdentifier(s);
//            if ( !quote ) {
//                // Ok, it's valid, so check the case
//                char[] chars = new char[s.length()];
//                s.getChars(0, chars.length, chars, 0);
//                for ( char c : chars ) {
//                    if ( Character.isLowerCase(c) ) {
//                        // As soon as we find a lower case character, we can abort the search
//                        quote = true;
//                        break;
//                    }
//                }
//            }
//        }
//        return  quote ? MessageFormat.format("\"{0}\"", s) : s;//$NON-NLS-1$
    }
    /**
     * Specifies whether errors should be reported to the user in an error dialog.
     * 
     * @param b
     *            a boolean indicating whether errors should be reported in a dialog
     * @deprecated 
     */
    public void setRaiseError(boolean b) {
        m_raiseError = b;
    }
    
    /**
     * Checks the specified identifier and quotes it if needed.
     * The identifier is checked to see if it requires quoting based on the following:
     * 1) Any Oracle reserved words (TABLE, for example) require quoting
     * 2) Any invalid characters (including lower case characters) requires quoting
     * @param identifier
     * @param quoteChar
     * @return the quoted identifier, or the original identifier is no quoting is needed.
     * @see Service.quoteIdentifier
     */
    public static String quoteIdentifier(String identifier, char quoteChar) {
    	// Check to see if the identifier is a reserved word, otherwise do a content check
    	return OracleUtil.getReservedWords().contains(identifier) ?  quoteChar + identifier + quoteChar :
    		Service.quoteIdentifier(identifier, quoteChar);
    }
    
    /**
     * Retrieves the list of bind names in a SQL statement.
     * 
     * @param in
     *            a String containing the statement to parse for bind names
     * @return a List of bind names
     */
    public static List<String> getBindNames(String in) {
        return Parser.getInstance().getBindNames(in);
    }

    /**
     * Retrieves the list of bind names in a SQL statement.
     * 
     * @param in
     *            a String containing the statement to parse for bind names
     * @param all
     *            whether to include duplicates
     * @return a List of bind names
     */
    public static List<String> getBindNames(String in, boolean all) {
        return Parser.getInstance().getBindNames(in, all);
    }
    
    /**
     * Retrieves the list of binds in a SQL statement.
     * 
     * @param in
     *            a String containing the statement to parse for bind names
     * @param all
     *            whether to include duplicates
     * @return a List of binds
     */
    public static List<Bind> getBinds(String in, boolean all) {
        return Parser.getInstance().getBinds(in, all);
    }
    
    
    @SuppressWarnings("unchecked")
    /*
     * Binds values into a statement. The specified statement will be parsed to determine the bind names in use. For Oracle Connections, the values will be bound by name. Calling
     * this method with a {@link PreparedStatement} that does not support binding by name is unsupported and may result in incorrect operation if more than one bind value is
     * specified. <p> This method requires that callers hold a lock on the Connection.
     * 
     * @param sql a String containing the SQL for the statement
     * 
     * @param stmt the PreparedStatement requiring values to be bound in
     * 
     * @param a Map of bind names and values
     * 
     * @throws SQLException if an error is encountered performed the binding
     */
    public static void bind(String sql, PreparedStatement stmt, Map bindMap) throws SQLException {
        if (bindMap != null) {
            Map binds = new HashMap();
            Iterator iter = bindMap.keySet().iterator();
            String bindName = null;
            Object bindValue = null;
            Set<String> bindsUsed = new HashSet<String>(getBindNames(sql));
            while (iter.hasNext()) {
                bindName = (String) iter.next();
                bindValue = bindMap.get(bindName);
                // String str = bindName.startsWith(":") ? bindName : ":" +
                // bindName;
                if (bindsUsed.contains(bindName.intern())) {
                    binds.put(bindName, bindMap.get(bindName));
                }
            }

            bind(stmt, binds);
        }
    }

    /**
     * Converts an object name to an unquoted form. For quoted names, the quotes are removed. For names that are not quoted, conversion to upper case is used instead.
     * 
     * @param s
     *            the String to convert
     * @return the converted String
     */
    public String scrubObjectName(String s) {
        return s.indexOf(QUOTE_DELIMITER) > -1 ? s.replaceAll(QUOTE_DELIMITER, EMPTY_STRING) : s.toUpperCase();
    }

    /**
     * Converts a name with a DBLink into the link name and remote object name.
     * 
     * @param object
     *            the qualified object name
     * @return a String array containing the name of the DB Link and the remote object name, or <code>null</code> if the object name does not reference a DB Link
     */
    public static String[] resolveDBLink(String object) {
        String s[] = object.split("@"); //$NON-NLS-1$
        return (s.length == 2) ? s : null;
    }

    /**
     * Resolves an identifier into a specific object, including the name, owner, and type of the object.
     * 
     * @param descr
     *            a String indicating the identifier
     * @return a Map of properties indicating the owner, name, and type of the object
     */
    public Map<String, String> resolveName(String descr) {
        return resolveName(descr, false, false);
    }
    /**
     * Resolves an identifier into a specific object, including the name, owner, and type of the object.
     * 
     * @param descr
     *            a String indicating the identifier
     * @param ignoreSynonyms   much faster if set to true, but the caller must be sure it's not a synonym
     * @return a Map of properties indicating the owner, name, and type of the object
     */
    public Map<String, String> resolveName(String descr, boolean withPackageBody, boolean ignoreSynonyms) {
        return null;
    }
    
     

    /**
     * This is old code (<2008) which purpose is difficult to trace,
     * but I guess it 
     * 1. guarantees bind uniqueness
     * 2. obfuscates the query
     * However, rewriting sql like that makes column position information in error diagnostics invalid
     * Therefore, I'm bailing out if all parsedBinds are unique
     */
    public final String prepareOracleSql( String query ) {
        List<Bind> parsedBinds = Parser.getInstance().getBinds(query, true);
        Set<String> cmp = new HashSet<String>();
        for( Bind b : parsedBinds )
        	cmp.add(b.getName());
        if( cmp.size() == parsedBinds.size() )
        	return query;
        
        String sql = query;
        int adjust = 0; // after insertion
        int count = 1;
        for (Bind b: parsedBinds) {
            //sql = sql.replaceFirst(":" + b.getName(), ":ZSqlDevUnIq" + count); //$NON-NLS-1$ //$NON-NLS-2$
        	// ^^^^^^^^^^ this is unreliable if there is bind var name occurence in comment or string literal 
        	// assume Parser.getInstance().getBinds(query, true); produces properly ordered binds
            String newBind = ":ZSqlDevUnIq" + count;
            int begin = b.getBegin();
			int end = b.getEnd();
			sql = sql.substring(0, begin+adjust)+newBind + sql.substring(end+adjust); 
			adjust += newBind.length() -end+begin;
            count++;
        }
        return sql;
    }

    public final String prepareNonOracleSql(String query) {
        // PreparedStatement pstmt = null;
        List<Bind> parserdBinds = Parser.getInstance().getBinds(query, true);
        int adjust = 0; // after insertion
        String sql = query;
        for (Bind b: parserdBinds) {
            //sql = sql.replaceFirst(":" + b.getName(), "?"); //$NON-NLS-1$ //$NON-NLS-2$
            String newBind = "?";
            int begin = b.getBegin();
			int end = b.getEnd();
			sql = sql.substring(0, begin+adjust)+newBind + sql.substring(end+adjust); 
			adjust += newBind.length() -end+begin;
        }
        return sql;
    }

    protected PreparedStatement prepareQuery(final String query) throws SQLException {
        String sql = prepareNonOracleSql(query);
        return m_conn.prepareStatement(sql);
    }

    public final PrepareResult prepareNonOracleSql(String query, Map<String, ?> binds) {
        if (query.indexOf("{:") != -1) { //$NON-NLS-1$
            query =  prepareNonOracleSqlMsgFormat(query, binds).getSQL();
        }
        List<Bind> parserdBinds = Parser.getInstance().getBinds(query, true);
        List<Object> bindList = new ArrayList<Object>();
        String sql = query;
        for (Bind b: parserdBinds) {
            if (binds != null) {
                bindList.add(binds.get(b.getName()));
            }
            sql = sql.replaceFirst(":" + b.getName(), "?"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        return new PrepareResult(sql, bindList);
    }

    private PrepareResult prepareNonOracleSqlMsgFormat(String query, Map<String, ?> binds) {
        int index = 0;
        // PreparedStatement pstmt = null;
        String formatSql = query;
        List<Bind> parserdBinds = Parser.getInstance().getBinds(query, true);
        List<Object> bindList = new ArrayList<Object>();
        String sql = query;
        for (Bind b: parserdBinds) {
            if (binds != null) {
                // bindList.add(binds.get(b.getName()));
                /*
                 * If the bind value is a string, use addQuotes method to enquote the string. The string can contain special chars which needs to be quoted specific to the
                 * database.
                 */
                if (binds.get(b.getName()) instanceof String) {
                    String bindValue = (String) binds.get(b.getName());
                    // Bug 10032492 fix.
                    bindValue = bindValue.trim();
                    CharSequence cs = " "; //$NON-NLS-1$
                    bindValue = bindValue.contains(cs) ? addDbQuotes(bindValue, true) : addDbQuotes(bindValue, false);
                    bindList.add(bindValue);
                } else {
                    bindList.add(binds.get(b.getName()));
                }
                sql = sql.replaceFirst("\\{:" + b.getName(), "{"+Integer.toString(index++)); //$NON-NLS-1$ //$NON-NLS-2$
               
            }
        }
        sql = sql.replaceAll("'","''"); //$NON-NLS-1$ //$NON-NLS-2$
        formatSql = MessageFormat.format(sql, bindList.toArray());
        return new PrepareResult(formatSql, bindList);
    }

    /*
     * addQuotes() method enquotes the string specific to the database connection.
     */
    public final String addDbQuotes(String value) {
        return addDbQuotes(value, true);
    }

    /*
     * addQuotes() method enquotes the string specific to the database connection.
     */
    public String addDbQuotes(String value, boolean force) {
    	// Default implementation
    	char quoteChar = getQuoteCharacter();
    	if ( force ) {
    		// Quote the identifier unless it already contains the quote
    		// character
    		return value.indexOf(quoteChar) >= 0 ? value : quoteChar + value + quoteChar;
    	}
    	
    	
    	return quoteIdentifier(value, quoteChar);
//        String quotedString = value.trim();
//        Database db = s_conns.getDatabase(m_conn);
//        if ( db != null ) {
//	        try {
//	            quotedString = db.quoteIdentifier(value, force);
//	        } catch (DBException ex) {
//	            String quote = db.getIdentifierQuoteString();
//	            if (force && quote != null)
//	                quotedString = quote + value + quote;
//	            else
//	                quotedString = value;
//	        }
//        }
//        return quotedString;
    }
    
    protected char getQuoteCharacter() {
    	return '"';
    }
    
    /** 
     * Retrieves the last exception encountered by this DBUtil instance in the current thread. Fetching the
     * last exception will clear it.
     * @return the last exception encountered.
     */
    public final SQLException getLastException() {
        SQLException ex = peekLastException();
        clearLastException();
        return ex;
    }
    
    private void clearLastException() {
        m_lastException.set(null);
    }
    
    /*
     * Sets Last Exception
     */
    private void setLastException(SQLException ex) {
        m_lastException.set(ex);
    }
    
    /*
     * Allow look at the last exception without clearing it
     */
    private SQLException peekLastException() {
        return m_lastException.get();
    }
    
    private void handleException(SQLException ex, String sql) {
        SQLException old = getLastException();
        // Print a warning if the old exception wasn't fetched. Ignoring exceptions is bad!
        if ( old != null ) {
            LOGGER.severe(MessageFormat.format(Messages.getString("DBUtil_UNHANDLED_EXCEPTION"), old.getLocalizedMessage()));   //$NON-NLS-1$
        }
        
        if ( m_raiseError ) {
        	ExceptionHandler.handleException(ex, mName, sql, -1);
        } else {
            setLastException(ex);
        }
    }
    

    public static void main(String[] dummy) throws Exception {
       // System.out.println(addDoubleQuote("\"test\""));// (authorized) //$NON-NLS-1$
       // System.out.println("\"test\"");// (authorized) //$NON-NLS-1$
        
        String banner = "Oracle Database 11g Enterprise Edition Release 11.2.0.1.0 - 64bit Production"; //$NON-NLS-1$
        String[] first = banner.split(" "); //$NON-NLS-1$
        String regex = ".*[0-9]\\.+[0-9]\\.+.*"; //$NON-NLS-1$
        String version=null;
        for(String s:first){
            System.out.println(s);            
            if (Pattern.matches(regex, s)) {
                version =s;
                System.out.println("--Matched"); //$NON-NLS-1$
            }
        }
        
    }

    // This could be removed when "Bug 6850716 - sqldev: need isalive and
    // hastransaction support from database" is fixed
    /**
     * Checks whether there is currently an active transaction on the specified connection.
     * 
     * @param connName
     *            a String representing the fully qualified connection name
     * @return whether there are uncommited changes in the session
     */
    public static boolean hasTransaction(String connName) {
        return getInstance(connName).hasTransaction();
    }

    public static boolean hasTransaction(Connection conn) {
        return getInstance(conn).hasTransaction();
    }

    protected boolean hasTransaction() {
        throw new UnsupportedOperationException("hasTransaction"); //$NON-NLS-1$
    }

    // This could be removed when "Bug 6850716 - sqldev: need isalive and
    // hastransaction support from database" is fixed
    /**
     * Checks whether the specified connection is still active.
     * 
     * @param oracleConn
     *            the Connection to check
     * @return true if ORACLE_CONNECTION_ALIVE_QUERY can be executed
     */
    public static boolean isOracleConnectionAlive(Connection oracleConn) {
        if (oracleConn == null /* || oracleConn.isClosed() */) return false;
        return getInstance(oracleConn).isOracleConnectionAlive();
    }

    protected boolean isOracleConnectionAlive() {
        throw new UnsupportedOperationException("isOracleConnectionAlive"); //$NON-NLS-1$
    }
    
    protected interface LockableOperation<V> extends Callable<V> {
        Connection getConnection();
        String getSQL();
        int getOffset();
    }

    protected abstract class OperImpl<V> implements LockableOperation<V> {
        public Connection getConnection() {
            return m_conn;
        }
        
        public String getSQL() {
            return null;
        }
        
        public int getOffset() {
            return -1;
        }
    }

    private abstract static class StatementOperImpl<V> implements LockableOperation<V> {
        private Connection m_conn;

        private StatementOperImpl(Statement stmt) throws SQLException {
            m_conn = stmt.getConnection();
        }

        public Connection getConnection() {
            return m_conn;
        }
        
        public String getSQL() {
            return null;
        }
        
        public int getOffset() {
            return -1;
        }
    }

    protected <V> V lockForOperation(LockableOperation<V> operation, V defaultValue) {
        V value = defaultValue;
        Connection conn = operation.getConnection();
        if (LockManager.lock(conn)) {
            try {
                // Remember Last Exception
                SQLException previousException = peekLastException();
                
                // Call operation
                value = operation.call();
                
                // Clear Last Exception is still the same one
                if (previousException == peekLastException()) {
                    clearLastException();
                }
            } catch (SQLException e) {
                handleException(e, operation.getSQL());
            } catch (Exception e) {
                if (m_raiseError) {
                	ExceptionHandler.handleException(e);
                } else {
                    m_currentErrorString = e.getMessage();
                    // Log this as an info message so we can check in debug builds. In theory we handle this
                    // by retrieving the last error message, but not every caller is doing so.
                    LOGGER.info(e.getMessage());
                }
            } finally {
            	LockManager.unlock(conn);
            }
        }
        return value;
    }

    static final boolean DEBUG_BUILD = Boolean.getBoolean("sqldev.debugbuild"); //$NON-NLS-1$

    private static ThreadLocal<Integer> s_lockCheckCount = new ThreadLocal<Integer>() {
        @Override
        protected Integer initialValue() {
            return 0;
        }

    };

    private static <V> V assertLock(LockableOperation<V> operation) throws Exception {
        if (DEBUG_BUILD) {
            if (s_lockCheckCount.get() == 0 && !LockManager.checkLock(operation.getConnection())) {
                IllegalMonitorStateException e = new IllegalMonitorStateException(Messages.getString("DBUtil.112")); //$NON-NLS-1$
                logStateException(e, "Illegal Lock State: {0} requires connection lock to be held. {1} called {2} without it"); //$NON-NLS-1$
            }
        }

        s_lockCheckCount.set(s_lockCheckCount.get() + 1);
        try {
            return operation.call();
        } finally {
            s_lockCheckCount.set(s_lockCheckCount.get() - 1);
        }
    }

    private static void logStateException(Exception e, String logMsg) {
        StackTraceElement[] trace = e.getStackTrace();
        int level = 1; // Skip the assertLock method on the stack
        StackTraceElement top = trace[ level++ ];
        String topClass = top.getClassName();
        StackTraceElement entryPoint = null;
        StackTraceElement caller = null;
        while (level < trace.length) {
            StackTraceElement element = trace[ level ];
            if (!topClass.equals(element.getClassName())) {
                entryPoint = trace[ level - 1 ];
                caller = element;
                break;
            }
            level++;
        }

        Object[] fmtArgs = new Object[] { top.getMethodName(), caller.toString(), entryPoint.toString() };
        MessageLogging.getInstance().reportAPIException(MessageFormat.format(logMsg, fmtArgs), e, top.getMethodName());

        LOGGER.log(Level.SEVERE, logMsg, fmtArgs);
    }

    protected static void cleanup(Statement stmt) {
        cleanup(stmt, null);
    }

    protected static void cleanup(ResultSet rs) {
    	if ( rs != null ) {
	    	Statement stmt = null;
	    	try {
	    		stmt = rs.getStatement();
	    	} catch (SQLException ex) {
	    		LOGGER.warning(ex.getLocalizedMessage());
	    	}
	    	cleanup(stmt, rs);
    	}
    }

    protected static void cleanup(Statement stmt, ResultSet rs) {
        if (rs != null) try {
            rs.close();
        } catch (Exception ex) {
    		LOGGER.warning(ex.getLocalizedMessage());
        }
        if (stmt != null) try {
            stmt.close();
        } catch (Exception ex) {
    		LOGGER.warning(ex.getLocalizedMessage());
        }
    }
    
    /**
     * Closes a {@link ResultSet} as well as the originating {@Statement} (if one exists).
     * @param rs the ResultSet to close
     */
    public static void closeResultSet(ResultSet rs) {
    	cleanup(rs);
    }
    
    final boolean checkAccess(String s) {
    	return checkAccessImpl(s);
    }
    
    protected boolean checkAccessImpl(String s) {
    	boolean hasAccess = false;
    	// make a bogus predicate to ensure no damage

    	try (Statement stmt = m_conn.createStatement()){
    		String sql = "select 1 from "+ s + " where 1=2"; //$NON-NLS-1$ //$NON-NLS-2$
    		stmt.execute( sql );

    		hasAccess = true;
    	}
    	catch (SQLException e) {
    		// TODO: Move message to DBUtil for 3.1 (Can't do it now without loosing translated ones)
    		LOGGER.info(s +oracle.dbtools.raptor.query.Messages.getString("QueryUtils.31")); //$NON-NLS-1$
    	}

    	return hasAccess;
    }
}
