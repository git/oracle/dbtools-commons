/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.SwingUtilities;

import oracle.dbtools.util.Logger;

/**
 * Default LockManager implementation. Reentrant locks are used to manage the locks on the Connections.
 * @author jmcginni
 *
 */
public class DefaultLockManager extends LockManager {
    protected static final long DEFAULT_INITIAL_TIMEOUT_MS = 3000; // 3 seconds including lag margin
    protected static final long DEFAULT_LAG_MARGIN_MS = 1000; // 1 second - absolute minimum allowed

    private final long initialWaitTime = initWaitTime();
    private final long lagMargin = initLagMargin();
    private long currentWaitTime = initialWaitTime;
    
    private Map<Connection, ReentrantLock> locks = new HashMap<Connection, ReentrantLock>();
    
    private ReentrantLock getLock(Connection conn) {
    	if ( conn == null ) {
    		throw new IllegalArgumentException("Attempt to get the lock for a null Connection"); //$NON-NLS-1$
    	}
    	synchronized(locks) {
    		ReentrantLock lock = locks.get(conn);
    		if ( lock == null ) {
    			lock = new ReentrantLock();
    			locks.put(conn, lock);
    		}
    		return lock;
    	}
    }

    protected long initWaitTime() {
        return  DEFAULT_INITIAL_TIMEOUT_MS;
    }
    
    protected long initLagMargin() {
    	return DEFAULT_LAG_MARGIN_MS;
    }
    
    private void recalcWaitTime(long startTime) {
        long elapsed = System.currentTimeMillis() - startTime;
        if (elapsed < initialWaitTime) {
            // within the initialized bounds, keep/go back to using them
            currentWaitTime = initialWaitTime;
        } else {
            currentWaitTime = elapsed + lagMargin;
        }
    }
    
	@Override
	public final boolean lockImpl(Connection conn, boolean promptUser) {
        boolean haveLock = false;
        long startTime = System.currentTimeMillis();
        long waitTimeMs = currentWaitTime;
        TimeUnit unit = TimeUnit.MILLISECONDS;
        
        ReentrantLock lock = getLock(conn);
        if (lock != null) {
            while (true) {
                try {
                    // Try to get the lock with a timeout and use the 
                    // elapsed time to (potentially) recalculate the timeout.
                    if (lock.tryLock(waitTimeMs, unit)) {
                        haveLock = true;
                        recalcWaitTime(startTime);
                        break;
                    } else if (promptUser) {
                        // Irrespective of the prompt parameter value, only
                        // show a busy message if the connection lock attempt
                        // is blocking the UI
                        boolean reTry = true;
                        if (SwingUtilities.isEventDispatchThread()) {
                            reTry = prompt(conn);
                        } else {
                            Logger.fine(LockManager.class, "Connection is busy"); //$NON-NLS-1$
                        }
                        // Either way (Prompt/Log), calculate a longer timeout
                        recalcWaitTime(startTime);
                        if (!reTry) {
                            // User selected Abort - get out
                            break;
                        }
                    }
                } catch (InterruptedException e) {
                    Logger.fine(LockManager.class, "Interrupted while trying to get a lock"); //$NON-NLS-1$
                    break;
                }
            }
        }
        return haveLock;
	}

	@Override
	public final void unlockImpl(Connection conn) {
		ReentrantLock lock = getLock(conn);
		if ( lock.isHeldByCurrentThread() ) {
			lock.unlock();
		} else {
			
		}
	}

	@Override
	public final boolean checkLockImpl(Connection conn) {
		ReentrantLock lock = getLock(conn);
		return lock.isHeldByCurrentThread();
	}

	@Override
	public boolean tryLockImpl(Connection conn, long timeout) {
		ReentrantLock lock = getLock(conn);
		if ( lock != null ) {
			try {
				return lock.tryLock(timeout, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e) {
	            Logger.fine(LockManager.class, "Interrupted while trying to get a lock"); //$NON-NLS-1$
			}
		}
		return false;
	}

	protected boolean prompt(Connection id) {
		return true;
	}
}
