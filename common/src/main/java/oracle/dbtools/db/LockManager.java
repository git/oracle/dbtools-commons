/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.sql.Connection;
import java.util.concurrent.Callable;

/**
 * The LockManager is responsible for enforcing a locking policy for {@link Connection} instances. Byt using the LockManager,
 * consumers of Connections can adapt to different locking requirements of applications.
 * 
 * The LockManager should be used to wrap operations that access Connections. The most common use case for locking is
 * 
 * <pre>
 *   if ( LockManager.lock(connection) ) {
 *     try {
 *       &lt;perform operation on Connection&gt;
 *     } finally {
 *       LockManager.unlock(connection);
 *     }
 * </pre>
 * 
 * In addition to the common lock and unlock operations, LockManager provides a {@link #checkLock} method to check whether the current thread
 * owns a lock for the Connection and {@link tryLock} to attempt to acquire a lock with a timeout.
 * 
 * @author jmcginni
 *
 */
public abstract class LockManager {
	/**
	 * A LockManager implementation that does not acquire any locks. Applications that are either single-threaded or rely on an external mechanism to control
	 * Connection access can use this LockManager implementation to bypass locking.
	 */
	public static final LockManager NOOP_LOCK_MANAGER = new LockManager() {
		@Override
		public void unlockImpl(Connection conn) {
			// No-op
		}
		
		@Override
		public boolean lockImpl(Connection conn, boolean promptUser) {
			// locks are always assumed to be acquired
			return true;
		}
		
		@Override
		public boolean checkLockImpl(Connection conn) {
			// Always assume it is 
			return true;
		}

		@Override
		public boolean tryLockImpl(Connection conn, long timeout) {
			return true;
		}
	};
	
	private static LockManager sInstance = NOOP_LOCK_MANAGER;
	
	// Package private - the LockManager should be updated by changing the ConnectionSupport instance
	static void setLockManager(LockManager impl) {
		synchronized (LockManager.class) {
			if ( impl != sInstance ) {
				if ( sInstance != null ) {
					sInstance.uninstall();
				}
				
				sInstance = impl != null ? impl : NOOP_LOCK_MANAGER;
				sInstance.install();
			}
		}
	}
	
	/**
	 * Acquire the lock for a Connection. The request may block until the lock can be required.
	 * @param conn the Connection
	 * @return whether the caller now holds the lock
	 */
	public static boolean lock(Connection conn) {
		return lock(conn, false);
	}
	
	/**
	 * Acquire the lock for a Connection. The request may block until the lock can be required. The user may optionally be prompted to wait if the lock is in use.
	 * @param conn the Connection
	 * @param promptUser whether the user should be prompted for a busy lock
	 * @return whether the caller now holds the lock
	 */
	public static boolean lock(Connection conn, boolean promptUser) {
		return sInstance.lockImpl(conn, promptUser);
	}
	
	/**
	 * Releases the lock for a Connection.
	 * @param conn the Connection
	 */
	public static void unlock(Connection conn) {
		sInstance.unlockImpl(conn);
	}
	
	/**
	 * Requests whether the current thread holds the lock for the Connection.
	 * @param conn the Connection
	 * @return whether the lock for the Connection is held by the current thread.
	 */
	public static boolean checkLock(Connection conn) {
		return sInstance.checkLockImpl(conn);
	}
	
	/**
	 * Attempts to acquire the lock for a Connection. If the specified timeout is exceeded, the attempt fails.
	 * @param conn the Connection
	 * @param timeout the request timeout, in milliseconds.
	 * @return whether the lock was acquired
	 */
	public static boolean tryLock(Connection conn, long timeout) {
		return sInstance.tryLockImpl(conn, timeout);
	}
	
	/**
	 * Executes an operation within the context of a lock. 
	 * @param conn the Connection
	 * @param oper the operation needing to be performed with the lock
	 * @return the result of the operation
	 * @throws Exception
	 */
	public static final <V> V executeWithLock(Connection conn, Callable<V> oper) throws Exception {
		if ( lock(conn) ) {
			try {
				return oper.call();
			} finally {
				unlock(conn);
			}
		}
		return null;
	}
	
	
	protected LockManager() {
	}
	
	/**
	 * Implementation of the lock() operation.
	 * @param conn the Connection
	 * @param promptUser whether the user should be prompted for a busy lock
	 * @return whether the caller now holds the lock
	 * @see lock(Connection, boolean)
	 */
	protected abstract boolean lockImpl(Connection conn, boolean promptUser);

	/**
	 * Implementation of the unlock() operation 
	 * @param conn the Connection
	 */
	protected abstract void unlockImpl(Connection conn);

	/**
	 * Implementation of the checkLock() operation.
	 * @param conn the Connection
	 * @return whether the lock for the Connection is held by the current thread.
	 */
	protected abstract boolean checkLockImpl(Connection conn);

	/**
	 * Implementation of the tryLock() operation
	 * @param conn the Connection
	 * @param timeout the request timeout, in milliseconds.
	 * @return whether the lock was acquired
	 */
	protected abstract boolean tryLockImpl(Connection conn, long timeout);
	
	/**
	 * Called when the LockManager implementation is set as the active LockManager instance
	 * @throws IllegalStateException
	 */
	protected void install() throws IllegalStateException {}
	
	/**
	 * Called when the LockManager implementation is removed as the active LockManager instance
	 * @throws IllegalStateException
	 */
	protected void uninstall() throws IllegalStateException {}
}
