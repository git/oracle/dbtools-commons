/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import oracle.dbtools.common.utils.MetaResource;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryXMLSupport;
import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.common.utils.Version;
import oracle.jdbc.OracleCallableStatement;

/**
 * Oracle specific implementation of the DBUtil class.
 * <p>
 * @author jmcginni
 *  @since 3.0
 */
final class OracleUtil extends DBUtil {
	
    private static QueryXMLSupport s_xml;

    /**
     * Load the XML queries for the OracleUtils
     * @return
     */
    protected static synchronized QueryXMLSupport getXMLQueries() {
        if (s_xml == null) {
            s_xml = QueryXMLSupport.getQueryXMLSupport(new MetaResource(DBUtil.class.getClassLoader(), "oracle/dbtools/db/dbutilsql.xml") ); //$NON-NLS-1$
        }
        return s_xml;
    }
    
    private static Set<String> s_reservedWords;
    
    /**
     * Set of all Oracle reserved words
     * @return
     */
    public static synchronized Set<String> getReservedWords() {
    	if ( s_reservedWords == null ) {
    		String[] words = {
    				"ACCESS", "ADD", "ALL", "ALTER", "AND", "ANY", "AS", "ASC", "AUDIT", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$
    				"BETWEEN", "BY", //$NON-NLS-1$ //$NON-NLS-2$
    				"CHAR", "CHECK", "CLUSTER", "COLUMN", "COMMENT", "COMPRESS", "CONNECT", "CREATE", "CURRENT", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$
    				"DATE", "DECIMAL", "DEFAULT", "DELETE", "DESC", "DISTINCT", "DROP", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
    				"ELSE", "EXCLUSIVE", "EXISTS", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    				"FILE", "FLOAT", "FOR", "FROM",  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
    				"GRANT", "GROUP", //$NON-NLS-1$ //$NON-NLS-2$
    				"HAVING", //$NON-NLS-1$
    				"IDENTIFIED", "IMMEDIATE", "IN", "INCREMENT", "INDEX", "INITIAL", "INSERT", "INTEGER", "INTERSECT", "INTO", "IS", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$
    				"LEVEL", "LIKE", "LOCK", "LONG", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
    				"MAXEXTENTS", "MINUS", "MLSLABEL", "MODE", "MODIFY", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
    				"NOAUDIT", "NOCOMPRESS", "NOT", "NOWAIT", "NULL", "NUMBER", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
    				"OF", "OFFLINE", "ON", "ONLINE", "OPTION", "OR", "ORDER", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
    				"PCTFREE", "PRIOR", "PUBLIC", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    				"RAW", "RENAME", "RESOURCE", "REVOKE", "ROW", "ROWNUM", "ROWS", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
    				"SELECT", "SESSION", "SET", "SHARE", "SIZE", "SMALLINT", "START", "SUCCESSFUL", "SYNONYM", "SYSDATE", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$
    				"TABLE", "THEN", "TO", "TRIGGER", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
    				"UID", "UNION", "UNIQUE", "UPDATE", "USER", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
    				"VALIDATE", "VALUES", "VARCHAR", "VARCHAR2", "VIEW", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
    				"WHENEVER", "WHERE", "WITH" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    		};
    		s_reservedWords = Collections.unmodifiableSet(new TreeSet<String>(Arrays.asList(words)));
    	}
    	return s_reservedWords;
    }

    /**
     * Create a new OracleUtil instance
     * @param cname
     * @param conn
     */
    OracleUtil(ConnectionIdentifier id) {
        super(id);
    }

    @Override
    public ResultSet executeQuery(String query, Map<String, ?> binds) {
        return executeOracleQuery(query, binds);
    }

    @Override
    protected PreparedStatement prepareQuery(final String query) throws SQLException {
        return m_conn.prepareCall(query);
    }
    
    @Override
    protected String fetchDbVersionImpl() throws SQLException {
        String result = VERSION_10G_STRING; 
        String banner = executeReturnOneCol("select * from v$version where banner like '%Oracle%'"); //$NON-NLS-1$                            
        String[] first = banner.split(" "); //$NON-NLS-1$
        // look for the string that is N.N.
        String regex = ".*[0-9]\\.+[0-9]\\.+.*"; //$NON-NLS-1$
        for(String s:first){
            if (Pattern.matches(regex, s)) {
                result = s;
                break;
            }
        }
        return result;
    }

    @Override
    public void setDBAction(String action) {
        Query q = getXMLQueries().getQuery("ACTION", m_conn); //$NON-NLS-1$
        execute(q.getSql(), Collections.singletonList(action == null ? EMPTY_STRING : action)); 
    }

    @Override
    public void setDBModuleAction(String action) {
        Query q = getXMLQueries().getQuery("MODULE", m_conn); //$NON-NLS-1$
        execute(q.getSql(), Arrays.asList(s_prodName, action == null ? EMPTY_STRING : action)); 
    }

    private static final Version LG_BUFFER_REQ_VERSION = new Version("10.2"); //$NON-NLS-1$

    private boolean largeBuffer(ConnectionIdentifier id) {
        return LG_BUFFER_REQ_VERSION.compareTo(VersionTracker.getDbVersion(id)) <= 0;
    }

    @Override
    public String getDBMSOUTPUT() {
        LockableOperation<String> callable = new OperImpl<String>() {
            public String call() throws SQLException {
                StringBuilder sb = new StringBuilder();
                CallableStatement stmt = null;
                try {
//                  stmt = m_conn.prepareCall("begin dbms_output.get_line(LINE=>?,status=>?); end;");
//                 stmt.registerOutParameter(1, Types.VARCHAR);
//                 stmt.registerOutParameter(2, Types.NUMERIC);    
//                 int status = 0;
//                 while(status == 0 ){
//                   stmt.execute();
//                   String line = stmt.getString(1);
//                   status = stmt.getInt(2);
//                   if ( line != null)
//                       sb.append(line);
//                   sb.append("\n");
//                 }

                  int dbV = m_conn.getMetaData().getDatabaseMajorVersion();
                  String sql = null;
                  if ( dbV>=10){
                    sql = "declare\r\n" + 
                        "    l_line varchar2(32767);\r\n" + 
                        "    l_done number;\r\n" + 
                        "    l_buffer varchar2(32767) := '';\r\n" + 
                        "    l_lengthbuffer number := 0;\r\n" + 
                        "    l_lengthline number := 0;\r\n" + 
                        "begin \r\n" + 
                        "  loop \r\n" + 
                        "    dbms_output.get_line( l_line, l_done ); \r\n" + 
                        "    if (l_buffer is null) then \r\n" + 
                        "      l_lengthbuffer := 0; \r\n" + 
                        "    else \r\n" + 
                        "      l_lengthbuffer := length(l_buffer); \r\n" + 
                        "    end if; \r\n" + 
                        "    if (l_line is null) then \r\n" + 
                        "      l_lengthline := 0; \r\n" + 
                        "    else \r\n" + 
                        "      l_lengthline := length(l_line); \r\n" + 
                        "    end if; \r\n" + 
                        "  exit when l_lengthbuffer + l_lengthline > :maxbytes OR l_lengthbuffer + l_lengthline > 32767 OR l_done = 1; \r\n" + 
                        "  l_buffer := l_buffer || l_line || chr(10); \r\n" + 
                        "    end loop; \r\n" + 
                        "    :done := l_done; \r\n" + 
                        "    :buffer := l_buffer; \r\n" + 
                        "    :line := l_line; \r\n" + 
                        "end;";
                  } else {
                    sql = "declare \r\n" + 
                        "    l_line varchar2(255); \r\n" + 
                        "    l_done number; \r\n" + 
                        "    l_buffer long; \r\n" + 
                        "begin \r\n" + 
                        "    loop \r\n" + 
                        "    exit when length(l_buffer)+255 > :maxbytes OR l_done = 1; \r\n" + 
                        "    dbms_output.get_line( l_line, l_done ); \r\n" + 
                        "    l_buffer := l_buffer || l_line || chr(10); \r\n" + 
                        "  end loop; \r\n" + 
                        "  :done := l_done; \r\n" + 
                        "    :buffer := l_buffer; \r\n" + 
                        "end;";
                  }
                  //Query q = getXMLQueries().getQuery("GET_DBMSOUTPUT", m_conn); //$NON-NLS-1$
                  
                    //stmt = m_conn.prepareCall(q.getSql());
                  stmt = m_conn.prepareCall(sql);
                    
                    
                    boolean useLargeBuffer = largeBuffer(mID);
                    if (useLargeBuffer) {
                        stmt.registerOutParameter(2, java.sql.Types.INTEGER);
                        stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
                        stmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                    } else {
                        stmt.registerOutParameter(2, java.sql.Types.INTEGER);
                        stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
                    }
                    // stmt = conn.prepareCall("declare " + " l_line
                    // varchar2(255); " +
                    // " l_done number; " + " l_buffer long; " + "begin " + "
                    // loop " + "
                    // exit when length(l_buffer)+255 > :maxbytes OR l_done = 1;
                    // " + "
                    // dbms_output.get_line( l_line, l_done ); " + " l_buffer :=
                    // l_buffer || l_line || chr(10); " + " end loop; " + "
                    // :done :=
                    // l_done; " + " :buffer := l_buffer; " + "end;");
                    for (;;) {
    
                        stmt.setInt(1, 1000);
    
                        stmt.executeUpdate();
                        if (useLargeBuffer) {
                            String got = stmt.getString(3);
                            if (got != null) {
                                sb.append(got);
                            }
                            got = stmt.getString(4);
                            if (got != null) {
                                sb.append(got + "\n"); //$NON-NLS-1$
                            } else {
                                sb.append("\n"); //$NON-NLS-1$
                            }
                        } else {
                            sb.append(stmt.getString(3));
                        }
    
                        if (stmt.getInt(2) == 1) break;
                    }
                    
                    
    
                } finally {
                    try {
                        if (stmt != null) stmt.close();
                    } catch (SQLException e) {
                    }
                }
    
                return sb.toString();
            }
        };
    
        return lockForOperation(callable, EMPTY_STRING); 
    }

    protected boolean isOracleConnectionAlive() {
        Query q = getXMLQueries().getQuery("IS_ALIVE", m_conn); //$NON-NLS-1$
        return execute(q.getSql(), Collections.EMPTY_LIST);
    }

    @Override
    public int getErrorOffset(final String sql) {
        LockableOperation<Integer> oper = new OperImpl<Integer>() {
            public Integer call() throws SQLException {
                CallableStatement stmt = null;
                try {
                    Query q = getXMLQueries().getQuery("ERR_SQL", m_conn); //$NON-NLS-1$
                    stmt = m_conn.prepareCall(q.getSql());
                    LOGGER.info(Messages.getString("DBUtil.55") + q.getSql()); //$NON-NLS-1$
                    stmt.setString(1, sql);
                    stmt.setString(2, sql);
                    stmt.registerOutParameter(3, Types.INTEGER);
                    stmt.execute();
                    return stmt.getInt(3);
                } finally {
                    if (stmt != null) try {
                        stmt.close();
                    } catch (SQLException e) {
                    }

                }
            }
        };
    
        return lockForOperation(oper, 0);
    }

    @Override
    protected boolean hasTransaction() {
        LockableOperation<Boolean> oper = new OperImpl<Boolean>() {
            @SuppressWarnings("unchecked")
            public Boolean call() throws SQLException {
                if (m_conn == null || m_conn.isClosed()) return false;
    
                Query q = getXMLQueries().getQuery("TRANSACTION", m_conn); //$NON-NLS-1$
                String ret = executeOracleReturnOneCol(q.getSql(), Collections.EMPTY_MAP);
    
                return ModelUtil.hasLength(ret);
            }
        };
    
        return lockForOperation(oper, Boolean.FALSE);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, String> resolveName( String descr, boolean withPackageBody, boolean ignoreSynonyms ) {
        String bodyWhere="N";//$NON-NLS-1$
        if (withPackageBody) {
            bodyWhere="Y"; //$NON-NLS-1$
        }
        // split the string
        String[] s = descr.split("\\."); //$NON-NLS-1$
        String objName = null;
        String ownerName = null;
        if (s.length == 2) {
            objName = s[ 1 ];
            ownerName = s[ 0 ];
        } else if (s.length == 1) {
            objName = s[ 0 ];
        } else {
            objName = descr;
        }
    
        String sql = null;
    
        objName = scrubObjectName(objName);
        if (ownerName != null) ownerName = scrubObjectName(ownerName);
    
        String dbLink[] = resolveDBLink(objName);
    
        Map<String, String> binds = new HashMap<String, String>();
        
        final String schemaContext = DBUtil.getInstance(m_conn).executeReturnOneCol(
                           "select UPPER(sys_context('USERENV', 'CURRENT_SCHEMA')) from dual" //$NON-NLS-1$
        );
        
        // refactored two duplicate code branches (with and without dblinks) into one 
        String atDbLink = "";
        if (dbLink != null) {
            atDbLink = "@"+ dbLink[ 1 ]; //$NON-NLS-1$
            binds.put("NAME", dbLink[ 0 ]); //$NON-NLS-1$
        } else {
            binds.put("NAME", objName); //$NON-NLS-1$     
        }
        binds.put("OWNER", ownerName); //$NON-NLS-1$
        binds.put("SCHEMA_CONTEXT", schemaContext); //$NON-NLS-1$
        binds.put("BODY_WHERE", bodyWhere); //$NON-NLS-1$
        
        String synonymsQuery = 
                  "  union all  " //$NON-NLS-1$
                + "  select ao.object_type,ao.owner,ao.object_name,decode(UPPER(syn.owner), nvl(:OWNER,:SCHEMA_CONTEXT),10,20)  " //$NON-NLS-1$
                + "  from all_objects"+atDbLink+" ao,all_synonyms"+atDbLink+" syn  " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                                                 //^^^^^^^^ was missing before refactoring
                + "  where ao.owner = syn.table_owner  " //$NON-NLS-1$
                + "  and   ao.object_name = syn.table_name  " //$NON-NLS-1$
                + "  and   syn.synonym_name = :NAME  " //$NON-NLS-1$
                + "  and ( UPPER(syn.owner) = nvl(:OWNER,:SCHEMA_CONTEXT) \n"  //$NON-NLS-1$
                + "        OR UPPER(syn.owner) = nvl(:OWNER,'PUBLIC') )" //$NON-NLS-1$
                ;
        if( ignoreSynonyms )
            synonymsQuery = "";

        sql = "select object_type,owner,object_name,rank from (  " //$NON-NLS-1$
                + "  select object_type,owner,object_name,0 rank  " //$NON-NLS-1$
                + "  from all_objects  " //$NON-NLS-1$
                + atDbLink + "  where object_name = :NAME  " //$NON-NLS-1$
                + "  and UPPER(owner) = nvl(:OWNER,:SCHEMA_CONTEXT)  " //$NON-NLS-1$
                + "  and object_type not in ( 'SYNONYM' )   " //$NON-NLS-1$
                + synonymsQuery 
                + " ) where ((:BODY_WHERE='Y') OR  (object_type not in ('PACKAGE BODY'))) AND rownum < 100 order by rank,"+ //$NON-NLS-1$
        "  CASE object_type " + // Ordering object types by their importance (Bug 13820340) //$NON-NLS-1$
        "    WHEN 'TABLE' " +   // ... < VIEW < TABLE //$NON-NLS-1$
        "    THEN 0 " +         //$NON-NLS-1$
        "    WHEN 'VIEW' " +    //$NON-NLS-1$
        "    THEN 1 " +         //$NON-NLS-1$
        "    WHEN 'INDEX' " +   //$NON-NLS-1$
        "    THEN 2 " + //$NON-NLS-1$
        "    WHEN 'PACKAGE' " + //$NON-NLS-1$
        "    THEN 3 " + //$NON-NLS-1$
        "    WHEN 'PACKAGE BODY' " + //$NON-NLS-1$
        "    THEN 4 " + //$NON-NLS-1$
        "    ELSE 5 " + //$NON-NLS-1$
        "  END"; //$NON-NLS-1$        
    
    
        List<Map<String, ?>> ret = executeReturnList(sql, binds); // 16696203: The query returned 90K rows
                                                                       // It took 5 sec on server side
                                                                       // and minites to send it over SQL*Net
                                                                       // The 100 safety limit is ad-hock
        if (ret.size() == 0 && objName != null && ownerName == null) {
            sql = "select 'SCHEMA' object_type , :OWNER owner , :OWNER object_name from all_users where username = :OWNER"; //$NON-NLS-1$
            binds.put("OWNER", objName); //$NON-NLS-1$
            ret = executeReturnList(sql, binds);
        }
    
        Map<String, String> result = null;
        if (ret.size() > 0) result = (Map<String, String>) ret.get(0);
    
        Object o;
        if (result != null && (o = result.get("OBJECT_TYPE")) != null //$NON-NLS-1$
                && ((String) o).indexOf("TABLE PARTITION") != -1) { //$NON-NLS-1$
            result.put("OBJECT_TYPE", "TABLE"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    
        return result;
    
    }
    
    @Override
    public String executeReturnOneCol(String query, Map<String, ? extends Object> binds) {
        return executeOracleReturnOneCol(query, binds);
    }
    
    @Override
    public String executeReturnOneCol(String query) {
        return executeOracleReturnOneCol(query, null);
    }
    
    @Override
    protected PreparedStatement prepareExecuteImpl(final String sql, final Map<String, ?> binds) throws SQLException {
        PreparedStatement stmt = m_conn.prepareCall(sql);
        LOGGER.info(Messages.getString("DBUtil.17") + sql); //$NON-NLS-1$
        if (binds != null) bind(stmt, binds);
        return stmt;
    }

	@Override
	protected boolean checkAccessImpl(String s) {
		boolean hasAccess = false;
		if ( m_conn != null ) {
            Query q = getXMLQueries().getQuery("ORACLE_NAME_RESOLVE", m_conn); //$NON-NLS-1$
			try (CallableStatement stmt = m_conn.prepareCall(q.getSql()) ) {
				// Bind the name to :obj_name
	            ((OracleCallableStatement)stmt).setStringAtName("obj_name", s); //$NON-NLS-1$
	            
	            // Executing the statement will result in an exception if the name doesn't exist or is inaccessible
	            ((CallableStatement)stmt).execute();
	            hasAccess = true;
				
			} catch (SQLException ex) {
				// Didn't have access, so just log the error
	            LOGGER.info(s +oracle.dbtools.raptor.query.Messages.getString("QueryUtils.31")); //$NON-NLS-1$
			}
		}
		return hasAccess;
	}

}
