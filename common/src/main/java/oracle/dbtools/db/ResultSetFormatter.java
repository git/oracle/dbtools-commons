/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Toolkit;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.raptor.backgroundTask.IRaptorTaskProgressUpdater;
import oracle.dbtools.raptor.format.FormatRegistry;
import oracle.dbtools.raptor.format.IResultFormatter;
import oracle.dbtools.raptor.format.ResultSetFormatterWrapper;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.WrapListenBufferOutputStream;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.jdbc.OracleResultSet;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.util.Service;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ResultSetFormatter.java">Barry McGillin</a> 
 *
 */
public class ResultSetFormatter {
    private static final int defaultPixelFactor=7;//size 12 monospace on my mac - override for getfontmetrics exception
    protected int spacePixelWidthMode1=-1; //cache width of space by method1
    protected int spacePixelWidthMode2=-1; //cache width of space by method2
    protected static int s_maxRows;
    protected static int s_maxLines;
    private static FontMetrics s_fmCache;
    private boolean getFontMetricsCalled=false;//only get font metrics once - avoid synchronization
    private static boolean fontMetricsException=false;
    private IRaptorTaskProgressUpdater m_progressUpdater = null;
    private static Logger m_logger = Logger.getLogger(ResultSetFormatter.class.getName());
    protected ScriptRunnerContext m_scriptRunnerContext = null;
    protected ISQLCommand cmd = null;

    public int formatResults(BufferedOutputStream out,ResultSet rset, ISQLCommand cmd) throws IOException, SQLException {
        this.cmd=cmd;
        return formatResults(out, rset, cmd.getSql());
    }
    /**
     * This procedure takes into account the set sqlformat ABC
     * Also the sql hint / * format * /
     * Then formats it properly
     * 
     * Example Usage is:
     * 
     *     ResultSetFormatter rFormat = new ResultSetFormatter(ctx);
     *     rFormat.setProgressUpdater(ctx.getTaskProgressUpdater());
     *     int cnt = rFormat.formatResults(ctx.getOutputStream(), rs, querySql);
     *     
     * @param out
     * @param rset
     * @param sql
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public int formatResults(BufferedOutputStream out,ResultSet rset, String sql) throws IOException, SQLException {
      int cnt=0;
      // is Output Suppressed check now in WrapListenBufferOutput
      List<String> types = FormatRegistry.getAllTypes();
        Boolean wasCacheNext=(Boolean)m_scriptRunnerContext.getProperty(ScriptRunnerContext.MAXROWS_NEXTTRUE);
        Boolean wasToSpoolOnly=(Boolean)m_scriptRunnerContext.getProperty(ScriptRunnerContext.MAXROWS_SPOOLONLY);
        try{
          boolean handled = false;
          String useFormat = (String) m_scriptRunnerContext.getProperty(ScriptRunnerContext.SQL_FORMAT);
          for (String format : types) {
            if (  sql.indexOf("/*" + format + "*/") > 0 ) { //$NON-NLS-1$ //$NON-NLS-2$
              useFormat = format;
            }
          }
          if (rset.getStatement() instanceof PreparedStatement && useFormat != null && !( useFormat.equals("pdf") || useFormat.equals("xls"))) { //$NON-NLS-1$ //$NON-NLS-2$
            IResultFormatter formatter = FormatRegistry.getFormatter(useFormat);
            formatter.setDataProvider(new ResultSetFormatterWrapper(rset,s_maxRows));
            boolean header= m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETHEADING).toString().equals("ON");
            formatter.isHeader(header);
            String tblName = getTableName(sql);
            if( tblName == null )
                tblName = "MYTABLE";
            formatter.setTableName(tblName);
            formatter.setScriptContext(m_scriptRunnerContext);
            formatter.setEncode(ScriptRunnerContext.ALWAYSUTF8); //$NON-NLS-1$ //used for writing to files and in html and xml header - keeping as utf8 for now
            /*
             * if outwriter is not set to null (the value may be left over from last
             * time) setOutputStream is ignored
             */
              formatter.setOutWriter(null);
            formatter.setOutputStream(out);
            cnt = formatter.print();
            handled = true;
          }
          if (!handled) {
              if (sql.startsWith("desc")||(cmd != null && cmd.getStmtSubType().equals(StmtSubType.G_S_DESCRIBE))) { //$NON-NLS-1$
                  
                  if ((m_scriptRunnerContext!=null && 
                          m_scriptRunnerContext.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null && 
                          Boolean.parseBoolean(m_scriptRunnerContext.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString()))) {
                        // classic
                        cnt = rset2sqlplus(rset, rset.getStatement().getConnection(), out);
                  }else {
                      // SQLCl
                      cnt = rset2sqlplusShrinkToSize(rset, rset.getStatement().getConnection(), out);
                  } 
              } else if (sql.indexOf("/*html*/") > 0) { //$NON-NLS-1$
                cnt = rset2html(rset, out);
              } else if (sql.indexOf("/*csv*/") > 0) { //$NON-NLS-1$
                cnt = rset2csv(rset, out);
              } else if (sql.indexOf("/*xml*/") > 0) { //$NON-NLS-1$
                cnt = rset2xml(rset, out);
              } else if (sql.indexOf("/*json*/") > 0) { //$NON-NLS-1$
                cnt = rset2xml(rset, out);
              } else {
                cnt = rset2sqlplus(rset, rset.getStatement().getConnection(), out);
              }
            }
        } finally {
            ResultSetFormatter.clearSpoolOnly(m_scriptRunnerContext, out, wasToSpoolOnly, wasCacheNext);
        }
        return cnt;
      }

    private String getTableName( String orig ) {
        try {
            Parsed parsed = new Parsed(
                                   orig, //"select emp from empno;",
                                   SqlEarley.getInstance(),
                                   "sql_statement" //$NON-NLS-1$
                               );
            ParseNode root = parsed.getRoot();
            for( ParseNode child : root.descendants() ) 
                if( child.to-child.from <= 4  // no inner views
                 && child.contains(SqlEarley.getInstance().getSymbol("query_table_expression")) ) 
                    return Service.handleMixedCase(orig.substring(parsed.getSrc().get(child.from).begin,parsed.getSrc().get(child.to-1).end), false);
        } catch( SyntaxError e ){
        }
        return null;
    }


    
    public void setProgressUpdater(IRaptorTaskProgressUpdater progressUpdater) {
        m_progressUpdater = progressUpdater;
    }

    public ResultSetFormatter() {

    }

    /**
     * allow script runner context in to support column command.
     * and set null command.
     */
    public ResultSetFormatter(final ScriptRunnerContext inScriptRunnerContext) {
        m_scriptRunnerContext = inScriptRunnerContext;
    }

    /**
     * actually I bet access to fm should not be on multiple threads -> synchronize
     * Nice to have: hard code return 7*string override for performance reasons. Or configurable.
     * not sure about getenv system properties etc being performant/thread safe to be configurable
     */
    protected static synchronized int getPixelSize(String theString, int mode) {
        FontMetrics fm=getFontMetricsInternal();
        if (fontMetricsException) {
            //band aid
            return defaultPixelFactor*(theString.length());
        }
        
        //mode = 1 fm.stringWidth(theString)
        if (mode==1) {
            return fm.stringWidth(theString);
        }
        //mode = 2 SwingUtilities
        if (mode==2) {
            return SwingUtilities.computeStringWidth(fm, theString);
        }
        //odd mode
        Logger.getLogger(ResultSetFormatter.class.getName()).log(Level.WARNING,"GETFONTMETRICS INCORRECT MODE", new Exception());  //$NON-NLS-1$
        return defaultPixelFactor*(theString.length());
    }
    protected void getFontMetrics() {
        /* called multiple times so first call/cache fm is unchanged after fm exception issue.
         * Fill cache, avoid synchronise if getFontMetrics has already been called (for this object)*/
        if (!getFontMetricsCalled) {
            getFontMetricsInternal();
            getFontMetricsCalled=true;
        }
    }
    private static synchronized FontMetrics getFontMetricsInternal() {
        if (s_fmCache == null&&fontMetricsException==false) {
            // this may be causing an issue as its not on the EDT and repaint is called during the construction of the BasicEditorPane.
            /*
             * java.lang.Thread.dumpThreads(Native Method) java.lang.Thread.getStackTrace(Thread.java:1383)
             * oracle.dbtools.util.CheckThreadViolationRepaintManager.checkThreadViolations(CheckThreadViolationRepaintManager.java:96)
             * oracle.dbtools.util.CheckThreadViolationRepaintManager.addDirtyRegion(CheckThreadViolationRepaintManager.java:87)
             * javax.swing.JComponent.repaint(JComponent.java:4527) java.awt.Component.repaint(Component.java:2747)
             * javax.swing.text.JTextComponent.setEditable(JTextComponent.java:1535) oracle.javatools.editor.BasicEditorPane.updateEditable(BasicEditorPane.java:2755)
             * oracle.javatools.editor.BasicEditorPane.setEditable(BasicEditorPane.java:1005) javax.swing.text.JTextComponent.<init>(JTextComponent.java:279)
             * javax.swing.JEditorPane.<init>(JEditorPane.java:173) oracle.javatools.editor.BasicEditorPane.<init>(BasicEditorPane.java:174)
             * oracle.dbtools.db.ResultSetFormatter.getFontMetrics(ResultSetFormatter.java:43)
             */
            // put it on the EDT just to be safe, even though we are not making it visible
            // BasicEditorPane textPanelArea = new BasicEditorPane();
//            final BasicEditorPane[] textPanels = { null };
//            try {
//                if (SwingUtilities.isEventDispatchThread()) {
//                    textPanels[ 0 ] = new BasicEditorPane();
//                } else {
//                    SwingUtilities.invokeAndWait(new Runnable() {
//                        public void run() {
//                            textPanels[ 0 ] = new BasicEditorPane();
//                        }
//                    });
//                }
//            } catch (InterruptedException e) {
//                m_logger.log(Level.SEVERE, e.getStackTrace()[ 0 ].toString(), e);
//            } catch (InvocationTargetException e) {
//                m_logger.log(Level.SEVERE, e.getStackTrace()[ 0 ].toString(), e);
//            }
//            BasicEditorPane textPanelArea = textPanels[ 0 ];

            //Font font = textPanelArea.getFont();// new Font("Courier",
            // Font.PLAIN, 12); //
            // font used to display
            // the result in the
            // worksheet
            
//            textPanelArea.setFont(new Font("Courier", Font.PLAIN, 12));
//            s_fm = textPanelArea.getFontMetrics(textPanelArea.getFont());
             //monospaced is a virtual name maps to default fixed width font
             //"Courier new" is microsoft specific. 
             //Note if this comes back with a variable length font you may get fractional (rounded to 0) length padding -> exception.
             try {
                 Font font = new Font("Monospaced", Font.PLAIN, 12); //$NON-NLS-1$
                 s_fmCache = Toolkit.getDefaultToolkit().getFontMetrics(font);
             } catch (Exception e) {
                 //likely to mis calculate size on no fonts and unicode
                 Logger.getLogger(ResultSetFormatter.class.getName()).log(Level.WARNING,"GETFONTMETRICS ERROR", e);  //$NON-NLS-1$
                 fontMetricsException=true;
             }

        }
        return s_fmCache;
    }

    public static void setMaxRows(int maxRows) {
        s_maxRows = maxRows;
    }

  public int rset2sqlplus(ResultSet rset, Connection conn, BufferedOutputStream out) throws IOException, SQLException {
      int cnt = 0;
      cnt = rset2sqlplus(rset, conn, (Object)out);
        // setup Spool as a consumer now
        // FIXME: Raghu: 2.0: Remove references to any command listener
        // if(SetSpool.spoolOut != null){
        // SetSpool.spoolOut.write(buf.toString().getBytes("UTF-8"));
        // SetSpool.spoolOut.write("\n\n".getBytes());
        // }
      return cnt;
    }
    
    
  public int rset2sqlplusShrinkToSize(ResultSet rset, Connection conn, BufferedOutputStream out) throws IOException, SQLException {
      int cnt = 0;
      StringBuffer buf = new StringBuffer();
      cnt = rset2sqlplusShrinkToSize(rset, conn, buf);
      // setup Spool as a consumer now
      // FIXME: Raghu: 2.0: Remove references to any command listener
      // if(SetSpool.spoolOut != null){
      // SetSpool.spoolOut.write(buf.toString().getBytes("UTF-8"));
      // SetSpool.spoolOut.write("\n\n".getBytes());
      // }

      out.write(buf.toString().getBytes(this.m_scriptRunnerContext.getEncoding())); //$NON-NLS-1$
      return cnt;
  }
    public int rset2sqlplus(ResultSet rset, Connection conn, Object out) throws IOException, SQLException {
        return rset2sqlplus(rset, conn, out, null);
    }
    public int rset2sqlplusShrinkToSize(ResultSet rset, Connection conn, StringBuffer out) throws IOException, SQLException {
        return rset2sqlplusShrinkToSize(rset, conn, out, null);
    }


    /**
     * Allow error stringbuffer to be passed in to show error if real stringuffer is ignored.
     * Shrink to size - cache all rows to figure out row column with - so describe will not be over padded with spaces due to potential long type names.
     */
    public int rset2sqlplusShrinkToSize(ResultSet rset, Connection conn, StringBuffer buf, StringBuffer errbuf) throws IOException, SQLException {
        int cnt = 0;
        ResultSetMetaData rmeta = rset.getMetaData();// VersionInfo
        int colCnt = rmeta.getColumnCount();
        int[] colsizes = new int[ colCnt + 1 ];
        StringBuffer header = new StringBuffer();
        StringBuffer headerLine = new StringBuffer();
        String[] columns = new String[ colCnt + 1 ];
        String[] lastVal = null;
        boolean newVal=(this.m_scriptRunnerContext!=null)&&
                (this.m_scriptRunnerContext.getColumnMap()!=null)&&
                (this.m_scriptRunnerContext.getColumnMap().size()>0);
        int[] displaySizeArray = new int[colCnt + 1];
        int[] columnTypeArray = new int[colCnt + 1];
        String[] columnNameArray = new String[colCnt + 1];
        int[] sizeSoFar = new int[colCnt + 1];
        ArrayList<StringBuffer[]> cacheAllRows = new ArrayList<StringBuffer[]>();
        for (int i = 1; i < colCnt + 1; i++) {
            displaySizeArray[i] = rmeta.getColumnDisplaySize(i);
            columnTypeArray[i] = rmeta.getColumnType(i);
            columnNameArray[i] = rmeta.getColumnName(i);
            sizeSoFar[i] = 0;
        }
        
        while (rset.next() && cnt < ResultSetFormatter.s_maxRows) {
            // check to see if the thread has paused
            checkCanProceed();
            StringBuffer sb[]=new StringBuffer[colCnt+1];
            cnt++;
            for (int i = 1; i < colCnt + 1; i++) {
                Object o = null;
                // if ( false /*5592534: rset instanceof OracleResultSet*/ ){
                // Bug No:5690176 - need getOracleObject where possible.
                if (rset instanceof OracleResultSet) {
                    try {
                        o = ((OracleResultSet) rset).getOracleObject(i);
                    } catch (SQLException e) {
                        o = rset.getObject(i);
                    }
                } else {
                    o = rset.getObject(i);
                }
                if ((this.m_scriptRunnerContext != null) && (o == null)) {
                    String setNull = (String) this.m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETNULL);
                    if ((setNull!=null)&&(!(setNull.equals("")))){ //$NON-NLS-1$
                        o = setNull;
                    }
                }
                String s = DataTypesUtil.stringValue(o, conn);

                StringBuffer nlsed = new StringBuffer();
                if (s != null) {

                    nlsed = new StringBuffer((DataTypesUtil.stringValue(s, rset.getStatement().getConnection())).replace("\r\n", "\n")); //$NON-NLS-1$ //$NON-NLS-2$
                }
                if (nlsed.length()>sizeSoFar[i]) {
                    sizeSoFar[i]=nlsed.length();
                }
                sb[i]=nlsed;
            }
            cacheAllRows.add(sb);
        }
        for (int i = 1; i < colCnt + 1; i++) {
            int displaySize = sizeSoFar[i];
            if (displaySize > 4000 || displaySize < 0) displaySize = 4000;
            if (columnTypeArray[i] == 91 || columnTypeArray[i] == 92 || columnTypeArray[i] == 93) {
                colsizes[ i ] = 25;
            } else if (displaySize < columnNameArray[i].length()) {
                colsizes[ i ] = columnNameArray[i].length();
            } else {
                colsizes[ i ] = displaySize;
            }
            columns[ i ] = columnNameArray[i];
            header.append(columns[ i ]);
            for (int j = 0, jj = (colsizes[ i ] - columnNameArray[i].length()); j < jj; j++) {
                header.append(" "); //$NON-NLS-1$
            }
            for (int j = 0, jj = colsizes[ i ]; j < jj; j++) {
                headerLine.append("-"); //$NON-NLS-1$
            }

            header.append(" "); //$NON-NLS-1$
            headerLine.append(" "); //$NON-NLS-1$
        }
        buf.append(header.toString());
        buf.append("\n"); //$NON-NLS-1$
        buf.append(headerLine.toString());
        buf.append("\n"); //$NON-NLS-1$


        for (StringBuffer[] rowSb:cacheAllRows) {
            ArrayList<StringBuffer> cacheRow = new ArrayList<StringBuffer>();
            for (int i = 0; i < colCnt + 1; i++) {
                cacheRow.add(rowSb[i]);
            }
            boolean multiline = false;
            boolean stopit = false;
            int spacePixelWidth=-1;
            if (spacePixelWidthMode1==-1) {
               spacePixelWidthMode1 = ResultSetFormatter.getPixelSize(" ", 1); //$NON-NLS-1$
            }
            spacePixelWidth=spacePixelWidthMode1;
            while (stopit == false) {
                stopit = true;
                for (int i = 1; i < colCnt + 1; i++) {

                    // next chunk is to next newline or all
                    StringBuffer nextChunk = cacheRow.get(i);
                    int ival = nextChunk.indexOf("\n"); //$NON-NLS-1$
                    if (ival != -1) {
                        multiline = true;
                        StringBuffer store = nextChunk;
                        nextChunk = new StringBuffer(nextChunk.substring(0, ival));
                        if ((store.length() - 1) > ival) {
                            store = new StringBuffer(store.substring(ival + 1));
                        } else {
                            store = new StringBuffer(""); //$NON-NLS-1$
                        }
                        cacheRow.set(i, store);
                        stopit = false;
                    } else {
                        cacheRow.set(i, new StringBuffer("")); //$NON-NLS-1$
                    }
                    if ((this.m_scriptRunnerContext != null)&&(newVal)) {
                        if (lastVal == null) {
                            lastVal = new String[colCnt + 1];
                        }
                        lastVal[i] = cacheRow.get(i).toString();
                    }
                    // pixelwidth calcualted for a particular font due to
                    // unicode width issues
                    int pixelWidth = ResultSetFormatter.getPixelSize(nextChunk.toString(), 1);

                    if (pixelWidth < (colsizes[ i ] * spacePixelWidth)) {

                        // how many spaces to add
                        int pixelsRequired = (colsizes[ i ] * spacePixelWidth) - pixelWidth;
                        int spacesRequired = pixelsRequired / spacePixelWidth;

                        for (int j = 0; j < spacesRequired; j++) {
                            nextChunk.append(" "); //$NON-NLS-1$
                        }
                    }

                    // No Vertical bars so that the result is SQL*Plus like
                    nextChunk.append(" "); //$NON-NLS-1$
                    buf.append(nextChunk.toString());
                }
                buf.append("\n"); //$NON-NLS-1$
            }
            if (multiline == true) {
                buf.append("\n"); //$NON-NLS-1$
            }
        }
        if (cnt == ResultSetFormatter.s_maxRows && rset.next()) {
            if (errbuf == null) {
                logMaxReached(buf);
            } else {
                logMaxReached(errbuf);
            }
        }
        if ((this.m_scriptRunnerContext != null)&&(newVal)) {
            //handle sqlplus: column ... new_val...
            if (cnt==0) {
                this.m_scriptRunnerContext.updateColumn(columns, null, null);
            } else {
                this.m_scriptRunnerContext.updateColumn(columns, lastVal, null);
            }
        }
        return cnt;
    }


    
    /**
     * Allow error stringbuffer to be passed in to show error if real stringuffer is ignored.
     */
    public int rset2sqlplus(ResultSet rset, Connection conn, Object out, StringBuffer errbuf) throws IOException, SQLException {
        int cnt = 0;
        ResultSetMetaData rmeta = rset.getMetaData();// VersionInfo
        int colCnt = rmeta.getColumnCount();
        int[] colsizes = new int[ colCnt + 1 ];
        StringBuffer header = new StringBuffer();
        StringBuffer headerLine = new StringBuffer();
        String[] columns = new String[ colCnt + 1 ];
        String[] lastVal = null;
        boolean newVal=(this.m_scriptRunnerContext!=null)&&
                (this.m_scriptRunnerContext.getColumnMap()!=null)&&
                (this.m_scriptRunnerContext.getColumnMap().size()>0);
        for (int i = 1; i < colCnt + 1; i++) {
            int displaySize = rmeta.getColumnDisplaySize(i);
            if (rmeta.getColumnType(i) == -3) {
                displaySize = displaySize * 2;// VARBINARY which is encoded to 2 characters per byte bug 5390196
            }
            if (displaySize==0) {//nulls Bug 19442242
                displaySize=1;
            }
            // FIXME: Raghu. Temp fix for 5904607
            if (displaySize > 4000 || displaySize <= 0) displaySize = 4000;
            if (rmeta.getColumnType(i) == 91 || rmeta.getColumnType(i) == 92 || rmeta.getColumnType(i) == 93) {
                colsizes[ i ] = 25;
            } else if (displaySize < rmeta.getColumnName(i).length()) {
                colsizes[ i ] = rmeta.getColumnName(i).length();
            } else {
                colsizes[ i ] = displaySize;
            }
            columns[ i ] = rmeta.getColumnName(i);
            header.append(columns[ i ]);
            for (int j = 0, jj = (colsizes[ i ] - rmeta.getColumnName(i).length()); j < jj; j++) {
                header.append(" "); //$NON-NLS-1$
            }
            for (int j = 0, jj = colsizes[ i ]; j < jj; j++) {
                headerLine.append("-"); //$NON-NLS-1$
            }

            header.append(" "); //$NON-NLS-1$
            headerLine.append(" "); //$NON-NLS-1$
        }
        ArrayList<StringBuffer> cacheRow = new ArrayList<StringBuffer>();        
        while ((ResultSetFormatter.replaceNext(cnt, s_maxRows, m_scriptRunnerContext, out, rset, errbuf,null)) && (cnt < ResultSetFormatter.s_maxRows||ResultSetFormatter.maxRowsSpoolOnly(m_scriptRunnerContext))) {
            if (cnt==0) {
                 write(out,header.toString());
                 write(out,"\n"); //$NON-NLS-1$
                 write(out,headerLine.toString());
                 write(out,"\n"); //$NON-NLS-1$
                 for (int i = 0; i < colCnt + 1; i++) {
                     cacheRow.add(new StringBuffer());
                 }
            }
            // check to see if the thread has paused
            checkCanProceed();
            cnt++;
            for (int i = 1; i < colCnt + 1; i++) {
                Object o = null;
                // if ( false /*5592534: rset instanceof OracleResultSet*/ ){
                // Bug No:5690176 - need getOracleObject where possible.
                if (rset instanceof OracleResultSet) {
                    try {
                        o = ScriptUtils.getOracleObjectWrap((OracleResultSet) rset,i);//((OracleResultSet) rset).getOracleObject(i);
                    } catch (SQLException e) {
                        o = rset.getObject(i);
                    }
                } else {
                    o = rset.getObject(i);
                }
                if ((this.m_scriptRunnerContext != null) && (o == null)) {
                    String setNull = (String) this.m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETNULL);
                    if ((setNull!=null)&&(!(setNull.equals("")))){ //$NON-NLS-1$
                        o = setNull;
                    }
                }
                String s = DataTypesUtil.stringValue(o, conn);

                StringBuffer nlsed = new StringBuffer();
                if (s != null) {

                    nlsed = new StringBuffer((DataTypesUtil.stringValue(s, rset.getStatement().getConnection())).replace("\r\n", "\n")); //$NON-NLS-1$ //$NON-NLS-2$
                }
                if ((this.m_scriptRunnerContext != null)&&(newVal)) {
                    if (lastVal == null) {
                        lastVal = new String[ colCnt + 1 ];
                    }
                    lastVal[ i ] = nlsed.toString();
                }
                cacheRow.set(i, nlsed);
            }
            boolean multiline = false;
            boolean stopit = false;

            int spacePixelWidth;
            if (spacePixelWidthMode1==-1) {
               spacePixelWidthMode1 = ResultSetFormatter.getPixelSize(" ", 1); //$NON-NLS-1$
            }
            spacePixelWidth=spacePixelWidthMode1;
            while (stopit == false) {
                stopit = true;
                for (int i = 1; i < colCnt + 1; i++) {

                    // next chunk is to next newline or all
                    StringBuffer nextChunk = cacheRow.get(i);
                    int ival = nextChunk.indexOf("\n"); //$NON-NLS-1$
                    if (ival != -1) {
                        multiline = true;
                        StringBuffer store = nextChunk;
                        nextChunk = new StringBuffer(nextChunk.substring(0, ival));
                        if ((store.length() - 1) > ival) {
                            store = new StringBuffer(store.substring(ival + 1));
                        } else {
                            store = new StringBuffer(""); //$NON-NLS-1$
                        }
                        cacheRow.set(i, store);
                        stopit = false;
                    } else {
                        cacheRow.set(i, new StringBuffer("")); //$NON-NLS-1$
                    }
                    // pixelwidth calcualted for a particular font due to
                    // unicode width issues
                    int pixelWidth = ResultSetFormatter.getPixelSize(nextChunk.toString(), 1);

                    if (pixelWidth < (colsizes[ i ] * spacePixelWidth)) {

                        // how many spaces to add
                        int pixelsRequired = (colsizes[ i ] * spacePixelWidth) - pixelWidth;
                        int spacesRequired = pixelsRequired / spacePixelWidth;

                        for (int j = 0; j < spacesRequired; j++) {
                            nextChunk.append(" "); //$NON-NLS-1$
                        }
                    }

                    // No Vertical bars so that the result is SQL*Plus like
                    nextChunk.append(" "); //$NON-NLS-1$
                    write(out,nextChunk.toString());
                }
                write(out,"\n"); //$NON-NLS-1$
            }
            if (multiline == true) {
                write(out,"\n"); //$NON-NLS-1$
            }
        }
        if (cnt==0) {
            //No rows so lets write that out.
          write(out,"\n"); //$NON-NLS-1$
             write(out, Messages.getString("ResultSetFormatter.4")); //$NON-NLS-1$
        }
        if ((!(ResultSetFormatter.maxRowsSpoolOnly(m_scriptRunnerContext)))&&(cnt == ResultSetFormatter.s_maxRows && rset.next())) {
            if (errbuf == null) {
                logMaxReached(out);
            } else {
                logMaxReached(errbuf);
            }
        }

        if ((this.m_scriptRunnerContext != null)&&(newVal)) {
            //handle sqlplus: column ... new_val...
            if (cnt==0) {
                this.m_scriptRunnerContext.updateColumn(columns, null, null);
            } else {
                this.m_scriptRunnerContext.updateColumn(columns, lastVal, null);
            }
        }
        return cnt;
    }
    
    protected void write(Object out, String output) throws IOException {
        if(out instanceof BufferedOutputStream){
            write((BufferedOutputStream)out,output);
        } else if( out instanceof StringBuffer){
            write((StringBuffer)out,output);
        }
    }
    protected void write(BufferedOutputStream out, String output) throws IOException {
        try {
            out.write(output.getBytes(this.m_scriptRunnerContext.getEncoding())); //$NON-NLS-1$
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }
    
    protected void write(StringBuffer out, String output) {
            out.append(output);
    }
    
    protected void checkCanProceed() {
        if (m_progressUpdater != null) {
            try {
                m_progressUpdater.checkCanProceed();
            } catch (ExecutionException e) {
            }
        }
    }

    public int rset2csv(ResultSet rset, BufferedOutputStream out) throws IOException, SQLException {
        int cnt = 0;

        ResultSetMetaData rmeta = rset.getMetaData();
        int colCnt = rmeta.getColumnCount();
        while ((ResultSetFormatter.replaceNext(cnt, s_maxRows, m_scriptRunnerContext, out, rset, null,null))&& (cnt < ResultSetFormatter.s_maxRows||ResultSetFormatter.maxRowsSpoolOnly(m_scriptRunnerContext))) {
            cnt++;
            for (int i = 0; i < colCnt; i++) {
                if (rset.getString(i + 1) != null) {
                    out.write('"');
                    out.write(DataTypesUtil.stringValue(rset.getObject(i + 1), rset.getStatement().getConnection()).getBytes(this.m_scriptRunnerContext.getEncoding())); //$NON-NLS-1$
                    out.write('"');
                }
                if (i != colCnt) {
                    out.write(',');
                }
            }
            out.write('\n');
        }
        if ((!ResultSetFormatter.maxRowsSpoolOnly(m_scriptRunnerContext))&&(cnt == ResultSetFormatter.s_maxRows && rset.next())) logMaxReached(out);
        return cnt;
    }

    public int rset2xml(ResultSet rset, BufferedOutputStream out) throws IOException, SQLException {
        int cnt = 0;
        int colCnt = rset.getMetaData().getColumnCount();

        out.write("<results>".getBytes()); //$NON-NLS-1$
        while ((ResultSetFormatter.replaceNext(cnt, s_maxRows, m_scriptRunnerContext, out, rset, null,null)) && (cnt < ResultSetFormatter.s_maxRows||ResultSetFormatter.maxRowsSpoolOnly(m_scriptRunnerContext))) {
            cnt++;
            for (int i = 0; i < colCnt; i++) {
                out.write("<![CDATA[".getBytes()); //$NON-NLS-1$
                if (rset.getBytes(i + 1) != null) out.write(DataTypesUtil.stringValue(rset.getObject(i + 1), rset.getStatement().getConnection()).getBytes(m_scriptRunnerContext.getEncoding())); //$NON-NLS-1$
                out.write("]]>".getBytes()); //$NON-NLS-1$
            }
        }
        out.write("</results>".getBytes()); //$NON-NLS-1$
        if ((!ResultSetFormatter.maxRowsSpoolOnly(m_scriptRunnerContext))&&(cnt == ResultSetFormatter.s_maxRows && rset.next())) logMaxReached(out);
        return cnt;
    }

    public int rset2html(ResultSet rset, BufferedOutputStream out) throws IOException, SQLException {
        int cnt = 0;
        int colCnt = rset.getMetaData().getColumnCount();

        out.write("<html>\n<body>\n<table>\n".getBytes()); //$NON-NLS-1$
        while ((ResultSetFormatter.replaceNext(cnt, s_maxRows, m_scriptRunnerContext, out, rset, null,null)) && (cnt < ResultSetFormatter.s_maxRows||ResultSetFormatter.maxRowsSpoolOnly(m_scriptRunnerContext))) {
            out.write("<tr>\n".getBytes()); //$NON-NLS-1$
            cnt++;
            for (int i = 0; i < colCnt; i++) {
                out.write("<td>".getBytes()); //$NON-NLS-1$
                if (rset.getBytes(i + 1) != null) out.write(DataTypesUtil.stringValue(rset.getObject(i + 1), rset.getStatement().getConnection()).toString().getBytes(m_scriptRunnerContext.getEncoding())); //$NON-NLS-1$
                out.write("</td>".getBytes()); //$NON-NLS-1$
            }
            out.write("</tr>\n".getBytes()); //$NON-NLS-1$
        }
        out.write("</table></body></html>".getBytes()); //$NON-NLS-1$
        if ((!ResultSetFormatter.maxRowsSpoolOnly(m_scriptRunnerContext))&&(cnt == ResultSetFormatter.s_maxRows && rset.next())) logMaxReached(out);
        return cnt;
    }

    public void logMaxReached(Object out) throws IOException {
        if(out instanceof BufferedOutputStream){
            logMaxReached((BufferedOutputStream) out);
        } else if(out instanceof StringBuffer){
            logMaxReached((StringBuffer) out);
        }
    }
    public void logMaxReached(BufferedOutputStream out) throws IOException {
        String msg = MessageFormat.format(Messages.getString("ResultSetFormatter.30"), new Object[] { ResultSetFormatter.s_maxRows }); //$NON-NLS-1$
        out.write(msg.getBytes(this.m_scriptRunnerContext.getEncoding())); //$NON-NLS-1$
       }

    public static void logMaxReached(StringBuffer buf) throws IOException {
        buf.append(MessageFormat.format(Messages.getString("ResultSetFormatter.31"), new Object[] { ResultSetFormatter.s_maxRows })); //$NON-NLS-1$
    }

    public static void setMaxLines(int maxLines) {
        s_maxLines = maxLines;
    }
    public static int getMaxLines(){
        return s_maxLines;
    }
    
    public static void main( String[] args ) {
          ResultSetFormatter rsf = new ResultSetFormatter();
        //System.out.println(rsf.getTableName("select * from scott.\"Mixedcase\" e")); //$NON-NLS-1$
        System.out.println(rsf.getTableName("select * from scott.emp e")); //$NON-NLS-1$
        System.out.println(rsf.getTableName("select * from (select * from emp) e")); //$NON-NLS-1$
    }
    
    /**
     * keep going to spool if you can ie wrapped output and can set ctx to spool only
     * @param ctx
     * @param out
     * @param rset
     * @param errBuf
     * @return
     */
    public static boolean setSpoolOnlyIfWrapped(ScriptRunnerContext ctx, Object out, ResultSet rset, StringBuffer errBuf, IResultFormatter iResFor){
        if ((!maxRowsSpoolOnly(ctx))&&(ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER)!=null)) {
            if (out instanceof WrapListenBufferOutputStream) {
                try {
                    if (rset.next()) {
                        ctx.putProperty(ScriptRunnerContext.MAXROWS_NEXTTRUE, Boolean.TRUE);
                        try{
                            if (errBuf!=null) {
                                logMaxReached(errBuf);
                            } else {
                                if (iResFor!=null) {
                                    if (iResFor.getOutWriter()!=null) {
                                        iResFor.getOutWriter().flush();
                                    }
                                    if (iResFor.getOutputStream()!=null) {
                                        iResFor.getOutputStream().flush();
                                    }
                                }
                                WrapListenBufferOutputStream theWrap=((WrapListenBufferOutputStream) out);
                                theWrap.flush();
                                String msg = MessageFormat.format(Messages.getString("ResultSetFormatter.32"), new Object[] { ResultSetFormatter.s_maxRows }); //$NON-NLS-1$
                                theWrap.writeToNotSpool(("\n"+msg).getBytes(ctx.getEncoding())); //$NON-NLS-1$ //$NON-NLS-2$
                                theWrap.flush();
                                //logMaxReached(out);
                            }
                        } catch (IOException e) {
                            Logger.getLogger(ResultSetFormatter.class.getName()).log(Level.WARNING,"IO ERROR", e);
                        }
                        ctx.putProperty(ScriptRunnerContext.MAXROWS_SPOOLONLY, Boolean.TRUE);
                        return true;
                    }
                } catch (SQLException e) {
                    Logger.getLogger(ResultSetFormatter.class.getName()).log(Level.WARNING,"Rset.next failed", e);
                }
            }
        }
        return false;
    }

    /**
     * reset settings to what they were before
     * @param ctx
     * @param out
     * @param wasToSpoolOnly reset to previous i.e. nested OK? just this one OK nested - print out internal result sets OK 
     * - left for now (internal long result sets -> not out to spool only if not nested).
     * i.e. code makes sure an internal result set does not change current tospool only setting to false.
     */
    public static void clearSpoolOnly(ScriptRunnerContext ctx, Object out, Boolean wasToSpoolOnly, Boolean wasCacheNext){
        if (maxRowsSpoolOnly(ctx)) {
            if (out instanceof WrapListenBufferOutputStream) {
                try {
                    ((WrapListenBufferOutputStream) out).flush();
                } catch (IOException e) {
                    Logger.getLogger(ResultSetFormatter.class.getName()).log(Level.WARNING,"IO ERROR", e);
                }
            }
        }
        ctx.putProperty(ScriptRunnerContext.MAXROWS_SPOOLONLY, /*Boolean.FALSE*/ wasToSpoolOnly);//faster check null == false
        ctx.putProperty(ScriptRunnerContext.MAXROWS_NEXTTRUE, wasCacheNext);
    }
    /**
     * assumption no call between while (..setspoolonlyifwrapped) and cachednext
     * not special case count =1 (where getObject is cached) avoided by not honoring maxrows <2.
     * @param ctx
     * @return
     */
    public static Boolean cachedNext(ScriptRunnerContext ctx) {
        Boolean curVal=(Boolean) ctx.getProperty(ScriptRunnerContext.MAXROWS_NEXTTRUE);
        if (curVal==null) {
            return Boolean.FALSE;
        }
        ctx.putProperty(ScriptRunnerContext.MAXROWS_NEXTTRUE, Boolean.FALSE);
        return curVal;
    }
    /**
     * helper to look up value and handle null
     * @param ctx
     * @return
     */
    public static boolean maxRowsSpoolOnly(ScriptRunnerContext ctx) {
        Boolean isSet=(Boolean) ctx.getProperty(ScriptRunnerContext.MAXROWS_SPOOLONLY);
        if (isSet==null) {
            isSet=Boolean.FALSE;
        }
        return isSet;
    }
    /* helper if currently on keep going or switch spool only on and cache a next - next cache to prove you go over limit. Assumption ctx used coherently (reused or new value))*/
    public static boolean continueIfPossible (long cnt, long s_maxRows, ScriptRunnerContext m_scriptRunnerContext, Object out, ResultSet rset, StringBuffer errbuf, IResultFormatter iResFor) {
        return (((m_scriptRunnerContext.getProperty(ScriptRunnerContext.MAXROWS_SPOOLONLY)!=null
        &&(m_scriptRunnerContext.getProperty(ScriptRunnerContext.MAXROWS_SPOOLONLY).equals(Boolean.TRUE))))||
                (cnt==s_maxRows&&cnt>1&&ResultSetFormatter.setSpoolOnlyIfWrapped(m_scriptRunnerContext, out, rset, errbuf, iResFor)));
    }
    /**
     * helper next replacement as continue if possible but unwind cached next if set and call rset.next under not setting up keep spooling
     * Purposely used long instead of int so caller can be int or long - and result not passed back so no long to int leaky conversion.
     */
    public static boolean replaceNext(long cnt, long s_maxRows, ScriptRunnerContext m_scriptRunnerContext, Object out, ResultSet rset, StringBuffer errbuf, IResultFormatter iResFor) throws SQLException{
        /* read somewhere max_rows=0 =infinity. below limit ||fired keep going */
        if (s_maxRows==0||cnt<s_maxRows||(m_scriptRunnerContext.getProperty(ScriptRunnerContext.MAXROWS_SPOOLONLY)!=null
                &&(m_scriptRunnerContext.getProperty(ScriptRunnerContext.MAXROWS_SPOOLONLY).equals(Boolean.TRUE)))) {
            return rset.next();
        }
        /* switch on*/
        return (cnt==s_maxRows&&cnt>1&&(ResultSetFormatter.setSpoolOnlyIfWrapped(m_scriptRunnerContext, out, rset, errbuf, iResFor)
                &&((cachedNext(m_scriptRunnerContext)!=null)/*undo cache*/)));
    }

}
