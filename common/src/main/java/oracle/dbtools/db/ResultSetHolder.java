/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.lang.ref.WeakReference;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.utils.MessageLogging;

/**
 * Holds a ResultSet and Statement for tracking. The {@link ResultSetWrapper} implementation is separated into two pieces. ResultSetWrapper constitutes the public portion of the
 * implementation, while the ResultSetHolder constitutes the private portion. The ResultSetWrapper contains a direct reference to the ResultSetHolder, which in turn has a direct
 * reference to the underlying ResultSet and Statement. A ResultSetWrapper instance is tracked using a {@link WeakReference}. The WeakReference contains an additional reference to
 * the ResultSetHolder. In this manner, the ResultSetHolder can be closed if the ResultSetWrapper instance is garbage collected.
 * <p>
 * ResultSetHolder provides additional support for tracking the call stack of the creating method when running in a debug build. This information can then be used to report any
 * unclosed ResultSetWrapper instances that get garbage collected.
 * <p>
 * 
 * @author jmcginni
 * 
 */
final class ResultSetHolder {
    private static final String MSG = "Unclosed ResultSetWrapper: {0}.{1} ({2}) called by {3}.{4} ({5})"; //$NON-NLS-1$

    private static class UnclosedWrapperException extends Exception {
		private static final long serialVersionUID = 1L;
    }

    private static final Logger s_logger = Logger.getLogger(ResultSetHolder.class.getName());

    private final UnclosedWrapperException m_ex;

    @SuppressWarnings("unused")
    private final long m_created = System.currentTimeMillis();

    private final ResultSet m_rs;
    private final Statement m_stmt;

    ResultSetHolder(ResultSet rs, Statement stmt) {
        m_rs = rs;
        m_stmt = stmt;

        if (DBUtil.DEBUG_BUILD) {
            m_ex = new UnclosedWrapperException();
        } else {
            m_ex = null;
        }
    }

    ResultSet getResultSet() {
        return m_rs;
    }

    void logStack(boolean dumpStack) {
        if (m_ex != null) {
            StackTraceElement[] stack = m_ex.getStackTrace();
            // Dump out some details to the log
            int idx = 2; // start with the caller of ResultSetWrapper.createWrapper()

            while (idx < stack.length) {
                StackTraceElement callee = stack[ idx - 1 ];
                StackTraceElement caller = stack[ idx ];

                String name = caller.getClassName();
                if (!name.startsWith(DBUtil.class.getName())) {
                    // Ok, found the caller

                    // First, log the error:
                    Object[] fmtArgs = { callee.getClassName(), callee.getMethodName(), callee.getLineNumber(), caller.getClassName(), caller.getMethodName(),
                            caller.getLineNumber() };
                    s_logger.log(Level.SEVERE, MSG, fmtArgs);

                    if (dumpStack) {
                        MessageLogging.getInstance().reportAPIException(MessageFormat.format(MSG, fmtArgs), m_ex, callee.getMethodName());
                    }
                    break;
                }
                idx++;
            }
        }
    }

    void close() {
        ResultSet rs = null;
        Statement stmt = null;

        synchronized (this) {
            rs = m_rs;
            stmt = m_stmt;
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (Exception ex) {
                // Swallow exception
            }
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch (Exception ex) {
                // Swallow exceptions on close
            }
        }
    }
}
