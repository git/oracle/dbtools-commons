/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Tracks ResultSetWrapper usages. ResultSetWrapper instances are tracked via a {@link WeakReference}.
 * A {@link ReferenceQueue} associated with the WeakReferences is then used to identify ResultSetWrapper
 * instances that have been garbage collected before being explicitly closed.
 * @author jmcginni
 *
 */
class ResultSetTracker
{
    private static final ReferenceQueue<ResultSetWrapper> s_queue = new ReferenceQueue<ResultSetWrapper>();
    private static final Collection<TrackerEntry> s_entries = new ArrayList<TrackerEntry>();
    
    /**
     * Start tracking a ResultSetWrapper.
     * @param wrapper the ResultSetWrapper to track
     */
    static void trackResultSet( ResultSetWrapper wrapper )
    {
        synchronized( s_entries )
        {
            cleanupDeadWrappers();
            s_entries.add( new TrackerEntry( wrapper ) );
        }
    }
    
    /**
     * Stop tracking a ResultSetWrapper.
     * @param wrapper the ResultSetWrapper that no longer needs to be tracked
     */
    static void untrackResultSet( ResultSetWrapper wrapper )
    {
        synchronized( s_entries )
        {
            TrackerEntry entry = new TrackerEntry( wrapper );
            s_entries.remove( entry );

            cleanupDeadWrappers();
        }
    }
    
    /**
     * Traverse the {@link ReferenceQueue} and clean up any tracked ResultSetWrappers that
     * have been garbage collected. If the ResultSetHolder associated with the ResultSetWrapper
     * is still alive (as shown by still having an entry in the tracking list), then the
     * ResultSetWrapper was not correctly closed before being garbage collected. This
     * API violation will be reported to the user in a debug build. In all cases, the ResultSetHolder
     * will be closed.
     */
    private static void cleanupDeadWrappers()
    {
        TrackerEntry entry;
        while ( ( entry = ( TrackerEntry ) s_queue.poll() ) != null )
        {
            // Check to see if we are still tracking the entry
            if ( s_entries.remove( entry ) )
            {
                // If so, and the wrapper is dead, then we need to log the error
                // and close the resultset/statement
                ResultSetHolder holder = entry.m_holder;
                holder.logStack( true );
                holder.close();
            }
        }
    }
    
    private static class TrackerEntry
        extends WeakReference<ResultSetWrapper>
    {
        private final ResultSetHolder m_holder;
        
        TrackerEntry( ResultSetWrapper wrapper )
        {
            super( wrapper, s_queue );
            m_holder = wrapper.getHolder();
        }
        
        public boolean equals( Object o )
        {
            return o instanceof TrackerEntry && ( ( TrackerEntry ) o ).m_holder == m_holder;
        }
        
        public int hashCode()
        {
            return m_holder.hashCode();
        }
        
    }
}
