/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.lang.ref.WeakReference;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Wrapper class for {@link ResultSet} instances. ResultSetWrapper provides a mechanism
 * for tracking the usage of ResultSet instances. As a ResultSetWrapper is created, it
 * is added to a list of open wrappers. Closing a wrapper removes it from the list. 
 * <p>
 * The ResultSetWrapper instances are tracked using {@link WeakReference}s. At the time
 * the reference is garbage collected, the wrapper is checked to see whether it has been
 * closed. An unclosed wrapper represents an incorrect API usage, and will be flagged in a
 * debug build.
 * <p>
 * @author jmcginni
 *
 */
public final class ResultSetWrapper
{
    private ResultSetHolder m_holder;
    
    /**
     * Construct a new ResultSetWrapper.
     * @param holder a ResultSetHolder containing the ResultSet and possibly a Statement
     */
    ResultSetWrapper( ResultSetHolder holder )
    {
        m_holder = holder;
    }
    
    /**
     * Retrieves the actual ResultSet instance being wrapped.
     * @return the ResultSet instance
     */
    public ResultSet getResultSet()
    {
        return m_holder.getResultSet();
    }
    
    /**
     * Closes the ResultSetWrapper. Closing the wrapper removes it from the tracking
     * list and closes any underlying ResultSets and Statements.
     */
    public void close()
    {
        ResultSetHolder holder = null;
        synchronized ( this )
        {
            ResultSetTracker.untrackResultSet( this );
            holder = m_holder;
            m_holder = null;
        }
        if (holder != null)
        	holder.close();
    }
    
    /**
     * Retrieves the ResultSetHolder containing the actual ResultSet and Statement instances.
     * @return the ResultSetHolder
     */
    ResultSetHolder getHolder()
    {
        return m_holder;
    }
    
    /**
     * Creates a new ResultSetWrapper. The wrapper will be added to the tracking list.
     * A Statement can optionally be provided; closing the wrapper will only close the statement
     * associated with the ResultSet if the statement was provided at creation time. This allows
     * multiple ResultSetWrapperss to be used with the same statement if required.
     * @param rs the ResultSet instance
     * @param stmt the optional Statement
     * @return the new ResultSetWrapper
     */
    public static ResultSetWrapper createWrapper( ResultSet rs, Statement stmt )
    {
        assert rs != null;
        
        ResultSetHolder holder = new ResultSetHolder( rs, stmt );
        ResultSetWrapper wrapper = new ResultSetWrapper( holder );
        
        ResultSetTracker.trackResultSet( wrapper );
        
        return wrapper;
    }
}
