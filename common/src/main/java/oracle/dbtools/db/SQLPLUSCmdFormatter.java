/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.io.IOException;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Struct;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.SwingUtilities;

import oracle.dbtools.app.SqlRecognizer;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.WrapListenBufferOutputStream;
import oracle.dbtools.raptor.newscriptrunner.commands.BottomTitle;
import oracle.dbtools.raptor.newscriptrunner.commands.Break;
import oracle.dbtools.raptor.newscriptrunner.commands.Compute;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.commands.SetPause;
import oracle.dbtools.raptor.newscriptrunner.commands.TopTitle;
import oracle.dbtools.raptor.nls.FormatType;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.dbtools.raptor.nls.OracleNLSProvider;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.jdbc.OracleDatabaseMetaData;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.oracore.OracleTypeADT;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.BINARY_DOUBLE;
import oracle.sql.BINARY_FLOAT;
import oracle.sql.CHAR;
import oracle.sql.DATE;
import oracle.sql.Datum;
import oracle.sql.INTERVALDS;
import oracle.sql.INTERVALYM;
import oracle.sql.NUMBER;
import oracle.sql.RAW;
import oracle.sql.REF;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import oracle.sql.TIMESTAMP;
import oracle.sql.TIMESTAMPLTZ;
import oracle.sql.TIMESTAMPTZ;


/**
 * This is a formatter and emulates some sqlplus commands mainly the column
 * formatting.
 * 
 * @author Ramprasad Thummala
 */
public class SQLPLUSCmdFormatter extends ResultSetFormatter {
	private HashMap<Integer, Integer> m_nls_formats = new HashMap<Integer, Integer>();
	private static final String m_lineSeparator = System
			.getProperty("line.separator");
	private int m_linesize = -1;
	private int m_pagesize = -1;
	private int m_newpage = 0;
	private String m_scan = "";
	private String m_colsep = "";
	private String m_headsep = "";
	private String m_headsepchar = "";
	private String m_heading = "";
	private String m_wrap = "";
	private int m_numwidth = -1;
	private String m_numformat = "";
	private int m_long = -1;
	private int m_longchunk = -1;
	private String m_null = "";
	private TopTitle m_TopTitle = null;
	private BottomTitle m_BottomTitle = null;
	private boolean m_embedded = false;
	private StringBuffer m_header = new StringBuffer();
	private StringBuffer m_headerLine = new StringBuffer();
	private String[] m_columns = null;
	private String[] m_formattedcols = null;
	private Integer[] m_colsizes = null;
	private int m_delim_lnsize = -1;
	private StringBuffer m_empty_page = null;
	private boolean m_foldedrows = false;
	private String m_sql = "";
	private Connection m_conn = null;
	private static final String DONE = "<<DONE>>";

	// START: Performance Variables for BREAK COMMAND
	private ArrayList<String> m_breakcolumns = new ArrayList<String>();
	private HashMap<String, String> m_column_to_brkcol = new HashMap<String, String>();
	private HashMap<String, Boolean> m_brkcol_to_nodup = new HashMap<String, Boolean>();
	private HashMap<String, Integer> m_brkcol_to_rank = new HashMap<String, Integer>();
	private boolean m_breakandselorderMatch = true;
	HashMap<Integer, Integer> m_breakcolToSkipLinesForGrp = new HashMap<Integer, Integer>();
	// END: Performance Variables for BREAK COMMAND

	// START: Performance Variables for BREAK AND COMPUTE COMMAND 
	// m_brkcmptcolumns is for Break Columns that are also part of the Compute command
	private ArrayList<String> m_brkcmptcolumns = null; 
	private ArrayList<String> m_computecolumns = null;
	private HashMap<String, String> m_column_to_brkcmptcol = new HashMap<String, String>();
	private HashMap<String, String> m_brkcol_to_cmplbl = new HashMap<String, String>();
	private HashMap<String, String> m_brkcol_to_cmpfn = new HashMap<String, String>();
	private HashMap<String, HashMap> m_brkcol_to_cmpfnlbl = new HashMap<String, HashMap>();
	//	private HashMap<String, String> m_column_to_cmpcol = new HashMap<String, String>();
	private TreeMap<String, String> m_column_to_cmpcol = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
	private HashMap<String, String> m_brkcmpkey_to_col = new HashMap<String, String>();
	private HashMap<String, String[]> m_brkcmpkey_to_cols = new HashMap<String, String[]>();
	private Object[] m_summaryLines = null;
	private HashMap<String, Object[]> m_column_to_summaryLines = new HashMap<String, Object[]>();
	private ArrayList<String> m_selTableList = new ArrayList<String>();
	private ArrayList<String> m_selColumnList = new ArrayList<String>();
	private ArrayList<String> m_selPredicateList = new ArrayList<String>();
	private ArrayList<String> m_selGroupByList = new ArrayList<String>();
	private ArrayList<String> m_selOrderByList = new ArrayList<String>();
	private TreeMap<String, String> m_column_to_alias = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
	// END: Performance variables for BREAK AND COMPUTE COMMAND
	private HashMap<String, Integer> m_dtformat_to_colsize = new HashMap<String, Integer>();
	private ArrayList<String> m_121IdentViewList = new ArrayList<String>();
	private String m_cursorHeader = "";
	

	public SQLPLUSCmdFormatter() {	
		super(ScriptRunnerContext.getCurrentContext()!=null?ScriptRunnerContext.getCurrentContext():new ScriptRunnerContext());
	}

	public SQLPLUSCmdFormatter(final ScriptRunnerContext inScriptRunnerContext) {
		super(inScriptRunnerContext);
	}

	/**
	 * Allow error stringbuffer to be passed in to show error if real
	 * stringuffer is ignored.
	 */
	@Override
	public int rset2sqlplus(ResultSet rset, Connection conn, Object out,
			StringBuffer errbuf) throws IOException, SQLException {
		int cnt = 0;
		m_conn = conn;
		ResultSetMetaData rmeta = rset.getMetaData();// VersionInfo
		int colCnt = rmeta.getColumnCount();
		Integer[] colsizes = new Integer[colCnt + 1];
		// Bug 19723119
		// This is used for col "new_value" option and should not have 
		// formatted values
		String[] columns = new String[colCnt + 1];
		String[] coldataTypes = new String[colCnt + 1];

		m_formattedcols = new String[colCnt + 1];
		// This will have column values that are formatted
		m_columns = new String[colCnt + 1]; 
		String[] lastVal = null;
		String[] oldlVal = null;
		boolean newVal=(this.m_scriptRunnerContext!=null)&&
				(this.m_scriptRunnerContext.getColumnMap()!=null)&&
				(this.m_scriptRunnerContext.getColumnMap().size()>0);

		boolean oldVal=(this.m_scriptRunnerContext!=null)&&
				(this.m_scriptRunnerContext.getOVColumnMap()!=null)&&
				(this.m_scriptRunnerContext.getOVColumnMap().size()>0);

		StringBuffer header_rows = new StringBuffer("");
		// StringBuffer header = new StringBuffer();
		// StringBuffer headerLine = new StringBuffer();
		StringBuffer tot_header = new StringBuffer();
		StringBuffer tTitle = null;
		StringBuffer btmTitle = null;
		boolean bComputed = false;
		int cursorCnt = 0;
		String cursorData = "";

		HashMap<String, ArrayList<String>> storedColsCmds = m_scriptRunnerContext
				.getStoredFormatCmds();
		// TreeMap<String, ArrayList<String>> storedBrkCmds =
		// m_scriptRunnerContext.getStoredBreakCmds();
		// CaseInsensitiveLinkedHashMap storedBrkCmds =
		// m_scriptRunnerContext.getStoredBreakCmds();
		LinkedHashMap<String, ArrayList<String>> storedBrkCmds = m_scriptRunnerContext
				.getStoredBreakCmds();
		m_pagesize = ((Integer) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETPAGESIZE)).intValue();
		m_linesize = ((Integer) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETLINESIZE)).intValue();
		m_scan = ((String) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETSCAN));
		m_colsep = ((String) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETCOLSEP));
		m_headsep = ((String) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETHEADSEP));
		m_headsepchar = ((String) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETHEADSEPCHAR));
		m_heading = ((String) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETHEADING));
		m_wrap = ((String) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETWRAP));
		m_long = ((Integer) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETLONG)).intValue();
		m_longchunk = ((Integer) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETLONGCHUNKSIZE)).intValue();
		m_numwidth = ((Integer) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETNUMWIDTH)).intValue();
		m_numformat = ((String) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETNUMFORMAT));
		m_null = ((String) m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SETNULL));
		m_TopTitle = (TopTitle) m_scriptRunnerContext.getTTitle();
		m_BottomTitle = (BottomTitle) m_scriptRunnerContext.getBTitle();
		m_delim_lnsize = m_linesize + m_lineSeparator.length();
		// Bug 25060009
		if(m_scriptRunnerContext.getProperty(ScriptRunnerContext.NEWPAGE) instanceof String && 
			((String)m_scriptRunnerContext.getProperty(ScriptRunnerContext.NEWPAGE)).equalsIgnoreCase("NONE")) {
				m_newpage = 0;
		}
		else {
				if(m_pagesize > 0)
					m_newpage = ((Integer) m_scriptRunnerContext.getProperty(ScriptRunnerContext.NEWPAGE)).intValue();
		}
		m_embedded = Boolean.parseBoolean(m_scriptRunnerContext
				.getProperty(ScriptRunnerContext.SQLPLUS_EMBEDDED_MODE).toString());
		Integer linenum = 0;
		Integer page = 0;
		
		// Bug 20301126
		// HashMap<Integer, ArrayDeque<String[]>> brkColValues = new
		// HashMap<Integer, ArrayDeque<String[]>>();
		ArrayDeque<String[]> brkColValues = new ArrayDeque<String[]>();

		// Bug 19723119 moved this code outside.
		Map<String, String> cMap = new HashMap<String, String>();
		Map<String, String> ovcMap = new HashMap<String, String>();
		if (m_scriptRunnerContext != null) {
			cMap = m_scriptRunnerContext.getColumnMap();
			ovcMap = m_scriptRunnerContext.getOVColumnMap();
		}
		if (cMap.size() != 0 || ovcMap.size() != 0) {
			// if NO rows and col settings return - fill in cols
			// assumption : columns is best as straight getColumnName
			// without formatting for col feature
			for (int i = 1; i < colCnt + 1; i++) {
				columns[i] = rmeta.getColumnName(i);
				coldataTypes[i] = rmeta.getColumnTypeName(i);
			}
		}

		//if (!rset.isBeforeFirst()) {
		//	m_scriptRunnerContext.updateColumn(columns, null);
		//	return cnt;
		//}
		
		// Initiate the Date/Time Format masks to column sizes map.
		populateDTFmtsToColsizes();
		ArrayList<Object> firstRow = new ArrayList<Object>();
		if (rset.next()) {
			for (int i = 1; i < colCnt + 1; i++) {
				if (rset instanceof OracleResultSet) {
					try {
						// Bug 23297745 
						// Workaround for LONG & LONG RAW Types where we run into "Stream has already been closed"
						if (rmeta.getColumnType(i) == -1 /* OracleTypes.LONGVARCHAR */ || 
							rmeta.getColumnType(i) == -2 /* OracleTypes.RAW */ ) {
							String data = DataTypesUtil.stringValue(ScriptUtils.getOracleObjectWrap((OracleResultSet) rset,i), conn);
							firstRow.add(data);
						}
						else
						  firstRow.add(ScriptUtils.getOracleObjectWrap((OracleResultSet) rset,i));
					} catch (SQLException e) {
						firstRow.add(rset.getObject(i));
					}
				} else {
					firstRow.add(rset.getObject(i));
				}
			}
		} else {
			m_scriptRunnerContext.updateColumn(columns, null, null);
			m_scriptRunnerContext.updateOVColumn(columns, null, null);
			return cnt;
		}
		
		
		
		
		int linediff = m_linesize;
		
		// LRG/SRG Regression Test checks doesn't apply to Production code.
		if(m_scriptRunnerContext.is121LongIdentUsed()) {
			storedColsCmds = m_scriptRunnerContext.get121IdentViewColCmds();
		}
		
		boolean b_ml_header = hasMultiLineHeaders(storedColsCmds, rset);
		
		for (int i = 1; i < colCnt + 1; i++) {
			String heading = "";
//			String colName = rmeta.getColumnName(i).toLowerCase();
			String colName = getColumnName(rset, i).toLowerCase();
			// Bug 20389766 commented out because break column may not be
			// printed but
			// its actions like skip lines should work.
			// if(printColumn(storedColsCmds, colName)) {
			int colType = rmeta.getColumnType(i);
			colsizes[i] = getColumnSize(conn, rset, i, storedColsCmds);
			// This is specific to LRG/SRG tests.
			HashMap<String, ArrayList<String>> prodStoredcolsCmds = m_scriptRunnerContext.getStoredFormatCmds();
			if (prodStoredcolsCmds != storedColsCmds) {
				heading = getHeading(colName, prodStoredcolsCmds);
			}
			else {
			    heading = getHeading(colName, storedColsCmds);
			}
			// Bug 19723119 using m_columns instead of columns 
			// because the values in columns variable are not column formatted.
			// columns variable is needed for new_value.
//			m_columns[i] = rmeta.getColumnName(i).length() > colsizes[i] ? rmeta
//					.getColumnName(i).substring(0, colsizes[i]) : rmeta
//					.getColumnName(i);
			String getColumnNameCache=getColumnName(rset, i);
			m_columns[i] = getColumnNameCache.length() > colsizes[i] ? 
				       getColumnNameCache.substring(0, colsizes[i]) : 
				       getColumnNameCache;
					// Bug 21106191		
					if(m_wrap.equalsIgnoreCase("OFF")) {
						if(i == colCnt) {
							linediff = linediff - colsizes[i];
							if(linediff < 0)
								colsizes[i] = colsizes[i] + linediff;  
						}
						else {
							linediff = linediff - (colsizes[i] + m_colsep.length());
							if(linediff < 0)
								colsizes[i] = colsizes[i] + linediff + m_colsep.length(); 
						}
					}    

					if (heading.equals("")) {
						// Bug 23095629
						if(storedColsCmds.get(colName) != null && storedColsCmds.get(colName).contains("heading ")) { //$NON-NLS-1$
							m_columns[i] = heading;
						}
						else {
							String colNameCache=getColumnName(rset, i);
							m_columns[i] = colNameCache.length() > colsizes[i] ? 
								       colNameCache.substring(0, colsizes[i]) : 
								       colNameCache;
									colsizes[i] = colsizes[i];
						}
					}
					
					// LRGSRG: Long identifier test fix. Identifier length can be longer than linesize. Use linesize 
					// as column size.
				        m_columns[i] = m_columns[i].length() > m_linesize ? m_columns[i].substring(0, m_linesize) : m_columns[i];

					if (b_ml_header) {
						heading = heading.equals("") ? m_columns[i] : heading;
						if (displayColumnAttributes(storedColsCmds, colName) && storedColsCmds.get(colName.toLowerCase()) != null)
							convertToMultiLineHeader(colsizes[i], i, heading, m_columns);
					} else {
						// Bug 12667530
						if (!heading.equals("")) {
							m_columns[i] = heading.length() > colsizes[i] ? heading
									.substring(0, colsizes[i]) : heading;
						}
					}

					if (printColumn(storedColsCmds, colName)) {
					        // LRGSRG: Long identifier test fix. Identifier length can be longer than linesize. Use linesize 
						// as column size.
					        int colsize = colsizes[i] > m_linesize ? m_linesize : colsizes[i];
						// Bug 19784536 additional check for number type
						// and for Bug 19784418
						if (isNumericColumn(colType)) {
							ArrayList<String> colcmds = storedColsCmds.get(colName.toLowerCase());
							if (colcmds != null && displayColumnAttributes(storedColsCmds, colName.toLowerCase()) && 
									(colcmds.contains("justify left") || colcmds.contains("justify right") || colcmds.contains("justify center"))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
								m_formattedcols[i] = justifyHeaders(storedColsCmds, colName.toLowerCase(), m_columns[i], colsize);
								m_header.append(m_formattedcols[i]);
							}
							else {
								m_formattedcols[i] = justify_right(m_columns[i], colsize);
								m_header.append(m_formattedcols[i]);
							}
						}
						else {
							if (isLongColumn(colType) && colsizes[i] >= m_linesize) {
								m_formattedcols[i] = justifyHeaders(storedColsCmds, colName.toLowerCase(), m_columns[i], m_linesize);
								m_header.append(m_formattedcols[i]);
								getFontMetrics();
								font_width_adjustment(m_header, m_linesize);
							}
							else {
								m_formattedcols[i] = justifyHeaders(storedColsCmds, colName.toLowerCase(), m_columns[i], colsize);
								m_header.append(m_formattedcols[i]);
							}
						}
					}

					char[] chars = new char[colsizes[i]];
					if (colsizes[i] >= m_linesize) {
						chars = new char[m_linesize];
						Arrays.fill(chars, '-');
					} else {
						Arrays.fill(chars, '-');
					}
					if (printColumn(storedColsCmds, colName)) {
						m_headerLine.append(chars);
						if (isLongColumn(colType) && colsizes[i] >= m_linesize) {
							getFontMetrics();
							font_width_adjustment(m_headerLine,
									m_linesize);
						}

						if (!b_ml_header) {
							if (i <= colCnt && linediff > 0) {
								m_header.append(m_colsep);
								m_headerLine.append(m_colsep);
							}
						} 
						else {
							if (i <= colCnt && linediff > 0) {
								m_headerLine.append(m_colsep);
							}
						}
					}
					// }
                    // Bug 25110219 if there is no room to fit the column we 
					// just break out here.
					if(m_wrap.equalsIgnoreCase("OFF") && linediff <= 0) {
						break;
					}

		}

		if (b_ml_header) {
			getFontMetrics();
			m_header = multilineHeader(rset, storedColsCmds, colsizes,
					m_columns, m_columns, true);
		} else {
			if (m_header.length() > 0) {
				if (m_colsep.equals(m_header.substring(m_header.length() - 1)) && linediff > 0)
					m_header.deleteCharAt(m_header.length() - 1);
			}
		}
		header_rows.append(m_header);
		header_rows.append(m_lineSeparator);
		if (m_headerLine.length() > 0) {
			if (m_colsep
					.equals(m_headerLine.substring(m_headerLine.length() - 1)) && linediff > 0) {
				m_headerLine.deleteCharAt(m_headerLine.length() - 1);
			}
		}
		header_rows.append(m_headerLine);
		header_rows.append(m_lineSeparator);

		boolean bPrintTitle = false;

		StringBuffer bufpage = new StringBuffer("");
		StringBuffer databuf = new StringBuffer("");
		SetPause dataBufHelper = null;
		Boolean isPause=(Boolean) m_scriptRunnerContext.getProperty(ScriptRunnerContext.SET_PAUSE);
		if ((isPause!=null)&&(isPause.equals(Boolean.TRUE))) {
			dataBufHelper=new SetPause();
		}
		StringBuffer tTitle_header = null;
		int pageNo = 1, lineNo = 0, bTitle_lines = 0, offSet = 0;
		boolean newPage = true;
		int oldPageNo = -1;
		
		// Bug 19497212 - set embedded off not supported
		// SET EMBED ON functionality starts from here.
		if(m_embedded && (int) m_scriptRunnerContext.getProperty(ScriptRunnerContext.LNO) >= 0) {
			lineNo = (int)m_scriptRunnerContext.getProperty(ScriptRunnerContext.LNO);
			pageNo = (int)m_scriptRunnerContext.getProperty(ScriptRunnerContext.PNO) == 0 ? pageNo : (int)m_scriptRunnerContext.getProperty(ScriptRunnerContext.PNO);
			lineNo = lineNo + m_pagesize * (pageNo - 1);
			if(lineNo == 0 || lineNo >= pageNo * m_pagesize) {
				if(lineNo > pageNo * m_pagesize) {
					offSet = lineNo - (pageNo * m_pagesize);
					lineNo = lineNo - offSet;
				}	
				offSet = 0;
				if(lineNo > 0)
				    pageNo++;
				for(int np = 0; np < m_newpage; np++) {
					databuf.append(m_lineSeparator);
					lineNo++;
				}
			}
			else {
			   m_newpage = 0;
			}
		}

		// Bug 21106191
		// look for column sizes that are nulls and remove those from the array.
		ArrayList<Integer> colsizesList = new ArrayList<Integer>();
		ArrayList<String>  columnsList = new ArrayList<String>();
		if(m_wrap.equalsIgnoreCase("OFF")) {
			for(int i = 0; i < colsizes.length; i++) {
				if(i == 0) {
					colsizesList.add(colsizes[i]); // null allowed here.
				}

				if(i > 0 && colsizes[i] != null) {
					colsizesList.add(colsizes[i]);
				}
			}
			colsizes = new Integer[colsizesList.size()];
			colsizesList.toArray(colsizes);


			for(int i = 0; i < m_columns.length; i++) {
				if(i == 0) {
					columnsList.add(m_columns[i]); // null allowed here.
				}

				if(i > 0 && m_columns[i] != null) {
					columnsList.add(m_columns[i]);
				}
			}

			if(columnsList.size() <  m_columns.length) {
				boolean rowstrunc = false;
				for(int i = 1; i < m_columns.length; i++) {
					if(i < columnsList.size()) {
						if(columnsList.get(i).equals(m_columns[i])) {
							continue;
						}
					}
					else {
//						String message = MessageFormat.format(ScriptRunnerDbArb.getString("ROWSTRUNCATED"), new Object[] {rmeta.getColumnName(i)}) + m_lineSeparator;
					        String message = MessageFormat.format(ScriptRunnerDbArb.getString("ROWSTRUNCATED"), new Object[] {getColumnName(rset, i)}) + m_lineSeparator;
						write(out, message);
						rowstrunc = true;
					}
				}

				if(rowstrunc) 
					write(out, m_lineSeparator);

				m_columns = new String[columnsList.size()];
				columnsList.toArray(m_columns);
				// Bug 25110219
				colCnt = m_columns.length;
			}
		}

		m_colsizes = colsizes;
		int titleLines = 0;
		int adjPagesize = 0;

		if (m_heading.equalsIgnoreCase("ON")) { //$NON-NLS-1$
			if ((m_pagesize < 0 && m_linesize < 0)) {
				write(out, header_rows.toString());
			} else {
				if (m_linesize == 0) {
					String msg = ScriptRunnerDbArb
							.getString(ScriptRunnerDbArb.ZERO_LNSIZE);
					write(out, msg + m_lineSeparator + m_lineSeparator);
					return cnt;
				}
				titleLines = getTotTitleLines();
				StringBuffer colsHeader = new StringBuffer("");
				int tot_hdr_lines = 0;
				if (b_ml_header) {
					// just create a random length multiline buffer for now.
					adjustHeaderToLineandPageSize(rset, storedColsCmds,
							m_colsizes, m_columns, tot_header, 0);
					if (tot_header.length() == 0) {
						tot_header = header_rows;
					}
				} else {
					if (isAValidLinesize())
						adjustToLineandPageSize(rset, storedColsCmds,
								m_colsizes, m_header, m_headerLine, tot_header,
								0);
				}

				adjPagesize = getAdjustedPagesize(tot_header, titleLines);

				if (m_pagesize >= 0 && !isAValidLinesize()) {
					// We try to set the line size to be reasonable so we can
					// create a report.
					// This also takes into account the Apex install scripts
					// that have
					// line sizes which are < 10
					int linesize = 0;
					if (m_pagesize > 2) {
						if (m_scriptRunnerContext.getBTitleFlag()
								|| m_scriptRunnerContext.getTTitleFlag()) {
							if (m_pagesize > 4)
								tot_header = header_rows;
						} else {
							if (m_pagesize > 2)
								tot_header = header_rows;
						}
					}
					for (int i = 1; i < colsizes.length; i++) {
						// String colName = m_columns[i];
						// if(!printColumn(storedColsCmds, columns[i]))
						// continue;
						if (i < m_colsizes.length - 1)
							linesize += m_colsizes[i] + m_colsep.length();
						else
							linesize += m_colsizes[i];
					}


					// Bug 12929347
					// recalculate the colsizes if the ratio of linesize to
					// m_linesize is 10:1

					if (m_linesize > 0 && linesize / m_linesize <= 10) {
						for (int i = 1; i < colsizes.length; i++) {
							// Bug 19498547 - SELECT OF TABLE WITH A COLUMN OF XMLTYPE, WILL SHOW VERY LONG BLANK RESULTS 
							// ignoring Colsize for Long data types
							int colType = rmeta.getColumnType(i);
							if(!isLongColumn(colType)) {
								if (m_linesize < colsizes[i]) {
									if (i < m_colsizes.length - 1)
										colsizes[i] = m_linesize
										+ m_colsep.length();
									else
										colsizes[i] = m_linesize;
								}
							}
						}
						m_delim_lnsize = m_linesize + m_lineSeparator.length();
						StringBuffer temp_header = new StringBuffer("");
						String[] strs = tot_header.toString().split(
								m_lineSeparator);
						for (int i = 0; i < strs.length; i++) {
							if (strs[i].length() > 0) {
								temp_header.append(strs[i].substring(0,
										m_linesize));
								temp_header.append(m_lineSeparator);
							}
						}
						tot_header = temp_header;
					}
					// Bug 13416751
					if (linesize > m_linesize)
						m_delim_lnsize = linesize + m_lineSeparator.length();

				}
			}
		}
		// Bug 20753653
		else {
			adjPagesize = m_pagesize;
		}

		// Bug 13062958 fix.
		//		tTitle_header = getTopTitleAndHeader(pageNo, tot_header);
		if (dataBufHelper!=null) {
			dataBufHelper.mark(m_scriptRunnerContext,out,databuf);
		}
		//		databuf.append(tTitle_header);
		//		lineNo = numberofLines(tTitle_header);
		//		bTitle_lines = numberofLines(getBottomTitle(pageNo));

		ArrayList<StringBuffer> cacheRow = new ArrayList<StringBuffer>();
		for (int i = 0; i < colCnt + 1; i++) {
			cacheRow.add(new StringBuffer());
		}
		ArrayList<StringBuffer> cpcacheRow = new ArrayList<StringBuffer>();
		ArrayList<StringBuffer> cpttlcacheRow = new ArrayList<StringBuffer>(); // for Title command.

		int beginIdx = 0, endIdx = 0, startIdx = 0, idx = 0;
		String lastStr = "";
		HashMap<Integer, String> prevColToVal = new HashMap<Integer, String>();
		ArrayList<Object[]> cmptd_rows = new ArrayList<Object[]>();
		ArrayList<Object> cmptd_columns = new ArrayList<Object>();
		HashMap<String, Object[][]> comp_rows = new HashMap<String, Object[][]>(1000);
		int grpRowCount = 0; // for keeping track of rows especially when there
		// are computed rows.
		int grpFirstCmptdCount = 0;
		ArrayList<Object[]> cmptd_rowset = null;
		//		HashMap<Integer, Integer> breakcolToSkipLinesForGrp = new HashMap<Integer, Integer>();
		ArrayList<Integer> rowIndexes = new ArrayList<Integer>();
		//		String breakcolcmptkeys = "";
		String[] breakcolcmptkeys = null;
		Object[][] cmptd_rowset_arr = null;
		int missingSkipLines = 0;
		int prevLevelRows = 0;
		int skipLinesPerColumn = 0;
		String missingLineTerminators = "";
		HashMap<String, Integer> bckeyprevLevelcounts = new HashMap<String, Integer>();
		// LRG/SRG fix for Multiline Rows. 
		boolean bAppendedMultiRowLnTerm = true;
		
		populateBreakCmdInfo();

        while (cnt < s_maxRows || continueIfPossible(cnt, s_maxRows, m_scriptRunnerContext, out, rset, errbuf, null)) {
			// check to see if the thread has paused
			checkCanProceed();
			boolean brsNext = false;
			boolean bCmptdReport = false;
			/** no cache */
			if ((cnt == 0) && (firstRow.size() == 0)) {
				break;
			}
			if (cnt != 0) {
				if (grpFirstCmptdCount > 0 && grpFirstCmptdCount == grpRowCount) {
					// Bugs 22538861 & 22571553
					if(isEmptyBreakColKeys(breakcolcmptkeys)) {
						grpRowCount = 0;
					}
					else if(cmptd_rowset_arr == null && breakcolcmptkeys.length >= 1) {
						if(isAllRowCountsEqual(comp_rows, cmptd_columns, breakcolcmptkeys)) {
							cmptd_rowset_arr = getComputedRowsForGroup(comp_rows, cmptd_columns, rset, breakcolcmptkeys);
							prevLevelRows = 0;
						}
						else if(prevLevelRows == getFirstCmptdRowCount(comp_rows, cmptd_columns, breakcolcmptkeys[breakcolcmptkeys.length-1])) {
							cmptd_rowset_arr = getComputedRowsForGroup(comp_rows, cmptd_columns, rset, breakcolcmptkeys);
							prevLevelRows = 0;
						}
						else {
							String[] rbreakcolcmptkeys = breakcolcmptkeys; 
							if (rbreakcolcmptkeys.length > 1) {
								for (int x = rbreakcolcmptkeys.length - 1; x > 0;  x--) {
									rbreakcolcmptkeys = (String[]) removeRow(rbreakcolcmptkeys, x);
								}
							}
							cmptd_rowset_arr = getComputedRowsForGroup(comp_rows, cmptd_columns, rset, rbreakcolcmptkeys);
						}
						
//						int linesLimit = (adjPagesize * pageNo) - (bTitle_lines + m_newpage);
//						while(skipLinesPerColumn > 0 && lineNo < linesLimit) {
//							skipLinesPerColumn--;
//							databuf.append(m_lineSeparator);
//							lineNo++;
//						}
//						// Bug 20443163
//						if(skipLinesPerColumn > 0) {
//							missingSkipLines += skipLinesPerColumn;
//							// we need to skip lines in the new page.
//						}

						
					}
				}
				/** no second row */
				else if (!(brsNext = (cachedNext(m_scriptRunnerContext)||rset.next())) && comp_rows.size() == 0 && cmptd_rowset_arr == null) {
					// Bug 20384066
					int skipLines = skipLinesForReport(storedBrkCmds);
					// Bug 20485946 skip Lines for the last group of the Report
					// here.
					Iterator<Map.Entry<Integer, Integer>> keys = m_breakcolToSkipLinesForGrp
							.entrySet().iterator();
					while (keys.hasNext()) {
						Map.Entry<Integer, Integer> key = keys.next();
						skipLines += key.getValue();
					}

					for (int k = 0; k < skipLines; k++) {
						if (lineNo < ((adjPagesize * pageNo) - bTitle_lines) - 1) {
							databuf.append(m_lineSeparator);
							lineNo++;
							write(out, databuf.toString());
							databuf.setLength(0);
						} else {
							break;
						}
					}

					break; // main while loop.
				}

				// Bug 21511877	we are left with Report computation that needs to 
				// be printed out at the end.
				if(grpRowCount == 0 && comp_rows.size() > 0 && cmptd_columns.size() > 1) {
					if(brsNext) {
						ArrayList<String> fList = filterList(m_brkcmptcolumns, "report");
						String[] prevbreakcolcmptkeys = breakcolcmptkeys;
						breakcolcmptkeys = generateBrkComputeColKeys(comp_rows, cmptd_columns, rset, "");
						boolean emptyBrkKeys = isEmptyBreakColKeys(breakcolcmptkeys);
						if(emptyBrkKeys) {
							prevLevelRows = 0;
						}
						else {
							if(breakcolcmptkeys.length > 0 && prevLevelRows > 0 && 
							   prevbreakcolcmptkeys[prevbreakcolcmptkeys.length - 1] != null && breakcolcmptkeys[breakcolcmptkeys.length - 1] != null && 		
							   !prevbreakcolcmptkeys[prevbreakcolcmptkeys.length - 1].equals(breakcolcmptkeys[breakcolcmptkeys.length - 1])) {
								// Bugs 22538861 & 22571553
								// store the prevLevelRows count to use it later.
								// If ORDER BY clause is not used we might run into this issue.
								bckeyprevLevelcounts.put(prevbreakcolcmptkeys[prevbreakcolcmptkeys.length - 1], prevLevelRows);
								prevLevelRows = 0;
							}
							grpFirstCmptdCount = getFirstCmptdRowCount(comp_rows, cmptd_columns, breakcolcmptkeys[0]);
							Integer prevval = bckeyprevLevelcounts.get(breakcolcmptkeys[breakcolcmptkeys.length - 1]);
							if(prevval != null) {
								prevLevelRows = prevval.intValue();
								bckeyprevLevelcounts.remove(breakcolcmptkeys.length - 1);
							}
							prevLevelRows += grpFirstCmptdCount;
						}
					}
					else {
						if(cmptd_rowset_arr == null && !isEmptyBreakColKeys(breakcolcmptkeys)) {
							cmptd_rowset_arr = getComputedRowsForGroup(comp_rows, cmptd_columns, rset, breakcolcmptkeys);
							bCmptdReport = true;
						}
					}
				}

			}

			if (cnt == 0 && firstRow.size() > 0) {
				brsNext = true;
			}

			if (brsNext)
				cnt++;

			if (cnt > 0 && !bComputed) {
				// We do the computations only ONCE when we start
				// to see a record in the Result set.
				computedData(comp_rows, cmptd_columns);
				bComputed = true;
				if (comp_rows.size() > 0 && cmptd_columns.size() > 0) {
					ArrayList<String> fList = filterList(m_brkcmptcolumns, "report");
					// Bugs 22538861 & 22571553
					if(fList.size() == 0 && m_brkcmptcolumns.contains("report")) {
						breakcolcmptkeys = generateBrkComputeColKeys(comp_rows, cmptd_columns, rset, "report");
					}
					else {
						breakcolcmptkeys = generateBrkComputeColKeys(comp_rows, cmptd_columns, rset, "");
					}
					grpFirstCmptdCount = getFirstCmptdRowCount(comp_rows, cmptd_columns, breakcolcmptkeys[0]);
					prevLevelRows += grpFirstCmptdCount;
				}
			}

			if (grpFirstCmptdCount > 0 && brsNext) {
				// we need to keep track of the row counts here.
				grpRowCount++;
			}

			int cmptdSize = !brsNext && cmptd_rowset != null ? cmptd_rowset
					.size() : 0;
					int ck = 0;
					boolean bCmptdData = false;
					
			// This is specific to LRG/SRG to apply possible column formats (that were 
			// defined in scripts) on data. Doesn't apply to Production code.
			HashMap<String, ArrayList<String>> prodStoredcolsCmds = m_scriptRunnerContext.getStoredFormatCmds();
			if (prodStoredcolsCmds != storedColsCmds) {
				storedColsCmds = prodStoredcolsCmds;
			}
					// Do cnt=1 with cached row and from then on with rset.next() row
					for (int i = 1; i < colCnt + 1; i++) {
						// Bug 25110219 especially when truncate is on and linesize is limited.
						if(i >= colsizes.length) break;
						Object o = null;
//						String colName = rmeta.getColumnName(i).toLowerCase();
						String colName = getColumnName(rset, i).toLowerCase();
						// if ( false /*5592534: rset instanceof OracleResultSet*/ ){
						// Bug No:5690176 - need getOracleObject where possible.
						if (cnt == 1 && brsNext) {
							o = firstRow.get(i - 1);
						} else {
							if (brsNext) {
								if (rset instanceof OracleResultSet) {
									try {
										// Bug 23297745 
										// Workaround for LONG Types where we run into "Stream has already been closed"
										if(rmeta.getColumnType(i) == -1 /* OracleTypes.LONGVARCHAR */ || 
										   rmeta.getColumnType(i) ==	-2 /* OracleTypes.RAW */) 
											o = DataTypesUtil.stringValue(ScriptUtils.getOracleObjectWrap((OracleResultSet) rset,i), conn);
										else
										    o = ScriptUtils.getOracleObjectWrap((OracleResultSet) rset,i);
									} catch (SQLException e) {
										o = rset.getObject(i);
									}
								} else {
									o = rset.getObject(i);
								}
								bCmptdData = false;
							} 
							else if(cmptd_rowset_arr != null && cmptd_rowset_arr.length > 0) {
								// Bug 25110219 especially when truncate is on and linesize is limited.
								if(i < colsizes.length) {
									o = cmptd_rowset_arr[0][i];
								}
								bCmptdData = true;
								if (i == colCnt || i == colsizes.length - 1) {
									cmptd_rowset_arr = removeRow(cmptd_rowset_arr, 0);
									if(cmptd_rowset_arr == null) {
										grpRowCount = 0;
									}
								}
							}
						}
						if (brsNext && (this.m_scriptRunnerContext != null)
								&& (o == null)) {
							String setNull = (String) this.m_scriptRunnerContext
									.getProperty(ScriptRunnerContext.SETNULL);
							// Bug 21477780 If COLUMN command NULL clause is set, 
							// this will always override the SET NULL command.
							ArrayList<StringBuffer> buf = new ArrayList<StringBuffer>();
							StringBuffer sbuf = new StringBuffer();
							for (int ii = 0; ii < colCnt + 1; ii++) {
								buf.add(new StringBuffer());
							}
							sbuf.append("");
							buf.add(i, sbuf);
							StringBuffer data = checkFormatOptions(rset, i, storedColsCmds, colsizes, buf);
							if(data.toString().length() > 0) {
								o = data.toString();
							}
							else if ((setNull != null) && (!(setNull.equals("")))) { //$NON-NLS-1$
								o = setNull;
							}
						}
						String s = "";
						//If stored cols cmds has no format directives skip the storedColsCmds 
						boolean colFormatAvailable=false;
						//duplicate code first if - to find if there is a format - second if to really go there
						if (storedColsCmds.containsKey(colName) && !colFormatAvailable
								&& (o instanceof oracle.sql.NUMBER
										|| o instanceof oracle.sql.BINARY_DOUBLE || o instanceof oracle.sql.BINARY_FLOAT)) {
							ArrayList<String> ls = storedColsCmds.get(colName);
							for (int k = 0; k < ls.size(); k++) {
								if (k == 0
										&& (ls.get(k).equalsIgnoreCase("off") || ls
												.get(k).startsWith("off"))) {
									break;
								}
								String colcmd = ls.get(k);
								String[] cmds = colcmd.split(" ");
								for (int k1 = 0; k1 < cmds.length; k1++) {
									if (!cmds[k1].equalsIgnoreCase("format"))
										continue;
									if (cmds[k1 + 1].charAt(0) == 'a' || cmds[k1 + 1].charAt(0) == 'A')
										continue;
									// Bugs 12673037, 12672987 & 12672948
									colFormatAvailable=true;//so try the format code
								}
							}
						}

						if (storedColsCmds.containsKey(colName) && colFormatAvailable
								&& (o instanceof oracle.sql.NUMBER
										|| o instanceof oracle.sql.BINARY_DOUBLE || o instanceof oracle.sql.BINARY_FLOAT)) {
							ArrayList<String> ls = storedColsCmds.get(colName);
							for (int k = 0; k < ls.size(); k++) {
								if (k == 0
										&& (ls.get(k).equalsIgnoreCase("off") || ls
												.get(k).startsWith("off"))) {
									s = DataTypesUtil.stringValue(o, conn);
									break;
								}
								String colcmd = ls.get(k);
								String[] cmds = colcmd.split(" ");
								for (int k1 = 0; k1 < cmds.length; k1++) {
									if (!cmds[k1].equalsIgnoreCase("format"))
										continue;
									if (cmds[k1 + 1].charAt(0) == 'a'
											|| cmds[k1 + 1].charAt(0) == 'A')
										continue;
									// Bugs 12673037, 12672987 & 12672948
									String fmtstr = cmds[k1 + 1];
									NUMBER num = brsNext ? ((OracleResultSet) rset)
											.getNUMBER(i) : (NUMBER) o;

											// s = getFormattedNumber(conn, num, fmtstr);
											try {
												s = num.toFormattedText(fmtstr, null);
											} catch (SQLException ex) {
												if (ex.getMessage().equalsIgnoreCase(
														"Unimplemented method called")) { //$NON-NLS-1$
													if (fmtstr.length() == 2
															&& (fmtstr.startsWith("r") || fmtstr
																	.startsWith("R"))) {
														// Bug 13027341
														int intval = num.ceil().intValue();
														s = RomanUtil.getRomanFmt(intval,
																colsizes[i]);
														if (Character.isLowerCase(fmtstr
																.charAt(0))) {
															s = s.toLowerCase();
														}
													} else if (fmtstr.startsWith("x")
															|| fmtstr.startsWith("X")) {
														int intval = num.ceil().intValue();
														// Bug 13027394
														if (intval < 0) { // SQL*PLUS doesn't
															// convert negative
															// values to
															// hexadecimal.
															char[] chars = new char[fmtstr
															                        .length() + 1];
															Arrays.fill(chars, '#');
															s = String.copyValueOf(chars);
														} else {
															// round to a nearest value first.
															if (Character.isLowerCase(fmtstr
																	.charAt(0))) {
																s = String.format("%x", num
																		.ceil()
																		.bigIntegerValue());
															} else {
																s = String.format("%X", num
																		.ceil()
																		.bigIntegerValue());
															}
														}
													}
												} else if (ex.getMessage().equalsIgnoreCase(
														"Invalid Oracle Number")) { //$NON-NLS-1$
													char[] chars = new char[formattedNUMBERLength(fmtstr)];
													Arrays.fill(chars, '#');
													s = String.copyValueOf(chars);
												} else {
													s = DataTypesUtil.stringValue(o, conn);
												}
											}
								}
							}
							if (s.equals(""))
								s = DataTypesUtil.stringValue(o, conn);
						} else {
							if (o instanceof oracle.sql.NUMBER) { // Bug 14182753
								try {
									NUMBER num = brsNext ? ((OracleResultSet) rset).getNUMBER(i) : (NUMBER) o;

									//NOTE:For numeric columns, COLUMN FORMAT settings take precedence over SET NUMFORMAT settings, 
									// which take precedence over SET NUMWIDTH settings.
									
									int colsize_i = colsizes[i];
									int numFormatLen = m_numformat.length();
									String numberString = NUMBER.toString(num.toBytes());
									int numberByteLen = NUMBER.toString(num.toBytes()).length();

									if ( numFormatLen == 0 ) {
										// there is no numformat.
										// 1) Check if byteLen < m_numwidth && numwidth < 7
										// because it is ok to show the number for numberByteLen < m_numwidth && m_numwidth < 7.
										if ( numberByteLen > m_numwidth && m_numwidth < 7) {
											//char[] chars = new char[m_numwidth];
											//Arrays.fill(chars, '#');
											//s = String.copyValueOf(chars);
											num = num.floatingPointRound(m_numwidth);
											numberString = NUMBER.toString(num.toBytes());
											s = processNumberLessThanNumwidth(numberString, m_numwidth);
										}
										else if (checkIntegralPart(numberString, m_numwidth)) {
											// Note: 2 for 9. (integral part) & 4 for EEEE (exponential part). 
											// The remaining goes into filling the decimal part restricted by numwidth.
											StringBuffer formatStringBuffer = new StringBuffer("9.");
											if (num.sign() >= 0) {
												for (int j = 0; j < m_numwidth - (4 + 2); j++) {
													formatStringBuffer.append("9");
												}
											}
											if (num.sign() < 0) {
												// 1 for minus sign.
												for (int j = 0; j < m_numwidth - (4 + 2 + 1); j++) {
													formatStringBuffer.append("9");
												}
											}
											formatStringBuffer.append("EEEE");
											s = formattedNUMBER(conn, num, formatStringBuffer.toString(), colsizes[i]); //$NON-NLS-1$
										}
										else {
											int localWidth = m_numwidth;
											NUMBER num1 = num.floatingPointRound(localWidth);
											String dummy = num1.stringValue();
											if ((dummy.contains(".")) && (m_numwidth > 1)) { //$NON-NLS-1$
												localWidth = m_numwidth - 1;// there is
												// an off by
												// one.
											}
											num = num.floatingPointRound(localWidth);
											s = DataTypesUtil.stringValue(num, conn);
											if ((dummy.startsWith("0.")) && (s.length() > 2)) { //$NON-NLS-1$
												s = s.substring(1);
											}
										}
									}
									else {
										// there is a numformat, then numformat takes precedence over numwidth.
										if ( numberByteLen > numFormatLen) {
											char[] chars = new char[numFormatLen];
											Arrays.fill(chars, '#');
											s = String.copyValueOf(chars);
										}
										else {
											s = formattedNUMBER(conn, num, m_numformat, colsizes[i]); //$NON-NLS-1$
										}
									}
								} catch (SQLException ex) {
								// handle error message here.
								}				
							} else if(o instanceof BINARY_DOUBLE) {
								
								BINARY_DOUBLE d = (BINARY_DOUBLE)o;
								// New code addresses the length of binary double upto 16 decimal places.
								// 25067909: DISPLAY OF BINARY_FLOAT/BINARY_DOUBLE/NUMBER WITH DIFFERENT NUMWIDTH
								double doubleValue = d.doubleValue();
								s = formattedDouble(conn, doubleValue, m_numformat, colsizes[i]);
							} else if(o instanceof BINARY_FLOAT) {
								// Bug 25073496
								float f = ((BINARY_FLOAT)o).floatValue();
								s = formattedFLOAT(conn, f, m_numformat, colsizes[i]);
							} else {
								// Bug 21389264
								if (o instanceof DATE || o instanceof TIMESTAMP ||
									o instanceof TIMESTAMPTZ || o instanceof TIMESTAMPLTZ || 	
									o instanceof INTERVALDS || o instanceof INTERVALYM) {
									//Bugs 21304531 & 14593097
									OracleNLSProvider onlsp = (OracleNLSProvider)NLSProvider.getProvider(conn);
									s = onlsp.format(o);

								} else if ((o instanceof Clob || DataTypesUtil.isXMLType(o)) //LRG/SRG Fix for XMLTypes.
										&& (this.m_scriptRunnerContext != null)
										&& (this.m_scriptRunnerContext
												.getProperty(ScriptRunnerContext.SETLONG) != null)) {
									s = DataTypesUtil
											.stringValue(
													o,
													conn,
													(Integer) this.m_scriptRunnerContext
													.getProperty(ScriptRunnerContext.SETLONG));
								} else if ((o instanceof Blob)
										&& (this.m_scriptRunnerContext != null)
										&& (this.m_scriptRunnerContext
												.getProperty(ScriptRunnerContext.SETLONG) != null)) {
									Blob bb = (Blob) o;
									String ss = null;
									if (bb!=null) {
										byte[] bdata = null;
										//doneill: return only the part of the blob we are going to print, not the entire thing as this can cause OutOfMemoryException
										//use the full length of the blob only if the ScriptRunnerContext.SETLONG was not set
										Integer printSize = (Integer)this.m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETLONG);
										printSize = printSize != null?printSize:(int) bb.length();
										bdata=bb.getBytes(1,printSize.intValue());
										ss = new String(ScriptUtils.bytesToHex(bdata)).toUpperCase();
									}
									s = DataTypesUtil
											.stringValue(
													ss,
													conn,
													(Integer) this.m_scriptRunnerContext
													.getProperty(ScriptRunnerContext.SETLONG));
								} 
								else if (o instanceof BINARY_FLOAT||o instanceof BINARY_DOUBLE) {
									if (o instanceof BINARY_FLOAT) {
										BINARY_FLOAT f = ((BINARY_FLOAT) o);
										// Bug 21381415: NUMFORMAT takes precedence if set.
										if (m_numformat.length() > 0) {
											NUMBER fv = new NUMBER(f.floatValue());
											s = formattedNUMBER(conn, fv, m_numformat,
													colsizes[i]);

										}
										else {
											if(!Float.isNaN(f.floatValue())) {
												NUMBER fv = new NUMBER(f.floatValue());
												float floatValue = fv.floatingPointRound(m_numwidth).floatValue();
												s = formattedFLOAT(conn, floatValue, m_numformat, colsizes[i]);
											}
											else
											   s = formattedFLOAT(conn, f.floatValue(), m_numformat, colsizes[i]);
											
										}
									}
									else {
										BINARY_DOUBLE d = ((BINARY_DOUBLE) o);
										// Bug 21381415: NUMFORMAT takes precedence if set.
										if (m_numformat.length() > 0) {
											if(m_numformat.matches("9*((.|,)(9+))?")) {
												String dfPattern = m_numformat.replaceAll("9", "0").replaceFirst("0", "#");
												DecimalFormat decimalFormat = new DecimalFormat(dfPattern);
												s = decimalFormat.format(d.doubleValue());
											}
											else {
												Double dbl = Double.parseDouble(String.valueOf(d.doubleValue()));
												if(dbl.isNaN() || dbl.isInfinite()) {
													s = formattedFLOAT(conn, Float.parseFloat(String.valueOf(d.doubleValue())), m_numformat, colsizes[i]);
												}
												else {
													NUMBER dv = new NUMBER(d.doubleValue());
													s = formattedNUMBER(conn, dv, m_numformat,
															colsizes[i]);
												}
											}
										}
										else {
											s = formattedFLOAT(conn, Float.parseFloat(String.valueOf(d.doubleValue())), m_numformat, colsizes[i]);
										}

									}
								} else if(o instanceof ResultSet)  {
									//LRG/SRG Regression for Cursor. The code in the Base class(ResultSetFormatter) is bad.
									SQLPLUSCmdFormatter formatter = new SQLPLUSCmdFormatter(m_scriptRunnerContext);
									String prev_cursorData = "";
									StringBuffer buf = new StringBuffer();
									cursorCnt = formatter.rset2sqlplus((ResultSet) o, conn, buf);
									m_cursorHeader = formatter.getCursorStmtHeader();
									// Bug 26530381 temporarily save the cursor data.
									if(cursorData.length() > 0) {
										prev_cursorData = cursorData;
									}
									cursorData = buf.toString();
									int feedback = m_scriptRunnerContext.getFeedback();
							        Object silentMode = m_scriptRunnerContext.getProperty(ScriptRunnerContext.SILENT);
							        boolean feedOn = ( feedback != ScriptRunnerContext.FEEDBACK_OFF | silentMode != null ) && !m_scriptRunnerContext.isJSONOutput();
							        if (feedOn && feedback!=ScriptRunnerContext.FEEDBACK_OFF &&  cursorCnt >= feedback) {
										if(cursorCnt == 1)
											cursorData += MessageFormat.format(ScriptRunnerDbArb.getString("SELECTONE"), new Object[] {String.valueOf(cursorCnt)});
										else
											cursorData += MessageFormat.format(ScriptRunnerDbArb.getString("SELECTED"), new Object[] {String.valueOf(cursorCnt)});
							        }
									s = MessageFormat.format(ScriptRunnerDbArb.getString("CURSOR_STATEMENT"), new Object[] {String.valueOf(i)});
									cursorData = m_lineSeparator + s +  m_lineSeparator + cursorData;
									if(prev_cursorData.length() > 0) {
										cursorData = prev_cursorData + m_lineSeparator + cursorData;
									}
								} else {
									s = DataTypesUtil.stringValue(o, conn);
								}
							}
						}
						
						// Bug 21141265
						if(o instanceof REF) {
							byte[] rbytes = ((REF)o).getBytes();
							s = new BigInteger(1, rbytes).toString(16).toUpperCase();
						}

						if (o instanceof STRUCT) {
							s = processTypeAttributesForStruct((Struct) o, conn);
						}
						if (o instanceof ARRAY) {
							s = processTypeAttributesForArray((ARRAY) o, conn);
						}
						StringBuffer nlsed = new StringBuffer();
						if (s != null) {
							if ((o instanceof Clob)
									&& (this.m_scriptRunnerContext != null)
									&& (this.m_scriptRunnerContext
											.getProperty(ScriptRunnerContext.SETLONG) != null)) {
								nlsed = new StringBuffer((DataTypesUtil.stringValue(s, rset
										.getStatement().getConnection(),(Integer)m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETLONG)).replace(
												"\r\n", m_lineSeparator))); //$NON-NLS-1$s = DataTypesUtil
							} else {
								nlsed = new StringBuffer((DataTypesUtil.stringValue(s, rset
										.getStatement().getConnection())).replace(
												"\r\n", m_lineSeparator)); //$NON-NLS-1$
							}
						}
						if ((this.m_scriptRunnerContext != null)&&(newVal)) {
							if (lastVal == null) {
								lastVal = new String[colCnt + 1];
							}
							lastVal[i] = nlsed.toString();
						}

						if (m_scriptRunnerContext != null && oldVal) {
							if (oldlVal == null) {
								oldlVal = new String[colCnt + 1];
							}
							oldlVal[i] = nlsed.toString();
						}

						cacheRow.set(i, nlsed);
					}
					boolean multiline = false;
					boolean stopit = false;
					boolean multiLineRow = false;
					getFontMetrics();
					boolean bLastCol = false;
					ArrayList<Integer> fmtd_colIndexes = new ArrayList<Integer>();
					boolean brkSkip = true;
					boolean noDupAllowed = false;
//					int skipLinesPerColumn = 0;

					//			ArrayList<StringBuffer> cpcacheRow = new ArrayList<StringBuffer>();
					if(cpcacheRow.size() > 0) {
						cpcacheRow.clear();
					}
					for (int i = 0; i < cacheRow.size(); i++) {
						cpcacheRow.add(new StringBuffer(cacheRow.get(i)));
					}

					if(!bCmptdData) {
						if(cpttlcacheRow.size() > 0) {
							cpttlcacheRow.clear();
						}
						for (int i = 0; i < cacheRow.size(); i++) {
							cpttlcacheRow.add(new StringBuffer(cacheRow.get(i)));
						}
					}


					if (cnt == 1 && !bCmptdData) {
						int hdr_tpttl_lines = numberofLines(tTitle_header);
//						int jdiff = (adjPagesize - bTitle_lines);
						int jdiff = (adjPagesize - bTitle_lines) - m_newpage;
						tTitle_header = getTopTitleAndHeader(pageNo, tot_header, cMap, ovcMap, cpttlcacheRow, columns);
						bTitle_lines = numberofLines(getBottomTitle(pageNo, cMap, ovcMap, cpttlcacheRow, columns));
						// Bug 19497212: Implementation of SET EMBEDDED TRUE command.
						if(m_embedded && cnt == 1) {
//							bTitle_lines = numberofLines(getBottomTitle(pageNo, cMap, ovcMap, cpttlcacheRow, columns));
							// check to see if there is enough room to fit the header
							if (jdiff >= hdr_tpttl_lines) {
								AtomicInteger lineN = new AtomicInteger(lineNo);
								AtomicInteger pageN = new AtomicInteger(pageNo);
								appendDataToBuffer(databuf, lineN, pageN, tTitle_header, offSet);
								lineNo = lineN.intValue();
								pageNo = pageN.intValue();
							}
						}
						else {
							// Changes here due to NEWPAGE command 
							for(int np = 0; np < m_newpage; np++) {
								databuf.append(m_lineSeparator);
								lineNo++;
							}
							// For bug 21475531: For bottom title changeing ovcMap to cMap. -sroychow 02/03/16
							// Added ovcMap so that both new and old values are available to the title code.
							// bug 23048140: BTITLE DOES NOT GET VALUE OF VAR SPECIFIED BY OLD_VALUE
//							bTitle_lines = numberofLines(getBottomTitle(pageNo, cMap, ovcMap, cpttlcacheRow, columns));
							// check to see if there is enough room to fit the header
							if (jdiff >= hdr_tpttl_lines) {
								databuf.append(tTitle_header);
								lineNo = lineNo + numberofLines(tTitle_header);
							}
						}

					}

					// Bug 20308000 look for duplicates here.
					// Bug 25087225 check for pagesize.
					if (brsNext) {
						if (oldPageNo < pageNo && m_pagesize > 0) {
							oldPageNo = pageNo;
							newPage = true;
						} else {
							newPage = false;
						}
						noDupAllowed = hasDuplicates(storedBrkCmds, rset, cpcacheRow,
								brkColValues, newPage);
					}

					while (stopit == false) {
						// int sizediff = m_delim_lnsize;
						int sizediff = m_linesize;
						boolean bsizediffed = false;
						stopit = true;
						bLastCol = false;
						for (int i = 1; i < colCnt + 1; i++) {
							if(i >= colsizes.length) break;
							// LRG/SRG fix
							// For Multi rows when the previous row was not line terminated we need 
							// to line terminate here.
							if (databuf.length() == 0 && /*m_foldedrows &&*/ missingLineTerminators.length() > 0) {
								Matcher m = Pattern.compile(m_lineSeparator).matcher(missingLineTerminators);
								int lineSeps = 0;
								while (m.find()) {
									lineSeps++;
								}
//								int diff = (adjPagesize * pageNo) - bTitle_lines;
								int diff = (adjPagesize * pageNo) - bTitle_lines - m_newpage;
								for(int ls = 0; ls < lineSeps && lineNo <= diff; ls++) {
									databuf.append(m_lineSeparator);
									lineNo++;
								}
								missingLineTerminators = "";
							}
							
							// next chunk is to next newline or all
							StringBuffer nextChunk = new StringBuffer();
//							String colName = rmeta.getColumnName(i).toLowerCase();
							String colName = getColumnName(rset, i).toLowerCase();
							// if(!printColumn(storedColsCmds, colName)) continue;
							boolean bPrint = printColumn(storedColsCmds, colName);
							if (!bPrint) {
								// Bug 20389766 we still need Break column actions if it
								// is "noprint"
								String brkCol = getBreakColumn(storedBrkCmds,
									getColumnName(rset, i));
								if (brkCol == null) {
									cacheRow.set(i, new StringBuffer(DONE));
									continue;
								}
							}
							int colType = rmeta.getColumnType(i);
							Datum o = null;
							if (bPrint) {
								// Bug 21145787 code cleanup
								if (cacheRow.get(i).toString().equals(DONE)) {
									if (multiLineRow) {
										// we accounted for this column before so let's go to the next column.
										continue; 
									}
									else {
										nextChunk.append(justify_left(" ", colsizes[i])); //$NON-NLS-1$
									}
								}
								else {
									if (fmtd_colIndexes.contains(i)) {
										nextChunk = cacheRow.get(i);
									}
									else {
										nextChunk = checkFormatOptions(rset, i, storedColsCmds, colsizes, cacheRow);
										if (nextChunk.length() == 0 && isNumericColumn(colType)) {
											nextChunk = cacheRow.get(i);
										}
									}

									if (nextChunk.length() == 0) {
									        // LRGSRG: Long identifier test fix. Identifier length can be longer than linesize. Use linesize 
										// as column size.
									        int colsize = colsizes[i] > m_linesize ? m_linesize : colsizes[i];
										// Bug 21113900, 12664179 & 20461131 for long columns.
										if (isLongColumn(colType)) {
											if (colsizes[i] <= m_long) {
												if (m_wrap.equalsIgnoreCase("ON")) {
													nextChunk = truncate(colType, m_long, cacheRow.get(i));
													nextChunk = wrap(colType, colsize, nextChunk);
												}
												else
													nextChunk = truncate(colType, colsize, cacheRow.get(i));
											}
											else {
												if (m_wrap.equalsIgnoreCase("ON")) {
													nextChunk = wrap(colType, colsize, cacheRow.get(i));
												}
												else {
													nextChunk = truncate(colType, colsize, cacheRow.get(i));
												}
											}

										}
										else {
										    if (m_wrap.equalsIgnoreCase("ON")) //$NON-NLS-1$
                                                    					nextChunk = wrap(colType, colsize, cacheRow.get(i));
                                                    				    else
                                                    					nextChunk = truncate(colType, colsize, cacheRow.get(i));
										}
									}
								}

								// fix for Windows line terminator issue.
								int tIdx = nextChunk.indexOf("\n");
								int ival = nextChunk.indexOf(m_lineSeparator);
								if(tIdx != ival && System.getProperty("os.name").startsWith("Windows")) {
									if (tIdx >= 0 && ival == -1 || tIdx != ival+1) {
										nextChunk = new StringBuffer(nextChunk.toString().replaceAll("\\n", m_lineSeparator));
									}

								}
								ival = nextChunk.indexOf(m_lineSeparator);
								if (ival != -1 && (nextChunk.lastIndexOf(m_lineSeparator) != ival || i >= 1/*to make sure we print data at the right column*/)
										) { 
									// Bug 19498508 do caching as long it is not the last line terminator.
									multiline = true;
									fmtd_colIndexes.add(i);
									StringBuffer store = nextChunk;
									nextChunk = new StringBuffer(nextChunk.substring(0,
											ival));

									if ((store.length() - 1) > ival) {
										store = new StringBuffer(store.substring(ival
												+ m_lineSeparator.length()));
									} else {
										store = new StringBuffer(""); //$NON-NLS-1$
									}
									// Bug 21544801
									if(store.length() == 0) {
										cacheRow.set(i, new StringBuffer(DONE));
									}
									else {
										cacheRow.set(i, store);
										stopit = false;
										brkSkip = false;
									}
								} else {
									cacheRow.set(i, new StringBuffer(DONE)); //$NON-NLS-1$
									stopit = true; // may have been set to false by a previous column.
									// LRG/SRG fix
									// We are in a Multi line row and this column had to be wrapped(wrap on) because 
									// of smaller column size if it is not the first column for that "LINE" we need fill up with spaces.
									if (multiLineRow && i > 1 && i < colCnt + 1 && m_colsizes[i] < m_linesize &&
										databuf.toString().endsWith(m_lineSeparator) && nextChunk.toString().matches("[^\\s]+.*")) {
										String[] headers = tot_header.toString().split(m_lineSeparator);
										for(int h = 0; h < headers.length; h++) {
											// LRG/SRG fix
										        // Making sure we got the exact matched column.
										        int hidx = -1;
										        Pattern p = Pattern.compile("(\\b" + Pattern.quote(m_columns[i]) + "\\b)");
										        Matcher m = p.matcher(headers[h]);
										        int mCnt = 0;
										        while (m.find()) {
										             mCnt++;
										             if(m.group(1).equals(m_columns[i])) { 
										        	 hidx = m.start(mCnt);
										        	 break;
										             }
										             else if(m.matches()) {
										                 hidx = m.start();
										                 break;
										             }
										        }
											if(hidx > 0) {
												nextChunk = new StringBuffer(justify_left(" ", hidx) + nextChunk.toString());
												sizediff = sizediff - hidx;
												// Bug 25026322 We don't want to recalculate the sizediff later. 
												bsizediffed = true;
												break;
											}
										}
									}
								}

								// Removed exclusion for Long type columns. Check isLongColumn(..) functions
								// for Long types. If the data for Long type cols is small enough we should pad them with spaces.
								// Bug 21498889 this is when console width becomes less than the column size.
								int consoleWidth = m_scriptRunnerContext.getSQLPlusConsoleReader() != null ? m_scriptRunnerContext.getSQLPlusConsoleReader().getWidth() : 0;
								if (brsNext) {
									if(consoleWidth < m_linesize && m_columns.length < 3) {
										font_width_adjustment(nextChunk,
												nextChunk.length());
									}
									else {
										font_width_adjustment(nextChunk,
												colsizes[i]);
									}

								} else {
									// could be the computed values.
									String val = nextChunk.toString();
									//								if (val.trim().matches("\\d+(\\.\\d+)?")) {
									if (val.trim().matches("(\\+|-)?\\d*(\\.\\d+((?i:e){1}(\\+|-))?\\d+)?")) {	
										nextChunk = new StringBuffer(justify_right(
												val.trim(), colsizes[i]));
									} else {
										font_width_adjustment(nextChunk,
												colsizes[i]);
									}
								}
								
								if (isNumericColumn(colType)) {
									String numRegexp = "(\\+|-)?\\d*(\\.\\d+(((?i:e){1}(\\+|-))?\\d+)?)?|Nan|(-)?Inf";
									String val = nextChunk.toString();
									if(brsNext) {
										if (!val.trim().matches(numRegexp)) {
											font_width_adjustment(nextChunk,
													colsizes[i]);
										}
										else {
										  // LRGSRG: Long identifier test fix. Identifier length can be longer than linesize. Use linesize 
										  // as column size.  
										  int colsize = colsizes[i] > m_linesize ? m_linesize : colsizes[i];  
										  nextChunk = new StringBuffer(justify_right(val.trim(), colsize));
										}
									}
									else {
										if (!val.trim().matches(numRegexp)) {
											font_width_adjustment(nextChunk,
													colsizes[i]);
										} 
										else {
											// Bug 20504737
											if (val.length() <= m_linesize) {
												// Bug 21543492
												if(o == null || o.isNull()) {
													if (val.trim().matches(numRegexp))
													  nextChunk = new StringBuffer(justify_right(val.trim(), colsizes[i]));
													else
													  nextChunk = new StringBuffer(justify_left(val.trim(), colsizes[i]));
												}
												else
												    nextChunk = new StringBuffer(justify_right(val.trim(), colsizes[i]));
											}
											else {
												String message = MessageFormat.format(ScriptRunnerDbArb.getString("LINESIZELIMIT"), new Object[] {String.valueOf(i), m_columns[i]}) + m_lineSeparator;
												write(out, message);
												return 0;
											}
										}
									}
								}
								// Bug 13870896: We don't left justify if data type is
								// not NUMBER
								// in this way any spaces on the left are preserved.
								/*
								 * else { String val = nextChunk.toString(); nextChunk =
								 * new StringBuffer(justify_left(val.trim(),
								 * val.length())); }
								 */
							}

							// Bug 13355429 fix, if don't print column check whether it
							// is the
							// last column.
							if (i == colCnt) {
								bLastCol = true;
							}

							if ((m_pagesize < 0 || m_linesize < 0) && bPrint) {
								// No Vertical bars so that the result is SQL*Plus like
								if (m_colsep.equals(" ")) { //$NON-NLS-1$
									nextChunk.append(m_colsep);
								} else {
									if (i < colCnt)
										nextChunk.append(m_colsep);
								}
								lastStr = nextChunk.toString();
								databuf.append(lastStr);
							} else if (numberofLines(tot_header) == 2
									&& !isAValidLinesize() && bPrint) { // no folding

								// Bug 20681988 
								if (lineNo >= ((adjPagesize * pageNo) - bTitle_lines) - 1 && i == 1) {	
									pageNo++;
									databuf.append(m_lineSeparator);
									lineNo++;
									databuf.append(tot_header.toString());
									lineNo = lineNo + numberofLines(tot_header);
								}
								// No Vertical bars so that the result is SQL*Plus like
								if (i < colCnt)
									nextChunk.append(m_colsep);

								lastStr = nextChunk.toString();
								databuf.append(lastStr);
							} else if (m_pagesize >= 0 || m_linesize > 0) {
								boolean bAllowColSep = true;
								if (bPrint) {
								        // LRGSRG: Long identifier test fix. Identifier length can be longer than linesize. Use linesize 
									// as column size.
								        int colsize = colsizes[i] > m_linesize ? m_linesize : colsizes[i];
								       // Bug 25026322 Added this check so we don't want to recalculate the sizediff
								       // value again if this was done before.
										if (!bsizediffed) {
											if (sizediff <= colsize) {
												sizediff = sizediff - colsize;
											} else {
												// Bug 20511433
												if (sizediff < colsize + m_colsep.length()) {
													sizediff = sizediff - colsize;
													bAllowColSep = false;
												} else {
													sizediff = sizediff - (colsize + m_colsep.length());
													bAllowColSep = true;
												}
											}
										}
										// reset the boolean back
										bsizediffed = bsizediffed ? false : bsizediffed;
								}

								if (sizediff < 0) {
									// Bug 19625168
									// Cleaned up this area as part of this bug fix and
									// other previous bugs
									// 13355429, 13408276 & 17671573
									if (bPrint) {
										if (m_colsep.length() > 0
												&& !m_colsep.equals(" ")) {
											String tempStr = lastStr;
											int strIdx = databuf.lastIndexOf(tempStr);
											tempStr = replaceColSep(tempStr);
											if (strIdx > -1) {
												databuf.replace(strIdx, strIdx
														+ tempStr.length(), tempStr);
												String tmp = databuf.toString();
												// Bugs 19520419 and 20511433
												// trim all the spaces at the end of the string.
												tmp = tmp.replaceAll("\\s+$", "");
												databuf.setLength(0);
												databuf.append(tmp);
											}
										}

										nextChunk = new StringBuffer(nextChunk
												.toString().trim());
										// save this value and only to be picked up in
										// the next iteration
										// of this for loop.
										if (cacheRow.get(i).toString().equals(DONE)) // if
											// there
											// is
											// no
											// data
											// in
											// cacheRow
											// then
											// we
											// set
											// it.
											cacheRow.set(i, nextChunk);
										else {
											if (multiline) {
												nextChunk.append(m_lineSeparator);
											}
											nextChunk.append(cacheRow.get(i));
											cacheRow.set(i, nextChunk);
										}
										multiLineRow = true;
										m_foldedrows = true;
									}
								} else {
									String str = "";
									if (bPrint) {
										if (i == colsizes.length - 1) {
											str = nextChunk.toString();
										} else {
											// Bug 20511433
											if(bAllowColSep) {
												// Bug 19498508 avoid colsep there isn't enough space left on the line.	
												if(sizediff > 0)	
													str = nextChunk.toString() + m_colsep;
												else
													str = nextChunk.toString();  
											}
											else
												// Bug 19498508 avoid unnecessary empty spaces as this causes
												// unnecessary lines for some reason.	
												str = nextChunk.toString().replaceAll("\\s+$", "");	
										}

										if (m_pagesize < 3 && m_pagesize <= adjPagesize
												&& i == 1 && cnt > 1) {
											pageNo++;
										}
									}

									// Break command Check for SKIP # of LINES/PAGE action.
									if(m_breakcolumns.size() > 0) {
										String columnName =  "";
										// Bug 21511877 bCmptdReport for Report computation.
										if((brsNext || bCmptdReport) && i == 1 && !multiline) {// new row.
//											bCmptdReport = false;
											boolean bskipLines = false;
											for (int j = 1; j < colCnt + 1; j++) {
												columnName = m_column_to_brkcol.get(m_columns[j]);
												if(columnName != null) {
													int cType = rmeta.getColumnType(j);
													int skipLines = skippedLines(storedBrkCmds, columnName);
													if(skipLines > 0) {
														String prevVal = (String)prevColToVal.get(j);
														if((prevVal == null || prevVal.length() == 0) && cnt == 1) {
															if(j == 1) {
																if(bPrint) {
																	// Bug 23047676 checking for numeric types.
																	if(isNumericColumn(cType)) {
																		Datum n = null;
																		try {
																			n = ScriptUtils.getOracleObjectWrap((OracleResultSet) rset,j);
																		}
																		catch(Exception e) {
																		}
																		String nStr = n != null ? n.stringValue() : "NULL";
																		prevColToVal.put(new Integer(j), nStr);
																	}
																	else
																	  prevColToVal.put(new Integer(j), str.trim());
																}
																else {
																	prevColToVal.put(new Integer(j), cacheRow.get(j).toString());
																}
															}
															else {
																// Bug 23047676 checking for numeric types.
																if(isNumericColumn(cType)) {
																	Datum n = null;
																	try {
																		n = ScriptUtils.getOracleObjectWrap((OracleResultSet) rset,j);
																	}
																	catch(Exception e) {
																	}
																	String nStr = n != null ? n.stringValue() : "NULL";
																	prevColToVal.put(new Integer(j), nStr);
																}
																else
																  prevColToVal.put(new Integer(j), cacheRow.get(j).toString());
															}
														}
														else if(prevVal != null && isNumericColumn(cType)) {
															// Bug 23047676 checking for numeric types.
															Datum n = null;
															try {
																n = ScriptUtils.getOracleObjectWrap((OracleResultSet) rset,j);
															}
															catch(Exception e) {
															}
															String nStr = n!=null ? n.stringValue() : "NULL";
															if(!prevVal.equals(nStr)) {
																bskipLines = true;
																if(bPrint)
																  prevColToVal.put(new Integer(j), nStr);
																else 
																  prevColToVal.put(new Integer(j), cacheRow.get(j).toString());
															}
														}
														// Bug 21690585 for "Nan" values Two Nan's are not equal. 
														else if(prevVal != null && (str.trim().equals("Nan")  ||
																!prevVal.equals(j == 1 && str.trim().length() > 0 ? str.trim() : cacheRow.get(j).toString() == null ? "" : cacheRow.get(j).toString().trim()))
																//														!prevVal.equals(j == 1 && str.trim().length() > 0 ? str.trim() : cacheRow.get(j).toString().length() > 0 ? cacheRow.get(j).toString() : prevVal))
																) {
															bskipLines = true;
															if(j == 1) {
																if(bPrint)
																	prevColToVal.put(new Integer(j), str.trim());
																else {
																	prevColToVal.put(new Integer(j), cacheRow.get(j).toString());
																}
															}
															else {
																prevColToVal.put(new Integer(j), cacheRow.get(j).toString());
															}															
														}
														// Bug 22538861
														m_breakcolToSkipLinesForGrp.put(j, skippedLines(storedBrkCmds, columnName));
													}

													int skipPage = skippedPage(storedBrkCmds, columnName);
													if(skipPage == 1) {
														String prevVal = (String)prevColToVal.get(j);
														if((prevVal == null || prevVal.length() == 0) && cnt == 1) {
															if(j == 1) {
																if(bPrint)
																	prevColToVal.put(new Integer(j), str.trim());
																else {
																	prevColToVal.put(new Integer(j), cacheRow.get(j).toString());
																}
															}
															else {
																prevColToVal.put(new Integer(j), cacheRow.get(j).toString());
															}
														}
														else if(prevVal != null && (prevVal.equals("Nan") || !prevVal.equals(j == 1 ? str.trim() : cacheRow.get(j).toString()))) {
															if(j == 1) {
																if(bPrint)
																	prevColToVal.put(new Integer(j), str.trim());
																else {
																	prevColToVal.put(new Integer(j), cacheRow.get(j).toString());
																}
															}
															else {
																prevColToVal.put(new Integer(j), cacheRow.get(j).toString());
															}
															// We skip the page here.
															// Bug 22538861
															int limit = (adjPagesize * pageNo) - (bTitle_lines);
															if(lineNo < limit) {
															    lineNo = limit;
															}
														}
													}
												}

												// Bug 20389766 we are done with the "noprint" break column here.
												if(!bPrint)
													cacheRow.set(i, new StringBuffer(DONE));
											}
											
											// Bug 20389766 Skip lines only if there are enough lines available in the page.
											// Bug 20485946 make sure to skip lines if it is the last Group for the Report. 
											// using m_breakcolToSkipLinesForGrp to keep track. 
											// Bug 23044669 Actions are executed beginning with the action specified for the 
											// innermost break and proceeding in reverse order toward the outermost break.
											if(bskipLines) {
												int size = m_breakcolToSkipLinesForGrp.size();
												Iterator<Integer> slIter = m_breakcolToSkipLinesForGrp.keySet().iterator();
//												for (int j = size; j > 0; j--) {
												while(slIter.hasNext()) {
													int key = slIter.next();
													if(m_breakcolToSkipLinesForGrp.get(key) != null)
													   skipLinesPerColumn += m_breakcolToSkipLinesForGrp.get(key);
												}
												int linesLimit = (adjPagesize * pageNo) - (bTitle_lines);
												// Bug 25087225 check for pagesize
												while(skipLinesPerColumn > 0 && (lineNo < linesLimit || m_pagesize == 0)) {
													skipLinesPerColumn--;
													databuf.append(m_lineSeparator);
													lineNo++;
												}
												// Bug 20443163
												if(skipLinesPerColumn > 0) {
													// we need to skip lines in the new page.
													missingSkipLines += skipLinesPerColumn;
												}
												skipLinesPerColumn = 0; // reset.
											}
											
											int skipPage = skippedPage(storedBrkCmds);
											if(skipPage == 1 && cnt > 1) {
												if(lineNo <  ((adjPagesize * pageNo) - bTitle_lines)) {
													lineNo =  ((adjPagesize * pageNo) - bTitle_lines);
												}
											}
										}
										// Bug 21519956 once we are done with break actions and the
										// break column is noprint then we are done with this column.
										else if(!brsNext || !bPrint) {
											cacheRow.set(i, new StringBuffer(DONE));
										}
									}

									// Bug 20511433
									if(!bAllowColSep && (cnt > 1 || i > 1) && lineNo < ((adjPagesize * pageNo) - bTitle_lines) - 1) {
										databuf.append(m_lineSeparator);
										lineNo++;
									}

									// Bug 12664267 need to adjust the pagesize if it is
									// set too small.
									// Changes here due to NEWPAGE command 
									int hdr_tpttl_lines = numberofLines(getTopTitleAndHeader(pageNo, tot_header, cMap, ovcMap, cpttlcacheRow, columns));
									int diff = (adjPagesize * pageNo) - bTitle_lines;
									int jdiff = (adjPagesize - bTitle_lines);
									if (bPrint && adjPagesize > 0 && cnt > 0 && lineNo >= diff) {
										if (jdiff >= hdr_tpttl_lines) {
											if (bTitle_lines > 0) {
												// For bug 21475531: For bottom title changeing ovcMap to cMap. -sroychow 02/03/16
												//databuf.append(getBottomTitle(pageNo, cMap, cpttlcacheRow, columns));
												// Added ovcMap so that both new and old values are available to the title code.
												// bug 23048140: BTITLE DOES NOT GET VALUE OF VAR SPECIFIED BY OLD_VALUE
												databuf.append(getBottomTitle(pageNo, cMap, ovcMap, cpttlcacheRow, columns));
												lineNo = lineNo + bTitle_lines;
											}
											pageNo++;
											// This may be necessary further testing is needed.
											//if(lineNo == diff) {
												for (int np = 0; np < m_newpage; np++) {
													databuf.append(m_lineSeparator);
													lineNo++;
												}
											//}
											tTitle_header = getTopTitleAndHeader(pageNo, tot_header, cMap, ovcMap, cpttlcacheRow, columns);
											int tot_hdr_lines = numberofLines(tTitle_header);
											databuf.append(tTitle_header);
											lineNo = lineNo + tot_hdr_lines;
										}
										else {
											pageNo++;
											for (int np = 0; np < m_newpage; np++) {
												databuf.append(m_lineSeparator);
												lineNo++;
											}
										}
										
										// LRG/SRG fix for Multiline Rows.
										if (m_foldedrows && !bAppendedMultiRowLnTerm && i == 1) {
											if (lineNo < ((adjPagesize * pageNo) - bTitle_lines)) {	
												databuf.append(m_lineSeparator);
												lineNo++;
												bAppendedMultiRowLnTerm = true;
											}
										}

										if (dataBufHelper != null) {
											dataBufHelper.mark(m_scriptRunnerContext, out, databuf);
										}
									}

									// SQL*PLUS prints data even if pagesize is 0
									// if(lineNo <= m_pagesize * pageNo || m_pagesize ==
									// 0) {
									// if(lineNo <= adjPagesize * pageNo || adjPagesize
									// == 0) {

									// Bug 20389766
									// We are in a new page so if we didn't skip earlier
									// because of lack of space, we can do that here.
									if (missingSkipLines > 0) {
										if (lineNo + missingSkipLines < ((adjPagesize * pageNo) - bTitle_lines) - 1) {
											for (int k = 0; k < missingSkipLines; k++) {
												databuf.append(m_lineSeparator);
											}
											lineNo = lineNo + missingSkipLines;
											missingSkipLines = 0; // reset back once
											// done.
										}
									}

									// Break command Check for DUPLICATES/NODUPLICATES
									//							if (brsNext
									//									&& nextChunk.length() > 0
									//									&& storedBrkCmds.size() > 0
									//									&& getBreakColumn(storedBrkCmds,
									//											rmeta.getColumnName(i)) != null) {
									
//									int linesLimit = (adjPagesize * pageNo) - (bTitle_lines + m_newpage);
//									while(skipLinesPerColumn > 0 && lineNo < linesLimit) {
//										skipLinesPerColumn--;
//										databuf.append(m_lineSeparator);
//										lineNo++;
//									}
//									// Bug 20443163
//									if(skipLinesPerColumn > 0) {
//										missingSkipLines += skipLinesPerColumn;
//										// we need to skip lines in the new page.
//									}
									
									if (brsNext && nextChunk.length() > 0 &&
											m_column_to_brkcol.get(m_columns[i]) != null) {
										// Bug 20308000
										// Bug 25087225 check for pagesize
										if (oldPageNo < pageNo && m_pagesize > 0) {
											newPage = true;
											oldPageNo = pageNo;
											noDupAllowed = hasDuplicates(storedBrkCmds,
														rset, cpcacheRow, brkColValues,
														newPage);
										}
										//								String columnName = getBreakColumn(
										//										storedBrkCmds, rmeta.getColumnName(i));
										String columnName = m_column_to_brkcol.get(m_columns[i]);
										if (bPrint) {
											//									if (noDupAllowed
											//											&& storedBrkCmds.get(columnName)
											//													.contains("nodup")) {
											if (noDupAllowed && m_brkcol_to_nodup.get(columnName)) {
												// we are not going to allow duplicate
												// value for this column.
												char[] chars = new char[str.length()];
												Arrays.fill(chars, ' ');
												databuf.append(chars);
											} else {
												// Bug 20397731
												if (m_brkcol_to_nodup.get(columnName)) {
													boolean noDupsFromOuterToInnerBreaks = hasDuplicates(
															storedBrkCmds, rset,
															cpcacheRow, brkColValues,
															i, newPage);
													if (noDupsFromOuterToInnerBreaks) {
														// we are not going to allow
														// duplicate value for this
														// column.
														char[] chars = new char[str
														                        .length()];
														Arrays.fill(chars, ' ');
														databuf.append(chars);
													} else {
														/*if (cnt > 1 && i == colsizes.length - 1) {
													if (lineNo < ((adjPagesize * pageNo) - bTitle_lines) - 1) {
														   databuf.append(m_lineSeparator);
														   lineNo++;
													}
												}*/
														databuf.append(str);
													}
												} else {
													databuf.append(str);
												}
											}
											if (newPage && i == colsizes.length - 1) {
												newPage = false;
											}
										}
									} else if (bPrint) {
										databuf.append(str);
										lastStr = str;
										if (i == colsizes.length - 1 && colCnt == 1
												&& multiline) {
											// sizediff = m_delim_lnsize;
											sizediff = m_linesize;
											multiline = false;
											if (!cacheRow.get(i).toString().equals(DONE)) {
												multiLineRow = true;
											}
											break;
										}
									}
								}

								if (i == colsizes.length - 1) {
									if (multiLineRow) {
										// add empty line for multi-line and/or fold
										// rows.
										// Bug 21505854 sometimes the data can be null.
										if(!databuf.toString().endsWith(m_lineSeparator) || lastStr.length() == 0) {
											databuf.append(m_lineSeparator);
										}
										if (m_pagesize > 0) {
											// Bug 19498508 this due to fixing the truncate method for clobs.
											int lines = lastStr.split(m_lineSeparator).length;
											for(int li = 0; li < lines; li++) {
												if (lineNo >= ((adjPagesize * pageNo) - bTitle_lines)) {	
													pageNo++;
												}
												lineNo++;
											}
										}
										// if(i == colCnt)
										bLastCol = true;
									}
									else {
										// Bug 19498508 for clob data type we need a line terminator
										// separating the record.
										int lines = lastStr.split(m_lineSeparator).length;
										if(!multiline && !m_foldedrows && lines > 1) {
											for(int j = 1; j < lines; j++) {
												if (lineNo < ((adjPagesize * pageNo) - bTitle_lines) - 1) {
													databuf.append(m_lineSeparator);
													lineNo++;
												}
											}
										}
									}
									// we want to loop back again if the data is cached.
									if (isDataCached(cacheRow)) {
										stopit = false;
									} else {
										if (m_foldedrows) {
											// LRG/SRG fix for Multiline Rows.
											if (lineNo < ((adjPagesize * pageNo) - bTitle_lines)) {	
												databuf.append(m_lineSeparator);
												lineNo++;
												bAppendedMultiRowLnTerm = true;
											}
											else {
												bAppendedMultiRowLnTerm = false;
											}
											multiLineRow = false;
											// Bug 19498508 to avoid unnecessary empty lines
											multiline = false;
										}

										
										if(cnt > 0) {
											// Check Break command for Report Element "ROW".
											// Bug 20503662
											// Bug 21374726 moved the code here. 
                                            if(brsNext || bCmptdReport) {
                                            	bCmptdReport = false;
												int skipLinesPerRow = skippedLines(storedBrkCmds);
												int linesLimit = ((adjPagesize * pageNo) - bTitle_lines) - 1;
												while(skipLinesPerRow > 0 && lineNo < linesLimit) {
													skipLinesPerRow--;
													databuf.append(m_lineSeparator);
													lineNo++;
												}
												if(skipLinesPerRow > 0) {
													missingSkipLines += skipLinesPerRow;
												}	
                                            }
										}
										
										// Bug 21039514 when page size is set to 2(lines).
										// Bug 25087225 check for pagesize
										if (adjPagesize == m_pagesize && m_pagesize <= 2 && m_pagesize != 0) {
											databuf.append(m_lineSeparator);
											lineNo++;
										}
									}
								}
							}
						}

						// We are done with a row here.
						// remove any column separators at the end of a row.
						if (m_colsep.length() > 0 && !m_colsep.equals(" ")) {
							String tempStr = lastStr;
							int strIdx = databuf.lastIndexOf(tempStr);
							if (strIdx > -1) {
								tempStr = replaceColSep(tempStr);
								databuf.replace(strIdx, strIdx + tempStr.length(),
										tempStr);
							}
						}
						// Bug 20681988 
						if (!m_foldedrows && (adjPagesize == 0 || lineNo <= ((adjPagesize * pageNo) - bTitle_lines))) {	
							databuf.append(m_lineSeparator);
							if(adjPagesize > 0)
							   lineNo++;
						}
					}
					// Need new line if it is just multiline
					// new line is created for multiLineRow
					if (multiline && !multiLineRow) {
						// we don't want to add more than 2 line terminators at the end.
						if(!databuf.toString().endsWith(m_lineSeparator + m_lineSeparator)) {
							databuf.append(m_lineSeparator);
							lineNo++;
						}
					}
					
					// Attach Cursor Data for this Row here. 
					if (cursorData.length() > 0) {
						databuf.append(cursorData);
						// Commenting out this code for now.
//						String[] cursorArr = cursorData.split(m_lineSeparator);
//						for (int li = 0; li < cursorArr.length; li++) {
//							if (lineNo >= ((adjPagesize * pageNo) - bTitle_lines) - m_newpage) {
//								pageNo++;
//								for (int np = 0; np < m_newpage; np++) {
//									databuf.append(m_lineSeparator);
//									lineNo++;
//								}
////								int hdr_lines = numberofLines(new StringBuffer(m_cursorHeader));
////								databuf.append(m_cursorHeader);
////								lineNo = lineNo + hdr_lines;
//							}
//							databuf.append(cursorArr[li]);
//							databuf.append(m_lineSeparator);
//							lineNo++;
//						}
						cursorData = "";
					}
					
					if(out instanceof StringBuffer) {
						// We may be dealing with Cursor here
						setCursorStmtHeader(tot_header.toString());
					}
					else {
						// LRG/SRG fix
						// For Multi rows sometimes the databuf may not end up with line terminators 
						// this could happen if some column values are wrapped(wrap on) because of smaller 
						// column sizes.
						if(m_foldedrows && !databuf.toString().endsWith(m_lineSeparator + m_lineSeparator)) {
							 if(databuf.toString().endsWith(m_lineSeparator)) {
								 missingLineTerminators = m_lineSeparator;
							 }
							 else {
							     missingLineTerminators = m_lineSeparator + m_lineSeparator;
							 }
						}
					}
					
					boolean writtenOut=false;
					// we are appending or writing to pipe every row. But logic looks ok
					if (dataBufHelper!=null) {
						writtenOut=dataBufHelper.writeOut(m_scriptRunnerContext,out,databuf);
					}
					if (!(writtenOut)) {
						write(out, databuf.toString());
					}
					databuf.setLength(0);
		}

		// if are dealing with btitle command then we have to append
		// the btitle at the end of the query.
		if (bTitle_lines > 0 && m_scriptRunnerContext.getBTitleFlag()) {
			int limit = (m_pagesize * pageNo) - bTitle_lines;
			while (lineNo < limit) {
				write(out, m_lineSeparator.toString());
				lineNo++;
			}
			// For bug 21475531: For bottom title changeing ovcMap to cMap. -sroychow 02/03/16
			// Added ovcMap so that both new and old values are available to the title code.
			// bug 23048140: BTITLE DOES NOT GET VALUE OF VAR SPECIFIED BY OLD_VALUE
			write(out, getBottomTitle(pageNo, cMap, ovcMap, cpttlcacheRow, columns).toString());
			lineNo = lineNo + bTitle_lines;
		}
        
		// Bug 19497212: As part of Implementation of the SET EMBEDDED command.
		if (!(out instanceof StringBuffer)) {
			int nLnNo = 0;
			if (pageNo > 1) {
				nLnNo = lineNo - m_pagesize * (pageNo - 1);
			} else {
				nLnNo = lineNo;
			}
			pageNo = m_pagesize == 0 ? 0 : pageNo; 
			m_scriptRunnerContext.putProperty(ScriptRunnerContext.LNO, new Integer(nLnNo));
			m_scriptRunnerContext.putProperty(ScriptRunnerContext.PNO, new Integer(pageNo));
		}

		//max rows being spooled. nested rs -> you get all of internal rs. seems OK if warning printed - else print warning if new ctx
		if ((!maxRowsSpoolOnly(m_scriptRunnerContext))&&(cnt == s_maxRows && rset.next())) {
			if (errbuf == null) {
				logMaxReached(out);
			} else {
				logMaxReached(errbuf);
			}
		}
		//handle sqlplus: column ... new_val...
		if ((this.m_scriptRunnerContext != null)&&(newVal)) {
			if (cnt==0) {
				m_scriptRunnerContext.updateColumn(columns, null, null);
			} else {
				m_scriptRunnerContext.updateColumn(columns, lastVal, coldataTypes);
			}
		}

		if ((this.m_scriptRunnerContext != null)&&(oldVal)) {
			if (cnt==0) {
				m_scriptRunnerContext.updateOVColumn(columns, null, null);
			} else {
				m_scriptRunnerContext.updateOVColumn(columns, oldlVal, coldataTypes);
			}
		}
		
		return cnt;
	}
	
	private void appendDataToBuffer(StringBuffer buffer, AtomicInteger lineNo, AtomicInteger pageNo, StringBuffer data, int offset) {
		if(data != null && data.length() > 0) {
			String[] arrdata = data.toString().split(m_lineSeparator);
			for(String ad : arrdata) {
				if(lineNo.get() >= m_pagesize * pageNo.get() + offset) {
					pageNo.incrementAndGet();
					m_newpage = ((Integer) m_scriptRunnerContext.getProperty(ScriptRunnerContext.NEWPAGE)).intValue();
					for(int np = 0; np < m_newpage; np++) {
						buffer.append(m_lineSeparator);
						lineNo.incrementAndGet();
					}
				}
				buffer.append(ad);
				buffer.append(m_lineSeparator);
				lineNo.incrementAndGet();
			}
		}
	}

	/*
	 * helper method which returns a column size.
	 */
	private int getColumnSize(Connection conn, ResultSet rs, int colindex,
			HashMap<String, ArrayList<String>> storedColCmds)
					throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int colsize = rsmd.getColumnDisplaySize(colindex);
		// LRG/SRG for Object Types just get the column name from the ResultSetMetaData.
		String colName = rsmd.getColumnName(colindex).toLowerCase();
		int formatSize = -1;
		int headingSize = -1;
		int colType = rsmd.getColumnType(colindex);
		if (storedColCmds.containsKey(colName)) {
			ArrayList<String> ls = storedColCmds.get(colName);
			if (displayColumnAttributes(storedColCmds, colName)) {
				for (int k = 0; k < ls.size(); k++) {
					String colcmd = ls.get(k);
					String[] cmds = colcmd.split(" ");
					if (cmds[0].equalsIgnoreCase("format")) {
						if (cmds[cmds.length - 1] != null) {
							if ((cmds[cmds.length - 1].charAt(0) == 'a' || cmds[cmds.length - 1].charAt(0) == 'A')) {
								// This is specific to LRG/SRG tests when _12.1_longident flag is set.
								if(ls.contains("_12.1_longident")) {  
									if(colsize == 128) {
									     formatSize = Integer.parseInt(cmds[cmds.length - 1].substring(1, cmds[cmds.length - 1].length()));
									}
								}
								else {
									formatSize = Integer.parseInt(	cmds[cmds.length - 1].substring(1, cmds[cmds.length - 1].length()));
								}
							}
							else {
								// Bug 19784418
								if (isNumericColumn(colType)) {
									String fmtstr = cmds[cmds.length - 1];
									formatSize = formattedNUMBERLength(fmtstr);
									String heading = getHeading(colName,
											storedColCmds);
									if (heading.length() > 0
											&& heading.length() >= formatSize) {
										formatSize = heading.length();
									} else {
										if (heading.length() == 0
												&& colName.length() > formatSize)
											formatSize = colName.length();
									}
								} 
								else
									formatSize = -1;
							}
						}
					}
					// Bug 25054189 if col command heading is defined then we need to use the 
					// size of the heading.
					String heading = getHeading(colName, storedColCmds);
					if (heading.length() > 0 && heading.length() >= formatSize) {
						headingSize = heading.length();
				     }
				}
			}
		}
		
		// This is specific to LRG/SRG tests.
		HashMap<String, ArrayList<String>> prodStoredcolCmds = m_scriptRunnerContext.getStoredFormatCmds();
		if (prodStoredcolCmds != storedColCmds) {
			if (prodStoredcolCmds.containsKey(colName)) {
				formatSize = getColumnSize(conn, rs, colindex, prodStoredcolCmds);
			}
		}
		

		// Apply default column size.
		int displaySize = colsize;
		// Bug 19784521
		if (isNumericColumn(colType)) {
			int size = rsmd.getPrecision(colindex);
			// Bug 14733141 make sure to check for set numformat and numwidth
			// commands first
			if (m_numformat.length() > 0) {
				if (m_numformat.length() <= 3) {
					 displaySize = 4;
				}
				else
					// displaySize = m_numformat.length() + 1;
					// Bug 23081364 - DASHES NUMBER FOR SET NUMFORMAT IS DIFFERENT FROM THAT OF SQL*PLUS 
					if(m_numformat.matches("9+")) {
						displaySize = m_numformat.length() + 1;
					}
					else {
					    displaySize = m_numformat.length();
					}
			} else if (m_numwidth > 1) {
				// Bug 26639667 Modifies this fix.
				// Bug 24969761 & 24971418
				// Bugs 21145390 & 21477992
				// ResultSetMetaData.getColumnDisplaySize(..) returns
				// unusually large display size for Numeric types
				// so adjusting the display size so the minimum length is
				// atleast the "numwidth". If the column is an expression then adjust
				// the size to the length of the expression.
				// we just ignore the displaySize value and take the
				// length of the column/expression name.
				int colExprLength = getColumnName(rs, colindex).length();
				displaySize = colExprLength;
				if (displaySize <= m_numwidth)
					displaySize = m_numwidth;

			} else if (size == 126) // default precision for Bug 14594565
				displaySize = colsize;
			else {
				// Bug 14733141
				if (size == 0) {
					// let's get data size.
					NUMBER num = ((OracleResultSet) rs).getNUMBER(colindex);
					size = NUMBER.toString(num.toBytes()).length();
				}
				displaySize = size > getColumnName(rs, colindex).length() ? size
					: getColumnName(rs, colindex).length();
				
				
			}
		}
		else if(colType == -8)/*OracleTypes.ROWID*/ {
			// default for ROWID type
			displaySize = 18;
		}
		else if(colType == -10)/*OracleTypes.CURSOR*/ {
			// default for CURSOR
			displaySize = 20;
		}

		// Bug 21304531
		int intvlsize = getIntervalTypeDisplaySize(conn, rs, colindex, colType);
		int dtsize = getDateTimeDisplaySize(conn, rs, colindex);
		displaySize = intvlsize == 0 ? dtsize == 0 ? displaySize : dtsize : intvlsize;		

				if (rsmd.getColumnType(colindex) == -3) {
					displaySize = displaySize * 2;// VARBINARY which is encoded to 2
					// characters per byte bug
					// 5390196
				}

				if (isLongColumn(colType)) {
					displaySize = m_long;
				}

				int max=4000;
				if ((this.m_scriptRunnerContext!=null)
						&&(this.m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETLONG)!=null)
						&&((Integer)this.m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETLONG)>4000)){
					max=(Integer)this.m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETLONG);
				}
				if (displaySize==0) {//nulls Bug 19442242
					displaySize=1;
				}
				// FIXME: Raghu. Temp fix for 5904607
				if (displaySize > max || displaySize <= 0)
					displaySize = max;

				// Bug 12664179 for long columns.
				if(isLongColumn(colType)) {
					if(m_linesize <= displaySize) {
						colsize = m_linesize;
					}
					else {
						colsize = displaySize;
					}
					
					// LRG/SRG fix the Header size Max. is 80.
					if(colsize > 80) {
						colsize = 80;
					}
					//use long chunk if it is greater than colsize.
					if ((m_longchunk>colsize)) {
						colsize=m_longchunk;
					}
					
				}
				else if(displaySize > colsize) {
					// This might be a date/time column here in that case we ignore(i.e., dtsize > 0) this condition.
					if(dtsize == 0 && displaySize < getColumnName(rs, colindex).length() && colType != -10) {
						colsize = getColumnName(rs, colindex).length();
					}else {
						colsize = displaySize;
					}
				}

				// column format command is used for this column.
				// overrides whatever value "displaySize" variable has set.
				if (formatSize != -1) {
					colsize = formatSize;
				}
				// Bug 25054123 consider use the heading size from col command.
				else if(headingSize != -1 && headingSize > colsize) {
					colsize = headingSize;
				}

				// Bug 17698595
				// Sometimes the display size we get from
				// ResultSetMetaData.getColumnDisplaySize()
				// for some datatypes especially those returned from function calls
				// can be greater than the linesize set by the user (for e.g.,
				// "set linesize ###")
				// in that case we take the linesize set by the user to be the colsize.
				// This also applies to column format command who size could also be
				// greater
				// than the user set linesize
				if (m_linesize > 0 && colsize > m_linesize && !isLongColumn(colType)) {
					colsize = m_linesize;
				}

				// The default for numeric types NUMWIDTH size is 10 in the absence of column formatting.
				// The Original Bug 19665641 fix breaks the Compute code and also Truncates the column names
				// for numeric types when column formatting is NOT used, so added some more checks
				// to handle Numeric types. Always column formatting takes higher priority when deciding column sizes.
//				if(isNumericColumn(colType) && rsmd.getColumnName(colindex).length() <= displaySize && formatSize < 0)
				if(isNumericColumn(colType) && getColumnName(rs, colindex).length() <= displaySize && formatSize < 0 && headingSize < 0)    
					return displaySize;
				else
					return colsize;
	}

	/*
	 * helper method for a column command
	 */
	private String getHeading(String colName,
			HashMap<String, ArrayList<String>> storedColCmds) {
		String heading = "";
		if (displayColumnAttributes(storedColCmds, colName)
				&& storedColCmds.containsKey(colName)) {
			ArrayList<String> ls = storedColCmds.get(colName);
			// Bug 25054189
			for (int k = 0; k < ls.size(); k++) {
				String hdgcmd = ls.get(k);
				if(hdgcmd.indexOf("heading") > -1) {
					heading = hdgcmd.replaceFirst("heading ", "");
					break;
				}
			}
		}
		return heading;
	}

	private boolean hasMultiLineHeaders(
			HashMap<String, ArrayList<String>> storedColCmds,
			ResultSet rs) throws IOException, SQLException {
	        ResultSetMetaData rsmd = rs.getMetaData();
		for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
//			String colName = rsmd.getColumnName(i).toLowerCase();
			String colName = getColumnName(rs, i).toLowerCase();
			ArrayList<String> colcmds = storedColCmds.get(colName);
			if (colcmds != null) {
				for (int j = 0; j < colcmds.size(); j++) {
					if (j == 0 && colcmds.get(j).matches("(?i:off)")) {
						break;
					}
					if (colcmds.get(j).matches("(?i:heading\\s+.+)")) {
						String[] cmds = colcmds.get(j).split(" ");
						for (int k = 0; k < cmds.length; k++) {
							if (cmds[k].indexOf(m_headsepchar) > -1) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	private void convertToMultiLineHeader(int colsize, int colindex,
			String colheading, String[] columns) {
		if (!colheading.equals("")
				&& ((String) (m_scriptRunnerContext
						.getProperty(ScriptRunnerContext.SETHEADSEP)))
						.equalsIgnoreCase("on")) {
			columns[colindex] = "";
			String rexp = "[\\" + m_headsepchar + "]";
			// Bug 21477111
			// the code checks for pattern and matcher may not be necessary.
			Pattern pattern = Pattern.compile(rexp);
			Matcher  matcher = pattern.matcher(colheading);
			int count = 0;
			while (matcher.find())
				count++;
			String[] tokens = colheading.split(rexp);
			if (count > 0 && tokens.length > 0) {
				String ltcolheading = colheading.replaceAll(rexp, m_lineSeparator);
				columns[colindex] = ltcolheading;
			}
			else {
				// Bug 23220805
				if(colheading.length() > colsize)
				    columns[colindex] = colheading.substring(0, colsize);
				else
					columns[colindex] = colheading;
			}
		} else {
			if (colheading.length() > 0) {
				String hdr = colheading;
				if (hdr.length() > colsize) {
					hdr = hdr.substring(0, colsize);
				}
				columns[colindex] = hdr;
			}
		}
	}

	/*
	 * Mimics sqlplus way of displaying multi-line column header. If a column
	 * heading command is applied to a particular column, the entire row of
	 * column headers get affected.
	 */
	private StringBuffer multilineHeader(ResultSet rs, HashMap<String, ArrayList<String>> storedColsCmds,
			Integer[] colsizes, String[] columns, String[] originalColumns, boolean printCheck)
			throws SQLException {
		StringBuffer header = new StringBuffer("");
		boolean bdone = false;
		boolean multiline = false;
		ResultSetMetaData rsmd = rs.getMetaData();
		ArrayList<StringBuffer> mlheader = new ArrayList<StringBuffer>();

		for (int i = 0; i < columns.length; i++) {
			mlheader.add(i, null);
		}

		while (bdone == false) {
			bdone = true;
			multiline = false;
			for (int i = 1; i < columns.length; i++) {
				if (columns[i] == null)
					continue;
				// String colName = rsmd.getColumnName(i).toLowerCase();
				String colName = getColumnName(rs, i).toLowerCase();
				if (printCheck && !printColumn(storedColsCmds, colName))
					continue;
				StringBuffer nextChunk = null;
				if (mlheader.get(i) == null) {
					mlheader.add(i, new StringBuffer(columns[i]));
					nextChunk = new StringBuffer(columns[i]);
				} else {
					nextChunk = mlheader.get(i);
				}
				int idx = nextChunk.indexOf(m_lineSeparator);
				if (idx != -1) {
					multiline = true;
					StringBuffer temp = nextChunk;
					nextChunk = new StringBuffer(nextChunk.substring(0, idx));
					if (temp.length() - 1 > idx) {
						temp = new StringBuffer(temp.substring(idx + m_lineSeparator.length()));
					} else {
						temp = new StringBuffer("");
					}
					mlheader.set(i, temp);
					bdone = false;
				} else {
					mlheader.set(i, new StringBuffer(""));
				}

				font_width_adjustment(nextChunk, colsizes[i]);
				int colType = -99;
				if (columns == originalColumns) {
					colType = rsmd.getColumnType(i);
				} else {
					for (int kk = 1; kk < originalColumns.length; kk++) {
						if (columns[i].equals(originalColumns[kk])) {
							colType = rsmd.getColumnType(kk);
							break;
						}
					}
				}
				if (colType == 2) {
					nextChunk = new StringBuffer(
							String.format("%1$" + nextChunk.toString().length() + "s", nextChunk.toString().trim()));
				} else {
					nextChunk = new StringBuffer(justifyHeaders(storedColsCmds,
							// rsmd.getColumnName(i).toLowerCase(),
							getColumnName(rs, i).toLowerCase(), nextChunk.toString().trim(),
							nextChunk.toString().length()));
				}
				if (i < columns.length - 1)
					nextChunk.append(m_colsep);
				header.append(nextChunk.toString());
			}

			if (multiline) {
				header.append(m_lineSeparator);
			}
		}

		int lines = header.toString().split(m_lineSeparator).length;
		if (lines > 1) {
			int ltIdx = m_lineSeparator.length() > 1 ? header.indexOf(m_lineSeparator) + 1
					: header.indexOf(m_lineSeparator);
			int hdrLineLen = ltIdx + 1;
			for (int k = 1; k < columns.length; k++) {
				if (columns[k] == null)
					continue;
				String colName = getColumnName(rs, k).toLowerCase();
				if (printCheck && !printColumn(storedColsCmds, colName))
					continue;
				if (columns[k].indexOf(m_lineSeparator) == -1) {
					int idx = header.indexOf(columns[k]);
					if (idx != -1) {
						String blankStr = "";
						blankStr = String.format("%1$-" + new Integer(columns[k].length()).toString() + "s", "");
						header.replace(idx, idx + columns[k].length(), blankStr);
						// Bug 21477111 column names aren't being aligned with the header lines
						// and rest of the data.
						int start = 0;
						int end = 0;
						int l = lines - 1;
						// Bug 24679934 making sure that column headers appear at the expected index.
						start = idx + (l * hdrLineLen);
						end = start + columns[k].length();
						header.replace(start, end, columns[k]);
					}
				} else if (isMultipleMultilineColumns(columns)) {
					String[] strs = columns[k].split(m_lineSeparator, -1);
					int collines = strs.length;
					// Bugs 23095700 & 24679934
					if (collines < lines) {
						int startLine = lines - collines;
						int fromIdx = -1;
						int idx = -1;
						HashMap<Integer, Integer> idx_to_stIdx = new HashMap<Integer, Integer>();
						// Bug 24679934
						// replace existing strings with blanks at the existing index
						// and recalculate the index based on which line the string will be placed.
						for (int k1 = 0; k1 < collines; k1++) {
							String str = strs[k1];
							// Bug 24679934 find the idx value from header.
							idx = header.indexOf(str, fromIdx);
							if (idx >= 0 && str.length() > 0) {
								String blankStr = String.format("%1$-" + new Integer(str.length()).toString() + "s",
										"");
								header.replace(idx, idx + str.length(), blankStr);
								// Bug 24679934 this is the "line" we start.
								int line = startLine + k1;
								int diff = 0;
								if (idx > ltIdx) {
									diff = idx - ltIdx - 1;
								} else {
									diff = idx;
								}
								while (diff > ltIdx) {
									diff = diff - ltIdx - 1;
								}
								int start = diff + (line * hdrLineLen);
								int end = start + str.length();
								idx_to_stIdx.put(k1, start);
							}
						}

						for (int k1 = 0; k1 < collines; k1++) {
							String str = strs[k1];
							if (str.length() > 0) {
								int start = idx_to_stIdx.get(k1);
								if (start < header.toString().length())
									header.replace(start, start + str.length(), str);
							}
						}

					}
				}

			}
		}
		return header;
	}

	private final boolean isMultipleMultilineColumns(String[] columns) {
		boolean gtOne = false;
		int count = 0;
		for (int i = 1; i < columns.length; i++) {
			int idx = columns[i].indexOf(m_lineSeparator);
			if (idx > -1)
				count++;
		}
		gtOne = count > 1 ? true : false;
		return gtOne;
	}

	/**
	 * moved functionality to utility file -  pass in m_long if used,
	 * @param dataType
	 * @param colsize
	 * @param val
	 * @return
	 */
	private StringBuffer wordwrap(int dataType, int colsize, StringBuffer val) {
		return ScriptUtils.wordwrap(dataType, colsize, val, m_long);
	}

	private StringBuffer truncate(int dataType, int colsize, StringBuffer val) {
		return ScriptUtils.truncate(dataType, colsize, val);
	}

	private StringBuffer wrap(int dataType, int colsize, StringBuffer val) {
		return ScriptUtils.wrap(dataType, colsize, val);
	}
	
	private StringBuffer xmlWordWrap(int colsize, StringBuffer val) {
		return ScriptUtils.xmlWordWrap(colsize, val, m_long);
	}

	private void font_width_adjustment(StringBuffer str,
			int colsize) {
		int spacePixelWidth =-1;
		if (spacePixelWidthMode2==-1) {
			spacePixelWidthMode2 = ResultSetFormatter.getPixelSize(" ", 2); //$NON-NLS-1$
		}
		spacePixelWidth=spacePixelWidthMode2;
		
		// pixelwidth calculated for a particular font due to
		// unicode width issues
		int pixelWidth = ResultSetFormatter.getPixelSize(str.toString(), 2);
		if (pixelWidth < (colsize * spacePixelWidth)) {
			int pixelsRequired = (colsize * spacePixelWidth) - pixelWidth;
			int spacesRequired = pixelsRequired / spacePixelWidth;
			str.append(justify_left(" ", spacesRequired));
		}
	}

	private String justify_left(String str, int width) {
		if (width<=0){
			return "";
		}
		return String.format("%1$-" + width + "s", str);
	}

	private String justify_right(String str, int width) {
		if (width<=0){
			return "";
		}
		return String.format("%1$" + width + "s", str);
	}

	private String justify_center(String str, int width) {
		String val = str.trim();
		if (width < val.length())
			return str;
		int lenR = (width - val.length()) / 2 + val.length();
		// Justify Right
		String strR = String.format("%1$" + lenR + "s", val);
		// Justify Left
		String ret = String.format("%1$-" + width + "s", strR);
		return ret;
	}

	private String justifyHeaders(
			HashMap<String, ArrayList<String>> storedColCmds, String column,
			String colName, int colsize) /* throws IOException, SQLException */{
		ArrayList<String> colcmds = storedColCmds.get(column);
		if (colcmds != null  && displayColumnAttributes(storedColCmds, column)) {
			for (int j = 0; j < colcmds.size(); j++) {
				if (colcmds.get(j).equalsIgnoreCase("justify right")) {
					return justify_right(colName, colsize);
				} else if (colcmds.get(j).equalsIgnoreCase("justify center")) {
					return justify_center(colName, colsize);
				} else {
					if (colcmds.get(j).toLowerCase().startsWith("justify"))
						return justify_left(colName, colsize);
				}
			}
		}
		return justify_left(colName, colsize);
	}

	private boolean displayColumnAttributes(
			HashMap<String, ArrayList<String>> storedColCmds, String column) {
		ArrayList<String> colcmds = storedColCmds.get(column);
		if (colcmds != null) {
			for (int j = 0; j < colcmds.size(); j++) {
				if (j == 0
						&& (colcmds.get(j).equalsIgnoreCase("off") || colcmds
								.get(j).startsWith("off"))) {
					return false;
				} else {
					return true;
				}
			}
		}
		return true;
	}

	private StringBuffer checkFormatOptions(ResultSet rs,
			int colidx, HashMap<String, ArrayList<String>> storedColsCmds,
			Integer[] colsizes, ArrayList<StringBuffer> cacheRow)
					throws SQLException {
		StringBuffer buf = new StringBuffer("");
		ResultSetMetaData rmeta = rs.getMetaData();
		int colType = rmeta.getColumnType(colidx);
		String colName = "";
		// Bug 24954448 For oracle.sql.STRUCT and oracle.sql.ARRAY the column command defined
		// uses is a simple column name instead of expanded column name used for UDTs.
		if(colType == 2002/*oracle.sql.STRUCT*/ || colType == 2003 /*oracle.sql.ARRAY*/ ) {
			colName = rmeta.getColumnName(colidx).toLowerCase();
		}
		else {
		    colName = getColumnName(rs, colidx).toLowerCase();
		}
		boolean isLongType = isLongColumn(colType);
		boolean bwrapOverride = false;
		if (storedColsCmds.containsKey(colName)) {
			ArrayList<String> ls = storedColsCmds.get(colName);

			// Bug 21113900 handle long types here.
			StringBuffer dataChunk = new StringBuffer();
			if(isLongType) {
				dataChunk = truncate(colType, m_long, cacheRow.get(colidx));
			}
			else {
				dataChunk = cacheRow.get(colidx);
			}


			for (int k = 0; k < ls.size(); k++) {
				String colcmd = ls.get(k);
				if (k == 0
						&& (colcmd.equalsIgnoreCase("off") || colcmd
								.startsWith("off"))) {
					break;
				}
				if (colcmd.startsWith("heading") || colcmd.startsWith("format")
						|| colcmd.startsWith("justify"))
					continue;

				if (colcmd.indexOf("truncated") > -1) {
					buf = truncate(colType, colsizes[colidx], dataChunk);
					bwrapOverride = true;
				} else if (colcmd.indexOf("word_wrapped") > -1) {
					buf = wordwrap(colType, colsizes[colidx],
							dataChunk);
					bwrapOverride = true;
				} else if (colcmd.indexOf("wrapped") > -1) {
					buf = wrap(colType, colsizes[colidx], dataChunk);
					bwrapOverride = true;
				} else if (colcmd.indexOf("null") > -1) {
					if (dataChunk.toString() == null
							|| dataChunk.toString().equals("")
							|| dataChunk.toString().equals(" ")) {
						// Bug 24684120
						String nullstr = colcmd.replaceFirst("null\\s*", "");
						buf = new StringBuffer(nullstr);
					}
				} else {
					if (!bwrapOverride) {
						if (m_wrap.equalsIgnoreCase("ON"))
							buf = wrap(colType, colsizes[colidx], dataChunk);
						else
							buf = truncate(colType, colsizes[colidx], dataChunk);
					}
				}
			}
		}
		return buf;
	}

	private boolean printColumn(
			HashMap<String, ArrayList<String>> storedColCmds, String column) {
		ArrayList<String> colcmds = storedColCmds.get(column);
		if (colcmds != null ) {
			for (int j = 0; j < colcmds.size(); j++) {
				if (colcmds.get(j).equalsIgnoreCase("off"))
					return true;
				if (colcmds.get(j).equalsIgnoreCase("noprint"))
					return false;
			}
		}
		return true;
	}

	/*
	 * private String getNLSDateFormat(Connection conn) { String sql =
	 * "SELECT property_value FROM database_properties WHERE property_name = 'NLS_DATE_FORMAT'"
	 * ; String datefmt = ""; ResultSetWrapper rswrapper = null; if(conn !=
	 * null) { try { DBUtil dbutil = DBUtil.getInstance(conn); rswrapper =
	 * dbutil.executeOracleQuery(sql, null); ResultSet rset =
	 * rswrapper.getResultSet(); while (rset.next()) { datefmt =
	 * rset.getString(1); } } catch(SQLException ex) {
	 * 
	 * } finally { if (rswrapper != null) { rswrapper.close(); } } } return
	 * datefmt; }
	 */
	
	/*
	 *  This function is a workaround for the Regression test failures. Not a perfect fix.
	 *  The return display size may fall short if the Mask DAY(Name of Day) and 
	 *  MONTH(Name of Month) are used.
	 */
	private int getDateTimeDisplaySize(Connection conn, ResultSet rs, int colindex) {
		int dataLength = 0;
		String maskFromNLS = "";
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			String dataType = rsmd.getColumnTypeName(colindex);
			int colType = rsmd.getColumnType(colindex); 
//			OracleNLSProvider onlsp = new OracleNLSProvider(conn);
			OracleNLSProvider onlsp = (OracleNLSProvider) NLSProvider.getProvider(conn);
			if (dataType.equalsIgnoreCase("date")) {
				DATE date = ((OracleResultSet) rs).getDATE(colindex);
				maskFromNLS = onlsp.getDateFormat();
				if( m_dtformat_to_colsize.get(maskFromNLS.toUpperCase()) != null)
				    dataLength = m_dtformat_to_colsize.get(maskFromNLS.toUpperCase()).intValue();
				if(dataLength == 0) {
					String val = onlsp.format(date);
					if(val != null)
					     dataLength = val.length();
					dataLength = dataLength == 0 ? 
							           getDateTimeDisplaySize(conn, 91) == 0 ? 
							           getDateTimeDisplaySize(conn, 93) == 0 ? 
							           dataLength : getDateTimeDisplaySize(conn, 93) : getDateTimeDisplaySize(conn, 91) : dataLength;
				}
			} else if (dataType.equalsIgnoreCase("timestamp")) {
				TIMESTAMP ts = ((OracleResultSet) rs).getTIMESTAMP(colindex);
				maskFromNLS = onlsp.getTimeStampFormat();
				String val = onlsp.format(ts);
				if(val != null)
				     dataLength = val.length();
				dataLength = dataLength == 0 ? getDateTimeDisplaySize(conn, 93) : dataLength;
			} else if (dataType.equalsIgnoreCase("timestamp with local time zone")) {
				TIMESTAMPLTZ tltz = ((OracleResultSet) rs).getTIMESTAMPLTZ(colindex);
				maskFromNLS = onlsp.getTimeStampWithLocalTimeZoneFormat();
				String val = onlsp.format(tltz);
				if(val != null)
				     dataLength = val.length();
				dataLength = dataLength == 0 ? getDateTimeDisplaySize(conn, colType) : dataLength;
			} else if (dataType.equalsIgnoreCase("timestamp with time zone")) {
				TIMESTAMPTZ tstz = ((OracleResultSet) rs).getTIMESTAMPTZ(colindex);
				maskFromNLS = onlsp.getTimeStampWithTimeZoneFormat();
				String val = onlsp.format(tstz);
				if(val != null)
				    dataLength = val.length();
				dataLength = dataLength == 0 ? getDateTimeDisplaySize(conn, colType) : dataLength;
			} else {
				dataLength = getDateTimeDisplaySize(conn, colType);
			}
			
			if(maskFromNLS.matches("(?i:D(L|S)\\s+TS)")) {
				// The length of the return string is determined as follows:
				// For AMERICAS the format starts with 'fmDay, Month dd, yyyy' followed by the timestamp
				// took the Max. length of the day which is Wednesday and
				// Max. length of the Month which is September
				dataLength = 41;
			}
		} catch (Exception ex) {
		}

		return dataLength;
	}

	private int getDateTimeDisplaySize(Connection conn, int colType) {
		if (m_nls_formats.containsKey(colType)) {
			// If we've already looked this up, return the cached value
			return m_nls_formats.get(colType);
		} else {
			String maskFromNLS = "";
			if (colType == 92) {/* OracleTypes.TIME */
				// I do not think this is used much - hence not adding it to
				// NLScache (and using a large value)
				maskFromNLS = "HH.MI.SSXFF am";
			} else if (colType == 91 /* OracleTypes.DATE */ || colType == 93 /* OracleTypes.TIMESTAMP */) {
				// Bug 21462482
				// The ResultSetMetaData for Oracle Date Type column always returns 93 which is of Timestamp type.
				// Since Timestamp is the extension of Date type modified the code here.
				maskFromNLS = NLSProvider.getProvider(conn).getTimeStampFormat();
				String dmaskFromNLS = NLSProvider.getProvider(conn).getDateFormat();
				if(dmaskFromNLS.matches("(?i:D(L|S)\\s+TS)") || maskFromNLS.matches("(?i:D(L|S)\\s+TS)")) {
					// The length of the return string is determined as follows:
					// For AMERICAS the format starts with 'fmDay, Month dd, yyyy' followed by the timestamp
					// took the Max. length of the day which is Wednesday and
					// Max. length of the Month which is September
					m_nls_formats.put(colType, 42);
					return 42;
				}

			} else if (colType == -101) {/* OracleTypes.TIMESTAMPTZ */
				maskFromNLS = NLSProvider.getProvider(conn)
						.getTimeStampWithTimeZoneFormat();
			} else if (colType == -102) {/* for OracleTypes.TIMESTAMPLTZ */
				m_nls_formats.put(colType, 35);
				return 35;
			}

			int fmtLength = queryFormatLength(conn, maskFromNLS);
			m_nls_formats.put(colType, fmtLength);
			return fmtLength;
		}
	}

	private int getIntervalTypeDisplaySize(Connection conn, ResultSet rs, int colindex, int colType) {
		if (m_nls_formats.containsKey(colType)) {
			// If we've already looked this up, return the cached value
			return m_nls_formats.get(colType);
		}
		else {
			try {
				String maskFromNLS = "";
				OracleNLSProvider onlsp = (OracleNLSProvider)NLSProvider.getProvider(conn);
				if(colType == -103 || colType == -104) {/* for OracleTypes.INTERVALYM or OracleTypes.INTERVALDS*/
					maskFromNLS = onlsp.format(ScriptUtils.getOracleObjectWrap((OracleResultSet)rs,colindex));
					int fmtLength = queryFormatLength(conn, maskFromNLS);
					m_nls_formats.put(colType, fmtLength);
					return fmtLength;
				}
			}
			catch(Exception ex) {
			}
		}
		return 0;
	}

	/**
	 * Runs a query for a sql statement. This is used specifically for getting
	 * formats from v$nls_parameters.
	 * 
	 * @param conn
	 *            to use
	 * @param maskFromNLS
	 *            the mask retreived from nls or ""
	 * @return the length of the format taking into account fifferences between
	 *         mask and eventual data length
	 */
	private int queryFormatLength(final Connection conn,
			final String maskFromNLS) {
		String fmt = "";
		if (maskFromNLS!=null &&!maskFromNLS.equals("")) {
			fmt = maskFromNLS.toLowerCase();
		}
		// for dates we are just getting the length of the format, which is ok
		// for most
		// however, for Fractional Seconds (xff) and Timezone offset (tzr) this
		// is not true
		// hence the need to adjust it.
		int adjustment = 0;
		if (fmt.contains("xff")) {
			adjustment += 7;
		}
		if (fmt.contains("hh24")) {
			adjustment -= 2;
		}
		if (fmt.contains("tzr")) {
			adjustment += 3;
		}

		if(fmt.matches("(?i:D(L|S)\\s+TS)")) {
			// Bug 21462482
			// The length of the return string is determined as follows:
			// For AMERICAS the format starts with 'fmDay, Month dd, yyyy' followed by the timestamp
			// took the Max. length of the day which is Wednesday and
			// Max. length of the Month which is September
			return 41;
		}
		return fmt.length() + adjustment;
	}
	
	/**
	 *  Maps the Date/Time Format masks to predefined column sizes so we can match with SQLPlus
	 *  column sizes for Date/Time Datatypes.
	 *  The OJDBC ResultSetMetaData.getColumnDisplaySize(..) function doesn't give valid 
	 *  sizes for Date/Time Datatypes. In some cases the column sizes are smaller than the 
	 *  data type values. This causes alignment problems when printing out the Report.
	 *  
	 *   The keys will continue to grow as we run into different DT formats.
	 */
	private void populateDTFmtsToColsizes() {
		m_dtformat_to_colsize.put("DD-MON-RR",  9);
		m_dtformat_to_colsize.put("DD/MON/RR",  9);
		m_dtformat_to_colsize.put("DD.MON.RR",  9);
		m_dtformat_to_colsize.put("DD;MON;RR",  9);
		m_dtformat_to_colsize.put("DD:MON:RR",  9);
		m_dtformat_to_colsize.put("DDMONRR", 7);
		m_dtformat_to_colsize.put("MM-DD-YYYY", 10);
		m_dtformat_to_colsize.put("MM/DD/YYYY", 10);
		m_dtformat_to_colsize.put("MM.DD.YYYY", 10);
		m_dtformat_to_colsize.put("MM;DD;YYYY", 10);
		m_dtformat_to_colsize.put("MM:DD:YYYY", 10);
		m_dtformat_to_colsize.put("MMDDYYYY", 8);
		m_dtformat_to_colsize.put("DD-MON-YYYY", 11);
		m_dtformat_to_colsize.put("DD/MON/YYYY", 11);
		m_dtformat_to_colsize.put("DD.MON.YYYY", 11);
		m_dtformat_to_colsize.put("DD;MON;YYYY", 11);
		m_dtformat_to_colsize.put("DDMONYYYY", 9);
		m_dtformat_to_colsize.put("DD-MM-YYYY", 10);
		m_dtformat_to_colsize.put("DD/MM/YYYY", 10);
		m_dtformat_to_colsize.put("DD.MM.YYYY", 10);
		m_dtformat_to_colsize.put("DD;MM;YYYY", 10);
		m_dtformat_to_colsize.put("DD:MM:YYYY", 10);
		m_dtformat_to_colsize.put("DDMMYYYY", 8);
		m_dtformat_to_colsize.put("DL", 42);
	}

	private String getFormattedNumber(Connection conn, NUMBER number,
			String fmtstr) {
		String retval = "";
		ResultSet rset = null;
		HashMap<String, Object> binds = new HashMap<String, Object>();
		binds.put("NUMVAL", number.stringValue());
		binds.put("FMT", fmtstr);

		String sql = "SELECT TO_CHAR(:NUMVAL, :FMT) FROM DUAL";

		if (conn != null && !sql.equals("")) {
			try {
				DBUtil dbutil = DBUtil.getInstance(conn);
				rset = dbutil.executeOracleQuery(sql, binds);
				while (rset.next()) {
					retval = rset.getString(1);
				}
			} catch (Exception ex) {
				// just return the original value without formatting.
				retval = DataTypesUtil.stringValue(number, conn);
			} finally {
				try {
					if (rset != null) {
						rset.close();
					}
				} catch (Exception ex) {
				}
			}
		}
		return retval;
	}

	/*
	 * if user m_pagesize values closer to 0 then this function emulates the
	 * sqlplus output.
	 * 
	 * if m_pagesize = 0 and titles are ON then only data is printed. Column
	 * headers are not printed.
	 * 
	 * if m_pagesize >= 1 and <= 4 and titles are ON then both titles and data
	 * are printed. Column headers are not printed.
	 * 
	 * if m_pagesize > 4 then everything is printed.
	 */
	private StringBuffer getTopTitleAndHeader(int page,
			StringBuffer header_lines, Map<String,String> subNewValueMap, Map<String,String> subOldValueMap, ArrayList<StringBuffer> row, String[] columns) {

		int tpLines = 0;
		int btmLines = 0;
		StringBuffer tpTitle = new StringBuffer("");
		StringBuffer btmTitle = new StringBuffer("");
		StringBuffer buf = new StringBuffer("");
		if (m_linesize > 0 && m_pagesize >= 0) {
			if (m_TopTitle != null && m_scriptRunnerContext.getTTitleFlag()
					&& m_pagesize > 0) {
				tpTitle = m_TopTitle.getTitle(m_scriptRunnerContext, page, subNewValueMap, subOldValueMap, row, columns, m_sql);
			}

			if (m_scriptRunnerContext.getTTitleFlag() /*
			 * || m_scriptRunnerContext.
			 * getBTitleFlag()
			 */) {
				// if(m_pagesize <= 4 ) {
				if (m_pagesize <= numberofLines(header_lines)) {
					// we have to print title & data
					buf.append(tpTitle);
				} else {
					buf.append(tpTitle);
					// if(tpTitle != null && tpTitle.length() > 0)
					// buf.append(tpTitle);
					int titleLines = numberofLines(tpTitle);
					int headerLines = numberofLines(header_lines);
					// int totLines = (titleLines - 1) + headerLines;
					int totLines = titleLines + headerLines;
					if (m_pagesize >= totLines)
						buf.append(header_lines);
				}
			} else {
				if (m_pagesize > 2 /* numberofLines(header_lines) */) {
					buf.append(header_lines);
				}
			}
		}
		return buf;
	}

	/*
	 * check comments for getTopTitleAndHeader(..)
	 */
	private StringBuffer getBottomTitle(int page, Map<String,String> subNewValue, Map<String,String> subOldValue, ArrayList<StringBuffer> row, String[] columns) {
		StringBuffer btmTitle = null;
		StringBuffer buf = new StringBuffer("");
		if (m_linesize > 0 && m_pagesize >= 0) {
			if (m_BottomTitle != null && m_scriptRunnerContext.getBTitleFlag()
					&& m_pagesize > 0) {
				btmTitle = m_BottomTitle.getTitle(m_scriptRunnerContext, page, subNewValue, subOldValue, row, columns, m_sql);
				String tmp = justify_left(" ", m_linesize);
				StringBuffer tmpbuf = new StringBuffer("");
				// *** 25087139:PAGINATION IS NOT CORRECTLY WITH BTITLE IN SQLCL
				// *** Extra lines are being added by the commented code below.
				//tmpbuf.append(tmp);
				//tmpbuf.append(m_lineSeparator);
				tmpbuf.append(btmTitle);
				btmTitle = tmpbuf;
			}

			if (m_scriptRunnerContext.getBTitleFlag()) {
				if (m_pagesize > 0) {
					// we have to print title & data
					if (btmTitle != null) {
						buf.append(btmTitle);	
					}
				}
			}
		}
		return buf;
	}

	private int getTotTitleLines() {
		int lines = 0;
		if (m_pagesize > 0) {
			if (m_scriptRunnerContext.getTTitleFlag()) {
				if (m_TopTitle != null) { 
					StringBuffer topTitleBuffer = m_TopTitle.getTitle(m_scriptRunnerContext, 1, null, null, null, null, m_sql);
					lines += numberofLines(topTitleBuffer);
				}
			}
			if (m_scriptRunnerContext.getBTitleFlag()) {
				if  (m_BottomTitle != null) {
					StringBuffer bottomTitleBuffer = m_BottomTitle.getTitle(m_scriptRunnerContext, 1, null, null, null, null, m_sql);
					lines += numberofLines(bottomTitleBuffer);
				}
			}
		}
		return lines;
	}

	private int getTopTitleLength() {
		int lines = 0;
		if (m_pagesize > 0) {
			if (m_scriptRunnerContext.getTTitleFlag()) {
				if (m_TopTitle != null) { 
					lines += numberofLines(m_TopTitle.getTitle(
						m_scriptRunnerContext, 1, null, null, null, null, m_sql));
				}
			}
		}
		return lines;
	}

	private Integer adjustToLineandPageSize(ResultSet rs,
			HashMap<String, ArrayList<String>> storedColsCmds,
			Integer[] colsizes, StringBuffer data, StringBuffer linedata,
			StringBuffer buf, Integer linenum) throws SQLException {
		int beginIdx = 0, endIdx = 0, startIdx = 0, idx = 0, lnendIdx = 0;
		int linesize = m_linesize;
		boolean bLastCol = false;
		boolean bLinedata = false;
		boolean bPrint = true;
		String str = "";
		int validLength = 0;
		String colsep = "\\Q" + m_colsep + "\\E";
		String[] columns = data.toString().split(colsep);
		String[] lines = linedata.toString().split(colsep);
		ResultSetMetaData rmeta = rs.getMetaData();
		if (data != null || data.length() > 0) {
			// Bug 20511433 
			// For unusual m_colsep lengths we check
			// whether we can fit two columns separated
			// by m_colsep with in a given m_linesize.
			// Calculate the validLength here. 
			if (m_columns.length > 2) {
				for (int i = 1; i < 3; i++) {
					// Bug 21482640 only check for printable columns.
					if(printColumn(storedColsCmds, getColumnName(rs, i).toLowerCase())) 
						validLength += colsizes[i];
					if (i < 2)
						validLength += m_colsep.length();
				}
			}

			// Bug 20511433 
			// if linesize not suitable to fit atleast
			// two columns separated by m_colsep then 
			// we eliminate the m_colsep.
			// Bug 21498889 
			// this is when console width is less than the line size.
			//			int consoleWidth = m_scriptRunnerContext.getSQLPlusConsoleReader() != null ? m_scriptRunnerContext.getSQLPlusConsoleReader().getTerminal().getWidth() : 0;
			//if SQL Developer Worksheet then set consoleWidth = 10,000
			int consoleWidth = 10000;
			if(m_scriptRunnerContext.isCommandLine()){
				try{
					//				  consoleWidth =  m_scriptRunnerContext.getSQLPlusConsoleReader().getTerminal().getWidth();
					consoleWidth = m_scriptRunnerContext.getSQLPlusConsoleReader() != null ? m_scriptRunnerContext.getSQLPlusConsoleReader().getWidth() : 0;
				} catch(Exception e){
					//ignore, if there is an exception then go with 10000. never want this to stop the results being printed
				}
			}
			
			boolean classic = false;
			if (m_scriptRunnerContext.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) !=null) 
				classic = Boolean.parseBoolean(m_scriptRunnerContext.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString());
		        
			if (!classic && linesize < validLength && consoleWidth < validLength && m_colsep.length() > 1 || (consoleWidth < linesize && m_columns.length < 3)) {
				for (int i = 1; i < m_formattedcols.length; i++) {
					// Bug 21264870
					if(m_formattedcols[i] != null) {
						buf.append(m_formattedcols[i].replaceAll("\\s+$", ""));
						buf.append(m_lineSeparator);
						linenum++;
						if(lines.length == m_formattedcols.length - 1 && lines[i - 1].length() > 0) {
							buf.append(lines[i - 1]);
							buf.append(m_lineSeparator);
							linenum++;
						}
					}
				}
			}
			else {
				boolean bHeaderDone = false; 
				for (int i = 1; i < colsizes.length; i++) {
					bPrint = printColumn(storedColsCmds, getColumnName(rs, i).toLowerCase());

					if (bPrint) {
						if (i == colsizes.length - 1) 
							linesize = linesize - colsizes[i];
						else
							linesize = linesize - (colsizes[i] + m_colsep.length());
					}
					else {
						if (i == colsizes.length - 1) {
							bLastCol = true;
							bHeaderDone = true;
						}
					}

					// if(linesize+(m_colsep.length()-(m_lineSeparator.length()))
					// <
					// 0 || bLastCol) {
					// The header should be done before we print out the header line.
					if(bHeaderDone && (linesize < 0 || bLastCol)) {	
						// 0 is OK if m_colsep.length = m_lineSeparator.length -
						// i.e. Linux and Mac
						// for example second last line is full length 20 char
						// when
						// line is 20 char
						int strIdx = buf.indexOf(str);
						if (strIdx > -1) {
							if (i > 1 && i <= colsizes.length - 1 && !bLastCol) {
								i--;
								str = replaceColSep(str);
								buf.replace(strIdx, strIdx + str.length(), str);
								// Bug 20511433 and 19520419
								// trim all the spaces at the end of the string.
								String tmp = buf.toString();
								tmp = tmp.replaceAll("\\s+$", "");
								buf.setLength(0);
								buf.append(tmp);

							}
							linenum++;
							if (linedata != null) {
								if ((buf != null) && (buf.length() != 0) && (m_colsep.equals(buf.substring(buf.length() - 1))) && !bLastCol) {
									buf.deleteCharAt(buf.length() - 1);
								}
								buf.append(m_lineSeparator);
								String str1 = "";
								// Bug 20511473 fix for header line which
								// happens to be a multiline.
								if (bPrint && linedata.lastIndexOf(m_colsep) > -1) {
									if (!bLastCol) {
										str1 = linedata.substring(idx, lnendIdx);
										str1 = replaceColSep(str1);
									}
									else
										str1 = linedata.substring(idx, lnendIdx);
								}
								// Bug 20623848
								else if (bLastCol) {
									// Bug 20785906 this case happens when column formatting option 
									// "noprint" is set.
									if(linedata.length() > lnendIdx)
										str1 = linedata.substring(idx, lnendIdx);
									else 
										str1 = linedata.substring(idx, linedata.length());
								}
								try {
									if (startIdx < m_linesize * linenum) {
										startIdx = m_linesize * linenum;
									}
									else if (startIdx == m_linesize * linenum) {
										startIdx++;
									}

									buf.append(str1);
									// Bug 20511433 and 19520419
									// trim all the spaces at the end of the
									// string.
									String tmp = buf.toString();
									tmp = tmp.replaceAll("\\s+$", "");
									buf.setLength(0);
									buf.append(tmp);
									buf.append(m_lineSeparator);
									bLinedata = true;
									startIdx = startIdx + str1.length();
									idx = endIdx + 1;
									linenum++;
								}
								catch (Exception ex) {
									String error = ex.getMessage();
								}
							}
							linesize = m_linesize;
							bHeaderDone = false;
						}
					}
					else {
						if (bPrint) {
						        // LRGSRG: Long identifier test fix. Identifier length can be longer than linesize. Use linesize 
							// as column size.
						        int colsize = colsizes[i] > m_linesize ? m_linesize : colsizes[i];
							endIdx = beginIdx + colsize;
							if (i == colsizes.length - 1) {
								if (endIdx > data.length())
									endIdx = data.length();
								str = data.substring(beginIdx, endIdx);
							}
							else
								str = data.substring(beginIdx, endIdx) + m_colsep;
							endIdx = beginIdx + str.length() - 1;


							if (startIdx < m_linesize * linenum) {
								startIdx = m_linesize * linenum;
							}

							buf.append(str);
							bLinedata = false;
							startIdx = startIdx + str.length();
							lnendIdx = endIdx + 1;
							beginIdx = endIdx + 1;
						}
						
						// Check to see whether there is room for another column 
						// on this line i.e., at this linenum. If there is room then 
						// we are NOT done with the Header.
						if(i < colsizes.length - 1 && linesize < colsizes[i+1])
							bHeaderDone = true;
						
						if (i == colsizes.length - 1) {
							if (bPrint)
								i--;
							bLastCol = true;
							bHeaderDone = true;
						}
					}
				}
			}
		}

		if (buf.length() > 0 && numberofLines(buf) == 1 && buf.indexOf(m_lineSeparator) == -1) {
			buf.append(m_lineSeparator);
		}
		return linenum;
	}

	private Integer adjustHeaderToLineandPageSize(ResultSet rs,
			HashMap<String, ArrayList<String>> storedColsCmds,
			Integer[] colsizes, String[] columns, StringBuffer buf,
			Integer linenum) throws SQLException {
		int beginIdx = 0, endIdx = 0, startIdx = 0, idx = 0;
		// int linesize = m_linesize;
		int linesize = m_delim_lnsize;
		boolean bLastCol = false;
		String[] colsLine = null;
		Integer[] colsizesLine = null;
		ArrayList<String> colList = null;
		ArrayList<Integer> colsizesList = null;
		StringBuffer header = new StringBuffer("");
		StringBuffer headerLine = new StringBuffer("");
		String lastColLine = "";
		
		ResultSetMetaData rmeta = rs.getMetaData();

		for (int i = 1; i < colsizes.length; i++) {
			if (!printColumn(storedColsCmds, getColumnName(rs, i)
					.toLowerCase()))
				continue;
			linesize = linesize - (colsizes[i] + m_colsep.length());
			// Bug 20504737
			if (linesize < 0 || bLastCol) {
				if (i <= colsizes.length - 1 && !bLastCol) {
					i--;
				}
				headerLine = new StringBuffer(headerLine.toString().trim());
				colsLine = new String[colList.size()];
				for (int ii = 0; ii < colList.size(); ii++) {
					colsLine[ii] = colList.get(ii);
				}
				colList = new ArrayList<String>();
				colList.add("");
				colsizesLine = new Integer[colsizesList.size()];
				for (int ii = 0; ii < colsizesList.size(); ii++) {
					colsizesLine[ii] = colsizesList.get(ii);
				}
				colsizesList = new ArrayList<Integer>();
				colsizesList.add(0);
				getFontMetrics();
				header = multilineHeader(rs, storedColsCmds, colsizesLine,
						colsLine, columns, false);
				String[] hdrstrs = header.toString().split(m_lineSeparator);
				for (int k = 0; k < hdrstrs.length; k++) {
					// startIdx = m_linesize*linenum;
					startIdx = m_delim_lnsize * linenum;
					String hdrln = hdrstrs[k];
					// buf.replace(startIdx, startIdx + hdrln.length(), hdrln);
					buf.append(hdrln);
					buf.append(m_lineSeparator);
					linenum++;
				}
				// startIdx = m_linesize*linenum;
				startIdx = m_delim_lnsize * linenum;
				// buf.replace(startIdx, startIdx +
				// headerLine.toString().length(), headerLine.toString());
				if (headerLine.substring(headerLine.length() - 1,
						headerLine.length()).equals(m_colsep)) {
					headerLine.delete(headerLine.length() - 1,
							headerLine.length());
				}
				buf.append(headerLine.toString());
				buf.append(m_lineSeparator);
				linenum++;
				headerLine = new StringBuffer("");
				// linesize = m_linesize;
				linesize = m_delim_lnsize;
			} else {
				if (colList == null) {
					colList = new ArrayList<String>();
					colList.add("");
				}
				if (colsizesList == null) {
					colsizesList = new ArrayList<Integer>();
					colsizesList.add(0);
				}
				colList.add(columns[i]);
				colsizesList.add(colsizes[i]);
				char[] chars = new char[colsizes[i]];
				Arrays.fill(chars, '-');
				lastColLine = String.copyValueOf(chars);
				headerLine.append(lastColLine);
				headerLine.append(m_colsep);
				if (i == colsizes.length - 1) {
					i--;
					bLastCol = true;
				}
			}
		}
		return linenum;
	}

	private String replaceColSep(String str) {
		String retval = "";
		int len = 0;
		if (str.length() > 0 && str.indexOf(m_colsep) > -1) {
			retval = str.replaceAll(Pattern.quote(m_colsep + "$"), "");
			for (int i = 0; i < m_colsep.length(); i++) {
				retval += " ";
			}
		}
		return retval;
	}

	private boolean isLongColumn(int colType) {
		if (colType == -1   /* OracleTypes.LONGVARCHAR */|| 
				colType == -13  /* OracleTypes.BFILE */||	
				colType == 2004 /* OracleTypes.BLOB */ ||
				colType == 2005 /* OracleTypes.CLOB*/ ||
				colType == 2007 /* OracleTypes.OPAQUE(SYS.XMLTYPE) */ || 
				colType == 2009 /* OracleTypes.SQLXML */ || 
				colType == 2011 /* OracleTypes.NCLOB */) {
			return true;
		}
		return false;
	}

	private boolean isNumericColumn(int colType) {
		if (colType == 2 /* OracleTypes.NUMBER or OracleTypes.NUMERIC */
				|| colType == 4 /* OracleTypes.INTEGER */|| colType == 6 /*
				 * OracleTypes
				 * .
				 * FLOAT
				 */
				|| colType == 100 /* OracleTypes.BINARY_FLOAT */|| colType == 101 /*
				 * OracleTypes
				 * .
				 * BINARY_DOUBLE
				 */) {
			return true;
		}
		return false;
	}

	/*
	 * If user sets a linesize using "set linesize" command, we have to decide
	 * whether the linesize is valid by comparing each of the column sizes, for
	 * a queried table. Column sizes could exceed upto a maximum of 4000 in
	 * length. If the linesize is less than a column size then the linesize set
	 * is not a valid one.
	 * 
	 * LRGSRG Fix: "lrgsrgd" has long identifier(> 30) tests that  
	 * have columns longer than the "linesize". SQLPlus shows results 
	 * as long as the linesize is 10 or more so had to modify. So this function 
	 * will return false if the linesize is below that value.
	 *   
	 */
	private boolean isAValidLinesize() {
		for (int i = 1; i < m_colsizes.length; i++) {
			if (m_colsizes[i] != null && m_linesize < m_colsizes[i] && m_linesize < 10) {
				return false;
			}
		}
		return true;
	}

	private int numberofLines(StringBuffer buf) {
		if (buf != null && buf.length() > 0)
			return new StringTokenizer(buf.toString(), m_lineSeparator)
		.countTokens();
		else
			return 0;
	}

	/*
	 * The pagesize needs to be adjusted if it is set too small by the
	 * "set pagesize" command.
	 */
	private Integer getAdjustedPagesize(StringBuffer hdr_lines, int titleLines) {
		// int numhdrLines = numberofLines(hdr_lines);
		//		int hdr_tpttl_lines = numberofLines(getTopTitleAndHeader(0, hdr_lines));
		int hdr_tpttl_lines = numberofLines(getTopTitleAndHeader(0, hdr_lines, null, null, null, null));
		int btm_ttl_lines = numberofLines(getBottomTitle(0, null, null, null, null));
		int lines = hdr_tpttl_lines + btm_ttl_lines;
		Integer adjPagesize = m_pagesize;
		if (m_pagesize > 0) {
			if (m_pagesize == 1) {
				adjPagesize = m_pagesize + 1 + titleLines;
			} else {
				if (m_pagesize > 1) {
					if (m_pagesize <= titleLines) {
						adjPagesize = 2 + titleLines;
					} else if (lines >= 2 && m_pagesize < lines + 2) { 
						// Bug 21039514 if the pagesize is set so that there is only 
						// room for title and column headers then we need to adjust
						// the page size to create room for data and space for the next 
						// row especially when the linesize is not enough to fit a row
						// of data.
						adjPagesize = 2 + lines;
					}
				}
			}
		}
		return adjPagesize;
	}

	private int strCount(String findStr, String str) {
		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			String strAti = str.substring(i, i + 1);
			if (findStr.equals(strAti)) {
				count++;
			}
		}
		return count;
	}

	static class RomanUtil {
		private static LinkedHashMap<Integer, String> m_int_to_roman = new LinkedHashMap<Integer, String>();

		private static String getRomanValue(Integer num) {
			if (num <= 3999 && num >= 1) {
				m_int_to_roman.put(1000, "M");
				m_int_to_roman.put(900, "CM");
				m_int_to_roman.put(500, "D");
				m_int_to_roman.put(400, "CD");
				m_int_to_roman.put(100, "C");
				m_int_to_roman.put(90, "XC");
				m_int_to_roman.put(50, "L");
				m_int_to_roman.put(40, "XL");
				m_int_to_roman.put(10, "X");
				m_int_to_roman.put(9, "IX");
				m_int_to_roman.put(5, "V");
				m_int_to_roman.put(4, "IV");
				m_int_to_roman.put(1, "I");
				Set<Map.Entry<Integer, String>> keys = m_int_to_roman
						.entrySet();
				StringBuffer result = new StringBuffer("");
				for (Entry<Integer, String> key : keys) {
					while (num >= key.getKey()) {
						num -= key.getKey();
						result.append(key.getValue());
					}
				}
				return result.toString();
			}
			return "";

		}

		private static String getRomanFmt(Integer num, int size) {
			String romanVal = getRomanValue(num);
			if (romanVal.length() > size || romanVal.equals("")) {
				char[] chars = new char[size];
				Arrays.fill(chars, '#');
				return String.copyValueOf(chars);
			}
			return String.format("%1$" + size + "s", romanVal);
		}
	}

	/*
	 * Determines the formatted column size for the oracle.sql.NUMBER types.
	 */
	private int formattedNUMBERLength(String fmtstr) {
		int tstInt = 10; // some random number.
		String s = "";
		int length = 0;
		NUMBER num = new NUMBER(tstInt);
		try {
			if(fmtstr.matches("(?i:tm)(?:|9|e|E)")) {
				// As per http://yourmachine.yourdomain/12/121/server.121/e18404/ch_twelve013.htm#BABBDHHE
				length = 64;
			}
			else if(fmtstr.matches("(?i:rn)")) {
				// for roman numerals setting the default.
				length = 15;
			}
			else {
				s = num.toFormattedText(fmtstr, null);
				length = s.length();
			}
		} catch (SQLException ex) {
			length = 0;
		}
		return length;
	}

	// NEW code. See some old related bugs 19665641 and 21550117. --sroychow 01/28/2016
	private String formattedFLOAT(Connection conn, float num, String fmtstr,
			int colsize) throws SQLException {

		boolean formatPreference = false;
		
		String formatted = null;
		String mask = "";

		// 1) handle Zero
		if ((num == 0) && ((fmtstr == null) || (fmtstr.length() == 0))) {
			return "0";
		}
		
		// Bug 24969761 & 24971418 Moved this code segment here so "numwidth" checks
		// are done here.
		int minsize = 7; // minimumsize required for floating point.
		char hashMarkChar = '#';
		if (colsize < minsize) {
			char[] chars = new char[colsize];
			Arrays.fill(chars, hashMarkChar);
			return String.valueOf(chars);
		}
		
		// 2) handle Inf 
		if ( Float.isInfinite(num) && ( (fmtstr == null) || (fmtstr.length() == 0)) ) {
			formatted = "Inf";
			if (num == Float.NEGATIVE_INFINITY) {
				formatted = "-" + formatted;
			}
			return formatted;
		}
		if ( Float.isInfinite(num) && ( (fmtstr != null) && (fmtstr.length() > 0)) ) {
			char[] chars = new char[fmtstr.length()];
			Arrays.fill(chars, hashMarkChar);
			formatted = String.valueOf(chars);
			if (num == Float.NEGATIVE_INFINITY) {
				formatted = "-" + formatted;
			}
			return formatted;
		}
		
		// Handle Nan separately.
		if ( Float.isNaN(num) && ((fmtstr == null) || (fmtstr.length() == 0)) ) {
			formatted = "Nan";
			return formatted;
		}
		if ( Float.isNaN(num) && ((fmtstr != null) && (fmtstr.length() > 0)) ) {
			char[] chars = new char[fmtstr.length()];
			Arrays.fill(chars, hashMarkChar);
			formatted = String.valueOf(chars);
			return formatted;
		}
		

		// 3) handle regular decimal number.
		if (colsize >= minsize)  {
			
			// create maske based on fmtstr..
			if ( (fmtstr == null) || (fmtstr.length() == 0) ) {
				mask = "0.0E000{0}";
			}
			else {
				// extract mask
				if (fmtstr.contains("E") && fmtstr.contains(".")) {
					String subFmtStr = (fmtstr.replace("E","")).trim();
					String[] parts = subFmtStr.split("\\.");
					int decimalPlaces = parts[1].length();
					int integralPlaces = parts[0].length();
					String decimalMask = "0";
					for (int i = 1; i < decimalPlaces; i++) {
						decimalMask = decimalMask + "0";
					}
					// zero is handled differently in SQLPLUS compared to non-zero numbers.
					if (num == 0) {
						mask = "." + decimalMask +"E00{0}";
					}
					else {
						mask = "0." + decimalMask +"E00{0}";
					}
					formatPreference = true;
				}
				else {
					mask = "0.0E000{0}";
				}
			}			
			
			// colsize = numWidth, and
			int numHashMarks = colsize - 7; // 1 for each character 0, \. , E,
											// 0, 0, 0, (+|-) for exponent.
			
			//Bug 21680784 - IEEE BINARY_FLOAT NUMBERS IN A VARRAY ARE NOT DISPLAY IN THE EXPECTED FORMAT -sroychow 07/15/2016
			if (colsize <= 10) {
				numHashMarks = colsize - 8;
			}
			
			// For negative numbers.
			if (num < 0) {
				numHashMarks = colsize - 8; // 1 for each character minus sign,
											// 0, \. , E, 0, 0, 0, (+|-) for
											// exponent
				if (numHashMarks == 0) {
					char[] chars = new char[colsize];
					Arrays.fill(chars, hashMarkChar);
					return String.valueOf(chars);
				}
			}

			// Another rule. numHashMarks cannot be more than 8.
			// SQL> set numw 20
			// SQL> select c1bf from stafii04 order by c3;
			// C1BF
			// --------------------
			// 1.11109996E+000
			// 1.77769995E+000
			// -2.29999995E+000
			if (numHashMarks > 8) {
				numHashMarks = 8;
			}

			String maskFiller = "";
			if (numHashMarks >= 0) {
				// Bug 25073496
				if(Math.signum(num) < 0) {
					// to accommodate -ve sign.
					if(numHashMarks > 0)
					    numHashMarks--;
				}
				char[] chars = new char[numHashMarks];
				Arrays.fill(chars, hashMarkChar);
				maskFiller = String.valueOf(chars);
			}
			else {
				maskFiller = new String() + hashMarkChar;
			}
			
			String pattern = null;
			if (formatPreference) {
				// Do not use the maskFiller for numformat.
				pattern = MessageFormat.format(mask, "");
			}
			else {
				pattern = MessageFormat.format(mask, maskFiller);
			}
			
			DecimalFormat formatter = new DecimalFormat(pattern);
			String s = formatter.format(num); // 2,147484E9
			String[] as = s.split("E");
			String exponentSign = "+";
			if (as[1].startsWith("-")) {
				exponentSign = "";
			}
			formatted = as[0] + "E" + exponentSign + as[1];
		}
		return formatted;
	}

	private String formattedNUMBER(Connection conn, NUMBER num, String fmtstr,
			int colsize) throws SQLException {
		String s = "";
		try {
			s = num.toFormattedText(fmtstr, null);
		} catch (SQLException ex) {
			if (ex.getMessage().equalsIgnoreCase("Unimplemented method called")) { //$NON-NLS-1$
				if (fmtstr.length() == 2
						&& (fmtstr.startsWith("r") || fmtstr.startsWith("R"))) {
					// Bug 13027341
					int intval = num.ceil().intValue();
					s = RomanUtil.getRomanFmt(intval, colsize);
					if (Character.isLowerCase(fmtstr.charAt(0))) {
						s = s.toLowerCase();
					}
				} else if (fmtstr.startsWith("x") || fmtstr.startsWith("X")) {
					int intval = num.ceil().intValue();
					// Bug 13027394
					if (intval < 0) { // SQL*PLUS doesn't convert negative
						// values to hexadecimal.
						char[] chars = new char[fmtstr.length() + 1];
						Arrays.fill(chars, '#');
						s = String.copyValueOf(chars);
					} else {
						// round to a nearest value first.
						if (Character.isLowerCase(fmtstr.charAt(0))) {
							s = String.format("%x", num.ceil()
									.bigIntegerValue());
						} else {
							s = String.format("%X", num.ceil()
									.bigIntegerValue());
						}
					}
				}
			} else if (ex.getMessage()
					.equalsIgnoreCase("Invalid Oracle Number")) { //$NON-NLS-1$
				char[] chars = new char[formattedNUMBERLength(fmtstr)];
				Arrays.fill(chars, '#');
				s = String.copyValueOf(chars);
			} else {
				s = DataTypesUtil.stringValue(num, conn);
			}
		}

		return s;
	}

	// Adding NEW code for formatted Double --sroychow 02/23/2017
	private String formattedDouble(Connection conn, double num, String fmtstr,
			int colsize) throws SQLException {

		String formatted = null;
		String mask = "";

		
		// We add a hack here. Set colsize = m_numwidth; we do not know why colsize 
		// has a different value than m_numwidth. E.g., colsize = 8 while n_numwidth=3.
		// It shouldn't be like this. 
		colsize = m_numwidth;
		
		
		// 1) handle Zero
		if ((num == 0) && ((fmtstr == null) || (fmtstr.length() == 0))) {
			return "0";
		}
		
		int minsize = 8; // minimumsize required for floating point.
		char hashMarkChar = '#';
		char zeroMarkChar = '0';
		if (colsize < minsize) {
			char[] chars = new char[colsize];
			Arrays.fill(chars, hashMarkChar);
			return String.valueOf(chars);
		}
		
		// 2) handle Inf 
		if ( Double.isInfinite(num) && ( (fmtstr == null) || (fmtstr.length() == 0)) ) {
			formatted = "Inf";
			if (num == Double.NEGATIVE_INFINITY) {
				formatted = "-" + formatted;
			}
			return formatted;
		}
		if ( Double.isInfinite(num) && ( (fmtstr != null) && (fmtstr.length() > 0)) ) {
			char[] chars = new char[fmtstr.length()];
			Arrays.fill(chars, hashMarkChar);
			formatted = String.valueOf(chars);
			if (num == Double.NEGATIVE_INFINITY) {
				formatted = "-" + formatted;
			}
			return formatted;
		}
		
		// Handle Nan separately.
		if ( Double.isNaN(num) && ((fmtstr == null) || (fmtstr.length() == 0)) ) {
			formatted = "Nan";
			return formatted;
		}
		if ( Double.isNaN(num) && ((fmtstr != null) && (fmtstr.length() > 0)) ) {
			char[] chars = new char[fmtstr.length()];
			Arrays.fill(chars, hashMarkChar);
			formatted = String.valueOf(chars);
			return formatted;
		}
		

		// 3) handle regular decimal number.
		if (colsize >= minsize)  {
						
			int numZeroMarks = 0;          // only for decimal more than 16. 
			// colsize = numWidth
			int numHashMarks = colsize - 7; // 1 for each character 0, \. , E,
											// 0, 0, 0, (+|-) for exponent.
			
			if (colsize <= 10) {
				numHashMarks = colsize - 8;
			}
			
			// For negative numbers.
			if (num < 0) {
				numHashMarks = colsize - 8; // 1 for each character minus sign,
											// 0, \. , E, 0, 0, 0, (+|-) for
											// exponent
				if (numHashMarks == 0) {
					char[] chars = new char[colsize];
					Arrays.fill(chars, hashMarkChar);
					return String.valueOf(chars);
				}
			}

			// Another rule. numHashMarks cannot be more than 8.
			// SQL> set numw 20
			// SQL> select c1bf from stafii04 order by c3;
			// C1BF
			// --------------------
			// 1.11109996E+000
			// 1.77769995E+000
			// -2.29999995E+000
			if (numHashMarks > 8 && numHashMarks < 16) {
				numHashMarks = 8;
			}
			// SQL> set numwidth 23
			// SQL> select * from t2;
            // C2BD
            // -----------------------
            // 2.2219999999999999E+001
            // 2.2879999999999999E+001
            // 3.3333300000000003E+002
            // 3.3399900000000002E+002
			if (numHashMarks > 16) {
				numHashMarks = 16;
				numZeroMarks = 16;
			}

			
			// create mask based on fmtstr..
			if ( (fmtstr == null) || (fmtstr.length() == 0) ) {
				if (numHashMarks == 16) {
				   mask = "0.0{0}E000{1}";
				}
				else {
					mask = "0.0E000{0}";
				}
			}
			else {
				// extract mask
				if (fmtstr.contains("E") && fmtstr.contains(".")) {
					String subFmtStr = (fmtstr.replace("E","")).trim();
					String[] parts = subFmtStr.split("\\.");
					int decimalPlaces = parts[1].length();
					int integralPlaces = parts[0].length();
					String decimalMask = "0";
					for (int i = 1; i < decimalPlaces; i++) {
						decimalMask = decimalMask + "0";
					}
					mask = "0." + decimalMask +"E00{0}";	
				}
				else {
					mask = "0.0E000{0}";
				}
			}
			
			String hashMaskFiller = "";
			String zeroMaskFiller = "";
			String decimalPattern = "";
			if (numHashMarks >= 0 && numHashMarks < 16) {
				if(Math.signum(num) < 0) {
					// to accommodate -ve sign.
					if(numHashMarks > 0)
					    numHashMarks--;
				}
				char[] hashChars = new char[numHashMarks];
				Arrays.fill(hashChars, hashMarkChar);
				hashMaskFiller = String.valueOf(hashChars);
				decimalPattern = MessageFormat.format(mask, hashMaskFiller);
			}
			else if ( numHashMarks == 16) {
				if(Math.signum(num) < 0) {
					// to accommodate -ve sign.
					if(numHashMarks > 0)
					    numHashMarks--;
				}
				
				char[] hashChars = new char[numHashMarks];
				Arrays.fill(hashChars, hashMarkChar);
				hashMaskFiller = String.valueOf(hashChars);
				
				char[] zeroChars = new char[numZeroMarks-1];
				Arrays.fill(zeroChars, zeroMarkChar);
				zeroMaskFiller = String.valueOf(zeroChars);
				
				decimalPattern = MessageFormat.format(mask, zeroMaskFiller, hashMaskFiller);
			}
			else {
				hashMaskFiller = new String() + hashMarkChar;
				decimalPattern = MessageFormat.format(mask, hashMaskFiller);
			}
			
			DecimalFormat formatter = new DecimalFormat(decimalPattern);
			formatter.setRoundingMode(RoundingMode.HALF_DOWN);
			String s = formatter.format(num); // 2,147484E9
			String[] as = s.split("E");
			String exponentSign = "+";
			if (as[1].startsWith("-")) {
				exponentSign = "";
			}
			formatted = as[0] + "E" + exponentSign + as[1];
		}
		return formatted;
	}
	
	private boolean isDataCached(ArrayList<StringBuffer> data) {
		boolean retval = false;
		for (int i = 1; i < m_columns.length; i++) {
			StringBuffer col = data.get(i);
			if (col != null && !col.toString().equals(DONE)) {
				retval = true;
				break;
			}
		}
		return retval;
	}

	/**
	 * 
	 * @param storedBrkCmds
	 * @param rmeta
	 * @param cacheRow
	 * @param brkColValues
	 * @param newPage
	 * @return
	 * @throws SQLException
	 * 
	 */
	private boolean hasDuplicates(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds,
			ResultSet rs, ArrayList<StringBuffer> cacheRow,
			ArrayDeque<String[]> brkColValues, boolean newPage)
					throws SQLException {
		boolean noDuplicate = false;
		ResultSetMetaData rmeta = rs.getMetaData();
		String[] rowValue = null;
		if (newPage) {
			brkColValues.clear();
		}

		// fixes for bugs 20301126 & 20308000
		ArrayList<String> cols = new ArrayList<String>();
		for (int k = 1; k < m_columns.length; k++) {
			if(m_columns[k] != null) {
				String columnName = m_column_to_brkcol.get(m_columns[k]);
				if (columnName != null && m_brkcol_to_nodup.get(columnName) != null) {
					// Bug 23047676 checking for numeric types.
					int colType = rmeta.getColumnType(k);
					if(isNumericColumn(colType)) {
						Datum n = null;
						try {
							n = ScriptUtils.getOracleObjectWrap((OracleResultSet) rs,k);
						}
						catch(Exception e) {
						}
						String nStr = n != null ? n.stringValue() : "";
						cols.add(nStr);
					}
					else 
					  cols.add(cacheRow.get(k).toString());
				}
			}
		}

		int size = cols.size();
		if(size > 0) {
			rowValue = new String[size];
			cols.toArray(rowValue);
		}

		if (rowValue != null) {
			if (brkColValues.size() == 0) {
				brkColValues.addFirst(rowValue);
				noDuplicate = false;
			} else {
				if (findBreakColValues(brkColValues, rowValue)) {
					// Bug 21690585
					if (!brkColValues.peekFirst()[0].equals("Nan") && Arrays.equals(brkColValues.peekFirst(), rowValue)) {
//					if (Arrays.equals(brkColValues.peekFirst(), rowValue)) {
						noDuplicate = true;
					} else {
						String[] foundRow = findBreakRow(brkColValues, rowValue);
						if (foundRow != null) {
							brkColValues.remove(foundRow);
							brkColValues.addFirst(rowValue);
							noDuplicate = false;
						}
					}
				} else {
					brkColValues.addFirst(rowValue);
				}
			}
		}
		return noDuplicate;
	}

	/*
	private boolean hasDuplicates(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds,
			ResultSetMetaData rmeta, ArrayList<StringBuffer> cacheRow,
			ArrayDeque<String[]> brkColValues, boolean newPage)
			throws SQLException {
		boolean noDuplicate = false;
		String[] rowValue = null;
		if (newPage) {
			brkColValues.clear();
		}

		// fixes for bugs 20301126 & 20308000
		ArrayList<String> cols = new ArrayList<String>();
		for (int k = 1; k < cacheRow.size(); k++) {
			String columnName = getBreakColumn(storedBrkCmds,
					rmeta.getColumnName(k));
			ArrayList<String> actions = storedBrkCmds.get(columnName);
			boolean hasNoDup = false;
			if (actions != null) {
				for (int i = 0; i < actions.size(); i++) {
					String action = actions.get(i);
					if (action.equalsIgnoreCase("nodup")) {
						hasNoDup = true;
						cols.add(cacheRow.get(k).toString());
						break;
					}
				}
			}
		}

		rowValue = new String[cols.size()];
		cols.toArray(rowValue);

		if (rowValue != null) {
			if (brkColValues.size() == 0) {
				brkColValues.addFirst(rowValue);
				noDuplicate = false;
			} else {
				if (findBreakColValues(brkColValues, rowValue)) {
					if (Arrays.equals(brkColValues.peekFirst(), rowValue)) {
						noDuplicate = true;
					} else {
						String[] foundRow = findBreakRow(brkColValues, rowValue);
						if (foundRow != null) {
							brkColValues.remove(foundRow);
							brkColValues.addFirst(rowValue);
							noDuplicate = false;
						}
					}
				} else {
					brkColValues.addFirst(rowValue);
				}
			}
		}
		return noDuplicate;
	}
	 */

	/**
	 * 
	 * @param storedBrkCmds
	 * @param rmeta
	 * @param cacheRow
	 * @param brkColValues
	 * @param colId
	 * @param newPage
	 * @return
	 * @throws SQLException
	 * 
	 *             This function depends on the brkColValues that was populated
	 *             by the function call hasDuplicates(LinkedHashMap<String,
	 *             ArrayList<String>> storedBrkCmds, ResultSetMetaData rmeta,
	 *             ArrayList<StringBuffer> cacheRow, ArrayDeque<String[]>
	 *             brkColValues, boolean newPage) Break Column order is
	 *             important here as this will determine if the action is
	 *             "nodup" when combined with other Break columns. The priority
	 *             for break columns starts with outermost break first all the
	 *             way to innermost break at the end.
	 */
	private boolean hasDuplicates(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds,
			ResultSet rs, ArrayList<StringBuffer> cacheRow,
			ArrayDeque<String[]> brkColValues, int colId, boolean newPage)
					throws SQLException {
		boolean noDuplicate = false;
		String[] rowValue = null;
                
		ResultSetMetaData rmeta = rs.getMetaData();
//		String colName = rmeta.getColumnName(colId);
		String colName = getColumnName(rs, colId);
		// Bug 21489951
		int brkOrder = m_brkcol_to_rank.get(colName) != null ? m_brkcol_to_rank.get(colName) : 
			m_brkcol_to_rank.get(colName.toLowerCase()) != null ? m_brkcol_to_rank.get(colName.toLowerCase()) : -1;

			if (!newPage) {
				String[] prevBrkValues = brkColValues.toArray().length > 1 ? (String[]) brkColValues
						.toArray()[1] : null;

						//String[] prevBrkValues = brkColValues.toArray().length >= 1 ? (String[]) brkColValues
						//		.peekFirst() : null;
						if (brkOrder != -1 && prevBrkValues != null) {
							if (m_breakandselorderMatch) {
								for (int i = 1; i < brkOrder + 1; i++) {
									// Bug 20486273
									int k = i - 1;
									int selColId = getSelColIdFromBreakColId(k);
									if (selColId >= 1 && k < prevBrkValues.length) {
										String prevColVal = prevBrkValues[k];
										String currColVal = cacheRow.get(selColId)
												.toString();
										//if (!prevColVal.equals("Nan") && !currColVal.equals("Nan") && currColVal.equals(prevColVal)) {
										if (currColVal.equals(prevColVal)) {
											if (k == 0) {
												noDuplicate = true;
											} else {
												noDuplicate &= true;
											}
										} else {
											if (k == 0) {
												noDuplicate = false;
											} else {
												noDuplicate &= false;
											}
										}
									}
								}
							} else {
								for (int i = 1; i < brkOrder + 1; i++) {
									// Bug 20486273
									int k = i - 1;
									int selColId = getSelColIdFromBreakColId(k);
									if (selColId >= 1 && prevBrkValues.length >= selColId) {
										String prevColVal = prevBrkValues[selColId - 1]; // -1 is to point to the right column here.
										String currColVal = cacheRow.get(selColId)
												.toString();
										if (currColVal.equals(prevColVal)) {
											if (k == 0) {
												noDuplicate = true;
											} else {
												noDuplicate &= true;
											}
										} else {
											if (k == 0) {
												noDuplicate = false;
											} else {
												noDuplicate &= false;
											}
										}
									}
								}
							}
						}
			}
			return noDuplicate;
	}

	/*
	private boolean hasDuplicates(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds,
			ResultSetMetaData rmeta, ArrayList<StringBuffer> cacheRow,
			ArrayDeque<String[]> brkColValues, int colId, boolean newPage)
			throws SQLException {
		boolean noDuplicate = false;
		String[] rowValue = null;

		String colName = rmeta.getColumnName(colId);
		int brkOrder = getBreakOrder(storedBrkCmds, colName);

		if (!newPage) {
			String[] prevBrkValues = brkColValues.toArray().length > 1 ? (String[]) brkColValues
					.toArray()[1] : null;
			if (brkOrder != -1 && prevBrkValues != null) {
				if (breakColandSelColOrderMatch()) {
					for (int i = 1; i < brkOrder + 1; i++) {
						// Bug 20486273
						int k = i - 1;
						int selColId = getSelColIdFromBreakColId(k);
						if (selColId >= 1) {
							String prevColVal = prevBrkValues[k];
							String currColVal = cacheRow.get(selColId)
									.toString();
							if (currColVal.equals(prevColVal)) {
								if (k == 0) {
									noDuplicate = true;
								} else {
									noDuplicate &= true;
								}
							} else {
								if (k == 0) {
									noDuplicate = false;
								} else {
									noDuplicate &= false;
								}
							}
						}
					}
				} else {
					for (int i = 1; i < brkOrder + 1; i++) {
						// Bug 20486273
						int k = i - 1;
						int selColId = getSelColIdFromBreakColId(k);
						if (selColId >= 1 && prevBrkValues.length >= selColId) {
							String prevColVal = prevBrkValues[selColId - 1]; // -1
																				// is
																				// to
																				// point
																				// to
																				// the
																				// right
																				// column
																				// here.
							String currColVal = cacheRow.get(selColId)
									.toString();
							if (currColVal.equals(prevColVal)) {
								if (k == 0) {
									noDuplicate = true;
								} else {
									noDuplicate &= true;
								}
							} else {
								if (k == 0) {
									noDuplicate = false;
								} else {
									noDuplicate &= false;
								}
							}
						}
					}
				}
			}
		}
		return noDuplicate;
	}
	 */

	private boolean findBreakColValues(ArrayDeque<String[]> brkColValues,
			String[] rowValue) {
		boolean bFound = false;
		Iterator<String[]> iter = brkColValues.iterator();
		while (iter.hasNext()) {
			bFound = Arrays.equals(iter.next(), rowValue);
			if (bFound) {
				return bFound;
			}
		}
		return bFound;
	}

	private String[] findBreakRow(ArrayDeque<String[]> brkColValues,
			String[] rowValue) {
		String[] row = null;
		Iterator<String[]> iter = brkColValues.iterator();
		while (iter.hasNext()) {
			row = iter.next();
			boolean found = Arrays.equals(row, rowValue);
			if (found) {
				return row;
			}
		}
		return row;
	}

	private String getBreakColumn(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds,
			String colName) {
		String breakColumn = null;
		breakColumn = storedBrkCmds.containsKey(colName) ? colName : null;
		if (breakColumn == null) {
			Iterator<String> iter = storedBrkCmds.keySet().iterator();
			while (iter.hasNext()) {
				String col = iter.next();
				if (col.equalsIgnoreCase(colName)) {
					breakColumn = col;
					break;
				}
			}
		}
		return breakColumn;
	}

	private int getBreakOrder(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds,
			String colName) {
		int breakOrder = -1;
		ArrayList<String> brkCols = new ArrayList<String>();
		brkCols.addAll(storedBrkCmds.keySet());
		for (int i = 0; i < brkCols.size(); i++) {
			if (brkCols.get(i).equalsIgnoreCase(colName)) {
				// Bug 20486273
				breakOrder = i + 1;
				break;
			}
		}
		return breakOrder;
	}

	private int getSelColIdFromBreakColId(int brkColId) {
		ArrayList<String> breakColList = new ArrayList<String>();
		ArrayList<String> breakAllColList = new ArrayList<String>();
		int colId = 0;

		if (m_scriptRunnerContext.getStoredBreakCmds().size() > 0) {
			breakAllColList.addAll(m_scriptRunnerContext.getStoredBreakCmds()
					.keySet());
			breakColList.addAll(filterList(breakAllColList, "report"));
			ArrayList<String> selList = new ArrayList<String>();
//			selList.addAll(SqlRecognizer.getColumns(m_sql));
			selList.addAll(m_selColumnList);
			for (String selCol : selList) {
				colId++;
				// String breakCol = breakColList.get(brkColId);
				String breakCol = breakColList.size() > brkColId ? breakColList
						.get(brkColId) : null;
						String colandalias[] = selCol.split("\\s+");
						String alias = colandalias.length > 2 ? colandalias[2]
								: colandalias.length > 1 ? colandalias[1] : selCol;
								if (breakCol != null) {
									if (breakCol.equalsIgnoreCase(selCol)
											|| breakCol.equalsIgnoreCase(alias)) {
										break;
									}
								}
			}
		}

		if (colId == 0) {
			colId = -1;
		}

		return colId;
	}

	private int skippedLines(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds,
			String colName) {
		int lines = 0;
		String SKIP1 = "(?i:\\bski(?:|p)\\b)\\s+\\d+";
		ArrayList<String> actions = storedBrkCmds.get(colName);
		if (actions != null) {
			for (int i = 0; i < actions.size(); i++) {
				String action = actions.get(i);
				if (action.matches(SKIP1)) {
					String lineStr = action.replaceAll(
							"(?i:\\bski(?:|p)\\b)\\s+", "");
					lines = new Integer(lineStr).intValue();
					break;
				}
			}
		}
		return lines;
	}

	private int skippedLines(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds) {
		int lines = 0;
		String SKIP1 = "(?i:\\bski(?:|p)\\b)\\s+\\d+";
		Iterator<String> iter = storedBrkCmds.keySet().iterator();
		ArrayList<String> actions = null;
		while (iter.hasNext()) {
			String val = iter.next();
			if (val.equalsIgnoreCase("row")) {
				actions = storedBrkCmds.get(val);
				for (int i = 0; i < actions.size(); i++) {
					String action = actions.get(i);
					if (action.matches(SKIP1)) {
						String lineStr = action.replaceAll(
								"(?i:\\bski(?:|p)\\b)\\s+", "");
						lines = new Integer(lineStr).intValue();
						break;
					}
				}
				break;
			}
		}
		return lines;
	}

	private int skipLinesForReport(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds) {
		int lines = 0;
		String SKIP1 = "(?i:\\bski(?:|p)\\b)\\s+\\d+";
		Iterator<String> iter = storedBrkCmds.keySet().iterator();
		ArrayList<String> actions = null;
		while (iter.hasNext()) {
			String val = iter.next();
			// Bug 21374726 removed for ROW.
			if (val.equalsIgnoreCase("report")) {
				actions = storedBrkCmds.get(val);
				for (int i = 0; i < actions.size(); i++) {
					String action = actions.get(i);
					if (action.matches(SKIP1)) {
						String lineStr = action.replaceAll(
								"(?i:\\bski(?:|p)\\b)\\s+", "");
						lines = new Integer(lineStr).intValue();
					}
					break;
				}
				break;
			}
		}
		return lines;
	}

	private int skippedPage(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds,
			String colName) {
		int page = 0;
		String SKIP2 = "((?i:\\bski(?:|p)\\b)\\s+)?(?i:\\bpage\\b)";
		ArrayList<String> actions = storedBrkCmds.get(colName);
		if (actions != null) {
			for (int i = 0; i < actions.size(); i++) {
				String action = actions.get(i);
				if (action.matches(SKIP2)) {
					page = 1;
					break;
				}
			}
		}
		return page;
	}

	private int skippedPage(
			LinkedHashMap<String, ArrayList<String>> storedBrkCmds) {
		int page = 0;
		String SKIP2 = "((?i:\\bski(?:|p)\\b)\\s+)?(?i:\\bpage\\b)";
		Iterator<String> iter = storedBrkCmds.keySet().iterator();
		ArrayList<String> actions = null;
		while (iter.hasNext()) {
			String val = iter.next();
			if (val.equalsIgnoreCase("row")) {
				actions = storedBrkCmds.get(val);
				for (int i = 0; i < actions.size(); i++) {
					String action = actions.get(i);
					if (action.matches(SKIP2)) {
						page = 1;
						break;
					}
				}
				break;
			}
		}
		return page;
	}

	/*
	 * This function is useful to figure out whether the query(sql) uses break
	 * and compute columns.
	 */
	public void setSQL(String sql) {
		m_sql = sql;
	}

	/**	
	 * Gets Rows for a particular group based on its key.
	 *  
	 * @param cmptd_rows
	 * @param cmptd_columns
	 * @param rs
	 * @param breakcolcmptkeys
	 * @return
	 * @throws SQLException
	 */
	private Object[][] getComputedRowsForGroup(
			HashMap<String, Object[][]> cmptd_rows,
			ArrayList<Object> cmptd_columns, ResultSet rs,
			String[] breakcolcmptkeys) throws SQLException {
		Object[][] grpCmptdRows = null;
		Object[][] comp_rows = null;

		ArrayList<String> brkcolsnorep = filterList(m_brkcmptcolumns, "report");
		if (brkcolsnorep.size() > 0) {
			boolean bSumLinesAdded = false;
			for (int k = 0; k < breakcolcmptkeys.length; k++) {
				if(breakcolcmptkeys[k] != null && breakcolcmptkeys[k].equals("report")) continue;
				comp_rows = getEqualRowCountRows(cmptd_rows, cmptd_columns, (breakcolcmptkeys[k]));
				if (comp_rows != null) {
					String[] columns = m_brkcmpkey_to_cols.get(breakcolcmptkeys[k]);
					if(columns.length == 1) {
						String column = columns[0];
						if(column != null) {
							if(m_column_to_summaryLines.get(column) == null) {
								m_summaryLines = getSummaryLines(cmptd_rows, cmptd_columns, breakcolcmptkeys[k], column);
								m_column_to_summaryLines.put(column, m_summaryLines);
							}
							else {
								m_summaryLines = m_column_to_summaryLines.get(column);
							}
						}
						grpCmptdRows = addRow(grpCmptdRows, m_summaryLines);
						for (int i = 0; i < comp_rows.length; i++) {
						Object[] comp_row = comp_rows[i];
						Object[][] data = getComputedRowsForGroup(comp_row, cmptd_columns, breakcolcmptkeys[k], column);
							if(data != null) {
								for(int j = 0; j < data.length; j++) {
									Object[] cmptd_row = data[j];
									grpCmptdRows = addRow(grpCmptdRows, cmptd_row);
								}
							}
					    }
					}
					else {
						for(String column : columns) {
							if(column != null) {
								if(m_column_to_summaryLines.get(column) == null) {
									m_summaryLines = getSummaryLines(cmptd_rows, cmptd_columns, breakcolcmptkeys[k], column);
									m_column_to_summaryLines.put(column, m_summaryLines);
								}
								else {
									m_summaryLines = m_column_to_summaryLines.get(column);
								}
							}
							grpCmptdRows = addRow(grpCmptdRows, m_summaryLines);	
		
							for (int i = 0; i < comp_rows.length; i++) {
								Object[] comp_row = comp_rows[i];
								String cmpfn = "";
								if(m_column_to_brkcol.get(column) != null) {
									cmpfn = m_brkcol_to_cmpfn.get(m_column_to_brkcol.get(column));
								}
								if(cmpfn.equals(getFunction(comp_row, cmptd_columns))) {
									Object[][] data = getComputedRowsForGroup(comp_row, cmptd_columns, breakcolcmptkeys[k], column);
									if(data != null) {
										for(int j = 0; j < data.length; j++) {
											Object[] cmptd_row = data[j];
											grpCmptdRows = addRow(grpCmptdRows, cmptd_row);
										}
									}
								}
							}
						}
					}
					
					
				}


				if(cmptd_rows.get(breakcolcmptkeys[k]) != null && comp_rows != null && 
						cmptd_rows.get(breakcolcmptkeys[k]).length != comp_rows.length) {
					Object[][] ocrows = cmptd_rows.get(breakcolcmptkeys[k]);
					for(int x = 0; x < comp_rows.length; x++) {
						ocrows = removeRow(ocrows, x);
					}
					cmptd_rows.remove(breakcolcmptkeys[k]);
					cmptd_rows.put(breakcolcmptkeys[k], ocrows);
				}
				else {
					// Done with the key so remove it.
					cmptd_rows.remove(breakcolcmptkeys[k]);
				}
			}
		}

		if (m_brkcmptcolumns.get(m_brkcmptcolumns.size()-1).equalsIgnoreCase("report") && 
				cmptd_rows.size() >= 1 && grpCmptdRows == null) {
//			comp_rows = cmptd_rows.get("report");
			comp_rows = getEqualRowCountRows(cmptd_rows, cmptd_columns, "report");
			if (comp_rows != null) {
				grpCmptdRows = addRow(grpCmptdRows, getSummaryLines(cmptd_rows, cmptd_columns, "report", "report"));
				for (int i = 0; i < comp_rows.length; i++) {
					Object[] comp_row = comp_rows[i];
					Object[][] data = getComputedRowsForReport(comp_row, cmptd_columns, "report");
					if(data != null) {
						for(int j = 0; j < data.length; j++) {
							Object[] cmptd_row = data[j];
							grpCmptdRows = addRow(grpCmptdRows, cmptd_row);
						}
					}
				}
			}

			// Done with the key so remove it.
			cmptd_rows.remove("report");

			if(cmptd_rows.size() > 0) {
				cmptd_rows.clear();
			}

		}

		return grpCmptdRows;
	}


	/**
	 * Get Computed Rows based on key.
	 * 
	 * @param comp_row
	 * @param cmptd_columns
	 * @param breakcolcmptkeys
	 * @return
	 */
	private Object[][] getComputedRowsForGroup(Object[] comp_row,
			ArrayList<Object> cmptd_columns, String breakcolcmptkeys, String column) {
		Object[][] grpCmptdRows = null;
		String breakcol = column;
		if (comp_row != null && cmptd_columns.size() > 1 && breakcol != null) {
			String labelTxt = "";
			ArrayList<String> compCols = new ArrayList<String>();
			ArrayList<TreeMap<String, HashMap<String, String>>> mapList = null;
			int colCnt = m_columns.length;
			boolean isComputedReport = false;
			ArrayList<String> prevcmptdFuncs = new ArrayList<String>();

			mapList = m_scriptRunnerContext.getStoredComputeCmds().get(breakcol);
			if(mapList == null && m_column_to_brkcol.get(breakcol) != null) {
				mapList = m_scriptRunnerContext.getStoredComputeCmds().get(m_column_to_brkcol.get(breakcol));
			}

			if(mapList != null) {
				Compute.sort(mapList);
				Iterator<TreeMap<String, HashMap<String, String>>> cmpcolIter = null;
				cmpcolIter = mapList.iterator();
				// There can be multiple Compute columns.
				while (cmpcolIter.hasNext()) {
					TreeMap<String, HashMap<String, String>> cmpColKey = cmpcolIter.next();
					String cmpCol = cmpColKey.firstKey();
					String currColFunc = "";
					boolean bReferredFunc = false;
					Object[] cmp_cols = new Object[colCnt];
					for (int j = 1; j < colCnt; j++) {
						if (m_columns[j] != null) {
							String col = "";

							String brkcol = m_column_to_brkcmptcol.get(m_columns[j]);
							if (colCnt > 2 && breakcol != null && breakcol.equals(m_columns[j]) &&
								!m_column_to_brkcol.get(m_columns[j]).equalsIgnoreCase(m_column_to_cmpcol.get(m_columns[j])) 
								/*if the break column and compute column are the same we ignore the function label here */
								) {
								int idx = -1;
								if(m_column_to_cmpcol.containsKey(cmpCol) ||
										m_column_to_cmpcol.containsKey(getHeading(cmpCol, m_scriptRunnerContext.getStoredFormatCmds()))) {
									idx = cmptd_columns.indexOf(cmpCol.toUpperCase() + "_CMPFN")  > -1 ? cmptd_columns.indexOf(cmpCol.toUpperCase() + "_CMPFN") :
										  cmptd_columns.indexOf(cmpCol + "_CMPFN") > -1 ? cmptd_columns.indexOf(cmpCol + "_CMPFN") : 
										  cmptd_columns.indexOf(cmpCol.toLowerCase() + "_CMPFN");
								}
								else if(m_column_to_cmpcol.containsValue(cmpCol)) { // for expressions
									Iterator<String> keysIter = m_column_to_cmpcol.keySet().iterator();
									while(keysIter.hasNext()) {
										String key = keysIter.next();
										if(m_column_to_cmpcol.get(key).equals(cmpCol)) {
											idx = cmptd_columns.indexOf(key.toUpperCase() + "_CMPFN") > -1 ? cmptd_columns.indexOf(key.toUpperCase() + "_CMPFN") :
												  cmptd_columns.indexOf(key + "_CMPFN") > -1 ? cmptd_columns.indexOf(cmpCol + "_CMPFN") : 
												  cmptd_columns.indexOf(cmpCol.toLowerCase() + "_CMPFN");
											break;
										}
									}
								}

								if(idx > -1) {
									currColFunc = DataTypesUtil.stringValue(comp_row[idx+1], m_conn);
									if(currColFunc != null) {
										String tempf = currColFunc;
										if(currColFunc.length() > m_colsizes[j]) 
											tempf = currColFunc.substring(0, m_colsizes[j]);
										if(prevcmptdFuncs.contains(tempf)) {
											bReferredFunc = true;
											// Bug 21528592 looking for a missing computed value here.
											if(j == colCnt-1) {
												// we are here because the last column has the name of the function.
												int jIdx = 0;
												for(int x = 1; x < cmp_cols.length; x++) {
													if(cmp_cols[x] == null || cmp_cols[x] == "") {
														continue;
													}
													else {
														jIdx = x;
														break;
													}
												}

												if(jIdx > 0) {
													if (cmpCol.length() > 0 && m_column_to_cmpcol.get(m_columns[jIdx]) != null && 
															(m_column_to_cmpcol.get(m_columns[jIdx]).equalsIgnoreCase(cmpCol) || 
																	m_column_to_cmpcol.containsKey(getHeading(cmpCol, m_scriptRunnerContext.getStoredFormatCmds())))) {
														for (int k = 0; k < cmptd_columns.size(); k++) {
															String cmp_col = (String) cmptd_columns.get(k);
															// Bug 21690473
															// Sometimes computes are done on a column which happens to be a break column
															if (cmp_col.equalsIgnoreCase(cmpCol) || cmp_col.equalsIgnoreCase(m_columns[jIdx]) /*for expressions*/) {
																if(bReferredFunc && grpCmptdRows[grpCmptdRows.length-1][jIdx] == "") {
																	grpCmptdRows[grpCmptdRows.length-1][jIdx] = comp_row[k + 1];
																}
																break;
															}
														}
													}
												}
											}
										}
									}

									if(!bReferredFunc) {
										HashMap<String, String> fn_to_lbl = cmpColKey.get(cmpCol);
										if(fn_to_lbl != null && fn_to_lbl.containsKey(currColFunc)) {
											String temp = fn_to_lbl.get(currColFunc).replaceAll("'", "");
											if(temp.length() > m_colsizes[j]) 
												temp = temp.substring(0, m_colsizes[j]);
											cmp_cols[j] = temp;
										}
										else {
											if(currColFunc != null && currColFunc.length() > m_colsizes[j])
												currColFunc = currColFunc.substring(0, m_colsizes[j]);
											else if(currColFunc == null) {
												currColFunc = "";
											}
											cmp_cols[j] = currColFunc;
										}
									}
								}
							}
							else if (cmpCol.length() > 0 && m_column_to_cmpcol.get(m_columns[j]) != null && 
									(m_column_to_cmpcol.get(m_columns[j]).equalsIgnoreCase(cmpCol) || m_column_to_cmpcol.get(m_columns[j]).equalsIgnoreCase(getColumnFromAlias(cmpCol)) ||
											m_column_to_cmpcol.containsKey(getHeading(cmpCol, m_scriptRunnerContext.getStoredFormatCmds())))) {
								for (int k = 0; k < cmptd_columns.size(); k++) {
									String cmp_col = (String) cmptd_columns.get(k);
									// Bug 21690473
									// Sometimes computes are done on a column which happens to be a break column
									if(cmptd_columns.get(k+1) != null && cmp_col.equals((String) cmptd_columns.get(k+1))) {
										k++;
									}
									if (cmp_col.equalsIgnoreCase(cmpCol) || cmp_col.equalsIgnoreCase(m_columns[j]) /*for expressions*/) {
										if(bReferredFunc) {
											grpCmptdRows[grpCmptdRows.length-1][j] = comp_row[k + 1];
										}
										else {
											cmp_cols[j] = comp_row[k + 1];
										}
										break;
									}
								}
							}
							else {
								cmp_cols[j] = "";
							}
						}
					}
					if(!bReferredFunc) {
						prevcmptdFuncs.add(currColFunc);
						grpCmptdRows = addRow(grpCmptdRows, cmp_cols);
					}
				}
			}
		}
		return grpCmptdRows;
	}

	/**
	 * This is Computed rows for the Entire Report. 
	 * 
	 * @param comp_row
	 * @param cmptd_columns
	 * @param breakcolcmptkeys
	 * @return
	 */
	private Object[][] getComputedRowsForReport(Object[] comp_row,
			ArrayList<Object> cmptd_columns, String breakcolcmptkeys) {
		Object[][] grpCmptdRows = null;

		if (comp_row != null && cmptd_columns.size() > 1) {
			String labelTxt = "";
			ArrayList<String> compCols = new ArrayList<String>();
			ArrayList<TreeMap<String, HashMap<String, String>>> mapList = null;
			if(breakcolcmptkeys.startsWith("report")) {
				mapList = m_scriptRunnerContext.getStoredComputeCmds().get("report");
				Compute.sort(mapList);
			}

			boolean isLabelAllowed = !firstColumnAComputeColumn(mapList);

			int colCnt = m_columns.length;
			boolean isComputedReport = false;
			ArrayList<String> prevcmptdFuncs = new ArrayList<String>();

			Iterator<TreeMap<String, HashMap<String, String>>> cmpcolIter = mapList.iterator();

			// There can be multiple Compute columns.
			while (cmpcolIter.hasNext()) {
				TreeMap<String, HashMap<String, String>> cmpColKey = cmpcolIter.next();
				String cmpCol = cmpColKey.firstKey();
				String currColFunc = "";
				boolean bReferredFunc = false;
				Object[] cmp_cols = new Object[colCnt];
				for (int j = 1; j < colCnt; j++) {
					if (m_columns[j] != null) {
						String col = "";

						String brkcol = m_column_to_brkcmptcol.get(m_columns[j]);
						if (colCnt > 2 && m_brkcmpkey_to_cols.get(breakcolcmptkeys) != null && 
								m_brkcmpkey_to_cols.get(breakcolcmptkeys)[j].equals(m_columns[j])) {	
							labelTxt = m_brkcol_to_cmplbl.get(brkcol);
							cmp_cols[j] = labelTxt;
						}
						else if (colCnt > 2 && j == 1 && breakcolcmptkeys.startsWith("report")) {
							HashMap<String, String> func_to_lbl = m_brkcol_to_cmpfnlbl.get(breakcolcmptkeys);

							int idx = -1;
							String heading = getHeading(cmpCol, m_scriptRunnerContext.getStoredFormatCmds());
							if(m_column_to_cmpcol.containsKey(cmpCol) || (heading.length() > 0 && m_column_to_cmpcol.containsKey(heading))) {
								idx = cmptd_columns.indexOf(cmpCol.toUpperCase() + "_CMPFN");
							}
							else if(m_column_to_cmpcol.containsValue(cmpCol)) { // for expressions
								Iterator<String> keysIter = m_column_to_cmpcol.keySet().iterator();
								while(keysIter.hasNext()) {
									String key = keysIter.next();
									if(m_column_to_cmpcol.get(key).equals(cmpCol)) {
										idx = cmptd_columns.indexOf(key.toUpperCase() + "_CMPFN");
										break;
									}
								}
							}

							if(idx > -1) {
								currColFunc = DataTypesUtil.stringValue(comp_row[idx+1], m_conn);
								if(currColFunc != null) {
									String tempf = currColFunc;
									if(currColFunc.length() > m_colsizes[j]) 
										tempf = currColFunc.substring(0, m_colsizes[j]);
									if(prevcmptdFuncs.contains(tempf)) {
										bReferredFunc = true;
									}

									if(!bReferredFunc || j == 1) {
										if(isLabelAllowed) {
											HashMap<String, String> fn_to_lbl = cmpColKey.get(cmpCol);
											if(fn_to_lbl != null && fn_to_lbl.containsKey(currColFunc)) {
												String temp = fn_to_lbl.get(currColFunc).replaceAll("'", "");
												if(temp.length() > m_colsizes[j]) 
													temp = temp.substring(0, m_colsizes[j]);
													cmp_cols[j] = temp;
											}
											else {
												if(currColFunc.length() > m_colsizes[j])
													currColFunc = currColFunc.substring(0, m_colsizes[j]);
													cmp_cols[j] = currColFunc;
											}
										}
										// Bug 20487969
										else  {
											for (int k = 0; k < cmptd_columns.size(); k++) {
												String cmp_col = (String) cmptd_columns.get(k);
												for(int x = k+1; x < cmptd_columns.size(); x++) {
													if(cmp_col.equalsIgnoreCase((String)cmptd_columns.get(x))) {
														k = x;
														break;
													}
												}
												if (cmp_col.equalsIgnoreCase(cmpCol) && (m_column_to_cmpcol.get(m_columns[j]).equalsIgnoreCase(cmpCol) || 
													m_column_to_cmpcol.get(m_columns[j]).equalsIgnoreCase(getColumnFromAlias(cmpCol)))) {
													if(bReferredFunc) {
														grpCmptdRows[grpCmptdRows.length-1][j] = comp_row[k + 1];
													}
													else {
														cmp_cols[j] = comp_row[k + 1];
													}
													break;
												}
											}

										}
									}
								}
							}
						}
						else if (cmpCol.length() > 0 && m_column_to_cmpcol.get(m_columns[j]) != null && 
								(m_column_to_cmpcol.get(m_columns[j]).equalsIgnoreCase(cmpCol) || m_column_to_cmpcol.get(m_columns[j]).equalsIgnoreCase(getColumnFromAlias(cmpCol)) ||
										m_column_to_cmpcol.containsKey(getHeading(cmpCol, m_scriptRunnerContext.getStoredFormatCmds())))) {
							for (int k = 0; k < cmptd_columns.size(); k++) {
								String cmp_col = (String) cmptd_columns.get(k);
								// Bug 21690473
								// Sometimes computes are done on a column which happens to be a break column
								if(cmptd_columns.get(k+1) != null && cmp_col.equals((String) cmptd_columns.get(k+1))) {
									k++;
								}
								if (cmp_col.equalsIgnoreCase(cmpCol) || cmp_col.equalsIgnoreCase(m_columns[j]) /*for expressions*/) {
									if(bReferredFunc && grpCmptdRows != null) {
										grpCmptdRows[grpCmptdRows.length-1][j] = comp_row[k + 1];
									}
									else {
										cmp_cols[j] = comp_row[k + 1];
									}
									break;
								}
							}
						}
						else {
							cmp_cols[j] = "";
						}

					}
				}

				if(!bReferredFunc) {
					prevcmptdFuncs.add(currColFunc);
					boolean canAdd = false;
					for(int i = 1; i < cmp_cols.length; i++) {
						if(cmp_cols[i] != null && cmp_cols[i] != "") {
							canAdd = true;
							break;
						}
					}

					if(canAdd)
						grpCmptdRows = addRow(grpCmptdRows, cmp_cols);
				}
			}
		}
		return grpCmptdRows;
	}


	boolean firstColumnAComputeColumn(ArrayList<TreeMap<String, HashMap<String, String>>> mapList) {
		Iterator<TreeMap<String, HashMap<String, String>>> cmpcolIter = mapList.iterator();
		while (cmpcolIter.hasNext()) {
			TreeMap<String, HashMap<String, String>> cmpColKey = cmpcolIter.next();
			String cmpCol = cmpColKey.firstKey();
			for (int j = 1; j < m_columns.length; j++) {
				if (m_columns[j] != null) {
					if(j == 1 && m_column_to_cmpcol.get(m_columns[j]) != null && 
							(m_column_to_cmpcol.get(m_columns[j]).equalsIgnoreCase(cmpCol) || 
							m_column_to_cmpcol.get(m_columns[j]).equalsIgnoreCase(getColumnFromAlias(cmpCol)))) {
						return true;
					}
				}
			}
		}
		return false;
	}


	/**
	 * This will generate Summary Lines.
	 * 
	 * @param cmptd_rows
	 * @param cmptd_columns
	 * @param rowIndex
	 * @param report
	 * @return
	 */	
	private Object[] getSummaryLines(HashMap<String, Object[][]> cmptd_rows,
			ArrayList<Object> cmptd_columns, String breakcolcmptkeys,
			String column) {
		Object[][] cmptdRows = cmptd_rows.get(breakcolcmptkeys);
		int colCnt = m_columns.length;
		Object[] cmp_cols = new Object[colCnt];
		boolean empty_cmp_cols = true;
		String breakcol = column;
		if (cmptdRows != null && cmptd_rows.size() > 0 && cmptd_columns.size() > 0 && 
				(breakcol != null || breakcolcmptkeys.equalsIgnoreCase("report"))) {
			ArrayList<TreeMap<String, HashMap<String, String>>> mapList = null;
			if(breakcol != null)
				mapList = m_scriptRunnerContext.getStoredComputeCmds().get(breakcol);
			else if(breakcolcmptkeys.equalsIgnoreCase("report")) {
				mapList = m_scriptRunnerContext.getStoredComputeCmds().get("report");
			}
			if(mapList == null && m_column_to_brkcol.get(breakcol) != null) {
				mapList = m_scriptRunnerContext.getStoredComputeCmds().get(m_column_to_brkcol.get(breakcol));
			}

			if (mapList != null) {
				Iterator<TreeMap<String, HashMap<String, String>>> cmpcolIter = null;
				cmpcolIter = mapList.iterator();
				while (cmpcolIter.hasNext()) {
					TreeMap<String, HashMap<String, String>> cmpColKey = cmpcolIter.next();
					String cmpCol = cmpColKey.firstKey();
					// fill up with "***" and "----"
					for (int j = 1; j < colCnt; j++) {
						if (m_columns[j] != null) {
							String cmptColumn = "";

							if (colCnt > 2 && ((breakcol != null && breakcol.equals(m_columns[j]) && !column.equalsIgnoreCase("report") && 
								!m_column_to_brkcol.get(m_columns[j]).equalsIgnoreCase(m_column_to_cmpcol.get(m_columns[j]))) ||
								/* if the break column and compute column are the same we ignore the *'s here */
								(j == 1 && breakcolcmptkeys.equalsIgnoreCase("report") && m_column_to_brkcol.get(m_columns[j]) != null && 
								(m_breakcolumns.size() > 0 && m_brkcmptcolumns.size() == 1)))
								/*For Report we need to do additional checks to get the *'s */
							   ) {
								char[] chars = new char[m_colsizes[j]];
								Arrays.fill(chars, '*');
								cmp_cols[j] = String.copyValueOf(chars);
							}
							// else if(m_column_to_cmpcol.get(m_columns[j]) != null) {
							else if (cmpCol.length() > 0 && m_column_to_cmpcol.get(m_columns[j]) != null && 
									(m_column_to_cmpcol.get(m_columns[j]).equalsIgnoreCase(cmpCol) || m_column_to_cmpcol.get(m_columns[j]).equalsIgnoreCase(getColumnFromAlias(cmpCol)) || 
											m_column_to_cmpcol.containsKey(getHeading(cmpCol, m_scriptRunnerContext.getStoredFormatCmds())))) {
								char[] chars = new char[m_colsizes[j]];
								Arrays.fill(chars, '-');
								cmp_cols[j] = String.copyValueOf(chars);
							}
							else {
								String val = cmp_cols[j] != null ? (String)cmp_cols[j] : null;
								if(val == null || !val.matches("(-)+"))
									cmp_cols[j] = String.format("%1$-" + new Integer(m_colsizes[j]).toString() + "s", "");
							}

						}
					}
				}
			}
		}
		return cmp_cols;
	}

	private final boolean isBreakColumn(String column) {
		boolean flag = false;
		for(int i = 0; i < m_brkcmptcolumns.size(); i++) {
			String breakCol = m_brkcmptcolumns.get(i);
			if(breakCol.equalsIgnoreCase(column)) {
				flag = true;
				break;
			}
			else {
				String colAlias = getAlias(breakCol);
				if(colAlias.length() > 0 && colAlias.equalsIgnoreCase(column)) {
					flag = true;
					break;
				}
				else {
					String colHeading = getHeading(breakCol.toLowerCase(), m_scriptRunnerContext.getStoredFormatCmds());
					if(colHeading.length() > 0 && colHeading.equalsIgnoreCase(column)) {
						flag = true;
						break;
					}
				}
			}
		}

		return flag;
	}

	private String getFunctionForCmptColumn(String cmptCol, String label,
			ArrayList<String> reportCols) {
		String function = "";
		boolean found = false;
		ArrayList<String> repColsWithAlias = getAliasedOrOrigCols(reportCols);
		for (int i = 0; i < repColsWithAlias.size(); i++) {
			String column = repColsWithAlias.get(i);
			ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext
					.getStoredComputeCmds().get(column);
			Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList
					.iterator();
			while (iter.hasNext()) {
				TreeMap<String, HashMap<String, String>> col_cmptfunc = iter
						.next();
				Iterator<String> citer = col_cmptfunc.keySet().iterator();
				while (citer.hasNext()) {
					String funckey = citer.next();
					if (cmptCol.equalsIgnoreCase(funckey)) {
						HashMap<String, String> cmpt_func = col_cmptfunc
								.get(funckey);
						Iterator<String> funcs = cmpt_func.keySet().iterator();
						while (funcs.hasNext()) {
							function = funcs.next();
							String lbl = cmpt_func.get(function);
							lbl = lbl.replaceAll("'", "");
							found = label.equalsIgnoreCase(lbl);
							if (found) {
								break;
							}
						}
						break;
					}
				}
				if (found) {
					break;
				}
			}
			if (found) {
				break;
			}
		}
		return function;
	}

	private boolean isComputedColumn(String column) {
		boolean isCmptCol = false;
		// ArrayList<String> repCols = getReportColumns();
		// ArrayList<String> cmptCols = getComputeColumns(repCols);
		for (String cmptCol : m_computecolumns) {
			if (column.equalsIgnoreCase(cmptCol)) {
				isCmptCol = true;
				break;
			}
		}
		return isCmptCol;
	}

	private int findStringIndex(ArrayList<Object> objList, String str) {
		int index = -1;
		if (numberofOccurences(objList, str) > 1) {
			int nFind = 0;
			for (int i = 0; i < objList.size(); i++) {
				String obj = (String) objList.get(i);
				if (obj != null && obj.equalsIgnoreCase(str)) {
					nFind++;
				}
				if (nFind > 1) {
					return i;
				}
			}
		} else {
			for (int i = 0; i < objList.size(); i++) {
				String obj = (String) objList.get(i);
				if (obj != null && obj.equalsIgnoreCase(str)) {
					return i;
				}
			}
		}
		return index;
	}

	private int numberofOccurences(ArrayList<Object> objList, String str) {
		int retval = 0;
		for (int i = 0; i < objList.size(); i++) {
			String obj = (String) objList.get(i);
			if (obj != null && obj.equalsIgnoreCase(str)) {
				retval++;
			}
		}
		return retval;
	}

	private String getBreakColCmptKey(HashMap<String, Object[][]> cmptd_rows,
			ArrayList<Object> cmptd_columns, ResultSet rs) throws SQLException {
		String brkcolKey = "";
		if (cmptd_rows.size() > 0) {
			ArrayList<String> reportCols = getAliasedOrOrigCols(m_brkcmptcolumns);
			ArrayList<String> fList = filterList(m_brkcmptcolumns, "report");
			int[] rsetIdxs = new int[fList.size() + 1];
			for (int i = 1; i < m_columns.length; i++) {
				for (int j = 0; j < fList.size(); j++) {
					if (fList.get(j).equalsIgnoreCase(m_columns[i])) {
						rsetIdxs[i] = i;
						break;
					}
				}
			}

			String[] brkcmpkeys = new String[rsetIdxs.length];
			for (int j = 1; j < rsetIdxs.length; j++) {
				if (rsetIdxs[j] > 0) {
					if (rs instanceof OracleResultSet) {
						Object obj = ScriptUtils.getOracleObjectWrap((OracleResultSet) rs,rsetIdxs[j]);
						String strval = DataTypesUtil.stringValue(obj, m_conn);
						if(cmptd_rows.get(strval) != null) {
							brkcolKey += strval;
						}
					}
				}
			}

			for(int j = 1; j < rsetIdxs.length; j++) {
				if(j == 1) {
					brkcolKey = "";
					Object obj = ((OracleResultSet) rs).getOracleObject(rsetIdxs[j]);
					String strval = DataTypesUtil.stringValue(obj, m_conn);
					if(cmptd_rows.get(strval) != null) {
						brkcolKey = strval;
						brkcmpkeys[0] = brkcolKey;
					}
				}
				else {
					brkcolKey = "";
					for (int k = 0; k <= j; k++) {
						Object obj = ScriptUtils.getOracleObjectWrap((OracleResultSet) rs,rsetIdxs[k+1]);
						String strval = DataTypesUtil.stringValue(obj, m_conn);
						if(cmptd_rows.get(brkcolKey + strval) != null) {
							brkcolKey += strval;
							brkcmpkeys[k] = brkcolKey;
						}
					}
				}
			}





			if(brkcolKey.length() == 0 && reportCols.contains("report")/*findString(reportCols, "report")*/) {
				brkcolKey = "report";
				int size = cmptd_columns.size(); 
				for(int i = 0; i < size; i++) {
					String cmpCol = (String)cmptd_columns.get(i);
					if(!cmpCol.endsWith("_CMPFN") && !cmpCol.equals("ROW_CNT")) {
						brkcolKey += cmpCol;
					}
				}
			}
		}

		return brkcolKey;
	}
	
	private String[] generateBrkComputeColKeys (HashMap<String, Object[][]> cmptd_rows,
			ArrayList<Object> cmptd_columns, ResultSet rs, String level) throws SQLException {
		ArrayList<String> fList = filterList(m_brkcmptcolumns, "report");
		fList = filterList(fList, "row");
		String[] brkcmpkeys = new String[m_brkcmptcolumns.size()];
		int[] rsetIdxs = new int[fList.size() + 1];
		m_brkcmpkey_to_cols.clear();
		for (int i = 1; i < m_columns.length; i++) {
			if(m_column_to_brkcmptcol.get(m_columns[i]) != null) {
				for(int j = 1; j < rsetIdxs.length; j++) {
					// make sure we get the right column indexes.
					String cmptd_col = (String)cmptd_columns.get(j-1);
					if(cmptd_col != null &&  
							(m_columns[i].equalsIgnoreCase(cmptd_col) || 
							 getHeading(m_columns[i], m_scriptRunnerContext.getStoredFormatCmds()).equalsIgnoreCase(cmptd_col) || // Bug 22755659
							 m_column_to_brkcmptcol.get(m_columns[i]).equalsIgnoreCase(cmptd_col) ||
							 getHeading(m_column_to_brkcmptcol.get(m_columns[i]), m_scriptRunnerContext.getStoredFormatCmds()).equalsIgnoreCase(cmptd_col)) && // Bugs 22539753 22755659
							 rsetIdxs[j] == 0) {
						rsetIdxs[j] = i;
						break;
					}
				}
			}
		}

		if(rsetIdxs.length > 1) {
			boolean isValidKey = false;
			String brkcolKey = "";
			ArrayList<String> brkckeys = new ArrayList<String>();
			// Bugs 22538861 & 22571553
			// GroupingId Levels are taken into account as they are used to form keys.
			HashMap<Integer, Object[]> lvltocol = groupLevels(fList);
			ArrayList<Integer> grpLevels = new ArrayList<Integer>();
			grpLevels.addAll(lvltocol.keySet());
			Iterator<Integer> lvlIter = grpLevels.iterator();
			while(lvlIter.hasNext()) {
				int lvl = (Integer)lvlIter.next();
				Object[] repcols = lvltocol.get(lvl);
				brkcolKey = lvl + "_";
				String strval = "";
				int rsetIdx = -1;
				for(int j = 1; j < rsetIdxs.length && j < repcols.length; j++) {
					rsetIdx = rsetIdxs[j];
					if (rsetIdx > 0) {
						Object obj = ScriptUtils.getOracleObjectWrap((OracleResultSet) rs,rsetIdxs[j]);
						// Bug 22539753 & 22755659
						// For brkcolkeys we don't substitute m_null value.
						if(obj instanceof BINARY_DOUBLE) {
							BINARY_DOUBLE d = (BINARY_DOUBLE)obj;
							float f = Float.parseFloat(String.valueOf(d.doubleValue()));
							strval = formattedFLOAT(m_conn, f, m_numformat, m_colsizes[rsetIdx]);
						}
						else if(obj instanceof BINARY_FLOAT) {
							float f = ((BINARY_FLOAT)obj).floatValue();
							strval = formattedFLOAT(m_conn, f, m_numformat, m_colsizes[rsetIdx]);
						}
						else {
							if(obj == null || ((Datum) obj).isNull()) {
								strval = "";
							}
							else {
							   strval = ((OracleResultSet) rs).getString(rsetIdxs[j]);	
							}
						}
						
						strval = strval == null ? "" : strval;
						brkcolKey += strval;
						if (j == repcols.length - 1) {
							if (cmptd_rows.get(brkcolKey) != null) {
								if(m_breakandselorderMatch) {
									String[] brkcols = m_brkcmpkey_to_cols.get(brkcolKey);
								    brkcols = addRow(brkcols, m_columns[rsetIdxs[j]]);
								    m_brkcmpkey_to_cols.put(brkcolKey, brkcols);
								}
								else {
									String[] columns = getColumns(cmptd_rows, cmptd_columns, brkcolKey);
									m_brkcmpkey_to_cols.put(brkcolKey, columns);
									
									
								}
								if (!brkckeys.contains(brkcolKey))
									brkckeys.add(brkcolKey);
							}
						}

					}
				}

				if((brkcolKey.matches("\\d+_$") || brkcolKey.matches("(?<!\\S)\\d\\S*")) && cmptd_rows.get(brkcolKey) != null) {
					if(!brkckeys.contains(brkcolKey))
						brkckeys.add(brkcolKey);
				}
			}

			if(brkckeys.size() > 0) {
				brkcmpkeys = new String[brkckeys.size()];
				brkckeys.toArray(brkcmpkeys);
			}
			
			if(isAllRowCountsEqual(cmptd_rows, cmptd_columns, brkcmpkeys)) {
				// ok, let sort the brkcmpkeys.
				HashMap<String, String> srtdFunctions = Compute.sort(m_brkcol_to_cmpfn);
				Set<String> brkcols = srtdFunctions.keySet();
				Iterator<String> brkcolsIter = brkcols.iterator();
				String[] srtdbrkcmpkeys = new String[brkcmpkeys.length];
				ArrayList<String> sortedBrkCmpKeys = new ArrayList<String>();
				while(brkcolsIter.hasNext()) {
					String brkcol = brkcolsIter.next();
					for(int i = 0; i < brkcmpkeys.length; i++) {
						String[] cols = m_brkcmpkey_to_cols.get(brkcmpkeys[i]);
						for(String col : cols) {
							String bcol = m_column_to_brkcmptcol.get(col);
							if(bcol != null && bcol.equals(brkcol) && sortedBrkCmpKeys.contains(brkcmpkeys[i])) {
								sortedBrkCmpKeys.add(brkcmpkeys[i]);
							}
						}
					}
				}
	
				if(sortedBrkCmpKeys.size() > 0) {
					sortedBrkCmpKeys.toArray(srtdbrkcmpkeys);
					brkcmpkeys = srtdbrkcmpkeys;
				}
			}
		}

		if(level.equalsIgnoreCase("report") && m_brkcmptcolumns.get(m_brkcmptcolumns.size()-1).equalsIgnoreCase("report")) {
			String brkcolKey = "report";
			int len = brkcmpkeys.length;
			if(len > 0 && cmptd_rows.get(brkcolKey) != null && brkcmpkeys[len-1] == null) {
				brkcmpkeys[len-1] = brkcolKey;
			}
		}

		return brkcmpkeys;
	}
	
	private int getFirstCmptdRowCount(HashMap<String, Object[][]> cmptd_rows,
			ArrayList<Object> cmptd_columns, String brkColCmptkeys)
					throws SQLException {
		int rcIdx = cmptd_columns.indexOf("ROW_CNT");
		Object[][] foundRows = cmptd_rows.get(brkColCmptkeys);
		int grpRowCount = 0;
		if(foundRows != null)
			grpRowCount = Integer.valueOf(DataTypesUtil.stringValue(foundRows[0][rcIdx+1], m_conn));
		return grpRowCount;
	}

	private boolean isRowCountsEqual(HashMap<String, Object[][]> cmptd_rows,
			ArrayList<Object> cmptd_columns, String brkColCmptkey) {
		boolean isEqual = true;
		int rcIdx = cmptd_columns.indexOf("ROW_CNT");
		Object[][] data = cmptd_rows.get(brkColCmptkey);
		int grpRowCount = -1;
		if(data != null && data.length > 1) {
			for(int j = 0; j < data.length; j++) {
				Object[] cmptd_row = data[j];
				if(grpRowCount == -1) {
					grpRowCount = Integer.valueOf(DataTypesUtil.stringValue(cmptd_row[rcIdx+1], m_conn));
					continue;
				}
				if(grpRowCount != Integer.valueOf(DataTypesUtil.stringValue(cmptd_row[rcIdx+1], m_conn))) {
					isEqual = false;
					break;
				}
			}
		}
		return isEqual;
	}


	private boolean isAllRowCountsEqual(HashMap<String, Object[][]> cmptd_rows,
			ArrayList<Object> cmptd_columns, String[] brkColCmptkeys) {
		boolean isEqual = true;
		int rcIdx = cmptd_columns.indexOf("ROW_CNT");
		int rowCount = -1;
		for(int i = 0; i < brkColCmptkeys.length; i++) {
			if(isRowCountsEqual(cmptd_rows, cmptd_columns, brkColCmptkeys[i])) {
				Object[][] data = cmptd_rows.get(brkColCmptkeys[i]);
				if(data != null) {
					if(i == 0) {
						rowCount = Integer.valueOf(DataTypesUtil.stringValue(data[0][rcIdx+1], m_conn));
					}
					else if(rowCount != Integer.valueOf(DataTypesUtil.stringValue(data[0][rcIdx+1], m_conn))) {
						isEqual = false;
						break;
					}
				}
			}
			else {
				isEqual = false;
				break;
			}
		}
		return isEqual;
	}

	private boolean isEmptyBreakColKeys(String[] breakcolcmptkeys) {
		boolean emptyBrkKeys = true;
		for(int k = 0; k < breakcolcmptkeys.length; k++){
			if(breakcolcmptkeys[k] == null) {
				emptyBrkKeys = true;
			}
			else {
				emptyBrkKeys = false;
				break;
			}
		}
		return emptyBrkKeys;
	}

	private Object[][] getEqualRowCountRows(HashMap<String, Object[][]> cmptd_rows,
			ArrayList<Object> cmptd_columns, String brkColCmptkey) {
		Object[][] samecntRows = null;
		int rcIdx = cmptd_columns.indexOf("ROW_CNT");
		Object[][] data = cmptd_rows.get(brkColCmptkey);
		int grpRowCount = -1;
		if (data != null) {
			for (int j = 0; j < data.length; j++) {
				Object[] cmptd_row = data[j];
				if (j == 0 && grpRowCount == -1) {
					grpRowCount = Integer.valueOf(DataTypesUtil.stringValue(cmptd_row[rcIdx + 1], m_conn));
					samecntRows = addRow(samecntRows, cmptd_row);
					continue;
				}
				if (grpRowCount == Integer.valueOf(DataTypesUtil.stringValue(cmptd_row[rcIdx + 1], m_conn))) {
					samecntRows = addRow(samecntRows, cmptd_row);
				}
			}

			// sort here.
			if (samecntRows.length > 1) {
				String col = "";
				boolean bFunctions = false;
				int colIdx = -1;
				ArrayList<Integer> cmpfnIdxs = new ArrayList<Integer>();
				for (int i = 0; i < cmptd_columns.size(); i++) {
					col = (String) cmptd_columns.get(i);
					if (col.endsWith("_CMPFN")) {
						for (int j = 0; j < samecntRows.length; j++) {
							String func = DataTypesUtil.stringValue(samecntRows[j][i + 1], m_conn);
							if (func.length() > 0) {
								colIdx = i + 1;
								cmpfnIdxs.add(colIdx);
							}
						}
					}
				}

				if (cmpfnIdxs.size() > 0) {
					final Integer[] idxs = new Integer[cmpfnIdxs.size()];
					cmpfnIdxs.toArray(idxs);
//					 lambda's not supported
//					 Arrays.sort(samecntRows, (Object[] s1, Object[] s2) -> DataTypesUtil.stringValue(s1[idx], m_conn).compareTo(DataTypesUtil.stringValue(s2[idx], m_conn)));
					Arrays.sort(samecntRows, new Comparator<Object[]>() {
						@Override
						public int compare(Object[] row1, Object[] row2) {
							int idx1 = 0;
							int idx2 = 0;
							for(int i = 0; i < idxs.length; i++) {
								int len1 = DataTypesUtil.stringValue(row1[idxs[i]], m_conn).length();
								if(len1 > 0) {
									idx1 = idxs[i];
									break;
								}
							}
							
							for(int i = 0; i < idxs.length; i++) {
								int len2 = DataTypesUtil.stringValue(row2[idxs[i]], m_conn).length();
								if(len2 > 0) {
									idx2 = idxs[i];
									break;
								}
							}
							
							String func1 = idx1 > 0 ? DataTypesUtil.stringValue(row1[idx1], m_conn) : "";
							String func2 = idx2 > 0 ? DataTypesUtil.stringValue(row2[idx2], m_conn) : "";
							int val1 = Compute.getIndexofFunction(func1);
							int val2 = Compute.getIndexofFunction(func2);
							return Integer.valueOf(val1).compareTo(Integer.valueOf(val2));
						}
					});
				}
			}
		}

		return samecntRows;
	}
	
	private String[] getColumns(HashMap<String, Object[][]> cmptd_rows, ArrayList<Object> cmptd_columns, String brkColCmptkey) {
		String[] columns = null;
		String[] funcs = null;
		Object[][] rows = cmptd_rows.get(brkColCmptkey);
		
		for (int i = 0; i < cmptd_columns.size(); i++) {
			String col = (String) cmptd_columns.get(i);
			if (col.endsWith("_CMPFN")) {
				for (int j = 0; j < rows.length; j++) {
					String func = DataTypesUtil.stringValue(rows[j][i + 1], m_conn);
					if (func.length() > 0) {
						funcs = addRow(funcs, func);
					}
				}
			}
		}
		
		Iterator<String> brkcolIter = m_brkcol_to_cmpfn.keySet().iterator();
		String[] brkcols = null;
		while(brkcolIter.hasNext()) {
			String brkcol = brkcolIter.next();
			for(String func : funcs) {
				if(m_brkcol_to_cmpfn.get(brkcol) != null && m_brkcol_to_cmpfn.get(brkcol).equals(func)) {
					brkcols = addRow(brkcols, brkcol);
				}
			}
		}
		
		Iterator<String> colIter = m_column_to_brkcol.keySet().iterator();
		while(colIter.hasNext()) {
			String col = colIter.next();
			for(String brkcol : brkcols) {
				if(m_column_to_brkcol.get(col) != null && m_column_to_brkcol.get(col).equals(brkcol)) {
					columns = addRow(columns, col);
				}
			}
		}
		return columns;
	}
	
	private String getFunction(Object[] row, ArrayList<Object> cmptd_columns) {
		String cmpfn = "";
		for (int i = 0; i < cmptd_columns.size(); i++) {
			String col = (String) cmptd_columns.get(i);
			if (col.endsWith("_CMPFN")) {
				cmpfn = DataTypesUtil.stringValue(row[i + 1], m_conn);
				break;
			}
		}
		return cmpfn;
	}


	/**
	 * Generates and Executes SQL that computes based on the aggregation
	 * function on the COMPUTE command, provided that the source query has the
	 * break columns(BREAK command).
	 * 
	 * @param rows
	 * @param columns
	 * @return
	 */
	private boolean computedData(HashMap<String, Object[][]> rows,
			ArrayList<Object> columns) {
		if (rows != null && columns != null) {
			String sql = "";
			m_brkcmptcolumns = getReportColumns();
			m_computecolumns = getComputeColumns(m_brkcmptcolumns);

			if (m_brkcmptcolumns.size() > 0 && m_computecolumns.size() > 0) {
				sql = generateComputeSQL(m_brkcmptcolumns, m_computecolumns);
				String err = "";
				try {
					executeComputeSQL(sql, rows, columns);
				}
				catch (SQLException ex) {
					Logger.getLogger(getClass().getName()).log(Level.WARNING,
							Messages.getString("ComputeFailure"), ex);
				}
				return rows.size() > 0;
			}
		}
		return false;
	}
	
	private void parseSQL() {
		// Parse the SQL statement
		m_selColumnList.addAll(SqlRecognizer.getColumns(m_sql));
		m_selTableList.addAll(SqlRecognizer.getTables(m_sql));
		m_selPredicateList.addAll(SqlRecognizer.getPredicates(m_sql));
		m_selGroupByList.addAll(SqlRecognizer.getGroupBy(m_sql));
		m_selOrderByList.addAll(SqlRecognizer.getOrderBy(m_sql));
	}

	/**
	 * This function will be used to Compute when ROW is used in the COMPUTE
	 * command. This function might be useful if we support this feature.
	 * 
	 * @param rows
	 * @param columns
	 * @param rs
	 * @throws SQLException
	 */
	private void computeDataForRow(ArrayList<Object[]> rows,
			ArrayList<Object> columns, ResultSet rs) throws SQLException {
		ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext
				.getStoredComputeCmds().get("row");
		Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList
				.iterator();
		while (iter.hasNext()) {
			TreeMap<String, HashMap<String, String>> col_cmptfunc = iter.next();
			if (rows != null && columns != null && col_cmptfunc != null) {
				// ArrayList<String> repCols = getReportColumns();
				boolean bRowFound = findString(m_brkcmptcolumns, "row");
				// ArrayList<String> cmptCols = getComputeColumns(repCols);
				int[] rsetIdxs = new int[m_computecolumns.size() + 1];

				Object[] rowobj = new Object[m_computecolumns.size() + 2 + 1]; // 2
				// is
				// for
				// ROW_CNT
				// and
				// COMPUTE
				if (bRowFound && m_computecolumns.size() > 0) {
					for (int i = 1; i < m_columns.length; i++) {
						for (int j = 0; j < m_computecolumns.size(); j++) {
							if (m_computecolumns.get(j).equalsIgnoreCase(
									m_columns[i])) {
								rsetIdxs[j + 1] = i;
								columns.add(m_columns[i]);
								break;
							}
						}
					}

					columns.add("ROW_CNT");
					columns.add("COMPUTE");

					for (int k = 1; k < rsetIdxs.length; k++) {
						try {
							rowobj[k] = ScriptUtils.getOracleObjectWrap((OracleResultSet) rs,rsetIdxs[k]);
						} catch (SQLException ex) {
							rowobj[k] = rs.getObject(rsetIdxs[k]);
						}
					}
					rowobj[m_computecolumns.size() + 1] = new NUMBER(1);
					ArrayList<String> cmptKeys = new ArrayList<String>();
					cmptKeys.addAll(col_cmptfunc.keySet());
					if (cmptKeys.size() > 0)
						rowobj[m_computecolumns.size() + 2] = new CHAR(
								col_cmptfunc.get(cmptKeys.get(0)),
								CHAR.DEFAULT_CHARSET);
					rows.add(rowobj);
				}
			}
		}
	}

	/**
	 * Report columns are the Columns used in the BREAK command and those same
	 * columns used in the COMPUTE's ON clause.
	 * 
	 * You will get Report columns if your source SQL has the columns used in
	 * BREAK and COMPUTE commands. Otherwise the list is empty.
	 * 
	 * @return
	 */
	private ArrayList<String> getReportColumns() {
		ArrayList<String> repCols = new ArrayList<String>();
		ArrayList<String> breakColsinSel = new ArrayList<String>();
		ArrayList<String> breakColList = new ArrayList<String>();
		boolean bfound = false; // for report

		if (m_scriptRunnerContext.getStoredBreakCmds().size() > 0) {
			breakColList.addAll(m_scriptRunnerContext.getStoredBreakCmds()
					.keySet());
			ArrayList<String> selList = new ArrayList<String>();
//			selList.addAll(SqlRecognizer.getColumns(m_sql));
			selList.addAll(m_selColumnList);
			// Bug 21528592
			if(selList.size() > 0) {
				for (String selCol : selList) {
					for (String col : breakColList) {
						String colandalias[] = selCol.split("\\s+");
//						String alias = colandalias.length > 2 ? colandalias[2]
//								: colandalias.length > 1 ? colandalias[1] : selCol;
						String alias = getAlias(selCol);
								if (col.equalsIgnoreCase(selCol) || col.equalsIgnoreCase(alias)) {
									breakColsinSel.add(selCol);
									break;
								}
					}
				}
				
				if(findString(breakColList, "report")) {
					breakColsinSel.add("report");
				}
			}
			else {
				breakColsinSel.addAll(breakColList);
			}

			ArrayList<String> cmptRepCols = new ArrayList<String>();
			cmptRepCols.addAll(m_scriptRunnerContext.getStoredComputeCmds()
					.keySet());
			
            if(m_breakandselorderMatch) { 			
				for (String brkCmptCol : breakColsinSel) {
					for (String col : cmptRepCols) {
//						String colandalias[] = brkCmptCol.split("\\s+");
//						String alias = colandalias.length > 2 ? colandalias[2]
//								: colandalias.length > 1 ? colandalias[1]
//										: brkCmptCol;
						String alias = getAlias(brkCmptCol);
								if (col.equalsIgnoreCase(brkCmptCol)
										|| col.equalsIgnoreCase(alias)) {
									repCols.add(brkCmptCol);
									break;
								}
					}
				}
            }
            else
			  repCols.addAll(breakColsinSel);

			// look for REPORT in both break and compute columns
			for (String brkcol : breakColList) {
				for (String cmptRepcol : cmptRepCols) {
					if (brkcol.equalsIgnoreCase(cmptRepcol) && brkcol.equalsIgnoreCase("report") &&
							!repCols.contains("report")) {// Bug 21475831 avoid duplicate "report" here.	
						repCols.add(brkcol);
						bfound = true;
						break;
					}
				}
				if (bfound)
					break;
			}

			// Bug 20379437
			// Now let's investigate the break columns(repCols)  
			// to see if a prior column of a particular break column 
			// in the Orderby list is also a break column.
			// If not, then we need to add the non-break column to the
			// repCols list.
			ArrayList<String> filrepCols = filterList(repCols, "report");
			ArrayList<String> ordByList = new ArrayList<String>();
//			ordByList.addAll(SqlRecognizer.getOrderBy(m_sql));
			ordByList.addAll(m_selOrderByList);
			ArrayList<String> missingCols = new ArrayList<String>();
			if (ordByList.size() > 0) {
				for (int i = 0; i < filrepCols.size(); i++) {
					String[] colandalias = filrepCols.get(i).split("\\s+");
					int ordByIdx = -1;
					for(int k = 0; k < colandalias.length; k++) {
						ordByIdx = ordByList.indexOf(colandalias[k]);
						if(ordByIdx > -1) {
							break;
						}
					}
					if (ordByIdx > 0) {
						for(int j = 0; j < ordByIdx; j++) {
							int repColIdx = filrepCols.indexOf(ordByList.get(j));
							if (repColIdx == -1) {
								String missingCol = ordByList.get(j);
								missingCols.add(missingCol);
								if (j < repCols.size()) {
									repCols.add(j, missingCol);
								}
								else {
									repCols.add(missingCol);
								}
							}
						}
					}
				}
			}
		}

		return repCols;
	}

	/**
	 * Compute columns are the columns referenced in the COMPUTE's OF clause.
	 * This is the Column where the Aggregate function is applied.
	 * 
	 * COMPUTE command functions are always executed in the sequence as
	 * mentioned in
	 * http://yourmachine.yourdomain/12/121/server.121/e18404/ch_twelve014
	 * .htm#i2697257
	 * 
	 * @param reptCols
	 * @return
	 */
	private ArrayList<String> getComputeColumns(ArrayList<String> reptCols) {
		ArrayList<String> cmptCols = new ArrayList<String>();
		ArrayList<String> selList = new ArrayList<String>();
		try {
//			selList.addAll(SqlRecognizer.getColumns(m_sql));
			selList.addAll(m_selColumnList);
		} catch (Error e) {//eat it!, definitely dont want to bomb out if not recognized
		}
		for (String breakCmptCol : reptCols) {
			String colandalias[] = breakCmptCol.split("\\s+");
//			String bkcoloralias = colandalias.length > 2 ? colandalias[2]
//					: colandalias.length > 1 ? colandalias[1] : breakCmptCol;
			String bkcoloralias = getAlias(breakCmptCol);
					ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext
							.getStoredComputeCmds().get(bkcoloralias);
					// Making sure that the function maps are sorted. Refer to the doc.
					// Bug 20385253 fix for sorting
					if(mapList != null) {
//						Compute.sortFunctions(mapList);
						Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList
								.iterator();
						while (iter.hasNext()) {
							TreeMap<String, HashMap<String, String>> col_cmptfunc = iter
									.next();
							ArrayList<String> cmptKeys = new ArrayList<String>();
							if (col_cmptfunc != null) {
								cmptKeys.addAll(col_cmptfunc.keySet());
								for (String col : cmptKeys) {
									// Bug 21528592
									if(selList.size() > 0) {
										col = getColumnFromAlias(col);
										// Bug 20443188 break column can also be a compute
										// column
										if (findString(selList, col)
												&& !findString(cmptCols, col)) {
											cmptCols.add(col);
										}
									}
									else {
										cmptCols.add(col);
									}
								}
							}
						}
					}
		}
		
		final ArrayList<String> tselList = selList;
		Collections.sort(cmptCols, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return tselList.indexOf(o1) - tselList.indexOf(o2);
			}			
			
		});

		return cmptCols;
	}

	private boolean findString(ArrayList<String> listStr, String findStr) {
		boolean found = false;
		for (String str : listStr) {
			if (str.equalsIgnoreCase(findStr)) {
				return true;
			} else if (str.split("\\s+").length > 1) {
				if(str.indexOf(findStr) > -1) {
					return true;
				}
				
//				String colandalias[] = str.split("\\s+");
//				String column = colandalias.length > 1 ? colandalias[0] : str;
//				if (column.equalsIgnoreCase(findStr)) {
//					return true;
//				}
				
				// return colandalias.length > 1 ?
				// colandalias[0].equalsIgnoreCase(findStr) ||
				// colandalias[1].equalsIgnoreCase(findStr) : false;

			}
		}
		return found;
	}

	private String getAlias(String column) {
		String alias = "";
		alias = m_column_to_alias.get(column);
		if(alias != null && alias.length() > 0)
			return alias;
		if(column.equalsIgnoreCase("report")) {
			alias = column;
			m_column_to_alias.put(column, "report");
			return alias;
		}
		else if(column.equalsIgnoreCase("row")) {
			alias = column;
			m_column_to_alias.put(column, "row");
			return alias;
		}
		ArrayList<String> selList = new ArrayList<String>();
//		selList.addAll(SqlRecognizer.getColumns(m_sql));
		selList.addAll(m_selColumnList);
		// Bug 21528592
		if(selList.size() == 0) {
			alias = column;
			m_column_to_alias.put(column, column);
			return alias;
		}
		for (int i = 0; i < selList.size(); i++) {
			String str = selList.get(i);
//			String[] strArr = str.split("\\s+");
			String[] strArr = null;
			List<String> matchList = new ArrayList<String>();
			Pattern regex = Pattern.compile("[^\\s\"']+|\"[^\"]*\"|'[^']*'");
			Matcher regexMatcher = regex.matcher(str);
			while (regexMatcher.find()) {
			    matchList.add(regexMatcher.group());
			}
			strArr = new String[matchList.size()];
			matchList.toArray(strArr);
			
			// Bug 24377372 
			if (column.equalsIgnoreCase(str)) {
//				alias = strArr.length > 1 ? strArr[strArr.length - 1] : strArr[0];
				alias = strArr.length > 1 ? strArr[strArr.length - 1].replaceAll("\"", "") : strArr[0];
			}
			else if(strArr[strArr.length - 1].replaceAll("\"", "").equalsIgnoreCase(column)) {
				alias = column;
			}
			else {
				String modcol = column.replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)");
				modcol = modcol.replaceAll("\\*", "\\\\*");
				modcol = "(?i:" + modcol + ")";
				Pattern ptrn = Pattern.compile(modcol);
				Matcher m = ptrn.matcher(str);
				while(m.find()) {
//					alias = str.replaceFirst(modcol, "").trim().replaceFirst("(?i:as)", "").trim();
					alias = str.replaceFirst(modcol, "").trim().replaceFirst("(?i:as)", "").trim().replaceAll("\"", "");
				}
			}
			
			if (alias != null && alias.length() > 0) {
				break;
			}
		}
		
		// Making sure that the column and its alias are properly known.
		String modcol = column.replaceAll("\"", "");
		if (alias != null && alias.length() > 0) {
	        int idx = modcol.lastIndexOf(alias);
		    if (idx > 0) {
		        modcol = modcol.substring(0, idx).trim();
		    }
		}
		
		if(alias.length() > 0)
		    m_column_to_alias.put(modcol, alias); 
		return alias;
	}

	private String getColumnFromAlias(String alias) {
		String column = "";
		ArrayList<String> selList = new ArrayList<String>();
//		selList.addAll(SqlRecognizer.getColumns(m_sql));
		selList.addAll(m_selColumnList);
		for (int i = 0; i < selList.size(); i++) {
			String str = selList.get(i);
//			String[] strArr = str.split("\\s+");
			String[] strArr = null;
			List<String> matchList = new ArrayList<String>();
			Pattern regex = Pattern.compile("[^\\s\"']+|\"[^\"]*\"|'[^']*'");
			Matcher regexMatcher = regex.matcher(str);
			while (regexMatcher.find()) {
			    matchList.add(regexMatcher.group());
			}
			strArr = new String[matchList.size()];
			matchList.toArray(strArr);
			// Bug 24377372
			if(strArr.length > 1 && strArr[strArr.length - 1].replaceAll("\"", "").equalsIgnoreCase(alias)) {
                // Look for last occurrence of alias or quoted alias.
				column = str.replaceAll("(?i:" + alias + ")$|\"(?i:" + alias + ")\"", "").replaceAll("\"", "");
				column = column.replaceFirst("(?i:as)", "").trim();
			}
			else if(strArr[0].equalsIgnoreCase(alias)) {
				column = strArr[0];
			}
			if (column.length() > 0)
				break;

		}
		return column;
	}
	
	private String getAliasFromColumn(String column) {
		String alias = "";
		ArrayList<String> selList = new ArrayList<String>();
//		selList.addAll(SqlRecognizer.getColumns(m_sql));
		selList.addAll(m_selColumnList);
		for (int i = 0; i < selList.size(); i++) {
			String str = selList.get(i);
//			String[] strArr = str.split("\\s+");
			String[] strArr = null;
			List<String> matchList = new ArrayList<String>();
			Pattern regex = Pattern.compile("[^\\s\"']+|\"[^\"]*\"|'[^']*'");
			Matcher regexMatcher = regex.matcher(str);
			while (regexMatcher.find()) {
			    matchList.add(regexMatcher.group());
			}
			strArr = new String[matchList.size()];
			matchList.toArray(strArr);
			String col =  strArr[0];
			if(col.equalsIgnoreCase(column)) {
//				alias = strArr.length > 2 ? strArr[2].replaceAll("\"", "") : strArr.length > 1 ?  strArr[1].replaceAll("\"", "") : strArr[0];
				alias = strArr.length > 2 ? strArr[2] : strArr.length > 1 ?  strArr[1] : strArr[0];
				break;
			}
		}
		return alias;
	}
	
	private boolean areCompColsFuncExpr(ArrayList<String> cmptCols) {
		boolean isFuncExpr = false;
		for(int i = 0; i < cmptCols.size(); i++) {
			 String cmpCol = cmptCols.get(i);
			 isFuncExpr = isCompColFuncExpr(cmpCol);
			 if(isFuncExpr) break;
		}
				
		return isFuncExpr;
	}
	
	private boolean isCompColFuncExpr(String cmptCol) {
		boolean isFuncExpr = false;
		// Some Oracle functions		
		String[] oraFunc = new String[] {
		 "AVG", "COLLECT", "CORR", "COUNT", "COVAR_POP", "COVAR_SAMP", 
		 "CUME_DIST", "DENSE_RANK", "FIRST", "GROUP_ID", "GROUPING", "GROUPING_ID", 
		 "LAST", "LISTAGG", "MAX", "MEDIAN", "MIN", "PERCENT_RANK", "PERCENTILE_CONT", 
		"PERCENTILE_DISC", "RANK", "STDDEV", "STDDEV_POP", "	STDDEV_SAMP", "SUM", "SYS_XMLAGG", 
		"VAR_POP", "VAR_SAMP", "VARIANCE", "XMLAGG"};
		for (int j = 0; j < oraFunc.length; j++) {
			if (cmptCol.toUpperCase().startsWith(oraFunc[j]) || cmptCol.toUpperCase().indexOf(oraFunc[j]) > -1) {
				 isFuncExpr = true;
				 break;
			}
		}
		return isFuncExpr;
	}
	
	

	/**
	 * Executes generated Compute SQL and stores the Computed rows in
	 * ArrayList<Object[]>
	 * 
	 * @param sql
	 * @param rows
	 * @param columns
	 * @throws SQLException
	 */
	private void executeComputeSQL(String sql,
			HashMap<String, Object[][]> rows, ArrayList<Object> columns)
					throws SQLException {
		PreparedStatement stmt = m_conn.prepareStatement(sql);
		ResultSet rset = stmt.executeQuery();
		ResultSetMetaData rmeta = rset.getMetaData();
		int count = rmeta.getColumnCount();
		columns.ensureCapacity(count);
		//		for (int i = 1; i < count + 1; i++) {
		for (int i = 1; i < count; i++) {
			columns.add(rmeta.getColumnName(i));
		}

		int rowCount = 0;
		String breakcolkey = "";
		while (rset.next() && rowCount < s_maxRows) {
			rowCount++;
			breakcolkey = "";
			Object[] obj = new Object[count];
			for (int i = 1; i <= count; i++) {
				if (rset instanceof OracleResultSet) {
					try {
						Object rObj = ScriptUtils.getOracleObjectWrap((OracleResultSet) rset,i);
						// Bug 20385253 look for a NULL coming from rset.
						if(i < count) {
							if (rObj == null || ((Datum) rObj).isNull()) {
								rObj = m_null != null && m_null.length() > 0 ? m_null : "";
							}
							obj[i] = rObj;
						}

						if(i == count) {
							// Bug 22755659 sometimes the column
							// for some reason the null value (set null) is 
							// not picked by DataTypesUtil.stringValue("") function
							// so we don't substitute m_null value here.
							breakcolkey = DataTypesUtil.stringValue(rObj, m_conn);
							// Bug 24971326
							breakcolkey = modifyScientificNotation(breakcolkey);
						}
					}
					catch (Exception ex) {
						obj[i] = rset.getObject(i);
					}
				}
			}

			// Due to slow Performance avoiding all the resultsets to be
			// populated as ArrayList<Object[]> as this is causing severe problems
			// when trying to access rows using ArrayList.
			// Using HashMap and 2D Arrays for better performance.
			ArrayList<String> fList = filterList(m_brkcmptcolumns, "report");
			int nBrkCols = fList.size();
			String brkcolskey = "";

			// Bugs 22538861 & 22571553
			// Grouping Levels is taken into account as they are used to form keys.			
			Object[][] existingRows = (Object[][]) rows.get(breakcolkey);
			Object[][] newArray = null;
			if (existingRows != null) {
				newArray = addRow(existingRows, obj);
				rows.put(breakcolkey, newArray);
			}
			else {
				Object[][] val = new Object[1][];
				val[0] = obj;
				rows.put(breakcolkey, val);
			}

		}

		// Initialize HashMap variables for the first and only time
		// here.
		if (rows.size() > 0) {
			if (m_column_to_brkcmptcol.size() == 0) {
				mapSelColstoBreakColumns();
			}

			if (m_column_to_cmpcol.size() == 0) {
				mapSelColstoComputeColumns();
			}

			if (m_brkcol_to_cmplbl.size() == 0) {
				populateBreakColumnsToComputeLabel();
			}
		}
	}

	/**	
	 * Increase the size of the 2D Array and 
	 * adds the object(newrow)
	 * @param existingArr
	 * @param newrow
	 * @return
	 */
	private Object[][] addRow(Object[][] existingArr, Object[] newrow) {
		int prevRowCount = existingArr != null ? existingArr.length : 0;
		Object[][] newArray = new Object[prevRowCount + 1][];
		if (existingArr != null)
			System.arraycopy(existingArr, 0, newArray, 0, existingArr.length);
		newArray[prevRowCount] = newrow;
		return newArray;
	}
	
	private Object[] addRow(Object[] existingArr, Object newobj) {
		int prevRowCount = existingArr != null ? existingArr.length : 0;
		Object[] newArray = new Object[prevRowCount + 1];
		if (existingArr != null)
			System.arraycopy(existingArr, 0, newArray, 0, existingArr.length);
		newArray[prevRowCount] = newobj;
		return newArray;
	}
	
	private String[] addRow(String[] existingArr, String newobj) {
		int prevRowCount = existingArr != null ? existingArr.length : 0;
		String[] newArray = new String[prevRowCount + 1];
		if (existingArr != null)
			System.arraycopy(existingArr, 0, newArray, 0, existingArr.length);
		newArray[prevRowCount] = newobj;
		return newArray;
	}

	/**
	 * Removes Row of the 2D Array at specified Index.
	 * @param existingArr
	 * @param index
	 * @return
	 */
	private Object[][] removeRow(Object[][] existingArr, int index) {
		Object[][] newArray = null;
		if (existingArr.length > 1) {
			newArray = new Object[existingArr.length - 1][];
			if (existingArr.length != index) {
				System.arraycopy(existingArr, index + 1, newArray, index,
						existingArr.length - index - 1);
			}
		}
		return newArray;
	}

	private String[] removeRow(String[] existingArr, int index) {
		String[] newArray = null;
		if (existingArr.length > 1) {
			newArray = new String[existingArr.length - 1];
			System.arraycopy(existingArr, 0, newArray, 0, index);
			if (existingArr.length != index) {
				System.arraycopy(existingArr, index + 1, newArray, index, existingArr.length - index - 1);
			}
		}
		return newArray;
	}

	private Object[] removeRow(Object[] existingArr, int index) {
		Object[] newArray = null;
		if (existingArr.length > 1) {
			newArray = new Object[existingArr.length - 1];
			System.arraycopy(existingArr, 0, newArray, 0, index);
			if (existingArr.length != index) {
				System.arraycopy(existingArr, index + 1, newArray, index, existingArr.length - index - 1);
			}
		}
		return newArray;
	}
	
	/**
	 * Generates SQL for Computing Aggregate functions. The Code generates for
	 * Different grouping levels if break columns are used in the Compute
	 * columns(ON CLAUSE).
	 * 
	 * @param reportCols
	 * @param cmptCols
	 * @return
	 */
	private String generateComputeSQL(ArrayList<String> reportCols,
			ArrayList<String> cmptCols) {
		String sql = "";

		int numFuncs = numfunctionsUsedOnComputeCol(reportCols, cmptCols);
		boolean hasFuncExpr = areCompColsFuncExpr(cmptCols);

		ArrayList<String> fltreportCols = filterList(reportCols, "report");
		if (fltreportCols.size() > 0) {
			for (int i = 0; i < numFuncs; i++) {
				// Bug 20308166 a fix on Computation
				// break column order is very important.
				// If the break column order is different from
				// select list order then we compute differently.
				if(m_breakandselorderMatch) {
					sql += "SELECT " + m_lineSeparator;
					for (String reportCol : fltreportCols) {
						if(hasFuncExpr) {
							reportCol = getAlias(reportCol);
						}
						//if(cmptCols.contains(reportCol)) continue;
						// Bug 21511952
						String heading = getHeading(reportCol, m_scriptRunnerContext.getStoredFormatCmds());
						if(heading.length() > 0) {
							reportCol += " " + heading; // set the heading as an alias for now.
						}
						sql += reportCol + ", ";
					}

					String caseExprs = generateSQLCaseExpressions(
							fltreportCols, cmptCols, "Expression", i, hasFuncExpr);
					sql += caseExprs;
					sql += ", ";
					caseExprs = generateSQLCaseExpressions(fltreportCols,
							cmptCols, "Function", i, hasFuncExpr);
					sql += caseExprs + ", ";
					sql += "COUNT(*) AS ROW_CNT, ";
					// Bugs 22538861 & 22571553
					// Added a BRKCOLKEY to be used in Hashmap later.
					if(hasFuncExpr) {
						sql += "GROUPING_ID(" + join(", ", getAliasedOrOrigCols(fltreportCols)) + ") " + "|| '_' ";
						sql += "|| " + join(" || ", genAliasColsWithDecode(fltreportCols)) + " AS BRKCOLKEY";
					}
					else {
					    sql += "GROUPING_ID(" + join(", ", getOrigColsStripAlias(fltreportCols)) + ") " + "|| '_' ";
					    sql += "|| " + join(" || ", genOrigColsWithDecode(fltreportCols)) + " AS BRKCOLKEY";
					}
					sql += " " + m_lineSeparator;
					sql += "FROM ";
					
					if(hasFuncExpr) {
						sql += m_lineSeparator + "(" + m_lineSeparator + m_sql + m_lineSeparator + ") " + m_lineSeparator;
					}
					else {
						ArrayList<String> tableList = new ArrayList<String>();
//						tableList.addAll(SqlRecognizer.getTables(m_sql));
						tableList.addAll(m_selTableList);
						sql += join(", ", tableList) + m_lineSeparator;
//						List<String> whereClause = SqlRecognizer
//								.getPredicates(m_sql);
						List<String> whereClause = m_selPredicateList;
						if (whereClause != null && whereClause.size() > 0) {
							sql += "WHERE ";
							ArrayList<String> whereList = new ArrayList<String>();
							whereList.addAll(whereClause);
							if (whereList.size() == 1) {
								sql += whereList.get(0) + m_lineSeparator;
							}
						}
					}
					sql += generateSQLGroupby(fltreportCols, hasFuncExpr);
				} else {
					for (int k = 0; k < fltreportCols.size(); k++) {
						String repCol = fltreportCols.get(k);
						String rCol = getAlias(repCol);
						if (m_scriptRunnerContext.getStoredComputeCmds().get(rCol) != null) {
							sql += "SELECT " + m_lineSeparator;
							for (String reportCol : fltreportCols) {
								// Bug 21511952
								String heading = getHeading(reportCol, m_scriptRunnerContext.getStoredFormatCmds());
								if (heading.length() > 0) {
									reportCol += " " + heading; // set the
																// heading
																// as an
																// alias for
																// now.
								}
								sql += reportCol + ", ";

							}

							String caseExprs = generateSQLCaseExpressions(repCol, fltreportCols, cmptCols,
									"Expression", i);
							sql += caseExprs;
							sql += ", ";
							caseExprs = generateSQLCaseExpressions(repCol, fltreportCols, cmptCols, "Function", i);
							sql += caseExprs + ", ";
							sql += "COUNT(*) AS ROW_CNT, ";
							// Bugs 22538861 & 22571553
							// Added a BRKCOLKEY to be used in Hashmap
							// later.
							sql += "GROUPING_ID(" + join(", ", getOrigColsStripAlias(fltreportCols)) + ")"
									+ "|| '_' ";
							sql += "|| " + join(" || ", genOrigColsWithDecode(fltreportCols)) + " AS BRKCOLKEY";
							sql += " " + m_lineSeparator;
							sql += "FROM ";
							ArrayList<String> tableList = new ArrayList<String>();
//							tableList.addAll(SqlRecognizer.getTables(m_sql));
							tableList.addAll(m_selTableList);
							sql += join(", ", tableList) + m_lineSeparator;

//							List<String> whereClause = SqlRecognizer.getPredicates(m_sql);
							List<String> whereClause = m_selPredicateList;
							if (whereClause != null && whereClause.size() > 0) {
								sql += "WHERE ";
								ArrayList<String> whereList = new ArrayList<String>();
								whereList.addAll(whereClause);
								if (whereList.size() == 1) {
									sql += whereList.get(0) + m_lineSeparator;
								}
							}

							sql += generateSQLGroupby1(fltreportCols) + m_lineSeparator;

							if (k < fltreportCols.size() - 1) {
								if (fltreportCols.get(k + 1) != null && m_scriptRunnerContext.getStoredComputeCmds()
										.get(fltreportCols.get(k + 1)) != null)
									sql += "UNION" + m_lineSeparator;
							}
						}
					}
				}

				if (i < numFuncs - 1) {
					sql += m_lineSeparator + "UNION" + m_lineSeparator;
				}
			}
		}

		// generate for report only
		String reptSQL = "";
		if (findString(reportCols, "report")) {
			numFuncs = numfunctionsUsedOnReportCol(reportCols, cmptCols);
			ArrayList<String> funcs = getFunctionsUsedForReport(reportCols, cmptCols);
			for (int i = 0; i < funcs.size(); i++) {
				reptSQL += "SELECT " + m_lineSeparator;
				// we still need these columns from fltreportCols list.
				for (String reportCol : fltreportCols) {
					//if(cmptCols.contains(reportCol)) continue;
					// assign NULL for "Report" because we are not dealing with
					// Groups here.
					// Bug 21475712
					reptSQL += "NULL " + getAlias(reportCol) + ", ";
				}

				reptSQL += generateComputeForColumn(reportCols, cmptCols, funcs.get(i));
				reptSQL += ", ";

				reptSQL += generateSQLForReportExpr(reportCols, cmptCols, funcs.get(i));
				reptSQL += ", ";

				reptSQL += "COUNT(*) AS ROW_CNT, " ;
				// Bugs 22538861 & 22571553
				// Added a BRKCOLKEY to be used in Hashmap later.
				reptSQL += "'report' AS BRKCOLKEY " ;

				reptSQL += " FROM ";
				ArrayList<String> tableList = new ArrayList<String>();
//				tableList.addAll(SqlRecognizer.getTables(m_sql));
				tableList.addAll(m_selTableList);
				reptSQL += join(", ", tableList) + m_lineSeparator;

//				List<String> whereClause = SqlRecognizer.getPredicates(m_sql);
				List<String> whereClause = m_selPredicateList;
				if (whereClause != null && whereClause.size() > 0) {
					reptSQL += "WHERE ";
					ArrayList<String> whereList = new ArrayList<String>();
					whereList.addAll(whereClause);
					if (whereList.size() == 1) {
						reptSQL += whereList.get(0) + " " + m_lineSeparator;
					}
				}
				
//				List<String> groupBy = SqlRecognizer.getGroupBy(m_sql);
				List<String> groupBy = m_selGroupByList;
				if (groupBy != null && groupBy.size() > 0) {
					reptSQL += "GROUP BY ";
					Iterator<String> iter = groupBy.iterator();
					String grpbyStr = "";
					while(iter.hasNext()) {
						if(grpbyStr.length() == 0) {
							grpbyStr = iter.next();
						}
						else {
							grpbyStr += ", " + iter.next();
						}
					}
					reptSQL += grpbyStr + " " + m_lineSeparator;
				}
				
				if (i < funcs.size() - 1) {
					//					reptSQL += "UNION ALL" + m_lineSeparator;
					reptSQL += "UNION" + m_lineSeparator;
				}

			}
		}

		sql += reptSQL.length() > 0 && sql.length() > 0 ? m_lineSeparator
				+ "UNION" + m_lineSeparator + reptSQL
				: reptSQL.length() > 0 ? reptSQL : "";
				return sql;
	}

	private String generateSQLGroupby(ArrayList<String> reportCols, boolean hasFuncExpr) {
		String groupBy = "";
		String grpCols = "";
		if (reportCols.size() > 0) {
			groupBy = "GROUP BY GROUPING SETS(";
			StrJoin allgrpSet = null;
			for (int i = 0; i < reportCols.size(); i++) {
//				String[] colalias = reportCols.get(i).split("\\s+");
//				String repCol = colalias.length > 1 ? colalias[1] : reportCols.get(i);
				String repCol = getAlias(reportCols.get(i));
				ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext.getStoredComputeCmds().get(repCol);
				if(mapList != null) {
					StrJoin grpSet = new StrJoin(", ", "(", ")");
					if (i == 0) {
						if (hasFuncExpr)
							grpSet.add(getAliasedOrOrigCols(reportCols).get(i));
						else
							grpSet.add(getOrigColsStripAlias(reportCols).get(i));
					} else {
						for (int j = 0; j <= i; j++) {
							if (hasFuncExpr)
								grpSet.add(getAliasedOrOrigCols(reportCols).get(j));
							else
								grpSet.add(getOrigColsStripAlias(reportCols).get(j));
						}
					}

					if(hasFuncExpr) {
						allgrpSet = grpSet;
					}
					else {
						if (i + 1 < reportCols.size())
							grpCols += grpSet.toString() + ", ";
						else {
							grpCols += grpSet.toString();
						}
					}
				}
			}
			
			if(allgrpSet != null && m_computecolumns.size() > 0) {
//				for (int i = 0; i < m_computecolumns.size(); i++) {
////					StrJoin grpSet = new StrJoin(", ", "(", ")");
//					String cmpCol = m_computecolumns.get(i);
//					if (isCompColFuncExpr(cmpCol)) {
//						allgrpSet.add(getAlias(cmpCol));
////						if(grpCols.endsWith(", "))
////						    grpCols += grpSet.toString();
////						else 
////							grpCols += ", " + grpSet.toString();
//					}
//				}
				grpCols += allgrpSet.toString();
			}
			
			
			groupBy += grpCols + ")";
		}
		
		
		
		return groupBy;
	}

	private String generateSQLGroupby1(ArrayList<String> reportCols) {
		String groupBy = "";
		String grpCols = "";
		if (reportCols.size() > 0) {
			groupBy = "GROUP BY GROUPING SETS(";
			StrJoin grpSet = new StrJoin(", ", "(", ")");
			for (int i = 0; i < reportCols.size(); i++) {
				grpSet.add(getOrigColsStripAlias(reportCols).get(i));
			}
			groupBy += grpSet + ")";
		}
		return groupBy;
	}

	/**
	 * We have to deal with compute command like COMPUTE AVG MAX OF SALARY ON
	 * DEPARTMENT_NAME in this case more than one compute function is applied to
	 * a Compute column.
	 * 
	 * @param reportCols
	 * @param cmptCols
	 * @return
	 */
	private int numfunctionsUsedOnComputeCol(ArrayList<String> reportCols,
			ArrayList<String> cmptCols) {
		int numofFuncs = 0;
		ArrayList<String> repColsAlias = getAliasedOrOrigCols(reportCols);
		for (int i = 0; i < repColsAlias.size(); i++) {
			String repCol = repColsAlias.get(i);
			ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext.getStoredComputeCmds().get(repCol);
			if(mapList != null) {
				Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList.iterator();
				while (iter.hasNext()) {
					HashMap<String, String> func = null;
					TreeMap<String, HashMap<String, String>> col_cmptfunc = iter.next();
					Iterator<String> ccIter = cmptCols.iterator();
					while (ccIter.hasNext()) {
						String cmptCol = ccIter.next();
						func = col_cmptfunc.get(cmptCol) != null ? col_cmptfunc.get(cmptCol) : col_cmptfunc.get(getAlias(cmptCol));
						if (func != null)
							break;
					}
					if (func != null) {
						int keys = func.size();
						if (keys > numofFuncs) {
							numofFuncs = keys;
						}
					}
				}
			}
		}
		return numofFuncs;
	}
	
	private int numfunctionsUsedOnReportCol(ArrayList<String> reportCols,
			ArrayList<String> cmptCols) {
		int numofFuncs = 0;
		ArrayList<String> repColsAlias = getAliasedOrOrigCols(reportCols);
		for (int i = 0; i < repColsAlias.size(); i++) {
			String repCol = repColsAlias.get(i);
			if(repCol.equals("report")) {
				ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext.getStoredComputeCmds().get(repCol);
				if(mapList != null) {
					Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList.iterator();
					while (iter.hasNext()) {
						HashMap<String, String> func = null;
						TreeMap<String, HashMap<String, String>> col_cmptfunc = iter.next();
						Iterator<String> ccIter = cmptCols.iterator();
						while (ccIter.hasNext()) {
							String cmptCol = ccIter.next();
							func = col_cmptfunc.get(cmptCol) != null ? col_cmptfunc.get(cmptCol) : col_cmptfunc.get(getAlias(cmptCol));
							if (func != null)
								break;
						}
						if (func != null) {
							int keys = func.size();
							if (keys > numofFuncs) {
							   numofFuncs = keys;
							}
						}
					}
				}
			}
		}
		return numofFuncs;
	}
	
	private ArrayList<String> getFunctionsUsedForReport(ArrayList<String> reportCols,
			ArrayList<String> cmptCols) {
		ArrayList<String> funcs = new ArrayList<String>();
		ArrayList<String> repColsAlias = getAliasedOrOrigCols(reportCols);
		for (int i = 0; i < repColsAlias.size(); i++) {
			String repCol = repColsAlias.get(i);
			if(repCol.equals("report")) {
				ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext.getStoredComputeCmds().get(repCol);
				if(mapList != null) {
//					Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList.iterator();
//					while (iter.hasNext()) {
//						HashMap<String, String> func = null;
//						TreeMap<String, HashMap<String, String>> col_cmptfunc = iter.next();
//						Iterator<String> ccIter = cmptCols.iterator();
//						while (ccIter.hasNext()) {
//							String cmptCol = ccIter.next();
//							func = col_cmptfunc.get(cmptCol) != null ? col_cmptfunc.get(cmptCol) : col_cmptfunc.get(getAlias(cmptCol));
//							if (func != null)
//								break;
//						}
//						if (func != null) {
//							Iterator<String> funcIter = func.keySet().iterator();
//							while(funcIter.hasNext()) {
//								String funct = funcIter.next();
//								if(!funcs.contains(funct)) {
//									funcs.add(funct);
//								}
//							}
//						}
//					}
					
					Iterator<String> ccIter = cmptCols.iterator();
					while (ccIter.hasNext()) {
						String cmptCol = ccIter.next();
						Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList.iterator();
						HashMap<String, String> func = null;
						while (iter.hasNext()) {
							TreeMap<String, HashMap<String, String>> col_cmptfunc = iter.next();
							func = col_cmptfunc.get(cmptCol) != null ? col_cmptfunc.get(cmptCol) : col_cmptfunc.get(getAlias(cmptCol));
							if (func != null) break;
						}
						if (func != null) {
							Iterator<String> funcIter = func.keySet().iterator();
							while(funcIter.hasNext()) {
								String funct = funcIter.next();
								if(!funcs.contains(funct)) {
									funcs.add(funct);
								}
							}
						}
					}
					
				}
			}
		}
		
		return funcs;
	}

	/**
	 * Generates CASE expression
	 * 
	 * @param reportCols
	 * @param cmptCols
	 * @param type
	 *            valid values are "Expression" or "Function"
	 * @param funcKeyIdx
	 * @return
	 */
	private String generateSQLCaseExpressions(ArrayList<String> reportCols,
			ArrayList<String> cmptCols, String type, int funcKeyIdx, boolean hasFuncExpr) {
		String totcaseStmt = "";

		for (int k = 0; k < cmptCols.size(); k++) {
			ArrayList<String> caseExprs = new ArrayList<String>();
			if (reportCols.size() > 0 && !findString(reportCols, "report")) {
				String grouping_id = "";
				if(hasFuncExpr) {
					grouping_id = "WHEN GROUPING_ID(" + join(", ", getAliasedOrOrigCols(reportCols)) + ")";
				}
				else {
				   grouping_id = "WHEN GROUPING_ID(" + join(", ", getOrigColsStripAlias(reportCols)) + ")";
				}
				String condition = "";
				String caseStmt = "";
				for (int i = 0; i < reportCols.size(); i++) {
//					String[] colalias = reportCols.get(i).split("\\s+");
//					String repCol = colalias.length > 1 ? colalias[1] : reportCols.get(i);
					String repCol = getAlias(reportCols.get(i));
					ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext.getStoredComputeCmds().get(repCol);
					if(mapList != null) {
						Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList.iterator();
						while (iter.hasNext()) {
							TreeMap<String, HashMap<String, String>> col_cmptfunc = iter.next();
							HashMap<String, String> func = col_cmptfunc.get(cmptCols.get(k)) != null ? col_cmptfunc.get(cmptCols.get(k)) : col_cmptfunc.get(getAlias(cmptCols.get(k)));
							if (func != null) {
								ArrayList<String> funcKeys = new ArrayList<String>();
								funcKeys.addAll(func.keySet());

								String funcKey = "";
								for (int j = funcKeyIdx; j >= 0; j--) {
									if (j < funcKeys.size() && funcKeys.get(j) != null) {
										funcKey = funcKeys.get(j);
										break;
									}
								}

								condition = grouping_id + " = BIN_TO_NUM(";
//								condition += genBitVectorStr(i, reportCols.size()) + ")";
								condition += genBitVectorforBreakCols(i, reportCols) + ")";
								condition += " THEN ";
								if (type.equalsIgnoreCase("Expression")) {
									String colOrAlias = cmptCols.get(k);
									if(isCompColFuncExpr(colOrAlias)) {
										colOrAlias = getAliasFromColumn(colOrAlias);
									}
									if (funcKey.equalsIgnoreCase("number")) {
										condition += "count(nvl(" + colOrAlias + ", 0))" + m_lineSeparator;
									}
									else {
										condition += funcKey + "(" + colOrAlias + ")" + m_lineSeparator;
									}
								}
								else if (type.equalsIgnoreCase("Function")) {
									condition += func.get(funcKey) + m_lineSeparator;
								}
								caseExprs.add(condition);
							}
						}
					}
				}

				if (caseExprs.size() > 0) {

					caseStmt = "CASE " + m_lineSeparator;

					for (String expr : caseExprs) {
						caseStmt += "\t" + expr;
					}

					caseStmt += "END AS ";
					if (type.equalsIgnoreCase("Expression")) {
						caseStmt += optionalQuote(getAlias(cmptCols.get(k)));
					}
					else {
						caseStmt += optionalQuote(getAlias(cmptCols.get(k)) + "_CMPFN");
					}

					totcaseStmt += caseStmt;
				}

			}

			if (k < cmptCols.size() - 1 && caseExprs.size() > 0) {
				totcaseStmt += ", " + m_lineSeparator;
			}
		}

		int lidx = totcaseStmt.lastIndexOf(", " + m_lineSeparator);
		if (totcaseStmt.endsWith(", " + m_lineSeparator) && lidx > 0) {
			totcaseStmt = totcaseStmt.substring(0, lidx);
		}

		return totcaseStmt;

	}

	private String generateSQLCaseExpressions(String reportCol,
			ArrayList<String> reportCols, ArrayList<String> cmptCols,
			String type, int funcKeyIdx) {
		String totcaseStmt = "";

		for (int k = 0; k < cmptCols.size(); k++) {
			ArrayList<String> caseExprs = new ArrayList<String>();
			if (reportCols.size() > 0 && !findString(reportCols, "report")) {
				String grouping_id = "WHEN GROUPING_ID(" + join(", ", getOrigColsStripAlias(reportCols)) + ")";
				String condition = "";
				String caseStmt = "";
				for (int i = 0; i < reportCols.size(); i++) {
					if (reportCol.equalsIgnoreCase(reportCols.get(i))) {
						String[] colalias = reportCols.get(i).split("\\s+");
						String repCol = colalias.length > 1 ? colalias[1].replaceAll("\"", "") : reportCols.get(i);
						ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext.getStoredComputeCmds().get(repCol);
						if(mapList != null) {
							Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList.iterator();
							while (iter.hasNext()) {
								TreeMap<String, HashMap<String, String>> col_cmptfunc = iter.next();
								HashMap<String, String> func = col_cmptfunc.get(cmptCols.get(k)) != null ? col_cmptfunc.get(cmptCols.get(k)) : col_cmptfunc.get(getAlias(cmptCols.get(k)));
								if (func != null) {
									ArrayList<String> funcKeys = new ArrayList<String>();
									funcKeys.addAll(func.keySet());

									String funcKey = "";
									for (int j = funcKeyIdx; j >= 0; j--) {
										if (j < funcKeys.size() && funcKeys.get(j) != null) {
											funcKey = funcKeys.get(j);
											break;
										}
									}

									condition = grouping_id + " = BIN_TO_NUM(";
									condition += genBitVectorStr(reportCols.size()) + ")";
									condition += " THEN ";
									if (type.equalsIgnoreCase("Expression")) {
										if (funcKey.equalsIgnoreCase("number")) {
											condition += "count(nvl(" + cmptCols.get(k) + ", 0))" + m_lineSeparator;
										}
										else {
											condition += funcKey + "(" + cmptCols.get(k) + ")" + m_lineSeparator;
										}
									}
									else if (type.equalsIgnoreCase("Function")) {
										condition += func.get(funcKey) + m_lineSeparator;
									}
									caseExprs.add(condition);
								}
							}
						}
					}
				}
				
				
				if(caseExprs.size() > 0) {
					caseStmt = "CASE " + m_lineSeparator;
	
					for (String expr : caseExprs) {
						caseStmt += "\t" + expr;
					}
	
					caseStmt += "END AS ";
				}
				else {
					caseStmt += "NULL AS ";
				}
				if (type.equalsIgnoreCase("Expression")) {
					caseStmt += optionalQuote(getAlias(cmptCols.get(k)));
				}
				else {
					caseStmt += optionalQuote(getAlias(cmptCols.get(k)) + "_CMPFN");
				}

				totcaseStmt += caseStmt;

			}

			if (k < cmptCols.size() - 1) {
				totcaseStmt += ", " + m_lineSeparator;
			}
		}

		return totcaseStmt;

	}
	
	private String optionalQuote(String selItem) {
		String qstr = selItem;
		Pattern p = Pattern.compile("\\s|\\p{Punct}");
		Matcher matcher = p.matcher(selItem);
		boolean found = matcher.find();
		if(found) {
			qstr = '"' + qstr + '"';
		}
		return qstr;
	}

	private ArrayList<String> getOrigColsStripAlias(ArrayList<String> reportCols) {
		ArrayList<String> colsNoAlias = new ArrayList<String>();
		for (String colNoAlias : reportCols) {
			String[] vals = colNoAlias.split("\\s+");
			String col = vals.length > 1 ? vals[0] : colNoAlias;
			colsNoAlias.add(col);
		}
		return colsNoAlias;
	}

	private ArrayList<String> genOrigColsWithDecode(ArrayList<String> reportCols) {
		ArrayList<String> decodeCols = new ArrayList<String>();
		ArrayList<String> colNoAlias = getOrigColsStripAlias(reportCols);
		for (String col : colNoAlias) {
			String genDecode = "DECODE(GROUPING(" + col + "), 1, " + col + ", NULL, '<NULL>', " + col + ")";   
			decodeCols.add(genDecode);
		}
		return decodeCols;
	}
	
	private ArrayList<String> genAliasColsWithDecode(ArrayList<String> reportCols) {
		ArrayList<String> decodeCols = new ArrayList<String>();
		ArrayList<String> colNoAlias = getAliasedOrOrigCols(reportCols);
		for (String col : colNoAlias) {
			String genDecode = "DECODE(GROUPING(" + col + "), 1, " + col + ", NULL, '<NULL>', " + col + ")";   
			decodeCols.add(genDecode);
		}
		return decodeCols;
	} 

	private ArrayList<String> getAliasedOrOrigCols(ArrayList<String> reportCols) {
		ArrayList<String> colsUseAlias = new ArrayList<String>();
//		for (String colUseAlias : reportCols) {
//			String[] vals = colUseAlias.split("\\s+");
//			String col = vals.length > 2 ? vals[2] : vals.length > 1 ? vals[1]
//					: colUseAlias;
//			colsUseAlias.add(col.replaceAll("\"", ""));
//		}
		
		for (String colUseAlias : reportCols) {
			String[] vals = colUseAlias.split("\\s+");
			String col = vals.length > 1 ? vals[vals.length - 1].replaceAll("\"", "") : vals[0];
			colsUseAlias.add(col);
		}
		return colsUseAlias;
	}

	private ArrayList<String> filterList(ArrayList<String> reportCols,
			String filter) {
		ArrayList<String> colsFiltered = new ArrayList<String>();
		for (String col : reportCols) {
			if (!col.equalsIgnoreCase(filter)) {
				colsFiltered.add(col);
			}
		}
		return colsFiltered;
	}

	private boolean breakColandSelColOrderMatch() {
		boolean orderMatch = false;
		ArrayList<String> repCols = new ArrayList<String>();
		ArrayList<String> breakColsinSel = new ArrayList<String>();
		ArrayList<String> nonBreakColsinSel = new ArrayList<String>();
		ArrayList<String> breakColList = new ArrayList<String>();
		ArrayList<String> breakAllColList = new ArrayList<String>();

		if (m_scriptRunnerContext.getStoredBreakCmds().size() > 0) {
			breakAllColList.addAll(m_scriptRunnerContext.getStoredBreakCmds()
					.keySet());
			ArrayList<String> tempList = filterList(breakAllColList, "report");
			// also filter "row"
			breakColList.addAll(filterList(tempList, "row"));
			ArrayList<String> selList = new ArrayList<String>();
//			selList.addAll(SqlRecognizer.getColumns(m_sql));
			selList.addAll(m_selColumnList);
			// Bug 21528592
			if(selList.size() == 0) {
				orderMatch = true;
				return orderMatch;
			}
			for (int i = 0; i < selList.size(); i++) {
				String selCol = selList.get(i);
				boolean bFound = false;
				for (int j = 0; j < breakColList.size(); j++) {
					String col = breakColList.get(j);
					String colandalias[] = selCol.split("\\s+");
					String alias = colandalias.length > 2 ? colandalias[2].replaceAll("\"", "")
							: colandalias.length > 1 ? colandalias[1].replaceAll("\"", "") : selCol;
							if (col.equalsIgnoreCase(selCol)
									|| col.equalsIgnoreCase(alias)) {
								bFound = true;
								break;
							} else {
								bFound = false;
							}
				}

				if (!bFound) {
					nonBreakColsinSel.add(selCol);
				}
			}

			if (selList.size() == breakColList.size()
					&& nonBreakColsinSel.size() == 0) {
				// that means all the selList and breakColumns are exactly the
				// same.
				orderMatch = true;
			}

			// Did this because the Select List order may not be as
			// consecutive as break column list order. But
			// filter out to get the order of break columns in
			// select list. Preserve the order of columns
			// in the select list.
			for (String selCol : nonBreakColsinSel) {
				if (breakColsinSel.size() == 0)
					breakColsinSel = filterList(selList, selCol);
				else
					breakColsinSel = filterList(breakColsinSel, selCol);
			}
		}

		// The breakColList and breakColsinSel lists can be same
		// but may not be in the same order as breakColList.
		if (breakColList.size() == breakColsinSel.size()) {
			for (int i = 0; i < breakColList.size(); i++) {
				orderMatch = getAlias(breakColList.get(i)).equalsIgnoreCase(
						getAlias(breakColsinSel.get(i)));
				if (!orderMatch)
					break;
			}
		}

		return orderMatch;
	}

	/**
	 * This is when "REPORT" is used.
	 * 
	 * @param reportCols
	 * @param cmptCols
	 * @param funcKeyIdx
	 * @return
	 */
	private String generateSQLForReportExpr(ArrayList<String> reportCols,
			ArrayList<String> cmptCols, int funcKeyIdx) {
		String colExpr = "";
		if (reportCols.size() > 0 && findString(reportCols, "report")) {
			for (int k = 0; k < cmptCols.size(); k++) {
				for (int i = 0; i < reportCols.size(); i++) {
					if (reportCols.get(i).equalsIgnoreCase("report")) {
						ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext
								.getStoredComputeCmds().get(reportCols.get(i));
						Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList
								.iterator();
						String expr = "";
						while (iter.hasNext()) {
							expr = "";
							TreeMap<String, HashMap<String, String>> col_cmptfunc = iter
									.next();
							HashMap<String, String> func = col_cmptfunc
									.get(cmptCols.get(k)) != null ? col_cmptfunc
											.get(cmptCols.get(k)) : col_cmptfunc
											.get(getAlias(cmptCols.get(k)));
											ArrayList<String> funcKeys = new ArrayList<String>();
											boolean bfound = false;
											if (func != null) {
												funcKeys.addAll(func.keySet());
												String funcKey = "";
												// Bug 20487969
												if(funcKeys.size() > funcKeyIdx) {
													for (int j = funcKeyIdx; j >= 0; j--) {
														funcKey = funcKeys.get(j);
														if (funcKey != null) {
															bfound = true;
															break;
														}
													}
												}

												if(funcKey.length() > 0) 
													expr += func.get(funcKey) + " ";
												if (bfound)
													break;
											}
						}
						if(expr.length() > 0) {
							colExpr += expr;
						}
						else {
							colExpr += "NULL" + " ";
						}
					}
				}
				colExpr += getAlias(cmptCols.get(k)) + "_CMPFN";

				if (k < cmptCols.size() - 1) {
					colExpr += ", " + m_lineSeparator;
				}
			}
		}
		return colExpr;
	}
	
	private String generateSQLForReportExpr(ArrayList<String> reportCols,
			ArrayList<String> cmptCols, String funct) {
		String colExpr = "";
		if (reportCols.size() > 0 && findString(reportCols, "report")) {
			for (int k = 0; k < cmptCols.size(); k++) {
				for (int i = 0; i < reportCols.size(); i++) {
					if (reportCols.get(i).equalsIgnoreCase("report")) {
						ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext
								.getStoredComputeCmds().get(reportCols.get(i));
						Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList
								.iterator();
						String expr = "";
						while (iter.hasNext()) {
							expr = "";
							TreeMap<String, HashMap<String, String>> col_cmptfunc = iter
									.next();
							HashMap<String, String> func = col_cmptfunc
									.get(cmptCols.get(k)) != null ? col_cmptfunc
											.get(cmptCols.get(k)) : col_cmptfunc
											.get(getAlias(cmptCols.get(k)));
											ArrayList<String> funcKeys = new ArrayList<String>();
											boolean bfound = false;
											if (func != null) {
												funcKeys.addAll(func.keySet());
												String funcKey = "";
												if(func.get(funct) != null) {
													expr += func.get(funct) + " ";
													break;
												}											}
						}
						if(expr.length() > 0) {
							colExpr += expr;
						}
						else {
							colExpr += "NULL" + " ";
						}
					}
				}
				colExpr += getAlias(cmptCols.get(k)) + "_CMPFN";

				if (k < cmptCols.size() - 1) {
					colExpr += ", " + m_lineSeparator;
				}
			}
		}
		return colExpr;
	}

	/**
	 * This is when "REPORT" is used.
	 * 
	 * @param reportCols
	 * @param cmptCols
	 * @param funcKeyIdx
	 * @return
	 */
	private String generateComputeForColumn(ArrayList<String> reportCols,
			ArrayList<String> cmptCols, int funcKeyIdx) {
		String cmptFnExpr = "";
		if (reportCols.size() > 0 && findString(reportCols, "report")) {
			for (int k = 0; k < cmptCols.size(); k++) {
				for (int i = 0; i < reportCols.size(); i++) {
					if (reportCols.get(i).equalsIgnoreCase("report")) {
						ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext
								.getStoredComputeCmds().get(reportCols.get(i));
						Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList
								.iterator();
						boolean bFound = false;
						String expr = "";
						while (iter.hasNext()) {
							expr = "";
							TreeMap<String, HashMap<String, String>> col_cmptfunc = iter
									.next();
							HashMap<String, String> func = col_cmptfunc
									.get(cmptCols.get(k)) != null ? col_cmptfunc
											.get(cmptCols.get(k)) : col_cmptfunc
											.get(getAlias(cmptCols.get(k)));
											ArrayList<String> funcKeys = new ArrayList<String>();
											if (func != null) {
												funcKeys.addAll(func.keySet());
												String funcKey = "";
												// Bug 20487969
												if(funcKeys.size() > funcKeyIdx) {
													for (int j = funcKeyIdx; j >= 0; j--) {
														funcKey = funcKeys.get(j);
														if (funcKey != null) {
															bFound = true;
															break;
														}
													}
												}

												if(funcKey.length() > 0) {
													if (funcKey.equalsIgnoreCase("number")) {
														expr = "count(nvl(" + cmptCols.get(k) + ", 0))" + " AS " + getAlias(cmptCols.get(k));
													}
													else {
														expr = funcKey + "(" + cmptCols.get(k) + ")" + " AS " + getAlias(cmptCols.get(k));
													}
												}
												if (bFound) {
													break;
												}
											}
						}
						if(expr.length() > 0) {
							cmptFnExpr += expr;
						}
						else {
							cmptFnExpr += "NULL" + " AS " + getAlias(cmptCols.get(k));	
						}
					}
				}

				if (k < cmptCols.size() - 1) {
					cmptFnExpr += ", " + m_lineSeparator;
				}
				
			}
		}
		return cmptFnExpr;
	}
	
	private String generateComputeForColumn(ArrayList<String> reportCols,
			ArrayList<String> cmptCols, String funct) {
		String cmptFnExpr = "";
		if (reportCols.size() > 0 && findString(reportCols, "report")) {
			for (int k = 0; k < cmptCols.size(); k++) {
				for (int i = 0; i < reportCols.size(); i++) {
					if (reportCols.get(i).equalsIgnoreCase("report")) {
						ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext
								.getStoredComputeCmds().get(reportCols.get(i));
						Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList
								.iterator();
						boolean bFound = false;
						String expr = "";
						while (iter.hasNext()) {
							expr = "";
							TreeMap<String, HashMap<String, String>> col_cmptfunc = iter
									.next();
							HashMap<String, String> func = col_cmptfunc
									.get(cmptCols.get(k)) != null ? col_cmptfunc
											.get(cmptCols.get(k)) : col_cmptfunc
											.get(getAlias(cmptCols.get(k)));
											ArrayList<String> funcKeys = new ArrayList<String>();
											if (func != null) {
												funcKeys.addAll(func.keySet());
												String funcKey = "";
												if(func.get(funct) != null) {
													funcKey = funct;
													if (funcKey.equalsIgnoreCase("number")) {
														expr = "count(nvl(" + cmptCols.get(k) + ", 0))" + " AS " + getAlias(cmptCols.get(k));
													}
													else {
														expr = funcKey + "(" + cmptCols.get(k) + ")" + " AS " + getAlias(cmptCols.get(k));
													}
													break;
												}
											}
						}
						if(expr.length() > 0) {
							cmptFnExpr += expr;
						}
						else {
							cmptFnExpr += "NULL" + " AS " + getAlias(cmptCols.get(k));	
						}
					}
				}

				if (k < cmptCols.size() - 1) {
					cmptFnExpr += ", " + m_lineSeparator;
				}
				
			}
		}
		return cmptFnExpr;
	}
	
	private String genBitVectorforBreakCols(int index, ArrayList<String> reportCols) {
		String ret = "";
		
		if(m_breakandselorderMatch) {
			ret = genBitVectorStr(index, reportCols.size());
		}
		else {
			
			String repCol = reportCols.get(index);
			int rank = m_brkcol_to_rank.get(repCol);
			if(rank > 0) {
			   int selIdx = getSelColIdFromBreakColId(rank);
			   if (selIdx >= 1) {
			       ret = genBitVectorStr(selIdx-1, reportCols.size());
			   }
			}
		}
		return ret;
	}

	/**
	 * Generates Bit Vectors. Used to verify the GROUPING_ID values
	 * 
	 * @param index
	 * @param length
	 * @return
	 */
	private String genBitVectorStr(int index, int length) {
		String ret = "";
		String[] val = new String[length];
		Arrays.fill(val, "1");
		for (int i = 0; i < length; i++) {
			if (i <= index) {
				val[i] = "0";
			}
		}
		ret = join(", ", val);
		return ret;
	}

	private String genBitVectorStr(int length) {
		String ret = "";
		String[] val = new String[length];
		Arrays.fill(val, "0");
		ret = join(", ", val);
		return ret;
	}

	private int[] groupingLevels(ArrayList<String> reportCols) {
		int[] grpLevels = null;
		int length = reportCols.size();
		String[] val = new String[length];
		Arrays.fill(val, "1");
		int k = 0;
		String binStr = "";
		ArrayList<Integer> decimals = new ArrayList<Integer>();
		for (int i = 0; i < length; i++) {
			if (i == 0) {
				val[i] = "0";
			} 
			else {
				for (int j = 0; j <= i; j++) {
					val[j] = "0";
				}
			}
			binStr = join("", val);
			int decimal = Integer.parseInt(binStr, 2);
			decimals.add(decimal);
		}
		grpLevels = new int[decimals.size()];
		for(int i = 0; i < decimals.size(); i++) {
			Integer iVal = decimals.get(i);
			grpLevels[i] = iVal.intValue();
		}
		Arrays.sort(grpLevels);
		return grpLevels;
	}
	
	private HashMap<Integer, Object[]> groupLevels(ArrayList<String> reportCols) {
		HashMap<Integer, Object[]> lvltocol = new HashMap<Integer, Object[]>();
		int length = reportCols.size();
		String[] val = new String[length];
		Object[] repCols = null;
		repCols = addRow(repCols, null);
		Arrays.fill(val, "1");
		int k = 0;
		String binStr = "";
		String repCol = "";
		int lvl = -1;
		for (int i = 0; i < length; i++) {
			val[i] = "0";
			repCol = reportCols.get(i);
			repCols = addRow(repCols, repCol);
			binStr = join("", val);
			lvl = Integer.parseInt(binStr, 2);
			lvltocol.put(lvl, repCols);
		}
		
		List lvlkeys = new ArrayList(lvltocol.keySet());
		Collections.sort(lvlkeys);
		
		HashMap<Integer, Object[]> sortedlvltocol = new HashMap<Integer, Object[]>();
		for(int i = 0; i < lvlkeys.size(); i++) {
			int lvlkey = (int) lvlkeys.get(i);
			sortedlvltocol.put(lvlkey, lvltocol.get(lvlkey));
		}
		if(sortedlvltocol.size() > 0)
		   lvltocol = sortedlvltocol;
		
		return lvltocol;
	}

	private String findColumnfromComputelabel(ArrayList<String> reportCols,
			String label) {
		String column = "";
		boolean found = false;
		ArrayList<String> repColsWithAlias = getAliasedOrOrigCols(reportCols);
		for (int i = 0; i < repColsWithAlias.size(); i++) {
			column = repColsWithAlias.get(i);
			ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext
					.getStoredComputeCmds().get(column);
			Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList
					.iterator();
			while (iter.hasNext()) {
				TreeMap<String, HashMap<String, String>> col_cmptfunc = iter
						.next();
				Iterator<String> citer = col_cmptfunc.keySet().iterator();
				while (citer.hasNext()) {
					String key = citer.next();
					HashMap<String, String> cmpt_func = col_cmptfunc.get(key);
					Iterator<String> funcs = cmpt_func.keySet().iterator();
					while (funcs.hasNext()) {
						String func = funcs.next();
						String lbl = cmpt_func.get(func);
						lbl = lbl.replaceAll("'", "");
						found = label.equalsIgnoreCase(lbl);
						if (found) {
							break;
						}
					}
					if (found) {
						break;
					}
				}
				if (found) {
					break;
				}
			}
			if (found) {
				break;
			}
		}
		return column;
	}

	private void populateBreakCmdInfo() {
		LinkedHashMap<String, ArrayList<String>> storedBrkCmds = m_scriptRunnerContext.getStoredBreakCmds();
		HashMap<String, ArrayList<String>> storedColsCmds = m_scriptRunnerContext.getStoredFormatCmds();
		if (m_columns != null && storedBrkCmds.size() > 0) {
			parseSQL();
			for (int i = 1; i < m_columns.length; i++) {
				String column = m_columns[i];
				Iterator<String> iter = storedBrkCmds.keySet().iterator();
				int rank = 0;
				while (iter.hasNext()) {
					String brkcol = iter.next();
					if(brkcol.equals("report")) continue;
					String aliascol = getAlias(brkcol);
					rank++;
					if (aliascol != null && (column.equalsIgnoreCase(aliascol) || column.equalsIgnoreCase(brkcol) || column.equalsIgnoreCase(getHeading(aliascol, storedColsCmds)) || 
							(aliascol.length() > m_colsizes[i] ? aliascol.substring(0, m_colsizes[i]) : aliascol).equalsIgnoreCase(column))) {
						m_breakcolumns.add(brkcol);
						m_column_to_brkcol.put(column, brkcol);
						m_brkcol_to_nodup.put(brkcol, storedBrkCmds.get(brkcol).contains("nodup"));
						m_brkcol_to_rank.put(brkcol, Integer.valueOf(rank));
						break;
					}
				}
			}
		}
		if(Break.hasRowElement(storedBrkCmds)) {
			m_breakcolumns.add("row");
		}
		m_breakandselorderMatch = breakColandSelColOrderMatch();
	}

	private void populateBreakColumnsToComputeLabel() {
		if (m_brkcmptcolumns.size() > 0) {
			for (int i = 0; i < m_brkcmptcolumns.size(); i++) {
				String column = getAlias(m_brkcmptcolumns.get(i));
				ArrayList<TreeMap<String, HashMap<String, String>>> mapList = m_scriptRunnerContext.getStoredComputeCmds().get(column);
				if(mapList != null) {
					Iterator<TreeMap<String, HashMap<String, String>>> iter = mapList.iterator();
					while (iter.hasNext()) {
						TreeMap<String, HashMap<String, String>> col_cmptfunc = iter.next();
						Iterator<String> citer = col_cmptfunc.keySet().iterator();
						while (citer.hasNext()) {
							String key = citer.next();
							HashMap<String, String> cmpt_func = col_cmptfunc.get(key);
							Iterator<String> funcs = cmpt_func.keySet().iterator();
							while (funcs.hasNext()) {
								String func = funcs.next();
								String lbl = cmpt_func.get(func);
								lbl = lbl.replaceAll("'", "");
								m_brkcol_to_cmplbl.put(column, lbl);
								m_brkcol_to_cmpfn.put(column, func);

								HashMap cmpfn_to_lbl = m_brkcol_to_cmpfnlbl.get(column);
								if(cmpfn_to_lbl == null) {
									cmpfn_to_lbl = new HashMap<String, String>();
								}
								cmpfn_to_lbl.put(func, lbl);
								m_brkcol_to_cmpfnlbl.put(column, cmpfn_to_lbl);

							}
						}
					}
				}
			}
		}
	}

	private void mapSelColstoBreakColumns() {
		int size = m_brkcmptcolumns.size();
		HashMap<String, ArrayList<String>> storedColsCmds = m_scriptRunnerContext.getStoredFormatCmds();
		for (int i = 1; i < m_columns.length; i++) {
			String col = m_columns[i];
			for (int j = 0; j < size; j++) {
				String brkCol = m_brkcmptcolumns.get(j);
				String aliascol = getAlias(brkCol);
				if (col.equalsIgnoreCase(aliascol) || col.equalsIgnoreCase(getHeading(aliascol, storedColsCmds)) ||
						(aliascol.length() > m_colsizes[i] ? aliascol.substring(0, m_colsizes[i]) : aliascol).equalsIgnoreCase(col)) {
					m_column_to_brkcmptcol.put(col, aliascol);
					break;
				}
			}
		}
	}

	private void mapSelColstoComputeColumns() {
		int size = m_computecolumns.size();
		HashMap<String, ArrayList<String>> storedColsCmds = m_scriptRunnerContext.getStoredFormatCmds();
		for (int i = 1; i < m_columns.length; i++) {
			String col = m_columns[i];
			for (int j = 0; j < size; j++) {
				String cmpCol = m_computecolumns.get(j);
				String aliascol = getAlias(cmpCol);
				if (col.equalsIgnoreCase(aliascol) || col.equalsIgnoreCase(getHeading(aliascol, storedColsCmds))) {
					m_column_to_cmpcol.put(col, cmpCol);
					break;
				}
			}
		}
	}

	/**
	 * This is a copy from JDK 1.8 as 4.1 supports JDK 1.7
	 * 
	 * @param delimiter
	 * @param elements
	 * @return
	 */

	private String join(CharSequence delimiter,
			Iterable<? extends CharSequence> elements) {
		Objects.requireNonNull(delimiter);
		Objects.requireNonNull(elements);
		StrJoin joiner = new StrJoin(delimiter);
		for (CharSequence cs : elements) {
			joiner.add(cs);
		}
		return joiner.toString();
	}

	/**
	 * This is a copy from JDK 1.8 as 4.1 supports JDK 1.7
	 * 
	 * @param delimiter
	 * @param elements
	 * @return
	 */
	public String join(CharSequence delimiter, CharSequence... elements) {
		Objects.requireNonNull(delimiter);
		Objects.requireNonNull(elements);
		// Number of elements not likely worth Arrays.stream overhead.
		StrJoin joiner = new StrJoin(delimiter);
		for (CharSequence cs : elements) {
			joiner.add(cs);
		}
		return joiner.toString();
	}

	/**
	 * This is a copy from JDK 1.8 as 4.1 supports JDK 1.7
	 * 
	 * @author ramprasad.thummala@oracle.com
	 *
	 */
	public final class StrJoin {
		private final String prefix;
		private final String delimiter;
		private final String suffix;
		private String emptyValue;
		private StringBuilder value;

		public StrJoin(CharSequence delimiter) {
			this(delimiter, "", "");
		}

		public StrJoin(CharSequence delimiter, CharSequence prefix,
				CharSequence suffix) {
			Objects.requireNonNull(prefix, "The prefix must not be null");
			Objects.requireNonNull(delimiter, "The delimiter must not be null");
			Objects.requireNonNull(suffix, "The suffix must not be null");
			// make defensive copies of arguments
			this.prefix = prefix.toString();
			this.delimiter = delimiter.toString();
			this.suffix = suffix.toString();
			this.emptyValue = this.prefix + this.suffix;
		}

		private StringBuilder prepareBuilder() {
			if (value != null) {
				value.append(delimiter);
			} else {
				value = new StringBuilder().append(prefix);
			}
			return value;
		}

		public StrJoin add(CharSequence newElement) {
			prepareBuilder().append(newElement);
			return this;
		}

		public String toString() {
			if (value == null) {
				return emptyValue;
			} else {
				if (suffix.equals("")) {
					return value.toString();
				} else {
					int initialLength = value.length();
					String result = value.append(suffix).toString();
					// reset value to pre-append initialLength
					value.setLength(initialLength);
					return result;
				}
			}
		}
	}

	
	private boolean checkIntegralPart(String numberString, int numwidth) {

		int numberStringLen = numberString.length();
		if (numberString.contains(".")) {
			// has decimal value. In this case only match integral value length and not the whole number.
			int decimalIndex = numberString.indexOf(".");
			String integralPartString = numberString.substring(0,decimalIndex);
			if (integralPartString.length() > numwidth) {
				return true;
			}
		}
		else {
			// has no decimal value. 
			if ( numberStringLen > numwidth ) {
				return true;
			}
		}
		return false;
	}

	
	

	private String getNumericFormatedValue(String value, String formatValue) {
	
		String integralPart = null;
		String decimalPart = null;
		String pattern = "";
		DecimalFormat myFormatter = null;
		boolean startsWithDollar = false;
		
		if (value == null) 
			return "";
		value = value.trim();
		if (value.length() == 0) {
			return "";
		}
		
		formatValue = formatValue.toUpperCase();
		if ( value.startsWith("-") && formatValue.length() > 0) {
			formatValue = "-" + formatValue;
		}
		if (formatValue.startsWith("$")) { 
			formatValue = formatValue.substring(1,formatValue.length());
			startsWithDollar = true;
		}
		if ( value.equalsIgnoreCase("Infinity")) {
			return "Inf";
		}
		if ( value.equalsIgnoreCase("-Infinity")) {
			return "-Inf";
		}
		if ( value.equalsIgnoreCase("NaN")) {
			return "Nan";
		}
		if ( value.length() > m_numwidth && m_numwidth < 7) {
			char[] chars = new char[m_numwidth];
			Arrays.fill(chars, '#');
			return String.copyValueOf(chars);
			//return processNumberLessThanNumwidth(value, m_numwidth);
		}
		else if (checkIntegralPart(value, m_numwidth)) {
			// The expected format for exponential is 9.999EEEE.
			pattern = createScientificNotation(value, m_numwidth, formatValue);
		}
		else {
			if (formatValue.length() == 0) {
				if ( value.startsWith("-") ) {
					char[] chars = new char[m_numwidth-3];
					Arrays.fill(chars, '#');
					pattern = "-#." + String.copyValueOf(chars);
				}
				else {
					char[] chars = new char[m_numwidth-2];
					Arrays.fill(chars, '#');
					pattern = "#." + String.copyValueOf(chars);	
				}
			}
			else if (formatValue.matches("\\d*D\\d*") ) { 
				String[] formatValues = formatValue.split("D");
				integralPart = formatValues[0].trim();
				integralPart = integralPart.replaceAll("9", "#");
				decimalPart = formatValues[1].trim();
				decimalPart = decimalPart.replaceAll("9", "0");
				pattern = integralPart + "." + decimalPart;
			}
			else if (formatValue.contains(".")) {
				String[] formatValues = formatValue.split("\\.");
				integralPart = formatValues[0].trim();
				integralPart = integralPart.replaceAll("9", "#");
				decimalPart = formatValues[1].trim();
				decimalPart = decimalPart.replaceAll("9", "0");
				pattern = integralPart + "." + decimalPart;
			}
			else {
				integralPart = formatValue.replaceAll("9", "#");
				pattern = integralPart;
			}
		}
		// Fix scientific number to comply with oracle format.
		if (pattern.contains("E") || pattern.contains("e")) {
			DecimalFormatSymbols SYMBOLS = DecimalFormatSymbols.getInstance();
			double doubleValue = Double.parseDouble(value);
		    if (doubleValue > 1 || doubleValue < -1 || doubleValue == 0) {
		        SYMBOLS.setExponentSeparator("E+");
		    } else {
		        SYMBOLS.setExponentSeparator("E");
		    }
		    myFormatter = new DecimalFormat(pattern,SYMBOLS);
		}
		else {
			myFormatter = new DecimalFormat(pattern);
			myFormatter.setRoundingMode(RoundingMode.HALF_EVEN);
		}
		
		Number num;
		String output = "";
		try {
			num = myFormatter.parse(value);
			output = myFormatter.format(num);
			// $ is format is not processed by DecimalFormatter.
			if (startsWithDollar) { 
				output = "$"+output;
			}
		} catch (ParseException e) {
			Logger.getLogger(getClass().getName()).log(Level.WARNING,
					Messages.getString("DecicmalFormatParseFailure"), e);
		}
	    
		return output;
	}

	// creates a decimal format pattern.
	private String createScientificNotation(String value, int numwidth, String numformat) {
	
		String pattern = null;
		if (numformat.length() == 0) {
			StringBuffer formatStringBuffer = new StringBuffer("0.");
			double valueDouble = Double.parseDouble(value);
			if (valueDouble >= 0) {
				for (int j = 0; j < numwidth - (4 + 2); j++) {
					formatStringBuffer.append("0");
				}
				formatStringBuffer.append("E00");
			}
			if (valueDouble < 0) {
				// 1 for minus sign.
				for (int j = 0; j < numwidth - (4 + 2 + 1); j++) {
					formatStringBuffer.append("0");
				}
				formatStringBuffer.append("E00");
			}
			pattern = formatStringBuffer.toString(); //$NON-NLS-1$
		}
		else {
			// 9.9999EEEE to 0.0000E00
			if (!numformat.toUpperCase().contains("EEEE")) {
				String message = MessageFormat.format(ScriptRunnerDbArb.getString("EXPONENTIAL_FORMAT_EEEE_MISSING"), new Object[] {}) + m_lineSeparator;
				//write(out, message);
			}
			pattern = new String(numformat).replace("9","0");
			pattern = pattern.toUpperCase().replace("EEEE", "E00");
		}
		return pattern;
	}

	private String processTypeAttributesForStruct(Struct obj, Connection conn) throws SQLException, IOException {
		OracleNLSProvider onlsp = (OracleNLSProvider)NLSProvider.getProvider(conn);
		// LRG/SRG For NULL's display NULL for object types.
		onlsp.setNullDisplay("NULL");
		String schemaDotType = obj.getSQLTypeName().toUpperCase();
		final StructDescriptor structDescriptor = StructDescriptor.createDescriptor(schemaDotType, conn);		
		//final ResultSetMetaData metaData = structDescriptor.getMetaData();
		OracleTypeADT oracleTypeADT = (OracleTypeADT) structDescriptor.getPickler();
		
		//need to split 
		String[] schemaDotTypeParts = schemaDotType.split("\\.");
		String schema = schemaDotTypeParts[0].trim();
		String objectTypeName = schemaDotTypeParts[1].trim();
		
		String s = objectTypeName.toUpperCase()+"(";
		
		Object[] data = obj.getAttributes();
				
		for( int k=0; k < data.length ; k++) {
			String attrName = oracleTypeADT.getAttributeName(k+1);
			String attrType = oracleTypeADT.getAttributeType(k+1);
			Object attributeData = data[k];
			
			if(attributeData != null) {
				// Bug 21146178
				if(attrType.equals("RAW")) {
					byte[] attrByte = (byte[])attributeData;
					RAW oraRAW = new RAW(attrByte);
					String orastr = oraRAW.stringValue();
					attributeData = "'" + orastr + "'";
		
				} else if (attrType.equals("BINARY_FLOAT")) { 
					//Bug 25026197 - BINARY_FLOAT COL IN OBJECT IS NOT DISPLAY IN THE SAME FORMAT AS IN SQL*PLUS 
					if (m_numformat.length() > 0) {
						NUMBER fv = new NUMBER(attributeData);
						attributeData = formattedNUMBER(conn, fv, m_numformat, 10);
					} else {
						if (!Float.isNaN((float)attributeData)) {
							NUMBER fv = new NUMBER(attributeData);
							float floatValue = fv.floatingPointRound(m_numwidth).floatValue();
							attributeData = formattedFLOAT(conn, floatValue, m_numformat, 10);
						} else
							attributeData = formattedFLOAT(conn, (float)attributeData, m_numformat,10);
					}
					attrType = "BINARY_FLOAT_TYPE";
				}
				else if(attributeData instanceof STRUCT) {
					// LRG/SRG Testing: Handling Object Types here. 
					attributeData = processTypeAttributesForStruct((STRUCT) attributeData, conn);
				}
				else if(attributeData instanceof ARRAY) {
					// LRG/SRG Testing: Handling Object Types here. 
					attributeData = processTypeAttributesForArray((ARRAY) attributeData, conn);
				}
				
				// Bug 21141601 for Oracle Date/Time & Interval types 
	//			OracleNLSProvider onlsp = new OracleNLSProvider(conn);
				String oradtstr = "";
				if(attributeData instanceof Timestamp) {
					Timestamp ats = (Timestamp)attributeData;
					if(attrType.equals("DATE")) {
						Date attrDate = new Date(ats.getTime());
						DATE oraDate = new DATE(attrDate);
						oradtstr = onlsp.formatDate(oraDate);
					}
					else if(attrType.equals("TIMESTAMP")) {
						TIMESTAMP oraTS = new TIMESTAMP(ats);
						oradtstr =(String) onlsp.getValue(oraTS, FormatType.PREFERRED);
					}
					else if(attrType.equals("TIMESTAMP WITH TZ")) {
						TIMESTAMPTZ oraTSTZ = new TIMESTAMPTZ(conn, ats);
						oradtstr =(String) onlsp.getValue(oraTSTZ, FormatType.PREFERRED);
					}
					else if(attrType.equals("TIMESTAMP WITH LOCAL TZ")) {
						TIMESTAMPLTZ oraTSLTZ = new TIMESTAMPLTZ(conn, ats);
						oradtstr =(String) onlsp.getValue(oraTSLTZ, FormatType.PREFERRED);
					}
				}
				else if(attributeData instanceof INTERVALYM && 
						attrType.equals("INTERVAL YEAR TO MONTH")) {
				    INTERVALYM oraIYM = (INTERVALYM)attributeData;
				    oradtstr =(String) onlsp.getValue(oraIYM, FormatType.PREFERRED);
				}
				else if(attributeData instanceof INTERVALDS &&
						attrType.equals("INTERVAL DAY TO SECOND")) {
					INTERVALDS oraIDS = (INTERVALDS)attributeData;
					oradtstr =(String) onlsp.getValue(oraIDS, FormatType.PREFERRED);
				} else if(attributeData instanceof oracle.sql.CLOB && attrType.equals("CLOB")) {
					Clob oraIDS = (Clob)attributeData;
					oradtstr =(String) onlsp.getValue(oraIDS, FormatType.PREFERRED);				
				} else if(attributeData instanceof oracle.sql.BLOB && attrType.equals("BLOB")) {
					Blob oraIDS = (Blob)attributeData;
					byte[] bdata = oraIDS.getBytes(1, (int) oraIDS.length());
					String ss = new String(ScriptUtils.bytesToHex(bdata)).toUpperCase();
					oradtstr = DataTypesUtil.stringValue(ss,conn,(Integer) this.m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETLONG));
				} 
	            
				if ( (attrType.equals("CLOB")) || (attrType.equals("BLOB"))) {
					if(attributeData == null) {
						attributeData = "NULL";
					}
					else if (oradtstr.length() == 0) {
						attributeData = "''";
					} else {
						attributeData = "'" + oradtstr + "'";
					}	
				} else if (oradtstr.length() > 0) {
					attributeData = "'" + oradtstr + "'";
				}
			}
			            
			boolean showAttributesFlag = true;
			
			// Do the attribute formatting after receiving the string from DataTypesUtil.
//			int maxLength = (Integer)m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETLONG);
			int maxLength = m_linesize;
			String dataTypeValue = DataTypesUtil.stringValue(attributeData, conn, 
					maxLength, false, DataTypesUtil.DISPLAY_ALL, FormatType.DEFAULT);
			
			HashMap<String, HashMap<String, String>> attributeMap = m_scriptRunnerContext.getObjectTypeAttributes().get(objectTypeName);
			
			if (attributeMap != null) {
				HashMap<String, String> attributeValueMap = attributeMap.get(attrName);
				
				if (attributeValueMap != null) {
					String formatPattern = attributeValueMap.get(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_FORMAT);
					String likeValue = attributeValueMap.get(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_LIKE);
					String onOffValue = attributeValueMap.get(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_ON_OFF);
					
					if (onOffValue != null) {
						if (onOffValue.equalsIgnoreCase("OFF")) {
							showAttributesFlag = false;
						}
						else {
							showAttributesFlag = true;
						}
					}
					
					if (showAttributesFlag) {					
						// FORMAT
						if (formatPattern != null && formatPattern.length() > 0) {
							s += processTypeAttributeFormat(formatPattern, attrType, dataTypeValue);
						}
						// LIKE
						else if (likeValue != null && likeValue.length() > 0 ) {
							// likeValue store either <type.attributes> or <alias>
							// attribute test_type_mixed_1.a like attribute test_type_mixed_3.a
							// 1) <type.attributes> option
							if (likeValue.contains (".")) {
								s += processLikeTypeAttribute(likeValue, attrType, dataTypeValue, attributeMap);
							}
							else {
								// there is an alias.
								// For the alias get <type.Attribute>
								String aliasValueName = likeValue.toUpperCase();
								String typeAttribute = (m_scriptRunnerContext.getAliasTypeAttributesMap()).get(aliasValueName);
								if (typeAttribute != null) {
									s += processLikeAlias( typeAttribute, attrType, dataTypeValue );
								}
							}
						}
						else {
							// just show without formating ...
							s += addQuotesToTextualAttributes(dataTypeValue, attrType);
						}
					}
					else {
						// just show without formating ...
						s += addQuotesToTextualAttributes(dataTypeValue, attrType);
					}
				}
				else {
					s += addQuotesToTextualAttributes(dataTypeValue, attrType);
				}
			}
			else {
				s += addQuotesToTextualAttributes(dataTypeValue, attrType);
			}
			
			if ( k < data.length-1) {
				s+=", ";
			}
		}
		s+=")";
		// reset back.
		onlsp.setNullDisplay(null);
		return s;
	}
	
	/*
	private String processTypeAttributesForArray(ARRAY obj, Connection conn) throws SQLException, IOException {
		
		String attrType = "";
		String schemaDotType = obj.getSQLTypeName().toUpperCase();

		//need to split 
		String[] schemaDotTypeParts = schemaDotType.split("\\.");
		String schema = schemaDotTypeParts[0].trim();
		String objectTypeName = schemaDotTypeParts[1].trim();
		
		String s = objectTypeName.toUpperCase()+"(";
		
		Object[] data = obj.getOracleArray();
				
		for( int k=0; k < data.length ; k++) {
			Object arrayElementData = data[k];	
			if (arrayElementData instanceof NUMBER) {
				attrType = "NUMBER";
			}
			if ( arrayElementData instanceof CHAR ) {
				attrType = "VARCHAR2";
			}
			// Do the attribute formatting after receiving the string from DataTypesUtil.
//			int maxLength = (Integer)m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETLONG);
			int maxLength = m_linesize;
			String dataTypeValue = DataTypesUtil.stringValue(arrayElementData, conn, 
					maxLength, false, DataTypesUtil.DISPLAY_ALL, FormatType.DEFAULT);
			
			s += addQuotesToTextualAttributes(dataTypeValue, attrType);
			
			if ( k < data.length-1) {
				s+=", ";
			}
		}

		s+=")";
		return s;
	}*/

	private String processTypeAttributesForArray(ARRAY obj, Connection conn) throws SQLException, IOException {
		OracleNLSProvider onlsp = (OracleNLSProvider)NLSProvider.getProvider(conn);
		// LRG/SRG For NULL's display NULL for object types.
		onlsp.setNullDisplay("NULL");
		String attrType = "";
		String schemaDotType = obj.getSQLTypeName().toUpperCase();

		//need to split 
		String[] schemaDotTypeParts = schemaDotType.split("\\.");
		String schema = schemaDotTypeParts[0].trim();
		String objectTypeName = schemaDotTypeParts[1].trim();
		
		String s = objectTypeName.toUpperCase()+"(";
		
		Object[] data = obj.getOracleArray();
			
		String dataTypeValue = null;
		int maxLength = m_linesize;
		
		for( int k=0; k < data.length ; k++) {
			Object arrayElementData = data[k];
			
			if ( arrayElementData instanceof BINARY_FLOAT ) {
				attrType = "BINARY_FLOAT";
				dataTypeValue = formattedFLOAT(m_conn, ((BINARY_FLOAT)arrayElementData).floatValue(), m_numformat, 10);
				s += dataTypeValue;
			} 
			else {
				if (arrayElementData instanceof NUMBER) {
					attrType = "NUMBER";
				}
				if ( arrayElementData instanceof CHAR ) {
					attrType = "VARCHAR2";	
				}
				if ( arrayElementData instanceof RAW ) {
					attrType = "RAW";
				}
			    if(arrayElementData instanceof STRUCT) 
			    	dataTypeValue = processTypeAttributesForStruct((STRUCT) arrayElementData, conn);
			    else 
					dataTypeValue = DataTypesUtil.stringValue(arrayElementData, conn);
			    
				// Discard Schema name appearing before the UDT object.
				String schemaName = "";
				if(arrayElementData instanceof ARRAY) {
					schemaName = ((ARRAY)arrayElementData).getOracleMetaData().getSchemaName();
					dataTypeValue = dataTypeValue != null ? dataTypeValue.replaceAll(schemaName + "\\.", "") : dataTypeValue;
				}
				else if(arrayElementData instanceof STRUCT) {
					schemaName = ((STRUCT)arrayElementData).getOracleMetaData().getSchemaName();
					dataTypeValue = dataTypeValue != null ? dataTypeValue.replaceAll(schemaName + "\\.", "") : dataTypeValue;
				}
				// bug 24969585: DISPLAY WRONG VALUE OF A VARRAY COLUMN -- sroychow 02/06/17
				// Commented the following line because it removes the point from decimal numbers etc.; 
				// Rolled it in "Array" and "Struct" code where it need a schemaName to be replaced.
				// Ideally, the struct and Array should be tested only once, not twice as done here.
				////>> dataTypeValue = dataTypeValue != null ? dataTypeValue.replaceAll(schemaName + "\\.", "") : dataTypeValue;
				
				if (dataTypeValue == null && arrayElementData == null) {
					s += "NULL";
				} 
				else {
					s += addQuotesToTextualAttributes(dataTypeValue, attrType);
				}
			}
			// Do the attribute formatting after receiving the string from DataTypesUtil.
//			int maxLength = (Integer)m_scriptRunnerContext.getProperty(ScriptRunnerContext.SETLONG);		
			if ( k < data.length-1) {
				s+=", ";
			}
		}
		s+=")";
		// reset back.
		onlsp.setNullDisplay(null);
		return s;
	}

	
	private String processLikeTypeAttribute(String likeValue, String attrType,
			String dataTypeValue, HashMap<String, HashMap<String, String>> attributeMap ) {
		
		String finalFormattedString = "";
		
		String[] likeValueParts = likeValue.split("\\.");
		String likeValueType = likeValueParts[0].trim().toUpperCase();
		String likeValueAttribute = likeValueParts[1].trim().toUpperCase();
		HashMap<String, HashMap<String, String>> likeValueAttributeMap = m_scriptRunnerContext.getObjectTypeAttributes().get(likeValueType);
		if ( likeValueAttributeMap != null ) {
			
			if ( likeValueAttribute != null && likeValueAttribute.length() > 0 ) {
				HashMap<String, String> likeValueAttributeValueMap = attributeMap.get(likeValueAttribute);		
					
				if (likeValueAttributeValueMap != null) {
					String likeValueFormatPattern = likeValueAttributeValueMap.get(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_LIKE);
					// this should have two parts TYPE and ATTRIBUTE
					String[] referencedlikeValueParts = likeValueFormatPattern.split("\\.");
					String referencedLikeValueType = referencedlikeValueParts[0].trim().toUpperCase();
					String referencedLikeValueAttribute = referencedlikeValueParts[1].trim().toUpperCase();
					
					HashMap<String, HashMap<String, String>> referencedlikeValueAttributeMap = 
							m_scriptRunnerContext.getObjectTypeAttributes().get(referencedLikeValueType);
					
					if (referencedlikeValueAttributeMap != null) {
						HashMap<String, String> referencedAttributeMap = referencedlikeValueAttributeMap.get(referencedLikeValueAttribute);
						
						if (referencedAttributeMap != null) {
							String referencedAttributePattern = referencedAttributeMap.get(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_FORMAT);
							finalFormattedString = processTypeAttributeFormat(referencedAttributePattern, attrType, dataTypeValue);
						}
					}
				}	
			}
		}
		return finalFormattedString;
	}

	
	private String processLikeAlias(String typeAttribute, String attrType, String dataTypeValue ) {
		
		String finalFormattedString = "";
		
		String[] typeAttributeParts = typeAttribute.split("\\.");
		String type = typeAttributeParts[0].trim().toUpperCase();
		String attribute = typeAttributeParts[1].trim().toUpperCase();
		HashMap<String, HashMap<String, String>> attributeMap = m_scriptRunnerContext.getObjectTypeAttributes().get(type);
		
		if (attributeMap != null) {
			HashMap<String, String> attributeValueMap = attributeMap.get(attribute);
			if (attributeValueMap != null) {
				String formatPattern = attributeValueMap.get(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_FORMAT);
				finalFormattedString = processTypeAttributeFormat(formatPattern, attrType, dataTypeValue);
			}
		}
		return finalFormattedString;
	}

	
	private String processTypeAttributeFormat(String formatPattern, String attrType, String dataTypeValue) {
		String formattedString = "";
		
		if (formatPattern.length() > 0) {
			if ( attrType.equalsIgnoreCase("VARCHAR") || attrType.equalsIgnoreCase("VARCHAR2") ) {
				if (formatPattern.matches("[a|A]\\d*")) {
					String columnWidthString = formatPattern.substring(1, formatPattern.length());
					int columnWidth = Integer.parseInt(columnWidthString);
					if ( columnWidth <= dataTypeValue.length() ) {
						dataTypeValue = dataTypeValue.substring(0, columnWidth);	 
					}
					formattedString += "'" + dataTypeValue + "'" ;
				}
				else {
					formattedString += "'" + dataTypeValue + "'" ;  // return the text if column format a10 etc. fails.
				}
			}
			if ( attrType.equalsIgnoreCase("NUMBER") || attrType.equalsIgnoreCase("INTEGER") ||
				 attrType.equalsIgnoreCase("DOUBLE") || attrType.equalsIgnoreCase("FLOAT") || 
				 attrType.equalsIgnoreCase("BINARY_DOUBLE") || attrType.equalsIgnoreCase("BINARY_FLOAT")
			   ) {
				formattedString += getNumericFormatedValue(dataTypeValue, formatPattern);
			} 
		}
		return formattedString;
	}

	
	private String addQuotesToTextualAttributes(String text, String attrType) {
		if(text.equals("NULL")) {
			return m_null != null && m_null.length() > 0 ? m_null : text;
		}
		if (( attrType.equalsIgnoreCase("VARCHAR") || attrType.equalsIgnoreCase("VARCHAR2") || attrType.equalsIgnoreCase("CHAR") ||
			 attrType.equalsIgnoreCase("RAW")	) && !text.matches("'([^']*)'") /*Check text if it is NOT quoted*/) {
			text = "'" + text + "'" ;
		}
		if ( attrType.equalsIgnoreCase("NUMBER") || attrType.equalsIgnoreCase("INTEGER") ||
			 attrType.equalsIgnoreCase("DOUBLE") || attrType.equalsIgnoreCase("FLOAT") || 
			 attrType.equalsIgnoreCase("BINARY_DOUBLE") || attrType.equalsIgnoreCase("BINARY_FLOAT") ) {
				text = getNumericFormatedValue(text, "");
		}
		return text;
	}
	
	private String getColumnName(ResultSet rset, int index) {
	    String colName = "";
	    if(index > 0 && rset != null) {
			try {
	        	 	Object obj = null;
	        	    // Bug 23297745 
					// Workaround for LONG Types where we run into "Stream has already been closed"
	        	 	if(rset.getMetaData().getColumnType(index) != -1 /* OracleTypes.LONGVARCHAR */ && 
	        	 	   rset.getMetaData().getColumnType(index) != -2  /* OracleTypes.RAW */) {
	        	 		obj = ScriptUtils.getOracleObjectWrap((OracleResultSet)rset,index);
	        	 	}
	        		if(obj != null) {
	        			String pcolName = "";
	        			pcolName = rset.getMetaData().getColumnName(index);
	        			if(obj instanceof STRUCT || obj instanceof ARRAY)
	        		        colName = getUDTColumnName(pcolName, obj);
	        		}
	        		if(colName.length() == 0) {
	        		    colName = rset.getMetaData().getColumnName(index);
	        		}
			}
			catch(Exception ex) {
			    String msg = ex.getMessage();
			    // Bug 23095919: Just get the column name.
			    if(msg.equalsIgnoreCase("Stream has already been closed") || ex instanceof SQLException) {
			    	try {
						colName = rset.getMetaData().getColumnName(index);
					}
					catch (SQLException e) {
					}
			    }
			}
	    }
	    
	    if(colName.equals("COLUMN_VALUE")) {
	    	colName = ScriptRunnerDbArb.getString("COLUMN_VALUE");
	    }
	    
	    return colName;
	}
	
	private String getUDTColumnName(String parentObj, Object obj) throws SQLException {
		String udtColName = "";
		String subCols = "";
		if (obj != null) {
			if (obj instanceof STRUCT) {
				Struct struct = (Struct) obj;
				String typeName = struct.getSQLTypeName();
				StructDescriptor structDescriptor = StructDescriptor.createDescriptor(typeName, m_conn);
				OracleTypeADT oracleTypeADT = (OracleTypeADT) structDescriptor.getPickler();
				Object[] attrs = struct.getAttributes();
				for (int i = 0; i < attrs.length; i++) {
					String attrName = oracleTypeADT.getAttributeName(i + 1);
					String subCol = "";
					if (attrs[i] instanceof STRUCT || attrs[i] instanceof ARRAY) {
						subCol = getUDTColumnName(attrName, attrs[i]);
					}
					if (i + 1 == 1) {
						if (parentObj.length() > 0)
							subCols = parentObj + "(" + (subCol.length() > 0 ? subCol : attrName);
						else
							subCols = attrName + "(" + subCol;
					} else {
							subCols = subCols + ", " + (subCol.length() > 0 ? subCol : attrName);
					}
				}
				udtColName = subCols + ")";
			} else if (obj instanceof ARRAY) {
				ArrayDescriptor arrayDescriptor = ((ARRAY) obj).getDescriptor();
				Object[] arrayData = ((ARRAY) obj).getOracleArray();
				OracleTypeADT oracleTypeADT = (OracleTypeADT) arrayDescriptor.getPickler();
				String attrName = "";
				for (int i = 0; i < arrayData.length; i++) {
					if (arrayData[i] instanceof ARRAY) {
						attrName = getUDTColumnName(parentObj, arrayData[i]);
						udtColName = attrName;
						break;
					} else if (arrayData[i] instanceof STRUCT) {
						attrName = getUDTColumnName(parentObj, arrayData[i]);
						udtColName = attrName;
					} else {
						attrName = oracleTypeADT.getAttributeName(i + 1);
						udtColName = attrName;
					}
				}
			}
		}
		return udtColName;
	}
	
	private void setCursorStmtHeader(String header) {
	   m_cursorHeader = header;
	}
	
	public String getCursorStmtHeader() {
		return m_cursorHeader;
	}
 
	
	
	/*
	 * This method is used for processing numbers that are less than numwidth.
	 */
	public String processNumberLessThanNumwidth(String numberString, int numwidth) {		

		String returnableString = "";
		
		int numberStringLen = numberString.length();
		if (numberString.contains(".")) {
			// has decimal value. In this case only match integral value length and not the whole number.
			int decimalIndex = numberString.indexOf(".");
			
			String integralPartString = numberString.substring(0,decimalIndex);
			int integralPartStringLen = integralPartString.length();
			
			String decimalPartString = numberString.substring(decimalIndex+1,numberStringLen);
			int decimalPartStringLen = decimalPartString.length();
			
			if ( integralPartStringLen > numwidth) {
				char[] chars = new char[numwidth];
			    Arrays.fill(chars, '#');
			    returnableString = String.copyValueOf(chars);;
			}
			else {
				// integralPartString < m_numwdith; therefore we need to cover integral and decimal parts.
				int delta = numwidth - (integralPartStringLen + 1); // 1 is for decimal point
				if (delta > 0) {
					returnableString = integralPartString + "." +  decimalPartString.substring(0,delta);
				}
				else {
					returnableString = integralPartString;
				}
			}
		}
		else {
			// has only integral value. 
			if ( numberStringLen > numwidth ) {
				char[] chars = new char[numwidth];
			    Arrays.fill(chars, '#');
			    returnableString = String.copyValueOf(chars);
			}
			else {
				returnableString = numberString;
			}
		}
		return returnableString;
	}
	
	/**
	 *  brkcolkey doesn't take into account the numformat command
	 *  and this was problems when trying to find brkcolkeys.
	 *  
	 *  
	 * @param brkcolkey
	 * @return
	 * @throws SQLException
	 */
	private final String modifyScientificNotation(String brkcolkey) throws SQLException {
		String modbrkcolkey = "";
		modbrkcolkey = brkcolkey.replaceFirst("\\d+_", "");
		Pattern ptrn = Pattern.compile("\\d+_");
	    Matcher m = ptrn.matcher(brkcolkey);
	    String prefix = "";
	    while(m.find()) {
	    	prefix = m.group(0);
	    	break;
	    }
		
		String floatval = "";
		if(modbrkcolkey.matches("-?\\d+(\\.\\d+)?((?i:e)(-|\\+)?\\d+)")) {
			float f = Float.parseFloat(String.valueOf(modbrkcolkey));
			floatval = formattedFLOAT(m_conn, f, m_numformat, 10);
			modbrkcolkey = prefix + floatval;
		}
		else if(modbrkcolkey.equals("Inf")) {
			float f = Float.POSITIVE_INFINITY;
			floatval = formattedFLOAT(m_conn, f, m_numformat, 7);
			modbrkcolkey = prefix + floatval;
		}
		else if(modbrkcolkey.equals("Nan")) {
			float f = Float.NaN;
			floatval = formattedFLOAT(m_conn, f, m_numformat, 7);
			modbrkcolkey = prefix + floatval;
		}
		else {
			modbrkcolkey = brkcolkey;
		}
		
		return modbrkcolkey;
	}
}
