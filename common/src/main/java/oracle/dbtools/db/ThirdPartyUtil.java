/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.Collections;

import oracle.dbtools.common.utils.MetaResource;
import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryXMLSupport;

/**
 * @author bamcgill
 * 
 */
public class ThirdPartyUtil extends DBUtil {

	private static QueryXMLSupport s_xml;

	private static synchronized QueryXMLSupport getXMLQueries() {
		if (s_xml == null) {
			s_xml = QueryXMLSupport
					.getQueryXMLSupport(new MetaResource(ThirdPartyUtil.class.getClassLoader(), "oracle/dbtools/db/dbutilsql.xml")); //$NON-NLS-1$
		}
		return s_xml;
	}

	protected ThirdPartyUtil(ConnectionIdentifier id) {
		super(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.db.DBUtil#setDBAction(java.lang.String)
	 */
	@Override
	public void setDBAction(String action) {
	      Query q = getXMLQueries().getQuery("ACTION", m_conn); //$NON-NLS-1$
	        execute(q.getSql(), Collections.singletonList(action == null ? EMPTY_STRING : action)); 
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.db.DBUtil#setDBModuleAction(java.lang.String)
	 */
	@Override
	public void setDBModuleAction(String action) {
	       Query q = getXMLQueries().getQuery("MODULE", m_conn); //$NON-NLS-1$
	        execute(q.getSql(), Arrays.asList(s_prodName, action == null ? EMPTY_STRING : action)); 
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.db.DBUtil#getDBMSOUTPUT()
	 */
	@Override
	public String getDBMSOUTPUT() {
		return EMPTY_STRING;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.db.DBUtil#getErrorOffset(java.lang.String)
	 */
	@Override
	public int getErrorOffset(final String sql) {
		LockableOperation<Integer> oper = new OperImpl<Integer>() {
			public Integer call() throws SQLException {
				CallableStatement stmt = null;
				try {
					Query q = getXMLQueries().getQuery("ERR_SQL", m_conn); //$NON-NLS-1$
					stmt = m_conn.prepareCall(q.getSql());
					LOGGER.info(Messages.getString("DBUtil.55") + q.getSql()); //$NON-NLS-1$
					stmt.setString(1, sql);
					stmt.setString(2, sql);
					stmt.registerOutParameter(3, Types.INTEGER);
					stmt.execute();
					return stmt.getInt(3);
				} finally {
					if (stmt != null)
						try {
							stmt.close();
						} catch (SQLException e) {
						}
				}
			}
		};
		return lockForOperation(oper, 0);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.db.DBUtil#hasTransaction()
	 */
	@Override
	protected boolean hasTransaction() {
        LockableOperation<Boolean> oper = new OperImpl<Boolean>() {
            @SuppressWarnings("unchecked")
            public Boolean call() throws SQLException {
                if (m_conn == null || m_conn.isClosed()) return false;

                Query q = getXMLQueries().getQuery("TRANSACTION", m_conn); //$NON-NLS-1$
                String ret = executeOracleReturnOneCol(q.getSql(), Collections.EMPTY_MAP);

                return ModelUtil.hasLength(ret);
            }
        };

        return lockForOperation(oper, Boolean.FALSE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.db.DBUtil#isOracleConnectionAlive()
	 */
	@Override
	protected boolean isOracleConnectionAlive() {
		return false;
	}
	
	/*
     * addQuotes() method enquotes the string specific to the database connection.
     */
	
    public String addDbQuotes(String value, boolean force) {
    	// Default implementation
    	char quoteChar = getQuoteCharacter();
    	if ( force ) {
    		// Quote the identifier unless it already contains the quote
    		// character
    		return value.indexOf(quoteChar) >= 0 ? value : quoteChar + value + quoteChar;
    	} else {
    		return value;
    	}
    }
   
}
