/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.db;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import oracle.dbtools.common.utils.Version;

public class VersionTracker {
	private static final VersionTracker INSTANCE = new VersionTracker();
	
	protected final VersionTracker getInstance() {
		return INSTANCE;
	}
	
	public static final Version getDbVersion(final ConnectionIdentifier id) {
		return INSTANCE.getVersionImpl(id);
	}

    public static boolean checkVersion(ConnectionIdentifier id, Version maxver, Version minver) {
    	return INSTANCE.checkVersionImpl(id, maxver, minver);
    }

    public static boolean checkVersion(Version dbVersion, Version maxver, Version minver) {
    	return INSTANCE.checkVersionImpl(dbVersion, maxver, minver);
    }

    private final Map<ConnectionIdentifier.Key, Version> cache = new HashMap<ConnectionIdentifier.Key, Version>();
    
    private Version getVersionImpl(final ConnectionIdentifier id) {
    	Connection conn = id.getConnection();
        if (conn == null) {
            return DBUtil.ORACLE10g_VERSION;
        }

        Version dbVersion = cache.get(id.getIdentifierKey());
        if (dbVersion == null) {
            dbVersion = DBUtil.getInstance(id).fetchDbVersion();
            // cache the version since it'll not change based on the connection
            synchronized (cache) {
                cache.put(id.getIdentifierKey(), dbVersion);
            }
        }
        return dbVersion;
    }

    /**
     * Checks whether a specified Connection is within a range of database Versions.
     * <p>
     * Note: Does <em>not</em> require the connection lock be held by callers.
     * 
     * @param conn
     *            the Connection to check
     * @param maxver
     *            the Version to use as the upper bound
     * @param minver
     *            the Version to use as the lower bound
     * @return whether the Version of the database represented by the connection name falls between the minimum and maximum versions (inclusive).
     */
    private boolean checkVersionImpl(ConnectionIdentifier id, Version maxver, Version minver) {
        Version dbVersion;
        try {
            dbVersion = getVersionImpl(id);
        } catch (UnsupportedOperationException usoe) {
            // Some 3rd Party JDBC Drivers won't supply a version (e.g. using
            // access via
            // JDBC-ODBC Bridge. In this case, its ok to call version null
            dbVersion = null;
        }

        return checkVersionImpl(dbVersion, maxver, minver);
    }

    /**
     * Checks whether the specified Version falls within the range of supported Versions. A missing minimum or maximum version is ignored.
     * 
     * @param dbVersion
     *            the Version to check
     * @param maxver
     *            the Version setting the upper limit
     * @param minver
     *            the Version setting the lower limit
     * @return whether the specified Version is between the two limiting Versions
     */
    private boolean checkVersionImpl(Version dbVersion, Version maxver, Version minver) {
        boolean ret = false;
        // if full version ==0 of db not found try it
        // if minver < db version db not new enough
        // if maxver < db version db is too new
        if ((dbVersion == null) || ((minver == null || (minver.compareTo(dbVersion) <= 0)) && (maxver == null || (maxver.compareTo(dbVersion) >= 0)))) {
            ret = true;
        }
        return ret;

    }

}
