/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.logging;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

/**
 * LogPaneHandler isA java.util.logging.Handler to route oracle.dbtools messages
 * to an ILoggingPage provided during initialization.
 * 
 * @see oracle.dbtools.raptor.standalone.LoggingPageAddin#initialize()
 * 
 *      Updated to take advantage of Java 5 concurrency features
 * 
 * @author klrice
 * @author <a
 *         href="mailto:brian.jeffries@oracle.com?subject=LogPaneHandler">Brian
 *         Jeffries</a>
 */
public class LogPaneHandler extends Handler {

	private LogPaneHandler() {
		LogManager lm = LogManager.getLogManager();
		Logger root = lm.getLogger(""); //$NON-NLS-1$
		root.addHandler(this);
	}

	private static LogPaneHandler INSTANCE;

	private static final Object INSTANCE_LOCK = new Object();

	/**
	 * Creation factory to return Singleton instance
	 * 
	 * @return the Singleton LogPaneHandler
	 */
	public static LogPaneHandler getInstance() {
		if (INSTANCE == null) {
			synchronized (INSTANCE_LOCK) {
				// ask again, we could have been waiting on lock while
				// someone else was doing this
				if (INSTANCE == null) {
					INSTANCE = new LogPaneHandler();
					INSTANCE.setLevel(Level.ALL);
				}
			}
		}
		return INSTANCE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.logging.Handler#close()
	 */
	@Override
	public void close() throws SecurityException {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.logging.Handler#flush()
	 */
	@Override
	public void flush() {
		// We are buffering messages if there is already a logRunner so
		// make sure to flush
		logMessages(true);
	}

	private static final Logger BASE_LOGGER = Logger.getLogger("oracle.dbtools"); //$NON-NLS-1$

	private Queue<LogRecord> m_records = new ConcurrentLinkedQueue<LogRecord>();
	private final static String PROXY_LOGGER= "oracle.dbtools.raptor.proxy.ProxyLogger"; //$NON-NLS-1$
    // include stack trace in all log pane entries?
	private static boolean sLogStackTraceAll = Boolean.valueOf(System.getProperty("LogStackTraceAll", "false"));
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.logging.Handler#publish(java.util.logging.LogRecord)
	 */
	@Override
	public void publish(LogRecord rec) {
		assert (rec != null);
		if (BASE_LOGGER.isLoggable(rec.getLevel())) {
		    // Block logging from oracle.dbtools.raptor.proxy.ProxyLogger (it has
		    // it's own logging page & very verbose)
		    if (PROXY_LOGGER.equals(rec.getLoggerName())) {
		        return;
		    }
		    // These records will be processed later potentially on another thread
		    // so force LogRecord to determine calling class/method here.
		    if (null == rec.getSourceClassName()) {
		        // make sure it won't try again
		        rec.setSourceClassName(null);
		    }
		    if (sLogStackTraceAll && null == rec.getThrown()) {
		        rec.setThrown(new Throwable("LogStackTraceAll")); // $NON-NLS-1$
		    }
			m_records.add(rec);
			logMessages(false);
		}
	}

	/*
	 * Setting the logPage class causes the next getLogPage call to create an
	 * instance of and switch to the new page
	 */
	private volatile Class<ILoggingPage> _logPageClass;

	public void setLogPageClass(Class clazz) {
		assert (clazz != null);
		_logPageClass = clazz;
	}

	private transient Runnable _logRunner;

	private void logMessages(boolean flush) {
		if (SwingUtilities.isEventDispatchThread()) {
			// If we are currently on the event dispatch thread, go ahead and
			// process any pending log messages
			logMessages();
		} else {
			// schedule a Runnable to process the queue. Try to only have
			// a single Runnable at a time (unless we need to flush)
			// Don't bother synchronizing for logRunner - worst case we'll
			// schedule multiples but only the first one will have to do
			// any work
			if (flush || null == _logRunner) {
				_logRunner = new Runnable() {
					public void run() {
						_logRunner = null;
						logMessages();
					}
				};
				SwingUtilities.invokeLater(_logRunner);
			}
		}
	}

	private void logMessages() {
		assert (SwingUtilities.isEventDispatchThread());
		ILoggingPage page = getLogPage();
		if (page != null) {
			LogRecord rec;
			while ((rec = m_records.poll()) != null) {
                page.log(rec);
			}
		}
	}

	private volatile ILoggingPage _logPage;

	private void setLogPage(ILoggingPage page) {
		_logPage = page;
	}

	private ILoggingPage getLogPage() {
		if (_logPageClass != null) {
			try {
				ILoggingPage logPage = _logPageClass.newInstance();
				_logPageClass = null;
				setLogPage(logPage);
			} catch (InstantiationException e) {
				BASE_LOGGER.log(Level.SEVERE, e.getMessage());
			} catch (IllegalAccessException e) {
				BASE_LOGGER.log(Level.SEVERE, e.getMessage());
			}
		}
		return _logPage;
	}
}
