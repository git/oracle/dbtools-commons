/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.logging;

import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * @author klrice
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=RaptorSimpleFormatter.java"
 *         >Barry McGillin</a>
 * 
 */
public class RaptorSimpleFormatter extends Formatter {

	private static Long lastTime = System.currentTimeMillis();

	private static String MSG = "%-10s%dms\t%s.%-20s\t%s\n"; //$NON-NLS-1$

	@Override
	public String format(LogRecord log) {
		String s = String.format(MSG, new Object[] { log.getLevel().getName(), new Long(log.getMillis() - lastTime),
		        log.getSourceClassName().substring(log.getSourceClassName().lastIndexOf(".") + 1), //$NON-NLS-1$
		        log.getSourceMethodName(), log.getMessage() });

		synchronized (RaptorSimpleFormatter.class) {
			lastTime = log.getMillis();
		}
		return s;
	}

	public static void initBasicLogger() {

		if (Logger.getLogger("") != null && Logger.getLogger("").getHandlers() != null && Logger.getLogger("").getHandlers().length == 1 && Logger.getLogger("").getHandlers()[0] != null) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			Logger.getLogger("").removeHandler(Logger.getLogger("").getHandlers()[0]); //$NON-NLS-1$ //$NON-NLS-2$

		ConsoleHandler c = new ConsoleHandler();
		c.setFormatter(new RaptorSimpleFormatter());

		Logger l = Logger.getLogger("oracle.dbtools.raptor"); //$NON-NLS-1$
		l.setLevel(Level.ALL);
		l.addHandler(c);

	}
}
