/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.logging;

import java.io.PrintStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Timer.java"
 *         >Barry McGillin</a>
 * 
 */
public final class Timer {
	private static final String COLON = ":";
	private static final String DOT = ".";
	private long startTime;
	private long endTime;
	private boolean cacheDuration = false;
	private List<Long> durationCache = new ArrayList<Long>();

	public Timer() {

	}

	public void start() {
		startTime = System.currentTimeMillis();
	}

	public void end() {
		endTime = System.currentTimeMillis();
		if (cacheDuration)
			durationCache.add(duration());
	}

	public long duration() {
		return (endTime - startTime);
	}

	public void printDuration(PrintStream out) {
		long elapsedTimeInSecond = duration();

		out.println("\nTotal execution time:" + elapsedTimeInSecond + " milliseconds");
	}

	public void printDuraion() {
		printDuration(System.out);
	}

	public void reset() {
		startTime = 0;
		endTime = 0;
		cacheDuration = false;
		durationCache.clear();
	}

	public void cacheDuration(boolean cache) {
		cacheDuration = cache;
	}

	public void printCachedDurations() {
		Collections.sort(durationCache);
		for (Long l : durationCache) {
			System.out.println("duration =" + l + " milliseconds");
		}
	}

	public void averageDuration() {
		if (durationCache.size() < 1)
			System.out.println("-1");
		long ret = 0;
		for (Long l : durationCache) {
			ret = ret + l;
		}
		System.out.println("average duration =" + (float) ret / durationCache.size() + " milliseconds");
	}

	public void totalTime() {
		long ret = 0;
		for (Long l : durationCache) {
			ret = ret + l;
		}
		System.out.println("total time =" + ret + " milliseconds");
	}

	public static void main(String[] args) {
		Timer timer = new Timer();
		timer.start();

		for (int i = 0; i < 500; i++) {
			System.out.print("#");
		}

		timer.end();
		timer.printDuration(System.out);

		timer.reset();
		timer.cacheDuration(true);

		for (int i = 0; i < 50; i++) {
			timer.start();
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				Logger.getLogger(Timer.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
			}
			timer.end();
			timer.printDuration(System.out);
			if (i % 4 == 0) {
				System.out.println(i + "  hahahahahhahahaha");
			}
		}

		timer.averageDuration();

	}

	public long durationInSecs() {
		return duration() / 1000;
	}

	public String durationSQLPlus() {
		long durationMilliSecs = duration();
		String x = "";
		long hrs = ((durationMilliSecs / (1000 * 60 * 60)) % 24);
		if (hrs > 0) {
			durationMilliSecs = durationMilliSecs - (hrs * 1000 * 60 * 60);
			x+=String.format("%02d", hrs)+COLON;
		}
		long mins = ((durationMilliSecs / (1000 * 60)) % 60);
		if (mins > 0) {
			durationMilliSecs = durationMilliSecs - (mins * 1000 * 60);
			x+=String.format("%02d", mins)+COLON;
		}
		long secs = (durationMilliSecs / 1000) % 60;
		if (secs > 0) {
			durationMilliSecs = durationMilliSecs - (secs * 1000);
			x+=String.format("%02d", secs)+DOT;
		}
		if (durationMilliSecs>0) {
			x+=String.format("%03d", durationMilliSecs);
		}
		return x;
	}
}