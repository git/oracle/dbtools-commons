/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * To process files containing BNF grammar definition 
 * @author Dim
 */
public class EBNF {
    private static Earley earley = new Earley(getRules()) {
        @Override
        protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
    		LexerToken token = src.get(y);
            return 
                  symbol == identifier && token.type == Token.IDENTIFIER 
              ||  symbol == identifier && token.type == Token.DQUOTED_STRING
            ;
        }
	};
    static { 
        expr= earley.symbolIndexes.get("expr");         //$NON-NLS-1$
        identifier= earley.symbolIndexes.get("identifier");         //$NON-NLS-1$
        string_literal= earley.symbolIndexes.get("string_literal"); //$NON-NLS-1$
        rule = earley.symbolIndexes.get("rule"); //$NON-NLS-1$
        grammar = earley.symbolIndexes.get("grammar"); //$NON-NLS-1$
        disjunct = earley.symbolIndexes.get("disjunct"); //$NON-NLS-1$
        concat = earley.symbolIndexes.get("concat"); //$NON-NLS-1$
        or = earley.symbolIndexes.get("'|'"); //$NON-NLS-1$
        sqOpen = earley.symbolIndexes.get("'['"); //$NON-NLS-1$
    }

    static Earley bnfParser() {
        return earley;
    }

	private static Set<RuleTuple> getRules() {
		Set<RuleTuple> rules = new TreeSet<RuleTuple>();
        rules.add(new RuleTuple("concat", new String[] {"expr"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("concat", new String[] {"concat","expr"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("disjunct", new String[] {"concat"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("disjunct", new String[] {"disjunct","'|'","concat"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("expr", new String[] {"identifier"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("expr", new String[] {"string_literal"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("expr", new String[] {"disjunct"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("expr", new String[] {"'{'","disjunct","'}'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("expr", new String[] {"'['","disjunct","']'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("expr", new String[] {"'{'","disjunct","'}'","'*'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("expr", new String[] {"'['","disjunct","']'","'*'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        //rules.add(new RuleTuple("rule", new String[] {"identifier","':'","expr","';'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("rule", new String[] {"identifier","':'","expr"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("grammar", new String[] {"rule"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("grammar", new String[] {"grammar", "rule"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        Set<RuleTuple> nonEmptyRules = rules; //RuleTransforms.eliminateEmptyProductions(rules); <--automatically transformed output is messy
		return nonEmptyRules;
	}
	
    static int expr;
    static int identifier;
    static int string_literal;
    static int rule;
    static int grammar;
    static int disjunct;
    static int concat;
    static int or;    
    static int sqOpen;    
    
    public static void grammar( ParseNode root, List<LexerToken> src, Set<RuleTuple> grammar ) {
        if( root.contains(rule) ) 
            rule(root, src, grammar); 
        else for( ParseNode child: root.children() ) 
            grammar(child, src, grammar);            
    }

	private static void rule( ParseNode node, List<LexerToken> src, Set<RuleTuple> grammar ) {
        String header = null;
        Set<RuleTuple> expr = new TreeSet<RuleTuple>();
        for( ParseNode child: node.children() ) {
            if( header==null && child.contains(identifier) ) {
                header = child.content(src); 
//if("dsslist".equals(header))
//System.out.println();
            } else if( child.contains(EBNF.expr) ) {
                expr(header, child, src, expr);
				grammar.addAll(expr);
            } 
        }
        //if( expr == null )
        	//grammar.add(new RuleTuple(header, new String[]{}));
    }

    private static void expr( String header, ParseNode node, List<LexerToken> src, Set<RuleTuple> ret ) {
    	if( node.from+1==node.to && (node.contains(identifier) || node.contains(string_literal)) ) {
            ret.add(new RuleTuple(header,new String[]{node.content(src)}));
    	} else if( node.contains(disjunct) ) {
            disjunct(header,node,src,ret);
    	} else {
    		Integer first = null;
    		ParseNode disjunct = null;
    		Integer third = null;
    		Integer last = null;
            for( ParseNode child: node.children() )  {
                if( first == null  ) 
                    first = child.content()[0];
                else if( disjunct == null )
                	disjunct = child;
                else if( third == null )
                	third = child.content()[0];
                else if( last == null )
                	last = child.content()[0];
            }
        	String aux = header+"["+disjunct.from+","+disjunct.to+")";
        	ret.add(new RuleTuple(header,new String[]{aux}));
        	if( first == sqOpen )
            	ret.add(new RuleTuple(header,new String[]{}));
        	if( last != null )
            	ret.add(new RuleTuple(header,new String[]{header,aux}));
            Set<RuleTuple> expr = new TreeSet<RuleTuple>();
        	expr(aux, disjunct, src, expr);
        	ret.addAll(expr);
    	} 
    	//throw new AssertionError("unexpected case"); //$NON-NLS-1$
	}


    private static void disjunct( String header, ParseNode node, List<LexerToken> src, Set<RuleTuple> ret ) {
    	if( node.contains(concat) ) {
            List<String> payload =  new LinkedList<String>();
            concat(header,node,src,ret,payload);
            ret.add(new RuleTuple(header, payload));
    	} else {
            for( ParseNode child: node.children() )  {
                if( child.contains(disjunct) || child.contains(concat) ) 
                    disjunct(header, child, src, ret);
            }
        }
    }

    private static void concat( String header, ParseNode node, List<LexerToken> src, Set<RuleTuple> ret, List<String> payload ) { 
        if( node.contains(identifier) || node.contains(string_literal) )
        	payload.add(node.content(src));
        else {
            for( ParseNode child: node.children() )  {
                if( child.contains(identifier) || node.contains(string_literal) ) 
                	payload.add(child.content(src));               
                else if( child.contains(expr) ) {
                	String aux = header+"["+child.from+","+child.to+")";
                	payload.add(aux);
                    Set<RuleTuple> expr = new TreeSet<RuleTuple>();
                	expr(aux, child, src, expr);
                	ret.addAll(expr);
                } else if( child.contains(concat) )
                	concat(header,child,src,ret,payload);
            }
        }
    }
    
    public static void main( String[] args ) throws Exception {
    	String input = "qon: [namequal] name"; //$NON-NLS-1$
        List<LexerToken> src = LexerToken.parse(input); 
        ParseNode root = parseGrammarFile(src, input);
        root.printTree();
        Set<RuleTuple> rules = new TreeSet<RuleTuple>();
        EBNF.grammar(root, src, rules);
        RuleTuple.printRules(rules);
	}

	public static ParseNode parseGrammarFile( List<LexerToken> src, String input ) throws Exception {
		return Grammar.parseGrammarFile(src, input, earley);
	}

}
