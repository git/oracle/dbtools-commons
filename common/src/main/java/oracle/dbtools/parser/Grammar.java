/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Service;

/**
 * To process files containing BNF grammar definition 
 * @author Dim
 */
public class Grammar {
    public static void main( String[] args ) throws Exception {
        Set<RuleTuple> rules = extractRules(SqlEarley.class, "json.ebnf");
        RuleTuple.printRules(rules);
        
        /*System.out.println("**************************");
        
        for( RuleTuple t: rules )
        	for( String s : SqlEarley.getInstance().allSymbols )
        		if( t.head.equals(s) )
        			System.out.println(s);*/
        
    }

	public static Set<RuleTuple> extractRules( Class c, String file ) throws IOException {
		String input = Service.readFile(c,file);
        input = input.replace("::=", ":");
        input = input.replace("\"", "'");
        List<LexerToken> src = LexerToken.parse(input);//, false, LexerToken.QuotedStrings/*+SqlPlusComments*/); 
        ParseNode root = Grammar.parseGrammarFile(src, input);
        root.printTree();
        Set<RuleTuple> rules = new TreeSet<RuleTuple>();
        Grammar.grammar(root, src, rules);
        RuleTransforms.eliminateEmptyProductions(rules);
        RuleTransforms.substituteSingleUnaryProductions(rules);
		return rules;
	}
    

    
    private static Earley earley = new Earley(getRules()) {
        @Override
        protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
    		LexerToken token = src.get(y);
            return 
                  symbol == identifier && token.type == Token.IDENTIFIER 
              ||  symbol == identifier && token.type == Token.DQUOTED_STRING
            ;
        }
	};
    static {
        rule = earley.symbolIndexes.get("rule");
        grammar = earley.symbolIndexes.get("grammar");
        variable = earley.symbolIndexes.get("variable");
        disjunct = earley.symbolIndexes.get("disjunct");
        postfix = earley.symbolIndexes.get("postfix");
        concat = earley.symbolIndexes.get("concat");
        or = earley.symbolIndexes.get("'|'");
        minus = earley.symbolIndexes.get("'-'");
        plus = earley.symbolIndexes.get("'+'");
        star = earley.symbolIndexes.get("'*'");
        question = earley.symbolIndexes.get("'?'");
        colon = earley.symbolIndexes.get("':'");
        semicolon = earley.symbolIndexes.get("';'");
        oparen = earley.symbolIndexes.get("'('");
        cparen = earley.symbolIndexes.get("')'");
    }
    static int rule;
    static int grammar;
    static int variable;
    static int disjunct;
    static int postfix;
    static int concat;
    static int or;
    static int minus;
    static int plus;
    static int star;
    static int question;
    static int colon;
    static int semicolon;
    static int oparen;
    static int cparen;

    public static Earley bnfParser() {
        return earley;
    }

    /**
     * A grammar for BNF grammar. Chicken-and-egg problem: Have to create this manually:-)
     * @return
     */
	private static Set<RuleTuple> getRules() {
		Set<RuleTuple> rules = new TreeSet<RuleTuple>();
        rules.add(new RuleTuple("variable", new String[] {"identifier"}));                              //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("variable", new String[] {"identifier","'['","identifier","']'"}));     //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("variable", new String[] {"string_literal"}));                          //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        
        rules.add(new RuleTuple("postfix", new String[] {"variable"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("postfix", new String[] {"variable","'*'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("postfix", new String[] {"variable","'+'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$        
        rules.add(new RuleTuple("postfix", new String[] {"variable","'?'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$        
        rules.add(new RuleTuple("postfix", new String[] {"'('","disjunct","')'","'*'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("postfix", new String[] {"'('","disjunct","')'","'+'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$        
        rules.add(new RuleTuple("postfix", new String[] {"'('","disjunct","')'","'?'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$        
        rules.add(new RuleTuple("postfix", new String[] {"'('","disjunct","')'"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$        
        /* Antlr syntactic predicate*/
        rules.add(new RuleTuple("postfix", new String[] {"'('","disjunct","')'","'='","'>'","disjunct"}));        //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        
        rules.add(new RuleTuple("concat", new String[] {"postfix"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("concat", new String[] {"concat","postfix"}));                     //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        
        rules.add(new RuleTuple("disjunct", new String[] {"concat"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("disjunct", new String[] {"'|'","concat"}));                          //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("disjunct", new String[] {"disjunct","'|'","concat"}));                //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("disjunct", new String[] {"disjunct","'|'"}));                         //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

        rules.add(new RuleTuple("rule", new String[] {"variable","':'","disjunct","';'"}));                //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("rule", new String[] {"variable","':'","';'"}));                           //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        /* exclude this rule */
        rules.add(new RuleTuple("rule", new String[] {"'-'","variable","';'"}));                //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("grammar", new String[] {"rule"}));                             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        rules.add(new RuleTuple("grammar", new String[] {"grammar", "rule"}));                  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        Set<RuleTuple> nonEmptyRules = rules; //RuleTransforms.eliminateEmptyProductions(rules); <--automatically transformed output is messy
		return nonEmptyRules;
	}
	

    /**
     * 
     * @param src
     * @param input
     * @return
     */
    public static ParseNode parseGrammarFile( List<LexerToken> src, String input ) {
    	return parseGrammarFile(src, input, earley);
    }
    /**
     * 
     * @param src
     * @param input
     * @param parser
     * @return
     */
    public static ParseNode parseGrammarFile( List<LexerToken> src, String input, Earley parser )  {
        Matrix matrix = new Matrix(parser);
    	/*boolean isCmdline = false;
        for( StackTraceElement elem : Thread.currentThread().getStackTrace() ) {
        	if( elem.toString().startsWith("oracle.dbtools.cmdline") ) {
        		isCmdline = true;
        		break;
        	}
        }
        if( !isCmdline )
        	matrix.visual = new Visual(src,parser);*/
    	
        parser.parse(src, matrix); 
        SyntaxError s = SyntaxError.checkSyntax(input, new String[]{"grammar"}, src, parser, matrix);
        if( s != null ) { //$NON-NLS-1$ //$NON-NLS-2$
        	if( matrix.visual != null  )
        		matrix.visual.draw(matrix);
            System.out.println("Syntax Error"); //$NON-NLS-1$
            System.out.println("at line#"+s.line);
            System.out.println(s.code);
            System.out.println(s.marker);
            System.out.println("Expected:  "); //$NON-NLS-1$
                for( String tmp : s.getSuggestions() )
                    System.out.print(tmp+',');
            throw new AssertionError(">>>> syntactically invalid code fragment <<<<"); //$NON-NLS-1$
        }
        
        ParseNode root = parser.forest(src, matrix);
        //root.printTree();

        return root;
    }
    
    /**
     * Descend recursively processing tree nodes
     * This is standard pattern
     * @param root
     * @param src
     * @param grammar
     */
    public static void grammar( ParseNode root, List<LexerToken> src, Set<RuleTuple> grammar ) {
        if( root.contains(rule) ) 
            rule(root, src, grammar); 
        else for( ParseNode child: root.children() ) 
            grammar(child, src, grammar);            
    }

    private static ParseNode ruleRoot;
    /**
     * Descend more
     * @param node
     * @param src
     * @param grammar
     */
	private static void rule( ParseNode node, List<LexerToken> src, Set<RuleTuple> grammar ) {
		ruleRoot = node;
        String header = null;
        boolean isEmpty = true;
        for( ParseNode child: node.children() ) {
            if( header==null && child.contains(minus) ) {
                delete(node, src, grammar);
                return;
            } 
                
            if( header==null && child.contains(variable) ) {
                header = child.content(src); 
            } else if( child.contains(disjunct) ) {
                grammar.addAll(disjunct(header, child, src));
                isEmpty = false;
            } else if( child.contains(colon) ) {
                continue;
            } else if( child.contains(semicolon) ) {
                if( isEmpty )
                    grammar.add(new RuleTuple(header,new String[]{}));
                continue;
            } else
                throw new AssertionError("not expr?");
        }
    }

	/**
	 * Descend further down
	 * @param node
	 * @param src
	 * @param grammar
	 */
    private static void delete( ParseNode node, List<LexerToken> src, Set<RuleTuple> grammar ) {
        String var = null;
        for( ParseNode child: node.children() ) 
            if( var==null && child.contains(variable) ) {
                var = child.content(src);
                break;
            }
        Set<RuleTuple> deletions = new TreeSet<RuleTuple>();
        for( RuleTuple t : grammar )
            if( t.head.equals(var) ) 
                deletions.add(t);
            else for( String rhs : t.rhs )
                if( rhs.equals(var) ) {
                    deletions.add(t);
                    break;
                }
        
        grammar.removeAll(deletions);        
    }

    /**
     * Descend one more step
     * @param header
     * @param node
     * @param src
     * @return
     */
    private static Set<RuleTuple> disjunct( String header, ParseNode node, List<LexerToken> src ) {
        Set<RuleTuple> ret =  new TreeSet<RuleTuple>();
    	if( node.contains(concat) ) {
            ret.add(new RuleTuple(header,concat(header, node, src, ret)));
    	} else {
            int cnt = -1;
            int lastOrPos = -1;
            for( ParseNode child: node.children() )  {
                cnt++;
                if( child.contains(disjunct) ) 
                    ret.addAll(disjunct(header, child, src));
                else if( child.contains(concat) ) {
                    ret.add(new RuleTuple(header,concat(header, child, src, ret)));
                }
                if( lastOrPos+1 == cnt && child.contains(or))
                    ret.add(new RuleTuple(header, new String[]{}));
                if( child.contains(or) ) {
                    lastOrPos = cnt;
                    if( cnt == node.children().size()-1 )
                    	 ret.add(new RuleTuple(header, new String[]{}));
                }
            }
        }
        return ret;
    }

    private static List<String> concat( String header, ParseNode node, List<LexerToken> src, Set<RuleTuple> grammar ) { 
        List<String> payload =  new LinkedList<String>();
        if( node.contains(postfix) ) {
        	payload.add(postfix(header, node, src, grammar));
        } else {
        	for( ParseNode child: node.children() ) {
                if( child.contains(postfix) ) {
                    payload.add(postfix(header, child, src, grammar));
                    continue;
                }
                if( child.contains(concat) ) {
                    payload.addAll(concat(header, child, src, grammar));
                    continue;
                }
                throw new AssertionError("!concat & !postfix?");
        	}
        }
        return payload;
    }
    
    private static String postfix( String header, ParseNode node, List<LexerToken> src, Set<RuleTuple> grammar ) {
        if( node.contains(variable) ) 
            return variable(node, src, grammar);
        String head = null;
        String rhs = null;
        for( ParseNode child: node.children() )  {
            if( child.contains(oparen) || child.contains(cparen) )
                continue;
            if( child.contains(variable) ) {
                rhs = variable(child, src, grammar);
                continue;
            }
            if( child.contains(disjunct) ) {
                rhs = auxiliarySymbol(header,node);
                grammar.addAll(disjunct(rhs, child, src));
                continue;
            }
            if( rhs != null ) {
                if( child.contains(star) ) {
                    head = auxiliarySymbol(header,node);
                    grammar.add(new RuleTuple(head,new String[]{}));
                    //grammar.add(new RuleTuple(header,new String[]{h}));
                    grammar.add(new RuleTuple(head,new String[]{head,rhs}));
                } else if( child.contains(plus) ) {
                    head = auxiliarySymbol(header,node);
                    grammar.add(new RuleTuple(head,new String[]{rhs}));
                    grammar.add(new RuleTuple(head,new String[]{head,rhs}));
                } else if( child.contains(question) ) {
                    head = auxiliarySymbol(header,node);
                    grammar.add(new RuleTuple(head,new String[]{}));
                    grammar.add(new RuleTuple(head,new String[]{rhs}));
                }
                continue;
            }
            throw new AssertionError("unexpected case");
        }
        if( head != null )
            return head;
        return rhs;
    }
    
    private static String auxiliarySymbol( String head, ParseNode node ) {
        String tmp = head;
        if( tmp.charAt(0) == '"' )
            tmp = tmp.substring(1,tmp.length()-1);
        if( 0 < tmp.indexOf('[') )
            tmp = tmp.substring(0,tmp.indexOf('['));
        return "\""+tmp+"["+(node.from-ruleRoot.from)+","+(node.to-ruleRoot.from)+")"+"\"";
    }
    
    /**
     * 
     * @param node
     * @param src
     * @param grammar
     * @return
     */
    private static String variable( ParseNode node, List<LexerToken> src, Set<RuleTuple> grammar ) {        
    	String prefix = src.get(node.from).content;
		if( node.from+1 == node.to )
    		return prefix;
    	
    	String postfix = src.get(node.from+2).content;
    	String header = prefix+"["+postfix+"]";
    	String quotetedHdr = "\""+header+"\"";
		for( int i = 0; i < postfix.length()+1; i++ ) 
    		grammar.add(new RuleTuple(quotetedHdr, new String[]{"'"+prefix+postfix.substring(0, i)+"'"}));
    	return quotetedHdr;
    }

    /**
     * Parser from BNF rules 
     * @return
     * @throws IOException 
     */
    public static Earley constructParser( Class location, String grammarFile ) throws IOException {
        String input = Service.readFile(location, grammarFile); //$NON-NLS-1$
        List<LexerToken> src = LexerToken.parse(input, false, LexerToken.QuotedStrings/*+SqlPlusComments*/); 
        ParseNode root = Grammar.parseGrammarFile(src, input);
        Set<RuleTuple> rules = new TreeSet<RuleTuple>();
        Grammar.grammar(root, src, rules);
       
        Earley ret = new Earley(rules) {
            @Override
            protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
                LexerToken token = src.get(y);
                return 
                        symbol == identifier && token.type == Token.IDENTIFIER 
                        ||  symbol == identifier && token.type == Token.DQUOTED_STRING
                        ;
            }
        };
        return ret;
    }
}
