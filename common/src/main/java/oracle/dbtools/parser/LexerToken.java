/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import oracle.dbtools.app.Format;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.util.Service;

public class LexerToken {
    public String content;
    public int begin;
    public int end;
    public Token type;

    /**
     * E.g.
     * select * from emp
		#Tokens=7
		--------------------------------
		0    [0,6) select   <IDENTIFIER>
		1    [6,7)          <WS>
		2    [7,8) *        <OPERATION>
		3    [8,9)          <WS>
		4    [9,13) from    <IDENTIFIER>
		5    [13,14)        <WS>
		6    [14,17) emp    <IDENTIFIER>
		      ^  ^    ^          ^
		      |  |    |         type
		      |  | content
		      |  |
		      |  to
		     from 
     * 
     */
    public LexerToken( CharSequence text, int from, int to, Token t ) {
        content = text.toString();
        begin = from;
        end = to;
        type = t;
    }

    public void print() {
        System.out.println(toString()); // (authorized)
    }
    public String toString() {
        return "["+begin+","+end+") "+content+"   <"+type+">";  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
    }
    public static void print( List<LexerToken> src ) {
        int j = 0;
        for( LexerToken t: src ) {
            System.out.print(j+"    "); // (authorized) //$NON-NLS-1$
            t.print(); 
            j++;
        }
    }
    public static void print( List<LexerToken> src, int from, int to ) {		
        print(src,from,to,false);
    }
    public static void print( List<LexerToken> src, int from, int to, boolean toErr ) {		
        for( int i = from; i < to; i++ ) {
        	if( toErr )
                System.err.print(" "+src.get(i).content); // (authorized) //$NON-NLS-1$
        	else
        		System.out.print(" "+src.get(i).content); // (authorized) //$NON-NLS-1$
        }
    	if( toErr )
            System.err.println(); // (authorized) 
    	else
    		System.out.println(); // (authorized)
    }

    static String defaultLineCommentSymbol = "-";
    static String lineCommentSymbol = defaultLineCommentSymbol;
    /**
     * Use this to switch default '-' to something like '/' for java sources
     * The default is restored as soon as lexing is finished
     * @param newLCS
     */
    public static void switchLineCommentSymbol( String newLCS ) {
        lineCommentSymbol = newLCS;
    }
    
    public static boolean isSqlPlusCmd = false;

    
    private static LinkedList<LexerToken> tokenize( String sourceExpr, int flags, String extraOper, InterruptedException interrupted ) throws InterruptedException{
    	//homegrown one pass method -- should be faster than regexpr based one
        LinkedList<LexerToken> ret = new LinkedList<LexerToken>();
        final String operation = "(){}[]^-|!*+.><='\",;:%@?/\\#~"+extraOper; //$NON-NLS-1$
        final String ws = " \n\r\t"; //$NON-NLS-1$
        StringTokenizer st = new StringTokenizer(sourceExpr,
                                                 //".*-+/|><=()\'\", \n\r\t"
                                                 operation + ws
                                                 ,true);
        int pos = 0;
        boolean isWrapped = false;
        while( st.hasMoreTokens() ) {
            if( interrupted != null && Thread.interrupted() )
                throw interrupted;
            
            String token = st.nextToken()/*.intern()*/;
            pos += token.length();
            // comments
            LexerToken last = null;
            if( ret.size() > 0 )
                last = ret.getLast();
            
            /*if( token.equalsIgnoreCase("accept")  //$NON-NLS-1$
             || token.equalsIgnoreCase("copy") //$NON-NLS-1$
             || token.equalsIgnoreCase("preformat") //$NON-NLS-1$
             || token.equalsIgnoreCase("recover") //$NON-NLS-1$
             || token.equalsIgnoreCase("ttitle") ) //$NON-NLS-1$
            	isSqlPlusCmd = true;*/
            
            if( isWrapped ) {    // nuke everything between WRAPPED and /
                if( "/".equals(token) && last != null && "\n".equals(last.content) ) {
                    final String marker = "\"/\"";
                    ret.add(new LexerToken(marker, pos-marker.length(), pos, Token.IDENTIFIER));
                    isWrapped = false;
                    continue;                
                } else if( "\n".equals(token) ) {
                    ret.add(new LexerToken(token, pos-token.length(), pos, Token.WS));
                    continue;                
                } else {
                    if( "\n".equals(last.content) )
                        last.content = "?";
                    continue;
                }                   
            }
            if( last != null && last.type == Token.COMMENT && (!last.content.endsWith("*/") || last.content.equals("/*/")) ) { //$NON-NLS-1$ //$NON-NLS-2$
                if( "*".equals(token) || "/".equals(token) ) //$NON-NLS-1$ //$NON-NLS-2$
                    last.content = last.content + token;
                else
                    last.content = "/* ... "; // Set up temporarily. Fixed on closing "*/". //$NON-NLS-1$
                last.end = pos;
                // Fix the comment
                if( last != null && last.type == Token.COMMENT && last.content.endsWith("*/") && !last.content.equals("/*/") ) { //$NON-NLS-1$ //$NON-NLS-2$
                    last.content = sourceExpr.substring(last.begin,last.end);
                }
                continue;
            }
            if( last != null && last.type == Token.LINE_COMMENT && !"\n".equals(token) ) { //$NON-NLS-1$
            	if( !"\r".equals(token) )   
            		last.content = last.content + token;
                continue;
            }
            if( last != null && last.type == Token.LINE_COMMENT && "\n".equals(token) ) { //$NON-NLS-1$
                last.end = pos-token.length();
                last.end = last.begin + last.content.length();
                // continue to process token=="\n"
            }
            if( last != null && last.type == Token.QUOTED_STRING 
                    && !(last.isStandardLiteral() || last.isAltLiteral())
                    ) {
                last.content = last.content + token;
                last.end = last.begin + last.content.length();
                continue;
            }
            
            if( last != null && last.type == Token.DQUOTED_STRING && !"\"".equals(token)  //$NON-NLS-1$
                    && !(last.content.endsWith("\"")&&last.content.length()>1)) { //$NON-NLS-1$
                last.content = last.content + token;
                last.end = last.begin + last.content.length();
                continue;
            }
            if( last != null && last.type == Token.DQUOTED_STRING && "\"".equals(token) ) { //$NON-NLS-1$
                //last.content = last.content + token;
                last.end = pos;
                last.content = sourceExpr.substring(last.begin,last.end);
                continue;
            }

            if( last != null && last.type == Token.BQUOTED_STRING && !"`".equals(token)  //$NON-NLS-1$
                    && !(last.content.endsWith("`")&&last.content.length()>1)) { //$NON-NLS-1$
                continue;
            }
            if( last != null && last.type == Token.BQUOTED_STRING && "`".equals(token) ) { //$NON-NLS-1$
                last.end = pos;
                last.content = sourceExpr.substring(last.begin,last.end);
                continue;
            }

            if( "*".equals(token) && last != null && "/".equals(last.content) ) { //$NON-NLS-1$ //$NON-NLS-2$
                last.content = last.content + token;
                last.end = last.begin + last.content.length();
                last.type = Token.COMMENT;
                continue;
            }
            if( lineCommentSymbol.equals(token) && last != null && lineCommentSymbol.equals(last.content) ) { //$NON-NLS-1$ //$NON-NLS-2$
                last.content = last.content + token;
                last.type = Token.LINE_COMMENT;
                continue;
            }    
            if( (flags&SqlPlusComments) == SqlPlusComments )
            if( ("rem".equalsIgnoreCase(token) || "rema".equalsIgnoreCase(token) || "remar".equalsIgnoreCase(token) || "remark".equalsIgnoreCase(token) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$//$NON-NLS-4$//$NON-NLS-5$//$NON-NLS-6$
            ||   "pro".equalsIgnoreCase(token) || "prom".equalsIgnoreCase(token) || "promp".equalsIgnoreCase(token) || "prompt".equalsIgnoreCase(token) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$//$NON-NLS-4$//$NON-NLS-5$//$NON-NLS-6$
                    ) && (last == null || ("\n".equals(last.content)||"\r".equals(last.content))) ) {   //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$//$NON-NLS-4$//$NON-NLS-5$//$NON-NLS-6$
                ret.add(new LexerToken(token, pos-token.length(), -9, Token.LINE_COMMENT));
                continue;
            }

            if( isSqlPlusCmd && last != null && "-".equals(last.content) ) {
            	if( "\n".equals(token) || "\r".equals(token) ) {
            		last.type = Token.SQLPLUSLINECONTINUE_SKIP;
            	}
            }           
            
            if( "$IF".equalsIgnoreCase(token)       //$NON-NLS-1$
             || "$ELSIF".equalsIgnoreCase(token)	//$NON-NLS-1$	
             || "$ELSE".equalsIgnoreCase(token)	    //$NON-NLS-1$	
             || "$END".equalsIgnoreCase(token)		//$NON-NLS-1$
             || "$ERROR".equalsIgnoreCase(token)		//$NON-NLS-1$
            ) {
            	ret.add(new LexerToken(token, pos-token.length(), pos, Token.MACRO_SKIP)); 
            	continue;
            }
            String lastUpper = "N/A";
            if( last != null )
            	lastUpper = last.content.toUpperCase();
			if( last != null && last.type == Token.MACRO_SKIP && lastUpper.startsWith("$IF") && lastUpper.endsWith("$THEN") ) {
            	ret.add(new LexerToken(token, pos-token.length(), pos, Token.MACRO_SKIP)); 
            	continue;
            }
            if( last != null && last.type == Token.MACRO_SKIP && (
            		lastUpper.startsWith("$IF") 
            	 ||	lastUpper.startsWith("$ELSIF") 
            	 ||	lastUpper.startsWith("$ELSE") 
            	 ||	lastUpper.startsWith("$ERROR") 
            	)  ) {
                last.content = last.content + token;
                last.end += token.length();
            	continue;
            }
            
            
            if( last != null && last.type == Token.IDENTIFIER && last.end == -11 && last.content.startsWith("@") && !("\n".equals(token)||"\r".equals(token)) ) {   //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$//$NON-NLS-4$//$NON-NLS-5$//$NON-NLS-6$
                last.content = last.content + token;
                continue;
            }
            if( last != null && last.type == Token.IDENTIFIER && last.end == -11 && last.content.startsWith("@") && ("\n".equals(token)||"\r".equals(token)) ) {   //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$//$NON-NLS-4$//$NON-NLS-5$//$NON-NLS-6$
                last.end = pos-1;
                ret.add(new LexerToken(token, pos-1, pos, Token.WS));
                continue;
            }
            if( (flags&QuotedStrings) == QuotedStrings && "'".equals(token) ) {  // start //$NON-NLS-1$
                if( last != null && (
                        "q".equalsIgnoreCase(last.content) //$NON-NLS-1$
                        || "N".equalsIgnoreCase(last.content) //$NON-NLS-1$
                        || "u".equalsIgnoreCase(last.content) //$NON-NLS-1$
                        || "nq".equalsIgnoreCase(last.content) //$NON-NLS-1$
                ) ) { 
                    last.content += token;
                    last.type = Token.QUOTED_STRING;
                } else
                    ret.add(new LexerToken(token, pos-1, -10, Token.QUOTED_STRING));
                continue;
            }
            if( (flags&QuotedStrings) == QuotedStrings && "\"".equals(token) ) { //$NON-NLS-1$
                ret.add(new LexerToken(token, pos-1, -11, Token.DQUOTED_STRING));
                continue;
            }
            if(  "`".equals(token) && 0 <= operation.indexOf('`') ) { //$NON-NLS-1$
                ret.add(new LexerToken(token, pos-1, -11, Token.BQUOTED_STRING));
                continue;
            }
            if( operation.contains(token) ) {
                ret.add(new LexerToken(token, pos-1, pos, Token.OPERATION));
                continue;
            }
            if( ws.contains(token) ) {
                ret.add(new LexerToken(token, pos-1, pos, Token.WS));
                continue;
            }
            if( '0'<=token.charAt(0) && token.charAt(0)<='9' ) {
                if( !fixedExponent(token,ret,pos-token.length()) ) {
                    if( token.charAt(token.length()-1)=='K' || token.charAt(token.length()-1)=='k'
                     || token.charAt(token.length()-1)=='M' || token.charAt(token.length()-1)=='m'
                     || token.charAt(token.length()-1)=='G' || token.charAt(token.length()-1)=='g'
                     || token.charAt(token.length()-1)=='T' || token.charAt(token.length()-1)=='t'
                     || token.charAt(token.length()-1)=='P' || token.charAt(token.length()-1)=='p'
                     || token.charAt(token.length()-1)=='E' || token.charAt(token.length()-1)=='e'
                    ) {
                        ret.add(new LexerToken(token.substring(0, token.length()-1), pos-token.length(), pos-1, Token.DIGITS));
                        ret.add(new LexerToken(token.substring(token.length()-1), pos-1, pos, Token.DIGITS));
                    } else 
                        ret.add(new LexerToken(token, pos-token.length(), pos, Token.DIGITS));
                }
                continue;
            } 
            if( "WRAPPED".equalsIgnoreCase(token) && last != null ) {
                Iterator<LexerToken> descIter = ret.descendingIterator();
                boolean sawId = false;
                while(descIter.hasNext()) {
                    LexerToken t = descIter.next();
                    if( sawId && ("PROCEDURE".equalsIgnoreCase(t.content) || "FUNCTION".equalsIgnoreCase(t.content) || "TRIGGER".equalsIgnoreCase(t.content) ||
                            "TYPE".equalsIgnoreCase(t.content) || "PACKAGE".equalsIgnoreCase(t.content) ||
                            "BODY".equalsIgnoreCase(t.content)) ) {
                       isWrapped = true;
                       break;
                    }
                    if( t.type == Token.WS || t.type == Token.COMMENT )
                        continue;
                    if( t.type == Token.IDENTIFIER ) {
                        sawId = true;
                        continue;
                    }
                    break;
                }
            }
            ret.add(new LexerToken(token, pos-token.length(), pos, Token.IDENTIFIER));      

        }

        lineCommentSymbol = defaultLineCommentSymbol;
        
        return ret;
    }
    // "1e01" is treated as "digits", "1e+01" is treated as "digits '+' digits" 
    // This seems to be a minor bug -- the containing expressions are OK  
    private static boolean fixedExponent( String input, List<LexerToken> ret, int pos ) {
        if( !input.contains("e") && !input.contains("f") && !input.contains("d") )
            return false;
        StringTokenizer st = new StringTokenizer(input,"efd",true);
        while( st.hasMoreTokens() ) {
            String token = st.nextToken();
            pos += token.length();
            if( '0'<=token.charAt(0) && token.charAt(0)<='9' )            
                ret.add(new LexerToken(token, pos-token.length(), pos, Token.DIGITS));
            else
                ret.add(new LexerToken(token, pos-token.length(), pos, Token.IDENTIFIER));
            
        }
        return true;
    }
    
    /**
     * Lexical part of parsing
     * @param sourceExpr="select * from emp"
     * @return
		#Tokens=4
		--------------------------------
		0    [0,6) select   <IDENTIFIER>
		1    [7,8) *        <OPERATION>
		2    [9,13) from    <IDENTIFIER>
		3    [14,17) emp    <IDENTIFIER>
     */ 
    public static List<LexerToken> parse( String input ) {
        return parse(input, false);
    }
    public static List<LexerToken> parse( String input, String extraOper ) {
        List<LexerToken> ret = new ArrayList<LexerToken>();
        try {
            parse(input, false, QuotedStrings+SqlPlusComments, extraOper, ret, null);
        } catch( InterruptedException e ) {
            throw new AssertionError("parse(...,interrupted==false) has thrown InterruptedException");
        }
        return ret;
    }
    /**
     * Lexical part of parsing
     * @param sourceExpr="select * from emp"
     * @return
     *  if( keepWSandCOMMENTS ) then
		#Tokens=7
		--------------------------------
		0    [0,6) select   <IDENTIFIER>
		1    [6,7)          <WS>     
		2    [7,8) *        <OPERATION>
		3    [8,9)          <WS>
		4    [9,13) from    <IDENTIFIER>
		5    [13,14)        <WS>
		6    [14,17) emp    <IDENTIFIER>
		else
		#Tokens=4
		--------------------------------
		0    [0,6) select   <IDENTIFIER>
		1    [7,8) *        <OPERATION>
		2    [9,13) from    <IDENTIFIER>
		3    [14,17) emp    <IDENTIFIER>
     */ 
    public static List<LexerToken> parse( String input, boolean keepWSandCOMMENTS ) {
        return parse(input, keepWSandCOMMENTS, QuotedStrings+SqlPlusComments);
    }
    
    public static final int QuotedStrings = 1;
    public static final int SqlPlusComments = 2;
    /**
     * @param input see above
     * @param keepWSandCOMMENTS see above
     * @param flags --  bitmask:
     *         QuotedStrings = 1
     *         SqlPlusComments = 2
     * @return
     */
    public static List<LexerToken> parse( String input, boolean keepWSandCOMMENTS, int flags ) {
        try {
            return parse(input,keepWSandCOMMENTS, flags, null);
        } catch ( InterruptedException e ) { // never thrown
            throw new AssertionError("parse(...,interrupted==false) has thrown InterruptedException");
        }         
    }
    public static List<LexerToken> parse( String input, boolean keepWSandCOMMENTS, int flags, InterruptedException interrupted ) throws InterruptedException {
        List<LexerToken> ret = new ArrayList<LexerToken>();
        parse(input, keepWSandCOMMENTS, flags, "", ret, interrupted);
        return ret;
    }
   private static void parse( String input, boolean keepWSandCOMMENTS, int flags, String extraOper, List<LexerToken> ret, InterruptedException interrupted ) throws InterruptedException {
        //return parse((CharSequence)input);
        LexerToken last = null;
        for( LexerToken token : tokenize(input, flags, extraOper, interrupted) ) {
            if( token.type == Token.QUOTED_STRING ) {   // glue strings together
                if( last != null && last.type == Token.QUOTED_STRING ) {
                    //last.content = last.content + token.content;
                    last.content = last.content + token.content/*.substring(1)*/; //accept c default 'de''fault'
                    last.end = token.end;
                    continue;
                }
                if( last != null && last.type == Token.IDENTIFIER 
                 && "n".equalsIgnoreCase(last.content) && last.end==token.begin ) {
                    last.begin = token.begin;
                    last.end = token.end;
                    last.type = token.type;
                    last.content = token.content;
                    continue;
                }
            }
            if( token.content.startsWith("@") )
            	token.end = token.begin + token.content.length();
            // Conflicting requirements for #: 
            // 1.  q'#Alt literals#'
            // 2. Identifiers: abc#23
            if( "#".equals(token.content) && last != null )if( last.type == Token.IDENTIFIER ) {
                last.end += 1;
                last.content += "#";
                continue;
            }
            if( (token.type == Token.IDENTIFIER||token.type == Token.DIGITS)  && last != null )if( last.content.endsWith("#") && last.type == Token.IDENTIFIER ) {
                last.end += token.content.length();
                last.content += token.content;
                continue;
            }
            if( keepWSandCOMMENTS || token.type != Token.WS && token.type != Token.COMMENT && token.type != Token.LINE_COMMENT && token.type != Token.MACRO_SKIP && token.type != Token.SQLPLUSLINECONTINUE_SKIP  )
                ret.add(token);
            last = token;
        }
    }

    /**
     * Converts character offset into LexerToken position
     * E.g. 
     * select * from emp
		0    [0,6) select   <IDENTIFIER>
		1    [6,7)          <WS>
		2    [7,8) *        <OPERATION>
		3    [8,9)          <WS>
		^     |
		|     |    scanner2parserOffset(8)==3
		 -----
		4    [9,13) from    <IDENTIFIER>
		5    [13,14)        <WS>
		6    [14,17) emp    <IDENTIFIER>
     * 
     */
    public static int scanner2parserOffset( List<LexerToken> src, int start ) {
        int offset = -1;
        for( LexerToken t : src ) {
            offset++;
            if( t.end > start ) 
                break;
        }
        return offset;
    }
    
    public static LexerToken getTokenAtCharOffset( List<LexerToken> src, int offset ) {
        if( src.size()==0 )
            return null;
        if( src.size()==1 ) { // special case; indexOf would throw AssertionError
            LexerToken ret = src.get(0);
            if( offset < ret.begin || ret.end <= offset )
                ret = null;
            return ret;
        }
        int index = indexOf(src,0,src.size()-1,offset);
        return src.get(index);
    }
    
    public static int char2lex( List<LexerToken> src, int offset ) {
        if( src.size()==0 )
            return 0;
        int pos = -1;
        for( LexerToken t:  src ) {
            pos++;
            if( t.begin <= offset && offset <= t.end )
                return pos;
            if( offset <= t.begin ) // next token
                return pos;
        }
        return pos+1;
        /*if( src.size()==0 )
            return 0;
        if( src.size() == 1 && offset <= src.get(0).end )
            return 0;
        if( src.get(src.size()-1).end < offset  )
            return src.size()-1;
        return indexOf(src,0,src.size()-1,offset);*/
    }

    
    /**
     * Helper "divide and concur"
     * @param x start of progressively narrowing range
     * @param y end
     * @param offset 
     * @return
     */
    private static int indexOf( List<LexerToken> src, int x,int y, int offset ) {
//System.out.println("x="+x);
//System.out.println("y="+y);
//System.out.println("offset="+offset);
        if( x == y )
            throw new AssertionError("x == y (== "+x+")");
        if( x+1 == y ) {
            LexerToken l1 = src.get(x);
            if( l1.begin <= offset && offset < l1.end )
                return x;
            else {
                LexerToken l2 = src.get(y);
                if( l2.begin <= offset && offset < l2.end )
                    return y;
                throw new AssertionError(x);
            }
        }
        int mid = (x+y)/2;
        if( offset < src.get(mid).begin )
            return indexOf(src, x,mid, offset);
        else
            return indexOf(src, mid,y, offset);
    }

    
    private static char matchingDelimiter( char ch ) {
        if( '<'==(ch) ) return '>'; //$NON-NLS-1$ //$NON-NLS-2$
        else if( '['==(ch) ) return ']'; //$NON-NLS-1$ //$NON-NLS-2$
        else if( '{'==(ch) ) return '}'; //$NON-NLS-1$ //$NON-NLS-2$
        else if( '('==(ch) ) return ')'; //$NON-NLS-1$ //$NON-NLS-2$
        else return ch;
    }
    boolean isStandardLiteral() {
        // fast fail
        if( content.length() < 2 )
            return false;
        if( !(content.charAt(0)=='\'' 
        		|| content.charAt(0)=='n' || content.charAt(0)=='N'
        		|| content.charAt(0)=='u' || content.charAt(0)=='U') )
            return false;
        
        String text = content;
        if( text.charAt(0)=='n' || text.charAt(0)=='N' 
         || text.charAt(0)=='u' || text.charAt(0)=='U') {
            if( text.length() < 3 )
                return false;
            text = text.substring(1);
        }
        if( text.length() < 2 )
            return false;
        return text.charAt(0)=='\'' && text.charAt(text.length()-1)=='\'';
    }
    boolean isAltLiteral() {
        // fast fail
        if( content.length() < 5 )
            return false;
        if( !(content.charAt(0)=='q' || content.charAt(0)=='Q' 
            || content.charAt(0)=='n' || content.charAt(0)=='N'
            ||	content.charAt(0)=='u' || content.charAt(0)=='U') )
            return false;
        
        String text = content;
        if( content.charAt(0)=='q' || content.charAt(0)=='Q'
        	||	content.charAt(0)=='u' || content.charAt(0)=='U') {
            text = text.substring(1);
        } else if( /*content.startsWith("Nq")*/
                 (content.charAt(0)=='n' || content.charAt(0)=='N') 
              && (content.charAt(1)=='q' || content.charAt(1)=='Q')    
        ) {
            if( text.length() < 6 )
                return false;
            text = text.substring(2);
        } else
            return false;
        if( text.charAt(0)=='\'' && text.charAt(text.length()-1)=='\'' )
            text = text.substring(1,text.length()-1);
        else
            return false;

        return matchingDelimiter(text.charAt(0)) == text.charAt(text.length()-1);       
    }
    
    /**
     * Shifts all tokens (i.e. increments all intervals) in src by offset
     * Typical usage includes a parsed fragment inside a larger fragment
     * where offset is the beginning of the fragment (in lexer units)
     * @param src
     * @param offset
     */
    public static void moveInterval( List<LexerToken> src, int offset ) {
        for( LexerToken t : src ) {
            t.begin += offset;
            t.end += offset;
        }
    }
    
    /**
     * Helper method to render abbreviated content of parsed text covered by ParseNode
     * @param from  -- beginning of node interval
     * @param to -- end of node interval
     * @param src  -- lexed text
     * @return
     */
    public static String mnemonics( int from, int to, List<LexerToken> src ) {
        int _8 = 8;
        if( from + 1 == to)
            return Service.padln(_8+2<src.get(from).content.length() ? src.get(from).content.substring(0,_8+2) : src.get(from).content, _8+2);
        else {  // Node containing many tokens
                // Get _8 word's first letters with capitalized keywords 
                // e.g. "emp=10 and dept=emp" -> "e=1Ad=e"
            StringBuilder ret = new StringBuilder("\"");
            for( int i = from; i < to && i < from+_8 ; i++ ) {               
                String token = src.get(i).content.toUpperCase();
                String t = token.substring(0,1).toLowerCase();
                for( int j = 0; j < SqlEarley.keywords.length; j++ ) 
                    if( SqlEarley.keywords[j].substring(1,SqlEarley.keywords[j].length()-1).equals(token) ) {
                        t = t.toUpperCase();
                        break;
                    }                   
                ret.append(t);
            }
            ret.append('\"');
            return Service.padln(ret.toString(),_8+2);
        }
    }

    
    public static void testFilesForFolder(final File folder) throws FileNotFoundException, IOException {
        for ( final File fileEntry : folder.listFiles() ) {
            if (fileEntry.isDirectory()) {
                testFilesForFolder(fileEntry);
            } else {
            	String fileName = fileEntry.getAbsolutePath();
            	if( fileName.endsWith(".sql") || fileName.endsWith(".plb") )
                testInput(Service.readFile(fileName));
            }
        }
    }

	private static void testApex() throws FileNotFoundException, IOException {
		final File folder = new File("C:\\Users\\Dim\\Downloads\\apex_5.1.1_en");
		testFilesForFolder(folder);	
	}
    
    static long incr = 0;
    private static void testInput(String input) throws IOException {
        //System.gc();
        //long t1 = System.currentTimeMillis();
        //long h = Runtime.getRuntime().totalMemory();
        //long hf = Runtime.getRuntime().freeMemory();
        //System.out.println("mem="+(h-hf)); // (authorized) //$NON-NLS-1$
        //List<LexerToken> out = parse(input,true,true);
    	long h = System.nanoTime();
        List<LexerToken> out = parse(input,true);
        incr += System.nanoTime()-h;
        //long t2 = System.currentTimeMillis();
        //System.out.println("Lexer time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
        //System.gc();
        //h = Runtime.getRuntime().totalMemory();
        //hf = Runtime.getRuntime().freeMemory();
        //System.out.println("mem1="+(h-hf)); // (authorized) //$NON-NLS-1$
        //System.out.println("#Tokens="+out.size()); // (authorized)
        if( out.size() < 1000 )
        	LexerToken.print(out);
    }

    /**
     * Usage example
     * @param args -- dummy
     * @throws Exception
     */
    public static void main( String[] args ) throws Exception {

        //Service.profile(10000, 100);
        String[] inputs = {
        /*  "select N'nchar literal'  LITERAL1   from dual"
         ,"select q'!name LIKE '%DBMS_%%'!' LITERAL2   from dual "
         ,"select q'<'So,' she said, 'It's finished.'>' LITERAL3   from dual "
         ,"select q'{SELECT * FROM employees WHERE last_name = 'Smith';}' LITERAL4 from dual "
         ,"select q'\"name like '['\"' LITERAL5   from dual "
         //,"$IF select 'aaaaabbbbb' LITERAL6   from dual "
         , "select nq'!ncha'r lit!' LITERAL7   from dual"
         , "select nQ'{nchar lit}' LITERAL8   from dual"
      		,"SELECT 'This string has a single quote ('')' LITERAL9 FROM dual"*/
            //ORA-01756: quoted string not properly terminated: , "select Nq'ï¿½ ï¿½1234 ï¿½' LITERAL10   from dual"
        	    "select /*aaa; ",
        		"select 'aaa; ",
        		"select \"EMP",
        		//Service.readFile(Earley.class, "test.sql"),
        		//Service.readFile(SqlEarley.class, "MSC_CL_PRE_PROCESS.pkgbdy"),
        };        


        long t1 = System.currentTimeMillis();
        for (int i = 0; i < inputs.length; i++) {
        	testInput(inputs[i]);
		}        
        long t2 = System.currentTimeMillis();
        System.out.println("Lexer time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
        System.out.println("Nano time = "+incr); // (authorized) //$NON-NLS-1$
        //System.out.println("length="+sum); // (authorized)
        
        /*for( LexerToken t : out )
        	if( t.begin < 843 && 843 < t.end )
        		System.out.println(t.type+"@["+t.begin+","+t.end+") "+t.toString());
        System.out.println("ascii(')="+(int)'\'');
        System.out.println((char)8221);
        String s = "select 'שדג' from  dual;";
        for( int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			System.out.println(c+"->"+Character.getDirectionality(c)+"    "+(int)c);
		}*/
    }


}
