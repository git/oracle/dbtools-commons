/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.dbtools.parser.Parser.Tuple;

/**
 * Major data structure for both Earley and CYK methods
 * @author Dim
 */
public class Matrix extends Matriceable {
	    
    private Map<Integer,Cell>[] cells = null;
    private int lastY = 0;
        
    public int[] allXs = null;
    public Integer LAsuspect = null;

    public Matrix( Parser p ) {
        this.parser = p;
    }

    //former TreeMap.get(Service.lPair(x, y));
    public Cell get( int x, int y ) {
        return cells[y].get(x);
    }
    public void put( int x, int y, Cell content ) {
        if( lastY < y )
            lastY = y;
        cells[y].put(x,content);
    }
    
    public void initCells( int length ) {
        cells = new Map[length+1];
        for( int i = 0; i < cells.length; i++ ) 
            cells[i] = new HashMap<Integer,Cell>();        
    }
    
    public int lastY() {
        return lastY;
    }
    
    public Map<Integer, Cell> getXRange( int y ) {
        return cells[y];
    }
    
    
    /**
     * Mostly for visualization as debugger variable
     * ASCII alternative to Visual GUI
     */
    public String toString() throws RuntimeException {
        StringBuffer ret = new StringBuffer();
        for( int y = 0; y < cells.length; y++ ) for( int x : cells[y].keySet() ) {
            Cell output = get(x, y);
            if( output ==  null ) {
                System.out.println(".\n");
                continue;
                //throw new AssertionError("no value corresponding to the key ["+x+","+y+")");  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            }
            ret.append("["+x+","+y+")");  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            final int cutover = 50;
            if( cutover < output.size() )
                ret.append(" ... "+output.size()+" more symbols");
            else
                for( int i = 0; i < output.size() && i < cutover+1 ; i++ ) {
                    ((Earley)parser).toString(output.getRule(i), output.getPosition(i), ret);
                    //if( i == cutover )
                        //ret.append(" ... "+output.size()+" more symbols");

                }

            ret.append("\n\n"); 
        }
        return ret.toString();
    }


    
    @Override
    public List<Integer> getEarleyBackptrs( int x, int y, Cell cell, int index ) {
        List<Integer> ret = new ArrayList<Integer>();
        if( x == y && x == 0 ) {
            ret.add(x);
            return ret;
        }
        int rule = cell.getRule(index);
        Tuple tuple = ((Earley)parser).rules[rule];
        int pos = cell.getPosition(index);
        
        // predict
        if( x == y ) {
            //long start = Service.lPair(0,y);
            //long end = Service.lPair(x,y);
            Map<Integer,Cell> xRange = cells[y];
            //SortedMap<Long,Cell> range = subMap(start, true, end, true);
            for( int mid : xRange.keySet() ) {
                //int mid = Service.lX(key);
                if( x < mid )
                    continue;
                Cell candidate = get(mid,x);
                if( candidate==null )
                    continue;
                for( int i = 0; i < candidate.size(); i++ ) {
                    Tuple candTuple = ((Earley)parser).rules[candidate.getRule(i)];
                    int candPos = candidate.getPosition(i);
					if( candPos < candTuple.rhs.length && candTuple.rhs[candPos]==tuple.head ) {
                        if( !ret.contains(mid) )
                            ret.add(mid);
                        break;
                    }
                }
            }
            return ret;
        }
        
        // scan
        Cell candidate = get(x,y-1);
        if( candidate!=null ) 
            for( int i = 0; i < candidate.size(); i++ ) {
                int candRule = candidate.getRule(i);
                if( rule != candRule )
                    continue;
                int candPos = candidate.getPosition(i);
                if( candPos + 1 != pos )
                    continue;
                Tuple candTuple = ((Earley)parser).rules[candRule];
                if( candTuple.rhs[candPos] == ((Earley)parser).identifier
                 || candTuple.rhs[candPos] == ((Earley)parser).string_literal    
                 || candTuple.rhs[candPos] == ((Earley)parser).digits 
                 || parser.allSymbols[candTuple.rhs[candPos]].charAt(0)=='\''   
                        ) {
                    ret.add(y+1);
                    break;
                }
            }
        
        // complete
        //long start = Service.lPair(x,y);
        //long end = Service.lPair(y,y);
        Map<Integer,Cell> xRange = cells[y];
        //SortedMap<Long,Cell> range = subMap(start, true, end, true);
        for( int mid : xRange.keySet() ) {
            if( mid < x || y < mid )
                continue;
            Cell pre = get(x,mid);
            if( pre==null )
                continue;
            Cell post = get(mid,y);
            if( post==null )
                continue;
            nextCell:      
            for( int i = 0; i < pre.size(); i++ ) 
            	for( int j = 0; j < post.size(); j++ ) {
                    int rulePre = pre.getRule(i);
                    int rulePost = post.getRule(j); 
                    int dotPre = pre.getPosition(i);
                    int dotPost = post.getPosition(j);
                    Tuple tPre = ((Earley)parser).rules[rulePre];
                    Tuple tPost = ((Earley)parser).rules[rulePost];
                    if( tPost.size() == dotPost ) {
                        if( rulePre != rule )
                        	continue;
                    	if( dotPre+1 != pos )
                    		continue;
                        int symPre = tPre.content(dotPre);
                        if( symPre != tPost.head )
                            continue;
                        ret.add(mid);
                        break nextCell;
                    }
					
				}
            	
            
        }
        
        
        return ret;
    }

	@Override
	public Matriceable recalc() {
        Matrix matrix = new Matrix(parser);
        matrix.visual = visual;
        return matrix;
	}
	

}

