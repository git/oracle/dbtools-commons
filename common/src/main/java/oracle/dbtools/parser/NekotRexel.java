/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import oracle.dbtools.util.Array;
import oracle.dbtools.util.Service;

/**
 * Do lexical analysis backwards
 * Useful for inputs with broken syntax, e.g. having quote or comment closing delimiter missing
 * @author Dim
 */
public class NekotRexel {

	private static LinkedList<LexerToken> tokenize( String sourceExpr, int flags, String extraOper, InterruptedException interrupted ) throws InterruptedException{
		//homegrown one pass method -- should be faster than regexpr based one
		LinkedList<LexerToken> ret = new LinkedList<LexerToken>();
		final String operation = "(){}[]^-|!*+.><='\",;:%@?/\\#~"+extraOper; //$NON-NLS-1$
		final String ws = " \n\r\t"; //$NON-NLS-1$
		String delimStr = operation + ws;
		int[] delims = null;
		for( int i = 0; i < delimStr.length(); i++ ) {
			delims = Array.insert(delims, delimStr.charAt(i));
		}

		int pos = 0;
		boolean isWrapped = false;

		String aggr = "";
		for( int i = sourceExpr.length()-1; 0 <= i ; i-- ) {

			if( interrupted != null && Thread.interrupted() )
				throw interrupted;

			char cur = sourceExpr.charAt(i);
			int curI = Array.indexOf(delims, cur);
			char match = (char) delims[curI];
			if( cur != match ) {
				aggr = cur + aggr;
				continue;
			} //else
			String token;
			if( aggr.length() == 0 ) {
				token = new String(new char[]{cur});
			} else {
				token = aggr.toString();
				aggr = "";
				i++;  // to process the delimiter once more
			}

			pos += token.length();
			// comments
			LexerToken last = null;
			if( ret.size() > 0 )
				last = ret.getLast();

			/*if( token.equalsIgnoreCase("accept")  //$NON-NLS-1$
	             || token.equalsIgnoreCase("copy") //$NON-NLS-1$
	             || token.equalsIgnoreCase("preformat") //$NON-NLS-1$
	             || token.equalsIgnoreCase("recover") //$NON-NLS-1$
	             || token.equalsIgnoreCase("ttitle") ) //$NON-NLS-1$
	            	isSqlPlusCmd = true;*/

			if( isWrapped ) {    // nuke everything between WRAPPED and /
				if( "/".equals(token) && last != null && "\n".equals(last.content) ) {
					final String marker = "\"/\"";
					ret.add(new LexerToken(marker, pos-marker.length(), pos, Token.IDENTIFIER));
					isWrapped = false;
					continue;                
				} else if( "\n".equals(token) ) {
					ret.add(new LexerToken(token, pos-token.length(), pos, Token.WS));
					continue;                
				} else {
					if( "\n".equals(last.content) )
						last.content = "?";
					continue;
				}                   
			}
			if( last != null && last.type == Token.COMMENT && (!last.content.endsWith("*/") || last.content.equals("/*/")) ) { //$NON-NLS-1$ //$NON-NLS-2$
				if( "*".equals(token) || "/".equals(token) ) //$NON-NLS-1$ //$NON-NLS-2$
					last.content = last.content + token;
				else
					last.content = "/* ... "; // Set up temporarily. Fixed on closing "*/". //$NON-NLS-1$
				last.end = pos;
				// Fix the comment
				if( last != null && last.type == Token.COMMENT && last.content.endsWith("*/") && !last.content.equals("/*/") ) { //$NON-NLS-1$ //$NON-NLS-2$
					last.content = sourceExpr.substring(last.begin,last.end);
				}
				continue;
			}
			if( last != null && last.type == Token.LINE_COMMENT && !"\n".equals(token) ) { //$NON-NLS-1$
				if( !"\r".equals(token) )   
					last.content = last.content + token;
				continue;
			}
			if( last != null && last.type == Token.LINE_COMMENT && "\n".equals(token) ) { //$NON-NLS-1$
				last.end = pos-token.length();
				last.end = last.begin + last.content.length();
				// continue to process token=="\n"
			}
			if( last != null && last.type == Token.QUOTED_STRING 
					&& !(last.isStandardLiteral() || last.isAltLiteral())
					) {
				last.content = last.content + token;
				last.end = last.begin + last.content.length();
				continue;
			}

			if( last != null && last.type == Token.DQUOTED_STRING && !"\"".equals(token)  //$NON-NLS-1$
					&& !(last.content.endsWith("\"")&&last.content.length()>1)) { //$NON-NLS-1$
				last.content = last.content + token;
				last.end = last.begin + last.content.length();
				continue;
			}
			if( last != null && last.type == Token.DQUOTED_STRING && "\"".equals(token) ) { //$NON-NLS-1$
				//last.content = last.content + token;
				last.end = pos;
				last.content = sourceExpr.substring(last.begin,last.end);
				continue;
			}

			if( last != null && last.type == Token.BQUOTED_STRING && !"`".equals(token)  //$NON-NLS-1$
					&& !(last.content.endsWith("`")&&last.content.length()>1)) { //$NON-NLS-1$
				continue;
			}
			if( last != null && last.type == Token.BQUOTED_STRING && "`".equals(token) ) { //$NON-NLS-1$
				last.end = pos;
				last.content = sourceExpr.substring(last.begin,last.end);
				continue;
			}

			if( "*".equals(token) && last != null && "/".equals(last.content) ) { //$NON-NLS-1$ //$NON-NLS-2$
				last.content = last.content + token;
				last.end = last.begin + last.content.length();
				last.type = Token.COMMENT;
				continue;
			}
			if( LexerToken.lineCommentSymbol.equals(token) && last != null && LexerToken.lineCommentSymbol.equals(last.content) ) { //$NON-NLS-1$ //$NON-NLS-2$
				last.content = last.content + token;
				last.type = Token.LINE_COMMENT;
				continue;
			}    
			if( (flags&SqlPlusComments) == SqlPlusComments )
				if( ("rem".equalsIgnoreCase(token) || "rema".equalsIgnoreCase(token) || "remar".equalsIgnoreCase(token) || "remark".equalsIgnoreCase(token) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$//$NON-NLS-4$//$NON-NLS-5$//$NON-NLS-6$
						||   "pro".equalsIgnoreCase(token) || "prom".equalsIgnoreCase(token) || "promp".equalsIgnoreCase(token) || "prompt".equalsIgnoreCase(token) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$//$NON-NLS-4$//$NON-NLS-5$//$NON-NLS-6$
						) && (last == null || ("\n".equals(last.content)||"\r".equals(last.content))) ) {   //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$//$NON-NLS-4$//$NON-NLS-5$//$NON-NLS-6$
					ret.add(new LexerToken(token, pos-token.length(), -9, Token.LINE_COMMENT));
					continue;
				}

			/* TODO : fix it
			if( isSqlPlusCmd && last != null && "-".equals(last.content) ) {
				if( "\n".equals(token) || "\r".equals(token) ) {
					last.type = Token.SQLPLUSLINECONTINUE_SKIP;
				}
			}*/           

			if( "$IF".equalsIgnoreCase(token)       //$NON-NLS-1$
					|| "$ELSIF".equalsIgnoreCase(token)	//$NON-NLS-1$	
					|| "$ELSE".equalsIgnoreCase(token)	    //$NON-NLS-1$	
					|| "$END".equalsIgnoreCase(token)		//$NON-NLS-1$
					|| "$ERROR".equalsIgnoreCase(token)		//$NON-NLS-1$
					) {
				ret.add(new LexerToken(token, pos-token.length(), pos, Token.MACRO_SKIP)); 
				continue;
			}
			String lastUpper = "N/A";
			if( last != null )
				lastUpper = last.content.toUpperCase();
			if( last != null && last.type == Token.MACRO_SKIP && lastUpper.startsWith("$IF") && lastUpper.endsWith("$THEN") ) {
				ret.add(new LexerToken(token, pos-token.length(), pos, Token.MACRO_SKIP)); 
				continue;
			}
			if( last != null && last.type == Token.MACRO_SKIP && (
					lastUpper.startsWith("$IF") 
					||	lastUpper.startsWith("$ELSIF") 
					||	lastUpper.startsWith("$ELSE") 
					||	lastUpper.startsWith("$ERROR") 
					)  ) {
				last.content = last.content + token;
				last.end += token.length();
				continue;
			}


			if( last != null && last.type == Token.IDENTIFIER && last.end == -11 && last.content.startsWith("@") && !("\n".equals(token)||"\r".equals(token)) ) {   //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$//$NON-NLS-4$//$NON-NLS-5$//$NON-NLS-6$
				last.content = last.content + token;
				continue;
			}
			if( last != null && last.type == Token.IDENTIFIER && last.end == -11 && last.content.startsWith("@") && ("\n".equals(token)||"\r".equals(token)) ) {   //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$//$NON-NLS-4$//$NON-NLS-5$//$NON-NLS-6$
				last.end = pos-1;
				ret.add(new LexerToken(token, pos-1, pos, Token.WS));
				continue;
			}
			if( (flags&QuotedStrings) == QuotedStrings && "'".equals(token) ) {  // start //$NON-NLS-1$
				if( last != null && (
						"q".equalsIgnoreCase(last.content) //$NON-NLS-1$
						|| "N".equalsIgnoreCase(last.content) //$NON-NLS-1$
						|| "u".equalsIgnoreCase(last.content) //$NON-NLS-1$
						|| "nq".equalsIgnoreCase(last.content) //$NON-NLS-1$
						) ) { 
					last.content += token;
					last.type = Token.QUOTED_STRING;
				} else
					ret.add(new LexerToken(token, pos-1, -10, Token.QUOTED_STRING));
				continue;
			}
			if( (flags&QuotedStrings) == QuotedStrings && "\"".equals(token) ) { //$NON-NLS-1$
				ret.add(new LexerToken(token, pos-1, -11, Token.DQUOTED_STRING));
				continue;
			}
			if(  "`".equals(token) && 0 <= operation.indexOf('`') ) { //$NON-NLS-1$
				ret.add(new LexerToken(token, pos-1, -11, Token.BQUOTED_STRING));
				continue;
			}
			if( operation.contains(token) ) {
				ret.add(new LexerToken(token, pos-1, pos, Token.OPERATION));
				continue;
			}
			if( ws.contains(token) ) {
				ret.add(new LexerToken(token, pos-1, pos, Token.WS));
				continue;
			}
			if( '0'<=token.charAt(0) && token.charAt(0)<='9' ) {
				if( !fixedExponent(token,ret,pos-token.length()) ) {
					if( token.charAt(token.length()-1)=='K' || token.charAt(token.length()-1)=='k'
							|| token.charAt(token.length()-1)=='M' || token.charAt(token.length()-1)=='m'
							|| token.charAt(token.length()-1)=='G' || token.charAt(token.length()-1)=='g'
							|| token.charAt(token.length()-1)=='T' || token.charAt(token.length()-1)=='t'
							|| token.charAt(token.length()-1)=='P' || token.charAt(token.length()-1)=='p'
							|| token.charAt(token.length()-1)=='E' || token.charAt(token.length()-1)=='e'
							) {
						ret.add(new LexerToken(token.substring(0, token.length()-1), pos-token.length(), pos-1, Token.DIGITS));
						ret.add(new LexerToken(token.substring(token.length()-1), pos-1, pos, Token.DIGITS));
					} else 
						ret.add(new LexerToken(token, pos-token.length(), pos, Token.DIGITS));
				}
				continue;
			} 
			if( "WRAPPED".equalsIgnoreCase(token) && last != null ) {
				Iterator<LexerToken> descIter = ret.descendingIterator();
				boolean sawId = false;
				while(descIter.hasNext()) {
					LexerToken t = descIter.next();
					if( sawId && ("PROCEDURE".equalsIgnoreCase(t.content) || "FUNCTION".equalsIgnoreCase(t.content) || "TRIGGER".equalsIgnoreCase(t.content) ||
							"TYPE".equalsIgnoreCase(t.content) || "PACKAGE".equalsIgnoreCase(t.content) ||
							"BODY".equalsIgnoreCase(t.content)) ) {
						isWrapped = true;
						break;
					}
					if( t.type == Token.WS || t.type == Token.COMMENT )
						continue;
					if( t.type == Token.IDENTIFIER ) {
						sawId = true;
						continue;
					}
					break;
				}
			}
			ret.add(new LexerToken(token, pos-token.length(), pos, Token.IDENTIFIER));      

		}

		LexerToken.lineCommentSymbol = LexerToken.defaultLineCommentSymbol;

		return ret;
	}
	// "1e01" is treated as "digits", "1e+01" is treated as "digits '+' digits" 
	// This seems to be a minor bug -- the containing expressions are OK  
	private static boolean fixedExponent( String input, List<LexerToken> ret, int pos ) {
		if( !input.contains("e") && !input.contains("f") && !input.contains("d") )
			return false;
		StringTokenizer st = new StringTokenizer(input,"efd",true);
		while( st.hasMoreTokens() ) {
			String token = st.nextToken();
			pos += token.length();
			if( '0'<=token.charAt(0) && token.charAt(0)<='9' )            
				ret.add(new LexerToken(token, pos-token.length(), pos, Token.DIGITS));
			else
				ret.add(new LexerToken(token, pos-token.length(), pos, Token.IDENTIFIER));

		}
		return true;
	}

	/**
	 * Lexical part of parsing
	 * @param sourceExpr="select * from emp"
	 * @return
			#Tokens=4
			--------------------------------
			0    [0,6) select   <IDENTIFIER>
			1    [7,8) *        <OPERATION>
			2    [9,13) from    <IDENTIFIER>
			3    [14,17) emp    <IDENTIFIER>
	 */ 
	public static List<LexerToken> parse( String input ) {
		return parse(input, false);
	}
	public static List<LexerToken> parse( String input, String extraOper ) {
		List<LexerToken> ret = new ArrayList<LexerToken>();
		try {
			parse(input, false, QuotedStrings+SqlPlusComments, extraOper, ret, null);
		} catch( InterruptedException e ) {
			throw new AssertionError("parse(...,interrupted==false) has thrown InterruptedException");
		}
		return ret;
	}
	/**
	 * Lexical part of parsing
	 * @param sourceExpr="select * from emp"
	 * @return
	 *  if( keepWSandCOMMENTS ) then
			#Tokens=7
			--------------------------------
			0    [0,6) select   <IDENTIFIER>
			1    [6,7)          <WS>     
			2    [7,8) *        <OPERATION>
			3    [8,9)          <WS>
			4    [9,13) from    <IDENTIFIER>
			5    [13,14)        <WS>
			6    [14,17) emp    <IDENTIFIER>
			else
			#Tokens=4
			--------------------------------
			0    [0,6) select   <IDENTIFIER>
			1    [7,8) *        <OPERATION>
			2    [9,13) from    <IDENTIFIER>
			3    [14,17) emp    <IDENTIFIER>
	 */ 
	public static List<LexerToken> parse( String input, boolean keepWSandCOMMENTS ) {
		return parse(input, keepWSandCOMMENTS, QuotedStrings+SqlPlusComments);
	}

	public static final int QuotedStrings = 1;
	public static final int SqlPlusComments = 2;
	/**
	 * @param input see above
	 * @param keepWSandCOMMENTS see above
	 * @param flags --  bitmask:
	 *         QuotedStrings = 1
	 *         SqlPlusComments = 2
	 * @return
	 */
	public static List<LexerToken> parse( String input, boolean keepWSandCOMMENTS, int flags ) {
		try {
			return parse(input,keepWSandCOMMENTS, flags, null);
		} catch ( InterruptedException e ) { // never thrown
			throw new AssertionError("parse(...,interrupted==false) has thrown InterruptedException");
		}         
	}
	public static List<LexerToken> parse( String input, boolean keepWSandCOMMENTS, int flags, InterruptedException interrupted ) throws InterruptedException {
		List<LexerToken> ret = new ArrayList<LexerToken>();
		parse(input, keepWSandCOMMENTS, flags, "", ret, interrupted);
		return ret;
	}
	private static void parse( String input, boolean keepWSandCOMMENTS, int flags, String extraOper, List<LexerToken> ret, InterruptedException interrupted ) throws InterruptedException {
		//return parse((CharSequence)input);
		LexerToken last = null;
		for( LexerToken token : tokenize(input, flags, extraOper, interrupted) ) {
			if( token.type == Token.QUOTED_STRING ) {   // glue strings together
				if( last != null && last.type == Token.QUOTED_STRING ) {
					//last.content = last.content + token.content;
					last.content = last.content + token.content/*.substring(1)*/; //accept c default 'de''fault'
					last.end = token.end;
					continue;
				}
				if( last != null && last.type == Token.IDENTIFIER 
						&& "n".equalsIgnoreCase(last.content) && last.end==token.begin ) {
					last.begin = token.begin;
					last.end = token.end;
					last.type = token.type;
					last.content = token.content;
					continue;
				}
			}
			if( token.content.startsWith("@") )
				token.end = token.begin + token.content.length();
			// Conflicting requirements for #: 
			// 1.  q'#Alt literals#'
			// 2. Identifiers: abc#23
			if( "#".equals(token.content) && last != null )if( last.type == Token.IDENTIFIER ) {
				last.end += 1;
				last.content += "#";
				continue;
			}
			if( (token.type == Token.IDENTIFIER||token.type == Token.DIGITS)  && last != null )if( last.content.endsWith("#") && last.type == Token.IDENTIFIER ) {
				last.end += token.content.length();
				last.content += token.content;
				continue;
			}
			if( keepWSandCOMMENTS || token.type != Token.WS && token.type != Token.COMMENT && token.type != Token.LINE_COMMENT && token.type != Token.MACRO_SKIP && token.type != Token.SQLPLUSLINECONTINUE_SKIP  )
				ret.add(token);
			last = token;
		}
	}



	static long incr = 0;
	private static void testInput( String input ) throws IOException {
		//System.gc();
		//long t1 = System.currentTimeMillis();
		//long h = Runtime.getRuntime().totalMemory();
		//long hf = Runtime.getRuntime().freeMemory();
		//System.out.println("mem="+(h-hf)); // (authorized) //$NON-NLS-1$
		//List<LexerToken> out = parse(input,true,true);
		long h = System.nanoTime();
		List<LexerToken> out = parse(input,true);
		incr += System.nanoTime()-h;
		//long t2 = System.currentTimeMillis();
		//System.out.println("Lexer time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
		//System.gc();
		//h = Runtime.getRuntime().totalMemory();
		//hf = Runtime.getRuntime().freeMemory();
		//System.out.println("mem1="+(h-hf)); // (authorized) //$NON-NLS-1$
		//System.out.println("#Tokens="+out.size()); // (authorized)
		if( out.size() < 1000 )
			LexerToken.print(out);
		System.out.println("-------------");
	}

	/**
	 * Usage example
	 * @param args -- dummy
	 * @throws Exception
	 */
	public static void main( String[] args ) throws Exception {

		//Service.profile(10000, 100);
		String[] inputs = {
				/*  "select N'nchar literal'  LITERAL1   from dual"
	         ,"select q'!name LIKE '%DBMS_%%'!' LITERAL2   from dual "
	         ,"select q'<'So,' she said, 'It's finished.'>' LITERAL3   from dual "
	         ,"select q'{SELECT * FROM employees WHERE last_name = 'Smith';}' LITERAL4 from dual "
	         ,"select q'\"name like '['\"' LITERAL5   from dual "
	         //,"$IF select 'aaaaabbbbb' LITERAL6   from dual "
	         , "select nq'!ncha'r lit!' LITERAL7   from dual"
	         , "select nQ'{nchar lit}' LITERAL8   from dual"
	      		,"SELECT 'This string has a single quote ('')' LITERAL9 FROM dual"*/
				//ORA-01756: quoted string not properly terminated: , "select Nq'ï¿½ ï¿½1234 ï¿½' LITERAL10   from dual"
				//"select /*aaa; ",
				//"select 'aaa; ",
				//"select \"EMP",
				Service.readFile(NekotRexel.class, "test.sql"),
				Service.readFile(oracle.dbtools.parser.plsql.SqlEarley.class, "MSC_CL_PRE_PROCESS.pkgbdy"),
		};        


		long t1 = System.currentTimeMillis();
		for (int i = 0; i < inputs.length; i++) {
			testInput(inputs[i]);
		}        
		long t2 = System.currentTimeMillis();
		System.out.println("Lexer time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
		System.out.println("Nano time = "+incr); // (authorized) //$NON-NLS-1$
	}


}

