/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.Program;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.util.Service;

/**
 * A helper class. Parser boilerplate code, i.e.
 * 
        String input = "select emp from empno;";
        List<LexerToken> src =  LexerToken.parse(input);
        SqlEarley earley = SqlEarley.partialRecognizer();
        Matrix matrix = new Matrix(earley);
        earley.parse(src, matrix); 
        ParseNode root = earley.forest(src, matrix);
 * 
 * is wrapped into
 *
        Parsed parsed = new Parsed(
            "select emp from empno;",
            SqlEarley.getInstance(),
            "sql_statement"
        );
        ParseNode root = parsed.getRoot();
 * 
 * @author Dim
 *
 */
public class Parsed {
    private final String input;
    protected List<LexerToken> src;
    protected ParseNode root;
    protected Earley earley;
    private String[] rootPayload = null;
    private SyntaxError err = null;
    
    //public boolean debug = true;
    public boolean debug = false;
    
    public Parsed( String input, Earley parser, String legitimateSymbol ) {
        this.input = input;
        this.earley = parser;
        if( legitimateSymbol != null )
            this.rootPayload = new String[]{legitimateSymbol};
    }
    public Parsed( String input, List<LexerToken> src, Earley parser, String legitimateSymbol ) {
        this.input = input;
        this.earley = parser;
		this.src = src;
		if( legitimateSymbol != null )
		    this.rootPayload = new String[]{legitimateSymbol};
    }
    public Parsed( String input, List<LexerToken> src, Earley parser, String[] rootPayload ) {
        this.input = input;
        this.earley = parser;
        this.src = src;
        this.rootPayload = rootPayload;
    }
    public Parsed( String input, List<LexerToken> src, ParseNode root ) {
		this.input = input;
		this.src = src;
		this.root = root;
	}
	public String getInput() {
        return input;
    }
    public synchronized List<LexerToken> getSrc() {
        if( src == null )
            src =  LexerToken.parse(input);
        return src;
    }
    protected Matrix matrix = null;
    public Matrix getMatrix() {
        getRoot();
        return matrix;
    }    
    public synchronized ParseNode getRoot() {
    	getSrc();
        if( root == null ) {
            //LexerToken.print(src);
            matrix = new Matrix(earley);
            if( debug ) {
            	boolean isCmdline = false;
                for( StackTraceElement elem : Thread.currentThread().getStackTrace() ) {
                	if( elem.toString().startsWith("oracle.dbtools.cmdline") ) {
                		isCmdline = true;
                		break;
                	}
                }
                if( !isCmdline )
                	matrix.visual = new Visual(src, earley);
            }
            earley.parse(src, matrix); 
            if( debug && matrix.visual != null )
            	matrix.visual.draw();            
            
            if( rootPayload != null && rootPayload.length == 1 && Yelrae.PARSE_WITH_ERRORS.equals(rootPayload[0]) )
            	root = Yelrae.duplexParse(src,matrix);
            else
            	root = earley.forest(src, matrix, true);
            
            if( rootPayload != null && !Yelrae.PARSE_WITH_ERRORS.equals(rootPayload[0]) && root.topLevel != null )
            	err = SyntaxError.checkSyntax(input, rootPayload, src, earley, matrix);

        }
        return root;
    }
    
    public SyntaxError getSyntaxError() {
    	if( err != null )
    		return err;
        getRoot();
        return err;
    }

    
    /* TODO: fix this arbori example
      public static void main(String[] args) throws Exception {
     
 		final Parsed xmlGrammar = new Parsed(
        		Service.readFile(Parsed.class, "xml.grammar"), //$NON-NLS-1$
        		Grammar.bnfParser(),
        		"grammar" //$NON-NLS-1$
        );
        //xmlGrammar.getRoot().printTree();    
        Set<RuleTuple> rules = new TreeSet<RuleTuple>();
        Grammar.grammar(xmlGrammar.getRoot(), xmlGrammar.getSrc(), rules);

        String input = Service.readFile(Parsed.class, "test.html");
        Earley htmlparser = new Earley(rules) {
			@Override
			protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
				LexerToken token = src.get(y);
				return 
				symbol == identifier && token.type == Token.IDENTIFIER 
				||  symbol == identifier && token.type == Token.DQUOTED_STRING
				;
			}        			
		};
		final Parsed target = new Parsed(
        		input, 
        		htmlparser,
        		"nodes" //$NON-NLS-1$
        );
        //target.getRoot().printTree();   
        
 		final Parsed prog = new Parsed(
        		Service.readFile(Parsed.class, "htmltable.prg"), //$NON-NLS-1$
        		Program.getArboriParser(),
        		"program" //$NON-NLS-1$
        );
        if( false )
        	prog.getRoot().printTree();
        
        Program r = new Program(htmlparser) {
        	// define bind values here...
        };
        //r.debug = true;
        r.program(prog.getRoot(), prog.getSrc(), prog.getInput());

        target.getRoot(); // to get better idea of timing
        long t1 = System.currentTimeMillis();
		//Service.profile(500, 10, 5);
        Map<String,MaterializedPredicate> output = r.eval(target);
		for( String p : output.keySet() )
			System.out.println(p+"="+output.get(p).toString(p.length()+1));
        System.out.println("\n *********** eval time ="+(System.currentTimeMillis()-t1)+"\n");        

	}*/
}
