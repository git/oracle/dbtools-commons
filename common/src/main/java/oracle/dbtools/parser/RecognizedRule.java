/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser;

import java.util.List;
import java.util.Map;

import oracle.dbtools.parser.Parser.Tuple;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.doc.DocURL;
import oracle.dbtools.parser.plsql.doc.HarvestDoc;

/**
 * *Partially* recognized tuple  
 * @author Dim
 *
 */
public class RecognizedRule extends RuleTuple {
    public int pos; // Earley progressed till pos
    public RecognizedRule( String head, String[] rhs, int pos ) {
        super(head, (String[])null);
        this.pos = pos;
        int len = rhs.length;
        if( pos+2 <= len  ) {
            len = pos+2;
            this.rhs = new String[len];
            for( int i = 0; i < pos+1; i++ ) 
                this.rhs[i] = rhs[i];
            this.rhs[pos+1] = "...";
        } else
            this.rhs = rhs;
    }
    public RecognizedRule( RecognizedRule src, int at, int[] replacement ) {
        super(src.head, (String[])null);
        if( at < src.pos )
            pos = src.pos+replacement.length-1;
        else
            pos = src.pos;
        int len = src.rhs.length+replacement.length-1;
        if( pos+2 <= len  )
            len = pos+2;
        rhs = new String[len];
        for( int i = 0; i < at; i++) 
            rhs[i] = src.rhs[i];
        for( int i = 0; i < replacement.length && at+i < len ; i++ ) 
            rhs[at+i] = SqlEarley.getInstance().allSymbols[replacement[i]];
        for( int i = at+1; i < src.rhs.length && i+replacement.length-1 < len ; i++ )
            rhs[i+replacement.length-1] = src.rhs[i];
        if( len == pos+2 )
            rhs[len-1]="...";
        
    }
    public boolean isTruncated() {
        return pos==rhs.length-2 && "...".equals(rhs[rhs.length-1]);
    }
    /**
     * 'CREATE' 'TABLE' >!< identifier ... -- is presentable
     * 'CREATE' 'TABLE' >!< colmapped_query_name -- is not
     * @return
     */
    public boolean isPresentable() {
        for( int i = 0; i < rhs.length; i++ ) {
            if( "...".equals(rhs[i]) )
                continue;
            int code = SqlEarley.getInstance().symbolIndexes.get(rhs[i]);
            Map<Integer,DocURL> railroads = HarvestDoc.getRailroads();
            DocURL tdoc = railroads.get(code);
            if( tdoc == null && rhs[i].charAt(0) != '\'' && !"identifier".equals(rhs[i]) ) {
                Tuple[] tuples = SqlEarley.getInstance().rules;
                for( int j = 0; j < tuples.length; j++ ) {
                    Tuple candidate = tuples[j];
                    if( candidate.head == code )
                        return false;
                }
            }
        }
        return true;
    }
    /**
     * head: 'CREATE' 'TABLE' >!< identifier  <-- redundant
     * head: 'CREATE' 'TABLE' >!< identifier ...
     * @return
     */
    public boolean isRedundant( List<RecognizedRule> rules ) {
        boolean ret = false;
        for( RecognizedRule candidate : rules ) {
            if( !head.equals(candidate.head) )
                continue;
            if( pos != candidate.pos )
                continue;
            if( !candidate.isTruncated() )
                continue;
            if( rhs.length+1 != candidate.rhs.length )
                continue;
            ret = true;
            for( int i = 0; i < rhs.length; i++ ) {
                if( !rhs[i].equals(candidate.rhs[i]) ) {
                    ret = false;
                    break;
                }
            }
            if( ret == true )
                return ret;                
       }
       return ret;
    }
    
    public static void merge( List<RecognizedRule> rules, List<RecognizedRule> addendum ) {
        for( RecognizedRule candidate : addendum ) {
            boolean matches = false;
            for( RecognizedRule cmp : rules ) 
                if( cmp.equals(candidate) ) {
                    matches = true;
                    break;
                }
            if( !matches )
                rules.add(candidate);
        }
    }
    
    @Override
    public int compareTo( Object obj ) {
        int cmp = super.compareTo(obj);
        if( cmp != 0 )
            return cmp;
        return pos - ((RecognizedRule)obj).pos;
    }
    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        if( head!=null )
            b.append(head+":"); //$NON-NLS-1$
        int i = -1;
        for( String t: rhs ) {
            i++;
            if( i==pos )
                b.append(" >!<");
            b.append(" "+t); //$NON-NLS-1$
        }
        return b.toString();
    }

}
