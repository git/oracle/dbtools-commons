/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.util.Service;

/**
 * Grammar transformations, such as eliminating empty productions
 * @author Dim
 */
public class RuleTransforms {
	private static void log( Set<RuleTuple> delete, Set<RuleTuple> add ) {
	    boolean debug = true;
	    debug = false;
		if( debug ) {
			System.out.println("---:"); //$NON-NLS-1$
			RuleTuple.printRules(delete);
			System.out.println("+++:"); //$NON-NLS-1$
			RuleTuple.printRules(add);
			System.out.println(); //$NON-NLS-1$
		}
	}

    public static void removeRules( String head, Set<RuleTuple> src ) {
        Set<RuleTuple> tmp = new TreeSet<RuleTuple>();
        for( RuleTuple candidate: src )
            if( head.equals(candidate.head) )
                tmp.add(candidate);
        src.removeAll(tmp);
    }
   
    /*
    select_list:  "*"
        |  select_list[9,12)
        |  select_term
	select_list[10,12):  "," select_list
	select_list[9,12):  select_list select_list[10,12) <<<<<<<<<
     */
    public static void substituteSingleBinaryProductions( Set<RuleTuple> src ) {
    	while( true ) {
    		Set<RuleTuple> delete = new TreeSet<RuleTuple>();
    		Set<RuleTuple> add = new TreeSet<RuleTuple>();
    		for( RuleTuple c1: src ) {
                //if( exceptions.contains(c1.head) ) 
                	//continue;
                if( !c1.head.endsWith(")") && !c1.head.endsWith("#") ) { //$NON-NLS-1$ //$NON-NLS-2$
                    continue;
                }
                
    			if( c1.rhs.length != 2 )
    				continue;
    			boolean ok = true;
    			for( RuleTuple c2: src ) {
    				if( c1.head.equals(c2.head) && !c1.equals(c2) ) {
    					ok = false;
    					break;
    				}
    			}
    			/*for( RuleTuple c2: src ) { //not to create rules of size 3?
    				if( c2.rhs.length == 2 && (c2.rhs[0].equals(c1.head) || c2.rhs[1].equals(c1.head) ) ) {
    					ok = false;
    					break;
    				}
    			}*/  		
    			if( ok ) {
    				delete.add(c1);
    				System.out.println(">>> "+c1.toString());
    				for( RuleTuple c2: src ) {
                        String[] tmp = new String[c2.rhs.length*2];
                        int pos = 0;
    				    for( int i = 0; i < c2.rhs.length; i++ ) {
                            if( c2.rhs[i].equals(c1.head) ) {
                                tmp[pos++] = c1.rhs[0];
                                tmp[pos++] = c1.rhs[1];
                            } else {
                                tmp[pos++] = c2.rhs[i];
                            }
    				    }
    				    if( pos == c2.rhs.length )
    				        continue;
                        delete.add(c2);
    				    String[] rhs = new String[pos];
                        for( int i = 0; i < rhs.length; i++ ) 
                            rhs[i] = tmp[i];
                        add.add(new RuleTuple(c2.head,rhs));
    				}
    				break;
    			}
    		} 
    		if( delete.size() == 0 /*|| add.size() == 0*/ )
    			break;
    		log(delete, add);
    		src.removeAll(delete);
    		src.addAll(add);
    	}
    }
    /*
     * select_:     'SELECT' ;
     */
    public static void substituteSingleUnaryProductions( Set<RuleTuple> src ) {
    	if( src.size() == 1 )
    		return;
        while( true ) {
            Set<RuleTuple> delete = new TreeSet<RuleTuple>();
            Set<RuleTuple> add = new TreeSet<RuleTuple>();
            for( RuleTuple c1: src ) {
                //if( exceptions.contains(c1.head) ) 
                //continue;
                /*if( !c1.head.endsWith(")") ) { //$NON-NLS-1$ //$NON-NLS-2$
                    continue;
                }*/
                //if( "filefunc[5009,5117)[5050,5062)".equals(c1.head) ) 
                    //System.out.println("######################## "+c1.toString());
                
                
                if( c1.rhs.length != 1 )
                    continue;
                
                if( c1.rhs[0].equals(c1.head) ) {// where this rule came from?
                    delete.add(c1);
                	continue;
                }
                
                boolean ok = true;
                for( RuleTuple c2: src ) {
                    if( c1.head.equals(c2.head) && !c1.equals(c2) ) {
                        ok = false;
                        break;
                    }
                }
                if( ok ) {
                    delete.add(c1);
                    System.out.println(">>> "+c1.toString());
                    for( RuleTuple c2: src ) {
                        String[] rhs = new String[c2.rhs.length];
                        ok = false;
                        for( int i = 0; i < c2.rhs.length; i++ ) {
                            if( c2.rhs[i].equals(c1.head) ) {
                                rhs[i] = c1.rhs[0];
                                ok = true;
                            } else
                                rhs[i] = c2.rhs[i];
                        }
                        if( ok ) {
                            delete.add(c2);
                            add.add(new RuleTuple(c2.head,rhs));
                        }
                    }
                    break;
                }
            } 
            if( delete.size() == 0 /*|| add.size() == 0*/ )
                break;
            log(delete, add);
            src.removeAll(delete);
            src.addAll(add);
        }
    }
    
    public static Set<String> dubiousEmptyProductions = new HashSet<String>();
    /*
     * This procedure eliminates an empty symbol, but produces some others!
     * If called locally, it would eliminate empty productions used globally
     * Record those in dubiousEmptyProductions
     */
    public static void eliminateEmptyProductions( Set<RuleTuple> src ) {
    	while( true ) {
    		Set<RuleTuple> delete = new TreeSet<RuleTuple>();
    		Set<RuleTuple> add = new TreeSet<RuleTuple>();
    		String head = null;
    		for( RuleTuple c1: src ) {
    			if( head == null && c1.rhs.length == 0 ) {
    				head = c1.head;
    				delete.add(c1);
    				// covered by log(delete,add): System.out.println("--- "+c1.toString());
    				if( !head.contains("[") )
    					dubiousEmptyProductions.add(head);
    				break;
    			}   			
    		} 
    		if( head == null )
    			break;
    		boolean isSingular = true;
    		for( RuleTuple c1: src ) {
    			if( c1.rhs.length != 0 && head.equals(c1.head) ) {
    				isSingular = false;
    				break;
    			}    			
    		} 
			for( RuleTuple c2: src ) {
				int matched = 0;
				for( String rhs : c2.rhs )
					if( rhs.equals(head) ) {
						matched++;
					}
				if( matched == 0 )
					continue;
				if( isSingular )
					delete.add(c2);
				if( matched > 2 )
					throw new AssertionError("head = "+head+" matched "+c2.toString());
				if( matched == 1 ) {
					String[] tmp = new String[c2.rhs.length-1];
					for( int i = 0, j = 0; i < tmp.length; i++,j++ ) {
						if( c2.rhs[i].equals(head) )
							j++;
						tmp[i] = c2.rhs[j];
					}
					add.add(new RuleTuple(c2.head, tmp));
				} if( matched == 2 ) {
					String[] tmp11 = new String[c2.rhs.length-2];
					String[] tmp01 = new String[c2.rhs.length-1];
					String[] tmp10 = new String[c2.rhs.length-1];
					for( int i = 0, j = 0; i < tmp11.length; i++,j++ ) {
						if( c2.rhs[i].equals(head) )
							j++;
						tmp11[i] = c2.rhs[j];
					}
					for( int i = 0, j = 0; i < tmp10.length; i++,j++ ) {
						if( i==j && c2.rhs[i].equals(head) )
							j++;
						tmp10[i] = c2.rhs[j];
					}
					boolean matchedOnce = false;
					for( int i = 0, j = 0; i < tmp01.length; i++,j++ ) {
						if( !matchedOnce && c2.rhs[i].equals(head) ) 
							matchedOnce = true;
						else if( matchedOnce && c2.rhs[i].equals(head) )
							j++;
						tmp01[i] = c2.rhs[j];
					}
					add.add(new RuleTuple(c2.head, tmp11));
					add.add(new RuleTuple(c2.head, tmp01));
					add.add(new RuleTuple(c2.head, tmp10));
				}
			}

    		if( delete.size() == 0 && add.size() == 0 )
    			break;
    		log(delete, add);
    		src.addAll(add);
    		src.removeAll(delete);
    	}
    } 
    /**
     *  [sort of] fix for dubiousEmptyProductions
     */
    public static void injectMissingEmptyProductions( Set<RuleTuple> src ) { 
    	Set<String> heirs = new HashSet<String>();
    	for( String s : dubiousEmptyProductions ) {
    		System.out.println("injecting "+s);
    		Set<RuleTuple> add = new TreeSet<RuleTuple>();
			for( RuleTuple c2: src ) {
				int matched = 0;
				for( String rhs : c2.rhs )
					if( rhs.equals(s) ) {
						matched++;
					}
				if( matched == 0 )
					continue;
				if( matched > 1 )
					throw new AssertionError("head = "+s+" matched "+c2.toString());
				if( c2.rhs.length == 1 )
					heirs.add(c2.head);
				String[] tmp = new String[c2.rhs.length-1];
				for( int i = 0, j = 0; i < tmp.length; i++,j++ ) {
					if( c2.rhs[i].equals(s) )
						j++;
					tmp[i] = c2.rhs[j];
				}
				add.add(new RuleTuple(c2.head, tmp));
			}

			System.out.println("++**++:"); //$NON-NLS-1$
			RuleTuple.printRules(add);
			System.out.println(); //$NON-NLS-1$
    		src.addAll(add);
    	}
    	dubiousEmptyProductions = heirs;
    }
    
    public static void printRulesHierachy( String name, Set<RuleTuple> rules ) {
        System.out.println("-------------Rules Hierachy------------"); // (authorized)                 //$NON-NLS-1$
        int prefixEnd = name.indexOf('[');
        List<RuleTuple> subset = new LinkedList<RuleTuple>();
        for( RuleTuple rule : rules )
            if( rule.head.startsWith(prefixEnd > 0 ? name.substring(0,prefixEnd) : name) //>=0
                    //||  allSymbols[rule.rhs0].contains(name)
                    //||  rule.rhs1>0 && allSymbols[rule.rhs1].contains(name)
            )
                subset.add(rule);
        
        printRule(name, subset, 0);
        //System.out.println(rule.toString()); // (authorized)
        System.out.println("-------------------------------------"); // (authorized) //$NON-NLS-1$
    }

    private static void printRule( String name, List<RuleTuple> subset, int offset ) {
        for( RuleTuple rule : subset )
            if( rule.head.equals(name) ) {
                Service.identln(offset, rule.toString()); // (authorized)
                for( int i = 0; i < rule.rhs.length; i++ )
                    printRule(rule.rhs[i], subset, offset+1); 
            }
        
    }
    
    public static void print( Set<RuleTuple> rules ) {
        for( RuleTuple r: rules ) {
            System.out.println(r.toString()+";"); // (authorized)                                                        //$NON-NLS-1$
        }
    }
    public  static void printSelectedRules( String name, Set<RuleTuple> rules ) {
        System.out.println("-------------Rules---------------"); // (authorized)                 //$NON-NLS-1$
        for( RuleTuple rule : rules ) 
            if( rule.toString().contains(name))
                System.out.println(rule.toString()); // (authorized)
        System.out.println("-------------------------------------"); // (authorized) //$NON-NLS-1$
    }

	public static boolean isEmptyHead( String head, Set<RuleTuple> output ) {
if("cluster_index_clause".equals(head))
head = "cluster_index_clause";
		boolean ret = emptyClosure(output).contains(head);
		//System.out.println(head+" isEmptyHead="+ret);
		return ret;
	}
	private static Set<String> emptyClosure( Set<RuleTuple> output ) {
		Set<String> ret = new HashSet<String>();
		for( RuleTuple t : output ) {
			if( t.rhs.length==0 )
				ret.add(t.head);
		}
		while(true) {
			Set<String> increment = new HashSet<String>();
			for( RuleTuple t : output ) {
				boolean add = true;
				for( String rhs : t.rhs )
					if( !ret.contains(rhs) ) {
						add = false;
						break;
					}
				if(add)
					increment.add(t.head);
			}
			if( ret.containsAll(increment) )
				break;
			ret.addAll(increment);
		} 
		return ret;
	}
	
}
