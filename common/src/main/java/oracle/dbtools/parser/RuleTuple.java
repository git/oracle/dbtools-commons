/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.List;
import java.util.Set;

/**
 * class RuleTuple, example:
 * 
 * expr : expr '+' expr
 * expr : expr '*' expr
 * 
 * head rhs
 * ---- -----------------------
 * expr {"expr", "'+'", "expr"}   
 * expr {"expr", "'*'", "expr"}   
 */
public class RuleTuple implements Comparable, Serializable {
    private static final long serialVersionUID = 1L;
    public String head;
    public String[] rhs;
    
    public RuleTuple( String h, List<String> r ) {
        head = h;
        rhs = new String[r.size()];
        int i = 0;
        for(String t: r)
            rhs[i++]=t;
    }
    public RuleTuple( String h, String[] r ) {
        head = h;
        rhs = r;
    }
    public boolean equals(Object obj) {
        return compareTo(obj)==0;
    }
    public int hashCode() {
        throw new RuntimeException("hashCode inconsistent with equals");  //$NON-NLS-1$
    }
    @Override
    public int compareTo( Object obj ) {
        RuleTuple src = (RuleTuple)obj;
        int cmp = (head == null) ? 0 : head.compareTo(src.head);
        if( cmp!=0 )
            return cmp;
        for( int i=0; i<rhs.length && i<src.rhs.length; i++ ) 
            if( rhs[i].compareTo(src.rhs[i]) != 0 )
                return rhs[i].compareTo(src.rhs[i]);
        return rhs.length - src.rhs.length;
    }
    /**
     * Is keyword is inside brackets? Then ignore the rule.
     */
    public boolean ignore( String keyword, String bra, String ket ) {
        boolean enteredBrackets = false; 
        for(String token: rhs) {
            if( token.equals(bra) ) {
                enteredBrackets = true;
            }
            if( enteredBrackets ) {
                if(token.equals(keyword))
                    return true;
            }
            if( token.equals(ket) ) {
                return false;
            }
        }
        return false;
    }
    public String toString() {
        StringBuffer b = new StringBuffer();
        if( head!=null )
            b.append(head+":"); //$NON-NLS-1$
        for(String t: rhs) 
            b.append(" "+t); //$NON-NLS-1$
        return b.toString();
    }

    public static void printRules( Set<RuleTuple> rules ) {
        RuleTuple predecessor = null;
        for( RuleTuple rule : rules ) {
            System.out.println(rule.toHTML(predecessor)); 
            predecessor = rule;
        }
    }   
    public String toHTML( RuleTuple predecessor ) {
        StringBuffer b = new StringBuffer();
        if( predecessor == null || !predecessor.head.equals(head) )
            b.append(head+":"+identln(12-head.length()-2," "));
        else
            b.append(identln(10, "| "));
        for( String t: rhs )
            //if( t.startsWith("'") )
                //b.append(" \""+t.substring(1,t.length()-1)+"\"");
            //else
                b.append(" "+t);
        return b.toString();
    }
    public static String identln( int level, String txt ) {
        StringBuffer b = new StringBuffer();
        for(int i = 0; i< level;i++)
            b.append(" "); 
        b.append(txt); 
        return b.toString();
    }
    public static void memorizeRules( Set<RuleTuple> rules, String location ) throws Exception {
        FileOutputStream fos = new FileOutputStream(location);
        ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(rules);
        out.close();
    }
    public static Set<RuleTuple> getRules( URL u ) throws Exception {
        InputStream is = u.openStream();
        ObjectInputStream in = new ObjectInputStream(is);
        Set<RuleTuple> rules = (Set<RuleTuple>) in.readObject();
        in.close();
        return rules;
    }
    
    public static void main( String[] args ) {
		System.out.println("'A'".compareTo("zzzz"));
		System.out.println("'A'".compareTo("A"));
		System.out.println("a".compareTo("bb"));
		System.out.println("a".compareTo("A"));
	}
}
