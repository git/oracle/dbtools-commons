/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.util.Service;

/**
 * Earley parser walking backwards
 * @author Dim
 */
public class Yelrae extends Parser {
	
	public final static String PARSE_WITH_ERRORS = "parse with errors";
	
	public Earley employee;
	
	static private Visual visual = null; 
    
    /**
     * Example usage for toy grammar of arithmetic expressions
     * @param args
     * @throws Exception
     */
	public static void main( String[] args ) throws Exception {
        //--------------------------------------
        //UnitTest.parser = sqlYelraeInstance();
        //UnitTest.main(null);
        
		String input = 
	            //Service.readFile(Yelrae.class, "test.sql") //$NON-NLS-1$
	            Service.readFile(SqlEarley.class, "test.sql") //$NON-NLS-1$
	        ;
		List<LexerToken> src =  LexerToken.parse(input);
		visual = new Visual(src, sqlYelraeInstance()); // bogus (!= null)
        ParseNode root = sqlYelraeInstance().parse(src);
        root.printTree();
        
        //SqlEarley.main(new String[] {input});

	}

	private static void testOnSimpleGrammar() {
		String input = 
            //Service.readFile(Earley.class, "test.sql") //$NON-NLS-1$
                "1+2+3*4"
        ;
        List<LexerToken> src =  LexerToken.parse(input);
        System.out.println("src.size()="+src.size());
	
        Set<RuleTuple> wiki = new TreeSet<RuleTuple>();
        wiki.add(new RuleTuple("P",new String[]{"S"}));
        wiki.add(new RuleTuple("S",new String[]{"S","'+'","M"}));
        wiki.add(new RuleTuple("S",new String[]{"M"}));
        wiki.add(new RuleTuple("M",new String[]{"T","'*'","M"}));
        wiki.add(new RuleTuple("M",new String[]{"T"}));
        wiki.add(new RuleTuple("T",new String[]{"'('","P","')'"}));
        wiki.add(new RuleTuple("T",new String[]{"digits"}));
        wiki.add(new RuleTuple("T",new String[]{"identifier"}));
        wiki.add(new RuleTuple("T",new String[]{"string_literal"}));
        
        /*wiki.add(new RuleTuple("G",new String[]{"A","B","C"}));
        wiki.add(new RuleTuple("A",new String[]{"digits","digits",}));
        wiki.add(new RuleTuple("B",new String[]{"identifier"}));
        wiki.add(new RuleTuple("C",new String[]{"string_literal","string_literal","string_literal"}));*/

        
        Set<RuleTuple> rules = new TreeSet<RuleTuple>();
        /*final Parsed p = new Parsed(
                                             Service.readFile(Earley.class, "pereira.grammar"), //$NON-NLS-1$
                                             Grammar.bnfParser(),
                                             "grammar" //$NON-NLS-1$
                                     );
        //p.debug = true;
        Grammar.grammar(p.getRoot(), p.getSrc(), rules);
        */ 
        rules = wiki;
        Yelrae yel = new Yelrae(new Earley(rules)) {
            //            
        };
        //yel.isCaseSensitive = true;
        Matrix matrix = new Matrix(yel.employee);
        Visual visual = null;
        visual = new Visual(inverseSrc(src), yel.employee);

        long t1 = System.currentTimeMillis();
        ParseNode out = yel.parse(src, matrix);
        long t2 = System.currentTimeMillis();
        System.out.println("Earley parse time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
        System.out.println("#tokens="+src.size());
        out.printTree();
        
        if( visual != null )
        	visual.draw(matrix);
        else
        	System.out.println(matrix.toString());
	}

	private static Yelrae sqlInstance = null;
	public static Yelrae sqlYelraeInstance() {
		if( sqlInstance == null ) {
			SqlEarley earley = SqlEarley.partialRecognizer(new String[]{"sql_statements","subprg_body","expr",
					"fml_part","paren_expr_list","basic_decl_item_list"}); 
			sqlInstance = new Yelrae(earley);
        }
		return sqlInstance;
	}

    public synchronized ParseNode parse( List<LexerToken> src, Matrix matrix ) {
    	ArrayList<LexerToken> crs = inverseSrc(src);
    	if( visual != null ) { // if bogus object set in the Yelare.main()
    		visual = new Visual(crs,employee);
    		matrix.visual = visual;
    		visual.matrix = matrix;
    	}
		employee.parse(crs, matrix);
    	if( visual != null ) // if bogus object set in the Yelare.main()
    		visual.draw();
		ParseNode out = employee.forest(crs, matrix, true);
		invert(src.size(), out);
		return out;
    }
    
	private void invert( int length, ParseNode node ) {
		int morf = length - node.from;
		node.from = length - node.to;
		node.to = morf;
		if( node.lft != null )
			invert(length, node.lft);
		if( node.rgt != null )
			invert(length, node.rgt);
		if( node.rgt != null && node.lft != null ) {
			ParseNode tmp = node.rgt;
			node.rgt = node.lft;
			node.lft = tmp;
		}
		if( node.topLevel != null ) {
			TreeSet<ParseNode> topLevel = new TreeSet<ParseNode>();
			for( ParseNode c : node.topLevel ) {
				invert(length, c);
				topLevel.add(c);
			}
			node.topLevel = topLevel;
		}
	}

    private synchronized void recognise( List<LexerToken> src, Matriceable m ) {
    	ArrayList<LexerToken> crs = inverseSrc(src);
		employee.parse(crs, m);
    }

	public static ArrayList<LexerToken> inverseSrc(List<LexerToken> src) {
		ArrayList<LexerToken> crs = new ArrayList<>();
    	final int size = src.size();
		for( int i = 0; i < size; i++ ) {
			crs.add(src.get(size-i-1));
		}
		return crs;
	}
	
	public Yelrae( Earley template ) {
		employee = template;
		employee.rules = template.invertRules();
		allSymbols = template.allSymbols;
		symbolIndexes = template.symbolIndexes;
		rules = employee.rules;
	}

	@Override
	public ParseNode parse( List<LexerToken> src ) {
        Matrix matrix = new Matrix(employee);
        return parse(src, matrix);
	}
	
	public static ParseNode duplexParse( List<LexerToken> src, Matrix forward ) {
		ParseNode root = SqlEarley.getInstance().forest(src, forward, true);
		int endOdParse = 0;
		for( ParseNode child : root.children() )
			endOdParse = child.to;
		if( root.to == endOdParse  )
			return root;
//System.out.println("root=");
//root.printTree();
		ParseNode addendum = sqlYelraeInstance().parse(src);
//System.out.println("addendum=");
//addendum.printTree();
		if( addendum.topLevel != null )   //  can be null at incomplete input like this " ... ) SELECT "
			for( ParseNode child : addendum.topLevel )
				root.addTopLevel(child);
		return root;
	}

	@Override
	ParseNode treeForACell(List<LexerToken> src, Matrix m, Cell cell, int x, int y, Map<Long, ParseNode> explored) {
		throw new AssertionError("Abstract method");
	}

	@Override
	void parse(List<LexerToken> src, Matriceable matrix) {
		throw new AssertionError("Abstract method");
	}

	@Override
	void toHtml(int ruleNo, int pos, boolean selected, int x, int mid, int y, Matriceable matrix, StringBuffer sb) {
		throw new AssertionError("Abstract method");
	}


}




