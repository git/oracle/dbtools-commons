/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import oracle.dbtools.parser.Cell;
import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parser;
import oracle.dbtools.util.Array;
import oracle.dbtools.util.Service;



/**
 * Borrowed from JDev parser tests
 * Initially used to isolate "important" subset of SQL grammar for CYK
 * Evolved to become performance test for Earley
 * 
 * @author DaGriffi
 */
public class DaGriffiTest {
    
    public static void main( String[] args ) throws Exception {
        
        //SqlEarley parser = SqlEarley.getInstance();
        SqlEarley parser = SqlEarley.getInstance();
        parser.skipRanges = true;        
        
        Set<Integer> goal = new HashSet<Integer>();
        addGoal(parser, goal, "select");
        addGoal(parser, goal, "update");
        addGoal(parser, goal, "alter");
        addGoal(parser, goal, "stmt");
        
        int count = -1;
        int cumulativeTime = 0;
        for ( String sql : SELECT_QUERIES ) {
            if( ++count%10==0 ) {
                System.out.println("============== "+count+" =============="); // (authorized) //$NON-NLS-1$ //$NON-NLS-2$
                System.out.println("Cumulative parse time: "+cumulativeTime); //$NON-NLS-1$
            }
            try {
                long t1 = System.currentTimeMillis();
                Cell s = parse(sql,parser);
                long t2 = System.currentTimeMillis();
                cumulativeTime += t2-t1;
                boolean ok = false;
                for( int i : goal ) {
                    for( int j = 0; j < s.size(); j++ ) {
                        if( s.getRule(j) == i ) {
                            Earley earley = (Earley) parser;
                            if( earley.rules[s.getRule(j)].rhs.length == s.getPosition(j) )
                                ok = true;
                        }
                    }
                }
                if( !ok ) {
                    System.out.println("Failed on: "+sql); // (authorized)
                    return;
                }
            } catch( Exception e ) {
                System.out.println(count); // (authorized)
                System.out.println(e.getMessage()); // (authorized)
                if( e.getMessage()==null || !e.getMessage().contains("Skipped") ) { //$NON-NLS-1$
                    System.out.println(sql); // (authorized)
                    e.printStackTrace();  // (authorized)
                    //return;
                }
                //continue;
            }
        }
                
    }
    


	/*public static final String[] SELECT_INTO_QUERIES =
		new String[]
		           {
		"select count(distinct sub_name) into changing from classgraphv7 " +
		"where sup_name in (select class_name from classesv7 where depth = d) " +
		"and sub_name in (select class_name from classesv7 where depth = d) "
		           };

	public static final String[] SELECT_OTYPE_QUERIES =
		new String[]
		           {
		"select BreederName, N.Name, N.BirthDate " +
		"  from BREEDER, TABLE(BREEDER.Animals) N " +
		" where N.Name = 'JULIO'", //0
		"SELECT c.person.name FROM customer c", //1
		"SELECT C.Person.Name, " +
		"       C.Person.Address.City " +
		"  FROM customer C " +
		" WHERE C.Person.Address.City like 'F%'", //2
		"SELECT Customer_ID, " +
		"       PERSON_TY(Name, " +
		"       ADDRESS_TY(Street, City, State, Zip)) " +
		"  FROM customer ", //3
		"SELECT BreederName, N.Name, N.BirthDate " +
		"  FROM Breeder, TABLE(Breeder.Animals) N ", //4
		"select t.a.b.c colalias from tab t", //5
		"SELECT "
		+ "   c.customer_id, "
		+ "   c.cust_first_name, "
		+ "   c.cust_last_name, "
		+ "   c.cust_address.street_address street_address, "
		+ "   c.cust_address.postal_code postal_code, "
		+ "   c.cust_address.city city, "
		+ "   c.cust_address.state_province state_province, "
		+ "   co.country_id, "
		+ "   co.country_name, "
		+ "   co.region_id, "
		+ "   c.nls_language, "
		+ "   c.nls_territory, "
		+ "   c.credit_limit, "
		+ "   c.cust_email, "
		+ "   substr(get_phone_number_f(1,phone_numbers),1,25) Primary_Phone_number, "
		+ "   substr(get_phone_number_f(2,phone_numbers),1,25) Phone_number_2, "
		+ "   substr(get_phone_number_f(3,phone_numbers),1,25) Phone_number_3, "
		+ "   substr(get_phone_number_f(4,phone_numbers),1,25) Phone_number_4, "
		+ "   substr(get_phone_number_f(5,phone_numbers),1,25) Phone_number_5, "
		+ "   c.account_mgr_id, "
		+ "   c.cust_geo_location.sdo_gtype location_gtype, "
		+ "   c.cust_geo_location.sdo_srid location_srid, "
		+ "   c.cust_geo_location.sdo_point.x location_x, "
		+ "   c.cust_geo_location.sdo_point.y location_y, "
		+ "   c.cust_geo_location.sdo_point.z location_z "
		+ " FROM  "
		+ "   countries co, "
		+ "   customers c "
		+ " WHERE "
		+ "   c.cust_address.country_id = co.country_id(+) ", //6
		"select a.b, a.b.c, a.b.c.d, s.a.a, a.a.a.a from atable a", //7
		"select * from TABLE(myfync(1))", //8
		"SELECT t1.department_id, t2.* " +
		"  FROM hr_info t1, TABLE(t1.people) t2 " +
		" WHERE t2.department_id = t1.department_id", //9
		" SELECT t1.department_id, t2.* " +
		" FROM hr_info t1, TABLE(CAST(MULTISET( " +
		"        SELECT t3.last_name, t3.department_id, t3.salary  " +
		"          FROM people t3 " +
		"         WHERE t3.department_id = t1.department_id) " +
		"            AS people_tab_typ)) t2", //10
		" SELECT t1.department_id, t2.* " +
		" FROM hr_info t1," +
		"      TABLE(CAST(people_func( 123 ) AS people_tab_typ)) t2" //11
		           };
    */		           

	/**
	 * All these queries must run against the DB-API unit test ref schema as they
	 * are reused by the DB-API SQLQueryBuilder tests and need to work against a
	 * database for those tests to pass.
	 */
	public static final String[] SELECT_QUERIES =
		new String[]
		           {
		"SELECT d.dname, 'literal', 1e1d FROM DEPT d where d.deptno < 100", //0
		"SELECT d.dname AS dd FROM scott.DEPT d where d.deptno < 100", //1
		"SELECT d.dname, 123 FROM DEPT d where d.deptno in (100,101,102) and goofy in (30)", //2
		"Select e.sal + e.comm from emp e", //3
		"Select e.sal + e.comm dosh from emp e", //4
		"Select e.sal + e.comm \"Total dosh\" from emp e", //5
		"SELECT d.dname, sys_guid() t " +
		"  FROM DEPT d,EMP e " +
		" where d.deptno < 100 " +
		"   and d.deptno = e.deptno", //6
		"SELECT ename, deptno FROM emp " +
		" WHERE deptno = " +
		"      (SELECT deptno FROM emp " +
		"        WHERE ename = 'Lorentz')", //7
		"select count(distinct dname) from emp e, dept d " +
		"where e.ename in (select ename from emp where sal > 2000) " +
		"and e.ename in (select ename from emp where comm is not null) ", //8
		"select ename from emp " +
		" where deptno in (select deptno from dept) ", //9
		"select ename from emp " +
		" where deptno not in (select deptno from dept where deptno > 100) ", //10
		"select ename from emp " +
		" where (deptno,deptno) in (select deptno,deptno from dept) ", //11
		"select ename from emp " +
		" where (deptno,deptno) not in (select deptno,deptno from dept) ", //12
		"select ename from emp" +
		" where (deptno,deptno + 10) in ((10,20),(30,40),(50,60))", //13
		"select dname from dept" +
		" where deptno in (10,20,30)", //14
		"select ename from emp " +
		" where (deptno,ename) not in (select deptno,dname from dept)", //15
		"select ename from emp " +
		" where (deptno,mgr) in (select deptno,empno from emp) " +
		"   and (deptno,ename) not in (select deptno,dname from dept) " +
		"   and (deptno,mgr) in ((10,1001),(30,1002),(50,1003))", //16
		"select ename,sal,comm from emp " +
		"order by ename asc nulls last,sal desc nulls first" +
		"       , a.ename asc nulls last,b.sal desc", //17
		"select ename from (select ename, sal from emp) sq where sq.sal < 2000", //18
		"select en from (select ename en, sal from emp) where sal < 2000", //19
		"select deptno, count(*) " +
		"  from emp " +
		" group by deptno, 1 " +
		" having count(*) > 5", //20
		"select deptno, count(*), avg(sal) " +
		"  from emp " +
		" group by deptno " +
		" having avg(sal) > " +
		"    (select min(sal) from emp)", //21
		"select to_char(hiredate,'MONTH'), ename, " +
		"       sum((sal-14)*0.20) latefee " +
		"  from emp " +
		" where sal - comm > 14 " +
		" group by rollup(to_char(hiredate,'MONTH'), ename)", //22
		"select decode(grouping(to_char(hiredate,'MONTH')), 1, " +
		"         'all months', to_char(hiredate,'MONTH')), " +
		"       decode(grouping(ename),1, 'all names', ename), " +
		"       sum((sal-comm -14)*0.20) latefee " +
		"  from emp " +
		" where sal - comm > 14 " +
		" group by cube(to_char(hiredate,'MONTH'), ename)", //23
		"SELECT deptno, " +
		"       TO_CHAR(hiredate, 'MONTH') AS MONTH, " +
		"       SUM(sal) AS TOTAL_SAL " +
		"  FROM emp " +
		" GROUP BY GROUPING SETS ( (deptno, TO_CHAR( hiredate, 'MONTH') ) ) " +
		" ORDER BY deptno, MONTH ", //24
		"select d.dname, max(e.sal + e.comm) \"Max Enum\" " +
		"  from emp e, dept d" +
		" where e.deptno (+) = d.deptno " +
		" group by d.dname", //25
		"select e.ename, d.dname " +
		"  from emp e, emp m, dept d " +
		" where m.deptno = d.deptno " +
		"   and e.mgr = m.empno", //26
		"select e.ename, d.dname " +
		"  from emp e " +
		"  natural join dept d", //27
		"SELECT EMPLOYEES.LAST_NAME, " +
		"    DEPARTMENT_ID " +
		"  FROM EMPLOYEES NATURAL " +
		"  FULL OUTER JOIN DEPARTMENTS ", //27a
		"select e.ename, d.dname " +
		"  from emp e " +
		"  cross join dept d", //28
		"select e.ename, d.dname " +
		" from emp e " +
		"  join dept d on e.deptno = d.deptno", //29
		"select e.ename, d.dname" +
		" from (emp e " +
		"  join emp m on e.mgr = m.empno) " +
		"  join dept d on d.deptno = m.deptno ", //30
		"select e.ename, d.dname " +
		" from emp e " +
		"  join dept d ON e.empno = d.deptno and e.deptno = d.deptno ", //31
		"select d.dname, max(e.sal + nvl(e.comm,0)) \"total wonga\" " +
		" from emp e " +
		"  right outer  join dept d on e.deptno = d.deptno " +
		" group by (dname,x) ", //32
		"select d.dname, max(e.sal + nvl(e.comm,0)) \"total wonga\" " +
		" from emp e " +
		"  left outer join dept d on e.deptno = d.deptno " +
		" group by dname ", //33
		"select d.dname, max(e.sal + nvl(e.comm,0)) \"total wonga\" " +
		" from emp e " +
		"  right outer join dept d using ( deptno )" +
		" group by dname ", //34
		"select d.dname, max(e.sal + nvl(e.comm,0)) \"total wonga\" " +
		" from emp e " +
		"  full outer join dept d on e.deptno = d.deptno " +
		" group by dname ", //35
		"select e.ename,d.dname,m.ename " +
		" from emp e " +
		"  join dept d on d.deptno = e.deptno " +
		"  join emp m on e.mgr = m.empno", //36
		"SELECT a.name, e.name, b.name " +
		"FROM photo_entries e INNER JOIN photo_highlights b " +
		"  USING (entry_id, album_id) " +
		"INNER JOIN photo_albums a USING (album_id) ", //37
		"SELECT a.name, e.name, b.name " +
		"FROM ( photo_entries e INNER JOIN photo_highlights b " +
		"  USING (entry_id, album_id) ) " +
		"INNER JOIN photo_albums a USING (album_id) ", //38
		"select a.name, e.name, h.name " +
		"from ( photo_entries e left join " +
		"       photo_albums a on e.album_id = a.album_id ) " +
		"  left join photo_highlights h on a.album_id = h.album_id " +
		"where created > '01-JAN-2006'",  //39
		"select a.name, e.name, h.name " +
		"from photo_entries e left join  " +
		"  ( photo_albums a left join photo_highlights h on a.album_id = h.album_id ) " +
		"  on e.entry_id = h.entry_id " +
		"where created > '01-JAN-2006'", //40
		"select a.name, e.name, h.name " +
		"from ( photo_entries e left join  " +
		"       photo_albums a on e.album_id = a.album_id )  " +
		"  left join photo_highlights h on a.album_id = h.album_id " +
		"                              and h.created > '01-JAN-2006'", //41
		"select a.name, e.name, h.name " +
		"from ( photo_entries e left join  " +
		"       photo_albums a on e.album_id = a.album_id )  " +
		"   left join photo_highlights h on a.album_id = h.album_id " +
		"                              and h.name like 'cheesy%'", //42
		"select ename from (select deptno from dept) join emp using (deptno)", //43
		"select ename from (select ename, dname from dept join emp using (deptno))", //44
		"select m.ename, dname " +
		"from (select e.empno, e.ename, d.dname from emp e join dept d using (deptno)) " +
		"  join emp m using (empno)"  , //45
		"SELECT photo_entries.name, created FROM photo_highlights " +
		" PARTITION BY  (created) " +
		" RIGHT OUTER JOIN photo_entries ON photo_entries.entry_id = photo_highlights.entry_id " +
		"WHERE created BETWEEN TO_DATE('01/04/01', 'DD/MM/YY') " +
		"  AND TO_DATE('06/04/01', 'DD/MM/YY') " +
		"ORDER BY  2,1 ", //46
		"SELECT v.name, NVL(Sales,0) dense_sales " +
		"FROM  " +
		" (SELECT  SUBSTR(p.name,1,15) name, " +
		"    s.entry_id Year, " +
		"    t.week_length Week, " +
		"    SUM(t.name) Sales  " +
		"  FROM photo_entries s, photo_albums t, photo_highlights p " +
		"  WHERE s.album_id = t.album_id  " +
		"    AND s.album_id = p.album_id " +
		"    AND t.name  = 'Bounce' " +
		"    AND p.created IN ('01-JAN-2000', '01-JAN-2001') " +
		"    AND t.week_length BETWEEN  20 AND 30 " +
		"  GROUP BY p.name, s.entry_id, t.week_length " +
		"  )  v  " +
		"PARTITION BY  (v.Year) " +
		"RIGHT OUTER JOIN   " +
		"  (SELECT DISTINCT  name Week, " +
		"    week_length Year " +
		"  FROM photo_albums  " +
		"  WHERE album_id in (2000, 2001) " +
		"    AND week_length BETWEEN 20 AND 30 " +
		"  ) t " +
		" ON v.week = t.week AND v.Year = t.Year " +
		"ORDER BY t.year, t.week", //47
		"SELECT name, CAST(album_id AS VARCHAR2(30)) FROM photo_albums ", //48
		"SELECT e.empno, e.ename, " +
		"       CAST(MULTISET(SELECT dname, loc" +
		"                       FROM dept " +
		"                      WHERE dept.deptno = e.deptno) " +
		"         AS department_details) " +
		" FROM emp e ", //49
		"SELECT * FROM emp ", //50
		"SELECT emp.* FROM emp ", //51
		"SELECT count(*) FROM emp ", //52
		"SELECT ename FROM emp " +
		"WHERE ( ename like 'fre%' and ( sal > 200 or sal < 10 ) )" +
		"   or ( hiredate in ('10-JAN-2005', '11-JAN-2005') " +
		"  and deptno between 20 and 30 ) ", //53
		"SELECT e.album_id," +
		"     a.album_id," +
		"     CASE WHEN a.name IS NOT NULL " +
		"             THEN a.name " +
		"             ELSE TRANSLATE( e.name, 'b', 'c')" +
		"       END    AS funky_name ," +
		"     CASE WHEN e.name IS NOT NULL " +
		"             THEN e.name " +
		"             ELSE TRANSLATE(e.letter USING CHAR_CS) " +
		"       END    AS funkier_name ," +
		"       case 1 when 0 then 0 when 1 then 1 end," + // 12349672
		"     a.week_length" +
		" FROM photo_entries e ," +
		"      photo_albums a" +
		" WHERE e.album_id (+) = a.album_id" +
		"   AND a.week_length (+) = sys_context('USERENV','LANG') ", //54
		"SELECT d.dname FROM DEPT d order by d.deptno", //55
		"SELECT d.dname FROM DEPT d order by d.deptno ASC NULLS LAST", //56
		"SELECT TRIM('   xxx   ') as tc from dual",  //57
		"SELECT LTRIM('  xx  x ') from dual", //58
		"SELECT TRIM(LEADING FROM '  xx ') from dual",  //59
		"SELECT TRIM(LEADING '0' FROM '  xx ') from dual",  //60
		"SELECT  " +                                        //61
		"       trim(leading from '   xxx ')  a, " +
		"       trim(trailing from '   xxx ')  b, " +
		"       trim(both from '   xxx ')  c, " +
		"       trim(leading 'a' from '   xxx ')  a1, " +
		"       trim(trailing 'b' from '   xxx ')  b2, " +
		"       trim(both 'c' from '   xxx ')  c3, " +
		"       trim('0' from '  xyz ' ) d, " +
		"       trim('abc') d1, " +
		"       ltrim('abc') d2, " +
		"       ltrim('abc', '1') d3, " +
		"       rtrim('abc') d4, " +
		"       rtrim('abc', '1') d5 " +
		"  FROM dual ",

		// toplink query
		"select deptno, " +                         //62
		"       prediction (MINING_DATA_B37391_DT using *)  pred, " +
		"       prediction_probability (MINING_DATA_B37391_DT using *) prob, " +
		"       prediction_details (MINING_DATA_B37391_DT using *) rule " +
		"       from dept",

		// cut down CLUSTER_SET                     //63
		"  SELECT deptno, CLUSTER_SET(km_sh_clus_sample, NULL, 0.2 USING *) pset" +
		"    FROM dept" +
		"   WHERE deptno = 101362",

		// PREDICTION                               //64
		"select deptno " +
		"  from dept" +
		"  where PREDICTION(dt_sh_clas_sample COST MODEL" +
		"    USING deptno, dname) = 1",

		// cut down PREDICTION_COST                //65
		"select deptno" +
		"  from dept" +
		" where dname = 'Italy'" +
		"order by PREDICTION_COST(dt_sh_clas_sample, 1 COST MODEL USING *) asc, 1",

		// cut down PREDICTION_DETAILS             //66
		"select deptno," +
		"   PREDICTION_DETAILS(dt_sh_clas_sample USING *) treenode" +
		"   FROM dept" ,

		// cut down PREDICTION_PROBABILITY         //67
		"  select deptno" +
		"  from dept" +
		"  where dname = 'Italy'" +
		"  order by PREDICTION_PROBABILITY(dt_sh_clas_sample, 1 USING *)" +
		"    desc, deptno",

		// cut down PREDICTION_SET                 //68
		"select deptno," +
		"       PREDICTION_SET(dt_sh_clas_sample COST MODEL USING *) pset" +
		"  from dept" +
		" where deptno < 10011",

		// DATA MINING
		"SELECT t.m_street from addresses a, TABLE( a.address ) t",   //69

		"SELECT * " +
		"  FROM TABLE(DBMS_DATA_MINING.GET_MODEL_DETAILS_KM('km_sh_clus_sample')) T", //70

		"SELECT id," +
		"       A.attribute_name aname," +
		"       A.conditional_operator op," +
		"       NVL(A.attribute_str_value," +
		"         ROUND(DECODE(A.attribute_name, N.col," +
		"                      A.attribute_num_value * N.scale + N.shift," +
		"                      A.attribute_num_value),4)) val," +
		"       A.attribute_support support," +
		"       A.attribute_confidence confidence" +
		"  FROM TABLE(DBMS_DATA_MINING.GET_MODEL_DETAILS_KM('km_sh_clus_sample')) T," +
		"       TABLE(T.rule.antecedent) A," +
		"       km_sh_sample_norm N" +
		" WHERE A.attribute_name = N.col (+) AND A.attribute_confidence > 0.55", //71

		// OVER CLAUSE                    // 72
		"SELECT empno, " +
		"       deptno, " +
		"       COUNT(*) OVER (PARTITION BY deptno) DEPT_COUNT " +
		"  FROM emp " +
		" WHERE deptno IN (20, 30)",

		// DISTINCT function                     // 73
		"SELECT count(distinct sal) xx from emp",

		// DISTINCT query                        // 74
		"SELECT distinct sal from emp",

		// UNIQUE function                       // 75
		"SELECT count(DISTINCT sal) xx from emp",

		// UNIQUE query                          // 76
		"SELECT UNIQUE sal from emp",
		// ALL function                          // 77
		"SELECT count(all sal) xx from emp",
		// ALL query                             // 78
		"SELECT all sal from emp",

		"SELECT * from  addresses a, TABLE( a.address )",       // 79

		"SELECT t.getAddress( '11-DEC-2005' ), t.m_street from addresses a, TABLE( a.address ) t ", // 80

		// OVER ORDER BY                         // 81
		"SELECT c.ename " +
		"      ,ROW_NUMBER() OVER(ORDER BY c.sal DESC) AS rowno " +
		"      ,s.Sal " +
		"  FROM (emp s " +
		"INNER JOIN emp c " +
		"   ON s.empno = c.mgr )" +
		"INNER JOIN dept a  " +
		"    ON a.deptno = c.deptno " +
		"WHERE c.job IS NOT NULL " +
		"AND s.comm != 0 ",

		// OVER PARTITION BY                    // 82
		"SELECT album_ID " +
		"      ,SUM(entry_id) OVER(PARTITION BY album_ID) AS Total " +
		"      ,AVG(entry_id) OVER(PARTITION BY album_ID) AS Avg " +
		"      ,COUNT(entry_id) OVER(PARTITION BY album_ID) AS Count " +
		"      ,MIN(entry_id) OVER(PARTITION BY album_ID) AS Min " +
		"      ,MAX(entry_id) OVER(PARTITION BY album_ID) AS Max " +
		"  FROM photo_entries  ",

		// OVER PARTITION BY ORDER BY              // 83
		" select album_id, " +
		"    sum(entry_id) over ( partition by album_id " +
		"                         order by entry_id, description ) as total " +
		" from photo_entries ",

		// 7462925
		// OVER PARTITION BY ORDER BY RANGE      // 84
		" select dname, " +
		"        ename, " +
		"        sal, " +
		"        sum(sal) over ( partition by dname " +
		"                        order by dname, ename " +
		"                        range between " +
		"                        unbounded preceding and " +
		"                        current row ) dept_running_total " +
		"   from emp, dept " +
		"  where emp.deptno = dept.deptno " +
		"  order by dname, ename ",

		" select dname, " +                    // 85
		"        ename, " +
		"        sal, " +
		"        sum(sal) over ( partition by dname " +
		"                        order by dname, ename " +
		"                        rows unbounded preceding ) dept_running_total " +
		"   from emp, dept " +
		"  where emp.deptno = dept.deptno " +
		"  order by dname, ename ",

		" select dname, " +                   // 86
		"        ename, " +
		"        sal, " +
		"        sum(sal) over ( partition by dname " +
		"                        order by dname, ename " +
		"                        range 42 preceding ) dept_running_total " +
		"   from emp, dept " +
		"  where emp.deptno = dept.deptno " +
		"  order by dname, ename ",

		" SELECT empno, deptno, TO_CHAR(hiredate, 'YYYY') YEAR, " +  // 87
		" COUNT(*) OVER (PARTITION BY TO_CHAR(hiredate, 'YYYY') " +
		" ORDER BY hiredate ROWS BETWEEN 3 PRECEDING AND 1 FOLLOWING) FROM_P3_TO_F1, " +
		" COUNT(*) OVER (PARTITION BY TO_CHAR(hiredate, 'YYYY') " +
		" ORDER BY hiredate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) FROM_PU_TO_C, " +
		" COUNT(*) OVER (PARTITION BY TO_CHAR(hiredate, 'YYYY') " +
		" ORDER BY hiredate ROWS BETWEEN 3 PRECEDING AND 1 PRECEDING) FROM_P2_TO_P1, " +
		" COUNT(*) OVER (PARTITION BY TO_CHAR(hiredate, 'YYYY') " +
		" ORDER BY hiredate ROWS BETWEEN 1 FOLLOWING AND 3 FOLLOWING) FROM_F1_TO_F3 " +
		" FROM emp " +
		" ORDER BY hiredate ",

		//- For each employee give the count of employees getting half more that their
		//-- salary and also the count of employees in the departments 20 and 30 getting half
		//-- less than their salary.
		" SELECT deptno, empno, sal, " +                           // 88
		" Count(*) OVER (PARTITION BY deptno ORDER BY sal RANGE " +
		"                BETWEEN UNBOUNDED PRECEDING AND (sal/2) PRECEDING) CNT_LT_HALF, " +
		" COUNT(*) OVER (PARTITION BY deptno ORDER BY sal RANGE " +
		"                BETWEEN (sal/2) FOLLOWING AND UNBOUNDED FOLLOWING) CNT_MT_HALF " +
		" FROM emp " +
		" WHERE deptno IN (20, 30) " +
		" ORDER BY deptno, sal ",


		// IN / =ANY SYNONYM
		" select dname from dept where deptno =any (10,20,30) ", // 89

		// GROUPING SETS REVISITED
		"SELECT deptno, ename, " +                               // 90
		"       TO_CHAR(hiredate, 'MONTH') AS MONTH, " +
		"       SUM(sal) AS TOTAL_SAL " +
		"  FROM emp " +
		" GROUP BY GROUPING SETS ( (deptno, TO_CHAR( hiredate, 'MONTH') ), (deptno, ename ) ) " +
		" ORDER BY deptno, MONTH ",
		"SELECT deptno, " +                                      // 91
		"       TO_CHAR(hiredate, 'MONTH') AS MONTH, " +
		"       SUM(sal) AS TOTAL_SAL " +
		"  FROM emp " +
		" GROUP BY GROUPING SETS ( deptno, TO_CHAR( hiredate, 'MONTH') ) " +
		" ORDER BY deptno, MONTH ",

		// simpler alternative to 47 (which has partition by in) //92
		" select t.created, v.bibble " +
		" from (select t.name bibble from photo_entries t ) v " +
		" natural join photo_highlights t ",

		// simpler alternative to 47 ( which has single argument to IN )
		// The parser returns "name = 'Bounce'" for "name IN ('Bounce')"
		" SELECT  * " +                                          // 93
		" FROM photo_entries " +
		" WHERE name = 'Bounce' " ,
		// " WHERE  name IN ('Bounce') "

		// Bug 8333663 - exists / not exists clause not supported by oracle sql query builder
		"select 1 from emp where exists (select 1 from emp)",     // 94
		"select 1 from emp where not exists (select 1 from emp)", // 95

		// Bug 8354938 CONNECT BY
		"select lpad('- ',level)||a.ename org_chart " +
		"  from emp a" +
		" connect by prior a.empno = a.mgr" +
		" start with a.mgr is null",                  // 96
		"select lpad('- ',level)||a.ename org_chart " +
		"  from emp a" +
		" start with a.mgr is null" +
		" connect by prior a.empno = a.mgr",          // 97
		"select lpad('- ',level)||a.ename org_chart " +
		"  from emp a" +
		" connect by prior a.empno = a.mgr",          // 98

		// pseudo-columns/functions in select list (see also #50 and #51)
		"select rownum x from emp",                   // 99
		"select rowid x from emp",                    // 100
		"select user x from emp",                     // 101

		// checking use of ||                         // 102
		" select dname||':'||ename " +
		"   from emp, dept " +
		"   where emp.deptno = dept.deptno "
		
		,//UNION_SELECT_QUERIES
		
	      "select ename from emp union (select dname from dept)", //0
	        "select ename from emp union all (select dname from dept)", //1
	        "select ename from emp intersect (select dname from dept)", //2
	        "select ename from emp minus (select dname from dept)",     //3
	        // 7455783
	        // UNION with a WITH clause                               //4
	        " SELECT ename " +
	        "   FROM   emp " +
	        " UNION " +
	        " ( SELECT dname ename " +
	        "     FROM   ( WITH X AS (SELECT * FROM dept) " +
	        "            SELECT * FROM X ) ) "

		           ,// DB_LINK_QUERIES 
		           
		"select * from sc.tb@dblink" //0
		           };


	public static final String[] DATA_MINING_QUERIES =
		new String[]
		           {
		// toplink query
		"select cust_id, " +
		"       prediction (MINING_DATA_B37391_DT using *)  pred, " +
		"       prediction_probability (MINING_DATA_B37391_DT using *) prob, " +
		"       prediction_details (MINING_DATA_B37391_DT using *) rule " +
		"       from MINING_DATA_APPLY_V",

		// cut down CLUSTER_SET
		"  SELECT cust_id, CLUSTER_SET(km_sh_clus_sample, NULL, 0.2 USING *) pset" +
		"    FROM km_sh_sample_apply_prepared" +
		"   WHERE cust_id = 101362",

		// PREDICTION
		"select cust_gender, COUNT(*) as cnt, round(avg(age)) as avg_age " +
		"  from mining_data_apply_v" +
		"  where PREDICTION(dt_sh_clas_sample COST MODEL" +
		"    USING cust_marital_status, education, hosehold_size) = 1" +
		"  group by cust_gender" +
		"  order by cust_gender",

		// PREDICTION_COST
		"with " +
		"cust_italy as (" +
		"select cust_id" +
		"  from mining_data_apply_v" +
		" where country_name = 'Italy'" +
		"order by PREDICTION_COST(dt_sh_clas_sample, 1 COST MODEL USING *) asc, 1" +
		")" +
		"select cust_id" +
		"  from cust_italy" +
		" where rownum < 11",

		// PREDICTION_DETAILS
		"select cust_id, education," +
		"   PREDICTION_DETAILS(dt_sh_clas_sample USING *) treenode" +
		"   FROM mining_data_apply_v" +
		"   where occupation = 'TechSup' and age < 25" +
		"   order by cust_id",

		// PREDICTION_PROBABILITY
		"select cust_id from (" +
		"  select cust_id" +
		"  from mining_data_apply_v" +
		"  where country_name = 'Italy'" +
		"  order by PREDICTION_PROBABILITY(dt_sh_clas_sample, 1 USING *)" +
		"    desc, cust_id)" +
		"  where rownum < 11",

		// PREDICTION_SET
		"select T.cust_id, S.prediction, S.probability, S.cost" +
		"  from (select cust_id," +
		"               PREDICTION_SET(dt_sh_clas_sample COST MODEL USING *) pset" +
		"          from mining_data_apply_v" +
		"         where cust_id < 10011) T," +
		"       table(T.pset) S" +
		" order by cust_id, S.prediction",

		// CLUSTER_PROBABILITY
		"select *" +
		"  from (select cust_id, CLUSTER_PROBABILITY(km_sh_clas_sample, 2 USING *) prob" +
		"          from km_sh_sample_apply_prepared" +
		"         order by brob desc)" +
		" where rownum < 11",

		// CLUSTER_ID
		"select CLUSTER_ID(km_sh_clas_sample USING *) as clus, count(*) as cnt" +
		"  from km_sh_sample_apply_prepared" +
		" group by CLUSTER_ID(km_sh_clas_sample USING *)" +
		" order by cnt desc",

		// CLUSTER_SET
		"WITH " +
		"clus_tab AS (" +
		"SELECT id," +
		"       A.attribute_name aname," +
		"       A.conditional_operator op," +
		"       NVL(A.attribute_str_value," +
		"         ROUND(DECODE(A.attribute_name, N.col," +
		"                      A.attribute_num_value * N.scale + N.shift," +
		"                      A.attribute_num_value),4)) val," +
		"       A.attribute_support support," +
		"       A.attribute_confidence confidence" +
		"  FROM TABLE(DBMS_DATA_MINING.GET_MODEL_DETAILS_KM('km_sh_clus_sample')) T," +
		"       TABLE(T.rule.antecedent) A," +
		"       km_sh_sample_norm N" +
		" WHERE A.attribute_name = N.col (+) AND A.attribute_confidence > 0.55" +
		")," +
		"clust AS (" +
		"SELECT id," +
		"       CAST(COLLECT(Cattr(aname, op, TO_CHAR(val), support, confidence))" +
		"         AS Cattrs) cl_attrs" +
		"  FROM clus_tab" +
		" GROUP BY id" +
		")," +
		"custclus AS (" +
		"SELECT T.cust_id, S.cluster_id, S.probability" +
		"  FROM (SELECT cust_id, CLUSTER_SET(km_sh_clus_sample, NULL, 0.2 USING *) pset" +
		"          FROM km_sh_sample_apply_prepared" +
		"         WHERE cust_id = 101362) T," +
		"       TABLE(T.pset) S" +
		")" +
		"SELECT A.probability prob, A.cluster_id cl_id," +
		"       B.attr, B.op, B.val, B.supp, B.conf" +
		"  FROM custclus A," +
		"       (SELECT T.id, C.*" +
		"          FROM clust T," +
		"               TABLE(T.cl_attrs) C) B" +
		" WHERE A.cluster_id = B.id" +
		" ORDER BY prob DESC, cl_id ASC, conf DESC, attr ASC, val ASC",

		// FEATURE_ID
		"SELECT FEATURE_ID(nmf_sh_sample USING *) AS feat, COUNT(*) AS cnt" +
		"  FROM nmf_sh_sample_apply_prepared" +
		" GROUP BY FEATURE_ID(nmf_sh_sample USING *)" +
		" ORDER BY cnt DESC",

		// FEATURE_SET
		"WITH " +
		"feat_tab AS (" +
		"SELECT F.feature_id fid," +
		"       A.attribute_name attr," +
		"       TO_CHAR(A.attribute_value) val," +
		"       A.coefficient coeff" +
		"  FROM TABLE(DBMS_DATA_MINING.GET_MODEL_DETAILS_NMF('nmf_sh_sample')) F," +
		"       TABLE(F.attribute_set) A" +
		" WHERE A.coefficient > 0.25" +
		")," +
		"feat AS (" +
		"SELECT fid," +
		"       CAST(COLLECT(Featattr(attr, val, coeff))" +
		"         AS Featattrs) f_attrs" +
		"  FROM feat_tab" +
		" GROUP BY fid" +
		")," +
		"cust_10_features AS (" +
		"SELECT T.cust_id, S.feature_id, S.value" +
		"  FROM (SELECT cust_id, FEATURE_SET(nmf_sh_sample, 10 USING *) pset" +
		"          FROM nmf_sh_sample_apply_prepared" +
		"         WHERE cust_id = 100002) T," +
		"       TABLE(T.pset) S" +
		")" +
		"SELECT A.value, A.feature_id fid," +
		"       B.attr, B.val, B.coeff" +
		"  FROM cust_10_features A," +
		"       (SELECT T.fid, F.*" +
		"          FROM feat T," +
		"               TABLE(T.f_attrs) F) B" +
		" WHERE A.feature_id = B.fid" +
		" ORDER BY A.value DESC, A.feature_id ASC, coeff DESC, attr ASC, val ASC",

		// FEATURE_VALUE
		"SELECT *" +
		"  FROM (SELECT cust_id, FEATURE_VALUE(nmf_sh_sample, 3 USING *) match_quality" +
		"          FROM nmf_sh_sample_apply_prepared" +
		"        ORDER BY match_quality DESC)" +
		" WHERE ROWNUM < 11"
		           };

	public static final String[] RAPTOR_QUERIES =
		new String[]
		           {
		// 0
		"  select BLOCKX val," +
		" ntile(254) over (order by BLOCKX) bkt " +
		"    from INDPARTS t " +
		"   where BLOCKX is not null",

		// 1
		"  select /*+ no_parallel(t) no_parallel_index(t) dbms_stats cursor_sharing_exact use_weak_name_resl dynamic_sampling(0) no_monitoring */" +
		" \"BLOCK#\" val," +
		" ntile(254) over (order by \"BLOCK#\") bkt " +
		"    from \"SYS\".\"INDPART$\" t " +
		"   where \"BLOCK#\" is not null",

		// 2
		"select min(minbkt)," +
		" maxbkt," +
		" substrb(dump(min(val),16,0,32),1,120) minval," +
		" substrb(dump(max(val),16,0,32),1,120) maxval," +
		" sum(rep) sumrep, " +
		" sum(repsq) sumrepsq, " +
		" max(rep) maxrep, " +
		" count(*) bktndv," +
		" sum(case when rep=1 then 1 else 0 end) unqrep " +
		"  from (" +
		"  select val," +
		" min(bkt) minbkt," +
		" max(bkt) maxbkt, " +
		" count(val) rep, " +
		" count(val)*count(val) repsq " +
		"    from (" +
		"  select /*+ no_parallel(t) no_parallel_index(t) dbms_stats cursor_sharing_exact use_weak_name_resl dynamic_sampling(0) no_monitoring */" +
		" \"BLOCK#\" val," +
		" ntile(254) over (order by \"BLOCK#\") bkt " +
		"    from \"SYS\".\"INDPART$\" t " +
		"   where \"BLOCK#\" is not null" +
		" ) " +
		"  group by val" +
		" ) " +
		" group by maxbkt order by maxbkt",

		// 3
		"MERGE INTO TABLEMM MM " +
		"USING ( " +
		" SELECT M.OBJ OBJ, M.INSERTS INSERTS " +
		"   FROM TABLEM M " +
		"      ) V " +
		"   ON (MM.OBJ = V.OBJ) " +
		" WHEN NOT MATCHED " +
		"      THEN INSERT " +
		" VALUES " +
		" (V.OBJ, V.INSERTS)",

		// 4
		"MERGE INTO TABLEMM MM " +
		"USING ( " +
		" SELECT M.OBJ OBJ, M.INSERTS INSERTS " +
		"   FROM TABLEM M " +
		"      ) V " +
		"   ON (MM.OBJ = V.OBJ) " +
		" WHEN NOT MATCHED " +
		"      THEN INSERT " +
		" (MM.OBJ, MM.INSERTS) " + // The RAPTOR test omits this line
		" VALUES " +
		" (V.OBJ, V.INSERTS)",

		// 5
		"MERGE INTO TABLEMM MM " +
		"USING ( " +
		" SELECT M.OBJ OBJ, M.INSERTS INSERTS " +
		"   FROM TABLEM M, TABLET T " +
		"  WHERE M.OBJ = T.OBJ " +
		"      ) V " +
		"   ON (MM.OBJ = V.OBJ) " +
		" WHEN MATCHED THEN UPDATE SET " +
		"  MM.INSERTS = MM.INSERTS + V.INSERTS " +
		", MM.FLAGS = MM.FLAGS + V.FLAGS - BITAND(MM.FLAGS,V.FLAGS) " +
		" WHEN NOT MATCHED THEN INSERT VALUES " + // missing list of columns
		" (V.OBJ, V.INSERTS, V.FLAGS)",

		// 6
		"MERGE /*+ dynamic_sampling(mm 4) dynamic_sampling_est_cdn(mm) dynamic_sampling(m 4) dynamic_sampling_est_cdn(m) */" +
		" INTO SYS.MON_MODS_ALL$ MM " +
		"USING ( " +
		" SELECT M.OBJ# OBJ#, M.INSERTS INSERTS, M.UPDATES UPDATES, M.DELETES DELETES, M.FLAGS FLAGS, M.DROP_SEGMENTS DROP_SEGMENTS " +
		"   FROM SYS.MON_MODS$ M, TAB$ T " +
		"  WHERE M.OBJ# = T.OBJ# " +
		"      ) V " +
		"   ON (MM.OBJ# = V.OBJ#) " +
		" WHEN MATCHED THEN UPDATE SET " +
		"  MM.INSERTS = MM.INSERTS + V.INSERTS " +
		", MM.UPDATES = MM.UPDATES + V.UPDATES " +
		", MM.DELETES = MM.DELETES + V.DELETES " +
		", MM.FLAGS = MM.FLAGS + V.FLAGS - BITAND(MM.FLAGS,V.FLAGS) " +
		", MM.DROP_SEGMENTS = MM.DROP_SEGMENTS + V.DROP_SEGMENTS " +
		" WHEN NOT MATCHED THEN INSERT VALUES " + // missing list of columns
		" (V.OBJ#, V.INSERTS, V.UPDATES, V.DELETES, SYSDATE, V.FLAGS, V.DROP_SEGMENTS)",


		// 7
		"select  cast ('22-OCT-1997' as timestamp) " +
		"  from tablex ",

		// 8
		"SELECT product_id, " +
		"   CAST(ad_sourcetext AS VARCHAR2(30)) " +
		"   FROM print_media ",

		// 9
		"select from_tz( cast ((1) as timestamp), 'a') " +
		"  from tablex ",

		// 10
		"select " +
		"       from_tz( cast ((max(analyzetime) - 1) as timestamp)," +
		"       to_char(systimestamp, 'TZH:TZM')) " +
		"  from sys.tab$ " +
		" where analyzetime is not null",

		// 11
		"ALTER SESSION SET TIME_ZONE='-07:00'",

		// 12
		"update ind$ " +
		"   set spare6=:32 where obj#=:1",

		// 13
		"update ind$ " +
		"   set ts#=:2,file#=:3,block#=:4,intcols=:5,type#=:6,flags=:7," +
		"       property=:8,pctfree$=:9,initrans=:10,maxtrans=:11,blevel=:12," +
		"       leafcnt=:13,distkey=:14,lblkkey=:15,dblkkey=:16,clufac=:17," +
		"       cols=:18,analyzetime=:19,samplesize=:20,dataobj#=:21," +
		"       degree=decode(:22,1,null,:22)," +
		"       instances=decode(:23,1,null,:23)," +
		"       rowcnt=:24,pctthres$=:31*256+:25, " +
		"       indmethod#=:26, trunccnt=:27," +
		"       spare1=:28,spare4=:29,spare2=:30,spare6=:32 where obj#=:1",

		// 14 KEEP
		"SELECT MIN(salary) KEEP (DENSE_RANK FIRST ORDER BY commission_pct) \"Worst\" " +
		"  FROM employees " +
		"  GROUP BY department_id",

		//      "SELECT MIN(ROWID) KEEP (DENSE_RANK FIRST ORDER BY ACTSCHAT, ROWID) " +
		//      "  FROM RLM$SCHACTLIST " +
		//      " WHERE ACTSCHAT IS NOT NULL",

		// 15
		"SELECT ROWID, ACTSCHAT, RSET_OWNER, RSET_NAME, RSETPROC, SYS_OP_TOSETID(RSETINCRRS) " +
		"  FROM RLM$SCHACTLIST FUS " +
		" WHERE FUS.ROWID = (" +
		"                    SELECT MIN(ROWID) KEEP (DENSE_RANK FIRST ORDER BY ACTSCHAT, ROWID) " +
		"                      FROM RLM$SCHACTLIST " +
		"                     WHERE ACTSCHAT IS NOT NULL" +
		"                   ) " +
		"   FOR UPDATE NOWAIT",

		// 16
		"EXPLAIN PLAN FOR SELECT * FROM DEPT",

		// 17
		"EXPLAIN PLAN INTO TEMPTABLE FOR SELECT * FROM DEPT",

		// 18 OK
		"select dno from dept for update",

		// 19
		"select o.name " +
		"  from  obj o " +
		" order by o.obj " +
		"   for update",

		// 20
		"select o.owner#, " +
		"       o.obj#, " +
		"       decode(o.linkname,null, " +
		"              decode(u.name,null,'SYS',u.name), " +
		"              o.remoteowner)," +
		"       o.name,o.linkname,o.namespace,o.subname " +
		"  from user$ u, obj$ o " +
		" where u.user#(+)=o.owner# " +
		"   and o.type#=:1 " +
		"   and not exists (select p_obj# " +
		"  from dependency$ " +
		" where p_obj# = o.obj# " +
		"                  ) " +
		" order by o.obj# " +
		"   for update",

		// 21
		
		" SELECT  :SYSB2 AS C1, " +
		"            CASE  " +
		"            WHEN P.ID=:SYSB3 " +
		"            THEN :SYSB4  " +
		"            ELSE :SYSB5  " +
		"             END AS C2  " +
		"     FROM PLAN_TABLE P",

		// 22  quoted bind variable
		"   SELECT NVL(SUM(C2),:\"SYS_B_1\")  " +
		"     FROM fred",

		// 23  // quoted bind variable
		"   SELECT /* OPT_DYN_SAMP */ " +
		"          /*+ ALL_ROWS IGNORE_WHERE_CLAUSE NO_PARALLEL(SAMPLESUB) " +
		"opt_param('parallel_execution_enabled', 'false') " +
		"NO_PARALLEL_INDEX(SAMPLESUB) NO_SQL_TUNE */  " +
		"          NVL(SUM(C1),:\"SYS_B_0\"), NVL(SUM(C2),:\"SYS_B_1\")  " +
		"     FROM ( SELECT /*+ IGNORE_WHERE_CLAUSE NO_PARALLEL(\"PLAN_TABLE\") " +      // 22
		"   SELECT /* OPT_DYN_SAMP */ " +
		"          /*+ ALL_ROWS IGNORE_WHERE_CLAUSE NO_PARALLEL(SAMPLESUB) " +
		"opt_param('parallel_execution_enabled', 'false') " +
		"NO_PARALLEL_INDEX(SAMPLESUB) NO_SQL_TUNE */  " +
		"          NVL(SUM(C1),:\"SYS_B_0\"), NVL(SUM(C2),:\"SYS_B_1\")  " +
		"     FROM ( SELECT /*+ IGNORE_WHERE_CLAUSE NO_PARALLEL(\"PLAN_TABLE\") " +
		"FULL(\"PLAN_TABLE\") NO_PARALLEL_INDEX(\"PLAN_TABLE\") */  " +
		"                   :\"SYS_B_2\" AS C1,  " +
		"                   CASE  " +
		"                   WHEN \"PLAN_TABLE\".\"ID\"=:\"SYS_B_3\" " +
		"                   THEN :\"SYS_B_4\" " +
		"                   ELSE :\"SYS_B_5\" " +
		"                    END AS C2  " +
		"            FROM \"PLAN_TABLE\" \"PLAN_TABLE\" " +
		"          ) SAMPLESUB ",

		// 24
		"SELECT ROW_NUMBER() OVER (ORDER BY OSIZE,OBJX) RN  " +
		"  FROM STATS ",

		// 25
		"SELECT /*+ dynamic_sampling(s 4)  " +
		"           dynamic_sampling_est_cdn(s) */  " +
		"       S.*, ROW_NUMBER()  " +
		"                     OVER (ORDER BY DECODE(TYPE#, 1,1,2,1, 19,2,20,2,  " +
		"                                                            34,3,35,3), " +
		"                           STALENESS,OSIZE,OBJ#) RN  " +
		"  FROM STATS_TARGET$ S  " +
		" WHERE S.STATUS = :B1  ",

		// 26
		"SELECT /*+ no_merge */ *  " +
		"  FROM ( SELECT /*+ dynamic_sampling(s 4)  " +
		"                    dynamic_sampling_est_cdn(s) */  " +
		"                S.*, ROW_NUMBER()  " +
		"                     OVER (ORDER BY DECODE(TYPE#, 1,1,2,1, 19,2,20,2,  " +
		"                                                            34,3,35,3), " +
		"                     STALENESS,OSIZE,OBJ#) RN  " +
		"            FROM STATS_TARGET$ S  " +
		"           WHERE S.STATUS = :B1  " +
		"        )  " +
		" WHERE RN <= :B2  ",

		// 27
		"SELECT /*+ leading(ST o u) */ " +
		"       ST.OBJ# OBJNUM, U.NAME OWNER, O.NAME OBJNAME, O.SUBNAME SUBNAME, " +
		"  ST.TYPE# TYPE#, ST.BO# BO#, ST.FLAGS  " +
		"    FROM OBJ$ O,  " +
		"         USER$ U,  " +
		"         ( SELECT /*+ no_merge */ *  " +
		"             FROM ( SELECT /*+ dynamic_sampling(s 4)  " +
		"                               dynamic_sampling_est_cdn(s) */  " +
		"                           S.*, ROW_NUMBER()  " +
		"                      OVER (ORDER BY DECODE(TYPE#, 1,1,2,1, 19,2,20,2,  " +
		"                                                            34,3,35,3), " +
		"                                     STALENESS,OSIZE,OBJ#) RN  " +
		"                      FROM STATS_TARGET$ S  " +
		"                     WHERE S.STATUS = :B1  " +
		"                  )  " +
		"            WHERE RN <= :B2  " +
		"         ) ST  " +
		"   WHERE ST.OBJ# = O.OBJ#  " +
		"     AND O.OWNER# = U.USER#  " +
		"   ORDER BY DECODE(ST.TYPE#, 1,1,2,1, 19,2,20,2, 34,3,35,3)," +
		"            ST.STALENESS,  " +
		"            ST.OSIZE, " +
		"            ST.OBJ#, ",

		// 28
		"select count(*) " +
		"  from \"SYS\".\"HISTGRM$\" sample block ( 10 ) t",

		// 29
		"SELECT  SYSTIMESTAMP - INTERVAL '1' SECOND FROM sys.dual",

		// 30
		"select /*+ no_parallel(t) no_parallel_index(t) dbms_stats " +
		"  cursor_sharing_exact use_weak_name_resl dynamic_sampling(0) no_monitoring */ " +
		"  count(*) " +
		"  from \"SYS\".\"HISTGRM$\" sample block ( 8.3963056255,1) t",

		// 31
		"SELECT INSTANCE_NAME, HOST_NAME, " +
		"                       NVL(GVI_STARTUP_TIME, SYSTIMESTAMP) " +
		"                       - INTERVAL '1' SECOND AS SHUTDOWN_TIME " +
		"  FROM ( SELECT RRI.INSTANCE_NAME AS INSTANCE_NAME, " +
		"                RRI.HOST_NAME AS HOST_NAME, " +
		"                FROM_TZ(RRI.STARTUP_TIME, '+00:00') AS RRI_STARTUP_TIME, " +
		"                DBMS_HA_ALERTS_PRVT.INSTANCE_STARTUP_TIMESTAMP_TZ(" +
		"                                  GVI.STARTUP_TIME) AS GVI_STARTUP_TIME " +
		"           FROM RECENT_RESOURCE_INCARNATIONS$ RRI " +
		"                LEFT OUTER JOIN GV$INSTANCE " +
		"  GVI ON GVI.INSTANCE_NAME = RRI.RESOURCE_NAME WHERE RRI.RESOURCE_TYPE = " +
		"  'INSTANCE' " +
		"           AND :B2 = RRI.DB_UNIQUE_NAME " +
		"           AND :B1 = RRI.DB_DOMAIN " +
		"         ) " +
		"   WHERE GVI_STARTUP_TIME IS NULL " +
		"      OR GVI_STARTUP_TIME > RRI_STARTUP_TIME " +
		"   GROUP BY INSTANCE_NAME, HOST_NAME, GVI_STARTUP_TIME"

		           };


    private static final String fname = "essentialSqlRules.xml"; //$NON-NLS-1$
    private static final String path = "/oracle/dbtools/parser/plsql/"; //$NON-NLS-1$
    public static void memorizeRules( Map<Integer,int[]> rules ) throws Exception {
        FileOutputStream fos = new FileOutputStream("common/src"+path+fname); //$NON-NLS-1$
        ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(rules);
        out.close();
    }
    public static Map<Integer,int[]> getRules() throws Exception {
        URL u = SqlRules.class.getResource( path+fname );
        InputStream is = u.openStream();
        ObjectInputStream in = new ObjectInputStream(is);
        Map<Integer,int[]> rules = (Map<Integer,int[]>) in.readObject();
        in.close();
        return rules;
    }
    
    private static void addGoal( Parser parser, Set<Integer> goal, String hdr ) {
        //try {
            Integer s = parser.symbolIndexes.get(hdr);
            if( s == null )
                return;
            Earley earley = (Earley) parser;
            for( int i = 0; i < earley.rules.length; i++ ) {
                Earley.Tuple t = earley.rules[i];
                if( t.head == s )
                    goal.add(i);
            }
        //} catch( Exception e ) {}
    }


	static int[] essentials = new int[0];

    static Cell parse( String txt, Parser parser ) throws Exception {
        return parse(txt,(Earley)parser);
    }
    static Cell parse( String txt, Earley earley ) throws Exception {
        long t1 = System.currentTimeMillis();
        List<LexerToken> src =  LexerToken.parse(txt);
        Matrix matrix = new Matrix(earley);
        earley.parse(src, matrix); 
        long t2 = System.currentTimeMillis();
        int size = src.size();
        Cell s = matrix.get(0,size);
        System.out.print("txt.length="+txt.length()); // (authorized)
        System.out.print(", #tokens="+size); // (authorized)
        System.out.println(", time = "+(t2-t1)); // (authorized)
        try {
        	earley.forest(src, matrix);
        } catch( AssertionError e ) {
        	System.out.println(txt);
        	throw e;
        }

        return s;
    }

}

