/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;

/**
 * 
 * @author Dim
 *
 */
public class DbObjectsDDL {
    static final String path = "/oracle/dbtools/parser/plsql/"; //$NON-NLS-1$
    
    private static Connection c = null;
    private static String query = null;
    private static ResultSet rs1 = null;
    private static PreparedStatement stmt2 = null;
    static {
        try {
            Class.forName("oracle.jdbc.OracleDriver");   //$NON-NLS-1$
            // split query into two because get_ddl was failing due to wrong edition in unidentified object
            c = DriverManager.getConnection("jdbc:oracle:thin:@yourmachine.yourdomain:1521/orcl12c","system", "dbtools"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            query = "select object_type,object_name,owner from all_objects \n"+ 
                    " where OWNER like 'SYS%' and object_type not like '%JAVA%' /*and object_type != 'VIEW' and rownum < 1000*/";
            PreparedStatement stmt1 = c.prepareStatement(query);
            rs1 = stmt1.executeQuery();
            stmt2 = c.prepareStatement("select DBMS_METADATA.GET_DDL(:type,:name,:owner) from dual");
        } catch( Exception e ) {
            throw new RuntimeException("failed to connect to database"); //$NON-NLS-1$
        }
    }

    private static Map<String, Integer> objectTypeCounts = new HashMap<String, Integer>();
    public static DbObject next() throws Exception {
        DbObject ret = null;
        ResultSet rs2 = null;
        if( rs1.next() ) {
            String type = rs1.getString(1);
            String name = rs1.getString(2);
            String owner = rs1.getString(3);
            System.out.println(owner+"."+name+" ("+type+")");
            stmt2.setString(1, type);
            stmt2.setString(2, name);
            stmt2.setString(3, owner);
            Integer cnt = objectTypeCounts.get(type);           
            if( cnt == null ) 
                cnt = 0;
            if( 10 < cnt )
                return new DbObject("",""); // skip
            try {
                rs2 = stmt2.executeQuery();
                rs2.next();
                ret = new DbObject(type,rs2.getString(1));
            } catch( SQLException e ) {
                System.err.println(owner+"."+name+" ("+type+")");
                System.err.println(e.getMessage());
                return new DbObject("",""); // skip
            } finally {
                if( rs2 != null )
                    rs2.close();
            }
        } 
        return ret;
    }
    
    public static void main( String[] args ) throws Exception {
        StringBuilder test = new StringBuilder();
        final SqlEarley earley = SqlEarley.getInstance();
        int testNo = 0;

        DbObject obj = null;

        while( (obj = next()) != null ) {
            String code = obj.definition;
            if( code.length() == 0 )
                continue;
            code = code.trim();
            if( !code.endsWith(";") )
                code = code+";";
            if( code.contains("***UNSUPPORTED DATA TYPE") )
                continue;
            if( code.contains("abcd") && code.contains("wrapped") )
                continue;
            
            boolean error = false;
                
            List<LexerToken> src =  LexerToken.parse(code);
            Matrix matrix = new Matrix(earley);
            earley.parse(src, matrix); 

            if( code.contains("CREATE OR REPLACE NONEDITIONABLE FUNCTION") && src.size() < 12 )
                continue;

            ParseNode root = null;
            try {
                root = earley.forest(src, matrix);
            } catch( AssertionError e ) {
                System.err.println("AssertionError"+e.getMessage()); // (authorized)
                System.err.println(code); // (authorized)
                error = true;
            }
            if( root == null || root.to<2 ) {
                System.err.println(code); // (authorized)
                error = true;
            }

            int from = 0;
            int to = src.size();
            if( root!=null && root.topLevel != null ) {
                for( ParseNode n : root.children() ) {
                    if( n.from == 0 ) {
                        root = n;
                        to = root.to;
                    }
                }
                if( root.topLevel != null ) {
                    System.err.println(code); // (authorized)
                    error = true;
                }
            }
            
            if( error ) {
                test.append("++");
                test.append(testNo++);
                test.append(";\n");
                test.append('`');
                test.append(code);
                test.append("`->\n"); 
                test.append("[0,0) dummy /*failed*/\n;\n\n");
                Integer cnt = objectTypeCounts.get(obj.type);           
                if( cnt == null ) 
                    cnt = 0;
                objectTypeCounts.put(obj.type,cnt+1);
            }

            
        }       
        System.out.println("------------------------------"); // (authorized)
        
        FileWriter fw = new FileWriter("common/src"+path+"dbobjects_sql.test");
        fw.write(test.toString());
        fw.close();           
    }
    
    static class DbObject {
        String type;
        String definition;        
        public DbObject(String type, String definition) {
            this.type = type;
            this.definition = definition;
        }
    }
}
