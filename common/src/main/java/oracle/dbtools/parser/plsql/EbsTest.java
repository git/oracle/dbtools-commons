/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.parser.Cell;

/**
 * Regression test on Oracle APPS database
 * @author Dim
 */
public class EbsTest {

	static boolean isSQL = false;

	/*private static Connection c = null;
	private static String query = null;
	private static ResultSet rs = null;
	static {
		try {
			Class.forName("oracle.jdbc.OracleDriver");	 //$NON-NLS-1$
			if( isSQL ) {
				c = DriverManager.getConnection("jdbc:oracle:thin:@yourmachine.yourdomain:1526:VIS","sqlrepo", "sqlrepo"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				query = "select sql_text||';' from statements\n" + //$NON-NLS-1$
				"where sql_id between 0 and 20000\n" + //$NON-NLS-1$
				"and length(sql_text) between 1000 and 5000\n" + //$NON-NLS-1$
				"and sql_text not like '%:symbolic_arguments:i_symargs%'\n" + //$NON-NLS-1$
				"and sql_text not like '%for update of order_number%'\n" + //$NON-NLS-1$
				"and sql_text not like '%and service_line_id = :B2 for update of unit_effectivity_id%'\n" + //$NON-NLS-1$
				"order by sql_id";  //$NON-NLS-1$
			} else {
				String url = "jdbc:oracle:thin:@yourmachine.yourdomain:1552:seed120";
				url = "jdbc:oracle:thin:@yourmachine.yourdomain:10300:VISSM01";
				c = DriverManager.getConnection(url,"apps", "apps"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				query = 
					"select text,line from dba_source\n" + //$NON-NLS-1$
					"where owner = 'APPS'\n" + //$NON-NLS-1$
					"and type = 'PACKAGE BODY'\n" + //$NON-NLS-1$
					//"and name like 'AMS%'" + //$NON-NLS-1$
					//"and name like 'FND%'" + //$NON-NLS-1$
					//"and name ='QP_PREQ_GRP'" +
					"and name ='AAAA'" +
					"order by name,line"  //$NON-NLS-1$
					;
			}
			PreparedStatement stmt = c.prepareStatement(query);
			rs = stmt.executeQuery();
			rs.next();
		} catch( Exception e ) {
			throw new RuntimeException("failed to connect to database"); //$NON-NLS-1$
		}
	}*/
	
	public static String getSource( String name ) throws SQLException {
		StringBuffer ret = new StringBuffer("create ");
		String url = "jdbc:oracle:thin:@yourmachine.yourdomain:10300:VISSM01";
		//url = "jdbc:oracle:thin:@yourmachine.yourdomain:1559:seed115";
		Connection conn = DriverManager.getConnection(url,"apps"/*_read_only"*/, "apps"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		String query = 
				"select text,line from dba_source\n" + //$NON-NLS-1$
						"where owner = 'APPS'\n" + //$NON-NLS-1$
						"and type = 'PACKAGE BODY'\n" + //$NON-NLS-1$
						//"and name like 'AMS%'" + //$NON-NLS-1$
						//"and name like 'FND%'" + //$NON-NLS-1$
						//"and name ='QP_PREQ_GRP'" +
						"and name = :1" +
						"order by name,line"  //$NON-NLS-1$
						;

		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1,name);
		ResultSet rs = stmt.executeQuery();
		while( rs.next() ) {
			int line = rs.getInt("line"); //$NON-NLS-1$
			String text = rs.getString("text");
			byte[] bytes = rs.getBytes("text");
			ret.append(new String(bytes)); //$NON-NLS-1$
			//ret.append("\n");
		}
		return ret.toString();
	}



	public static void main(String[] args) throws Exception {
		String input = getSource("QP_PREQ_GRP");
		//String input = getSource("ABM_MAIN");
		System.out.println(input.length());
		SqlEarley.parse(input);
	}


}
