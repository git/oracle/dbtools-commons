/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Set;

import oracle.dbtools.parser.RuleTuple;

/**
 * Output grammar to various applications (for .Net team, JS parser, etc).
 * @author Dim
 *
 */
public class ExportGrammar {
    private static Set<RuleTuple> origRules;
    static {
    	origRules = SqlEarley.getInstance().origRules;
    }
    
    public static void main( String[] args ) throws Exception {
        
        boolean isJS = true;
        if( isJS ) {
        	//https://yourmachine.yourdomain/svn/sqldev/SQLDevWeb/trunk/js/libs/sdw/oracle-sql/Grammar.js  
            copyGrammar(isJS, "C:\\Temp\\Grammar.js");       
            copyGrammar(isJS, "C:\\SQLDevWeb\\js\\libs\\sdw\\oracle-sql\\Grammar.js");       
        } else
            copyGrammar(isJS, "C:\\Temp\\sqlgrammar.txt"); 
        	//saveGrammar();
                            
    }
    
    private static void copyGrammar(boolean isJS, String fname) throws IOException {
        File file = new File(fname);
        
        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        if( isJS ) {             // for parser in javascript
        	String prelude = "/*\r\n" + 
        			" * Copyright (c) 2015-17, Oracle and/or its affiliates. All rights reserved.\r\n" + 
        			" */\r\n" + 
        			"\r\n" + 
        			"define(\r\n" + 
        			"    [],\r\n" + 
        			"    function(){\r\n" + 
        			"        var rules = [\r\n" + 
        			"            // Added by Andres\r\n" + 
        			"            {head:\"sqldev_block\",rhs:[\"sql_query_or_dml_stmt\"]},\r\n" + 
        			"            {head:\"sqldev_blocks\",rhs:[\"sqldev_block\"]},\r\n" + 
        			"            {head:\"sqldev_blocks\",rhs:[\"sqldev_block\",\"'/'\"]},\r\n" + 
        			"            {head:\"sqldev_blocks\",rhs:[\"sqldev_block\",\"'/'\",\"sqldev_blocks\"]},\r\n" + 
        			"            // End added\r\n" + 
        			"";
            bw.write(prelude);
        }
        for( RuleTuple rule : origRules ) {
            if( isJS ) {             // for parser in javascript
                
                /*if( rule.head.toLowerCase().contains("xml") 
                 || rule.head.toLowerCase().contains("partition") 
                 || rule.head.toLowerCase().contains("table_properties") 
                        ) {
                    System.out.println(rule.toString());
                    continue;
                }*/
                
                bw.write("            {head:\"");
                bw.write(rule.head.replace("\"", "\\\""));
                bw.write("\",rhs:[\"");
                int pos = -1;
                for( String t: rule.rhs ) {
                    if( 0 < ++pos )
                        bw.write("\",\"");
                    bw.write(t.replace("\"", "\\\""));
                }
                bw.write("\"]},\n");

            } else {  // Print rules for .NET team    
                bw.write(rule.toString()+"\n");
                
            }
        }
        if( isJS ) {             // for parser in javascript
            String finale = "        ];\r\n" + 
            		"\r\n" + 
            		"        return {\r\n" + 
            		"            Rules : rules,\r\n" + 
            		"            StartRule : 'sqldev_blocks'\r\n" + 
            		"        };\r\n" + 
            		"    }\r\n" + 
            		")";
            bw.write(finale);
        }
        bw.close();
    }
    private static void saveGrammar() throws Exception {
        Class.forName("oracle.jdbc.OracleDriver"); //$NON-NLS-1$
        Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@llg00hon.uk.oracle.com:1521/DB12201","hr","hr"); 
        final PreparedStatement stmt = conn.prepareStatement("insert into sql122_grammar values(?,?)");
        for( RuleTuple rule : origRules ) {
        	String r = rule.toString();
        	int semiColPos = r.indexOf(':');
        	String rhs = r.substring(semiColPos+1);
        	//rhs = rhs.replace("'", "`");
        	stmt.setString(1, rule.head);
        	stmt.setString(2, rhs);
        	stmt.execute();
        	//conn.commit();
        	System.out.println(r);
        }
    }

}
