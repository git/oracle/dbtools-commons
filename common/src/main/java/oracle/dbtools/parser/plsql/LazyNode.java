/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.Icon;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;


/**
 * Compile node on demand: if client code wants to drill down to children
 * StackParser artifact
 * @author vtropash
 */
public class LazyNode extends ParseNode {
    public String startToken;
    List<LexerToken> src;

    public LazyNode( int begin, String start, List<LexerToken> src ) {
        super(begin, -1, -1, -1, null);
        startToken = start;
        this.src = src;
    }

    private ParseNode branch = null;
    public ParseNode getBranch() {
        return branch;
    }
    public Set<ParseNode> children() {
        if( branch == null )
            return super.children();
        else {
            Set<ParseNode> ret = new TreeSet<ParseNode>();
            ret.addAll(super.children());
            ret.add(branch);
            return ret;
        }
    }
    
    @Override
    public ParseNode childAt( int head, int tail ) {
        if( branch == null )
            return super.childAt(head, tail);
        else 
            return branch.childAt(head, tail);
    }    

    /*
     * Shallow methods do not explore branches which are genuine ParseNodes
     */
    
    public List<LazyNode> shallowChildren() {
        List<LazyNode> ret = new ArrayList<LazyNode>();
        for( ParseNode child : super.children() )
            if( child instanceof LazyNode )
                ret.add((LazyNode)child);
        return ret;
    }

    public List<LazyNode> shallowDescendants() {
        List<LazyNode> ret = new ArrayList<LazyNode>();
        ret.add(this);
        for( ParseNode n : shallowChildren() )
            ret.addAll(((LazyNode)n).shallowDescendants());
        return ret;
    }

    public  List<LazyNode> shallowIntermediates( int head, int tail ) {
        List<LazyNode> ret = new ArrayList<LazyNode>();
        if( from <= head && tail <= to ) 
            ret.add(this);
        for( LazyNode n : shallowChildren() )
            if( n.from <= head && tail <= n.to ) 
                ret.addAll(n.shallowIntermediates(head, tail));
        return ret;
    }
    public LazyNode shallowParent( int head, int tail ) {
        for( LazyNode descendant : shallowIntermediates(head, tail) )
            for( LazyNode child : descendant.shallowChildren() )
                if( child.from == head && child.to == tail )
                    return descendant;
        return null;
    }
    public LazyNode shallowLeaf( int head, int tail ) {
        for( LazyNode descendant : shallowIntermediates(head, tail) ) {
            if( descendant.shallowChildren().size() == 0 &&
                descendant.from <= head && tail <= descendant.to )
                return descendant;
        }
        return null;
    }
    /**
     * this -- the root node of the tree
     * @return the narrowest LazyNode containing [head,tail)
     */
    public LazyNode ancestor( int head, int tail ) {
        List<LazyNode> candidates = shallowChildren();
        if( candidates.size() == 0 )
            return this;
        for( LazyNode child : candidates )
            if( child.from <= head && tail <= child.to )
                return child.ancestor(head, tail);
        return this;
    }


    public boolean isAuxiliary() {
        return false;
    }

    protected String toString( int depth ) {      
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < depth ;i++)
            sb.append("  ");  //$NON-NLS-1$
        sb.append("["+from+","+to+") ");  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        sb.append("  "+ startToken); //$NON-NLS-1$
        return sb.toString();
    }

    public boolean isAs() {
        return "is".equalsIgnoreCase(startToken) //$NON-NLS-1$
        ||     "as".equalsIgnoreCase(startToken) //$NON-NLS-1$
        ||     "before".equalsIgnoreCase(startToken) //$NON-NLS-1$
        ||     "after".equalsIgnoreCase(startToken) //$NON-NLS-1$
        ||     "of".equalsIgnoreCase(startToken); //$NON-NLS-1$
    }
    public boolean isCompilationUnit() {
        return "package".equalsIgnoreCase(startToken) || //$NON-NLS-1$
               "trigger".equalsIgnoreCase(startToken) || //$NON-NLS-1$
        isProcedure();
    }
    public boolean isProcedure() {
        return "function".equalsIgnoreCase(startToken) || //$NON-NLS-1$
        "procedure".equalsIgnoreCase(startToken); //$NON-NLS-1$
    }
    
    /**
     * Backward compatibility
     */
    public boolean isDML() {
        return isDML(null);
    }
    /**
     * @param next  - supplying non null value empowers better recognition (e.g. "procedure delete;" )
     * @return
     */
    public boolean isDML( String next ) {
        return 
        !"(".equals(next) && !";".equals(next) &&       
        ("insert".equalsIgnoreCase(startToken) || //!merge //$NON-NLS-1$
        "update".equalsIgnoreCase(startToken) || //!for update...etc //$NON-NLS-1$
        "delete".equalsIgnoreCase(startToken)  || // generalised for parent != DML //$NON-NLS-1$
        "select".equalsIgnoreCase(startToken) || //$NON-NLS-1$
        "with".equalsIgnoreCase(startToken) && !"context".equalsIgnoreCase(next) || //$NON-NLS-1$
        "cursor".equalsIgnoreCase(startToken) && !"is".equalsIgnoreCase(next) && !"as".equalsIgnoreCase(next) || //$NON-NLS-1$
        "merge".equalsIgnoreCase(startToken) || //$NON-NLS-1$
        "fetch".equalsIgnoreCase(startToken) )//$NON-NLS-1$
        ;
    }
    public boolean isDDL( String next ) {
        return "alter".equalsIgnoreCase(startToken) ||  //$NON-NLS-1$
        "create".equalsIgnoreCase(startToken) ||  //$NON-NLS-1$
        "grant".equalsIgnoreCase(startToken) ||  //$NON-NLS-1$
        "drop".equalsIgnoreCase(startToken)  || //$NON-NLS-1$
        "comment".equalsIgnoreCase(startToken)  ||   //$NON-NLS-1$
        "administer".equalsIgnoreCase(startToken)  && "key".equalsIgnoreCase(next)   //$NON-NLS-1$
        ;
    }
    boolean isControlStmt( LazyNode root ) {
        String parentStartToken = "N/A";
        LazyNode parent = root.shallowParent(from, to);
        if( parent != null )
            parentStartToken = ((LazyNode)parent).startToken;
        return "if".equalsIgnoreCase(startToken) ||  //$NON-NLS-1$
        "case".equalsIgnoreCase(startToken) ||  //$NON-NLS-1$
        "for".equalsIgnoreCase(startToken) ||  //$NON-NLS-1$
        "while".equalsIgnoreCase(startToken) || //$NON-NLS-1$
        "loop".equalsIgnoreCase(startToken) && !"for".equalsIgnoreCase(parentStartToken) && !"while".equalsIgnoreCase(parentStartToken) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        ;
    }
    public boolean isStmt( LazyNode root ) {
        if( "declare".equalsIgnoreCase(startToken) ) //$NON-NLS-1$
            return true;
        if( "begin".equalsIgnoreCase(startToken) ) { //$NON-NLS-1$
            LazyNode parent = root.shallowParent(from, to);
            if( parent == null ) 
                return true;
            if( !"declare".equalsIgnoreCase(parent.startToken) && !parent.isProcedure() ) //$NON-NLS-1$
                return true;
        }
        return isControlStmt(root) || isDML(null) || isDDL(null);
    }
    
    public void expandInterruptably() throws InterruptedException {
        expand(true);
    }

    public void expand()  {
        try {
            expand(false);
        } catch( InterruptedException e ) {}
    }
    
    public synchronized void expand( boolean interrupt ) throws InterruptedException  {
    	if( branch != null )
    		return;
        List<LexerToken> fragment = new ArrayList<LexerToken>();
        int k = -1;
        for( LexerToken tok : src ) {
            k++;
            if( interrupt && Thread.interrupted() )
                throw new InterruptedException();
            if( from <= k && k < to ) {
                fragment.add(tok);
            }
        }

        /* Can't parse fragments with Earley ?
        Matrix matrix = PlsqlCYK.getInstance().initArray1(fragment);
        int size = matrix.size();
        TreeMap<Integer,Integer> skipRanges = new TreeMap<Integer,Integer>();
        PlsqlCYK.getInstance().closure(matrix, 0, size+1, skipRanges,-1);
        branch = PlsqlCYK.getInstance().forest(fragment, matrix);
        */
        SqlEarley earley = SqlEarley.getInstance();
        Matrix matrix = new Matrix(earley);
        earley.parse(fragment, matrix, interrupt? new InterruptedException() : null); 
        branch = earley.forest(fragment, matrix);
        branch.moveInterval(from);
//System.out.println("from,to)=("+from+","+to+")");
//System.out.println("eval="+(System.currentTimeMillis()-t1));
    }
    
    /**
     * https://community.oracle.com/thread/3521866 LinkedList -> ArrayList
     * @return
     */
    public List<LexerToken> getSrcFragment() {
        List<LexerToken> fragment = new ArrayList<LexerToken>();
        int k = -1;
        int offset = 0;
        for( LexerToken tok : src ) {
            k++;
            if( to <= k ) 
                break;
            if( from == k  )
                offset = tok.begin;
            if( from <= k  ) 
                fragment.add(new LexerToken(tok.content, tok.begin-offset, tok.end-offset, tok.type));
        }
        return fragment;
    }

    @Override
    public void addContent( String symbol ) {
        startToken = symbol;
    }
    
}
