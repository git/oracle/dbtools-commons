/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.util.List;

import oracle.dbtools.parser.Cell;
import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.Visual;
import oracle.dbtools.raptor.newscriptrunner.commands.HiddenParameters;
import oracle.dbtools.util.Service;

public class ParsedSql extends Parsed {

    public ParsedSql( String input ) throws InterruptedException {
        super(input, SqlEarley.getInstance(), null);
        try {
            src =  LexerToken.parse(input,false,LexerToken.QuotedStrings+LexerToken.SqlPlusComments, new InterruptedException());
        } catch( InterruptedException e ) {
            earley = null;
            throw new InterruptedException();
        }
    }

    @Override
    public synchronized List<LexerToken> getSrc() {
        if( src == null || earley == null )
            throw new AssertionError("Impossible ParsedSql instance with broken lexical analysis");
        return src;
    }
     
    /**
     * Parsed.getRoot() amended with parsing aborted upon interruption
     * @return
     * @throws InterruptedException
     */
    public synchronized ParseNode parse() throws InterruptedException {
        if( earley == null )
            throw new InterruptedException("Interrupted earlier");
        if( root == null ) {
            //LexerToken.print(src);
            Visual visual = null;
            if( debug )
                visual = new Visual(src, earley);
            matrix = new Matrix(earley);
            try {
                ((SqlEarley)earley).parse(src, matrix, new InterruptedException()); 
                //earley.parse(src, matrix); 
            } catch( InterruptedException e ) {
            //} catch( /*Interrupted*/ Exception e ) {
                src = null;
                throw new InterruptedException();
            }
            if( debug )
                visual.draw(matrix);
            //Cell top = matrix.get(0, src.size());
            //if( top == null ) // if input is syntactically recognized 
                //System.out.println("***** Syntactically Invalid code fragment *****"); //$NON-NLS-1$
            /*if( rootSyntax != null ) {
                err = SyntaxError.checkSyntax(input, new String[]{rootSyntax}, src, earley, matrix);
                if( err != null ) { //$NON-NLS-1$ //$NON-NLS-2$
                    throw err; //$NON-NLS-1$
                }
            }*/
            
            root = earley.forest(src, matrix);
        }
        return root;
    }
    
    /**
     * This method is has wrong signature, use parse()
     */
    @Override
    public synchronized ParseNode getRoot() {
        // Guarded against accidentally inheriting this method 
        throw new AssertionError("overriden by parse(), which throws InterruptedException");
    }


    
    /**
     * Recognize a command for sqlcli
     * Algorithm: 
     *  1. find matrix cell where parsing advanced the most
     *  2. examine cell content
     *  3. follow recognized symbols up the rules dependency to see what their destination is
     *  @ return enum {SQL, PLSQL, SQLPLUS}
     */
    public enum Language {SQL, PLSQL, SQLPLUS}
    public static Language partialRecognize( String input ) {
        if( "true".equals(HiddenParameters.parameters.get("debugPartialRecognize")) ) {
            System.out.println("input=`"+input+"`");
        }
        List<LexerToken> src =  LexerToken.parse(input);
        if( src.size() == 0 ) {
            throw new AssertionError("empty input: `"+input+"`");
        }
        SqlEarley earley = partialRecognizer;
        Matrix matrix = new Matrix(earley);
        earley.parse(src, matrix);
        int symbol = leftBottomSymbol(matrix);
        return crawlRulesDependency(symbol);
    }

    static SqlEarley partialRecognizer = SqlEarley.partialRecognizer(new String[]{"sql_statements","subprg_body"});
    private static int leftBottomSymbol( Matrix matrix ) {
        OUTER:
        for( int i = matrix.lastY() ; 0 < i ; i--) {
            Cell cell = matrix.get(0, i);
            if( cell != null ) {
                int bestRule = 0;
                int advanced = 0;
                for( int j = 0; j < cell.size(); j++ ) {
                    int rule = cell.getRule(j);
                    int pos = cell.getPosition(j);
                    Earley.Tuple tuple = partialRecognizer.rules[rule];
                    if( advanced < pos 
                     //|| advanced == pos && partialRecognizer.allSymbols[tuple.rhs[pos]].charAt(0) == '\'' 
                     || pos == tuple.rhs.length ) {
                        bestRule = rule;
                        advanced = pos;
                    }
                    if( pos == tuple.rhs.length ) {
                        if( tuple.head==partialRecognizer.sql_statements
                         || tuple.head==sql_statement
                         || tuple.head==library_unit
                        )
                            continue OUTER;
                        return tuple.head;  // for syntactically legitimate statements tuple.head might be sql_statement
                    }
                }
                // no complete rules 
                Earley.Tuple tuple = partialRecognizer.rules[bestRule];
                return tuple.head;  
            }
        }
        throw new AssertionError("all empty cells?");    
    }
    
    static int library_unit = partialRecognizer.getSymbol("library_unit");
    static int create_plsql = partialRecognizer.getSymbol("create_plsql");
    static int call_statement = partialRecognizer.getSymbol("call_statement");
    static int sqlplus_command = partialRecognizer.getSymbol("sqlplus_command");
    static int sql_statement = partialRecognizer.getSymbol("sql_statement");
    static int sql_stmt = partialRecognizer.getSymbol("sql_stmt");
    static int unlabeled_nonblock_stmt = partialRecognizer.getSymbol("unlabeled_nonblock_stmt");
    private static Language crawlRulesDependency( int symbol ) {
        if( symbol == sql_stmt )
            return Language.SQL;
        if(  symbol == create_plsql
         || symbol == call_statement
         || symbol == unlabeled_nonblock_stmt)
            return Language.PLSQL;
        if( symbol == sqlplus_command)
            return Language.SQLPLUS;
        if( symbol == sql_statement )
            return Language.SQL;
        for( int i = 0; i < SqlEarley.getInstance().rules.length; i++ ) {
            Earley.Tuple tuple = SqlEarley.getInstance().rules[i];
            if( tuple.head == tuple.rhs[0] )
                continue;
            if( tuple.rhs[0] == symbol ) 
                try {
                    return crawlRulesDependency(tuple.head);
                } catch( AssertionError e ) {
                    continue;
                }
        }
        throw new AssertionError("failed to follow dependency chain for "+SqlEarley.getInstance().allSymbols[symbol]);
    }

    public static void main( String[] args ) throws Exception {
        String input =  Service.readFile(ParsedSql.class, "test.sql");
        System.out.println(partialRecognize(input));
        //--------------------------------
        test("select",Language.SQL);
        test("select 1",Language.SQL);
        test("select 1,a",Language.SQL);
        test("select 1,a from",Language.SQL);
        test("select 1,a from dual",Language.SQL);
        test("select 1,a from dual where ",Language.SQL);
        test("select 1,a from dual where (select",Language.SQL);
        test("create table",Language.SQL);
        //--------------------------------
        test("begin",Language.PLSQL);
        test("begin null",Language.PLSQL);
        test("begin null end",Language.PLSQL);
        test("create function",Language.PLSQL);  // TODO: fix
        //--------------------------------
        test("CONNECT",Language.SQLPLUS);
        test("CONNECT HR/",Language.SQLPLUS);
        test("SET INSTANCE FIN2",Language.SQLPLUS);
        test("EDIT REPORT",Language.SQLPLUS);
        test("STARTUP NOMOUNT",Language.SQLPLUS);
    }
    private static void test( String input, Language cmp ) {
        if( partialRecognize(input) != cmp ) {
            System.out.println("**** Failed on\n"+input+"\n"+"Recognized "+partialRecognize(input));
        } else
            System.out.println(input+" -- OK");
    }

    /*public static void main( String[] args ) throws Exception {
    // Interrupt signal. When parsing MSC_CL_PRE_PROCESS.pkgbdy setting sleep(80) would interrupt lexing
    // sleep(5000) would interrupt parsing
    final Thread main = Thread.currentThread();
    new Thread() {
        @Override
        public void run() {
            try {
                sleep(50000); // go to completion: 50000 ms is more than 25 sec requred to parse MSC_CL_PRE_PROCESS
            } catch (InterruptedException e) {
            }
            main.interrupt();
        }            
    }.start();
    
    String input = 
            //"SELECT S FROM ( SELECT STRING_COL S FROM TEST )"
            //Service.readFile(SqlEarley.class, "test.sql") //$NON-NLS-1$
            //Service.readFile(SqlEarley.class, "FND_STATS.pkgbdy") //$NON-NLS-1$
            Service.readFile(SqlEarley.class, "MSC_CL_PRE_PROCESS.pkgbdy") //$NON-NLS-1$
        ;
    SqlEarley.fullRecognizer();// to exclude this one time initialization from lex time
    
    long t1 = System.currentTimeMillis();
    ParsedSql parsed = new ParsedSql(input);
    long t2 = System.currentTimeMillis();
    System.out.println("Constructor (Lex) time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
    
    t1 = System.currentTimeMillis();
    List<LexerToken> src2 = parsed.getSrc();
    t2 = System.currentTimeMillis();
    System.out.println("getter time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
    System.out.println("tokens= "+src2.size()); // (authorized) //$NON-NLS-1$
    
    t1 = System.currentTimeMillis();
    ParseNode root = parsed.parse();
    t2 = System.currentTimeMillis();
    System.out.println("parse time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
    if( src2.size() < 100 )
        root.printTree();
}*/

    
}
