/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

import oracle.dbtools.parser.Grammar;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.RuleTransforms;
import oracle.dbtools.parser.RuleTuple;
import oracle.dbtools.util.Service;

/**
 * Generates PL/SQL grammar from plsql.y
 * @author Dim
 */
public class PlsqlRules {

    public static void main(String[] args) throws Exception {
    	throw new AssertionError("VT: broken");
        //memorizeRules();
    }

    /*private static final String fname = "plsqlBNF.serial"; //$NON-NLS-1$
    private static final String path = "/oracle/dbtools/parser/plsql/"; //$NON-NLS-1$
    */
    /*private static void memorizeRules() throws Exception {
        Set<RuleTuple> rules = extractRules();				    
        FileOutputStream fos = new FileOutputStream("utils-nodeps/src"+path+fname); //$NON-NLS-1$
        ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(rules);
        out.close();
        RuleTuple.printRules(rules);
    }*/
    /*public static Set<RuleTuple> getRules() throws Exception {
        URL u = PlsqlRules.class.getResource( path+fname );
        InputStream is = u.openStream();
        ObjectInputStream in = new ObjectInputStream(is);
        Set<RuleTuple> rules = (Set<RuleTuple>) in.readObject();
        in.close();
        return rules;
    }*/

    /**
     * Digests bnfFile into rules 
     */
    public static Set<RuleTuple> parseBNFtext( String input ) throws Exception {
        Set<RuleTuple> ret = new TreeSet<RuleTuple>();        
        List<LexerToken> src1 = LexerToken.parse(input, false, LexerToken.QuotedStrings/*+SqlPlusComments*/);
        List<LexerToken> src = new ArrayList<LexerToken>();
        for( LexerToken t : src1 ) {
        	if( t.content.charAt(0)!='.' && t.content.charAt(0)!='_' && t.content.charAt(t.content.length()-1)=='_' && t.content.toUpperCase().equals(t.content) ) 
        		t.content = "'"+t.content.substring(0, t.content.length()-1).toUpperCase()+"'"; 
        	src.add(t);
        }
        //LexerToken.print(src);
        ParseNode root = Grammar.parseGrammarFile(src, input);
        Grammar.grammar(root, src, ret);
        substitute(ret, "expr", "pls_expr");
        //substitute(ret, "alt_ty_attr_list", "\"alt_ty_attr_list\""); // to suppress optim
        return ret;
    }
    
    private static void substitute( Set<RuleTuple> ret, String expr, String subst  ) {
		Set<RuleTuple> delete = new TreeSet<RuleTuple>();
		Set<RuleTuple> add = new TreeSet<RuleTuple>();
        boolean replace = false;
		for( RuleTuple r: ret ) {
			RuleTuple clone = new RuleTuple(r.head, new String[r.rhs.length]);
			if( r.head.equals(expr) ) { 
                clone.head = subst;
                replace = true;
			}
		    for( int i = 0; i < r.rhs.length; i++ ) 
		    	if( r.rhs[i].equals(expr) ) { 
		    		clone.rhs[i] = subst;
		    		replace = true;
		    	} else
		    		clone.rhs[i] = r.rhs[i];
		    		    
		    if( replace ) {
                delete.add(r);
                add.add(clone);	
		    }
		} 
		ret.removeAll(delete);
		ret.addAll(add);
	}
    
    
    static void fixIdentifierIsOrAs( Set<RuleTuple> ret ) {
        Set<RuleTuple> more = new TreeSet<RuleTuple>();
        for( RuleTuple t : ret ) {
            if( 3 <= t.rhs.length ) {
                if( ("'PACKAGE'".equals(t.rhs[0])||"'TYPE'".equals(t.rhs[0])||"'PROCEDURE'".equals(t.rhs[0])||"'FUNCTION'".equals(t.rhs[0])||"'TRIGGER'".equals(t.rhs[0]) )
                    && "identifier".equals(t.rhs[1])  
                    //authid_or_white_list_opt
                    //&& "is_or_as".equals(t.rhs[?])                           
                ) {
                    RuleTuple t1 = new RuleTuple(t.head,new String[t.rhs.length+2]);
                    t1.rhs[0] = t.rhs[0];
                    t1.rhs[1] = "identifier";
                    t1.rhs[2] = "'.'";
                    for( int i = 1; i < t.rhs.length; i++ ) 
                        t1.rhs[i+2] = t.rhs[i];                   
                    more.add(t1);
                    continue;
                }
            }
            if( 4 <= t.rhs.length ) {
                if( "'BODY'".equals(t.rhs[1])
                    && "identifier".equals(t.rhs[2])
                    //authid_or_white_list_opt
                    //&& "is_or_as".equals(t.rhs[?])                           
                ) {
                    RuleTuple t1 = new RuleTuple(t.head,new String[t.rhs.length+2]);
                    t1.rhs[0] = t.rhs[0];
                    t1.rhs[1] = t.rhs[1];
                    t1.rhs[2] = "identifier";
                    t1.rhs[3] = "'.'";
                    for( int i = 2; i < t.rhs.length; i++ ) 
                        t1.rhs[i+2] = t.rhs[i];                   
                    more.add(t1);
                    continue;
                }
            }
        }
        ret.addAll(more);
    }

    /**
     * @param ret
     */
    /* duplicate of PlsqlFixes.grammar
     * public static void numberRules(Set<RuleTuple> ret) {
        ret.add(new RuleTuple(".fd.", new String[] {})); //$NON-NLS-1$
        ret.add(new RuleTuple(".fd.", new String[] {"'f'"})); //$NON-NLS-1$ //$NON-NLS-2$
        ret.add(new RuleTuple(".fd.", new String[] {"'d'"})); //$NON-NLS-1$ //$NON-NLS-2$
        ret.add(new RuleTuple(".+-.", new String[] {})); //$NON-NLS-1$
        ret.add(new RuleTuple(".+-.", new String[] {"'+'"})); //$NON-NLS-1$ //$NON-NLS-2$
        ret.add(new RuleTuple(".+-.", new String[] {"'-'"})); //$NON-NLS-1$ //$NON-NLS-2$
        ret.add(new RuleTuple(".exp.", new String[] {})); //$NON-NLS-1$
        ret.add(new RuleTuple(".exp.", new String[] {"'E'",".+-.","digits"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        ret.add(new RuleTuple("numeric_literal", new String[] {"digits",".exp.",".fd."})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        ret.add(new RuleTuple("numeric_literal", new String[] {"digits","'.'",".exp.",".fd."})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
        ret.add(new RuleTuple("numeric_literal", new String[] {"'.'","digits",".exp.",".fd."})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
        ret.add(new RuleTuple("numeric_literal", new String[] {"digits","'.'","digits",".exp.",".fd."})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
    }*/

    public static Set<String> getKeywords( Set<RuleTuple> rules ) {
        Set<String> ret = new TreeSet<String>();
        for(RuleTuple rule: rules) 
            for(String token: rule.rhs)
                if(token.startsWith("'")) //$NON-NLS-1$
                    ret.add(token);
        return ret;
    }

    static String readBNFfile() throws Exception {
        String rulesTxt = Service.readFile(PlsqlRules.class, "plsql.y"); //$NON-NLS-1$
        //String rulesTxt = Service.readFile("d:/raptor_trunk/utils-nodeps/src/oracle/dbtools/parser/plsql/plsql.y"); //$NON-NLS-1$

        final String startPattern = "/*---------------------------- Start of Rules -------------------------------*/"; //$NON-NLS-1$
        rulesTxt=rulesTxt.substring(rulesTxt.indexOf(startPattern)+startPattern.length());

        rulesTxt=removeNestedBlock(rulesTxt,"/*","*/"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt=removeNestedBlock(rulesTxt,"{","}"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt=removeNestedBlock(rulesTxt,"{","}");  // 2-nd level //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt=removeNestedBlock(rulesTxt,"{","}");   //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt=removeNestedBlock(rulesTxt,"{","}");   //$NON-NLS-1$ //$NON-NLS-2$


        rulesTxt = rulesTxt.replace("ASSIGN_", "':' '='"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("LTEQ_", "'<' '='"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("GTEQL_", "'>' '='"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("NOTEQL_", "'!' '='"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("BOX_", "'<' '>'"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("CAT_", "'|' '|'"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace(" DBLDOT_ ", " '.' '.' "); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("ARROW_", "'=' '>'"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("EXP_ pri", "'*' '*' pri"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("L_LBL_", "'<' '<'"); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("R_LBL_", "'>' '>'"); //$NON-NLS-1$ //$NON-NLS-2$
        
        //wrong! http://docs.oracle.com/database/121/LNPCC/pc_04dat.htm#LNPCC3206
        // rulesTxt = rulesTxt.replace(" INDICATOR_ ", " ':' "); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("bind_var INDICATOR_ id", "bind_var INDICATOR_ identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        
        //rulesTxt = rulesTxt.replace("ph1psh_", " "); //$NON-NLS-1$ //$NON-NLS-2$

        rulesTxt = rulesTxt.replace("')'", " ')' ");   //to fix      idx_c: '(' collection_limit')'; //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("'('", " '(' "); //$NON-NLS-1$ //$NON-NLS-2$

        rulesTxt = rulesTxt.replace("%%", ""); //$NON-NLS-1$ //$NON-NLS-2$
        
        rulesTxt = rulesTxt.replace("bind_var.INDICATOR_id.", "\"bind_var.INDICATOR_id.\""); //$NON-NLS-1$ //$NON-NLS-2$
        rulesTxt = rulesTxt.replace("...from_table_reference_or_subquery..", "\"...from_table_reference_or_subquery..\""); //$NON-NLS-1$ //$NON-NLS-2$

        //System.out.println(rulesTxt);
        //System.out.println("*** EOF ***");
        
        return rulesTxt;
    }

    /**
     * Removes one level of nesting for a block delimited between br1 and br2
     * @param src
     * @param br1 - opening block delimiter; e.g. {*
     * @param br2 - closing block delimiter; e.g. *}
     * @return
     */
    static String removeNestedBlock( String src, String br1, String br2 ) {
        final int len1 = br1.length(); 
        final int len2 = br2.length(); 
        int beg = 0;
        int end = 0;
        StringBuffer out = new StringBuffer();
        while( beg < src.length() ) {
            beg = src.indexOf(br1, end);
            int beg1 = src.indexOf(br1, beg+len1);
            int end1 = src.indexOf(br2, beg+len1) +len2;
            if( beg >0 && beg < beg1 && beg1 < end1 ) {
                out.append(src.substring(end, beg+len1));
                end = beg+len1;
                continue;
            }
            if( beg < 0 ) 
                beg = src.length();
            out.append(src.substring(end, beg));
            end = src.indexOf(br2, beg+len1) +len2;
            if( end	 < len2 ) {
                out.append(src.substring(beg));
                break;
            }
        }
        return out.toString();
    }

}
