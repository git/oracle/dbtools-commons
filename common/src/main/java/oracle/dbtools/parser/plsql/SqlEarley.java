/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.parser.Cell;
import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matriceable;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parseable;
import oracle.dbtools.parser.RuleTuple;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.Visual;
import oracle.dbtools.util.Array;
import oracle.dbtools.util.Service;

/**
 * Earley parsing method adapted for parsing SQL & PL/SQL & and some SQL*Plus commands
 * The grammar is fed by UnifiedRules 
 * @author vtropash
 *
 */
public class SqlEarley extends Earley implements Parseable {
	
    public static boolean visualize = true;

    /**
     * Example usage
     * @param args -- standard dummy
     * @throws Exception
     */
    public static void main( String[] args ) throws Exception {
        String file = "test.sql"; //$NON-NLS-1$
        //file = "FND_STATS.pkgbdy"; //$NON-NLS-1$
        //file = "MSC_CL_PRE_PROCESS.pkgbdy"; //$NON-NLS-1$
        // read input
        String input =  Service.readFile(SqlEarley.class, file);
        //String input =  Service.readFile("/Users/Dim/git/dbtools-commons/common/src/main/resources/oracle/dbtools/parser/plsql/"+ file);
        //input =  Service.readFile("/temp/createxmlschemaindexestest.sql");
        if( 1 == args.length ) {
            input = args[0];
        }
        
        long t1 = System.currentTimeMillis();
        // Compare init timing: Earley init time = 171
        SqlEarley earley = SqlEarley.getInstance();        
        //earley = SqlEarley.partialRecognizer();        
        long t2 = System.currentTimeMillis();
        System.out.println("Earley init time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
        
        final boolean T = true;   // less typing
        final boolean F = false;
        earley.printOrderedRules("ext_tbl_string_literal", /*headOnly=*/F, /*exactmatch=*/F);
        //System.out.println(earley.terminalPredictions[earley.getSymbol("group_by_clause")].toString()); //'GROUP' 
        //System.out.println(earley.terminalPredictions[earley.getSymbol("select_list")].toString());
        
		//Service.profile(15000, 50);
        int maxRuleLen = 0;
        for( Tuple rule : SqlEarley.getInstance().rules ) {
            if( maxRuleLen < rule.rhs.length ) 
                maxRuleLen = rule.rhs.length;
            if( rule.rhs.length == 1 && rule.head == rule.rhs[0] )
                System.out.println("***** "+getInstance().allSymbols[rule.head]+":"+getInstance().allSymbols[rule.head]);
        }
                
        
        System.out.println("----rules="+SqlEarley.getInstance().rules.length); 
        System.out.println("----maxRuleLen="+maxRuleLen); 
    	
        System.out.println("!nil="+getInstance().getSymbol("!nil")); // (authorized)
        System.out.println("allSymbols[0]="+getInstance().allSymbols[0]); // (authorized)
        System.out.println("allSymbols[1]="+getInstance().allSymbols[1]); // (authorized)
        System.out.println("allSymbols[2]="+getInstance().allSymbols[2]); // (authorized)
        
        //for (int i = 0; i < 5; i++) 
        	parse(input);
    }
    static void parse( String input ) throws IOException, AssertionError {
        // lexical analysis
        long t1 = System.currentTimeMillis();
        List<LexerToken> src =  LexerToken.parse(input);
        if( src.size() > Integer.MAX_VALUE )
        	throw new AssertionError(src.size()+" tokens");
        
        long t2 = System.currentTimeMillis();
        System.out.println("Lex time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
        
        //if( src.size() < 1000 )
        	//LexerToken.print(src);

        // instantiate Earley
        SqlEarley earley = SqlEarley.getInstance();
                
        //System.out.println("max mem ="+Runtime.getRuntime().maxMemory()); // (authorized) //$NON-NLS-1$
        //System.out.println("total mem="+Runtime.getRuntime().totalMemory()); // (authorized) //$NON-NLS-1$
        //System.out.println("free mem="+Runtime.getRuntime().freeMemory()); // (authorized) //$NON-NLS-1$
        // instantiate Matrix
        Matrix matrix = new Matrix(earley);
        if( src.size() < 1000 ) // For small input visualize matrix. Visualization is
        	                    // for troubleshooting and is not used in production code
            matrix.visual = new Visual(src, earley);
        if( !visualize  )
            matrix.visual = null;

        t1 = System.currentTimeMillis();

        //earley.skipRanges = false; //<-- disable optimization
        earley.oldSkipRanges = earley.skipRanges;
        
        //earley.fifteenPctLAImpr = false;

        // the most important and resource intensive step -- recognition
        earley.parse(src, matrix); 
        t2 = System.currentTimeMillis();
        System.out.println("Earley parse time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
        
        //System.out.println("max mem ="+Runtime.getRuntime().maxMemory()); // (authorized) //$NON-NLS-1$
        //System.out.println("total mem="+Runtime.getRuntime().totalMemory()); // (authorized) //$NON-NLS-1$
        //System.out.println("free mem="+Runtime.getRuntime().freeMemory()); // (authorized) //$NON-NLS-1$
        /*System.out.println(matrix.size());
        long cnt = 0;
        for( long s : matrix.keySet()) {
            cnt += matrix.get(s).size();
        }
        System.out.println(cnt); */   
        
        /*
         second parse to measure amortized timing
        t1 = System.currentTimeMillis();
        earley.parse(src, matrix); 
        t2 = System.currentTimeMillis();
        System.out.println("Earley parse time2 = "+(t2-t1)); // (authorized) //$NON-NLS-1$
        */       
        
        if( matrix.visual != null )
            matrix.visual.draw(matrix); 
        
        
        // get the bottom-right corner of the matrix 
        // this is "top" in the "bottom-to-the-top" parser terminology
        Cell top = matrix.get(0, src.size());
        if( top != null ) // if input is syntactically recognized 
            System.out.println(src.size()+ " tokens, top = " + top.toString()); //$NON-NLS-1$
        else
            System.out.println("***** Syntactically Invalid fargment *****"); //$NON-NLS-1$
        
        t1 = System.currentTimeMillis();
        // the final parsing stage: building parse tree
        ParseNode root = earley.forest(src, matrix, true);
        t2 = System.currentTimeMillis();
        System.out.println("Reduction time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
        if( src.size() < 1000 )
            root.printTree();
    }
    private static void outputHighlights() {
        //// keywords
        Set<String> keywords = new TreeSet<String>();
        for( RuleTuple rule : origRules ) {
            /*if( rule.head.startsWith("\'") )
                keywords.add(rule.head.substring(1,rule.head.length()-1).toLowerCase() );
            for( int i = 0; i < rule.rhs.length; i++ ) 
                if( rule.rhs[i].startsWith("\'") )
                    keywords.add(rule.rhs[i].substring(1,rule.rhs[i].length()-1).toLowerCase() );*/
            if( "sql_statement".equals(rule.head) )
                if( rule.rhs[0].startsWith("'") )
                    keywords.add(rule.rhs[0].substring(1,rule.rhs[0].length()-1).toLowerCase() );
                else
                    for( RuleTuple rule1 : origRules ) {
                        if( rule.rhs[0].equals(rule1.head) )
                            if( rule1.rhs[0].startsWith("'") )
                                keywords.add(rule1.rhs[0].substring(1,rule1.rhs[0].length()-1).toLowerCase() );
                            else
                                for( RuleTuple rule2 : origRules ) {
                                    if( rule1.rhs[0].equals(rule2.head) )
                                        if( rule2.rhs[0].startsWith("'") )
                                            keywords.add(rule2.rhs[0].substring(1,rule2.rhs[0].length()-1).toLowerCase() );
                                        else
                                            for( RuleTuple rule3 : origRules ) {
                                                if( rule2.rhs[0].equals(rule3.head) )
                                                    if( rule3.rhs[0].startsWith("'") )
                                                        keywords.add(rule3.rhs[0].substring(1,rule3.rhs[0].length()-1).toLowerCase() );
                                            }
                                }
                    }
        }
        String exclude =   " true false null " +
            " array bigint binary bit blob boolean char character date dec decimal float int integer interval number  " +
            " numeric real serial smallint varchar varchar2 varying int8 serial8 text ";

        int lineLen = 0;
        String padding = "    ";
        System.out.print(padding+"\'");
        for( String k : keywords ) {
            if( -1 < exclude.indexOf(" "+k+" ") )
                continue;
            lineLen += k.length();
            if( lineLen > 80 ) {
                System.out.println("\'+"); // (authorized)
                System.out.print(padding+"\'"); // (authorized)
                lineLen = 0;
            }
            System.out.print(k); // (authorized)
            System.out.print(" "); // (authorized)
        }
        System.out.println(" \'+"); // (authorized)    
    }

    private static SqlEarley partialRecInst = null;
    public static SqlEarley partialRecognizer() {
    	if( partialRecInst == null ) {
    		partialRecInst = partialRecognizer(new String[]{"sql_statements","subprg_body","expr"});
    	}
    	return partialRecInst;
    }
    public static SqlEarley partialRecognizer( String[] what2Recognize ) {
        SqlEarley ret = new SqlEarley() {
            @Override
            protected boolean lookaheadOK( Tuple t, int pos, Matrix matrix ) {
                return true;  // 20041903: SqlEarley.lookaheadOK won't prompt 'TO' in "GRANT CREATE SESSION T"
            }

			@Override
			protected boolean scan( Matrix matrix, List<LexerToken> src ) {
				boolean ret = super.scan(matrix, src);
				matrix.LAsuspect = null;
				return ret;
			}           
        };
        for( String symbol : what2Recognize )
            ret.addSymbol2Recognize(symbol);
        return ret;
    }
    
 
    private static SqlEarley fullRecInst = null;
    //make synchronized to make thread safe
    public static synchronized SqlEarley getInstance() {
        if( fullRecInst == null ) {
            fullRecInst = new SqlEarley();
            fullRecInst.addSymbol2Recognize("sql_statements");
            fullRecInst.addSymbol2Recognize("subprg_body");
            fullRecInst.addSymbol2Recognize("expr");
            fullRecInst.addSymbol2Recognize("fml_part");
            fullRecInst.addSymbol2Recognize("paren_expr_list");
            fullRecInst.addSymbol2Recognize("basic_decl_item_list"); 
        }
        return fullRecInst;
    }
    
    /**
     * Oracle SQL grammar extensions.
     * Re Eugene Perkov: In particular we are dealing with so-called logical SQL 
     * which is a supreset of ANSI-92 Standard enhanced with some proprietary extensions 
     * (e.g. FILTER(column_expression) USING(expression) and many others alike).
     *  This is something that Oracle BI Server (OBIEE) produces out of presentation model + business model. 
     * @param additionalRules
     * @return
     */
    public SqlEarley grammarExtension( Set<RuleTuple> additionalRules ) {
    	Set<RuleTuple> rules = new TreeSet<RuleTuple>();
    	rules.addAll(origRules);
    	rules.addAll(additionalRules);
    	return new SqlEarley(rules);
    }

    
	static Set<RuleTuple> origRules;
	static {
		try {
			origRules = UnifiedRules.getRules();			
			// adjustments TODO: move into SqlFixes.bnf
			//origRules.add(new RuleTuple("...",new String[]{"..."}));
			origRules.remove(new RuleTuple("identifier", new String[]{"idq"})); //$NON-NLS-1$
			origRules.remove(new RuleTuple("identifier", new String[]{"id"}));  //$NON-NLS-1$
			
			origRules.remove(new RuleTuple("unconstrained_type", new String[]{"alldatetime_d"}));  //$NON-NLS-1$
			//breaks 51 tests: origRules.remove(new RuleTuple("unconstrained_type_wo_datetime", new String[]{"link_expanded_n"}));
			//breaks 140 tests: origRules.remove(new RuleTuple("unconstrained_type_wo_datetime", new String[]{"link_expanded_n","'%'","attribute_designator"}));

			origRules.remove(new RuleTuple("create_synonym", new String[]{"'CREATE'","'PUBLIC'","'SYNONYM'","identifier","'.'","identifier","'FOR'","identifier","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_synonym", new String[]{"'CREATE'","'PUBLIC'","'SYNONYM'","identifier","'.'","identifier","'FOR'","identifier","'.'","identifier","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_synonym", new String[]{"'CREATE'","'PUBLIC'","'SYNONYM'","identifier","'.'","identifier","'FOR'","identifier","'@'","dblink","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_synonym", new String[]{"'CREATE'","'PUBLIC'","'SYNONYM'","identifier","'.'","identifier","'FOR'","identifier","'.'","identifier","'@'","dblink","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_synonym", new String[]{"'CREATE'","'OR'","'REPLACE'","'PUBLIC'","'SYNONYM'","identifier","'.'","identifier","'FOR'","identifier","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_synonym", new String[]{"'CREATE'","'OR'","'REPLACE'","'PUBLIC'","'SYNONYM'","identifier","'.'","identifier","'FOR'","identifier","'.'","identifier","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_synonym", new String[]{"'CREATE'","'OR'","'REPLACE'","'PUBLIC'","'SYNONYM'","identifier","'.'","identifier","'FOR'","identifier","'@'","dblink","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_synonym", new String[]{"'CREATE'","'OR'","'REPLACE'","'PUBLIC'","'SYNONYM'","identifier","'.'","identifier","'FOR'","identifier","'.'","identifier","'@'","dblink","';'"}));  //$NON-NLS-1$

			origRules.remove(new RuleTuple("create_database_link", new String[]{"'CREATE'","'SHARED'","'DATABASE'","'LINK'","identifier","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_database_link", new String[]{"'CREATE'","'SHARED'","'DATABASE'","'LINK'","create_database_link[41,48)","identifier","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_database_link", new String[]{"'CREATE'","'SHARED'","'PUBLIC'","'DATABASE'","'LINK'","identifier","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_database_link", new String[]{"'CREATE'","'SHARED'","'DATABASE'","'LINK'","identifier","'USING'","string_literal","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_database_link", new String[]{"'CREATE'","'SHARED'","'PUBLIC'","'DATABASE'","'LINK'","create_database_link[41,48)","identifier","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_database_link", new String[]{"'CREATE'","'SHARED'","'DATABASE'","'LINK'","create_database_link[41,48)","identifier","'USING'","string_literal","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_database_link", new String[]{"'CREATE'","'SHARED'","'PUBLIC'","'DATABASE'","'LINK'","identifier","'USING'","string_literal","';'"}));  //$NON-NLS-1$
			origRules.remove(new RuleTuple("create_database_link", new String[]{"'CREATE'","'SHARED'","'PUBLIC'","'DATABASE'","'LINK'","create_database_link[41,48)","identifier","'USING'","string_literal","';'"}));  //$NON-NLS-1$


		} catch (Exception e) {}
	}
	

    private int as;
    private int aliased_dml_table_expression_clause;
    private int basic_decl_item;
    private int begin;    
    private int body;    
    private int boolean_primary;    
    private int compound_expression;    
    private int dotted_name;    
    public int decl_id;    
    private int distinct; 
    private int ELSE;
    private int expr;
    public int multiset_except;    
    private int pkg_spec;    
    private int pls_expr;    
    public int select;    
    public int simple_expression;    
    public int sql_statements;    
    private int start;    
    public int table_reference;
    private int unlabeled_nonblock_stmt;
    private SqlEarley() {
        this(origRules);
    }
    private SqlEarley( Set<RuleTuple> rules ) {
        super(rules, false);
        initKeywords();
        as = getSymbol("'AS'");  //$NON-NLS-1$
        aliased_dml_table_expression_clause = getSymbol("aliased_dml_table_expression_clause");  //$NON-NLS-1$
        basic_decl_item = getSymbol("basic_decl_item");  //$NON-NLS-1$
        begin = getSymbol("'BEGIN'");  //$NON-NLS-1$
        body = getSymbol("'BODY'");  //$NON-NLS-1$
        boolean_primary = getSymbol("boolean_primary");  //$NON-NLS-1$
        compound_expression = getSymbol("compound_expression");  //$NON-NLS-1$
        dotted_name = getSymbol("dotted_name"); //$NON-NLS-1$
        decl_id = getSymbol("decl_id"); //$NON-NLS-1$
        distinct = getSymbol("'DISTINCT'"); //$NON-NLS-1$
    	ELSE = getSymbol("'ELSE'");
    	expr = getSymbol("expr");
        multiset_except = getSymbol("multiset_except"); //$NON-NLS-1$
        pkg_spec = getSymbol("pkg_spec"); //$NON-NLS-1$
        pls_expr = getSymbol("pls_expr"); //$NON-NLS-1$
        select = getSymbol("select"); //$NON-NLS-1$
        simple_expression = getSymbol("simple_expression"); //$NON-NLS-1$
        sql_statements = getSymbol("sql_statements"); //$NON-NLS-1$
        start = getSymbol("'START'"); //$NON-NLS-1$
        table_reference = getSymbol("table_reference"); //$NON-NLS-1$
        unlabeled_nonblock_stmt = getSymbol("unlabeled_nonblock_stmt"); //$NON-NLS-1$
                 
        prioritizeRules();
    }
    

    private void prioritizeRules() {
    	prioritizeRules(getSymbol("alter_table[17,34)"), new int[] { //$NON-NLS-1$
    			getSymbol("constraint_clauses"), //$NON-NLS-1$
    			getSymbol("column_clauses"), //$NON-NLS-1$   
    			getSymbol("alter_external_table"), //$NON-NLS-1$   
    	});
    	prioritizeRules(getSymbol("analytic_function"), new int[] { //$NON-NLS-1$
    			getSymbol("count"), //$NON-NLS-1$
    			getSymbol("nth_value"), //$NON-NLS-1$
    			getSymbol("first_last_value"), //$NON-NLS-1$
    			getSymbol("listagg"), //$NON-NLS-1$
    			getSymbol("lag"), //$NON-NLS-1$
    			getSymbol("lead"), //$NON-NLS-1$
    			getSymbol("sum"), //$NON-NLS-1$
    			getSymbol("min"), //$NON-NLS-1$
    			getSymbol("max"), //$NON-NLS-1$
    			getSymbol("a_f"), //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("basic_d"), new int[] { //$NON-NLS-1$
    			getSymbol("subprg_i"), //$NON-NLS-1$
    			getSymbol("object_d"), //$NON-NLS-1$   
    	});
    	prioritizeRules(basic_decl_item, new int[] { //$NON-NLS-1$
    			getSymbol("pragma"), //$NON-NLS-1$
    			getSymbol("basic_d"), //$NON-NLS-1$   
    	});
    	prioritizeRules(boolean_primary, new int[] { //$NON-NLS-1$
    			getSymbol("sim_expr"), //$NON-NLS-1$
    			getSymbol("condition"), //$NON-NLS-1$   
    			getSymbol("function_expression"), //$NON-NLS-1$   
    	});
   	// prioritized       boolean_primary:  sim_expr  relal_op_sim_expr_opt;
    	// instead of        boolean_primary:  sim_expr;
    	// Fix it:
    	Earley.Tuple st = null;
    	int is = -1; 
    	Earley.Tuple ct = null;
    	int ic = -1;
    	for( int i = 0; i < rules.length; i++ ) {
    		Earley.Tuple t = rules[i];
    		if( "boolean_primary:  sim_expr;".equals(t.toString()) ) {
    			st = t;
    			is = i;
    		}
    		if( "boolean_primary:  condition;".equals(t.toString()) ) {
    			ct = t;
    			ic = i;
    		}
    	}
    	rules[is] = ct;	
    	rules[ic] = st;	

    	prioritizeRules(getSymbol("cell_assignment[13,37)"), new int[] { //$NON-NLS-1$
    			getSymbol("multi_column_for_loop"), //$NON-NLS-1$
    			getSymbol("cell_assignment[14,21)"), //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("col_properties"), new int[] { //$NON-NLS-1$
    			getSymbol("out_of_line_ref_constraint"), //$NON-NLS-1$
    			getSymbol("out_of_line_constraint"), //$NON-NLS-1$
    			getSymbol("column_definition"), //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("column_definition[72,82)"), new int[] { //$NON-NLS-1$
    			getSymbol("inline_ref_constraint"), //$NON-NLS-1$
    			getSymbol("inline_constraint"), //$NON-NLS-1$   
    	});
    	prioritizeRules(getSymbol("comparison_condition"), new int[] { //$NON-NLS-1$
    			getSymbol("between_condition"), //$NON-NLS-1$
    			getSymbol("group_comparison_condition"), //$NON-NLS-1$
    			getSymbol("simple_comparison_condition"), //$NON-NLS-1$
    	});
    	prioritizeRules(compound_expression, new int[] { //$NON-NLS-1$
    			getSymbol("expr"), //$NON-NLS-1$
    			getSymbol("compound_expression[8,22)"), //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("datetime_expression[32,57)"), new int[] { //$NON-NLS-1$
    			getSymbol("'DBTIMEZONE'"), //$NON-NLS-1$
    			getSymbol("string_literal"), //$NON-NLS-1$
    			expr, //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("expr#"), new int[] { //$NON-NLS-1$
    			simple_expression, //$NON-NLS-1$
    			getSymbol("function_expression"), //$NON-NLS-1$
    			getSymbol("model_expression"), //$NON-NLS-1$
    			getSymbol("object_access_expression"), //$NON-NLS-1$
    			getSymbol("type_constructor_expression"), //$NON-NLS-1$
    			getSymbol("JSON_object_access_expr"), //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("function_expression"), new int[] { //$NON-NLS-1$
    			getSymbol("function"), //$NON-NLS-1$
    			getSymbol("function_call"), //$NON-NLS-1$   
    	});
    	prioritizeRules(getSymbol("function"), new int[] { //$NON-NLS-1$
    			getSymbol("aggregate_function"), //$NON-NLS-1$
    			getSymbol("analytic_function"), //$NON-NLS-1$
    			//??getSymbol("model__function"), //$NON-NLS-1$
    			getSymbol("single_row_function"), //$NON-NLS-1$
    			getSymbol("user_defined_function"), //$NON-NLS-1$
    			getSymbol("object_reference_function"), //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("group_by_col"), new int[] { //$NON-NLS-1$
    			getSymbol("rollup_cube_clause"), //$NON-NLS-1$
    			getSymbol("expr"), //$NON-NLS-1$   
    	});
    	
    	prioritizeRules(getSymbol("modify_column_clauses[14,21)"), new int[] { //$NON-NLS-1$
    			getSymbol("modify_col_visibility"), //$NON-NLS-1$
    			getSymbol("modify_col_properties"), //$NON-NLS-1$   
    			getSymbol("virtual_column_definition"), //$NON-NLS-1$   
    	});
    	
    	prioritizeRules(pls_expr, new int[] { //$NON-NLS-1$
    			getSymbol("pls_expr"), //$NON-NLS-1$
    			getSymbol("and_expr"), //$NON-NLS-1$
    	});
		prioritizeRules(getSymbol("select_term"), new int[] { //$NON-NLS-1$
    			expr, //$NON-NLS-1$
    			getSymbol("\"aliased_expr\""), //$NON-NLS-1$
    	});
    	prioritizeRules(simple_expression, new int[] { //$NON-NLS-1$
    			getSymbol("'NULL'"), //$NON-NLS-1$
    			getSymbol("'CONNECT_BY_ROOT'"), //$NON-NLS-1$
    			getSymbol("'ROWID'"), //$NON-NLS-1$
    			getSymbol("'ROWNUM'"), //$NON-NLS-1$
    			getSymbol("'CONNECT_BY_ISCYCLE'"), //$NON-NLS-1$
    			getSymbol("'CONNECT_BY_ISLEAF'"), //$NON-NLS-1$
    			getSymbol("identifier"), //$NON-NLS-1$
    			getSymbol("column"), //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("sim_stmt"), new int[] { //$NON-NLS-1$
    			getSymbol("exit_stmt"), //$NON-NLS-1$
    			getSymbol("procedure_call"), //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("ty_def"), new int[] { //$NON-NLS-1$
    			getSymbol("array_ty_def"), //$NON-NLS-1$
    			getSymbol("tbl_ty_def"), //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("unconstrained_type_wo_datetime"), new int[] { //$NON-NLS-1$
    			getSymbol("'SYS_REFCURSOR'"), //$NON-NLS-1$
    			getSymbol("pls_number_datatypes"), //$NON-NLS-1$
    			getSymbol("link_expanded_n"), //$NON-NLS-1$
    	});
    	prioritizeRules(unlabeled_nonblock_stmt, new int[] { //$NON-NLS-1$
    			getSymbol("sql_stmt"), //$NON-NLS-1$
    			getSymbol("sim_stmt"), //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("values_clause[12,20)"), new int[] { //$NON-NLS-1$
    			getSymbol("par_expr_list"), //$NON-NLS-1$
    			expr, //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("windowing_clause[31,73)"), new int[] { //$NON-NLS-1$
    			getSymbol("'UNBOUNDED'"), //$NON-NLS-1$
    			expr, //$NON-NLS-1$
    	});
    	prioritizeRules(getSymbol("windowing_clause[81,123)"), new int[] { //$NON-NLS-1$
    			getSymbol("'UNBOUNDED'"), //$NON-NLS-1$
    			expr, //$NON-NLS-1$
    	});
    }

    /**
     * Swap rules indexes in order to influence derivation
     * Rule priority example:
     *   expr:simple_expression < expr:function_expression < expr:object_access_expression
     * @param head
     * @param symbols
     */
    private void prioritizeRules( final int head, int[] symbols ) {
        int ruleNumbers[] = new int[symbols.length];
    	for( int i = 0; i < rules.length; i++ ) {
    		Earley.Tuple t = rules[i];
        	if( t.head == head && (t.rhs.length == 1 
          			   || t.head == boolean_primary
        			   || t.head == basic_decl_item
        			   || t.head == compound_expression
        			   || t.head == simple_expression
           			   || t.head == pls_expr
           			   || t.head == unlabeled_nonblock_stmt
            ) )
        	    for( int j = 0; j < symbols.length; j++ ) {
                    if( t.rhs[0] == symbols[j] ) {
                        //ruleNumbers[j] = -i; // to facilitate reverse order sort
                    	ruleNumbers[j] = i;
                    	break;
                    }
                }
		}
    	Tuple[] tuples = new Tuple[symbols.length];
        for( int j = 0; j < symbols.length; j++ ) 
            //tuples[j] = rules[-ruleNumbers[j]];
        	tuples[j] = rules[ruleNumbers[j]];
        Arrays.sort(ruleNumbers);
        for( int i = 0; i < ruleNumbers.length; i++ ) {
            //rules[-ruleNumbers[ruleNumbers.length-i-1]] = tuples[i];
            //rules[-ruleNumbers[i]] = tuples[i];
        	rules[ruleNumbers[i]] = tuples[i];
        }
    }
    
    	
	private int[] whatToRecognize = new int[]{};
	/**
	 * Reconizer starts expecting whatToRecognize symbols
	 * Expand this list by one more symbol
	 * @param additionalSymbol
	 */
    public void addSymbol2Recognize( String additionalSymbol ) {
        addSymbol2Recognize(getSymbol(additionalSymbol));
    }
	public void addSymbol2Recognize( int additionalSymbol ) {
	    
	    int[] tmp = new int[whatToRecognize.length+1];
	    for( int i = 0; i < whatToRecognize.length; i++ ) {
	        if( whatToRecognize[i] == additionalSymbol )
	            return; // don't add symbol which is already listed
	        tmp[i] = whatToRecognize[i];	        
	    }
	    tmp[whatToRecognize.length] = additionalSymbol;
	    whatToRecognize = tmp;
	}
	
    @Override
    protected void initCell00(List<LexerToken> src, Matrix matrix) {
        long t1 = 0;
        if( matrix.visual != null )
            t1 = System.nanoTime();
        matrix.initCells(src.size());
        initCell(matrix, whatToRecognize,0);
        if( matrix.visual != null ) {
            long t2 = System.nanoTime();
            matrix.visual.visited[0][0] += (int)(t2-t1);
        }
        LexerToken LAtoken = src.get(0);
        matrix.LAsuspect = symbolIndexes.get("'" + LAtoken.content.toUpperCase() + "'");

    }
    
    boolean oldSkipRanges = skipRanges;
    @Override 
	protected boolean scan( Matrix matrix, List<LexerToken> src ) {	
        int y = matrix.lastY();
        if( src.size() <= y  ) {
            return false;
        }

        LexerToken token = src.get(y);
        String tokUpper = token.content.toUpperCase();
        
        // ultraambiguous @ sqlplus command
        if( "@".equals(tokUpper) ) {
            oldSkipRanges = skipRanges;
            skipRanges = false;
        }
        if( skipRanges != oldSkipRanges && 
                ( ";".equals(tokUpper) || ",".equals(tokUpper) ) 
        )
            skipRanges = oldSkipRanges;
        
        Integer suspect = symbolIndexes.get("'" + tokUpper + "'");
        matrix.LAsuspect = null;
        if( y+1 < src.size() ) {
            LexerToken LAtoken = src.get(y+1);
            matrix.LAsuspect = symbolIndexes.get("'" + LAtoken.content.toUpperCase() + "'");
        }

        boolean ret = false;
        for( int i = matrix.allXs.length-1; 0 <= i; i-- ) {
            int x = matrix.allXs[i];
            if( scan(matrix, y, src, x, suspect) )
                ret = true;
        }
        if( scan(matrix, y, src, y, suspect) )
            ret = true;

        return ret;		
	}

    
    // with lookahead to prune spurious rules quickly
    private boolean scan( Matrix matrix, int y, List<LexerToken> src, int x, Integer suspect ) {
        long t1 = 0;
        if( matrix.visual != null )
            t1 = System.nanoTime();

		long[] content = null;
		Cell candidateRules = matrix.get(x,y);
		if( candidateRules == null )
			return false;
		for( int j = 0; j < candidateRules.size(); j++ ) {
			int pos = candidateRules.getPosition(j);
			int ruleNo = candidateRules.getRule(j);
			Tuple t = rules[ruleNo];
			
			if( t.size()-1 < pos )
				continue;
//if( 11<y && t.toString().startsWith("modify_virtcol_properties")  )
//System.out.println("t.toString()="+t.toString(pos));
			if( isScannedSymbol(y,src, pos, t, suspect) ) {
				if( !lookaheadOK(t,pos+1,matrix) )
					continue;
				final long cellElem = makeMatrixCellElem(ruleNo, pos+1,t);
                content = Array.insert(content,cellElem);
                if( t.rhs.length == pos+1 )
                    matrix.enqueue(Service.lPair(x, t.head));
                /*if( skipRanges && t.rhs.length-1 == pos )
                    for( int k = x+1; k < y; k++ ) {
                        matrix.allXs = Array.delete(matrix.allXs,k);
                    }*/
			}
			
		}
		if( content == null )
			return false;
		matrix.put(x, y+1, new EarleyCell(content));
		matrix.allXs = Array.insert(matrix.allXs,x);
        if( matrix.visual != null ) {
            long t2 = System.nanoTime();
            matrix.visual.visited[x][y+1] += (int)(t2-t1);
        }
		return true;
	}
        
    // Lookahead a keyword and dismiss mismatching tuples
	@Override
	protected boolean lookaheadOK( Tuple t, int pos, Matrix matrix ) {
        if( pos > t.size()-1  ) 
            return true;
        int nextInTuple = t.content(pos); 
        if( isTerminal(nextInTuple) )
            return matrix.LAsuspect!=null && nextInTuple==matrix.LAsuspect;
        PredictedTerminals terminal = terminalPredictions[nextInTuple];
        if( terminal != null )
            return terminal.matches(matrix.LAsuspect);
        return true;
	    /* 6 cases:
	     *                               |  !isTerminal(nextInTuple)  |  isTerminal(nextInTuple) 
	     *  -----------------------------+----------------------------+------------------------
	     *  matrix.LAsuspect == null     |           true             |        false
         *  isOper(supectedKW)           | true? investigate nextInTuple |   nextInTuple==matrix.LAsuspect    
         *  !isOper(supectedKW)          |           true?            |   nextInTuple==matrix.LAsuspect
	     */
        /*if( matrix.LAsuspect == null ) {  
            return !isTerminal(nextInTuple);
        } 
        if( !isTerminal(nextInTuple) ) {  
            PredictedTerminals terminal = terminalPredictions[nextInTuple];
            if( terminal != null )
                if( !terminal.matches(matrix.LAsuspect) )
                    return false;
            return true;
        } 
        return nextInTuple==matrix.LAsuspect;*/
        // was:
		//return matrix.LAsuspect == null || !isTerminal(nextInTuple) || nextInTuple==matrix.LAsuspect;
	}
	
    /*final static String operation = "(){}[]^-|!*+.><='\",;:%@?/\\#~";
	static private boolean isOper( String supectedKW ) {
	    return supectedKW.charAt(0)=='\'' && 0 <= operation.indexOf(supectedKW.charAt(1));
	}*/

	@Override
    protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
		if( symbol != identifier )
			return false;
		if( suspect != null && _keywords.contains(suspect) )
			return false;
		
		LexerToken token = src.get(y);
		if( token.type == Token.DQUOTED_STRING )
			return true;
		if( token.type != Token.IDENTIFIER )
			return false;
		if( suspect == null )
			return true;
			
		LexerToken prior = null;
		if( 0 < y )
			prior = src.get(y-1);
        // cumbersome:
		//return prior !=null && !"YEAR".equalsIgnoreCase(prior.content) && !"HOUR".equalsIgnoreCase(prior.content) && !"MINUTE".equalsIgnoreCase(prior.content) //$NON-NLS-1$
		    //|| !"TO".equalsIgnoreCase(token.content);  //$NON-NLS-1$
        if( "TO".equalsIgnoreCase(token.content) && prior != null 
           &&( "YEAR".equalsIgnoreCase(prior.content) || "HOUR".equalsIgnoreCase(prior.content) || "MINUTE".equalsIgnoreCase(prior.content)) )
            return false;  
        if( "BY".equalsIgnoreCase(token.content) && prior != null && "CONNECT".equalsIgnoreCase(prior.content) )
            return false;  
        return true;
    }
    
    @Override
    protected boolean notConfusedAsId( int suspect, int head, int pos ) {
        return (suspect != begin 
             || head != dotted_name && head != decl_id
             || pos != 0) 
        &&  
               (suspect != start
             || head != table_reference
             || pos != 1)                   
        &&
               (suspect != distinct
             || head != multiset_except
             || pos != 3)                   
        &&
               (suspect != body
             || head != pkg_spec
             || pos != 1)                   
        &&
               (suspect != as
             || head != table_reference
             || pos != 1)                   
        &&
               (suspect != ELSE
             || head != aliased_dml_table_expression_clause
             || pos != 1)                   
        ;
    }
    
    @Override
    public synchronized void parse( List<LexerToken> src, Matriceable matrix ) {
        //Parse(src,matrix);
        super.parse(src,matrix);
    }
        

    
    /**
     * @param src -- input after lexical analysis
     * @return how many tokens were recognized
     */
    public int recognize( List<LexerToken> src ) {
        Matrix matrix = new Matrix(getInstance());
        parse(src, matrix);

        for( int i = src.size(); 0 < i; i--) {
            Cell top = matrix.get(0, i);
            if( top != null ) 
            	return i;
		}
        throw new AssertionError("all empty cells?"); //$NON-NLS-1$
    }

    static public String[] keywords = {
        "'WITH'", //$NON-NLS-1$
        "'SELECT'", //$NON-NLS-1$
        "'FROM'", //$NON-NLS-1$
        "'WHERE'", //$NON-NLS-1$
        "'AND'", //$NON-NLS-1$
        "'OR'", //$NON-NLS-1$
        "'NOT'", //$NON-NLS-1$ 
        
        "'DISTINCT'", //$NON-NLS-1$ 
        "'UNION'", //$NON-NLS-1$ 
        "'ALL'", //$NON-NLS-1$ 
        "'INNER'", //$NON-NLS-1$ 
        //"'LEFT'", //$NON-NLS-1$ 
        "'NATURAL'", //$NON-NLS-1$ 
        "'FULL'", //$NON-NLS-1$ 
        //"'OUTER'", //$NON-NLS-1$ 
        //20881746  "'JOIN'", //$NON-NLS-1$ 
        "'ON'", //$NON-NLS-1$ 

        "'INSERT'", //$NON-NLS-1$ 
        "'UPDATE'", //$NON-NLS-1$ 
        
        "'CREATE'", //$NON-NLS-1$        
        "'ALTER'", //$NON-NLS-1$        
        "'TABLE'", //$NON-NLS-1$        
        //"'TABLESPACE'", //$NON-NLS-1$
        // with tabje as (
        //SELECT   t.TABLESPACE, t.totalspace AS total_mb,
        //...

        "'VALUES'", //$NON-NLS-1$ 
        
        "'NUMBER'", //$NON-NLS-1$        
        "'VARCHAR2'", //$NON-NLS-1$        
        "'DATE'", //$NON-NLS-1$        
        "'INTEGER'", //$NON-NLS-1$         

        "'MULTISET'", //$NON-NLS-1$         

        "'CASE'", //$NON-NLS-1$         
        "'WHEN'", //$NON-NLS-1$         
    };
	private Set<Integer> _keywords = new TreeSet<Integer>(); // pure Keywords
	void initKeywords() {
	    for( String k : keywords )
	        _keywords.add(getSymbol(k));
	}

    
    @Override
    public ParseNode treeForACell( List<LexerToken> src, Matrix m, Cell cell, int x, int y, Map<Long,ParseNode> explored ) {
        int rule = -1;
        int pos = -1;
        for( int i = 0; i < cell.size(); i++ ) {
            rule = cell.getRule(i);
            Tuple t = rules[rule];
            if( t.head != sql_statements && t.head != select )
                continue;
            pos = cell.getPosition(i);
            if( rules[rule].rhs.length == pos )
                return tree(src, m, x, y, rule,pos, explored);
        }
        return super.treeForACell(src, m, cell, x, y, explored);
    }
    public void parse( List<LexerToken> src, Matrix matrix, InterruptedException interrupted ) throws InterruptedException {
        //try {
            initCell00(src, matrix);
            predict(matrix);
            while( true ) {
                if( interrupted != null && Thread.interrupted() )
                    throw interrupted;
                //int before = matrix.size();
                if( !scan(matrix, src) )
                    break;
                complete(matrix, src.size());
                predict(matrix);
                //if( before == matrix.size() )
                //break;
            }
        //} catch( IndexoutOfBoundsException e ) {}
    }
    
        
}
