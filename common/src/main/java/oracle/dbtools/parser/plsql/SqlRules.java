/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.Grammar;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.RuleTransforms;
import oracle.dbtools.parser.RuleTuple;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.Visual;
import oracle.dbtools.parser.plsql.doc.HarvestDoc;
import oracle.dbtools.util.Service;

/** 
 * SQL grammar; 
   Walk through EBNF grammar website maintained by Diana Lorentz
        http://yourmachine.yourdomain/database/121/SQLRF/img_text
        http://yourmachine.yourdomain/sql_grammar/sqlbnf/bnffiles  -- WIP
        http://yourmachine.yourdomain/sql_grammar/sqlbnf112/bnffiles/  -- 11.2
   starting with "select" and descend down to define all the dependents.
   Transform each EBNF rule into a set of BNF rules.
   EBNF to BNF translator is built upon CYK parser engine.
 */
public class SqlRules {

    private static void testParseSqlBnf() throws Exception {
        final String input = 
            Service.readFile(SqlRules.class, "testsql.bnf") //$NON-NLS-1$
            ;      
        List<LexerToken> src =  LexerToken.parse(input);
        correctDots(src);
        Visual visual = null;
        visual = new Visual(src, earley);
        //LexerToken.print(src);
        Matrix matrix = new Matrix(earley);
        earley.parse(src, matrix); 
        ParseNode root = earley.forest(src, matrix);
        //cyk.print(matrix, 0, size);
        root.printTree();
        if( visual != null )
        	visual.draw(matrix);
        
        Set<RuleTuple> grammar = new TreeSet<RuleTuple>();
        String hdr = "test";
        //descend(root, grammar,hdr, src);
        grammar.addAll(bnf(root, src, hdr));
        System.out.println("-------------Optimized---------------"); // (authorized)                 //$NON-NLS-1$
        RuleTuple.printRules(grammar);
        System.out.println("-------------------------------------"); // (authorized) //$NON-NLS-1$       
    }

    private static char[] identifiers = new char[] {'(',')','\'',/*'.',*/',',';',':','=','+','-','*','/','@','!','^','~','e','f','d','?'};
    private static void bnfIdentifiers( Set<RuleTuple> rules ) {
        for( char id : identifiers )
            rules.add(new RuleTuple("identifier", new String[] {"'"+id+"'"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }
        
    private static Earley earley;
    static {
        try {
            earley = bnfParser();
            bnf = earley.symbolIndexes.get("bnf");
            rawbnf = earley.symbolIndexes.get("rawbnf");
            boldrawbnf = earley.symbolIndexes.get("boldrawbnf");
            identifier = earley.symbolIndexes.get("identifier");
            concat = earley.symbolIndexes.get("concat");
            block = earley.symbolIndexes.get("block");
            sqBr = earley.symbolIndexes.get("'['");
            curlyBr = earley.symbolIndexes.get("'{'");
            dot = earley.symbolIndexes.get("'.'");
            lt = earley.symbolIndexes.get("lt");
            gt = earley.symbolIndexes.get("gt");
            lbr = earley.symbolIndexes.get("lbr");
            rbr = earley.symbolIndexes.get("rbr");
            lcp = earley.symbolIndexes.get("lcp");
            rcp = earley.symbolIndexes.get("rcp");
            dbar = earley.symbolIndexes.get("dbar");
            sbar = earley.symbolIndexes.get("sbar");
        } catch( Exception e ) {
            throw new AssertionError(e);
        }
    }
    static int bnf;
    static int rawbnf;
    static int boldrawbnf;
    static int identifier;
    static int concat;
    static int block;
    static int sqBr;
    static int curlyBr;
    static int dot;
    static int lt;
    static int gt;
    static int lbr;
    static int rbr;
    static int lcp;
    static int rcp;
    static int dbar;
    static int sbar;
    
  
    /**
     * Init parser for BNF grammar (defined in DocBnf.grammar)
     * @return
     * @throws Exception
     */
    private static Earley bnfParser() throws Exception {
        Set<RuleTuple> rules = new TreeSet<RuleTuple>();
        
        String input = Service.readFile(SqlRules.class, "DocBnf.grammar"); //$NON-NLS-1$
        List<LexerToken> src = LexerToken.parse(input, false, LexerToken.QuotedStrings/*+SqlPlusComments*/); 
        //LexerToken.print(src);
        ParseNode root = Grammar.parseGrammarFile(src, input);
        Grammar.grammar(root, src, rules);
        //root.printTree();

        return new Earley(rules) {
            @Override
            protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
        		LexerToken token = src.get(y);
                return 
                      symbol == identifier && token.type == Token.IDENTIFIER 
                  ||  symbol == identifier && token.type == Token.DQUOTED_STRING
                ;
            }
        };
    }

    static Map<String,String> missingSymbols = new TreeMap<String,String>();
    static Set<RuleTuple> extractRules() throws Exception {
        Set<RuleTuple> sqlRules = new TreeSet<RuleTuple>();
              
        //missingSymbols.put("#", "'#'"); //$NON-NLS-1$ //$NON-NLS-2$

        missingSymbols.put("access_driver_type", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("admin_user_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("alias", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("argument", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("attribute", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("attr_dim", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("c_alias", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("c_alias", new String[] {"'AS'","identifier"})); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("category", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("char1", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("char2", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("character_set", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("charset", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("child_level", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("child_key_column", "column"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("cluster", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("class", "literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("class_value", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("cluster", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("cluster_id", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("cluster_id", new String[] {"identifier","'.'","identifier"})); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("cost_value", "literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("collection_item", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("collation_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        //sqlRules.add(new RuleTuple("column", "identifier"); // there is a rule already!
        missingSymbols.put("column_alias", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("column_collation_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("column_expression", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("column_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("comparison_expr", "expr"); // in simple_case_expression //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("constant", "literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("constraint_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("container_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("container_data_object", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("cpu_cost", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        //missingSymbols.put("constraint", "identifier"); // there is actually a rule with that name //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("create_table_statement", "create_table"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("create_view_statement", "create_view"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("data_item", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("data_item", new String[] {"identifier","'.'","data_item"})); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("data_item", new String[] {"':'","identifier"})); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("database", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("date", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("db_user_proxy", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("default_selectivity", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("dependent_column", "column"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("dimension", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("dimension_column", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("directory_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("directory_object_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("disk_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("diskgroup_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("domain", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("edition_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("else_expr", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("element", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$        
        missingSymbols.put("end_time_column", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("esc_char", "string_literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("expr1", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("expr2", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("expr3", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("failgroup_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("filename", "string_literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("file_number", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("file_number", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("flashback_archive", "identifier");         //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("fmt", "string_literal");         //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("fractional_second_precision", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("function_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("grant_statement", "grant"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("hash_partition_quantity", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("hash_subpartition_quantity", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("hierarchy", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("HSM_auth_string", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        //sqlRules.add(new RuleTuple("function", "identifier");  !there is a rule with that name
        missingSymbols.put("id", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("implementation_type", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("index", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("indextype", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("integer", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("io_cost", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("java_ext_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("JSON_object_key", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("JSON_column", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("JSON_path_expression", "string_literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("JSON_basic_path_expression", "string_literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("keystore_password", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("keystore2_password", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("keystore3_password", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("leading_field_precision", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("len", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("level", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("level_table", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("level_column", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("lib_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("library_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("LOB_item", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("LOB_segname", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("log_group", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("main_model_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("materialized_view", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("match_string", "string_literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("measure_column", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("method", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("mining_model_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("model", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("n", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("namespace", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("nested_item", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("nested_table", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("nested_table1", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("nested_table2", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("network_cost", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("new_keystore_password", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("new_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("new_table_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        //number is defined as numeric_literal in SqlFixes    missingSymbols.put("number", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("object", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("object_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("object_step", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("object_privilege", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("object_table_alias", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("old_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("old_password", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("old_keystore_password", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("offset", "digits");         //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("operator", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("ordering_column", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("outline", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("package", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("package_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("parameter", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("parameter_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("parameter_type", "prm_spec_unconstrained_type"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("parameter_value", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("parent_level", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("partition", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("partition_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("partition_name_old", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("password", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("pattern", "string_literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("pdb_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("percent", "numeric_literal");         //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("policy", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("position", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("precision", new String[] {"digits"})); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("precision", new String[] {"'*'"})); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("primary_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("procedure_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("procedure", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("profile", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("query_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("reference_spreadsheet_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("restore_point", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("return_expr", "expr"); // in simple_case_expression //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("return_type", "func_return_prm_spec_unconstrained_type"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("role", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("role_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("rollback_segment", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("row_pattern_measure_column", "column"); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("rowcount", new String[] {"digits"})); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("rowcount", new String[] {"identifier"})); //$NON-NLS-1$ //$NON-NLS-2$
        //missingSymbols.put("sample_percent", "digits");         //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("scale", new String[] {"digits"})); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("scale", new String[] {"'-'","digits"})); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("schema", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("schema_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("scope_table", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("search_string", "string_literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("secret", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        //missingSymbols.put("seed_value", "digits");         //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("sequence", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("server_file_name", "string_literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("shardspace", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("size", "digits"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("software_keystore_password", "identifier");       //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("source_outline", "identifier");       //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("src_pdb_name", "identifier");       //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("statement", "sql_stmt");       //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("start_time_column", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("storage_table", "identifier");       //$NON-NLS-1$ //$NON-NLS-2$
        //missingSymbols.put("string", "string_literal");       //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("subpartition", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("synonym", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("t_alias", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("table", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("table_alias", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("tablespace", "identifier");                //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("tablespace_set", "identifier");                //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("template_name", "identifier");                //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("topN", "digits");                //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("trigger_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("type_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("type", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("user", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("user_defined_types", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("user_defined_types", new String[] {"identifier","'.'","identifier"})); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("user_defined_types", new String[] {"'REF'","identifier"})); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("value_expr", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("value_expression", "expr"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("view", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("valid_time_column", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("variable_expression", "bind_var"); //$NON-NLS-1$ //$NON-NLS-2$ 
        missingSymbols.put("variable_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("varray_item", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("varray_type", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("xml_encoding_spec", "string_literal"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("XMLSchema_URL", "string"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("XQuery_string", "string"); //$NON-NLS-1$ //$NON-NLS-2$
        missingSymbols.put("zonemap_name", "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        
        sqlRules.add(new RuleTuple("literal", new String[] {"string_literal"})); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("literal", new String[] {"number"})); //$NON-NLS-1$ //$NON-NLS-2$
        //Too broad: sqlRules.add(new RuleTuple("literal", new String[] {"expr"})); //e.g. TO_DATE('01-APR-1998','DD-MON-YYYY') //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("datetime_literal", new String[] {"'TO_DATE'","'('","string_literal","','","string_literal","')'"})); //$NON-NLS-1$ //$NON-NLS-2$
        sqlRules.add(new RuleTuple("literal", new String[] {"datetime_literal"})); //$NON-NLS-1$ //$NON-NLS-2$
        
        sqlRules.add(new RuleTuple("not_eq", new String[] {"'<'","'>'"})); //$NON-NLS-1$ //$NON-NLS-2$
        
        sqlRules.add(new RuleTuple("character_datatypes", new String[] {"'VARCHAR2'"})); //$NON-NLS-1$ //$NON-NLS-2$

        addToDo("commit");  //$NON-NLS-1$

        addToDo("sql_statements");  //$NON-NLS-1$
        
        while( 0 < toDo.size() ) {
        	String candidate = nextToDo();
        	recursiveCollectBNF(sqlRules,candidate);
        	toDo.remove(candidate);
        }
                
        for( String key : notFound )
            if( !missingSymbols.containsKey(key) )
                if( key.contains("name") ) 
                    missingSymbols.put(key, "identifier"); //$NON-NLS-1$ //$NON-NLS-2$
        
        for( String key : missingSymbols.keySet() ) {
            sqlRules.add(new RuleTuple(key, new String[] {missingSymbols.get(key)})); 
            notFound.remove(key);
        }
        
        sqlRules.addAll(Grammar.extractRules(SqlEarley.class, "json.ebnf"));
        
        //RuleTransforms.printSelectedRules("measure_column",sqlRules);  //$NON-NLS-1$
        
        RuleTransforms.eliminateEmptyProductions(sqlRules);
        RuleTransforms.injectMissingEmptyProductions(sqlRules);
        RuleTransforms.substituteSingleBinaryProductions(sqlRules); 
        RuleTransforms.substituteSingleUnaryProductions(sqlRules); 
        
        straightenSemicolon(sqlRules);
        //Set<String> dot = new HashSet<String>();
        //dot.add("'.'");
        //RuleTransforms.substituteSymbols("<b>.</b>", dot, sqlRules); //$NON-NLS-1$ //$NON-NLS-2$
        
        //RuleTransforms.printSelectedRules("create",sqlRules);  //$NON-NLS-1$
        
        System.out.println("*************NOT FOUND************"); // (authorized)
        System.out.println(notFound); // (authorized)
        return sqlRules;
    }

    private static void straightenSemicolon( Set<RuleTuple> sqlRules ) {
        String[] heads = new String[]{"alter_session","set_transaction",};
        for( String head : heads )
            straightenSemicolon(sqlRules, head);
        
    }
    
    /**
     * Optional trailing semicolon; add rules without semi
     * @param sqlRules
     * @param head
     */
    private static void straightenSemicolon( Set<RuleTuple> sqlRules, String head ) {
        Set<RuleTuple> rules = new TreeSet<RuleTuple>(); 
        for( RuleTuple rule : sqlRules )
            if( head.equals(rule.head) ) {
                if( !"';'".equals(rule.rhs[rule.rhs.length-1]) )
                    throw new AssertionError("!\"';'\".equals(rule.rhs[rule.rhs.length-1])");
                String[] rhs = new String[rule.rhs.length-1];
                for( int i = 0; i < rhs.length; i++) 
                    rhs[i] = rule.rhs[i];
                RuleTuple modifiedRule = new RuleTuple(head, rhs); 
                rules.add(modifiedRule);
            }
        sqlRules.addAll(rules);
    }

    private static final String path = "/oracle/dbtools/parser/plsql/"; //$NON-NLS-1$
    public static void memorizeRules() throws Exception {
    	memorizeRules("isqlBNF.serial");
    }
    public static Set<RuleTuple> getRules() throws Exception {
    	return getRules("sqlBNF.serial");
    }
    private static void memorizeRules( String fname ) throws Exception {
        Set<RuleTuple> rules = extractRules();  
        FileOutputStream fos = new FileOutputStream("src"+path+fname); //$NON-NLS-1$
        ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(rules);
        out.close();
    }
    private static Set<RuleTuple> getRules( String fname ) throws Exception {
        URL u = SqlRules.class.getResource( path+fname );
        if( u == null ) 
           	u = Service.fileNameGitResource(HarvestDoc.class, path+fname);            
        InputStream is = u.openStream();
        ObjectInputStream in = new ObjectInputStream(is);
        Set<RuleTuple> rules = (Set<RuleTuple>) in.readObject();
        in.close();
        return rules;
    }

    private static Set<String> toDo = new HashSet<String>();
    private static void addToDo( String s ) {
    	if( s.charAt(0)!='\'' && !notFound.contains(s) && !masterRules.contains(s) )
    		toDo.add(s);
    }
    private static String nextToDo() {
    	for( String candidate : toDo ) 
    		return candidate;
    	throw new AssertionError("Empty toDo");
    }
    private static Set<String> notFound = new HashSet<String>();
    private static Set<String> masterRules = new HashSet<String>();
    private static void recursiveCollectBNF( Set<RuleTuple> output, String masterRuleName ) throws Exception {
        if( masterRules.contains(masterRuleName) )
            return;        
        if( notFound.contains(masterRuleName) )
            return;
        
        System.out.println("searching for...  "+masterRuleName);
        	
        if( "hint".equals(masterRuleName) )   //$NON-NLS-1$
            return;
        if( "array_step".equals(masterRuleName) )   //$NON-NLS-1$
            return;
        if( !fetchFromFixes(output, masterRuleName) )
        	fetchFromDocWebsite(output, masterRuleName);    
    }

	final static String masterBNFurl = "https://docs.oracle.com/en/database/oracle/oracle-database/12.2/sqlrf/";
    /**
     * @param output
     * @param masterRuleName
     * @throws Exception
     */
    private static void fetchFromDocWebsite( Set<RuleTuple> output, String masterRuleName ) throws Exception {
    		
        final String sqlBNFfiles = masterBNFurl+"/img_text";            //$NON-NLS-1$
        String masterrulename = masterRuleName.toLowerCase();
        
    	//if( "XQuery_string".equalsIgnoreCase(masterRuleName) )
    		//masterrulename = masterRuleName.toLowerCase();
    	
        if( masterrulename.contains("grouping_hint") )
        	return;
        
        String input = readURL(masterrulename, sqlBNFfiles);
        if( input == null ) {
        	if( notFound.contains(masterrulename) )
        		return;
        	input = readURL(masterrulename, sqlBNFfiles);
        	if( input == null )
        		System.exit(1);
        }
        input = input.replace("<span class=\"bold\">", "<b>");
        input = input.replace("</span>", "</b>");
        
        int ipre = input.indexOf("<pre"); 
        int jpre = input.indexOf("</pre>");
        input = input.substring(ipre, jpre+"</pre>".length());

        input = input.replace("<pre class=\"oac_no_warn\" dir=\"ltr\">", "<pre>");
        
        while( true ) {
            int iQuote = input.indexOf("'");
            if( iQuote < 0 )
                break;
            String literal = input.substring(iQuote, input.indexOf("'",iQuote+1)+1);
            input = input.replace(literal, "string_literal");
        }

        
        List<LexerToken> src = LexerToken.parse(input,false,/*QuotedStrings+*/LexerToken.SqlPlusComments);
        correctDots(src);

        System.out.println(masterRuleName+"...");
        Matrix matrix = new Matrix(earley);
        earley.parse(src, matrix); 
        ParseNode root = earley.forest(src, matrix);
        
        if( !root.contains(earley.symbolIndexes.get("bnf")) ) {//$NON-NLS-1$ 
            System.err.println("failed to parse >>>"+masterRuleName+"<<< to bnf");  //$NON-NLS-1$ //$NON-NLS-2$
            System.exit(0);
        }
        output.addAll(bnf(root, src, masterRuleName));
    }

	public static String readURL(String masterRuleName, final String sqlBNFfiles ) {
		final String htm = "html";
		try {
            return readURL(sqlBNFfiles+"/"+masterRuleName + "."+htm); //$NON-NLS-1$ //$NON-NLS-2$
        } catch( Exception e ) { 
        	if( e.getClass().getPackage().getName().contains("java.net") ) {
        		System.err.println("Failed to connect to "+e.getMessage());
        		return null;
        	} else
        		try { 
        			return readURL(sqlBNFfiles+"/"+masterRuleName + "s."+htm); //$NON-NLS-1$ //$NON-NLS-2$
        		} catch( Exception ee ) { 
        			if( e.getClass().getPackage().getName().contains("java.net") ) {
                		System.err.println("Failed to connect to "+e.getMessage());
                		return null;
                	}        			
        			notFound.add(masterRuleName);
        			return null;
        		}
        }
	}

    private static boolean contains(Set<RuleTuple> ret, String masterRuleName) {
        for( RuleTuple rule : ret )
            if( rule.head.equals(masterRuleName) )
                return true;
        return false;
    }

    /**
     * Doc grammar contains lots of errors; process [parsed] SqlFixes.bnf first
     * @param src
     * @param input
     * @return
     * @throws Exception
     */
    private static ParseNode parseFixesFile( List<LexerToken> src, String input ) throws Exception {
        correctDots(src);
        //LexerToken.print(src);
        Matrix matrix = new Matrix(earley);
        earley.parse(src, matrix); 
        
        SyntaxError s = SyntaxError.checkSyntax(input, new String[]{"bnflist"}, src, earley, matrix);
        if( s != null ) { //$NON-NLS-1$ //$NON-NLS-2$
        	//if( visual != null  )
        		//visual.draw(matrix);
            System.out.println("Syntax Error");
            System.out.println("at line#"+s.line);
            System.out.println(s.code);
            System.out.println(s.marker);
            System.out.println("Expected:  ");
                for( String tmp : s.getSuggestions() )
                    System.out.print(tmp+',');
            throw new Exception(">>>> Parse error in SqlFixes.bnf <<<<"); //$NON-NLS-1$
        }
        
        ParseNode root = earley.forest(src, matrix);
        //root.printTree();
        return root;
    }
    
	private static ParseNode fixesParseTree = null;
	private static List<LexerToken> fixesScan = null;
    private static boolean fetchFromFixes( Set<RuleTuple> output, String masterRuleName ) throws Exception {
    	if( fixesParseTree == null || fixesScan == null ) { // do it once only
    		String input = Service.readFile(SqlRules.class,"SqlFixes.bnf");  //$NON-NLS-1$  
            input = input.replace("<span class=\"bold\">", "<b>");
            input = input.replace("</span>", "</b>");
    		//fixesScan = LexerToken.parse(input,false,false); // deemphasizing some symbols e.g. "expr_list" to fix optimization 
    		fixesScan = LexerToken.parse(input);//i.e.(input,false,true);
    		fixesParseTree = parseFixesFile(fixesScan,input);
            bnflist(fixesParseTree, fixesScan, output);
    	}
        return masterRules.contains(masterRuleName);
    }
    

    private static void correctDots( List<LexerToken> src ) {
        int i = -1;
        for( LexerToken t : src ) {
            i++;
            if( ".".equals(t.content)  //$NON-NLS-1$
            && (i == 0 || !".".equals(src.get(i-1).content))   //$NON-NLS-1$
            && (i == src.size()-1 || !".".equals(src.get(i+1).content))   //$NON-NLS-1$
            ) {
               t.content = "'.'"; //$NON-NLS-1$
               t.type = Token.IDENTIFIER;
            }
               
        }
    }
   
    private static void bnflist( ParseNode root, List<LexerToken> src, Set<RuleTuple> grammar ) {
        if( root.contains(bnf) ) 
            grammar.addAll(bnf(root, src, null)); 
        else for( ParseNode child: root.children() ) 
        	bnflist(child, src, grammar);            
    }
    
    private static ParseNode bnfRoot = null;
    private static Set<RuleTuple> bnf( ParseNode root, List<LexerToken> src, String header ) {
    	bnfRoot = root;
    	Set<RuleTuple> grammar = new TreeSet<RuleTuple>();
    	for( ParseNode child : root.children() )
            if( header == null && child.contains(identifier) ) {
            	header = child.content(src);            	
            } else if( child.contains(rawbnf) ) {
            	grammar.addAll(rawbnf(child, src, header, false));            	
            }
    	RuleTransforms.eliminateEmptyProductions(grammar);
    	//RuleTransforms.substituteAll(output); 
        RuleTransforms.substituteSingleUnaryProductions(grammar); 
    	RuleTransforms.substituteSingleBinaryProductions(grammar);
    	masterRules.add(header);
    	return grammar;
    }
    
    private static Set<RuleTuple> rawbnf( ParseNode root, List<LexerToken> src, String header, final boolean isBold ) {
        if( root.contains(concat) ) 
        	return concat(root, src, header, isBold); 
        else {
        	Set<RuleTuple> ret =  new TreeSet<RuleTuple>();
        	for( ParseNode child: root.children() ) {
        		if( child.contains(identifier) && child.from+1 == child.to ) {
        			String id = processId(child, src, isBold);
        			ret.add(new RuleTuple(header,new String[]{id}));
        			addToDo(id);
        		} else if( child.contains(rawbnf) || child.contains(concat) )
        			ret.addAll(rawbnf(child, src, header, isBold));
        	}
        	return ret;
        }
    }    
     
    /**
     * Descend and process rule concatenation operator
     * @param root
     * @param src
     * @param header
     * @param isBold
     * @return
     */
    private static Set<RuleTuple> concat( ParseNode root, List<LexerToken> src, String header, final boolean isBold ) {
        if( root.contains(block) ) 
        	return block(root, src, header,isBold);         
        else { 
            List<String> payload =  new LinkedList<String>();
            Set<RuleTuple> ret = new TreeSet<RuleTuple>();
        	for( ParseNode child: root.children() )
        		if( child.contains(identifier) && child.from+1 == child.to ) {
        			String id = processId(child, src, isBold);
        			payload.add(id);
        			addToDo(id);
        		} else if( child.contains(concat) ) {
          			int intervalPos = header.indexOf('[');
          			String childInterval = "["+(child.from-bnfRoot.from)+","+(child.to-bnfRoot.from)+")";
        			String childHdr = (0 < intervalPos ? header.substring(0,intervalPos) : header)+childInterval;
					Set<RuleTuple> childRules = concat(child, src, childHdr, isBold);
					Set<RuleTuple> major = new TreeSet<RuleTuple>();
					for( RuleTuple candidate : childRules )
						if( candidate.head.equals(childHdr) ) {
							major.add(candidate);
						}
					if( major.size() == 1 ) {
					    RuleTuple singleton = null;
					    for( RuleTuple r : major )
					        singleton = r;
					    childRules.remove(singleton);
					    ret.addAll( childRules );
					    for( String rhs : singleton.rhs )
					        payload.add(rhs);					    
					} else {
                        ret.addAll( childRules );
                        payload.add(childHdr);
					}
        		} else if( child.contains(block) ) {
          			int intervalPos = header.indexOf('[');
          			String childInterval = "["+(child.from-bnfRoot.from)+","+(child.to-bnfRoot.from)+")";
        			String childHdr = (0 < intervalPos ? header.substring(0,intervalPos) : header)+childInterval;
					ret.addAll( block(child, src, childHdr, isBold) );
					payload.add(childHdr);
        		}
            RuleTuple ct = new RuleTuple(header,payload);
            ret.add(ct);
            return ret;
        }
	}

	private static Set<RuleTuple> block( ParseNode root, List<LexerToken> src, String header, final boolean isBold ) {
		if( root.contains(boldrawbnf) )
			return boldrawbnf(root,src,header);
		else if( root.contains(identifier) ) {
			String id = processId(root, src, isBold);
			addToDo(id);
            RuleTuple ct = new RuleTuple(header,new String[]{id});
            Set<RuleTuple> ret = new TreeSet<RuleTuple>();
            ret.add(ct);
			return ret;
		} else {
            Set<RuleTuple> ret = new TreeSet<RuleTuple>();
			for( ParseNode child: root.children() ) {
                RuleTuple empty = new RuleTuple(header, new String[]{});
                if( child.contains(sqBr) ) 
					ret.add(empty);
				else if( child.contains(rawbnf) ) 
					ret.addAll(rawbnf(child,src,header,/*false*/isBold));
				else if( child.contains(dot) ) {
				    //boolean optional = ret.contains(empty);
				    for( RuleTuple t : ret )
				        if( t.head.equals(header) && 0 < t.rhs.length )
				            t.head = header+"#";
                    ret.add(new RuleTuple(header, new String[]{header+"#"}));
				    ret.add(new RuleTuple(header, new String[]{header,header+"#"}));
				    break;
				}
            }
			return ret;
		}
	}

    private static String processId(ParseNode root, List<LexerToken> src, final boolean isBold) {
        String id = root.content(src);
        boolean isPunctuation = false;
        if( id.length() == 1 ) {
            for( char bnfId : identifiers )
                if( id.charAt(0) == bnfId )
                	isPunctuation = true;
        }
        if( "lt".equals(id) ) 
            id="'<'";
        else if( "gt".equals(id) ) 
            id="'>'";
        else if( "lbr".equals(id) ) 
            id="'['";
        else if( "rbr".equals(id) ) 
            id="']'";
        else if( "lcp".equals(id) ) 
            id="'{'";
        else if( "rcp".equals(id) ) 
            id="'}'";
        else if( "dbar".equals(id) ) 
            id="'||'";
        else if( "sbar".equals(id) ) 
            id="'|'";
        else if( isBold || isPunctuation )
        	id = "'"+id.toUpperCase()+"'";
        return id;
    }

/**
 * Descend to parse node which is keyword
 * @param root
 * @param src
 * @param header
 * @return
 */
	private static Set<RuleTuple> boldrawbnf( ParseNode root, List<LexerToken> src, String header ) {
		//if( header.equals("create_plsql[138,146)"))
			//header = "create_plsql[138,146)";
        Set<RuleTuple> ret = new TreeSet<RuleTuple>();
       	for( ParseNode child: root.children() )
    		if( child.contains(rawbnf) ) 
    			ret.addAll(rawbnf(child,src,header,true));
    		else if( child.contains(lt) ) 
                ret.add(new RuleTuple(header, new String[]{"'<'"}));
    		else if( child.contains(gt) ) 
                ret.add(new RuleTuple(header, new String[]{"'>'"}));
    		else if( child.contains(lbr) ) 
                ret.add(new RuleTuple(header, new String[]{"'['"}));
    		else if( child.contains(rbr) ) 
                ret.add(new RuleTuple(header, new String[]{"']'"}));
            else if( child.contains(lcp) ) 
                ret.add(new RuleTuple(header, new String[]{"'{'"}));
            else if( child.contains(rcp) ) 
                ret.add(new RuleTuple(header, new String[]{"'}'"}));
    		else if( child.contains(dbar) ) 
                ret.add(new RuleTuple(header, new String[]{"'|'","'|'"}));
            else if( child.contains(sbar) ) 
                ret.add(new RuleTuple(header, new String[]{"'|'",}));
       	if( ret.size() == 0 ) 
       		throw new AssertionError("unexpected ret for "+header);
       	else
       		return ret;
	}



    static String readURL( final String masterBNFurl ) throws Exception {
        byte[] bytes = new byte[4096]; 
        int bytesRead = 0; 
        URL url = new URL( masterBNFurl/*.toLowerCase()*/ ); 
        BufferedInputStream bin = new BufferedInputStream( url.openStream() ); 
        StringBuffer out = new StringBuffer();
        bytesRead = bin.read( bytes, 0, bytes.length ); 
        StringBuffer sb = new StringBuffer();
        while ( bytesRead != -1 ){ 
            sb.append(new String(bytes).substring(0,bytesRead));
            bytesRead = bin.read( bytes, 0, bytes.length ); 
        }
        String ret = sb.toString();
        
        int ind = ret.indexOf("<p>Note:"); //$NON-NLS-1$
        if( ind > 0 )
            ret = ret.substring(0, ind);
        ind = ret.indexOf("Note:"); //$NON-NLS-1$
        if( ind > 0 )
            ret = ret.substring(0, ind);
        ind = ret.indexOf("</pre>"); //$NON-NLS-1$
        if( ind < 0 )
            ind = ret.indexOf("</PRE>"); //$NON-NLS-1$
        if( ind < 0 )
            ret = ret + "</pre>"; //$NON-NLS-1$
        
        return ret;
    }

    public static void main( String[] args ) throws Exception {
	    boolean test = true;
	    //test = false;
        if( test )
        	testParseSqlBnf();
        else {
            String page = readURL(masterBNFurl+"Calculated-Measure-Expressions.htm");
			//System.out.println(page);
			String str = "<p class=\"subhead2\"><span class=\"italic\">";
			for( int pos = page.indexOf(str); 0 < pos; pos = page.indexOf(str, pos+str.length()) ) {
				int end = page.indexOf("</span>::=</p>", pos+str.length());
				int end1 = page.indexOf("</span> ::=</p>", pos+str.length());
				if( 0 < end1 && end1 < end ) {					
					end = end1;
				}
				if( end < 0 ) 					
					break;
				if( pos+100 < end )
					continue;
				String rule = page.substring(pos+str.length(), end);
				//System.out.println(rule);
				int hrefPos = page.indexOf("<a href=\"", end);
				int hrefEnd = page.indexOf("\">", hrefPos);
				String guid = page.substring(hrefPos+"<a href=\"".length(), hrefEnd);
				//System.out.println(guid);
				String ruleBNF = readURL(masterBNFurl+guid);
				String ruleTag = "<pre class=\"oac_no_warn\" dir=\"ltr\">";
				int from = ruleBNF.indexOf(ruleTag);
				int to = ruleBNF.indexOf("</pre>",from);
				ruleBNF = ruleBNF.substring(from+ruleTag.length(), to);
				ruleBNF = ruleBNF.replace("<span class=\"bold\">", "<b>");
				ruleBNF = ruleBNF.replace("</span>", "</b>");
		        System.out.println("<pre "+rule+">");
		        System.out.println(ruleBNF);
		        System.out.println("</pre>");
		        System.out.println();
		        System.out.println();
			}
        }
    }

}
