/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parseable;
import oracle.dbtools.parser.Token;
import oracle.dbtools.util.Service;


/**
 * Latest revision of Stack-based parser -- "simpler" and more performant than CYK and Earley. 
 * A theory behind is Dyck language 
 * (or in lay terms - language of parenthesis:-)
 * 
 * History: QuickParser was the earlier prototype, 
 * followed by worksheet.folding.BlockNode
 * @author vtropash
 */
public class StackParser implements Parseable {

    private static StackParser instance = null;
    public static StackParser getInstance() {
        if( instance == null )
            instance = new StackParser();     
        return instance;
    }
    
	/**
	 * Major function
	 * @param src -- output of lexical analysis
	 * @return root of the tree
	 */
    public LazyNode parse( List<LexerToken> src ) {
        LinkedList<LazyNode> stack = new LinkedList<LazyNode>(); 
        LazyNode pseudoRoot = new LazyNode(0,"pseudo root",src); //$NON-NLS-1$
        pseudoRoot.to = src.size();
        stack.add(pseudoRoot);

        LexerToken prior = null;
        LexerToken prior2 = null;
        LexerToken current = null;
        int pos = -1;
        for( LexerToken next : src ) {
//careful, two stmts below: 
//System.out.print(pos+" level="+stack.size()+"       ");if(current!=null)current.print();

            if( stack.size()==0 )
                break;
               
            if( current != null ) {
            	String token = current.content;
            	if( token == null ) // 9193285 
            		break;
            	LazyNode n = oneIter(src, stack, prior, prior2, next, pos, token); 
            	if( n != null )
            		return n;
            }
            //System.out.println(stack);
            prior2 = prior;
            prior = current;
            current = next;
            pos++;
        }
        if( current != null ) {
        	String token = current.content;
        	if( token != null && stack.size()!=0 ) {// 9193285
        		LazyNode n = oneIter(src, stack, prior, prior2, null, pos, token); 
        		if( n != null )
        			return n;
        	}
        }
        
        
        Map<LazyNode,LazyNode> declare2plsql = new HashMap<LazyNode,LazyNode>();
    	LazyNode create = null;
    	int end = -1;
        for( LazyNode child : pseudoRoot.shallowChildren() ) {
        	end = child.to;
            if( "create".equalsIgnoreCase(child.startToken) ) {
            	create = child;
            	continue;
            }
            if( create != null && child.isCompilationUnit() ) {
            	declare2plsql.put(create, child);
            	create = null;
            	continue;
            }
        }
        for( LazyNode declair : declare2plsql.keySet() ) {
        	LazyNode plsql = declare2plsql.get(declair);
			declair.addTopLevel(plsql);
        	declair.to = plsql.to; 
        	pseudoRoot.topLevel.remove(plsql);
        }
        if( pseudoRoot.shallowChildren().size() == 0 && pseudoRoot.from < src.size() ) {
            pseudoRoot.startToken = src.get(pseudoRoot.from).content;
        }
        if( pseudoRoot.shallowChildren().size() == 1 ) {
            for( LazyNode child : pseudoRoot.shallowChildren() ) {
                if( child.from == pseudoRoot.from && child.to == pseudoRoot.to )
                    pseudoRoot = child;
            }
        }
        if( -1 < end && end < pseudoRoot.to ) { // add incomplete fragment
        	//List<LexerToken> remainder = src.subList(end,src.size());
            String label = src.get(end).content; //remainder.get(0).content
        	LazyNode more = new LazyNode(end,label,src/*remainder*/);
        	more.to = src.size();
        	if( more.isDML(null) ) {
        		pseudoRoot.addTopLevel(more);
        	}
        }

        return pseudoRoot;
    }


	private static LazyNode oneIter(List<LexerToken> src,
			LinkedList<LazyNode> stack, LexerToken prior, LexerToken prior2, LexerToken next,
			int pos, String token ) {
        LazyNode candidate = new LazyNode(pos,token,src);
        LazyNode top = stack.getLast();
        if( top == null )
            return new LazyNode(pos,"top==null",src); //$NON-NLS-1$            
        String nextStr = next == null? null : next.content;
        String nextTop = null;
		// There are 4 alternatives: 
		// 1. identify candidate to push onto stack
		// 1a. pop stack because of false positive ( "select ... for update" )
		// 2. replace top of the stack with candidate
		// 3. push candidate to stack
		if( 
		           candidate.isDDL(nextStr) && (prior==null || ";".equals(prior.content) || "/".equals(prior.content))
		                             && !top.isDDL(nextTop)
		        || candidate.isAs() && top.isCompilationUnit() && !"external".equalsIgnoreCase(nextStr)   //$NON-NLS-1$
		        || candidate.isCompilationUnit() && !top.isDDL(nextTop) && !top.isDML(nextTop) && !"(".equals(top.startToken) && (prior==null || !".".equals(prior.content))
		        												 && (!candidate.isProcedure() || !"type".equals(top.startToken) || prior==null || !"member".equalsIgnoreCase(prior.content) && !"static".equalsIgnoreCase(prior.content))
		        || candidate.isDML(nextStr) && !top.isDML(nextTop) && !"(".equals(top.startToken) && !top.isDDL(nextTop) && !top.isAs()//<-trigger //$NON-NLS-1$
		                             && ( prior == null || !"with".equalsIgnoreCase(token) || (!"time".equalsIgnoreCase(prior.content) && !"timestamp".equalsIgnoreCase(prior.content)) )  //$NON-NLS-1$ //$NON-NLS-2$ 
		                             && ( prior == null || !"update".equalsIgnoreCase(token) || !"for".equalsIgnoreCase(prior.content) )  //$NON-NLS-1$ //$NON-NLS-2$ 
		                             //&& ( prior == null || prior2 == null || !"insert".equalsIgnoreCase(token) || !"instead".equalsIgnoreCase(prior2.content) || !"of".equalsIgnoreCase(prior.content) )  //$NON-NLS-1$ //$NON-NLS-2$ 
		        || "declare".equalsIgnoreCase(token) && !top.isDDL(nextTop) && !top.isAs() //<-trigger //$NON-NLS-1$
		        || "begin".equalsIgnoreCase(token) && !top.isAs() && !top.isDDL(nextTop) //$NON-NLS-1$
		        || "if".equalsIgnoreCase(token) && prior!=null && !"end".equalsIgnoreCase(prior.content) && !top.isDDL(nextTop) //$NON-NLS-1$ //$NON-NLS-2$
		        || "case".equalsIgnoreCase(token) && prior!=null && !"end".equalsIgnoreCase(prior.content) && !top.isDDL(nextTop) //$NON-NLS-1$ //$NON-NLS-2$
		        || "loop".equalsIgnoreCase(token) && !top.isDML(nextTop)  && !top.isAs() && !"(".equals(top.startToken) && prior!=null && !"end".equalsIgnoreCase(prior.content) && !top.isDDL(nextTop) //$NON-NLS-1$ //$NON-NLS-2$
		        || "type".equalsIgnoreCase(token) && top.isDDL(nextTop) && !"replace".equals(prior.content) && !"create".equals(prior.content)
		        || "for".equalsIgnoreCase(token) 
		          && (prior2==null || !"open".equalsIgnoreCase(prior2.content))  //$NON-NLS-1$
		          && !isPivot(top,src)
		          && !top.isDDL(nextTop) 
		          && !top.isAs() //<-trigger 
                  && !"ORDINALITY".equalsIgnoreCase(nextStr) 
		        || "while".equalsIgnoreCase(token) && !top.isDDL(nextTop) //$NON-NLS-1$
		        || "(".equals(token)  //$NON-NLS-1$
		) { 
		    stack.add(candidate); // push
		    
		} else if( 
		        "update".equalsIgnoreCase(token) && prior!=null && "for".equalsIgnoreCase(prior.content)
	               && "for".equalsIgnoreCase(top.startToken) 
        ) { 
		 top.to = pos;
//System.out.println("+level="+stack.size()+"       "+top.startToken+"..."+src.get(top.to-1).content);
		 stack.removeLast();  // pop
		 if( stack.size() == 0 )
		     return new LazyNode(0,"error",src); //$NON-NLS-1$
		 
		} else if( 
		           "begin".equalsIgnoreCase(token) && top.isAs() //$NON-NLS-1$
		        || candidate.isCompilationUnit() && "create".equalsIgnoreCase(top.startToken) //$NON-NLS-1$
		) { 
		    top.to = pos;
//System.out.println("+level="+stack.size()+"       "+top.startToken+"..."+src.get(top.to-1).content);
		    stack.removeLast();  // pop
		    if( stack.size() == 0 )
		        return new LazyNode(0,"error",src); //$NON-NLS-1$
		    stack.getLast().addTopLevel(top);
		    stack.add(candidate); // push
		    
		} else if( 
		        ";".equals(token) && top.isCompilationUnit() //$NON-NLS-1$
		        || ";".equals(token) && top.isDML(nextTop) //$NON-NLS-1$
		        || ";".equals(token) && top.isDDL(nextTop) //$NON-NLS-1$
		        || "/".equals(token) && top.isDDL(nextTop) //$NON-NLS-1$
		        || ";".equals(token) && "if".equalsIgnoreCase(top.startToken) && prior!=null && "if".equalsIgnoreCase(prior.content) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		        || ";".equals(token) && "for".equalsIgnoreCase(top.startToken) && prior!=null && "loop".equalsIgnoreCase(prior.content) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		        || ";".equals(token) && "for".equalsIgnoreCase(top.startToken) && prior2!=null && "loop".equalsIgnoreCase(prior2.content) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		        || ";".equals(token) && "while".equalsIgnoreCase(top.startToken) && prior!=null && "loop".equalsIgnoreCase(prior.content) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		        || ";".equals(token) && "while".equalsIgnoreCase(top.startToken) && prior2!=null && "loop".equalsIgnoreCase(prior2.content) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		        || ";".equals(token) && "declare".equalsIgnoreCase(top.startToken) && prior!=null && "end".equalsIgnoreCase(prior.content) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		        || ";".equals(token) && "declare".equalsIgnoreCase(top.startToken) && prior2!=null && "end".equalsIgnoreCase(prior2.content) //12344443 //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		        || "end".equalsIgnoreCase(token) && "case".equalsIgnoreCase(top.startToken) //$NON-NLS-1$ //$NON-NLS-2$
		        || "end".equalsIgnoreCase(token) && "begin".equalsIgnoreCase(top.startToken) //$NON-NLS-1$ //$NON-NLS-2$
		        || "end".equalsIgnoreCase(token) && "loop".equalsIgnoreCase(top.startToken) //$NON-NLS-1$ //$NON-NLS-2$
		        || "end".equalsIgnoreCase(token) && "type".equalsIgnoreCase(top.startToken) 
		        || "end".equalsIgnoreCase(token) && top.isAs() // end of package //$NON-NLS-1$
		        || ")".equals(token)  //$NON-NLS-1$
		        
		        /*PROCEDURE JAVA_XMLTRANSFORM (xml CLOB, xslt CLOB, output CLOB) AS LANGUAGE JAVA
		        NAME 'JavaXslt.XMLTtransform(oracle.sql.CLOB, oracle.sql.CLOB, oracle.sql.CLOB)';*/
		        || prior!=null && "language".equalsIgnoreCase(prior.content) && "java".equalsIgnoreCase(token) && top.isAs() //$NON-NLS-1$
		        
		)  {
		    top.to = pos+1;
		    if( "begin".equalsIgnoreCase(top.startToken) && next!=null && next.content.equalsIgnoreCase(";"))
		    	top.to = pos+2;
//System.out.println("+level="+stack.size()+"       "+top.startToken+"..."+src.get(top.to-1).content);
		    stack.removeLast();  // pop
		    if( stack.size() == 0 )
		        return new LazyNode(pos,"error",src); //$NON-NLS-1$
		    stack.getLast().addTopLevel(top);
		    
		}
		return null;
	}


    private static boolean isPivot( LazyNode top, List<LexerToken> src ) {    	
        int priorPos = top.from-1;
        if( priorPos < 0 )
            return false;
        String priorToTop = src.get(priorPos).content;
        boolean isUPivot = priorToTop != null && priorToTop.toLowerCase().endsWith("pivot");
        boolean isNulls = "nulls".equalsIgnoreCase(priorToTop);
        if( isNulls && !isUPivot ) {
            int priorPos2 = priorPos-2;
            if( priorPos2 < 0 )
                return false;
            String prior2 = src.get(priorPos2).content;
            isUPivot = prior2 != null && prior2.toLowerCase().endsWith("pivot");
        }
		return "(".equals(top.startToken) && 0 < top.from && isUPivot;
    }


    /**
     * Example usage
     * @param dummy
     * @throws Exception
     */
    public static void main( String[] dummy ) throws Exception {
        final String input = 
            //"create or replace PACKAGE INLINE00 AS"
            Service.readFile(StackParser.class,"test.sql") //$NON-NLS-1$
            //Service.readFile(StackParser.class,"FND_STATS.pkb") //$NON-NLS-1$
            //Service.readFile(StackParser.class, "MSC_CL_PRE_PROCESS.pkgbdy") //$NON-NLS-1$
            //Service.readFile("D:\\Documents and Settings\\Dim\\.sqldeveloper\\BigTabs\\BIGEMP216959.sql") //$NON-NLS-1$
            ;
        final Thread main = Thread.currentThread();
        
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(850);
                } catch (InterruptedException e) {
                }
                main.interrupt();
            }            
        }.start();

        //Service.profile(60000, 10);
        long h = Runtime.getRuntime().totalMemory();
        long hf = Runtime.getRuntime().freeMemory();
        System.out.println("mem="+(h-hf)); // (authorized) //$NON-NLS-1$
        
        long t1 = System.currentTimeMillis();
        List<LexerToken> src =  LexerToken.parse(input);
        //LexerToken.print(src);
        System.gc();
        
        h = Runtime.getRuntime().totalMemory();
        hf = Runtime.getRuntime().freeMemory();
        System.out.println("lex mem="+(h-hf)); // (authorized) //$NON-NLS-1$
        long t2 = System.currentTimeMillis();
        System.out.println("lex time = "+(t2-t1)); // (authorized) //$NON-NLS-1$        
        System.gc();
        
        LazyNode root = getInstance().parse(src);
        h = Runtime.getRuntime().totalMemory();
        hf = Runtime.getRuntime().freeMemory();
        System.out.println("parse mem="+(h-hf)); // (authorized) //$NON-NLS-1$
        long t3 = System.currentTimeMillis();
        System.out.println("parse time = "+(t3-t2)); // (authorized) //$NON-NLS-1$
        
        Set<Long> tmp = new HashSet<Long>();
        for( ParseNode desc : root.descendants() ) {
            for( long t : tmp ) {
                int x = Service.lX(t);
                int y = Service.lY(t);
                if( desc.from < x && x < desc.to && desc.to < y 
                 || x < desc.from && desc.from < y && y < desc.to  
                ) {
                    System.out.println("[x,y)="+"["+x+","+y+")\n" +
                    		       "[from,to)="+"["+desc.from+","+desc.to+")"
                                       );
                    t2 = System.currentTimeMillis();
                    System.out.println("time = "+(t2-t1)); // (authorized) //$NON-NLS-1$
                    System.exit(0);
                }
            }
            tmp.add(Service.lPair(desc.from, desc.to));
        }
            
        if( src.size() < 1000 )
            root.printTree();
        //root.collectIdentifiers();
        
        t1 = System.currentTimeMillis();
        for( ParseNode desc : root.descendants() ) {
            LazyNode node = (LazyNode)desc;
            if( node.isProcedure() ) {
                //System.out.println(src.get(node.from+1).content);
                node.expand();
            }
        }
        t2 = System.currentTimeMillis();
        System.out.println("expand time = "+(t2-t1)); // (authorized) //$NON-NLS-1$

    }
}
