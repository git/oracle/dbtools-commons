/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import oracle.dbtools.parser.Cell;
import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.Parser.Tuple;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.RecognizedRule;
import oracle.dbtools.parser.plsql.doc.HarvestDoc;
import oracle.dbtools.util.Messages;
import oracle.dbtools.util.Service;

/**
 * SQL syntax analysis with user friendly error messages 
 * see: SyntaxError.main() for example usage
 * @author Dim
 */
public class SyntaxError extends AssertionError {
	//private static final String marker = "^^^";
    public int line;
	public int offset;
	public int end;
	public String code;
	public String marker = "^^^";
    public String detailedMessageKey = "SyntaxError_DetailedMessage"; //$NON-NLS-1$
	
	
    private Map<Long,Integer> weightedRules;   
	
    private Earley earley;
    
	static String TITLE = "Syntax Error"; //$NON-NLS-1$
    static final String MESSAGE_KEY = "SyntaxError_Message"; //$NON-NLS-1$
    static {
    	try {
    		TITLE = Messages.getString("SyntaxError_Title");
    	} catch( ExceptionInInitializerError e ) {}
    }
    
    public static SyntaxError checkSQLQuerySyntax( String input ) {
        return checkSyntax(input, new String[]{"subquery","select","sql_statement"} ); //$NON-NLS-1$ //$NON-NLS-2$
    }
    public static SyntaxError checkSQLQuerySyntax( String input, String marker, String format ) {
    	return checkSyntax(input, new String[]{"subquery","select","sql_statement"}, marker, format ); //$NON-NLS-1$ //$NON-NLS-2$
    }
    
    public static SyntaxError checkSQLStatementSyntax( String input ) {
        return checkSyntax(input, new String[]{"sql_statements"} ); //$NON-NLS-1$ //$NON-NLS-2$
    }
    public static SyntaxError checkSQLStatementSyntax( String input, String marker, String format ) {
        return checkSyntax(input, new String[]{"sql_statements"}, marker, format ); //$NON-NLS-1$ //$NON-NLS-2$
    }
    public static SyntaxError checkSingleStatement( String input ) {
        return checkSyntax(input, new String[]{"sql_statement","select","insert","update","delete","merge"} ); //$NON-NLS-1$ //$NON-NLS-2$
    }

    
    public String[] getSuggestions() {
        List<String> ret = new LinkedList<String>();
        for( Long s : topNsuggestions() ) {
            String candidate = earley.allSymbols[Service.lX(s)];
            if( ret.contains(candidate) )
                continue;
            if( 0 < candidate.indexOf('[') )
                continue;
            ret.add(candidate);
        }
        return ret.toArray(new String[0]);
    }

    
	/**
     * @param input -- sql text
     * @param grammarSymbols -- expected grammar symbols, e.g. "select"
     * http://yourmachine.yourdomain/sql_grammar/sqlbnf/bnffiles/select.htm
     * @return null if valid input, otherwise
     * 		   syntax error message structured as SyntaxError 
     */
    public static SyntaxError checkSyntax( String input, String[] grammarSymbols ) {
        return checkSyntax(input, grammarSymbols, "^^^", "SyntaxError_DetailedMessage");
    }
    private static SyntaxError checkSyntax( String input, String[] grammarSymbols, String marker, String format ) {
        List<LexerToken> src =  LexerToken.parse(input);
        Earley earley = SqlEarley.partialRecognizer();
        Matrix matrix = new Matrix(earley);
        earley.parse(src, matrix); 
        return checkSyntax(input, grammarSymbols, src, earley, matrix, marker, format);
    }
    public static SyntaxError checkSyntax( String input, String[] grammarSymbols, List<LexerToken> src, Earley earley, Matrix matrix) {
        return checkSyntax(input, grammarSymbols, src, earley, matrix, "^^^", "SyntaxError_DetailedMessage");
    }
    public static SyntaxError checkSyntax( String input, String[] grammarSymbols, List<LexerToken> src, Earley earley, Matrix matrix, String marker, String format) {
        Cell top = matrix.get(0, src.size());
        
        if( top != null ) {
            for( String s : grammarSymbols ) {
                for( int i = 0; i < top.size(); i++ ) {
                    Tuple tuple = earley.rules[top.getRule(i)];
                    String candidate = earley.allSymbols[tuple.head];
                    if( candidate.equals(s) )
                        return null;
                }
            }
        }
        
        // not clear what to do if unexpected content in the top cell
        
        int maxY = matrix.lastY();
        
		int end = 0;
		if( 0 < maxY )
			end = src.get(maxY-1).end;
        
        int line = 0;
        int beginLinePos = 0;
        int endLinePos = input.length(); 
        for( int i = 0; i < endLinePos; i++ ) {
			if( input.charAt(i)=='\n' ) {
				if ( i < end ) {
					line++;
					beginLinePos = i;
				} else {
					endLinePos = i;
					break;
				}
			}
		}
                
        String code = input.substring(beginLinePos,endLinePos);
        final int offset = end - beginLinePos;
        
        LexerToken token = null;
        if( maxY < src.size() )
            token = src.get(maxY);
        else if( maxY == src.size() )
            token = src.get(maxY-1);
        
        Map<Long,Integer> rules = new HashMap<Long,Integer>();
        pendingRules(earley, matrix, maxY, null, rules);
        if( token.end == input.length() ) // prefix
            pendingRules(earley, matrix, maxY-1, token, rules);        
       
        for( long rp : rules.keySet() ) {
            long s = parentChildSymbols(rp, earley);
            String ss = earley.allSymbols[Service.lX(s)];
            if( isTypo(ss,token) ) {  
                rules = new HashMap<Long,Integer>();
                rules.put(rp, 1);
                return new SyntaxError(line,offset,token.begin,code,Service.identln(offset, marker),rules,earley,format);
            }
        }
        
        return new SyntaxError(line,offset,end,code,Service.identln(offset, marker),rules,earley,format);
    }
    
    private static void pendingRules( Earley earley, Matrix matrix, int y, LexerToken token, Map<Long,Integer> rules ) {
        if( y < 0 )
            return;
        for( int x = 0; x <= y; x++ ) {
            Cell cell = matrix.get(x,y);
            if( cell != null ) {
            	for( int i = 0; i < cell.size(); i++ ) {
            		int rule = cell.getRule(i);
            		int pos = cell.getPosition(i);
            		if( pos < earley.rules[rule].rhs.length ) {
            			String symbol = earley.allSymbols[earley.rules[rule].rhs[pos]];
            			if( !symbol.startsWith("xml") ) {
            				//suggestions.add(Service.lPair(earley.rules[rule].rhs[pos], earley.rules[rule].head));
                            long element = cell.getContent()[i];
                            if(     token == null )
                                rules.put(element,1);
                            else if(   token.type == Token.IDENTIFIER 
                                 && earley.isTerminal(earley.rules[rule].rhs[pos]) 
                                 && symbol.substring(1).toUpperCase().startsWith(token.content.toUpperCase()) 
                                 && symbol.length()!=token.content.length()+2 
                            )
                                rules.put(element,10);

            			}
            		}
            	}
            }			
		}
    }

    
	private static boolean isTypo( String symbol, LexerToken token ) {
	    if( token == null || token.begin+1 == token.end || token.begin+2 == token.end )
	        return false;
	    String candidate = "'"+token.content.toUpperCase()+"'";
		if( symbol.length()+1 == candidate.length() 
		 ||	symbol.length()   == candidate.length() +1	
		 ||	symbol.length()   == candidate.length() 	
		) {
			int matched = 0;
			int brokenAt = -1;
			for( int i = 0; i < symbol.length() && i < candidate.length(); i++ ) {
				if( symbol.charAt(i) == candidate.charAt(i) )
					matched++;
				else {
					brokenAt = i;
					break;
				}
			}
			if( brokenAt+1 < symbol.length() && brokenAt+1 < candidate.length() 
			 && symbol.charAt(brokenAt+1) == candidate.charAt(brokenAt+1) ) 
				for( int i = brokenAt+1; i < symbol.length() && i < candidate.length(); i++ ) {
					if( symbol.charAt(i) == candidate.charAt(i) )
						matched++;
					else 
						break;
				}
			else if( brokenAt+1 < symbol.length() 
			         && symbol.charAt(brokenAt+1) == candidate.charAt(brokenAt) ) 
				for( int i = brokenAt; i+1 < symbol.length() && i < candidate.length(); i++ ) {
					if( symbol.charAt(i+1) == candidate.charAt(i) )
						matched++;
					else 
						break;
				}
			else if( brokenAt+1 < candidate.length() 
					 && symbol.charAt(brokenAt) == candidate.charAt(brokenAt+1) ) 
				for( int i = brokenAt; i < symbol.length() && i+1 < candidate.length(); i++ ) {
					if( symbol.charAt(i) == candidate.charAt(i+1) )
						matched++;
					else 
						break;
				}
			if( matched == symbol.length()-1 || matched == candidate.length()-1 )
				return true;
		}
		return false;
	}
	
	private SyntaxError( int line, int offset, int end, String code, String marker, Map<Long,Integer> rules, Earley earley, String format ) {
		this.line = line;
		this.offset = offset;
		this.end = end;
		this.code = code;
		this.marker = marker;
		this.weightedRules = rules;
        this.earley = earley;
        this.detailedMessageKey = format;
	}

    @Override
    public String toString() {
        return getDetailedMessage();
    }
    
    public String getDetailedMessage() {
        // Pad out marker to be same size as code so center aligned text
        // lines up correctly
        String pointer = Service.padln(marker, code.length());
    	StringBuilder allSuggestions = new StringBuilder();
        List<String> ret = new LinkedList<String>();
		for( long s : topNsuggestions() ) {
	        String candidate = earley.allSymbols[Service.lX(s)];
	        if( ret.contains(candidate) )
	            continue;
            if( 0 < candidate.indexOf('[') )
                continue;
	        ret.add(candidate);
			allSuggestions.append(candidate+',');
		}
        String suggestions = allSuggestions.toString();
        if( 60 < suggestions.length() )
        	suggestions = suggestions.substring(0,50);
        try {
        	return Messages.format(detailedMessageKey, line, offset, suggestions, code, pointer);
        } catch( NoClassDefFoundError e ) { // standalone run; Messages not defined
        	return "Syntax Error at line "+line+", column "+offset+"\n\n"+suggestions+"\n"+code+"\n\nExpected: "+pointer;
        }
    }

    @Override
    public String getMessage() {
    	StringBuilder allSuggestions = new StringBuilder();
        //SqlEarley earley = SqlEarley.getInstance();
        List<String> ret = new LinkedList<String>();
        for( long s : topNsuggestions() ) {
            String candidate = earley.allSymbols[Service.lX(s)];
            if( ret.contains(candidate) )
                continue;
            if( 0 < candidate.indexOf('[') )
                continue;
            ret.add(candidate);
            allSuggestions.append(candidate+',');
        }
        String suggestions = allSuggestions.toString();
        if( 60 < suggestions.length() )
        	suggestions = suggestions.substring(0,50);
        return Messages.format(MESSAGE_KEY, line, offset, suggestions);
    }

    public String getTitle() {
        return TITLE;
    }
    
    public List<Long> topNsuggestions() {
        Map<Long,Integer> topN = new TreeMap<Long,Integer>();  // symbol -> frequency
        final int N = 10;
        for( long rp : weightedRules.keySet() ) {
            long minVar = -1;
            int minVal = Integer.MAX_VALUE;
            for( long s : topN.keySet() ) {
                int tmp = topN.get(s);
                if( tmp < minVal ) {
                    minVar = s;
                    minVal = tmp;
                }
            }
            long suggestedVar = parentChildSymbols(rp, earley);
            Integer suggestedVal = HarvestDoc.getFrequencies().get(suggestedVar);
//System.out.println(earley.allSymbols[Service.lX(suggestedVar)]+"="+suggestedVal);
            if( suggestedVal == null )
                suggestedVal = 0;
            if( topN.size() == N ) {
                if( suggestedVal != null && minVal < suggestedVal ) {
                    topN.remove(minVar);
                    topN.put(suggestedVar,suggestedVal);
                }
            } else
                topN.put(suggestedVar,suggestedVal);
        }
        
        // http://stackoverflow.com/questions/13015699/representing-binary-relation-in-java
        List<Entry<Long, Integer>> myList = new ArrayList<Entry<Long, Integer>>();
        for (Entry<Long, Integer> e : topN.entrySet())
              myList.add(e);

        Collections.sort( myList, new Comparator<Entry<Long, Integer>>(){
            public int compare( Entry a, Entry b ){
                // compare b to a to get reverse order
                return ((Integer) b.getValue()).compareTo((Integer)a.getValue());
            }
        } );

        //myList = myList.sublist(0, 3);
        
        List<Long> ret = new LinkedList<Long>();
        int i = 0;
        for( Entry<Long, Integer> e : myList ) {
            if( N < i )
                break;
            ret.add(e.getKey());
            i++;
        }
        return ret;
    }
    
    /**
     * Convert rules into RecognizedRule and order them by cumulative frequency. Cut top N
     * @param N -- limit
     * @return
     */
    public List<RecognizedRule> topNrules( final int N, boolean excludeAux ) {
        Map<RecognizedRule,Integer> topN = new TreeMap<RecognizedRule,Integer>();  // rule -> cumulative frequency
        for( long rp : weightedRules.keySet() ) {
            long suggestedVar = parentChildSymbols(rp, earley);
            Integer suggestedVal = HarvestDoc.getFrequencies().get(suggestedVar);
//System.out.println(earley.allSymbols[Service.lX(suggestedVar)]+"="+suggestedVal);
            if( suggestedVal == null )
                suggestedVal = 0;
            suggestedVal *= weightedRules.get(rp);
            
            Earley.Tuple t = earley.rules[Earley.ruleFromEarleyCell(rp)];
            int pos = Earley.posFromEarleyCell(rp);
            String[] rhs = new String[t.rhs.length];
            for ( int i = 0; i < rhs.length; i++ ) 
                rhs[i] = earley.allSymbols[t.rhs[i]];
            String head = earley.allSymbols[t.head];
            if( excludeAux && 0 < head.indexOf('[') )
                continue;
            RecognizedRule candidate = new RecognizedRule(head,rhs,pos);

            Integer frequency = topN.get(candidate);
            if( frequency == null )
                frequency = 0;
            topN.put(candidate,frequency+suggestedVal);            
        }
        
        // http://stackoverflow.com/questions/13015699/representing-binary-relation-in-java
        List<Entry<RecognizedRule, Integer>> myList = new ArrayList<Entry<RecognizedRule, Integer>>();
        for (Entry<RecognizedRule, Integer> e : topN.entrySet())
              myList.add(e);

        Collections.sort( myList, new Comparator<Entry<RecognizedRule, Integer>>(){
            public int compare( Entry a, Entry b ){
                // compare b to a to get reverse order
                return ((Integer) b.getValue()).compareTo((Integer)a.getValue());
            }
        } );

        //myList = myList.sublist(0, 3);
        
        List<RecognizedRule> ret = new LinkedList<RecognizedRule>();
        int i = 0;
        for( Entry<RecognizedRule, Integer> e : myList ) {
            if( N < i )
                break;
            ret.add(e.getKey());
            i++;
        }
        return ret;
    }
    
    private static long parentChildSymbols( long rp, Earley earley ) {
        int rule = Earley.ruleFromEarleyCell(rp);
        Earley.Tuple t = earley.rules[rule];
        int pos = Earley.posFromEarleyCell(rp);
        long entry = Service.lPair(t.rhs[pos],t.head);
        return entry;
    }
    

    /**
     * Example usage
     * @param args
     * @throws Exception
     */
    public static void main( String[] args ) throws Exception {
        
        
        String input = "select * from dual";
        input = Service.readFile(SyntaxError.class,"test.sql"); //$NON-NLS-1$ 
        SyntaxError ret = checkSingleStatement(input);
        if( ret != null ) {
        	/*System.out.println("Syntax Error");
        	System.out.println("at line#"+ret.line);
        	System.out.println(ret.code);
        	System.out.println(ret.marker);*/
        	//System.out.println("Expected:  ");
    		//for( String s : ret.suggestions )
    			//System.out.print(s+',');
        	System.out.println(ret.getDetailedMessage());
        }
        System.out.println("-----------------------------");
        //for( RecognizedRule rr : ret.topNrules(3,true) ) 
            //System.out.println(rr.toString());
        
        
    }
}
