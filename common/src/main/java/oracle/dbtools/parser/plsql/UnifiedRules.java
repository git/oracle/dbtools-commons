/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.Grammar;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.RuleTransforms;
import oracle.dbtools.parser.RuleTuple;
import oracle.dbtools.parser.plsql.doc.DocStatementsTest;
import oracle.dbtools.parser.plsql.doc.HarvestDoc;
import oracle.dbtools.util.Service;

/**
 * Combined SQL+PL/SQL grammar
 * @author Dim
 */
public class UnifiedRules {

    public static void main(String[] args) throws Exception {
        //Service.profile(100000, 1000);

        memorizeRules();
        
        // Is manual refresh in Eclipse navigator necessary in order to sync documentation templates below?
        //HarvestDoc.main(args);
        //if( true )
            //DocStatementsTest.main(args);
    }
    
    private static final String fname = "allRules.serial"; //$NON-NLS-1$
    private static final String path = "/oracle/dbtools/parser/plsql/"; //$NON-NLS-1$
    private static void memorizeRules() throws Exception {
        Set<RuleTuple> rules = extractRules();				    
        FileOutputStream fos = new FileOutputStream("src/main/resources"+path+fname); //$NON-NLS-1$
        ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(rules);
        out.close();
        //RuleTuple.printRules(rules);
    }
	public static Set<RuleTuple> getRules() throws Exception {
        final Class<PlsqlRules> refClass = PlsqlRules.class;
		URL u = refClass.getResource( path+fname );
        if( u == null ) 
        	u = Service.fileNameGitResource(refClass, fname);      
        InputStream is = u.openStream();
        ObjectInputStream in = new ObjectInputStream(is);
        Set<RuleTuple> rules = (Set<RuleTuple>) in.readObject();
        
        //rules.add(new RuleTuple("library_unit", new String[]{"library_unit","'/'",}));
        
        in.close();
        return rules;
    }

    
    private static Set<RuleTuple> extractRules() throws Exception {
        Set<RuleTuple> ret = PlsqlRules.parseBNFtext(PlsqlRules.readBNFfile());
        {
        String input = Service.readFile(UnifiedRules.class, "SqlPlus.grammar"); //$NON-NLS-1$
        List<LexerToken> src = LexerToken.parse(input, false, LexerToken.QuotedStrings/*+SqlPlusComments*/); 
        //LexerToken.print(src);
        ParseNode root = Grammar.parseGrammarFile(src, input);
        Grammar.grammar(root, src, ret);
        }
        {
        String input = Service.readFile(UnifiedRules.class, "PlsqlFixes.grammar"); //$NON-NLS-1$
        List<LexerToken> src = LexerToken.parse(input, false, LexerToken.QuotedStrings/*+SqlPlusComments*/);; 
        //LexerToken.print(src);
        ParseNode root = Grammar.parseGrammarFile(src, input);
        Grammar.grammar(root, src, ret);
        
        PlsqlRules.fixIdentifierIsOrAs(ret);
        }
        final boolean T = true;   // less typing
        final boolean F = false;
        printOrderedRules("set_arg", /*headOnly=*/T, /*exactmatch=*/T,ret);
        
        Set<RuleTuple> sql = SqlRules.extractRules();
        ret.addAll(sql);        
        
        ret.add(new RuleTuple("pls_expr", new String[] {"multiset_expression"})); //$NON-NLS-1$ //$NON-NLS-2$        
        
        /*System.out.println("***********************");
        for( RuleTuple t: ret )
        	if( "json_name_value_pair".equals(t.head) )
        		System.out.println(t.toString());
        System.out.println("***********************");*/
        RuleTransforms.eliminateEmptyProductions(ret);

        RuleTransforms.substituteSingleUnaryProductions(ret);
               
        
		return ret;
	}
    
    private static void printOrderedRules( final String symbol, boolean headOnly, boolean exactMatch, Set<RuleTuple> ret ) {
        for( RuleTuple t: ret ) {  
            if(  (!exactMatch && t.head.contains(symbol) 
                           || exactMatch && t.head.equals(symbol)
                    ) ) 
                System.out.println("     "+t.toString()); // (authorized)
            if( !headOnly ) {
                for( String rhsi : t.rhs ) {
                    if(  (!exactMatch && rhsi.contains(symbol) 
                            || exactMatch && rhsi.equals(symbol)
                     ) ) 
                        System.out.println(t.toString()); // (authorized)
                }
            }
        }
    }

}
