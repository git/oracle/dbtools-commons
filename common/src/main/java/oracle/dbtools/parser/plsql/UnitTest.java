/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.parser.Earley;
import oracle.dbtools.parser.Grammar;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parser;
import oracle.dbtools.parser.RuleTuple;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.Visual;
import oracle.dbtools.parser.Yelrae;
import oracle.dbtools.util.Service;

/**
 * Catches regression in parse tree shapes due to SQL grammar and parser code evolutions
 * @author Dim
 */
public class UnitTest {

    static int testNo = 0;
    private static TreeSet<Integer> failedTests = new TreeSet<Integer>();
    
    private static int output = -1;
    private static int tree = -1;
    private static int node = -1;
    private static int range = -1;
    private static int digits = -1;
    private static int label = -1;
    private static int assertion = -1;
    public static int query = -1;
    private static int atest = -1;
    private static int sql_fragment = -1;
    private static int comment = -1;
    
    public static Earley testParser = null;
    static {
        try {
            testParser = new Earley(getRules()) {
                @Override
                protected boolean isIdentifier( int y, List<LexerToken> src, int symbol, Integer suspect ) {
                    LexerToken token = src.get(y);
                    return 
                          symbol == identifier && token.type == Token.IDENTIFIER 
                      ||  symbol == identifier && token.type == Token.DQUOTED_STRING
                      ||  symbol == identifier && token.type == Token.BQUOTED_STRING
                    ;
                }
            };
        } catch ( Exception e ) {
            e.printStackTrace(); // (authorized)
        }        
        //RuleTuple.printRules(rules);
        output = testParser.symbolIndexes.get("output");
        tree = testParser.symbolIndexes.get("tree");
        assertion = testParser.symbolIndexes.get("assertion");
        query = testParser.symbolIndexes.get("query");
        atest = testParser.symbolIndexes.get("atest");
        node = testParser.symbolIndexes.get("node");
        range = testParser.symbolIndexes.get("range");
        label = testParser.symbolIndexes.get("label");
        digits = testParser.symbolIndexes.get("digits");
        sql_fragment = testParser.symbolIndexes.get("sql_fragment");
        comment = testParser.symbolIndexes.get("comment");
    }

    
    public static void main( String[] args ) throws Exception {
                        
        String testFile = "unit.test";
        
        test(testFile);
    }
    
    static Random random = new Random();
    static protected boolean introduceRandomError = false;
    
    public static void test( String testFile ) throws Exception {
        String input = Service.readFile(UnitTest.class, testFile); //$NON-NLS-1$
        List<LexerToken> src =  LexerToken.parse(input, "`"); 
        
/*int pos = 99873; //65340
System.out.println(src.get(pos++).content);
System.out.println(src.get(pos++).content);
System.out.println(src.get(pos++).content);
System.out.println(src.get(pos++).content);
System.out.println(src.get(pos++).content);*/
        Visual visual = null;
        //visual = new Visual(src, earley)
        Matrix matrix = new Matrix(testParser);
        testParser.parse(src, matrix); 
         
        SyntaxError s = SyntaxError.checkSyntax(input, new String[]{"atest"}, src, testParser, matrix);
        if( s != null ) { //$NON-NLS-1$ //$NON-NLS-2$
        	if( visual != null )
        		visual.draw(matrix);
            System.err.println("Syntax Error");
            System.err.println("at line#"+s.line);
            System.err.println(s.code);
            System.err.println(s.marker);
            System.err.println("Expected:  ");
                for( String tmp : s.getSuggestions() )
                    System.out.print(tmp+',');
            throw new Exception(">>>> syntactically invalid code fragment <<<<"); //$NON-NLS-1$
        }
        
        ParseNode root = testParser.forest(src, matrix);
        atest(root, src, input);
        
        if( failedTests.size() == 0 )
            System.out.println("*** ALL "+testNo+" TESTS are OK *** ---> "); //$NON-NLS-1$
        else 
            System.err.println("*** FAILED "+failedTests.size()+" tests *** ");        //$NON-NLS-1$
        int cnt = -1;
        for( int testNo : failedTests ) {
            cnt++;
            if( cnt < 10 )
                System.err.print(testNo+",");
            else if( cnt == 10 )
                System.err.print(" ... ");
            else if( failedTests.size() - 10 < cnt  )
                System.err.print(","+testNo);
        }
        if( 0 < failedTests.size() )
            System.exit(2);
    }

    private static Set<RuleTuple> getRules() throws Exception {
        String input = Service.readFile(UnitTest.class, "unitTest.grammar"); //$NON-NLS-1$
        List<LexerToken> src = LexerToken.parse(input, false, LexerToken.QuotedStrings/*+SqlPlusComments*/); 
        ParseNode root = Grammar.parseGrammarFile(src, input);
        Set<RuleTuple> ret = new TreeSet<RuleTuple>();
        Grammar.grammar(root, src, ret);
        return ret;
    }
    
    /**
     * @param root
     * @param src
     * @return node that violates assertion
     * @throws Exception
     */
    private static void atest( ParseNode root, List<LexerToken> src, String input ) throws Exception {
        if( root.contains(assertion) ) {
            //System.out.println("TEST#"+testNo); //$NON-NLS-1$ //$NON-NLS-2$
            assertion(root,src,input);
            return;
        }
    	
        if( root.contains(output) ) {
        	for( ParseNode child : root.children() ) {
                //if( child.contains(query) ) {        	    
        		ParseNode out = query(child,src,input);
        		System.out.println("TEST#"+testNo+":  ---> "); //$NON-NLS-1$ //$NON-NLS-2$
        		out.printTree();
        	    return; 
        	}
        	return;
        }
        
        if( root.contains(comment) )
           comment(root,src,input);

        for( ParseNode child : root.children() ) {
            atest(child,src, input);   
        }
    }

    
    private static ParseNode comment(ParseNode root, List<LexerToken> src, String input) throws Exception {
        String testNum = input.substring(src.get(root.from+2).begin,src.get(root.to-1).end-1);
        testNo = Integer.parseInt(testNum);
        return null;
    }

    private static ParseNode query( ParseNode root, List<LexerToken> src, String input ) {            	
        return parse(sql_fragment(root,src,input));
    }

    public static Parser parser = SqlEarley.getInstance();
    private static ParseNode parse( String q ) {
        List<LexerToken> src1 =  LexerToken.parse(q);
        if( introduceRandomError ) {
            int pos = random.nextInt(src1.size()-1);
            if( 10 < pos ) {
                LexerToken t = src1.get(pos);
                t.type = Token.OPERATION;
                t.content = "';'";
            }
        }
        return parser.parse(src1);
    }
    
	private static String sql_fragment( ParseNode root, List<LexerToken> src, String input ) {	    
        String ret = input.substring(src.get(root.from).begin, src.get(root.to-1).end);
        if( ret.charAt(0)=='"' )
            ret = ret.substring(1,ret.length()-1);
        if( ret.charAt(0)=='`' )
            ret = ret.substring(1,ret.length()-1);
        return " "+ret+" ";
	}
	
    private static ParseNode assertion( ParseNode root, List<LexerToken> src, String input ) throws Exception {
    	ParseNode output = null;
    	ParseNode cmp = null;
    	for( ParseNode child : root.children() ) {
        	if( child.contains(query) ) {
        		output = query(child,src,input);
        		//output.printTree();
        		continue;
        	}
            if( child.from+1 == child.to )
                if( "-".equals(child.content(src)) 
                 || ">".equals(child.content(src)) 
                 || ";".equals(child.content(src)) 
                ) 
                	continue; 
        	if( child.contains(tree) ) {
        		cmp = tree(nodeList(child,src,input));
        		continue;
        	}
        }
        boolean test = isSubtree(cmp,output);
        if( !test ) {
            failedTests.add(testNo);
            System.out.println("TEST#"+testNo+":  broken"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        //output.printTree();
        return null;
    }

	private static ParseNode tree( Set<ParseNode> nodeList ) {
		for( ParseNode parent : nodeList ) 
			for( ParseNode grandchild : nodeList ) {
				if( parent.from < grandchild.from && grandchild.to <= parent.to 
				 || parent.from <= grandchild.from && grandchild.to < parent.to  ) {
	                boolean isAdjacent = true;
					for( ParseNode child : nodeList ) { 
						if( parent.from < child.from && child.to <= parent.to 
						 || parent.from <= child.from && child.to < parent.to  ) 
							if( child.from < grandchild.from && grandchild.to <= child.to 
							 || child.from <= grandchild.from && grandchild.to < child.to  ) {
							    isAdjacent = false;
								break;
							}
					}
					if( !isAdjacent )
					    continue;
                    parent.addTopLevel(grandchild);
				}
			}
		return root(nodeList);
	}
	private static ParseNode root( Set<ParseNode> nodeList ) {
		ParseNode ret = null;
		for( ParseNode candidate : nodeList ) {
			if( ret == null ) {
				ret = candidate;
				continue;
			}
			if( candidate.from < ret.from && ret.to <= candidate.to 
			 || candidate.from <= ret.from && ret.to < candidate.to  ) 
				ret = candidate;				
				
		}
		return ret;
	}

	/**
	 * Check if content of node a is subset of b
	 * @param a -- node structure read from test file 
	 * @param b -- node returned by test output
	 * @return
	 */
	private static boolean isSubtree( ParseNode a, ParseNode b ) {
	    if( a.from != b.from || a.to != b.to ) {
            System.out.println("Mismatching intervals: ["+a.from+","+a.to+") and ["+b.from+","+b.to+")"); //$NON-NLS-1$
	        return false;
	    }
		Integer mismatch = contentIsSubsetWithOneExc(a,b);
		if( mismatch != null ) {
			System.out.println("Different content at nodes ["+a.from+","+a.to+") "); //$NON-NLS-1$
			System.out.println("Not found:"+parser.allSymbols[mismatch]);
			return false;
		}
		if( a.topLevel==null && b.children().size() == 0 )
			return true;
		if( a.topLevel.size() != b.children().size() ) {
			System.out.println("Children count mismatch at nodes ["+a.from+","+a.to+")"); //$NON-NLS-1$
			System.out.println("a.topLevel.size()="+a.topLevel.size()+", b.children().size()="+b.children().size()); //$NON-NLS-1$
			for( ParseNode n : a.topLevel ) 
				System.out.println(n.toString());
			for( ParseNode n : b.children() ) 
				System.out.println(n.toString());
			return false;
		}
		for( ParseNode ca : a.topLevel ) {
			boolean matched = false;
			for( ParseNode cb : b.children() ) {
				if( ca.from == cb.from && ca.to == cb.to )
					if( isSubtree(ca, cb) ) {
						matched = true;
						break;
					}
			}
			if( !matched ) {
	            //System.out.println("!isSubtree: ["+a.from+","+a.to+") and ["+b.from+","+b.to+")"); //$NON-NLS-1$
                //System.out.println("!matched: ["+ca.from+","+ca.to+")"); //$NON-NLS-1$
				return false;
			}
		}
		return true;
	}

	private static Integer contentIsSubsetWithOneExc( ParseNode a, ParseNode b ) {
	    if( a instanceof LazyNode && !(b instanceof LazyNode) || !(a instanceof LazyNode) && b instanceof LazyNode )
	        throw new AssertionError( a.getClass() + "!=" + b.getClass()  ); //$NON-NLS-1$
        if( a instanceof LazyNode && b instanceof LazyNode ) {
            String contentA = ((LazyNode)a).startToken;
            if( contentA == null )
                return null;
            return contentA.equals(((LazyNode)b).startToken)? null : -1;
        }
		int i = 0;
		if( a.content()[0] == -1 ) // exception
			i++;
		for( int j = 0; i < a.content().length && j < b.content().length ; i++,j++) {
			while( j < b.content().length && a.content()[i] != b.content()[j] ) 
				j++;
			if( j == b.content().length	)
				return a.content()[i];
		}
		return null;
	}

	private static Set<ParseNode> nodeList( ParseNode root, List<LexerToken> src, String input ) {
		Set<ParseNode> ret = new TreeSet<ParseNode>();
		//if( root.from==150 && root.to==161 )
			//root.from=150;
    	for( ParseNode child : root.children() ) {
    		if( child.contains(node) )
    			ret.add(node(child,src,input));
    		if( child.contains(tree) )
    			ret.addAll(nodeList(child,src,input));
    	}
        if(ret==null)
            ret=null;
		return ret;
	}

	private static ParseNode node( ParseNode root, List<LexerToken> src, String input) {
        ParseNode ret = null;
        if( root.contains(range) ) {
            long rng = range(root,src,input);
            if( parser instanceof SqlEarley || parser instanceof Yelrae && ((Yelrae)parser).employee instanceof SqlEarley )
                ret = new ParseNode(Service.lX(rng), Service.lY(rng), -1, -1, SqlEarley.getInstance());
            else {
                ret = new LazyNode(Service.lX(rng), null, src);
                ret.to = Service.lY(rng);
            }
            return ret;
        }
        //if( root.from==24 && root.to==29 )
            //root.from=24;
    	for( ParseNode child : root.children() ) {
    		if( child.contains(label) ) {
                String symbol = src.get(child.from).content;
                ret.addContent(symbol);
    		} else if( child.contains(node) )
                ret = node(child,src,input);
    	}
    	if(ret==null)
    	    ret=null;
		return ret;
	}

	private static long range( ParseNode root, List<LexerToken> src, String input ) {
		int x = -1;
		int y = -1;
    	for( ParseNode child : root.children() ) {
    		if( child.contains(digits) ) 
    			if( x == -1 )
    				x = Integer.parseInt(src.get(child.from).content);
    			else
    				y = Integer.parseInt(src.get(child.from).content);
    	}
		return Service.lPair(x, y);
	}
}
