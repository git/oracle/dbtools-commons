/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql;

import java.util.List;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Token;
import oracle.dbtools.util.Service;

/**
 * 
 * @author Dim
 *
 */
public class Util {
    public static void main( String[] args ) throws Exception {
        // read input
        String input = 
            //"\"select emp from empno \n\"+\"where deptno = 10\""
            Service.readFile(Util.class, "test.sql.java") //$NON-NLS-1$
        ;
        System.out.println(input);
        System.out.println(javaToSQL(input));
    }
    static String javaToSQL( String input ) { 
        StringBuilder ret = new StringBuilder();
        List<LexerToken> src =  LexerToken.parse(input);
        for( LexerToken t : src )
            if( t.type == Token.DQUOTED_STRING )
                ret.append(t.content.substring(1, t.content.length()-1).replace("\\n", "\n"));
            else if( t.type == Token.WS )
                ret.append(t.content);
            else if( t.type == Token.OPERATION && "+".equals(t.content) )
                continue;
            //else
                //throw new AssertionError("Not SQL embedded in Java");
        return ret.toString();
    }
}
