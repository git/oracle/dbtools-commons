/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql.doc;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.util.Service;

public class DocURL implements Serializable {
	private static final long serialVersionUID = 1L;

	//final static String eSQL =    "e26088/";
	//final static String ePLSQL =  "e25519/";
	public enum Manual implements Serializable {
        SQL, PLSQL, SQLPLUS;
        public String getBasePrivateURL() {
        	return getBasePublicURL();
		}
        public String getBasePublicURL() {
        	switch (this) {
				case SQL:   return "https://docs.oracle.com/en/database/oracle/oracle-database/12.2/sqlrf/";
				case PLSQL: return "https://docs.oracle.com/en/database/oracle/oracle-database/12.2/lnpls/";
                case SQLPLUS: return "https://docs.oracle.com/en/database/oracle/oracle-database/12.2/sqpqr/";
        	}
        	throw new AssertionError("VT:impossible");
		}
    }
    
	private final String pageParagraph;
	private final Manual man;
	
	public DocURL( Manual man, String doc ) {
		super();
		this.pageParagraph = doc;
		this.man = man;
	}
	
	public String getPublicURL() {
    	return man.getBasePublicURL() + pageParagraph;
	}
	public String getPrivateURL() {
    	return man.getBasePrivateURL() + pageParagraph;
	}

	@Override
	public String toString() {
		return getPublicURL();
	}
	
	public static void main(String[] args) {
	    /*Map<Integer, DocURL> templates = HarvestDoc.getRailroads();
	    for( Integer key : templates.keySet() ) {
	        System.out.println(SqlEarley.getInstance().allSymbols[key]+" -> "+templates.get(key).getPublicURL());
	    }*/
		int regexp_like_condition = SqlEarley.getInstance().getSymbol("regexp_like_condition");
		int where_clause = SqlEarley.getInstance().getSymbol("where_clause");
		Set<Substr> templates = HarvestDoc.getTemplates().get(Service.lPair(regexp_like_condition, where_clause));
		for( Substr t : templates) 
			System.out.println(t.toString());
    }
	
}
