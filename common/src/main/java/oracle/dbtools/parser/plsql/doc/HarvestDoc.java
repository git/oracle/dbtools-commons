/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql.doc;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.dbtools.parser.Cell;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parser.Tuple;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.doc.DocURL.Manual;
import oracle.dbtools.util.Logger;
import oracle.dbtools.util.Service;

/**
 * 
 * @author Dim
 *
 */
public class HarvestDoc {
    
    /**
     * Gather templates and code fragments frequencies
     * @param args
     * @throws Exception
     */
    public static void main( String[] args ) throws Exception {
    	LexerToken.isSqlPlusCmd = true;
        crawlDocWebsite(Manual.SQLPLUS); 
    	LexerToken.isSqlPlusCmd = false;
        crawlDocWebsite(Manual.PLSQL); 
        crawlDocWebsite(Manual.SQL); 
        for( long s : templates.keySet() ) {
            int parent = Service.lY(s);
            int child = Service.lX(s);
            if( parent < 0 ) {
                System.err.println("parent<0 ("+parent+")");
                continue;
            }
            if( SqlEarley.getInstance().allSymbols.length-1 < parent ) {
                System.err.println("len<parent ("+parent+")");
                continue;
            }
            if( child < 0 ) {
                System.err.println("child<0 ("+child+")");
                continue;
            }
            if( SqlEarley.getInstance().allSymbols.length-1 < child ) {
                System.err.println("len<child ("+child+")");
                continue;
            }
            System.out.println(SqlEarley.getInstance().allSymbols[parent]+"->"+SqlEarley.getInstance().allSymbols[child]+
                               "="+templates.get(s).size()); // (authorized)
        }
        FileOutputStream fos = new FileOutputStream("src/main/resources"+path+fname); //$NON-NLS-1$
        ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(templates);
        out.close();
        fos = new FileOutputStream("src/main/resources"+path+fname1); //$NON-NLS-1$
        out = new ObjectOutputStream(fos);
        out.writeObject(frequencies);
        out.close(); 
        fos = new FileOutputStream("src/main/resources"+path+fname2); //$NON-NLS-1$
        out = new ObjectOutputStream(fos);
        out.writeObject(railroads);
        out.close(); 
        
        if( false ) { // pl/sql and sql doc test baseline has been built already
            //FileWriter fw = new FileWriter("common/src"+path+"doc_plsql.test");
            //fw.write(plsqlTest.toString());
            //fw.close();           
            //fw = new FileWriter("common/src"+path+"doc_sql.test");
            //fw.write(sqlTest.toString());
            //fw.close();           
        }
    }

    private static StringBuilder plsqlTest = new StringBuilder("/* test generated from doc website */\n");
    private static StringBuilder sqlTest = new StringBuilder("/* test generated from doc website */\n");
    private static StringBuilder sqlPlusTest = new StringBuilder("/* test generated from doc website */\n");
    private static int testNo = 0;
    
    
    private static Set<String> processed = new HashSet<String>();
    private static void crawlDocWebsite( Manual man ) { 
    	
        testNo = 0;
        
    	processed = new HashSet<String>();
		String input = null;
        try {
            String masterBNFurl = man.getBasePrivateURL()+"toc.htm";
            if( man == Manual.PLSQL  )
            	masterBNFurl = man.getBasePrivateURL()+"loe.html";
            input = readURL(masterBNFurl); //$NON-NLS-1$ //$NON-NLS-2$            
        } catch( java.io.FileNotFoundException e ) {
            System.err.println("\nFileNotFoundException "+e.getMessage()); // (authorized)
            System.exit(0);
        } catch( Exception e ) { 
            if( e.getClass().getPackage().getName().contains("java.net") ) {
                System.err.println("Failed to connect to "+e.getMessage()); // (authorized)
                System.exit(0);
            }
        }
        String pre = "<a href=\"";
        String post = "\">";
        for( int iPre = input.indexOf(pre); iPre > 0; iPre = input.indexOf(pre, iPre+pre.length()) ) {
            int iPost = input.indexOf(post,iPre+pre.length());
            String url = input.substring(iPre+pre.length(),iPost);
            if( url.startsWith("http://") || url.startsWith("..") )
            	continue;
            try {
                int index = url.indexOf('#');
                if( index==0 )
                	continue;
                String page = url;
                if( index > 0 )
                	page = page.substring(0,index);
            	if( page.contains("plsql-predefined-data-types") )
            		continue;
                index = page.indexOf("\" class=");
                if( 0 < index )
                	page = page.substring(0,index);
                processDoc(man,page);
            } catch( Exception e ) {
            	System.err.println(url+" " + e.getMessage()+" "); // (authorized)
            }

        }
		
	}

	private static void processDoc( final Manual man, final String page ) throws FileNotFoundException, IOException {
        StringBuilder test =  plsqlTest;
        if( Manual.SQL.equals(man) )
        	test =  sqlTest;
        if( Manual.SQLPLUS.equals(man) )
        	test =  sqlPlusTest;
        
        String input = null;
        SqlEarley earley = SqlEarley.getInstance();
        try {
            String fullUrl = man.getBasePrivateURL() + page;
            if( !processed.contains(fullUrl) ) {
            	processed.add(fullUrl);
            } else
            	return;
            input = readURL(fullUrl); //$NON-NLS-1$ //$NON-NLS-2$
        } catch( java.io.FileNotFoundException e ) {
            System.err.println("\nFileNotFoundException "+e.getMessage()); // (authorized)
            System.exit(0);
        } catch( Exception e ) { 
            if( e.getClass().getPackage().getName().contains("java.net") ) {
                System.err.println("URL="+page);  // (authorized)
                System.err.println("Failed to connect to "+e.getMessage()); // (authorized)
                System.exit(0);
            }
        }
        
        String pre1 = "<div class=\"section\" id=\"";
        String post1 = "\"";
        String pre2 = "<span class=\"italic\">";
        String post2 = "::=";
        for( int iPre1 = input.indexOf(pre1); iPre1 > 0; iPre1 = input.indexOf(pre1, iPre1+pre1.length()) ) {
            int iPost1 = input.indexOf(post1,iPre1+pre1.length());
            String id = input.substring(iPre1+pre1.length(),iPost1);
            int iPre2 = input.indexOf(pre2, iPost1);
            int iPost2 = input.indexOf(post2,iPre2+pre2.length());
            if( iPost2 < 0 )
                continue;
            String symbol = input.substring(iPre2+pre2.length(),iPost2);
            if( 60 < symbol.length() )
                continue;
            int iSpan = symbol.indexOf("</span>");
            if( iSpan < 0 )
                continue;
            symbol = symbol.substring(0, iSpan);
            Integer s = SqlEarley.getInstance().symbolIndexes.get(symbol);
            if( s == null )
                continue;
            railroads.put(s, new DocURL(man,page+"#"+id));
        }
        
        
        //String pre = "<pre xml:space=\"preserve\" class=\"oac_no_warn\">";
        String pre = "<pre class=\"oac_no_warn\" dir=\"ltr\">";
        String post = "</pre>";
        for( int iPre = input.indexOf(pre); iPre > 0; iPre = input.indexOf(pre, iPre+pre.length()) ) {
            int iPost = input.indexOf(post,iPre+pre.length());
            String code = input.substring(iPre+pre.length(),iPost);
            code = code.replace("&gt;", ">"); //$NON-NLS-1$
            code = code.replace("&lt;", "<"); //$NON-NLS-1$
            code = code.replace("<span class=\"italic\">", ""); //$NON-NLS-1$
            code = code.replace("<span class=\"bold\">", ""); //$NON-NLS-1$
            code = code.replace("</span>", ""); //$NON-NLS-1$ 

            code = code.replace("Call completed.", ""); //$NON-NLS-1$ 
            code = code.replace("1 row created.", ""); //$NON-NLS-1$ 
            code = code.replace("1 row updated.", ""); //$NON-NLS-1$ 
            code = code.replace("Commit complete.", ""); //$NON-NLS-1$ 
            code = code.replace("Enter password: password", ""); //$NON-NLS-1$ 
            code = code.replace("C    F_RATIO    P_VALUE", ""); //$NON-NLS-1$ 
            code = code.replace("- ---------- ----------", ""); //$NON-NLS-1$ 
            code = code.replace("F 5.59536943 4.7840E-09", ""); //$NON-NLS-1$ 
            code = code.replace("M  9.2865001 6.7139E-17", ""); //$NON-NLS-1$ 
            code = code.replace("CAST(POWERMULTISET_BY_CARDINALITY(CUST_ADDRESS_NTAB,2) AS CUST_ADDRESS_TAB_TAB_TYP)", ""); //$NON-NLS-1$ 
            //breaks explain plan: code = code.replace("T_", ""); //$NON-NLS-1$  
            code = code.replace("YP)", ""); //$NON-NLS-1$ 
            code = code.replace("LASVALUE(quantity", "LAST_VALUE(quantity"); //$NON-NLS-1$ 
            code = code.replace("CREATE INDEX index ON table", "CREATE INDEX index ON tbl"); //$NON-NLS-1$ 
            code = code.replace("ALTER TABLE table", "ALTER TABLE tbl"); //$NON-NLS-1$ 
            code = code.replace("operator t_alias2.column", "= t_alias2.column"); //$NON-NLS-1$ 
            code = code.replace("WHERE expr operator", "WHERE expr IN"); //$NON-NLS-1$ 
            code = code.replace("WHERE column operator", "WHERE column IN"); //$NON-NLS-1$ 
            code = code.replace("       salary + NVL(commission_pct, 0),", "      , salary + NVL(commission_pct, 0),"); //$NON-NLS-1$ 
            code = code.replace("employee_id                              -- and employee id.", ",employee_id                              -- and employee id."); //$NON-NLS-1$
            code = code.replace("<span class=\"codeinlineitalic\"", "<span class=dqt codeinlineitalic dqt"); //$NON-NLS-1$
            code = code.replace(". . .", "i integer);"); //$NON-NLS-1$
            code = code.replace("Grant succeeded.", ""); //$NON-NLS-1$
            code = code.replace("User altered.", ""); //$NON-NLS-1$
            code = code.replace("Session altered.", ""); //$NON-NLS-1$
            code = code.replace("View created.", ""); //$NON-NLS-1$
            code = code.replace("View dropped.", ""); //$NON-NLS-1$
            code = code.replace("Procedure created.", ""); //$NON-NLS-1$
            code = code.replace("5 rows updated.", ""); //$NON-NLS-1$
            code = code.replace("CAST(POWERMULTISET(CUST_ADDRESS_NTAB) AS CUST_ADDRESS_TAB_TAB_T", ""); //$NON-NLS-1$
            code = code.replace("no rows selected", ""); //$NON-NLS-1$
            code = code.replace("statement;", "NULL;"); //$NON-NLS-1$
             code = code.replace("&rsquo;", "'"); //$NON-NLS-1$
            code = code.replace("......", ""); //$NON-NLS-1$
            code = code.replace("....", "NULL;"); //$NON-NLS-1$
            code = code.replace("PROCEDURE inner", "PROCEDURE inner1"); //$NON-NLS-1$
            code = code.replace("<input table>", "tbl1"); //$NON-NLS-1$
            code = code.replace("&amp;", "&"); //$NON-NLS-1$
            code = code.replace("Enter value for ", "--"); //$NON-NLS-1$
            code = code.replace("WHERE JOB_ID='SA_MAN'", "WHERE JOB_ID='SA_MAN';"); //$NON-NLS-1$
            code = code.replace("WHERE JOB_ID='SA_MAN';;", "WHERE JOB_ID='SA_MAN';"); //$NON-NLS-1$
            code = code.replace("WHERE JOB_ID='SA_MAN'; ;", "WHERE JOB_ID='SA_MAN';"); //$NON-NLS-1$
            code = code.replace(":port/", ":10/"); //$NON-NLS-1$
            code = code.replace("MAP MEMBER FUNCTION rational_order -", "MAP MEMBER FUNCTION rational_order "); //$NON-NLS-1$
            code = code.replace("END;\n" + 
            		"\n" + 
            		"USD ", "END;"); //$NON-NLS-1$            
            code = code.replace("<code class=\"codeph\">", ""); //$NON-NLS-1$
            code = code.replace("</code>", ""); //$NON-NLS-1$
            
            int begin = 0;
            if( 10000 < iPre )
            	begin = iPre - 10000;
            String priorChunk = input.substring(begin,iPre);
			String paragraphPre = "<a id=\"";
			int iParagraph = priorChunk.lastIndexOf(paragraphPre);
            String pageParagraph = page;
            if( 0 <= iParagraph ) {
                int iEndOfPar = priorChunk.indexOf('"',iParagraph+paragraphPre.length());
            	pageParagraph += "#"+priorChunk.substring(iParagraph+paragraphPre.length(),iEndOfPar);
            }
            
            
            if( code.charAt(0) == '\n' )
                code = code.substring(1);
            
            int dash3index = code.indexOf("---");
            if( 3 < dash3index && code.charAt(dash3index-1)==' ' )
                dash3index--;
            
            if( 2 < dash3index && code.charAt(dash3index-1)=='\n' ) {
                code = code.substring(0,dash3index);
                int lastNewLineIndex = code.lastIndexOf("\n");
                if( lastNewLineIndex < 1 )
                    continue;
                code = code.substring(0,lastNewLineIndex);
                lastNewLineIndex = code.lastIndexOf("\n");
                if( lastNewLineIndex < 1 )
                    continue;
                code = code.substring(0,lastNewLineIndex);
            }
                         
            if( code.toUpperCase().startsWith("EXECUTE IMMEDIATE ") ) {
                if( !code.trim().endsWith(";") )
                    code = code + ";";
                code = "begin\n" + code + "\n end; \n";
            }
            /*if( code.toUpperCase().startsWith("TYPE ")
             //|| code.toUpperCase().startsWith("CURSOR ") 
            ) {
                //if( !code.trim().endsWith(";") )
                    //code = code + ";";
                code = "declare " + code + "\n begin null; end; \n";
            }*/
            
            if( 0<=code.indexOf("version=\"1.0\"") ) {
                test.append("--version=\"1.0\" ; skipped \n\n");
                continue;
            } else if( "controlstatements.html#LNPLS431".equals(pageParagraph) ) {
                test.append("/*controlstatements.html#LNPLS431 ; skipped*/ \n\n");
                continue;
            } if( 0<=code.indexOf("ORA-") ) {
                test.append("/*ORA- ; skipped*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("...") ) {
                if( code.length() < 300 ) {
                    test.append("/*incomplete ... ; skipped*/ \n\n");
                    continue;
                } else {
                    code = code.replace("...", "/*...*/");
                }
            } else if( 0<=code.indexOf("ORACLE exception") ) {
                test.append("/*ORACLE exception ; skipped*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("PL/SQL procedure successfully completed") ) {
                test.append("/*PL/SQL procedure successfully completed <-- skipped*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("SQL>") ) {
                test.append("/*SQL>  ; skipped*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("field_name") ) {
                test.append("/*  field_name    ; skipped*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("WHEN condition_n THEN statements_n") ) {
                test.append("/*  WHEN condition_n THEN statements_n    ; skipped*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("------------------------------------------------------------------------") ) {
                test.append("/*  ------------------------------------------------------------------------  ; skipped*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("\"sthref809\"") ) {
                test.append("/*  \"sthref809\"    ; skipped*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("<<end_loop>>") ) {
                test.append("/*  <<end_loop>>    ; skipped*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("hr.tb1.col1.x") ) {
                test.append("/*  hr.tb1.col1.x    ; skipped*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("We expect this THEN to always be performed") ) {
                test.append("/*  We expect this THEN to always be performed    ; skipped*/ \n\n");
                continue;
            } else if( 0==code.indexOf("Cursor is closed") ) {
                test.append("/*  Cursor is closed...    ; skipped*/ \n\n");
                continue;
            } else if( code.length() < 30 && !Manual.SQLPLUS.equals(man) ) {
                test.append("/*skipped: "+code+"*/ \n\n");
                continue;
            } else if( code.length() < 20 && Manual.SQLPLUS.equals(man) ) {
                test.append("/*skipped: "+code+"*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("<a id=\"d10") ) {
                test.append("/*skipped: "+code+"*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("  2") && 0<=code.indexOf("  3") ) {
                test.append("/*skipped: "+code+"*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("  3") && 0<=code.indexOf("  4") ) {
                test.append("/*skipped: "+code+"*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("[") && code.indexOf("[")<=10 && 0<=code.indexOf("]") && code.indexOf("]")<=10 ) {
                test.append("/*skipped: "+code+"*/ \n\n");
                continue;
            } else if( 0<=code.indexOf("xquery for")  ) {
                test.append("/*skipped: "+code+"*/ \n\n");
                continue;
            } 
            String[] skippedPrefixes = {
                    "IF condition",
                     "SUBTYPE subtype_name IS base_type",
                     ":host_variable",
                     "LOOP",
                     "SUBTYPE INTEGER IS NUMBER(38,0)",
                     "FUNCTION TO_CHAR (right DATE) RETURN VARCHAR2;",
                     "ALTER SESSION SET NLS_DATE_FORMAT='\"'' OR service_type=''Merger\"';",
                     "-- Second %s in \"Expected %s, found %s\":",
                     "BEFORE",
                     "-- Retrieve ora_sql_txt into  sql_text variable",
                     "ALTER TRIGGER [schema.]trigger_name { ENABLE | DISABLE };",
                     "ALTER TABLE table_name { ENABLE | DISABLE } ALL TRIGGERS;",
                     "declare type VARCHAR2",
                     "declare type DATE",
                     "declare type NUMBER",
                     "--- Original order data ---",
                     "First",
                     "Before",
                     "--- Processing all results simultaneously ---",
                     "------- Results from One Bulk Fetch --------",
                     "EXCEPTION",
                     "-- To save the output of the query to a file:",
                     "Delete succeeded",
                     "CURSOR cursor_name [",
                     "Raise for employee",
                     "declare TYPE type_name IS REF CURSOR [ RETURN return_type ]",
                     "EXEC SQL",
                     "Sum of",
                     "<< label >> (optional)",
                     "With first name first, email is: John.Doe@AcmeCorp",
                     "Table updated?",
                     "INSERT INTO TABLE (", // triggers.htm#CIHFBGDC -- just one case in PL/SQL doc
                     
                     "(SUM",
                     "SUM((",
                     "SELECT TO_CHAR(number, 'fmt')",
                     "SELECT /*+ PARALLEL_INDEX(table1, index1, 3) */",
                     "TIMESTAMP [(fractional_seconds_precision)]",
                     "INTERVAL YEAR [(year_precision)]",
                     "INTERVAL DAY [(day_precision)]",
                     "SELECT CHR (196 USING NCHAR_CS)", // unwanted query output
                     "SELECT COMPOSE( 'o' || UNISTR(", // unwanted query output
                     "('CLARK' || 'SMITH')",
                     "MIN([table.]column), MAX([table.]column)",
                     "INTERVAL '5-3' YEAR TO MONTH + INTERVAL'20' MONTH =",
                     "INTERVAL'20' DAY - INTERVAL'240' HOUR = INTERVAL'10-0' DAY TO SECOND",
                     "(10, 20, 40)",
                     ":employee_name INDICATOR :employee_name_indicator_var",
                     "SELECT feature_id, value", // functions069.htm#SQLRF55872 should be feature_details
                     "  If (CRN = FRN = RN) then the result is",
                     "                     NULL if VAR_POP(expr2)  = 0",
                     "   if (CRN = FRN = RN) then",
                     "SELECT NCHR(187)",
                     "SELECT TO_SINGLE_BYTE( CHR(15711393)) FROM DUAL;",
                     "PROCEDURE top_protected_proc",
                     "CREATE OR REPLACE PACKAGE protected_pkg",
                     "CREATE trigger FOR dml_event_clause",
                     "Table name",
                     "PROCEDURE protected_proc2",
                     "ALTER TABLE tbl_name {",
                     "PRAGMA COVERAGE ('NOT_FEASIBLE_START');",
                     "type VARCHAR2 is new CHAR_BASE;",
                     "EXEC DBMS_OUTPUT.PUT_LINE(ORA_MAX_NAME_LEN_SUPPORTED);",
                     "   a NATURAL LEFT ",
                     "DEF[INE]",
                     "DEFINE DEPARTMENT_ID = \"20\" (",
                     "DEFINE _DATE = \"\" (",
                     "DEFINE _SQLPLUS_RELEASE = \"1201000100\" (",
                     "DEFINE L_NAME = \"SMITH\" (",
                     "EDIT SALES<a id=\"",
                     "SELECT &GROUP_COL, MAX(&NUMBER_COL) MAXIMUM",
                     "MIN (&&",
                     "SUM(&&",
                     "AVG(&&",
                     "CREATE OR REPLACE FUNCTION EmpInfo_fn RETURN -",
                     "FUNCTION afunc RETURNS NUMBER",
                     "ALTER SESSION SET <a",
                     "SELECT HIRE_DATEFROM",
                     "(DESCRIPTION=",
                     "CONNECT hr[",
                     "SELECT CITY, COUNTRY_NAMEFROM",
                     "Table SALESMAN created.",
                     "SELECT FIRST_NAME, LAST_NAME, JOB_ID, SALARYFROM EMP_DETAILS_VIEWWHERE",
                     "COLUMN column_name HEADING column_heading",
                     "LAST                         MONTHLY",
                     "COLUMN<a id=",
                     "BREAK<a",
                     "BREAK ON break_column ON REPORT",
                     "APPEND , COMMISSION_PCT;",
                     //"APPEND<a id=\"",
                     //"CHANGE<a id=\"",
                     "                    A C M E  W I D G E T",
                     "                         A C M E  W I D G E T",
                     "TTITLE<a",
                     "COLUMN column_name NEW_VALUE<a",
                     "SPOOL<a",
                     "DEL [n",
                     "SHUTDOWN [ABORT",
                     "NUMBER CHAR CHAR (n [CHAR",
                     "SAV[E]",
                     "SPO[OL]",
                     "create table D2_t2 (unique1 number) parallel -",
                     "create table D2_t1 (unique1 number) parallel -",
                     "RECOVER {",
                     "COL n S[",
                     "CREATE OR REPLACE TYPE BODY rational AS OBJECT",
                     "DESCRIBE DEPTH 2 LINENUM OFF INDENT ON",
                     "WHENEVER OSERROR {",
                     "WHENEVER SQLERROR {",
                     "PASSWORD",
                     "STARTUP db_options",
                     "STORE SET file_name[",
                     "Number of employees deleted:",
                     "TYPE type_name IS REF CURSOR [",
                     /*TODO*/"SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -",
                     "CAST (<table subquery>",
            };
            boolean skip = false;
            for( String skipped : skippedPrefixes ) {
                if( code.startsWith(skipped) ) {
                    skipped = skipped.replace("/*", "/+");
                    skipped = skipped.replace("*/", "+/");
                    test.append("/*skipped "+skipped+" */ \n\n");
                    skip = true;
                    break;
                }
            }
            if( skip ) 
                continue;

            skip = true;
            List<LexerToken> src =  LexerToken.parse(code);
            Matrix matrix = new Matrix(earley);
            earley.parse(src, matrix); 
            Cell c01 = matrix.get(0, 1);
            for( int i = 0; i < c01.size(); i++) {
                int pos = c01.getPosition(i);
                if( 0 < pos ) {
                    int rule = c01.getRule(i);
                    int rhs0 = earley.rules[rule].rhs[0];
                    if( earley.allSymbols[rhs0].charAt(0)=='\'' )
                        skip = false;
                }
            }            
            if( skip ) {
                test.append("/* failed scan "+code.substring(0,20)+" */ \n\n");
                continue;
            } else { 
                test.append("++");
                test.append(testNo++);
                test.append(";  /*  ");
                test.append(pageParagraph);
                test.append("  */\n");
                
                test.append('`');
                test.append(code);
                test.append("`->\n");               
            }                                

            Cell top = matrix.get(0, src.size());            
            if( top == null ) {
            	System.err.println("-------vvvvvvvvvvvvv-------");
            	System.err.println(code);
            	System.exit(1);
            }
            
            
            ParseNode root = null;
            try {
            	root = earley.forest(src, matrix);
            } catch( AssertionError e ) {
            	System.err.println("AssertionError"+e.getMessage()); // (authorized)
            	System.err.println(code); // (authorized)
                test.append("[0,0) dummy /***failed***/\n;\n\n");
            	continue;
            }
            if( root == null || root.to<2 ) {
            	System.err.println(code); // (authorized)
                test.append("[0,0) dummy /**failed**/\n;\n\n");
            	continue;
            }
            
            int from = 0;
            int to = src.size();
            if( root.topLevel != null ) {
            	for( ParseNode n : root.children() ) {
            		if( n.from == 0 ) {
            			root = n;
            			to = root.to;
            		}
            	}
            	if( root.topLevel != null ) {
            		System.err.println(code); // (authorized)
                    test.append("[0,0) dummy /*failed*/\n;\n\n");
            		continue;
            	}
            }
            test.append(root.tree()+"\n;\n\n");
            
    		//String fragment = code.substring(src.get(from).begin, src.get(to-1).end);
    		//LexerToken.moveInterval(src,src.get(from).begin);
            System.out.println(code); // (authorized)
            System.out.println("------------------------------"); // (authorized)
            try {
                Substr.generateTemplate(root,src,code, templates, 0, man, pageParagraph);
                countFrequencies(root);
            } catch( Exception e ) {
                System.err.println(page+" --> "+e.getMessage()); // (authorized)
            }
        }
                
    }

    private static final String fname = "templates.serial"; //$NON-NLS-1$
    private static final String fname1 = "frequencies.serial"; //$NON-NLS-1$
    private static final String fname2 = "railroad.serial"; //$NON-NLS-1$
    static final String path = "/oracle/dbtools/parser/plsql/doc/"; //$NON-NLS-1$
    
    private static Map<Long,Set<Substr>> templates = new HashMap<Long,Set<Substr>>();    //lPair(child,parent)
    public static Map<Long,Set<Substr>> getTemplates() {
        if( templates.size() == 0 ) {
            URL u = HarvestDoc.class.getResource( path+fname );
            try {
                InputStream is = u.openStream();
                ObjectInputStream in = new ObjectInputStream(is);
                templates = (Map<Long,Set<Substr>>) in.readObject();
                in.close();            
            } catch( Exception e ) {
                Logger.severe(HarvestDoc.class, "Failed to read templates: "+e.getMessage());
            }
        }
        return templates;
    }
    
    private static Map<Long,Integer> frequencies = null;  //lPair(child,parent)
    private static void countFrequencies( ParseNode root ) {
        if( frequencies == null ) {
            frequencies = new HashMap<Long,Integer>();
        }
        for( ParseNode descendant: root.descendants() ) {
            ParseNode parent = descendant.parent();
            if( parent != null ) {
                for( int i : descendant.content() )
                    for( int j : parent.content() ) {
                        long key = Service.lPair(i, j);
                        Integer frequency = frequencies.get(key);
                        if( frequency == null )
                            frequency = 0;
                        frequencies.put(key, frequency+1);
                    }
                // take care of simple alternatives, e.g. is_or_as: 'IS';
                for( int c : descendant.content() )
                    for( int p : descendant.content() ) {
                        if( c == p )
                            continue;
                        for( Tuple t : SqlEarley.getInstance().rules ) {
                            if( t.head == p && t.rhs.length == 1 ) {
                                if( t.rhs[0] == c ){
                                    long key = Service.lPair(c, p);
                                    Integer frequency = frequencies.get(key);
                                    if( frequency == null )
                                        frequency = 0;
                                    frequencies.put(key, frequency+1);
                                    break;

                                }
                            }
                                
                        }
                    }
            } else {
                for( int i : descendant.content() ) {
                    long key = Service.lPair(i, 0);
                    Integer frequency = frequencies.get(key);
                    if( frequency == null )
                        frequency = 0;
                    frequencies.put(key, frequency+1);
                }
            }
               
        }
    }
    public static Map<Long,Integer> getFrequencies() {
        if( frequencies == null ) {
            URL u = HarvestDoc.class.getResource( path+fname1 );
            try {
                if( u == null ) 
                   	u = Service.fileNameGitResource(HarvestDoc.class, path+fname1);            
                InputStream is = u.openStream();
                ObjectInputStream in = new ObjectInputStream(is);
                frequencies = (Map<Long,Integer>) in.readObject();
                in.close();            
            } catch( Exception e ) {
            	Logger.severe(HarvestDoc.class, "Failed to read frequnecies: "+e.getMessage());
            }
        }
        return frequencies;
    }
    
    private static Map<Integer,DocURL> railroads = new HashMap<Integer,DocURL>();    //lPair(child,parent)
    public static Map<Integer,DocURL> getRailroads() {
        if( railroads.size() == 0 ) {
            URL u = HarvestDoc.class.getResource( path+fname2 );
            try {
                InputStream is = u.openStream();
                ObjectInputStream in = new ObjectInputStream(is);
                railroads = (Map<Integer,DocURL>) in.readObject();
                in.close();            
            } catch( Exception e ) {
                Logger.severe(HarvestDoc.class, "Failed to read railroad diagram URLs: "+e.getMessage());
            }
        }
        return railroads;
    }


    static String readURL( final String masterBNFurl ) throws Exception {
        if( masterBNFurl.endsWith(".pdf") 
            ||    masterBNFurl.endsWith(".mobi")
            ||    masterBNFurl.endsWith(".epub")
                )
            return "";
        byte[] bytes = new byte[4096]; 
        int bytesRead = 0; 
        URL url = new URL( masterBNFurl ); 
        System.out.print("opening "+masterBNFurl+"..."); // (authorized)
        BufferedInputStream bin = new BufferedInputStream( url.openStream() ); 
        System.out.println("done."); // (authorized)
        bytesRead = bin.read( bytes, 0, bytes.length ); 
        StringBuilder sb = new StringBuilder();
        while ( bytesRead != -1 ){ 
            sb.append(new String(bytes).substring(0,bytesRead));
            bytesRead = bin.read( bytes, 0, bytes.length ); 
        }
        bin.close();
        return sb.toString();        
    }


}
