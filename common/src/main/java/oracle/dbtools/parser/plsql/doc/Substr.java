/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.parser.plsql.doc;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.doc.DocURL.Manual;
import oracle.dbtools.util.Service;

/**
 * Code snippet from documentation
 * 
 * @author Dim
 *
 */
public class Substr implements Serializable {
    
    static final long serialVersionUID = 3L;
	final int from;
	final int to;
	final String src;
	final DocURL doc;
	final long weight;
	
	/**
	 * Character strings src are interned, thus from and to offsets
	 * @param from
	 * @param to
	 * @param src
	 * @param weight -- to emphasize frequently used 
	 * @param doc -- back pointer to documentation where it occured
	 */
	public Substr( int from, int to, String src, long weight, DocURL doc ) {
		this.from = from;
		this.to = to;
		this.src = src;
		this.weight = weight;
		this.doc = doc;
	}
	
    public DocURL getDoc() {
        return doc;
    }

    /**
     * ordering 
     * @param s comparison
     * @return
     */
	public boolean le( Substr s ) {
		if( weight != s.weight )
			return weight > s.weight; 
		return to-from < s.to-s.from;
	}
	
	/**
	 * @return truncated src
	 */
	public String cutSrc() {
		return src.substring(from,to);
	}
	
	@Override
	public boolean equals( Object obj ) {
		if( !(obj instanceof Substr) )
			return false;
		Substr s = (Substr)obj;
		return cutSrc().equals(s.cutSrc());
	}	
	@Override
	public int hashCode() {
		return cutSrc().hashCode();
	}

	@Override
	public String toString() {
		return cutSrc();
	}
	
	
    static void add( Set<Substr> target, Substr elem ) {
        String els = elem.cutSrc();
		List<LexerToken> elemSrc = LexerToken.parse(els); 
        Substr match = null;
        for( Substr s : target ) {
        	String ss = s.cutSrc();
			List<LexerToken> sSrc = LexerToken.parse(ss); 
        	int pos = -1;
        	int elemSize = elemSrc.size();
        	int sSize = sSrc.size();
    		final int maxLength = elem.weight == 0 ? 16 : 32;
    		if( elemSize == sSize 
    				|| elemSize == sSize+1 && elemSrc.get(elemSize-1).content.equals(";")
    				|| sSize == elemSize+1 && sSrc.get(sSize-1).content.equals(";")
    	            || elemSize == sSize+1 && elemSrc.get(elemSize-1).content.equals("/")
    	            || sSize == elemSize+1 && sSrc.get(sSize-1).content.equals("/")
    				|| elemSize > maxLength && sSize > maxLength 
    		) {
    			match = s;
                //if( match.toString().contains("org_chart (eid, emp_last, mgr_id, reportLevel, salary, job_id) AS") 
                 //&& elem.toString().contains("SELECT  /*+ ORDERED */ o.order_id, c.customer_id, l.unit_price * l.quantity") )
                //match = match;

    			for( LexerToken t : sSrc ) {
    				pos++;
    				if( pos >= maxLength || elemSize <= pos )
    					break;
    				final Token type = elemSrc.get(pos).type;
    				final String content = elemSrc.get(pos).content;
    				boolean matches = type==t.type;
    				if( matches ) {
    				    if( type==Token.IDENTIFIER && 2 < content.length() 
    				     && SqlEarley.getInstance().symbolIndexes.containsKey("'"+content+"'")
    				    )
    				        matches = content.equalsIgnoreCase(t.content);
    				    else if( type==Token.OPERATION )
    				        matches = content.equalsIgnoreCase(t.content)
    				                  || "/".equals(content) && ";".equals(t.content)
                                      || ";".equals(content) && "/".equals(t.content)
    				        ;
    				    /*else if( type==Token.DIGITS
    				            || type==Token.LINE_COMMENT 
                                || type==Token.COMMENT 
                                || type==Token.WS    
                                || type==Token.QUOTED_STRING    
                                || type==Token.DQUOTED_STRING
    				           ) ;
    				    */
    				    
    				    
    				}
                    
    				if( !matches ) {
    					match = null;
    					break;
    				}
    			}
    		} 
        	if( match != null )
        		break;
        }
        if( match == null ) {
        	target.add(elem);
        } else {
        	if( elem.le(match) ) {
                //if( match.toString().contains("UNION ALL") && match.toString().startsWith("WITH") )
                    //target = target;
        		target.remove(match);
        		target.add(elem);
        	}
        }
    }
    
    static void generateTemplate( ParseNode root, List<LexerToken> src, final String fragment, Map<Long,Set<Substr>> templates,
    		long weight, final Manual man, final String pageParagraph ) {
        for( ParseNode n : root.descendants() ) {
            if( n.to-n.from < 3 )
                continue;
            if( n.to-n.from == 3 && ".".equals(src.get(n.from+1).content) )
                continue;
            for( int s : n.content() ) {
                String symb = SqlEarley.getInstance().allSymbols[s];
				if( symb.charAt(0)=='\'' 
                 || symb.contains("[")
                 || symb.equals("iteration_scheme") //$NON-NLS-1$
                 || symb.equals("select_clause") //$NON-NLS-1$
                 || symb.equals("DECLARE_decls_opt") //$NON-NLS-1$
                 || symb.equals("subprg_spec") //$NON-NLS-1$
                 || symb.equals("with_clause") //$NON-NLS-1$
                 || symb.equals("table_reference") //$NON-NLS-1$
               )
                    continue;
				
	            ParseNode parent = n.parent();
	            if( parent != null ) {
	                for( int j : parent.content() ) {
	                    long key = Service.lPair(s, j);

	                    Set<Substr> tmp = templates.get(key);
	                    if( tmp == null ) {
	                        tmp = new HashSet<Substr>(); 
	                        templates.put(key,tmp);
	                    }
	                    DocURL url = null;
	                    if( man != null )
	                        url = new DocURL(man,pageParagraph);
	                    if( symb.equals("aggregate_function") )
	                        weight = -1;
	                    Substr.add(tmp, new Substr(src.get(n.from).begin,src.get(n.to-1).end,fragment.intern(),weight,url));
	                }
	            } else {
	                long key = Service.lPair(s, 0);
	                Set<Substr> tmp = templates.get(key);
	                if( tmp == null ) {
	                    tmp = new HashSet<Substr>(); 
	                    templates.put(key,tmp);
	                }
	                DocURL url = null;
	                if( man != null )
	                    url = new DocURL(man,pageParagraph);
	                if( symb.equals("aggregate_function") )
	                    weight = -1;
	                Substr.add(tmp, new Substr(src.get(n.from).begin,src.get(n.to-1).end,fragment.intern(),weight,url));	                
	            }
            }
        }
    }

	public static void listSuggestions( Set<Long> symbols, String prEfIx, String follower, Map<Long, Set<Substr>> templates, Set<Substr> topSuggestions, int limit ) {
		for( Long ls : symbols ) {
            Set<Substr> set = templates.get(ls);
            if( set == null )
            	continue;
			for( Substr ts : set ) {
				String tsl = ts.cutSrc().toLowerCase();
				Substr tsc = ts;
				if( !tsl.startsWith(prEfIx.toLowerCase()) )
                    continue;
				if( follower != null ) {
					List<LexerToken> src = LexerToken.parse(tsl);
					if( src.size() < 2 )
						continue;
					if( !src.get(1).content.startsWith(follower) )
						continue;
					else
						tsc = new Substr(ts.from+src.get(1).begin,ts.to,ts.src,ts.weight,ts.doc);
				}
				Substr max = null;    
                for( Substr tmp : topSuggestions ) {
                    if( max == null || max.le(tmp) )
                        max = tmp;
                }
                if( topSuggestions.size() < limit || tsc.le(max) ) {
                	Substr.add(topSuggestions, tsc);
                    if( limit < topSuggestions.size() )
                        topSuggestions.remove(max);                    
                }
            }
        }
	}
	
	public static void main( String[] args ) {
        Map<Long, Set<Substr>> templates = HarvestDoc.getTemplates();
        for( long key : templates.keySet() ) {
            Set<Substr> ts = templates.get(key);
            for( Substr s : ts )
                if( s.toString().startsWith("WHI") ) {
                    System.out.println(Service.lX(key)+"="+SqlEarley.getInstance().allSymbols[Service.lX(key)]);
                    System.out.println(Service.lY(key)+"="+SqlEarley.getInstance().allSymbols[Service.lY(key)]);
                    System.out.println(" "+s.toString());
                }
        }
    }
}
