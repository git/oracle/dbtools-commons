/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.plsql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import oracle.dbtools.app.Format;
import oracle.dbtools.arbori.SqlProgram;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.util.Service;

/**
 * Parses PL/SQL compilation unit
 * Applications: pldoc, swagger rest apis
 */
public class CompilationUnit {
	
	public static void main( String[] args ) throws Exception {
    	/*DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
    	final Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@gbr30060.uk.oracle.com:1521/DB12PERF", "hr", "hr");
    	*/
    	/*Map<String,CompilationUnit> docs = getPlsqlDocs("HR",conn);
    	int cut = 0;
    	for( String obj : docs.keySet() ) {
    		if( 5 < cut++ ) {
    			System.out.println("...");
    			break;
    		}
    		System.out.println(obj+":");
    		System.out.println("==============");
    		System.out.println(docs.get(obj));
    	}*/
    	//System.out.println(getPlsqlDoc("HR","wpg_docload".toUpperCase(),conn));
		String input = Service.readFile(CompilationUnit.class, "customer_data.pkgspec"); //$NON-NLS-1$
		CompilationUnit obj = new CompilationUnit(input);
		System.out.println(obj.toString());
	}
	
	
	
	public CompilationUnit( String input ) throws IOException {
		this.input = input;
		parse();
	}
	
	public CompilationUnit( String schema, String object, Connection conn ) throws Exception {
		StringBuilder ret = new StringBuilder();
		PreparedStatement stmt = conn.prepareStatement("select text from all_source \n" //$NON-NLS-1$
				+ "where owner = :owner and name = :name \n"                            //$NON-NLS-1$
				+ "and type in ('PACKAGE','PROCEDURE','FUNCTION') \n"                   //$NON-NLS-1$
				+ "order by line ");                                                    //$NON-NLS-1$
		stmt.setString(1, schema);
		stmt.setString(2, object);
		ResultSet rs = stmt.executeQuery();
        while( rs.next() ) {
        	ret.append(rs.getString(1));   
        	//ret.append("\n");   
        }
        rs.close();
        stmt.close();
        this.input = ret.toString();
		parse();
	}
	
	/**
	 * Get package level comment 
	 * Make sure it is not a comment for the first package member 
	 * Clean up comment demarcation symbols
	 * e.g. customer_data.pkgspec would return 
     * Project:         Test Project (<a href="http://pldoc.sourceforge.net">PLDoc</a>)<br/>
     * Description:     Customer Data Management<br/>
	 */
	public String getHeader() {
		/*if( headerStart == -1 )
			return "";
		StringBuilder ret = new StringBuilder();
		String hdr = input.substring(headerStart,headerEnd);
		List<LexerToken> tmp = LexerToken.parse(hdr, true);
        for ( LexerToken t : tmp ) {
            if( t.type == Token.WS )
                ret.append(t.content);
            else if( t.type == Token.COMMENT )
                ret.append(t.content.substring(2,t.content.length()-2));
            else if( t.type == Token.LINE_COMMENT )
                ret.append(t.content.substring(2));
        }
        return ret.toString();*/
		for( LexerToken t: fullCode ) {
        	if( t.type != Token.COMMENT )
        		continue;
        	if( 0 < t.content.indexOf("@headcom") ) //$NON-NLS-1$
        		return t.content;
		}
		return null;
	}
	
	/**
	 * @return  members (types, functions, procedures) indexed by char position in the <b>input</b>
	 */
	public Map<Integer,Member> getMembers() {
        return members;
	}
	
	/**
	 * @param schema        collection of doc for entire schema
	 * @throws SQLException 
	 */
	public static Map<String,CompilationUnit> getPlsqlDocs( String schema, Connection conn ) throws Exception {
		Map<String,CompilationUnit> ret = new HashMap<String,CompilationUnit>();
		PreparedStatement stmt = conn.prepareStatement("select object_name from all_objects \n" //$NON-NLS-1$
				+ "where owner = :owner and object_type in ('PACKAGE','PROCEDURE','FUNCTION')"); //$NON-NLS-1$
		stmt.setString(1, schema);
		ResultSet rs = stmt.executeQuery();
        while( rs.next() ) {
        	String name = rs.getString(1);
        	ret.put(name, null);   //$NON-NLS-1$
        }
        rs.close();
        stmt.close();
        for( String name : ret.keySet() )
        	ret.put(name, new CompilationUnit(schema, name, conn));
        return ret;
	}
	
	
	
		
    @Override
	public String toString() {
    	StringBuilder ret = new StringBuilder();
		ret.append(getHeader());
		ret.append("\n");
    	for( int charOffset : getMembers().keySet() ) {
    		ret.append(getMembers().get(charOffset).toString());
    		ret.append("\n");
    		ret.append("-------------------------------------\n");
    	}
		return ret.toString();
	}


    
    
    
    
    
    
    
    /////////////// Implementation details beyond this point; please ignore ///////////////
	
    
    
    
		
	private static final String path = "/oracle/dbtools/plsql/"; //$NON-NLS-1$
    private static SqlProgram programInstance = null;
	private void parse() throws IOException {
        if( programInstance == null )
            programInstance = new SqlProgram(Service.readFile(Format.class, path+"pldoc.arbori")); //$NON-NLS-1$
        
        fullCode = LexerToken.parse(input, true);
        List<LexerToken> src1 = new LinkedList<LexerToken>();
        for (LexerToken t : fullCode)
            if (t.type != Token.WS && t.type != Token.COMMENT && t.type != Token.LINE_COMMENT)
                src1.add(t);
		
        SqlEarley earley = SqlEarley.getInstance();
        Matrix matrix = new Matrix(earley);
        earley.parse(src1, matrix); 
        ParseNode root = earley.forest(src1, matrix);
        //root.printTree();
        
        target = new Parsed(input, src1, root);       
        programInstance.eval(target, this);
        
        // link comments to basic_decl_item s
        for( LexerToken t : fullCode ) {
        	if( t.type != Token.COMMENT && t.type != Token.LINE_COMMENT)
        		continue;
        	if( t.content.contains("@headcom") ) //$NON-NLS-1$
        		continue;
        	boolean embeddedComment = false;
        	int closestDeclOffset = Integer.MAX_VALUE;
        	for( int charOffset : members.keySet() ) {
        		Member cmp = members.get(charOffset);
        		if( charOffset <= t.begin && t.end <= charOffset+cmp.code.length() ) {
        			embeddedComment = true;
        			break;
        		}
        		if( charOffset < t.begin )
        			continue;
        		if( charOffset < closestDeclOffset ) 
        			closestDeclOffset = charOffset;
        	}
        	if( embeddedComment )
        		continue;
        	if( closestDeclOffset == Integer.MAX_VALUE )
        		continue;
        	members.get(closestDeclOffset).setComment(t.content);
        }

	}

	private String input = null;	
	private List<LexerToken> fullCode = null;
	private Parsed target = null;
		
	//////////////////////////// Callbacks: ////////////////////////////
	int headerStart = -1;
	int headerEnd = -1;
    void headerLocation( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(tuple.toString());
        // Less verbose:
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));

        ParseNode is = tuple.get("is_or_as");
        headerStart = target.getSrc().get(is.from).end;
        //ParseNode declList = tuple.get("basic_decl_item_list");
        headerEnd = target.getSrc().get(is.to).begin;
        
    }		
    
    Map<Integer,Member> members = new TreeMap<Integer,Member>();  // charOffset-> 
    void type( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        
        ParseNode decl = tuple.get("ty_d"); //$NON-NLS-1$
        String name = target.getSrc().get(decl.from+1).content;
        String code = target.getInput().substring(target.getSrc().get(decl.from).begin, target.getSrc().get(decl.to).begin);
        members.put(target.getSrc().get(decl.from).begin/*same begin offset as dasic_decl_item*/, new Type(name, code));
    }
    
    void procedure( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        
        ParseNode decl = tuple.get("subprg_spec"); //$NON-NLS-1$
        String name = target.getSrc().get(decl.from+1).content;
        String code = target.getInput().substring(target.getSrc().get(decl.from).begin, target.getSrc().get(decl.to).begin);
        members.put(target.getSrc().get(decl.from).begin/*same begin offset as dasic_decl_item*/, new Procedure(name, code));
    }
    
    void function( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        
        ParseNode decl = tuple.get("subprg_spec");  //$NON-NLS-1$
        String name = target.getSrc().get(decl.from+1).content;
        String code = target.getInput().substring(target.getSrc().get(decl.from).begin, target.getSrc().get(decl.to).begin);
        members.put(target.getSrc().get(decl.from).begin/*same begin offset as dasic_decl_item*/, new Function(name, code));
    }

    void parameter( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        
        ParseNode decl = tuple.get("basic_decl_item");  //$NON-NLS-1$
        Member member = members.get(target.getSrc().get(decl.from).begin);
        ParseNode prm = tuple.get("prm_spec");  //$NON-NLS-1$
        String name = target.getSrc().get(prm.from).content;
        int charOffset = target.getSrc().get(prm.from).begin;
		String code = target.getInput().substring(charOffset, target.getSrc().get(prm.to).begin);
        ParseNode type = tuple.get("datatype");  //$NON-NLS-1$
		String deflt = null;
		if( prm.to != type.to)
			deflt = target.getInput().substring(
					target.getSrc().get(type.to).begin, 
					target.getSrc().get(prm.to).begin
			);
        member.addParam(charOffset, 
        		        new Parameter(name,target.getSrc().get(type.from).content,
        		        mode(code,"IN"),mode(code,"OUT"),mode(code,"NOCOPY"),deflt) //$NON-NLS-1$
        );
    }

    private static boolean mode( String code, String mode ) {
    	return 0 < code.toLowerCase().indexOf(mode.toLowerCase());
    }
    
    void ret( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        
        ParseNode decl = tuple.get("basic_decl_item");  //$NON-NLS-1$
        Member member = members.get(target.getSrc().get(decl.from).begin);
        ParseNode ret = tuple.get("func_return");  //$NON-NLS-1$
        int charOffset = target.getSrc().get(ret.from).begin;
        member.addParam(charOffset, 
        		        new Parameter(null,target.getSrc().get(ret.from).content,
        		        false,true,false,null) //$NON-NLS-1$
        );
    }

    void fld( Parsed target, Map<String,ParseNode> tuple ) {
        //System.out.println(MaterializedPredicate.tupleMnemonics(tuple,target.getSrc()));
        
        ParseNode decl = tuple.get("basic_decl_item");  //$NON-NLS-1$
        Member member = members.get(target.getSrc().get(decl.from).begin);
        ParseNode field = tuple.get("field");  //$NON-NLS-1$
        String name = target.getSrc().get(field.from).content;
        int charOffset = target.getSrc().get(field.from).begin;
        ParseNode type = tuple.get("datatype");  //$NON-NLS-1$
		String datatype = target.getInput().substring(
				target.getSrc().get(type.from).begin, 
				target.getSrc().get(type.to-1).end
		);
		String deflt = null;
		if( field.to != type.to)
			deflt = target.getInput().substring(
					target.getSrc().get(type.to).begin, 
					target.getSrc().get(field.to).begin
			);
        member.addParam(charOffset, 
        		        new Parameter(name,datatype,
        		        false,false,false,deflt) //$NON-NLS-1$
        );
    }

}
