/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.plsql;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Member {
	public String name;
	public String code;
	public Map<Integer, Parameter> parameters = new TreeMap<Integer, Parameter>();  // indexed by char position in the <b>input</b>
	public String comment;
	
	public Member( String name, String code ) {
		this.name = name;
		this.code = code;
	}
	
	public void addParam( int charPos, Parameter prm ) {
		parameters.put(charPos, prm);
	}
	
	public void setComment( String comment ) {
		if( parameters != null) {
			for( int charPos : parameters.keySet() ) {
				Parameter prm = parameters.get(charPos);
				String tag = "@param"+" "+prm.name;  //$NON-NLS-1$
				int start = comment.indexOf(tag);
				if( 0 < start )
					prm.comment = comment.substring(start+tag.length(),comment.indexOf("\n",start));
			}
		}
	    comment = comment.replace("/**", "");
	    comment = comment.replace("/*", "");
		String tag = "@param";  //$NON-NLS-1$
		int start = comment.indexOf(tag);
		if( 0 < start )
			comment = comment.substring(0,start);
	    comment = comment.replace("*/", "");
		this.comment = comment;		
	}

	@Override
	public String toString() {
		String ret = "name="+name+", comment="+comment;
		if( parameters != null) {
			ret = ret +"\n";
			for( int charPos : parameters.keySet() ) {
				ret = ret + "charPos="+charPos + ",parameter={"+parameters.get(charPos).toString()+"}\n";
			}
		}
		return ret;
	}
	
	
}

