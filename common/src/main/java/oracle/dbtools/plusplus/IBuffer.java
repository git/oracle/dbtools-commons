/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.plusplus;

import java.util.ArrayList;
import java.util.List;

public interface IBuffer extends Clearable, Deletable, FileSystem, Listable, Editable, TerminalPosition {

	public String setCurrentLine(int lineNumber);

	public int getCurrentLine();

	public int linesInBuffer();

	/**
	 * Get the current prompt.
	 * 
	 * @return
	 */
	public String getPrompt();

	public String getPrompt(int line);

	/**
	 * Get the whole buffer as a String.
	 * 
	 * @return the buffer String
	 */
	public String getBuffer();

	public void resetBuffer(List<String> bufferList);
	public void resetBuffer(String stringList);

	/**
	 * Give me the raw text of the buffer line.
	 * 
	 * @param lineNumber
	 *            to get assuming there is one.
	 * @return the strig at that line
	 */
	public String getLine(int lineNumber);

	/**
	 * Move the pointer to the previous element in the buffer.
	 *
	 * @return true if we successfully went to the previous element
	 */
	public boolean previous();

	/**
	 * Move the pointer to the next element in the buffer.
	 *
	 * @return true if we successfully went to the next element
	 */
	public boolean next();

	/**
	 * get the size of the buffer.
	 * 
	 * @return
	 */
	public int size();

	/**
	 * Return the list of lines in the buffer
	 * 
	 * @return {@link ArrayList} of Strings
	 */
	public ArrayList<String> getBufferList();

	/**
	 * When we return history and move the cursor, we need to set editing on
	 * again.
	 */
	public void startEditingAfterHistory();

	public boolean isEmpty();

	/**
	 * When we have a buffer, sometimes, we need to keep a buffer for using
	 * later after this command or statement.
	 * 
	 * @param bufferList
	 */
	public void setBufferSafe(ArrayList<String> bufferList);
	
	public IBuffer getBufferSafe();
	
	public String getCurrentBufferState(String line);

	public int getCursorLocation(int cursor);
	
	/**
	 * get Cursor position on the line.
	 * @return
	 */
	public int getCursor() ;

	public String getCursorContents() ;
	/**
	 * @param _readLineBuffer the _readLineBuffer to set
	 */
	public void setReadLineBuffer(Object buffer) ;
	/**
	 * Get the buffer as a string without line feeds
	 * @return
	 */
	public String getBufferString();

	/**
	 * This will get us the buffer position in raw numbers based on the cursor position on the current line
	 * @param currentLine
	 * @param cursor
	 * @return
	 */
	public int getBufferCursor(int currentLine, int cursor);
	
	/**
	 * In SQLPlus, any buffer which executed with / on the end needs cleaned and sometimes this needs to be set up
	 */
	public void cleanSlashFromBuffer() ;
}
