/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.plusplus;

/**
 * * LIST |; or L |lists all lines in the SQL buffer<br>
 * LIST n |L n or n |lists line n<br>
 * LIST * |L * |lists the current line<br>
 * LIST n * |L n * |lists line n through the current line<br>
 * LIST LAST|L LAST |lists the last line<br>
 * LIST m n |L m n |lists a range of lines (m to n)<br>
 * LIST * n |L * n |lists the current line through line n<br>
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Listable.java"
 *         >Barry McGillin</a>
 *
 */
public interface Listable {
	
	/**
	 * lists all lines in the SQL buffer as it is
	 * 
	 * @return @link {@link String string} to list
	 */
	public String list();
	
	/**
	 * lists all lines in the SQL buffer
	 * 
	 * @return @link {@link String string} to list
	 */
	public String list(boolean trim);

	/**
	 * Lists line n
	 * 
	 * @param {@link Integer} n
	 * @return {@link String string} to list
	 */
	public String list(int n,boolean trim);

	/**
	 * lists the current line
	 * 
	 * @return {@link String string} to list
	 */
	public String listStar(boolean trim);

	/**
	 * Lists line n through the current line<br>
	 * Also l n *
	 * 
	 * @param {@link Integer} n
	 * @return {@link String string} to list
	 */
	public String listNStar(int n,boolean trim);

	/**
	 * List the last line
	 * 
	 * @return {@link String string} to list
	 */
	public String listLast(boolean trim);

	/**
	 * lists a range of lines (m to n)
	 * 
	 * @param {@link Integer} m
	 * @param {@link Integer} n
	 * @return {@link String string} to list
	 */
	public String list(int m, int n,boolean trim);

	/**
	 * lists the current line through line n
	 * 
	 * @param {@link Integer} n
	 * @return {@link String string} to list
	 */
	public String listStarN(int n,boolean trim);
}
