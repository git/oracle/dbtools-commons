/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.backgroundTask;

/**
 * Task Status (state) values
 */
public enum IRaptorTaskStatus {

    /** The runnable state. */
    RUNNABLE() {
        public String value() {
            return Messages.getString("IRaptorTaskStatus.0"); //$NON-NLS-1$
        }
    },

    /** The pause state. */
    PAUSED() {
        public String value() {
            return Messages.getString("IRaptorTaskStatus.2"); //$NON-NLS-1$
        }
    },

    /** The finished state. */
    FINISHED() {
        public String value() {
            return Messages.getString("IRaptorTaskStatus.4"); //$NON-NLS-1$
        }

        @Override
        public boolean isAlive() {
            return false;
        }
    },

    /** The new state. */
    NEW() {
        public String value() {
            return Messages.getString("IRaptorTaskStatus.5"); //$NON-NLS-1$
        }
    },

    /** The fail state. */
    FAILED {
        public String value() {
            return Messages.getString("IRaptorTaskStatus.6"); //$NON-NLS-1$
        }

        @Override
        public boolean isAlive() {
            return false;
        }
    };

    /**
     * @return the NLS translated status value
     */
    public abstract String value();

    /**
     * @return true if the task has not terminated
     */
    public boolean isAlive() {
        return true;
    }

    /**
     * @param stat
     * @return true if the task is new or runnable or paused
     * @obsolete @see {@link #isAlive()}
     */
    public static boolean isAlive(IRaptorTaskStatus stat) {
        if (stat == NEW || stat == RUNNABLE || stat == PAUSED)
            return true;
        else
            return false;
    }

}
