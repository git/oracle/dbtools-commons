/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.backgroundTask;

import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

import oracle.dbtools.raptor.backgroundTask.ui.ITaskUI;

/**
 * This interface will put distance between the taskdialog from the UI and
 * RaptorTask Manager We need to register one of these with the task manager in
 * order to show feedback etc. Otherwise, there will be no feedback.
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ITaskDialog.java"
 *         >Barry McGillin</a>
 * 
 */
public interface ITaskDialog {

	public static final String RUN_IN_BACKGROUND = "run_in_background"; //$NON-NLS-1$

	public static final String CANCEL_TASK = "cancel_task"; //$NON-NLS-1$

	public static final String CLOSE_TASK = "close_dialog"; //$NON-NLS-1$

	/**
	 * Block UI, wait .5s (task has already been submitted), then ublock UI and
	 * if task is still running, show dialog showDelayedDialog
	 * 
	 * @param task
	 *            ITaskDialog
	 */
	public void showDelayedDialog(final RaptorTask<?> task);

	/**
	 * Set a listener on the run in background button and the cancel button.
	 * setButtonListener
	 * 
	 * @param listener
	 *            ITaskDialog
	 */
	public void setButtonListener(ActionListener listener);

	/**
	 * Add a listener for closing the dialog. setWindowCloseListener
	 * 
	 * @param l
	 *            ITaskDialog
	 */
	public void setWindowCloseListener(WindowListener l);

	/**
	 * Set the task dialog visible setVisible
	 * 
	 * @param visible
	 *            ITaskDialog
	 */
	public void setVisible(final boolean visible);

	/**
	 * Is the task dialog visiible isVisible
	 * 
	 * @return ITaskDialog
	 */
	public boolean isVisible();

	/**
	 * Is the dialog vdisplayable? isDisplayable
	 * 
	 * @return ITaskDialog
	 */
	public boolean isDisplayable();

	/**
	 * Close the window and get rid of everything dispose ITaskDialog
	 */
	public void dispose();

	/**
	 * createNewDialog will allow this class to create a new version of it self
	 * with the parameters given and return it to the framework for use
	 * 
	 * @param defaultUI 
	 * @param task to run
	 * @param runBkGrnd backgroundable?
	 * @return ITaskDialog new ITaskDialog
	 */
	public ITaskDialog createNewDialog(ITaskUI defaultUI, RaptorTask<?> task, boolean runBkGrnd);
}
