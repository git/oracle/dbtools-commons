/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.backgroundTask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.SwingUtilities;

import oracle.dbtools.raptor.backgroundTask.IRaptorTaskRunMode;
import oracle.dbtools.raptor.backgroundTask.RaptorTask;
import oracle.dbtools.raptor.backgroundTask.RaptorTaskAdapter;
import oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent;
import oracle.dbtools.raptor.backgroundTask.TaskException;
import oracle.dbtools.util.Logger;

/**
 * 
 * RaptorMultiTask - A RaptorTask to run a collection of RaptorTasks
 * 
 * By definition, a RaptorMultiTask is considered deterministic. That is,
 * its progress is defined as <i>tasks complete</i> / <i>task collection size</i>.
 * 20160413 Add constructor to control isDeterministic - some usages e.g., CopyToOracle
 * need to use indeterminate mode.
 * 
 * Depending on the nature of your process, you can either create a 
 * RaptorMultiTask and add tasks to it or override to have doWork add the
 * tasks before calling super.doWork. 
 *  
 * Results for individual tasks can be determined by either keeping a 
 * reference to them or adding a listener to this task 
 * 
 * multiTask.getDescriptor().addListener(new RaptorTaskAdapter() { ... });
 * 
 * and using
 * 
 * List<T> getCancelledTasks()
 * List<T> getFailedTasks()
 * List<T> getFinishedTasks()
 * 
 * See DeferUIOperationTask for an example of how to add a listener to 
 * perform UI operations when the task is finished 
 * 
 * @author <a href="mailto:brian.jeffries@oracle.com?subject=oracle.dbtools.raptor.backgroundTask.RaptorMultiTask">Brian Jeffries</a>
 * @since SQL Developer 4.1
 */
public class RaptorMultiTask<T extends RaptorTask<?>> extends RaptorTask<Void> {

    /**
     * @param name The Task/Thread name
     * @param mode MODAL, MODAL_OPTIONAL, TASKVIEWER, IDE_STATUSBAR, NO_GUI
     */
    public RaptorMultiTask(final String name, final IRaptorTaskRunMode mode) {
        super(name, false, mode);
    }

    /**
     * @param name The Task/Thread name
     * @param isInDeterminate
     * @param mode MODAL, MODAL_OPTIONAL, TASKVIEWER, IDE_STATUSBAR, NO_GUI
     */
    public RaptorMultiTask(String name, boolean isInDeterminate, IRaptorTaskRunMode mode) {
        super(name, isInDeterminate, mode);
    }

    /**
     * Add tasks to be run by this one
     * (the collection is copied)  
     */
    public void addTasks(final Collection<T> tasks) {
        for (T task : tasks) {
            task.getDescriptor().addListener(_taskListener);
            _queue.add(task);
            _taskCount.incrementAndGet();
        }
    }

    /**
     * Add a task to be run by this one 
     */
    public void addTask(final T task) {
        addTasks(Collections.singleton(task));
    }
    
    /**
     * Set the number of tasks to run concurrently
     */
    public void setParallelism(final int parallelism) {
        _parallelism.set(parallelism);
    }
    
    /**
     * Get the number of tasks to run concurrently
     */
    public int getParallelism() {
        return _parallelism.get();
    }
    
    /**
     * @return the list of cancelled tasks
     */
    public List<T> getCancelledTasks() {
        return _cancelled;
    }
    
    /**
     * @return the list of failed tasks
     */
    public List<T> getFailedTasks() {
        return _failed;
    }
    
    /**
     * @return the list of finished tasks
     */
    public List<T> getFinishedTasks() {
        return _finished;
    }
    
    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.backgroundTask.RaptorTask#doWork()
     */
    @Override
    protected Void doWork() throws TaskException {
        try {
            while (!isFinished()) {
                checkCanProceed();
                processQueue();
                // update progress only when child tasks complete
                //setProgress();
                // Don't appear to hog cpu
                Thread.sleep(250); // 1/4 second
            }
        }
        catch (Throwable t) {
            Logger.severe(getClass(), getDescriptor().getName(), t);
            throw new TaskException(t);
        }
        if (!getFailedTasks().isEmpty()) {
        	T task = getFailedTasks().get(0);
        	Throwable t = task.getDescriptor().getThrowable();
        	String msg = "Child task(s) failed. "; // $NON-NLS-1$
        	if (t != null) {
        		throw new TaskException(msg+t.getMessage(), t);
        	} else {
        		throw new TaskException(msg);
        	}
        }
        return null;
    }


    
    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.backgroundTask.RaptorTask#cancel()
     */
    @Override
    public boolean cancel() {
        boolean cancelled = true;
        for (T task : _active.values()) {
            // run through them all and pick up if any of them say no
            if (!task.requestCancel()) {
                cancelled = false;
            }
        }
        return cancelled;
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.backgroundTask.RaptorTask#pause()
     */
    @Override
    protected void pause() {
        pauseActiveTasks();
        super.pause();
        resumeActiveTasks();
    }

    /**
     * Make a good faith effort to pause active tasks
     * They can ignore this and success is dependent on how often they 
     * call checkCanProceed internally 
     */
    private void pauseActiveTasks() {
        for (T task : _active.values()) {
            task.requestPause();
        }
    }

    private void resumeActiveTasks() {
        for (T task : _active.values()) {
            task.resume();
        }
    }

    // All this concurrent variable typing rather than 'plain' synchronized
    // in preparation for dynamic/recursive addTask[s], setParallelism
    private BlockingQueue<T> _queue = new LinkedBlockingQueue<>();
    private Map<RaptorTaskDescriptor,T> _active = Collections.synchronizedMap(new HashMap<RaptorTaskDescriptor,T>());
    private List<T> _cancelled = Collections.synchronizedList(new ArrayList<T>());
    private List<T> _failed = Collections.synchronizedList(new ArrayList<T>());
    private List<T> _finished = Collections.synchronizedList(new ArrayList<T>());
    private AtomicInteger _parallelism = new AtomicInteger(4);
    private AtomicInteger _taskCount = new AtomicInteger(0);
    private IRaptorTaskListener _taskListener = new RaptorMultiTaskAdapter();

    private boolean isFinished() {
        // processed everything AND accounted for everything
        // avoid race condition
        return _queue.isEmpty() && _active.isEmpty() &&
                (_taskCount.get() == _cancelled.size() + _failed.size() + _finished.size());
    }
    

    /**
     * Submit a queued task to the RaptorTaskManager if a slot is available
     */
    private void processQueue() {
        if (_active.size() <= getParallelism()) {
            final T task = _queue.poll();
            if (task != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        RaptorTaskManager.getInstance().addTask(task);
                    }
                });
                _active.put(task.getDescriptor(), task);
            }
        }
    }
    
    private void setProgress() {
        double units = 100 * (1 - ( (double)(_active.size() + _queue.size()) / _taskCount.get() ) );
        getDescriptor().setProgress((int)units);
    }
    
    
    /**
     * 
     * RaptorMultiTaskAdapter - listener for tasks run by this one
     *
     * @author <a href="mailto:brian.jeffries@oracle.com?subject=oracle.dbtools.raptor.backgroundTask.RaptorMultiTaskAdapter">Brian Jeffries</a>
     * @since SQL Developer 4.1
     */
    class RaptorMultiTaskAdapter extends RaptorTaskAdapter {

        /* (non-Javadoc)
         * @see oracle.dbtools.raptor.backgroundTask.RaptorTaskAdapter#taskCancelled(oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent)
         */
        @Override
        public void taskCancelled(final RaptorTaskEvent event) {
            // Free slot for new task
            T task = _active.remove(event.getTaskDescriptor());
            if (task != null) {
                _cancelled.add(task);
            }
            setProgress();
        }

        /* (non-Javadoc)
         * @see oracle.dbtools.raptor.backgroundTask.RaptorTaskAdapter#taskFailed(oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent)
         */
        @Override
        public void taskFailed(final RaptorTaskEvent event) {
            // Free slot for new task
            T task = _active.remove(event.getTaskDescriptor());
            if (task != null) {
                _failed.add(task);
            }
            setProgress();
        }

        /* (non-Javadoc)
         * @see oracle.dbtools.raptor.backgroundTask.RaptorTaskAdapter#taskFinished(oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent)
         */
        @Override
        public void taskFinished(final RaptorTaskEvent event) {
            // Free slot for new task
            T task = _active.remove(event.getTaskDescriptor());
            if (task != null) {
                _finished.add(task);
            }
            setProgress();
        }
    }
}
