/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.backgroundTask;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.naming.OperationNotSupportedException;

/**
 * The base class for creating tasks that can be run in background. The clients
 * must extend <code>RaptorTask</code> and provide implementation for the
 * <code>doWork()</code> method.
 * <p>
 * A task can be paused in middle of it's execution. By default a task is not
 * pausable. If you want to allow pausing, do task.setPausable(true)
 * before submitting the task for execution. Another thread can pause a task by
 * calling requestPause(). This call just marks the thread for pausing. Actual
 * pausing will happen when a call to checkCanProceed() is made in
 * <code>doWork()</code> method. This implies that it's up to to clients to
 * decide that at what stage of execution they allow the task to be paused. A
 * paused task can be resumed by calling resume(). Typically, you should not be
 * pausing a task programmatically, but leave it to the user to trigger it
 * through a GUI widget.
 * </p>
 * <p>
 * A task could be canceled before it finishes the execution. By default a task
 * is cancellable. If you do not want to allow canceling a task, do
 * task.setCancellable(false) before the task is submitted for execution.
 * Another thread can cancel a task by calling requestCancel(). This call just
 * marks the thread for canceling. Actual cancellation will happen when a call
 * to checkCanProceed() is made in <code>doWork()</code> method. A task marked
 * for cancellation through these means would exit execution by throwing an
 * ExecutionException which wraps up a CancellationException when call to
 * checkCanProceed is made. This ExecutionException must be catched in doWork()
 * method and necessary cleanup must be performed before returning. This implies
 * that it's up to to clients to decide that at what stage of execution they
 * allow the task to be canceled.Typically, you should not be canceling a task
 * programmatically, but leave it to the user to trigger it through a GUI
 * widget.
 * </p>
 *
 * @author Barry McGillin, Abhimanu Kumar, Rakesh Rao, Raghvendra Saboo, Barry McGillin
 */
public abstract class RaptorTask<V> implements Callable<V> {

    /** The pausable variable. */
    private boolean _isPausable = false;

    /** The cancelable variable. */
    private boolean _isCancelable = true;

    /** The requested status. Can be PAUSE or FINISHED(cancel) */
    private IRaptorTaskStatus _requestedStatus = null;

    /** Pause lock. */
  private Object _pauseLock = new Object();

    /** The task descriptor. */
  private RaptorTaskDescriptor _raptorTaskDescriptor;

    /** The future object to get task result. */
    private Future<V> _future = null;
//    /** try holding the outcome so getResult can be called multiple times, but wipe _future **/
//    private V _outcome;
// nope, only allow once so we don't hold anything    
    private boolean _outcomeSet;

    private IRaptorTaskProgressUpdater _updater = new TaskProgressUpdater();

    /**
     * The Constructor with name.
     *
     * @param name
     *          the task name
     */
    public RaptorTask(final String name) {
        _raptorTaskDescriptor = new RaptorTaskDescriptor(name);
    }

    /**
     * The Constructor with indeterminate property variable.
     *
     * @param isInDeterminate
     *          true if progress is indeterminate
     * @param name
     *          the task name
     */
    public RaptorTask(final String name, final boolean isInDeterminate) {
        _raptorTaskDescriptor = new RaptorTaskDescriptor(name, isInDeterminate);
    }

    /**
     * The Constructor with indeterminate property variable & run mode
     *
     * @param isInDeterminate
     *          true if progress is indeterminate
     * @param name
     *          the task name
     * @param mode
     *          task run mode
     */
    public RaptorTask(final String name, final boolean isInDeterminate, final IRaptorTaskRunMode mode) {
        _raptorTaskDescriptor = new RaptorTaskDescriptor(name, isInDeterminate, mode);
    }

    @Override
    public String toString() {
        return _raptorTaskDescriptor.getName() + "_" + hashCode(); //$NON-NLS-1$
    }

    /**
     * Invoked when task cancel is to be done. User may override this if specific
     *
     * cancel operation needs to be performed in response to cancel. eg:
     * cancelling a JDBC statement call
     * @return true if request was successfully made
     */
    public final boolean requestCancel() {

        if (!isCancellable()) {
            return false;
        }

        _requestedStatus = IRaptorTaskStatus.FINISHED;
        setMessage(Messages.getString("RaptorTask.3")); //$NON-NLS-1$
        boolean cancelled = cancel();
        if (cancelled) {
            setMessage(Messages.getString("RaptorTask.0")); //$NON-NLS-1$
        } else {
            setMessage(Messages.getString("RaptorTask.5")); //$NON-NLS-1$
        }
        return cancelled;
    }

    public boolean cancel() {
        return true;
    }

    /**
     * Checks if task cancellable.
     *
     * @return true, if is cancellable
     */
    public boolean isCancellable() {
        return _isCancelable;
    }

    /**
     * @param cancellable
     *          false if this task is not cancellable. By default a task is
     *          cancellable.
     *
     */
    public void setCancellable(final boolean cancellable) {
        _isCancelable = cancellable;
    }

    /**
     * Checks if task cancellable.
     *
     * @return true, if is cancellable
     */
    protected boolean isPausable() {
        return _isPausable;
    }

    /**
     * @param pausable
     *          false if this task is not pausable. By default a task is pausable.
     */
    public void setPausable(final boolean pausable) {
        _isPausable = pausable;
    }

    /**
     * Invoked when task pause is to be done.
     */
    public final void requestPause() {

        if (!isPausable()) {
            return;
        }
        _requestedStatus = IRaptorTaskStatus.PAUSED;
        setMessage(Messages.getString("RaptorTask.6")); //$NON-NLS-1$
    }

    /**
     * (non-Javadoc).
     *
     * @see java.util.concurrent.Callable#call()
     */
    public final V call() throws Exception {
        setStatus(IRaptorTaskStatus.RUNNABLE);
        // We dont want any default message from framework. Task implementor needs to do it
//        getDescriptor().setMessage(Messages.getString("RaptorTask.1")); //$NON-NLS-1$
        V returnObj = null;
        try {
            returnObj = doWork();
        } catch (Exception e) {
            throw e;
        } finally {
            tearDown();
        }
        synchronized (this) {
            notify();
        }
        return returnObj;
    }

	/**
	* This method is intended to be overriden if there is a specific action the task should perform just before it completes
	* By default nothing is done
	*/
    protected void tearDown(){       
    }
    
    /**
     * Do actual task.
     *
     * @return the V
     *
     * @throws TaskException the task exception
     */
    protected abstract V doWork() throws TaskException;

    /**
     * Resume paused task.
     */
    public final void resume() {

        synchronized (_pauseLock) {
            _requestedStatus = IRaptorTaskStatus.RUNNABLE;
            _pauseLock.notify();
        }
    }

    /**
     * Gets the task status.
     *
     * @return the status
     */
    public final IRaptorTaskStatus getStatus() {
        return _raptorTaskDescriptor.getStatus();
    }

    /**
     * Gets the task descriptor.
     *
     * @return the descriptor
     */
    public final RaptorTaskDescriptor getDescriptor() {
        return _raptorTaskDescriptor;
    }

    /**
     * Sets the task status.
     *
     * @param status
     *          the status
     */
    /* friendly */void setStatus(final IRaptorTaskStatus status) {
        getDescriptor().setStatus(status);
    }

    public void setMessage(final String message) {
        getDescriptor().setMessage(message);
    }

    /**
     * Pause a task.
     */
    protected void pause() {
        synchronized (_pauseLock) {
            try {
                _raptorTaskDescriptor.setStatus(IRaptorTaskStatus.PAUSED);
                String oldMessage = getDescriptor().getMessage();
                setMessage(Messages.getString("RaptorTask.8")); //$NON-NLS-1$
                _pauseLock.wait();
                // revert back to old message
                setMessage(oldMessage);
                _raptorTaskDescriptor.setStatus(IRaptorTaskStatus.RUNNABLE);
            } catch (InterruptedException e) {
                // FIXME: what to do here ?
            }
        }
    }

    /**
     * Poll call to check if task can proceed to next stage. This is a call which
     * task implementor needs to make at every cancellable/pausable stage during
     * task execution.
     *
     * @throws ExecutionException
     *           the interrupted exception
     */
    protected void checkCanProceed() throws ExecutionException {
        if (_requestedStatus != null && _requestedStatus != _raptorTaskDescriptor.getStatus()) {
            switch (_requestedStatus) {
                case PAUSED:
                    pause();
                    break;
                case FINISHED:
                    getDescriptor().setCancelled(true);
                    throw new ExecutionException(new CancellationException(Messages.getString("RaptorTask.0"))); //$NON-NLS-1$
            }
        }
    }

    /**
     * Sets the future. Not intended to be used by users.
     *
     * @param f
     *          the future
     */
    /*friendly*/void setFuture(final Future<V> f) {
        _future = f;
    }

    /**
     * Waits if necessary for the computation to complete, and then retrieves its result.
     *
     * @return the result
     *
     * @throws ExecutionException
     *           the execution exception
     * @throws InterruptedException
     *           the interrupted exception
     */
    public V getResult() throws InterruptedException, ExecutionException {
        // RaptorTask is holding a circular reference since FutureTask references it 
        // while it references FutureTask to implement getResult
        // break the chain once done with it.
        if (_outcomeSet) {
            throw new ExecutionException(new OperationNotSupportedException("Can only call getResult once."));
            //return _outcome;
        }
        
        // task not yet scheduled
        while (_future == null && getStatus() == IRaptorTaskStatus.NEW) {
            synchronized (this) {
                wait(1000);
            }
        }
        // future being null should never arise as task manager sets it before execution begins
        if (_future == null) {
            throw new ExecutionException(Messages.getString("RaptorTask.10"), new NullPointerException()); //$NON-NLS-1$
        }
        try {
            V _outcome = _future.get();
            _outcomeSet = true;
            _future = null;
            return _outcome;
        } catch (CancellationException ce) {
            throw new ExecutionException(ce);
        }
    }

    public ISchedulingRule getSchedulingRule() {
        return null;
    }

    /**
     * @return the IRaptorTaskUpdater that can be passed to objects that are
     *         used in doWork() to interact with the task for setting message,
     *         progress and responding to pause/cancel requests
     */
    public IRaptorTaskProgressUpdater getRaptorTaskProgressUpdater() {
        return _updater;
    }

    private final class TaskProgressUpdater implements IRaptorTaskProgressUpdater {
        public void checkCanProceed() throws ExecutionException {
            RaptorTask.this.checkCanProceed();
        }

        public RaptorTaskDescriptor getDescriptor() {
            return RaptorTask.this.getDescriptor();
        }

        public boolean isCancellable() {
            return RaptorTask.this.isCancellable();
        }

        public boolean isPausable() {
            return RaptorTask.this.isPausable();
        }
    }

    /**
     * API that determines if a running task may be interrupted. If true calls Future.cancel(true).
     * For more details @see Future
     * 
     * By default, tasks are not interruptible.
     * 
     * Care should be taken when permitting a task to be interrupted. Interruption on underlying IO operations may result in unexpected 
     * behavior. Specifically, interrupting the execution of a JDBC statement with the Oracle 12.2 driver may result in the closing of the
     * socket (and thus the JDBC connection).
     * @return true if interruptible
     */
    protected boolean mayInterrupt() {
        // by default, don't interrupt; we may be calling external APIs that handle interrupts in unpredictable ways.
        return false;
    }
}
