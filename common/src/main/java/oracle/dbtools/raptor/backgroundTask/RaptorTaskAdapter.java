/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.backgroundTask;

/**
 * The Class RaptorTaskAdapter.
 */
public abstract class RaptorTaskAdapter implements IRaptorTaskListener {

  /**
   * Task cancelled. Subclasses may override. This method is not called on the event thread.
   * So any GUI operations should be done using SwingUtilities.invoke***
   *
   * @param event the event
   */
  public void taskCancelled(RaptorTaskEvent event) {
    //
  }

  /**
   * Task failed. Subclasses may override. This method is not called on the event thread.
   * So any GUI operations should be done using SwingUtilities.invoke***
   *
   * @param event the event
   */
  public void taskFailed(RaptorTaskEvent event) {
    //
  }

  /**
   * Task finished. Subclasses may override. This method is not called on the event thread.
   * So any GUI operations should be done using SwingUtilities.invoke***
   *
   * @param event the event
   */
  public void taskFinished(RaptorTaskEvent event) {
    //
  }

  /**
   * Task paused. Subclasses may override. This method is not called on the event thread.
   * So any GUI operations should be done using SwingUtilities.invoke***
   *
   * @param event the event
   */
  public void taskPaused(RaptorTaskEvent event) {
    //
  }

  /**
   * Task running. Subclasses may override. This method is not called on the event thread.
   * So any GUI operations should be done using SwingUtilities.invoke***
   *
   * @param event the event
   */
  public void taskRunning(RaptorTaskEvent event) {
    //
  }

  /**
   * Task scheduled. Subclasses may override. This method is almost certainly
   * called on the event thread.
   * @see RaptorTaskManager#addTask(RaptorTask, java.util.List, java.util.List)
   * which "must be run on the event thread" which calls submitTask, which sets
   * this state.
   * Note this is BEFORE the tasks executes so any long running process will
   * appear to hang the UI.
   *
   * @param event the event
   */
  public void taskScheduled(RaptorTaskEvent event) {
    //
  }

  /**
   * (non-Javadoc).
   * @see oracle.dbtools.raptor.backgroundTask.IRaptorTaskListener#messageChanged(oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent)
   */
  public void messageChanged(RaptorTaskEvent event) {
    //
  }

  /**
   * (non-Javadoc).
   * @see oracle.dbtools.raptor.backgroundTask.IRaptorTaskListener#progressChanged(oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent)
   */
  public void progressChanged(RaptorTaskEvent event) {
    //
  }

  /**
   * Refines a given task state into multiple api calls.
   * @see oracle.dbtools.raptor.backgroundTask.IRaptorTaskListener#stateChanged(oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent)
   */
  public void stateChanged(RaptorTaskEvent event) {
    switch (event.getTaskDescriptor().getStatus()) {
    case PAUSED:
      taskPaused(event);
      break;
    case RUNNABLE:
      taskRunning(event);
      break;
    case FINISHED:
      if (event.getTaskDescriptor().isCancelled()) {
          taskCancelled(event);
          break;
      }
      taskFinished(event);
      break;
    case NEW:
      taskScheduled(event);
      break;
    case FAILED:
      if (event.getTaskDescriptor().isCancelled()) {
          taskCancelled(event);
          break;
      }			
      taskFailed(event);
      break;
    default:
      throw new IllegalStateException(Messages.getString("RaptorTaskDescriptor.0") + event.getTaskDescriptor().getStatus()); //$NON-NLS-1$
    }
  }
}
