/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.backgroundTask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.Icon;

import oracle.dbtools.raptor.backgroundTask.ui.IImageRegistry;

/**
 * The implementation of IRaptorTaskDescriptor.
 */
public class RaptorTaskDescriptor {

  /**
   * The modifiable properties of a task which need to be communicated to task
   * listeners.
   */
  public static enum PROPERTY { NAME, MESSAGE, PROGRESS, STATUS }

  /** The task name, message. */
  private String _name, _message;

  /** The task icon. */
//  private Icon _taskIcon;

  /** The start, last progress time. */
  private long _startTime, _lastProgressTime;

  /** This can be used to override the elapse time which is normally calculated from the _startTime, _lastProgressTime**/
  private long _elapseTime = -1;
  
  /** The progress. */
  private int _progress;

  /** The status. */
  private IRaptorTaskStatus _status;

  /** The Run Mode. */
  private IRaptorTaskRunMode _runMode = IRaptorTaskRunMode.MODAL_OPTIONAL;

  /** The task listener list. */
  private List<IRaptorTaskListener> _listenerList;

  /** The indeterminate property. */
  private boolean _isInDeterminate = true;

  /** The exception which caused task failure. */
  private Throwable _throwable;

  /** The task cancelled flag. */
  private boolean m_isCancelled;

  /** Listsner lock object*/
  ReentrantLock _listenerLock = new ReentrantLock();

  /** post notifications to System.err */
  static boolean DEBUG_TASK_STATES = false;

  /**
   * The Constructor.
   *
   * @param taskName the task name
   */
  public RaptorTaskDescriptor(final String taskName) {
    _name = taskName;
    setStatus(IRaptorTaskStatus.NEW);
    _listenerList = new ArrayList<IRaptorTaskListener>();
  }

  /**
   * The Constructor.
   *
   * @param flagInDeterminate the indeterminate property
   * @param taskName the task name
   */
  public RaptorTaskDescriptor(final String taskName, final boolean flagInDeterminate) {
    this(taskName);
    _isInDeterminate = flagInDeterminate;
  }

  /**
   * @param taskName
   * @param flagInDeterminate
   * @param mode
   */
  public RaptorTaskDescriptor(final String taskName, final boolean flagInDeterminate, IRaptorTaskRunMode mode) {
    this(taskName,flagInDeterminate);
    _runMode = mode;
  }

  /**
   * Removes the task listener.
   *
   * @param listener the listener
   */
  public void removeListener(final IRaptorTaskListener listener) {
    synchronized (_listenerList) {
      _listenerList.remove(listener);
    }
  }
  
  void removeAllListeners() {
      synchronized (_listenerList) {
        _listenerList.clear();
      }
  }

  /**
   * Adds the task listener.
   *
   * @param listener the listener
   */
  public void addListener(final IRaptorTaskListener listener) {
    synchronized (_listenerList) {
      _listenerList.add(listener);
    }
  }

  /**
   * Notify listeners.
   *
   * @param field the property changed
   * @param oldVal the old value of property changed
   */
   void notifyListener(final PROPERTY field, final Object oldVal) {
    if (_startTime  > 0 && //Bug 10401343 - ea2: regression: elapsed time not the same in worksheet & script results tab
            (!(oldVal instanceof IRaptorTaskStatus) && !(_status == IRaptorTaskStatus.FINISHED || _status == IRaptorTaskStatus.FAILED))) {
          _lastProgressTime = System.currentTimeMillis();
    }
    if (_listenerList == null) {
      return;
    }
	try {
        _listenerLock.lock();
		synchronized (_listenerList) {

			RaptorTaskEvent raptorTaskEvent = new RaptorTaskEvent(this,
					System.currentTimeMillis());
			raptorTaskEvent.setChangedProperty(field, oldVal);
			raptorTaskEvent.setThrowable(_throwable);
			for (IRaptorTaskListener listener : _listenerList) {
				switch (field) {
				case STATUS:
					listener.stateChanged(raptorTaskEvent);
					break;
				case MESSAGE:
					listener.messageChanged(raptorTaskEvent);
					break;
				case PROGRESS:
					listener.progressChanged(raptorTaskEvent);
					break;
				default:
					throw new IllegalStateException(
							Messages.getString("RaptorTaskDescriptor.0") + getStatus()); //$NON-NLS-1$
				}
			}
		}
	} finally {
	    _listenerLock.unlock();
    }
    String msg = null;
    if (DEBUG_TASK_STATES) {
        msg = getName() + " " + field.toString() + "=";
        switch (field) {
            case STATUS:
                msg += getStatus();
                break;
            case MESSAGE:
                msg += getMessage();
                break;
            case PROGRESS:
                msg += getProgress();
                break;
            default:
        }
        System.err.println(msg + " >listeners notified"); // (authorized)
    }
  }

  private boolean tryListenerLocked() {
    try {
        return _listenerLock.tryLock(500, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
        // nothing todo
    }
    return false;
  }
  
  private void unlockListener() {
    if (_listenerLock.isHeldByCurrentThread()) {
      _listenerLock.unlock();
    }
  }
  
  public boolean isCancelled() {
    if (m_isCancelled) {
      return true;
    }
    if (_throwable instanceof ExecutionException) {
      return ((ExecutionException)_throwable).getCause() instanceof CancellationException;
    }
    return _throwable instanceof CancellationException;
  }

  /**
   * Sets the cancelled flag.
   *
   * @param isCancelled the cancelled flag
   */
  public void setCancelled(final boolean isCancelled) {
    this.m_isCancelled = isCancelled;
  }

  /**
   * Gets the elapsed time.
   *
   * @return the elapsed time
   */
  public long getElapsedTime() {
      if(_elapseTime != -1){
          return _elapseTime;
      } else {
          return _lastProgressTime - _startTime;
      }
  }
  /**
   * Gets the task start time. ie: Time when task state was first changed to RUNNABLE
   *
   * @return the start time
   */
  public long getStartTime() {
    return _startTime;
  }

  /**
   * Gets the last progress time.
   *
   * @return the last progress time
   */
  public long getLastProgressTime() {
      return _lastProgressTime;
  }

  /**
   * Gets the task icon.
   *
   * @return the icon
   */
  public Icon getIcon() {
		if (getStatus() == null) {
			return null;
		}
		if (getStatus() == IRaptorTaskStatus.FAILED) {
			return IImageRegistry.LIGHT_RED;
		} else if (getStatus() == IRaptorTaskStatus.RUNNABLE) {
			return IImageRegistry.LIGHT_GREEN;
		} else if (getStatus() == IRaptorTaskStatus.NEW || getStatus() == IRaptorTaskStatus.PAUSED) {
			return IImageRegistry.LIGHT_YELLOW;
		} else if (getStatus() == IRaptorTaskStatus.FINISHED && !isCancelled()) {
			//Removing OracleIcons 
			return IImageRegistry.LIGHT_GREEN;
		} else {
			return IImageRegistry.LIGHT_RED;
		}
	}

  /**
   * Gets the progress message.
   *
   * @return the message
   */
  public String getMessage() {
    return _message;
  }

  /**
   * Gets the task name.
   *
   * @return the name
   */
  public String getName() {
    return _name;
  }

  /**
   * Gets the numeric progress.
   *
   * @return the progress
   */
  public int getProgress() {
    return _progress;
  }

  /**
   * Gets the task status.
   *
   * @return the status
   */
  public IRaptorTaskStatus getStatus() {
    return _status;
  }

  /**
   * Return exception object if any.
   * @return Exception object
   */
  public Throwable getThrowable() {
    return _throwable;
  }

  /**
   * Sets the numeric progress incrementally.
   *
   * @param units the units
   */
  public void makeProgress(final int units) {
    int oldVal = this._progress;
    this._progress += units;
    notifyListener(PROPERTY.PROGRESS, oldVal);
  }

  /**
   * Updates the numeric progress incrementally only after trying to get listener lock.
   * If lock is not available within specific time, returns without any update.
   *
   * @param units
   * @param useListenerLock
   */
    public void makeProgress(final int units, boolean useListenerLock) {
        if (useListenerLock) {
            try {
                if (tryListenerLocked()) {
                    makeProgress(units);
                } else {
                    // update progress without notification
                    this._progress += units; 
                }
            } finally {
                unlockListener();
            }
        } else {
            makeProgress(units);
            return;
        }
    }
  
  /**
   * Sets the progress message.
   *
   * @param message the message
   */
  public void setMessage(final String message) {
    String oldMsg = this._message;
    this._message = message;
    notifyListener(PROPERTY.MESSAGE, oldMsg);
  }

  /**
   * Sets the absolute numeric progress.
   *
   * @param units the progress
   */
  public void setProgress(final int units) {
    int oldVal = this._progress;
    this._progress = units;
    notifyListener(PROPERTY.PROGRESS, oldVal);
  }

  /**
   * Sets the task status.
   *
   * @param status the status
   */
  /*friendly*/void setStatus(final IRaptorTaskStatus status) {
    setStatus(status, null);
  }

  /**
   * Sets the task status.
   *
   * @param status the status
   * @param th the throwable object, generally passed by a failed task
   */
  /*friendly*/ void setStatus(final IRaptorTaskStatus status, final Throwable th) {
    Object oldStatus = this._status;
    if (_startTime == 0 && status == IRaptorTaskStatus.RUNNABLE) {
      _startTime = System.currentTimeMillis();
      _lastProgressTime = _startTime;
    }
    this._status = status;
    this._throwable = th;
    if (oldStatus != status || status == IRaptorTaskStatus.NEW ) {
      notifyListener(PROPERTY.STATUS, oldStatus);
    }
  }

  /**
   * Checks if progress is determinate.
   *
   * @return true, if is indeterminate
   */
  public boolean isInDeterminate() {
    return _isInDeterminate;
  }

  public IRaptorTaskRunMode getRunMode() {
    return _runMode;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  //@Override
/*  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (_isInDeterminate ? 1231 : 1237);
    result = prime * result + ((_name == null) ? 0 : _name.hashCode());
    result = prime * result + ((_runMode == null) ? 0 : _runMode.hashCode());
    return result;
  }*/

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  /*@Override
  public boolean equals(Object obj) {
    if (this == obj) {
        return true;
    }
    if (obj == null) {
        return false;
    }
    if (getClass() != obj.getClass()) {
        return false;
    }
    final RaptorTaskDescriptor other = (RaptorTaskDescriptor) obj;
    if (_isInDeterminate != other._isInDeterminate) {
        return false;
    }
    if (_name == null) {
      if (other._name != null) {
        return false;
    }
    } else if (!_name.equals(other._name)) {
        return false;
    }
    if (_runMode == null) {
      if (other._runMode != null) {
        return false;
    }
    } else if (!_runMode.equals(other._runMode)) {
        return false;
    }
    if (_status == null) {
      if (other._status != null) {
        return false;
    }
    } else if (!_status.equals(other._status)) {
        return false;
    }
    return true;
  }*/
  
  public void overrideElapseTime(long elapseTime){
      _elapseTime = elapseTime;
  }
}
