/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.backgroundTask;

import oracle.dbtools.raptor.backgroundTask.RaptorTaskDescriptor.PROPERTY;

/**
 * The Task Event.
 */
final public class RaptorTaskEvent {

  /** The task descriptor. */
  private RaptorTaskDescriptor _taskDescriptor;

  /** The event time. */
  private long _eventTime;

  /** The source. */
  private Object _source;

  private PROPERTY _propertyChanged;

  private Object _oldPropertyValue;

  /** The throwable object to be passed to listeners as part of task notification. */
  private Throwable _throwable;

  /**
   * The Constructor.
   *
   * @param taskDescriptor the task descriptor
   */
  public RaptorTaskEvent(final RaptorTaskDescriptor taskDescriptor) {
    _taskDescriptor = taskDescriptor;
  }

  /**
   * The Constructor.
   *
   * @param taskDescriptor the task descriptor
   * @param currentTimeMillis the current time millis
   */
  public RaptorTaskEvent(final RaptorTaskDescriptor taskDescriptor, final long currentTimeMillis) {
    this(taskDescriptor);
    this._eventTime = currentTimeMillis;
  }

  /**
   * Gets the event time.
   *
   * @return the event time
   */
  public long getEventTime() {
    return _eventTime;
  }

  /**
   * Gets the source.
   *
   * @return the source
   */
  public Object getSource() {
    return _source;
  }

  /**
   * Sets the events throwable object.
   *
   * @param _throwable the throwable object
   */
  public void setThrowable(final Throwable _throwable) {
    this._throwable = _throwable;
  }

  /**
   * Gets the throwable object.
   *
   * @return the throwable object
   */
  public Throwable getThrowable() {
    return _throwable;
  }

  /**
   * Gets the task descriptor.
   *
   * @return the task descriptor
   */
  public RaptorTaskDescriptor getTaskDescriptor() {
    return _taskDescriptor;
  }
  
  /**
   * Sets the information about property being changed.
   *
   * @param field the property changed
   * @param oldValue the old property value
   */
   /* friendly*/void setChangedProperty(final PROPERTY field, final Object oldValue) {

    this._propertyChanged = field;
    this._oldPropertyValue = oldValue;
  }
  
  /**
   * Gets the field which was changed.
   *
   * @return the field changed
   */
  public PROPERTY getPropertyChanged() {
    return _propertyChanged;
  }

  /**
   * Gets the old value of the changed field.
   *
   * @return the old field value
   */
  public Object getOldValue() {
    return _oldPropertyValue;
  }
}
