/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.backgroundTask;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;

import oracle.dbtools.raptor.backgroundTask.internal.IRaptorTaskUIListener;
import oracle.dbtools.raptor.backgroundTask.ui.ITaskUI;
import oracle.dbtools.raptor.backgroundTask.ui.ITaskViewer;
import oracle.dbtools.raptor.backgroundTask.utils.RaptorTaskWrapper;
import oracle.dbtools.util.Logger;

/**
 * This class handles scheduling of tasks as an independent thread.
 */
public class RaptorTaskManager {

    /** The singleton instance. */
    private static RaptorTaskManager schedulerInstance;

    /**
     * Task tracker.  This could be better but for now, only concerned with Raptor task in SQLDev. 
     */
    private ITaskTracker _tracker = null;
    private ITaskViewer _viewer = null;
    private ITaskDialog _taskDialog = null;
    private ITaskUI _defaultTaskUI = null;
    /**
     * The queue of tasks to be executed.
     */
    final private TaskLinkedList<RaptorTask<?>> _schedulerTaskList = new TaskLinkedList<RaptorTask<?>>();

    /** The task - thread map. */
    final private Map<RaptorTask<?>, Future<?>> _scheduledTaskMap = new ConcurrentHashMap<RaptorTask<?>, Future<?>>();
    
    final private RaptorTaskThreadFactory _threadFactory = new RaptorTaskThreadFactory();

    /** The task executor. */
    // Atleast 1 thread will be alive in pool, no limit on number threads created on demand, minimum thread idle time = 60 secs.
    final private ExecutorService _taskExecutor = new ThreadPoolExecutor(1,  Integer.MAX_VALUE,
                                        60L, TimeUnit.SECONDS,
                                        new SynchronousQueue<Runnable>(), _threadFactory);
/*
    final private ExecutorService _taskExecutor = Executors.newCachedThreadPool();
*/

    final private UncaughtExceptionHandler _uncaught = new UncaughtExceptionHandler() {
      @Override
      public void uncaughtException(Thread t, Throwable e) {
        // Bug 18349944 - BROWSE APPLICATION CODE GET ERROR IN LOGGING PAGE
        // Use our Logger for better message handling
        Logger.severe(getClass(), e);  
//        Logger.getAnonymousLogger().log(Level.SEVERE, e.getLocalizedMessage());
//        StringWriter sw = new StringWriter();
//        PrintWriter pw = new PrintWriter(sw);
//        e.printStackTrace(pw);
//        Logger.getAnonymousLogger().log(Level.INFO, sw.toString());
      }
    };
    
    
    /** The scheduler thread. */
    private SchedulerThread _schedulerThread;

    private IRaptorTaskListener _taskListener;

    private Map<RaptorTaskDescriptor, ITaskDialog> _taskDialogMap;

    /**
	 * @return the _defaultTaskUI
	 */
    public ITaskUI getDefaultTaskUI() {
	    return _defaultTaskUI;
    }

	/**
	 * @param _defaultTaskUI the _defaultTaskUI to set
	 */
    public void setDefaultTaskUI(ITaskUI _defaultTaskUI) {
	    this._defaultTaskUI = _defaultTaskUI;
    }

	/**
	 * @return the _taskDialog
	 */
    public ITaskDialog getTaskDialog() {
	    return _taskDialog;
    }

	/**
	 * @param _taskDialog the _taskDialog to set
	 */
    public void setTaskDialog(ITaskDialog _taskDialog) {
	    this._taskDialog = _taskDialog;
    }

	/**
	 * @return the _viewer
	 */
    public ITaskViewer getTaskProgressViewer() {
	    return _viewer;
    }

	/**
	 * @param _viewer the _viewer to set
	 */
    public void setTaskProgressViewer(ITaskViewer viewer) {
	    this._viewer = viewer;
    }

	/**
	 * @return the _tracker1
	 */
    public ITaskTracker getIDETaskTracker() {
	    return _tracker;
    }

	/**
	 * @param _tracker1 the _tracker1 to set
	 */
    public void setIDETaskTracker(ITaskTracker _tracker1) {
	    this._tracker = _tracker1;
    }

	/**
     * The Constructor.
     */
    private RaptorTaskManager() {
        _schedulerThread = new SchedulerThread();
        _schedulerThread.setDaemon(true);
        _schedulerThread.setName("Scheduler"); //$NON-NLS-1$

        // listener to wake up task list so that scheduler thread can fetch next task
        _taskListener = new RaptorTaskAdapter() {
            public void stateChanged(RaptorTaskEvent event) {
                IRaptorTaskStatus status = event.getTaskDescriptor().getStatus();
                if (status == IRaptorTaskStatus.FAILED
                        || status == IRaptorTaskStatus.FINISHED
                        || status == IRaptorTaskStatus.PAUSED) {
                    synchronized (_schedulerTaskList) {
                        _schedulerTaskList.notify();
                    }
                }
            }

        };
        _schedulerThread.start();
        _taskDialogMap = new ConcurrentHashMap<RaptorTaskDescriptor, ITaskDialog>();
    }

    /**
     * Gets the instance.
     *
     * @return the scheduler instance
     */
    public static synchronized RaptorTaskManager getInstance() {
        if (schedulerInstance == null) {
            schedulerInstance = new RaptorTaskManager();
        }
        return schedulerInstance;
    }

    /**
     * Schedule the task for execution. How the task progress in displayed in the
     * GUI depends upon IRaptorTaskRunMode of the task
     * <p>
     * If <code>IRaptorTaskRunMode.NO_GUI</code> the task progress is run in a
     * headless mode i.e. the task progress in not shown in the GUI.
     * </p>
     * <p>
     * if <code>IRaptorTaskRunMode.MODAL</code> the task progress is shown in a
     * modal dialog and the GUI is blocked till the task finishes.
     * </p>
     * <p>
     * If <code>IRaptorTaskRunMode.MODAL_OPTIONAL</code> the task progress is
     * shown in a modal dialog and the GUI is blocked till the task finishes, but
     * the user has an option to shift the task to background and the task
     * progress in shown in the default task viewer.
     * </p>
     * <p>
     * If <code>IRaptorTaskRunMode.IDE_STATUSBAR</code> the task progress is
     * shown in the default task viewer as well as the ide's status bar.
     * </p>
     * <p>
     * If <code>IRaptorTaskRunMode.TASKVIEWER</code> the task progress is
     * shown in the default task viewer only.
     * </p>
     * <p><b>This method must be called from Event Dispatch Thread</b>
     * </p>
     *
     * @param task
     *          the task
     */
    public void addTask(final RaptorTask<?> task) {
        addTask(task, new ArrayList<ITaskViewer>());
    }

    /**
     * Schedule the task for execution and shows it's progress in the viewers
     * How the task progress in displayed in the GUI depends upon
     * IRaptorTaskRunMode of the task
     * <p>
     * If <code>IRaptorTaskRunMode.NO_GUI</code> the task progress is run in a
     * headless mode i.e. the task progress in not shown in the GUI.
     * </p>
     * <p>
     * if <code>IRaptorTaskRunMode.MODAL</code> the task progress is shown in a
     * modal dialog and the GUI is blocked till the task finishes.
     * </p>
     * <p>
     * If <code>IRaptorTaskRunMode.MODAL_OPTIONAL</code> the task progress is
     * shown in a modal dialog and the GUI is blocked till the task finishes, but
     * the user has an option to shift the task to background and the task
     * progress in shown in the default task viewer.
     * </p>
     * <p>
     * If <code>IRaptorTaskRunMode.IDE_STATUSBAR</code> the task progress is
     * shown in the default task viewer as well as the ide's status bar.
     * </p>
     * <p>
     * If <code>IRaptorTaskRunMode.TASKVIEWER</code> the task progress is
     * shown in the default task viewer only.
     * </p>
     * <p><b>This method must be called from Event Dispatch Thread</b>
     * </p>
     *
     * @param viewers
     *          additional viewers apart from the default task viewer where task
     *          progress is shown
     * @param task
     *          the task
     */
    public void addTask(final RaptorTask<?> task, final List<? extends ITaskViewer> viewers) {
        addTask(task, new ArrayList<IRaptorTaskUIListener>(), viewers);
    }

    /**
     * Convenience method to add task, taskListeners, UIListeners, TaskViewers
     * <p><b>This method must be called from Event Dispatch Thread</b>
     * </p>
     * @param wrapper
     * @see oracle.dbtools.raptor.backgroundTask.RaptorTaskManager#addTask(RaptorTask, List, List)
     */
    public void addTask(final RaptorTaskWrapper<?> taskWrapper) {
        addTask(taskWrapper.getTask(), taskWrapper.getRaptorTaskUIListeners(), taskWrapper.getTaskViewers());
    }

    /**
     * Schedule the task for execution, shows it's progress in the viewers and
     * informs IRaptorTaskUIListener's for the operations done on the task
     * progress GUI component How the task progress in displayed in the GUI
     * depends upon IRaptorTaskRunMode of the task
     * <p>
     * If <code>IRaptorTaskRunMode.NO_GUI</code> the task progress is run in a
     * headless mode i.e. the task progress in not shown in the GUI.
     * </p>
     * <p>
     * if <code>IRaptorTaskRunMode.MODAL</code> the task progress is shown in a
     * modal dialog and the GUI is blocked till the task finishes.
     * </p>
     * <p>
     * If <code>IRaptorTaskRunMode.MODAL_OPTIONAL</code> the task progress is
     * shown in a modal dialog and the GUI is blocked till the task finishes, but
     * the user has an option to shift the task to background and the task
     * progress in shown in the default task viewer.
     * </p>
     * <p>
     * If <code>IRaptorTaskRunMode.IDE_STATUSBAR</code> the task progress is
     * shown in the default task viewer as well as the ide's status bar.
     * </p>
     * <p>
     * If <code>IRaptorTaskRunMode.TASKVIEWER</code> the task progress is
     * shown in the default task viewer and submitted viewers.
     * </p>
     * <p><b>This method must be called from Event Dispatch Thread</b>
     * </p>
     *
     * @param viewers
     *          the viewers
     * @param taskUIListeners
     *          additional listeners for the operations done on the task progress
     *          GUI component
     * @param task
     *          the task
     */
    public void addTask(final RaptorTask<?> task, final List<? extends IRaptorTaskUIListener> taskUIListeners,
            final List<? extends ITaskViewer> viewers) {
        task.getDescriptor().addListener(_taskListener);
        final IRaptorTaskRunMode mode = task.getDescriptor().getRunMode();

        if (mode == IRaptorTaskRunMode.NO_GUI) {
            submitTask(task);
            return;
        }

        if (mode == IRaptorTaskRunMode.MODAL || mode == IRaptorTaskRunMode.MODAL_OPTIONAL) {
            submitTask(task);
            SwingUtilities.invokeLater( //always invokeLater, even if on EDT. Bug 16571863
                new Runnable() {
                    public void run() {
                        Thread.currentThread().setDefaultUncaughtExceptionHandler(_uncaught);
                        // create a modal dialog
                        ITaskUI taskUI = null;
                        // if viewers have been supplied, use the task ui from first viewer to display in background dialog
                        if (viewers.size() > 0) {
                            taskUI = viewers.get(0).createTaskUI(task.getDescriptor());
                        }
                        if (taskUI == null) {
                            // use default progress bar with pause and cancel buttons
                            taskUI = getDefaultTaskUI().getNewTaskUI(task.getDescriptor());
                        }
                        initViewers(task, viewers, taskUIListeners);
                        // associate task to UI using ui listeners. A default listener handles task pause/cancel
                        List<IRaptorTaskUIListener> uiListeners = null;
                        if (taskUIListeners != null) {
                            uiListeners = new ArrayList<IRaptorTaskUIListener>(taskUIListeners);
                        } else {
                            uiListeners = new ArrayList<IRaptorTaskUIListener>();
                        }
                        uiListeners.add(0, new DefaultTaskUIListener(task, null));
                        setTaskUIState(taskUI, task, uiListeners);
                        final ITaskDialog tDialog = createBackgroundDialog(task, taskUIListeners, viewers, taskUI);
                        showModalDialog(task, tDialog);
                    }
                }
                );
        } else if (mode == IRaptorTaskRunMode.TASKVIEWER || mode == IRaptorTaskRunMode.IDE_STATUSBAR) {
        	Runnable taskUIRunnable = new Runnable() { //Bug 16492645 - SWING VIOLATION IN WORKSHEET
                public void run() {
                  Thread.currentThread().setDefaultUncaughtExceptionHandler(_uncaught);
                	 List<ITaskViewer> viewerss = new ArrayList<ITaskViewer>(viewers);
                     viewerss.add(0, getTaskProgressViewer());
                     initViewers(task, viewerss, taskUIListeners);
                }
            };
            invokeInDispatchThreadIfNeeded(taskUIRunnable);
            submitTask(task);
        }
    }
    
    /**
     * Run the following in the Swing EDT thread. If required invoke in the Swing thread.
     * @param runnable
     */
    public static void invokeInDispatchThreadIfNeeded(Runnable runnable) {
        if (EventQueue.isDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    /**
     * Show modal task progress dialog.
     * @param task
     * @param tDialog
     */
    private void showModalDialog(final RaptorTask<?> task, final ITaskDialog tDialog) {
        tDialog.showDelayedDialog(task);
    }

    private ITaskDialog createBackgroundDialog(final RaptorTask<?> task, final List<? extends IRaptorTaskUIListener> taskUIListeners,
            final List<? extends ITaskViewer> viewers, final ITaskUI defaultUI) {
        boolean runBkGrnd = task.getDescriptor().getRunMode() == IRaptorTaskRunMode.MODAL_OPTIONAL;
        final ITaskDialog tDialog = getTaskDialog().createNewDialog(defaultUI, task, runBkGrnd);
        ActionListener l = new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                if (cmd.equals(ITaskDialog.CANCEL_TASK) && IRaptorTaskStatus.isAlive(task.getDescriptor().getStatus())) {
                    if (!cancelTask(task)) {
                        Logger.severe(getClass(), e.toString());
//                        Logger.getLogger(getClass().getName()).log(Level.SEVERE, /*Messages.getString("RaptorTaskManager.5")*/
//                                                                   "", e); //$NON-NLS-1$
                    }
                } else if (cmd.equals(ITaskDialog.RUN_IN_BACKGROUND)) {
                    List<ITaskViewer> viewerss = new ArrayList<ITaskViewer>(viewers);
                    viewerss.add(0, getTaskProgressViewer());
                    initViewers(task, viewerss, taskUIListeners);
                    tDialog.setVisible(false);
                } else if(cmd.equals(ITaskDialog.CLOSE_TASK)){
                	 tDialog.setVisible(false);
                }
                
            }
        };
        WindowListener wl = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (IRaptorTaskStatus.isAlive(task.getDescriptor().getStatus())) {
                    if (!cancelTask(task)) {
                        Logger.severe(getClass(), e.toString());
//                        Logger.getLogger(getClass().getName()).log(Level.SEVERE, /*Messages.getString("RaptorTaskManager.5")*/
//                                                                   "", e); //$NON-NLS-1$
                    }
                }
            }
        };
        tDialog.setButtonListener(l);
        tDialog.setWindowCloseListener(wl);
        _taskDialogMap.put(task.getDescriptor(), tDialog);
        Logger.fine(getClass(), "_taskDialogMap.put("+String.valueOf(task.getDescriptor())+")");
        return tDialog;
    }

    private void initViewers(RaptorTask<?> task, List<? extends ITaskViewer> viewers, List<? extends IRaptorTaskUIListener> taskUIListeners) {

        for (ITaskViewer viewer : viewers) {
            ArrayList<IRaptorTaskUIListener> uiListeners = new ArrayList<IRaptorTaskUIListener>(taskUIListeners);
            ITaskUI ui = viewer.createTaskUI(task.getDescriptor());
            uiListeners.add(0, new DefaultTaskUIListener(task, viewers));
            setTaskUIState(ui, task, uiListeners);
            viewer.addTaskUI(ui);
        }
    }

    private void setTaskUIState(ITaskUI taskUI, RaptorTask<?> task, List<? extends IRaptorTaskUIListener> taskUIListeners) {
    	if (taskUI!=null && task!=null) {
	        taskUI.setCancellable(task.isCancellable());
	        taskUI.setPausable(task.isPausable());
	        for (IRaptorTaskUIListener taskUIListener : taskUIListeners) {
	            taskUI.addListener(taskUIListener);
	        }
	        task.getDescriptor().addListener(taskUI);
    	}
    }

    /**
     * Schedule the task for execution.
     *
     * @param task
     *          the task
     */
    private void submitTask(final RaptorTask<?> task) {

        if (!_schedulerThread.isAlive()) {
            throw new IllegalStateException(Messages.getString("RaptorTaskManager.3")); //$NON-NLS-1$
        }
        task.setStatus(IRaptorTaskStatus.NEW);
        _schedulerTaskList.offer(task);
    }

    /**
     * Cancel task called from ui.
     *
     * @param task the task
     *
     * @return true, if cancel() call returns true
     */
    public boolean cancelTask(RaptorTask<?> task) {
        if (task.getStatus() == IRaptorTaskStatus.NEW) {
            _schedulerTaskList.remove(task);
            task.getDescriptor().setCancelled(true);
            task.setStatus(IRaptorTaskStatus.FINISHED);
            return true;
        }
        task.requestCancel();
        if (task.mayInterrupt()) {
            Future<?> taskThread = _scheduledTaskMap.get(task);
            if (taskThread != null) {
                boolean cancelled = taskThread.cancel(true);
                task.getDescriptor().setCancelled(cancelled);
                return cancelled;
            }
        }
        return false;
    }

    /**
     * Shutdown scheduler thread. JVM will otherwise kill it as this is daemon
     * thread.
     */
    public void shutdown() {
        _schedulerThread.interrupt();
        _taskExecutor.shutdown();
        return;
    }

//    /**
//     * Removes the dead tasks from queue.
//     */
//    private void removeDeadTasks() {
//        for (Iterator<RaptorTask<?>> iterator = _scheduledTaskMap.keySet().iterator(); iterator.hasNext();) {
//            RaptorTask<?> task = iterator.next();
//            if (task.getStatus() == IRaptorTaskStatus.FINISHED || task.getStatus() == IRaptorTaskStatus.FAILED) {
//                task.getDescriptor().removeAllListeners();
//                iterator.remove();
//            }
//        }
//        // clear takdescriptor - task dialog map
//        for (Iterator<RaptorTaskDescriptor> iterator = _taskDialogMap.keySet().iterator(); iterator.hasNext();) {
//            RaptorTaskDescriptor taskDesc = iterator.next();
//            if (taskDesc.getStatus() == IRaptorTaskStatus.FINISHED || taskDesc.getStatus() == IRaptorTaskStatus.FAILED) {
//                // remove from task background dialog - task map
//                ITaskDialog taskDialog = _taskDialogMap.get(taskDesc);
//                if (taskDialog != null && !taskDialog.isVisible()) {
//                    taskDialog.dispose();
//                }
//                taskDesc.removeAllListeners();
//                iterator.remove();
//            }
//        }
//    }

    /**
     * The SchedulerThread class.
     */
    private class SchedulerThread extends Thread {

        /**
         * (non-Javadoc).
         *
         * @see java.lang.Thread#run()
         */
        public void run() {
            while (true) {
                try {
                    // Bug 26668531 - AFTER CLOSING THE DIFF TAB THE RESOURCES AREN'T RELEASED (STILL IN JAVA HEAP) 
                    // remove dead tasks before checking for new ones, not after finding a new one
                    //removeDeadTasks();
                    // See raptorFutureTask() - added listener to do cleanup after cancel,fail,finish
                    // doing async scan for finished tasks & wiping the listener list sets up a race condition
                    // that can cause really bizarre results.

                    // This call is blocking - the keySet is dynamic so tracks the underlying map
                    RaptorTask<?> taskToSchedule = _schedulerTaskList.takeNextTask(_scheduledTaskMap.keySet());
                    if (taskToSchedule == null) {
                        continue;
                    }
                    if (taskToSchedule.getStatus().equals(IRaptorTaskStatus.NEW)) {
                        RaptorFutureTask taskResult = new RaptorFutureTask(taskToSchedule);
                        taskToSchedule.setFuture(taskResult);
                        _threadFactory.setThreadName(taskToSchedule.getDescriptor().getName());
                        _taskExecutor.submit(taskResult);
                        _scheduledTaskMap.put(taskToSchedule, taskResult);
                    }
                    //removeDeadTasks() <-- was here
                } catch (Throwable e) {
                    Logger.severe(getClass(), Messages.getString("RaptorTaskManager.4"), e);
//                    Logger.getLogger(getClass().getName()).log(Level.SEVERE,
//                                                               Messages.getString("RaptorTaskManager.4") + e.getStackTrace()[0].toString(), e); //$NON-NLS-1$
                }                
            }
        }
    }

    /**
     * The future task wrapper for submitted task.
     */
    private class RaptorFutureTask<V> extends FutureTask<V> {

        /** The task. */
        RaptorTask<V> _task = null;

        /**
         * The Constructor.
         *
         * @param callable
         *          the callable task
         */
        public RaptorFutureTask(RaptorTask<V> callable) {
            super(callable);
            _task = callable;
            _task.getDescriptor().addListener(new RaptorTaskAdapter() {

                @Override
                public void taskCancelled(RaptorTaskEvent event) {
                    cleanup();
                }

                @Override
                public void taskFailed(RaptorTaskEvent event) {
                    cleanup();
                }

                @Override
                public void taskFinished(RaptorTaskEvent event) {
                    cleanup();
                }
                
                private void cleanup() {
                    _scheduledTaskMap.remove(_task);
                    // WE are the last listener BUT the listener list is synchronized
                    // (I.e., ConcurrentModificationException)  so add a task for it
                    if (!(_task instanceof RaptorTaskManager.RaptorFutureTask.RaptorTaskCleanup)) {
                        addTask(new RaptorTaskCleanup(String.valueOf(_task)) {
                            @Override
                            protected Void doWork() throws TaskException {
                                final RaptorTaskDescriptor taskDesc = _task.getDescriptor();
                                // remove from task background dialog - task map
                                final ITaskDialog taskDialog = _taskDialogMap.get(taskDesc);
                                if (taskDialog != null) {
                                    Logger.fine(getClass(), "taskDialog = "+(taskDialog.isVisible()?"(Visible) ":"(!Visible) ")+String.valueOf(taskDialog));
                                    _taskDialogMap.remove(taskDesc);
                                    Logger.fine(getClass(), "_taskDialogMap.remove("+String.valueOf(taskDesc)+")");
                                    SwingUtilities.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            Logger.fine(getClass(), "taskDialog = "+(taskDialog.isVisible()?"(Visible) ":"(!Visible) ")+String.valueOf(taskDialog));
                                            if (!taskDialog.isVisible()) {
                                                taskDialog.dispose();
                                                Logger.fine(getClass(), "taskDialog.dispose()");
                                            }
                                        }
                                    });
                                }
                                taskDesc.removeAllListeners();
                                _task = null;
                                return null;
                            }
                        });
                    }
                }
            });
        }

        private abstract class RaptorTaskCleanup extends RaptorTask<Void> {
            public RaptorTaskCleanup(String name) {
                super(name, true, IRaptorTaskRunMode.NO_GUI);
            }
        }
        
        /**
         * (non-Javadoc).
         *
         * @see java.util.concurrent.FutureTask#done()
         */
        @Override
        protected void done() {
            super.done();
            try {
                try {
                    // Bug 19788934: JDEV: EXCEPTION ON CANCELLING/CLOSING IN CONNECTION OPENER DIALOG
                    // Bug 19703274: EXCEPTION ON CANCELLING/CLOSING IN CONNECTION OPENER DIALOG
                    // Calling cancel() ends up calling this so need to check state in done() before trying get()
                    if (this.isCancelled()) {
// DEBUG: TODO:    GRRRR - run with breakpoint again & see what the connection drop down is checking for
//                        _task.setStatus(IRaptorTaskStatus.);
                        int x = 0;
                    }
                    // Bug 16691202 MIGRATION ACTIONS THROW INTERRUPTED EXCEPTION
                    // NavigatorQueryTask interrupts the thread on cancel
                    if (!Thread.interrupted()) {
                        get(); // WHY IS THIS HERE AT ALL??
                        //_task.getResult(); // use task (rather than futureTask) get so _task_future link gets broken 
                    }
                    _task.setStatus(IRaptorTaskStatus.FINISHED);
                } catch (ExecutionException ex) {
                    // Unwrap the execution exception
                    Throwable t = ex.getCause();
                    throw t;
                }
            } catch ( Throwable t ) {
                try { // no exceptions from here - trap them all
                    if (t instanceof TaskException) {
                        _task.getDescriptor().setStatus(IRaptorTaskStatus.FAILED, ((TaskException)t).getCause());
                    } else {
                        _task.getDescriptor().setStatus(IRaptorTaskStatus.FAILED, t);
                    }
                } catch ( Throwable t2 ) {
                    Logger.severe(getClass(), String.valueOf(_task), t);
                    Logger.severe(getClass(), String.valueOf(_task), t2);
                }
            }
        }

        @Override
        public void run() {
            if (_task.getDescriptor().getRunMode() == IRaptorTaskRunMode.IDE_STATUSBAR) {
            	_tracker.add(_task,this);
                
            }
            // It would be really nice if thread knew task name for debug
            final String tname = Thread.currentThread().getName();
            String rtname = _task.getDescriptor().getName();
            if (rtname != null && !rtname.isEmpty()) {
            	Thread.currentThread().setName(tname+'-'+rtname);
            }
            try {
            	super.run();
            }
            finally {
            	Thread.currentThread().setName(tname);
            }
        }
    }
//
//    private class IdeTaskTracker<V> implements ProgressTrackedTask {
//        private IRaptorTaskProgressUpdater _taskUpdater;
//
//        private Future<?> _future;
//
//        public IdeTaskTracker(final IRaptorTaskProgressUpdater taskUpdater, final Future<?> future) {
//            _taskUpdater = taskUpdater;
//            _future = future;
//        }
//
//        public String getCurrentText() {
//            return _taskUpdater.getDescriptor().getMessage();
//        }
//
//        public int getCurrentValue() {
//            return _taskUpdater.getDescriptor().getProgress();
//        }
//
//        public int getMaximum() {
//            return 100;
//        }
//
//        public int getMinimum() {
//            return 0;
//        }
//
//        public String getTaskDescription() {
//            return _taskUpdater.getDescriptor().getName();
//        }
//
//        public boolean isIndeterminate() {
//            return _taskUpdater.getDescriptor().isInDeterminate();
//        }
//
//        public void run() {
//            try {
//                _future.get();
//            } catch (InterruptedException e) { // some logging ?
//            } catch (CancellationException e) { // some logging ?
//            } catch (ExecutionException e) { // some logging ?
//            }
//        }
//    }

    private class DefaultTaskUIListener implements IRaptorTaskUIListener {
        WeakReference<RaptorTask<?>> _taskRef = null;
        List<? extends ITaskViewer> _viewers = null;

        public DefaultTaskUIListener(RaptorTask<?> task, List<? extends ITaskViewer> viewers) {
            _taskRef = new WeakReference<RaptorTask<?>>(task);
            _viewers = viewers;
        }

        public void cancelClicked(RaptorTaskDescriptor desc) {
            if (_viewers != null) {
                for (ITaskViewer v : _viewers) {
                    v.taskCancelRequested(desc);
                }
            }
            RaptorTask<?> task = _taskRef.get();
            if (task != null) {
                cancelTask(task);
            }
        }

        public void pauseClicked(RaptorTaskDescriptor desc) {
            if (_viewers != null) {
                for (ITaskViewer v : _viewers) {
                    v.taskPauseRequested(desc);
                }
            }
            RaptorTask<?> task = _taskRef.get();
            if (task != null) {
                if (task.getStatus() == IRaptorTaskStatus.PAUSED) {
                    task.resume();
                } else if (task.getStatus() == IRaptorTaskStatus.RUNNABLE) {
                    task.requestPause();
                }
            }
        }

        public void taskClicked(RaptorTaskDescriptor desc) {
            if (desc.getRunMode() == IRaptorTaskRunMode.MODAL_OPTIONAL && getTaskProgressViewer()!= null) {
            	ITaskUI taskUI = getTaskProgressViewer().getTaskUI(desc);
                if (taskUI != null) {
                    shiftToForeGround(taskUI);
                }
            }
        }
    }
    private void shiftToForeGround(ITaskUI taskUI) {
        ITaskDialog taskDialog = _taskDialogMap.get(taskUI.getTaskDescriptor());
        if (taskDialog!=null && taskDialog.isDisplayable() && getTaskProgressViewer()!=null) {
            getTaskProgressViewer().removeTaskUI(taskUI);
            taskDialog.setVisible(true);
        }
    }
    
    private static class RaptorTaskThreadFactory implements ThreadFactory {
        
        private final ThreadFactory _delegate = Executors.defaultThreadFactory();
        private String _threadName = "";
        private int counter = 1;
        private DecimalFormat df = new DecimalFormat("#00"); //$NON-NLS-1$
        
        @Override
        public Thread newThread(Runnable r) {
            Thread t = _delegate.newThread(r);
//            if (_threadName != null && !_threadName.equals("")) {
//                t.setName("RaptorTask: "+_threadName); //$NON-NLS-1$
//            }
            t.setName("RaptorTaskThread"+df.format(counter)); //$NON-NLS-1$
            counter++;
            return t;
        }
        
        synchronized void setThreadName(String threadName) {
            _threadName = threadName;
        }        
    }
}
