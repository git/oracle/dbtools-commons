/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.backgroundTask;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.LinkedList;

/**
 * The queue of tasks to be executed.
 */
public class TaskLinkedList<S> extends LinkedList<RaptorTask<?>> {
  private static final long serialVersionUID = 1L;

  public boolean offer(RaptorTask<?> o) {
    boolean ret = true;
    synchronized (this) {
      ret = super.offer(o);
      this.notify();
    }
    return ret;
  }

  @Override
  public boolean remove(Object o) {
    boolean ret = true;
    synchronized (this) {
      ret = super.remove(o);
      this.notify();
    }
    return ret;
  }

  /**
   * Take next task. Waits till a task is available for scheduling
   *
   * @param runningTaskList
   *          the current running tasks - this is dynamic and follows the underlying map
   *
   * @return the schedulable raptor task
   *
   * @throws InterruptedException
   *           the interrupted exception
   */
  public RaptorTask<?> takeNextTask(Collection<RaptorTask<?>> runningTaskList) throws InterruptedException {
    RaptorTask<?> nextTask = null;
    synchronized (this) {
      while (getNextTask(runningTaskList) == null) {
        wait();
      }
      nextTask = getNextTask(runningTaskList);
      this.remove(nextTask);
    }
    return nextTask;
  }

  private RaptorTask<?> getNextTask(Collection<RaptorTask<?>> runningTaskList) {
    RaptorTask<?> elementToReturn = null;
    for (RaptorTask<?> element : this) {
      RaptorTask<?> conflictingTask = getConflictingTask(element, runningTaskList);
      // Need to update message for all tasks in queue
      if (conflictingTask != null) {
        element.getDescriptor().setMessage(MessageFormat.format(Messages.getString("TaskLinkedList.0"), new Object[] { conflictingTask.getDescriptor().getName()})); //$NON-NLS-1$
      } else if (elementToReturn == null) {
        elementToReturn = element;
      }
    }
    return elementToReturn;
  }

  private RaptorTask<?> getConflictingTask(final RaptorTask<?> raptorTask,
      final Collection<RaptorTask<?>> runningTaskList) {
    ISchedulingRule currentTaskRule = raptorTask.getSchedulingRule();
    if (currentTaskRule == null) {
      return null;
    }
    for (RaptorTask<?> task : runningTaskList) {
      if (task.getStatus() != IRaptorTaskStatus.FINISHED && task.getStatus() != IRaptorTaskStatus.FAILED &&
              task.getSchedulingRule() != null) {
        ISchedulingRule runningTaskRule = task.getSchedulingRule();
        if ( currentTaskRule.isConflicting(runningTaskRule) || runningTaskRule.isConflicting(currentTaskRule)) {
              return task;
          }
      }
    }
    return null;
  }
}
