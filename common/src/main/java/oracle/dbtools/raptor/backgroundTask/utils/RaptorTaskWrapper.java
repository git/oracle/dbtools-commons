/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.backgroundTask.utils;

import java.util.Collections;
import java.util.List;

import oracle.dbtools.raptor.backgroundTask.IRaptorTaskListener;
import oracle.dbtools.raptor.backgroundTask.RaptorTask;
import oracle.dbtools.raptor.backgroundTask.internal.IRaptorTaskUIListener;
import oracle.dbtools.raptor.backgroundTask.ui.ITaskViewer;

public class RaptorTaskWrapper<V> {
    private RaptorTask<V> m_task;
    private List<IRaptorTaskListener> m_taskListeners;
    private List<IRaptorTaskUIListener> m_uiListeners;
    private List<ITaskViewer> m_viewers;

    public RaptorTaskWrapper(RaptorTask<V> task, List<IRaptorTaskListener> taskListeners,
            List<IRaptorTaskUIListener> uiListeners, List<ITaskViewer> viewers) {
        m_task = task;
        m_taskListeners = taskListeners != null ? taskListeners
                : Collections.EMPTY_LIST;
        for (IRaptorTaskListener listener: m_taskListeners) {
            m_task.getDescriptor().addListener(listener);
        }
        m_uiListeners = uiListeners != null ? uiListeners : Collections.EMPTY_LIST;
        m_viewers = viewers != null ? viewers : Collections.EMPTY_LIST;
    }

    /**
     * @return the task
     */
    public RaptorTask<V> getTask() {
        return m_task;
    }

    /**
     * @return the taskListeners
     */
    public List<IRaptorTaskListener> getRaptorTaskListeners() {
        return m_taskListeners;
    }

    /**
     * @return the uiListeners
     */
    public List<IRaptorTaskUIListener> getRaptorTaskUIListeners() {
        return m_uiListeners;
    }

    /**
     * @return the viewers
     */
    public List<ITaskViewer> getTaskViewers() {
        return m_viewers;
    }
}
