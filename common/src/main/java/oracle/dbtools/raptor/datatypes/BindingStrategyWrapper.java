/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes;

import java.sql.SQLException;


import java.sql.Statement;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=CallableBindingWrapper.java">Barry McGillin</a>
 *
 * @param <P>
 */
public class BindingStrategyWrapper<S extends Statement, P extends DataBinding> implements BindingStrategy<S,P> {

    protected final P param;
    protected final BindingStrategy<S,P> binding;

    public BindingStrategyWrapper(BindingStrategy<S,P> binding, P param) {
        this.binding = binding;
        this.param = param;
    }

    @Override
    public BindContext<S> getBindContext() {
        return binding.getBindContext();
    }

    @Override
    public P getParameter() {
        return param;
    }

    @Override
    public DataType getDataType() {
        return binding.getDataType();
    }

    @Override
    public BindingMode getMode() {
        return binding.getMode();
    }

    @Override
    public void setByPositionBindToken(String bindToken) {
        binding.setByPositionBindToken(bindToken);
    }

    @Override
    public String getByPositionBindToken() {
        return binding.getByPositionBindToken();
    }

    @Override
    public void setByNameBindToken(String bindToken) {
        binding.setByNameBindToken(bindToken);
    }

    @Override
    public String getByNameBindToken() {
        return binding.getByNameBindToken();
    }

    @Override
    public BindingStyle getBindingStyle(BindingMode mode) {
        return binding.getBindingStyle(mode);
    }

    @Override
    public String getBindToken(BindingMode mode) {
        return binding.getBindToken(mode);
    }

    @Override
    public DataValue getInput() throws SQLException {
        return binding.getInput();
    }

    @Override
    public DataValue getOutput() throws SQLException {
        return binding.getOutput();
    }

    @Override
    public final S getStatement() throws SQLException {
        return binding.getStatement();
    }

    @Override
    public final void bind(S stmt, DataValue value) throws SQLException {
         binding.bind(stmt, value);
    }

    @Override
    public final void bind(S stmt) throws SQLException {
        binding.bind(stmt);
    }

    @Override
    public PLSQLBoundBlockBuilder getBuilder() {
        return binding.getBuilder();
    }

    @Override
    public PLSQLBoundBlockBuilder getBuilder(PLSQLBoundBlockBuilder builder) {
        return binding.getBuilder(builder);
    }

    @Override
    public void reportBinding(StringBuilder buffer, DataValue value) {
        binding.reportBinding(buffer, value);
    }

    @Override
    public void reportBinding(StringBuilder buffer, String nullToken, DataValue value) {
        binding.reportBinding(buffer, nullToken, value);
    }

    @Override
    public BindingStrategySplitMode getSplitModeBinding() {
        return binding.getSplitModeBinding();
    }
    
    @Override
    public boolean isSupported(BindingMode mode) {
        return binding.isSupported(mode);
    }

    @Override
    public void uncacheInput() {
        binding.uncacheInput();
    }

    @Override
    public void uncacheOutput() {
        binding.uncacheOutput();
    }

    @Override
    public void uncacheValues() {
        binding.uncacheValues();
    }
}
