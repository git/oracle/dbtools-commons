/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes;

import java.util.Collection;
import java.util.List;

import oracle.dbtools.raptor.datatypes.marshal.DataValueUnmarshalEventListener;

public interface DataType extends DataValueUnmarshalEventListener {
    public interface Types {
        public final int RECORD = -333;
    }
    
    public TypeMetadata getTypeMetadata();

    public DataValue getDataValue(Object object);

    public Collection<NamedValue<DataType>> getTypeComponents();

    public Iterable<String> getComponentNames();
  
    public Class getTypedClass(ValueType valueType);

    /**
     * Used for test value look-up table key I think
     * @return
     */
    public String getDataTypeString();

    public String getBaseDataTypeString();

    public String getPlainDataTypeString();

    public String getUnconstrainedDataTypeString();

    public String getConstrainedDataTypeString();

    public String getUserDataTypeString();

    public <P extends DataBinding> BindingStrategy getBind(BindContext context, P param);

    public int getSqlDataType(ValueType valueType);

    // For standard Raptor cell editor usage - see DataTypeImpl

    /**
     * Called before the cell editor stops editing
     * @return null if the edited value is valid. If invalid return the message to be displayed
     */
    public String validateValue(Object object);

    public boolean isEnumeration();

    public List<DataValue> getEnumerationValues();

    public boolean isSupported();
    
    public StructureType getStructureType();
    
    public boolean requiresConnection();
    public DataTypeConnectionProvider getDataTypeConnectionProvider();
    public DataTypeFactory getDataTypeFactory();
}
