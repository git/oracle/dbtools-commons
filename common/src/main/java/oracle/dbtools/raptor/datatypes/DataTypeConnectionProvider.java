/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes;

import java.sql.Connection;

import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension.ExtensionAccess;
import oracle.dbtools.raptor.datatypes.DataTypeContext.ContextAccess;

/**
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=DataTypeConnectionProvider.java"
 *         >Barry McGillin</a>
 * 
 * @param <C>
 */
public interface DataTypeConnectionProvider<C extends Connection> extends DataTypeFactoryExtension.Extendable {
    final static String CONNECTION = "connection"; //$NON-NLS-1$

    interface ConnectionProviderAccess<C extends Connection> extends ExtendableAccess, PropertyChanger {
        DataTypeFactory getDataTypeFactory();
        ExtensionAccess getExtensionAccess();
        DataTypeConnectionProvider<C> getDataTypeConnectionProvider();
        
        ContextAccess getContextAccess();
        ContextAccess peekContextAccess();
    }

    interface Builder<C extends Connection> extends DataTypeFactoryExtension.Extendable.Builder {
        ConnectionProviderAccess<C> build();
        
        Class<C> getConnectionClass();
        
        Builder setExtensionAccess(ExtensionAccess extensionAccess);

        Builder setDataTypeConnectionReference(DataTypeConnectionReference<C> connectionReference);
        DataTypeConnectionReference<C> getDataTypeConnectionReference();
        
        Builder setNLSConnectionWrapper(DataTypeConnectionWrapper<C> connectionWrapper);
        DataTypeConnectionWrapper<C> getNLSConnectionWrapper();
        
        Builder setSupported(boolean suppored);
        boolean isSupported();
    }
    
    DataTypeFactoryExtension getDataTypeFactoryExtension();
    
    DataTypeContext getDataTypeContext();

    /*
     * DataTypeConnection
     */
    
    DataTypeConnectionWrapper<C> getDataTypeConnectionWrapper();

    @Deprecated
	C getDataTypeConnection() throws DataTypeConnectionException;

    @Deprecated
	C peekDataTypeConnection();

    @Deprecated
	C getValidDataTypeConnection() throws DataTypeConnectionException;

    @Deprecated
	C lockDataTypeConnection() throws DataTypeConnectionException;

    @Deprecated
	C lockDataTypeConnection(boolean prompt) throws DataTypeConnectionException;

    @Deprecated
	void unlockDataTypeConnection() throws DataTypeConnectionException;

    /*
     * NLSConnection
     */
    
    DataTypeConnectionWrapper<C> getNLSConnectionWrapper();

    @Deprecated
	C getNLSConnection() throws DataTypeConnectionException;

    @Deprecated
	C peekNLSConnection();

    @Deprecated
	C getValidNLSConnection() throws DataTypeConnectionException;

    @Deprecated
	C lockNLSConnection() throws DataTypeConnectionException;

    @Deprecated
	C lockNLSConnection(boolean prompt) throws DataTypeConnectionException;

    @Deprecated
	void unlockNLSConnection() throws DataTypeConnectionException;

	Class<C> getDataTypeConnnectionClass();
    
    boolean isSupported();
}
