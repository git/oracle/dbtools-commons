/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.io.Serializable;

import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider.ConnectionProviderAccess;
import oracle.dbtools.raptor.datatypes.DataTypeContext.ContextAccess;

import oracle.dbtools.raptor.datatypes.extensions.OraclePLSQLDataTypeFactoryExtension;
import oracle.dbtools.raptor.datatypes.extensions.OracleSQLDataTypeFactoryExtension;
import oracle.dbtools.raptor.datatypes.extensions.UnsupportedDataTypeFactoryExtension;
import oracle.dbtools.raptor.datatypes.extensions.UnsupportedDataTypeFactoryExtension.UnsupportedDataProvider;
import oracle.dbtools.raptor.datatypes.impl.BindContextImpl;
import oracle.dbtools.raptor.datatypes.impl.DataParameterImpl;
import oracle.dbtools.raptor.datatypes.impl.DataVariableImpl;

import oracle.dbtools.raptor.datatypes.oracle.plsql.OraclePLSQLDataTypeFactory;
import oracle.dbtools.raptor.datatypes.oracle.sql.OracleSQLDataTypeFactory;
import oracle.dbtools.raptor.datatypes.util.ConcurrentWeakIdentityHashMap;

import oracle.dbtools.util.Closeables;

/**
 * Implements a Data Type Factory
 */
public class DataTypeFactory implements Serializable {

    @SuppressWarnings("compatibility:-604447334892762242")
    private static final long serialVersionUID = 1L;
    
    private static List<DataTypeFactoryExtension.Builder> defaultExtensionList = defaultExtensionList();;

    private final DataTypeFactoryExtensionManager extensionManager;
    
    private final FactoryAccess factoryAccess;
    private final Map<Connection, ConnectionProviderAccess> connectionProviderCache;
    private final Map<DataTypeConnectionProvider, ContextAccess> contextCache;
    private final PropertyChangeListener connectionChangeListener;
    private final PropertyChangeListener connectionProviderChangeListener;
    
    private final boolean local;
    
    private int creates;
    private int frees;
    private int reuses;

    private static class DataTypeFactoryInit {
        public static final DataTypeFactory instance = new DataTypeFactory(false, 
                                   (DataTypeFactory.defaultExtensionList != null) 
                                     ? DataTypeFactory.defaultExtensionList 
                                     : defaultExtensionList());
    }

    // Damn you serialization
    @SuppressWarnings("unused")
    private DataTypeFactory readResolve() {
        return DataTypeFactoryInit.instance;
    }
    
    public class FactoryAccess {
        private FactoryAccess() {};
        
        public DataTypeFactory getDataTypeFactory() {
            return DataTypeFactory.this;
        }
        
        public ContextAccess getContextAccess(ConnectionProviderAccess connectionProviderAccess) {
            return DataTypeFactory.this.getContextAccess(connectionProviderAccess);
        }
        
        public ContextAccess peekContextAccess(ConnectionProviderAccess connectionProviderAccess) {
            return DataTypeFactory.this.peekContextAccess(connectionProviderAccess);
        }
    }

    /**
     * Protected Constructor
     */
    private DataTypeFactory(boolean local, List<DataTypeFactoryExtension.Builder> extensionList) {
        // Initialize to null - providers initialized on demand
        this.creates = 0;
        this.frees = 0;
        this.reuses = 0;
        this.local = local;
         
        this.factoryAccess = new FactoryAccess();
        this.connectionProviderCache = new ConcurrentWeakIdentityHashMap<Connection, ConnectionProviderAccess>();
        this.contextCache = new ConcurrentWeakIdentityHashMap<DataTypeConnectionProvider, ContextAccess>();
        this.extensionManager = new DataTypeFactoryExtensionManager(this.factoryAccess, extensionList);
        this.connectionChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (DataTypeConnectionProvider.CONNECTION.equals(evt.getPropertyName())) {
                    // Unpack event
                    ConnectionProviderAccess srcConnectionProviderAccess = (ConnectionProviderAccess)evt.getSource();
                    Connection oldConnection = (Connection)evt.getOldValue();
                    Connection newConnection = (Connection)evt.getNewValue();
                    
                    // Handle old connection provider
                    if (oldConnection != null) {
                        ConnectionProviderAccess oldConnectionProviderAccess = connectionProviderCache.get(oldConnection);
                         
                        if (oldConnectionProviderAccess != null) {
                            if (oldConnectionProviderAccess == srcConnectionProviderAccess) {
                                connectionProviderCache.remove(oldConnection);
                            } else {
                                // Close unexpected connection provider
                                Closeables.close(oldConnectionProviderAccess.getDataTypeConnectionProvider());
                            }
                        }
                    }
                     
                    // Handle new connection provider
                    if (newConnection != null && srcConnectionProviderAccess != null) {
                        connectionProviderCache.put(newConnection, srcConnectionProviderAccess);
                    }
                }
            }
        };
        this.connectionProviderChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (DataTypeContext.CONNECTION_PROVIDER.equals(evt.getPropertyName())) {
                    // Unpack event
                    ContextAccess srcContextAccess = (ContextAccess)evt.getSource();
                    ConnectionProviderAccess oldProviderAccess = (ConnectionProviderAccess)evt.getOldValue();
                    ConnectionProviderAccess newProviderAccess = (ConnectionProviderAccess)evt.getNewValue();
                     
                    // Remove old context
                    if (oldProviderAccess != null) {
                        ContextAccess oldContextAccess = contextCache.get(oldProviderAccess.getDataTypeConnectionProvider());
                         
                         if (oldContextAccess != null) {
                            if (oldContextAccess == srcContextAccess) {
                                // Remove this provider
                                contextCache.remove(oldProviderAccess.getDataTypeConnectionProvider());
                            } else {
                                // Close unexpected provider
                                Closeables.close(oldContextAccess.getDataTypeContext());
                            }
                        }
                    }
                    
                    // Add new context
                    if (newProviderAccess != null && srcContextAccess != null) {
                        contextCache.put(newProviderAccess.getDataTypeConnectionProvider(), srcContextAccess);
                    }
                }
            }
        };
    }

    /**
     * Singleton Instance
     * 
     * @return Data Type Factory
     */
    public static DataTypeFactory getInstance() {
        return DataTypeFactoryInit.instance;
    }

    /**
     * Local Instance
     * 
     * @return Data Type Factory
     */
    public static DataTypeFactory getLocalInstance(List<DataTypeFactoryExtension.Builder> extensionList) {
        return new DataTypeFactory(true, extensionList);
    }
    
    /**
     * Local Instance
     * 
     * @return Data Type Factory
     */
    public static DataTypeFactory getLocalInstance() {
        return new DataTypeFactory(true, 
                                   (DataTypeFactory.defaultExtensionList != null) 
                                     ? DataTypeFactory.defaultExtensionList 
                                     : defaultExtensionList());
    }
    
    public boolean isLocal() {
        return local;
    }
    
    public static void setDefaultExtensionList(List<DataTypeFactoryExtension.Builder> defaultExtensionList) {
        DataTypeFactory.defaultExtensionList = defaultExtensionList;
    }
    
    /*
     * DataType
     */
    private static LinkedList<DataTypeFactoryExtension.Builder> defaultExtensionList() {
        LinkedList<DataTypeFactoryExtension.Builder> extensionList = new LinkedList<DataTypeFactoryExtension.Builder>();
        UnsupportedDataTypeFactoryExtension.Builder unsupportedExtension =
            UnsupportedDataTypeFactoryExtension.builder(UnsupportedDataTypeFactoryExtension.Builder.ID)
                .setDataTypeProvider(UnsupportedDataProvider.builder(UnsupportedDataProvider.Builder.ID));
        extensionList.add(unsupportedExtension);
        
        OracleSQLDataTypeFactoryExtension.Builder sqlExtension = 
            OracleSQLDataTypeFactoryExtension.builder(OracleSQLDataTypeFactoryExtension.Builder.ID)
                .setSuperExtensionURI(UnsupportedDataTypeFactoryExtension.Builder.ID)
                .setDataTypeProvider(OracleSQLDataTypeFactory.builder(OracleSQLDataTypeFactory.Builder.ID)
                                     .setSuperDataTypeProviderURI(UnsupportedDataProvider.Builder.ID));
        extensionList.add(sqlExtension);
        
        OraclePLSQLDataTypeFactoryExtension.Builder plsqlExtension = 
            OraclePLSQLDataTypeFactoryExtension.builder(OraclePLSQLDataTypeFactoryExtension.Builder.ID)
                .setSuperExtensionURI(OracleSQLDataTypeFactoryExtension.Builder.ID)
                .setDataTypeProvider(OraclePLSQLDataTypeFactory.builder(OraclePLSQLDataTypeFactory.Builder.ID)
                                     .setSuperDataTypeProviderURI(OracleSQLDataTypeFactory.Builder.ID));
        extensionList.add(plsqlExtension);
        
        return extensionList;
    }
    
    /**
     * Get Data Type
     * 
     * @param connection
     * @param typeMetadata
     * @return
     */
    public DataType getDataType(Connection connection, TypeMetadata typeMetadata) {
        return getDataType(getDataTypeConnectionProvider(connection), typeMetadata);
    }
    
    /**
     * Get Data Type
     * 
     * @param connectionProvider
     * @param typeMetadata
     * @return
     */
    public DataType getDataType(DataTypeConnectionProvider connectionProvider, TypeMetadata typeMetadata) {
        return getDataType(connectionProvider.getDataTypeContext(), typeMetadata);
    }

    /**
     * Get Data Type
     * 
     * @param context
     * @param typeMetadata
     * @return
     */
    public DataType getDataType(DataTypeContext context, TypeMetadata typeMetadata) {
        return context.getDataType(typeMetadata);
    }

    /**
     * Lookup Data Type
     * 
     * @param connection
     * @param typeMetadata
     * @return
     */
    public DataType lookupDataType(Connection connection, TypeMetadata typeMetadata) {
        return lookupDataType(getDataTypeConnectionProvider(connection), typeMetadata);
    }
    
    /**
     * Lookup Data Type
     * 
     * @param connectionProvider
     * @param typeMetadata
     * @return
     */
    public DataType lookupDataType(DataTypeConnectionProvider connectionProvider, TypeMetadata typeMetadata) {
        return lookupDataType(connectionProvider.getDataTypeContext(), typeMetadata);
    }

    /**
     * Lookup Data Type
     * 
     * @param context
     * @param typeMetadata
     * @return
     */
    public DataType lookupDataType(DataTypeContext context, TypeMetadata typeMetadata) {
        return context.lookupDataType(typeMetadata);
    }

    public void mapDataType(TypeMetadata typeMetadata, DataType dataType) {
        if (dataType != null) {
            dataType.getDataTypeConnectionProvider().getDataTypeContext().mapDataType(typeMetadata, dataType);
        }
    }

    public DataType buildDataType(DataTypeContext context, TypeMetadata typeMetadata) {
        return extensionManager.getDataType(context, typeMetadata);
    }

    public <P extends DataBinding> BindingStrategy getBind(BindContext context, P param) {
        return param.getDataType().getBind(context, param);
    }

    /**
     * Tests if Type is supported
     * 
     * @param connectionProvider
     * @param typeMetadata
     * @return
     */
    public boolean isSupportedType(DataTypeConnectionProvider connectionProvider, TypeMetadata typeMetadata) {
        try {
            DataType datatype = getDataType(connectionProvider, typeMetadata);
            
            return (datatype != null && datatype.isSupported());
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage(), e);        
        }
        
        return false;
    }

    /**
     * Tests if Binding is supported
     * 
     * @param <P>
     * @param context
     * @param param
     * @return
     */
    public <P extends DataBinding> boolean isSupportedBinding(BindContext context, P param) {
        try {
            BindingStrategy binding = getBind(context, param);
            
            return binding.isSupported(param.getMode());
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage(), e);        
        }
        
        return false;
    }

    /**
     * Tests if Type is supported
     * 
     * @param connection
     * @param typeMetadata
     * @return
     */
    public boolean isSupportedType(Connection connection, TypeMetadata typeMetadata) {
        return isSupportedType(getDataTypeConnectionProvider(connection), typeMetadata);
    }

    /**
     * Tests if Type is supported
     * 
     * @param connectionProvider
     * @param oracleType
     * @return
     */
    public boolean isSupportedType(DataTypeConnectionProvider connectionProvider, Integer oracleType) {
        try {
            TypeMetadata typeMetadata = getTypeMetadata(connectionProvider, oracleType);
            
            return isSupportedType(connectionProvider, typeMetadata);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage(), e);        
        }
        
        return false;
    }

    /**
     * Tests if Type is supported
     * 
     * @param connection
     * @param oracleType
     * @return
     */
    public boolean isSupportedType(Connection connection, Integer oracleType) {
        return isSupportedType(getDataTypeConnectionProvider(connection), oracleType);
    }

    public Set<String> getSupportedTypes(Class<? extends Connection> connectionClass) {
        return extensionManager.getSupportedTypes(connectionClass);
    }

    public Set<String> getSupportedTypes(DataTypeConnectionProvider connectionProvider) {
        return getSupportedTypes(connectionProvider.getDataTypeConnnectionClass());
    }

    public Set<String> getSupportedTypes(Connection connection) {
        
        return getSupportedTypes(connection.getClass());
    }

    public Set<Integer> getSupportedSQLTypes(Class<? extends Connection> connectionClass) {
        return extensionManager.getSupportedSQLTypes(connectionClass);
    }

    public Set<Integer> getSupportedSQLTypes(DataTypeConnectionProvider connectionProvider) {
        return getSupportedSQLTypes(connectionProvider.getDataTypeConnnectionClass());
    }

    public Set<Integer> getSupportedSQLTypes(Connection connection) {
        return getSupportedSQLTypes(connection.getClass());
    }

    /*
     * TypeMetadata
     */

    public Map<String, TypeMetadata> getSupportedTypeMetadata(Class<? extends Connection> connectionClass) {
        return extensionManager.getSupportedTypeMetadata(connectionClass);
    }

    public Map<String, TypeMetadata> getSupportedTypeMetadata(DataTypeConnectionProvider connectionProvider) {
        return getSupportedTypeMetadata(connectionProvider.getDataTypeConnnectionClass());
    }

    public Map<String, TypeMetadata> getSupportedTypeMetadata(Connection connection) {
        return getSupportedTypeMetadata(connection.getClass());
    }

    public TypeMetadata getTypeMetadata(Map<TypeMetadata.Attribute, Object> attributes) {
        return new TypeMetadata(attributes);

    }

    public static TypeMetadata getTypeMetadata(Map<TypeMetadata.Attribute, Object> attributes, boolean syntaxSynonym) {
        return new TypeMetadata(attributes, syntaxSynonym);

    }

    public static TypeMetadata getTypeMetadata(TypeMetadata source, Map<TypeMetadata.Attribute, Object> attributes) {
        return TypeMetadata.overideTypeMetadata(source, attributes);

    }

    public static TypeMetadata getTypeMetadata(TypeMetadata source, TypeMetadata source2) {
        return TypeMetadata.overideTypeMetadata(source, source2);

    }

    public TypeMetadata getTypeMetadata(DataTypeConnectionProvider connectionProvider, Integer oracleType) {
        return extensionManager.getTypeMetadata(connectionProvider, oracleType);
    }

    public TypeMetadata getTypeMetadata(Connection connection, Integer oracleType) {
        return getTypeMetadata(getDataTypeConnectionProvider(connection), oracleType);
    }

    public TypeMetadata getTypeMetadata(DataTypeConnectionProvider connectionProvider, String typeName) {
        return extensionManager.getTypeMetadata(connectionProvider, typeName);
    }

    public TypeMetadata getTypeMetadata(Connection connection, String typeName) {
        return getTypeMetadata(getDataTypeConnectionProvider(connection), typeName);
    }

    public TypeMetadata getTypeMetadata(ResultSetMetaData resultSetMetadata, int column) {
        try {
            final int typeCode = resultSetMetadata.getColumnType(column);
            final String datatype = resultSetMetadata.getColumnTypeName(column);
            final int precision = resultSetMetadata.getPrecision(column);
            final int scale = resultSetMetadata.getScale(column);
            
            return getTypeMetadata(typeCode, datatype, (precision != 0) ? precision : null, (precision != 0) ? scale : null);
        } catch (SQLException e) {
            throw new DataTypeSQLException(e);
        }
    }

    public TypeMetadata getTypeMetadata(Integer oracleType, String typeName, Integer precision, Integer scale) {
        return new TypeMetadata(oracleType, typeName, precision, scale);
    }

    public TypeMetadata getTypeMetadata(Integer oracleType, String typeName) {
        return new TypeMetadata(oracleType, typeName, null, null);
    }

    /*
     * Variable, Parameter and Binding
     */

    public DataVariable getDataVariable(String name, DataType dataType, BindingMode mode) {
        return new DataVariableImpl(name, dataType, mode);
    }

    public DataParameter getDataParameter(String name, DataType dataType, BindingMode mode) {
        return new DataParameterImpl(name, dataType, mode);
    }
    
    public <S extends Statement> BindContext<S> getBindContext(Class<? extends S> stmtclass, DataTypeConnectionProvider connectionProvider) {
        return new BindContextImpl<S>(stmtclass, connectionProvider);
    }

    public <S extends Statement> BindContext<S> getBindContext(Class<? extends S> stmtclass, Connection connection) {
        return getBindContext(stmtclass, getDataTypeConnectionProvider(connection));
    }

    /*
     * DataTypeContext
     */

    private final ContextAccess getContextAccess(ConnectionProviderAccess connectionProviderAccess) {
        return resolveDataTypeContext(connectionProviderAccess);
    }
    
    private final ContextAccess peekContextAccess(ConnectionProviderAccess connectionProviderAccess) {
        return contextCache.get(connectionProviderAccess.getDataTypeConnectionProvider());
    }
    
    private final ContextAccess resolveDataTypeContext(ConnectionProviderAccess connectionProviderAccess) {
        ContextAccess contextAccess = peekContextAccess(connectionProviderAccess);
        
        if (contextAccess == null) {
            synchronized(contextCache) {
                contextAccess = peekContextAccess(connectionProviderAccess);
                if (contextAccess == null) {
                    contextAccess = buildDataTypeContext(connectionProviderAccess);
                    creates++;
                } else {
                    reuses++;
                }
            }
        } else {
            reuses++;
        }
        
        return contextAccess;
    }
    
    protected ContextAccess buildDataTypeContext(ConnectionProviderAccess connectionProviderAccess) {
        ContextAccess contextAccess = extensionManager.buildDataTypeContext(connectionProviderAccess);
        
        addConnectionProviderChangeListener(contextAccess);
        
        if (connectionProviderAccess != null && contextAccess != null) {
            contextCache.put(connectionProviderAccess.getDataTypeConnectionProvider(), contextAccess);
        }
        
        return contextAccess;
    }

    /*
     * DataTypeConnectionProvider
     */

    public final <C extends Connection> DataTypeConnectionProvider getDataTypeConnectionProvider(C connection) {
        return resolveDataTypeConnectionProvider(connection).getDataTypeConnectionProvider();
    }
    
    public final <C extends Connection> DataTypeConnectionProvider getDataTypeConnectionProvider(DataTypeConnectionReference<C> connectionReference) {
        return getDataTypeConnectionProvider(connectionReference, null);
    }
    
    public final <C extends Connection> DataTypeConnectionProvider getDataTypeConnectionProvider(DataTypeConnectionReference<C> connectionReference, DataTypeConnectionWrapper<C> nlsWrapper) {
        return resolveDataTypeConnectionProvider(connectionReference, nlsWrapper).getDataTypeConnectionProvider();
    }
    
    public final <C extends Connection> void release(C Connection) {
        ConnectionProviderAccess<C> provider = connectionProviderCache.get(Connection);
        if (provider != null) {
            Closeables.close(provider.getDataTypeConnectionProvider());
        }
    }
    
    public final <C extends Connection> void release(DataTypeConnectionProvider<C> connectionProvider) {
        Closeables.close(connectionProvider);
    }
    
    public final <C extends Connection> DataTypeConnectionProvider<C> getDataTypeConnectionProvider(Class<C> connectionClass) {
        return buildDataTypeConnectionProvider(connectionClass).getDataTypeConnectionProvider();
    }
    
    private final <C extends Connection> ConnectionProviderAccess<C> resolveDataTypeConnectionProvider(C connection) {
        ConnectionProviderAccess<C> connectionProviderAccess = connectionProviderCache.get(connection);
        
        if (connectionProviderAccess == null) {
            synchronized(connectionProviderCache) {
                connectionProviderAccess = connectionProviderCache.get(connection);
                if (connectionProviderAccess == null) {
                    connectionProviderAccess = buildDataTypeConnectionProvider(connection);
                    creates++;
                } else {
                    reuses++;
                }
            }
            creates++;
        } else {
            reuses++;
        }
        
        return connectionProviderAccess;
    }
    
    private final <C extends Connection> ConnectionProviderAccess<C> resolveDataTypeConnectionProvider(DataTypeConnectionReference<C> connectionReference, DataTypeConnectionWrapper<C> nlsWrapper) {
        return buildDataTypeConnectionProvider(connectionReference, nlsWrapper);
    }
    
    protected <C extends Connection> ConnectionProviderAccess<C> buildDataTypeConnectionProvider(C connection) {
        return buildDataTypeConnectionProvider((connection == null) ? (Class<C>)Connection.class : (Class<C>)connection.getClass(), connection);
    }

    protected <C extends Connection> ConnectionProviderAccess<C> buildDataTypeConnectionProvider(Class<C> connectionClass) {
        return buildDataTypeConnectionProvider(connectionClass, null);
    }
    
    protected <C extends Connection> void addConnectionChangeListener(ConnectionProviderAccess<C> connectionProviderAccess) {
        connectionProviderAccess.addPropertyChangeListener(connectionChangeListener);
    }

    protected <C extends Connection> void addConnectionProviderChangeListener(ContextAccess contextAccess) {
        contextAccess.addPropertyChangeListener(connectionProviderChangeListener);
    }

    protected <C extends Connection> ConnectionProviderAccess<C> buildDataTypeConnectionProvider(Class<C> connectionClass, C connection) {
        ConnectionProviderAccess<C> connectionProviderAccess = extensionManager.buildDataTypeConnectionProvider(connectionClass, connection);
        
        // Add Connection Change Handler
        addConnectionChangeListener(connectionProviderAccess);
        
        if (connection != null && connectionProviderAccess != null) {
            connectionProviderCache.put(connection, connectionProviderAccess);
        }
        
        return connectionProviderAccess;
    }
    
    protected <C extends Connection> ConnectionProviderAccess<C> buildDataTypeConnectionProvider(DataTypeConnectionReference<C> connectionReference) {
        return buildDataTypeConnectionProvider(connectionReference, null);
    }
    
    protected <C extends Connection> ConnectionProviderAccess<C> buildDataTypeConnectionProvider(DataTypeConnectionReference<C> connectionReference, DataTypeConnectionWrapper<C> nlsWrapper) {
        ConnectionProviderAccess<C> connectionProviderAccess = extensionManager.buildDataTypeConnectionProvider(connectionReference, nlsWrapper);
        
        // Add Connection Change Handler
        addConnectionChangeListener(connectionProviderAccess);
        
        if (connectionProviderAccess != null) {
            DataTypeConnectionProvider<C> provider = connectionProviderAccess.getDataTypeConnectionProvider();
            C connection = provider.peekDataTypeConnection();
            
            if (connection != null) {
                connectionProviderCache.put(connection, connectionProviderAccess);
            }
        }
        
        return connectionProviderAccess;
    }
    
    /*
     * Cache Maintenance
     */
    
    private void doCleanupCache() {
        synchronized(contextCache) {
            // Cleanup DataTypeContext Cache
            Iterator<Map.Entry<DataTypeConnectionProvider, ContextAccess>> contextCacheIter = contextCache.entrySet().iterator();
            while (contextCacheIter.hasNext()) {
                contextCacheIter.next().getValue().cleanupDataTypeCache();
            }
        }
    }
}
