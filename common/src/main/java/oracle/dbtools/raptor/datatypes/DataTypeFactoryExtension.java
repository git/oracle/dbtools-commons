/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes;

import java.io.Closeable;

import java.net.URI;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtensionManager.ExtensionManagerAccess;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider.ConnectionProviderAccess;
import oracle.dbtools.raptor.datatypes.DataTypeContext.ContextAccess;

public interface DataTypeFactoryExtension {
    interface ExtensionAccess {
        DataTypeFactory getDataTypeFactory();
        ExtensionManagerAccess getExtensionManagerAccess();
        DataTypeFactoryExtension getDataTypeFactoryExtension();
        
        DataTypeProvider lookupDataTypeProvider(URI dataTypeProviderURI);
        DataTypeProvider resolveDataTypeProvider(URI dataTypeProviderURI);
        
        ContextAccess getContextAccess(ConnectionProviderAccess connectionProviderAccess);
        ContextAccess peekContextAccess(ConnectionProviderAccess connectionProviderAccess);
        
        DataType resolveDataType(DataTypeContext context, TypeMetadata typeMetadata);
    }
    
    interface Extendable extends Closeable {
        interface ExtendableAccess {
            DataTypeFactory getDataTypeFactory();
            ExtensionAccess getExtensionAccess();
        }
        
        interface Builder {
            ExtendableAccess build();
            Builder setExtensionAccess(ExtensionAccess extensionAccess);
            ExtensionAccess getExtensionAccess();
        }
        
        DataTypeFactoryExtension getDataTypeFactoryExtension();
        DataTypeFactory getDataTypeFactory();
    }
    
    interface Builder {
        ExtensionAccess build();
        
        URI getExensionURI();

        Builder setExtensionManagerAccess(ExtensionManagerAccess extensionManagerAccess);
        ExtensionManagerAccess getExtensionManagerAccess();
        
        Builder setSuperExtensionURI(URI extensionURI);
        URI getSuperExensionURI();

        Builder setDataTypeProvider(DataTypeProvider.Builder dataTypeProvider);
        DataTypeProvider.Builder getDataTypeProvider();
    }
    
    Extendable.Builder builder(Class<?> clazz, Object... obj);
    
    boolean handlesExplicitly(Class<?> clazz);
    boolean handles(Class<?> clazz);
    
    DataTypeFactoryExtensionManager getDataTypeFactoryExtensionManager();
    
    DataTypeFactory getDataTypeFactory();
    
    DataTypeProvider getDataTypeProvider();
}
