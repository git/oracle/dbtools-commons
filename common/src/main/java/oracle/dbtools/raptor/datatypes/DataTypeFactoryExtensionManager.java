/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes;

import java.net.URI;

import java.sql.Connection;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import java.util.Set;

import java.util.TreeSet;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import oracle.dbtools.raptor.datatypes.DataTypeFactory.FactoryAccess;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension.ExtensionAccess;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider.ConnectionProviderAccess;
import oracle.dbtools.raptor.datatypes.DataTypeContext.ContextAccess;

public class DataTypeFactoryExtensionManager {
    private final CopyOnWriteArrayList<ExtensionAccess> extensions;
    private final ConcurrentHashMap<URI, ExtensionAccess> extensionMap;
    private final FactoryAccess factoryAccess;
    private final ExtensionManagerAccess extensionManagerAccess;
    
    DataTypeFactoryExtensionManager(FactoryAccess dataTypeFactoryAccess, List<DataTypeFactoryExtension.Builder> extensionList) {
        super();
        this.extensionManagerAccess = new ExtensionManagerAccess();
        this.factoryAccess = dataTypeFactoryAccess;
        this.extensions = new CopyOnWriteArrayList<ExtensionAccess>();
        this.extensionMap = new ConcurrentHashMap<URI, ExtensionAccess>();
        
        for (DataTypeFactoryExtension.Builder extensionBuilder : extensionList) {
            addExtension(extensionBuilder);
        }
    }
    
    public class ExtensionManagerAccess {
        private ExtensionManagerAccess() {};
        
        public FactoryAccess getFactoryAccess() {
            return factoryAccess;
        }        
        
        public DataTypeFactory getDataTypeFactory() {
            return DataTypeFactoryExtensionManager.this.getDataTypeFactory();
        }        
        
        public final DataTypeFactoryExtensionManager getDataTypeFactoryExtensionManager() {
            return DataTypeFactoryExtensionManager.this;
        }
        
        public ExtensionAccess lookupExtension(URI extensionURI) {
            return DataTypeFactoryExtensionManager.this.lookupExtension(extensionURI);
        }
        
        public ContextAccess getContextAccess(ConnectionProviderAccess connectionProviderAccess) {
            return getFactoryAccess().getContextAccess(connectionProviderAccess);
        }
        
        public ContextAccess peekContextAccess(ConnectionProviderAccess connectionProviderAccess) {
            return getFactoryAccess().peekContextAccess(connectionProviderAccess);
        }
    }
    
    final DataTypeFactory getDataTypeFactory() {
        return factoryAccess.getDataTypeFactory();
    }
    
    synchronized void addExtension(DataTypeFactoryExtension.Builder extensionBuilder) {
        ExtensionAccess extension = extensionBuilder.setExtensionManagerAccess(extensionManagerAccess).build();
        extensionMap.put(extensionBuilder.getExensionURI(), extension);
        extensions.add(0,extension);
    }
    
    ExtensionAccess lookupExtension(URI extensionURI) {
        return (extensionURI != null) ? extensionMap.get(extensionURI) : null;
    }
    
    <C extends Connection> DataTypeFactoryExtension extensionFor(Class<C> connectionClass) {
        for (ExtensionAccess extensionAccess : extensions) {
            if (extensionAccess != null) {
                DataTypeFactoryExtension extension = extensionAccess.getDataTypeFactoryExtension();
                if (extension.handlesExplicitly(connectionClass)) {
                    return extension;
                }
            }
        }
        
        return null; // TODO Handle Unsupported better
    }
    
    DataType getDataType(DataTypeContext context, TypeMetadata typeMetadata) {
        return context.getDataTypeFactoryExtension().getDataTypeProvider().getDataType(context, typeMetadata);
    }
    
    TypeMetadata getTypeMetadata(DataTypeConnectionProvider connectionProvider, String typeName) {
        return connectionProvider.getDataTypeFactoryExtension().getDataTypeProvider().getTypeMetadata(typeName);
    }
    
    TypeMetadata getTypeMetadata(DataTypeConnectionProvider connectionProvider, Integer oracleType) {
        return connectionProvider.getDataTypeFactoryExtension().getDataTypeProvider().getTypeMetadata(oracleType);
    }
    
    Map<String, TypeMetadata> getSupportedTypeMetadata(Class<? extends Connection> connectionClass) {
        DataTypeFactoryExtension extension = extensionFor(connectionClass);
        if (extension != null) {
            DataTypeProvider provider = extension.getDataTypeProvider();
            
            if (provider != null) {
                return provider.getSupportedTypeMetadata();
            }
        }
        
        return new HashMap<String, TypeMetadata>();
    }

    Set<Integer> getSupportedSQLTypes(Class<? extends Connection> connectionClass) {
        DataTypeFactoryExtension extension = extensionFor(connectionClass);
        if (extension != null) {
            DataTypeProvider provider = extension.getDataTypeProvider();
            
            if (provider != null) {
                return provider.getSupportedSQLTypes();
            }
        }
        
        return new TreeSet<Integer>();
    }
    
    Set<String> getSupportedTypes(Class<? extends Connection> connectionClass) {
        DataTypeFactoryExtension extension = extensionFor(connectionClass);
        if (extension != null) {
            DataTypeProvider provider = extension.getDataTypeProvider();
            
            if (provider != null) {
                return provider.getSupportedTypes();
            }
        }
        
        return new TreeSet<String>();
    }
    
    <C extends Connection> ConnectionProviderAccess<C> buildDataTypeConnectionProvider(Class<C> connectionClass, C connection) {
        DataTypeFactoryExtension extension = extensionFor(connectionClass);
        
        return (ConnectionProviderAccess<C>)extension.builder(ConnectionProviderAccess.class, connection)
            .build();
    }

    <C extends Connection> ConnectionProviderAccess<C> buildDataTypeConnectionProvider(DataTypeConnectionReference<C> connectionReference) {
        DataTypeFactoryExtension extension = extensionFor(connectionReference.getConnectionClass());
        
        return (ConnectionProviderAccess<C>)extension.builder(ConnectionProviderAccess.class, connectionReference)
            .build();
    }

    <C extends Connection> ConnectionProviderAccess<C> buildDataTypeConnectionProvider(DataTypeConnectionReference<C> connectionReference, DataTypeConnectionWrapper<C> nlsWrapper) {
        DataTypeFactoryExtension extension = extensionFor(connectionReference.getConnectionClass());
        
        return (ConnectionProviderAccess<C>)extension.builder(ConnectionProviderAccess.class, connectionReference, nlsWrapper)
            .build();
    }

    ContextAccess buildDataTypeContext(ConnectionProviderAccess connectionProviderAccess) {
        DataTypeFactoryExtension extension = connectionProviderAccess.getExtensionAccess().getDataTypeFactoryExtension();
        
        return (ContextAccess)extension.builder(ContextAccess.class, connectionProviderAccess)
                .build();
    }
}
