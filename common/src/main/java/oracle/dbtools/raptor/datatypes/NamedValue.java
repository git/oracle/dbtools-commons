/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes;

/**
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=NamedValue.java"
 *         >Barry McGillin</a>
 * 
 * @param <V>
 */
public class NamedValue<V> {
	protected final String name;
	protected final V value;
	private volatile Integer hashCode;

	public NamedValue(V value) {
		this(null, value);
	}

	public NamedValue(String name, V value) {
		this.name = name;
		this.value = value;
		this.hashCode = null;
	}

	public String getName() {
		return this.name;
	}

	public V getValue() {
		return value;
	}

	@Override
	public boolean equals(Object obj) {
		boolean isEqual = (this == obj);

		if (!isEqual && obj instanceof NamedValue) {
			NamedValue namedValue = (NamedValue) obj;

			isEqual = (areEquals(this.name, namedValue.name) && areEquals(this.value, namedValue.value));
		}

		return isEqual;
	}

	private static boolean areEquals(Object obj1, Object obj2) {
		return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
	}

	public int hashCode() {
		if (hashCode == null) {
            synchronized(this) {
                if (hashCode == null) {
                    int hash = 23;
                    hash = 31 * hash + (null == name ? 0 : name.hashCode());
                    hash = 31 * hash + (null == value ? 0 : value.hashCode());
                    hashCode = hash;
                }
            }
		}

		return hashCode;
	}
}
