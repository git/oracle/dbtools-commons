/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=PLSQLBlockBuilder.java">Barry McGillin</a> 
 *
 */
public class PLSQLBlockBuilder {
    public static final String IDENT = " "; //$NON-NLS-1$
    protected ArrayList<LinkedList<String>> components;

    public PLSQLBlockBuilder() {
        int len = PLSQLBlockComponent.values().length;
        components = new ArrayList<LinkedList<String>>(len);

        // Initialize Component
        for (int i = 0; i < len; i++) {
            components.add(null);
        }
    }

    public void setComponent(PLSQLBlockComponent component, List<String> value) {
        LinkedList<String> newComponent = null;

        if (value != null) {
            newComponent =
                    addLists(component.getPrepend(), component.getUnique(), newComponent, value);
        }

        components.set(component.getSequence(), newComponent);
    }

    public void setComponent(PLSQLBlockComponent component, String[] value) {
        LinkedList<String> newComponent = null;

        if (value != null) {
            newComponent =
                    addLists(component.getPrepend(), component.getUnique(), newComponent, value);
        }

        components.set(component.getSequence(), newComponent);
    }

    public List<String> getComponent(PLSQLBlockComponent component) {
        return components.get(component.getSequence());
    }

    public List<String> addComponent(PLSQLBlockComponent component, List<String> value) {
        LinkedList<String> target = components.get(component.getSequence());

        target = addLists(component.getPrepend(), component.getUnique(), target, value);

        components.set(component.getSequence(), target);

        return target;
    }

    public List<String> addComponent(PLSQLBlockComponent component, String[] value) {
        LinkedList<String> target = components.get(component.getSequence());

        target = addLists(component.getPrepend(), component.getUnique(), target, value);

        components.set(component.getSequence(), target);

        return target;
    }

    public List<String> addComponent(PLSQLBlockComponent component, String value) {
        LinkedList<String> target = components.get(component.getSequence());

        if (value != null) {
            if (target == null) {
                target = new LinkedList<String>();
                components.set(component.getSequence(), target);
            }

            if (!component.getUnique() || !target.contains(value)) {
                if (component.getPrepend()) {
                    target.addFirst(value);
                } else {
                    target.add(value);
                }
            }
        }

        return target;
    }

    public List<String> addComponent(PLSQLBlockComponent component, StringBuffer value) {
        return addComponent(component, (value != null) ? value.toString() : null);
    }

    public PLSQLBlockBuilder addBuilder(PLSQLBlockBuilder source) {
        for (PLSQLBlockComponent component : PLSQLBlockComponent.values()) {
            int sequence = component.getSequence();
            boolean prepend = component.getPrepend();
            boolean unique = component.getUnique();

            components.set(sequence,
                           addLists(prepend, unique, components.get(sequence), source.components.get(sequence)));
        }

        return this;
    }

    private static LinkedList<String> addLists(boolean prepend, boolean unique,
                                               LinkedList<String> target, String[] source) {
        LinkedList<String> ret = target;

        if (source != null) {
            int i = 0;
            for (String text : source) {
                if (!unique || !contains(ret, text)) {
                    if (prepend) {
                        ret = add(ret, i, text);
                        i++;
                    } else {
                        ret = add(ret, text);
                    }
                }
            }
        }

        return ret;
    }

    private static LinkedList<String> addLists(boolean prepend, boolean unique,
                                               LinkedList<String> target, List<String> source) {
        LinkedList<String> ret = target;

        if (source != null) {
            if (!unique) {
                if (prepend) {
                    ret = addAll(ret, 0, source);
                } else {
                    ret = addAll(ret, source);
                }
            } else {
                int i = 0;
                for (String text : source) {
                    if (!contains(ret, text)) {
                        if (prepend) {
                            ret = add(ret, i, text);
                            i++;
                        } else {
                            ret = add(ret, text);
                        }
                    }
                }
            }
        }

        return ret;
    }

    private static boolean contains(LinkedList<String> list, String value) {
        return (list != null && list.contains(value));
    }

    private static LinkedList<String> add(LinkedList<String> list, String value) {
        LinkedList<String> ret = (list == null) ? new LinkedList<String>() : list;
        ;

        ret.add(value);

        return ret;
    }

    private static LinkedList<String> add(LinkedList<String> list, int index, String value) {
        LinkedList<String> ret = (list == null) ? new LinkedList<String>() : list;
        ;

        ret.add(index, value);

        return ret;
    }

    private static LinkedList<String> addAll(LinkedList<String> list, Collection<String> value) {
        LinkedList<String> ret = (list == null) ? new LinkedList<String>() : list;
        ;

        ret.addAll(value);

        return ret;
    }

    private static LinkedList<String> addAll(LinkedList<String> list, int index,
                                             Collection<String> value) {
        LinkedList<String> ret = (list == null) ? new LinkedList<String>() : list;
        ;

        ret.addAll(index, value);

        return ret;
    }

    public String generateSql() {
        StringBuilder sb = new StringBuilder();

        // Get Components
        List<String> Prototype = getComponent(PLSQLBlockComponent.Prototype);
        List<String> dataDecls = getComponent(PLSQLBlockComponent.DataDecls);
        List<String> stdProcDecls = getComponent(PLSQLBlockComponent.StdProcDecls);
        List<String> procDecls = getComponent(PLSQLBlockComponent.ProcDecls);
        List<String> dataInitBlocks = getComponent(PLSQLBlockComponent.DataInitBlocks);
        List<String> preCallBlocks = getComponent(PLSQLBlockComponent.PreCallBlocks);
        List<String> preCallWrapper = getComponent(PLSQLBlockComponent.PreCallWrapper);
        List<String> call = getComponent(PLSQLBlockComponent.Call);
        List<String> paramBinding = getComponent(PLSQLBlockComponent.ParamBinding);
        List<String> postCallWrapper = getComponent(PLSQLBlockComponent.PostCallWrapper);
        List<String> postCallBlocks = getComponent(PLSQLBlockComponent.PostCallBlocks);
        List<String> otherBlocks = getComponent(PLSQLBlockComponent.OthersBlocks);

        int identLevel = 0;

        if (Prototype != null) {
            formatStatement(sb, identLevel++, Prototype);
            formatStatement(sb, identLevel, dataDecls);
            formatStatement(sb, identLevel, stdProcDecls);
            formatStatement(sb, identLevel--, procDecls);
        } else {
            if (dataDecls != null || stdProcDecls != null || procDecls != null) {
                formatStatement(sb, identLevel++, "DECLARE"); //$NON-NLS-1$
                formatStatement(sb, identLevel, dataDecls);
                formatStatement(sb, identLevel, stdProcDecls);
                formatStatement(sb, identLevel--, procDecls);
            }
        }

        formatStatement(sb, identLevel++, "BEGIN"); //$NON-NLS-1$

        formatStatement(sb, identLevel, dataInitBlocks);

        if (otherBlocks != null) {
            formatStatement(sb, identLevel++, "BEGIN"); //$NON-NLS-1$
        }

        formatStatement(sb, identLevel, preCallBlocks);

        if (preCallWrapper != null) {
            formatText(sb, identLevel, preCallWrapper);
        } else {
            formatText(sb, identLevel, ""); //$NON-NLS-1$
        }

        formatText(sb, 0, call);

        if (paramBinding != null) {
            formatText(sb, 0, "("); //$NON-NLS-1$
            formatParameters(sb, identLevel + 1, paramBinding);
            formatText(sb, 0, ")"); //$NON-NLS-1$
        }

        if (postCallWrapper != null) {
            formatText(sb, 0, postCallWrapper);
        }

        if (preCallWrapper != null || call != null || paramBinding != null ||
            postCallWrapper != null) {
            formatStatement(sb, 0, ";"); //$NON-NLS-1$
        }

        formatStatement(sb, identLevel, postCallBlocks);

        if (otherBlocks != null) {
            formatStatement(sb, --identLevel, "EXCEPTION"); //$NON-NLS-1$
            formatStatement(sb, ++identLevel, "WHEN OTHERS"); //$NON-NLS-1$
            formatStatement(sb, identLevel++, "THEN"); //$NON-NLS-1$
            formatStatement(sb, identLevel, otherBlocks);
            formatStatement(sb, --identLevel, "END;"); //$NON-NLS-1$
        }

        formatStatement(sb, --identLevel, "END;"); //$NON-NLS-1$

        return sb.toString();
    }

    protected StringBuilder formatStatement(StringBuilder sb, int identLevel,
                                            List<String> blocks) {
        StringBuilder retBuffer = sb;

        if (blocks != null) {
            for (String block : blocks) {
                retBuffer = formatStatement(retBuffer, identLevel, block);
            }
        }

        return retBuffer;
    }

    private StringBuilder formatStatement(StringBuilder sb, int identLevel, String block) {
        if (block != null) {
            formatText(sb, identLevel, block);
            sb.append('\n');
        }

        return sb;
    }

    protected StringBuilder formatText(StringBuilder sb, int identLevel, List<String> blocks) {
        StringBuilder retBuffer = sb;

        if (blocks != null) {
            for (String block : blocks) {
                retBuffer = formatText(retBuffer, identLevel, block);
            }
        }

        return retBuffer;
    }

    protected StringBuilder formatText(StringBuilder sb, int identLevel, String block) {
        if (block != null) {
            formatIdent(sb, identLevel);
            sb.append(block);
        }

        return sb;
    }

    protected StringBuilder formatParameters(StringBuilder sb, int identLevel,
                                             List<String> parameters) {
        if (parameters != null) {
            int i = 0;
            for (String parameter : parameters) {
                if (parameter != null) {
                    if (i > 0) {
                        sb.append(","); //$NON-NLS-1$

                        if (identLevel > 0) {
                            sb.append('\n');
                            formatIdent(sb, identLevel);
                        } else {
                            sb.append(' ');
                        }
                    }

                    sb.append(parameter);
                    i++;
                }
            }
        }

        return sb;
    }

    protected StringBuilder formatIdent(StringBuilder sb, int identLevel) {
        for (int i = 0; i < identLevel; i++) {
            sb.append(IDENT);
        }

        return sb;
    }
}
