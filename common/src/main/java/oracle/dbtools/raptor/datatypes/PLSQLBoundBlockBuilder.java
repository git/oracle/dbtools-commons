/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=PLSQLBoundBlockBuilder.java"
 *         >Barry McGillin</a>
 * 
 */
public class PLSQLBoundBlockBuilder extends PLSQLBlockBuilder {
	private final PLSQLBlockBuilder bindBuilder;

	public PLSQLBoundBlockBuilder() {
		super();

		bindBuilder = new PLSQLBlockBuilder();
	}

	public PLSQLBlockBuilder getBindBuilder() {
		return bindBuilder;
	}

	/**
	 * 
	 * addComponent
	 * 
	 * @param component
	 * @param value
	 * @param binds
	 * @return PLSQLBoundBlockBuilder
	 */
	public List<String> addComponent(PLSQLBlockComponent component, String value, String... binds) {
		List<String> target = components.get(component.getSequence());

		if (value != null) {
			if (binds != null) {
				String newValue = String.format(value, (Object[]) binds);
				target = addComponent(component, newValue);

			} else {
				target = addComponent(component, value);
			}
		}

		// Add Binds
		bindBuilder.addComponent(component, binds);

		return target;
	}

	public List<String> addComponent(PLSQLBlockComponent component, StringBuffer value, String... binds) {
		List<String> target = components.get(component.getSequence());

		if (value != null) {
			if (binds != null) {
				String newValue = String.format(value.toString(), (Object[]) binds);
				target = addComponent(component, newValue);
			} else {
				target = addComponent(component, value);
			}
		}

		// Add Binds
		bindBuilder.addComponent(component, binds);

		return target;
	}

	public PLSQLBoundBlockBuilder addBuilder(PLSQLBoundBlockBuilder source) {
		super.addBuilder(source);
		bindBuilder.addBuilder(source.bindBuilder);

		return this;
	}

	public List<String> getBindTokenList() {
		List<String> bindTokens = new LinkedList<String>();

		// Concat tokens
		for (PLSQLBlockComponent component : PLSQLBlockComponent.values()) {
			List<String> componentValue = getBindBuilder().getComponent(component);

			if (componentValue != null) {
				bindTokens.addAll(componentValue);
			}
		}

		return (bindTokens.isEmpty()) ? null : bindTokens;
	}
}
