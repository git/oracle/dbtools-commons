/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=TypeMetadata.java">Barry McGillin</a> 
 *
 */
public class TypeMetadata {
    private final Map<Attribute, Object> attributes;
    private final boolean syntaxSynonym;
    private Integer hashCode;

    public enum Attribute {
        DATA_TYPE,
        DATA_LENGTH,
        DATA_PRECISION,
        DATA_SCALE,
        DATA_RADIX,
        PLS_TYPE,
        TYPE_OWNER,
        TYPE_NAME,
        TYPE_SUBNAME,
        TYPE_LINK,
        CHAR_LENGTH,
        CHAR_USED,
        CHAR_SET_NAME,
        TYPE_COMPONENTS,
        TYPE_CODE,
        BASE_TYPE,
        IMPL_CLASS,
        IMPL_DATA_TYPE;
    }
    
    public final static String CHAR_FLAG = "C";
    public final static String BYTE_FLAG = "B";
    public final static String NULL_FLAG = "0";

    public List<NamedValue<TypeMetadata>> get_type_components() {
        return get_type_components((List<NamedValue<TypeMetadata>>)attributes.get(Attribute.TYPE_COMPONENTS));
    }

    static List<NamedValue<TypeMetadata>> get_type_components(List<NamedValue<TypeMetadata>> components) {
        if (components != null) {
            return new ArrayList<NamedValue<TypeMetadata>>(components);
        } else {
            return components;
        }
    }

    public String get_type_subname() {
        return (String)attributes.get(Attribute.TYPE_SUBNAME);
    }

    public String get_type_owner() {
        return (String)attributes.get(Attribute.TYPE_OWNER);
    }

    public String get_type_link() {
        return (String)attributes.get(Attribute.TYPE_LINK);
    }

    public Integer get_data_length() {
        return (Integer)attributes.get(Attribute.DATA_LENGTH);
    }

    public String get_type_name() {
        return (String)attributes.get(Attribute.TYPE_NAME);
    }

    public Integer get_data_scale() {
        return (Integer)attributes.get(Attribute.DATA_SCALE);
    }

    public Integer get_radix() {
        return (Integer)attributes.get(Attribute.DATA_RADIX);
    }

    public Integer get_data_precision() {
        return (Integer)attributes.get(Attribute.DATA_PRECISION);
    }

    public String get_data_type() {
        return (String)attributes.get(Attribute.DATA_TYPE);
    }

    public String get_pls_type() {
        return (String)attributes.get(Attribute.PLS_TYPE);
    }

    public String get_char_used() {
        return (String)attributes.get(Attribute.CHAR_USED);
    }

    public Integer get_char_length() {
        return (Integer)attributes.get(Attribute.CHAR_LENGTH);
    }

    public String get_char_set_name() {
        return (String)attributes.get(Attribute.CHAR_SET_NAME);
    }

    public Integer get_type_code() {
        return (Integer)attributes.get(Attribute.TYPE_CODE);
    }

    public String get_base_type() {
        return (String)attributes.get(Attribute.BASE_TYPE);
    }

    public Class<? extends DataType> get_impl_class() throws ClassNotFoundException {
        Object clazz = attributes.get(Attribute.IMPL_CLASS);
        
        if (clazz instanceof String) {
            String className = get_impl_class_name();
            Class<? extends DataType> foundClass = (Class<? extends DataType>)Class.forName(className);
            attributes.put(Attribute.IMPL_CLASS, foundClass);
            return foundClass;
        } else {
            return (Class<? extends DataType>)clazz;
        }
    }

    public String get_impl_class_name(boolean expand) {
        Object clazz = attributes.get(Attribute.IMPL_CLASS);
        String className;
        
        if (clazz == null) {
            className = null;
        } else if (clazz instanceof String) {
            className = (String)clazz;
            if (expand && className.startsWith("o.d.r.")) {
                className = "oracle.dbtools.raptor." + className.substring(6);
            }
        } else {
            className = ((Class<? extends DataType>)clazz).getName();
        }
        
        return className;
    }

    public String get_impl_class_name() {
        return get_impl_class_name(true);
    }

    public String get_impl_data_type() {
        return (String)attributes.get(Attribute.IMPL_DATA_TYPE);
    }

    public Object getAttribute(Attribute attribute) {
        return attributes.get(attribute);
    }

    public boolean isIncomplete() {
        return attributes.containsValue(null);
    }

    public boolean isSyntaxSynonym() {
        return syntaxSynonym;
    }

    TypeMetadata(Map<Attribute, Object> attributeMap) {
        this(attributeMap, false);
    }

    TypeMetadata(Map<Attribute, Object> attributeMap, boolean syntaxSynonym) {
        this.syntaxSynonym = syntaxSynonym;
        this.hashCode = null;
        this.attributes = new HashMap<Attribute, Object>();
        if (attributeMap != null) {
            this.attributes.putAll(attributeMap);
        }
    }

    TypeMetadata(final Integer typeCode, final String dataType, final Integer precision, final Integer scale) {
        this(new HashMap<TypeMetadata.Attribute, Object>(){
            {
                if (typeCode != null)  put(TypeMetadata.Attribute.TYPE_CODE, typeCode);
                if (dataType != null)  put(TypeMetadata.Attribute.DATA_TYPE, dataType);
                if (precision != null) put(TypeMetadata.Attribute.DATA_PRECISION, precision);
                if (scale != null)     put(TypeMetadata.Attribute.DATA_SCALE, scale);
            }
        });
    }

    static TypeMetadata overideTypeMetadata(TypeMetadata source, Map<Attribute, Object> attributeMap, boolean fromTypeMetadata) {
        if (source != null && (attributeMap == null || containsAll(source.attributes, attributeMap))) {
            return source;
        } else {
            Map<Attribute, Object> attributes = new HashMap<Attribute, Object>();
            // Accept all source attributes
            if (source != null) {
                attributes.putAll(source.attributes);
            }
            
            // Remove IMPL_DATA_TYPE if not with IMPL_CLASS
            if (!attributes.containsKey(Attribute.IMPL_CLASS)) {
                attributes.remove(Attribute.IMPL_DATA_TYPE);
            }
            
            // Prepare for incoming datatype - all overrides techically create a new datatype
            // so Attribute.DATA_TYPE should be set.
            if (attributes.containsKey(Attribute.DATA_TYPE)) {
                String datatype = (String)attributes.get(Attribute.DATA_TYPE);
                String basetype = (String)attributes.get(Attribute.BASE_TYPE);
                if (basetype == null && datatype != null) {
                    attributes.put(Attribute.BASE_TYPE, datatype);
                }
                attributes.remove(Attribute.DATA_TYPE);
            }
            
            // Override attributes
            if (attributeMap != null) {
                attributes.putAll(attributeMap);
            }
            
            if (!fromTypeMetadata && attributeMap.containsKey(Attribute.TYPE_COMPONENTS)) {
                attributes.put(Attribute.TYPE_COMPONENTS, 
                               get_type_components((List<NamedValue<TypeMetadata>>)attributeMap.get(Attribute.TYPE_COMPONENTS)));
            }
            
            // Ensure IMPL_DATATYPE set for IMPL_CLASS
            if (attributes.containsKey(Attribute.IMPL_CLASS) && 
                attributes.containsKey(Attribute.DATA_TYPE) && 
                !attributes.containsKey(Attribute.IMPL_DATA_TYPE)) {
                attributes.put(Attribute.IMPL_DATA_TYPE, attributes.get(Attribute.DATA_TYPE));
            }
            
            return new TypeMetadata(attributes);
        }
    }

    static TypeMetadata overideTypeMetadata(TypeMetadata source, Map<Attribute, Object> attributeMap) {
        return overideTypeMetadata(source, attributeMap, false);
    }

    static TypeMetadata overideTypeMetadata(TypeMetadata source, TypeMetadata source2) {
        if (source2 == source || (source2 != null && (source == null || containsAll(source2.attributes, source.attributes)))) {
            return source2;
        } else {
            return overideTypeMetadata(source, source2.attributes, true);
        }
    }

    public void export(Map<Attribute, Object> attributeMap) {
        attributeMap.putAll(attributes);
    }

    public boolean equals(Object obj) {
        boolean isEqual = (this == obj);

        if (!isEqual && obj instanceof TypeMetadata) {
            TypeMetadata objTypeMetadata = (TypeMetadata)obj;
            isEqual = areEquals(attributes, objTypeMetadata.attributes);
        }

        return isEqual;
    }

    private static boolean containsAll(Map<Attribute, Object> source, Map<Attribute, Object> attributeMap) {
        for (Map.Entry<Attribute, Object> entry : attributeMap.entrySet()) {
            if (!areEquals(entry.getValue(), source.get(entry.getKey()))) {
                return false;
            }
        }
        
        return true;
    }
    private static boolean areEquals(Object obj1, Object obj2) {
        return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
    }
    
    public int hashCode()
    {
        if (hashCode == null) {
            int hash = 7;
            hash = 31 * hash + (null == attributes ? 0 : attributes.hashCode());
            hashCode = hash;
        }
        
        return hashCode;
    }
    
    
}