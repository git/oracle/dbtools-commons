/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.blob;

/**
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=BlobMimeType.java"
 *         >Barry McGillin</a>
 * 
 */
public class BlobMimeType {

	public String getMagicNumber() {
		return m_magicNumber;
	}

	public void setMagicNumber(String magicNumber) {
		m_magicNumber = magicNumber;
	}

	public int getMagicNumberLength() {
		return m_magicNumberLength;
	}

	public void setMagicNumberLength(int magicNumberLength) {
		m_magicNumberLength = magicNumberLength;
	}

	public String getMimeType() {
		return m_mimeType;
	}

	public void setMimeType(String mimeType) {
		m_mimeType = mimeType;
	}

	public String getApplicationType() {
		return m_applicationType;
	}

	public void setApplicationType(String applicationType) {
		m_applicationType = applicationType;
	}

	public int getOffsetNumberLength() {
		return m_offsetNumberLength;
	}

	public void setOffsetNumberLength(int offsetNumberLength) {
		m_offsetNumberLength = offsetNumberLength;
	}

	public String getOffsetNumber() {
		return m_offsetNumber;
	}

	public void setOffsetNumber(String offsetNumber) {
		m_offsetNumber = offsetNumber;
	}

	public String getExtension() {
		return m_extension;
	}

	public void setExtension(String extension) {
		m_extension = extension;
	}

	public int getOffset() {
		return m_offset;
	}

	public void setOffset(int offset) {
		m_offset = offset;
	}

	public void setDescription(String description) {
		m_description = description;
	}

	public String getDescription() {
		return m_description;
	}

	private int m_magicNumberLength = 0;
	private int m_offset = 0;
	private String m_magicNumber = null;
	private String m_mimeType = null;
	private String m_applicationType = null;
	private int m_offsetNumberLength = 0;
	private String m_offsetNumber = null;
	private String m_extension = null;
	private String m_description = null;

	public BlobMimeType() {

	}

	public BlobMimeType(int magicNumberLength, String magicNumber) {
		this.m_magicNumberLength = magicNumberLength;
		this.m_magicNumber = magicNumber;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Magic Number and Mime Types - ");
		sb.append("MagicNumberLength:" + getMagicNumberLength());
		sb.append(", ");
		sb.append("MagicNumber:" + getMagicNumber());
		sb.append(", ");
		sb.append("Mime Type:" + getMimeType());
		sb.append(", ");
		sb.append("Extension:" + getExtension());
		sb.append(", ");
		sb.append("Application Type:" + getApplicationType());
		sb.append(", ");
		sb.append("Description:" + getDescription());
		sb.append(".");

		return sb.toString();
	}
}
