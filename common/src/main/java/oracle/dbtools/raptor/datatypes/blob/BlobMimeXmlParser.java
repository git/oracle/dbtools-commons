/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.blob;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import oracle.dbtools.raptor.utils.ExceptionHandler;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class BlobMimeXmlParser extends DefaultHandler implements IBlobMimeXmlParser {

	public BlobMimeXmlParser() {
	}
	
    public BlobMimeXmlParser(InputStream inputstream) throws IOException {
        initializeBlobParser(inputstream);
    }
    
	public void initializeBlobParser(InputStream inputstream) throws IOException {
        mimeType = new ArrayList<BlobMimeType>();
	    m_isMimeTypeSupported = getHexStream(inputstream);
	    
	    if (m_isMimeTypeSupported) {
	        parseBlobXML(getHexMimeString());
	        // System.out.println(" Hex String -> " + getHexMimeString());
	        getMimeAndApplicationType();
	    }
	}
	
	private boolean parseBlobXML(String magicNumber) {
		setMagicNumber(magicNumber);
		// get a factory
		SAXParserFactory spf = SAXParserFactory.newInstance();
		try {
			// get a new instance of parser
			SAXParser sp = spf.newSAXParser();
			// parse the file and also register this class for call backs
			sp.parse(getMimeMagicNumberXmlFile().openStream(), this);
		} catch (SAXException se) {
			Logger.getLogger(getClass().getName()).log(Level.WARNING, se.getStackTrace()[ 0 ].toString(), se);
			ExceptionHandler.handleException(se);
		} catch (ParserConfigurationException pce) {
			Logger.getLogger(getClass().getName()).log(Level.WARNING, pce.getStackTrace()[ 0 ].toString(), pce);
			ExceptionHandler.handleException(pce);
		} catch (IOException ie) {
			Logger.getLogger(getClass().getName()).log(Level.WARNING, ie.getStackTrace()[ 0 ].toString(), ie);
			ExceptionHandler.handleException(ie);
		}
		return true;
	}

	/**
	 * Iterate through the list and print the contents
	 */
	private boolean getMimeAndApplicationType() {
		// System.out.println("No of Records '" + mimeType.size() + "'.");
		Iterator<BlobMimeType> it = mimeType.iterator();
		while (it.hasNext()) {
			BlobMimeType tempBlobMimeType = it.next();
			//System.out.println("BlobMimeXmlParser,  toString:"+tempBlobMimeType.toString());
			if (compareMagicNumber(tempBlobMimeType.getMagicNumber(), tempBlobMimeType
					.getMagicNumberLength())) {

				if (tempBlobMimeType.getOffset() != 0) {
					if (compareOffsetNumber(tempBlobMimeType.getOffset(), tempBlobMimeType
							.getOffsetNumber(), tempBlobMimeType.getOffsetNumberLength())) {

						setMimeType(tempBlobMimeType.getMimeType());
						setApplicationType(tempBlobMimeType.getApplicationType());
						setExtension(tempBlobMimeType.getExtension());
                        setDescription(tempBlobMimeType.getDescription());
						break;
					}
				} else {
					setMimeType(tempBlobMimeType.getMimeType());
					setApplicationType(tempBlobMimeType.getApplicationType());
					setExtension(tempBlobMimeType.getExtension());
                    setDescription(tempBlobMimeType.getDescription());
					break;
				}
			}

		}
		return false;
	}

	// Event Handlers
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
		tempVal = "";
		if (qName.equalsIgnoreCase("match")) {
			tempBlobMime = new BlobMimeType();
		} else if (qName.equalsIgnoreCase("MagicNumber") && attributes.getLength() > 0) {
			tempBlobMime.setOffset(Integer.parseInt(attributes.getValue("offset")));
			tempBlobMime.setMagicNumberLength(Integer.parseInt(attributes.getValue("length")));

		} else if (qName.equalsIgnoreCase("OffsetNumber") && attributes.getLength() > 0) {
			tempBlobMime.setOffsetNumberLength(Integer.parseInt(attributes.getValue("length")));
			tempBlobMime.setOffsetNumber(attributes.getValue("OffsetNumber"));
		}
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		tempVal = new String(ch, start, length);
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {

		if (qName.equalsIgnoreCase("match")) {
			mimeType.add(tempBlobMime);
		} else if (qName.equalsIgnoreCase("datatype")) {
		} else if (qName.equalsIgnoreCase("offset")) {
			tempBlobMime.setOffset(Integer.parseInt(tempVal));
		} else if (qName.equalsIgnoreCase("OffsetNumber")) {
			tempBlobMime.setOffsetNumber(tempVal);
		} else if (qName.equalsIgnoreCase("length")) {
			tempBlobMime.setMagicNumberLength(Integer.parseInt(tempVal));

		} else if (qName.equalsIgnoreCase("MagicNumber")) {
			tempBlobMime.setMagicNumber(tempVal);

		} else if (qName.equalsIgnoreCase("Mimetype")) {
			tempBlobMime.setMimeType(tempVal);
			
		} else if (qName.equalsIgnoreCase("Extension")) {
			tempBlobMime.setExtension(tempVal);			
			
		} else if (qName.equalsIgnoreCase("Application")) {
			tempBlobMime.setApplicationType(tempVal);
			
		} else if (qName.equalsIgnoreCase("Description")) {
		    tempBlobMime.setDescription(tempVal);
		}
	}

	private boolean compareMagicNumber(String xmlMagicNumber, int length) {
		String hexCodeNumber = getMagicNumber();
		String compHexCode = hexCodeNumber.substring(0, length);
//		System.out.println("in compareMagicNumber :START ");
//		System.out.println("in compareMagicNumber :hexCodeNumber-> " + hexCodeNumber);
//		System.out.println("in compareMagicNumber :xmlMagicNumber-> " + xmlMagicNumber);
//		System.out.println("in compareMagicNumber :compHexCode-> " + compHexCode);
//		System.out.println("in compareMagicNumber :length-> " + length);
//		System.out.println("in compareMagicNumber :END ");
		if ((hexCodeNumber.substring(0, length)).equals(xmlMagicNumber)) {
			//System.out.println("MATCH found !!!");
			return true;
		}
		return false;
	}

	private boolean compareOffsetNumber(int bytesOffset, String xmlOffsetNumber, int length) {
		String hexCodeNumber = getMagicNumber();
		String compHexCode = hexCodeNumber.substring((bytesOffset * 2), (bytesOffset * 2) + length);
//		System.out.println("in compareOffsetNumber :START ");
//		System.out.println("in compareOffsetNumber :hexCodeNumber-> " + hexCodeNumber);
//		System.out.println("in compareOffsetNumber :bytesOffset-> " + bytesOffset);
//		System.out.println("in compareOffsetNumber :xmlMagicNumber-> " + xmlOffsetNumber);
//		System.out.println("in compareOffsetNumber :compHexCode-> " + compHexCode);
//		System.out.println("in compareOffsetNumber :length-> " + length);
		
		//System.out.println("in compareOffsetNumber :END ");
		if ((hexCodeNumber.substring((bytesOffset * 2), (bytesOffset * 2) + length))
				.equals(xmlOffsetNumber)) {
			// System.out.println(" OFFSET MATCH found !!!");
			return true;
		}
		return false;
	}

	public String getMagicNumber() {
		return m_magicNumber;
	}

	private void setMagicNumber(String magicNumber) {
		m_magicNumber = magicNumber;
	}

	public String getExtension() {
		return m_extension;
	}

	private void setExtension(String extension) {
		m_extension = extension;
	}
	
	public String getMimeType() {
		return m_mimeType;
	}

	private void setMimeType(String mimeType) {
		m_mimeType = mimeType;
	}

	public String getApplicationType() {
		return m_applicationType;
	}

	private void setApplicationType(String applicationType) {
		m_applicationType = applicationType;
	}

    private void setDescription(String description) {
        m_description = description;
    }

    public String getDescription() {
        return m_description;
    }
    
	public boolean isMimeTypeSupported() {
	    return m_isMimeTypeSupported;
	}

	private URL getMimeMagicNumberXmlFile() {
		String path = "/oracle/dbtools/raptor/datatypes/blob/MimeMagicNumber.xml"; //$NON-NLS-1$
        return getClass().getResource( path );
    }
	
    //TODO for small byte arrays
    private boolean getHexStream(InputStream instream) throws IOException  {
      
        try { 
               byte[] myByteArray = new byte[NO_OF_BYTES];
               instream.mark(NO_OF_BYTES+1);
               instream.read(myByteArray);
               setHexMimeString(getHexString(myByteArray));
               // System.out.println("Hex String -> " + getHexMimeString());
               instream.reset();  
               return true;
        } catch (IOException e) {
//          Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
//          ExceptionHandler.handleException(e);
            return false;
        }
    }
	
  //TODO for small byte arrays
    public String getOleHexStream(InputStream instream) throws IOException  {
        try { 
               byte[] myByteArray = new byte[NO_OF_OLE_BYTES];
               instream.mark(NO_OF_OLE_BYTES+1);
               instream.read(myByteArray);
               instream.reset(); 
               return (getHexString(myByteArray));
        } catch (IOException e) {
//          Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
//          ExceptionHandler.handleException(e);
            return null;
        }
    }
   
    public String getHexString(byte[] raw) throws UnsupportedEncodingException {
        byte[] hex = new byte[2 * raw.length];
        int index = 0;

        for (byte b : raw) {
            int v = b & 0xFF;
            hex[index++] = HEX_CHAR_TABLE[v >> 4];
            hex[index++] = HEX_CHAR_TABLE[v & 0xF];
        }
        return new String(hex, "ASCII");
    }
	
    public void setHexMimeString(String hexString) {
        m_hexMimeString = hexString ;
    }
    
    public String getHexMimeString() {
        return m_hexMimeString ;
    }
    
    private static final byte[] HEX_CHAR_TABLE = { 
        (byte) '0', (byte) '1', (byte) '2', (byte) '3', (byte) '4', 
        (byte) '5', (byte) '6', (byte) '7', (byte) '8', (byte) '9', 
        (byte) 'A', (byte) 'B', (byte) 'C', (byte) 'D', (byte) 'E', (byte) 'F' };

    private static final int NO_OF_BYTES = 520;
    
    private String m_hexMimeString = null;
    
	ArrayList<BlobMimeType> mimeType;
	private String tempVal;
	private BlobMimeType tempBlobMime;
	private String m_magicNumber = null;
	private String m_mimeType = null;
	private String m_applicationType = null;
	private String m_extension = null;	
    private String m_description = null;
    
    private static final int NO_OF_OLE_BYTES = 20400;
    private boolean m_isMimeTypeSupported = false;    
}
