/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.extensions;

import java.net.URI;

import java.net.URISyntaxException;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider.ConnectionProviderAccess;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionReference;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionWrapper;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtensionManager.ExtensionManagerAccess;
import oracle.dbtools.raptor.datatypes.DataTypeProvider;
import oracle.dbtools.raptor.datatypes.DataTypeProvider.DataTypeProviderAccess;
import oracle.dbtools.raptor.datatypes.impl.AbstractDataTypeFactoryExtensionImpl;
import oracle.dbtools.raptor.datatypes.impl.DataTypeConnectionProviderImpl;
import oracle.dbtools.raptor.datatypes.impl.DataTypeConnectionReferenceImpl;

import oracle.jdbc.OracleConnection;

public class OracleSQLDataTypeFactoryExtension extends AbstractDataTypeFactoryExtensionImpl {
    public static Builder builder(URI extensionURI) {
        return new Builder(extensionURI);
    }
    
    public static class Builder extends AbstractDataTypeFactoryExtensionImpl.Builder {
        public static final URI ID;

        static {
            URI tmpURI = null;
            
            // Initialize SQL Extension URI
            try {
                tmpURI = new URI("http://www.oracle.com/raptor/datatypes/extensions/sql/");
            } catch(URISyntaxException e) {
                ;
            }
            
            ID = tmpURI;
        }

        protected Builder(URI extensionURI) {
            super(extensionURI);
        }

        @Override
        public ExtensionAccess build() {
            OracleSQLDataTypeFactoryExtension extension =
                new OracleSQLDataTypeFactoryExtension(getExtensionManagerAccess(),
                                                      getExtensionManagerAccess().lookupExtension(getSuperExensionURI()), 
                                                      getDataTypeProvider());
            
            return extension.getExtensionAccess();
        }

        @Override
        public Builder setDataTypeProvider(DataTypeProvider.Builder dataTypeProvider) {
            super.setDataTypeProvider(dataTypeProvider);
            return this;
        }

        @Override
        public Builder setSuperExtensionURI(URI superExtensionURI) {
            super.setSuperExtensionURI(superExtensionURI);
            return this;
        }

        @Override
        public Builder setExtensionManagerAccess(ExtensionManagerAccess extensionManagerAccess) {
            super.setExtensionManagerAccess(extensionManagerAccess);
            return this;
        }
    }

    private final ExtensionAccess extensionAccess;
    private final DataTypeProviderAccess dataTypeProviderAccess;
    
    private OracleSQLDataTypeFactoryExtension(ExtensionManagerAccess extensionManagerAccess,
                                              ExtensionAccess superExtensionAccess,
                                              DataTypeProvider.Builder dataTypeProvider) {
        super(extensionManagerAccess, superExtensionAccess);
        this.extensionAccess = new DefaultExtensionAccess();
        this.dataTypeProviderAccess = (dataTypeProvider != null) 
            ? dataTypeProvider.setExtensionAccess(getExtensionAccess()).build()
            : null;
    }

    @Override
    protected ExtensionAccess getExtensionAccess() {
        return extensionAccess;
    }
    
    @Override
    public DataTypeProvider getDataTypeProvider() {
        return (dataTypeProviderAccess != null) ? dataTypeProviderAccess.getDataTypeProvider() : null;
    }

    @Override
    public Extendable.Builder builder(Class<?> clazz, Object... obj) {
        Extendable.Builder builder = null;
        
        if (clazz == ConnectionProviderAccess.class) {
            PARSE_PROVIDER:
            if (obj != null) {
                DataTypeConnectionProviderImpl.Builder candidate = null;
                if (obj.length >= 1) {
                    if (OracleConnection.class.isAssignableFrom(obj[0].getClass())) {
                        candidate = DataTypeConnectionProviderImpl.builder(OracleConnection.class)
                                    .setDataTypeConnectionReference(new DataTypeConnectionReferenceImpl(OracleConnection.class, (OracleConnection)obj[0]));
                    } else if (DataTypeConnectionReference.class.isAssignableFrom(obj[0].getClass())) {
                        DataTypeConnectionReference connectionReference = (DataTypeConnectionReference)obj[0];
                        if (OracleConnection.class.isAssignableFrom(connectionReference.getConnectionClass())) {
                            candidate = DataTypeConnectionProviderImpl.builder(OracleConnection.class)
                                        .setDataTypeConnectionReference(connectionReference);
                        } else {
                            break PARSE_PROVIDER;
                        }
                    } else {
                        break PARSE_PROVIDER;
                    }
                } else {
                    break PARSE_PROVIDER;
                }
                if (obj.length == 1) {
                    ;
                } else if (obj.length == 2) {
                    if (obj[1] != null) {
                        if (DataTypeConnectionWrapper.class.isAssignableFrom(obj[1].getClass())) {
                            DataTypeConnectionWrapper nlsWrapper = (DataTypeConnectionWrapper)obj[1];
                            if (OracleConnection.class.isAssignableFrom(nlsWrapper.getConnectionClass())) {
                                candidate.setNLSConnectionWrapper(nlsWrapper);
                            } else {
                                break PARSE_PROVIDER;  
                            }
                        } else {
                            break PARSE_PROVIDER;
                        }
                    }
                } else {
                    break PARSE_PROVIDER;
                }
                
                builder = candidate;
            }
        } else {
            builder = super.builder(clazz, obj);
        }
        
        if (builder != null) {
            builder = builder.setExtensionAccess(getExtensionAccess());
        }
        
        return builder;
    }

    @Override
    public boolean handlesExplicitly(Class<?> clazz) {
        return clazz != null && OracleConnection.class.isAssignableFrom(clazz);
    }
}
