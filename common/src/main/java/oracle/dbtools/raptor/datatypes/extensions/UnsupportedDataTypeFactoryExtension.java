/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.extensions;

import java.io.IOException;

import java.net.URI;

import java.net.URISyntaxException;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtensionManager.ExtensionManagerAccess;
import oracle.dbtools.raptor.datatypes.DataTypeProvider;
import oracle.dbtools.raptor.datatypes.DataTypeProvider.DataTypeProviderAccess;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.impl.AbstractDataTypeFactoryExtensionImpl;
import oracle.dbtools.raptor.datatypes.impl.AbstractDataTypeProviderImpl;

public class UnsupportedDataTypeFactoryExtension extends AbstractDataTypeFactoryExtensionImpl {
    public static Builder builder(URI extensionURI) {
        return new Builder(extensionURI);
    }

    public static class Builder extends AbstractDataTypeFactoryExtensionImpl.Builder {
        public static final URI ID;

        static {
            URI tmpURI = null;
            
            // Initialize Unsupported Extension URI
            try {
                tmpURI = new URI("http://www.oracle.com/raptor/datatypes/extensions/unsupported/");
            } catch(URISyntaxException e) {
                ;
            }
            
            ID = tmpURI;
        }

        protected Builder(URI extensionURI) {
            super(extensionURI);
        }

        @Override
        public ExtensionAccess build() {
            UnsupportedDataTypeFactoryExtension extension =
                new UnsupportedDataTypeFactoryExtension(getExtensionManagerAccess(),
                                                        getExtensionManagerAccess().lookupExtension(getSuperExensionURI()),
                                                        getDataTypeProvider());
            return extension.getExtensionAccess();
        }

        @Override
        public Builder setDataTypeProvider(DataTypeProvider.Builder dataTypeProvider) {
            super.setDataTypeProvider(dataTypeProvider);
            return this;
        }

        @Override
        public Builder setSuperExtensionURI(URI superExtensionURI) {
            super.setSuperExtensionURI(superExtensionURI);
            return this;
        }

        @Override
        public Builder setExtensionManagerAccess(ExtensionManagerAccess extensionManagerAccess) {
            super.setExtensionManagerAccess(extensionManagerAccess);
            return this;
        }
    }

    public static class UnsupportedDataProvider extends AbstractDataTypeProviderImpl {
        private final UnsupportedDataTypeProviderAccess providerAccess;

        public class UnsupportedDataTypeProviderAccess implements DataTypeProviderAccess {
            protected UnsupportedDataTypeProviderAccess() {};
            
            public DataTypeProvider getDataTypeProvider() {
                return UnsupportedDataProvider.this;
            }
            
            public ExtensionAccess getExtensionAccess() {
                return UnsupportedDataProvider.this.getExtensionAccess();
            }
            
            public DataTypeFactory getDataTypeFactory() {
                return getExtensionAccess().getDataTypeFactory();
            }
        }

        @Override
        protected UnsupportedDataTypeProviderAccess getDataTypeProviderAccess() {
            return providerAccess;
        }

        public static Builder builder(URI dataTypeProviderURI) {
            return new Builder(dataTypeProviderURI);
        }
        
        public final static class Builder extends AbstractDataTypeProviderImpl.Builder {
            public static final URI ID;

            static {
                URI tmpURI = null;
                
                // Initialize Unsupported DataTypeProvider URI
                try {
                    tmpURI = new URI("http://www.oracle.com/raptor/datatypes/providers/unsupported/");
                } catch(URISyntaxException e) {
                    ;
                }
                
                ID = tmpURI;
            }

            private Builder(URI dataTypeProviderURI) {
                super(dataTypeProviderURI);
            }

            public UnsupportedDataTypeProviderAccess build() {
                UnsupportedDataProvider provider = 
                    new UnsupportedDataProvider(getExtensionAccess(),
                                                getDataTypeProviderURI(),
                                                getExtensionAccess().resolveDataTypeProvider(getSuperDataTypeProviderURI()),
                                                typeMetadataProps);
                
                return provider.getDataTypeProviderAccess();
            }

            @Override
            public Builder setExtensionAccess(ExtensionAccess extensionAccess) {
                super.setExtensionAccess(extensionAccess);
                return this;
            }

            @Override
            public Builder setSuperDataTypeProviderURI(URI superDataTypeProvider) {
                super.setSuperDataTypeProviderURI(superDataTypeProvider);
                return this;
            }
        }

        private static final Map<String, NamedValue<Map<TypeMetadata.Attribute, Object>>> typeMetadataProps =
            new HashMap<String, NamedValue<Map<TypeMetadata.Attribute, Object>>>();
            
        private UnsupportedDataProvider(ExtensionAccess extensionAccess,
                                        URI dataTypeProviderURI,
                                        DataTypeProvider next,
                                        Map<String, NamedValue<Map<TypeMetadata.Attribute, Object>>> typeMetadataProps) {
            super(extensionAccess, dataTypeProviderURI, next, typeMetadataProps);
            
            this.providerAccess = new UnsupportedDataTypeProviderAccess();
        }

        @Override
        public void close() throws IOException {
            ; // Move along - nothing to see here
        }
    }

    private final ExtensionAccess extensionAccess;
    private final DataTypeProviderAccess dataTypeProviderAccess;

    protected UnsupportedDataTypeFactoryExtension(ExtensionManagerAccess extensionManagerAccess,
                                                  ExtensionAccess superExtensionAccess,
                                                  DataTypeProvider.Builder dataTypeProvider) {
        super(extensionManagerAccess, superExtensionAccess);
        this.extensionAccess = new AbstractDataTypeFactoryExtensionImpl.DefaultExtensionAccess();
        this.dataTypeProviderAccess =
            (dataTypeProvider != null) ? dataTypeProvider.setExtensionAccess(getExtensionAccess()).build() : null;
    }

    @Override
    protected ExtensionAccess getExtensionAccess() {
        return extensionAccess;
    }

    @Override
    public DataTypeProvider getDataTypeProvider() {
        return (dataTypeProviderAccess != null) ? dataTypeProviderAccess.getDataTypeProvider() : null;
    }

    @Override
    public Extendable.Builder builder(Class<?> clazz, Object... obj) {
        Extendable.Builder builder = super.builder(clazz, obj);

        if (builder != null) {
            builder = builder.setExtensionAccess(getExtensionAccess());
            if (builder instanceof DataTypeConnectionProvider.Builder) {
                ((DataTypeConnectionProvider.Builder)builder).setSupported(false);
            }
        }

        return builder;
    }

    @Override
    public boolean handlesExplicitly(Class<?> clazz) {
        return clazz != null && Connection.class.isAssignableFrom(clazz);
    }

    /*@Override
        public <H> H handle(Class<H> clazz, Object obj) {
            if (clazz == DataTypeProvider.class) {
                return clazz.cast(unsupportedDataProvider);
            } else {
                return super.handle(clazz, obj);
            }
        }*/
}
