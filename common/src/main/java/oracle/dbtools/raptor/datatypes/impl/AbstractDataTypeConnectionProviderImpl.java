/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.sql.Connection;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionException;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionMutableWrapper;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionReference;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionWrapper;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeContext.ContextAccess;
import oracle.dbtools.raptor.datatypes.DataTypeException;
import oracle.dbtools.raptor.datatypes.DataTypeExtensionObject;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension.ExtensionAccess;
import oracle.dbtools.raptor.datatypes.PropertyChanger;
import oracle.dbtools.util.Closeables;

/**
 *
 * @author <a href=
 * "mailto:barry.mcgillin@oracle.com@oracle.com?subject=AbstractDataTypeConnectionProviderImpl.java"
 * >Barry McGillin</a>
 *
 * @param <C>
 */
public abstract class AbstractDataTypeConnectionProviderImpl<C extends Connection> extends DataTypeExtensionObject implements DataTypeConnectionProvider<C> {
    public abstract static class Builder<C extends Connection> extends DataTypeExtensionObject.Builder implements DataTypeConnectionProvider.Builder<C> {
        private final Class<C> connectionClass;
        private DataTypeConnectionReference<C> datatypeConnection;
        private DataTypeConnectionWrapper<C> nlsConnection;
        private boolean supported;
        
        protected Builder(Class<C> connectionClass) {
            super();
            this.connectionClass = connectionClass;
            this.datatypeConnection = null;
            this.nlsConnection = null;
            this.supported = true;
}

        @Override
        public Builder setExtensionAccess(ExtensionAccess extensionAccess) {
            super.setExtensionAccess(extensionAccess);
            return this;
        }

        @Override
        public Class<C> getConnectionClass() {
            return this.connectionClass;
        }

        @Override
        public Builder setDataTypeConnectionReference(DataTypeConnectionReference<C> connectionReference) {
            if (connectionReference != null) {
                connectionClass.asSubclass(connectionReference.getConnectionClass());
                this.datatypeConnection = connectionReference;
            } else {
                this.datatypeConnection = null;
            }
            return this;
        }

        @Override
        public DataTypeConnectionReference<C> getDataTypeConnectionReference() {
            return this.datatypeConnection;
        }
        
        @Override
        public Builder setNLSConnectionWrapper(DataTypeConnectionWrapper<C> connectionWrapper) {
            if (connectionWrapper != null) {
                connectionClass.asSubclass(connectionWrapper.getConnectionClass());
                this.nlsConnection = connectionWrapper;
            } else {
                this.nlsConnection = null;
            }
            return this;
        }

        @Override
        public DataTypeConnectionWrapper<C> getNLSConnectionWrapper() {
            return this.nlsConnection;
        }
        
        @Override
        public Builder setSupported(boolean supported) {
            this.supported = supported;
            return this;
        }

        @Override
        public boolean isSupported() {
            return this.supported;
        }
    }
    
    private final Class<C> connectionClass;
	private final DataTypeConnectionReference<C> datatypeConnection;
    private final DataTypeConnectionWrapper<C> nlsConnection;
    private final boolean supported;

	protected AbstractDataTypeConnectionProviderImpl(ExtensionAccess extensionAccess,
                                                     Class<C> connectionClass,
                                                     DataTypeConnectionReference<C> datatypeConnection, 
                                                     boolean supported) {
        this(extensionAccess, connectionClass, datatypeConnection, null, supported);
	}

    protected AbstractDataTypeConnectionProviderImpl(ExtensionAccess extensionAccess,
                                                     Class<C> connectionClass,
                                                     DataTypeConnectionReference<C> datatypeConnection,
                                                     DataTypeConnectionWrapper<C> nlsConnection,
                                                     boolean supported) {
        super(extensionAccess);
        this.supported = supported;
        this.connectionClass = connectionClass;
	      this.datatypeConnection = datatypeConnection;
        this.nlsConnection = (nlsConnection == null) 
            ? new DataTypeConnectionWrapperDelegate<C>(datatypeConnection) 
            : nlsConnection;
        

        if (this.datatypeConnection instanceof PropertyChanger) {
            ((PropertyChanger)this.datatypeConnection).addPropertyChangeListener(DataTypeConnectionReference.CONNECTION, new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        final Object oldValue = evt.getOldValue();
                        final Object newValue = evt.getNewValue();
                        
                        if (newValue == null) {
                            ContextAccess contextAccess = getConnectionProviderAccess().peekContextAccess();
                            
                            if (contextAccess != null) {
                                Closeables.close(contextAccess.getDataTypeContext());
                            }
                        }
                        
                        // Fire referent change
                        AbstractDataTypeConnectionProviderImpl.this.firePropertyChange(CONNECTION, oldValue, newValue);
                    }
                });
        }
    }

    protected abstract ConnectionProviderAccess<C> getConnectionProviderAccess();

    @Override
    public abstract DataTypeContext getDataTypeContext();

    protected C releaseResources() throws DataTypeException {
        return datatypeConnection.releaseConnection();
    }

    /*
     * DataTypeConnection
     */
    
    @Override
    public DataTypeConnectionWrapper<C> getDataTypeConnectionWrapper() {
        return datatypeConnection;
    }

    @Override
    @Deprecated
    @SuppressWarnings("deprecation")
    public C peekDataTypeConnection() {
	    return datatypeConnection.peekConnection();
	}

    @Override
    @Deprecated
	public C getDataTypeConnection() {
		return datatypeConnection.getConnection();
	}

    @Override
    @Deprecated
    @SuppressWarnings("deprecation")
    public C getValidDataTypeConnection() {
        if (datatypeConnection instanceof DataTypeConnectionMutableWrapper) {
            return ((DataTypeConnectionMutableWrapper<C>)datatypeConnection).getValidConnection();
        } else {
            return getDataTypeConnection();
        }
	}

    @Override
    @Deprecated
	public void unlockDataTypeConnection() throws DataTypeConnectionException {
	  datatypeConnection.unlockConnection();
	}

    @Override
    @Deprecated
	public C lockDataTypeConnection() throws DataTypeConnectionException {
		return datatypeConnection.lockConnection();
	}

    @Override
    @Deprecated
	public C lockDataTypeConnection(boolean prompt) throws DataTypeConnectionException {
		return datatypeConnection.lockConnection(prompt);
	}

    /*
     * NLSConnection
     */
    
    public DataTypeConnectionWrapper<C> getNLSConnectionWrapper() {
        return nlsConnection;
    }

    @Override
    @Deprecated
	public C getNLSConnection() {
		return nlsConnection.getConnection();
	}

    @Override
    @Deprecated
    @SuppressWarnings("deprecation")
    public C peekNLSConnection() {
	    return nlsConnection.peekConnection();
	}

    @Override
    @Deprecated
    @SuppressWarnings("deprecation")
    public C getValidNLSConnection() throws DataTypeConnectionException {
		return nlsConnection.getConnection();
	}

    @Override
    @Deprecated
	public void unlockNLSConnection() throws DataTypeConnectionException {
	  nlsConnection.unlockConnection();
	}

    @Override
    @Deprecated
	public C lockNLSConnection() throws DataTypeConnectionException {
		return nlsConnection.lockConnection();
	}

    @Override
    @Deprecated
	public C lockNLSConnection(boolean prompt) throws DataTypeConnectionException {
		return nlsConnection.lockConnection(prompt);
	}

    @Override
	public final Class<C> getDataTypeConnnectionClass() {
		return datatypeConnection.getConnectionClass();
	}
    
    @Override
    public final boolean isSupported() {
        return supported;
    }
    
    @Override
    public final boolean equals(Object obj) {
        return super.equals(obj);
    }
    
    @Override
    public final int hashCode() {
        return super.hashCode();
    }
    
    protected abstract void firePropertyChange(String propertyName, Object oldValue, Object newValue);
}
