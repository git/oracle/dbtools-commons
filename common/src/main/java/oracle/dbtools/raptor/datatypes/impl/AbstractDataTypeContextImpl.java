/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.lang.ref.SoftReference;

import java.util.Iterator;
import java.util.Map;

import java.util.concurrent.ConcurrentHashMap;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider.ConnectionProviderAccess;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeException;
import oracle.dbtools.raptor.datatypes.DataTypeExtensionObject;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension.ExtensionAccess;
import oracle.dbtools.raptor.datatypes.TypeMetadata;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=DataTypeContext.java">Barry McGillin</a>
 *
 */
public abstract class AbstractDataTypeContextImpl extends DataTypeExtensionObject implements DataTypeContext {
    protected abstract static class Builder extends DataTypeExtensionObject.Builder implements DataTypeContext.Builder {
        private ConnectionProviderAccess connectionProviderAccess;
        
        protected Builder(ConnectionProviderAccess connectionProviderAccess) {
            super();
            this.connectionProviderAccess = connectionProviderAccess;
        }
        
        protected Builder() {
            this(null);
        }

        @Override
        public Builder setExtensionAccess(ExtensionAccess extensionAccess) {
            super.setExtensionAccess(extensionAccess);
            return this;
        }
        
        @Override
        public Builder setConnectionProviderAccess(ConnectionProviderAccess connectionProviderAccess) {
            this.connectionProviderAccess = connectionProviderAccess;
            return this;
        }
        
        @Override
        public ConnectionProviderAccess getConnectionProviderAccess() {
            return this.connectionProviderAccess;
        }
    }
    
    private volatile ConnectionProviderAccess connectionProviderAccess;
    private final Map<TypeMetadata, SoftReference<DataType>> datatypeCache;
    private transient int creates;
    private transient int frees;
    private transient int reuses;

    protected AbstractDataTypeContextImpl(ExtensionAccess extensionAccess,
                                          ConnectionProviderAccess connectionProviderAccess) {
        super(extensionAccess);
        this.creates = 0;
        this.frees = 0;
        this.reuses = 0;
        this.connectionProviderAccess = connectionProviderAccess;
        this.datatypeCache = new ConcurrentHashMap<TypeMetadata, SoftReference<DataType>>();
    }

    protected abstract ContextAccess getContextAccess();

    public DataTypeConnectionProvider getDataTypeConnectionProvider() {
        return (connectionProviderAccess != null) ? connectionProviderAccess.getDataTypeConnectionProvider() : null;
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }
    
    @Override
    public final int hashCode() {
        return super.hashCode();
    }
    
    protected DataType cacheDataType(TypeMetadata typeMetadata, DataType datatype) {
        if (datatype != null) {
            // Cache DataType
            datatypeCache.put(typeMetadata, new SoftReference<DataType>(datatype));
            creates++;
        }
        
        return datatype;
    }
    
    public DataType getDataType(TypeMetadata typeMetadata) {
        DataType datatype = lookupDataTypeCache(typeMetadata);
        
        if (datatype == null) {
            datatype = cacheDataType(typeMetadata, resolveDataType(typeMetadata));
        } else {
            reuses++;
        }
        
        return datatype;
    }
    
    public DataType lookupDataType(TypeMetadata typeMetadata) {
        DataType datatype = lookupDataTypeCache(typeMetadata);
        
        if (datatype != null) {
            reuses++;
        }
        
        return datatype;
    }
    
    public void mapDataType(TypeMetadata typeMetadata, DataType datatype) {
        DataType mappedDatatype = lookupDataTypeCache(typeMetadata);
        
        if (datatype != null) {
            if (mappedDatatype == null) {
                cacheDataType(typeMetadata, datatype);
            }
        }
    }
    
    protected DataType resolveDataType(TypeMetadata typeMetadata) {
        return getExtensionAccess().resolveDataType(this, typeMetadata);
    }
    
    protected DataType lookupDataTypeCache(TypeMetadata typeMetadata) {
        // Get Soft Reference
        SoftReference<DataType> ref = datatypeCache.get(typeMetadata);
        
        // Cleanup cache and return
        if (ref != null) {
            DataType dt = ref.get();
            
            if (dt == null) {
                datatypeCache.remove(typeMetadata);
                frees++;
            } else {
                return dt;
            }
        }
        
        return null;
    }
    
    /*
     * Cache Mantenance
     */


    protected synchronized void releaseResources() throws DataTypeException {
        // Nullify connectionProvider ASAP
        setDataTypeConnectionProvider(null);

        // Teardown datatype cache
        Iterator<Map.Entry<TypeMetadata, SoftReference<DataType>>> dataTypeMapIter = datatypeCache.entrySet().iterator();
        while (dataTypeMapIter.hasNext()) {
            Map.Entry<TypeMetadata, SoftReference<DataType>> dataTypeMapEntry = dataTypeMapIter.next();
            SoftReference<DataType> value = dataTypeMapEntry.getValue();
            if (value != null) {
                value.clear();
            }
        }
        datatypeCache.clear();
        
        // Destroy stats
        creates = 0;
        frees = 0;
        reuses = 0;
    }
    
    private void setDataTypeConnectionProvider(ConnectionProviderAccess connectionProviderAccess) {
        if (this.connectionProviderAccess != connectionProviderAccess) {
            // Set Connection Provider
            ConnectionProviderAccess oldConnectionProviderAccess = this.connectionProviderAccess;
            this.connectionProviderAccess = connectionProviderAccess;
            
            // Fire Change
            firePropertyChange(CONNECTION_PROVIDER, oldConnectionProviderAccess, connectionProviderAccess);
        }
    }

    protected synchronized void cleanupDataTypeCache() {
        // Cleanup DataType Cache
        Iterator<Map.Entry<TypeMetadata, SoftReference<DataType>>> dataTypeMapIter = datatypeCache.entrySet().iterator();
        while (dataTypeMapIter.hasNext()) {
            Map.Entry<TypeMetadata, SoftReference<DataType>> dataTypeMapEntry = dataTypeMapIter.next();
            
            if (dataTypeMapEntry.getValue() == null || dataTypeMapEntry.getValue().get() == null) {
                dataTypeMapIter.remove();
                frees++;
            }
        }
    }
    
    protected abstract void firePropertyChange(String propertyName, Object oldValue, Object newValue);
}
