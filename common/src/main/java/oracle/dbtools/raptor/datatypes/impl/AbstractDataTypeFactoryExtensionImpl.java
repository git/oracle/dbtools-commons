/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.net.URI;

import java.sql.Connection;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider.ConnectionProviderAccess;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionReference;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionWrapper;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeContext.ContextAccess;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtensionManager;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtensionManager.ExtensionManagerAccess;
import oracle.dbtools.raptor.datatypes.DataTypeProvider;
import oracle.dbtools.raptor.datatypes.TypeMetadata;

public abstract class AbstractDataTypeFactoryExtensionImpl implements DataTypeFactoryExtension {
    protected class DefaultExtensionAccess implements ExtensionAccess {
        public DefaultExtensionAccess() {};


        @Override
        public DataTypeFactory getDataTypeFactory() {
            return AbstractDataTypeFactoryExtensionImpl.this.getDataTypeFactory();
        }

        @Override
        public ExtensionManagerAccess getExtensionManagerAccess() {
            return AbstractDataTypeFactoryExtensionImpl.this.getExtensionManagerAccess();
        }

        @Override
        public DataTypeFactoryExtension getDataTypeFactoryExtension() {
            return AbstractDataTypeFactoryExtensionImpl.this;
        }

        @Override
        public DataTypeProvider lookupDataTypeProvider(URI dataTypeProviderURI) {
            return AbstractDataTypeFactoryExtensionImpl.this.lookupDataTypeProvider(dataTypeProviderURI);
        }

        @Override
        public DataTypeProvider resolveDataTypeProvider(URI dataTypeProviderURI) {
            return AbstractDataTypeFactoryExtensionImpl.this.resolveDataTypeProvider(dataTypeProviderURI);
        }

        @Override
        public ContextAccess getContextAccess(ConnectionProviderAccess connectionProviderAccess) {
            return getExtensionManagerAccess().getContextAccess(connectionProviderAccess);
        }

        @Override
        public ContextAccess peekContextAccess(ConnectionProviderAccess connectionProviderAccess) {
            return getExtensionManagerAccess().peekContextAccess(connectionProviderAccess);
        }
        
        @Override
        public DataType resolveDataType(DataTypeContext context, TypeMetadata typeMetadata) {
            return AbstractDataTypeFactoryExtensionImpl.this.resolveDataType(context, typeMetadata);
        }
    }
    
    public abstract static class Builder implements DataTypeFactoryExtension.Builder {
        private final URI extensionURI;
        private URI superExtensionURI;
        private ExtensionManagerAccess extensionManagerAccess;
        private DataTypeProvider.Builder dataTypeProvider;
        

        protected Builder(URI extensionURI) {
            this.extensionURI = extensionURI;
            this.superExtensionURI = null;
            this.dataTypeProvider = null;
        }

        @Override
        public URI getExensionURI() {
            return this.extensionURI;
        }

        @Override
        public URI getSuperExensionURI() {
            return this.superExtensionURI;
        }

        @Override
        public Builder setSuperExtensionURI(URI superExtensionURI) {
            this.superExtensionURI = superExtensionURI;
            return this;
        }

        @Override
        public Builder setExtensionManagerAccess(ExtensionManagerAccess extensionManagerAccess) {
            this.extensionManagerAccess = extensionManagerAccess;
            return this;
        }

        @Override
        public ExtensionManagerAccess getExtensionManagerAccess() {
            return extensionManagerAccess;
        }
        
        @Override
        public DataTypeProvider.Builder getDataTypeProvider() {
            return dataTypeProvider;
        }

        @Override
        public Builder setDataTypeProvider(DataTypeProvider.Builder dataTypeprovider) {
            this.dataTypeProvider = dataTypeprovider;
            return this;
        }
    }
    
    private final ExtensionManagerAccess extensionManagerAccess;
    private final ExtensionAccess superExtensionAccess;
    
    public AbstractDataTypeFactoryExtensionImpl(ExtensionManagerAccess extensionManagerAccess,
                                                ExtensionAccess superExtensionAccess) {
        super();
        this.extensionManagerAccess = extensionManagerAccess;
        this.superExtensionAccess = superExtensionAccess;
    }
    
    protected ExtensionManagerAccess getExtensionManagerAccess() {
        return extensionManagerAccess;
    }
    
    protected abstract ExtensionAccess getExtensionAccess();
    
    public final DataTypeFactoryExtensionManager getDataTypeFactoryExtensionManager() {
        return extensionManagerAccess.getDataTypeFactoryExtensionManager();
    }
    
    public final DataTypeFactory getDataTypeFactory() {
        return extensionManagerAccess.getDataTypeFactory();
    }
    
    protected final ExtensionAccess getSuperExtensionAccess() {
        return superExtensionAccess;
    }
    
    protected final DataTypeFactoryExtension getSuperExtension() {
        return (superExtensionAccess != null) ? superExtensionAccess.getDataTypeFactoryExtension() : null;
    }
    
    public final boolean handles(Class<?> clazz) {
        boolean handles = handlesExplicitly(clazz);
        if (!handles) {
            DataTypeFactoryExtension superExtension = getSuperExtension();
            if (superExtension != null) {
                handles = superExtension.handles(clazz);
            }
        }
        
        return handles;
    }
    
    public boolean handlesExplicitly(Class<?> clazz) {
        return false;
    }
    
    
    public abstract DataTypeProvider getDataTypeProvider();
    
    protected DataType resolveDataType(DataTypeContext context, TypeMetadata typeMetadata) {
        return getDataTypeProvider().getDataType(context, typeMetadata);
    }
    
    protected DataTypeProvider lookupDataTypeProvider(URI dataTypeProviderURI) {
        DataTypeProvider provider = getDataTypeProvider();
        
        if (provider != null && provider.getDataTypeProviderURI().equals(dataTypeProviderURI)) {
            return provider;
        } else {
            return null;
        }
    }

    protected DataTypeProvider resolveDataTypeProvider(URI dataTypeProviderURI) {
        DataTypeProvider provider = lookupDataTypeProvider(dataTypeProviderURI);
        
        if (provider == null) {
            ExtensionAccess superExtensionAccess = getSuperExtensionAccess();
            
            if (superExtensionAccess != null) {
                provider = superExtensionAccess.resolveDataTypeProvider(dataTypeProviderURI);
            }
        }
        
        return provider;
    }
    
    @Override
    public Extendable.Builder builder(Class<?> clazz, Object... obj) {
        Extendable.Builder builder = null;
        
        // Defer to super extension
        DataTypeFactoryExtension superExtension = getSuperExtension();
        if (superExtension != null) {
            builder = superExtension.builder(clazz, obj);
        }
        
        // Provide own default
        if (builder == null) {
            if (clazz == ContextAccess.class) { 
                if (obj != null && obj.length == 1) {
                    if (ConnectionProviderAccess.class.isAssignableFrom(obj[0].getClass())) {
                        builder =  DataTypeContextImpl.builder((ConnectionProviderAccess)obj[0]);
                    }
                }
            } else if (clazz == ConnectionProviderAccess.class) {
                PARSE_PROVIDER:
                if (obj != null) {
                    DataTypeConnectionProviderImpl.Builder candidate = null;
                    if (obj.length >= 1) {
                        if (Connection.class.isAssignableFrom(obj[0].getClass())) {
                            candidate = DataTypeConnectionProviderImpl.builder(Connection.class)
                                        .setDataTypeConnectionReference(new DataTypeConnectionReferenceImpl(Connection.class, (Connection)obj[0]));
                        } else if (DataTypeConnectionReference.class.isAssignableFrom(obj[0].getClass())) {
                            DataTypeConnectionReference connectionReference = (DataTypeConnectionReference)obj[0];
                            if (Connection.class.isAssignableFrom(connectionReference.getConnectionClass())) {
                                candidate = DataTypeConnectionProviderImpl.builder(Connection.class)
                                            .setDataTypeConnectionReference(connectionReference);
                            } else {
                                break PARSE_PROVIDER;
                            }
                        } else {
                            break PARSE_PROVIDER;
                        }
                    } else {
                        break PARSE_PROVIDER;
                    }
                    if (obj.length == 1) {
                        ;
                    } else if (obj.length == 2) {
                        if (obj[1] != null) {
                            if (DataTypeConnectionWrapper.class.isAssignableFrom(obj[1].getClass())) {
                                DataTypeConnectionWrapper nlsWrapper = (DataTypeConnectionWrapper)obj[1];
                                if (Connection.class.isAssignableFrom(nlsWrapper.getConnectionClass())) {
                                    candidate.setNLSConnectionWrapper(nlsWrapper);
                                } else {
                                    break PARSE_PROVIDER;  
                                }
                            } else {
                                break PARSE_PROVIDER;
                            }
                        }
                    } else {
                        break PARSE_PROVIDER;
                    }
                    
                    builder = candidate;
                }
            }
        }
        
        if (builder != null) {
            builder = builder.setExtensionAccess(getExtensionAccess());
        }
        
        return builder;
    }
}
