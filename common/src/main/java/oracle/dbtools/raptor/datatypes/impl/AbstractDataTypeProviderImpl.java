/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.lang.reflect.Constructor;

import java.net.URI;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeExtensionObject;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension.ExtensionAccess;
import oracle.dbtools.raptor.datatypes.DataTypeProvider;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.TypeMetadata.Attribute;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=AbstractDataTypeProviderImpl.java">Barry McGillin</a>
 *
 */
public abstract class AbstractDataTypeProviderImpl extends DataTypeExtensionObject implements DataTypeProvider {
    protected abstract static class Builder extends DataTypeExtensionObject.Builder implements DataTypeProvider.Builder {
        private URI dataTypeProviderURI;
        private URI superDataTypeProviderURI;
        
        protected Builder(URI dataTypeProviderURI) {
            super();
            this.dataTypeProviderURI = dataTypeProviderURI;
            this.superDataTypeProviderURI = null;
        }

        @Override
        public Builder setExtensionAccess(ExtensionAccess extensionAccess) {
            super.setExtensionAccess(extensionAccess);
            return this;
        }
       
        public URI getSuperDataTypeProviderURI() {
            return this.superDataTypeProviderURI;
        }

        public Builder setSuperDataTypeProviderURI(URI superDataTypeProvider) {
            this.superDataTypeProviderURI = superDataTypeProvider;
            return this;
        }
        
        public URI getDataTypeProviderURI() {
            return this.dataTypeProviderURI;
        }
    }
    
    private final URI dataTypeProviderURI;
    private final DataTypeProvider next;

    private final Map<Integer, TypeMetadata> typeCodeMetadataMap;
    private final Map<String, TypeMetadata> typeNameMetadataMap;

    protected AbstractDataTypeProviderImpl(ExtensionAccess extensionAccess,
                                           final URI dataTypeProviderURI,
                                           DataTypeProvider next,
                                           Map<String, NamedValue<Map<TypeMetadata.Attribute, Object>>> typePropertyMap) {
        super(extensionAccess);
        this.dataTypeProviderURI = dataTypeProviderURI;
        this.next = next;
        this.typeCodeMetadataMap = new HashMap<Integer, TypeMetadata>();
        this.typeNameMetadataMap = new HashMap<String, TypeMetadata>();
        
        
        // Populate TypeMetata objects
        for (Map.Entry<String, NamedValue<Map<TypeMetadata.Attribute, Object>>> entry : typePropertyMap.entrySet()) {
            String typeName = entry.getKey();
            NamedValue<Map<TypeMetadata.Attribute, Object>> namedProps = entry.getValue();
            Map<TypeMetadata.Attribute, Object> propMap = new HashMap<TypeMetadata.Attribute, Object>();
            getTypePropertyMap(next, propMap, typePropertyMap, typeName, namedProps);
            
            boolean syntaxSynomym = (namedProps != null && namedProps.getValue() == null && namedProps.getName() != null && !namedProps.getName().contains("("));

            TypeMetadata typeMetadata = DataTypeFactory.getTypeMetadata(propMap, syntaxSynomym);
            
            if (typeMetadata != null) {
                typeNameMetadataMap.put(typeName, typeMetadata);
            }
        }
        
        //Populate Type Code
        for (Map.Entry<String,NamedValue<Map<TypeMetadata.Attribute, Object>>> entry : typePropertyMap.entrySet()) {
            String typeName = entry.getKey();
            NamedValue<Map<TypeMetadata.Attribute, Object>> namedProps = entry.getValue();
            Map<TypeMetadata.Attribute, Object> props = namedProps.getValue();
            
            if (props != null ) {
                // Populate type_code vs TypeMetadata
                Integer typeCode = (Integer)props.get(TypeMetadata.Attribute.TYPE_CODE);
                if (typeCode != null) {
                    TypeMetadata typeMetadata = typeNameMetadataMap.get(typeName);
                    
                    if (typeMetadata != null) {
                        typeCodeMetadataMap.put(typeCode, typeMetadata);
                    }
                }
            }
        }
    }
    
    protected abstract DataTypeProviderAccess getDataTypeProviderAccess();

    private static void getTypePropertyMap(DataTypeProvider next, Map<TypeMetadata.Attribute, Object> propMap, Map<String, NamedValue<Map<TypeMetadata.Attribute, Object>>> typePropertyMap, String typeName, NamedValue<Map<TypeMetadata.Attribute, Object>> namedProps) {
        if (namedProps != null) {
            String basedOnName = namedProps.getName();
            Map<TypeMetadata.Attribute, Object> props = namedProps.getValue();
            
            if (basedOnName != null) {
                // Do Base Type First
                NamedValue<Map<TypeMetadata.Attribute, Object>> basedOnNamedProps = typePropertyMap.get(basedOnName);
                getTypePropertyMap(next, propMap, typePropertyMap, basedOnName, basedOnNamedProps);
            }
            
            // Add these properties
            if (props != null) {
                propMap.putAll(props);
            }
            
            // Apply the Basename
            if (!propMap.containsKey(TypeMetadata.Attribute.BASE_TYPE)) {
                propMap.put(TypeMetadata.Attribute.BASE_TYPE, typeName);
            }
            
        } else {
            TypeMetadata typeMetadata = (next != null ? next.getTypeMetadata(typeName) : null);
            
            if (typeMetadata != null) {
                typeMetadata.export(propMap);
            }
        }
        
        // Apply the Datatype name
        propMap.put(TypeMetadata.Attribute.DATA_TYPE, typeName);
    }

    public DataType getDataType(DataTypeContext context, TypeMetadata typeMetadata) {
        DataType datatype = null;
        TypeMetadata expandedTypeMetadata = expandTypeMetadata(context, typeMetadata);
        
        if (expandedTypeMetadata == null) {
            if (next != null) {
                return next.getDataType(context, typeMetadata);
            }
        } else {
            try {
                Class<? extends DataType> dataTypeClass = getDataTypeClass(expandedTypeMetadata);
    
                if (dataTypeClass != null) {
                    Constructor<? extends DataType> constr =
                        dataTypeClass.getDeclaredConstructor(DataTypeContext.class, TypeMetadata.class);
                    constr.setAccessible(true);
                    datatype = constr.newInstance(context, expandedTypeMetadata);
                }
            } catch (Exception e) {
                Logger.getLogger(getClass().getClass().getName()).log(Level.SEVERE, e.getStackTrace()[0].toString(), e);
            }
        }
        
        return (datatype == null) ? new UnsupportedDataTypeImpl(context, typeMetadata) : datatype;
    }
    
    protected TypeMetadata filterExpandedTypeMetadata(TypeMetadata expandedTypeMetadata, TypeMetadata typeMetadata) {
        return customFilterExpandedTypeMetadata(expandedTypeMetadata, typeMetadata);
    }

    protected TypeMetadata customFilterExpandedTypeMetadata(TypeMetadata expandedTypeMetadata, TypeMetadata typeMetadata) {
        if (expandedTypeMetadata != null) {
            // Ensure Required Implementation
            String expandedImplDataType = expandedTypeMetadata.get_impl_data_type();
            if (typeMetadata != null) {
                String implDataType = typeMetadata.get_impl_data_type();
                
                if (implDataType != null) {
                    if (expandedImplDataType == null || !expandedImplDataType.equals(implDataType)) {
                        return null;
                    }
                }
            }
            
            // Ensure Supported
            if (!customSupportedType(expandedTypeMetadata)) {
                return null;
            }
        }
        
        return expandedTypeMetadata;
    }

    protected Class<? extends DataType> getDataTypeClass(TypeMetadata typeMetadata) throws ClassNotFoundException{
        return typeMetadata.get_impl_class();
    }

    protected String getDataTypeName(TypeMetadata typeMetadata) {
        return typeMetadata.get_data_type();
    }

    protected boolean customSupportedType(TypeMetadata typeMetadata) {
        return customImplementedType(typeMetadata);
    }

    protected boolean customImplementedType(TypeMetadata typeMetadata) {
        try {
            return (getDataTypeClass(typeMetadata) != null);
        } catch (ClassNotFoundException e) {
            return true;
        }
    }

    public final Set<String> getSupportedTypes() {
        Set<String> chainTypes = (next != null) ? next.getSupportedTypes() : new TreeSet<String>();
        for (Map.Entry<String, TypeMetadata> entry : typeNameMetadataMap.entrySet()) {
            if (entry.getKey() != null && entry.getValue() != null && !entry.getKey().contains("(") && customImplementedType(entry.getValue())) {
                chainTypes.add(entry.getKey());
            }
        }
        return chainTypes;
    }

    public final Set<Integer> getSupportedSQLTypes() {
        Set<Integer> chainTypes = (next != null) ? next.getSupportedSQLTypes() : new TreeSet<Integer>();
        for (Map.Entry<Integer, TypeMetadata> entry : typeCodeMetadataMap.entrySet()) {
            if (entry.getKey() != null && entry.getValue() != null && !customImplementedType(entry.getValue())) {
                chainTypes.add(entry.getKey());
            }
        }
        return chainTypes;
    }

    public final Map<String, TypeMetadata> getSupportedTypeMetadata() {
        Map<String, TypeMetadata> chainTypeMetadata = (next != null) ? next.getSupportedTypeMetadata() : new HashMap<String, TypeMetadata>();
        for (Map.Entry<String, TypeMetadata> entry : typeNameMetadataMap.entrySet()) {
            if (entry.getKey() != null && entry.getValue() != null && !entry.getKey().contains("(") && customImplementedType(entry.getValue())) {
                chainTypeMetadata.put(entry.getKey(), entry.getValue());
            }
        }
        return chainTypeMetadata;
    }

    protected TypeMetadata expandTypeMetadata(DataTypeContext context, final TypeMetadata typeMetadata){
        // Refine type
        TypeMetadata refinedTypeMetadata = refineTypeMetadata(context, typeMetadata);
        
        // Lookup Template
        TypeMetadata template = lookupTypeMetadataTemplate(refinedTypeMetadata);

        // Apply template
        TypeMetadata expandedTypeMetadata = (template != null)
            ? filterExpandedTypeMetadata(DataTypeFactory.getTypeMetadata(template, refinedTypeMetadata), refinedTypeMetadata)
            : null;
        
        // Expand DataType into Type attribute
        if (expandedTypeMetadata != null) {
            Map<Attribute, Object> attributes = new HashMap<Attribute, Object>();
            
            expandDataTypeAttributes(expandedTypeMetadata, attributes);
            
            if (attributes.size() > 0) {
                expandedTypeMetadata = DataTypeFactory.getTypeMetadata(expandedTypeMetadata, attributes);
            }
        }
        
        return expandedTypeMetadata;
    }
    
    protected final TypeMetadata refineTypeMetadata(DataTypeContext context, final TypeMetadata typeMetadata) {
        return customRefineTypeMetadata(context, typeMetadata);
    }
    
    protected TypeMetadata customRefineTypeMetadata(DataTypeContext context, final TypeMetadata typeMetadata) {
        return typeMetadata;
    }
    
    protected TypeMetadata lookupTypeMetadataTemplate(final TypeMetadata typeMetadata) {
        TypeMetadata template = null;
        
        Integer typeCode = typeMetadata.get_type_code();

        // Find Template via Name
        if (typeMetadata.get_data_type() != null) {
            template = typeNameMetadataMap.get(typeMetadata.get_data_type());
            
            if (template != null && typeCode != null && !typeCode.equals(template.get_type_code())) {
                template = null;
            }
        }
        if (template == null && typeMetadata.get_base_type() != null) {
            template = typeNameMetadataMap.get(typeMetadata.get_base_type());
            
            if (template != null && typeCode != null && !typeCode.equals(template.get_type_code())) {
                template = null;
            }
        }
        
        // Find Template via TypeCode
        if (template == null && typeCode != null) {
            template = typeCodeMetadataMap.get(typeCode);
        }
        
        return template;
    }

    protected void expandDataTypeAttributes(TypeMetadata typeMetadata, Map<Attribute, Object> attributeMap) {
        String datatype = typeMetadata.get_data_type();
        
        if (datatype != null && (datatype.contains(".") || datatype.contains("@")) && 
            typeMetadata.get_type_owner() == null && 
            typeMetadata.get_type_name() == null &&
            typeMetadata.get_type_subname() == null &&
            typeMetadata.get_type_link() == null)
        {
            String[] locations = datatype.split("[@]");
            String[] ids = locations[0].split("[.]");
            
            if (ids.length == 2) {
                attributeMap.put(TypeMetadata.Attribute.TYPE_OWNER, ids[0]);
                attributeMap.put(TypeMetadata.Attribute.TYPE_NAME, ids[1]);
            } else if (ids.length == 3) {
                attributeMap.put(TypeMetadata.Attribute.TYPE_OWNER, ids[0]);
                attributeMap.put(TypeMetadata.Attribute.TYPE_NAME, ids[1]);
                attributeMap.put(TypeMetadata.Attribute.TYPE_SUBNAME, ids[2]);
            }
            
            if (locations.length == 2) {
                attributeMap.put(TypeMetadata.Attribute.TYPE_LINK, locations[1]);
                
                if (ids.length == 1) {
                    attributeMap.put(TypeMetadata.Attribute.TYPE_NAME, ids[0]);
                }
            }
            
            if (attributeMap.size() != 0 && attributeMap.get(TypeMetadata.Attribute.DATA_TYPE) == null) {
                attributeMap.put(TypeMetadata.Attribute.DATA_TYPE, datatype);
            }
        }
    }

    public final TypeMetadata getTypeMetadata(String datatype){
        TypeMetadata typeMetaData = customTypeMetadata(datatype);
        return (typeMetaData == null && next != null) ? next.getTypeMetadata(datatype) : typeMetaData;
    }

    protected TypeMetadata customTypeMetadata(String datatype) {
        return typeNameMetadataMap.get(datatype);
    }
    
    public final TypeMetadata getTypeMetadata(Integer oracleType) {
        TypeMetadata typeMetaData = customTypeMetadata(oracleType);
        return (typeMetaData == null && next != null) ? next.getTypeMetadata(oracleType) : typeMetaData;
    }

    protected TypeMetadata customTypeMetadata(Integer oracleType) {
        return typeCodeMetadataMap.get(oracleType);
    }


    @Override
    public final URI getDataTypeProviderURI() {
        return dataTypeProviderURI;
    }
}
