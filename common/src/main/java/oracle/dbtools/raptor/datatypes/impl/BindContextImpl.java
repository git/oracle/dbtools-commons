/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.sql.Statement;

import java.util.HashMap;
import java.util.List;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingStrategy;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypePlatformException;

import oracle.dbtools.raptor.datatypes.DataValue;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OraclePreparedStatement;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=BindContextImpl.java">Barry McGillin</a>
 *
 */
public class BindContextImpl<S extends Statement> implements BindContext<S> {
    private final Class<? extends S> stmtClass;
    private S stmt;
        
    protected final DataTypeConnectionProvider connectionProvider;

    private int lastBindIndex;
    private List<String> bindTokens;
    
    private final HashMap <BindingStrategy,DataValue> inputs = new HashMap<BindingStrategy,DataValue>();
    private final HashMap <BindingStrategy,DataValue> outputs = new HashMap<BindingStrategy,DataValue>();
    
    public BindContextImpl(Class<? extends S> stmtClass, DataTypeConnectionProvider provider) {
        this.stmtClass = stmtClass;
        this.connectionProvider = provider;
        this.lastBindIndex = 0;
        this.bindTokens = null;
    }

    public String nextBindToken() {
        return ":" + ++lastBindIndex;
    }

    public String lastBindToken() {
        return ":" + lastBindIndex;
    }

    public int remapPosition(String bindToken) {
        int newPosition = -1;
        if (bindTokens != null) {
            newPosition = bindTokens.indexOf(bindToken); //$NON-NLS-1$
            if (newPosition >= 0) {
                newPosition++;
            }
        }

        return newPosition;
    }

    public void setBindTokens(List<String> bindTokens) {
        this.bindTokens = bindTokens;
    }

    public List<String> getBindTokens() {
        return this.bindTokens;
    }

    public DataTypeConnectionProvider getDataTypeConnectionProvider() {
        return this.connectionProvider;
    }

    @Override
    public Class<? extends S> getStatementClass() {
        return stmtClass;
    }

    public Class<? extends Statement> getEffectiveStatementClass() {
        if (stmt != null) {
            if (stmt instanceof OracleCallableStatement) {
                return OracleCallableStatement.class;
            }
        } else if (stmtClass != null && OracleCallableStatement.class.isAssignableFrom(stmtClass)) {
            return OracleCallableStatement.class;
        }
        
        return OraclePreparedStatement.class;
    }

    @Override
    public final S getStatement() {
      return stmt;
    }

    @Override
    public final void setStatement(S stmt) {
      try {
          this.stmt = stmtClass.cast(stmt);
      } catch (ClassCastException e) {
          throw new DataTypePlatformException(OracleCallableStatement.class);
      }
    }
    
    @Override
    public DataValue getInput(BindingStrategy binding) {
        return inputs.get(binding);
    }
    
    @Override
    public void cacheInput(BindingStrategy binding, DataValue value) {
        inputs.put(binding, value);
    }
    
    @Override
    public DataValue getOutput(BindingStrategy binding) {
        return outputs.get(binding);
    }
    
    @Override
    public void cacheOutput(BindingStrategy binding, DataValue value) {
        outputs.put(binding, value);
    }
    
    @Override
    public void emptyInputCache() {
        inputs.clear();
    }
    
    @Override
    public void emptyOutputCache() {
        outputs.clear();
    }
    
    @Override
    public void emptyValueCaches() {
        emptyInputCache();
        emptyOutputCache();
    }
    
    @Override
    public void uncacheInput(BindingStrategy binding) {
        inputs.remove(binding);
    }
    
    @Override
    public void uncacheOutput(BindingStrategy binding) {
        outputs.remove(binding);
    }
    
    @Override
    public void uncacheValues(BindingStrategy binding) {
        uncacheOutput(binding);
        uncacheInput(binding);
    }
}
