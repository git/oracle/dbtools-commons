/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.io.IOException;

import java.sql.Connection;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionReference;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionWrapper;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeContext.ContextAccess;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension.ExtensionAccess;

/**
 * Default DataTypeConnectionProvider
 */
public class DataTypeConnectionProviderImpl<C extends Connection> extends AbstractDataTypeConnectionProviderImpl<C> {
    private final ConnectionProviderAccessImpl<C> connectionProviderAccess;
    
    public class ConnectionProviderAccessImpl<C extends Connection> implements ConnectionProviderAccess<C> {
        private final PropertyChangeSupport propertyChangeSupport;

        protected ConnectionProviderAccessImpl() {
            this.propertyChangeSupport = new PropertyChangeSupport(this);
        };
        
        @Override
        public DataTypeConnectionProvider getDataTypeConnectionProvider() {
            return DataTypeConnectionProviderImpl.this;
        }
        
        @Override
        public ExtensionAccess getExtensionAccess() {
            return DataTypeConnectionProviderImpl.this.getExtensionAccess();
        }
        
        @Override
        public DataTypeFactory getDataTypeFactory() {
            return getExtensionAccess().getDataTypeFactory();
        }

        @Override
        public ContextAccess getContextAccess() {
            return  getExtensionAccess().getContextAccess(getConnectionProviderAccess());
        }

        @Override
        public ContextAccess peekContextAccess() {
            return  getExtensionAccess().peekContextAccess(getConnectionProviderAccess());
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(listener);
        }

        @Override
        public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
        }

        protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
            propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(listener);
        }

        @Override
        public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
        }
    }
                
    public static <C extends Connection> Builder<C> builder(Class<C> connectionClass) {
        return new Builder(connectionClass);
    }
    
    public static final class Builder<C extends Connection> extends AbstractDataTypeConnectionProviderImpl.Builder<C> {
        private Builder(Class<C> connectionClass) {
            super(connectionClass);
        }

        @Override
        public ConnectionProviderAccess<C> build() {
            DataTypeConnectionProviderImpl<C> connectionProvider = 
                new DataTypeConnectionProviderImpl<C>(getExtensionAccess(), 
                                                      getConnectionClass(), 
                                                      getDataTypeConnectionReference(), 
                                                      getNLSConnectionWrapper(), 
                                                      isSupported());
            
            return connectionProvider.getConnectionProviderAccess();
        }

        @Override
        public Builder setExtensionAccess(ExtensionAccess extensionAccess) {
            super.setExtensionAccess(extensionAccess);
            return this;
        }

        @Override
        public Builder setDataTypeConnectionReference(DataTypeConnectionReference<C> connectionReference) {
            super.setDataTypeConnectionReference(connectionReference);
            return this;
        }

        @Override
        public Builder setNLSConnectionWrapper(DataTypeConnectionWrapper<C> connectionWrapper) {
            super.setNLSConnectionWrapper(connectionWrapper);
            return this;
        }
        
        @Override
        public Builder setSupported(boolean supported) {
            super.setSupported(supported);
            return this;
        }
        
    }
    
    protected DataTypeConnectionProviderImpl(ExtensionAccess extensionAccess,
                                             Class<C> connectionClass, 
                                             DataTypeConnectionReference<C> datatypeConnection,
                                             DataTypeConnectionWrapper<C> nlsConnection,
                                             boolean supported) {
        super(extensionAccess, connectionClass, datatypeConnection, nlsConnection, supported);
        this.connectionProviderAccess = new ConnectionProviderAccessImpl();
    }


    @Override
    protected ConnectionProviderAccess<C> getConnectionProviderAccess() {
        return connectionProviderAccess;
    }


    @Override
    public DataTypeContext getDataTypeContext() {
        return getConnectionProviderAccess().getContextAccess().getDataTypeContext();
    }

    @Override
    public void close() throws IOException {
        try {
            releaseResources();
        } catch (Exception e) {
            throw new IOException(e);
        }
    }
    
    @Override
    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        connectionProviderAccess.firePropertyChange(propertyName, oldValue, newValue);
    }
}
