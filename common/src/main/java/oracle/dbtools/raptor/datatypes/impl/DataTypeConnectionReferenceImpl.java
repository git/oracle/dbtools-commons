/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.lang.ref.WeakReference;

import java.sql.Connection;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionException;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionReference;
import oracle.dbtools.raptor.datatypes.PropertyChanger;

public class DataTypeConnectionReferenceImpl<C extends Connection> extends DataTypeConnectionLockableImpl<C> implements DataTypeConnectionReference<C>, PropertyChanger{
    protected static final Object _UNKNOWN_ = new Object();
    private final PropertyChangeSupport propertyChangeSupport;
    
    private volatile Object connection;

    public DataTypeConnectionReferenceImpl(Class<C> connectionClass) {
        this(connectionClass, (C)null);
    }

    public DataTypeConnectionReferenceImpl(Class<C> connectionClass, C connection) {
        super(connectionClass);
        this.propertyChangeSupport = new PropertyChangeSupport(this);
        this.connection = _UNKNOWN_;
        this.connection = wrap(connection);
    }

    protected DataTypeConnectionReferenceImpl(Class<C> connectionClass, Object connection) {
        super(connectionClass);
        this.propertyChangeSupport = new PropertyChangeSupport(this);
        this.connection = connection;
        
        // Test connection object
        peekConnection();
    }
    
    protected Object wrap(C connection) {
        return new WeakReference(connection);
    }

    protected C unwrap(Object connection) {
        return getConnectionClass().cast(((WeakReference)connection).get());
    }

    protected C nullify() {
        C ret = unwrap(connection);
        ((WeakReference)connection).clear();
        return ret;
    }

    @Override
    public final synchronized C releaseConnection() {
        return setConnection((C)null);
    }
    
    protected final C setConnection(Object connection) throws DataTypeConnectionException {
        // Retain old value
        final C ret = unwrap(this.connection);
        
        // Test new value
        unwrap(connection);
        
        // Apply new value
        this.connection = connection;
        
        return ret;
    }

    protected C setConnection(C connection) throws DataTypeConnectionException {
        final C oldConnection = (connection == null) 
            ? nullify()
            : setConnection(wrap(connection));
        
        // Fire any propery change
        if (oldConnection != connection) {
            firePropertyChange(CONNECTION, oldConnection, connection);
        }
        
        return oldConnection;
    }
    
    @Override
    public C getConnection() {
        return peekConnection();
    }

    @Override
    public final C peekConnection() {
        return unwrap(connection);
    }

    @Override
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
    }
}
