/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.sql.Connection;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionException;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionReference;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionWrapper;

public class DataTypeConnectionWrapperDelegate<C extends Connection> implements DataTypeConnectionWrapper<C> {
    private final DataTypeConnectionReference<C> delegate;
    private final boolean delegateHold;

    public DataTypeConnectionWrapperDelegate(DataTypeConnectionReference<C> delegate) {
        this(delegate, false);
    }

    public DataTypeConnectionWrapperDelegate(DataTypeConnectionReference<C> delegate, boolean delegateHold) {
        super();
        this.delegateHold = delegateHold;
        this.delegate = delegate;
    }
    
    @Override
    public Class<C> getConnectionClass() {
        return delegate.getConnectionClass();
    }

    public C getConnection() {
        return delegate.getConnection();
    }

    @Override
    public C peekConnection() {
        return delegate.peekConnection();
    }

    @Override
    public void unlockConnection() throws DataTypeConnectionException {
        delegate.unlockConnection();
    }

    @Override
    public C lockConnection() throws DataTypeConnectionException {
        return delegate.lockConnection();
    }

    @Override
    public C lockConnection(boolean prompt) throws DataTypeConnectionException {
        return delegate.lockConnection(prompt);
    }
}
