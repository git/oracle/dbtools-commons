/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.io.IOException;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider.ConnectionProviderAccess;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension.ExtensionAccess;

/**
 * Default DataTypeConext
 */
public class DataTypeContextImpl extends AbstractDataTypeContextImpl {
    private final ContextAccessImpl contextAccess;

    public class ContextAccessImpl implements ContextAccess {
        private final PropertyChangeSupport propertyChangeSupport;

        protected ContextAccessImpl() {
            this.propertyChangeSupport = new PropertyChangeSupport(this);
        };
        
        @Override
        public DataTypeContext getDataTypeContext() {
            return DataTypeContextImpl.this;
        }
        
        @Override
        public ExtensionAccess getExtensionAccess() {
            return DataTypeContextImpl.this.getExtensionAccess();
        }
        
        @Override
        public DataTypeFactory getDataTypeFactory() {
            return getExtensionAccess().getDataTypeFactory();
        }

        @Override
        public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(listener);
        }
    
        protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
            propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
        }
        
        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(listener);
        }
    
        @Override
        public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
        }
        
        @Override
        public synchronized void cleanupDataTypeCache() {
            DataTypeContextImpl.this.cleanupDataTypeCache();
        }
    }
                
    public static final Builder builder(ConnectionProviderAccess connectionProviderAccess) {
        return new Builder(connectionProviderAccess);
    }
    
    public static class Builder extends AbstractDataTypeContextImpl.Builder {
        private Builder(ConnectionProviderAccess connectionProviderAccess) {
            super(connectionProviderAccess);
        }

        @Override
        public ContextAccess build() {
            DataTypeContextImpl context = new DataTypeContextImpl(getExtensionAccess(), getConnectionProviderAccess());
            return context.getContextAccess();
        }

        @Override
        public Builder setExtensionAccess(ExtensionAccess extensionAccess) {
            super.setExtensionAccess(extensionAccess);
            return this;
        }

        @Override
        public Builder setConnectionProviderAccess(ConnectionProviderAccess connectionProviderAccess) {
            super.setConnectionProviderAccess(connectionProviderAccess);
            return this;
        }
    }
    
    protected DataTypeContextImpl(ExtensionAccess extensionAccess, ConnectionProviderAccess connectionProviderAccess) {
        super(extensionAccess, connectionProviderAccess);
        
        this.contextAccess = new ContextAccessImpl();
    }
    
    @Override
    protected ContextAccess getContextAccess() {
        return contextAccess;
    }

    @Override
    public void close() throws IOException {
        try {
            releaseResources();
        } catch (Exception e) {
            throw new IOException(e);
        }
    }
    
    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        contextAccess.firePropertyChange(propertyName, oldValue, newValue);
    }
}
