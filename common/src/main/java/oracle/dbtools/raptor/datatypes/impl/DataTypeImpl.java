/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeException;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeIOException;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.DataTypeMarshallingException;
import oracle.dbtools.raptor.datatypes.DataTypeSQLException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.StructureType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.values.NamedDataValue;
import oracle.dbtools.raptor.utils.DataTypesUtil;

import org.xml.sax.SAXException;

/**
 * Base (abstract) implementation for DataType.
 * Each object is a hybrid. It is both a representation of a type (having the
 * ability to convert, create, validate, provide a cell editor and renderer
 * for values of this type) and a value holder capable of managing value
 * instances of this type. (Currently two, "input" and "result".)
 * <p>
 *
 * Just about all of the required functionality is provided by this class.
 * Extension classes generally only need to provide implementations for
 * {@link getSqlDataType}specifying which type they support, a type
 * conversion method {@link customTypedValue}to create the internal value,
 * and to register themselves with the {@link DataTypeFactory}.
 * <p>
 *
 * As far as choosing the internal implementation type goes, the "book" says:
 * <p>
 * ----------------------------------------------------------------------<p>
 * From Oracle Database JDBC Developer's Guide and Reference,
 * 11g Release 1 (11.1)
 * Part Number B31224-04<p>
 * Note:<p>
 * Oracle recommends the use of standard JDBC types or Java types
 * whenever possible. The types in the package oracle.sql.* are provided
 * primarily for backward compatibility or for support of a few Oracle
 * specific features such as OPAQUE, OraData, TIMESTAMPTZ, and so on.<p>
 * ----------------------------------------------------------------------
 * <p>
 * but that needs to be interpreted within the constraints of the support
 * provided by editing and utility frameworks available. A list of
 * <code>java.sql.Types</code> and <code>oracle.jdbc.Oracletypes</code>
 * can be found at the end of the java file.
 * <p>
 * Depending on the choice of implementation class and the support available,
 * you may also need to provide overrides for:
 * <p>
 * XML persistence handling ( {@link customGetXmlStringValue}and its inverse,
 * {@link customGetXmlValue}) if <code>getStringValue</code> may loose
 * information (like for DATE who's display is environment dependent.)
 * <p>
 * {@link getStringValue}if DataTypesUtil.stringValue doesn't know how
 * to create a string representation of your data type.
 * <p>
 * Creators of the more complex types may need to supply additional
 * information such as pre/post bind procedures. Refer to the individual
 * method javadoc and source comments for details.
 *
 * @author skutz
 * @author Brian.Jeffries@oracle.com
 *
 */
public abstract class DataTypeImpl implements DataType {
    protected final TypeMetadata typeMetadata;
    // Some specializations of this class (E.g., BLOB) require the connection
    // provided by the data type context in order to function correctly.
    // Since it doesn't know how to recalculate one, it better keep a real 
    // reference.
    // If wrong approach, changes made in minimalistic fashion so can be reset easily 
    private final DataTypeContext dataTypeContext;
    protected final LinkedHashMap<String,NamedValue<DataType>> typeComponents;
    private final String dataTypeString;

    protected DataTypeImpl(DataTypeContext context, TypeMetadata typeMetadata) {
        super();

        this.dataTypeContext = context;
        this.typeComponents = customTypeComponents(context,  typeMetadata.get_type_components());
        this.typeMetadata = buildExpandedTypeMetadata(typeMetadata, getTypeComponents());
        this.dataTypeString = initDataTypeString();
        
        // Initialize connection if require - slight unsafe calling during construction!
        if (requiresConnection() && context != null) {
            context.getDataTypeConnectionProvider().getValidDataTypeConnection();
        }
    }

    protected LinkedHashMap<String,NamedValue<DataType>> customTypeComponents(DataTypeContext context,
                                                            List<NamedValue<TypeMetadata>> metaComponents) {
        LinkedHashMap<String,NamedValue<DataType>> typeComponents = null;
        
        if (metaComponents != null) {
            typeComponents = new LinkedHashMap<String,NamedValue<DataType>>();

            for (NamedValue<TypeMetadata> typeComponent : metaComponents) {
                typeComponents.put(typeComponent.getName(),
                                   new NamedValue<DataType>(typeComponent.getName(),
                                                            context.getDataTypeFactory().getDataType(context,
                                                                                                      typeComponent.getValue())));
            }
        }
        
        return typeComponents;
    }
    
    private TypeMetadata buildExpandedTypeMetadata(TypeMetadata typeMetadata, Iterable<NamedValue<DataType>> subTypes) {
        TypeMetadata builtTypeMetadata = typeMetadata;
        
        if (subTypes != null) {
            List<NamedValue<TypeMetadata>> buildTypeMetadataComponents = new LinkedList<NamedValue<TypeMetadata>>();
            
            // Recover Build TypeMetadata
            for (NamedValue<DataType> namedComponent : subTypes) {
                buildTypeMetadataComponents.add(new NamedValue<TypeMetadata>(namedComponent.getName(), namedComponent.getValue().getTypeMetadata()));
            }
            
            // Package as Attrubute
            Map<TypeMetadata.Attribute, Object> attributes = new HashMap<TypeMetadata.Attribute, Object>();
            attributes.put(TypeMetadata.Attribute.TYPE_COMPONENTS, buildTypeMetadataComponents);
            
            // Build this TypeMetadata
            builtTypeMetadata = DataTypeFactory.getTypeMetadata(builtTypeMetadata, attributes);
        }
        
        return builtTypeMetadata;
    }
        
    /*NOT final*/
    public static boolean _DEBUG = false;

    public final TypeMetadata getTypeMetadata() {
        return this.typeMetadata;
    }

    protected DataTypeContext getDataTypeContext() {
        return dataTypeContext;
    }

    public final DataValue getDataValue(Object object) {
        try {
            if (object instanceof DataValue) {
                DataValue value = (DataValue)object;
                
                if (isDataType(value)) {
                    // Strip any name
                    return NamedDataValue.getNamedDataValue(null, value);
                } else {
                    return getDataValue(value.getTypedValue(ValueType.DEFAULT));
                }
            } else {
                return customDataValue(object);
            }
        } catch (NullPointerException npe) {
            throw npe;
        } catch (DataTypeException de) {
            throw de;
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, object, e);
        }
    }

    /**
     * @param object
     * @return
     * Override to provide an implmentation were the typed value requires
     * a binding strategy
     */
    protected DataValue customDataValue(Object object) {
        return new DataValueImpl(this, object);
    }

    public final Collection<NamedValue<DataType>> getTypeComponents() {
        return (typeComponents != null) ? typeComponents.values() : null;
    }

    public final Iterable<String> getComponentNames() {
        return customComponentNames();
    }
    
    protected Iterable<String> customComponentNames() {
        return (typeComponents != null) ? typeComponents.keySet() : null;
    }
    
    protected final List<DataValue> getComponents(DataValueInternal value) {
        try {
            return (value.getInternalValue() == null || getTypeComponents() == null) ? null :
                   customComponents(value);
        } catch (NullPointerException npe) {
            throw npe;
        } catch (DataTypeException de) {
            throw de;
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, value.getInternalValue(), e);
        }
    }

    protected List<DataValue> customComponents(DataValueInternal value) {
        // Do know how to handle components.
        if (value.getInternalValue() == null || getTypeComponents() == null) {
            List<DataValue> ret = null;
            return ret;
        } else {
            throw new UnsupportedOperationException();
        }
    }

    protected void _debug(String msg) {
        if (_DEBUG) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, msg);
            //System.out.println(this + msg);
        }
    }

    // Generalize value handling so we only have to have one set of get/set
    // logic implemented. Idea is that the values will be held in and accessed
    // from this base class in opaque (java.lang.Object) form, while the
    // specialization classes will be responsible for type conversion and
    // checking.

    // What values can we hold? [To add more, only need to add additional enum
    // initialization value and accessors for each.]

    // Type conversion - centralize null value handling here

    /**
     * @return this data type's string representation of the value
     * @throws InvalidArgumentException if value cannot be converted
     * NOTE: getStringValue(null) == "null"
     * NOTE: for any string returned by this method,
     *  string.equals(getStringValue(getTypedValue(string))) must be true.
     */
    protected final StringValue getStringValue(DataValueInternal value, StringType stringType, int maxLen) {
        return getStringValue(dataTypeContext.getDataTypeConnectionProvider(), value, stringType, maxLen);
    }
    
    protected final StringValue getStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        Object internalValue = value.getInternalValue();
        
        try {
            StringValue stringValue = (internalValue == null) ? new StringValue(null) : customStringValue(connectionProvider, value, stringType, maxLen);
            
            String valueText = stringValue.toString();
            
            if (maxLen < 0 || valueText == null || valueText.length() <= maxLen) {
                return stringValue;
            } else {
                return new StringValue(valueText.substring(0, maxLen), stringValue.getMaxLength());
            }
        } catch (NullPointerException npe) {
            throw npe;
        } catch (DataTypeException de) {
            throw de;
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, internalValue, e);
        }
    }

    /**
     * Specialization classes may supply alt implementation that (at minimum)
     * handles java.lang.String and their 'native' type.
     *
     *  Note that for any string returned by this method,
     *  string.equals(getStringValue(getTypedValue(string))) must be true.
     *
     * @param value a non-null Object
     * @return a String representation of the value
     * @throws InvalidArgumentException if value cannot be converted
     */
     protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        Object internalValue = value.getInternalValue();
        Connection nlsConnection = connectionProvider.peekNLSConnection();
        
        switch (stringType) {
            case NLS:
                nlsConnection = connectionProvider.getNLSConnection();
            case REST:
            case GENERIC:
                try {
                    String valueText = (maxLen >= 0)
                        ? DataTypesUtil.stringValueChecked(internalValue, nlsConnection, maxLen)
                        : DataTypesUtil.stringValueChecked(internalValue, nlsConnection);
                    
                    return new StringValue(valueText);
                } catch (NullPointerException npe) {
                    throw npe;
                } catch (DataTypeException de) {
                    throw de;
                } catch (SQLException e) {
                    throw new DataTypeSQLException(e);
                } catch (IOException e) {
                    throw new DataTypeIOException(e);
                } catch (Exception e) {
                    throw new DataTypeIllegalArgumentException(this, internalValue, e);
                }
            case DEFAULT:
                return customStringValue(connectionProvider, value, (nlsConnection != null) ? stringType.NLS : stringType.GENERIC, maxLen);
            default:
                return customStringValue(connectionProvider, value, StringType.DEFAULT, maxLen);
        }
    }

    /**
     * @return an object of this data type's internal type initialized to value
     * @throws InvalidArgumentException if value cannot be converted
     * NOTE: getTypedValue([null|""|"null"]) == null
     */
    protected final Object getTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        Object internalValue = value.getInternalValue();
        
        // What is displayed for null is based on a user preference
        // If they happen to type that in, treat it as null also.
        try {
            return customTypedValueFilter(connectionProvider, value, valueType, target);
        } catch (NullPointerException npe) {
            throw npe;
        } catch (DataTypeException de) {
            throw de;
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, internalValue, e);
        }
    }

    protected final Object getTypedValue(DataValueInternal value, ValueType valueType, Object target) {
        return getTypedValue(dataTypeContext.getDataTypeConnectionProvider(), value, valueType, target); // TODO: RJW Seems right
    }
    protected final Object getTypedValue(DataValueInternal value, ValueType valueType) {
        return getTypedValue(value, valueType, null);
    }
    
    protected final Object getTypedValue(DataValueInternal value, Object target) {
        return getTypedValue(value, ValueType.TARGET, target);
    }
    
    public final Class getTypedClass(ValueType valueType) {
        return customTypedClass(dataTypeContext.getDataTypeConnectionProvider(), valueType);
    }
    
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case DEFAULT:
                return Object.class;
            default:
                return customTypedClass(connectionProvider, ValueType.DEFAULT);
        }
    }
    
    protected Object customTypedValueFilter(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        // By default the typed value is the same as the internal value
        return (value.getInternalValue() == null) ? target : customTypedValue(connectionProvider, value, valueType, target);
    }

    /**
     * Specialization classes need to supply implementation that (at minimum)
     * handles java.lang.String and their 'native' type.
     *
     *  Note that for any object returned by this method,
     *  object.equals(getTypedValue(getStringValue(object))) must be true.
     *
     * @param value a non-null Object
     * @return an object of the correct type
     * @throws InvalidArgumentException if value cannot be converted
     */
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        Object internalValue = value.getInternalValue();
        
        // By default the typed value is the same as the internal value
        switch (valueType) {
            case DEFAULT:
                return internalValue;
            case TARGET:
                throw new DataTypeIllegalArgumentException(this, target);
            default:
                return customTypedValue(connectionProvider, value, ValueType.DEFAULT, target);
        }
    }

    protected final Object getInternalValue(Object value) {
        // What is displayed for null is based on a user preference
        // If they happen to type that in, treat it as null also.
        try {
            return customInternalValueFilter(dataTypeContext.getDataTypeConnectionProvider(), value);
        } catch (NullPointerException npe) {
            throw npe;
        } catch (DataTypeException de) {
            throw de;
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, value, e);
        }
    }

    protected Object customInternalValueFilter(DataTypeConnectionProvider connectionProvider, Object value) {
        // By default the typed value is the same as the internal value
        return (DataTypesUtil.isNull(value)) ? null : customInternalValue(connectionProvider, value);
    }

    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        return value;
    }

    protected final void marshal(DataValueInternal value, DataValueMarshalHandler hd, String name)  throws SAXException {
        try {
            customMarshal(dataTypeContext.getDataTypeConnectionProvider(), value, hd, name);
        } catch (NullPointerException npe) {
            throw npe;
        } catch (DataTypeException de) {
            throw de;
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, value.getInternalValue(), e);
        }
    }

    protected void customMarshal(DataTypeConnectionProvider connectionProvider, DataValueInternal value, DataValueMarshalHandler hd, String name) throws SAXException {
        // Get StringType for marshalling
        StringType stringType = hd.getStringType();
        
        StringValue stringValue = getStringValue(value, stringType, -1);

        hd.bodyDataValue(this, name, stringValue.toString());
    }

    // Allow override of storage format for special cases

    public final boolean equals(Object obj) {
        boolean isEqual = (this == obj);

        if (!isEqual && obj instanceof DataTypeImpl) {
            isEqual = customEquals(obj);
        }

        return isEqual;
    }

    protected boolean customEquals(Object obj) {
        if (obj instanceof DataTypeImpl) {
            DataTypeImpl objDataType = (DataTypeImpl)obj;

            return (areEquals(dataTypeContext/*.get()*/, objDataType.dataTypeContext/*.get()*/) &&
                    areEquals(typeMetadata, objDataType.typeMetadata) &&
                    areEquals(getTypeComponents(), objDataType.getTypeComponents()));
        } else {
            return false;
        }
    }

    final boolean equals(Object obj1, Object obj2) {
        return ((obj1 == obj2) || (obj1 != null && obj2 != null && customEquals(obj1, obj2)));
    }

    protected boolean customEquals(Object obj1, Object obj2) {
        return areEquals(obj1, obj2);
    }

    protected static boolean areEquals(Object obj1, Object obj2) {
        return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
    }

    private boolean isDataType(DataValue value) {
        return (areEquals(this, value.getDataType()));
    }

    // For standard Raptor cell editor usage
    // oracle.dbtools.raptor.controls.celleditor.ResultSetTableCellEditor

    /**
     * @return the java.sql.Types or (when needed) oracle.jdbc.OracleTypes
     *         value for this data type. See list at end of DataTypeImpl
     *
     */
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DEFAULT:
                {
                    Integer typeCode = typeMetadata.get_type_code();
            
                    // Try Plain Datatype lookup
                    if (typeCode == null) {
                        typeCode = DataTypesUtil.getJdbcTypeCode(getPlainDataTypeString());
                    } else {
                        return typeCode;
                    }
            
                    // Try Base Datatype lookup
                    if (typeCode == null) {
                        typeCode = DataTypesUtil.getJdbcTypeCode(getBaseDataTypeString());
                    } else {
                        return typeCode;
                    }
                    
                    // Report as OTHER
                    if (typeCode == null) {
                        return java.sql.Types.OTHER;
                    } else {
                        return typeCode;
                    }
                }
            default:
                return customSqlDataType(ValueType.DEFAULT);
        }
    }

    public final int getSqlDataType(ValueType valueType) {
        return customSqlDataType(valueType);
    }
    
    /**
     * Used for some user display and specifically for test value look-up
     * table key I think
     * @return name representing the data type
     */
    public final String getDataTypeString() {
        return dataTypeString;
    }

    public final String initDataTypeString() {
        return DataTypesUtil.reformatDataTypeString(customDataTypeString());
    }

    protected String customDataTypeString() {
        String dataTypeString = typeMetadata.get_data_type();
        
        if (dataTypeString == null) {
            dataTypeString = getUserDataTypeString();
            
            if (dataTypeString == null) {
                // Get metadata
                Integer _data_precision = typeMetadata.get_data_precision();
                Integer _data_scale = typeMetadata.get_data_scale();
                
                // Get datatype
                StringBuffer buffer = new StringBuffer(getPlainDataTypeString());
        
                // Qualify datatype
                if (_data_precision != null && _data_scale != null) {
                    buffer.append("(").append(_data_precision).append(",").append(_data_scale).append(")");
                } else if (_data_precision != null) {
                    buffer.append("(").append(_data_precision).append(")");
                }
        
                dataTypeString = buffer.toString();
            }
        }
        
        return dataTypeString;
    }

    public final String getPlainDataTypeString() {
        return DataTypesUtil.stripDataTypeStringConstraints(customPlainDataTypeString());
    }
    
    protected String customPlainDataTypeString() {
        String plainDataTypeString = typeMetadata.get_data_type();
        
        if (plainDataTypeString == null) {
             plainDataTypeString = getUserDataTypeString();
            
            if (plainDataTypeString == null) {
                plainDataTypeString = typeMetadata.get_base_type();
            }
        } 
        
        return plainDataTypeString;
    }
    
    public final String getUserDataTypeString() {
        return customUserDataTypeString();
    }

    public static String getUserDataTypeString(TypeMetadata typeMetadata) {
        // Get metadata
        String _type_owner = typeMetadata.get_type_owner();
        String _type_name = typeMetadata.get_type_name();
        String _type_subname = typeMetadata.get_type_subname();

        StringBuilder buffer = new StringBuilder();

        appendIdentifier(buffer, _type_owner);
        appendIdentifier(buffer, _type_name);
        appendIdentifier(buffer, _type_subname);

        return (buffer.length() == 0) ? null : buffer.toString();
    }

    protected String customUserDataTypeString() {
        return DataTypeImpl.getUserDataTypeString(typeMetadata);
    }

    public final String getBaseDataTypeString() {
        return DataTypesUtil.stripDataTypeStringConstraints(customBaseDataTypeString());
    }

    protected String customBaseDataTypeString() {
        String baseDataTypeString = typeMetadata.get_base_type();
        
        if (baseDataTypeString == null) {
            baseDataTypeString = typeMetadata.get_data_type();
        }
        
        return baseDataTypeString;
    }

    public final String getConstrainedDataTypeString() {
        return customConstrainedDataTypeString();
    }

    protected String customConstrainedDataTypeString() {
        return getDataTypeString();
    }

    public final String getUnconstrainedDataTypeString() {
        return customUnconstrainedDataTypeString();
    }

    protected String customUnconstrainedDataTypeString() {
        return getDataTypeString();
    }

    /**
     * Called before the cell editor stops editing
     * @return null if the edited value is valid. If invalid return the message to be displayed
     */
    public String validateValue(Object object) {
        // Basic check to make sure set will work
        String msg = null;

        try {
            if (!(object instanceof DataValue)) {
                getDataValue(object);
            }
        } catch (DataTypeIllegalArgumentException e) {
            msg = e.getLocalizedMessage();
        }

        return msg;
    }

    /*
	 * Used for Datatype such as Boolean
	 */

    public boolean isEnumeration() {
        return false;
    }

    /*
	 * Used for Datatype such as Boolean
	 */

    public List<DataValue> getEnumerationValues() {
        return null;
    }

    public final boolean isSupported() {
        if (customSupported()) {
            Iterable<NamedValue<DataType>> components = getTypeComponents();
            
            if (components != null) {
                for (NamedValue<DataType> namedType: components) {
                    DataType componentType = namedType.getValue();
                    
                    if (!componentType.isSupported()) {
                        return false;
                    }
                }
            }
            
            return true;
        }
        
        return false;
    }

    protected boolean customSupported() {
        return true;
    }

    public StructureType getStructureType() {
        if (getTypeComponents() != null && getTypeComponents().size() > 0) {
             return StructureType.TABLE;
         } else {
            return StructureType.PRIMITIVE;
        }
    }
    
    public final boolean requiresConnection(){
        if (getTypeComponents() != null) {
            for (NamedValue<DataType> typecomponent: getTypeComponents()) {
                if (typecomponent.getValue().requiresConnection()) {
                    return true;
                }
            }
        }
        return customRequiresConnection();
    }
        
    public DataTypeFactory getDataTypeFactory() {
        return dataTypeContext.getDataTypeFactory();
    }

    public DataTypeConnectionProvider getDataTypeConnectionProvider() {
        return dataTypeContext.getDataTypeConnectionProvider();
    }

    protected boolean customRequiresConnection() {
        return false;
    }

    public Object startDataValue(String name, boolean isNull) {
        if (isNull) {
            return null;
        } else {
            return new StringBuilder();
        }
    }

    public void bodyDataValue(NamedValue value, char[] ch, int start, int length) {
        StringBuilder sb = (StringBuilder)value.getValue();
        sb.append(ch, start, length);
    }

    public void bodyDataValue(NamedValue value, String text) {
        char[] chars = text.toCharArray();
        bodyDataValue(value, chars, 0, chars.length);
    }

    public void bodyDataValue(NamedValue value, DataValue childValue) {
        throw new DataTypeMarshallingException(value.getName(), this);
    }

    public DataValue endDataValue(NamedValue value) {
        StringBuilder sb = (StringBuilder)value.getValue();
        return getDataValue((sb != null) ? sb.toString() : null);
    }
    
    protected static StringBuilder appendIdentifier(StringBuilder target, String source) {
        if (source != null) {
            if (target.length() == 0) {
                target.append(source);
            } else {
                target.append(".").append(source);
            }
        }

        return target;
    }

    @Override
    public String toString() {
        try {
            return getPlainDataTypeString();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                       e.getStackTrace()[0].toString(), e);

            return (super.toString());
        }
    }

    /*	

	static {
        // Dump raw field data to stdout to use in creating the list below
        java.lang.reflect.Field[] fields = null;
        System.out.println("// oracle.jdbc.OracleTypes");
        fields = oracle.jdbc.OracleTypes.class.getFields();
        for (java.lang.reflect.Field field : fields) {
            try {
            	System.out.println(String.format(
            			"// %1$8s  %2$-50s"
            			, field.get(null)
            			, field.getName()
            			));
            } catch (IllegalAccessException e) {
            }
	    }
        System.out.println("// java.sql.Types");
        fields = java.sql.Types.class.getFields();
        for (java.lang.reflect.Field field : fields) {
            try {
            	System.out.println(String.format(
            			"// %1$8s  %2$-50s"
            			, field.get(null)
            			, field.getName()
            			));
            } catch (IllegalAccessException e) {
            }
	    }
	}
*/

    // oracle.jdbc.OracleTypes + java.sql.Types
    // OracleTypes BUILD_DATE Wed_Jul_25_08:30:21_PDT_2007
    //
    // * = same as java.sql.Types
    // # = Only in java.sql.Types
    //
    // *   2003  ARRAY
    //      -13  BFILE
    // *     -5  BIGINT
    // *     -2  BINARY
    //      101  BINARY_DOUBLE
    //      100  BINARY_FLOAT
    // *     -7  BIT
    // *   2004  BLOB
    // *     16  BOOLEAN
    // *      1  CHAR
    // *   2005  CLOB
    //      -10  CURSOR
    // *     70  DATALINK
    // *     91  DATE
    // *      3  DECIMAL
    // #   2001  DISTINCT
    // *      8  DOUBLE
    //      999  FIXED_CHAR
    // *      6  FLOAT
    // *      4  INTEGER
    //     -104  INTERVALDS
    //     -103  INTERVALYM
    // *   2000  JAVA_OBJECT
    //     2008  JAVA_STRUCT
    // #    -16  LONGNVARCHAR
    // *     -4  LONGVARBINARY
    // *     -1  LONGVARCHAR
    // #    -15  NCHAR
    // #   2011  NCLOB
    // *      0  NULL
    //        2  NUMBER
    // *      2  NUMERIC
    // #     -9  NVARCHAR
    //     2007  OPAQUE
    // *   1111  OTHER
    //      -14  PLSQL_INDEX_TABLE
    //       -2  RAW
    // *      7  REAL
    // *   2006  REF
    // *     -8  ROWID
    // *      5  SMALLINT
    // #   2009  SQLXML
    // *   2002  STRUCT
    // *     92  TIME
    // *     93  TIMESTAMP
    //     -102  TIMESTAMPLTZ
    //     -100  TIMESTAMPNS
    //     -101  TIMESTAMPTZ
    // *     -6  TINYINT
    // *     -3  VARBINARY
    // *     12  VARCHAR
}
