/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeExtensionObject;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension.ExtensionAccess;
import oracle.dbtools.raptor.datatypes.DataTypeProvider;
import oracle.dbtools.raptor.datatypes.TypeMetadata;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=AbstractDataTypeProviderImpl.java">Barry McGillin</a>
 *
 */
public abstract class DataTypeProviderDelegate extends DataTypeExtensionObject implements DataTypeProvider {
    protected abstract static class Builder extends DataTypeExtensionObject.Builder implements DataTypeProvider.Builder {
        private DataTypeProvider superDataTypeProvider;
        private DataTypeProvider delegateDataTypeProvider;
        
        protected Builder() {
            super();
            this.superDataTypeProvider = null;
            this.delegateDataTypeProvider = null;
        }
       
        @Override
        public Builder setExtensionAccess(ExtensionAccess extensionAccess) {
            super.setExtensionAccess(extensionAccess);
            return this;
        }

        public Builder setSuperDataTypeProvider(DataTypeProvider superDataTypeProvider) {
            this.superDataTypeProvider = superDataTypeProvider;
            return this;
        }
        
        public DataTypeProvider getSuperDataTypeProvider() {
            return this.superDataTypeProvider;
        }

        public Builder setDelegateDataTypeProvider(DataTypeProvider delegateDataTypeProvider) {
            this.delegateDataTypeProvider = delegateDataTypeProvider;
            return this;
        }
        
        public DataTypeProvider getDelegateDataTypeProvider() {
            return this.delegateDataTypeProvider;
        }
    }

    private DataTypeProvider delegate;
    private DataTypeProvider noxt;

    public DataTypeProviderDelegate(ExtensionAccess extensionAccess, DataTypeProvider delegate, DataTypeProvider next) {
        super(extensionAccess);
        this.delegate = delegate;
        this.noxt = next;
    }
    
    public DataTypeProviderDelegate(ExtensionAccess extensionAccess, DataTypeProvider delegate) {
        this(extensionAccess, delegate, null);
    }
    
    public final DataType getDataType(DataTypeContext context, TypeMetadata typeMetadata) {
        DataType datatype = delegate.getDataType(context, typeMetadata);
        
        if (datatype == null && noxt != null) {
            datatype = noxt.getDataType(context, typeMetadata);
        }
        
        return (datatype == null) ? new UnsupportedDataTypeImpl(context, typeMetadata) : datatype;
    }
    
    public final Set<String> getSupportedTypes() {
        Set<String> chainTypes = (noxt != null) ? noxt.getSupportedTypes() : new TreeSet<String>();
        Set<String> delegateTypes = delegate.getSupportedTypes();
        if (delegateTypes != null) {
            chainTypes.addAll(delegateTypes);
        }
        return chainTypes;
    }

    public final Set<Integer> getSupportedSQLTypes() {
        Set<Integer> chainTypes = (noxt != null) ? noxt.getSupportedSQLTypes() : new TreeSet<Integer>();
        Set<Integer> delegateTypes = delegate.getSupportedSQLTypes();
        if (delegateTypes != null) {
            chainTypes.addAll(delegateTypes);
        }
        return chainTypes;
    }

    public final Map<String, TypeMetadata> getSupportedTypeMetadata() {
        Map<String, TypeMetadata> chainTypeMetadata = (noxt != null) ? noxt.getSupportedTypeMetadata() : new HashMap<String, TypeMetadata>();
        Map<String, TypeMetadata> delegateTypeMetadata = delegate.getSupportedTypeMetadata();
        if (delegateTypeMetadata != null) {
            chainTypeMetadata.putAll(delegateTypeMetadata);
        }
        return chainTypeMetadata;
    }

    public final TypeMetadata getTypeMetadata(String datatype){
        TypeMetadata typeMetaData = delegate.getTypeMetadata(datatype);
        return (typeMetaData == null && noxt != null) ? noxt.getTypeMetadata(datatype) : typeMetaData;
    }

    public final TypeMetadata getTypeMetadata(Integer oracleType) {
        TypeMetadata typeMetaData = delegate.getTypeMetadata(oracleType);
        return (typeMetaData == null && noxt != null) ? noxt.getTypeMetadata(oracleType) : typeMetaData;
    }
}
