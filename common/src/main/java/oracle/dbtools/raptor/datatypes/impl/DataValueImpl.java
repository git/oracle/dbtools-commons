/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.impl;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeException;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.DataTypeMarshallingException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.values.NamedDataValue;
import oracle.dbtools.raptor.utils.DataTypesUtil;

import org.xml.sax.SAXException;

/**
 * Implements a Data Value.
 * 
 * A value has an internal value, a flag if it is considered null 
 * and a data type
 */
public class DataValueImpl implements DataValue {
    private class DataValueAccessor implements DataValueInternal {
        private DataValueAccessor() {};
        
        public Object getInternalValue() {
            return internalValue;
        }
        
        public boolean isNull() {
            return valueIsNull;
        }
    }
    private final DataValueAccessor accessor = new DataValueAccessor();

    private final Object internalValue;
    private final boolean valueIsNull;
    protected final DataTypeImpl datatype;
    
    protected DataValueImpl(DataTypeImpl datatype) {
        this(datatype, null);
    }

    protected DataValueImpl(DataTypeImpl datatype, Object value) {
        this.datatype = datatype;
        try {
            // Get Internal Value
            this.internalValue = datatype.getInternalValue(value);

            // Check valid as Typed Value
            datatype.getTypedValue(accessor, ValueType.DEFAULT);

            // Save null status
            valueIsNull = DataTypesUtil.isNull(this.internalValue);
        } catch (NullPointerException npe) {
            throw npe;
        } catch (DataTypeIllegalArgumentException dtiae) {
            throw dtiae;
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, e);
        }
    }

    protected DataValueImpl(DataTypeImpl datatype, Collection<DataValue> internalValue) {
        this.datatype = datatype;
        try {
            // Get Internal Value
            this.internalValue = internalValue;

            // Check valid as Typed Value
            datatype.getTypedValue(accessor, ValueType.DEFAULT);

            // Save null status
            valueIsNull = DataTypesUtil.isNull(this.internalValue);
        } catch (NullPointerException npe) {
            throw npe;
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, e);
        }
    }

    public final String getName() {
        return null;
    }

    public final DataType getDataType() {
        return datatype;
    }

    public DataValue getBindingDataValue() {
        return this;
    }

    /**
     * @return this data type's string representation of the value
     * NOTE: getStringValue(null) == "null"
     */
    public final StringValue getStringValue() {
        return getStringValue(-1);
    }
    
    public final StringValue getStringValue(StringType stringType) {
        return getStringValue(stringType, -1);
    }
    
    public final StringValue getStringValue(int maxLen) {
        return getStringValue(StringType.DEFAULT, maxLen);
    }

    /**
     * @param stringType
     * @param maxLen
     * @return
     */
    public final StringValue getStringValue(StringType stringType, int maxLen) {
        try {
            return customStringValue(stringType, maxLen);
        } catch (NullPointerException npe) {
            throw npe;
        } catch (DataTypeException de) {
            throw de;
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, e);
        }
    }

    protected StringValue customStringValue(StringType stringType, int maxLen) {
        return datatype.getStringValue(accessor, stringType, maxLen);
    }

    public final StringValue getStringValue(DataTypeConnectionProvider connectionProvider, StringType stringType) {
        return getStringValue(connectionProvider, stringType, -1);
    }
                                                                                                                  
    public final StringValue getStringValue(DataTypeConnectionProvider connectionProvider, StringType stringType, int maxLen) {
        try {
            return customStringValue(connectionProvider, stringType, maxLen);
        } catch (NullPointerException npe) {
            throw npe;
        } catch (DataTypeException de) {
            throw de;
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, e);
        }
    }
    
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, StringType stringType, int maxLen) {
        return datatype.getStringValue(connectionProvider, accessor, stringType, maxLen);
    }

    /**
     * @return an object of this data type's internal type initialized to value
     * NOTE: getTypedValue([null|""|"null"]) == null
     */
    public final Object getTypedValue() {
        return datatype.getTypedValue(accessor, ValueType.DEFAULT);
    }

    public final Object getTypedValue(ValueType valueType) {
        return datatype.getTypedValue(accessor, valueType);
    }

    public final Object getTypedValue(Object target) {
        return datatype.getTypedValue(accessor, target);
    }

    public final Object getTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        return getTypedValue(connectionProvider, valueType, null);
    }

    public final Object getTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target) {
        return datatype.getTypedValue(connectionProvider, accessor, valueType, target);
    }

    @Override
    public String toString() {
        try {
            return (valueIsNull) ? null : getStringValue(-1).toString();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                       e.getStackTrace()[0].toString(), e);

            return (internalValue == null) ? null : internalValue.toString();
        }
    }

    public final boolean isNull() {
        return valueIsNull;
    }

    @Override
    public final boolean equals(Object obj) {
        // Implementation based on Object superclass
        boolean isEquals = (this == obj);

        if (!isEquals) {
            if (obj instanceof NamedDataValue) {
                isEquals = obj.equals(this);
            } else if (obj instanceof DataValueImpl) {
                DataValueImpl otherDataValue = (DataValueImpl)obj;
    
                isEquals =
                        ((this.getClass() == otherDataValue.getClass()) && ((isNull() && otherDataValue.isNull()) ||
                                                                            (!isNull() &&
                                                                             !otherDataValue.isNull() &&
                                                                             customEquals(obj))));
            }
        }

        return isEquals;
    }

    protected boolean customEquals(Object obj) {
        // Name is excluded
        if (obj instanceof DataValueImpl) {
            DataValueImpl objDataValue = (DataValueImpl)obj;

            return (areEquals(datatype, objDataValue.datatype) &&
                    datatype.equals(internalValue, objDataValue.internalValue));
        } else {
            return false;
        }
    }

    private static boolean areEquals(Object obj1, Object obj2) {
        return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
    }

    public final void marshal(DataValueMarshalHandler hd, String name) {
        try {
            hd.startDataValue(getDataType(), name, isNull());
            
            if (!isNull()) {
                customMarshal(hd, name);
            }
        } catch (NullPointerException npe) {
            throw npe;
        } catch (Exception e) {
            throw new DataTypeMarshallingException(this, e);
        } finally {
            try {
                hd.endDataValue(getDataType(), name, isNull());
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                           e.getStackTrace()[0].toString(), e);
                
            }
        }
    }
    
    public final void marshal(DataValueMarshalHandler hd) {
        hd.marshal(this, getName());
    }
    
    protected void customMarshal(DataValueMarshalHandler hd, String name) throws SAXException {
        datatype.marshal(accessor, hd, name);
    }

    public boolean isSupported() {
        return true;
    }

    public final List<DataValue> getComponents() {
        return customComponents();
    }

    protected List<DataValue> customComponents() {
        return datatype.getComponents(accessor);
    }
}
