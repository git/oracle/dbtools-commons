/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.marshal;

import java.util.Stack;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeMarshallingException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.values.NamedDataValue;
/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=DataValueUnmarshalHandler.java">Barry McGillin</a>
 *
 */
public class DataValueUnmarshalHandler {
    private final Stack<DataType> datatypes;
    private final Stack<NamedValue> internalValues;
    
    private final DataType datatype;
    private final boolean useName;
    
    private DataValue datavalue;
    private String name;
   
    public DataValueUnmarshalHandler(DataType datatype, String name) {
        this(datatype, name, true);
    }

    public DataValueUnmarshalHandler(DataType datatype) {
        this(datatype, null, false);
    }

    private DataValueUnmarshalHandler(DataType datatype, String name, boolean useName) {
        datatypes = new Stack<DataType>();
        internalValues = new Stack<NamedValue>();
        this.datatype = datatype;
        this.datavalue = null;
        this.name = name;
        this.useName = useName;
    }
    
    
    public void startDataValue(String name, boolean isNull) {
        DataType datatype = null;
        String focusName = name;
        
        if (internalValues.size() > 0) {
            // Find Child DataType
            NamedValue<DataType>  typeComponent = lookupComponentType(datatypes.peek(), focusName);
            
            if (typeComponent != null) {
                focusName =  typeComponent.getName();
                datatype = typeComponent.getValue();
                customPushDataType(datatype);
            }
        } else {
            // Either use or initialize the name
            if (useName) {
                focusName = this.name;
            } else {
                this.name = focusName;
            }
          
            // Use Initial DataType
            datatype = this.datatype;
            customPushDataType(datatype);
            
        }
        
        if (datatype != null) {
            // Start Marshalling for this DataType
            internalValues.push(new NamedValue(focusName, datatype.startDataValue(focusName, isNull)));
        } else {
            // No DataType found
            focusName = (internalValues.size() == 0) ? focusName : internalValues.peek().getName();
            DataType focusDataType = datatypes.peek();
            throw new DataTypeMarshallingException(focusName, focusDataType);
        }
    }
    
    protected void customPushDataType(DataType datatype) {
        datatypes.push(datatype);
    }
    
    protected DataType customPopDataType() {
        return datatypes.pop();
    }
    
    public void bodyDataValue(char ch[], int start, int length) {
        datatypes.peek().bodyDataValue(internalValues.peek(), ch, start,length);
    }
    
    public void bodyDataValue(String text) {
        datatypes.peek().bodyDataValue(internalValues.peek(), text);
    }
    
    public void endDataValue() {
        String name = internalValues.peek().getName();
        DataValue datavalue = NamedDataValue.getNamedDataValue(name, customPopDataType().endDataValue(internalValues.pop()));
        
        if (datatypes.size() > 0) {
                datatypes.peek().bodyDataValue(internalValues.peek(), datavalue);
        } else {
            this.datavalue = datavalue;
        }
    }
    
    public DataValue getDataValue() {
        return this.datavalue;
    }
    
    public DataType getDataType() {
        return this.datatype;
    }
    
    public String getName() {
        return this.name;
    }
    
    protected NamedValue<DataType> lookupComponentType(DataType datatype, String focusName) {
        Iterable<NamedValue<DataType>> components = datatype.getTypeComponents();

        for (NamedValue<DataType> typeComponent : components) {
            String typeName = typeComponent.getName();
            
            if (typeName == null || 
            	typeName == focusName || 
            	(typeName != null &&
            		("ELEMENT".equals(typeName) || (focusName != null && typeName.equals(focusName))))) {
                return typeComponent;
            }
        }
        
        return null;
    }
}
