/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.metadata;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import oracle.dbtools.raptor.datatypes.BindingMode;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.TypeMetadata.Attribute;
import oracle.dbtools.raptor.utils.DataTypesUtil;

public final class ArgMetadata extends NamedValue<Map<TypeMetadata.Attribute, Object>> {
    public final static class ArgIdentifier implements Cloneable {
        ArgIdentifier(int position, Integer level) {
            this.position = position;
            this.level = level;
        }
    
        @Override
        public Object clone() {
            return new ArgIdentifier(position, level);
        }
    
        @Override
        public boolean equals(Object obj) {
            if (obj == this) return true;
            if (obj instanceof ArgIdentifier) {
                ArgIdentifier other = (ArgIdentifier)obj;
                return DataTypesUtil.areEqual(position, other.position) &&
                       DataTypesUtil.areEqual(level, other.level);
            } else {
                return false;
            }
        }
    
        @Override
        public int hashCode() {
            int hash = 23;
            hash = 31 * hash + (position == null ? 0 : position.hashCode());
            hash = 31 * hash + (level == null ? 0 : level.hashCode());
            return hash;
        }
        
        public Integer getPosition() {
            return position;
        }
            
        public Integer getLevel() {
            return level;
        }

        boolean matches(ArgIdentifier identifier) {
            return DataTypesUtil.areEqual(this, identifier);
        }
        
        private final Integer position;
        private final Integer level;
    }
    
    public ArgMetadata(ArgIdentifier identifier,
                       String argName, 
                       String inOut,
                       Map<Attribute, Object> typeAttributes,
                       String defaulted, String defaultValue, Integer defaultLength,
                       List<ArgMetadata> components) {
        super(argName, typeAttributes);
        
        this.identifier = identifier;
        
        this.components = new TreeMap<Integer,ArgMetadata>();
        
        if (argName == null && identifier.getPosition() != null && identifier.getPosition().intValue() == 0) {
            this.mode = BindingMode.RETURN;
        } else {
            this.mode = (inOut != null) ? BindingMode.getBindingMode(inOut) : null;
        }
        
        this.defaulted = "Y".equals(defaulted);
        this.defaultValue = defaultValue;
        this.defaultLength = defaultLength;
        
        this.minCompPosition = null;
        this.maxCompPosition = null;

        // Add Components
        if (components != null) {
            for (ArgMetadata component : components) {
                addComponent(component);
            }
        }
    }
    
    public ArgMetadata(ArgIdentifier identifier,
                       String argName, 
                       Map<Attribute, Object> typeAttributes,
                       String defaulted, String defaultValue, Integer defaultLength,
                       List<ArgMetadata> components) {
        this(identifier, argName, null, typeAttributes, defaulted, defaultValue, defaultLength, components);
    }

    public ArgMetadata(ArgIdentifier identifier,
                       String argName, 
                       String inOut,
                       Map<Attribute, Object> typeAttributes,
                       String defaulted, String defaultValue, Integer defaultLength) {
        this(identifier, argName, inOut, typeAttributes, defaulted, defaultValue, defaultLength, null);
    }
    
    public ArgMetadata(ArgIdentifier identifier,
                       String argName, 
                       Map<Attribute, Object> typeAttributes,
                       String defaulted, String defaultValue, Integer defaultLength) {
        this(identifier, argName, null, typeAttributes, defaulted, defaultValue, defaultLength);
    }
    
    public ArgMetadata(ArgIdentifier identifier,
                       String argName, 
                       Map<Attribute, Object> typeAttributes) {
        this(identifier, argName, null, typeAttributes, null, null, null);
    }
    
    public ArgIdentifier getIdentifier() {
        return identifier;
    }
    
    public boolean isDefaulted() {
        return defaulted;
    }
    
    public String getDefaultValue() {
        return defaultValue;
    }
    
    public Integer getDefaultLength() {
        return defaultLength;
    }

    public BindingMode getMode() {
        return mode;
    }
        
    public ArgMetadata getComponent(int position) {
        return components.get(position);
    }
    
    boolean addComponent(ArgMetadata component) {
        if (component != null) {
            Integer position = component.identifier.getPosition();
            
            if (position == null) {
                position = components.size() + 1;
            } else if (position.intValue() < 0) {
                throw new IllegalArgumentException();
            }
            
            ArgMetadata oldComponent = components.put(position, component);
            
            if (oldComponent != null && oldComponent != component) {
                // Put old argument back
                components.put(oldComponent.identifier.getPosition(), oldComponent);
                throw new IllegalArgumentException();
            }
            
            if (minCompPosition == null || minCompPosition.intValue() > position.intValue()) minCompPosition = position;
            if (maxCompPosition == null || maxCompPosition.intValue() < position.intValue()) maxCompPosition = position;
            
            return true;
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    public int countComponents() {
        return components.size();
    }
    
    public boolean hasComponents() {
        return !components.isEmpty();
    }
    
    public Iterator<ArgMetadata> componentIterator() {
        return components.values().iterator();
    }
    
    public NamedValue<TypeMetadata> extractTypeMetadata(DataTypeFactory dataTypeFactory) {
        // Replicate attribute map
        Map<TypeMetadata.Attribute, Object> typeAttributes = new HashMap<TypeMetadata.Attribute, Object>(getValue());
        
        // Extract sub components
        List<NamedValue<TypeMetadata>> typeComponents = new LinkedList<NamedValue<TypeMetadata>>();
        for (ArgMetadata argMetadata: components.values()) {
            typeComponents.add(argMetadata.extractTypeMetadata(dataTypeFactory));
        }
        
        // Added sub component, if any
        if (typeComponents.size() != 0) {
            typeAttributes.put(TypeMetadata.Attribute.TYPE_COMPONENTS, typeComponents);
        }
        
        // Create TypeMetaData
        TypeMetadata typeMetaData = dataTypeFactory.getTypeMetadata(typeAttributes);
        
        return new NamedValue<TypeMetadata>(getName(), typeMetaData);
    }
    
    boolean identifiedBy(ArgIdentifier identifier) {
        return this.identifier.matches(identifier);
    }
    
    public Integer getMinCompPosition() {
        return minCompPosition;
    }

    public Integer getMaxCompPosition() {
        return maxCompPosition;
    }

    final TreeMap<Integer, ArgMetadata> components;
    final BindingMode mode;
    final boolean defaulted;
    final String defaultValue;
    final Integer defaultLength;
    
    final ArgIdentifier identifier;

    Integer minCompPosition;
    Integer maxCompPosition;
}
