/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.metadata;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.TypeMetadata.Attribute;
import oracle.dbtools.raptor.datatypes.metadata.ArgMetadata.ArgIdentifier;
import oracle.dbtools.raptor.datatypes.metadata.ProgMetadata.ProgIdentifier;
import oracle.dbtools.raptor.datatypes.metadata.SigMetadata.SigIdentifier;
import oracle.dbtools.raptor.utils.DataTypesUtil;

public class MetadataLoader {
    final Connection conn;

    public MetadataLoader(Connection conn) {
        this.conn = conn;;
    }
    
    /*
     * Signature Arguments
     */
    public void loadSignatureArguments(Deque<LoadedArgMetadata> loadStack, String _objName, String _objOwner, String _objCall, Integer _objOverload) throws SQLException {
        HashMap<String, String> binds = new HashMap<String, String>();
        ResultSet rset = null;
        
        try {
            DBUtil dbUtil = DBUtil.getInstance(conn);
            dbUtil.setRaiseError(false);
            String query = MetadataUtil.getQuery(conn, "loadArguments"); //$NON-NLS-1$
            binds.put("OBJECT_OWNER", _objOwner); //$NON-NLS-1$
            binds.put("OBJECT_NAME", _objName); //$NON-NLS-1$
            binds.put("OBJECT_CALL", _objCall); //$NON-NLS-1$
            binds.put("OVERLOAD", (_objOverload != null) ? _objOverload.toString() : null); //$NON-NLS-1$
            rset = dbUtil.executeOracleQuery(query, binds);
            if (rset != null) {
                while (rset.next()) {
                    final LoadedArgMetadata loadedArgMetadata = new LoadedArgMetadata(
                                  loadArgMetadata(rset),
                                  loadSigIdentifier(rset),
                                  loadProgIdentifier(rset));
                    
                    loadStack.push(loadedArgMetadata);
                }
            }
        } finally {
            DBUtil.closeResultSet(rset);
        }
    }

    private ProgIdentifier loadProgIdentifier(ResultSet rset) throws SQLException {
        return new ProgIdentifier(rset.getString("OWNER"), rset.getString("OBJECT_NAME"), rset.getString("PACKAGE_NAME"));
    }
    
    private SigIdentifier loadSigIdentifier(ResultSet rset) throws SQLException {
        return new SigIdentifier(getIntegerResult(rset, "OVERLOAD"), getIntegerResult(rset, "OBJECT_ID"), getIntegerResult(rset, "SUBPROGRAM_ID"));
    }
    
    private ArgMetadata loadArgMetadata(ResultSet rset) throws SQLException {
        ArgMetadata argMetadata;
        
        String dataType = rset.getString("DATA_TYPE");
        if (dataType != null) { // packaged procedures without parameters may have a spurious null return value (probably to remove ambiguity in public views).
            String plsType = rset.getString("PLS_TYPE");
            String argName = rset.getString("ARGUMENT_NAME"); //$NON-NLS-1$
            
            int position = rset.getInt("POSITION"); //$NON-NLS-1$
            int level = rset.getInt("DATA_LEVEL"); //$NON-NLS-1$
            
            String inOut = rset.getString("IN_OUT"); //$NON-NLS-1$
            String defaulted = rset.getString("DEFAULTED"); //$NON-NLS-1$
            String defaultValue = rset.getString("DEFAULT_VALUE"); //$NON-NLS-1$
            Integer defaultLen = getIntegerResult(rset, "DEFAULT_LENGTH"); //$NON-NLS-1$
            
            // Detect textual nature of datatype
            String charSetName = rset.getString("CHARACTER_SET_NAME");
            String  charUsed = rset.getString("CHAR_USED");
            boolean isCharUnit = "C".equals(charUsed);
            boolean isTextual = (charSetName != null) || isCharUnit;
            
            // Set Char Length for textual datatypes
            Integer charLen;
            if (!isTextual) {
                charLen = null;
                charUsed = null;
            } else {
                charLen = getIntegerResult(rset, "CHAR_LENGTH");
                if (charLen == null) {
                    charLen = 0;
                }
            }
            Map<Attribute, Object> attributeMap = new HashMap<Attribute, Object>();
            
            // Populate attributeMap
            addAttribute(attributeMap, Attribute.DATA_TYPE, DataTypesUtil.reformatDataTypeString(dataType)); //$NON-NLS-1$
            addAttribute(attributeMap, Attribute.PLS_TYPE, DataTypesUtil.reformatDataTypeString(plsType));
            addAttribute(attributeMap, Attribute.DATA_LENGTH, getIntegerResult(rset, "DATA_LENGTH")); //$NON-NLS-1$
            addAttribute(attributeMap, Attribute.DATA_PRECISION, getIntegerResult(rset, "DATA_PRECISION")); //$NON-NLS-1$
            addAttribute(attributeMap, Attribute.DATA_SCALE, getIntegerResult(rset, "DATA_SCALE")); //$NON-NLS-1$
            addAttribute(attributeMap, Attribute.DATA_RADIX, getIntegerResult(rset, "RADIX")); //$NON-NLS-1$
            addAttribute(attributeMap, Attribute.TYPE_OWNER, rset.getString("TYPE_OWNER")); //$NON-NLS-1$
            addAttribute(attributeMap, Attribute.TYPE_NAME, rset.getString("TYPE_NAME")); //$NON-NLS-1$
            addAttribute(attributeMap, Attribute.TYPE_SUBNAME, rset.getString("TYPE_SUBNAME")); //$NON-NLS-1$
            addAttribute(attributeMap, Attribute.TYPE_LINK, rset.getString("TYPE_LINK")); //$NON-NLS-1$
            addAttribute(attributeMap, Attribute.CHAR_SET_NAME, charSetName);
            addAttribute(attributeMap, Attribute.CHAR_LENGTH, charLen);
            addAttribute(attributeMap, Attribute.CHAR_USED, charUsed);
            
            ArgIdentifier argIdentifier = new ArgIdentifier(position, level);
        
            argMetadata = new ArgMetadata(argIdentifier, argName, inOut, attributeMap, defaulted, defaultValue, defaultLen);
        } else {
            argMetadata = null;
        }
        
        return argMetadata;
    }
    
    public void loadSQLObject(Deque<ArgMetadata> loadStack, String typeOwner, String typeName, int atLevel) throws SQLException {
        loadSQLTypeMetadata("loadSQLObject", loadStack, typeOwner, typeName, atLevel);
    }

    public void loadSQLCollection(Deque<ArgMetadata> loadStack, String typeOwner, String typeName, int atLevel) throws SQLException {
        loadSQLTypeMetadata("loadSQLCollection", loadStack, typeOwner, typeName, atLevel);
    }

    public void loadSQLType(Map<TypeMetadata.Attribute, Object> attributes, String typeOwner, String typeName) throws SQLException {
        HashMap<String, String> binds = new HashMap<String, String>();
        ResultSet rset = null;
        
        try {
            DBUtil dbUtil = DBUtil.getInstance(conn);
            dbUtil.setRaiseError(false);
            String query = MetadataUtil.getQuery(conn, "loadSQLType"); //$NON-NLS-1$
            if (query != null) {
                binds.put("TYPE_OWNER", typeOwner); //$NON-NLS-1$
                binds.put("TYPE_NAME", typeName); //$NON-NLS-1$
                rset = dbUtil.executeOracleQuery(query, binds);
                if (rset != null) {
                    while (rset.next()) {
                        attributes.put(Attribute.DATA_TYPE,rset.getString("TYPECODE"));
                    }
                }
            }
        } finally {
            DBUtil.closeResultSet(rset);
        }
    }

    public void followSQLSynonym(Map<TypeMetadata.Attribute, Object> attributes, String typeOwner, String typeName) throws SQLException {
        HashMap<String, String> binds = new HashMap<String, String>();
        ResultSet rset = null;
        
        try {
            DBUtil dbUtil = DBUtil.getInstance(conn);
            dbUtil.setRaiseError(false);
            String query = MetadataUtil.getQuery(conn, "followSQLSynonym"); //$NON-NLS-1$
            if (query != null) {
                binds.put("TYPE_OWNER", typeOwner); //$NON-NLS-1$
                binds.put("TYPE_NAME", typeName); //$NON-NLS-1$
                rset = dbUtil.executeOracleQuery(query, binds);
                if (rset != null) {
                    while (rset.next()) {
                        attributes.put(Attribute.TYPE_OWNER,rset.getString("TABLE_OWNER"));
                        attributes.put(Attribute.TYPE_NAME,rset.getString("TABLE_NAME"));
                        attributes.put(Attribute.TYPE_LINK,rset.getString("DB_LINK"));
                    }
                }
            }
        } finally {
            DBUtil.closeResultSet(rset);
        }
    }

    public void loadSQLAttributes(Deque<ArgMetadata> loadStack, String typeOwner, String typeName, int atLevel) throws SQLException {
        loadSQLTypeMetadata("loadSQLAttributes", loadStack, typeOwner, typeName, atLevel);
    }

    public void loadSQLTypeMetadata(String queryKey, Deque<ArgMetadata> loadStack, String typeOwner, String typeName, int atLevel) throws SQLException {
        HashMap<String, String> binds = new HashMap<String, String>();
        ResultSet rset = null;
        
        try {
            DBUtil dbUtil = DBUtil.getInstance(conn);
            dbUtil.setRaiseError(false);
            String query = MetadataUtil.getQuery(conn, queryKey); //$NON-NLS-1$
            if (query != null) {
                binds.put("TYPE_OWNER", typeOwner); //$NON-NLS-1$
                binds.put("TYPE_NAME", typeName); //$NON-NLS-1$
                rset = dbUtil.executeOracleQuery(query, binds);
                if (rset != null) {
                    while (rset.next()) {
                        final ArgMetadata argMetadata = 
                                      loadAttrArgMetadata(DataTypeDomain.SQL, rset, atLevel);
                        
                        loadStack.push(argMetadata);
                    }
                }
            }
        } finally {
            DBUtil.closeResultSet(rset);
        }
    }

    public void loadPLSQLRecord(Deque<ArgMetadata> loadStack, String typeOwner, String typePackage, String typeName, int atLevel) throws SQLException {
        loadPLSQLTypeMetadata("loadPLSQLRecord", loadStack, typeOwner, typePackage, typeName, atLevel);
    }

    public void loadPLSQLCollection(Deque<ArgMetadata> loadStack, String typeOwner, String typePackage, String typeName, int atLevel) throws SQLException {
        loadPLSQLTypeMetadata("loadPLSQLCollection", loadStack, typeOwner, typePackage, typeName, atLevel);
    }

    public void loadPLSQLType(Map<TypeMetadata.Attribute, Object> attributes, String typeOwner, String typePackage, String typeName) throws SQLException {
        HashMap<String, String> binds = new HashMap<String, String>();
        ResultSet rset = null;
        
        try {
            DBUtil dbUtil = DBUtil.getInstance(conn);
            dbUtil.setRaiseError(false);
            String query = MetadataUtil.getQuery(conn, "loadPLSQLType"); //$NON-NLS-1$
            if (query != null) {
                binds.put("TYPE_OWNER", typeOwner); //$NON-NLS-1$
                binds.put("TYPE_PACKAGE", typePackage); //$NON-NLS-1$
                binds.put("TYPE_NAME", typeName); //$NON-NLS-1$
                rset = dbUtil.executeOracleQuery(query, binds);
                if (rset != null) {
                    while (rset.next()) {
                        attributes.put(Attribute.DATA_TYPE,rset.getString("TYPECODE"));
                    }
                }
            }
        } finally {
            DBUtil.closeResultSet(rset);
        }
    }

    public void loadPLSQLAttributes(Deque<ArgMetadata> loadStack, String typeOwner, String typePackage, String typeName, int atLevel) throws SQLException {
        loadPLSQLTypeMetadata("loadPLSQLAttributes", loadStack, typeOwner, typePackage, typeName, atLevel);
    }

    public void loadPLSQLTypeMetadata(String queryKey, Deque<ArgMetadata> loadStack, String typeOwner, String typePackage, String typeName, int atLevel) throws SQLException {
        HashMap<String, String> binds = new HashMap<String, String>();
        ResultSet rset = null;
        
        try {
            DBUtil dbUtil = DBUtil.getInstance(conn);
            dbUtil.setRaiseError(false);
            String query = MetadataUtil.getQuery(conn, queryKey); //$NON-NLS-1$
            if (query != null) {
                binds.put("TYPE_OWNER", typeOwner); //$NON-NLS-1$
                binds.put("TYPE_PACKAGE", typePackage); //$NON-NLS-1$
                binds.put("TYPE_NAME", typeName); //$NON-NLS-1$
                rset = dbUtil.executeOracleQuery(query, binds);
                if (rset != null) {
                    while (rset.next()) {
                        final ArgMetadata argMetadata = 
                                      loadAttrArgMetadata(DataTypeDomain.PLSQL, rset, atLevel);
                        
                        loadStack.push(argMetadata);
                    }
                }
            }
        } finally {
            DBUtil.closeResultSet(rset);
        }
    }

    private ArgMetadata loadAttrArgMetadata(DataTypeDomain attrDomain, ResultSet rset, int atLevel) throws SQLException {
        ArgMetadata argMetadata;
        
        String typeCode    = rset.getString("TYPECODE");
        String typeOwner   = rset.getString("TYPE_OWNER");
        String typePackage = (attrDomain == DataTypeDomain.PLSQL) ? rset.getString("TYPE_PACKAGE") : null;
        String typeName    = (typePackage != null) ? typePackage : rset.getString("TYPE_NAME");
        String typeSubName = (typePackage != null) ? rset.getString("TYPE_NAME") : null;
        
        // Determine data type
        String dataType;
        if (typeCode != null) {
            dataType = typeCode;
        } else {
            if (typeOwner == null && typeSubName == null) {
                dataType = typeName;
                typeName = null;
            } else {
                dataType = null; 
            }
        }
        
        if (dataType != null || typeName != null) { // packaged procedures without parameters may have a spurious null return value (probably to remove ambiguity in public views).
            // Pre-load attributes
            String attrName = rset.getString("ATTR_NAME"); //$NON-NLS-1$
            int position = rset.getInt("POSITION"); //$NON-NLS-1$
            int level = rset.getInt("DATA_LEVEL")+atLevel; //$NON-NLS-1$
            Integer len = getIntegerResult(rset, "LENGTH"); //$NON-NLS-1$
            
            // Detect textual nature of datatype
            String charSetName = rset.getString("CHARACTER_SET_NAME");
            String  charUsed = rset.getString("CHAR_USED");
            boolean isCharUnit = "C".equals(charUsed);
            boolean isTextual = (charSetName != null) || isCharUnit;
            
            // Set Char Length for textual datatypes
            Integer charLen;
            if (!isTextual) {
                charLen = null;
                charUsed = null;
            } else {
                charLen = len;
            }
            
            // Set Data Length when not a character based datatype
            Integer dataLen = null;
            if (typeCode == null &&  len != null && (!isTextual || !isCharUnit)) {
                dataLen = len;
            } 
            
            Map<Attribute, Object> attributeMap = new HashMap<Attribute, Object>();
            
            // Populate attributeMap
                addAttribute(attributeMap, Attribute.DATA_TYPE, DataTypesUtil.reformatDataTypeString(dataType));
                addAttribute(attributeMap, Attribute.DATA_LENGTH, dataLen);
                addAttribute(attributeMap, Attribute.DATA_PRECISION, getIntegerResult(rset, "PRECISION")); //$NON-NLS-1$
                addAttribute(attributeMap, Attribute.DATA_SCALE, getIntegerResult(rset, "SCALE")); //$NON-NLS-1$
                addAttribute(attributeMap, Attribute.TYPE_OWNER, typeOwner);
                addAttribute(attributeMap, Attribute.TYPE_NAME, typeName);
                addAttribute(attributeMap, Attribute.TYPE_SUBNAME, typeSubName); 
                addAttribute(attributeMap, Attribute.CHAR_SET_NAME, charSetName);
                addAttribute(attributeMap, Attribute.CHAR_LENGTH, charLen);
                addAttribute(attributeMap, Attribute.CHAR_USED, charUsed);
            
            ArgIdentifier argIdentifier = new ArgIdentifier(position, level);
        
            argMetadata = new ArgMetadata(argIdentifier, attrName, attributeMap);
        } else {
            argMetadata = null;
        }
        
        return argMetadata;
    }
    
    private static boolean addAttribute(Map<Attribute, Object> attributeMap, Attribute attribute, Object value) {
        if (value != null) {
            attributeMap.put(attribute, value);
            return true;
        } else {
            return false;
        }
    }
    
    private static Integer getIntegerResult(ResultSet rset, String name) throws SQLException {
        return (rset.getObject(name) != null) ? rset.getInt(name) : null;
    }
    
    public static enum DataTypeDomain {
        SQL,
        PLSQL
    }
}
