/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.metadata;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import oracle.dbtools.raptor.datatypes.StructureType;
import oracle.dbtools.raptor.datatypes.TypeMetadata.Attribute;
import oracle.dbtools.raptor.datatypes.metadata.ProgMetadata.ProgIdentifier;
import oracle.dbtools.raptor.datatypes.metadata.SigMetadata.SigIdentifier;

public final class PLSQMetadataBuilder {
    final Map<ProgIdentifier,ProgMetadata> programs;
    final Deque<ArgMetadata> argStack;
    final Deque<LoadedArgMetadata> loadStack;
    final MetadataLoader loader;
    ProgMetadata program;
    SigMetadata signature;
    
    public PLSQMetadataBuilder(Connection conn) {
        this.loader = new MetadataLoader(conn);
        this.argStack = new LinkedList<ArgMetadata>();
        this.loadStack = new LinkedList<LoadedArgMetadata>();
        this.programs = new HashMap<ProgIdentifier,ProgMetadata>();
        this.program = null;
        this.signature = null;
    }
    
    public Map<ProgIdentifier,ProgMetadata> createArguments(String _objName, String _objOwner, String _objCall, Integer _objOverload) throws SQLException {
        // Load Arguemnts in stack
        loader.loadSignatureArguments(loadStack, _objName, _objOwner, _objCall, _objOverload);
        
        // build up arguments
        buildArguments();
        
        return programs;
    }
    
    private void buildArguments() throws SQLException {
        while (!loadStack.isEmpty() || !argStack.isEmpty()) {
            boolean proceed = true;
            
            if (!loadStack.isEmpty()) {
                LoadedArgMetadata loadedArgMetadata = loadStack.pop();
                
                proceed = pushArgMetadata(loadedArgMetadata);
            } else {
                // Rollup Rest of Stack
                proceed = rollupProgram();
            }
        }
        
        // Final Rollup Rest of Stack
        rollupProgram();
    }
    
    private boolean pushArgMetadata(LoadedArgMetadata loadedArgMetadata) throws SQLException {
        boolean proceed = true;
        
        proceed = switchTo(loadedArgMetadata);
        
        if (proceed) {
            proceed = rollupArgStack(loadedArgMetadata);
        }
        
        if (proceed && loadedArgMetadata.argMetadata != null) {
            argStack.push(loadedArgMetadata.argMetadata);
        }
        
        return proceed;
    }

    private boolean rollupArgStack(LoadedArgMetadata loadedArgMetadata) throws SQLException {
        Integer level = (loadedArgMetadata != null && loadedArgMetadata.argMetadata != null) 
        		? loadedArgMetadata.argMetadata.identifier.getLevel() 
        		: -1;
        
        while (!argStack.isEmpty()) {
            ArgMetadata top = argStack.peek();
            
            final boolean forced = (level != null && level == -1);
            final boolean tryRollup = (forced || top.identifier.getLevel() == null || level <= top.identifier.getLevel());
            
            if (tryRollup) {
                if (!hasArgComponents(top)) {
                    final StructureType structureType = getArgStructureType(top);
                    
                    if (structureType != StructureType.PRIMITIVE) {
                        if (loadedArgMetadata != null) {
                            // Push back onto load stack
                            loadStack.push(loadedArgMetadata);
                        }
                        
                        final int startingSize = loadStack.size();
                        
                        if (loadStack.size() > startingSize) {
                            LoadedArgMetadata parent = loadStack.pop();
                            
                            // Refine
                            refineDataType(top, parent);
                            
                            return false;
                        } else if (loadStack.size() == startingSize) {
                            if (loadedArgMetadata != null) {
                                // Restore load stack state
                                loadStack.pop();
                            }
                        }
                    }
                }
                
                ArgMetadata arg = argStack.pop();
                
                if (argStack.isEmpty()) {
                    signature.addArgument(arg);
                } else {
                    argStack.peek().addComponent(arg);
                }
            } else {
                break;
            }
        }
        
        return true;
    }

    private boolean rollupArgStack() throws SQLException {
        return rollupArgStack(null);
    }

    private boolean rollupSignature() throws SQLException {
        boolean proceed = true;
        
        if (signature != null) {
            proceed = rollupArgStack();
            
            if (proceed) {
                program.addSignature(signature);
                signature = null;
            }
        }
        
        return proceed;
    }
    
    private boolean rollupProgram() throws SQLException {
        boolean proceed = true;
        
        if (program != null) {
            proceed = rollupSignature();
            if (proceed) {
                programs.put(program.getIdentifier(), program);
                program = null;
            }
        }
        
        return proceed;
    }
    

    private StructureType getArgStructureType(ArgMetadata argMetadata) {
        String dataType = (String)argMetadata.getValue().get(Attribute.DATA_TYPE);
        
        if (dataType != null) {
            if (dataType.equals("PL/SQL RECORD") ||
                dataType.equals("OBJECT")) {
                return StructureType.RECORD;
            } else if (dataType.equals("PL/SQL TABLE") || 
                       dataType.equals("PL/SQL INDEX TABLE") || 
                       dataType.equals("COLLECTION") || 
                       dataType.equals("TABLE") || 
                       dataType.equals("VARRAY")) {
                return StructureType.TABLE;
            }
        }

        return StructureType.PRIMITIVE;
    }
    
    private void refineDataType(ArgMetadata target, LoadedArgMetadata source) {
        String dataType = (String)source.argMetadata.getValue().get(Attribute.DATA_TYPE);
        
        if (dataType != null) {
            //dataType = getDataTypeName(dataType);
            target.getValue().put(Attribute.DATA_TYPE, dataType);
        }
    }

    private boolean hasArgComponents(ArgMetadata argMetadata) {
        return argMetadata.hasComponents();
    }

    private SigMetadata findOrCreateSig(SigIdentifier sigIdentifier, ProgMetadata program) {
        // Find or Create Signature
        SigMetadata signature = program.getSignature(sigIdentifier.overload);
        if (signature == null) {
            signature = new SigMetadata(sigIdentifier);
        } else {
            // Ensure full match
            SigMetadata.SigMatch implMatches = signature.identifiedBy(sigIdentifier);
        
            if (implMatches == SigMetadata.SigMatch.SOFT_MATCHED) {
                signature.identifier = sigIdentifier;
            }
        }
        
        return signature;
    }

    private ProgMetadata findOrCreateProg(ProgIdentifier progIdentifier) {
        // Find or Create Program
        ProgMetadata program = programs.get(progIdentifier);
        if (program == null) {
            program = new ProgMetadata(progIdentifier);
        }
        
        return program;
    }

    private boolean switchTo(LoadedArgMetadata loadedArgMetadata) throws SQLException {
        boolean proceed = true;
        
        ProgIdentifier progIdentifier = loadedArgMetadata.progIdentifier;
        SigIdentifier sigIdentifier = loadedArgMetadata.sigIdentifier;
        
        // check if needs signature rollup
        if (sigIdentifier != null && signature != null && proceed) {
            SigMetadata.SigMatch implMatches = signature.identifiedBy(sigIdentifier);
            
            switch (implMatches) {
                case SOFT_MATCHED:
                    if (sigIdentifier != null) {
                        signature.identifier = sigIdentifier;
                    }
                    break;
                case DIFFERENT:
                    proceed = rollupSignature();
                    break;
            }
        }
        
        // check if needs program rolup
        if (progIdentifier != null && program != null && proceed) {
            if (!program.identifiedBy(progIdentifier)) {
                proceed = rollupProgram();
            }
        }
        
        if (proceed) {
            if (program == null) {
                // Find or Create Program
                program = findOrCreateProg(progIdentifier);
            }
            
            if (signature == null) {
                // Find or Create Signature
                signature = findOrCreateSig(sigIdentifier, program);
            }
        }
        
        return proceed;
    }
}
