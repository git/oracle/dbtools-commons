/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.metadata;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import oracle.dbtools.raptor.utils.DataTypesUtil;

public final class ProgMetadata {
    public final static class ProgIdentifier implements Cloneable {
        public ProgIdentifier(String owner, String objectName, String packageName) {
            this.owner = owner;
            this.objectName = objectName;
            this.packageName = packageName;
        }

        public ProgIdentifier() {
            this(null, null, null);;
        }

        @Override
        public Object clone() {
            return new ProgIdentifier(owner, objectName, packageName);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) return true;
            if (obj instanceof ProgIdentifier) {
                ProgIdentifier other = (ProgIdentifier)obj;
                return DataTypesUtil.areEqual(owner, other.owner) &&
                       DataTypesUtil.areEqual(objectName, other.objectName) &&
                       DataTypesUtil.areEqual(packageName, other.packageName);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            int hash = 23;
            hash = 31 * hash + (null == owner ? 0 : owner.hashCode());
            hash = 31 * hash + (null == objectName ? 0 : objectName.hashCode());
            hash = 31 * hash + (null == packageName ? 0 : packageName.hashCode());
            return hash;
        }

        public String getOwner() {
            return owner;
        }
        
        public String getObjectName() {
            return objectName;
        }
        
        public String getPackageName() {
            return packageName;
        }
        
        boolean matches(ProgIdentifier identifier) {
            return DataTypesUtil.areEqual(this, (identifier != null) ? identifier : NULL_PROG_ID);
        }
        
        final String owner;
        final String objectName;
        final String packageName;
        final static ProgIdentifier NULL_PROG_ID = new ProgIdentifier();
    }
    
    public ProgMetadata(ProgIdentifier identifier, List<SigMetadata> signatures) {
        this.identifier = identifier;
        
        this.signatures = new TreeMap<Integer,SigMetadata>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1 == null) {
                    if (o2 != null) {
                        return -1;
                    } else {
                        return 0;
                    }
                } else {
                    if (o2 == null) {
                        return 1;
                    } else {
                        return o1.compareTo(o2);
                    }
                }
            }
        });
        
        this.minSigOverload = null;
        this.maxSigOverLoad = null;
        
        // Add Signatures
        if (signatures != null) {
            for (SigMetadata signature : signatures) {
                addSignature(signature);
            }
        }
    }
    
    public ProgMetadata(ProgIdentifier identifier) {
        this(identifier, null);
    }
    
    public ProgIdentifier getIdentifier() {
        return identifier;
    }
    
    public Integer getMinSigOverload() {
        return minSigOverload;
    }
    
    public Integer getMaxSigOverload() {
        return maxSigOverLoad;
    }
    
    public SigMetadata getSignature(Integer overload) {
        return signatures.get(overload);
    }
    
    boolean addSignature(SigMetadata signature) {
        if (signature != null) {
            Integer overload = signature.identifier.overload;
            
            SigMetadata oldSignature = signatures.put(overload, signature);
            
            if (oldSignature != null && oldSignature != signature) {
                // Put old signature back
                signatures.put(oldSignature.identifier.overload, oldSignature);
                throw new IllegalArgumentException();
            }

            if (minSigOverload == null || overload == null || minSigOverload.intValue() > overload.intValue()) minSigOverload = overload;
            if (maxSigOverLoad == null || (overload != null && maxSigOverLoad.intValue() < overload.intValue())) maxSigOverLoad = overload;
            
            return true;
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    public int countSignatures() {
        return signatures.size();
    }

    public Iterator<SigMetadata> signatureIterator() {
        return signatures.values().iterator();
    }
    
    boolean identifiedBy(ProgIdentifier identifier) {
        return this.identifier.matches(identifier);
    }
    
    final TreeMap<Integer,SigMetadata> signatures;
    
    final ProgIdentifier identifier;

    Integer minSigOverload;
    Integer maxSigOverLoad;
}
