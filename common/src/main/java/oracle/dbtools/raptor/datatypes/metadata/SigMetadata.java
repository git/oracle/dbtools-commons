/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.metadata;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import oracle.dbtools.raptor.utils.DataTypesUtil;

public final class SigMetadata {
    public final static class SigIdentifier implements Cloneable {
        SigIdentifier(Integer overload, Integer objectId, Integer subObjectId) {
            this.overload = overload;
            this.objectId = objectId;
            this.subObjectId = subObjectId;
        }

        SigIdentifier() {
            this(null, null, null);
        }

        @Override
        public Object clone() {
            return new SigIdentifier(overload, objectId, subObjectId);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) return true;
            if (obj instanceof SigIdentifier) {
                SigIdentifier other = (SigIdentifier)obj;
                return DataTypesUtil.areEqual(overload, other.overload) &&
                       DataTypesUtil.areEqual(objectId, other.objectId) &&
                       DataTypesUtil.areEqual(subObjectId, other.subObjectId);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            int hash = 23;
            hash = 31 * hash + (null == overload ? 0 : overload.hashCode());;
            hash = 31 * hash + (null == objectId ? 0 : objectId.hashCode());
            hash = 31 * hash + (null == subObjectId ? 0 : subObjectId.hashCode());
            return hash;
        }
        
        public Integer getOverload() {
            return overload;
        }
        
        public Integer getObjectId() {
            return objectId;
        }
        
        public Integer getSubObjectId() {
            return subObjectId;
        }
        
        SigMatch matches(SigIdentifier identifier) {
            SigIdentifier against = (identifier != null) ? identifier : NULL_SIG_ID;
            
            if (objectId == null && subObjectId == null) {
                if (DataTypesUtil.areEqual(overload, against.overload)) {
                    return SigMatch.SOFT_MATCHED;
                }
            } else {
                if (DataTypesUtil.areEqual(this, against)) {
                    return SigMatch.MATCHED;
                }
            }
            return SigMatch.DIFFERENT;
        }
        
        final Integer overload;
        final Integer objectId;
        final Integer subObjectId;
        final static SigIdentifier NULL_SIG_ID = new SigIdentifier();
    }
    
    public SigMetadata(SigIdentifier identifier, List<ArgMetadata> arguments) {
        this.identifier = identifier;
        
        this.arguments = new TreeMap<Integer,ArgMetadata>();
        
        this.minArgPosition = null;
        this.maxArgPosition = null;
        
        // Add Arguments
        if (arguments != null) {
            for (ArgMetadata argument : arguments) {
                addArgument(argument);
            }
        }
    }
    
    public SigMetadata(SigIdentifier identifier) {
        this(identifier, null);
    }
    
    public SigIdentifier getIdentifier() {
        return identifier;
    }
    
    public Integer getMinArgPosition() {
        return minArgPosition;
    }

    public Integer getMaxArgPosition() {
        return maxArgPosition;
    }

    public ArgMetadata getArgument(int position) {
        return arguments.get(position);
    }
    
    boolean addArgument(ArgMetadata argument) {
        if (argument != null) {
            Integer position = argument.identifier.getPosition();
            
            if (position == null || position.intValue() < 0) {
                throw new IllegalArgumentException();
            }
            
            ArgMetadata oldArgument = arguments.put(position, argument);
            
            if (oldArgument != null && oldArgument != argument) {
                // Put old argument back
                arguments.put(oldArgument.identifier.getPosition(), oldArgument);
                throw new IllegalArgumentException();
            }
            
            if (minArgPosition == null || minArgPosition.intValue() > position.intValue()) minArgPosition = position;
            if (maxArgPosition == null || maxArgPosition.intValue()  < position.intValue()) maxArgPosition = position;
            
            return true;
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    public int countArguments() {
        return arguments.size();
    }

    public Iterator<ArgMetadata> argumentIterator() {
        return arguments.values().iterator();
    }
    
    SigMatch identifiedBy(SigIdentifier identifier) {
        return this.identifier.matches(identifier);
    }
    
    final Map<Integer,ArgMetadata> arguments;
    
    SigIdentifier identifier;
    
    Integer minArgPosition;
    Integer maxArgPosition;
    
    public static enum SigMatch {
        MATCHED,
        SOFT_MATCHED,
        DIFFERENT;
    }
}
