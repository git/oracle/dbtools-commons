/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.io.ByteArrayInputStream;
import java.io.CharArrayReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.util.encoding.EncodingException;
import oracle.dbtools.util.encoding.EncodingType;

import oracle.jdbc.OracleConnection;

import oracle.sql.BLOB;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=BlobDatum.java">Barry McGillin</a> 
 *
 */
public class BlobDatum extends LongBinary {
    public class BlobDatumBuilder extends LongBinaryBuilder {
        protected BlobDatumBuilder(EncodingType encodingType, int cutOverLen) {
            super(encodingType, cutOverLen);
        }
        
        @Override
        public BlobDatumBuilder write(Reader reader) throws IOException, EncodingException {
            return (BlobDatumBuilder)super.write(reader);
        }
        
        public BlobDatumBuilder write(InputStream istream) throws IOException {
            return (BlobDatumBuilder)super.write(istream);
        }
            
        @Override
        public BlobDatum build() throws IOException {
            super.build();
            
            return BlobDatum.this;
        }
    }

    public BlobDatum() {
        super();
    }

    protected BlobDatum(BlobDatum source) {
        super(source);
    }

    protected BlobDatum(Object object) {
        super(object);
    }
    
    public BlobDatum(Blob value) throws SQLException {
        super(value);
    }

    public BlobDatum(byte[] value) {
        super(value);
    }

    public BlobDatum(File value) {
        super(value);
    }

    public BlobDatum(InputStream istream) throws IOException {
        this(istream, CUTOVER_LENGTH);
    }

    public BlobDatum(InputStream istream, int cutOverLen) throws IOException {
        this(getBuilder(EncodingType.ENCODING_NONE, cutOverLen).write(istream).build());
    }

    public BlobDatum(CharSequence hex) throws IOException, EncodingException {
        this(hex, CUTOVER_LENGTH);
    }

    public BlobDatum(CharSequence hex, int cutOverLen) throws IOException, EncodingException {
        this(getBuilder(EncodingType.ENCODING_HEX, cutOverLen).write(new StringReader(hex.toString())).build());
    }

    public BlobDatum(char[] chars) throws IOException, EncodingException {
        this(chars, CUTOVER_LENGTH);
    }

    public BlobDatum(char[] chars, int cutOverLen) throws IOException, EncodingException {
        this(getBuilder(EncodingType.ENCODING_HEX, cutOverLen).write(new CharArrayReader(chars)).build());
    }

    public BlobDatum(Reader reader) throws IOException, EncodingException {
        this(reader, CUTOVER_LENGTH);
    }

    public BlobDatum(Reader reader, int cutOverLen) throws IOException, EncodingException {
        this(getBuilder(EncodingType.ENCODING_HEX, cutOverLen).write(reader).build());
    }
    
    public static BlobDatum constructFrom(Object value) throws IOException, SQLException, EncodingException {
        if (value instanceof Blob) {
            return new BlobDatum((Blob)value);
        } else if (value instanceof byte[]) {
            return new BlobDatum((byte[])value);
        } else if (value instanceof InputStream) {
            return new BlobDatum((InputStream)value);
        } else if (value instanceof char[]) {
            return new BlobDatum(String.valueOf((char[])value));
        } else if (value instanceof CharSequence) {
            return new BlobDatum((CharSequence)value);
        } else if (value instanceof File) {
            return new BlobDatum((File)value);
        } else if (value instanceof Reader) {
            return new BlobDatum((Reader)value);
        } else if (value instanceof oracle.sql.Datum) {
            oracle.sql.Datum datumValue = (oracle.sql.Datum)value;
            return new BlobDatum(datumValue.stringValue());
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    @Override
    public long getLength() throws SQLException {
        if (getValue() instanceof Blob) {
            Blob blob = (Blob)getValue();
            if (isEmptyLob(blob)) {
                return 0L;
            } else {
                return ((Blob)getValue()).length();
            }
        } else {
            return super.getLength();
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = (this == obj);

        if (!isEqual && obj instanceof BlobDatum) {
            isEqual = super.equals(obj);
        }

        return isEqual;
    }
    
    public InputStream getInputStream() throws IOException, SQLException {
        if (getValue() instanceof Blob) {
            Blob blob = (Blob)getValue();
            if (isEmptyLob(blob)) {
                return new ByteArrayInputStream(new byte[0]);
            } else {
                return blob.getBinaryStream();
            }
        } else {
            return super.getInputStream();
        }
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target)  throws IOException, SQLException {
        Object value = getValue();
        
        switch (valueType) {
            case DATUM:
                if (value instanceof Blob) {
                    return value;
                } else {
                    Blob tempBlob = getTemporaryBlob(connectionProvider.getValidDataTypeConnection());
                    open(tempBlob, BLOB.MODE_READWRITE);
                    try {
                        OutputStream ostream = tempBlob.setBinaryStream(1L);
                        copyBytes(getInputStream(), ostream);
                        ostream.close();
                    } finally {
                        try {
                            close(tempBlob);
                        } catch (SQLException e) {
                            Logger.getLogger(ClobDatum.class.getName()).log(Level.WARNING,
                                                                          e.getLocalizedMessage(), e);
                            
                        }
                    }
                    return tempBlob;
                }
            default:
                return super.customTypedValue(connectionProvider, valueType, target);
        }
    }
    
    public static BlobDatumBuilder getBuilder() {
        return getBuilder(CUTOVER_LENGTH);
    }

    public static BlobDatumBuilder getBuilder(EncodingType encoding, int cutOverLen) {
        return new BlobDatum().getBuilder0(encoding, cutOverLen);
    }

    public static BlobDatumBuilder getBuilder(EncodingType encoding) {
        return getBuilder(encoding, CUTOVER_LENGTH);
    }

    public static BlobDatumBuilder getBuilder(int cutOverLen) {
        return new BlobDatum().getBuilder0(EncodingType.ENCODING_BASE64, cutOverLen);
    }

    private BlobDatumBuilder getBuilder0(EncodingType encoding, int cutOverLen) {
        return new BlobDatumBuilder(encoding, cutOverLen);
    }

    protected static Blob getTemporaryBlob(Connection connection) throws SQLException {
        if (connection instanceof OracleConnection) {
            return BLOB.createTemporary(connection, true, BLOB.DURATION_SESSION);
        } else {
            return connection.createBlob();
        }
    }
    
    protected static Blob getEmptyBlob(Connection connection) throws SQLException {
        if (connection instanceof OracleConnection) {
            return BLOB.getEmptyBLOB();
        } else {
            return getTemporaryBlob(connection);
        }
    }
    
    protected static boolean isEmptyLob(Blob blob) throws SQLException {
        if (blob instanceof BLOB) {
            return ((BLOB)blob).isEmptyLob();
        } else {
            return false;
        }
    }
    
    protected static void open(Blob blob, int mode) throws SQLException {
        if (blob instanceof BLOB) {
            BLOB oraBLOB = (BLOB)blob;
            if (!oraBLOB.isEmptyLob() && !oraBLOB.isOpen()) {
                oraBLOB.open(mode);
            }
        }
    }
    
    protected static void close(Blob blob) throws SQLException {
        if (blob instanceof BLOB) {
            BLOB oraBLOB = (BLOB)blob;
            if (!oraBLOB.isEmptyLob() && oraBLOB.isOpen()) {
                oraBLOB.close();
            }
        }
    }
    
    protected static void free(Blob blob) throws SQLException {
        if (blob instanceof BLOB) {
            BLOB oraBLOB = (BLOB)blob;
            if (oraBLOB.isTemporary() && !oraBLOB.isOpen()) {
                oraBLOB.freeTemporary();
            }
        }
            
        blob.free();
    }
}
