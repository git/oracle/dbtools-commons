/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import java.io.Writer;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.ValueType;

import oracle.jdbc.OracleConnection;

import oracle.sql.CLOB;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ClobDatum.java">Barry McGillin</a> 
 *
 */
public class ClobDatum extends LongVarchar {
    public class ClobDatumBuilder extends LongVarcharBuilder {
        protected ClobDatumBuilder(int cutOverLen) {
            super(cutOverLen);
        }
        
        @Override
        public ClobDatumBuilder write(Reader reader) throws IOException {
            return (ClobDatumBuilder)super.write(reader);
        }
        
        @Override
        public ClobDatum build() throws IOException {
            super.build();
            
            return ClobDatum.this;
        }
    }
    
    public ClobDatum() {
        super();
    }
    
    protected ClobDatum(ClobDatum source) {
        super(source);
    }
    
    protected ClobDatum(Object object) {
        super(object);
    }
    
    public ClobDatum(Clob value) throws SQLException {
        super(value);
    }
    
    public ClobDatum(CharSequence value) {
        super(value);
    }
    
    public ClobDatum(File value) {
        super(value);
    }
    
    public ClobDatum(Reader reader) throws IOException {
        this(new ClobDatum().getBuilder().write(reader).build());
    }
    
    public ClobDatum(Reader reader, int cutOverLen) throws IOException {
        this(new ClobDatum().getBuilder(cutOverLen).write(reader).build());
    }
    
    public static ClobDatum constructFrom(Object value) throws IOException, SQLException {
        if (value instanceof Clob) {
            return new ClobDatum((Clob)value);
        } else if (value instanceof char[]) {
            return new ClobDatum(String.valueOf((char[])value));
        } else if (value instanceof CharSequence) {
            return new ClobDatum((CharSequence)value);
        } else if (value instanceof File) {
            return new ClobDatum((File)value);
        } else if (value instanceof Reader) {
            return new ClobDatum((Reader)value);
        } else if (value instanceof oracle.sql.Datum) {
            oracle.sql.Datum datumValue = (oracle.sql.Datum)value;
            return new ClobDatum(datumValue.stringValue());
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    @Override
    public long getLength() throws SQLException {
        if (getValue() instanceof Clob) {
            Clob clob = (Clob)getValue();
            if (isEmptyLob(clob)) {
                return 0L;
            } else {
                return ((Clob)getValue()).length();
            }
        } else {
            return super.getLength();
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = (this == obj);

        if (!isEqual && obj instanceof ClobDatum) {
            isEqual = super.equals(obj);
        }

        return isEqual;
    }
    
    @Override
    public Reader getReader() throws IOException, SQLException {
        if (getValue() instanceof Clob) {
            Clob clob = (Clob)getValue();
            if (isEmptyLob(clob)) {
                return new StringReader("");
            } else {
                return clob.getCharacterStream();
            }
        } else {
            return super.getReader();
        }
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target)  throws IOException, SQLException {
        Object value = getValue();
        
        switch (valueType) {
            case DATUM:
                if (value instanceof Clob) {
                    return value;
                } else {
                    Clob tempClob = getTemporaryClob(connectionProvider.getValidDataTypeConnection());
                    open(tempClob, CLOB.MODE_READWRITE);
                    try {
                        Writer writer = tempClob.setCharacterStream(1L);
                        copyCharacters(getReader(), writer);
                        writer.close();
                    } finally {
                        try {
                            close(tempClob);
                        } catch (SQLException e) {
                            Logger.getLogger(ClobDatum.class.getName()).log(Level.WARNING,
                                                                          e.getLocalizedMessage(), e);
                            
                        }
                    }
                    return tempClob;
                }
            default:
                return super.customTypedValue(connectionProvider, valueType, target);
        }
    }
    
    public static ClobDatumBuilder getBuilder() {
        return getBuilder(CUTOVER_LENGTH);
    }

    private ClobDatumBuilder getBuilder0(int cutOverLen) {
        return new ClobDatumBuilder(cutOverLen);
    }

    public static ClobDatumBuilder getBuilder(int cutOverLen) {
        return new ClobDatum().getBuilder0(cutOverLen);
    }

    protected static Clob getTemporaryClob(Connection connection) throws SQLException {
        if (connection instanceof OracleConnection) {
            return CLOB.createTemporary(connection, true, CLOB.DURATION_SESSION);
        } else {
            return connection.createClob();
        }
    }
    
    protected static Clob getEmptyClob(Connection connection) throws SQLException {
        if (connection instanceof OracleConnection) {
            return CLOB.getEmptyCLOB();
        } else {
            return getTemporaryClob(connection);
        }
    }
    
    protected static boolean isEmptyLob(Clob clob) throws SQLException {
        if (clob instanceof CLOB) {
            return ((CLOB)clob).isEmptyLob();
        } else {
            return false;
        }
    }
    
    protected static void open(Clob clob, int mode) throws SQLException {
        if (clob instanceof CLOB) {
            CLOB oraCLOB = (CLOB)clob;
            if (!oraCLOB.isEmptyLob() && !oraCLOB.isOpen()) {
                oraCLOB.open(mode);
            }
        }
    }
    
    protected static void close(Clob clob) throws SQLException {
        if (clob instanceof CLOB) {
            CLOB oraCLOB = (CLOB)clob;
            if (!oraCLOB.isEmptyLob() && oraCLOB.isOpen()) {
                oraCLOB.close();
            }
        }
    }
    
    protected static void free(Clob clob) throws SQLException {
        if (clob instanceof CLOB) {
            CLOB oraCLOB = (CLOB)clob;
            if (oraCLOB.isTemporary() && !oraCLOB.isOpen()) {
                oraCLOB.freeTemporary();
            }
        }
            
        clob.free();
    }
}
