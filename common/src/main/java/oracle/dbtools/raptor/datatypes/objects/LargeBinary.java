/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import java.sql.Blob;
import java.sql.SQLException;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeEncodingException;
import oracle.dbtools.raptor.datatypes.DataTypeIOException;
import oracle.dbtools.raptor.datatypes.DataTypeSQLException;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;
import oracle.dbtools.raptor.datatypes.util.CappedFilterWriter;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.util.encoding.Decoder;
import oracle.dbtools.util.encoding.EncodingException;
import oracle.dbtools.util.encoding.EncodingFactory;
import oracle.dbtools.util.encoding.EncodingType;
import oracle.dbtools.util.encoding.HEXEncoding;
import oracle.dbtools.util.encoding.MimeType;

import org.xml.sax.SAXException;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=LargeBinary.java">Barry McGillin</a> 
 *
 */
public abstract class LargeBinary extends LargeDatum {
    protected static final byte FILL_BYTE = 0;

    public abstract class LargeBinaryBuilder extends LargeDatumBuilder {
        private char[] charBank;
        private int banked;
        protected final Decoder decoder;
        protected final int decoderBlockSize;
        protected OutputStream ostream;
        protected OutputStream switchableStream;

        protected LargeBinaryBuilder(EncodingType encodingType, int cutOverLen) {
            super(cutOverLen);
            this.decoder = EncodingFactory.getDecoder(encodingType, MimeType.MIME_TEXT);
            this.decoderBlockSize = decoder.getDecodeBlocksize();
            this.charBank = new char[decoderBlockSize];
            this.banked = 0;
            this.ostream = new ByteArrayOutputStream();
            this.switchableStream = new FilterOutputStream(ostream) {
                    @Override
                    public void write(int b) throws IOException {
                        checkForCutOver(1);
                        out = ostream;
                        super.write(b);
                        length++;
                    }

                    @Override
                    public void flush() throws IOException {
                        super.flush();
                        checkForCutOver(0);
                        out = ostream;
                        super.flush();
                    }

                    @Override
                    public void close() throws IOException {
                        flush();
                        super.close();
                    }
                };
        }

        public LargeBinaryBuilder write(char[] chars, int start, int len) throws IOException,
                                                                                EncodingException {
            // Setup offsets and length
            int end = start + len;
            int bulkStart = start;
            int bulkLen = len;

            // Handle banked Characters
            if (banked > 0) {
                if (banked < decoderBlockSize) {
                    for (; banked < decoderBlockSize && bulkStart < end; bulkLen--) {
                        charBank[banked++] = chars[bulkStart++];
                    }
                }

                // Write Bank
                if (banked >= decoderBlockSize) {
                    writeBank();
                } else {
                    // Bank not full yet
                    return this;
                }
            }

            // Calculate remainder and shorten to blocks
            int remainder = bulkLen % decoderBlockSize;
            bulkLen -= remainder;

            // Write Bulk
            if (bulkLen > 0) {
                write(new CharArrayReader(chars, bulkStart, bulkLen));
            }

            // Bank Remainder
            if (remainder > 0) {
                int remainderOffset = bulkStart + bulkLen;
                for (int i = 0; i < remainder; i++) {
                    charBank[banked++] = chars[remainderOffset + i];
                }
            }

            return this;
        }

        public LargeBinaryBuilder write(Reader reader) throws IOException, EncodingException {
            // Write Bytes
            decoder.decodeStream(reader, switchableStream);

            return this;
        }

        public LargeBinaryBuilder write(byte[] bytes, int start, int len) throws IOException {
            // Write Bytes
            switchableStream.write(bytes, start, len);

            return this;
        }

        public LargeBinaryBuilder write(InputStream istream) throws IOException {
            // Read in 256 byte blocks
            byte[] buf = new byte[BUFFER_SIZE];

            // Read chunk
            int len = istream.read(buf);

            // Read and Write to the end
            while (len != -1) {
                // Write Buffer
                write(buf, 0, len);

                // Read next chunk
                len = istream.read(buf);
            }

            return this;
        }

        private void writeBank() throws IOException, EncodingException {
            if (banked > 0) {
                write(new CharArrayReader(charBank, 0, banked));
                banked = 0;
            }
        }
    }

    public LargeBinary() {
        super();
    }

    protected LargeBinary(Object value) {
        super(value);
    }

    protected LargeBinary(LargeBinary source) {
        super(source);
    }

    public OutputStream exportTo(OutputStream ostream) throws IOException, SQLException {
        copyBytes(getInputStream(), ostream);
        ostream.flush();
        return ostream;
    }

    public Blob exportTo(Blob blob) throws IOException, SQLException {
        OutputStream ostream = blob.setBinaryStream(1L);
        exportTo(ostream);
        ostream.close();
        return blob;
    }

    public File exportTo(File file) throws IOException, SQLException {
        exportToObject(file);
        return file;
    }

    public Object exportTo(Object target) throws IOException, SQLException {
        return exportToObject(target);
    }

    protected Object exportToObject(Object target) throws IOException, SQLException {
        if (target instanceof OutputStream) {
            return exportTo((OutputStream)target);
        } else if (target instanceof Blob) {
            return exportTo((Blob)target);
        } else if (target instanceof File) {
            return exportTo(new FileWriter((File)target));
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = (this == obj);

        try {
            if (!isEqual && obj instanceof LargeBinary) {
                LargeBinary objLargeBinary = (LargeBinary)obj;
                isEqual = (getLength() == objLargeBinary.getLength() && 
                           ((getValue() == objLargeBinary.getValue()) ||
                            (getValue() == null && objLargeBinary.getValue() == null) ||
                            (getValue() != null && objLargeBinary.getValue() != null && compareData(getInputStream(), objLargeBinary.getInputStream()))));
            }
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage(),
                                                          e);

        }

        return isEqual;
    }

    public abstract InputStream getInputStream() throws IOException, SQLException;

    protected StringValue customStringValue(StringType stringType, int maxLen) {
        switch (stringType) {
            case DEFAULT:
                StringValue ret = null;
    
                try {
                    HEXEncoding hexEnc =
                        (HEXEncoding)EncodingFactory.getEncoder(EncodingType.ENCODING_HEX, MimeType.MIME_TEXT);
    
                    // Create writer
                    StringWriter stringWriter = new StringWriter();
                    BufferedWriter bufferedWriter = new BufferedWriter(stringWriter);
                    Writer filterWriter = new CappedFilterWriter(bufferedWriter, maxLen);
    
                    // Get input stream
                    InputStream istream = getInputStream();
    
                    // Encode to Hex
                    try {
                        hexEnc.encode(istream, filterWriter);
                    } finally {
                        // Try to make a String Value Anyway
                        try {
                            // Flush Writer - no need to close as String-based
                            filterWriter.flush();
    
                            ret = new StringValue(stringWriter.toString(), hexEnc.getEncodeLength(getLength()));
                        } catch (IOException e) {
                            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                                       e.getLocalizedMessage(), e);
                        }
    
                        // Close Input Stream
                        try {
                            istream.close();
                        } catch (IOException e) {
                            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                                       e.getLocalizedMessage(), e);
                        }
                    }
                } catch (SQLException e) {
                    if (ret == null || maxLen < 0 || ret.toString().length() < maxLen) {
                        throw new DataTypeSQLException(e);
                    }
                } catch (IOException e) {
                    if (ret == null || maxLen < 0 || ret.toString().length() < maxLen) {
                        throw new DataTypeIOException(e);
                    }
                } catch (EncodingException e) {
                    throw new DataTypeEncodingException(e);
                }
    
                return ret;
            default:
                return super.customStringValue(stringType, maxLen);
        }
    }

    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target)  throws IOException, SQLException {
        switch (valueType) {
            case TARGET:
                return exportTo(target);
            default:
                return super.customTypedValue(connectionProvider, valueType, target);
        }
    }

    public void marshal(DataType datatype, DataValueMarshalHandler hd,
                        String name) throws SAXException {
        try {
            // Get input stream
            InputStream istream = getInputStream();

            // Copy Characters
            try {
                hd.bodyDataValue(datatype, name, istream);
            } finally {
                try {
                    istream.close();
                } catch (IOException e) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                               e.getLocalizedMessage(), e);
                }
            }
        } catch (SQLException e) {
            throw new DataTypeSQLException(e);
        } catch (IOException e) {
            throw new DataTypeIOException(e);
        }
    }

    public static void copyBytes(InputStream istream, OutputStream ostream) throws IOException {
        copyBytes(istream, ostream, BUFFER_SIZE);
    }
    
    public static void copyBytes(InputStream istream, OutputStream ostream, int bufferSize) throws IOException {
        // Read in 256 byte blocks
        byte[] buf = new byte[bufferSize];
        
        // Read chunk
        int len = istream.read(buf);
        
        // Read and Write to the end
        while (len != -1) {
            // Write Buffer
            ostream.write(buf, 0, len);
            
            // Read next chunk
            len = istream.read(buf);
        }
    }
    
    public static boolean compareData(InputStream r1, InputStream r2) throws IOException {
        // Read in 256 byte blocks
        byte[] b1 = new byte[BUFFER_SIZE];
        byte[] b2 = new byte[BUFFER_SIZE];

        // Fill Arrays
        Arrays.fill(b1, FILL_BYTE);
        Arrays.fill(b2, FILL_BYTE);

        // Read chunk
        int l1 = r1.read(b1);
        int l2 = r2.read(b2);

        // Read and Write to the end
        while (l1 != -1 && l2 != -1 && l1 == l2) {
            // Write Buffer

            if (!Arrays.equals(b1, b2)) {
                return false;
            }

            l1 = r1.read(b1);
            l2 = r2.read(b2);
        }

        return (l1 == l2);
    }
}
