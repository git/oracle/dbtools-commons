/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.io.IOException;

import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;
import oracle.dbtools.raptor.datatypes.util.StringValue;

import org.xml.sax.SAXException;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=LargeDatum.java">Barry McGillin</a> 
 *
 */
public abstract class LargeDatum {
    public static final int CUTOVER_LENGTH = 32768;
    protected static final int BUFFER_SIZE = 2400;
    
    private Object value;
    private boolean deleteOnFinalize;

    protected abstract class LargeDatumBuilder {
        protected int cutOverLen;
        protected long length;
        
        protected LargeDatumBuilder(int cutOverLen) {
            this.cutOverLen = cutOverLen;
            this.length = 0L;
        }
        
        protected void checkForCutOver(int len) throws IOException {
            if (cutOverLen >= 0 && length+len > cutOverLen) {
                try {
                    // Cut-over
                    cutOver();
                } finally {    
                    // Switch off cut-over
                    cutOverLen = -1;
                }
            }
        }
        
        public abstract LargeDatum build() throws IOException, SQLException;
        protected abstract void cutOver() throws IOException;
    }

    protected LargeDatum() {
        this.value = null;
        this.deleteOnFinalize = false;
    }

    protected LargeDatum(Object value) {
        this.value = value;
        this.deleteOnFinalize = false;
    }

    protected LargeDatum(LargeDatum source) {
        this.value = source.value;
        this.deleteOnFinalize = source.deleteOnFinalize;
        source.deleteOnFinalize = false;
    }
    
    public boolean isNull() {
        return (value == null);
    }

    public Object getValue() {
        return value;
    }
    
    protected void setValue(Object value, boolean deleteOnFinalize) {
        this.value = value;
        this.deleteOnFinalize = deleteOnFinalize;
    }
    
    @Override
    public String toString() {
        try {
            StringValue stringValue = getStringValue(StringType.DEFAULT, -1);
            return (stringValue == null) ? null : stringValue.toString();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                       e.getStackTrace()[0].toString(), e);

            return super.toString();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            customFinalize(value, deleteOnFinalize);
        } finally {
            super.finalize();
        }
    }
    
    protected void customFinalize(Object value, boolean deleteOnFinalize) throws Throwable {
        if (deleteOnFinalize && value != null) {
            ; // Do nothing
        }
    }

    public final Object getTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target) throws IOException, SQLException {
        return (value == null) ? null : customTypedValue(connectionProvider, valueType, target);
    }

    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target)  throws IOException, SQLException {
        switch (valueType) {
            case JAVA:
                return customTypedValue(connectionProvider, ValueType.JDBC, target);
            case DEFAULT:
                return value;
            default:
                return customTypedValue(connectionProvider, ValueType.DEFAULT, target);
        }
    }

    public abstract long getLength() throws SQLException;

    public final StringValue getStringValue(StringType stringType, int maxLen) {
        StringValue stringValue = (value == null) ? new StringValue(null) : customStringValue(stringType, maxLen);
        
        String valueText = stringValue.toString();
        
        if (maxLen < 0 || valueText == null || valueText.length() <= maxLen) {
            return stringValue;
        } else {
            return new StringValue(valueText.substring(0, maxLen), stringValue.getMaxLength());
        }
    }
    
    protected StringValue customStringValue(StringType stringType, int maxLen) {
        switch (stringType) {
            case DEFAULT:
                return new StringValue(value.toString());
            default:
                return customStringValue(StringType.DEFAULT, maxLen);
        }
    }
    
    public abstract void marshal(DataType datatype, DataValueMarshalHandler hd, String name) throws SAXException;
}
