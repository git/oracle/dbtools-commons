/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import java.sql.Clob;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeIOException;
import oracle.dbtools.raptor.datatypes.DataTypeSQLException;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;
import oracle.dbtools.raptor.datatypes.util.CappedFilterWriter;
import oracle.dbtools.raptor.datatypes.util.StringValue;

import oracle.dbtools.raptor.utils.DataTypesUtil;

import org.xml.sax.SAXException;

public abstract class LargeVarchar extends LargeDatum {
    public abstract class LargeVarcharBuilder extends LargeDatumBuilder {
        protected Writer writer; // $NOT_FINAL_OK: Builder only

        protected LargeVarcharBuilder(Writer writer, int cutOverLen) {
            super(cutOverLen);
            this.writer = writer;
        }

        public LargeVarcharBuilder write(char[] chars, int start, int len) throws IOException {
            // Check for cut-over
            checkForCutOver(len);

            // Write Characters
            writer.write(chars, start, len);

            // Write Characters
            length += len;

            return this;
        }

        public LargeVarcharBuilder write(Reader reader) throws IOException {
            // Read in 256 byte blocks
            char[] buf = new char[BUFFER_SIZE];

            // Read chunk
            int len = reader.read(buf);

            // Read and Write to the end
            while (len != -1) {
                // Write Buffer
                write(buf, 0, len);

                // Read next chunk
                len = reader.read(buf);
            }

            return this;
        }
    }

    public LargeVarchar() {
        super();
    }

    protected LargeVarchar(Object value) {
        super(value);
    }

    protected LargeVarchar(LargeVarchar source) {
        super(source);
    }

    public Writer exportTo(Writer writer) throws IOException, SQLException {
        copyCharacters(getReader(), writer);
        writer.flush();
        return writer;
    }

    public StringBuffer exportTo(StringBuffer buffer) throws IOException, SQLException {
        copyCharacters(getReader(), buffer);
        return buffer;
    }

    public StringBuilder exportTo(StringBuilder buffer) throws IOException, SQLException {
        copyCharacters(getReader(), buffer);
        return buffer;
    }

    public Clob exportTo(Clob clob) throws IOException, SQLException {
        Writer writer = clob.setCharacterStream(1L);
        exportTo(writer);
        writer.close();
        return clob;
    }

    public File exportTo(File file) throws IOException, SQLException {
        exportToObject(file);
        return file;
    }

    public Object exportTo(Object target) throws IOException, SQLException {
        return exportToObject(target);
    }

    protected Object exportToObject(Object target) throws IOException, SQLException {
        if (target instanceof Writer) {
            return exportTo((Writer)target);
        } else if (target instanceof Clob) {
            return exportTo((Clob)target);
        } else if (target instanceof File) {
            return exportTo(new FileWriter((File)target));
        } else if (target instanceof StringBuilder) {
            return exportTo((StringBuilder)target);
        } else if (target instanceof StringBuffer) {
            return exportTo((StringBuffer)target);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public abstract Reader getReader() throws IOException, SQLException;

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = (this == obj);

        try {
            if (!isEqual && obj instanceof LargeVarchar) {
                LargeVarchar objLargeVarchar = (LargeVarchar)obj;
                isEqual = (getLength() == objLargeVarchar.getLength() && 
                           ((getValue() == null && objLargeVarchar.getValue() == null) ||
                            (getValue() != null && objLargeVarchar.getValue() != null && compareData(getReader(), objLargeVarchar.getReader()))));
            }
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                          e.getLocalizedMessage(), e);
            
        }

        return isEqual;
    }

    @Override
    protected StringValue customStringValue(StringType stringType, int maxLen) {
        switch (stringType) {
            case DEFAULT:
                StringValue ret = null;
        
                try {
                    // Create writer
                    StringWriter stringWriter = new StringWriter();
                    BufferedWriter bufferedWriter = new BufferedWriter(stringWriter);
                    Writer filterWriter = new CappedFilterWriter(bufferedWriter, maxLen);
        
                    // Get Reader
                    Reader reader = getReader();
        
                    // Copy Characters
                    try {
                        copyCharacters(reader, filterWriter, 256);
                    } finally {
                        // Try to make a String Value Anyway
                        try {
                            filterWriter.close();
        
                            ret = new StringValue(stringWriter.toString(), getLength());
                        } catch (IOException e) {
                            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                                       e.getLocalizedMessage(), e);
                        }
        
                        // Close Reader
                        try {
                            reader.close();
                        } catch (IOException e) {
                            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                                       e.getLocalizedMessage(), e);
                        }
                    }
                } catch (SQLException e) {
                    if (ret == null || maxLen < 0 || ret.toString().length() < maxLen) {
                        throw new DataTypeSQLException(e);
                    }
                } catch (IOException e) {
                    if (ret == null || maxLen < 0 || ret.toString().length() < maxLen) {
                        throw new DataTypeIOException(e);
                    }
                }
        
                return ret;
            default:
                return super.customStringValue(stringType, maxLen);
        }
    }

    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target)  throws IOException, SQLException {
        switch (valueType) {
            case TARGET:
                return exportTo(target);
            default:
                return super.customTypedValue(connectionProvider, valueType, target);
        }
    }

    @Override
    public void marshal(DataType datatype, DataValueMarshalHandler hd,
                        String name) throws SAXException {
        try {
            // Get input stream
            Reader reader = getReader();

            // Copy Characters
            try {
                hd.bodyDataValue(datatype, name, reader);
            } finally {
                try {
                    reader.close();
                } catch (IOException e) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                               e.getLocalizedMessage(), e);
                }
            }
        } catch (SQLException e) {
            throw new DataTypeSQLException(e);
        } catch (IOException e) {
            throw new DataTypeIOException(e);
        }
    }

    public static void copyCharacters(Reader reader, Writer writer) throws IOException {
        copyCharacters(reader, writer, BUFFER_SIZE);
    }
    
    public static void copyCharacters(Reader reader, Writer writer, int bufferSize) throws IOException {
        // Read in 256 byte blocks
        char[] buf = new char[bufferSize];
        
        // Read chunk
        int len = reader.read(buf);
        
        // Read and Write to the end
        while (len != -1) {
            // Write Buffer
            writer.write(buf, 0, len);
            
            // Read next chunk
            len = reader.read(buf);
        }
    }
    
    public static void copyCharacters(Reader reader, StringBuffer buffer) throws IOException {
        DataTypesUtil.copyCharacters(reader, buffer, BUFFER_SIZE);
    }
    
    public static void copyCharacters(Reader reader, StringBuffer buffer, int bufferSize) throws IOException {
        DataTypesUtil.copyCharacters(reader, buffer, bufferSize);
    }
    
    public static void copyCharacters(Reader reader, StringBuilder buffer) throws IOException {
        DataTypesUtil.copyCharacters(reader, buffer, BUFFER_SIZE);
    }
    
    public static void copyCharacters(Reader reader, StringBuilder buffer, int bufferSize) throws IOException {
        DataTypesUtil.copyCharacters(reader, buffer, bufferSize);
    }
    
    public static boolean compareData(Reader r1, Reader r2) throws IOException {
        return DataTypesUtil.compareData(r1, r2);
    }
}
