/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;

import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.util.encoding.EncodingException;
import oracle.dbtools.util.encoding.EncodingType;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=LongBinary.java">Barry McGillin</a> 
 *
 */
public class LongBinary extends LargeBinary {
    public class LongBinaryBuilder extends LargeBinaryBuilder {
        protected LongBinaryBuilder(EncodingType encodingType, int cutOverLen) {
            super(encodingType, cutOverLen);
        }

        @Override
        public LongBinary build() throws IOException {
            // Close out the ostream
            switchableStream.flush();
            switchableStream.close();

            // Create LongBinary
            if (getValue() != null && getValue() instanceof File) {
                File file = (File)getValue();

                // Set File Readonly first
                file.setReadOnly();
            } else {
                setValue(((ByteArrayOutputStream)ostream).toByteArray(), false);
            }

            return LongBinary.this;
        }
        
        @Override
        public LongBinaryBuilder write(Reader reader) throws IOException, EncodingException {
            return (LongBinaryBuilder)super.write(reader);
        }

        
        public LongBinaryBuilder write(InputStream istream) throws IOException {
            // Read in 256 byte blocks
            byte[] buf = new byte[BUFFER_SIZE];

            // Read chunk
            int len = istream.read(buf);

            // Read and Write to the end
            while (len != -1) {
                // Write Buffer
                write(buf, 0, len);

                // Read next chunk
                len = istream.read(buf);
            }

            return this;
        }

        @Override
        protected void cutOver() throws IOException {
            // Close Write
            ostream.flush();
            ostream.close();

            // Create ByteArrayInputStream for what has been read
            InputStream sr = new ByteArrayInputStream(((ByteArrayOutputStream)ostream).toByteArray());

            // Create Temporary File
            File tempFile = File.createTempFile("sqldev", ".tmp");
            tempFile.deleteOnExit();

            // Create OutputStream
            OutputStream fw = new FileOutputStream(tempFile);

            // Copy from Binary value to File
            copyBytes(sr, fw);

            // Switch to file
            setValue(tempFile, true);
            ostream = fw;
        }
    }

    public LongBinary() {
        super();
    }

    protected LongBinary(LongBinary source) {
        super(source);
    }

    protected LongBinary(Object object) {
        super(object);
    }
    
    public LongBinary(byte[] value) {
        super(value);
    }

    public LongBinary(File value) {
        super(value);
    }

    public LongBinary(InputStream istream) throws IOException {
        this(istream, CUTOVER_LENGTH);
    }

    public LongBinary(InputStream istream, int cutOverLen) throws IOException {
        this(getBuilder(EncodingType.ENCODING_NONE, cutOverLen).write(istream).build());
    }

    public LongBinary(CharSequence hex) throws IOException, EncodingException {
        this(hex, CUTOVER_LENGTH);
    }

    public LongBinary(CharSequence hex, int cutOverLen) throws IOException, EncodingException {
        this(getBuilder(EncodingType.ENCODING_HEX, cutOverLen).write(new StringReader(hex.toString())).build());
    }

    public LongBinary(char[] chars) throws IOException, EncodingException {
        this(chars, CUTOVER_LENGTH);
    }

    public LongBinary(char[] chars, int cutOverLen) throws IOException, EncodingException {
        this(getBuilder(EncodingType.ENCODING_HEX, cutOverLen).write(new CharArrayReader(chars)).build());
    }

    public LongBinary(Reader reader) throws IOException, EncodingException {
        this(reader, CUTOVER_LENGTH);
    }

    public LongBinary(Reader reader, int cutOverLen) throws IOException, EncodingException {
        this(getBuilder(EncodingType.ENCODING_HEX, cutOverLen).write(reader).build());
    }
    
    public static LongBinary constructFrom(Object value) throws IOException, SQLException, EncodingException {
        return constructFrom(value, CUTOVER_LENGTH);
    }
    
    public static LongBinary constructFrom(Object value, int cutOverLen) throws IOException, SQLException, EncodingException {
        if (value instanceof byte[]) {
            return new LongBinary((byte[])value);
        } else if (value instanceof InputStream) {
            return new LongBinary((InputStream)value, cutOverLen);
        } else if (value instanceof File) {
            return new LongBinary((File)value);
        } else if (value instanceof char[]) {
            return new LongBinary((char[])value, cutOverLen);
        } else if (value instanceof CharSequence) {
            return new LongBinary((CharSequence)value, cutOverLen);
        } else if (value instanceof Reader) {
            return new LongBinary((Reader)value, cutOverLen);
        } else if (value instanceof oracle.sql.Datum) {
            oracle.sql.Datum datumValue = (oracle.sql.Datum)value;
            return new LongBinary(datumValue.stringValue(), cutOverLen);
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    @Override
    public long getLength() throws SQLException {
        if (getValue() instanceof File) {
            return ((File)getValue()).length();
        } else if (getValue() instanceof byte[]) {
            return ((byte[])getValue()).length;
        } else {
            return 0L;
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = (this == obj);

        if (!isEqual && obj instanceof LongBinary) {
            isEqual = super.equals(obj);
        }

        return isEqual;
    }

    @Override
    protected void customFinalize(Object value, boolean deleteOnFinalize) throws Throwable {
        try {
            if (deleteOnFinalize && value instanceof File) {
                ((File)value).delete();
            }
        } finally {
            super.customFinalize(value, deleteOnFinalize);
        }
    }

    @Override
    public InputStream getInputStream() throws IOException, SQLException {
        if (getValue() instanceof File) {
            return new FileInputStream((File)getValue());
        } else if (getValue() instanceof byte[]) {
            return new ByteArrayInputStream((byte[])getValue());
        } else {
            return null;
        }
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target)  throws IOException, SQLException {
        switch (valueType) {
            case JDBC:
                return getInputStream();
            default:
                return super.customTypedValue(connectionProvider, valueType, target);
        }
    }
    
    public static LongBinaryBuilder getBuilder() {
        return getBuilder(CUTOVER_LENGTH);
    }

    public static LongBinaryBuilder getBuilder(EncodingType encoding, int cutOverLen) {
        return new LongBinary().getBuilder0(encoding, cutOverLen);
    }

    public static LongBinaryBuilder getBuilder(EncodingType encoding) {
        return getBuilder(encoding, CUTOVER_LENGTH);
    }

    public static LongBinaryBuilder getBuilder(int cutOverLen) {
        return new LongBinary().getBuilder0(EncodingType.ENCODING_BASE64, cutOverLen);
    }

    private LongBinaryBuilder getBuilder0(EncodingType encoding, int cutOverLen) {
        return new LongBinaryBuilder(encoding, cutOverLen);
    }
}
