/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.ValueType;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=LongVarchar.java">Barry McGillin</a> 
 *
 */
public class LongVarchar extends LargeVarchar {
    public class LongVarcharBuilder extends LargeVarcharBuilder {
        protected LongVarcharBuilder(int cutOverLen) {
            super(new StringWriter(), cutOverLen);
        }
        
        @Override
        public LongVarcharBuilder write(Reader reader) throws IOException {
            return (LongVarcharBuilder)super.write(reader);
        }

        @Override
        public LongVarchar build() throws IOException {
            // Close out the writer
            writer.flush();
            writer.close();
            
            // Create LongVarchar
            if (getValue() != null && getValue() instanceof File) {
                File file = (File)getValue();
                
                // Set File Readonly first
                file.setReadOnly();
            } else {
                setValue(writer.toString(), false);
            }
            
            // Nullify Writer
            writer = null;
            
            return LongVarchar.this;
        }

        @Override
        protected void cutOver() throws IOException {
            // Close Write
            writer.flush();
            writer.close();
            
            // Create StringReader for what has been read
            Reader sr = new StringReader(writer.toString());
            
            // Create Temporary File
            File tempFile = File.createTempFile("sqldev", ".tmp");
            tempFile.deleteOnExit();
            
            // Create Writer
            Writer fw = new FileWriter(tempFile);
            
            // Copy from String to File
            copyCharacters(sr, fw);
            
            // Flush
            fw.flush();
            
            // Switch to file
            setValue(tempFile, true);
            writer = fw;
        }
    }
    
    public LongVarchar() {
        super();
    }
    
    protected LongVarchar(LongVarchar source) {
        super(source);
    }
    
    protected LongVarchar(Object object) {
        super(object);
    }
    
    public LongVarchar(CharSequence value) {
        super(value.toString());
    }
    
    public LongVarchar(File value) {
        super(value);
    }
    
    public LongVarchar(Reader reader) throws IOException {
        this(new LongVarchar().getBuilder().write(reader).build());
    }
    
    public LongVarchar(Reader reader, int cutOverLen) throws IOException {
        this(new LongVarchar().getBuilder(cutOverLen).write(reader).build());
    }
    
    public static LongVarchar constructFrom(Object value) throws IOException, SQLException {
        return constructFrom(value, CUTOVER_LENGTH);
    }
    
    public static LongVarchar constructFrom(Object value, int cutOverLen) throws IOException, SQLException {
        if (value instanceof char[]) {
            return new LongVarchar(String.valueOf((char[])value));
        } else if (value instanceof CharSequence) {
            return new LongVarchar((CharSequence)value);
        } else if (value instanceof File) {
            return new LongVarchar((File)value);
        } else if (value instanceof Reader) {
            return new LongVarchar((Reader)value, cutOverLen);
        } else if (value instanceof oracle.sql.Datum) {
            oracle.sql.Datum datumValue = (oracle.sql.Datum)value;
            return new LongVarchar(datumValue.stringValue());
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    @Override
    public long getLength() throws SQLException {
        if (getValue() instanceof File) {
            return ((File)getValue()).length();
        } else if (getValue() instanceof CharSequence) {
            return ((CharSequence)getValue()).length();
        } else  {
            return 0L;
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = (this == obj);

        if (!isEqual && obj instanceof LongVarchar) {
            isEqual = super.equals(obj);
        }

        return isEqual;
    }
    
    @Override
    public Reader getReader() throws IOException, SQLException {
        if (getValue() instanceof File) {
            return new FileReader((File)getValue());
        } else if (getValue() instanceof CharSequence) {
            return new StringReader(((CharSequence)getValue()).toString());
        } else {
            return null;
        }
    }

    
    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target)  throws IOException, SQLException {
        switch (valueType) {
            case JDBC:
                return getReader();
            default:
                return super.customTypedValue(connectionProvider, valueType, target);
        }
    }
    
    public static LongVarcharBuilder getBuilder() {
        return getBuilder(CUTOVER_LENGTH);
    }

    public static LongVarcharBuilder getBuilder(int cutOverLen) {
        return new LongVarchar().getBuilder0(cutOverLen);
    }

    private LongVarcharBuilder getBuilder0(int cutOverLen) {
        return new LongVarcharBuilder(cutOverLen);
    }

    @Override
    protected void customFinalize(Object value, boolean deleteOnFinalize) throws Throwable {
        try {
            if (deleteOnFinalize && value instanceof File) {
                ((File)value).delete();
            }
        } finally {
            super.customFinalize(value, deleteOnFinalize);
        }
    }
}
