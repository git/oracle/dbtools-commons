/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import oracle.dbtools.raptor.datatypes.objects.OraTimestampDatum.OraTimestampDatumImpl;
import oracle.dbtools.raptor.nls.FormatType;
import oracle.dbtools.raptor.nls.OraDATEFormat;
import oracle.dbtools.raptor.nls.OracleNLSProvider;
import oracle.i18n.util.OraLocaleInfo;
import oracle.sql.DATE;

/**
 * Implements the reconstruction and decomposition of an Oracle DATE
 */
public class OraDATE extends OraResolvableDatum {
    /**
     * Constructs from time now. Resulting value is presented in UTC time zone
     * @return new OraDATE
     */
    public static OraDATE getInstance() {
        return getInstance(LocalDateTime.now());
    }

    /**
     * Constructs local from time now. Resulting value is presented in default time zone
     * @return new OraDATE
     */
    public static OraDATE getLocalInstance() {
        return getLocalInstance(Instant.now());
    }

    /**
     * Constructs from a Calendar. Resulting value is presented in default time zone
     * @param calendar calendar value 
     * @return new OraDATE or null if calendar is null
     */
    @Deprecated
    public static OraDATE getInstance(Calendar calendar) {
        return (calendar != null) ? new OraDATE(new OraDATEImpl(calendar.toInstant())) : null;
    }
    
    /**
     * Constructs from datetime. Resulting value is presented in the default time zone
     * @param datetime
     * @return new OraDATE or null if datetime is null
     */
    public static OraDATE getLocalInstance(DATE datetime) {
        return (datetime != null) ? new OraDATE(new OraDATEImpl(datetime, Realization.LOCAL_INSTANT)) 
                                  : null;
    }
    
    public static OraDATE getLocalInstance(LocalDateTime datetime) {
        return (datetime != null) ? new OraDATE(new OraDATEImpl(datetime, Realization.LOCAL_INSTANT)) 
                                  : null;
    }
    
    public static OraDATE getLocalInstance(Instant instant) {
        return (instant != null) ? new OraDATE(new OraDATEImpl(instant, Realization.LOCAL_INSTANT)) 
                                 : null;
    }
    
    /**
     * Constructs from datetime. Resulting value is presented in the UTC time zone
     * @param datetime
     * @return new OraDATE or null if datetime is null
     */
    public static OraDATE getInstance(DATE datetime) {
        return (datetime != null) ? new OraDATE(new OraDATEImpl(datetime)) 
                : null;
    }
    
    public static OraDATE getInstance(LocalDateTime datetime) {
        return (datetime != null) ? new OraDATE(new OraDATEImpl(datetime)) 
                : null;
    }
    
    /**
     * Constructs from instant. Resulting value is presented in the default time zone
     * @param datetime
     * @return new OraDate or null if datetime is null
     */
    public static OraDATE getInstance(Date datetime) {
        return (datetime != null) ? getInstance(datetime.toInstant())
                              : null;
    }
    
    public static OraDATE getInstance(Instant instant) {
        return (instant != null) ? new OraDATE(new OraDATEImpl(instant)) 
                : null;
    }
    
    /**
     * Constructs from another Temporal Datum
     * @param temporalDatum
     * @return new OraDATE or null if temporalDatum is null
     */
    public static OraDATE getInstance(final OraTemporalDatum temporalDatum) {
        OraDATE d;
        
        if (temporalDatum != null) {
            d = getInstance();
            d.replicate(temporalDatum);
        } else {
        	d = null;
        }
        
        return d;
    }
    
    protected OraDATE(OraDATEImpl impl) {
        super(impl);
    }
    
    @Override
    protected OraDATEImpl getImpl() {
        return (OraDATEImpl)super.getImpl();
    }
    
    @Override
    public OraDATE setSessionTimeZone(TimeZone sessionTimeZone) {
        super.setSessionTimeZone(sessionTimeZone);
    	return this;
    }
    
    @Override
    public OraDATE setSessionZone(ZoneId zone) {
        super.setSessionZone(zone);
        return this;
    }
    
    @Override
    public DATE getDatum() {
    	return (DATE)super.getDatum();
    }

    @Override
    public String toString() {
        try {
            OraDATEFormat formatter = new OraDATEFormat(new OracleNLSProvider(null).getDateFormat(FormatType.GENERIC), 
                                                        OraLocaleInfo.getInstance(Locale.US));
            return formatter.format(this.getDatum());
        } catch (Exception e) {
            return super.toString();
        }
    }

    protected final static class OraDATEImpl extends OraResolvableDatumImpl implements OraTimestampDatumImpl {
        protected OraDATEImpl(LocalDateTime datetime, Realization realization) {
            super(datetime, realization);
            setFractionalPrecision(0);
        }
        
        protected OraDATEImpl(Instant instant, Realization realization) {
            super(instant, realization);
            setFractionalPrecision(0);
        }
        
        protected OraDATEImpl(Instant instant) {
            this(instant, Realization.INSTANT);
            setFractionalPrecision(0);
        }
        
        protected OraDATEImpl(LocalDateTime datetime) {
            this(datetime, Realization.LOCAL);
        }
        
        private OraDATEImpl(DATE datetime, Realization realization) {
            this(unpackTemporalDatum(datetime.shareBytes()), realization);
        }
        
        private OraDATEImpl(DATE datetime) {
            this(datetime, Realization.LOCAL);
        }
        
        @Override
        public boolean nanosSupported() {
            return false;
        }

        @Override
        public DATE getDatum() {
            // Create Datum byte[]
            byte[] bytes = new byte[SIZE_DATE];
            
            // Pack Datum
            packDatumBytes(bytes);
            
            // Create Oracle-typed value
            return new DATE(bytes);
        }
    }
}
