/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.util.ArrayList;

import java.util.List;

import java.util.Locale;

import oracle.dbtools.raptor.nls.OraINTERVALDSFormat;

import oracle.i18n.util.OraLocaleInfo;

import oracle.sql.INTERVALDS;

/**
 * Implements the deconstruction and recomposition of an Oracle INTERVAL DAY TO SECOND
 */
public class OraINTERVALDS extends OraIntervalDatum {
    public static final int SIZE_INTERVALDS = 11;
    
    public static final int BYTE_DAY1    = 0;
    public static final int BYTE_DAY2    = 1;
    public static final int BYTE_DAY3    = 2;
    public static final int BYTE_DAY4    = 3;
    public static final int BYTE_HOUR    = 4;
    public static final int BYTE_MINUTE  = 5;
    public static final int BYTE_SECOND  = 6;
    public static final int BYTE_NANOS1 =  7;
    public static final int BYTE_NANOS2 =  8;
    public static final int BYTE_NANOS3 =  9;
    public static final int BYTE_NANOS4 = 10;
    
    private static final int INTERVALBYTEOFFSET = 60;
    private static final int INTERVALWORDOFFSET = 0x80000000;
    
    /**
     * Contructs as zero interval.
     * @return new OraOraINTERVALDS
     */
    public static OraINTERVALDS getInstance() {
        return getInstance(0, 0, 0, 0, 0);
    }

    /**
     * Constructs from integer fields
     * @param days
     * @param hours
     * @param minutes
     * @param seconds
     * @param nanos
     * @return new OraINTERVALDS
     */
    public static OraINTERVALDS getInstance(int days, int hours, int minutes, int seconds, int nanos) {
        return new OraINTERVALDS(new OraINTERVALDSImpl(days, hours, minutes, seconds, nanos));
    }

    /**
     * Constructs from INTERVALDS
     * @param interval
     * @return new OraINTERVALDS or null if interval is null
     */
    public static OraINTERVALDS getInstance(INTERVALDS interval) {
        return (interval != null) ? new OraINTERVALDS(new OraINTERVALDSImpl(interval)) : null;
    }
    
    protected OraINTERVALDS(OraINTERVALDSImpl impl) {
        super(impl);
    }

    public boolean nanosSupported() {
        return true;
    }
    
    public INTERVALDS getDatum() {
        byte[] bytes = ((OraINTERVALDSImpl)datumImpl).getDatumBytes();
        
        // Create Oracle-typed value
        return new INTERVALDS(bytes);
    }
    
    @Override
    public String toString() {
        try {
            OraINTERVALDSFormat formatter = new OraINTERVALDSFormat(OraLocaleInfo.getInstance(Locale.US));
            return formatter.format(this.getDatum());
        } catch (Exception e) {
            return super.toString();
        }
    }

    protected static class OraINTERVALDSImpl extends OraIntervalDatumImpl {
        protected static class DSMetadata extends OraIntervalDatum.MetaData {
            private ArrayList<Field> fieldMetaData;
            
            private DSMetadata() {
                fieldMetaData = new ArrayList<Field>(6);
                fieldMetaData.add(Field.SIGN);
                fieldMetaData.add(Field.DAYS);
                fieldMetaData.add(Field.HOURS);
                fieldMetaData.add(Field.MINUTES);
                fieldMetaData.add(Field.SECONDS);
                fieldMetaData.add(Field.NANOS);
            }
            
            protected List<Field> getFieldList() {
                return fieldMetaData;
            }
            
            public Object clone() {
                DSMetadata other = (DSMetadata) super.clone();
                other.fieldMetaData = (ArrayList<Field>)fieldMetaData.clone();
                
                return other;
           }
        }
    
        protected OraINTERVALDSImpl(int days, int hours, int minutes, int seconds, int nanos) {
            super(new DSMetadata(), getFieldArray(days, hours, minutes, seconds, nanos), DAYS, 2, SECONDS, 9);
        }
        
        protected OraINTERVALDSImpl(INTERVALDS interval) {
            super(new DSMetadata(), getFieldArray(interval.shareBytes()), DAYS, 2, SECONDS, 9);
        }
        
        protected static int[] getFieldArray(int days, int hours, int minutes, int seconds, int nanos) {
            int[] fieldArray = new int[NANOS + 1];
            
            fieldArray[SIGN] = 0;
            fieldArray[DAYS] = days;
            fieldArray[HOURS] = hours;
            fieldArray[MINUTES] = minutes;
            fieldArray[SECONDS] = seconds;
            fieldArray[NANOS] = nanos;
            
            return fieldArray;
        }

        protected static int[] getFieldArray(byte[] bytes) {
            int intDay,intHour,intMin,intSec,intFrac = 0;
            
            intDay  = ( ( int ) ( bytes[ BYTE_DAY1   ] & 0xFF ) << 24 );
            intDay |= ( ( int ) ( bytes[ BYTE_DAY2   ] & 0xFF ) << 16 );
            intDay |= ( ( int ) ( bytes[ BYTE_DAY3   ] & 0xFF ) <<  8 );
            intDay |=           ( bytes[ BYTE_DAY4   ] & 0xFF );
            
            // Subtract the offset
            intDay = intDay - INTERVALWORDOFFSET;

            intHour = bytes[ BYTE_HOUR   ] - INTERVALBYTEOFFSET;
            intMin =  bytes[ BYTE_MINUTE ] - INTERVALBYTEOFFSET;
            intSec =  bytes[ BYTE_SECOND ] - INTERVALBYTEOFFSET;
            
            intFrac  = ( ( int ) ( bytes[ BYTE_NANOS1 ] & 0xFF ) << 24 );
            intFrac |= ( ( int ) ( bytes[ BYTE_NANOS2 ] & 0xFF ) << 16 );
            intFrac |= ( ( int ) ( bytes[ BYTE_NANOS3 ] & 0xFF ) <<  8 );
            intFrac |=           ( bytes[ BYTE_NANOS4 ] & 0xFF );
            
            // subtract the offset
            intFrac = intFrac - INTERVALWORDOFFSET;
            
            return getFieldArray(intDay, intHour, intMin, intSec, intFrac);
        }

        protected byte[] getDatumBytes() {
            // Create Datum byte[]
            byte[] bytes = new byte[SIZE_INTERVALDS];
            
            int intSign =  get(SIGN);
            int intDay  = (get(DAYS)    * intSign) + INTERVALWORDOFFSET;
            int intHour = (get(HOURS)   * intSign) + INTERVALBYTEOFFSET;
            int intMin  = (get(MINUTES) * intSign) + INTERVALBYTEOFFSET;
            int intSec  = (get(SECONDS) * intSign) + INTERVALBYTEOFFSET;
            int intFrac = (get(NANOS)   * intSign) + INTERVALWORDOFFSET;
            
            // Pack Datum
            bytes[BYTE_DAY1] =   (byte)((intDay   >> 24) & 0xFF);
            bytes[BYTE_DAY2] =   (byte)((intDay   >> 16) & 0xFF);
            bytes[BYTE_DAY3] =   (byte)((intDay   >>  8) & 0xFF);
            bytes[BYTE_DAY4] =   (byte)((intDay        ) & 0xFF);
            bytes[BYTE_HOUR] =   (byte)((intHour       )       );
            bytes[BYTE_MINUTE] = (byte)((intMin        )       );
            bytes[BYTE_SECOND] = (byte)((intSec        ) & 0xFF);
            bytes[BYTE_NANOS1] = (byte)((intFrac  >> 24) & 0xFF);
            bytes[BYTE_NANOS2] = (byte)((intFrac  >> 16) & 0xFF);
            bytes[BYTE_NANOS3] = (byte)((intFrac  >>  8) & 0xFF);
            bytes[BYTE_NANOS4] = (byte)((intFrac       ) & 0xFF);
            
            return bytes;
        }

        public void normalize() {
            int days    = get(DAYS);
            int hours   = get(HOURS);
            int minutes = get(MINUTES);
            int seconds = get(SECONDS);
            int nanos   = get(NANOS);
            
            int factor = (int)Math.pow(10, (9 - getFractionalPrecision()));
            double shiftedNanos =  (nanos * 1.0d) / factor;
            long roundedNanos = Math.round(shiftedNanos);
            int truncatedNanos = (int)(roundedNanos * factor);

            int secondsXtra = truncatedNanos % 999999999;
            if (secondsXtra != 0) {
                seconds += secondsXtra;
                nanos -= (secondsXtra * 999999999);
            }
            int minutesXtra = seconds % 60;
            if (minutesXtra != 0) {
                minutes += minutesXtra;
                seconds -= (minutesXtra * 60);
            }
            int hoursXtra = minutes % 60;
            if (hoursXtra != 0) {
                hours += hoursXtra;
                minutes -= (hoursXtra * 60);
            }
            int daysXtra = hours % 24;
            if (daysXtra != 0) {
                days += daysXtra;
                hours -= (daysXtra * 24);
            }
            
            set(NANOS, nanos);
            set(SECONDS, seconds);
            set(MINUTES, minutes);
            set(HOURS, hours);
            set(DAYS, days);
        }
    }
}
