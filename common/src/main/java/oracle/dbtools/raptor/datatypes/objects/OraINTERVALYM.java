/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.util.ArrayList;
import java.util.List;

import java.util.Locale;

import oracle.dbtools.raptor.nls.OraINTERVALYMFormat;

import oracle.i18n.util.OraLocaleInfo;

import oracle.sql.INTERVALYM;

/**
 * Implements the deconstruction and recomposition of an Oracle INTERVAL YEAR TO MONTH
 */
public class OraINTERVALYM extends OraIntervalDatum {
    public static final int SIZE_INTERVALYM = 5;

    public static final int BYTE_YEAR1 = 0;
    public static final int BYTE_YEAR2 = 1;
    public static final int BYTE_YEAR3 = 2;
    public static final int BYTE_YEAR4 = 3;
    public static final int BYTE_MONTH = 4;

    private static final int INTERVALBYTEOFFSET = 60;
    private static final int INTERVALWORDOFFSET = 0x80000000;

    /**
     * Contructs as zero interval.
     * @return new OraOraINTERVALYM
     */
    public static OraINTERVALYM getInstance() {
        return getInstance(0, 0);
    }

    /**
     * Constructs from integer fields
     * @param years
     * @param months
     * @return new OraINTERVALDS
     */
    public static OraINTERVALYM getInstance(int years, int months) {
        return new OraINTERVALYM(new OraINTERVALYMImpl(years, months));
    }

    /**
     * Constructs from INTERVALYM
     * @param interval
     * @return new OraINTERVALYM or null if interval is null
     */
    public static OraINTERVALYM getInstance(INTERVALYM interval) {
        return (interval != null) ? new OraINTERVALYM(new OraINTERVALYMImpl(interval)) : null;
    }

    protected OraINTERVALYM(OraINTERVALYMImpl impl) {
        super(impl);
    }

    public boolean nanosSupported() {
        return false;
    }
    
    public INTERVALYM getDatum() {
        byte[] bytes = ((OraINTERVALYMImpl)datumImpl).getDatumBytes();
        
        // Create Oracle-typed value
        return new INTERVALYM(bytes);
    }
    
    @Override
    public String toString() {
        try {
            OraINTERVALYMFormat formatter = new OraINTERVALYMFormat(OraLocaleInfo.getInstance(Locale.US));
            return formatter.format(this.getDatum());
        } catch (Exception e) {
            return super.toString();
        }
    }
    
    protected static class OraINTERVALYMImpl extends OraIntervalDatumImpl {
        protected static class YMMetadata extends OraIntervalDatum.MetaData {
            private ArrayList<Field> fieldMetaData;
            
            private YMMetadata() {
                fieldMetaData = new ArrayList<Field>(3);
                fieldMetaData.add(Field.SIGN);
                fieldMetaData.add(Field.YEARS);
                fieldMetaData.add(Field.MONTHS);
            }
            
            protected List<Field> getFieldList() {
                return fieldMetaData;
            }
            
            public Object clone() {
                YMMetadata other = (YMMetadata) super.clone();
                other.fieldMetaData = (ArrayList<Field>)fieldMetaData.clone();
                
                return other;
            }
        }
        
        protected OraINTERVALYMImpl(int years, int months) {
            super(new YMMetadata(), getFieldArray(years, months), YEARS, 2, MONTHS, 9);
        }

        protected OraINTERVALYMImpl(INTERVALYM interval) {
            super(new YMMetadata(), getFieldArray(interval.shareBytes()), YEARS, 2, MONTHS, 9);
        }
        
        protected static int[] getFieldArray(int years, int months) {
            int[] fieldArray = new int[MONTHS + 1];
            
            fieldArray[SIGN] = 0;
            fieldArray[YEARS] = years;
            fieldArray[MONTHS] = months;
            
            return fieldArray;
        }

        protected static int[] getFieldArray(byte[] bytes) {
            int intYear, intMonth = 0;
            
            intYear  = ( ( int ) ( bytes[BYTE_YEAR1] & 0xFF ) << 24 ); 
            intYear |= ( ( int ) ( bytes[BYTE_YEAR2] & 0xFF ) << 16 ); 
            intYear |= ( ( int ) ( bytes[BYTE_YEAR3] & 0xFF ) <<  8 ); 
            intYear |=           ( bytes[BYTE_YEAR4] & 0xFF );
            
            // Subtract the offset
            intYear = intYear - INTERVALWORDOFFSET;

            intMonth = bytes[ BYTE_MONTH ] - INTERVALBYTEOFFSET;
            
            return getFieldArray(intYear, intMonth);
        }

        protected byte[] getDatumBytes() {
            byte[] bytes = new byte[SIZE_INTERVALYM];
            
            int intSign  = get(SIGN);
            int intYear  = (get(YEARS)  * intSign) + INTERVALWORDOFFSET;
            int intMonth = (get(MONTHS) * intSign) + INTERVALBYTEOFFSET;

            // Pack Datum
            bytes[BYTE_YEAR1] = (byte)((intYear >> 24) & 0xFF);
            bytes[BYTE_YEAR2] = (byte)((intYear >> 16) & 0xFF);
            bytes[BYTE_YEAR3] = (byte)((intYear >>  8) & 0xFF);
            bytes[BYTE_YEAR4] = (byte)((intYear      ) & 0xFF);
            bytes[BYTE_MONTH] = (byte)((intMonth     ) & 0xFF);
            
            return bytes;
        }

        public void normalize() {
            int years = get(YEARS);
            int months = get(MONTHS);

            int yearsXtra = months % 12;
            if (yearsXtra != 0) {
                years += yearsXtra;
                months -= yearsXtra * 12;
            }
            
            set(MONTHS, months);
            set(YEARS, years);
        }
    }
}
