/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.beans.PropertyChangeListener;

import java.beans.PropertyChangeSupport;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;

import oracle.sql.Datum;

/**
 * Abstract Class implementing majority of Interval functionality
 */
public abstract class OraIntervalDatum implements Cloneable, Comparable<OraIntervalDatum> {
    public final static int ALL_FIELDS = 255;
    public final static int NULL_FIELD =  -1;
    public final static int SIGN    =      0;
    public final static int YEARS   =      1;
    public final static int MONTHS  =      2;
    public final static int DAYS    =      1;
    public final static int HOURS   =      2;
    public final static int MINUTES =      3;
    public final static int SECONDS =      4;
    public final static int NANOS   =      5;

    /**
     * Implements MetaData, normal used for editing
     */
    public abstract static class MetaData implements Cloneable {
        protected abstract List<Field> getFieldList();
        
        public Field[] getFields() {
            return getFieldList().toArray(new Field[getFieldList().size()]);
        }
        
        public Field getField(int ordinal) {
            return getFields()[ordinal];
        }
        
        public Field getField(String id) {
            Field field = Field.valueOf(id);
            return (getFieldList().contains(field)) ? field : null;
        }
        
        public int getFieldOrdinal(Field field) {
            return getFieldList().indexOf(field);
        }
                
        public String getFieldId(Field field) {
            return (getFieldList().contains(field)) ? field.toString() : null;
        }

        public int getFieldOrdinal(String id) {
            return getFieldOrdinal(getField(id));
        }
        
        public String getFieldId(int ordinal) {
            return getFieldId(getField(ordinal));
        }
        
        public Object clone() {
            try {
                return super.clone();
            }
            catch (CloneNotSupportedException e) {
                // this shouldn't happen, since we are Cloneable
                throw new InternalError();
            }
        }
    }

    /**
     * Field MetaData
     */
    public static enum Field {
        SIGN   (0,        1, 1, 1),
        YEARS  (0,999999999, 2, 9),
        MONTHS (0,       11, 2, 2),
        DAYS   (0,999999999, 2, 9),
        HOURS  (0,       23, 2, 2),
        MINUTES(0,       59, 2, 2),
        SECONDS(0,       59, 2, 2),
        NANOS  (0,999999999,-1,-1);
        
        private int minValue;
        private int maxValue;
        private int minDisplayPrecision;
        private int maxDisplayPrecision;
        
        private Field(int minValue, int maxValue, int minDisplayPrecision, int maxDisplayPrecision) {
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.minDisplayPrecision = minDisplayPrecision;
            this.maxDisplayPrecision = maxDisplayPrecision;
        }
        
        public int getMinValue() {
            return minValue;
        }
        
        public int getMaxValue() {
            return maxValue;
        }
        
        public int getMinDisplayPrecision() {
            return minDisplayPrecision;
        }
        
        public int getMaxDisplayPrecision() {
            return maxDisplayPrecision;
        }
    }

    public static final String LEADINGPRECISION = "LEADINGPRECISION";
    public static final String FRACTIONALPRECISION = "FRACTIONALPRECISION";

    protected OraIntervalDatumImpl datumImpl; // $NOT_FINAL_OK: No final to support deep cloning only
    
    protected static boolean areEqual(Object obj1, Object obj2) {
        return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
    }
    
    protected OraIntervalDatum(OraIntervalDatumImpl datumImpl) {
        this.datumImpl = datumImpl;
    }
    
    public final void addPropertyChangeListener(PropertyChangeListener pcl) {
        this.datumImpl.addPropertyChangeListener(pcl);
    }
    
    public final void removePropertyChangeListener(PropertyChangeListener pcl) {
        this.datumImpl.removePropertyChangeListener(pcl);
    }

    public abstract Datum getDatum();
    
    public MetaData getMetaData() {
        return this.datumImpl.getMetaData();
    }

    /**
     * Clear all fields, returning to zero interval
     */
    public void clear() {
        this.datumImpl.clear();
    }

    /**
     * Return field outside of allowed valid range to valid state
     */
    public void normalize() {
        this.datumImpl.normalize();
    }

    /**
     * Get field value
     * @param field
     * @return integer value
     */
    public int get(int field) {
        return this.datumImpl.get(field);
    }

    /**
     * Set field value
     * @param field
     * @param value positive integer value except for sign
     */
    public void set(int field, int value) {
        this.datumImpl.set(field, value);
    }
    
    public int getFromField() {
        return this.datumImpl.getFromField();
    }

    public int getToField() {
        return this.datumImpl.getToField();
    }

    public final int getLeadingPrecision() {
        return this.datumImpl.getLeadingPrecision();
    }
    
    public final void setLeadingPrecision(int precision) {
        this.datumImpl.setLeadingPrecision(precision);
    }
    
    public final int getFractionalPrecision() {
        return this.datumImpl.getFractionalPrecision();
    }
    
    public final void setFractionalPrecision(int precision) {
        this.datumImpl.setFractionalPrecision(precision);
    }
    
    public abstract boolean nanosSupported();
    
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (obj.getClass() != this.getClass()) return false;
        
        OraIntervalDatum another = (OraIntervalDatum)obj;
        
        return areEqual(datumImpl, another.datumImpl);
    }
    
    public int hashCode()
    {
        int hash = 7;
        hash = hash * 31 + (datumImpl == null ? 0 : datumImpl.hashCode());
        return hash;
    }
    
    public int compareTo(OraIntervalDatum another) {
        int ret = 0;
        
        if (this != another) {
            // Compare Calendar
            ret = this.datumImpl.compareTo(another.datumImpl);
        }
        
        return ret;
    }
    
    /**
     * Creates and returns a copy of this object.
     *
     * @return a copy of this object.
     */
    public Object clone()
    {
        try {
            OraIntervalDatum other = (OraIntervalDatum) super.clone();
            other.datumImpl = (OraIntervalDatumImpl)datumImpl.clone();
            
            return other;
        }
        catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError();
        }
    }
    
    public final void replicate(OraIntervalDatum value) {
        if (value.getClass() == this.getClass()) {
            datumImpl.replicate(value.datumImpl);
        }
    }

    public final void setValue(OraIntervalDatum value) {
        if (value.getClass() == this.getClass()) {
            datumImpl.setValue(value.datumImpl);
        }
    }

    protected abstract static class OraIntervalDatumImpl implements Cloneable, Comparable<OraIntervalDatumImpl> {
        protected int[] fields;
        protected MetaData metadata; 

        private int leadingPrecision; // ignored in comparisons
        private int fractionalPrecision; // ignored in comparisons
        private int fromField; // ignored in comparisons
        private int toField; // ignored in comparisons
        
        private PropertyChangeSupport changesSupport;
        
        protected void addPropertyChangeListener(PropertyChangeListener pcl) {
            changesSupport.addPropertyChangeListener(pcl);
        }
        
        protected void removePropertyChangeListener(PropertyChangeListener pcl) {
            changesSupport.removePropertyChangeListener(pcl);
        }

        private void fireFieldChange(int field, int oldValue, int newValue) {
            // Fire Property Change
            changesSupport.firePropertyChange(metadata.getFieldId(field), oldValue, newValue);
        }
        
        private void fireLeadingPrecisionChange(int oldPrecision, int newPrecision) {
            // Fire Property Change
            changesSupport.firePropertyChange(LEADINGPRECISION, oldPrecision, newPrecision);
        }
        
        private void fireFractionalPrecisionChange(int oldFractionPrecision, int newFractionalPrecision) {
            // Fire Property Change
            changesSupport.firePropertyChange(FRACTIONALPRECISION, oldFractionPrecision, newFractionalPrecision);
        }
        
        protected OraIntervalDatumImpl(MetaData metadata, int[] fields, int fromField, int leadingPrecision, int toField, int fractionalPrecision) {
            this.changesSupport = new PropertyChangeSupport(this);
            this.metadata = metadata;

            this.fields = fields;
            this.fromField = fromField;
            this.toField = toField;
            this.leadingPrecision = leadingPrecision;
            this.fractionalPrecision = fractionalPrecision;
            
            set(SIGN, 1);

            // Factor out sign
            for (int i = 1; i < fields.length; i++) {
                int field = fields[i];
                
                if (field == 0) {
                    continue;
                } else if (field < 0) {
                    set(SIGN, -1);
                }
                
                for (int j = i; j < fields.length; j++) {
                    set(j, Math.abs(fields[j]));
                }
            }
        }
        
        public boolean equals(Object obj) {
            if (obj == null)
                return false;
            if (obj == this)
                return true;
            if (obj.getClass() != this.getClass())
                return false;

            OraIntervalDatumImpl another = (OraIntervalDatumImpl)obj;

            return Arrays.equals(fields, another.fields);
        }

        public int compareTo(OraIntervalDatumImpl obj) {
            int ret = 0;

            if (this != obj) {
                // Get sign
                int sign = fields[SIGN];
                int objSign = obj.fields[SIGN];
                
                // Interate field and compare
                for (int i = 1; ret == 0 && i < fields.length; i++) {
                    int thisField = fields[i] * sign;
                    int objField = obj.fields[i] * objSign;
                    
                    // Compare Years
                    if (thisField != objField) {
                        if (thisField < objField) {
                            ret = -1;
                        } else if (thisField > objField) {
                            ret = 1;
                        }
                    }
                }
            }

            return ret;
        }

        public int hashCode() {
            int hash = 7;
            hash = hash * 31 + Arrays.hashCode(fields);
            return hash;
        }

        /**
         * Creates and returns a copy of this object.
         *
         * @return a copy of this object.
         */
        public Object clone() {
            try {
                OraIntervalDatumImpl other = (OraIntervalDatumImpl)super.clone();
                
                other.metadata = (MetaData)metadata.clone();
                other.fields = fields.clone();
                
                other.fromField = fromField;
                other.toField = toField;
                other.leadingPrecision = leadingPrecision;
                other.fractionalPrecision = fractionalPrecision;

                return other;
            } catch (CloneNotSupportedException e) {
                // this shouldn't happen, since we are Cloneable
                throw new InternalError();
            }
        }
        
        public void replicate(OraIntervalDatumImpl impl) {
            // Copy from and top constraints
            fromField = impl.fromField;
            toField = impl.toField;
            
            // Set Precisions
            setLeadingPrecision(getLeadingPrecision());
            setFractionalPrecision(getFractionalPrecision());
            
            setValue(impl);
            
            // no need to replicate metadata
        }
        
        public void setValue(OraIntervalDatumImpl impl) {
            // Copy Fields
            for (int i = 0; i < fields.length; i++) {
                if (i == SIGN || (i >= fromField && i <= toField)) {
                    // Copy Field Value
                    set(i, impl.fields[i]);
                    
                    // Do NANOS is SECONDS does too
                    if (i == SECONDS) {
                        set(NANOS, impl.fields[NANOS]);
                        i++;
                    }
                } else  {
                    // Zero Field Value
                    set(i, 0);
                }
            }
        }
        
        public MetaData getMetaData() {
            return metadata;
        }

        public int getFromField() {
            return fromField;
        }

        public int getToField() {
            return toField;
        }

        public void setLeadingPrecision(int precision) {
            int oldPrecision = this.leadingPrecision;
          
            if (precision != oldPrecision) {
                this.leadingPrecision = precision;
                
                if (oldPrecision > precision) {
                    leading_precision_changed(oldPrecision, precision);
                }
                
                fireLeadingPrecisionChange(oldPrecision, precision);
                
                if (oldPrecision < precision) {
                    leading_precision_changed(oldPrecision, precision);
                }
            }
        }
        
        public final int getLeadingPrecision() {
            return this.leadingPrecision;
        }
        
        protected void leading_precision_changed(int oldPrecision, int precision) {
            // No action at this level
        }
        
        public final void setFractionalPrecision(int precision) {
            int oldPrecision = this.fractionalPrecision;
            
            if (precision != oldPrecision) {
                this.fractionalPrecision = precision;
                
                if (oldPrecision > precision) {
                    fractional_precision_changed(oldPrecision, precision);
                }
                
                fireFractionalPrecisionChange(oldPrecision, precision);
                
                if (oldPrecision < precision) {
                    fractional_precision_changed(oldPrecision, precision);
                }
            }
        }
        
        public final int getFractionalPrecision() {
            return this.fractionalPrecision;
        }
        
        protected void fractional_precision_changed(int oldPrecision, int precision) {
            // No action at this level
        }
        
        protected int get(int field) {
            return fields[field];
        }
        
        protected void set(int field, int newValue) {
            int oldValue = fields[field];
            
            if (oldValue != newValue) {
                fields[field] = newValue;
                
                fireFieldChange(field, oldValue, newValue);
            }
        }
        
        abstract protected byte[] getDatumBytes();

        public void clear() {
            set(SIGN, 1);
            for (int i = 1; i < fields.length; i++) {
                set(i, 0);
            }
        }

        abstract public void normalize();
    }
}
