/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.TimeZone;

public abstract class OraResolvableDatum extends OraTemporalDatum{
    protected OraResolvableDatum(OraResolvableDatumImpl impl) {
        super(impl);
    }
    
    @Override
    protected OraResolvableDatumImpl getImpl() {
        return (OraResolvableDatumImpl)super.getImpl();
    }
    
    @Override
    public OraResolvableDatum setSessionTimeZone(TimeZone sessionTimeZone) {
        super.setSessionTimeZone(sessionTimeZone);
        return this;
    }
    
    @Override
    public OraResolvableDatum setSessionZone(ZoneId zone) {
        super.setSessionZone(zone);
        return this;
    }
    
    protected static abstract class OraResolvableDatumImpl extends OraTemporalDatumImpl{
        private transient Realization realization;
        
        protected static ZoneId getDefaultRealizedZone(Realization realization) {
        	switch (realization) {
        		case LOCAL:
        			return UTC_ID;
        		default:
        			return getDefaultDatumZone();
        	}
        }
	    
	    protected OraResolvableDatumImpl(Instant instant, Realization realization) {
	        super(instant, getDefaultRealizedZone(realization));
	        this.realization = realization;
	    }
	    
	    protected OraResolvableDatumImpl(LocalDateTime ldt, Realization realization) {
	        super(ldt, getDefaultRealizedZone(realization));
	        this.realization = realization;
	    }
	    
	    @Override
	    public Change today() {
			Change change;
			
	    	if (getRealization() == Realization.LOCAL) {
	    		change = with(ZonedDateTime.now(getDefaultDatumZone(getSessionZone())).toLocalDate());
	    	} else {
	    		change = super.today();
        	}
	    	
	    	return change;
        }

	    @Override
	    public Change now() {
			Change change;
			
	    	if (getRealization() == Realization.LOCAL) {
	    		change = with(ZonedDateTime.now(getDefaultDatumZone(getSessionZone())).toLocalDateTime());
	    	} else {
	    		change = super.now();
        	}
	    	
	    	return change;
        }
        
	    @Override
	    public ZonedDateTime getSessionZonedDateTime() {
			if (getRealization() == Realization.LOCAL && getSessionZone() != null) {
				return toLocalDateTime().atZone(getSessionZone());
			} else {
				return super.getSessionZonedDateTime();
			}
	    }
	    
	    @Override
	    public Resolution getResolution() {
	    	if (sessionZoneAdjusted()) {
	    		return Resolution.ZONED;
	    	} else {
	    		return getRealization().getResolution();
	    	}
        }
        
	    @Override
        public boolean sessionZoneAdjusted() {
            return (getRealization() != Realization.LOCAL && getSessionZone() != null);
        }
        
    	protected final void setRealization(Realization realization) {
    		this.realization = realization;
    	}
	    
    	protected final Realization getRealization() {
    		return this.realization;
    	}
	    
        @Override
        public boolean userTimeZoneSupported() {
            return false;
        }
        
        @Override
        public boolean localSupported() {
            return true;
        }
        
    	@Override
    	public Change setSessionZone(ZoneId sessionZone) {
			ZoneId zone = (sessionZone != null) ? sessionZone : getDefaultRealizedZone(getRealization());
			
			Change change;
			switch (getRealization()) {
				case LOCAL:
					change = Change.NONE;
					break;
				case LOCAL_INSTANT:
					change = withZoneSameLocal(zone);
					break;
				default:
					change = withZoneSameInstant(zone);
					break;
			}

            super.setSessionZone(sessionZone);
			
			return change;
        }
    	
        @Override
        public Change replicate(OraTemporalDatumImpl src) {
        	// Initially default to instant
        	this.realization = Realization.INSTANT;
        	
        	Change superChange = super.replicate(src);
        	Change setChange;
        	
        	if (src instanceof OraResolvableDatumImpl) {
        		OraResolvableDatumImpl other =  (OraResolvableDatumImpl)src;
        		setChange = setZonedDateTime(other.getZonedDateTime());
        		this.realization = other.realization;
        	} else {
        		setChange = setZonedDateTime(ZonedDateTime.ofInstant(src.toSessionInstant(), getDefaultDatumZone(getSessionZone())));
        	}
        	
        	return Change.combine(superChange, setChange);
        }
        
        @Override
        public Change setValue(OraTemporalDatumImpl src) {
        	Change change;
        	
        	Resolution tgtRes = getResolution();
        	Resolution srcRes = src.getResolution();
        	
        	switch (tgtRes) {
        		case ZONED:
        		case INSTANT:
	        		switch(srcRes) {
	        			case ZONED:
	        			case INSTANT:
	        				change = with(src.toInstant());
	    	        		break;
	    	        	default:
	            			if (src.getSessionZone() != null) {
	            				change = with(src.toSessionInstant());
	            			} else {
	            				change = with(src.toLocalDateTime());
	            			}
	        		}
	        		break;
	        	default:
	        		change = with(src.toLocalDateTime());
        	}
        	
        	return change;
        }
        
        /**
         * Creates and returns a copy of this object.
         *
         * @return a copy of this object.
         */
    	@Override
        public Object clone()
        {
        	OraResolvableDatumImpl other = (OraResolvableDatumImpl) super.clone();
        	
            other.realization = realization;
            
            return other;
        }
    }
    
    public static enum Realization {
    	LOCAL,
    	LOCAL_INSTANT,
    	INSTANT,
    	LTZ_INSTANT;
    	
    	public Realization getEffectiveRealization() {
    		switch (this) {
				case LOCAL_INSTANT:
					return LOCAL;
    			case LTZ_INSTANT:
    				return INSTANT;
    			default:
    				return this;
    		}
    	}
    	
    	public Resolution getResolution() {
    		switch (getEffectiveRealization()) {
				case LOCAL:
					return Resolution.LOCAL;
    			default:
    				return Resolution.INSTANT;
    		}
    	}
    }
}
