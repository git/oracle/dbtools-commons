/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import oracle.dbtools.raptor.nls.FormatType;
import oracle.dbtools.raptor.nls.OraConversions;
import oracle.dbtools.raptor.nls.OraTIMESTAMPFormat;
import oracle.dbtools.raptor.nls.OracleNLSProvider;
import oracle.i18n.util.OraLocaleInfo;
import oracle.sql.TIMESTAMP;

/**
 * Implements the reconstruction and decomposition of an Oracle TIMESTAMP
 */
public class OraTIMESTAMP extends OraResolvableDatum implements OraTimestampDatum {
    /**
     * Constructs from time now. Resulting value is presented in UTC time zone
     * @return new OraTIMESTAMP
     */
    public static OraTIMESTAMP getInstance() {
        return getInstance(LocalDateTime.now());
    }

    /**
     * Constructs local from time now. Resulting value is presented in default time zone
     * @return new OraTIMESTAMP
     */
    public static OraTIMESTAMP getLocalInstance() {
        return getLocalInstance(Instant.now());
    }

    /**
     * Constructs from a Calendar. Resulting value is presented in the default time zone
     * @param calendar calendar value 
     * @param nanos nanoseconds value 
     * @return new OraTIMESTAMP or null if calendar is null
     */
    @Deprecated
    public static OraTIMESTAMP getInstance(Calendar calendar, Integer nanos) {
        return (calendar != null) ? getInstance(OraConversions.toInstant(calendar, nanos)) 
                                  : null;
    }
    
    @Deprecated
    public static OraTIMESTAMP getInstance(Calendar calendar) {
        return getInstance(calendar, null);
    }
    
    /**
     * Constructs from datetime. Resulting value is presented in the default time zone
     * @param datetime
     * @return new OraTIMESTAMP or null if datetime is null
     */
    public static OraTIMESTAMP getLocalInstance(TIMESTAMP datetime) {
        return (datetime != null) ? new OraTIMESTAMP(new OraTIMESTAMPImpl(datetime, Realization.LOCAL_INSTANT))
                                  : null;
    }
    
    public static OraTIMESTAMP getLocalInstance(LocalDateTime datetime) {
        return (datetime != null) ? new OraTIMESTAMP(new OraTIMESTAMPImpl(datetime, Realization.LOCAL_INSTANT)) 
                                  : null;
    }
    
    public static OraTIMESTAMP getLocalInstance(Instant instant) {
        return (instant != null) ? new OraTIMESTAMP(new OraTIMESTAMPImpl(instant, Realization.LOCAL_INSTANT)) 
                                 : null;
    }
    
    /**
     * Constructs from datetime. Resulting value is presented in the UTC time zone
     * @param datetime
     * @return new OraTIMESTAMP or null if datetime is null
     */
    public static OraTIMESTAMP getInstance(TIMESTAMP datetime) {
        return (datetime != null) ? new OraTIMESTAMP(new OraTIMESTAMPImpl(datetime)) 
                : null;
    }
    
    public static OraTIMESTAMP getInstance(LocalDateTime datetime) {
        return (datetime != null) ? new OraTIMESTAMP(new OraTIMESTAMPImpl(datetime)) 
                : null;
    }
    
   /**
     * Constructs from instant. Resulting value is presented in the default time zone
     * @param datetime
     * @return new OraTIMESTAMP or null if datetime is null
     */
    public static OraTIMESTAMP getInstance(Date datetime) {
        return (datetime != null) ? getInstance(datetime.toInstant())
                              : null;
    }
    
    public static OraTIMESTAMP getInstance(Instant instant) {
        return (instant != null) ? new OraTIMESTAMP(new OraTIMESTAMPImpl(instant)) 
                : null;
    }
    
    /**
     * Constructs from another Temporal Datum
     * @param temporalDatum
     * @return new OraTIMESTAMP or null if temporalDatum is null
     */
    public static OraTIMESTAMP getInstance(final OraTemporalDatum temporalDatum) {
        OraTIMESTAMP ts;
        
        if (temporalDatum != null) {
            ts = getInstance();
            ts.replicate(temporalDatum);
        } else {
        	ts = null;
        }
        
        return ts;
    }
    
    protected OraTIMESTAMP(OraTIMESTAMPImpl impl) {
        super(impl);
    }
    
    @Override
    protected OraTIMESTAMPImpl getImpl() {
        return (OraTIMESTAMPImpl)super.getImpl();
    }
    
    @Override
    public OraTIMESTAMP setSessionTimeZone(TimeZone sessionTimeZone) {
        super.setSessionTimeZone(sessionTimeZone);
        return this;
    }
    
    @Override
    public OraTIMESTAMP setSessionZone(ZoneId zone) {
        super.setSessionZone(zone);
        return this;
    }
    
    @Override
    public TIMESTAMP getDatum() {
    	return (TIMESTAMP)super.getDatum();
    }

    @Override
    public String toString() {
        try {
            OraTIMESTAMPFormat formatter = new OraTIMESTAMPFormat(new OracleNLSProvider(null).getTimeStampFormat(FormatType.GENERIC), 
                                                                  OraLocaleInfo.getInstance(Locale.US));
            return formatter.format(this.getDatum());
        } catch (Exception e) {
            return super.toString();
        }
    }

    protected final static class OraTIMESTAMPImpl extends OraResolvableDatumImpl implements OraTimestampDatumImpl {
        protected OraTIMESTAMPImpl(LocalDateTime datetime, Realization realization) {
            super(datetime, realization);
        }
        
        protected OraTIMESTAMPImpl(Instant instant, Realization realization) {
            super(instant, realization);
        }
        
        protected OraTIMESTAMPImpl(Instant instant) {
            this(instant, Realization.INSTANT);
        }
        
        protected OraTIMESTAMPImpl(LocalDateTime datetime) {
        	this(datetime, Realization.LOCAL);
        }
        
        private OraTIMESTAMPImpl(TIMESTAMP datetime, Realization realization) {
            this(unpackTemporalDatum(datetime.shareBytes()), realization);
        }
        
        private OraTIMESTAMPImpl(TIMESTAMP datetime) {
            this(datetime, Realization.LOCAL);
        }
        
        @Override
        public TIMESTAMP getDatum() {
            // Create Datum byte[]
            byte[] bytes = new byte[SIZE_TIMESTAMP];
            
            // Pack Datum
            packDatumBytes(bytes);
            
            // Create Oracle-typed value
            return new TIMESTAMP(bytes);
        }
    }
}
