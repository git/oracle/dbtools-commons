/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import oracle.dbtools.raptor.nls.FormatType;
import oracle.dbtools.raptor.nls.OraConversions;
import oracle.dbtools.raptor.nls.OraTIMESTAMPLTZFormat;
import oracle.dbtools.raptor.nls.OracleNLSProvider;
import oracle.i18n.util.OraLocaleInfo;
import oracle.sql.TIMESTAMPLTZ;

/**
 * Implements the reconstruction and decomposition of an Oracle TIMESTAMPLTZ
 */
public class OraTIMESTAMPLTZ extends OraResolvableDatum implements OraTimestampDatum {
    /**
     * Constructs from time now. Resulting value is presented in default time zone
     * @return new OraTIMESTAMPLTZ
     */
    public static OraTIMESTAMPLTZ getInstance() {
        return getInstance(Instant.now());
    }

    /**
     * Constructs from time now. Resulting value is presented in default time zone
     * @param dbTimeZone database time zone
     * @return new OraTIMESTAMPLTZ
     */
    public static OraTIMESTAMPLTZ getInstance(TimeZone dbTimeZone) {
    	if (dbTimeZone != null) {
    		return getInstance(dbTimeZone.toZoneId());
    	} else {
    		return getInstance();
    	}
    }

    public static OraTIMESTAMPLTZ getInstance(ZoneId dbZone) {
    	OraTIMESTAMPLTZ instance = getInstance();
    	if (dbZone != null) {
    		instance.setDBZone(dbZone);
    	}
    	return instance;
    }

    /**
     * Constructs from calendar and nanos. Resulting value is presented in default time zone
     * @param calendar
     * @param nanos
     * @return new OraTIMESTAMPLTZ or null if calendar is null
     */
    @Deprecated
    public static OraTIMESTAMPLTZ getInstance(Calendar calendar, Integer nanos) {
        return (calendar != null) ? getInstance(OraConversions.toInstant(calendar, nanos))
                                  : null;
    }

    /**
     * Constructs from calendar. Resulting value is presented in default time zone
     * @param calendar
     * @return new OraTIMESTAMPLTZ or null if calendar is null
     */
    @Deprecated
    public static OraTIMESTAMPLTZ getInstance(Calendar calendar) {
        return getInstance(calendar, null);
    }

    /**
     * Constructs from TIMESTAMPLTZ. Resulting value is presented in the default time zone
     * @param datetime
     * @return new TIMESTAMPLTZ or null if datetime is null
     */
    public static OraTIMESTAMPLTZ getInstance(TIMESTAMPLTZ datetime) {
        return (datetime != null) ? new OraTIMESTAMPLTZ(new OraTIMESTAMPLTZImpl(datetime, Realization.LTZ_INSTANT, null)) 
                : null;
    }
    
    /**
     * Constructs from datetime and db time zone. Resulting value is presented in the default time zone
     * @param datetime
     * @param dbTimeZone database time zone
     * @return new OraTIMESTAMP or null if datetime is null
     */
    public static OraTIMESTAMPLTZ getInstance(TIMESTAMPLTZ datetime, TimeZone dbTimeZone) {
    	if (datetime == null) {
    		return null;
    	} else {
    		if (dbTimeZone != null) {
    			return getInstance(datetime, dbTimeZone.toZoneId());
    		} else {
    			return getInstance(datetime);
    		}
    	}
    }
    
    public static OraTIMESTAMPLTZ getInstance(TIMESTAMPLTZ datetime, ZoneId dbZone) {
    	if (datetime == null) {
    		return null;
    	} else {
    		if (dbZone != null) {
    			return new OraTIMESTAMPLTZ(new OraTIMESTAMPLTZImpl(datetime, Realization.INSTANT, dbZone));
    		} else {
    			return getInstance(datetime);
    		}
    	}
    }
    
    public static OraTIMESTAMPLTZ getInstance(LocalDateTime datetime, ZoneId dbZone) {
    	if (datetime == null) {
    		return null;
    	} else {
    		if (dbZone != null) {
    			return new OraTIMESTAMPLTZ(new OraTIMESTAMPLTZImpl(ZonedDateTime.of(datetime, dbZone).toInstant(), 
    					                                           Realization.INSTANT, 
    					                                           dbZone));
    		} else {
    			return getInstance(datetime);
    		}
    	}
    }
    
    public static OraTIMESTAMPLTZ getInstance(Instant instant) {
        return (instant != null) ? new OraTIMESTAMPLTZ(new OraTIMESTAMPLTZImpl(instant)) 
                : null;
    }
    
    /**
     * Constructs from datetime. Resulting value is presented in the default time zone
     * @param datetime
     * @return new TIMESTAMPLTZ or null if datetime is null
     */
    public static OraTIMESTAMPLTZ getInstance(Date datetime) {
        return (datetime != null) ? getInstance(datetime.toInstant())
                              : null;
    }
    
    public static OraTIMESTAMPLTZ getInstance(LocalDateTime datetime) {
        return (datetime != null) ? new OraTIMESTAMPLTZ(new OraTIMESTAMPLTZImpl(datetime)) 
                : null;
    }
    
    public static OraTIMESTAMPLTZ getInstance(LocalDateTime datetime, TimeZone dbTimeZone) {
    	if (datetime == null) {
    		return null;
    	} else {
    		if (dbTimeZone != null) {
    			return getInstance(datetime, dbTimeZone.toZoneId());
    		} else {
    			return getInstance(datetime);
    		}
    	}
    }
    
    /**
     * Contructs from another Temporal Datum
     * @param temporalDatum
     * @return new OraTIMESTAMPLTZ or null if temporalDatum is null
     */
    public static OraTIMESTAMPLTZ getInstance(final OraTemporalDatum temporalDatum) {
        OraTIMESTAMPLTZ tsLTZ = null;
        
        if (temporalDatum != null) {
            tsLTZ = getInstance();
            tsLTZ.replicate(temporalDatum);
        }
        
        return tsLTZ;
    }
    
    protected OraTIMESTAMPLTZ(OraTIMESTAMPLTZImpl impl) {
        super(impl);
    }
    
    @Override
    protected OraTIMESTAMPLTZImpl getImpl() {
        return (OraTIMESTAMPLTZImpl)super.getImpl();
    }
    
    public final OraTIMESTAMPLTZ setDBTimeZone(TimeZone timezone) {
        return setDBZone((timezone != null) ? timezone.toZoneId() : null);
    }
    
    public final TimeZone getDBTimeZone() {
    	ZoneId zone = getDBZone();
        return (zone != null) ? TimeZone.getTimeZone(zone) : null;
    }
    
    public final OraTIMESTAMPLTZ setDBZone(ZoneId zone) {
        getImpl().setDBZone(zone);
        return this;
    }
    
    public final ZoneId getDBZone() {
        return getImpl().getDBZone();
    }
    
    @Override
    public OraTIMESTAMPLTZ setSessionTimeZone(TimeZone sessionTimeZone) {
        super.setSessionTimeZone(sessionTimeZone);
    	return this;
    }

    @Override
    public OraTIMESTAMPLTZ setSessionZone(ZoneId zone) {
        super.setSessionZone(zone);
        return this;
    }
    
    @Override
    public TIMESTAMPLTZ getDatum() {
    	return (TIMESTAMPLTZ)super.getDatum();
    }

    @Override
    public String toString() {
        try {
            OraTIMESTAMPLTZFormat formatter = new OraTIMESTAMPLTZFormat(new OracleNLSProvider(null).getTimeStampWithTimeZoneFormat(FormatType.GENERIC), 
                                                                        OraLocaleInfo.getInstance(Locale.US),
                                                                        getDBTimeZone(),
                                                                        getSessionTimeZone());
            return formatter.format(this.getDatum());
        } catch (Exception e) {
            return super.toString();
        }
    }

    protected final static class OraTIMESTAMPLTZImpl extends OraResolvableDatum.OraResolvableDatumImpl implements OraTimestampDatumImpl {
        private transient ZoneId dbZone;

        protected OraTIMESTAMPLTZImpl(LocalDateTime datetime, Realization realization, ZoneId dbZone) {
            super(datetime, realization);
            this.dbZone = dbZone;
        }
       
        protected OraTIMESTAMPLTZImpl(Instant instant, Realization realization, ZoneId dbZone) {
            super(instant, realization);
            this.dbZone = dbZone;
        }
       
        private OraTIMESTAMPLTZImpl(TIMESTAMPLTZ datetime, Realization realization, ZoneId dbZone) {
            this(unpackTIMESTAMPLTZ(datetime.shareBytes(), dbZone), realization, dbZone);
        }
        
        protected OraTIMESTAMPLTZImpl(Instant instant) {
            this(instant, Realization.INSTANT, null);
        }
        
        protected OraTIMESTAMPLTZImpl(LocalDateTime datetime) {
        	this(datetime, Realization.LOCAL_INSTANT, null);
        }
        
        public ZoneId getDBZone() {
        	return dbZone;
        }
        
        @Override
        public Change setFractionalPrecision(int precision) {
        	Realization oldRealization = getRealization();
        	try {
	        	return super.setFractionalPrecision(precision);
        	} finally {
            	setRealization(oldRealization);
        	}
		}

		@Override
		public Change setPrecision(Precision precision) {
        	Realization oldRealization = getRealization();
        	try {
        		return super.setPrecision(precision);
        	} finally {
            	setRealization(oldRealization);
        	}
		}

		@Override
		public Change setZonedDateTime(ZonedDateTime newZDT) {
        	Change change = super.setZonedDateTime(newZDT);
        	
        	if (getRealization() == Realization.LTZ_INSTANT) {
        		if (change != Change.NONE && change != Change.ZONE) {
                    setRealization(Realization.INSTANT);
        		}
        	}
        	
        	return change;
        }
        
		public Change setDBZone(ZoneId dbZone) {
        	Change change = Change.NONE;
			
            if (dbZone != null && getRealization() == Realization.LTZ_INSTANT) {
                ZonedDateTime newZDT = getZonedDateTime();
                
                ZoneId savedZI = newZDT.getZone();
                
                // return time zone back to UTC
                newZDT.withZoneSameInstant(UTC_ID);
                
                // overlay DB time zone
                newZDT.withZoneSameLocal(dbZone);
                
                // restore save time zone
                newZDT.withZoneSameInstant(savedZI);
                
                change = setZonedDateTime(newZDT);
                
                setRealization(Realization.INSTANT);
            }

            this.dbZone = dbZone;
            
            return change;
        }
        
        @Override
        public Change replicate(OraTemporalDatumImpl src) {
        	Change change = Change.NONE;
        	
        	// Remember old realization
        	Realization candidate = getRealization();
        	
        	try {
        		change = super.replicate(src);

            	if (src instanceof OraTIMESTAMPLTZImpl) {
            		OraTIMESTAMPLTZImpl other =  (OraTIMESTAMPLTZImpl)src;
            		candidate = other.getRealization();
            		this.dbZone = other.dbZone;
            	} else if (getRealization() == Realization.LOCAL_INSTANT) {
            		candidate = Realization.INSTANT;
            	}
        	} finally {
            	setRealization(candidate);
        	}
        	
        	return change;
        }
        
        /**
         * Creates and returns a copy of this object.
         *
         * @return a copy of this object.
         */
    	@Override
        public Object clone()
        {
    		OraTIMESTAMPLTZImpl other = (OraTIMESTAMPLTZImpl) super.clone();
        	
            other.dbZone = dbZone;
            
            return other;
        }
    	
        @Override
        public TIMESTAMPLTZ getDatum() {
            // Create Datum byte[]
            byte[] bytes = new byte[SIZE_TIMESTAMP];
            
            // Pack Datum
            packDatumBytes(bytes);
            
            // Create Oracle-typed value
            return new TIMESTAMPLTZ(bytes);
        }

        @Override
        protected void packDatumBytes(byte[] bytes) {
            ZoneId zone = (getDBZone() != null) ? getDBZone() : UTC_ID;

            packDatumBytes(bytes, zone);
        }
        
        protected void packDatumBytes(byte[] bytes, ZoneId zone) {
        	ZonedDateTime dbZDT = getZonedDateTime();
        	
        	if (getRealization() != Realization.LOCAL) {
	        	if (getRealization() == Realization.LTZ_INSTANT) {
	        		// unresolved instant
	        		dbZDT = dbZDT.withZoneSameInstant(UTC_ID);
	        	} else {
	        		// instant
	        		dbZDT = dbZDT.withZoneSameInstant(zone);
	        	}
        	}

            packDatumBytes(bytes, dbZDT.toLocalDateTime());
        }
        
        static Instant unpackTIMESTAMPLTZ(byte[] bytes, ZoneId zone) {
        	LocalDateTime ldt = unpackTemporalDatum(bytes);
        	
        	ZonedDateTime zdt = ZonedDateTime.of(ldt, (zone != null) ? zone : UTC_ID);
        	
        	return zdt.toInstant();
        }
    
        static Instant unpackTIMESTAMPLTZ(byte[] bytes) {
        	return unpackTIMESTAMPLTZ(bytes, null);
        }
    }
}
