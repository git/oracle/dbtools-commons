/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import oracle.dbtools.raptor.nls.FormatType;
import oracle.dbtools.raptor.nls.OraConversions;
import oracle.dbtools.raptor.nls.OraTIMESTAMPTZFormat;
import oracle.dbtools.raptor.nls.OracleNLSProvider;
import oracle.i18n.util.OraLocaleInfo;
import oracle.sql.TIMESTAMPTZ;

/**
 * Implements the reconstruction and decomposition of an Oracle TIMESTAMP WITH TIME ZONE
 */
public class OraTIMESTAMPTZ extends OraTemporalDatum implements OraTimestampDatum {

    /**
     * Contructs from time now. Resulting value is presented in the default timezone
     * @return new OraTIMESTAMPTZ
     */
    public static OraTIMESTAMPTZ getInstance() {
        return getInstance(ZonedDateTime.now());
    }

    /**
     * Constructs from time now. Resulting value is presented in the specified timezone
     * @param timeZone
     * @return
     */
    public static OraTIMESTAMPTZ getInstance(TimeZone timeZone) {
    	return getInstance(timeZone.toZoneId());
    }

    public static OraTIMESTAMPTZ getInstance(ZoneId zone) {
        return getInstance(ZonedDateTime.now(zone));
    }

    public static OraTIMESTAMPTZ getInstance(Calendar calendar, Integer nanos) {
        return (calendar != null) ? getInstance(OraConversions.toInstant(calendar, nanos).atZone(calendar.getTimeZone().toZoneId()))
                                  : null;
    }
    
    /**
     * Constructs from a Calendar. Resulting value is presented in calendar's timezone
     * @param calendar calendar value 
     * @return new OraTIMESTAMPTZ or null if calendar is null
     */
    public static OraTIMESTAMPTZ getInstance(Calendar calendar) {
        return getInstance(calendar, null);
    }
    
    /**
     * Constructs from TIMESTAMPTZ. Resulting value is presented in date's timezone
     * @param date
     * @return new OraTIMESTAMPTZ or null if date is null
     */
    public static OraTIMESTAMPTZ getInstance(TIMESTAMPTZ datetime) {
        return (datetime != null) ? new OraTIMESTAMPTZ(new OraTIMESTAMPTZImpl(datetime)) : null;
    }
    
    public static OraTIMESTAMPTZ getInstance(ZonedDateTime datetime) {
        return (datetime != null) ? new OraTIMESTAMPTZ(new OraTIMESTAMPTZImpl(datetime)) : null;
    }
    
    /**
     * Constructs from another Temporal Datum
     * @param temporalDatum
     * @return new OraTIMESTAMPTZ or null if temporalDatum is null
     */
    public static OraTIMESTAMPTZ getInstance(final OraTemporalDatum temporalDatum) {
        OraTIMESTAMPTZ tsTZ;
        
        if (temporalDatum != null) {
            tsTZ = getInstance();
            tsTZ.replicate(temporalDatum);
        } else {
        	tsTZ = null;
        }
        
        return tsTZ;
    }
    
    protected OraTIMESTAMPTZ(OraTIMESTAMPTZImpl impl) {
        super(impl);
    }
  
    @Override
    protected OraTIMESTAMPTZImpl getImpl() {
        return (OraTIMESTAMPTZImpl)super.getImpl();
    }
    
    /**
     * Creates and returns a copy of this object.
     *
     * @return a copy of this object.
     */
    @Override
    public Object clone()
    {
        OraTIMESTAMPTZ other = (OraTIMESTAMPTZ) super.clone();
        
        return other;
    }
    
    public void setTimeZone(TimeZone timezone) {
        setZone(timezone.toZoneId());
    }
    
    public void setZone(ZoneId zone) {
    	getImpl().withZoneSameInstant(zone);
    }
    
    @Override
    public OraTIMESTAMPTZ setSessionTimeZone(TimeZone sessionTimeZone) {
        super.setSessionTimeZone(sessionTimeZone);
    	return this;
    }
    
    @Override
    public OraTIMESTAMPTZ setSessionZone(ZoneId zone) {
        super.setSessionZone(zone);
        return this;
    }
    
    @Override
    public void setCalendar(Calendar calendar, Integer nanos) {
    	ZonedDateTime newZDT = OraConversions.toInstant(calendar, nanos).atZone(calendar.getTimeZone().toZoneId());
    	setZonedDateTime(newZDT);
    }

    public void setZonedDateTime(ZonedDateTime datetime) {
    	getImpl().setZonedDateTime(datetime);
    }
    
    @Override
    public TIMESTAMPTZ getDatum() {
    	return (TIMESTAMPTZ)super.getDatum();
    }

    @Override
    public String toString() {
        try {
            OraTIMESTAMPTZFormat formatter = new OraTIMESTAMPTZFormat(new OracleNLSProvider(null).getTimeStampWithTimeZoneFormat(FormatType.GENERIC), 
                                                                      OraLocaleInfo.getInstance(Locale.US));
            return formatter.format(this.getDatum());
        } catch (Exception e) {
            return super.toString();
        }
    }

    protected final static class OraTIMESTAMPTZImpl extends OraTemporalDatumImpl implements OraTimestampDatumImpl {
        /* Region id bit flag */
        private static final int REGIONIDBIT = 0x80;
    
        /* Excess offsets for time zone */
        private static final byte OFFSET_HOUR = 20;
        private static final byte OFFSET_MINUTE = 60;
    
        private static final String[] digit =
        { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$
    
        private OraTIMESTAMPTZImpl(TIMESTAMPTZ datetime) {
            super(unpackTIMESTAMPTZ(datetime.shareBytes()));
        }
        
        protected OraTIMESTAMPTZImpl(ZonedDateTime datetime) {
            super(datetime);
        }
        
        @Override
        public Resolution getResolution() {
            return Resolution.ZONED;
        }
        
        @Override
        public boolean sessionZoneAdjusted() {
            return false;
        }
        
        @Override
        public boolean userTimeZoneSupported() {
            return true;
        }
        
        @Override
        public boolean localSupported() {
            return false;
        }
        
        @Override
        public Change replicate(OraTemporalDatumImpl src) {
        	Change a = super.replicate(src);
        	Change b = setZonedDateTime(src.getZonedDateTime());
        	return Change.combine(a, b);
        }
        
        @Override
        public Change setValue(OraTemporalDatumImpl src) {
        	Change change;
        	
        	switch(src.getResolution()) {
        		case ZONED:
        			change = setZonedDateTime(src.getZonedDateTime());
        			break;
        		case INSTANT:
        			change = with(src.toInstant());
        			break;
        		default:
        			if (src.getSessionZone() != null) {
        				change = setZonedDateTime(src.getSessionZonedDateTime());
        			} else {
        				change = with(src.toLocalDateTime());
        			}
        			break;
        	}

        	return change;
        }
        
        @Override
        public TIMESTAMPTZ getDatum() {
            // Create Datum byte[]
            byte[] bytes = new byte[SIZE_TIMESTAMPTZ];
            
            packDatumBytes(bytes);
            
            // Create Oracle-typed value
            return new TIMESTAMPTZ(bytes);
        }

       /**
         * Unpacks Oracle Datum byte array into calendar
         *
         * @param bytes raw representation
         */
        
        /*
         * Protected Static Methods
         *
         */
    
        protected static ZonedDateTime unpackTIMESTAMPTZ(byte[] bytes) {
        	LocalDateTime ldt = unpackTemporalDatum(bytes);
        	
            String tzID = unpackTimeZoneID(bytes);
            
            ZoneId zone;
            if (tzID != null) {
            	TimeZone tz = TimeZone.getTimeZone(tzID);
            	zone = tz.toZoneId();
            } else {
            	zone = getDefaultDatumZone();
            }
            
        	return ZonedDateTime.of(ldt, UTC_ID).withZoneSameInstant(zone);
        }
    
        /**
         * Returns Java TimeZone ID of the time zone stored in TIMESTAMPTZ bytes array.
         *
         * @param bytes the TIMESTAMPTZ array.
         *
         * @return Java TimeZone ID. Null if time zone not recognised.
         *
         */
        protected static String unpackTimeZoneID(byte[] bytes) {
            if (bytes.length < SIZE_TIMESTAMPTZ)
                return null;
    
            if ((bytes[BYTE_TIMEZONE1] & REGIONIDBIT) != 0) {
                // Time Zone is a region
                int regID =
                    ((bytes[BYTE_TIMEZONE1] & 0x7F) << 6) + ((bytes[BYTE_TIMEZONE2] & 0xFC) >> 2);
                return OraTimeZoneUtil.getInstance().getRegion(regID);
            } else {
                // Time Zone is an offset
                int hours = bytes[BYTE_TIMEZONE1] - OFFSET_HOUR;
                int minutes = bytes[BYTE_TIMEZONE2] - OFFSET_MINUTE;
                boolean negative = (hours < 0 || minutes < 0);
    
                if (negative) {
                    hours = -hours;
                    minutes = -minutes;
                }
                return "GMT" + (negative ? "-" : "+") + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    digit[hours / 10] + digit[hours % 10] + ":" + //$NON-NLS-1$
                    digit[minutes / 10] + digit[minutes % 10];
            }
        }
        
        /**
         * Packs bytes and time zone into Oracle Datum byte array
         *
         * @param bytes raw representation
         * @param tz decode time zone.
         */
        
        @Override
        protected void packDatumBytes(byte[] bytes) {
        	packDatumBytes(bytes, getZone());
        }
        
        protected void packDatumBytes(byte[] bytes, ZoneId zone) {
        	ZonedDateTime datetime = getZonedDateTime();
        	ZonedDateTime asUTC = datetime.withZoneSameInstant(UTC_ID);
        	
            packDatumBytes(bytes, asUTC.toLocalDateTime());
            
            packTimeZoneID(bytes, datetime.toInstant().toEpochMilli(), zone);
        }
        
        /**
         * Packs time zone into Oracle Datum byte array
         *
         * @param tz time zone
         * @param onDate date point of reference in milliseconds
         */
        
        static void packTimeZoneID(byte[] bytes, long onDate, ZoneId zone) {
        	TimeZone tz = TimeZone.getTimeZone(zone);
            int regID = OraTimeZoneUtil.getInstance().getID(tz.getID()); // OraTimeZoneMap.getID() is case sensitive !!!
    
            if (regID > 0) {
                packTZRegion(bytes, regID);
            } else {
                packTZOffset(bytes, tz, onDate);
            }
        }
        
        /**
         * Packs time zone region into Oracle Datum byte array
         *
         * @param region time zone require
         */
        
        static void packTZRegion(byte[] bytes, int region) {
            // Region time zone
            bytes[BYTE_TIMEZONE1] = (byte)(((region & 0x1FC0) >> 6) | REGIONIDBIT);
            bytes[BYTE_TIMEZONE2] = (byte)((region & 0x3F) << 2);
        }
        
        /**
         * Packs time zone offset of time zone from UTC at the specified
         * date 
         *
         * @param tz time zone
         * @param time date and time point of reference in milliseconds
         */
        
        static void packTZOffset(byte[] bytes, TimeZone tz, long time) {
            // Offset time zone
            int offset = tz.getOffset(time) / 60000; // offset in minutes
            bytes[BYTE_TIMEZONE1] = (byte)(offset / 60 + OFFSET_HOUR);
            bytes[BYTE_TIMEZONE2] = (byte)(offset % 60 + OFFSET_MINUTE);
        }
    }
}
