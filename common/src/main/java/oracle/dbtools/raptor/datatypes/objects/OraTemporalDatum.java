/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import oracle.dbtools.raptor.nls.Messages;
import oracle.dbtools.raptor.nls.OraConstants;
import oracle.dbtools.raptor.nls.OraConversions;
import oracle.sql.Datum;

public abstract class OraTemporalDatum implements OraConstants, Cloneable, Comparable<OraTemporalDatum> {
    public static final String TIMEZONE = "TIMEZONE";
    public static final String TIME = "TIME";
    public static final String SECONDSTIME = "SECONDSTIME";
    public static final String NANOS = "NANOS";
    public static final String PRECISION = "PRECISION";
    public static final String FRACTIONALPRECISION = "FRACTIONALPRECISION";
    
    private OraTemporalDatumImpl datumImpl; // $NOT_FINAL_OK: Not final to support deep cloning only
    
    /* Statics */
    public final static TimeZone UTC = TimeZone.getTimeZone("UTC"); //$NON-NLS-1$
    public final static ZoneId UTC_ID = UTC.toZoneId(); //$NON-NLS-1$
    
    public final static ZoneId getDefaultDatumZone() {
        return ZoneId.systemDefault();
    }
    
    public final static ZoneId getDefaultDatumZone(ZoneId zone) {
        return (zone != null) ? zone : getDefaultDatumZone();
    }

    public final static TimeZone getDefaultTimeZone(TimeZone timezone) {
        return (timezone != null) ? timezone : TimeZone.getDefault();
    }
    
    public final static Locale getDefaultLocale(Locale locale) {
        return (locale != null) ? locale : Locale.getDefault();
    }
    
    public final static GregorianCalendar getGregorianInstance(TimeZone timezone, Locale locale) {
        return new GregorianCalendar(getDefaultTimeZone(timezone), getDefaultLocale(locale));
    }
    
    protected final static boolean areEqual(Object obj1, Object obj2) {
        return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
    }
    
    protected OraTemporalDatum(OraTemporalDatumImpl dataumImpl) {
        this.datumImpl = dataumImpl;
    }
    
    protected OraTemporalDatumImpl getImpl() {
        return datumImpl;
    }
    
    public final void addPropertyChangeListener(PropertyChangeListener pcl) {
        datumImpl.addPropertyChangeListener(pcl);
    }
    
    public final void removePropertyChangeListener(PropertyChangeListener pcl) {
        datumImpl.removePropertyChangeListener(pcl);
    }

    public Datum getDatum() {
    	return datumImpl.getDatum();
    }
    
    public final boolean nanosSupported() {
        return datumImpl.nanosSupported();
    }

    public final boolean userTimeZoneSupported() {
        return datumImpl.userTimeZoneSupported();
    }
    
    public final boolean localSupported() {
        return datumImpl.localSupported();
    }
    
    public final boolean sessionTimeZoneAdjusted() {
        return datumImpl.sessionZoneAdjusted();
    }
    
    public final Resolution getResolution() {
        return datumImpl.getResolution();
    }
    
    public final void setFractionalPrecision(int precision) {
        // Precision ignored is not supported
        if (nanosSupported()) {
            datumImpl.setFractionalPrecision(precision);
        }
    }
    
    public final int getFractionalPrecision() {
        return datumImpl.getFractionalPrecision();
    }
    
    public final void setPrecision(Precision precision) {
        datumImpl.setPrecision(precision);
    }
    
    public final Precision getPrecision() {
        return datumImpl.getPrecision();
    }
    
    public final Calendar getCalendar(Calendar target) {
        return datumImpl.getCalendar(target);
    }

    @Deprecated
    public final void setCalendar(Calendar calendar) {
        setCalendar(calendar, null);
    }

    @Deprecated
    public void setCalendar(Calendar calendar, Integer nanos) {
    	datumImpl.with(OraConversions.toInstant(calendar, nanos));
    }

    public final ZonedDateTime toZonedDateTime() {
        return datumImpl.getZonedDateTime();
    }

    public final ZonedDateTime toSessionZonedDateTime() {
        return datumImpl.getSessionZonedDateTime();
    }

    public final Timestamp toTimestamp() {
    	return Timestamp.from(datumImpl.toInstant());
    }
    
    public final LocalDateTime toLocalDateTime() {
        return datumImpl.toLocalDateTime();
    }

    public final Instant toInstant() {
        return datumImpl.toInstant();
    }
    
    public final Instant toSessionInstant() {
        return datumImpl.toSessionInstant();
    }
    
    public final void setToday() {
        datumImpl.today();
    }

    public final void setNow() {
    	datumImpl.now();
    }
    
    public final void replicate(OraTemporalDatum src) {
        datumImpl.replicate(src.getImpl());
    }
    
    public final void setValue(OraTemporalDatum src) {
        datumImpl.setValue(src.getImpl());
    }
    
    public final void setTimeInMillis(long timeInMillis) {
        setTimeInMillis(timeInMillis, null);
    }
    
    public final void setTimeInMillis(long timeInMillis, Integer nanos) {
    	datumImpl.with(OraConversions.toInstant(timeInMillis, nanos));
    }
    
    public final long getTimeInMillis() {
        return datumImpl.getTimeInMillis();
    }
    
    public final void setTime(Date date) {
        setTime(date.toInstant());
    }
    
    public final void setTime(Instant instant) {
        datumImpl.with(instant);
    }
    
    public final void setTime(LocalDateTime datetime) {
        datumImpl.with(datetime);
    }
    
    public final Date getTime() {
        return toTimestamp();
    }
    
    public final void setDate(java.sql.Date date) {
        setTime(date);
    }
    
    public final void setDate(LocalDate datetime) {
        datumImpl.with(datetime);
    }
    
    public final java.sql.Date getDate() {
        return new java.sql.Date(getTimeInMillis());
    }
    
    public final void setNanos(int nanos) {
        datumImpl.withNano(nanos);
    }

    public final int getNanos() {
        return datumImpl.getNano();
    }

    public final TimeZone getTimeZone() {
        return TimeZone.getTimeZone(getZone());
    }
    
    public final ZoneId getZone() {
        return datumImpl.getZone();
    }
    
    public final TimeZone getSessionTimeZone() {
    	ZoneId zone = getSessionZone();
        return (zone != null) ? TimeZone.getTimeZone(zone) : null;
    }
    
    public final ZoneId getSessionZone() {
        return datumImpl.getSessionZone();
    }
    
    public OraTemporalDatum setSessionTimeZone(TimeZone sessionTimeZone) {
    	return setSessionZone((sessionTimeZone != null) ? sessionTimeZone.toZoneId() : null);
    }
    
    public OraTemporalDatum setSessionZone(ZoneId sessionZoneId) {
    	datumImpl.setSessionZone(sessionZoneId);
    	return this;
    }
    
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (obj.getClass() != this.getClass()) return false;
        
        OraTemporalDatum another = (OraTemporalDatum)obj;
        
        return areEqual(datumImpl, another.datumImpl);
    }
    
    public int hashCode()
    {
        int hash = 7;
        hash = hash * 31 + datumImpl.hashCode();
        return hash;
    }
    
    public int compareTo(OraTemporalDatum another) {
        int ret = 0;
        
        if (this != another) {
            // Compare Calendar
            ret = this.datumImpl.compareTo(another.datumImpl);
        }
        
        return ret;
    }
    
    /**
     * Creates and returns a copy of this object.
     *
     * @return a copy of this object.
     */
    public Object clone()
    {
        try {
            OraTemporalDatum other = (OraTemporalDatum) super.clone();
            other.datumImpl = (OraTemporalDatumImpl)datumImpl.clone();
            
            return other;
        }
        catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError();
        }
    }
    
    protected interface OraTemporalDatumMetadata {
        boolean nanosSupported();

        boolean userTimeZoneSupported();
        
        boolean localSupported();
    }
    
    protected abstract static class OraTemporalDatumImpl implements OraTemporalDatumMetadata, OraConstants, Cloneable, Comparable<OraTemporalDatumImpl> {
        //--------------------------------------------------------------------------------------
        // property change methods
        
        protected void addPropertyChangeListener(PropertyChangeListener pcl) {
            changesSupport.addPropertyChangeListener(pcl);
        }
        
        protected void removePropertyChangeListener(PropertyChangeListener pcl) {
            changesSupport.removePropertyChangeListener(pcl);
        }

        private void fireEpochSecondsChange(long oldSecs, long newSecs) {
	        // Fire Seconds Property Change
	        changesSupport.firePropertyChange(SECONDSTIME, oldSecs, newSecs);
        }
        
        private void fireTimeInMillisChange(long oldMillis, long newMillis) {
            // Fire Millis Property Change
            changesSupport.firePropertyChange(TIME, oldMillis, newMillis);
        }
        
        private void fireNanosChange(int oldNanos, int newNanos) {
            // Fire Property Change
            changesSupport.firePropertyChange(NANOS, oldNanos, newNanos);
        }
        
        private void fireZoneChange(TimeZone oldTZ, TimeZone newTZ) {
            // Fire Property Change
            changesSupport.firePropertyChange(TIMEZONE, oldTZ, newTZ);
        }
        
        private void fireZoneChange(ZoneId oldZI, ZoneId newZI) {
            // Fire Property Change
        	fireZoneChange(TimeZone.getTimeZone(oldZI), TimeZone.getTimeZone(newZI));
        }
        
        private void firePrecisionChange(Precision oldPrecision, Precision newPrecision) {
            // Fire Property Change
            changesSupport.firePropertyChange(PRECISION, oldPrecision, newPrecision);
        }
        
        private void fireFractionalPrecisionChange(int oldFractionPrecision, int newFractionalPrecision) {
            // Fire Property Change
            changesSupport.firePropertyChange(FRACTIONALPRECISION, oldFractionPrecision, newFractionalPrecision);
        }
        
        // Constructors
        private ZoneId sessionZone;
        private ZonedDateTime resolved;
        private Precision precision;
        private int fractionalPrecision;
        private PropertyChangeSupport changesSupport;
        
        protected OraTemporalDatumImpl(ZoneId datumZone) {
            this(ZonedDateTime.now(getDefaultDatumZone(datumZone)));
        }
        
        protected OraTemporalDatumImpl(Instant instant, ZoneId datumZone) {
            this(instant.atZone(getDefaultDatumZone(datumZone)));
        }
        
        protected OraTemporalDatumImpl(LocalDateTime ldt, ZoneId datumZone) {
            this(ZonedDateTime.of(ldt, getDefaultDatumZone(datumZone)));
        }
    
        protected OraTemporalDatumImpl(ZonedDateTime zdt) {
            this.resolved = zdt;
            this.sessionZone = null;
            
            this.precision = Precision.NANOSECOND;
            this.fractionalPrecision = 9;
            
            this.changesSupport = new PropertyChangeSupport(this);
        }
        
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (obj == this) return true;
            if (obj.getClass() != this.getClass()) return false;
            
            OraTemporalDatumImpl another = (OraTemporalDatumImpl)obj;
            
            return this.resolved.equals(another.resolved);
        }
        
        public int hashCode()
        {
            int hash = 7;
            hash = hash * 31 + resolved.hashCode();
            hash = hash * 31 + precision.hashCode();
            hash = hash * 31 + Integer.valueOf(fractionalPrecision).hashCode();
            return hash;
        }
    
        public int compareTo(OraTemporalDatumImpl another) {
            int ret = 0;
            
            if (this != another) {
                // Compare Calendar
                ret = this.resolved.compareTo(another.resolved);
            }
            
            return ret;
        }
        
        public Change replicate(OraTemporalDatumImpl src) {
            Change a = setPrecision(src.getPrecision());
            Change b = setFractionalPrecision(src.getFractionalPrecision());
            this.sessionZone = src.sessionZone;
            return Change.combine(a,b);
        }
        
        public abstract Change setValue(OraTemporalDatumImpl src);
        
        /**
         * Creates and returns a copy of this object.
         *
         * @return a copy of this object.
         */
        public Object clone()
        {
            try {
                OraTemporalDatumImpl other = (OraTemporalDatumImpl) super.clone();
                other.resolved = resolved;
                other.precision = precision;
                other.fractionalPrecision = fractionalPrecision;
                other.sessionZone = sessionZone;
                other.changesSupport = new PropertyChangeSupport(other);
                
                return other;
            }
            catch (CloneNotSupportedException e) {
                // this shouldn't happen, since we are Cloneable
                throw new InternalError();
            }
        }
        
        // Accessors
        
        public abstract Resolution getResolution();
        
        public abstract boolean sessionZoneAdjusted();
        
        public abstract Datum getDatum();
        
        public final ZoneId getSessionZone() {
        	return this.sessionZone;
        }
        
        public final int getFractionalPrecision() {
            return this.fractionalPrecision;
        }
        
        public final Precision getPrecision() {
            return this.precision;
        }
        
        public final Calendar getCalendar(Calendar target) {
            target.setTimeZone(TimeZone.getTimeZone(getZone()));
            target.setTimeInMillis(getTimeInMillis());
            return target;
        }
        
        public final ZonedDateTime getZonedDateTime() {
        	return resolved;
        }
        
        public ZonedDateTime getSessionZonedDateTime() {
        	return getZonedDateTime();
        }
        
        public Instant toInstant() {
        	return getZonedDateTime().toInstant();
        }
        
        public Instant toSessionInstant() {
        	return getSessionZonedDateTime().toInstant();
        }
        
        public LocalDateTime toLocalDateTime() {
        	return getZonedDateTime().toLocalDateTime();
        }
        
        public LocalDate toLocalDate() {
        	return toLocalDateTime().toLocalDate();
        }
        
        public final ZoneId getZone() {
            return getZonedDateTime().getZone();
        }
    
        public final int getNano() {
            return toInstant().getNano();
        }
        
        public final long getSecondsInMillis() {
            return (getTimeInMillis()/1000)*1000;
        }
        
        public final long getTimeInMillis() {
            return toInstant().toEpochMilli();
        }
        
        // Modifiers
        
        public Change setSessionZone(ZoneId sessionZone) {
        	this.sessionZone = sessionZone;
        	return Change.NONE;
        }
        
        public Change setFractionalPrecision(int precision) {
            if (precision < 0 || precision > 9 || 
                precision > this.precision.getMaxFractionalSecondPrecision()) {
                throw new IllegalArgumentException(Messages.getString("OraSimpleDATEFormat.65")); //$NON-NLS-1$
            }
            
            Change change = Change.NONE;
            
            if (precision != this.fractionalPrecision) {
                int oldPrecision = this.fractionalPrecision;
                this.fractionalPrecision = precision;
                
                // Reduce nano value first
                if (precision < oldPrecision) {
                	change = recompute();
                }
                
                // Fire FractionPrecision Change
                fireFractionalPrecisionChange(oldPrecision, precision);
            }
            
            return change;
        }
        
        public Change setPrecision(Precision precision) {
            Change change = Change.NONE;
            
            if (precision != this.precision) {
                final int maxFractrionalPrecision = precision.getMaxFractionalSecondPrecision();
                
                // Reduce Fractional Precision
                if (getFractionalPrecision() > maxFractrionalPrecision) {
                    setFractionalPrecision(maxFractrionalPrecision);
                }
                
                Precision oldPrecision = this.precision;
                this.precision = precision;
                
                // Reduce millis value first
                if (precision.ordinal() > oldPrecision.ordinal()) {
                	change = recompute();
                }
                
                // Fire Precision Change
                firePrecisionChange(oldPrecision, precision);
            }
            
            return change;
        }
        
        public Change setZonedDateTime(ZonedDateTime newZDT) {
        	final ZonedDateTime oldZDT = this.resolved;
        	ZonedDateTime candidate = newZDT;
        	
        	LocalDateTime newLDT = newZDT.toLocalDateTime();
        	
        	LocalDateTime truncated = enforcePrecision(newLDT);
        	
        	if (!truncated.equals(newLDT)) {
        		candidate = newZDT.with(truncated);
        	}

    		boolean ldtchanged = (!oldZDT.toLocalDateTime().equals(candidate.toLocalDateTime()));
    		boolean offsetchanged = (!oldZDT.getOffset().equals(candidate.getOffset()));
    		boolean zonechanged = (!oldZDT.getZone().equals(candidate.getZone()));

    		boolean somethingChanged = (ldtchanged || offsetchanged || zonechanged);
        	
        	Change change;
        	
        	if (somethingChanged) {
            	change = Change.ALL;

        		this.resolved = candidate;
        		
        		Instant oldInstant = oldZDT.toInstant();
        		Instant candidateInstant = candidate.toInstant();
        		
        		boolean instantchanged = (!oldInstant.equals(candidateInstant));
        		boolean millischanged = (oldInstant.toEpochMilli() != candidateInstant.toEpochMilli());
        		boolean secschanged = (oldInstant.getEpochSecond() != candidateInstant.getEpochSecond());
        		boolean nanochanged = (oldInstant.getNano() != candidateInstant.getNano());
        		
        		
            	if (zonechanged) {
            		final ZoneId oldZI = oldZDT.getZone();
            		final ZoneId newZI = candidate.getZone();
            		
            		fireZoneChange(oldZI, newZI);;
            	}
            	
            	if (secschanged) {
            		fireEpochSecondsChange(oldInstant.getEpochSecond(), candidateInstant.getEpochSecond());
            	}
            	
            	if (millischanged) {
            		fireTimeInMillisChange(oldInstant.toEpochMilli(), candidateInstant.toEpochMilli());
            	}
            	
            	if (nanochanged) {
            		fireNanosChange(oldInstant.getNano(), candidateInstant.getNano());
            	}
            	
            	if (zonechanged) {
            		if (!instantchanged) {
            			change = Change.ZONE;
            		}
            	} else {
        			if (ldtchanged) {
        				LocalDateTime oldLDT = oldZDT.toLocalDateTime();
        				LocalDateTime candidateLDT = candidate.toLocalDateTime();
        				
        				boolean timeChanged = (!oldLDT.toLocalTime().equals(candidateLDT.toLocalTime()));
        				
        				if (!timeChanged) {
        					change = Change.LOCAL_DATE;
        				} else {
        					change = Change.LOCAL_DATE_TIME;
        				}
        			} else {
        				change = Change.INSTANT;
        			}
            	}
        	} else {
        		change = Change.NONE;
        	}
        	
        	return change;
        }
        
        public Change today() {
            return with(ZonedDateTime.now(getZone()).toLocalDate());
        }

        public Change now() {
        	return with(Instant.now());
        }
        
        public Change with(Instant instant) {
        	return setZonedDateTime(getZonedDateTime().with(instant));
        }

        public Change with(LocalDateTime ldt) {
        	return setZonedDateTime(getZonedDateTime().with(ldt));
        }
        
        public Change with(LocalDate ld) {
        	return with(LocalDateTime.of(ld, getZonedDateTime().toLocalTime()));
        }
        
        public Change withZoneSameInstant(ZoneId zone) {
        	return setZonedDateTime(getZonedDateTime().withZoneSameInstant(zone));
        }
        
        public Change withZoneSameLocal(ZoneId zone) {
        	return setZonedDateTime(getZonedDateTime().withZoneSameLocal(zone));
        }
        
        public Change withNano(int nano) {
        	return setZonedDateTime(getZonedDateTime().withNano(nano));
        }
        
        public Change recompute() {
        	return setZonedDateTime(getZonedDateTime());
        }
        
        /*
         * Protected Static Methods
         *
         */
    
        /**
         * Enforce Precision setting
         * 
         * @return  true, if recompute required, else false
         */
        protected final LocalDateTime enforcePrecision(LocalDateTime ldt) {
        	TemporalUnit tu;
            switch(precision) {
                case DATE:
                    if (ldt.getHour() != 0) {
                    	tu = ChronoUnit.DAYS;
                        break;
                    }
                    if (ldt.getMinute() != 0) {
                    	tu = ChronoUnit.HOURS;
                    	break;
                    }
                    // fall-thru
                case MINUTE:
                    if (ldt.getSecond() != 0) {
                    	tu = ChronoUnit.MINUTES;
                    	break;
                    }
                    // fall-thru
                case SECOND:
                    if (ldt.getNano() != 0) {
                    	tu = ChronoUnit.SECONDS;
                    	break;
                    }
                    // fall-thru
                case MILLISECOND:
                    if (ldt.getNano()%1000000 != 0) {
                    	tu = ChronoUnit.MILLIS;
                    	break;
                    }
                    // fall-thru
                case MICROSECOND:
                    if (ldt.getNano()%1000 != 0) {
                    	tu = ChronoUnit.MICROS;
                    	break;
                    }
                    // fall-thru
                default:
                	tu = null;
            }
                
        	// Truncate Time Component
            LocalDateTime newLDT;
            if (tu != null) {
            	newLDT = ldt.truncatedTo(tu);
            } else {
            	newLDT = ldt;
            }
            
        	// Truncate Nanos
            int nanos = newLDT.getNano();
            int newNanos = truncateNanos(nanos);
            
            // Apple any Nano change
            if (newNanos != nanos) {
            	return newLDT.withNano(newNanos);
            } else {
            	return newLDT;
            }
        }
        
        protected final int truncateNanos(int nanos) {
        	if (nanos == 0) {
        		return nanos;
        	}
        	
            OraConversions.checkNanosRange(nanos);

            int newNanos = nanos;
            
            // Round nanos to nearest precision positon
            if (fractionalPrecision == 0 || precision.getMaxFractionalSecondPrecision() == 0) {
                newNanos = 0;
            } else {
                int factor = (int)Math.pow(10, (9 - fractionalPrecision));
                double shiftedNanos =  (nanos * 1.0d) / factor;
                long roundedNanos = Math.round(shiftedNanos);
                newNanos = (int)(roundedNanos * factor);
            }
            
            return newNanos;
        }

        /**
         * Packs calendar into Oracle Datum byte array
         *
         * @param bytes the Datum buffer
         */
        
        protected void packDatumBytes(byte[] bytes) {
        	packDatumBytes(bytes, getZonedDateTime().toLocalDateTime());
        }
    
        protected void packDatumBytes(byte[] bytes, LocalDateTime ldt) {
            packDateTime(bytes, ldt);
        }
    
        /**
         * Packs calendar into Oracle Datum byte array
         *
         * @param bytes the Datum buffer
         * @param calendar the date/time
         */
        
        static final void packDateTime(byte[] bytes, LocalDateTime ldt) {
            int year = ldt.getYear();
            
            bytes[BYTE_YEAR1]  = (byte)(year / 100 + 100);
            bytes[BYTE_YEAR2]  = (byte)(year % 100 + 100);
            bytes[BYTE_MONTH]  = (byte)(ldt.getMonthValue());
            bytes[BYTE_DAY]    = (byte)(ldt.getDayOfMonth());
            bytes[BYTE_HOUR]   = (byte)(ldt.getHour() + 1);
            bytes[BYTE_MINUTE] = (byte)(ldt.getMinute() + 1);
            bytes[BYTE_SECOND] = (byte)(ldt.getSecond() + 1);
            
            if (bytes.length > SIZE_DATE) {
                int nanos = ldt.getNano();
                
                bytes[BYTE_NANOS1] = (byte)((nanos >> 24) & 0xFF);
                bytes[BYTE_NANOS2] = (byte)((nanos >> 16) & 0xFF);
                bytes[BYTE_NANOS3] = (byte)((nanos >> 8) & 0xFF);
                bytes[BYTE_NANOS4] = (byte)(nanos & 0xFF);
            }
        }
        
        /**
         * Unpacks Oracle Datum byte array into calendar
         *
         * @param bytes the Datum buffer
         * @param calendar the date/time
         */
        
        static LocalDateTime unpackTemporalDatum(byte[] bytes) {
            // convert an Oracle year to a Java year
            int year = OraConversions.convertYear(bytes);
            
            int nanos = (bytes.length > SIZE_DATE) 
            		? OraConversions.convertNanos(bytes)
            		: 0;
    
            LocalDateTime ldt = LocalDateTime.of(
            	year,
                bytes[BYTE_MONTH],
            	bytes[BYTE_DAY],
            	bytes[BYTE_HOUR] - 1,
            	bytes[BYTE_MINUTE] - 1,
            	bytes[BYTE_SECOND] - 1,
            	nanos);
            
            return ldt;
        }
    }
    
    public static enum Precision {
        NANOSECOND(0.000001d, 9),
        MICROSECOND(0.001d, 6),
        MILLISECOND(1, 3),
        SECOND(1000,0),
        MINUTE(60*1000,0),
        //HOUR(60*60*1000,0), // Makes no sense as a level and causes issues with truncation and offset gaps
        DATE(24*60*60*1000,0);
        
        private final int milliSecondFactor;
        private final int nanoSecondFactor;
        private final int maxSecondPrecision;
        
        private Precision(int milliSecondFactor, int maxSecondPrecision) {
            this.milliSecondFactor = milliSecondFactor;
            this.nanoSecondFactor = milliSecondFactor * 1000000;
            this.maxSecondPrecision = maxSecondPrecision;
        }
        
        private Precision(double milliSecondFactor, int maxSecondPrecision) {
            this.milliSecondFactor = (milliSecondFactor < 1) ? 0 : new Double(milliSecondFactor).intValue();
            this.nanoSecondFactor = new Double(milliSecondFactor * 1000000).intValue();
            this.maxSecondPrecision = maxSecondPrecision;
        }
        
        public int getMilliSecondFactor() {
            return milliSecondFactor;
        }
        
        public int getNanoSecondFactor() {
            return nanoSecondFactor;
        }
        
        public int getMaxFractionalSecondPrecision() {
            return maxSecondPrecision;
        }
    }
    
    public static enum Resolution {
    	LOCAL,
    	INSTANT,
    	ZONED;
    }
    
    static enum Change {
    	NONE,
    	ALL,
    	INSTANT,
    	LOCAL_DATE_TIME,
    	LOCAL_DATE,
    	ZONE;
    	
    	public static Change combine(Change a, Change b) {
    	   if (a == b ) {
    	       return a;
    	   } else if (a == ALL || b == ALL) {
    	       return ALL;
    	   } else if (a == NONE) {
    	       return b;
    	   } else if (b == NONE) {
    	       return a;
    	   } else if (a == ZONE && b != ZONE) {
    	       return ALL;
    	   } else if (a != ZONE && b == ZONE) {
    	       return ALL;
    	   } else if (a == INSTANT || b == INSTANT) {
    	       return INSTANT;
    	   } else {
    	       return LOCAL_DATE_TIME;
    	   }
    	}
    }
}
