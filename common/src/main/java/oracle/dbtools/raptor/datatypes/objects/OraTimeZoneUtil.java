/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.objects;

//import java.util.ArrayList;
//import java.util.Arrays;
import java.util.HashMap;

//import java.util.List;
//import java.util.TimeZone;

import oracle.sql.ZONEIDMAP;

public class OraTimeZoneUtil {
    /*static class TimeZoneGroup {  
        List<String> ids = new ArrayList<String>();  
        TimeZone example = null;  
        TimeZone tla_example = null;  
    
        TimeZoneGroup(TimeZone tz) {
            ids.add(tz.getID());
            if (example == null) {
                example = tz;
            }
            if (tla_example == null && tz.getID().length() == 3) {
                tla_example = tz;
            }
        }  
    }*/
    
    private static OraTimeZoneUtil INSTANCE = null;
    
    private final String[] tzNames;
    private final HashMap<String, Integer> tzIDMap;
    //private ArrayList<TimeZoneGroup> groups = new ArrayList<TimeZoneGroup>();  
    //private HashMap<String, String> tz2idMap;
    //private HashMap<String, String> id2tzMap;
   
    private OraTimeZoneUtil() {
        tzNames = new String[4610];
        tzIDMap = new HashMap<String, Integer>(4610);
        //tz2idMap = new HashMap<String, String>();
        //id2tzMap = new HashMap<String, String>();
        
        tzIDMap.put(tzNames[1]="Etc/GMT",1); //$NON-NLS-1$
        tzIDMap.put(tzNames[2]="Etc/GMT-14",2); //$NON-NLS-1$
        tzIDMap.put(tzNames[3]="Etc/GMT-13",3); //$NON-NLS-1$
        tzIDMap.put(tzNames[4]="Etc/GMT-12",4); //$NON-NLS-1$
        tzIDMap.put(tzNames[5]="Etc/GMT-11",5); //$NON-NLS-1$
        tzIDMap.put(tzNames[6]="Etc/GMT-10",6); //$NON-NLS-1$
        tzIDMap.put(tzNames[7]="Etc/GMT-9",7); //$NON-NLS-1$
        tzIDMap.put(tzNames[8]="Etc/GMT-8",8); //$NON-NLS-1$
        tzIDMap.put(tzNames[9]="Etc/GMT-7",9); //$NON-NLS-1$
        tzIDMap.put(tzNames[10]="Etc/GMT-6",10); //$NON-NLS-1$
        tzIDMap.put(tzNames[11]="Etc/GMT-5",11); //$NON-NLS-1$
        tzIDMap.put(tzNames[12]="Etc/GMT-4",12); //$NON-NLS-1$
        tzIDMap.put(tzNames[13]="Etc/GMT-3",13); //$NON-NLS-1$
        tzIDMap.put(tzNames[14]="Etc/GMT-2",14); //$NON-NLS-1$
        tzIDMap.put(tzNames[15]="Etc/GMT-1",15); //$NON-NLS-1$
        tzIDMap.put(tzNames[16]="Etc/GMT+1",16); //$NON-NLS-1$
        tzIDMap.put(tzNames[17]="Etc/GMT+2",17); //$NON-NLS-1$
        tzIDMap.put(tzNames[18]="Etc/GMT+3",18); //$NON-NLS-1$
        tzIDMap.put(tzNames[19]="Etc/GMT+4",19); //$NON-NLS-1$
        tzIDMap.put(tzNames[20]="Etc/GMT+5",20); //$NON-NLS-1$
        tzIDMap.put(tzNames[21]="Etc/GMT+6",21); //$NON-NLS-1$
        tzIDMap.put(tzNames[22]="Etc/GMT+7",22); //$NON-NLS-1$
        tzIDMap.put(tzNames[23]="Etc/GMT+8",23); //$NON-NLS-1$
        tzIDMap.put(tzNames[24]="Etc/GMT+9",24); //$NON-NLS-1$
        tzIDMap.put(tzNames[25]="Etc/GMT+10",25); //$NON-NLS-1$
        tzIDMap.put(tzNames[26]="Etc/GMT+11",26); //$NON-NLS-1$
        tzIDMap.put(tzNames[27]="Etc/GMT+12",27); //$NON-NLS-1$
        tzIDMap.put(tzNames[28]="Etc/UTC",28); //$NON-NLS-1$
        tzIDMap.put(tzNames[29]="Etc/UCT",29); //$NON-NLS-1$
        tzIDMap.put(tzNames[30]="Africa/Algiers",30); //$NON-NLS-1$
        tzIDMap.put(tzNames[31]="Africa/Luanda",31); //$NON-NLS-1$
        tzIDMap.put(tzNames[32]="Africa/Porto-Novo",32); //$NON-NLS-1$
        tzIDMap.put(tzNames[33]="Africa/Gaborone",33); //$NON-NLS-1$
        tzIDMap.put(tzNames[34]="Africa/Ouagadougou",34); //$NON-NLS-1$
        tzIDMap.put(tzNames[35]="Africa/Bujumbura",35); //$NON-NLS-1$
        tzIDMap.put(tzNames[36]="Africa/Douala",36); //$NON-NLS-1$
        tzIDMap.put(tzNames[37]="Africa/Bangui",37); //$NON-NLS-1$
        tzIDMap.put(tzNames[38]="Africa/Ndjamena",38); //$NON-NLS-1$
        tzIDMap.put(tzNames[39]="Africa/Kinshasa",39); //$NON-NLS-1$
        tzIDMap.put(tzNames[40]="Africa/Lubumbashi",40); //$NON-NLS-1$
        tzIDMap.put(tzNames[41]="Africa/Brazzaville",41); //$NON-NLS-1$
        tzIDMap.put(tzNames[42]="Africa/Abidjan",42); //$NON-NLS-1$
        tzIDMap.put(tzNames[43]="Africa/Djibouti",43); //$NON-NLS-1$
        tzIDMap.put(tzNames[44]="Africa/Cairo",44); //$NON-NLS-1$
        tzIDMap.put(tzNames[45]="Africa/Malabo",45); //$NON-NLS-1$
        tzIDMap.put(tzNames[47]="Africa/Addis_Ababa",47); //$NON-NLS-1$
        tzIDMap.put(tzNames[48]="Africa/Libreville",48); //$NON-NLS-1$
        tzIDMap.put(tzNames[49]="Africa/Banjul",49); //$NON-NLS-1$
        tzIDMap.put(tzNames[50]="Africa/Accra",50); //$NON-NLS-1$
        tzIDMap.put(tzNames[51]="Africa/Conakry",51); //$NON-NLS-1$
        tzIDMap.put(tzNames[52]="Africa/Bissau",52); //$NON-NLS-1$
        tzIDMap.put(tzNames[53]="Africa/Nairobi",53); //$NON-NLS-1$
        tzIDMap.put(tzNames[54]="Africa/Maseru",54); //$NON-NLS-1$
        tzIDMap.put(tzNames[55]="Africa/Monrovia",55); //$NON-NLS-1$
        tzIDMap.put(tzNames[56]="Africa/Tripoli",56); //$NON-NLS-1$
        tzIDMap.put(tzNames[57]="Africa/Blantyre",57); //$NON-NLS-1$
        tzIDMap.put(tzNames[58]="Africa/Bamako",58); //$NON-NLS-1$
        tzIDMap.put(tzNames[60]="Africa/Nouakchott",60); //$NON-NLS-1$
        tzIDMap.put(tzNames[61]="Africa/Casablanca",61); //$NON-NLS-1$
        tzIDMap.put(tzNames[62]="Africa/El_Aaiun",62); //$NON-NLS-1$
        tzIDMap.put(tzNames[63]="Africa/Maputo",63); //$NON-NLS-1$
        tzIDMap.put(tzNames[64]="Africa/Windhoek",64); //$NON-NLS-1$
        tzIDMap.put(tzNames[65]="Africa/Niamey",65); //$NON-NLS-1$
        tzIDMap.put(tzNames[66]="Africa/Lagos",66); //$NON-NLS-1$
        tzIDMap.put(tzNames[67]="Africa/Kigali",67); //$NON-NLS-1$
        tzIDMap.put(tzNames[68]="Africa/Sao_Tome",68); //$NON-NLS-1$
        tzIDMap.put(tzNames[69]="Africa/Dakar",69); //$NON-NLS-1$
        tzIDMap.put(tzNames[70]="Africa/Freetown",70); //$NON-NLS-1$
        tzIDMap.put(tzNames[71]="Africa/Mogadishu",71); //$NON-NLS-1$
        tzIDMap.put(tzNames[72]="Africa/Johannesburg",72); //$NON-NLS-1$
        tzIDMap.put(tzNames[73]="Africa/Khartoum",73); //$NON-NLS-1$
        tzIDMap.put(tzNames[74]="Africa/Mbabane",74); //$NON-NLS-1$
        tzIDMap.put(tzNames[75]="Africa/Dar_es_Salaam",75); //$NON-NLS-1$
        tzIDMap.put(tzNames[76]="Africa/Lome",76); //$NON-NLS-1$
        tzIDMap.put(tzNames[77]="Africa/Tunis",77); //$NON-NLS-1$
        tzIDMap.put(tzNames[78]="Africa/Kampala",78); //$NON-NLS-1$
        tzIDMap.put(tzNames[79]="Africa/Lusaka",79); //$NON-NLS-1$
        tzIDMap.put(tzNames[80]="Africa/Harare",80); //$NON-NLS-1$
        tzIDMap.put(tzNames[81]="Africa/Ceuta",81); //$NON-NLS-1$
        tzIDMap.put(tzNames[82]="Antarctica/Rothera",82); //$NON-NLS-1$
        tzIDMap.put(tzNames[83]="America/Santarem",83); //$NON-NLS-1$
        tzIDMap.put(tzNames[84]="Asia/Novokuznetsk",84); //$NON-NLS-1$
        tzIDMap.put(tzNames[85]="Antarctica/Macquarie",85); //$NON-NLS-1$
        tzIDMap.put(tzNames[86]="America/Recife",86); //$NON-NLS-1$
        tzIDMap.put(tzNames[87]="America/Matamoros",87); //$NON-NLS-1$
        tzIDMap.put(tzNames[88]="America/North_Dakota/Center",88); //$NON-NLS-1$
        tzIDMap.put(tzNames[89]="America/North_Dakota/New_Salem",89); //$NON-NLS-1$
        tzIDMap.put(tzNames[90]="America/Bahia",90); //$NON-NLS-1$
        tzIDMap.put(tzNames[91]="America/Kentucky/Monticello",91); //$NON-NLS-1$
        tzIDMap.put(tzNames[92]="America/Moncton",92); //$NON-NLS-1$
        tzIDMap.put(tzNames[93]="America/Ojinaga",93); //$NON-NLS-1$
        tzIDMap.put(tzNames[94]="America/Indiana/Tell_City",94); //$NON-NLS-1$
        tzIDMap.put(tzNames[95]="America/Bahia_Banderas",95); //$NON-NLS-1$
        tzIDMap.put(tzNames[96]="America/Santa_Isabel",96); //$NON-NLS-1$
        tzIDMap.put(tzNames[97]="America/Argentina/San_Luis",97); //$NON-NLS-1$
        tzIDMap.put(tzNames[98]="America/Argentina/Rio_Gallegos",98); //$NON-NLS-1$
        tzIDMap.put(tzNames[99]="America/Argentina/La_Rioja",99); //$NON-NLS-1$
        tzIDMap.put(tzNames[100]="America/New_York",100); //$NON-NLS-1$
        tzIDMap.put(tzNames[101]="America/Chicago",101); //$NON-NLS-1$
        tzIDMap.put(tzNames[102]="America/Denver",102); //$NON-NLS-1$
        tzIDMap.put(tzNames[103]="America/Los_Angeles",103); //$NON-NLS-1$
        tzIDMap.put(tzNames[104]="America/Juneau",104); //$NON-NLS-1$
        tzIDMap.put(tzNames[105]="America/Yakutat",105); //$NON-NLS-1$
        tzIDMap.put(tzNames[106]="America/Anchorage",106); //$NON-NLS-1$
        tzIDMap.put(tzNames[107]="America/Nome",107); //$NON-NLS-1$
        tzIDMap.put(tzNames[108]="America/Adak",108); //$NON-NLS-1$
        tzIDMap.put(tzNames[109]="America/Phoenix",109); //$NON-NLS-1$
        tzIDMap.put(tzNames[110]="America/Boise",110); //$NON-NLS-1$
        tzIDMap.put(tzNames[111]="America/Indianapolis",111); //$NON-NLS-1$
        tzIDMap.put(tzNames[112]="America/Indiana/Marengo",112); //$NON-NLS-1$
        tzIDMap.put(tzNames[113]="America/Indiana/Knox",113); //$NON-NLS-1$
        tzIDMap.put(tzNames[114]="America/Indiana/Vevay",114); //$NON-NLS-1$
        tzIDMap.put(tzNames[115]="America/Louisville",115); //$NON-NLS-1$
        tzIDMap.put(tzNames[116]="America/Detroit",116); //$NON-NLS-1$
        tzIDMap.put(tzNames[117]="America/Menominee",117); //$NON-NLS-1$
        tzIDMap.put(tzNames[118]="America/St_Johns",118); //$NON-NLS-1$
        tzIDMap.put(tzNames[119]="America/Goose_Bay",119); //$NON-NLS-1$
        tzIDMap.put(tzNames[120]="America/Halifax",120); //$NON-NLS-1$
        tzIDMap.put(tzNames[121]="America/Glace_Bay",121); //$NON-NLS-1$
        tzIDMap.put(tzNames[122]="America/Montreal",122); //$NON-NLS-1$
        tzIDMap.put(tzNames[123]="America/Thunder_Bay",123); //$NON-NLS-1$
        tzIDMap.put(tzNames[124]="America/Nipigon",124); //$NON-NLS-1$
        tzIDMap.put(tzNames[125]="America/Rainy_River",125); //$NON-NLS-1$
        tzIDMap.put(tzNames[126]="America/Winnipeg",126); //$NON-NLS-1$
        tzIDMap.put(tzNames[127]="America/Regina",127); //$NON-NLS-1$
        tzIDMap.put(tzNames[128]="America/Swift_Current",128); //$NON-NLS-1$
        tzIDMap.put(tzNames[129]="America/Edmonton",129); //$NON-NLS-1$
        tzIDMap.put(tzNames[130]="America/Vancouver",130); //$NON-NLS-1$
        tzIDMap.put(tzNames[131]="America/Dawson_Creek",131); //$NON-NLS-1$
        tzIDMap.put(tzNames[132]="America/Pangnirtung",132); //$NON-NLS-1$
        tzIDMap.put(tzNames[133]="America/Iqaluit",133); //$NON-NLS-1$
        tzIDMap.put(tzNames[134]="America/Rankin_Inlet",134); //$NON-NLS-1$
        tzIDMap.put(tzNames[135]="America/Cambridge_Bay",135); //$NON-NLS-1$
        tzIDMap.put(tzNames[136]="America/Yellowknife",136); //$NON-NLS-1$
        tzIDMap.put(tzNames[137]="America/Inuvik",137); //$NON-NLS-1$
        tzIDMap.put(tzNames[138]="America/Whitehorse",138); //$NON-NLS-1$
        tzIDMap.put(tzNames[139]="America/Dawson",139); //$NON-NLS-1$
        tzIDMap.put(tzNames[140]="America/Cancun",140); //$NON-NLS-1$
        tzIDMap.put(tzNames[141]="America/Mexico_City",141); //$NON-NLS-1$
        tzIDMap.put(tzNames[142]="America/Chihuahua",142); //$NON-NLS-1$
        tzIDMap.put(tzNames[143]="America/Hermosillo",143); //$NON-NLS-1$
        tzIDMap.put(tzNames[144]="America/Mazatlan",144); //$NON-NLS-1$
        tzIDMap.put(tzNames[145]="America/Tijuana",145); //$NON-NLS-1$
        tzIDMap.put(tzNames[146]="America/Anguilla",146); //$NON-NLS-1$
        tzIDMap.put(tzNames[147]="America/Antigua",147); //$NON-NLS-1$
        tzIDMap.put(tzNames[148]="America/Nassau",148); //$NON-NLS-1$
        tzIDMap.put(tzNames[149]="America/Barbados",149); //$NON-NLS-1$
        tzIDMap.put(tzNames[150]="America/Belize",150); //$NON-NLS-1$
        tzIDMap.put(tzNames[151]="America/Cayman",151); //$NON-NLS-1$
        tzIDMap.put(tzNames[152]="America/Costa_Rica",152); //$NON-NLS-1$
        tzIDMap.put(tzNames[153]="America/Havana",153); //$NON-NLS-1$
        tzIDMap.put(tzNames[154]="America/Dominica",154); //$NON-NLS-1$
        tzIDMap.put(tzNames[155]="America/Santo_Domingo",155); //$NON-NLS-1$
        tzIDMap.put(tzNames[156]="America/El_Salvador",156); //$NON-NLS-1$
        tzIDMap.put(tzNames[157]="America/Grenada",157); //$NON-NLS-1$
        tzIDMap.put(tzNames[158]="America/Guadeloupe",158); //$NON-NLS-1$
        tzIDMap.put(tzNames[159]="America/Guatemala",159); //$NON-NLS-1$
        tzIDMap.put(tzNames[160]="America/Port-au-Prince",160); //$NON-NLS-1$
        tzIDMap.put(tzNames[161]="America/Tegucigalpa",161); //$NON-NLS-1$
        tzIDMap.put(tzNames[162]="America/Jamaica",162); //$NON-NLS-1$
        tzIDMap.put(tzNames[163]="America/Martinique",163); //$NON-NLS-1$
        tzIDMap.put(tzNames[164]="America/Montserrat",164); //$NON-NLS-1$
        tzIDMap.put(tzNames[165]="America/Managua",165); //$NON-NLS-1$
        tzIDMap.put(tzNames[166]="America/Panama",166); //$NON-NLS-1$
        tzIDMap.put(tzNames[167]="America/Puerto_Rico",167); //$NON-NLS-1$
        tzIDMap.put(tzNames[168]="America/St_Kitts",168); //$NON-NLS-1$
        tzIDMap.put(tzNames[169]="America/St_Lucia",169); //$NON-NLS-1$
        tzIDMap.put(tzNames[170]="America/Miquelon",170); //$NON-NLS-1$
        tzIDMap.put(tzNames[171]="America/St_Vincent",171); //$NON-NLS-1$
        tzIDMap.put(tzNames[172]="America/Grand_Turk",172); //$NON-NLS-1$
        tzIDMap.put(tzNames[173]="America/Tortola",173); //$NON-NLS-1$
        tzIDMap.put(tzNames[174]="America/St_Thomas",174); //$NON-NLS-1$
        tzIDMap.put(tzNames[175]="America/Buenos_Aires",175); //$NON-NLS-1$
        tzIDMap.put(tzNames[177]="America/Cordoba",177); //$NON-NLS-1$
        tzIDMap.put(tzNames[178]="America/Jujuy",178); //$NON-NLS-1$
        tzIDMap.put(tzNames[179]="America/Catamarca",179); //$NON-NLS-1$
        tzIDMap.put(tzNames[180]="America/Mendoza",180); //$NON-NLS-1$
        tzIDMap.put(tzNames[181]="America/Aruba",181); //$NON-NLS-1$
        tzIDMap.put(tzNames[182]="America/La_Paz",182); //$NON-NLS-1$
        tzIDMap.put(tzNames[183]="America/Noronha",183); //$NON-NLS-1$
        tzIDMap.put(tzNames[184]="America/Belem",184); //$NON-NLS-1$
        tzIDMap.put(tzNames[185]="America/Fortaleza",185); //$NON-NLS-1$
        tzIDMap.put(tzNames[186]="America/Araguaina",186); //$NON-NLS-1$
        tzIDMap.put(tzNames[187]="America/Maceio",187); //$NON-NLS-1$
        tzIDMap.put(tzNames[188]="America/Sao_Paulo",188); //$NON-NLS-1$
        tzIDMap.put(tzNames[189]="America/Cuiaba",189); //$NON-NLS-1$
        tzIDMap.put(tzNames[190]="America/Porto_Velho",190); //$NON-NLS-1$
        tzIDMap.put(tzNames[191]="America/Boa_Vista",191); //$NON-NLS-1$
        tzIDMap.put(tzNames[192]="America/Manaus",192); //$NON-NLS-1$
        tzIDMap.put(tzNames[193]="America/Porto_Acre",193); //$NON-NLS-1$
        tzIDMap.put(tzNames[194]="America/Santiago",194); //$NON-NLS-1$
        tzIDMap.put(tzNames[195]="America/Bogota",195); //$NON-NLS-1$
        tzIDMap.put(tzNames[196]="America/Curacao",196); //$NON-NLS-1$
        tzIDMap.put(tzNames[197]="America/Guayaquil",197); //$NON-NLS-1$
        tzIDMap.put(tzNames[198]="America/Cayenne",198); //$NON-NLS-1$
        tzIDMap.put(tzNames[199]="America/Guyana",199); //$NON-NLS-1$
        tzIDMap.put(tzNames[200]="America/Asuncion",200); //$NON-NLS-1$
        tzIDMap.put(tzNames[201]="America/Lima",201); //$NON-NLS-1$
        tzIDMap.put(tzNames[202]="America/Paramaribo",202); //$NON-NLS-1$
        tzIDMap.put(tzNames[203]="America/Port_of_Spain",203); //$NON-NLS-1$
        tzIDMap.put(tzNames[204]="America/Montevideo",204); //$NON-NLS-1$
        tzIDMap.put(tzNames[205]="America/Caracas",205); //$NON-NLS-1$
        tzIDMap.put(tzNames[206]="America/Scoresbysund",206); //$NON-NLS-1$
        tzIDMap.put(tzNames[207]="America/Godthab",207); //$NON-NLS-1$
        tzIDMap.put(tzNames[208]="America/Thule",208); //$NON-NLS-1$
        tzIDMap.put(tzNames[209]="America/Indiana/Vincennes",209); //$NON-NLS-1$
        tzIDMap.put(tzNames[210]="America/Indiana/Petersburg",210); //$NON-NLS-1$
        tzIDMap.put(tzNames[211]="EST",211); //$NON-NLS-1$
        tzIDMap.put(tzNames[212]="MST",212); //$NON-NLS-1$
        tzIDMap.put(tzNames[213]="HST",213); //$NON-NLS-1$
        tzIDMap.put(tzNames[214]="EST5EDT",214); //$NON-NLS-1$
        tzIDMap.put(tzNames[215]="CST6CDT",215); //$NON-NLS-1$
        tzIDMap.put(tzNames[216]="MST7MDT",216); //$NON-NLS-1$
        tzIDMap.put(tzNames[217]="PST8PDT",217); //$NON-NLS-1$
        tzIDMap.put(tzNames[218]="America/Indiana/Winamac",218); //$NON-NLS-1$
        tzIDMap.put(tzNames[219]="America/Resolute",219); //$NON-NLS-1$
        tzIDMap.put(tzNames[220]="America/Toronto",220); //$NON-NLS-1$
        tzIDMap.put(tzNames[221]="America/Atikokan",221); //$NON-NLS-1$
        tzIDMap.put(tzNames[222]="America/Campo_Grande",222); //$NON-NLS-1$
        tzIDMap.put(tzNames[223]="America/Danmarkshavn",223); //$NON-NLS-1$
        tzIDMap.put(tzNames[224]="America/Blanc-Sablon",224); //$NON-NLS-1$
        tzIDMap.put(tzNames[225]="America/Eirunepe",225); //$NON-NLS-1$
        tzIDMap.put(tzNames[226]="America/Merida",226); //$NON-NLS-1$
        tzIDMap.put(tzNames[227]="America/Monterrey",227); //$NON-NLS-1$
        tzIDMap.put(tzNames[228]="America/Argentina/Tucuman",228); //$NON-NLS-1$
        tzIDMap.put(tzNames[229]="America/Argentina/San_Juan",229); //$NON-NLS-1$
        tzIDMap.put(tzNames[230]="Antarctica/Casey",230); //$NON-NLS-1$
        tzIDMap.put(tzNames[231]="Antarctica/Davis",231); //$NON-NLS-1$
        tzIDMap.put(tzNames[232]="Antarctica/Mawson",232); //$NON-NLS-1$
        tzIDMap.put(tzNames[233]="Antarctica/DumontDUrville",233); //$NON-NLS-1$
        tzIDMap.put(tzNames[234]="Antarctica/Syowa",234); //$NON-NLS-1$
        tzIDMap.put(tzNames[235]="Antarctica/Palmer",235); //$NON-NLS-1$
        tzIDMap.put(tzNames[236]="Antarctica/McMurdo",236); //$NON-NLS-1$
        tzIDMap.put(tzNames[237]="America/Argentina/Salta",237); //$NON-NLS-1$
        tzIDMap.put(tzNames[238]="Pacific/Chuuk",238); //$NON-NLS-1$
        tzIDMap.put(tzNames[239]="Pacific/Pohnpei",239); //$NON-NLS-1$
        tzIDMap.put(tzNames[240]="Asia/Kabul",240); //$NON-NLS-1$
        tzIDMap.put(tzNames[241]="Asia/Yerevan",241); //$NON-NLS-1$
        tzIDMap.put(tzNames[242]="Asia/Baku",242); //$NON-NLS-1$
        tzIDMap.put(tzNames[243]="Asia/Bahrain",243); //$NON-NLS-1$
        tzIDMap.put(tzNames[244]="Asia/Dacca",244); //$NON-NLS-1$
        tzIDMap.put(tzNames[246]="Asia/Brunei",246); //$NON-NLS-1$
        tzIDMap.put(tzNames[247]="Asia/Rangoon",247); //$NON-NLS-1$
        tzIDMap.put(tzNames[248]="Asia/Phnom_Penh",248); //$NON-NLS-1$
        tzIDMap.put(tzNames[249]="Asia/Harbin",249); //$NON-NLS-1$
        tzIDMap.put(tzNames[250]="Asia/Shanghai",250); //$NON-NLS-1$
        tzIDMap.put(tzNames[251]="Asia/Chungking",251); //$NON-NLS-1$
        tzIDMap.put(tzNames[252]="Asia/Urumqi",252); //$NON-NLS-1$
        tzIDMap.put(tzNames[253]="Asia/Kashgar",253); //$NON-NLS-1$
        tzIDMap.put(tzNames[254]="Asia/Hong_Kong",254); //$NON-NLS-1$
        tzIDMap.put(tzNames[255]="Asia/Taipei",255); //$NON-NLS-1$
        tzIDMap.put(tzNames[256]="Asia/Macao",256); //$NON-NLS-1$
        tzIDMap.put(tzNames[257]="Asia/Nicosia",257); //$NON-NLS-1$
        tzIDMap.put(tzNames[258]="Asia/Tbilisi",258); //$NON-NLS-1$
        tzIDMap.put(tzNames[259]="Asia/Dili",259); //$NON-NLS-1$
        tzIDMap.put(tzNames[260]="Asia/Calcutta",260); //$NON-NLS-1$
        tzIDMap.put(tzNames[261]="Asia/Jakarta",261); //$NON-NLS-1$
        tzIDMap.put(tzNames[262]="Asia/Ujung_Pandang",262); //$NON-NLS-1$
        tzIDMap.put(tzNames[263]="Asia/Jayapura",263); //$NON-NLS-1$
        tzIDMap.put(tzNames[264]="Asia/Tehran",264); //$NON-NLS-1$
        tzIDMap.put(tzNames[265]="Asia/Baghdad",265); //$NON-NLS-1$
        tzIDMap.put(tzNames[266]="Asia/Jerusalem",266); //$NON-NLS-1$
        tzIDMap.put(tzNames[267]="Asia/Tokyo",267); //$NON-NLS-1$
        tzIDMap.put(tzNames[268]="Asia/Amman",268); //$NON-NLS-1$
        tzIDMap.put(tzNames[269]="Asia/Almaty",269); //$NON-NLS-1$
        tzIDMap.put(tzNames[270]="Asia/Aqtobe",270); //$NON-NLS-1$
        tzIDMap.put(tzNames[271]="Asia/Aqtau",271); //$NON-NLS-1$
        tzIDMap.put(tzNames[272]="Asia/Bishkek",272); //$NON-NLS-1$
        tzIDMap.put(tzNames[273]="Asia/Seoul",273); //$NON-NLS-1$
        tzIDMap.put(tzNames[274]="Asia/Pyongyang",274); //$NON-NLS-1$
        tzIDMap.put(tzNames[275]="Asia/Kuwait",275); //$NON-NLS-1$
        tzIDMap.put(tzNames[276]="Asia/Vientiane",276); //$NON-NLS-1$
        tzIDMap.put(tzNames[277]="Asia/Beirut",277); //$NON-NLS-1$
        tzIDMap.put(tzNames[278]="Asia/Kuala_Lumpur",278); //$NON-NLS-1$
        tzIDMap.put(tzNames[279]="Asia/Kuching",279); //$NON-NLS-1$
        tzIDMap.put(tzNames[280]="Asia/Hovd",280); //$NON-NLS-1$
        tzIDMap.put(tzNames[281]="Asia/Ulaanbaatar",281); //$NON-NLS-1$
        tzIDMap.put(tzNames[282]="Asia/Katmandu",282); //$NON-NLS-1$
        tzIDMap.put(tzNames[283]="Asia/Muscat",283); //$NON-NLS-1$
        tzIDMap.put(tzNames[284]="Asia/Karachi",284); //$NON-NLS-1$
        tzIDMap.put(tzNames[285]="Asia/Gaza",285); //$NON-NLS-1$
        tzIDMap.put(tzNames[286]="Asia/Manila",286); //$NON-NLS-1$
        tzIDMap.put(tzNames[287]="Asia/Qatar",287); //$NON-NLS-1$
        tzIDMap.put(tzNames[288]="Asia/Riyadh",288); //$NON-NLS-1$
        tzIDMap.put(tzNames[292]="Asia/Singapore",292); //$NON-NLS-1$
        tzIDMap.put(tzNames[293]="Asia/Colombo",293); //$NON-NLS-1$
        tzIDMap.put(tzNames[294]="Asia/Damascus",294); //$NON-NLS-1$
        tzIDMap.put(tzNames[295]="Asia/Dushanbe",295); //$NON-NLS-1$
        tzIDMap.put(tzNames[296]="Asia/Bangkok",296); //$NON-NLS-1$
        tzIDMap.put(tzNames[298]="Asia/Dubai",298); //$NON-NLS-1$
        tzIDMap.put(tzNames[299]="Asia/Samarkand",299); //$NON-NLS-1$
        tzIDMap.put(tzNames[300]="Asia/Tashkent",300); //$NON-NLS-1$
        tzIDMap.put(tzNames[301]="Asia/Saigon",301); //$NON-NLS-1$
        tzIDMap.put(tzNames[302]="Asia/Aden",302); //$NON-NLS-1$
        tzIDMap.put(tzNames[303]="Asia/Yekaterinburg",303); //$NON-NLS-1$
        tzIDMap.put(tzNames[304]="Asia/Omsk",304); //$NON-NLS-1$
        tzIDMap.put(tzNames[305]="Asia/Novosibirsk",305); //$NON-NLS-1$
        tzIDMap.put(tzNames[306]="Asia/Krasnoyarsk",306); //$NON-NLS-1$
        tzIDMap.put(tzNames[307]="Asia/Irkutsk",307); //$NON-NLS-1$
        tzIDMap.put(tzNames[308]="Asia/Yakutsk",308); //$NON-NLS-1$
        tzIDMap.put(tzNames[309]="Asia/Vladivostok",309); //$NON-NLS-1$
        tzIDMap.put(tzNames[310]="Asia/Magadan",310); //$NON-NLS-1$
        tzIDMap.put(tzNames[311]="Asia/Kamchatka",311); //$NON-NLS-1$
        tzIDMap.put(tzNames[312]="Asia/Anadyr",312); //$NON-NLS-1$
        tzIDMap.put(tzNames[313]="Asia/Pontianak",313); //$NON-NLS-1$
        tzIDMap.put(tzNames[314]="Asia/Qyzylorda",314); //$NON-NLS-1$
        tzIDMap.put(tzNames[315]="Asia/Oral",315); //$NON-NLS-1$
        tzIDMap.put(tzNames[316]="Asia/Choibalsan",316); //$NON-NLS-1$
        tzIDMap.put(tzNames[317]="Asia/Sakhalin",317); //$NON-NLS-1$
        tzIDMap.put(tzNames[318]="America/Sitka",318); //$NON-NLS-1$
        tzIDMap.put(tzNames[319]="America/Metlakatla",319); //$NON-NLS-1$
        tzIDMap.put(tzNames[320]="America/North_Dakota/Beulah",320); //$NON-NLS-1$
        tzIDMap.put(tzNames[321]="Africa/Juba",321); //$NON-NLS-1$
        tzIDMap.put(tzNames[322]="America/Lower_Princes",322); //$NON-NLS-1$
        tzIDMap.put(tzNames[323]="America/Kralendijk",323); //$NON-NLS-1$
        tzIDMap.put(tzNames[324]="Asia/Hebron",324); //$NON-NLS-1$
        tzIDMap.put(tzNames[325]="America/Creston",325); //$NON-NLS-1$
        tzIDMap.put(tzNames[330]="Atlantic/Bermuda",330); //$NON-NLS-1$
        tzIDMap.put(tzNames[331]="Atlantic/Stanley",331); //$NON-NLS-1$
        tzIDMap.put(tzNames[332]="Atlantic/South_Georgia",332); //$NON-NLS-1$
        tzIDMap.put(tzNames[334]="Atlantic/Reykjavik",334); //$NON-NLS-1$
        tzIDMap.put(tzNames[336]="Atlantic/Azores",336); //$NON-NLS-1$
        tzIDMap.put(tzNames[337]="Atlantic/Madeira",337); //$NON-NLS-1$
        tzIDMap.put(tzNames[338]="Atlantic/Canary",338); //$NON-NLS-1$
        tzIDMap.put(tzNames[339]="Atlantic/Cape_Verde",339); //$NON-NLS-1$
        tzIDMap.put(tzNames[340]="Atlantic/St_Helena",340); //$NON-NLS-1$
        tzIDMap.put(tzNames[345]="Australia/Darwin",345); //$NON-NLS-1$
        tzIDMap.put(tzNames[346]="Australia/Perth",346); //$NON-NLS-1$
        tzIDMap.put(tzNames[347]="Australia/Brisbane",347); //$NON-NLS-1$
        tzIDMap.put(tzNames[348]="Australia/Lindeman",348); //$NON-NLS-1$
        tzIDMap.put(tzNames[349]="Australia/Adelaide",349); //$NON-NLS-1$
        tzIDMap.put(tzNames[350]="Australia/Hobart",350); //$NON-NLS-1$
        tzIDMap.put(tzNames[351]="Australia/Melbourne",351); //$NON-NLS-1$
        tzIDMap.put(tzNames[352]="Australia/Sydney",352); //$NON-NLS-1$
        tzIDMap.put(tzNames[353]="Australia/Broken_Hill",353); //$NON-NLS-1$
        tzIDMap.put(tzNames[354]="Australia/Lord_Howe",354); //$NON-NLS-1$
        tzIDMap.put(tzNames[355]="Australia/Currie",355); //$NON-NLS-1$
        tzIDMap.put(tzNames[356]="Australia/Eucla",356); //$NON-NLS-1$
        tzIDMap.put(tzNames[365]="WET",365); //$NON-NLS-1$
        tzIDMap.put(tzNames[366]="CET",366); //$NON-NLS-1$
        tzIDMap.put(tzNames[367]="MET",367); //$NON-NLS-1$
        tzIDMap.put(tzNames[368]="EET",368); //$NON-NLS-1$
        tzIDMap.put(tzNames[369]="Europe/London",369); //$NON-NLS-1$
        tzIDMap.put(tzNames[370]="Europe/Belfast",370); //$NON-NLS-1$
        tzIDMap.put(tzNames[371]="Europe/Dublin",371); //$NON-NLS-1$
        tzIDMap.put(tzNames[372]="Europe/Tirane",372); //$NON-NLS-1$
        tzIDMap.put(tzNames[373]="Europe/Andorra",373); //$NON-NLS-1$
        tzIDMap.put(tzNames[374]="Europe/Vienna",374); //$NON-NLS-1$
        tzIDMap.put(tzNames[375]="Europe/Minsk",375); //$NON-NLS-1$
        tzIDMap.put(tzNames[376]="Europe/Brussels",376); //$NON-NLS-1$
        tzIDMap.put(tzNames[377]="Europe/Sofia",377); //$NON-NLS-1$
        tzIDMap.put(tzNames[378]="Europe/Prague",378); //$NON-NLS-1$
        tzIDMap.put(tzNames[379]="Europe/Copenhagen",379); //$NON-NLS-1$
        tzIDMap.put(tzNames[380]="Europe/Tallinn",380); //$NON-NLS-1$
        tzIDMap.put(tzNames[381]="Europe/Helsinki",381); //$NON-NLS-1$
        tzIDMap.put(tzNames[382]="Europe/Paris",382); //$NON-NLS-1$
        tzIDMap.put(tzNames[383]="Europe/Berlin",383); //$NON-NLS-1$
        tzIDMap.put(tzNames[384]="Europe/Gibraltar",384); //$NON-NLS-1$
        tzIDMap.put(tzNames[385]="Europe/Athens",385); //$NON-NLS-1$
        tzIDMap.put(tzNames[386]="Europe/Budapest",386); //$NON-NLS-1$
        tzIDMap.put(tzNames[387]="Europe/Rome",387); //$NON-NLS-1$
        tzIDMap.put(tzNames[388]="Europe/Riga",388); //$NON-NLS-1$
        tzIDMap.put(tzNames[389]="Europe/Vaduz",389); //$NON-NLS-1$
        tzIDMap.put(tzNames[390]="Europe/Vilnius",390); //$NON-NLS-1$
        tzIDMap.put(tzNames[391]="Europe/Luxembourg",391); //$NON-NLS-1$
        tzIDMap.put(tzNames[392]="Europe/Malta",392); //$NON-NLS-1$
        tzIDMap.put(tzNames[393]="Europe/Chisinau",393); //$NON-NLS-1$
        tzIDMap.put(tzNames[395]="Europe/Monaco",395); //$NON-NLS-1$
        tzIDMap.put(tzNames[396]="Europe/Amsterdam",396); //$NON-NLS-1$
        tzIDMap.put(tzNames[397]="Europe/Oslo",397); //$NON-NLS-1$
        tzIDMap.put(tzNames[398]="Europe/Warsaw",398); //$NON-NLS-1$
        tzIDMap.put(tzNames[399]="Europe/Lisbon",399); //$NON-NLS-1$
        tzIDMap.put(tzNames[400]="Europe/Bucharest",400); //$NON-NLS-1$
        tzIDMap.put(tzNames[401]="Europe/Kaliningrad",401); //$NON-NLS-1$
        tzIDMap.put(tzNames[402]="Europe/Moscow",402); //$NON-NLS-1$
        tzIDMap.put(tzNames[403]="Europe/Samara",403); //$NON-NLS-1$
        tzIDMap.put(tzNames[404]="Europe/Madrid",404); //$NON-NLS-1$
        tzIDMap.put(tzNames[405]="Europe/Stockholm",405); //$NON-NLS-1$
        tzIDMap.put(tzNames[406]="Europe/Zurich",406); //$NON-NLS-1$
        tzIDMap.put(tzNames[407]="Europe/Istanbul",407); //$NON-NLS-1$
        tzIDMap.put(tzNames[408]="Europe/Kiev",408); //$NON-NLS-1$
        tzIDMap.put(tzNames[409]="Europe/Uzhgorod",409); //$NON-NLS-1$
        tzIDMap.put(tzNames[410]="Europe/Zaporozhye",410); //$NON-NLS-1$
        tzIDMap.put(tzNames[411]="Europe/Simferopol",411); //$NON-NLS-1$
        tzIDMap.put(tzNames[412]="Europe/Belgrade",412); //$NON-NLS-1$
        tzIDMap.put(tzNames[413]="Europe/Volgograd",413); //$NON-NLS-1$
        tzIDMap.put(tzNames[435]="Indian/Kerguelen",435); //$NON-NLS-1$
        tzIDMap.put(tzNames[436]="Indian/Chagos",436); //$NON-NLS-1$
        tzIDMap.put(tzNames[437]="Indian/Maldives",437); //$NON-NLS-1$
        tzIDMap.put(tzNames[438]="Indian/Antananarivo",438); //$NON-NLS-1$
        tzIDMap.put(tzNames[439]="Indian/Christmas",439); //$NON-NLS-1$
        tzIDMap.put(tzNames[440]="Indian/Cocos",440); //$NON-NLS-1$
        tzIDMap.put(tzNames[441]="Indian/Comoro",441); //$NON-NLS-1$
        tzIDMap.put(tzNames[442]="Indian/Mahe",442); //$NON-NLS-1$
        tzIDMap.put(tzNames[443]="Indian/Mauritius",443); //$NON-NLS-1$
        tzIDMap.put(tzNames[444]="Indian/Mayotte",444); //$NON-NLS-1$
        tzIDMap.put(tzNames[445]="Indian/Reunion",445); //$NON-NLS-1$
        tzIDMap.put(tzNames[450]="Pacific/Honolulu",450); //$NON-NLS-1$
        tzIDMap.put(tzNames[451]="Pacific/Easter",451); //$NON-NLS-1$
        tzIDMap.put(tzNames[452]="Pacific/Galapagos",452); //$NON-NLS-1$
        tzIDMap.put(tzNames[453]="Pacific/Rarotonga",453); //$NON-NLS-1$
        tzIDMap.put(tzNames[454]="Pacific/Fiji",454); //$NON-NLS-1$
        tzIDMap.put(tzNames[455]="Pacific/Gambier",455); //$NON-NLS-1$
        tzIDMap.put(tzNames[456]="Pacific/Marquesas",456); //$NON-NLS-1$
        tzIDMap.put(tzNames[457]="Pacific/Tahiti",457); //$NON-NLS-1$
        tzIDMap.put(tzNames[458]="Pacific/Guam",458); //$NON-NLS-1$
        tzIDMap.put(tzNames[459]="Pacific/Tarawa",459); //$NON-NLS-1$
        tzIDMap.put(tzNames[460]="Pacific/Enderbury",460); //$NON-NLS-1$
        tzIDMap.put(tzNames[461]="Pacific/Kiritimati",461); //$NON-NLS-1$
        tzIDMap.put(tzNames[462]="Pacific/Saipan",462); //$NON-NLS-1$
        tzIDMap.put(tzNames[463]="Pacific/Majuro",463); //$NON-NLS-1$
        tzIDMap.put(tzNames[464]="Pacific/Kwajalein",464); //$NON-NLS-1$
        tzIDMap.put(tzNames[466]="Pacific/Truk",466); //$NON-NLS-1$
        tzIDMap.put(tzNames[467]="Pacific/Ponape",467); //$NON-NLS-1$
        tzIDMap.put(tzNames[468]="Pacific/Kosrae",468); //$NON-NLS-1$
        tzIDMap.put(tzNames[469]="Pacific/Nauru",469); //$NON-NLS-1$
        tzIDMap.put(tzNames[470]="Pacific/Noumea",470); //$NON-NLS-1$
        tzIDMap.put(tzNames[471]="Pacific/Auckland",471); //$NON-NLS-1$
        tzIDMap.put(tzNames[472]="Pacific/Chatham",472); //$NON-NLS-1$
        tzIDMap.put(tzNames[473]="Pacific/Niue",473); //$NON-NLS-1$
        tzIDMap.put(tzNames[474]="Pacific/Norfolk",474); //$NON-NLS-1$
        tzIDMap.put(tzNames[475]="Pacific/Palau",475); //$NON-NLS-1$
        tzIDMap.put(tzNames[476]="Pacific/Port_Moresby",476); //$NON-NLS-1$
        tzIDMap.put(tzNames[477]="Pacific/Pitcairn",477); //$NON-NLS-1$
        tzIDMap.put(tzNames[478]="Pacific/Pago_Pago",478); //$NON-NLS-1$
        tzIDMap.put(tzNames[479]="Pacific/Apia",479); //$NON-NLS-1$
        tzIDMap.put(tzNames[481]="Pacific/Guadalcanal",481); //$NON-NLS-1$
        tzIDMap.put(tzNames[482]="Pacific/Fakaofo",482); //$NON-NLS-1$
        tzIDMap.put(tzNames[483]="Pacific/Tongatapu",483); //$NON-NLS-1$
        tzIDMap.put(tzNames[484]="Pacific/Funafuti",484); //$NON-NLS-1$
        tzIDMap.put(tzNames[485]="Pacific/Johnston",485); //$NON-NLS-1$
        tzIDMap.put(tzNames[486]="Pacific/Midway",486); //$NON-NLS-1$
        tzIDMap.put(tzNames[487]="Pacific/Wake",487); //$NON-NLS-1$
        tzIDMap.put(tzNames[488]="Pacific/Efate",488); //$NON-NLS-1$
        tzIDMap.put(tzNames[489]="Pacific/Wallis",489); //$NON-NLS-1$
        tzIDMap.put(tzNames[513]="GMT",513); //$NON-NLS-1$
        tzIDMap.put(tzNames[541]="UCT",541); //$NON-NLS-1$
        tzIDMap.put(tzNames[556]="Egypt",556); //$NON-NLS-1$
        tzIDMap.put(tzNames[558]="Africa/Asmera",558); //$NON-NLS-1$
        tzIDMap.put(tzNames[568]="Libya",568); //$NON-NLS-1$
        tzIDMap.put(tzNames[570]="Africa/Timbuktu",570); //$NON-NLS-1$
        tzIDMap.put(tzNames[612]="US/Eastern",612); //$NON-NLS-1$
        tzIDMap.put(tzNames[613]="US/Central",613); //$NON-NLS-1$
        tzIDMap.put(tzNames[614]="Navajo",614); //$NON-NLS-1$
        tzIDMap.put(tzNames[615]="US/Pacific",615); //$NON-NLS-1$
        tzIDMap.put(tzNames[618]="US/Alaska",618); //$NON-NLS-1$
        tzIDMap.put(tzNames[620]="America/Atka",620); //$NON-NLS-1$
        tzIDMap.put(tzNames[621]="US/Arizona",621); //$NON-NLS-1$
        tzIDMap.put(tzNames[623]="America/Fort_Wayne",623); //$NON-NLS-1$
        tzIDMap.put(tzNames[625]="America/Knox_IN",625); //$NON-NLS-1$
        tzIDMap.put(tzNames[627]="America/Kentucky/Louisville",627); //$NON-NLS-1$
        tzIDMap.put(tzNames[628]="US/Michigan",628); //$NON-NLS-1$
        tzIDMap.put(tzNames[630]="Canada/Newfoundland",630); //$NON-NLS-1$
        tzIDMap.put(tzNames[632]="Canada/Atlantic",632); //$NON-NLS-1$
        tzIDMap.put(tzNames[634]="Canada/Eastern",634); //$NON-NLS-1$
        tzIDMap.put(tzNames[638]="Canada/Central",638); //$NON-NLS-1$
        tzIDMap.put(tzNames[639]="Canada/East-Saskatchewan",639); //$NON-NLS-1$
        tzIDMap.put(tzNames[641]="Canada/Mountain",641); //$NON-NLS-1$
        tzIDMap.put(tzNames[642]="Canada/Pacific",642); //$NON-NLS-1$
        tzIDMap.put(tzNames[650]="Canada/Yukon",650); //$NON-NLS-1$
        tzIDMap.put(tzNames[653]="Mexico/General",653); //$NON-NLS-1$
        tzIDMap.put(tzNames[656]="Mexico/BajaSur",656); //$NON-NLS-1$
        tzIDMap.put(tzNames[657]="America/Ensenada",657); //$NON-NLS-1$
        tzIDMap.put(tzNames[665]="Cuba",665); //$NON-NLS-1$
        tzIDMap.put(tzNames[670]="America/Marigot",670); //$NON-NLS-1$
        tzIDMap.put(tzNames[674]="Jamaica",674); //$NON-NLS-1$
        tzIDMap.put(tzNames[686]="America/Virgin",686); //$NON-NLS-1$
        tzIDMap.put(tzNames[687]="America/Argentina/Buenos_Aires",687); //$NON-NLS-1$
        tzIDMap.put(tzNames[689]="America/Argentina/Cordoba",689); //$NON-NLS-1$
        tzIDMap.put(tzNames[690]="America/Argentina/Jujuy",690); //$NON-NLS-1$
        tzIDMap.put(tzNames[691]="America/Argentina/Catamarca",691); //$NON-NLS-1$
        tzIDMap.put(tzNames[692]="America/Argentina/Mendoza",692); //$NON-NLS-1$
        tzIDMap.put(tzNames[695]="Brazil/DeNoronha",695); //$NON-NLS-1$
        tzIDMap.put(tzNames[700]="Brazil/East",700); //$NON-NLS-1$
        tzIDMap.put(tzNames[704]="Brazil/West",704); //$NON-NLS-1$
        tzIDMap.put(tzNames[705]="Brazil/Acre",705); //$NON-NLS-1$
        tzIDMap.put(tzNames[706]="Chile/Continental",706); //$NON-NLS-1$
        tzIDMap.put(tzNames[733]="America/Coral_Harbour",733); //$NON-NLS-1$
        tzIDMap.put(tzNames[748]="Antarctica/South_Pole",748); //$NON-NLS-1$
        tzIDMap.put(tzNames[756]="Asia/Dhaka",756); //$NON-NLS-1$
        tzIDMap.put(tzNames[757]="Asia/Thimbu",757); //$NON-NLS-1$
        tzIDMap.put(tzNames[762]="PRC",762); //$NON-NLS-1$
        tzIDMap.put(tzNames[763]="Asia/Chongqing",763); //$NON-NLS-1$
        tzIDMap.put(tzNames[766]="Hongkong",766); //$NON-NLS-1$
        tzIDMap.put(tzNames[767]="ROC",767); //$NON-NLS-1$
        tzIDMap.put(tzNames[768]="Asia/Macau",768); //$NON-NLS-1$
        tzIDMap.put(tzNames[769]="Europe/Nicosia",769); //$NON-NLS-1$
        tzIDMap.put(tzNames[772]="Asia/Kolkata",772); //$NON-NLS-1$
        tzIDMap.put(tzNames[774]="Asia/Makassar",774); //$NON-NLS-1$
        tzIDMap.put(tzNames[776]="Iran",776); //$NON-NLS-1$
        tzIDMap.put(tzNames[778]="Asia/Tel_Aviv",778); //$NON-NLS-1$
        tzIDMap.put(tzNames[779]="Japan",779); //$NON-NLS-1$
        tzIDMap.put(tzNames[785]="ROK",785); //$NON-NLS-1$
        tzIDMap.put(tzNames[793]="Asia/Ulan_Bator",793); //$NON-NLS-1$
        tzIDMap.put(tzNames[797]="Asia/Kathmandu",797); //$NON-NLS-1$
        tzIDMap.put(tzNames[804]="Singapore",804); //$NON-NLS-1$
        tzIDMap.put(tzNames[809]="Asia/Ashkhabad",809); //$NON-NLS-1$
        tzIDMap.put(tzNames[813]="Asia/Ho_Chi_Minh",813); //$NON-NLS-1$
        tzIDMap.put(tzNames[845]="Atlantic/Faeroe",845); //$NON-NLS-1$
        tzIDMap.put(tzNames[846]="Iceland",846); //$NON-NLS-1$
        tzIDMap.put(tzNames[857]="Australia/North",857); //$NON-NLS-1$
        tzIDMap.put(tzNames[858]="Australia/West",858); //$NON-NLS-1$
        tzIDMap.put(tzNames[859]="Australia/Queensland",859); //$NON-NLS-1$
        tzIDMap.put(tzNames[861]="Australia/South",861); //$NON-NLS-1$
        tzIDMap.put(tzNames[862]="Australia/Tasmania",862); //$NON-NLS-1$
        tzIDMap.put(tzNames[863]="Australia/Victoria",863); //$NON-NLS-1$
        tzIDMap.put(tzNames[864]="Australia/ACT",864); //$NON-NLS-1$
        tzIDMap.put(tzNames[865]="Australia/Yancowinna",865); //$NON-NLS-1$
        tzIDMap.put(tzNames[866]="Australia/LHI",866); //$NON-NLS-1$
        tzIDMap.put(tzNames[881]="GB",881); //$NON-NLS-1$
        tzIDMap.put(tzNames[883]="Eire",883); //$NON-NLS-1$
        tzIDMap.put(tzNames[890]="Europe/Bratislava",890); //$NON-NLS-1$
        tzIDMap.put(tzNames[893]="Europe/Mariehamn",893); //$NON-NLS-1$
        tzIDMap.put(tzNames[899]="Europe/Vatican",899); //$NON-NLS-1$
        tzIDMap.put(tzNames[905]="Europe/Tiraspol",905); //$NON-NLS-1$
        tzIDMap.put(tzNames[909]="Arctic/Longyearbyen",909); //$NON-NLS-1$
        tzIDMap.put(tzNames[910]="Poland",910); //$NON-NLS-1$
        tzIDMap.put(tzNames[911]="Portugal",911); //$NON-NLS-1$
        tzIDMap.put(tzNames[914]="W-SU",914); //$NON-NLS-1$
        tzIDMap.put(tzNames[919]="Turkey",919); //$NON-NLS-1$
        tzIDMap.put(tzNames[924]="Europe/Ljubljana",924); //$NON-NLS-1$
        tzIDMap.put(tzNames[962]="US/Hawaii",962); //$NON-NLS-1$
        tzIDMap.put(tzNames[963]="Chile/EasterIsland",963); //$NON-NLS-1$
        tzIDMap.put(tzNames[976]="Kwajalein",976); //$NON-NLS-1$
        tzIDMap.put(tzNames[978]="Pacific/Yap",978); //$NON-NLS-1$
        tzIDMap.put(tzNames[983]="NZ",983); //$NON-NLS-1$
        tzIDMap.put(tzNames[984]="NZ-CHAT",984); //$NON-NLS-1$
        tzIDMap.put(tzNames[990]="US/Samoa",990); //$NON-NLS-1$
        tzIDMap.put(tzNames[1025]="Etc/GMT+0",1025); //$NON-NLS-1$
        tzIDMap.put(tzNames[1052]="Universal",1052); //$NON-NLS-1$
        tzIDMap.put(tzNames[1126]="US/Mountain",1126); //$NON-NLS-1$
        tzIDMap.put(tzNames[1132]="US/Aleutian",1132); //$NON-NLS-1$
        tzIDMap.put(tzNames[1135]="US/East-Indiana",1135); //$NON-NLS-1$
        tzIDMap.put(tzNames[1137]="US/Indiana-Starke",1137); //$NON-NLS-1$
        tzIDMap.put(tzNames[1151]="Canada/Saskatchewan",1151); //$NON-NLS-1$
        tzIDMap.put(tzNames[1169]="Mexico/BajaNorte",1169); //$NON-NLS-1$
        tzIDMap.put(tzNames[1182]="America/St_Barthelemy",1182); //$NON-NLS-1$
        tzIDMap.put(tzNames[1201]="America/Rosario",1201); //$NON-NLS-1$
        tzIDMap.put(tzNames[1203]="America/Argentina/ComodRivadavia",1203); //$NON-NLS-1$
        tzIDMap.put(tzNames[1217]="America/Rio_Branco",1217); //$NON-NLS-1$
        tzIDMap.put(tzNames[1290]="Israel",1290); //$NON-NLS-1$
        tzIDMap.put(tzNames[1376]="Australia/Canberra",1376); //$NON-NLS-1$
        tzIDMap.put(tzNames[1393]="GB-Eire",1393); //$NON-NLS-1$
        tzIDMap.put(tzNames[1411]="Europe/San_Marino",1411); //$NON-NLS-1$
        tzIDMap.put(tzNames[1421]="Atlantic/Jan_Mayen",1421); //$NON-NLS-1$
        tzIDMap.put(tzNames[1431]="Asia/Istanbul",1431); //$NON-NLS-1$
        tzIDMap.put(tzNames[1436]="Europe/Sarajevo",1436); //$NON-NLS-1$
        tzIDMap.put(tzNames[1502]="Pacific/Samoa",1502); //$NON-NLS-1$
        tzIDMap.put(tzNames[1537]="GMT+0",1537); //$NON-NLS-1$
        tzIDMap.put(tzNames[1564]="Etc/Zulu",1564); //$NON-NLS-1$
        tzIDMap.put(tzNames[1637]="CST",1637); //$NON-NLS-1$
        tzIDMap.put(tzNames[1638]="America/Shiprock",1638); //$NON-NLS-1$
        tzIDMap.put(tzNames[1639]="US/Pacific-New",1639); //$NON-NLS-1$
        tzIDMap.put(tzNames[1647]="America/Indiana/Indianapolis",1647); //$NON-NLS-1$
        tzIDMap.put(tzNames[1888]="Australia/NSW",1888); //$NON-NLS-1$
        tzIDMap.put(tzNames[1905]="Europe/Jersey",1905); //$NON-NLS-1$
        tzIDMap.put(tzNames[1948]="Europe/Skopje",1948); //$NON-NLS-1$
        tzIDMap.put(tzNames[2049]="Etc/GMT-0",2049); //$NON-NLS-1$
        tzIDMap.put(tzNames[2076]="Zulu",2076); //$NON-NLS-1$
        tzIDMap.put(tzNames[2151]="PST",2151); //$NON-NLS-1$
        tzIDMap.put(tzNames[2417]="Europe/Guernsey",2417); //$NON-NLS-1$
        tzIDMap.put(tzNames[2460]="Europe/Zagreb",2460); //$NON-NLS-1$
        tzIDMap.put(tzNames[2561]="GMT-0",2561); //$NON-NLS-1$
        tzIDMap.put(tzNames[2972]="Europe/Podgorica",2972); //$NON-NLS-1$
        tzIDMap.put(tzNames[3073]="Etc/GMT0",3073); //$NON-NLS-1$
        tzIDMap.put(tzNames[3585]="GMT0",3585); //$NON-NLS-1$
        tzIDMap.put(tzNames[4097]="Etc/Greenwich",4097); //$NON-NLS-1$
        tzIDMap.put(tzNames[4609]="Greenwich",4609); //$NON-NLS-1$
        
        /*String[] availableIDs = TimeZone.getAvailableIDs();
        Arrays.sort(availableIDs);  
        for (String id : availableIDs) {  
            TimeZone tz = TimeZone.getTimeZone(id);  
            boolean found = false;  
            for (TimeZoneGroup tzGroup : groups) {  
                if (tz.hasSameRules(tzGroup.example)) {  
                    tzGroup.ids.add(tz.getID());  
                    found = true;  
                    break;  
                }  
            }  
            if (!found) {  
                groups.add(new TimeZoneGroup(tz));  
            }  
        }*/
        
        /*
        tz2idMap.put("Africa/Asmara","Africa/Asmera");
        tz2idMap.put("America/Argentina/Catamarca","America/Catamarca");
        tz2idMap.put("America/Argentina/Cordoba","America/Cordoba");
        tz2idMap.put("America/Argentina/Jujuy","America/Jujuy");
        tz2idMap.put("America/Kentucky/Louisville","America/Louisville");
        tz2idMap.put("America/Argentina/Mendoza","America/Mendoza");
        tz2idMap.put("America/Argentina/Ushuaia","America/Rosario");
        tz2idMap.put("Antarctica/Vostok","Africa/Timbuktu");
        tz2idMap.put("Asia/Ashgabat","Asia/Ashkhabad");
        tz2idMap.put("Asia/Chongqing","Asia/Chungking");
        tz2idMap.put("Asia/Dhaka","Asia/Dacca");
        tz2idMap.put("Asia/Kathmandu","Asia/Katmandu");
        tz2idMap.put("Asia/Kolkata","Asia/Calcutta");
        tz2idMap.put("Asia/Macau","Asia/Macao");
        tz2idMap.put("Asia/Thimphu","Asia/Thimbu");
        tz2idMap.put("Atlantic/Faroe","Atlantic/Faeroe");
        tz2idMap.put("Etc/Universal","UTC");
        tz2idMap.put("Europe/Isle_of_Man","Isle_of_Man");
        tz2idMap.put("Europe/Nicosia","Asia/Nicosia");
        tz2idMap.put("Pacific/Pohnpei","Pacific/Ponape");
        */
    }
    
    public static OraTimeZoneUtil getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new OraTimeZoneUtil();
        }
        
        return INSTANCE;
    }
    
    public int getID(String region) {
        Integer id = tzIDMap.get(region);
        
        if (id == null || id.intValue() == -1) {
            id = ZONEIDMAP.getID(region);
            /*if (id == null || id.intValue() == -1) {
                region = tz2idMap.get(region);
                
                if (region != null) {
                    id = tzIDMap.get(region);
                    
                    if (id == null || id.intValue() == -1) {
                        id = ZONEIDMAP.getID(region);
                    }
                }
            }*/
        }
        
        /*if (id == -1 && region != null & region.length() > 3) {
            TimeZone tz = TimeZone.getTimeZone(region);
            
            if (tz != null) {
                for (TimeZoneGroup tzGroup : groups) {  
                    if (tz.hasSameRules(tzGroup.example)) { 
                        if (tzGroup.tla_example != null) {
                            id = getID(tzGroup.tla_example.getID());
                        }
                        break;
                    }  
                }  
            }
        }*/
        
        return (id != null) ? id.intValue() : -1;
    }

    public String getRegion(int id) {
        String region = null;
        
        try {
            region = tzNames[id];
        } catch (IndexOutOfBoundsException e) {
            ; // Use ZONEIDMAP
        }
        
        if (region == null) {
            region = ZONEIDMAP.getRegion(id);
        }
        
        String altRegion = null; //id2tzMap.get(region);
        
        return (altRegion != null) ? altRegion : region;
    }

    public boolean isValidID(int id) {
        return (getRegion(id) != null);
    }

    public boolean isValidRegion(String region) {
        return (getID(region) > -1);
    }
}
