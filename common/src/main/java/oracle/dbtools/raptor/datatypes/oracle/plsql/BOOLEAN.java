/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.plsql;

import java.util.LinkedList;
import java.util.List;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingStrategy;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.strategies.callablestatement.CallableBindingBOOLEAN;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.values.DataValueMapped;
import oracle.dbtools.raptor.utils.DataTypesUtil;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=BOOLEAN.java">Barry McGillin</a> 
 *
 */
public class BOOLEAN extends PLSQLDatum {
    private final DataType numberType;
    private DataValue trueValue; // $NOT_FINAL_OK: Lazy/delayed initialization only
    private DataValue falseValue; // $NOT_FINAL_OK: Lazy/delayed initialization only

    protected BOOLEAN(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);

        DataTypeFactory factory = context.getDataTypeFactory();
        
        TypeMetadata numberMetadata =
            factory.getTypeMetadata(context.getDataTypeConnectionProvider(), OracleTypes.NUMBER);

        numberType = factory.getDataType(context, numberMetadata);
        
        trueValue = null;
        falseValue = null;;
    }
    
    public DataValue getTrueValue() {
        DataValue value = trueValue;
        
        if (value == null) {
            synchronized(this) {
                value = trueValue;
                
                if (value == null) {
                    value = getDataValue(Boolean.TRUE);
                    trueValue = value;
                }
            }
        }
        
        return trueValue;
    }

    public DataValue getFalseValue() {
        DataValue value = falseValue;
        
        if (value == null) {
            synchronized(this) {
                value = falseValue;
                
                if (value == null) {
                    value = getDataValue(Boolean.FALSE);
                    falseValue = value;
                }
            }
        }
        
        return falseValue;
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        return new StringValue(internalStringValue(value.getInternalValue()));
    }

    private String internalStringValue(Object value) {
        String stringValue = DataTypesUtil.stringValue(value, null);

        if (stringValue != null) {
            Boolean boolValue = null;

            try {
                int intValue = Integer.parseInt(stringValue);

                boolValue = Boolean.valueOf((intValue == 0) ? false : true);
            } catch (NumberFormatException e) {
                boolValue = Boolean.valueOf(stringValue);
            }

            stringValue = DataTypesUtil.stringValue(boolValue, null);
        }

        return stringValue;
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case DEFAULT:
                return Boolean.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.BOOLEAN;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        if (value instanceof Boolean) {
            // already correctly typed
            return value;
        } else {
            return Boolean.valueOf(internalStringValue(value));
        }
    }

    @Override
    protected String customPlainDataTypeString() {
        return "BOOLEAN";
    }

    @Override
    protected String customBaseDataTypeString() {
        return "PL/SQL BOOLEAN";
    }

    @Override
    protected DataValue customDataValue(Object object) {
        DataValue typedDataValue = super.customDataValue(object);

        Boolean boolValue = (Boolean)typedDataValue.getTypedValue(ValueType.DEFAULT);

        Integer bitValue =
            (boolValue == null) ? null : ((boolValue.booleanValue() == true) ? 1 : 0);

        DataValue bindingDataValue = numberType.getDataValue(bitValue);

        return new DataValueMapped(typedDataValue, bindingDataValue);
    }

    @Override
    public boolean isEnumeration() {
        return true;
    }

    @Override
    public List<DataValue> getEnumerationValues() {
        final DataValue trueValue = getTrueValue();
        final DataValue falseValue = getFalseValue();
        
        return new LinkedList<DataValue>() {
            {
                add(trueValue);
                add(falseValue);
            }
        };
    }

    public <P extends DataBinding> BindingStrategy getBind(BindContext context, P param) {
        return new CallableBindingBOOLEAN<OracleCallableStatement,P>(context, param, numberType);
    }
}
