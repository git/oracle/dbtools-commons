/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.plsql;

import java.io.IOException;

import java.net.URI;

import java.net.URISyntaxException;

import java.sql.Connection;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension.ExtensionAccess;
import oracle.dbtools.raptor.datatypes.DataTypeProvider;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.TypeMetadata.Attribute;
import oracle.dbtools.raptor.datatypes.impl.AbstractDataTypeProviderImpl;

import oracle.dbtools.raptor.datatypes.metadata.ArgMetadata;
import oracle.dbtools.raptor.datatypes.metadata.MetadataLoader;

import oracle.jdbc.OracleTypes;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=OraclePLSQLDataTypeFactory.java">Barry McGillin</a> 
 *
 */
public final class OraclePLSQLDataTypeFactory extends AbstractDataTypeProviderImpl {
    private final PLSQLDataTypeProviderAccess providerAccess;

    public class PLSQLDataTypeProviderAccess implements DataTypeProviderAccess {
        protected PLSQLDataTypeProviderAccess() {};
        
        public DataTypeProvider getDataTypeProvider() {
            return OraclePLSQLDataTypeFactory.this;
        }
        
        public ExtensionAccess getExtensionAccess() {
            return OraclePLSQLDataTypeFactory.this.getExtensionAccess();
        }
        
        public DataTypeFactory getDataTypeFactory() {
            return getExtensionAccess().getDataTypeFactory();
        }
    }

    @Override
    protected PLSQLDataTypeProviderAccess getDataTypeProviderAccess() {
        return providerAccess;
    }

    public static Builder builder(URI dataTypeProviderURI) {
        return new Builder(dataTypeProviderURI);
    }
    
    public final static class Builder extends AbstractDataTypeProviderImpl.Builder {
        public static final URI ID;

        static {
            URI tmpURI = null;
            
            // Initialize PLSQL DataTypeProvider URI
            try {
                tmpURI = new URI("http://www.oracle.com/raptor/datatypes/providers/plsql/");
            } catch(URISyntaxException e) {
                ;
            }
            
            ID = tmpURI;
        }

        private Builder(URI dataTypeProviderURI) {
            super(dataTypeProviderURI);
        }

        public PLSQLDataTypeProviderAccess build() {
            OraclePLSQLDataTypeFactory datatypeFactory = 
                new OraclePLSQLDataTypeFactory(getExtensionAccess(),
                                               getDataTypeProviderURI(),
                                               getExtensionAccess().resolveDataTypeProvider(getSuperDataTypeProviderURI()), 
                                               typeMetadataProps);
            
            return datatypeFactory.getDataTypeProviderAccess();
        }

        @Override
        public Builder setExtensionAccess(ExtensionAccess extensionAccess) {
            super.setExtensionAccess(extensionAccess);
            return this;
        }

        @Override
        public Builder setSuperDataTypeProviderURI(URI superDataTypeProvider) {
            super.setSuperDataTypeProviderURI(superDataTypeProvider);
            return this;
        }
    }
    
    private final static String PLSQL_RECORD = "PL/SQL RECORD"; //$NON-NLS-1$
    private final static String PLSQL_INDEX_TABLE = "PL/SQL INDEX TABLE"; //$NON-NLS-1$
    private final static String PLSQL_TABLE = "PL/SQL TABLE"; //$NON-NLS-1$
    private final static String TABLE = "TABLE"; //$NON-NLS-1$
    private final static String ARRAY = "ARRAY"; //$NON-NLS-1$
    private final static String VARRAY = "VARRAY"; //$NON-NLS-1$
    private final static String VARYING_ARRAY = "VARYING ARRAY"; //$NON-NLS-1$
    private final static String CURSOR_FOR_NAME = CURSOR_FOR.class.getName();

    private static OraclePLSQLDataTypeFactory instance;

    private static final Map<String, NamedValue<Map<TypeMetadata.Attribute, Object>>> typeMetadataProps =
        new HashMap<String, NamedValue<Map<TypeMetadata.Attribute, Object>>>() {
        {
            put("PL/SQL BOOLEAN",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.BOOLEAN);
                            put(TypeMetadata.Attribute.IMPL_CLASS, BOOLEAN.class);
                        }
                    })
               );
            put("BOOLEAN",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "PL/SQL BOOLEAN", null) //$NON-NLS-1$
               );
            /*put("TINYINT",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NUMBER", 
                        new HashMap<TypeMetadata.Attribute, Object>() {
                            {
                                put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.TINYINT);
                                put(TypeMetadata.Attribute.DATA_PRECISION, 1);
                                put(TypeMetadata.Attribute.DATA_SCALE, 0);
                            }
                    })
               );
            put("BIT",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TINYINT", 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.BIT);
                        }
                    })
               );*/
            put("BINARY_INTEGER", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTEGER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "BINARY_INTEGER"); //$NON-NLS-1$
                            put(TypeMetadata.Attribute.IMPL_CLASS, BINARY_INTEGER.class);
                        }
                    })
               );
            // Float Points
            put("FLOAT", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NUMBER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.FLOAT);
                            put(TypeMetadata.Attribute.BASE_TYPE, "FLOAT"); //$NON-NLS-1$
                            put(TypeMetadata.Attribute.IMPL_CLASS, FLOAT.class);
                        }
                    })
               );
            put("DOUBLE PRECISION", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "FLOAT",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.DOUBLE);
                            put(TypeMetadata.Attribute.BASE_TYPE, "DOUBLE PRECISION"); //$NON-NLS-1$
                        }
                    })
               );
            put("REAL", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "FLOAT",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.REAL);
                            put(TypeMetadata.Attribute.BASE_TYPE, "REAL"); //$NON-NLS-1$
                        }
                    })
               );
            put("PLS_INTEGER", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "BINARY_INTEGER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "PLS_INTEGER"); //$NON-NLS-1$
                        }
                    })
               );
            put("NATURAL", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "BINARY_INTEGER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "NATURAL"); //$NON-NLS-1$
                            put(TypeMetadata.Attribute.IMPL_CLASS, NATURAL.class);
                        }
                    })
               );
            put("NATURALN", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "BINARY_INTEGER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "NATURALN"); //$NON-NLS-1$
                            put(TypeMetadata.Attribute.IMPL_CLASS, NATURALN.class);
                        }
                    })
               );
            put("POSITIVE", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "BINARY_INTEGER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "POSITIVE"); //$NON-NLS-1$
                            put(TypeMetadata.Attribute.IMPL_CLASS, POSITIVE.class);
                        }
                    })
               );
            put("POSITIVEN", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "BINARY_INTEGER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "POSITIVEN"); //$NON-NLS-1$
                            put(TypeMetadata.Attribute.IMPL_CLASS, POSITIVEN.class);
                        }
                    })
               );
            put("SIMPLE_INTEGER", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "BINARY_INTEGER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "SIMPLE_INTEGER"); //$NON-NLS-1$
                            put(TypeMetadata.Attribute.IMPL_CLASS, SIMPLE_INTEGER.class);
                        }
                    })
               );
            put("SIGNTYPE", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "BINARY_INTEGER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "SIGNTYPE"); //$NON-NLS-1$
                            put(TypeMetadata.Attribute.IMPL_CLASS, SIGNTYPE.class);
                        }
                    })
               );

            // Extended Types
            put(PLSQL_INDEX_TABLE,
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null,
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.PLSQL_INDEX_TABLE);
                            put(TypeMetadata.Attribute.IMPL_CLASS, PLSQL_INDEX_TABLE.class);
                            put(TypeMetadata.Attribute.TYPE_COMPONENTS, null);
                        }
                    })
               );
            put(PLSQL_RECORD,
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null,
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, DataType.Types.RECORD);
                            put(TypeMetadata.Attribute.IMPL_CLASS, RECORD.class);
                            put(TypeMetadata.Attribute.TYPE_COMPONENTS, null);
                        }
                    })
               );
            put(PLSQL_TABLE,
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    PLSQL_INDEX_TABLE, null)
               );
            put(VARYING_ARRAY,
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    ARRAY, null)
               );
            put(VARRAY,
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    VARYING_ARRAY, null)
               );
            put(TABLE,
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    VARRAY,
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, TABLE);
                        }
                    })
               );
            
            put("TIMESTAMP(9)", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.DATA_SCALE, 9);
                        }
                    })
               );
            put("TIMESTAMP_UNCONSTRAINED",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP(9)", null) //$NON-NLS-1$
               );
            put("TIMESTAMP(9) WITH TIME ZONE", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP WITH TIME ZONE",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.DATA_SCALE, 9);
                        }
                    })
               );
            put("TIMESTAMP_TZ_UNCONSTRAINED",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP(9) WITH TIME ZONE", null) //$NON-NLS-1$
               );
            put("TIMESTAMP(9) WITH LOCAL TIME ZONE", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP WITH LOCAL TIME ZONE",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.DATA_SCALE, 9);
                        }
                    })
               );
            put("TIMESTAMP_LTZ_UNCONSTRAINED",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP(9) WITH LOCAL TIME ZONE", null) //$NON-NLS-1$
               );
            put("INTERVAL DAY(9) TO SECOND(9)", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTERVAL DAY TO SECOND",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.DATA_PRECISION, 9);
                            put(TypeMetadata.Attribute.DATA_SCALE, 9);
                        }
                    })
               );
            put("DSINTERVAL_UNCONSTRAINED",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTERVAL DAY(9) TO SECOND(9)", null) //$NON-NLS-1$
               );
            put("INTERVAL YEAR(9) TO MONTH", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTERVAL YEAR TO MONTH",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.DATA_PRECISION, 9);
                        }
                    })
               );
            put("YMINTERVAL_UNCONSTRAINED",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTERVAL YEAR(9) TO MONTH", null) //$NON-NLS-1$
               );
            
            // Cursor/ResultSet
            put("PL/SQL CURSOR_FOR", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.CURSOR);
                            put(TypeMetadata.Attribute.IMPL_CLASS, CURSOR_FOR.class);
                        }
                    })
               );
            put("CURSOR", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "PL/SQL CURSOR_FOR", null) //$NON-NLS-1$
               );
            put("REF CURSOR", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "CURSOR", null) //$NON-NLS-1$
               );
        }
    };
        
        
    public OraclePLSQLDataTypeFactory(ExtensionAccess extensionAccess,
                                      URI dataTypeProviderURI,
                                      DataTypeProvider next,
                                      Map<String, NamedValue<Map<TypeMetadata.Attribute, Object>>> typeMetadataProps) {
        super(extensionAccess, dataTypeProviderURI, next, typeMetadataProps);
        
        this.providerAccess = new PLSQLDataTypeProviderAccess();
    }
    
    protected boolean isStructuredType(String typeName) {
        return (PLSQL_RECORD.equals(typeName) ||
                PLSQL_TABLE.equals(typeName) ||
                PLSQL_INDEX_TABLE.equals(typeName) ||
                TABLE.equals(typeName) ||
                ARRAY.equals(typeName) ||
                VARRAY.equals(typeName) ||
                VARYING_ARRAY.equals(typeName));
    }

    protected boolean isSupportedType(String typeName) {
        return (PLSQL_RECORD.equals(typeName) ||
                PLSQL_TABLE.equals(typeName) ||
                PLSQL_INDEX_TABLE.equals(typeName));
    }

    protected boolean customSupportedType(TypeMetadata typeMetadata) {
        // PL/SQL Types have as type name and sub-name
        return (super.customSupportedType(typeMetadata) && 
        		(
        				(typeMetadata.get_type_name() == null && typeMetadata.get_type_subname() == null) ||
        				(
        						typeMetadata.get_type_name() != null && typeMetadata.get_type_subname() != null && 
        						typeMetadata.get_base_type() != null && isSupportedType(typeMetadata.get_base_type())
        				)
                )
               );
    }
    
    @Override
    protected TypeMetadata customRefineTypeMetadata(DataTypeContext context, final TypeMetadata typeMetadata) {
        TypeMetadata candidate = typeMetadata;
        
        boolean triedSynonym = false;
        do {
            String type_owner = candidate.get_type_owner();

            // Ensure qualify by owner
            if (type_owner == null) {
                return candidate;
            }
            
            String type_subname = candidate.get_type_subname();
            
            // Ensure type name qualify by owner
            if (type_subname == null) {
                return candidate;
            }
            
            String type_name = candidate.get_type_name();
            
            // Ensure has a type name which is not remote
            if (type_name == null || candidate.get_type_link() != null) {
                return candidate;
            }
            
            String data_type = candidate.get_data_type();
            
            // Query database catalog
            if (data_type == null) {
                Map<Attribute, Object> attributeMap = new HashMap<Attribute, Object>();

                // Try Type from database catalog
                DataTypeConnectionProvider provider = context.getDataTypeConnectionProvider();
                
                Connection conn = provider.getDataTypeConnection();
                if (conn != null) {
                    provider.lockDataTypeConnection();
                    
                    try {
                        new MetadataLoader(conn).loadPLSQLType(attributeMap, type_owner, type_name, type_subname);
                    } catch (Exception e) {
                        ;
                    } finally {
                        provider.unlockDataTypeConnection();
                    }
                }
                
                if (!attributeMap.isEmpty()) {
                    candidate = DataTypeFactory.getTypeMetadata(candidate, attributeMap);
                    data_type = candidate.get_data_type();
                }
            }
            
            // Refine Collection
            if ("COLLECTION".equals(data_type)) {
                DataTypeConnectionProvider provider = context.getDataTypeConnectionProvider();
                
                Map<Attribute, Object> attributeMap = new HashMap<Attribute, Object>();

                Connection conn = provider.getDataTypeConnection();
                if (conn != null) {
                    provider.lockDataTypeConnection();
                    
                    try {
                        Deque<ArgMetadata> loadStack = new LinkedList<ArgMetadata>();
                        
                        new MetadataLoader(conn).loadPLSQLCollection(loadStack, type_owner, type_name, type_subname, 0);
                        
                        if (!loadStack.isEmpty()) {
                            ArgMetadata collArgMetadata = loadStack.pop();
                            
                            String collType = (String)collArgMetadata.getValue().get(Attribute.DATA_TYPE);
                            
                            attributeMap.put(Attribute.DATA_TYPE, collType);
                            
                            // Added missing components - performance improvement
                            List<NamedValue<TypeMetadata>> typeComponents = candidate.get_type_components();
                            if (typeComponents == null) {
                                List<NamedValue<TypeMetadata>> components = new LinkedList<NamedValue<TypeMetadata>>();

                                while (!loadStack.isEmpty() && loadStack.peek().getIdentifier().getLevel() == 1) {
                                    ArgMetadata componentArgMetadata = loadStack.pop();
                                    
                                    TypeMetadata componentTypeMetadata = context.getDataTypeFactory().getTypeMetadata(componentArgMetadata.getValue());
                                    
                                    components.add(new NamedValue<TypeMetadata>(componentArgMetadata.getName(), componentTypeMetadata));
                                }
                                
                                if (components.size() > 0) {
                                    attributeMap.put(Attribute.TYPE_COMPONENTS, components);
                                }
                            }
                        }
                    } catch (Exception e) {
                        ;
                    } finally {
                        provider.unlockDataTypeConnection();
                    }
                }
                
                if (!attributeMap.isEmpty()) {        
                    return DataTypeFactory.getTypeMetadata(candidate, attributeMap);
                }
            } else if (data_type != null) {
                break;
            }
            
            // Break if better candiidate found
            if (candidate != typeMetadata) {
                break;
            }
            
            // Infinite Loop safe-guard
            if (triedSynonym) {
                return candidate;
            } else {
                triedSynonym = true;
            }
            
            // Try Synonym
            DataTypeConnectionProvider provider = context.getDataTypeConnectionProvider();
            
            Map<Attribute, Object> attributeMap = new HashMap<Attribute, Object>();

            Connection conn = provider.getDataTypeConnection();
            if (conn != null) {
                provider.lockDataTypeConnection();
                
                try {
                    new MetadataLoader(conn).followSQLSynonym(attributeMap, type_owner, type_name);
                } catch (Exception e) {
                    ;
                } finally {
                    provider.unlockDataTypeConnection();
                }
            }
            
            if (!attributeMap.isEmpty()) {
                candidate = DataTypeFactory.getTypeMetadata(candidate, attributeMap);
                continue;
            } else {
                return candidate;
            }
        } while (true);

        return candidate;
    }

    protected TypeMetadata customFilterExpandedTypeMetadata(TypeMetadata expandedTypeMetadata, TypeMetadata typeMetadata) {
        if (expandedTypeMetadata != null) {
            String implClassName = expandedTypeMetadata.get_impl_class_name();
            
            if (CURSOR_FOR_NAME.equals(implClassName)) { //TODO: Handle multiple inheritance
                if (typeMetadata != null) {
                    String implDataType = typeMetadata.get_impl_data_type();
                    
                    if (implDataType == null || !implDataType.equals("PL/SQL CURSOR_FOR")) {
                        return null;
                    }
                }
            }
        }
        
        return super.customFilterExpandedTypeMetadata(expandedTypeMetadata, typeMetadata);
    }

    @Override
    public void close() throws IOException {
        ; // Move along - nothing to see here
    }
}
