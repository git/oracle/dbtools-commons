/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.plsql;

import java.lang.reflect.Array;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingStrategy;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.DataTypeSQLException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.StructureType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.metadata.ArgMetadata;
import oracle.dbtools.raptor.datatypes.metadata.MetadataLoader;
import oracle.dbtools.raptor.datatypes.strategies.callablestatement.CallableBindingIndexTABLE;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.values.CompositeValue;
import oracle.dbtools.raptor.datatypes.values.NamedDataValue;
import oracle.dbtools.raptor.utils.DataTypesUtil;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;

/**
 * Implemented PL/SQL Index Table
 */
@SuppressWarnings("unchecked")
public class PLSQL_INDEX_TABLE extends PLSQLDatum {
    protected final DataType subType;

    protected PLSQL_INDEX_TABLE(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, expandTypeMetadata(context, typeMetadata));

        subType = getTypeComponents().iterator().next().getValue();;
    }

    protected static TypeMetadata expandTypeMetadata(DataTypeContext context, TypeMetadata typeMetadata) {
        TypeMetadata newTypeMetadata = typeMetadata;
        
        if (typeMetadata.getAttribute(TypeMetadata.Attribute.TYPE_COMPONENTS) == null) {
            try {
                // Load Attributes
                List<NamedValue<TypeMetadata>> attributes = loadAttributes(context, typeMetadata);
                
                Map<TypeMetadata.Attribute, Object> attributeMap = new HashMap<TypeMetadata.Attribute, Object>();
                attributeMap.put(TypeMetadata.Attribute.TYPE_COMPONENTS, attributes);
                
                newTypeMetadata = DataTypeFactory.getTypeMetadata(typeMetadata, attributeMap);
            } catch (SQLException e) {
                throw new DataTypeSQLException(e);
            }
        }
        
        return newTypeMetadata;
    }
    
    private static List<NamedValue<TypeMetadata>> loadAttributes(DataTypeContext context, TypeMetadata typeMetadata) throws SQLException {
        DataTypeConnectionProvider provider = context.getDataTypeConnectionProvider();
        
        List<NamedValue<TypeMetadata>> typeComponents = new LinkedList<NamedValue<TypeMetadata>>();
        
        Connection conn = provider.lockDataTypeConnection();
        
        if (conn != null) {
            try {
                Deque<ArgMetadata> loadStack = new LinkedList<ArgMetadata>();
                
                final String typeOwner = typeMetadata.get_type_owner();
                final String typeName = typeMetadata.get_type_name();
                final String typeSubName = typeMetadata.get_type_subname();
                
                new MetadataLoader(conn).loadPLSQLCollection(loadStack, typeOwner, typeName, typeSubName, 0);
                
                if (!loadStack.isEmpty()) {
                    loadStack.pop(); // Ignore Collection recrd
                    
                    while (!loadStack.isEmpty() && loadStack.peek().getIdentifier().getLevel() == 1) {
                        ArgMetadata argMetadata = loadStack.pop();
                        
                        TypeMetadata componentTypeMetadata = context.getDataTypeFactory().getTypeMetadata(argMetadata.getValue());
                        
                        typeComponents.add(new NamedValue<TypeMetadata>(argMetadata.getName(), componentTypeMetadata));
                    }
                }
            } finally {
                provider.unlockDataTypeConnection();
            }
        }
    
        return typeComponents;
    }

    /**
     * Get to String value
     * 
     * @param connectionProvider
     * @param value
     * @param stringType
     * @param maxLen
     * @return
     */
    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        StringBuilder sb = new StringBuilder();
        int len = 0;
        
        // Append Subtype String
        String subTypeSting = subType.getPlainDataTypeString();
        if (maxLen < 0 || sb.length() < maxLen) {
            sb.append(subTypeSting);
        }
        len = len + subTypeSting.length();

        // Append Open Brace
        if (maxLen < 0 || sb.length() < maxLen) {
            sb.append("("); //$NON-NLS-1$
        }
        len++;

        // Ensure is record
        List<DataValue> components = getComponents(value);

        int i = 0;
        for (DataValue component : components) {
            // Append Separator
            if (i > 0) {
                if (maxLen < 0 || sb.length() < maxLen) {
                    sb.append(", "); //$NON-NLS-1$
                }
                len += 2;
            }
            
            // Append Component String
            String componentValue = "" + component.getStringValue(connectionProvider, stringType, maxLen); //$NON-NLS-1$
            if (maxLen < 0 || sb.length() < maxLen) {
                sb.append(componentValue);
            }
            len = len + componentValue.length();
            
            i++;
        }

        // Append Close Brace
        if (maxLen < 0 || sb.length() < maxLen) {
            sb.append(")");
        }
        len++;

        return new StringValue(sb.toString(), len);
    }

    /**
     * Get Type Value
     * 
     * @param connectionProvider
     * @param value
     * @param valueType
     * @param target
     * @return
     */
    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        ArrayList<DataValue> dataValueArray = customComponents(value);
        int arraySize = dataValueArray.size();

        ArrayList objectArray = new ArrayList(arraySize);

        // Convert elements to typed values
        for (DataValue dataValue : dataValueArray) {
            objectArray.add(dataValue.getTypedValue(valueType));
        }
        
        if (valueType == ValueType.JDBC) {
            Object[] arrayTemplate = (Object[])Array.newInstance(subType.getTypedClass(valueType), arraySize);

            return objectArray.toArray(arrayTemplate);
        } else {
            return objectArray;
        }
    }

    /**
     * Get Typed Value class
     * 
     * @param connectionProvider
     * @param valueType
     * @return
     */
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case JDBC:
                Object[] arrayTemplate = (Object[])Array.newInstance(subType.getTypedClass(valueType), 0);
                return arrayTemplate.getClass();
            default:
                return ArrayList.class;
        }
    }

    /**
     * Get SQL Type code
     * 
     * @param valueType
     * @return
     */
    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.PLSQL_INDEX_TABLE;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    /**
     * Get Internal Storage Value
     * 
     * @param connectionProvider
     * @param value
     * @return
     */
    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        ArrayList<DataValue> dataValueArray = new ArrayList<DataValue>();

        try {
            if (value == null) {
                return customInternalValue(connectionProvider, dataValueArray);
            } else if (value instanceof oracle.sql.Datum[]) {
                oracle.sql.Datum[] datumArray = (oracle.sql.Datum[])value;

                for (oracle.sql.Datum datum : datumArray) {
                    dataValueArray.add(subType.getDataValue(datum));
                }

                return dataValueArray;
            } else if (value instanceof oracle.sql.ARRAY) {
                oracle.sql.ARRAY valueArray = (oracle.sql.ARRAY)value;

                oracle.sql.Datum[] datumArray = valueArray.getOracleArray();

                return customInternalValue(connectionProvider, datumArray);
            } else if (value.getClass().isArray()) {
                int arrayLength = Array.getLength(value);

                for (int i = 0; i < arrayLength; i++) {
                    Object arrayElement = Array.get(value, i);
                    DataValue dataValue = subType.getDataValue(arrayElement);
                    dataValueArray.add(dataValue);
                }

                return dataValueArray;
            } else if (value instanceof Collection) {
                Collection collection = (Collection)value;

                Object[] values = collection.toArray(new Object[0]);

                return customInternalValue(connectionProvider, values);
            } else {
                throw new DataTypeIllegalArgumentException(this, value);
            }
        } catch (SQLException e) {
            throw new DataTypeIllegalArgumentException(this, value);
        }
    }

    @Override
    protected Object customInternalValueFilter(DataTypeConnectionProvider connectionProvider, Object value) {
        return customInternalValue(connectionProvider, (DataTypesUtil.isNull(value)) ? null : value);
    }

    @Override
    protected ArrayList<DataValue> customComponents(DataValueInternal value) {
        return (ArrayList<DataValue>)value.getInternalValue();
    }

    @Override
    protected DataValue customDataValue(Object object) {
        return new CompositeValue(this, object);
    }

    @Override
    public Object startDataValue(String name, boolean isNull) {
        if (isNull) {
            return null;
        } else {
            return customInternalValue(getDataTypeContext().getDataTypeConnectionProvider(), null);
        }
    }

    @Override
    public void bodyDataValue(NamedValue value, char[] ch, int start, int length) {
        ; // Ignore body data;
    }

    @Override
    public void bodyDataValue(NamedValue value, String text) {
        ; // Ignore body data;
    }

    @Override
    public void bodyDataValue(NamedValue value, DataValue dataValue) {
        ArrayList<DataValue> dataValueArray = (ArrayList<DataValue>)value.getValue();
        dataValueArray.add(NamedDataValue.getNamedDataValue(null, dataValue));
    }

    @Override
    public DataValue endDataValue(NamedValue value) {
        return customDataValue(value.getValue());
    }

    public <P extends DataBinding> BindingStrategy getBind(BindContext context, P param) {
        return new CallableBindingIndexTABLE<OracleCallableStatement,P>(context, param);
    }
    
    @Override
    protected boolean customRequiresConnection() {
        return true;
    }


    /**
     * Determine if Supported. RECOD subtypes are not supported
     * 
     * @return
     */
    @Override
    protected boolean customSupported() {
        boolean supported = super.customSupported();
        
        if (supported && subType != null) {
            int typeCode = subType.getSqlDataType(ValueType.JDBC);
            
            if (typeCode == DataType.Types.RECORD) {
                supported = false;
            }
        }
        
        return supported;
    }

    @Override
    public StructureType getStructureType() {
        return StructureType.TABLE;
    }
}
