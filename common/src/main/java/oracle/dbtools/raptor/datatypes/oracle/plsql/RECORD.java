/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.plsql;

import java.lang.reflect.Array;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import java.util.Map;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingStrategy;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.DataTypeSQLException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.StructureType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.metadata.ArgMetadata;
import oracle.dbtools.raptor.datatypes.metadata.MetadataLoader;
import oracle.dbtools.raptor.datatypes.objects.PLSQLRecord;
import oracle.dbtools.raptor.datatypes.strategies.callablestatement.CallableBindingRECORD;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.values.CompositeValue;
import oracle.dbtools.raptor.datatypes.values.NamedDataValue;
import oracle.dbtools.raptor.utils.DataTypesUtil;

import oracle.jdbc.OracleCallableStatement;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=RECORD.java">Barry McGillin</a>
 *
 */
public class RECORD extends PLSQLDatum {
    protected RECORD(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, expandTypeMetadata(context, typeMetadata));
    }

    protected static TypeMetadata expandTypeMetadata(DataTypeContext context, TypeMetadata typeMetadata) {
        TypeMetadata newTypeMetadata = typeMetadata;
        
        if (typeMetadata.getAttribute(TypeMetadata.Attribute.TYPE_COMPONENTS) == null) {
            try {
                // Load Attributes
                List<NamedValue<TypeMetadata>> attributes = loadAttributes(context, typeMetadata);
                
                Map<TypeMetadata.Attribute, Object> attributeMap = new HashMap<TypeMetadata.Attribute, Object>();
                attributeMap.put(TypeMetadata.Attribute.TYPE_COMPONENTS, attributes);
                
                newTypeMetadata = DataTypeFactory.getTypeMetadata(typeMetadata, attributeMap);
            } catch (SQLException e) {
                throw new DataTypeSQLException(e);
            }
        }
        
        return newTypeMetadata;
    }
    
    private static List<NamedValue<TypeMetadata>> loadAttributes(DataTypeContext context, TypeMetadata typeMetadata) throws SQLException {
        DataTypeConnectionProvider provider = context.getDataTypeConnectionProvider();
        
        List<NamedValue<TypeMetadata>> typeComponents = new LinkedList<NamedValue<TypeMetadata>>();
        
        Connection conn = provider.lockDataTypeConnection();
        
        if (conn != null) {
            try {
                Deque<ArgMetadata> loadStack = new LinkedList<ArgMetadata>();
                
                final String typeOwner = typeMetadata.get_type_owner();
                final String typeName = typeMetadata.get_type_name();
                final String typeSubName = typeMetadata.get_type_subname();
                
                new MetadataLoader(conn).loadPLSQLAttributes(loadStack, typeOwner, typeName, typeSubName, 0);
                
                while (!loadStack.isEmpty()) {
                    ArgMetadata argMetadata = loadStack.pop();
                    
                    TypeMetadata componentTypeMetadata = context.getDataTypeFactory().getTypeMetadata(argMetadata.getValue());
                    
                    typeComponents.add(new NamedValue<TypeMetadata>(argMetadata.getName(), componentTypeMetadata));
                }
            } finally {
                provider.unlockDataTypeConnection();
            }
        }
    
        return typeComponents;
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        StringBuilder sb = new StringBuilder();
        int len = 0;
        
        // Append Open Brace
        if (maxLen < 0 || sb.length() < maxLen) {
            sb.append("<"); //$NON-NLS-1$
        }
        len++;

        // Ensure is record
        List<DataValue> components = getComponents(value);

        int i = 0;
        for (DataValue component : components) {
            // Append Separator
            if (i > 0) {
                if (maxLen < 0 || sb.length() < maxLen) {
                    sb.append(", "); //$NON-NLS-1$
                }
                len += 2;
            }
            
            String fieldTag = (component.getName() != null) ? component.getName() + "=" : "";
            String componentValue = "" + component.getStringValue(connectionProvider, stringType, maxLen); //$NON-NLS-1$

            // Append Field Tag
            if (maxLen < 0 || sb.length() < maxLen) {
                sb.append(fieldTag);
            }
            len = len + fieldTag.length();

            // Append Component String
            if (maxLen < 0 || sb.length() < maxLen) {
                sb.append(componentValue);
            }
            len = len + componentValue.length();

            i++;
        }

        // Append Close Brace
        if (maxLen < 0 || sb.length() < maxLen) {
            sb.append(">");
        }
        len++;

        return new StringValue(sb.toString(), len);
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        PLSQLRecord record = new PLSQLRecord(this);

        LinkedList<DataValue> dataValueList = customComponents(value);

        for (DataValue dataValue : dataValueList) {
            record.add(dataValue.getTypedValue(valueType));
        }
        
        return record;
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        return PLSQLRecord.class;
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return Types.RECORD;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        LinkedList<DataValue> dataValueList = new LinkedList<DataValue>();

        if (value == null) {
            return customInternalValue(connectionProvider, dataValueList);
        } else if (value.getClass().isArray()) {
            int arrayLength = Array.getLength(value);

            // Via Types
            int i = 0;
            Iterable<NamedValue<DataType>> components = getTypeComponents();
            for (NamedValue<DataType> typeComponent : components) {
                // Missing fields will become null
                Object fieldValue = (i < arrayLength) ? Array.get(value, i++) : null;

                DataValue newDataValue = typeComponent.getValue().getDataValue(fieldValue);
                dataValueList.add(NamedDataValue.getNamedDataValue(typeComponent.getName(), newDataValue));
            }

            return dataValueList;
        } else if (value instanceof Collection) {
            Collection collection = (Collection)value;

            Object[] values = collection.toArray(new Object[0]);

            return customInternalValue(connectionProvider, values);
        } else if (value instanceof PLSQLRecord) {
            PLSQLRecord record = (PLSQLRecord)value;
            return getInternalValue(record.getRoot());
        } else {
            throw new DataTypeIllegalArgumentException(this, value);
        }
    }

    public static String getUserDataTypeString(TypeMetadata typeMetadata) {
        // Build without OWNER
        String _type_name = typeMetadata.get_type_name();
        String _type_subname = typeMetadata.get_type_subname();
        
        StringBuilder buffer = new StringBuilder();
        
        appendIdentifier(buffer, _type_name);
        appendIdentifier(buffer, _type_subname);
        
        return (buffer.length() == 0) ? null : buffer.toString();
    }

    @Override
    protected String customUserDataTypeString() {
        return RECORD.getUserDataTypeString(typeMetadata);
    }

    @Override
    protected Object customInternalValueFilter(DataTypeConnectionProvider connectionProvider, Object value) {
        return customInternalValue(connectionProvider, (DataTypesUtil.isNull(value)) ? null : value);
    }

    public <P extends DataBinding> BindingStrategy getBind(BindContext context, P param) {
        return new CallableBindingRECORD<OracleCallableStatement,P>(context, param);
    }

    @Override
    protected LinkedList<DataValue> customComponents(DataValueInternal value) {
        return (LinkedList<DataValue>)value.getInternalValue();
    }

    @Override
    protected DataValue customDataValue(Object object) {
        return new CompositeValue(this, object);
    }

    @Override
    public Object startDataValue(String name, boolean isNull) {
        return customInternalValue(getDataTypeContext().getDataTypeConnectionProvider(), null);
    }

    @Override
    public void bodyDataValue(NamedValue value, char[] ch, int start, int length) {
        ; // Ignore body data;
    }

    @Override
    public void bodyDataValue(NamedValue value, String text) {
        ; // Ignore body data;
    }

    @Override
    public void bodyDataValue(NamedValue value, DataValue dataValue) {
        LinkedList<DataValue> dataValueList = (LinkedList<DataValue>)value.getValue();
        
        // Insert Value into list - ignore if does not match to handle updated records
        int i = 0;
        for (DataValue valueSlot : dataValueList) {
            String slotName = valueSlot.getName();
            String valueName = dataValue.getName();
            if (slotName != null && valueName != null &&
                slotName.equals(valueName)) {
                dataValueList.set(i, NamedDataValue.getNamedDataValue(slotName, dataValue));
            }
            i++;
        }
    }

    @Override
    public DataValue endDataValue(NamedValue value) {
        return customDataValue(value.getValue());
    }

    @Override
    public StructureType getStructureType() {
        return StructureType.RECORD;
    }
}
