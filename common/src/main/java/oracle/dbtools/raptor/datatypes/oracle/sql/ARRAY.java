/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.lang.reflect.Array;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import java.util.Map;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.DataTypeSQLException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.StructureType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.values.CompositeValue;
import oracle.dbtools.raptor.datatypes.values.NamedDataValue;
import oracle.dbtools.raptor.utils.DataTypesUtil;

import oracle.dbtools.raptor.utils.JDBCProxyUtil;

import oracle.jdbc.OracleTypes;

import oracle.sql.ArrayDescriptor;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ARRAY.java">Barry McGillin</a> 
 *
 */
@SuppressWarnings("unchecked")
public class ARRAY extends DatumWithConnection {
    protected final DataType subType;

    protected ARRAY(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, expandTypeMetadata(context, typeMetadata));

        subType = getTypeComponents().iterator().next().getValue();
    }

    protected static TypeMetadata expandTypeMetadata(DataTypeContext context, TypeMetadata typeMetadata) {
        TypeMetadata newTypeMetadata = typeMetadata;
        
        if (typeMetadata.getAttribute(TypeMetadata.Attribute.TYPE_COMPONENTS) == null) {
            try {
                // Load Attributes
                List<NamedValue<TypeMetadata>> attributes = loadAttributes(context, typeMetadata);
                
                Map<TypeMetadata.Attribute, Object> attributeMap = new HashMap<TypeMetadata.Attribute, Object>();
                attributeMap.put(TypeMetadata.Attribute.TYPE_COMPONENTS, attributes);
                
                newTypeMetadata = DataTypeFactory.getTypeMetadata(typeMetadata, attributeMap);
            } catch (SQLException e) {
                throw new DataTypeSQLException(e);
            }
        }
        
        return newTypeMetadata;
    }
    
    private static List<NamedValue<TypeMetadata>> loadAttributes(DataTypeContext context, TypeMetadata typeMetadata) throws SQLException {
        DataTypeConnectionProvider provider = context.getDataTypeConnectionProvider();
        
        List<NamedValue<TypeMetadata>> typeComponents = new LinkedList<NamedValue<TypeMetadata>>();
        
        Connection conn = provider.lockDataTypeConnection();
        
        if (conn != null) {
            try {
                // Get Delegate - Bug 19513781 STRUCT does a downcast to an internal concrete class so unwrap
                Connection validConnection = JDBCProxyUtil.getInstance().unwrap(conn);
                
                ArrayDescriptor desc =
                    ArrayDescriptor.createDescriptor(getUserDataTypeString(typeMetadata), validConnection);
                
                // Get TypeMetadata object
                TypeMetadata componentTypeMetadata = context.getDataTypeFactory().getTypeMetadata(desc.getBaseType(), desc.getBaseName());
                
                typeComponents.add(new NamedValue<TypeMetadata>(null, componentTypeMetadata));
            } finally {
                provider.unlockDataTypeConnection();
            }
        }
    
        return typeComponents;
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        StringBuilder sb = new StringBuilder();
        int len = 0;
        
        // Append Subtype String
        String subTypeSting = subType.getPlainDataTypeString();
        if (maxLen < 0 || sb.length() < maxLen) {
            sb.append(subTypeSting);
        }
        len = len + subTypeSting.length();

        // Append Open Brace
        if (maxLen < 0 || sb.length() < maxLen) {
            sb.append("("); //$NON-NLS-1$
        }
        len++;

        // Ensure is record
        List<DataValue> components = getComponents(value);

        int i = 0;
        for (DataValue component : components) {
            // Append Separator
            if (i > 0) {
                if (maxLen < 0 || sb.length() < maxLen) {
                    sb.append(", "); //$NON-NLS-1$
                }
                len += 2;
            }
            
            // Append Component String
            String componentValue = "" + component.getStringValue(connectionProvider, stringType, maxLen); //$NON-NLS-1$
            if (maxLen < 0 || sb.length() < maxLen) {
                sb.append(componentValue);
            }
            len = len + componentValue.length();
            
            i++;
        }

        // Append Close Brace
        if (maxLen < 0 || sb.length() < maxLen) {
            sb.append(")");
        }
        len++;

        return new StringValue(sb.toString(), len);
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        Object internalValue = value.getInternalValue();
        
        try {
            ArrayList<DataValue> dataValueArray = customComponents(value);
            int arraySize = dataValueArray.size();

            ArrayList objectArray = new ArrayList(arraySize);
            
            Class subTypeJDBCClass = subType.getTypedClass(ValueType.JDBC);
            
            // Avoid VARCHAR2 datum in ARRAY
            ValueType subTypeValueType = (String.class.equals(subTypeJDBCClass)) ? ValueType.JDBC : valueType;

            // Convert elements to typed values
            for (DataValue dataValue : dataValueArray) {
                objectArray.add(dataValue.getTypedValue(subTypeValueType));
            }

            Object[] arrayTemplate = (Object[])Array.newInstance(subType.getTypedClass(subTypeValueType), arraySize);

            switch (valueType) {
                case JDBC:
                    return objectArray.toArray(arrayTemplate);
                case DATUM:
                    // Create new oracle.sql.ARRAY
                    Connection validConnection = connectionProvider.getValidDataTypeConnection();
                
                    // Get Delegate - Bug 19513781 STRUCT does a downcast to an internal concrete class so unwrap
                    validConnection = JDBCProxyUtil.getInstance().unwrap(validConnection);
                
                    ArrayDescriptor desc =
                        ArrayDescriptor.createDescriptor(getUserDataTypeString(), validConnection);
                    return new oracle.sql.ARRAY(desc, validConnection,
                                                objectArray.toArray(arrayTemplate));
                default:
                    return objectArray;
            }
        } catch (SQLException e) {
            throw new DataTypeIllegalArgumentException(this, internalValue);
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case JDBC:
                Object[] arrayTemplate = (Object[])Array.newInstance(subType.getTypedClass(valueType), 0);
                return arrayTemplate.getClass();
            case DEFAULT:
                return oracle.jdbc.OracleArray.class;
            default:
                return ArrayList.class;
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.ARRAY;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        ArrayList<DataValue> dataValueArray = new ArrayList<DataValue>();

        try {
            if (value == null) {
                return customInternalValue(connectionProvider, dataValueArray);
            } else if (value instanceof oracle.sql.Datum[]) {
                oracle.sql.Datum[] datumArray = (oracle.sql.Datum[])value;

                for (oracle.sql.Datum datum : datumArray) {
                    dataValueArray.add(subType.getDataValue(datum));
                }

                return dataValueArray;
            } else if (value instanceof oracle.jdbc.internal.OracleArray) {
                oracle.jdbc.internal.OracleArray valueArray = (oracle.jdbc.internal.OracleArray)value;

                oracle.sql.Datum[] datumArray = valueArray.getOracleArray();

                return customInternalValue(connectionProvider, datumArray);
            } else if (value.getClass().isArray()) {
                int arrayLength = Array.getLength(value);

                for (int i = 0; i < arrayLength; i++) {
                    Object arrayElement = Array.get(value, i);
                    DataValue dataValue = subType.getDataValue(arrayElement);
                    dataValueArray.add(dataValue);
                }

                return dataValueArray;
            } else if (value instanceof Collection) {
                Collection collection = (Collection)value;

                Object[] values = collection.toArray(new Object[0]);

                return customInternalValue(connectionProvider, values);
            } else {
                throw new DataTypeIllegalArgumentException(this, value);
            }
        } catch (SQLException e) {
            throw new DataTypeIllegalArgumentException(this, value);
        }
    }

    @Override
    protected Object customInternalValueFilter(DataTypeConnectionProvider connectionProvider, Object value) {
        if (DataTypesUtil.isEmpty(value)) {
            return customInternalValue(connectionProvider, null);
        } else {
            return super.customInternalValueFilter(connectionProvider, value);
        }
    }

    @Override
    protected ArrayList<DataValue> customComponents(DataValueInternal value) {
        return (ArrayList<DataValue>)value.getInternalValue();
    }

    @Override
    protected DataValue customDataValue(Object object) {
        return new CompositeValue(this, object);
    }

    @Override
    public Object startDataValue(String name, boolean isNull) {
        if (isNull) {
            return null;
        } else {
            return customInternalValue(getDataTypeContext().getDataTypeConnectionProvider(), null);
        }
    }

    @Override
    public void bodyDataValue(NamedValue value, char[] ch, int start, int length) {
        ; // Ignore body data;
    }

    @Override
    public void bodyDataValue(NamedValue value, String text) {
        ; // Ignore body data;
    }

    @Override
    public void bodyDataValue(NamedValue value, DataValue dataValue) {
        ArrayList<DataValue> dataValueArray = (ArrayList<DataValue>)value.getValue();
        dataValueArray.add(NamedDataValue.getNamedDataValue(null, dataValue));
    }

    @Override
    public DataValue endDataValue(NamedValue value) {
        return customDataValue(value.getValue());
    }

    @Override
    public StructureType getStructureType() {
        return StructureType.TABLE;
    }
}

