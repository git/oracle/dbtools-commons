/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.math.BigDecimal;

import java.sql.SQLException;

import java.util.Arrays;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.dbtools.raptor.nls.OracleNLSProvider;

import oracle.jdbc.OracleTypes;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=BINARY_FLOAT.java">Barry McGillin</a> 
 *
 */
public class BINARY_FLOAT extends NumericDatum {
    protected BINARY_FLOAT(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        Object internalValue = value.getInternalValue();
        switch (stringType) {
            case REST:
            case GENERIC:
                // Store number in normal number escape format
                try {
                   return new StringValue(getGenericStringValue((oracle.sql.BINARY_FLOAT)internalValue));
                } catch (SQLException e) {
                   // SQLException signals Illegal Argument - not connection related
                   throw new DataTypeIllegalArgumentException(this, internalValue);
                }
            default:
                return super.customStringValue(connectionProvider, value, stringType, maxLen);
        }
    }
    
    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        switch (valueType) {
            case JAVA:
                return super.customTypedValue(connectionProvider, value, ValueType.JDBC, target);
            case JDBC:
                return customTypedValue(connectionProvider, value, ValueType.DEFAULT, target);
            default:
                return super.customTypedValue(connectionProvider, value, valueType, target);
        }
    }

    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case JAVA:
                return Float.class;
            case JDBC:
                return customTypedClass(connectionProvider, ValueType.DEFAULT);
            case DEFAULT:
                return oracle.sql.BINARY_FLOAT.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.BINARY_FLOAT;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customUnscaledInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        try {
            if (value instanceof oracle.sql.BINARY_FLOAT) {
                // already correctly typed
                return value;
            } else if (value instanceof Float) {
                return new oracle.sql.BINARY_FLOAT(((Float)value));
            } else if (value instanceof Double) {
                return new oracle.sql.BINARY_FLOAT(((Double)value).floatValue());
            } else if (value instanceof byte[]) {
                return new oracle.sql.BINARY_FLOAT(Arrays.copyOf((byte[])value, ((byte[])value).length));
            } else {
                String stringValue = value.toString();
                // Need to accept both NLS and GENERIC formats
                
                if (isPosInf(stringValue)) {
                        return new oracle.sql.BINARY_FLOAT(Float.POSITIVE_INFINITY);
                } else if (isNegInf(stringValue)) {
                    return new oracle.sql.BINARY_FLOAT(Float.NEGATIVE_INFINITY);
                } else if (isNaN(stringValue)) {
                    return new oracle.sql.BINARY_FLOAT(Float.NaN);
                } else {
                    try {
                        return new oracle.sql.BINARY_FLOAT(Float.valueOf(stringValue));
                    } catch (Exception e1) {
                        return typedValueFromNLSString(connectionProvider, stringValue);
                    }
                }
            }
        } catch (SQLException e) {
            // SQLException signals Illegal Argument - not connection related   
            throw new DataTypeIllegalArgumentException(this, value);
        }
    }

    protected oracle.sql.BINARY_FLOAT typedValueFromNLSString(DataTypeConnectionProvider connectionProvider, String value) throws SQLException {
        return new oracle.sql.BINARY_FLOAT(((OracleNLSProvider)NLSProvider.getProvider(connectionProvider.getNLSConnection())).parseNumber(value).floatValue());
    }
    
    private BigDecimal getBigDecimal(oracle.sql.BINARY_FLOAT flt) throws SQLException {
        BigDecimal ret = null;
        
        if (flt != null) {
            ret = new BigDecimal(Double.valueOf(flt.stringValue()));
        }
        
        return ret;
    }
    
    private String getGenericStringValue(oracle.sql.BINARY_FLOAT flt) throws SQLException {
        String ret = null;
        
        if (flt != null) {
            ret =getBigDecimal(flt).toString();
        }
        
        return ret;
    }
}
