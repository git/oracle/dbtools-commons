/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingStrategy;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;
import oracle.dbtools.raptor.datatypes.strategies.callablestatement.CallableBindingCURSOR;

import oracle.dbtools.raptor.datatypes.strategies.preparedstatement.PrepareBindingCURSOR;
import oracle.dbtools.raptor.datatypes.values.ResultSetValue;
import oracle.dbtools.raptor.utils.DataTypesUtil;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleTypes;

import org.xml.sax.SAXException;

/**
 * Implements a SQL Cursor value.
 * 
 * The string value is now cached  so that it many be retrieved
 * multiple times.
 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=CURSOR.java">Barry McGillin</a> 
 *
 */
public class CURSOR extends Datum {
    protected CURSOR(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        java.sql.ResultSet ret = (java.sql.ResultSet)value;
        return ret;
    }

    @Override
    protected DataValue customDataValue(Object object) {
        return new ResultSetValue(this, object);
    }
    
    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        switch (valueType) {
            case JDBC:
                return customTypedValue(connectionProvider, value, ValueType.DEFAULT, target);
            default:
                return super.customTypedValue(connectionProvider, value, valueType, target);
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case DEFAULT:
                return java.sql.ResultSet.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.CURSOR;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    /**
     * Compares 32K string value
     * 
     * @param obj1
     * @param obj2
     * @return
     */
    @Override
    protected boolean customEquals(Object obj1, Object obj2) {
        DataTypeConnectionProvider provider = getDataTypeConnectionProvider();
	    
        try {
            // No longer supported
            /*long leftHash = hashResultSet(provider, (java.sql.ResultSet)obj1); 
            long rightHash = hashResultSet(provider, (java.sql.ResultSet)obj2); 
            return (leftHash == rightHash);
`           */
            
            // Use Text comparison
            Connection nlsConnection = provider.peekNLSConnection();
            String leftText = DataTypesUtil.stringValueChecked(obj1, nlsConnection, 32768);
            String rightText = DataTypesUtil.stringValueChecked(obj2, nlsConnection, 32768);
            
            return areEquals(leftText, rightText);
        } catch (Exception e) {
            // TODO: Handle exception
            return false;
        }
    }
    
    public <P extends DataBinding> BindingStrategy getBind(BindContext context, P param) {
        if (CallableStatement.class.isAssignableFrom(context.getEffectiveStatementClass())) {
            return new CallableBindingCURSOR<OracleCallableStatement,P>(context, param);
        } else {
            return new PrepareBindingCURSOR<OraclePreparedStatement,P>(context, param);
        }
    }
    
    @Override
    public Object startDataValue(String name, boolean isNull) {
        return null;
    }
    
    @Override
    public void bodyDataValue(NamedValue value, char[] ch, int start, int length) {
        ; // Null Operation
    }

    @Override
    protected void customMarshal(DataTypeConnectionProvider connectionProvider, DataValueInternal value, DataValueMarshalHandler hd, String name) throws SAXException {
        hd.bodyDataValue(this, name, (String)null);
    }

    private long hashResultSet(DataTypeConnectionProvider provider, java.sql.ResultSet rs) throws SQLException {

        long hash = 0;
        
        Connection conn = provider.lockDataTypeConnection();

        if (conn != null) {

            try {
                String sql = "DECLARE " + //$NON-NLS-1$
                    "   ctx DBMS_XMLGEN.ctxHandle; " + //$NON-NLS-1$
                    "   hashSum NUMBER; " + //$NON-NLS-1$
                    "   TYPE rc IS REF CURSOR; " + //$NON-NLS-1$
                    "   ref1 rc := :p1; " + //$NON-NLS-1$
                    "BEGIN " + //$NON-NLS-1$
                    "   ctx := DBMS_XMLGEN.newContext(ref1); " + //$NON-NLS-1$
                    "   DBMS_XMLGEN.setMaxRows(ctx, 1);" + //$NON-NLS-1$
                    "   hashSum := 0; " + //$NON-NLS-1$
                    "   LOOP " + //$NON-NLS-1$
                    "       DECLARE " + //$NON-NLS-1$
                    "           rowVal CLOB; " + //$NON-NLS-1$
                    "           hashVal NUMBER; " + //$NON-NLS-1$
                    "       BEGIN " + //$NON-NLS-1$
                    "           rowVal := DBMS_XMLGEN.getXML(ctx); " + //$NON-NLS-1$
                    "           EXIT WHEN DBMS_XMLGEN.getNumRowsProcessed(ctx) = 0; " +
                    //$NON-NLS-1$
                    "           hashVal := DBMS_UTILITY.get_hash_value(rowVal, 1, POWER(2,16)-1); " +
                    //$NON-NLS-1$
                    "           hashSum := hashSum + hashVal; " + //$NON-NLS-1$
                    "       END; " + //$NON-NLS-1$
                    "   END LOOP; " + //$NON-NLS-1$
                    "   DBMS_XMLGEN.closeContext(ctx); " + //$NON-NLS-1$
                    "   ? := hashSum;" + //$NON-NLS-1$
                    "END;"; //$NON-NLS-1$

                OracleCallableStatement stmt = (OracleCallableStatement)conn.prepareCall(sql);
                stmt.setCursorAtName("p1", rs);
                stmt.registerOutParameter(2, java.sql.Types.NUMERIC);

                try {
                    stmt.execute();
                    hash = stmt.getLong(2);
                } finally {
                    stmt.close();
                }

            } finally {
                provider.unlockDataTypeConnection();
            }
        }

        return hash;
    }
}
