/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;

import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;

import oracle.jdbc.OracleTypes;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=CharacterDatum.java">Barry McGillin</a> 
 *
 */
public abstract class CharacterDatum extends Datum {
    protected CharacterDatum(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        Object internalValue = value.getInternalValue();
        
        String stringValue = (String)internalValue;
        try {
            switch (valueType) {
                case DATUM:
                    return new oracle.sql.CHAR(stringValue, oracle.sql.CHAR.DEFAULT_CHARSET);
                default:
                    return super.customTypedValue(connectionProvider, value, valueType, target);
            }
        } catch (SQLException e) {
            throw new DataTypeIllegalArgumentException(this, internalValue);
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return oracle.sql.CHAR.class;
            case DEFAULT:
                return String.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.CHAR;
            default:
                return super.customSqlDataType(valueType);
        }
    }
    
    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        if (value instanceof String) {
            // already correctly typed
            return value;
        } else if (value instanceof oracle.sql.CHAR) {
            oracle.sql.CHAR charString = (oracle.sql.CHAR)value;
            return charString.stringValue();
        } else {
            return String.valueOf(value);
        }
    }

    @Override
    protected String customDataTypeString() {
        String _plain_type = getPlainDataTypeString();
        String _char_used = typeMetadata.get_char_used();
        Integer _char_length = typeMetadata.get_char_length();

        return buildConstraintedString(_plain_type, _char_used, _char_length);
    }

    @Override
    protected String customConstrainedDataTypeString() {
        Integer _char_length = typeMetadata.get_char_length();

        if (_char_length == null || 0 == _char_length) {
            return customUnconstrainedDataTypeString();
        } else {
            return super.customConstrainedDataTypeString();
        }
    }

    @Override
    protected String customUnconstrainedDataTypeString() {
        String _char_used = typeMetadata.get_char_used();

        return buildConstraintedString(getPlainDataTypeString(), _char_used, 2000);
    }

    private String buildConstraintedString(String typeString, String charUsed,
                                           Integer charLength) {
        // Qualify datatype
        if (typeString != null && charLength != null) {
            StringBuffer buffer = new StringBuffer(typeString);

            if (charUsed != null && TypeMetadata.BYTE_FLAG.equals(charUsed)) {
                buffer.append("(").append(charLength).append(" BYTES").append(")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            } else if (charUsed != null && TypeMetadata.CHAR_FLAG.equals(charUsed)) {
                buffer.append("(").append(charLength).append(" CHARS").append(")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            } else {
                buffer.append("(").append(charLength).append(")"); //$NON-NLS-1$ //$NON-NLS-2$
            }

            return buffer.toString();
        } else {
            return typeString;
        }
    }
}
