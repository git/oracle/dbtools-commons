/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Arrays;

import java.util.TimeZone;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.objects.OraDATE;
import oracle.dbtools.raptor.datatypes.objects.OraTemporalDatum;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.util.TemporalUtil;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.dbtools.raptor.nls.OraDATEFormat;
import oracle.dbtools.raptor.nls.OracleNLSProvider;
import oracle.jdbc.OracleTypes;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=DATE.java">Barry McGillin</a> 
 *
 */
public class DATE extends TemporalDatum {
    protected DATE(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        Object internalValue = value.getInternalValue();
        
        switch (stringType) {
            case REST:
                try {
                    if (internalValue instanceof oracle.sql.DATE) {
                        TimeZone sessionTZ = (NLSProvider.getProvider(connectionProvider.getNLSConnection())).getSessionTimeZone();
                        OraDATE date = OraDATE.getInstance((oracle.sql.DATE)internalValue).setSessionTimeZone(sessionTZ);
                    
                        return new StringValue(TemporalUtil.toRestString(date));
                    }
                } catch (Exception e) {
                    throw new DataTypeIllegalArgumentException(this, internalValue);
                }
            case GENERIC:
                // Store date in JDBC date escape format
                // Formats a date in the date escape format yyyy-mm-dd.
                return new StringValue(((oracle.sql.DATE)internalValue).timestampValue().toString().substring(0,19));
            default:
                return super.customStringValue(connectionProvider, value, stringType, maxLen);
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case JDBC:
                return Timestamp.class;
            case DEFAULT:
                return oracle.sql.DATE.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.DATE;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        if (value instanceof oracle.sql.DATE) {
            // already correctly typed
            return value;
        } else if (value instanceof byte[]) {
            return new oracle.sql.DATE(Arrays.copyOf((byte[])value, ((byte[])value).length));
        } else {
            // Try super class
            Object superObject = super.customInternalValue(connectionProvider, value);
            
            if (superObject instanceof OraTemporalDatum) {
                return new oracle.sql.DATE(((OraTemporalDatum)superObject).toTimestamp());
            }
            
            // Try NLS format
            return typedValueFromNLSString(connectionProvider, superObject.toString());
        }
    }

    protected oracle.sql.DATE typedValueFromNLSString(DataTypeConnectionProvider connectionProvider, String value) {
        OraDATEFormat format;
        
        try {
            format = ((OracleNLSProvider)NLSProvider.getProvider(connectionProvider.getNLSConnection())).getOraDATEFormat();
        } catch(ParseException pe) {
            format = null;
        }

        return format.parse(value, new ParsePosition(0));
    }
}
