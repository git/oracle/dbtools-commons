/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.sql.CallableStatement;
import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingStrategy;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.DataTypeSQLException;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataTypeImpl;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.strategies.callablestatement.CallableBindingDatumAtName;
import oracle.dbtools.raptor.datatypes.strategies.preparedstatement.PrepareBindingDatumAtName;
import oracle.dbtools.raptor.datatypes.util.StringValue;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OraclePreparedStatement;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Datum.java">Barry McGillin</a>
 *
 */
public abstract class Datum extends DataTypeImpl {
    protected Datum(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        switch (stringType) {
            case REST:
            case GENERIC:
                Object internalValue = value.getInternalValue();
                if (internalValue instanceof oracle.sql.Datum) {
                    try {
                        return new StringValue(((oracle.sql.Datum)internalValue).stringValue());
                    } catch (SQLException e) {
                        throw new DataTypeSQLException(e);
                    }
                } else {
                    return super.customStringValue(connectionProvider, value, stringType, maxLen);
                }
            default:
                return super.customStringValue(connectionProvider, value, stringType, maxLen);
        }
    }

    public <P extends DataBinding> BindingStrategy getBind(BindContext context, P param) {
        if (CallableStatement.class.isAssignableFrom(context.getEffectiveStatementClass())) {
            return new CallableBindingDatumAtName<OracleCallableStatement,P>(context, param);
        } else {
            return new PrepareBindingDatumAtName<OraclePreparedStatement,P>(context, param);
        }
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        Object internalValue = value.getInternalValue();
        
        try {
            switch (valueType) {
                case JAVA:
                    return customTypedValue(connectionProvider, value, ValueType.JDBC, target);
                case DATUM:
                    {
                        Object superValue = super.customTypedValue(connectionProvider, value, valueType, target);

                        // Casting double check
                        oracle.sql.Datum datum = (oracle.sql.Datum)superValue;
                        return datum;
                    }
                case JDBC:
                    {
                        Object superValue = super.customTypedValue(connectionProvider, value, valueType, target);

                        if (superValue instanceof oracle.sql.Datum) {
                            oracle.sql.Datum datum = (oracle.sql.Datum)superValue;
                            return datum.toJdbc();
                        } else {
                            return superValue;
                        }
                    }
                default:
                    return super.customTypedValue(connectionProvider, value, valueType, target);
            }
        } catch (Exception e) {
            throw new DataTypeIllegalArgumentException(this, internalValue, e);
        }
    }
    
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case JAVA:
                return customTypedClass(connectionProvider, ValueType.JDBC);
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }
    
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case JAVA:
                return customSqlDataType(ValueType.JDBC);
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected boolean customRequiresConnection() {
        return false;
    }
}
