/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.util.Arrays;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.objects.OraINTERVALDS;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.util.TemporalUtil;

import oracle.jdbc.OracleTypes;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=INTERVALDS.java">Barry McGillin</a>
 *
 */
public class INTERVALDS extends Datum {

    protected INTERVALDS(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        Object internalValue = value.getInternalValue();
        
        switch (stringType) {
            case REST:
                if (internalValue != null) {
                    return new StringValue(TemporalUtil.ISO8601format(OraINTERVALDS.getInstance((oracle.sql.INTERVALDS)internalValue)));
                }
                // fall-thru if null
            default:
                return super.customStringValue(connectionProvider, value, stringType, maxLen);
        }
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        switch (valueType) {
            case JDBC:
                return customTypedValue(connectionProvider, value, ValueType.DEFAULT, target);
            default:
                return super.customTypedValue(connectionProvider, value, valueType, target);
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case DEFAULT:
                return oracle.sql.INTERVALDS.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.INTERVALDS;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        if (value instanceof oracle.sql.INTERVALDS) {
            // already correctly typed
            return value;
        } else if (value instanceof byte[]) {
            return new oracle.sql.INTERVALDS(Arrays.copyOf((byte[])value, ((byte[])value).length));
        } else if (value instanceof OraINTERVALDS) {
            return ((OraINTERVALDS)value).getDatum();
        } else if (value instanceof CharSequence) {
            CharSequence text = (CharSequence)value;
            
            if ((text.length() > 0 && text.charAt(0) == 'P') ||
                (text.length() > 1 && (text.charAt(0) == '+' || text.charAt(0) == '-') && text.charAt(1) == 'P')) {
                OraINTERVALDS oraInterval = TemporalUtil.DS_ISO8601parse(text);
                
                if (oraInterval != null) {
                    return oraInterval.getDatum();
        }
    }

        }
        return new oracle.sql.INTERVALDS(value.toString());
    }

    @Override
    protected String customUnconstrainedDataTypeString() {
        return "DSINTERVAL_UNCONSTRAINED"; //$NON-NLS-1$
    }

    @Override
    protected String customDataTypeString() {
        Integer precision = typeMetadata.get_data_precision();
        Integer scale = typeMetadata.get_data_scale();

        if (precision != null && scale != null) {
            return "INTERVAL DAY (" + precision + ") TO SECOND (" + scale +
                ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        } else if (precision != null) {
            return "INTERVAL DAY (" + precision + ") TO SECOND"; //$NON-NLS-1$ //$NON-NLS-2$
        } else {
            return "INTERVAL DAY TO SECOND"; //$NON-NLS-1$
        }
    }
        }
