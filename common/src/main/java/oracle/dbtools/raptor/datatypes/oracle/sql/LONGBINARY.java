/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.io.IOException;
import java.io.InputStream;

import java.sql.CallableStatement;
import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingStrategy;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeEncodingException;
import oracle.dbtools.raptor.datatypes.DataTypeIOException;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;
import oracle.dbtools.raptor.datatypes.objects.LongBinary;
import oracle.dbtools.raptor.datatypes.strategies.callablestatement.CallableBindingLONGBINARY;
import oracle.dbtools.raptor.datatypes.strategies.preparedstatement.PrepareBindingLONGBINARY;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.util.encoding.EncodingException;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleTypes;

import org.xml.sax.SAXException;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=LONGBINARY.java">Barry McGillin</a> 
 *
 */
public class LONGBINARY extends Datum {
    protected LONGBINARY(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        LongBinary longBinary = (LongBinary)value.getInternalValue();
        return longBinary.getStringValue(stringType, maxLen);
    }
    
    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        Object internalValue = value.getInternalValue();
        
        LongBinary longBinary = (LongBinary)internalValue;
        try {
            switch (valueType) {
                case JDBC:
                case DATUM:
                case TARGET:
                    return longBinary.getTypedValue(connectionProvider, valueType, target);
                default:
                    return super.customTypedValue(connectionProvider, value, valueType, target);
            }
        } catch (IllegalArgumentException e) {
            throw new DataTypeIllegalArgumentException(this, internalValue, e);
        } catch (SQLException e) {
            throw new DataTypeIllegalArgumentException(this, internalValue, e);
        } catch (IOException e) {
            throw new DataTypeIllegalArgumentException(this, internalValue, e);
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case JDBC:
                return InputStream.class;
            case DEFAULT:
                return LongBinary.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.LONGVARBINARY;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        try {
            if (value == null) {
                return new LongBinary();
            } else if (value instanceof LongBinary) {
                return value;
            } else {
                return LongBinary.constructFrom(value);
            }
        } catch (IllegalArgumentException e) {
            throw new DataTypeIllegalArgumentException(this, value, e);
        } catch (EncodingException e) {
            throw new DataTypeIllegalArgumentException(this, value, e);
        } catch (SQLException e) {
            throw new DataTypeIllegalArgumentException(this, value, e);
        } catch (IOException e) {
            throw new DataTypeIllegalArgumentException(this, value, e);
        }
    }

    public <P extends DataBinding> BindingStrategy getBind(BindContext context, P param) {
        if (CallableStatement.class.isAssignableFrom(context.getEffectiveStatementClass())) {
            return new CallableBindingLONGBINARY<OracleCallableStatement,P>(context, param);
        } else {
            return new PrepareBindingLONGBINARY<OraclePreparedStatement,P>(context, param);
        }
    }

    @Override
    protected void customMarshal(DataTypeConnectionProvider connectionProvider, DataValueInternal value, DataValueMarshalHandler hd, String name) throws SAXException {
        LongBinary longBinary = (LongBinary)value.getInternalValue();
        longBinary.marshal(this, hd, name);
    }

    @Override
    public Object startDataValue(String name, boolean isNull) {
        if (isNull) {
            return null;
        } else {
            return LongBinary.getBuilder();
        }
    }

    @Override
    public void bodyDataValue(NamedValue value, char[] ch, int start, int length) {
        LongBinary.LongBinaryBuilder builder = (LongBinary.LongBinaryBuilder)value.getValue();
        try {
            builder.write(ch, start, length);
        } catch (IOException e) {
            throw new DataTypeIOException(e);
        } catch (EncodingException e) {
            throw new DataTypeEncodingException(e);
        }
    }

    @Override
    public DataValue endDataValue(NamedValue value) {
        LongBinary.LongBinaryBuilder builder = (LongBinary.LongBinaryBuilder)value.getValue();
        try {
            return getDataValue((builder != null) ? builder.build() : null);
        } catch (IOException e) {
            throw new DataTypeIOException(e);
        }
    }
}
