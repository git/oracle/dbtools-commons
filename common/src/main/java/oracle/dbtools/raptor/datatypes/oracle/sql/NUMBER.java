/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.sql.SQLException;

import java.util.Arrays;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.dbtools.raptor.nls.OracleNLSProvider;

import oracle.jdbc.OracleTypes;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=NUMBER.java">Barry McGillin</a> 
 *
 */
public class NUMBER extends NumericDatum {
    protected NUMBER(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }
    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        Object internalValue = value.getInternalValue();
        switch (stringType) {
            case REST:
            case GENERIC:
                return new StringValue(((oracle.sql.NUMBER)internalValue).stringValue());
            default:
                return super.customStringValue(connectionProvider, value, stringType, maxLen);
        }
    }

    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case JDBC:
                return BigDecimal.class;
            case DEFAULT:
                return oracle.sql.NUMBER.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.NUMBER;
            default:
                return super.customSqlDataType(valueType);
        }
    }
    
    @Override
    protected Object customUnscaledInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        try {
            if (value instanceof oracle.sql.NUMBER) {
                // already correctly typed
                return value;
            } else if (value instanceof BigDecimal) {
                return new oracle.sql.NUMBER((BigDecimal)value);
            } else if (value instanceof BigInteger) {
                return new oracle.sql.NUMBER(asBigDecimal((BigInteger)value));
            } else if (value instanceof byte[]) {
                return new oracle.sql.NUMBER(Arrays.copyOf((byte[])value, ((byte[])value).length));
            } else if (value instanceof String) {
                String stringValue = value.toString();
                // Need to accept both NLS and GENERIC formats
                
                if (isPosInf(stringValue)) {
                        return oracle.sql.NUMBER.posInf();
                } else if (isNegInf(stringValue)) {
                        return oracle.sql.NUMBER.negInf();
                } else {
                    try {
                        return new oracle.sql.NUMBER(new BigDecimal(stringValue));
                    } catch (Exception e1) {
                        return typedValueFromNLSString(connectionProvider, stringValue);
                    }
                }
            } else {
                return new oracle.sql.NUMBER(value);
            }
        } catch (SQLException e) {
            // SQLException signals Illegal Argument - not connection related	
            throw new DataTypeIllegalArgumentException(this, value);
        }
    }

    protected oracle.sql.NUMBER typedValueFromNLSString(DataTypeConnectionProvider connectionProvider, String value) throws SQLException {
        return new oracle.sql.NUMBER(asBigDecimal(((OracleNLSProvider)NLSProvider.getProvider(connectionProvider.getNLSConnection())).parseNumber(value)));
    }
    
    protected BigDecimal asBigDecimal(Number number) {
        if (number instanceof BigDecimal) {
            return (BigDecimal)number;
        } else if (number instanceof BigInteger) {
            return new BigDecimal((BigInteger)number);
        } else {
            return null;
        }
    }
    
    @Override
    protected Object customScaleInternalValue(DataTypeConnectionProvider connectionProvider, Object value) throws SQLException {
        oracle.sql.NUMBER unscaledValue = (oracle.sql.NUMBER)value;
        
        oracle.sql.NUMBER scaledValue = scaleNUMBER(unscaledValue, typeMetadata.get_data_precision(), typeMetadata.get_data_scale());
        
        return constrainNUMBER(scaledValue, unscaledValue, typeMetadata.get_data_precision(), typeMetadata.get_data_scale());
    }
    
    protected final static int MAX_FP_PRECISION = 40; 
    protected final static int MAX_PRECISION = 38; 
    
    protected oracle.sql.NUMBER scaleNUMBER(oracle.sql.NUMBER value, Integer precision, Integer scale) throws SQLException {
        oracle.sql.NUMBER newValue = value;
        
        // Scale Value
        if (value != null && !value.isZero()) {
            if (scale != null && scale.intValue() == -127) {
                newValue = scaleNUMBER(value, precision);
            } else if (precision != null || scale != null) {
                // Constrain Precision
                final int ip = getPrecisionDigits(precision, scale);
                
                if (ip > 0) {
                    // Treat as Fixed Point
                    boolean[] exceeded = new boolean[1];
                    
                    if (!value.isInf()) {
                        // Constrain Scale
                        int s = getScaleDigits(precision, scale);
                        
                        int shiftby = 0;
                        int p = ip - s;
                        
                        if (s < 0 || p < 0) {
                            shiftby = s;
                            p = ip;
                            s = 0;
                        }
                        
                        // Shift Value
                        if (shiftby != 0) {
                            newValue = newValue.shift(shiftby);
                        }
                        
                        // Scale value
                        newValue = newValue.scale(p, s, exceeded);
                
                        // Shift Value back
                        if (shiftby != 0) {
                            newValue = newValue.shift(shiftby*-1);
                        }
                        
                        if (exceeded[0] == true) {
                            newValue = scaleNUMBER(value, ip);
                        }
                    }
                } else {
                    newValue = oracle.sql.NUMBER.zero();
                }
            } else {
                // NUMBER Floating Point
                newValue = scaleNUMBER(value);
            }
        }
        
        // Only return a changed value
        if (newValue != value && !newValue.equals(value)) {
            return newValue;
        } else {
            return value;
        }
    }
    
    protected oracle.sql.NUMBER scaleNUMBER(oracle.sql.NUMBER value, Integer precision) throws SQLException {
        oracle.sql.NUMBER newValue = value;
        
        // Scale Value
        if (value != null && !value.isZero()) {
            // Constrain Precision
            final int p = getPrecisionDigits(precision);
            
            if (p > 0) {
                if (!value.isInf()) {
                    newValue = value.floatingPointRound(p);
                }
            } else {
                newValue = oracle.sql.NUMBER.zero();
            }
        }
        
        // Only return a changed value
        if (newValue != value && !newValue.equals(value)) {
            return newValue;
        } else {
            return value;
        }
    }
    
    protected oracle.sql.NUMBER scaleNUMBER(oracle.sql.NUMBER value) throws SQLException {
        return scaleNUMBER(value, null);
    }
    
    protected oracle.sql.NUMBER posNUMBERMax(Integer precision, Integer scale) throws SQLException {
        oracle.sql.NUMBER newValue = oracle.sql.NUMBER.zero();
        
        if (scale != null && scale.intValue() == -127) {
            newValue = posNUMBERMax(precision);
        } else if (precision != null || scale != null) {
            // Constrain Precision
            final int p = getPrecisionDigits(precision, scale);
            
            if (p > 0) {
                // Constrain Scale
                final int s = getScaleDigits(precision, scale);

                int shiftby = s * -1;

                return posFixedNUMBERMax(p, shiftby);
            }
        } else {
            // NUMBER Floating Point
            newValue = posNUMBERMax();
        }
            
        return newValue;
    }
    
    protected oracle.sql.NUMBER posNUMBERMax(Integer precision) throws SQLException {
        final int p = getPrecisionDigits(precision);
        
        if (p > 0) {
            return oracle.sql.NUMBER.posInf();
        } else {
            return oracle.sql.NUMBER.zero();
        }
    }
    
    protected oracle.sql.NUMBER posNUMBERMax() throws SQLException {
        return posNUMBERMax(null);
    }
    
    protected oracle.sql.NUMBER negNUMBERMax(Integer precision, Integer scale) throws SQLException {
        return posNUMBERMax(precision, scale).negate();
    }
    
    protected oracle.sql.NUMBER negNUMBERMax(Integer precision) throws SQLException {
        return posNUMBERMax(precision).negate();
    }
    
    protected oracle.sql.NUMBER negNUMBERMax() throws SQLException {
        return posNUMBERMax().negate();
    }
        
    protected oracle.sql.NUMBER posNUMBERMin(Integer precision, Integer scale) throws SQLException {
        oracle.sql.NUMBER newValue = oracle.sql.NUMBER.zero();
        
        if (scale != null && scale.intValue() == -127) {
            newValue = posNUMBERMin(precision);
        } else if (precision != null || scale != null) {
            // Constrain Precision
            final int p = getPrecisionDigits(precision, scale);
            
            if (p > 0) {
                // Constrain Scale
                final int s = getScaleDigits(precision, scale);

                int shiftby = s * -1;
                
                newValue = posFixedNUMBERMin(p, shiftby);
            }
        } else {
            return posNUMBERMin();
        }
        
        return newValue;
    }
    
    protected oracle.sql.NUMBER posNUMBERMin(Integer precision) throws SQLException {
        // Constrain Precision
        final int p = getPrecisionDigits(precision);
        
        if (p > 0) {
            return oracle.sql.NUMBER.negInf();
        } else {
            return oracle.sql.NUMBER.zero();
        }
    }
    
    protected oracle.sql.NUMBER posNUMBERMin() throws SQLException {
        return posNUMBERMin(null);
    }
    
    protected oracle.sql.NUMBER negNUMBERMin(Integer precision, Integer scale) throws SQLException {
        return posNUMBERMin(precision, scale).negate();
    }
    
    protected oracle.sql.NUMBER negNUMBERMin(Integer precision) throws SQLException {
        return posNUMBERMin(precision).negate();
    }
    
    protected oracle.sql.NUMBER negNUMBERMin() throws SQLException {
        return posNUMBERMin().negate();
    }
    
    protected oracle.sql.NUMBER posFixedNUMBERMax(int precision, int scale) throws SQLException {
        if (precision > 0) {
            char[] buf = new char[precision];
            
            // Fill Arrays
            Arrays.fill(buf, '9');
            
            oracle.sql.NUMBER newValue = new oracle.sql.NUMBER(String.valueOf(buf));
            
            if (scale != 0) {
                newValue = newValue.shift(scale);
            }
            
            return newValue;
        } else {
            return oracle.sql.NUMBER.zero();
        }
    }
    
    protected oracle.sql.NUMBER posFixedNUMBERMin(int precision, int scale) throws SQLException {
        if (precision > 0) {
            return new oracle.sql.NUMBER(1).shift(scale);
        } else {
            return oracle.sql.NUMBER.zero();
        }
    }
    
    protected oracle.sql.NUMBER constrainNUMBER(final oracle.sql.NUMBER newValue, 
                                                final oracle.sql.NUMBER oldValue,
                                                Integer precision,
                                                Integer scale) throws SQLException {
        if (scale == null || scale.intValue() != -127) {
            final oracle.sql.NUMBER maxPosValue = posNUMBERMax(precision, scale);
            final oracle.sql.NUMBER minPosValue = posNUMBERMin(precision, scale);
            final oracle.sql.NUMBER maxNegValue = negNUMBERMax(precision, scale);
            final oracle.sql.NUMBER minNegValue = negNUMBERMin(precision, scale);
            
            return constrainNUMBER(newValue, oldValue, maxPosValue, minPosValue, maxNegValue, minNegValue);
        } else {
            return constrainNUMBER(newValue, oldValue, precision);
        }
    }
    
    protected oracle.sql.NUMBER constrainNUMBER(final oracle.sql.NUMBER newValue, 
                                                final oracle.sql.NUMBER oldValue,
                                                Integer precision) throws SQLException {
                                  
        final oracle.sql.NUMBER maxPosValue = posNUMBERMax(precision);
        final oracle.sql.NUMBER minPosValue = posNUMBERMin(precision);
        final oracle.sql.NUMBER maxNegValue = negNUMBERMax(precision);
        final oracle.sql.NUMBER minNegValue = negNUMBERMin(precision);
        
        return constrainNUMBER(newValue, oldValue, maxPosValue, minPosValue, maxNegValue, minNegValue);
    }
    
    protected oracle.sql.NUMBER constrainNUMBER(final oracle.sql.NUMBER newValue, 
                                                final oracle.sql.NUMBER oldValue,
                                                final oracle.sql.NUMBER maxPosValue,
                                                final oracle.sql.NUMBER minPosValue,
                                                final oracle.sql.NUMBER maxNegValue,
                                                final oracle.sql.NUMBER minNegValue) {
                                  
        if (newValue != null) {
            final int newSign = newValue.sign();
            
            if (oldValue != null) {
                switch (oldValue.sign()) {
                    case 1:
                        if (newSign < 0) {
                            return maxPosValue;
                        }
                        break;
                    case -1:
                        if (newSign > 0) {
                            return maxNegValue;
                        }
                        break;
                }
            }
            
            if (newValue.compareTo(maxPosValue) > 0) {
                return maxPosValue;
            } else if (newValue.compareTo(maxNegValue) < 0) {
                return maxNegValue;
            } else if (newSign > 0 && newValue.compareTo(minPosValue) < 0) {
                return minPosValue;
            } else if (newSign < 0 && newValue.compareTo(minNegValue) > 0) {
                return minNegValue;
            }
        }
        
        return newValue;
    }
    
    protected int getPrecisionDigits(Integer precision, Integer scale) throws SQLException {
        if (scale == null || scale.intValue() != -127) {
            final int maxValue = MAX_PRECISION;
            
            int p = (precision == null) ? maxValue : precision.intValue();
            
            if (p < 0) {
                return 0;
            } else if (p > maxValue) {
                return maxValue;
            } else {
                return p;
            }
        } else {
            return getPrecisionDigits(precision);
        }
    }
    
    protected int getPrecisionDigits(Integer precision) throws SQLException {
        final int maxValue = MAX_FP_PRECISION;
        
        int p = (precision == null) ? maxValue : precision.intValue();
        
        if (p < 0) {
            return 0;
        } else if (p > maxValue) {
            return maxValue;
        } else {
            return p;
        }
    }
    
    protected int getPrecisionDigits() throws SQLException {
        return getPrecisionDigits(null);
    }
    
    protected int getScaleDigits(Integer precision, Integer scale) throws SQLException {
        // Validate Precision only
        getPrecisionDigits(precision, scale);
        
        int s = (scale == null) ? 0 : scale.intValue();
        
        if (s < -84) {
            return -84;
        } else if (s > 127) {
            return 127;
        } else {
            return s;
        }
    }
}
