/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.utils.NLSUtils;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=NumericDatum.java">Barry McGillin</a> 
 *
 */
public abstract class NumericDatum extends Datum {
    protected NumericDatum(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
       switch (stringType) {
            case NLS:
                try {
                    return new StringValue((CharSequence)NLSUtils.getValue(connectionProvider.getNLSConnection(), value.getInternalValue()));
                } catch (NullPointerException e) {
                    // Please don't remove this exception handler
                    // until you make NLSUtils.getValue super duper robust
                    return customStringValue(connectionProvider, value, StringType.GENERIC, maxLen);
                }
            default:
                return super.customStringValue(connectionProvider, value, stringType, maxLen);
        }
    }
    
    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        return getScaledInternalValue(connectionProvider, customUnscaledInternalValue(connectionProvider, value));
    }
    
    protected final Object getScaledInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        try {
            return (value == null) ? value : customScaleInternalValue(connectionProvider, value);
        } catch (SQLException e) {
            // SQLException signals Illegal Argument - not connection related   
            throw new DataTypeIllegalArgumentException(this, value);
        }
    }
    
    protected Object customUnscaledInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        return super.customInternalValue(connectionProvider, value);
    }
    
    protected Object customScaleInternalValue(DataTypeConnectionProvider connectionProvider, Object value) throws SQLException {
        return value;
    }
    
    protected boolean isPosInf(String str) {
        return (str != null && (str.equalsIgnoreCase("Infinity") || str.equalsIgnoreCase("+Infinity") || str.equalsIgnoreCase("INF") || str.equalsIgnoreCase("+INF") || str.equalsIgnoreCase("~") || str.equalsIgnoreCase("+~"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
    }
    
    protected boolean isNegInf(String str) {
        return (str != null && (str.equalsIgnoreCase("-Infinity") || str.equalsIgnoreCase("-INF") || str.equalsIgnoreCase("-~"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }
    
    protected boolean isNaN(String str) {
        return (str != null && str.equalsIgnoreCase("NaN")); //$NON-NLS-1$
    }
}
