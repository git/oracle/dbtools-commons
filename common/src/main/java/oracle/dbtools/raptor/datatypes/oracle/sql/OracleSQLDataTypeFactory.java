/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.io.IOException;

import java.net.URI;

import java.net.URISyntaxException;

import java.sql.Connection;
import java.sql.Types;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeFactoryExtension.ExtensionAccess;
import oracle.dbtools.raptor.datatypes.DataTypeProvider;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.TypeMetadata.Attribute;
import oracle.dbtools.raptor.datatypes.impl.AbstractDataTypeProviderImpl;

import oracle.dbtools.raptor.datatypes.metadata.ArgMetadata;
import oracle.dbtools.raptor.datatypes.metadata.MetadataLoader;

import oracle.jdbc.OracleTypes;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=OracleSQLDataTypeFactory.java">Barry McGillin</a> 
 *
 */
public final class OracleSQLDataTypeFactory extends AbstractDataTypeProviderImpl {
    private final SQLDataTypeProviderAccess providerAccess;

    public class SQLDataTypeProviderAccess implements DataTypeProviderAccess {
        protected SQLDataTypeProviderAccess() {};
        
        public DataTypeProvider getDataTypeProvider() {
            return OracleSQLDataTypeFactory.this;
        }
        
        public ExtensionAccess getExtensionAccess() {
            return OracleSQLDataTypeFactory.this.getExtensionAccess();
        }
        
        public DataTypeFactory getDataTypeFactory() {
            return getExtensionAccess().getDataTypeFactory();
        }
    }

    @Override
    protected SQLDataTypeProviderAccess getDataTypeProviderAccess() {
        return providerAccess;
    }

    public static Builder builder(URI dataTypeProviderURI) {
        return new Builder(dataTypeProviderURI);
    }
    
    public final static class Builder extends AbstractDataTypeProviderImpl.Builder {
        public static final URI ID;

        static {
            URI tmpURI = null;
            
            // Initialize SQL DataTypeProvider URI
            try {
                tmpURI = new URI("http://www.oracle.com/raptor/datatypes/providers/sql/");
            } catch(URISyntaxException e) {
                ;
            }
            
            ID = tmpURI;
        }

        private Builder(URI dataTypeProviderURI) {
            super(dataTypeProviderURI);
        }

        public SQLDataTypeProviderAccess build() {
            OracleSQLDataTypeFactory datatypeFactory = new OracleSQLDataTypeFactory(getExtensionAccess(),
                                                getDataTypeProviderURI(),
                                                getExtensionAccess().resolveDataTypeProvider(getSuperDataTypeProviderURI()), 
                                                typeMetadataProps);
            
            return datatypeFactory.getDataTypeProviderAccess();
        }

        @Override
        public Builder setExtensionAccess(ExtensionAccess extensionAccess) {
            super.setExtensionAccess(extensionAccess);
            return this;
        }

        @Override
        public Builder setSuperDataTypeProviderURI(URI superDataTypeProvider) {
            super.setSuperDataTypeProviderURI(superDataTypeProvider);
            return this;
        }
    }
    
    private final static String TABLE = "TABLE"; //$NON-NLS-1$
    private final static String ARRAY = "ARRAY"; //$NON-NLS-1$
    private final static String VARRAY = "VARRAY"; //$NON-NLS-1$
    private final static String VARYING_ARRAY = "VARYING ARRAY"; //$NON-NLS-1$

    private static final Map<String, NamedValue<Map<TypeMetadata.Attribute, Object>>> typeMetadataProps =
        new HashMap<String, NamedValue<Map<TypeMetadata.Attribute, Object>>>() {
        {
            // Numerics
            put("NUMBER", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.NUMBER);
                            put(TypeMetadata.Attribute.IMPL_CLASS, NUMBER.class);
                            put(TypeMetadata.Attribute.DATA_LENGTH, 22);
                            put(TypeMetadata.Attribute.DATA_RADIX, 10);
                        }
                    })
               );
            put("NUMERIC", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NUMBER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "NUMERIC"); //$NON-NLS-1$
                        }
                    })
               );
            put("DECIMAL", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NUMERIC",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.DECIMAL);
                            put(TypeMetadata.Attribute.BASE_TYPE, "DECIMAL"); //$NON-NLS-1$
                        }
                    })
                       );
            put("DEC",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "DECIMAL", null)
               );
            
            // Integers
            put("NUMBER(38)", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NUMBER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.DATA_PRECISION, 38);
                            put(TypeMetadata.Attribute.DATA_SCALE, 0);
                            put(TypeMetadata.Attribute.IMPL_CLASS, INTEGER.class);
                        }
                    })
               );
            put("INTEGER", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NUMBER(38)",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.INTEGER);
                            put(TypeMetadata.Attribute.BASE_TYPE, "INTEGER"); //$NON-NLS-1$
                            put(TypeMetadata.Attribute.IMPL_CLASS, INTEGER.class);
                        }
                    })
               );
            put("INT",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTEGER", null) //$NON-NLS-1$
               );
            put("SMALLINT", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTEGER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.SMALLINT);
                            put(TypeMetadata.Attribute.BASE_TYPE, "SMALLINT"); //$NON-NLS-1$
                        }
                    })
               );
            
            // Float Points
            put("FLOAT", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NUMBER",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.FLOAT);
                            put(TypeMetadata.Attribute.BASE_TYPE, "FLOAT"); //$NON-NLS-1$
                            put(TypeMetadata.Attribute.IMPL_CLASS, FLOAT.class);
                        }
                    })
               );
            put("FLOAT(63)", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "FLOAT",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.DATA_PRECISION, 63);
                        }
                    })
               );
            put("FLOAT(126)", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "FLOAT",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.DATA_PRECISION, 126);
                        }
                    })
               );
            put("DOUBLE PRECISION", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "FLOAT(126)",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.DOUBLE);
                            put(TypeMetadata.Attribute.BASE_TYPE, "DOUBLE PRECISION"); //$NON-NLS-1$
                        }
                    })
               );
            put("REAL", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "FLOAT(63)",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.REAL);
                            put(TypeMetadata.Attribute.BASE_TYPE, "REAL"); //$NON-NLS-1$
                        }
                    })
               );
           
            // Binary Floating Points 
            put("BINARY_FLOAT", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.BINARY_FLOAT);
                            put(TypeMetadata.Attribute.IMPL_CLASS, BINARY_FLOAT.class);
                            put(TypeMetadata.Attribute.DATA_LENGTH, 4);
                            put(TypeMetadata.Attribute.DATA_RADIX, 10);
                        }
                    })
               );
            put("BINARY_DOUBLE", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.BINARY_DOUBLE);
                            put(TypeMetadata.Attribute.IMPL_CLASS, BINARY_DOUBLE.class);
                            put(TypeMetadata.Attribute.DATA_LENGTH, 8);
                            put(TypeMetadata.Attribute.DATA_RADIX, 10);
                        }
                    })
               );
            
            // Characters
            put("CHAR",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.CHAR);
                            put(TypeMetadata.Attribute.IMPL_CLASS, CHAR.class);
                        }
                    })
               );
            // Characters
            put("CHARACTER", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "CHAR", null) //$NON-NLS-1$
               );
            put("VARCHAR", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.VARCHAR);
                            put(TypeMetadata.Attribute.IMPL_CLASS, VARCHAR.class);
                        }
                    })
               );
            put("VARCHAR2", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "VARCHAR",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "VARCHAR2"); //$NON-NLS-1$
                        }
                    })
               );
            put("CHAR VARYING", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "VARCHAR",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "CHAR VARYING"); //$NON-NLS-1$
                        }
                    })
               );
            put("CHARACTER VARYING", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "CHAR VARYING", null) //$NON-NLS-1$
               );
            put("LONG", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.LONGVARCHAR);
                            put(TypeMetadata.Attribute.IMPL_CLASS, LONGVARCHAR.class);
                        }
                    })
               );
            
            // National Characters
            put("NCHAR", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, Types.NCHAR);
                            put(TypeMetadata.Attribute.IMPL_CLASS, CHAR.class);
                        }
                    })
               );
            put("NATIONAL CHAR", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NCHAR", null) //$NON-NLS-1$
               );
            put("NATIONAL CHARACTER",
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NCHAR", null) //$NON-NLS-1$
               );
            put("NVARCHAR", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, Types.NVARCHAR);
                            put(TypeMetadata.Attribute.IMPL_CLASS, VARCHAR.class);
                        }
                    })
               );
            put("NVARCHAR2", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NVARCHAR",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "NVARCHAR2"); //$NON-NLS-1$
                        }
                    })
               );
            put("NCHAR VARYING", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NVARCHAR",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, "NCHAR VARYING"); //$NON-NLS-1$
                        }
                    })
               );
            put("NATIONAL CHAR VARYING", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NCHAR VARYING", null) //$NON-NLS-1$
               );
            put("NATIONAL CHARACTER VARYING", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "NCHAR VARYING", null) //$NON-NLS-1$
               );

            // Date and Time
            put("DATE", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.DATE);
                            put(TypeMetadata.Attribute.IMPL_CLASS, DATE.class);
                            put(TypeMetadata.Attribute.DATA_LENGTH, 7);
                        }
                    })
               );
            put("TIMESTAMP", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.TIMESTAMP);
                            put(TypeMetadata.Attribute.IMPL_CLASS, TIMESTAMP.class);
                            put(TypeMetadata.Attribute.DATA_LENGTH, 11);
                            put(TypeMetadata.Attribute.DATA_SCALE, 6);
                        }
                    })
               );
            put("TIMESTAMP(6)", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP", null) //$NON-NLS-1$
               );
            put("TIMESTAMP WITH TIME ZONE", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.TIMESTAMPTZ);
                            put(TypeMetadata.Attribute.IMPL_CLASS, TIMESTAMPTZ.class);
                            put(TypeMetadata.Attribute.DATA_LENGTH, 13);
                            put(TypeMetadata.Attribute.DATA_SCALE, 6);
                            put(TypeMetadata.Attribute.BASE_TYPE, "TIMESTAMP WITH TIME ZONE"); //$NON-NLS-1$
                        }
                    })
               );
            put("TIMESTAMP(6) WITH TIME ZONE", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP WITH TIME ZONE", null) //$NON-NLS-1$
               );
            put("TIMESTAMP WITH LOCAL TIME ZONE", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP",  //$NON-NLS-1$
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.TIMESTAMPLTZ);
                            put(TypeMetadata.Attribute.IMPL_CLASS, TIMESTAMPLTZ.class);
                            put(TypeMetadata.Attribute.DATA_LENGTH, 11);
                            put(TypeMetadata.Attribute.DATA_SCALE, 6);
                            put(TypeMetadata.Attribute.BASE_TYPE, "TIMESTAMP WITH LOCAL TIME ZONE"); //$NON-NLS-1$
                        }
                    })
               );
            put("TIMESTAMP(6) WITH LOCAL TIME ZONE", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "TIMESTAMP WITH LOCAL TIME ZONE", null) //$NON-NLS-1$
               );
            put("INTERVAL YEAR TO MONTH", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.INTERVALYM);
                            put(TypeMetadata.Attribute.IMPL_CLASS, INTERVALYM.class);
                            put(TypeMetadata.Attribute.DATA_PRECISION, 2);
                            put(TypeMetadata.Attribute.DATA_SCALE, 0);
                        }
                    })
               );
            put("INTERVAL YEAR(2) TO MONTH", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTERVAL YEAR TO MONTH", null) //$NON-NLS-1$
               );
            put("INTERVALYM", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTERVAL YEAR TO MONTH", null) //$NON-NLS-1$
               );
            put("INTERVAL DAY TO SECOND", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.INTERVALDS);
                            put(TypeMetadata.Attribute.IMPL_CLASS, INTERVALDS.class);
                            put(TypeMetadata.Attribute.DATA_LENGTH, 11);
                            put(TypeMetadata.Attribute.DATA_PRECISION, 2);
                            put(TypeMetadata.Attribute.DATA_SCALE, 6);
                        }
                    })
               );
            put("INTERVAL DAY(2) TO SECOND(6)", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTERVAL DAY TO SECOND", null) //$NON-NLS-1$
               );
            put("INTERVALDS", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "INTERVAL DAY TO SECOND", null) //$NON-NLS-1$
               );

            // Row ID
            put("ROWID", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.ROWID);
                            put(TypeMetadata.Attribute.IMPL_CLASS, ROWID.class);
                            put(TypeMetadata.Attribute.DATA_LENGTH, 10);
                        }
                    })
               );

            // Raw
            put("RAW", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.RAW);
                            put(TypeMetadata.Attribute.IMPL_CLASS, RAW.class);
                        }
                    })
               );
            put("LONG RAW", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.LONGVARBINARY);
                            put(TypeMetadata.Attribute.IMPL_CLASS, LONGBINARY.class);
                        }
                    })
               );

            // Extended Types
            put(ARRAY,
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, VARRAY);
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.ARRAY);
                            put(TypeMetadata.Attribute.IMPL_CLASS, ARRAY.class);
                            put(TypeMetadata.Attribute.TYPE_COMPONENTS, null);
                        }
                    })
               );
            put(VARYING_ARRAY,
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    ARRAY, null)
               );
            put(VARRAY,
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    VARYING_ARRAY, null)
               );
            put(TABLE,
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    VARRAY,
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.BASE_TYPE, TABLE); //$NON-NLS-1$
                        }
                    })
               );

            // Large Objects
            put("CLOB", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.CLOB);
                            put(TypeMetadata.Attribute.IMPL_CLASS, CLOB.class);
                        }
                    })
               );
            // Large Objects
            put("NCLOB", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, Types.NCLOB);
                            put(TypeMetadata.Attribute.IMPL_CLASS, CLOB.class);
                        }
                    })
               );
            put("BLOB", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.BLOB);
                            put(TypeMetadata.Attribute.IMPL_CLASS, BLOB.class);
                        }
                    })
               );

            // User Defined Objects
            put("STRUCT", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.STRUCT);
                            put(TypeMetadata.Attribute.IMPL_CLASS, STRUCT.class);
                            put(TypeMetadata.Attribute.TYPE_COMPONENTS, null);
                        }
                    })
               );
            put("OBJECT", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "STRUCT", null) //$NON-NLS-1$
               );
            
            // Cursor/ResultSet
            put("CURSOR", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    null, 
                    new HashMap<TypeMetadata.Attribute, Object>() {
                        {
                            put(TypeMetadata.Attribute.TYPE_CODE, OracleTypes.CURSOR);
                            put(TypeMetadata.Attribute.IMPL_CLASS, CURSOR.class);
                        }
                    })
               );
            put("REF CURSOR", //$NON-NLS-1$
                new NamedValue<Map<TypeMetadata.Attribute, Object>>(
                    "CURSOR", null) //$NON-NLS-1$
               );
        }
    };

    private OracleSQLDataTypeFactory(ExtensionAccess extensionAccess,
                                     URI dataTypeProviderURI,
                                     DataTypeProvider next,
                                     Map<String, NamedValue<Map<TypeMetadata.Attribute, Object>>> typeMetadataProps) {
        super(extensionAccess, dataTypeProviderURI, next, typeMetadataProps);
        
        this.providerAccess = new SQLDataTypeProviderAccess();
    }

    protected boolean isStructuredType(String typeName) {
        return (TABLE.equals(typeName) ||
                ARRAY.equals(typeName) ||
                VARRAY.equals(typeName) ||
                VARYING_ARRAY.equals(typeName));
    }

    @Override
    protected boolean customSupportedType(TypeMetadata typeMetadata) {
        // PL/SQL Types have a sub-name
        return (super.customSupportedType(typeMetadata) && typeMetadata.get_type_subname() == null);
    }

    @Override
    protected TypeMetadata customRefineTypeMetadata(DataTypeContext context, final TypeMetadata typeMetadata) {
        TypeMetadata candidate = typeMetadata;
        
        boolean triedSynonym = false;
        do {
            String type_owner = candidate.get_type_owner();

            // Ensure qualify by owner
            if (type_owner == null) {
                return candidate;
            }
            
            String type_name = candidate.get_type_name();
            
            // Ensure has a type name which is not remote
            if (type_name == null || candidate.get_type_link() != null) {
                return candidate;
            }
            
            String data_type = candidate.get_data_type();
            
            // Query database catalog
            if (data_type == null) {
                Map<Attribute, Object> attributeMap = new HashMap<Attribute, Object>();

                // Try qualified known types
                if ("SYS".equals(type_owner)) {
                    if ("XMLTYPE".equals(type_name)) {
                        data_type = type_name;
                        attributeMap.put(Attribute.DATA_TYPE, data_type);
                    }
                } else if ("PUBLIC".equals(type_owner)) {
                    if ("CLOB".equals(type_name) || "BLOB".equals(type_name)) {
                        data_type = type_name;
                        attributeMap.put(Attribute.DATA_TYPE, data_type);
                    }
                }
                
                if (!attributeMap.isEmpty()) {
                    return DataTypeFactory.getTypeMetadata(candidate, attributeMap);
                }
                
                // Try Type from database catalog
                DataTypeConnectionProvider provider = context.getDataTypeConnectionProvider();
                
                Connection conn = provider.getDataTypeConnection();
                if (conn != null) {
                    provider.lockDataTypeConnection();
                    
                    try {
                        new MetadataLoader(conn).loadSQLType(attributeMap, type_owner, type_name);
                    } catch (Exception e) {
                        ;
                    } finally {
                        provider.unlockDataTypeConnection();
                    }
                }
                
                if (!attributeMap.isEmpty()) {
                    candidate = DataTypeFactory.getTypeMetadata(candidate, attributeMap);
                    data_type = candidate.get_data_type();
                }
            }
            
            // Refine Collection
            if ("COLLECTION".equals(data_type)) {
                DataTypeConnectionProvider provider = context.getDataTypeConnectionProvider();
                
                Map<Attribute, Object> attributeMap = new HashMap<Attribute, Object>();

                Connection conn = provider.getDataTypeConnection();
                if (conn != null) {
                    provider.lockDataTypeConnection();
                    
                    try {
                        Deque<ArgMetadata> loadStack = new LinkedList<ArgMetadata>();
                        
                        new MetadataLoader(conn).loadSQLCollection(loadStack, type_owner, type_name, 0);
                        
                        if (!loadStack.isEmpty()) {
                            ArgMetadata collArgMetadata = loadStack.pop();
                            
                            String collType = (String)collArgMetadata.getValue().get(Attribute.DATA_TYPE);
                            
                            attributeMap.put(Attribute.DATA_TYPE, collType);
                            
                            // Added missing components - performance improvement
                            List<NamedValue<TypeMetadata>> typeComponents = candidate.get_type_components();
                            if (typeComponents == null) {
                                List<NamedValue<TypeMetadata>> components = new LinkedList<NamedValue<TypeMetadata>>();

                                while (!loadStack.isEmpty() && loadStack.peek().getIdentifier().getLevel() == 1) {
                                    ArgMetadata componentArgMetadata = loadStack.pop();
                                    
                                    TypeMetadata componentTypeMetadata = context.getDataTypeFactory().getTypeMetadata(componentArgMetadata.getValue());
                                    
                                    components.add(new NamedValue<TypeMetadata>(componentArgMetadata.getName(), componentTypeMetadata));
                                }
                                
                                if (components.size() > 0) {
                                    attributeMap.put(Attribute.TYPE_COMPONENTS, components);
                                }
                            }
                        }
                    } catch (Exception e) {
                        ;
                    } finally {
                        provider.unlockDataTypeConnection();
                    }
                }
                
                if (!attributeMap.isEmpty()) {        
                    return DataTypeFactory.getTypeMetadata(candidate, attributeMap);
                }
            } else if (data_type != null) {
                break;
            }
            
            // Break if better candiidate found
            if (candidate != typeMetadata) {
                break;
            }
            
            // Infinite Loop safe-guard
            if (triedSynonym) {
                return candidate;
            } else {
                triedSynonym = true;
            }
            
            // Try Synonym
            DataTypeConnectionProvider provider = context.getDataTypeConnectionProvider();
            
            Map<Attribute, Object> attributeMap = new HashMap<Attribute, Object>();

            Connection conn = provider.getDataTypeConnection();
            if (conn != null) {
                provider.lockDataTypeConnection();
                
                try {
                    new MetadataLoader(conn).followSQLSynonym(attributeMap, type_owner, type_name);
                } catch (Exception e) {
                    ;
                } finally {
                    provider.unlockDataTypeConnection();
                }
            }
            
            if (!attributeMap.isEmpty()) {
                candidate = DataTypeFactory.getTypeMetadata(candidate, attributeMap);
                continue;
            } else {
                return candidate;
            }
        } while (true);

        return candidate;
    }

    @Override
    public void close() throws IOException {
        ; // Move along - nothing to see here
    }
}
