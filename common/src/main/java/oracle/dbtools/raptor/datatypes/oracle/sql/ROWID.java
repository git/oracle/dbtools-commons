/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.IOException;

import java.io.StringWriter;

import java.util.Arrays;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.util.encoding.BASE64Encoding;
import oracle.dbtools.util.encoding.EncodingException;
import oracle.dbtools.util.encoding.EncodingFactory;
import oracle.dbtools.util.encoding.EncodingType;
import oracle.dbtools.util.encoding.MimeType;

import oracle.jdbc.OracleTypes;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ROWID.java">Barry McGillin</a> 
 *
 */
public class ROWID extends Datum {
    protected ROWID(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        switch (valueType) {
            case JDBC:
                return customTypedValue(connectionProvider, value, ValueType.DEFAULT, target);
            default:
                return super.customTypedValue(connectionProvider, value, valueType, target);
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case DEFAULT:
                return oracle.sql.ROWID.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.ROWID;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        if (value instanceof oracle.sql.ROWID) {
            // already correctly typed
            return value;
        } else if (value instanceof byte[]) {
            return new oracle.sql.ROWID(Arrays.copyOf((byte[])value, ((byte[])value).length));
        } else {
            return new oracle.sql.ROWID(String.valueOf(value).getBytes());
        }
    }
    
    @Override
    protected boolean customRequiresConnection() {
        return true;
    }
    
    public static byte[] decodeRowId(Object value) throws IOException, EncodingException {
            return decodeRowId(String.valueOf(value));
    }
    
    public static byte[] decodeRowId(String value) throws IOException, EncodingException {
        if (value.contains(".")) {
            return decodeOracle7RowId(value.toCharArray());
        } else {
            return decodeOracle8RowId(value.toCharArray());
        }
    }
    
    public static String encodeRowId(byte[] b) throws IOException, EncodingException {
        if (b.length == 8) {
            return encodeOracle7RowId(b);
        } else {
            return encodeOracle8RowId(b);
        }
    }
    
    public static byte[] decodeOracle8RowId(char[] ch) throws IOException, EncodingException {
        BASE64Encoding b64Enc =
            (BASE64Encoding)EncodingFactory.getDecoder(EncodingType.ENCODING_BASE64,
                                                       MimeType.MIME_TEXT);
        // Decode Object ID
        char[] objectIdBlock = new char[8];
        
        int i = 0;
        int j = 0;
        objectIdBlock[i++] = 'A'; //$NON-NLS-1$
        objectIdBlock[i++] = 'A'; //$NON-NLS-1$
        for (; j < 6; j++) {
            objectIdBlock[i++] = ch[j];
        }
    
        byte[] objectId = b64Enc.decodeToByteArray(objectIdBlock);
        
        // Decode File Number
        char[] fileNumberBlock = new char[4];
        
        i = 0;
        j++; // skip padding character
        for (; j < 9; j++) {
            fileNumberBlock[i++] = ch[j];
        }
        fileNumberBlock[i++] = 'A'; //$NON-NLS-1$
        fileNumberBlock[i++] = '='; //$NON-NLS-1$
        
        byte[] fileNumber = b64Enc.decodeToByteArray(fileNumberBlock);
        
        // Decode Block Number
        char[] blockNumberBlock = new char[4];
        
        i = 0;
        j++; // skip padding character
        j++; // skip padding character
        for (; j < 15; j++) {
            blockNumberBlock[i++] = ch[j];
        }
        
        byte[] blockNumber = b64Enc.decodeToByteArray(blockNumberBlock);
        
        // Decode Row Number
        char[] rowNumberBlock = new char[4];
        
        i = 0;
        rowNumberBlock[i++] = 'A'; //$NON-NLS-1$
        for (; j < 18; j++) {
            rowNumberBlock[i++] = ch[j];
        }
        
        byte[] rowNumber = b64Enc.decodeToByteArray(rowNumberBlock);
        
        // Build Row Id
        byte[] rowid = new byte[10];
        
        rowid[0] = objectId[2];
        rowid[1] = objectId[3];
        rowid[2] = objectId[4];
        rowid[3] = objectId[5];
        rowid[4] = fileNumber[0];
        rowid[5] = (byte)(fileNumber[1] | blockNumber[0]);
        rowid[6] = blockNumber[1];
        rowid[7] = blockNumber[2];
        rowid[8] = rowNumber[1];
        rowid[9] = rowNumber[2];
        
        return rowid;
    }
    
    public static String encodeOracle8RowId(byte[] b) throws IOException, EncodingException {
        BASE64Encoding b64Enc =
            (BASE64Encoding)EncodingFactory.getDecoder(EncodingType.ENCODING_BASE64,
                                                       MimeType.MIME_TEXT);
        
        StringBuilder sb = new StringBuilder();
        
        // Encode Object Id
        byte[] objectId = new byte[6];
        objectId[2] = b[0];
        objectId[3] = b[1];
        objectId[4] = b[2];
        objectId[5] = b[3];
        char[] objectIdBlock = b64Enc.encodeToCharArray(objectId);
        sb.append(objectIdBlock, 2, 6);
        
        // Append separator
        sb.append("A"); //$NON-NLS-1$

        // Encode File Number
        byte[] fileNumber = new byte[2];
        fileNumber[0] = b[4];
        fileNumber[1] = (byte)(b[5] & 0xF0);
        char[] fileNumberBlock = b64Enc.encodeToCharArray(fileNumber);
        sb.append(fileNumberBlock, 0, 2);
        
        // Append separator
        sb.append("AA"); //$NON-NLS-1$

        // Encode Block Number
        byte[] blockNumber = new byte[3];
        blockNumber[0] = (byte)(b[5] & 0xF);
        blockNumber[1] = b[6];
        blockNumber[2] = b[7];
        char[] blockNumberBlock = b64Enc.encodeToCharArray(blockNumber);
        sb.append(blockNumberBlock);
        
        // Encode Row Number
        byte[] rowNumber = new byte[3];
        rowNumber[1] = b[8];
        rowNumber[2] = b[9];
        char[] rowNumberBlock = b64Enc.encodeToCharArray(rowNumber);
        sb.append(rowNumberBlock, 1, 3);
        
        return sb.toString();
    }
    
    public static byte[] decodeOracle7RowId(char[] ch) throws IOException, EncodingException {
        BASE64Encoding hexEnc =
            (BASE64Encoding)EncodingFactory.getDecoder(EncodingType.ENCODING_HEX,
                                                       MimeType.MIME_TEXT);
        
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        
        // Decode Block Number
        hexEnc.decode(new CharArrayReader(ch, 0, 8), ostream);
        
        // Decode Row Number
        hexEnc.decode(new CharArrayReader(ch, 9, 4), ostream);
        
        // Decode File Number
        hexEnc.decode(new CharArrayReader(ch, 14, 4), ostream);
        
        return ostream.toByteArray();
    }
    
    public static String encodeOracle7RowId(byte[] b) throws IOException, EncodingException {
        BASE64Encoding hexEnc =
            (BASE64Encoding)EncodingFactory.getDecoder(EncodingType.ENCODING_HEX,
                                                       MimeType.MIME_TEXT);
        
        StringWriter writer = new StringWriter();
        
        // Encode Block Number
        hexEnc.encode(new ByteArrayInputStream(b, 0, 4), writer);
        
        // Append separator
        writer.append('.');
        
        // Encode Row Number
        hexEnc.encode(new ByteArrayInputStream(b, 5, 2), writer);
        
        // Append separator
        writer.append('.');
        
        // Encode File Number
        hexEnc.encode(new ByteArrayInputStream(b, 5, 2), writer);
        
        return writer.toString();
    }
}
