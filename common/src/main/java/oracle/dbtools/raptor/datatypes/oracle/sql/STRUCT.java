/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.lang.reflect.Array;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingStrategy;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.DataTypeSQLException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.StructureType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.metadata.ArgMetadata;
import oracle.dbtools.raptor.datatypes.metadata.MetadataLoader;
import oracle.dbtools.raptor.datatypes.strategies.callablestatement.CallableBindingUDT;
import oracle.dbtools.raptor.datatypes.strategies.preparedstatement.PrepareBindingUDT;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.values.CompositeValue;
import oracle.dbtools.raptor.datatypes.values.NamedDataValue;
import oracle.dbtools.raptor.datatypes.xml.XMLUtil;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.dbtools.raptor.utils.JDBCProxyUtil;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleTypes;

import oracle.sql.StructDescriptor;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=STRUCT.java">Barry McGillin</a>
 *
 */
@SuppressWarnings("unchecked")
public class STRUCT extends DatumWithConnection {
    protected STRUCT(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, expandTypeMetadata(context, typeMetadata));
    }

    protected static TypeMetadata expandTypeMetadata(DataTypeContext context, TypeMetadata typeMetadata) {
        TypeMetadata newTypeMetadata = typeMetadata;
        
        try {
            // Load Attributes
            List<NamedValue<TypeMetadata>> attributes = loadAttributes(context, typeMetadata);
            
            Map<TypeMetadata.Attribute, Object> attributeMap = new HashMap<TypeMetadata.Attribute, Object>();
            attributeMap.put(TypeMetadata.Attribute.TYPE_COMPONENTS, attributes);
            
			newTypeMetadata = DataTypeFactory.getTypeMetadata(typeMetadata, attributeMap);
        } catch (SQLException e) {
            throw new DataTypeSQLException(e);
        }
        
        return newTypeMetadata;
    }

    private static List<NamedValue<TypeMetadata>> loadAttributes(DataTypeContext context, TypeMetadata typeMetadata) throws SQLException {
        DataTypeConnectionProvider provider = context.getDataTypeConnectionProvider();
        
        List<NamedValue<TypeMetadata>> typeComponents = new LinkedList<NamedValue<TypeMetadata>>();
        
        Connection conn = provider.lockDataTypeConnection();
        
        if (conn != null) {
            try {
                Deque<ArgMetadata> loadStack = new LinkedList<ArgMetadata>();
                
                final String typeOwner = typeMetadata.get_type_owner();
                final String typeName = typeMetadata.get_type_name();
                
                new MetadataLoader(conn).loadSQLAttributes(loadStack, typeOwner, typeName, 0);
                
                while (!loadStack.isEmpty()) {
                    ArgMetadata argMetadata = loadStack.pop();
                    
                    TypeMetadata componentTypeMetadata = context.getDataTypeFactory().getTypeMetadata(argMetadata.getValue());
                    
                    typeComponents.add(new NamedValue<TypeMetadata>(argMetadata.getName(), componentTypeMetadata));
                }
            } finally {
                provider.unlockDataTypeConnection();
            }
        }
        
        return typeComponents;
    }

    private static Integer getIntegerResult(ResultSet rset, String name) throws SQLException {
        return (rset.getObject(name) != null) ? rset.getInt(name) : null;
    }

    private static void addAttribute(HashMap<TypeMetadata.Attribute, Object> attributes, TypeMetadata.Attribute attribute, Object value) {
            if (value != null) {
                    attributes.put(attribute, value);
            }
    }
    
    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        StringBuilder sb = new StringBuilder();
        int len = 0;
        
        // Append Open Brace
        if (maxLen < 0 || sb.length() < maxLen) {
            sb.append("("); //$NON-NLS-1$
        }
        len++;

        // Ensure is record
        List<DataValue> components = getComponents(value);

        int i = 0;
        for (DataValue component : components) {
            // Append Separator
            if (i > 0) {
                if (maxLen < 0 || sb.length() < maxLen) {
                    sb.append(", "); //$NON-NLS-1$
                }
                len += 2;
            }
            
            String fieldTag = (component.getName() != null) ? component.getName() + "=" : "";
            String componentValue = "" + component.getStringValue(connectionProvider, stringType, maxLen); //$NON-NLS-1$

            // Append Field Tag
            if (maxLen < 0 || sb.length() < maxLen) {
                sb.append(fieldTag);
            }
            len = len + fieldTag.length();

            // Append Component String
            if (maxLen < 0 || sb.length() < maxLen) {
                sb.append(componentValue);
            }
            len = len + componentValue.length();

            i++;
        }

        // Append Close Brace
        if (maxLen < 0 || sb.length() < maxLen) {
            sb.append(")");
        }
        len++;

        return new StringValue(sb.toString(), len);
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        Object internalValue = value.getInternalValue();
        
       Throwable t = new IllegalArgumentException();
       t.fillInStackTrace();
       try {
            LinkedList<DataValue> dataValueList = customComponents(value);
            int arraySize = dataValueList.size();

            ArrayList objectArray = new ArrayList(arraySize);

            // Convert elements to typed values
            for (DataValue dataValue : dataValueList) {
                objectArray.add(dataValue.getTypedValue(ValueType.DATUM));
            }

            // Create new oracle.sql.STRUCT
            Connection validConnection = connectionProvider.getValidDataTypeConnection();
            
            // Get Delegate - Bug 19513781 STRUCT does a downcast to an internal concrete class so unwrap
            validConnection = JDBCProxyUtil.getInstance().unwrap(validConnection);
            
            StructDescriptor desc =
                StructDescriptor.createDescriptor(getUserDataTypeString(), validConnection);
            return new oracle.sql.STRUCT(desc, validConnection,
                                        objectArray.toArray(new Object[arraySize]));
        } catch (SQLException e) {
            if (t != null) {
                throw new DataTypeIllegalArgumentException(this, internalValue, t);
            } else {
                throw new DataTypeIllegalArgumentException(this, internalValue, t);
            }
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        return oracle.jdbc.OracleStruct.class;
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.STRUCT;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        LinkedList<DataValue> dataValueList = new LinkedList<DataValue>();

        try {
            if (value == null) {
                return customInternalValue(connectionProvider, dataValueList);
            } else if (value instanceof oracle.jdbc.internal.OracleStruct) {
                oracle.jdbc.internal.OracleStruct valueStruct = (oracle.jdbc.internal.OracleStruct)value;

                oracle.sql.Datum[] datumArray = valueStruct.getOracleAttributes();

                return customInternalValue(connectionProvider, datumArray);
            } else if (value.getClass().isArray()) {
                int arrayLength = Array.getLength(value);

                // Via Types
                int i = 0;
                Iterable<NamedValue<DataType>> components = getTypeComponents();
                for (NamedValue<DataType> typeComponent : components) {
                    // Missing fields will become null
                    Object fieldValue = (i < arrayLength) ? Array.get(value, i++) : null;

                    DataValue newDataValue = typeComponent.getValue().getDataValue(fieldValue);
                    dataValueList.add(NamedDataValue.getNamedDataValue(typeComponent.getName(), newDataValue));
                }

                return dataValueList;
            } else if (value instanceof Collection) {
                Collection collection = (Collection)value;

                Object[] values = collection.toArray(new Object[0]);

                return customInternalValue(connectionProvider, values);
            } else {
                throw new DataTypeIllegalArgumentException(this, value);
            }
        } catch (SQLException e) {
            throw new DataTypeIllegalArgumentException(this, value);
        }
    }

    @Override
    protected Object customInternalValueFilter(DataTypeConnectionProvider connectionProvider, Object value) {
        if (DataTypesUtil.isEmpty(value)) {
            return customInternalValue(connectionProvider, null);
        } else {
            return super.customInternalValueFilter(connectionProvider, value);
        }
    }

    public <P extends DataBinding> BindingStrategy getBind(BindContext context, P param) {
        if (CallableStatement.class.isAssignableFrom(context.getEffectiveStatementClass())) {
            return new CallableBindingUDT<OracleCallableStatement,P>(context, param);
        } else {
            return new PrepareBindingUDT<OraclePreparedStatement,P>(context, param);
        }
    }

    @Override
    protected LinkedList<DataValue> customComponents(DataValueInternal value) {
        return (LinkedList<DataValue>)value.getInternalValue();
    }
    @Override
    protected DataValue customDataValue(Object object) {
        return new CompositeValue(this, object);
    }

    @Override
    public Object startDataValue(String name, boolean isNull) {
        if (isNull) {
            return null;
        } else {
            return customInternalValue(getDataTypeContext().getDataTypeConnectionProvider(), null);
        }
    }

    @Override
    public void bodyDataValue(NamedValue value, char[] ch, int start, int length) {
        ; // Ignore body data;
    }

    @Override
    public void bodyDataValue(NamedValue value, String text) {
        ; // Ignore body data;
    }

    @Override
    public void bodyDataValue(NamedValue value, DataValue dataValue) {
        LinkedList<DataValue> dataValueList = (LinkedList<DataValue>)value.getValue();
        
        // Insert Value into list - ignore if does not match to handle updated records
        int i = 0;
        for (DataValue valueSlot : dataValueList) {
            String slotName = valueSlot.getName();
            String valueName = dataValue.getName();
            if (slotName != null && valueName != null &&
                slotName.equals(valueName)) {
                dataValueList.set(i, NamedDataValue.getNamedDataValue(slotName, dataValue));
            }
            i++;
        }
    }

    @Override
    public DataValue endDataValue(NamedValue value) {
        return customDataValue(value.getValue());
    }

    @Override
    public StructureType getStructureType() {
        return StructureType.RECORD;
    }
}

