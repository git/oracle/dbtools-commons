/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.sql.SQLException;
import java.sql.Timestamp;

import java.text.ParseException;
import java.text.ParsePosition;

import java.util.Arrays;

import java.util.TimeZone;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.objects.OraTIMESTAMP;
import oracle.dbtools.raptor.datatypes.objects.OraTemporalDatum;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.util.TemporalUtil;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.dbtools.raptor.nls.OraTIMESTAMPFormat;
import oracle.dbtools.raptor.nls.OracleNLSProvider;

import oracle.jdbc.OracleTypes;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=TIMESTAMP.java">Barry McGillin</a> 
 *
 */
public class TIMESTAMP extends TemporalDatum {
    protected TIMESTAMP(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        Object internalValue = value.getInternalValue();
        
        switch (stringType) {
            case REST:
                try {
                    if (internalValue instanceof oracle.sql.TIMESTAMP) {
                        TimeZone sessionTZ = (NLSProvider.getProvider(connectionProvider.getNLSConnection())).getSessionTimeZone();
                        OraTIMESTAMP ts = OraTIMESTAMP.getInstance((oracle.sql.TIMESTAMP)internalValue).setSessionTimeZone(sessionTZ);
                    
                        return new StringValue(TemporalUtil.toRestString(ts));
                    }
                } catch (Exception e) {
                    throw new DataTypeIllegalArgumentException(this, internalValue);
                }
            case GENERIC:
                // Formats a date in the date escape format yyyy-mm-dd.
                try {
                    return new StringValue(((oracle.sql.TIMESTAMP)internalValue).timestampValue().toString());
                } catch (SQLException e) {
                    // SQLException signals Illegal Argument - not connection related
                    throw new DataTypeIllegalArgumentException(this, internalValue);
                }
            default:
                return super.customStringValue(connectionProvider, value, stringType, maxLen);
        }
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        Object internalValue = value.getInternalValue();
        
        oracle.sql.TIMESTAMP timestampValue = (oracle.sql.TIMESTAMP)internalValue;
        try {
            switch (valueType) {
                case JDBC:
                    return timestampValue.timestampValue();
                default:
                    return super.customTypedValue(connectionProvider, value, valueType, target);
            }
        } catch (SQLException e) {
            throw new DataTypeIllegalArgumentException(this, internalValue);
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case DEFAULT:
                return oracle.sql.TIMESTAMP.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.TIMESTAMP;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        if (value instanceof oracle.sql.TIMESTAMP) {
            // already correctly typed
            return value;
        } else if (value instanceof oracle.sql.DATE) {
            return new oracle.sql.TIMESTAMP((oracle.sql.DATE)value);
        } else if (value instanceof Timestamp) {
            return new oracle.sql.TIMESTAMP((Timestamp)value);
        } else if (value instanceof byte[]) {
            return new oracle.sql.TIMESTAMP(Arrays.copyOf((byte[])value, ((byte[])value).length));
        } else {
            // Try super class
            Object superObject = super.customInternalValue(connectionProvider, value);
            
            if (superObject instanceof OraTemporalDatum) {
                return new oracle.sql.TIMESTAMP(((OraTemporalDatum)superObject).toTimestamp());
            }
            
            // Try NLS format
            return typedValueFromNLSString(connectionProvider, superObject.toString());
        }
    }

    protected oracle.sql.TIMESTAMP typedValueFromNLSString(DataTypeConnectionProvider connectionProvider, String value) {
        OraTIMESTAMPFormat format;
        try {
            format = ((OracleNLSProvider)NLSProvider.getProvider(connectionProvider.getNLSConnection())).getOraTIMESTAMPFormat();
        } catch (ParseException pe) {
            format = null;
        }

        return format.parse(value, new ParsePosition(0));
    }

    @Override
    protected String customDataTypeString() {
        Integer scale = typeMetadata.get_data_scale();

        if (scale != null) {
            return "TIMESTAMP (" + scale + ")"; //$NON-NLS-1$ //$NON-NLS-2$
        } else {
            return "TIMESTAMP"; //$NON-NLS-1$
        }
    }

    @Override
    protected String customUnconstrainedDataTypeString() {
        return "TIMESTAMP_UNCONSTRAINED"; //$NON-NLS-1$
    }
}
