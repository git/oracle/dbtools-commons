/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.sql.SQLException;
import java.sql.Timestamp;

import java.text.ParseException;
import java.text.ParsePosition;

import java.util.Arrays;
import java.util.Calendar;

import java.util.TimeZone;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.DataTypeIllegalArgumentException;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.objects.OraTIMESTAMPLTZ;
import oracle.dbtools.raptor.datatypes.objects.OraTemporalDatum;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.util.TemporalUtil;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.dbtools.raptor.nls.OraTIMESTAMPLTZFormat;
import oracle.dbtools.raptor.nls.OracleNLSProvider;

import oracle.jdbc.OracleTypes;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=TIMESTAMPLTZ.java">Barry McGillin</a> 
 *
 */
public class TIMESTAMPLTZ extends TimeZoneDatum {
    protected TIMESTAMPLTZ(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        Object internalValue = value.getInternalValue();
        
        switch (stringType) {
            case REST:
                try {
                    if (internalValue instanceof oracle.sql.TIMESTAMPLTZ) {
                        TimeZone dbTZ = (NLSProvider.getProvider(connectionProvider.getNLSConnection())).getDatabaseTimeZone();
                        TimeZone sessionTZ = (NLSProvider.getProvider(connectionProvider.getNLSConnection())).getSessionTimeZone();
                        OraTIMESTAMPLTZ tsltz = OraTIMESTAMPLTZ.getInstance((oracle.sql.TIMESTAMPLTZ)internalValue, dbTZ);
                        tsltz.setSessionTimeZone(sessionTZ);
                    
                        return new StringValue(TemporalUtil.toRestString(tsltz));
                    }
                } catch (Exception e) {
                    throw new DataTypeIllegalArgumentException(this, internalValue);
                }
            case GENERIC:
                // Formats a date in the date escape format yyyy-mm-dd.
                try {
                    if (internalValue instanceof oracle.sql.TIMESTAMPLTZ) {
                       return new StringValue(((oracle.sql.TIMESTAMPLTZ)internalValue).timestampValue(connectionProvider.getNLSConnection(), Calendar.getInstance()).toString());
                    }
                } catch (SQLException e) {
                    // SQLException signals Illegal Argument - not connection related
                    throw new DataTypeIllegalArgumentException(this, internalValue);
                }
            default:
                return super.customStringValue(connectionProvider, value, stringType, maxLen);
        }
    }

    @Override
    protected Object customTypedValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, ValueType valueType, Object target) {
        Object internalValue = value.getInternalValue();
        
        switch (valueType) {
            case JDBC:
                try {
                    if (internalValue instanceof oracle.sql.TIMESTAMPLTZ) {
                        oracle.sql.TIMESTAMPLTZ timestampValue = (oracle.sql.TIMESTAMPLTZ)internalValue;
                        return oracle.sql.TIMESTAMPLTZ.toTimestamp(connectionProvider.getNLSConnection(), timestampValue.getBytes());
                        // NOTE: Used above conversion routine because the following throws an exception
                        //return timestampValue.timestampValue();
                    }
                } catch (SQLException e) {
                    throw new DataTypeIllegalArgumentException(this, internalValue);
                }
            default:
                return super.customTypedValue(connectionProvider, value, valueType, target);
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case DEFAULT:
                if (connectionProvider.peekNLSConnection() == null) {
                    return super.customTypedClass(connectionProvider, valueType);
                }
                // break;
            case DATUM:
                return oracle.sql.TIMESTAMPLTZ.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected int customSqlDataType(ValueType valueType) {
        switch (valueType) {
            case DATUM:
                return OracleTypes.TIMESTAMPLTZ;
            default:
                return super.customSqlDataType(valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        try {
            if (value instanceof oracle.sql.TIMESTAMPLTZ) {
                // already correctly typed
                return value;
            } else if (value instanceof byte[]) {
                return new oracle.sql.TIMESTAMPLTZ(Arrays.copyOf((byte[])value, ((byte[])value).length));
            } else {
                // Try super class
                Object superObject = super.customInternalValue(connectionProvider, value);
                
                if (superObject instanceof OraTIMESTAMPLTZ) {
                    return ((OraTIMESTAMPLTZ)superObject).getDatum();
                } else if (superObject instanceof OraTemporalDatum) {
                    final Timestamp ts = ((OraTemporalDatum)superObject).toTimestamp();
                    
                    if (connectionProvider.peekNLSConnection() != null) {
                        return new oracle.sql.TIMESTAMPLTZ(connectionProvider.getNLSConnection(), ts);
                    } else {
                        return OraTIMESTAMPLTZ.getInstance(ts).getDatum();
                    }
                }
                
                // Try NLS format
                return typedValueFromNLSString(connectionProvider, superObject.toString());
            }
        } catch (SQLException e) {
            throw new DataTypeIllegalArgumentException(this, value);
        }
    }

    protected oracle.sql.TIMESTAMPLTZ typedValueFromNLSString(DataTypeConnectionProvider connectionProvider, String value) {
        OraTIMESTAMPLTZFormat format;
        try {
            format = ((OracleNLSProvider)NLSProvider.getProvider(connectionProvider.getNLSConnection())).getOraTIMESTAMPLTZFormat();
        } catch (ParseException pe) {
            format = null;
        }

        return format.parse(value, new ParsePosition(0));
    }

    @Override
    protected String customUnconstrainedDataTypeString() {
        return "TIMESTAMP_LTZ_UNCONSTRAINED"; //$NON-NLS-1$
    }

    @Override
    protected String customDataTypeString() {
        Integer scale = typeMetadata.get_data_scale();

        if (scale != null) {
            return "TIMESTAMP (" + scale +
                ") WITH LOCAL TIME ZONE"; //$NON-NLS-1$ //$NON-NLS-2$
        } else {
            return "TIMESTAMP WITH LOCAL TIME ZONE"; //$NON-NLS-1$
        }
    }
}
