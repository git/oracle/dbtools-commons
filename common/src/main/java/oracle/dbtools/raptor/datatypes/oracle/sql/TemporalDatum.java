/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.oracle.sql;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;

import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataTypeContext;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataValueInternal;
import oracle.dbtools.raptor.datatypes.util.StringValue;
import oracle.dbtools.raptor.datatypes.util.TemporalUtil;
import oracle.dbtools.raptor.utils.NLSUtils;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=TemporalDatum.java">Barry McGillin</a> 
 *
 */
public abstract class TemporalDatum extends Datum {
    protected TemporalDatum(DataTypeContext context, TypeMetadata typeMetadata) {
        super(context, typeMetadata);
    }

    @Override
    protected StringValue customStringValue(DataTypeConnectionProvider connectionProvider, DataValueInternal value, StringType stringType, int maxLen) {
        switch (stringType) {
            case NLS:
                try {
                    return new StringValue((CharSequence)NLSUtils.getValue(connectionProvider.getNLSConnection(), value.getInternalValue()));
                } catch (NullPointerException e) {
                    // Please don't remove this exception handler
                    // until you make NLSUtils.getValue super duper robust
                    return customStringValue(connectionProvider, value, StringType.GENERIC, maxLen);
                }
            default:
                return super.customStringValue(connectionProvider, value, stringType, maxLen);
        }
    }
    
    @Override
    protected Class customTypedClass(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        switch (valueType) {
            case JDBC:
                return Timestamp.class;
            default:
                return super.customTypedClass(connectionProvider, valueType);
        }
    }

    @Override
    protected Object customInternalValue(DataTypeConnectionProvider connectionProvider, Object value) {
        Object candidate = value;
      
        if (value instanceof oracle.sql.TIMESTAMP) {
            try {
                candidate = ((oracle.sql.TIMESTAMP)value).timestampValue();
            } catch (SQLException e) {
                return value;
            }
        } else if (value instanceof oracle.sql.DATE) {
            candidate = ((oracle.sql.DATE)value).timestampValue();
        } else if (value instanceof CharSequence) {
            String stringValue = value.toString();
            // Try GENERIC formats
            try {
                candidate = Timestamp.valueOf(stringValue);
            } catch (Exception e1) {
                try {
                    candidate = Date.valueOf(stringValue);
                } catch (Exception e2) {
                    ; // Continue;
                }
            }
        }
        
        try {
            return TemporalUtil.toOraTemporalDatum(candidate);
        } catch (Exception e1) {
            ; // Continue;
        }
        
        return value;
     }
}
