/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.strategies.callablestatement;

import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingMode;
import oracle.dbtools.raptor.datatypes.BindingStyle;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataParameter;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.PLSQLBlockComponent;
import oracle.dbtools.raptor.datatypes.PLSQLBoundBlockBuilder;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.BindingStrategySplitMode;
import oracle.dbtools.raptor.datatypes.values.DataValueMapped;

import oracle.jdbc.OracleCallableStatement;

/**
 *
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=CallableBindingBOOLEAN.java"
 *         >Barry McGillin</a>
 *
 * @param <P>
 */
public class CallableBindingBOOLEAN<S extends OracleCallableStatement, P extends DataBinding> extends CallableBindingBase<S,P> implements
        BindingStrategySplitMode<S,P> {
	private final CallableBindingSplit<S,P> splitBinding;

	private final static String BOOL2INT = "FUNCTION bool2int" + //$NON-NLS-1$
	        "(" + //$NON-NLS-1$
	        "b BOOLEAN" + //$NON-NLS-1$
	        ")" + //$NON-NLS-1$
	        "RETURN NUMBER " + //$NON-NLS-1$
	        "AS " + //$NON-NLS-1$
	        "BEGIN " + //$NON-NLS-1$
	        "IF (b IS NULL)" + //$NON-NLS-1$
	        "THEN " + //$NON-NLS-1$
	        "RETURN NULL;" + //$NON-NLS-1$
	        "ELSE " + //$NON-NLS-1$
	        "CASE b " + //$NON-NLS-1$
	        "WHEN true " + //$NON-NLS-1$
	        "THEN " + //$NON-NLS-1$
	        "RETURN 1;" + //$NON-NLS-1$
	        "ELSE " + //$NON-NLS-1$
	        "RETURN 0;" + //$NON-NLS-1$
	        "END CASE;" + //$NON-NLS-1$
	        "END IF;" + //$NON-NLS-1$
	        "END bool2int;"; //$NON-NLS-1$

	private final static String INT2BOOL = "FUNCTION int2bool" + //$NON-NLS-1$
	        "(" + //$NON-NLS-1$
	        "i NUMBER" + //$NON-NLS-1$
	        ")" + //$NON-NLS-1$
	        "RETURN BOOLEAN " + //$NON-NLS-1$
	        "AS " + //$NON-NLS-1$
	        "BEGIN " + //$NON-NLS-1$
	        "IF (i IS NULL)" + //$NON-NLS-1$
	        "THEN " + //$NON-NLS-1$
	        "RETURN NULL;" + //$NON-NLS-1$
	        "ELSE " + //$NON-NLS-1$
	        "RETURN (i <> 0);" + //$NON-NLS-1$
	        "END IF;" + //$NON-NLS-1$
	        "END int2bool;"; //$NON-NLS-1$

	/**
	 * 
	 * CallableBindingBOOLEAN
	 * 
	 * @param context
	 * @param param
	 * @param mappedType
	 */
	public CallableBindingBOOLEAN(BindContext<S> context, P param, DataType mappedType) {
		super(context, param);

		splitBinding = new CallableBindingSplit<S,P>(context, param, mappedType) {
			@Override
			public PLSQLBoundBlockBuilder customBuilder(PLSQLBoundBlockBuilder builder) {
				boolean add_bool2int = false;
				boolean add_int2bool = false;
                
                final P param = getParameter();
                
				String name = param.getName();
				DataType datatype = param.getDataType();
				BindingMode mode = param.getMode();
                
				if (param instanceof DataParameter) {
					switch (mode) {
					case RETURN:
						builder.addComponent(PLSQLBlockComponent.PreCallWrapper, "%s := bool2int(", //$NON-NLS-1$
						        getBindToken(mode));
						builder.addComponent(PLSQLBlockComponent.PostCallWrapper, ")"); //$NON-NLS-1$
						add_bool2int = true;
						break;
					case IN:
						builder.addComponent(PLSQLBlockComponent.ParamBinding, name + "=>int2bool(%s)", //$NON-NLS-1$
						        getBindToken(mode));
						add_int2bool = true;
						break;
					case IN_OUT:
						builder.addComponent(PLSQLBlockComponent.PreCallBlocks, name + " := int2bool(%s);", //$NON-NLS-1$
						        inDatum.getBindToken(inDatum.getMode()));
						add_int2bool = true;
						// fall thru
					case OUT:
						builder.addComponent(PLSQLBlockComponent.DataDecls,
						        name + " " + datatype.getConstrainedDataTypeString() + ";"); //$NON-NLS-1$ //$NON-NLS-2$
						builder.addComponent(PLSQLBlockComponent.ParamBinding, name + "=>" + name); //$NON-NLS-1$
						builder.addComponent(PLSQLBlockComponent.PostCallBlocks, "%s := bool2int(" + name + ");", //$NON-NLS-1$ //$NON-NLS-2$
						        getBindToken(mode));
						add_bool2int = true;
						break;
					}
				} else {
					switch (mode) {
					case IN:
						builder.addComponent(PLSQLBlockComponent.PreCallBlocks, name + " := int2bool(%s);", //$NON-NLS-1$
						        getBindToken(mode));
						add_int2bool = true;
						break;
					case IN_OUT:
						builder.addComponent(PLSQLBlockComponent.PreCallBlocks, name + " := int2bool(%s);", //$NON-NLS-1$
						        inDatum.getBindToken(inDatum.getMode()));
						add_int2bool = true;
						// fall thru for IN/OUT
					case OUT:
						builder.addComponent(PLSQLBlockComponent.PostCallBlocks, "%s := bool2int(" + name + ");", //$NON-NLS-1$ //$NON-NLS-2$
						        getBindToken(mode));
						add_bool2int = true;
						break;
					}
				}

				// Add Dependents
				if (add_bool2int) {
					builder.addComponent(PLSQLBlockComponent.StdProcDecls, BOOL2INT);
				}
				// more
				if (add_int2bool) {
					builder.addComponent(PLSQLBlockComponent.StdProcDecls, INT2BOOL);
				}

				return builder;
			}
		};
	}

	@Override
	public void setByNameBindToken(String bindToken) {
		splitBinding.setByNameBindToken(bindToken);
	}

	@Override
	/**
	 * @return String
	 */
	public String getByNameBindToken() {
		return splitBinding.getByNameBindToken();
	}

	/**
 * 
 */
	@Override
	public void setByPositionBindToken(String bindToken) {
		splitBinding.setByPositionBindToken(bindToken);
	}

	/**
	 * @return String
	 */
	@Override
	public String getByPositionBindToken() {
		return splitBinding.getByPositionBindToken();
	}

	@Override
	public BindingStyle getBindingStyle(BindingMode mode) {
		return splitBinding.getBindingStyle(mode);
	}

	@Override
	public String getBindToken(BindingMode mode) {
		return splitBinding.getBindToken(mode);
	}

	@Override
	protected void customBind(DataValue value) throws SQLException {
		DataValue bindingValue = (value instanceof DataValueMapped) ? value.getBindingDataValue()
		        : value;

		splitBinding.bind(getStatement(), bindingValue);
	}

	@Override
	protected void customBindIN(DataValue value) throws SQLException {
		; // No implementation required
	}

	@Override
	public void customReportBinding(StringBuilder buffer, String nullToken, DataValue value) {
		DataValue bindingValue = value.getBindingDataValue();
		splitBinding.customReportBinding(buffer, nullToken, bindingValue);
	}

	@Override
	public PLSQLBoundBlockBuilder customBuilder(PLSQLBoundBlockBuilder builder) {
		return splitBinding.customBuilder(builder);
	}

	@Override
	protected void customBind() throws SQLException {
		splitBinding.bind(getStatement());
	}

	@Override
	protected void customBindOUT() throws SQLException {
		; // No implementation required
	}

	@Override
	protected DataValue customOutput() throws SQLException {
        DataValue bindingValue = splitBinding.getOutput();
        return getDataType().getDataValue(bindingValue.getTypedValue(getBindContext().getDataTypeConnectionProvider(),
                                          ValueType.DEFAULT));
	}

	@Override
	protected BindingStrategySplitMode<S,P> customSplitModeBinding() {
		return this;
	}
}
