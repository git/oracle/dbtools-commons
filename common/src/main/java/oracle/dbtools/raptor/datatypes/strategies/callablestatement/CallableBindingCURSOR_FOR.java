/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.strategies.callablestatement;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingMode;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataParameter;
import oracle.dbtools.raptor.datatypes.PLSQLBlockComponent;
import oracle.dbtools.raptor.datatypes.PLSQLBoundBlockBuilder;

import oracle.jdbc.OracleCallableStatement;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=CallableBindingCURSOR_FOR.java">Barry McGillin</a>
 *
 * @param <P>
 */
public class CallableBindingCURSOR_FOR<S extends OracleCallableStatement, P extends DataBinding> extends CallableBindingVARCHAR<S,P> {

    public CallableBindingCURSOR_FOR(BindContext<S> context, P param) {
        super(context, param);
    }

    @Override
    protected PLSQLBoundBlockBuilder customBuilder(PLSQLBoundBlockBuilder builder) {
        String name = getParameter().getName();

        builder.addComponent(PLSQLBlockComponent.DataDecls,
                             name + " " + "SYS_REFCURSOR"/*dataType.getConstrainedDataTypeString()*/ +
                             ";"); //$NON-NLS-1$ //$NON-NLS-2$

        if (getMode() == BindingMode.IN || getMode() == BindingMode.IN_OUT) {
            builder.addComponent(PLSQLBlockComponent.DataInitBlocks,
                                 "OPEN " + name + " FOR %s;", //$NON-NLS-1$ //$NON-NLS-2$
                                 getBindToken(getMode()));
            builder.addComponent(PLSQLBlockComponent.OthersBlocks,
                                 "BEGIN CLOSE " + name + "; EXCEPTION WHEN OTHERS THEN NULL; END;", //$NON-NLS-1$
                                 getBindToken(getMode())); //$NON-NLS-1$ //$NON-NLS-2$
            
        }

        if (getParameter() instanceof DataParameter) {
           switch (getMode()) {
                case RETURN:
                    builder.addComponent(PLSQLBlockComponent.PreCallWrapper,
                                         name + " := "); //$NON-NLS-1$
                    break;
                default:
                    builder.addComponent(PLSQLBlockComponent.ParamBinding,
                                         name + "=>" + name); //$NON-NLS-1$
                    break;
            }
        } else {
            switch (getMode()) {
                case OUT:
                    builder.addComponent(PLSQLBlockComponent.PostCallBlocks, "%s := " + name + ";",
                                         getBindToken(getMode())); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    break;
            }
        }

        return builder;
    }
    
    @Override
    public boolean isSupported(BindingMode mode) {
        // OUT not supported
        switch (mode.getEffectiveMode()) {
            case OUT:
            case IN_OUT:
                return false;
            default:
                return super.isSupported(mode);
        }
    }
}
