/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.strategies.callablestatement;

import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingMode;
import oracle.dbtools.raptor.datatypes.BindingStyle;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataParameter;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.PLSQLBlockComponent;
import oracle.dbtools.raptor.datatypes.PLSQLBoundBlockBuilder;
import oracle.dbtools.raptor.datatypes.ValueType;

import oracle.dbtools.raptor.datatypes.BindingStrategySplitMode;

import oracle.jdbc.OracleCallableStatement;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=CallableBindingDatumAtName.java">Barry McGillin</a> 
 *
 * @param <P>
 */
public class CallableBindingDatumAtName<S extends OracleCallableStatement, P extends DataBinding> extends CallableBindingBase<S,P> {
    public CallableBindingDatumAtName(BindContext<S> context, P param) {
        this(context, param, param.getDataType());
    }

    public CallableBindingDatumAtName(BindContext<S> context, P param, DataType dataType) {
        this(context, param, dataType, param.getMode());
    }

    public CallableBindingDatumAtName(BindContext<S> context, P param, BindingMode modeOverride) {
        this(context, param, param.getDataType(), modeOverride);
    }

    protected CallableBindingDatumAtName(BindContext<S> context, P param, DataType dataType,
                                         BindingMode modeOverride) {
        super(context, param, dataType, modeOverride);
    }
    
    @Override
    public BindingStyle getBindingStyle(BindingMode mode) {
        if (mode.getEffectiveMode() == BindingMode.IN && getByNameBindToken() != null) {
            return BindingStyle.NAME;
        } else {
            return BindingStyle.POSITION;
        }
    }
    
    @Override
    public String getBindToken(BindingMode mode) {
        if (getBindingStyle(mode) == BindingStyle.NAME) {
            return getByNameBindToken();
        } else {
            return getByPositionBindToken();
        }
    }

    @Override
    protected DataValue customOutput() throws SQLException {
        return customOutput(getBindContext().remapPosition(getByPositionBindToken()));
    }

    @Override
    protected PLSQLBoundBlockBuilder customBuilder(PLSQLBoundBlockBuilder builder) {
        String name = getParameter().getName();

        if (getParameter() instanceof DataParameter) {
            switch (getMode()) {
                case RETURN:
                    builder.addComponent(PLSQLBlockComponent.PreCallWrapper, "%s := ",
                                         getBindToken(getMode())); //$NON-NLS-1$ //$NON-NLS-2$
                    break;
                default:
                    builder.addComponent(PLSQLBlockComponent.ParamBinding, name + "=>%s",
                                         getBindToken(getMode())); //$NON-NLS-1$ //$NON-NLS-2$
                    break;
            }
        } else {
            switch (getMode()) {
                case IN:
                    builder.addComponent(PLSQLBlockComponent.PreCallBlocks, name + " := %s;",
                                         getBindToken(getMode())); //$NON-NLS-1$ //$NON-NLS-2$
                    break;
                case OUT:
                    builder.addComponent(PLSQLBlockComponent.PostCallBlocks, "%s := " + name + ";",
                                         getBindToken(getMode())); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    break;
            }
        }

        return builder;
    }

    /**
     * Bind the "input" value to a statement at the given position. Only need
     * to override if the JDBC driver does not auto-magically map the internal
     * implementation class of this data type based on the SQL data type.
     */
    @Override
    protected void customBindIN(DataValue value) throws SQLException {
        if (getBindingStyle(getMode()) == BindingStyle.NAME) {
            customBindIN(value, getByNameBindToken());
        } else {
            customBindIN(value, getBindContext().remapPosition(getByPositionBindToken()));
        }
    }

    protected void customBindIN(DataValue value, int pos) throws SQLException {
        Object typedValue = (value != null) ? value.getTypedValue(getBindContext().getDataTypeConnectionProvider(), ValueType.DATUM) : null;

        if (typedValue != null) {
          getStatement().setObject(pos, typedValue, getDataType().getSqlDataType(ValueType.DATUM));
        } else {
          getStatement().setNull(pos, getDataType().getSqlDataType(ValueType.DATUM));
        }
    }

    protected void customBindIN(DataValue value, String name) throws SQLException {
        Object typedValue = (value != null) ? value.getTypedValue(getBindContext().getDataTypeConnectionProvider(), ValueType.DATUM) : null;

        if (typedValue != null) {
            getStatement().setObjectAtName(name, typedValue, getDataType().getSqlDataType(ValueType.DATUM));
        } else {
            String userTypeName = getDataType().getUserDataTypeString();

            if (userTypeName != null) {
                getStatement().setNull(name, getDataType().getSqlDataType(ValueType.DATUM), userTypeName);
            } else {
                getStatement().setNull(name, getDataType().getSqlDataType(ValueType.DATUM));
            }
        }
    }

    /**
     * Register the "result" value SQL type for a statement at the given
     * position. Probably never need to override this.
     */
    @Override
    protected void customBindOUT() throws SQLException {
        if (getBindingStyle(getMode()) == BindingStyle.NAME) {
            customBindOUT(getByNameBindToken());
        } else {
            customBindOUT(getBindContext().remapPosition(getByPositionBindToken()));
        }
    }

    protected void customBindOUT(String bindToken) throws SQLException {
        String userTypeName = getDataType().getUserDataTypeString();

        if (userTypeName != null) {
            getStatement().registerOutParameter(bindToken, getDataType().getSqlDataType(ValueType.DATUM), userTypeName);
        } else {
            getStatement().registerOutParameter(bindToken, getDataType().getSqlDataType(ValueType.DATUM));
        }
    }

    protected void customBindOUT(int pos) throws SQLException {
        String userTypeName = getDataType().getUserDataTypeString();

        if (userTypeName != null) {
            getStatement().registerOutParameter(pos, getDataType().getSqlDataType(ValueType.DATUM),userTypeName);
        } else {
            getStatement().registerOutParameter(pos, getDataType().getSqlDataType(ValueType.DATUM));
        }
    }

    @Override
    protected void customReportBinding(StringBuilder buffer, String nullToken, DataValue value) {
        reportBinding(buffer, nullToken, getBindToken(getMode()), value.getDataType(), getMode().getEffectiveMode(),
                      value.getStringValue().toString());
    }

    @Override
    protected BindingStrategySplitMode<S,P> customSplitModeBinding() {
        CallableBindingSplit<S,P> binding = new CallableBindingSplit<S,P>(getBindContext(), getParameter(), getDataType(), getMode());
        binding.setByNameBindToken(getByNameBindToken());
        binding.setByPositionBindToken(getByPositionBindToken());
        return binding;
    }

    /**
     * Get the value at the given position of the statement as a value of
     * this type. Only need to override if your <code>customTypedValue</code>
     * cannot create a correctly typed instance from whatever JDBC returns
     * for the SQL type registered for this position via {@link pos}
     * <p>
     * Note that the method is private since the intent is that this is one
     * of the types you should be able to create a value instance from.
     */
    protected DataValue customOutput(int pos) throws SQLException {
        Object result = null;
        
        try {
            /* warning orest getOracleObject can return unimplementsed for clob and blob (at least)) */
            result = getStatement().getOracleObject(pos);
        } catch (SQLException ex) {
            // Bug 19059301 : Suppress ORA-17021 as stmt structure is malformed
            if (ex.getErrorCode() != 17021) {
                throw ex;
            }   
        }
        
        return getDataType().getDataValue(result);
    }
}
