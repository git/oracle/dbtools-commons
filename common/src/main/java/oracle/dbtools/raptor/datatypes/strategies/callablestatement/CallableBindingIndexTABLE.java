/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.strategies.callablestatement;

import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingMode;
import oracle.dbtools.raptor.datatypes.BindingStyle;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.ValueType;

import oracle.jdbc.OracleCallableStatement;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=CallableBindingIndexTABLE.java">Barry McGillin</a>
 *
 * @param <P>
 */
public class CallableBindingIndexTABLE<S extends OracleCallableStatement, P extends DataBinding> extends CallableBindingDatum<S,P> {

    public CallableBindingIndexTABLE(BindContext<S> context, P param) {
        super(context, param);
    }

    @Override
    public BindingStyle getBindingStyle(BindingMode mode) {
        return BindingStyle.POSITION;
    }

    @Override
    protected void customBindIN(DataValue value, int pos) throws SQLException {
        Object[] typedValue = (Object[])value.getTypedValue(getBindContext().getDataTypeConnectionProvider(), ValueType.JDBC);

        // Get element type
        Iterable<NamedValue<DataType>> typeComponents = getDataType().getTypeComponents();
        DataType typeComponent = typeComponents.iterator().next().getValue();

        // Get SQL type code for element type
        int elemSqlType = typeComponent.getSqlDataType(ValueType.JDBC);

        // Get max data length for element type
        Integer elemDataLength = typeComponent.getTypeMetadata().get_data_length();
        int elemMaxLen = (elemDataLength != null) ? elemDataLength.intValue() : 0;

        // Set Index Table
        getStatement().setPlsqlIndexTable(pos, typedValue,
                                          typedValue.length, typedValue.length,
                                          elemSqlType, elemMaxLen);

    }

    @Override
    protected void customBindOUT(int pos) throws SQLException {
        // Get element type
        Iterable<NamedValue<DataType>> typeComponents = getDataType().getTypeComponents();
        DataType typeComponent = typeComponents.iterator().next().getValue();

        // Get SQL type code for element type
        int elemSqlType = typeComponent.getSqlDataType(ValueType.JDBC);

        // Get max data length for element type
        Integer elemDataLength = typeComponent.getTypeMetadata().get_data_length();
        int elemMaxLen = (elemDataLength != null) ? elemDataLength.intValue() : 0;

        getStatement().registerIndexTableOutParameter(pos,
                                                      1000, elemSqlType, elemMaxLen);
    }

    @Override
    protected DataValue customOutput(int pos) throws SQLException {
        Object result = getStatement().getPlsqlIndexTable(pos);
        return getDataType().getDataValue(result);
    }
}
