/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.strategies.callablestatement;

import java.sql.SQLException;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingMode;
import oracle.dbtools.raptor.datatypes.BindingStyle;
import oracle.dbtools.raptor.datatypes.BindingStrategy;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataParameter;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.DataVariable;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.PLSQLBlockComponent;
import oracle.dbtools.raptor.datatypes.PLSQLBoundBlockBuilder;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.objects.PLSQLRecord;
import oracle.dbtools.raptor.datatypes.BindingStrategySplitMode;

import oracle.dbtools.raptor.datatypes.DataTypeFactory;

import oracle.jdbc.OracleCallableStatement;

/**
 *
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=CallableBindingRECORD.java"
 *         >Barry McGillin</a>
 *
 * @param <P>
 */
public class CallableBindingRECORD<S extends OracleCallableStatement, P extends DataBinding> extends CallableBindingBase<S,P> implements
        BindingStrategySplitMode<S,P> {
	private List<BindingStrategySplitMode<S,DataVariable>> subBindings;

	public CallableBindingRECORD(BindContext<S> context, P param) {
		super(context, param);
	}

	/**
 * 
 */
	@Override
	public void setByNameBindToken(String bindToken) {
		; // No implementation
	}

	/**
 * 
 */
	@Override
	public String getByNameBindToken() {
		return null;
	}

	/**
 * 
 */
	@Override
	public void setByPositionBindToken(String bindToken) {
		; // No implementation
	}

	/**
 * 
 */
	@Override
	public String getByPositionBindToken() {
		return null;
	}

	/**
 * 
 */
	@Override
	public BindingStyle getBindingStyle(BindingMode mode) {
		return BindingStyle.CUSTOM;
	}

	/**
 * 
 */
	@Override
	public String getBindToken(BindingMode mode) {
		return null;
	}

	@Override
	protected void customBind(DataValue value) throws SQLException {
		List<DataValue> subComponents = value.getComponents();
		Iterator<DataValue> subComponentsIter = subComponents.iterator();

		for (BindingStrategySplitMode<S,DataVariable> binding : subBindings) {
			binding.bind(getStatement(), subComponentsIter.next());
		}
	}

	@Override
	protected void customBindIN(DataValue value) throws SQLException {
		; // No implementation required
	}

	@Override
	protected void customBind() throws SQLException {
		for (BindingStrategySplitMode<S,DataVariable> binding : subBindings) {
			binding.bind(getStatement());
		}
	}

	@Override
	protected void customBindOUT() throws SQLException {
		; // No implementation required
	}

	@Override
	public void customReportBinding(StringBuilder buffer, String nullToken, DataValue value) {
		List<DataValue> subComponents = value.getComponents();
		Iterator<DataValue> subComponentsIter = subComponents.iterator();

		for (BindingStrategySplitMode<S,DataVariable> binding : subBindings) {
			binding.reportBinding(buffer, nullToken, subComponentsIter.next());
		}
	}

	@Override
	public PLSQLBoundBlockBuilder customBuilder(PLSQLBoundBlockBuilder builder) {
		subBindings = new LinkedList<BindingStrategySplitMode<S,DataVariable>>();

		String name = (getParameter().getName() == null) ? "\"<RETURN>\"" : getParameter().getName(); //$NON-NLS-1$
		DataType datatype = getParameter().getDataType();

		Iterable<NamedValue<DataType>> components = datatype.getTypeComponents();
		for (NamedValue<DataType> typeComponent : components) {
			String componentName = name + "." + typeComponent.getName();
			DataType componentDataType = typeComponent.getValue();

			// Convert parameter to a field
			DataVariable field = datatype.getDataTypeFactory().getDataVariable(componentName, componentDataType, getMode());

			// Bind Field Binding
			BindingStrategy<S,DataVariable> fieldbinding = DataTypeFactory.getInstance().getBind(getBindContext(), field);

			// Convert to Split IN/OUT if necessary
			BindingStrategySplitMode<S,DataVariable> splitBinding = fieldbinding.getSplitModeBinding();

			// Build bindings
			PLSQLBoundBlockBuilder fieldBuilder = splitBinding.getBuilder();

			// Add field builder
			builder.addBuilder(fieldBuilder);

			// Remember Binding
			subBindings.add(splitBinding);
		}

		if (getParameter() instanceof DataParameter) {
			switch (getMode()) {
			case RETURN:
				builder.addComponent(PLSQLBlockComponent.PreCallWrapper, name + " := "); //$NON-NLS-1$
				break;
			default:
				builder.addComponent(PLSQLBlockComponent.ParamBinding, name + "=>" + name); //$NON-NLS-1$
				break;
			}

			// Add deplaration last
			builder.addComponent(PLSQLBlockComponent.DataDecls, name + " " + datatype.getConstrainedDataTypeString() + //$NON-NLS-1$
			        ";"); //$NON-NLS-1$
		}

		return builder;
	}

	@Override
	protected DataValue customOutput() throws SQLException {
        PLSQLRecord record = new PLSQLRecord(getDataType());

        // Get Value for each binding
        for (BindingStrategySplitMode<S,DataVariable> binding : subBindings) {
            DataValue dataValue = binding.getOutput();

            // Add Typed value to record
            record.add(dataValue.getTypedValue(getBindContext().getDataTypeConnectionProvider(), ValueType.DEFAULT));
        }

        // Construct Reord DataValue
        return getDataType().getDataValue(record);
	}

	@Override
	protected BindingStrategySplitMode<S,P> customSplitModeBinding() {
		return this;
	}
}
