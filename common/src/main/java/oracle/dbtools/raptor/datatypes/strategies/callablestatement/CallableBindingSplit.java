/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.strategies.callablestatement;

import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingMode;
import oracle.dbtools.raptor.datatypes.BindingStrategy;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataParameter;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.DataVariable;
import oracle.dbtools.raptor.datatypes.PLSQLBlockComponent;
import oracle.dbtools.raptor.datatypes.PLSQLBoundBlockBuilder;
import oracle.dbtools.raptor.datatypes.BindingStrategySplitMode;

import oracle.dbtools.raptor.datatypes.DataTypeFactory;

import oracle.jdbc.OracleCallableStatement;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=CallableBindingSplit.java">Barry McGillin</a>
 *
 * @param <P>
 */
public class CallableBindingSplit<S extends OracleCallableStatement, P extends DataBinding> extends CallableBindingDatumAtName<S,P> implements BindingStrategySplitMode<S,P> {
    protected final BindingStrategy<S,DataVariable> inDatum;

    public CallableBindingSplit(BindContext<S> context, P param) {
        this(context, param, param.getDataType());
    }

    public CallableBindingSplit(BindContext<S> context, P param, DataType dataType) {
        this(context, param, dataType, param.getMode());
    }

    public CallableBindingSplit(BindContext<S> context, P param, BindingMode modeOverride) {
        this(context, param, param.getDataType(), modeOverride);
    }

    public CallableBindingSplit(BindContext<S> context, P param, DataType dataType,
                                BindingMode modeOverride) {
        // For IN/OUT this object is switched to the OUT part and inDatum will be used for the IN part
        super(context, param, dataType,
              (modeOverride.getEffectiveMode() == BindingMode.IN_OUT) ? BindingMode.OUT :
              modeOverride);
        
        // Used for IN part of IN/OUT only. Everything else handled by this binding
        if ((param.getMode().getEffectiveMode() == BindingMode.IN_OUT)) {
            // Convert parameter to a field
            DataVariable inVariable =
                dataType.getDataTypeFactory().getDataVariable(param.getName(), dataType,
                                                              BindingMode.IN);

            // Bind Field Binding
            inDatum = DataTypeFactory.getInstance().getBind(context, inVariable);
        } else {
            inDatum = null;
        }
    }

    @Override
    public void setByNameBindToken(String bindToken) {
        if (inDatum != null) {
            inDatum.setByNameBindToken(bindToken);
        } else {
            super.setByNameBindToken(bindToken);
        }
    }

    @Override
    public String getByNameBindToken() {
        if (inDatum != null) {
            return inDatum.getByNameBindToken();
        } else {
            return super.getByNameBindToken();
        }
    }

    @Override
    protected void customBind(DataValue value) throws SQLException {
        if (inDatum != null) {
            inDatum.bind(getStatement(), value);
        }

        super.customBind(value);
    }

    @Override
    public PLSQLBoundBlockBuilder customBuilder(PLSQLBoundBlockBuilder builder) {
        String name = getParameter().getName();
        DataType dataType = getDataType();

        BindingMode mode = getParameter().getMode();
        if (getParameter() instanceof DataParameter) {
            switch (mode) {
                case IN:
                    super.customBuilder(builder);
                    break;
                case IN_OUT:
                    inDatum.getBuilder(builder);
                    // Intentionally fall thru for OUT part of IN/OUT
                case OUT:
                    builder.addComponent(PLSQLBlockComponent.DataDecls,
                                         name + " " + dataType.getConstrainedDataTypeString() +
                                         ";"); //$NON-NLS-1$ //$NON-NLS-2$
                    builder.addComponent(PLSQLBlockComponent.ParamBinding,
                                         name + "=>" + name); //$NON-NLS-1$
                    break;
                default:
                    super.customBuilder(builder);
                    break;
            }
        } else {
            switch (mode) {
                case IN:
                    super.customBuilder(builder);
                    break;
                case IN_OUT:
                    inDatum.getBuilder(builder);
                    // Intentionally fall thru for OUT part of IN/OUT
                case OUT:
                    super.customBuilder(builder);
                    break;
                default:
                    super.customBuilder(builder);
                    break;
            }
        }

        return builder;
    }

    @Override
    public void customReportBinding(StringBuilder buffer, String nullToken, DataValue value) {
        super.customReportBinding(buffer, nullToken, value);
        if (inDatum != null) {
            inDatum.reportBinding(buffer, nullToken, value);
        }
    }

    @Override
    protected BindingStrategySplitMode<S,P> customSplitModeBinding() {
        return this;
    }
}
