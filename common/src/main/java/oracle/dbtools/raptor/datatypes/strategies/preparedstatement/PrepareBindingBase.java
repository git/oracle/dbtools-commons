/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.strategies.preparedstatement;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingMode;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.PLSQLBoundBlockBuilder;
import oracle.dbtools.raptor.datatypes.BindingStrategySplitMode;
import oracle.dbtools.raptor.datatypes.strategies.statement.StatementBindingBase;

/**
 * @author Administrator
 *
 */
public abstract class PrepareBindingBase<S extends PreparedStatement, P extends DataBinding> extends StatementBindingBase<S,P> {
    protected PrepareBindingBase(BindContext<S> context, P param) {
        this(context, param, param.getDataType(), param.getMode());
    }

    protected PrepareBindingBase(BindContext<S> context, P param, DataType dataType, BindingMode mode) {
        super(context, param, dataType, mode);
    }

    @Override
    public final DataValue getInput() throws SQLException {
        return getBindContext().getInput(this);
    }

    protected final void setInput(DataValue inputValue) throws SQLException {
        getBindContext().cacheInput(this, inputValue);
    }

    /**
     * Bind the "input" value to a statement at the given position. Only need
     * to override if the JDBC driver does not auto-magically map the internal
     * implementation class of this data type based on the SQL data type.
     */
    @Override
    public final void bind(S stmt, DataValue value) throws SQLException {
        setStatement(stmt);
        setInput(value);
        customBind(value);
    }

    protected void customBind(DataValue value) throws SQLException {
        switch (getMode().getEffectiveMode()) {
            case IN:
                customBindIN(value);
                break;
            case OUT:
                customBindOUT();
                break;
            case IN_OUT:
                customBindIN(value);
                customBindOUT();
                break;
        }
    }

    protected abstract void customBindIN(DataValue value) throws SQLException;

    /**
     * Bind the "input" value to a statement at the given position. Only need
     * to override if the JDBC driver does not auto-magically map the internal
     * implementation class of this data type based on the SQL data type.
     */
    @Override
    public final void bind(S stmt) throws SQLException {
        bind(stmt, null);
    }

    protected void customBind() throws SQLException {
        switch (getMode().getEffectiveMode()) {
            case OUT:
            case IN_OUT:
                customBindOUT();
                break;
        }
    }

    /**
     * Register the "result" value SQL type for a statement at the given
     * position. Probably never need to override this.
     */
    protected abstract void customBindOUT() throws SQLException;


    @Override
    public DataValue getOutput() throws SQLException {
        return null; // no implementation
    }

    protected PLSQLBoundBlockBuilder customBuilder(PLSQLBoundBlockBuilder builder) {
        return builder;
    }

    @Override
    public BindingStrategySplitMode<S, P> getSplitModeBinding() {
        // TODO Implement this method
        return null;
    }
}
