/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.strategies.preparedstatement;

import oracle.jdbc.OraclePreparedStatement;
import java.sql.SQLException;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingMode;
import oracle.dbtools.raptor.datatypes.BindingStyle;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.ValueType;

/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=PrepareBindingDatumAtName.java">Barry McGillin</a>
 *
 * @param <P>
 */
public class PrepareBindingDatumAtName<S extends OraclePreparedStatement, P extends DataBinding> extends PrepareBindingBase<S,P> {
    public PrepareBindingDatumAtName(BindContext<S> context, P param) {
        this(context, param, param.getDataType());
    }

    public PrepareBindingDatumAtName(BindContext<S> context, P param, DataType dataType) {
        this(context, param, dataType, param.getMode());
    }

    public PrepareBindingDatumAtName(BindContext<S> context, P param, BindingMode modeOverride) {
        this(context, param, param.getDataType(), modeOverride);
    }

    protected PrepareBindingDatumAtName(BindContext<S> context, P param, DataType dataType,
                                         BindingMode modeOverride) {
        super(context, param, dataType, modeOverride);
    }

    @Override
    public BindingStyle getBindingStyle(BindingMode mode) {
        if (getByNameBindToken() != null) {
            return BindingStyle.NAME;
        } else {
            return BindingStyle.POSITION;
        }
    }

    @Override
    public String getBindToken(BindingMode mode) {
        if (getBindingStyle(mode) == BindingStyle.NAME) {
            return getByNameBindToken();
        } else {
            return getByPositionBindToken();
        }
    }

    /**
     * Bind the "input" value to a statement at the given position. Only need
     * to override if the JDBC driver does not auto-magically map the internal
     * implementation class of this data type based on the SQL data type.
     */
    @Override
    protected void customBindIN(DataValue value) throws SQLException {
        String bindToken = getByNameBindToken();
        if (getBindingStyle(getMode()) == BindingStyle.NAME) {
            customBindIN(value, bindToken);
        } else {
            customBindIN(value, remapPosition(bindToken));
        }
    }

    protected void customBindIN(DataValue value, int pos) throws SQLException {
        Object typedValue = (value != null) ? value.getTypedValue(getBindContext().getDataTypeConnectionProvider(), ValueType.DATUM) : null;

        if (typedValue != null) {
          getStatement().setObject(pos, typedValue, getDataType().getSqlDataType(ValueType.DATUM));
        } else {
          getStatement().setNull(pos, getDataType().getSqlDataType(ValueType.DATUM));
        }
    }

    protected void customBindIN(DataValue value, String name) throws SQLException {
        Object typedValue = (value != null) ? value.getTypedValue(getBindContext().getDataTypeConnectionProvider(), ValueType.DATUM) : null;

        if (typedValue != null) {
            getStatement().setObjectAtName(name, typedValue, getDataType().getSqlDataType(ValueType.DATUM));
        } else {
            String userTypeName = getDataType().getUserDataTypeString();

            if (userTypeName != null) {
                getStatement().setNullAtName(name, getDataType().getSqlDataType(ValueType.DATUM), userTypeName);
            } else {
                getStatement().setNullAtName(name, getDataType().getSqlDataType(ValueType.DATUM));
            }
        }
    }

    /**
     * Register the "result" value SQL type for a statement at the given
     * position. Probably never need to override this.
     */
    @Override
    protected void customBindOUT() throws SQLException {
        String bindToken = getByNameBindToken();
        if (getBindingStyle(getMode()) == BindingStyle.NAME) {
            customBindOUT(bindToken);
        } else {
            customBindOUT(remapPosition(bindToken));
        }
    }

    protected void customBindOUT(String bindToken) throws SQLException {
        customBindOUT(remapPosition(bindToken));
    }

    protected void customBindOUT(int pos) throws SQLException {
        String userTypeName = getDataType().getUserDataTypeString();

        if (userTypeName != null) {
            getStatement().registerReturnParameter(pos, getDataType().getSqlDataType(ValueType.DATUM),userTypeName);
        } else {
            getStatement().registerReturnParameter(pos, getDataType().getSqlDataType(ValueType.DATUM));
        }
    }

    @Override
    protected void customReportBinding(StringBuilder buffer, String nullToken, DataValue value) {
        reportBinding(buffer, nullToken, getBindToken(getMode()), value.getDataType(), getMode().getEffectiveMode(),
                      value.getStringValue().toString());
    }
}
