/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.strategies.statement;

import java.sql.Statement;

import oracle.dbtools.raptor.datatypes.BindContext;
import oracle.dbtools.raptor.datatypes.BindingMode;
import oracle.dbtools.raptor.datatypes.DataBinding;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypePlatformException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.PLSQLBoundBlockBuilder;
import oracle.dbtools.raptor.datatypes.BindingStrategy;

import oracle.jdbc.OracleCallableStatement;

/**
 * @author Administrator
 *
 */
public abstract class StatementBindingBase<S extends Statement, P extends DataBinding> implements BindingStrategy<S,P> {
    private final static String FMT = "%1$-15s %2$-15s %3$-6s %4$-40s\n"; //$NON-NLS-1$

    private final P param;
    private final BindingMode mode;
    private final DataType dataType;
    
    private final BindContext<S> context;
    private String byPositionBindToken;
    private String byNameBindToken;
    
    protected StatementBindingBase(BindContext<S> context, P param, DataType dataType, BindingMode mode) {
        this.param = param;
        this.dataType = dataType;
        this.mode = mode;
        
        this.context = context;
        this.byPositionBindToken = null;
        this.byNameBindToken = null;
    }

    @Override
    public void setByNameBindToken(String bindToken) {
        this.byNameBindToken = bindToken;
    }

    @Override
    public String getByNameBindToken() {
        return this.byNameBindToken;
    }

    @Override
    public void setByPositionBindToken(String bindToken) {
        this.byPositionBindToken = bindToken;
    }

    @Override
    public String getByPositionBindToken() {
        if (this.byPositionBindToken == null) {
            this.byPositionBindToken = context.nextBindToken();
        }

        return this.byPositionBindToken;
    }
    
    protected int remapPosition(String bindToken) {
        int index = context.remapPosition(bindToken);
        if (index == -1) {
            index = context.remapPosition(getByPositionBindToken());
        }
        
        return index;
    }

    @Override
    public BindContext<S> getBindContext() {
        return context;
    }

    @Override
    public final P getParameter() {
        return param;
    }

    @Override
    public final DataType getDataType() {
        return dataType;
    }

    @Override
    public final BindingMode getMode() {
        return mode;
    }

    @Override
    public final S getStatement() {
      return getBindContext().getStatement();
    }

    protected final void setStatement(S stmt) {
      try {
          getBindContext().setStatement(stmt);
      } catch (ClassCastException e) {
          throw new DataTypePlatformException(OracleCallableStatement.class);
      }
    }

    public final void reportBinding(StringBuilder buffer, DataValue value) {
        customReportBinding(buffer, value);
    }

    public final void reportBinding(StringBuilder buffer, String nullToken, DataValue value) {
        customReportBinding(buffer, nullToken, value);
    }

    protected void customReportBinding(StringBuilder buffer, DataValue value) {
        customReportBinding(buffer, null, value);
    }

    protected abstract void customReportBinding(StringBuilder buffer, String nullToken,
                                                DataValue value);

    protected final void reportBinding(StringBuilder buffer, String nullToken, String bindToken,
                                       DataType dataType, BindingMode mode, String value) {
        String debugValue =
            (mode == BindingMode.OUT) ? nullToken : ((value == null) ? nullToken : value);

        if (bindToken != null) {
            String inOut = mode.toString();
            buffer.append(String.format(FMT, bindToken, dataType.getPlainDataTypeString(), inOut,
                                        debugValue));
        }
    }

    public boolean isSupported(BindingMode mode) {
        return getDataType().isSupported();
    }

    public final PLSQLBoundBlockBuilder getBuilder() {
        return getBuilder(new PLSQLBoundBlockBuilder());
    }

    public final PLSQLBoundBlockBuilder getBuilder(PLSQLBoundBlockBuilder builder) {
        return customBuilder(builder);
    }
    
    protected abstract PLSQLBoundBlockBuilder customBuilder(PLSQLBoundBlockBuilder builder);


    @Override
    public void uncacheInput() {
        getBindContext().uncacheInput(this);
    }

    @Override
    public void uncacheOutput() {
        getBindContext().uncacheOutput(this);
    }

    @Override
    public void uncacheValues() {
        getBindContext().uncacheValues(this);
    }
}
