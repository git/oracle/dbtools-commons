/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.util;

/**
 * Implementation of a String Value allowing more 
 * flexible sizing.
 * 
 * This class wraps String and adds a maximum length so the 
 * string length is less that the max length then the string
 * has been truncated.
 */
public class StringValue implements CharSequence {
    protected final String value;
    protected final long maxLength;

    public StringValue(CharSequence value, long maxLen) {
        this.value = (value == null) ? null : value.toString();
        this.maxLength = maxLen;
    }

    public StringValue(CharSequence value) {
        this(value, (value == null) ? -1 : value.length());
    }

    public StringValue() {
        this(null, -1);
    }

    public String toString() {
        return value;
    }

    public int getLength() {
        return ((value == null) ? -1 : value.length());
    }

    public long getMaxLength() {
        return maxLength;
    }
    
    public boolean isTruncated() {
        return (getLength() < getMaxLength());
    }

    public boolean isNull() {
        return (value == null);
    }

    // CharSequence implementation
    public int length() {
        return value.length();
    }

    public char charAt(int index) {
        return value.charAt(index);
    }

    public CharSequence subSequence(int start, int end) {
        return new StringValue(value.subSequence(start, end), maxLength);
    }

    public boolean equals(Object obj) {
        if (obj instanceof StringValue) {
            StringValue objStringValue = (StringValue)obj;

            return  maxLength == objStringValue.maxLength &&
                    areEquals(value, objStringValue.value);
        } else {
            return false;
        }
    }

    private static boolean areEquals(Object obj1, Object obj2) {
        return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
    }
}
