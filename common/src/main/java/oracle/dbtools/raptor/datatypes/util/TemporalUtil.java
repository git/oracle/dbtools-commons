/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.util;

import java.sql.Timestamp;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

import java.time.format.DateTimeFormatter;

import java.time.format.DateTimeParseException;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.datatypes.objects.OraDATE;
import oracle.dbtools.raptor.datatypes.objects.OraINTERVALDS;
import oracle.dbtools.raptor.datatypes.objects.OraINTERVALYM;
import oracle.dbtools.raptor.datatypes.objects.OraTIMESTAMP;
import oracle.dbtools.raptor.datatypes.objects.OraTIMESTAMPTZ;
import oracle.dbtools.raptor.datatypes.objects.OraTemporalDatum;
import oracle.dbtools.raptor.datatypes.objects.OraTemporalDatum.Resolution;

/**
 * Utility methods for converting Java epoch (<code>1970-01-01T00:00:00Z</code>)
 * based timestamps to and from textual representations
 *
 * @see http://tools.ietf.org/html/rfc3339
 * @author cdivilly
 *
 */
public abstract class TemporalUtil {
	/**
	 * Determine if a string matches the formatting of a RFC3339 timestamp.
	 *
	 * @param text
	 * @return
	 */
	public static boolean isTimestamp(final CharSequence text) {
		try {
			ZonedDateTime.parse(text, DateTimeFormatter.ISO_ZONED_DATE_TIME);
			return true;
		} catch (DateTimeParseException pe) {
			try {
				ZonedDateTime.parse(text, DateTimeFormatter.ISO_DATE_TIME.withZone(TimeZone.getDefault().toZoneId()));
				return true;
			} catch (DateTimeParseException pe2) {
				return false;
			}
		}
	}

	public static boolean isRestTimestamp(final CharSequence text) {
		try {
			ZonedDateTime.parse(text, DateTimeFormatter.ISO_ZONED_DATE_TIME);
			return true;
		} catch (DateTimeParseException pe) {
			return false;
		}
	}

	/*
	 * toRestString Implementations
	 */
	public static String toRestString(final long date) {
		return toRestString(Instant.ofEpochMilli(date));
	}

	public static String toRestString(final Timestamp timestamp) {
		return toRestString(timestamp.toInstant());
	}

	public static String toRestString(final OraTemporalDatum temporalDatum) {
		if (temporalDatum.userTimeZoneSupported()) {
			return toRestString(temporalDatum.toSessionZonedDateTime());
		} else {
			return toRestString(temporalDatum.toSessionInstant());
		}
	}

	public static String toRestString(final Instant instant) {
		return toRestString(instant.atZone(OraTemporalDatum.UTC_ID));
	}

	public static String toRestString(final ZonedDateTime zdt) {
		return toRestString(zdt.toOffsetDateTime()).replaceAll("Z\\[UTC\\]", "Z");
	}

	public static String toRestString(final OffsetDateTime odt) {
		return DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(odt);
	}

	/*
	 * toPaddedString Implementations
	 */
	public static String toPaddedString(final long date) {
		return toPaddedString(Instant.ofEpochMilli(date));
	}

	public static String toPaddedString(final Timestamp timestamp) {
		return toPaddedString(timestamp.toInstant());
	}

	public static String toPaddedString(final OraTemporalDatum temporalDatum) {
		if (temporalDatum.getResolution() == Resolution.LOCAL) {
			return toPaddedString(temporalDatum.toLocalDateTime());
		} else if (temporalDatum.userTimeZoneSupported()) {
			return toPaddedString(temporalDatum.toZonedDateTime());
		} else {
			return toPaddedString(temporalDatum.toInstant());
		}
	}

	public static String toPaddedString(final Instant instant) {
		if (instant.getNano() == 0) {
			return instant.plusNanos(777000000).toString().replaceAll("[.]777", ".000");
		} else {
			return instant.toString();
		}
	}

	public static String toPaddedString(final LocalDateTime ldt) {
		if (ldt.getNano() == 0) {
			return ldt.plusNanos(777000000).toString().replaceAll("[.]777", ".000");
		} else {
			return ldt.toString();
		}
	}

	public static String toPaddedString(final ZonedDateTime zdt) {
		if (zdt.getNano() == 0) {
			return zdt.plusNanos(777000000).toString().replaceAll("[.]777", ".000").replaceAll("Z\\[UTC\\]", "Z");
		} else {
			return zdt.toString().replaceAll("Z\\[UTC\\]", "Z");
		}
	}

	public static String toPaddedString(final OffsetDateTime odt) {
		if (odt.getNano() == 0) {
			return odt.plusNanos(777000000).toString().replaceAll("[.]777", ".000");
		} else {
			return odt.toString();
		}
	}

	/**
	 * Convert a timestamp value to RFC3339 syntax. Values are always rendered in
	 * the UTC (Z) time zone.
	 *
	 * @param date
	 * @return
	 */
	public static String toString(final long date) {
		return toString(Instant.ofEpochMilli(date));
	}

	public static String toString(final Timestamp timestamp) {
		return toString(timestamp.toInstant());
	}

	public static String toString(final OraTemporalDatum temporalDatum) {
		if (temporalDatum.getResolution() == Resolution.LOCAL) {
			return toString(temporalDatum.toLocalDateTime());
		} else if (temporalDatum.userTimeZoneSupported()) {
			return toString(temporalDatum.toZonedDateTime());
		} else {
			return toString(temporalDatum.toInstant());
		}
	}

	public static String toString(final Instant instant) {
		if (instant.getNano() == 0) {
			return instant.plusNanos(777000000).toString().replaceAll("[.]777", "");
		} else {
			return instant.toString();
		}
	}

	public static String toString(final LocalDateTime ldt) {
		if (ldt.getNano() == 0) {
			return ldt.plusNanos(777000000).toString().replaceAll("[.]777", "");
		} else {
			return ldt.toString();
		}
	}

	public static String toString(final ZonedDateTime zdt) {
		if (zdt.getNano() == 0) {
			return zdt.plusNanos(777000000).toString().replaceAll("[.]777", "").replaceAll("Z\\[UTC\\]", "Z");
		} else {
			return zdt.toString().replaceAll("Z\\[UTC\\]", "Z");
		}
	}

	public static String toString(final OffsetDateTime odt) {
		if (odt.getNano() == 0) {
			return odt.plusNanos(777000000).toString().replaceAll("[.]777", "");
		} else {
			return odt.toString();
		}
	}

	/**
	 * Parse a string formatted using RFC3339 syntax into a timestamp value
	 *
	 * @param date
	 * @return
	 */
	public static OraTemporalDatum toOraTemporalDatum(final Object obj) {
		if (obj instanceof Calendar) {
			return OraTIMESTAMPTZ.getInstance((Calendar) obj);
		} else if (obj instanceof ZonedDateTime) {
			return OraTIMESTAMPTZ.getInstance((ZonedDateTime) obj);
		} else if (obj instanceof Timestamp) {
			return OraTIMESTAMP.getInstance((Timestamp) obj);
		} else if (obj instanceof Instant) {
			return OraTIMESTAMP.getInstance((Instant) obj);
		} else if (obj instanceof LocalDateTime) {
			return OraTIMESTAMP.getInstance((LocalDateTime) obj);
		} else if (obj instanceof Date) {
			return OraDATE.getInstance((Date) obj);
		} else if (obj instanceof CharSequence) {
			final String date = obj.toString();
			final String errMsg = "Not a correctly formatted timestamp: " + date;

			try {
				try {
					ZonedDateTime zdt = ZonedDateTime.parse(date, DateTimeFormatter.ISO_ZONED_DATE_TIME);

					// Treat as Timstamp with Timezone
					return OraTIMESTAMPTZ.getInstance(zdt);
				} catch (DateTimeParseException pe) {
					LocalDateTime ldt = LocalDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME);
					// LocalDateTime ldt = LocalDateTime.parse(date,
					// DateTimeFormatter.ISO_DATE_TIME.withZone(TimeZone.getDefault().toZoneId()));

					// Treat as Timstamp
					return OraTIMESTAMP.getInstance(ldt);
				}
			} catch (Exception e) {
				throw new IllegalArgumentException(errMsg);
			}
		}

		throw new IllegalArgumentException("Unsupported temporal type: " + obj.getClass().getCanonicalName());
	}

	public static Timestamp toTimestamp(final Object obj) {
		return toOraTemporalDatum(obj).toTimestamp();
	}

	@Deprecated
	/*
	 * Use toTimestamp instead
	 */
	public static long valueOf(final String date) {
		return toTimestamp(date).getTime();
	}

	public static String ISO8601format(OraINTERVALYM interval) {
		StringBuilder sb = new StringBuilder();

		if (interval != null) {
			int sign = interval.get(OraINTERVALYM.SIGN);
			int years = interval.get(OraINTERVALYM.YEARS);
			int months = interval.get(OraINTERVALYM.MONTHS);

			if (years != 0 || months != 0) {
				if (sign == -1) {
					sb.append('-');
				}
				sb.append('P');
				if (years != 0) {
					sb.append(String.format("%d", years));
					sb.append('Y');
				}
				if (months != 0) {
					sb.append(String.format("%d", months));
					sb.append('M');
				}
			} else {
				sb.append("P0Y0M");
			}

			return sb.toString();
		} else {
			return null;
		}
	}

	public static OraINTERVALYM YM_ISO8601parse(CharSequence text) {
		OraINTERVALYM interval = OraINTERVALYM.getInstance();

		if (text != null) {
			Matcher matcher = YM_PATTERN.matcher(text);
			if (matcher.matches()) {
				boolean negate = "-".equals(matcher.group(1));
				String yearMatch = matcher.group(2);
				String monthMatch = matcher.group(3);

				int sign = (negate) ? -1 : 1;
				int years = (yearMatch != null && yearMatch.length() > 0) ? Integer.valueOf(yearMatch) : 0;
				int months = (monthMatch != null && monthMatch.length() > 0) ? Integer.valueOf(monthMatch) : 0;

				interval.set(OraINTERVALYM.SIGN, sign);
				interval.set(OraINTERVALYM.YEARS, years);
				interval.set(OraINTERVALYM.MONTHS, months);

			}
		} else {
			return null;
		}

		return interval;
	}

	public static String ISO8601format(OraINTERVALDS interval) {
		StringBuilder sb = new StringBuilder();

		if (interval != null) {
			int sign = interval.get(OraINTERVALDS.SIGN);
			int days = interval.get(OraINTERVALDS.DAYS);
			int hours = interval.get(OraINTERVALDS.HOURS);
			int minutes = interval.get(OraINTERVALDS.MINUTES);
			int seconds = interval.get(OraINTERVALDS.SECONDS);
			int nanos = interval.get(OraINTERVALDS.NANOS);

			if (days != 0 || hours != 0 || minutes != 0 || seconds != 0 || nanos != 0) {
				if (sign == -1) {
					sb.append('-');
				}
				sb.append('P');
				if (days != 0) {
					sb.append(String.format("%d", days));
					sb.append('D');
				}

				if (hours != 0 || minutes != 0 || seconds != 0 || nanos != 0) {
					sb.append('T');
					if (hours != 0) {
						sb.append(String.format("%d", hours));
						sb.append('H');
					}
					if (minutes != 0) {
						sb.append(String.format("%d", minutes));
						sb.append('M');
					}
					String frationalNanos = formatNanos(nanos);
					if (seconds != 0 || frationalNanos != null) {
						sb.append(String.format("%d", seconds));
						if (frationalNanos != null) {
							sb.append('.');
							sb.append(frationalNanos);
						}
						sb.append('S');
					}
				}
			} else {
				sb.append("P0DT0H0M0S");
			}

			return sb.toString();
		} else {
			return null;
		}
	}

	public static OraINTERVALDS DS_ISO8601parse(CharSequence text) {
		OraINTERVALDS interval = OraINTERVALDS.getInstance();

		if (text != null) {
			Matcher matcher = DS_PATTERN.matcher(text);
			if (matcher.matches()) {
				boolean negate = "-".equals(matcher.group(1));
				String daysMatch = matcher.group(2);
				/* String timeMatch = */ matcher.group(3);
				String hoursMatch = matcher.group(4);
				String minutesMatch = matcher.group(5);
				String secondsMatch = matcher.group(6);
				String nanosMatch = matcher.group(7);

				int sign = (negate) ? -1 : 1;
				int days = (daysMatch != null && daysMatch.length() > 0) ? Integer.valueOf(daysMatch) : 0;
				int hours = (hoursMatch != null && hoursMatch.length() > 0) ? Integer.valueOf(hoursMatch) : 0;
				int minutes = (minutesMatch != null && minutesMatch.length() > 0) ? Integer.valueOf(minutesMatch) : 0;
				int seconds = (secondsMatch != null && secondsMatch.length() > 0) ? Integer.valueOf(secondsMatch) : 0;
				int nanos = formatNanos(nanosMatch);

				interval.set(OraINTERVALDS.SIGN, sign);
				interval.set(OraINTERVALDS.DAYS, days);
				interval.set(OraINTERVALDS.HOURS, hours);
				interval.set(OraINTERVALDS.MINUTES, minutes);
				interval.set(OraINTERVALDS.SECONDS, seconds);
				interval.set(OraINTERVALDS.NANOS, nanos);
			}
		} else {
			return null;
		}

		return interval;
	}

	private static String formatNanos(int nanos) {
		if (nanos == 0) {
			return null;
		}

		String fractionalNanos = String.format("%09d", nanos);

		return fractionalNanos.replaceAll("0*$", "");
	}

	private static int formatNanos(String fractionalNanos) {
		if (fractionalNanos == null || fractionalNanos.length() == 0) {
			return 0;
		}

		fractionalNanos = fractionalNanos + "000000000";
		fractionalNanos = fractionalNanos.substring(0, 9);

		try {
			return Integer.parseInt(fractionalNanos);
		} catch (NumberFormatException | ArithmeticException ex) {
			return 0;
		}
	}

	/**
	 * Munged because DateFormat expects time zone to be e.g. +0800, whereas RFC
	 * 3339 expects +08:00, note the extra colon
	 */

	private static final Pattern YM_PATTERN = Pattern.compile("([-+]?)P(?:([0-9]+)Y)?(?:([0-9]+)M)?",
			Pattern.CASE_INSENSITIVE);

	private static final Pattern DS_PATTERN = Pattern.compile(
			"([-+]?)P(?:([0-9]+)D)?(T(?:([0-9]+)H)?(?:([0-9]+)M)?(?:([0-9]+)(?:[.,]([0-9]{0,9}))?S)?)?",
			Pattern.CASE_INSENSITIVE);
}
