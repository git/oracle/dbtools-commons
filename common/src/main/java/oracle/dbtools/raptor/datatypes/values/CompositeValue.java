/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.values;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.impl.DataTypeImpl;
import oracle.dbtools.raptor.datatypes.impl.DataValueImpl;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=CompositeValue.java">Barry McGillin</a> 
 *
 */
public class CompositeValue extends DataValueImpl {
    public CompositeValue(DataTypeImpl datatype) {
        super(datatype);
    }

    public CompositeValue(DataTypeImpl datatype, Object value) {
        super(datatype, value);
    }

    public CompositeValue(DataTypeImpl datatype, Collection<DataValue> internalValue) {
        super(datatype, internalValue);
    }

    @Override
    protected boolean customEquals(Object obj) {
        if (obj instanceof CompositeValue && super.customEquals(obj)) {
            CompositeValue objCompositeValue = (CompositeValue)obj;

            List<DataValue> thisComponents = this.getComponents();
            List<DataValue> objComponents = objCompositeValue.getComponents();

            return areEquals(thisComponents, objComponents);
        } else {
            return false;
        }
    }

    private boolean areEquals(List<DataValue> thisComponents, List<DataValue> thatComponents) {
        if (thisComponents != thatComponents) {
            if (thisComponents != null && thatComponents != null) {
                int thisListLength = thisComponents.size();
                if (thisListLength == thatComponents.size()) {
                    Iterator<DataValue> thatIterator = thatComponents.iterator();

                    for (DataValue thisValue : thisComponents) {
                        DataValue thatValue = thatIterator.next();

                        if (!thisValue.equals(thatValue)) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        return true;
    }

    @Override
    protected void customMarshal(DataValueMarshalHandler hd, String name) {
        customMarshalComponents(hd, name);
    }

    protected void customMarshalComponents(DataValueMarshalHandler hd, String name) {
        List<DataValue> componentValues = getComponents();

        if (componentValues != null) {
            for (DataValue componentValue : componentValues) {
                hd.marshal(componentValue, componentValue.getName());
            }
        }
    }
}
