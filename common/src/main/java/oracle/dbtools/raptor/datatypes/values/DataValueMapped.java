/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.values;

import java.util.List;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;
import oracle.dbtools.raptor.datatypes.util.StringValue;

/**
 * 
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=DataValueMapped.java"
 *         >Barry McGillin</a>
 * 
 * 
 */
public class DataValueMapped implements DataValue {
	private final DataValue typedDataValue;
	private final DataValue bindingDataValue;

	public DataValueMapped(DataValue typedDataValue, DataValue bindingDataValue) {
		this.typedDataValue = typedDataValue;
		this.bindingDataValue = bindingDataValue;
	}

	/**
 * 
 */
	@Override
	public final StringValue getStringValue() {
		return typedDataValue.getStringValue();
	}

	/**
 * 
 */
	@Override
	public final StringValue getStringValue(int maxLen) {
		return typedDataValue.getStringValue(maxLen);
	}

	/**
 * 
 */
	@Override
	public final StringValue getStringValue(StringType stringType) {
		return typedDataValue.getStringValue(stringType);
	}

	/**
 * 
 */
	@Override
	public final StringValue getStringValue(StringType stringType, int maxLen) {
		return typedDataValue.getStringValue(stringType, maxLen);
	}

	/**
 * 
 */
	@Override
	public final StringValue getStringValue(DataTypeConnectionProvider connectionProvider, StringType stringType) {
		return getStringValue(connectionProvider, stringType, -1);
	}

	/**
    *                                                                                                                
    */
	@Override
	public final StringValue getStringValue(DataTypeConnectionProvider connectionProvider, StringType stringType,
	        int maxLen) {
		return typedDataValue.getStringValue(connectionProvider, stringType, maxLen);
	}

	/**
     * 
     */
	@Override
	public final Object getTypedValue() {
		return typedDataValue.getTypedValue();
	}

	/**
 * 
 */
	@Override
	public final Object getTypedValue(ValueType valueType) {
		switch (valueType) {
		case JDBC:
		case DATUM:
			return bindingDataValue.getTypedValue(valueType);
		default:
			return typedDataValue.getTypedValue(valueType);
		}
	}

	/**
 * 
 */
	@Override
	public final Object getTypedValue(Object target) {
		return bindingDataValue.getTypedValue(target);
	}

	/**
 * 
 */
	@Override
	public final Object getTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
		return getTypedValue(connectionProvider, valueType, null);
	}

	@Override
	public final Object getTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target) {
		switch (valueType) {
		case TARGET:
		case JDBC:
		case DATUM:
			return bindingDataValue.getTypedValue(connectionProvider, valueType, target);
		default:
			return typedDataValue.getTypedValue(connectionProvider, valueType, target);
		}
	}

	public final DataValue getTypingDataValue() {
		return typedDataValue;
	}

	public final DataValue getTypingDataValueRecursive() {
		if (typedDataValue instanceof DataValueMapped) {
			return ((DataValueMapped) typedDataValue).getTypingDataValueRecursive();
		} else {
			return typedDataValue;
		}
	}

	public final DataValue getBindingDataValue() {
		return bindingDataValue;
	}

	@Override
	public String toString() {
		return typedDataValue.toString();
	}

	@Override
	public final boolean isNull() {
		return typedDataValue.isNull();
	}

    @Override
    public final void marshal(DataValueMarshalHandler hd, String name) {
        hd.marshal(typedDataValue, name);
    }

    @Override
    public final void marshal(DataValueMarshalHandler hd) {
        hd.marshal(typedDataValue);
    }

	@Override
	public final boolean isSupported() {
		return typedDataValue.isSupported();
	}

	@Override
	public final List<DataValue> getComponents() {
		return typedDataValue.getComponents();
	}

	@Override
	public final String getName() {
		return typedDataValue.getName();
	}

	@Override
	public final DataType getDataType() {
		return typedDataValue.getDataType();
	}

	@Override
	public boolean equals(Object obj) {
		boolean isEquals = (this == obj);

		if (!isEquals) {
			if (obj instanceof NamedDataValue) {
				isEquals = obj.equals(this);
			} else if (obj instanceof DataValueMapped) {
				isEquals = customEquals(obj);
			}
		}

		return isEquals;
	}

	protected boolean customEquals(Object obj) {
		if (obj instanceof DataValueMapped) {
			DataValueMapped objMappedValue = (DataValueMapped) obj;

			return areEquals(typedDataValue, objMappedValue.typedDataValue)
			        && areEquals(bindingDataValue, objMappedValue.bindingDataValue);
		} else {
			return false;
		}
	}

	private static boolean areEquals(Object obj1, Object obj2) {
		return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
	}
}
