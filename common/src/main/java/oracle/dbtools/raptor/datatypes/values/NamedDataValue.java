/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.values;

import java.util.List;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeConnectionProvider;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.NamedValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;
import oracle.dbtools.raptor.datatypes.util.StringValue;
/**
 * 
 * 
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=NamedDataValue.java">Barry McGillin</a> 
 *
 */
public class NamedDataValue extends NamedValue<DataValue> implements DataValue {

    private NamedDataValue(String name, DataValue dataValue) {
        super(name, getUnnamedDataValue(dataValue));
    }

    public static DataValue getNamedDataValue(String name, DataValue datavalue) {
        
        if (areEquals(name, datavalue.getName())) {
            // Reuse datavalue if already name
            return datavalue;
        } else if (name == null) {
            return getUnnamedDataValue(datavalue);
        } else {
            // Otherwise Rename the value
            return new NamedDataValue(name, datavalue);
        }
    }
    
    @Override
    public final StringValue getStringValue() {
        return value.getStringValue();
    }

    @Override
    public final StringValue getStringValue(int maxLen) {
        return value.getStringValue(maxLen);
    }

    @Override
    public final StringValue getStringValue(StringType stringType) {
        return value.getStringValue(stringType);
    }

    @Override
    public final StringValue getStringValue(StringType stringType, int maxLen) {
        return value.getStringValue(stringType, maxLen);
    }

    @Override
    public final StringValue getStringValue(DataTypeConnectionProvider connectionProvider, StringType stringType) {
        return getStringValue(connectionProvider, stringType, -1);
    }
                                                                                                                  
    @Override
    public final StringValue getStringValue(DataTypeConnectionProvider connectionProvider, StringType stringType, int maxLen) {
        return value.getStringValue(connectionProvider, stringType, maxLen);
    }
    
    @Override
    public final Object getTypedValue() {
        return value.getTypedValue();
    }

    @Override
    public final Object getTypedValue(ValueType valueType) {
        return value.getTypedValue(valueType);
    }

    @Override
    public final Object getTypedValue(Object target) {
        return value.getTypedValue(target);
    }

    @Override
    public final Object getTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType) {
        return getTypedValue(connectionProvider, valueType, null);
    }

    @Override
    public final Object getTypedValue(DataTypeConnectionProvider connectionProvider, ValueType valueType, Object target) {
        return value.getTypedValue(connectionProvider, valueType, target);
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public final boolean isNull() {
        return value.isNull();
    }

    @Override
    public final void marshal(DataValueMarshalHandler hd, String name) {
        value.marshal(hd, name);
    }

    @Override
    public final void marshal(DataValueMarshalHandler hd) {
        hd.marshal(value, getName());
    }

    @Override
    public final boolean isSupported() {
        return value.isSupported();
    }

    @Override
    public final List<DataValue> getComponents() {
        return value.getComponents();
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final DataType getDataType() {
        return value.getDataType();
    }

    @Override
    public DataValue getBindingDataValue() {
        return value.getBindingDataValue();
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEquals = (this == obj);

        if (!isEquals && obj instanceof DataValue) {
            DataValue objValue = (DataValue)obj;
            DataValue unnamedObj = getUnnamedDataValue(objValue);
            DataValue unnamedValue = getUnnamedDataValue(value);
            
            isEquals = areEquals(unnamedValue, unnamedObj);
        }

        return isEquals;
    }
    
    private static DataValue getUnnamedDataValue(DataValue datavalue) {
        DataValue ret = datavalue;
        
        while (ret instanceof NamedDataValue) {
            NamedDataValue retDatavalue = (NamedDataValue)ret;
            ret = retDatavalue.value;
        }
        
        return ret;
    }
    
    private static boolean areEquals(Object obj1, Object obj2) {
        return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
    }
}
