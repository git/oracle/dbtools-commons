/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.xml;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;

import java.io.Writer;

import java.util.Map;

import javax.xml.transform.sax.TransformerHandler;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeMarshallingException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;

import oracle.dbtools.raptor.utils.DataTypesUtil;

import oracle.dbtools.util.encoding.BASE64Encoding;
import oracle.dbtools.util.encoding.EncodingException;
import oracle.dbtools.util.encoding.EncodingFactory;
import oracle.dbtools.util.encoding.EncodingType;
import oracle.dbtools.util.encoding.MimeType;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
/**
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=DataValueXMLMarshalHandler.java">Barry McGillin</a>
 *
 */
public class DataValueXMLMarshalHandler implements DataValueMarshalHandler {
    protected final TransformerHandler hd;
    protected final Writer marshallWriter;
    
    public DataValueXMLMarshalHandler(TransformerHandler hd) {
        this.hd = hd;
        this.marshallWriter = new MarshallWriter(hd);
    }
    
    public StringType getStringType() {
        return StringType.GENERIC;
    }
    
    public void marshal(DataValue value, String name) {
        value.marshal(this, name);
    }

    public final void marshal(DataValue value) {
        value.marshal(this);
    }

    public void startDataValue(DataType datatype, String name, boolean isNull, Map<String, Object> propertyMap){
        AttributesImpl atts = new AttributesImpl();
        
        // Add Properties as Attributes
        if (propertyMap != null) {
            for (Map.Entry<String, Object> entry: propertyMap.entrySet()) {
                if (entry.getValue() != null) {
                    XMLUtil.setAttribute(atts, entry.getKey(), entry.getValue().toString());
                }
            }
        }
        
        // Add Null Attribute
        if (isNull) {
            XMLUtil.markNull(atts);
        }
        
        // Add Name Attribute
        if (name != null) {
            XMLUtil.setName(atts, name);
        }
        
        try {
            XMLUtil.startDataValueElement(hd, atts);
        } catch (SAXException e) {
            throw new DataTypeMarshallingException(name, datatype, e);
        }
    }
    
    public void startDataValue(DataType datatype, String name, boolean isNull){
        startDataValue(datatype, name, isNull, null);
    }
    
    @Deprecated
    public void bodyDataValue(DataType datatype, String name, String value){
        if (value != null) {
            bodyDataValue(datatype, name, value.toCharArray(), 0, value.length());
        }
    }
    
    public void bodyDataValue(DataType datatype, String name, Readable text) {
        // Create writer
        BufferedWriter writer =
            new BufferedWriter(marshallWriter, DataTypesUtil.BUFFER_SIZE);

        // Copy Characters
        try {
            DataTypesUtil.copyCharacters(text, writer);
        } catch (IOException e) {
            throw new DataTypeMarshallingException(name, datatype, (e.getCause() instanceof SAXException) ? e.getCause() : e);
        }

        try {
            // Flush writer
            writer.flush();
        } catch (IOException e) {
            throw new DataTypeMarshallingException(name, datatype, e);
        }
    }
    
    public void bodyDataValue(DataType datatype, String name, InputStream bytes) {
        BASE64Encoding b64Enc =
            (BASE64Encoding)EncodingFactory.getEncoder(EncodingType.ENCODING_BASE64,
                                                       MimeType.MIME_TEXT);
        // Create writer
        BufferedWriter writer =
            new BufferedWriter(marshallWriter, DataTypesUtil.BUFFER_SIZE);

        // Encode to Base64
        try {
            b64Enc.encode(bytes, writer);
        } catch (EncodingException e) {
            throw new DataTypeMarshallingException(name, datatype, e);
        } catch (IOException e) {
            throw new DataTypeMarshallingException(name, datatype, (e.getCause() instanceof SAXException) ? e.getCause() : e);
        }

        try {
            // Flush writer
            writer.flush();
        } catch (IOException e) {
            throw new DataTypeMarshallingException(name, datatype, e);
        }
    }
    
    public void bodyDataValue(DataType datatype, String name, char ch[], int start, int length){
        if (ch != null) {
            try {
                hd.characters(ch, start, length);
            } catch (SAXException e) {
                throw new DataTypeMarshallingException(name, datatype, e);
            }
        }
    }
    
    public void endDataValue(DataType datatype, String name, boolean isNull){
        try {
            XMLUtil.endDataValueElement(hd);
        } catch (SAXException e) {
            throw new DataTypeMarshallingException(name, datatype, e);
        }
    }

    protected class MarshallWriter extends Writer {
        public MarshallWriter(TransformerHandler hd) {
            super(hd);
        }
        
        public void write(char cbuf[], int off, int len) throws IOException {
            try {
                ((TransformerHandler)lock).characters(cbuf, off, len);
            } catch (RuntimeException re) {
                throw re;
            } catch (Exception e) {
                throw new IOException(e);
            }
        }

        public void flush() throws IOException {
            ; // No action required
        }

        public void close() throws IOException {
            ; // No action required
        }
    }
}
