/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.datatypes.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.marshal.DataValueMarshalHandler;

import oracle.dbtools.raptor.datatypes.metadata.MetadataUtil;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryXMLSupport;

import oracle.sql.CLOB;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * 
 * 
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=XMLUtil.java"
 *         >Barry McGillin</a>
 * 
 */
public abstract class XMLUtil {
	public final static String XML_IS_NULL_ATTR_TAG = "is_null"; //$NON-NLS-1$

	public final static String XML_NULL = ""; //$NON-NLS-1$

	public final static String XML_CDATA = "CDATA"; //$NON-NLS-1$

	public final static String XML_NAME_ATT = "name"; //$NON-NLS-1$

	public final static String XML_DATAVALUE_ELEMENT = "DataValue"; //$NON-NLS-1$

	public final static String XML_ENCODING = "UTF-8"; //$NON-NLS-1$

	/**
	 * 
	 * isMarkedNull
	 * 
	 * @param is_null
	 * @return XMLUtil
	 */
	public static boolean isMarkedNull(String is_null) {
		Boolean marked;

		try {
			if (is_null != null && is_null.length() > 0) {
				// Parse Attribue
				marked = Boolean.parseBoolean(is_null);
			} else {
				marked = null;
			}
		} catch (Exception e) {
			// Assume Not null
			marked = null;
		}

		return (marked == null) ? false : marked;
	}

	/**
	 * 
	 * setAttribute
	 * 
	 * @param atts
	 * @param qualifiedName
	 * @param value
	 *            XMLUtil
	 */
	public static void setAttribute(AttributesImpl atts, String qualifiedName, String value) {
		atts.addAttribute(XML_NULL, XML_NULL, qualifiedName, XML_CDATA, value);
	}

	/**
	 * 
	 * getAttribute
	 * 
	 * @param atts
	 * @param qualifiedName
	 * @return XMLUtil
	 */
	public static String getAttribute(Attributes atts, String qualifiedName) {
		return atts.getValue(qualifiedName);
	}

	public static void markNull(AttributesImpl atts) {
		setAttribute(atts, XML_IS_NULL_ATTR_TAG, Boolean.TRUE.toString());
	}

	public static void markNull(AttributesImpl atts, boolean flag) {
		setAttribute(atts, XML_IS_NULL_ATTR_TAG, Boolean.valueOf(flag).toString());
	}

	public static void markNotNull(AttributesImpl atts) {
		setAttribute(atts, XML_IS_NULL_ATTR_TAG, Boolean.FALSE.toString());
	}

	public static boolean isMarkedNull(Attributes atts) {
		// Get Attribute
		String is_null = getAttribute(atts, XML_IS_NULL_ATTR_TAG);

		return isMarkedNull(is_null);
	}

	public static void setName(AttributesImpl atts, String value) {
		if (value != null) {
			setAttribute(atts, XML_NAME_ATT, value);
		}
	}

	public static String getName(Attributes atts) {
		return getAttribute(atts, XML_NAME_ATT);
	}

	public static void startDataValueElement(TransformerHandler hd, AttributesImpl atts) throws SAXException {
		hd.startElement(XML_NULL, XML_NULL, XML_DATAVALUE_ELEMENT, atts);
	}

	public static void endDataValueElement(TransformerHandler hd) throws SAXException {
		hd.endElement(XML_NULL, XML_NULL, XML_DATAVALUE_ELEMENT);
	}

	public static void marshallDataValue(TransformerHandler th, DataValue value)
	        throws TransformerConfigurationException, SAXException {
		DataValueMarshalHandler hd = new DataValueXMLMarshalHandler(th);

		hd.marshal(value);
	}

	public static void marshallDataValue(TransformerHandler th, DataValue value, String name) {
		DataValueMarshalHandler hd = new DataValueXMLMarshalHandler(th);

		hd.marshal(value, name);
	}

	private static void marshallDataValue(OutputStream os, DataValue value, String name, boolean useName)
	        throws TransformerConfigurationException, SAXException {
		try {
			// SAX2.0 Complient.
			StreamResult streamResult = new StreamResult(os);
			SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();

			TransformerHandler th = tf.newTransformerHandler();

			Transformer serializer = th.getTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING, XML_ENCODING);
			serializer.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$
			th.setResult(streamResult);

			DataValueMarshalHandler hd = new DataValueXMLMarshalHandler(th);

			th.startDocument();

			try {
				if (useName) {
					hd.marshal(value, name);
				} else {
					hd.marshal(value);
				}
			} finally {
				try {
					th.endDocument();
				} catch (Exception e) {
					Logger.getLogger(XMLUtil.class.getName()).log(Level.WARNING, e.getLocalizedMessage(), e);
				}
			}
		} finally {
			try {
				os.flush();
			} catch (IOException e) {
				Logger.getLogger(XMLUtil.class.getName()).log(Level.WARNING, e.getLocalizedMessage(), e);
			}
		}
	}

	public static void marshallDataValue(OutputStream os, DataValue value, String name)
	        throws TransformerConfigurationException, SAXException {
		marshallDataValue(os, value, name, true);
	}

	public static void marshallDataValue(OutputStream os, DataValue value) throws TransformerConfigurationException,
	        SAXException {
		marshallDataValue(os, value, null, false);
	}

	public static Clob marshallDataValue(Connection conn, DataValue value, String name)
	        throws TransformerConfigurationException, SAXException, SQLException {
		Clob clob = getClobLocator(conn);
		marshallDataValue(clob, value, name);
		return clob;
	}

	public static Clob marshallDataValue(Connection conn, DataValue value) throws TransformerConfigurationException,
	        SAXException, SQLException {
		Clob clob = getClobLocator(conn);
		marshallDataValue(clob, value);
		return clob;
	}

	public static void marshallDataValue(Clob clob, DataValue value, String name)
	        throws TransformerConfigurationException, SAXException, SQLException {
		// Bug 9342438: LINUX: LOSES THE INPUT PARAMETERS FOR IMPLEMENTATIONS
		// SQLException if there is an error accessing the <code>CLOB</code>
		// value or if pos is less than 1
		// Starting with 11.2 jdbc driver, this is actually enforced
		OutputStream os = clob.setAsciiStream(1);
		marshallDataValue(os, value, name);
	}

	public static void marshallDataValue(Clob clob, DataValue value) throws TransformerConfigurationException,
	        SAXException, SQLException {
		OutputStream os = clob.setAsciiStream(1);
		marshallDataValue(os, value);
	}

	private static DataValue unmarshallDataValue(InputStream is, DataType datatype, String name, boolean useName)
	        throws SAXException, IOException {
		XMLReader xr = XMLReaderFactory.createXMLReader();
		DataValueXMLUnmarshalHandler handler = (useName) ? new DataValueXMLUnmarshalHandler(datatype, name)
		        : new DataValueXMLUnmarshalHandler(datatype);
		xr.setContentHandler(handler);
		xr.setErrorHandler(handler);

		xr.parse(new InputSource(is));

		return handler.getDataValue();
	}

	public static DataValue unmarshallDataValue(InputStream is, DataType datatype, String name) throws SAXException,
	        IOException {
		return unmarshallDataValue(is, datatype, name, true);
	}

	public static DataValue unmarshallDataValue(InputStream is, DataType datatype) throws SAXException, IOException {
		return unmarshallDataValue(is, datatype, null, false);
	}

	public static DataValue unmarshallDataValue(Clob clob, DataType datatype, String name) throws SAXException,
	        IOException, SQLException {
		return unmarshallDataValue(clob.getAsciiStream(), datatype, name, true);
	}

	public static DataValue unmarshallDataValue(Clob clob, DataType datatype) throws SAXException, IOException,
	        SQLException {
		return unmarshallDataValue(clob.getAsciiStream(), datatype, null, false);
	}

	private static Clob getClobLocator(Connection conn) throws SQLException {
		return CLOB.createTemporary(conn, false, CLOB.DURATION_SESSION);
	}

	public static String getQuery(Connection conn, String id) {
		return MetadataUtil.getQuery(conn, id);
	}

}
