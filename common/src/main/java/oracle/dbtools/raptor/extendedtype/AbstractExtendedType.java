/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.extendedtype;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.sql.SQLException;

import java.text.MessageFormat;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.stream.FileImageInputStream;

import oracle.dbtools.raptor.backgroundTask.RaptorTaskDescriptor;
import oracle.dbtools.raptor.utils.ReaderInputStream;

//import oracle.ide.net.URLFileSystem;

/**
 * Implement Abstract Extended Type
 */
public abstract class AbstractExtendedType implements ExtendedType<Object> {

    protected final int sqlType;
    private Object origValue;
    private boolean isNULL;
    private URL loadFrom;
    private RaptorTaskDescriptor toDiskTaskDescriptor;

    /**
     * Constract Abstract Extended Type
     * 
     * @param sqlType
     * @param origValue
     */
    protected AbstractExtendedType(int sqlType, Object origValue) {
        this(sqlType, origValue, (origValue == null));
    }

    /**
     * Constract Abstract Extended Type
     * 
     * @param sqlType
     * @param origValue
     * @param isNULL
     */
    protected AbstractExtendedType(int sqlType, Object origValue, boolean isNULL) {
        this.sqlType = sqlType;
        this.origValue = origValue;
        this.isNULL = isNULL;
        this.loadFrom = null;
        this.toDiskTaskDescriptor = null;
    }
    
    protected void setOrigValue(Object origValue, boolean isNULL) {
        this.origValue = origValue;
        this.isNULL = isNULL;
        loadFrom = null;
        this.toDiskTaskDescriptor = null;
    }

    protected void setOrigValue(Object origValue) {
        setOrigValue(origValue, (origValue == null));
    }

    public Object getOrigValue() {
        return this.origValue;
    }

    @Override
    public void setNULL(boolean isNULL) {
        this.isNULL  = isNULL;
        if (this.isNULL) {
            this.loadFrom = null;
        }
    }

    @Override
    public boolean isSetNULL() {
        return this.isNULL;
    }

    @Override
    public final URL getLoadFromURL() {
        return this.loadFrom;
    }

    @Override
    public void setLoadFromURL(URL loadFrom) {
        this.loadFrom = loadFrom;
        if (loadFrom != null) {
            setNULL(false);
        }
    }
    
    @Override
    public final Reader getReader(String defaultCharset) throws Exception {
        return getReader(true, defaultCharset);
    }
    
    @Override
    public final Reader getReader(Boolean orig) throws Exception {
        return getReader(orig, null);
    }
    
    @Override
    public final Reader getReader() throws Exception {
        return getReader(true, null);
    }
    
    @Override
    public Reader getReader(Boolean orig, String defaultCharset) throws Exception {
        InputStream in = null;
        
        if ((orig == null && loadFrom != null) || (orig != null && orig.booleanValue() == false)) {
            if (loadFrom != null) {
            //    in = URLFileSystem.openInputStream(loadFrom);
            }
        } else {
            in = getBinaryStream(); 
        }
          
        if  (in != null) {
            if (defaultCharset != null) {
                return new InputStreamReader(in, defaultCharset);
            } else {
                return new InputStreamReader(in);
            }
        } else {
            return null;
        }
    }

    @Override
    public final InputStream getBinaryStream() throws SQLException {
        try {
            return getBinaryStream(true);
        } catch (SQLException se) {
            throw se;
        } catch (RuntimeException re) {
            throw re;
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
            return null;
        }
    }
    
    @Override
    public InputStream getBinaryStream(Boolean orig) throws Exception {
        InputStream in = null;
        
        if ((orig == null && loadFrom != null) || (orig != null && orig.booleanValue() == false)) {
            if (loadFrom != null) {
            	//TODO: Verify this is correct
            	in = new FileInputStream(new File(loadFrom.toURI())); 
                //in = URLFileSystem.openInputStream(loadFrom);
            	
            }
        } else {
            ; // Object type unknown
        }
        
        return in;
    }

    @Override
    public void setToDiskTaskDescriptor(RaptorTaskDescriptor taskDescriptor) {
        this.toDiskTaskDescriptor = taskDescriptor;
    }

    @Override
    public RaptorTaskDescriptor getToDiskTaskDescriptor() {
        return this.toDiskTaskDescriptor;
    }

    @Override
    public boolean isToDiskInDeterministic() {
        return true;
    }
    
    protected void setToDiskTaskProgress(RaptorTaskDescriptor taskDescriptor, int percentage) {
        taskDescriptor.setProgress(percentage);
        taskDescriptor.setMessage(MessageFormat.format(Messages.getString("GenericBlobType.1"), new Object[] { percentage }));
    }


}
