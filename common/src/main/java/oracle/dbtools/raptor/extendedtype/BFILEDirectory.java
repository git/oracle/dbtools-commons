/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.extendedtype;

import oracle.dbtools.raptor.datatypes.objects.OraIntervalDatum;
import oracle.dbtools.raptor.utils.DataTypesUtil;

/**
 * Implement a Directory Alias
 * 
 * Adds supplementary owner and physical path
 */
public class BFILEDirectory implements Comparable<BFILEDirectory>, Cloneable{
    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private String owner;
    private String name;
    private String path;

    /**
     * Creates a Directory Alias
     * 
     * @param owner
     * @param name
     * @param path
     */
    public BFILEDirectory(String owner, String name, String path) {
        this.owner = owner;
        this.name = name;
        this.path = path;
    }

    /**
     * Creates a Directory Alias
     * 
     * @param name
     */
    public BFILEDirectory(String name) {
        this(null, name, null);
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public String getPath() {
        return path;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (obj.getClass() != this.getClass()) return false;
        
        BFILEDirectory another = (BFILEDirectory)obj;
        
        return DataTypesUtil.areEqual(name, another.name);
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = hash * 31 + ((name != null) ? name.hashCode() : 0);
        return hash;
    }
    
    @Override
    public int compareTo(BFILEDirectory another) {
        int ret = 0;
        
        if (this != another) {
            if (this.name == null && another.name != null){
                ret = -1;
            }
            else if (this.name != null && another.name == null){
                ret = 1;
            } else {
                ret = this.name.compareTo(another.name);
            }
        }
        
        return ret;
    }
    
    @Override
    public Object clone()
    {
        try {
            BFILEDirectory other = (BFILEDirectory) super.clone();
            other.owner = owner;
            other.name = name;
            other.path = path;
            
            return other;
        }
        catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError();
        }
    }
}