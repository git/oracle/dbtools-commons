/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.extendedtype;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;

import java.math.BigDecimal;

import java.net.MalformedURLException;
import java.net.URL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.backgroundTask.RaptorTaskDescriptor;

import oracle.jdbc.OracleTypes;
import oracle.jdbc.driver.OracleConnection;

import oracle.sql.BFILE;
import oracle.sql.BfileDBAccess;

/**
 * Implements a BFILE wrapper support changed and null values
 */
public class BFILEType extends AbstractExtendedType {

	private OutputStream out;
	private BFILEDirectory dirAlias_new;
	private String fileName_new;

    protected BFILEType(int sqlType, BFILE origValue) {
        super(sqlType, origValue);
        out = null;
        dirAlias_new = null;
        fileName_new = null;
    }

    /**
     * Construct Wrapper from BFILE
     * 
     * @param bfile
     */
    public BFILEType(BFILE bfile) {
        this(OracleTypes.BFILE, bfile);
	}

    /**
     * Get Original BFILE
     * 
     * @return
     */
    public BFILE getOrigBFile() {
		return getOrigValue();
	}

    /**
     * Get Original BFILE
     * 
     * @return
     */
    public BFILE getOrigValue() {
        return (BFILE)super.getOrigValue();
    }

    /**
     * Get New Directory Alias
     * 
     * @return
     */
    public BFILEDirectory getDirAlias_new() {
		return dirAlias_new;
	}

    /**
     * Set New Directory Alias
     * 
     * @param dirAlias_new
     */
    public void setDirAlias_new(BFILEDirectory dirAlias_new) {
		this.dirAlias_new = dirAlias_new;
	    setLoadFromURL(this.dirAlias_new, this.fileName_new);
	}

    /**
     * Get New Terminal File Name
     * 
     * @return
     */
    public String getFileName_new() {
		return fileName_new;
	}

    /**
     * Set New Terminal File Name
     * 
     * @param fileName_new
     */
    public void setFileName_new(String fileName_new) {
		this.fileName_new = fileName_new;
	    setLoadFromURL(this.dirAlias_new, this.fileName_new);
	}

    /**
     * Create new BFILE from Directory Alas and File Name.
     * 
     * Overwrites original BFILE
     * 
     * @param conn
     * @param tableName
     * @param columnName
     * @param rowId
     * @param rowScn
     * @param dataIntegrity
     * @param log
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public boolean saveToDb(Connection conn, String tableName,
			String columnName, String rowId, String rowScn,
			boolean dataIntegrity, List<String> log) throws SQLException, IOException {
		if(dirAlias_new != null && fileName_new != null) {
		    CallableStatement stmt = null;
            PreparedStatement prep = null;
            try {
			StringBuffer insertSql = new StringBuffer("BEGIN UPDATE "+tableName+"SET "+columnName+"=bfilename('"+dirAlias_new+"','"+ //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					fileName_new+"')"+" WHERE ROWID=:sqldevrowid"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			if (dataIntegrity && rowScn!=null)
				insertSql.append(" AND ORA_ROWSCN=:sqldevrowscn"); //$NON-NLS-1$ //$NON-NLS-2$
			insertSql.append(" RETURNING ROWID INTO ?; END;"); //$NON-NLS-1$
			log.add(insertSql.toString());
			stmt = conn.prepareCall(insertSql.toString());
			int i  = 0;
            stmt.setString(++i, rowId);
            if (rowScn != null && dataIntegrity) stmt.setString(++i, rowScn);
            stmt.registerOutParameter(++i, Types.CHAR);
			int updates = stmt.executeUpdate();
			if (updates == 1) {
				StringBuffer selectSql = new StringBuffer("SELECT "+columnName+" FROM "+tableName+" WHERE ROWID=:sqldevrowid"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				if (dataIntegrity && rowScn!=null)
					selectSql.append(" AND ORA_ROWSCN=:sqldevrowscn"); //$NON-NLS-1$ //$NON-NLS-2$
				prep  = conn.prepareStatement(selectSql.toString());
				prep.setString(1,rowId);
	            if (rowScn != null && dataIntegrity) prep.setString(2, rowScn);
				ResultSet rs = prep.executeQuery();
				if (rs.next()) {
					setOrigValue(rs.getObject(1));
				}					
				return true;
			}
			else
				return false;
            } finally {
                try {
                    if (prep != null) prep.close();
                    if (stmt != null) stmt.close();
                } catch (Exception e) {}
            }
		}
		return false;
	}

    /**
     * Save original file to local disk.
     * 
     * @param tableName
     * @param columnName
     * @param rowId
     * @param rowScn
     * @param dataIntegrity
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public boolean saveToDisk(String tableName, String columnName,
			String rowId, String rowScn, boolean dataIntegrity)
			throws SQLException, IOException {
	    final OutputStream out = this.out;
	    final RaptorTaskDescriptor taskDescriptor = getToDiskTaskDescriptor();
	    
	    if (taskDescriptor != null) {
	        setToDiskTaskProgress(taskDescriptor, 0);
	    }
	    
        BFILE origBFile = getOrigValue();
		if (origBFile != null && out != null) {
			InputStream blobStream = null;
			try {
			    long expectedLength = origBFile.getLength();
				origBFile.openFile();
				blobStream = origBFile.getBinaryStream();
				if (blobStream != null && expectedLength > 0L) {
                    long written = 0L;
				    blobStream = new BufferedInputStream(blobStream, 16 * 1024);
				    byte[] buffer = new byte[8 * 1024];
				    int nbytes = 0; 
				    while((out == this.out) && 
                          (taskDescriptor == null || !taskDescriptor.isCancelled()) &&
                          (nbytes = blobStream.read(buffer)) != -1) { 
				        out.write(buffer, 0, nbytes); 
                        written = written + nbytes;
                        if (taskDescriptor != null) {
                          final int percentage = (int)((written * 100)/expectedLength);
                          setToDiskTaskProgress(taskDescriptor, percentage);
                        }
                    }
	    	        if (taskDescriptor != null) {
                        setToDiskTaskProgress(taskDescriptor, 100);
                    }
				    
                    return (out == this.out);
				} 
			} finally {
			    try {
				    if (blobStream!=null) blobStream.close();
			    } catch (Exception e) {
			        ; // ignore
			    }
			    try {
				    if (origBFile!=null) origBFile.closeFile();
			    } catch (Exception e) {
			        ; // ignore
			    }
			    try {
				    if (out != null) out.close();
			    } catch (Exception e) {
			        ; // ignore
			    }
			}
		}
        
        return false;
	}
    
    @Override
    public boolean isToDiskInDeterministic() {
        return false;
    }

	public void setLoadFromURL(URL url) {
        if (url == null) {
            setDirAlias_new(null);
            setFileName_new(null);
        } else {
            // ignore
        }
	}

    protected void setSuperLoadFromURL(URL url) {
        super.setLoadFromURL(url);
    }

	public void setToDiskStream(OutputStream out) {
		this.out=out;
	}
	
	@Override
	public String toString() {
		
		return "(BFILE)"; //$NON-NLS-1$
	}

    
	public InputStream asciiStreamValue() throws SQLException {
		return getOrigValue().asciiStreamValue();
	}

	public BigDecimal bigDecimalValue() throws SQLException {
		return getOrigValue().bigDecimalValue();
	}

	public InputStream binaryStreamValue() throws SQLException {
		return getOrigValue().binaryStreamValue();
	}

	public boolean booleanValue() throws SQLException {
		return getOrigValue().booleanValue();
	}

	public byte byteValue() throws SQLException {
		return getOrigValue().byteValue();
	}

	public Reader characterStreamValue() throws SQLException {
		return getOrigValue().characterStreamValue();
	}

	public void close() throws SQLException {
		getOrigValue().close();
	}

	public void closeFile() throws SQLException {
		getOrigValue().closeFile();
	}

	public Date dateValue() throws SQLException {
		return getOrigValue().dateValue();
	}

	public double doubleValue() throws SQLException {
		return getOrigValue().doubleValue();
	}

	public boolean equals(Object arg0) {
		return this==arg0;
	}

	public boolean fileExists() throws SQLException {
		return getOrigValue().fileExists();
	}

	public float floatValue() throws SQLException {
		return getOrigValue().floatValue();
	}

	public InputStream getBinaryStream(Boolean orig) throws Exception {
	    if ((orig == null && getLoadFromURL() != null) || (orig != null && orig.booleanValue() == false)) {
            return super.getBinaryStream(orig);
	    } else if (getOrigValue() != null) {
            return getOrigValue().getBinaryStream();
        }
        
        return null;
	}

	public InputStream getBinaryStream(long arg0) throws SQLException {
		return getOrigValue().getBinaryStream(arg0);
	}

	public byte[] getBytes() {
		return getOrigValue().getBytes();
	}

	public int getBytes(long arg0, int arg1, byte[] arg2) throws SQLException {
		return getOrigValue().getBytes(arg0, arg1, arg2);
	}

	public byte[] getBytes(long arg0, int arg1) throws SQLException {
		return getOrigValue().getBytes(arg0, arg1);
	}

	@SuppressWarnings("deprecation")
    public OracleConnection getConnection() throws SQLException {
		return getOrigValue().getConnection();
	}

	public BfileDBAccess getDBAccess() throws SQLException {
		return getOrigValue().getDBAccess();
	}

	public String getDirAlias() throws SQLException {
		return getOrigValue().getDirAlias();
	}

	public oracle.jdbc.internal.OracleConnection getInternalConnection() throws SQLException {
		return getOrigValue().getInternalConnection();
	}

	public Connection getJavaSqlConnection() throws SQLException {
		return getOrigValue().getJavaSqlConnection();
	}

	public long getLength() {
		return getOrigValue().getLength();
	}

	public byte[] getLocator() {
		return getOrigValue().getLocator();
	}

	public String getName() throws SQLException {
		return getOrigValue().getName();
	}

	public oracle.jdbc.OracleConnection getOracleConnection() throws SQLException {
		return getOrigValue().getOracleConnection();
	}

	public InputStream getStream() {
		return getOrigValue().getStream();
	}

	public int hashCode() {
		return getOrigValue().hashCode();
	}

	public int intValue() throws SQLException {
		return getOrigValue().intValue();
	}

	public boolean isConvertibleTo(Class arg0) {
		return getOrigValue().isConvertibleTo(arg0);
	}

	public boolean isFileOpen() throws SQLException {
		return getOrigValue().isFileOpen();
	}

	public boolean isOpen() throws SQLException {
		return getOrigValue().isOpen();
	}

	public long length() throws SQLException {
		return getOrigValue().length();
	}

	public long longValue() throws SQLException {
		return getOrigValue().longValue();
	}

	public Object makeJdbcArray(int arg0) {
		return getOrigValue().makeJdbcArray(arg0);
	}

	public void open() throws SQLException {
		getOrigValue().open();
	}

	public void open(int arg0) throws SQLException {
		getOrigValue().open(arg0);
	}

	public void openFile() throws SQLException {
		getOrigValue().openFile();
	}

	public long position(BFILE arg0, long arg1) throws SQLException {
		return getOrigValue().position(arg0, arg1);
	}

	public long position(byte[] arg0, long arg1) throws SQLException {
		return getOrigValue().position(arg0, arg1);
	}

	public void setBytes(byte[] arg0) {
		getOrigValue().setBytes(arg0);
	}

	public void setLocator(byte[] arg0) {
		getOrigValue().setLocator(arg0);
	}

	public void setPhysicalConnectionOf(Connection arg0) {
		getOrigValue().setPhysicalConnectionOf(arg0);
	}

	public void setShareBytes(byte[] arg0) {
		getOrigValue().setShareBytes(arg0);
	}

	public byte[] shareBytes() {
		return getOrigValue().shareBytes();
	}

	public String stringValue() throws SQLException {
		return getOrigValue().stringValue();
	}

	public Timestamp timestampValue() throws SQLException {
		return getOrigValue().timestampValue();
	}

	public Time timeValue() throws SQLException {
		return getOrigValue().timeValue();
	}

	public Object toJdbc() throws SQLException {
		return getOrigValue().toJdbc();
	}

	public InputStream getCharStream() throws SQLException {
		return asciiStreamValue();
	}

	public void duplicate() throws SQLException {
		/*if (origBFile!=null) {
			dirAlias_new=origBFile.getDirAlias();
			fileName_new=origBFile.getName();
		}*/
	}

    public void setLoadFromURL(BFILEDirectory dirAlias, String fileName) {
        try {
            if (dirAlias != null && dirAlias.getPath() != null && fileName != null) {
                setSuperLoadFromURL(new File(dirAlias.getPath()+File.separator+fileName).toURI().toURL());
                return;
            }
        } catch (MalformedURLException e) {
        }
        setSuperLoadFromURL(null);
    }
}
