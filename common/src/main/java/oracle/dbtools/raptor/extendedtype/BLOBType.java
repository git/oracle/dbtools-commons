/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.extendedtype;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;

import java.math.BigDecimal;

import java.net.URL;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;

import java.util.List;

import oracle.dbtools.raptor.utils.DataTypesUtil;

import oracle.jdbc.OracleTypes;
import oracle.jdbc.driver.OracleConnection;

import oracle.sql.BLOB;
import oracle.sql.BlobDBAccess;

/**
 * Implements BLOB Type delegate
 *
 * The actual blob contents are never copied into memory. Only handles to the
 * blob are managed
 */
@SuppressWarnings("deprecation")
public class BLOBType extends GenericBlobType {

    protected BLOBType(int sqlType, BLOB origValue) {
        super(sqlType, origValue);
    }

    /**
     * Construct delegate from Oracle BLOB
     * 
     * @param blob
     */
    public BLOBType(BLOB blob) {
        this(OracleTypes.BLOB, blob);
    }

    public BLOBType() {
        this(null);
    }

    public BLOB getOrigValue() {
        return (BLOB)super.getOrigValue();
    }

    /**
     * Get Original BLOB
     * 
     * @return
     */
    public BLOB getOrigBlob() {
        return getOrigValue();
    }

    /**
     * Save Blob content to database
     * 
     * @param conn
     * @param tableName
     * @param columnName
     * @param rowId
     * @param rowScn
     * @param dataIntegrity
     * @param log
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public boolean saveToDb(Connection conn, String tableName, String columnName, String rowId, String rowScn, boolean dataIntegrity, List<String> log) throws SQLException,
            IOException {
        PreparedStatement prep = null;
        URL loadFromURL = getLoadFromURL();
        if (loadFromURL != null) {
            in = loadFromURL.openStream();
            if (in != null) {
                BLOB blob = DataTypesUtil.getTemporaryBLOB(conn);
                try {
                    OutputStream blobOutputStream = null;
                    try {
                        // blob.open(BLOB.MODE_READWRITE);
                        blob.truncate(0L);
                        blobOutputStream = blob.setBinaryStream(1);
                        int bufferSize = blob.getBufferSize();
                        bufferSize = (bufferSize > 0) ? bufferSize : 10 * 1024;
                        byte[] buffer = new byte[ bufferSize ];
                        int nread = -1;
                        while ((nread = in.read(buffer)) != -1)
                            blobOutputStream.write(buffer, 0, nread);
                        blobOutputStream.flush();
                        
                        StringBuffer sqlBuff = new StringBuffer();
                        sqlBuff.append("UPDATE ").append(tableName).append(" SET ").append(columnName).append("=? WHERE ROWID=:sqldevrowid"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                        if (dataIntegrity && rowScn != null) {
                            sqlBuff.append(" AND ORA_ROWSCN=:sqldevrowscn"); //$NON-NLS-1$ //$NON-NLS-2$
                        }
                        log.add(sqlBuff.toString());
                        prep = conn.prepareStatement(sqlBuff.toString());
                        
                        prep.setObject(1, blob);
                        prep.setString(2, rowId);
                        if (dataIntegrity && rowScn != null)  prep.setString(3, rowScn);
                        int updates = prep.executeUpdate();
                        loadFromURL = null;
                        return updates == 1;
                    } finally {
                        in.close();
                        if (blobOutputStream != null) blobOutputStream.close();
                        // blob.close();
                        setOrigValue(blob);
                    }
                } finally {
                    try {
                        if (prep != null) prep.close();
                    } catch (Exception e) {}
                }
            }
        } else {
            try {
                StringBuffer insertEmptySql = new StringBuffer();
                String value = isSetNULL()?"= NULL":"= EMPTY_BLOB()";
                insertEmptySql.append("UPDATE " + tableName + " SET " + columnName + value + " WHERE ROWID=:sqldevrowid"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
                if (dataIntegrity && rowScn != null) insertEmptySql.append(" AND ORA_ROWSCN=:sqldevrowscn"); //$NON-NLS-1$ //$NON-NLS-2$
                log.add(insertEmptySql.toString());
                prep = conn.prepareStatement(insertEmptySql.toString());
                int i =0;
                prep.setString(++i, rowId);
                if (dataIntegrity && rowScn != null) prep.setString(++i, rowScn);
                int updates = prep.executeUpdate();
                if (updates == 1)
                    return true;
                else
                    return false;
            } finally {
                try {
                    if (prep != null) prep.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * Get ROWSCN for trget database row
     * 
     * @param conn
     * @param tableName
     * @param rowId
     * @return
     */
    private String selectRowscn(Connection conn, String tableName, String rowId) {
        String selectRowscn = "SELECT ORA_ROWSCN FROM " + tableName + // NO RES //$NON-NLS-1$
                " WHERE ROWID = ?"; // NO RES //$NON-NLS-1$
        ResultSet rs = null;
        PreparedStatement prep2 = null;
        try {
            prep2 = conn.prepareStatement(selectRowscn);
            prep2.setString(1, rowId);

            rs = prep2.executeQuery();
            if (!rs.next()) {
                return rs.getString(1);
            }

        } catch (SQLException ex) {

        } finally {
            try {
                if (rs != null) rs.close();
            } catch (SQLException ex) {
            }
            try {
                if (prep2 != null) prep2.close();
            } catch (SQLException ex) {
            }
        }
        return null;
    }

    public void setLoadFromURL(URL url) {
        setSuperLoadFromURL(url);
    }

    public InputStream asciiStreamValue() throws SQLException {
        return getOrigValue().asciiStreamValue();
    }

    public BigDecimal bigDecimalValue() throws SQLException {
        return  getOrigValue().bigDecimalValue();
    }

    public InputStream binaryStreamValue() throws SQLException {
        return  getOrigValue().binaryStreamValue();
    }

    public boolean booleanValue() throws SQLException {
        return  getOrigValue().booleanValue();
    }

    public byte byteValue() throws SQLException {
        return  getOrigValue().byteValue();
    }

    public Reader characterStreamValue() throws SQLException {
        return  getOrigValue().characterStreamValue();
    }

    public void close() throws SQLException {
         getOrigValue().close();
    }

    public Date dateValue() throws SQLException {
        return  getOrigValue().dateValue();
    }

    public double doubleValue() throws SQLException {
        return  getOrigValue().doubleValue();
    }

    public float floatValue() throws SQLException {
        return  getOrigValue().floatValue();
    }

    public void freeTemporary() throws SQLException {
         getOrigValue().freeTemporary();
    }

    public OutputStream getBinaryOutputStream() throws SQLException {
        return  getOrigValue().getBinaryOutputStream();
    }

    public OutputStream getBinaryOutputStream(long arg0) throws SQLException {
        return  getOrigValue().getBinaryOutputStream(arg0);
    }

    public InputStream getBinaryStream(long arg0) throws SQLException {
        return  getOrigValue().getBinaryStream(arg0);
    }

    public int getBufferSize() throws SQLException {
        return  getOrigValue().getBufferSize();
    }

    public byte[] getBytes() {
        return  getOrigValue().getBytes();
    }

    public int getBytes(long arg0, int arg1, byte[] arg2) throws SQLException {
        return  getOrigValue().getBytes(arg0, arg1, arg2);
    }

    public int getChunkSize() throws SQLException {
        return  getOrigValue().getChunkSize();
    }

    public OracleConnection getConnection() throws SQLException {
        return  getOrigValue().getConnection();
    }

    public BlobDBAccess getDBAccess() throws SQLException {
        return  getOrigValue().getDBAccess();
    }

    public oracle.jdbc.internal.OracleConnection getInternalConnection() throws SQLException {
        return  getOrigValue().getInternalConnection();
    }

    public Connection getJavaSqlConnection() throws SQLException {
        return  getOrigValue().getJavaSqlConnection();
    }

    public long getLength() {
        return  getOrigValue().getLength();
    }

    public byte[] getLocator() {
        return  getOrigValue().getLocator();
    }

    public oracle.jdbc.OracleConnection getOracleConnection() throws SQLException {
        return  getOrigValue().getOracleConnection();
    }

    public InputStream getStream() {
        return  getOrigValue().getStream();
    }

    public int intValue() throws SQLException {
        return  getOrigValue().intValue();
    }

    public boolean isConvertibleTo(Class arg0) {
        return  getOrigValue().isConvertibleTo(arg0);
    }

    public boolean isEmptyLob() throws SQLException {
        return  getOrigValue().isEmptyLob();
    }

    public boolean isOpen() throws SQLException {
        return  getOrigValue().isOpen();
    }

    public boolean isTemporary() throws SQLException {
        return  getOrigValue().isTemporary();
    }

    public long longValue() throws SQLException {
        return  getOrigValue().longValue();
    }

    public Object makeJdbcArray(int arg0) {
        return  getOrigValue().makeJdbcArray(arg0);
    }

    public void open(int arg0) throws SQLException {
         getOrigValue().open(arg0);
    }

    public int putBytes(long arg0, byte[] arg1, int arg2) throws SQLException {
        return  getOrigValue().putBytes(arg0, arg1, arg2);
    }

    public int putBytes(long arg0, byte[] arg1) throws SQLException {
        return  getOrigValue().putBytes(arg0, arg1);
    }

    public void setBytes(byte[] arg0) {
         getOrigValue().setBytes(arg0);
    }

    public void setLocator(byte[] arg0) {
         getOrigValue().setLocator(arg0);
    }

    public void setPhysicalConnectionOf(Connection arg0) {
         getOrigValue().setPhysicalConnectionOf(arg0);
    }

    public void setShareBytes(byte[] arg0) {
         getOrigValue().setShareBytes(arg0);
    }

    public byte[] shareBytes() {
        return  getOrigValue().shareBytes();
    }

    public String stringValue() throws SQLException {
        return  getOrigValue().stringValue();
    }

    public Timestamp timestampValue() throws SQLException {
        return  getOrigValue().timestampValue();
    }

    public Time timeValue() throws SQLException {
        return  getOrigValue().timeValue();
    }

    public Object toJdbc() throws SQLException {
        return  getOrigValue().toJdbc();
    }

    public void trim(long arg0) throws SQLException {
         getOrigValue().trim(arg0);
    }

    public InputStream getCharStream() throws SQLException {
        return asciiStreamValue();
    }

    @Override
    public String toString() {
        return isSetNULL()?"(NULL BLOB)":"(BLOB)"; //$NON-NLS-1$
    }
}
