/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.extendedtype;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import java.net.URL;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.backgroundTask.RaptorTaskDescriptor;

import oracle.jdbc.OracleTypes;

/**
 * Implement Generic Blob Type
 */
public class GenericBlobType extends AbstractExtendedType implements Blob {

    protected InputStream in;
    protected OutputStream out;

    /**
     * Constructs Generic Blob Type
     * 
     * @param sqlType
     * @param origValue
     */
    protected GenericBlobType(int sqlType, Blob origValue) {
        super(sqlType, origValue);
        this.in = null;
        this.out = null;
    }

    public GenericBlobType() {
        this(null);
    }

    public GenericBlobType(Blob blob) {
        this(OracleTypes.BLOB, blob);
    }

    public Blob getOrigValue() {
        return (Blob)super.getOrigValue();
    }

    public Blob getOrigBlob() {
        return getOrigValue();
    }

    public void duplicate() throws SQLException {
        if (getOrigValue() != null) this.in = getOrigValue().getBinaryStream();
    }

    public InputStream getBinaryStream(Boolean orig) throws Exception {
        if ((orig == null && getLoadFromURL() != null) || (orig != null && orig.booleanValue() == false)) {
            return super.getBinaryStream(orig);
        } else if (getOrigValue() != null) {
            return getOrigValue().getBinaryStream();
        }
        
        return null;
    }

    public InputStream getCharStream() throws SQLException {
        return null;
    }

    /**
     * Save to Database
     * 
     * @param conn
     * @param tableName
     * @param columnName
     * @param rowId
     * @param rowScn
     * @param dataIntegrity
     * @param log
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public boolean saveToDb(Connection conn, String tableName, String columnName, String rowId, String rowScn, boolean dataIntegrity, List<String> log) throws SQLException, IOException {
        return false;
    }

    /**
     * Save To Disk
     * 
     * @param tableName
     * @param columnName
     * @param rowId
     * @param rowScn
     * @param dataIntegrity
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public boolean saveToDisk(String tableName, String columnName, String rowId, String rowScn, boolean dataIntegrity) throws SQLException, IOException {
        final OutputStream out = this.out;
        final RaptorTaskDescriptor taskDescriptor = getToDiskTaskDescriptor();
        
        if (taskDescriptor != null) {
            setToDiskTaskProgress(taskDescriptor, 0);
        }
        
        Blob origBlob = getOrigValue();
        if (origBlob != null && out != null) {
            InputStream blobStream = null;
            try {
                long expectedLength = origBlob.length();
                blobStream = origBlob.getBinaryStream();
                
                if (blobStream != null && expectedLength > 0L) {
                    long written = 0L;
                    blobStream = new BufferedInputStream(blobStream, 16 * 1024);
                    byte[] buffer = new byte[8 * 1024];
                    int nbytes = 0;
                    while ((out == this.out) && 
                           (taskDescriptor == null || !taskDescriptor.isCancelled()) && 
                           (nbytes = blobStream.read(buffer)) != -1) {
                        out.write(buffer, 0, nbytes);
                        written = written + nbytes;
                        if (taskDescriptor != null) {
                            final int percentage = (int)((written * 100)/expectedLength);
                            setToDiskTaskProgress(taskDescriptor, percentage);
                        }
                    }
                    
                    if (taskDescriptor != null) {
                        setToDiskTaskProgress(taskDescriptor, 100);
                    }
                    
                    return (out == this.out);
                }
            } finally {
                try {
                    out.close();
                } catch (Exception e) {
                    ; // ignore
                }
                try {
                    if (blobStream != null) blobStream.close();
                } catch (Exception e) {
                    ; // ignore
                }
            }
        }
        
        return false;
    }
    
    @Override
    public boolean isToDiskInDeterministic() {
        return false;
    }

    public void setLoadFromURL(URL url) {
        if (url == null) {
            setSuperLoadFromURL(null);
        } else {
            // ignore
        }
    }

    protected void setSuperLoadFromURL(URL url) {
        super.setLoadFromURL(url);
    }

    public void setToDiskStream(OutputStream out) {
      if (out == null || out instanceof BufferedOutputStream) {
        this.out = out;
      } else {
        this.out = new BufferedOutputStream(out, 64 * 1024);
      }
    }

    public byte[] getBytes(long pos, int length) throws SQLException {
        return getOrigValue().getBytes(pos, length);
    }

    public long length() throws SQLException {
        return getOrigValue().length();
    }

    public long position(byte[] pattern, long start) throws SQLException {
        return getOrigValue().position(pattern, start);
    }

    public long position(Blob pattern, long start) throws SQLException {
        return getOrigValue().position(pattern, start);
    }

    public OutputStream setBinaryStream(long pos) throws SQLException {
        return getOrigValue().setBinaryStream(pos);
    }

    public int setBytes(long pos, byte[] bytes) throws SQLException {
        return getOrigValue().setBytes(pos, bytes);
    }

    public int setBytes(long pos, byte[] bytes, int offset, int len) throws SQLException {
        return getOrigValue().setBytes(pos, bytes, offset, len);
    }

    public void truncate(long len) throws SQLException {
        getOrigValue().truncate(len);
    }

    @Override
    public boolean equals(Object o) {
        return o == this;
    }

    public int hashCode() {
        return getOrigValue().hashCode();
    }

    @Override
    public String toString() {
        return "(Blob)"; //$NON-NLS-1$
    }

    public void free() throws SQLException {
    }

    public InputStream getBinaryStream(long pos, long length) throws SQLException {
        return null;
    }
}
