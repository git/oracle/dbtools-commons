/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.extendedtype;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.URL;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;

import oracle.jdbc.OracleTypes;

import oracle.sql.RAW;

/**
 * Implements RAW Type
 */
public class RAWType extends AbstractExtendedType {

	protected RAWType(int sqlType, RAW origValue) {
        super(sqlType, origValue);
	}

    public RAWType(RAW raw) {
        this(OracleTypes.RAW, raw);
    }
    
    @Override
    public RAW getOrigValue() {
        return (RAW)super.getOrigValue();
    }

    /**
     * Save to Database
     * 
     * @param conn
     * @param tableName
     * @param columnName
     * @param rowId
     * @param rowScn
     * @param dataIntegrity
     * @param log
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public boolean saveToDb(Connection conn, String tableName,
			String columnName, String rowId, String rowScn,
			boolean dataIntegrity, List<String> log) throws SQLException, IOException {
		
		return false;
	}

    /**
     * Save to Disk
     * 
     * @param tableName
     * @param columnName
     * @param rowId
     * @param rowScn
     * @param dataIntegrity
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public boolean saveToDisk(String tableName, String columnName,
			String rowId, String rowScn, boolean dataIntegrity)
			throws SQLException, IOException {
		
		return false;
	}

	public void setLoadFromURL(URL url) {
        if (url == null) {
            setSuperLoadFromURL(null);
        } else {
            // ignore
        }
	}

    protected void setSuperLoadFromURL(URL url) {
        super.setLoadFromURL(url);
    }

	public void setToDiskStream(OutputStream out) {
		

	}

	public InputStream getBinaryStream(Boolean orig) throws Exception {
		return null;
	}

	public InputStream getCharStream() throws SQLException {
		return getOrigValue().asciiStreamValue();
	}

	public void duplicate() throws SQLException {
		
		
	}
}
