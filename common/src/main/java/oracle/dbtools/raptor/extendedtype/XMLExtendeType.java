/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.extendedtype;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.stream.XMLStreamException;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.jdbc.OracleTypes;
import oracle.sql.CLOB;
import oracle.sql.OPAQUE;
import oracle.xdb.XMLType;

/**
 * Implements XML Extended Type
 */
public class XMLExtendeType extends AbstractExtendedType {

    /**
     * Implement Tempory Clob Close Reader
     */
    protected static class TemporaryClobReader extends BufferedReader {
        private CLOB clob;
        
        public TemporaryClobReader(CLOB clob) throws SQLException {
            super(clob.getCharacterStream());
            this.clob = clob;
        }

        public void close() throws IOException {
            super.close();
            try {
                if (clob != null && clob.isTemporary()) {
                    clob.freeTemporary();
                }
            } catch (SQLException sq) {
                ; //ignore - unknown state
            }
        }
    }

    protected XMLExtendeType(int sqlType, OPAQUE origValue) {
        super(sqlType, origValue);
    }

    public XMLExtendeType(OPAQUE data) {
        this(OracleTypes.SQLXML, data);
    }
    
    public void setValue(OPAQUE opaque) {
        setOrigValue(opaque);
    }

    public OPAQUE getObjectValue() {
        return getOrigValue();
    }

    public OPAQUE getOrigValue() {
        return (OPAQUE)super.getOrigValue();
    }

    @Override
    public String toString() {
        try {
            URL _selectedURL = getLoadFromURL();
            if (_selectedURL != null) {
                return "[" + _selectedURL!=null?_selectedURL.toString():"" + "]";
            } else if (getOrigValue() != null) {
                return DataTypesUtil.stringValue(getOrigValue(), getOrigValue().getOracleConnection());
            }
        } catch (SQLException e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
        }
        return "";
    }

    public void duplicate() throws SQLException {
        // TODO Auto-generated method stub

    }

    /**
     * Get Binary Stream
     * 
     * @param orig
     * @return
     * @throws Exception
     */
    public InputStream getBinaryStream(Boolean orig) throws Exception {
        if ((orig == null && getLoadFromURL() != null) || (orig != null && orig.booleanValue() == false)) {
            return super.getBinaryStream(orig);
        } else if (getOrigValue() != null) {
            XMLType xmlType = XMLType.createXML(getOrigValue());

            Clob clob = xmlType.getClobVal();
            try {
                return DataTypesUtil.getXMLAsBytes(clob.getCharacterStream());
            } catch (Exception e) {
                return clob.getAsciiStream();
            }
        }
        
        return null;
    }

    public InputStream getCharStream() throws SQLException {
        return null;
    }

    /**
     * Get Reader
     * 
     * @param orig
     * @param defaultCharset
     * @return
     * @throws Exception
     */
    public Reader getReader(Boolean orig, String defaultCharset) throws Exception {
        URL loadFrom = getLoadFromURL();
        if ((orig == null && loadFrom != null) || (orig != null && orig.booleanValue() == false)) {
            if (loadFrom != null) {
                InputStream in = loadFrom.openStream();
                
                if (in != null) {
                    if (defaultCharset == null) {
                        defaultCharset = ScriptRunnerContext.ALWAYSUTF8;
                    }
                    
                    return DataTypesUtil.getXMLAsReader(in, defaultCharset);
                }
            }
        } else {
            OPAQUE opq = getOrigValue();
            
            XMLType xmlType;
            if (opq instanceof XMLType) {
                xmlType = (XMLType)opq;
            } else if (opq != null) {
                xmlType = XMLType.createXML(opq);
            } else {
                xmlType = null;
            }
            
            if (xmlType != null) {
                CLOB clob = xmlType.getClobVal();
                
                if (clob != null) {
                    return new TemporaryClobReader(clob);
                }
            }
        }
        
        return null;
    }

    /**
     * Save to Database
     * 
     * @param conn
     * @param tableName
     * @param columnName
     * @param rowId
     * @param rowScn
     * @param dataIntegrity
     * @param log
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public boolean saveToDb(Connection conn, String tableName, String columnName, String rowId, String rowScn,
            boolean dataIntegrity, List<String> log) throws SQLException, IOException {
        PreparedStatement prep = null;
        try {
            Object dbXmlType = null;
            URL _selectedURL = getLoadFromURL();
            if (_selectedURL != null) {
                // DocumentBuilder newDocumentBuilder =
                // DocumentBuilderFactory.newInstance().newDocumentBuilder();
                // Document doc = newDocumentBuilder.parse(_selectedFile);
                InputStream is = _selectedURL.openStream();
                CLOB clob = DataTypesUtil.getTemporaryCLOB(conn);
                try {
                    Writer clobWriter = clob.setCharacterStream(1L);
                    try {
                        DataTypesUtil.getXMLAsCharacters(is, clobWriter);
                        clobWriter.flush();
                        dbXmlType = XMLType.createXML(conn, clob);
                    } catch (XMLStreamException xe) {
                        try {
                            is.close();
                        } catch (IOException ie) {
                            ; //ignore
                        }
                        
                        is = _selectedURL.openStream();
                        dbXmlType = XMLType.createXML(conn, is);
                    } finally {
                        try {
                            clobWriter.close();
                        } catch (IOException ie) {
                            ; //ignore
                        }
                    }
                } finally {
                    try {
                        clob.close();
                    } catch (SQLException se) {
                        ; //ignore
                    }
                    try {
                        if (is != null) {
                            is.close();
                        }
                    } catch (IOException ie) {
                        ; //ignore
                    }
                }
            } else if (getOrigValue() != null){
                dbXmlType = XMLType.createXML(getOrigValue());
            }
            StringBuffer sqlBuff = new StringBuffer();
            sqlBuff.append("UPDATE ").append(tableName).append(" SET ").append(columnName).append("=? WHERE ROWID=:sqldevrowid"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            if (dataIntegrity && rowScn != null) {
                sqlBuff.append(" AND ORA_ROWSCN=:sqldevrowscn"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            log.add(sqlBuff.toString());
            prep = conn.prepareStatement(sqlBuff.toString());
            
            prep.setObject(1, dbXmlType);
            prep.setString(2, rowId);
            if (dataIntegrity && rowScn != null)  prep.setString(3, rowScn);
            int updates = prep.executeUpdate();
            _selectedURL = null;
            return updates == 1;
        } catch (SQLException e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
            throw e;
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
            throw e;
        } finally {
            if (prep != null) {
                prep.close();
            }
        }
    }

    /**
     * Saved to Disk
     * 
     * @param tableName
     * @param columnName
     * @param rowId
     * @param rowScn
     * @param dataIntegrity
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public boolean saveToDisk(String tableName, String columnName, String rowId, String rowScn, boolean dataIntegrity)
            throws SQLException, IOException {
        return false;
    }

    public void setToDiskStream(OutputStream out) {
        // TODO Auto-generated method stub
    }
}