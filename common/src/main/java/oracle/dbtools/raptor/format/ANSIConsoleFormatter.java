/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.commands.SetPause;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.dbtools.raptor.utils.IListPrinter;
import oracle.dbtools.raptor.utils.ListPrinter;
import oracle.dbtools.raptor.utils.NLSUtils;
import oracle.dbtools.util.Logger;
import oracle.sql.NUMBER;

public class ANSIConsoleFormatter extends ResultsFormatter {
  public static final String TYPE = "ansiconsole"; //$NON-NLS-1$

  private List<List<?>> data = new ArrayList<List<?>>();
  private ArrayList<String> lastRow = null;
  private ArrayList<String> currRow = null;
  private ArrayList<String> colNames;
  private ArrayList<Boolean> numberCol = new ArrayList<Boolean>();

  boolean hasData=false;
  private static IListPrinter _listPrinter = new ListPrinter();

  private int _totalRows = 0;
  
  public ANSIConsoleFormatter() {
    super(TYPE, null, ".log"); //$NON-NLS-1$
  }

  public void start() throws IOException {
    // clear out the old data could be still populated if an error happened
    data.clear();
    lastRow=null;
    hasData=false;
    _totalRows=0;
    
    colNames = new ArrayList<String>();
    for(int i=0;i<this.getColumnCount();i++){
      colNames.add(this.getColumnName(i));
      numberCol.add( (this.getDataType(i) == Types.NUMERIC) );

    }
  }

  public void startRow() throws IOException {
    lastRow = currRow;
    currRow = new ArrayList<String>();
  }

  public void printColumn(Object col, int viewIndex, int modelIndex) throws IOException {
    hasData=true;
    // need to conver to format
    if ( col != null && col instanceof NUMBER ){
      try {
        col = ((NUMBER)col).bigDecimalValue();
      } catch (SQLException e) {
        //
        //
      }
    }
    
    // use nls to format according to the user's session
    String val = null;
    String[] formatterOptions = (String[]) getScriptContext().getProperty(ScriptRunnerContext.SQL_FORMAT_FULL);
    String numFormat = null;
    
    if ( formatterOptions != null && formatterOptions.length > 3 ){
      //numFormat = formatterOptions[3];
      String[] opts = new String[formatterOptions.length-3];
      System.arraycopy(formatterOptions, 3,opts, 0, formatterOptions.length-3);
      
      numFormat = join(Arrays.asList(opts)," ");
    }
    if ( col instanceof Number){
      if ( numFormat != null ) {
         if ( numFormat.equalsIgnoreCase("default")){
             DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance();
             formatter.applyPattern("###,###.###########################################################################");
             val = formatter.format((Number)col) ;
         } else {
           try {
               DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance();
               formatter.applyPattern(numFormat);
               val = formatter.format((Number)col) ;
           } catch (Exception e){
             val="INVALID FORMAT";
           }
         }
      }
      if ( val == null || numFormat == null ){
          val = NLSUtils.format(this.getConnection(), col);
      }
      currRow.add(val);
    } else if (col instanceof Clob) {
      Integer size=null;
      if (this.getScriptContext()!=null){
        size=(Integer)(this.getScriptContext().getProperty(ScriptRunnerContext.SETLONG));
      }
      if (size!=null) {
        val=DataTypesUtil.stringValue(col,this.getConnection(),size);
      } else {
        val=DataTypesUtil.stringValue(col,this.getConnection());
      }
      currRow.add(val);
    }  else if (col instanceof Blob) {
      Integer size=null;
      if (this.getScriptContext()!=null){
        size=(Integer)(this.getScriptContext().getProperty(ScriptRunnerContext.SETLONG));
      }
      Blob bb = (Blob) col;
      String ss = null;
      if (bb!=null) {
        byte[] bdata = null;
        try{
          //doneill: return only the part of the blob we are going to print, not the entire thing as this can cause OutOfMemoryException
          //use the full length of the blob only if the ScriptRunnerContext.SETLONG was not set	
          int printSize = size != null?size:(int) bb.length();
          bdata=bb.getBytes(1, printSize);
          ss = new String(ScriptUtils.bytesToHex(bdata)).toUpperCase();
        } catch (SQLException e) {// log at least
        	Logger.warn(this.getClass(), e);
        }
      }
      if (size!=null) {
        val=DataTypesUtil.stringValue(ss,this.getConnection(),size);
      } else {
        val=DataTypesUtil.stringValue(ss,this.getConnection());
      }
      currRow.add(val);
    } else if (col instanceof ResultSet) {
    	currRow.add("Cursor");		
    } 
    else {
      currRow.add(NLSUtils.format(this.getConnection(), col));
    }
  }

  public void endRow() throws IOException {
    data.add(data.size(),currRow);
    
    // if page size print and start over..
    int pagesize = getPageSize();
    if ( data.size() >= pagesize ){
      getOutputStream().flush();
      /* potentially 'press any key to continue' message */
      SetPause.directWriteOut(getScriptContext(), new OutputStreamWriter(getOutputStream()));
      printDataList(pagesize,"");

    }
  }

  public void end() throws IOException {
    int pagesize = getPageSize();
    if ( data!=null&&(!data.isEmpty())){
      getOutputStream().flush();
      /* potentially 'press any key to continue' message */
      SetPause.directWriteOut(getScriptContext(), new OutputStreamWriter(getOutputStream()));
    }
    printDataList(pagesize,"");
    /* get last full data row if possible */
    ArrayList<String> lastData=null;
    if ((currRow!=null)&&(!currRow.isEmpty())) {
        lastData=currRow;
    } else {
        lastData=lastRow;
    }
    /*
     * update column for 'column ... new_val ...' sqlplus command 
     * Covert last row into substitution variables.
     * - pass null for norows 
     * Note array index counts from 1 not 0. So dummy first value needed.
     */
    if ((getScriptContext()!=null)&&
            (getScriptContext().getColumnMap()!=null)&&
            (!getScriptContext().getColumnMap().isEmpty())&&
            colNames!=null) {
        ArrayList<String> colNamesPlus1=new ArrayList<String>();
        //need first column name and value to be dummy first value
        colNamesPlus1.add("");  //$NON-NLS-1$
        for (String colName:colNames) {
            colNamesPlus1.add(colName);
        }
        if (hasData==false) {
            //Handle sqlplus: column ... new_val ...
            getScriptContext().updateColumn(colNamesPlus1.toArray(new String[colNamesPlus1.size()]),
                                            null, null);
        } else {
            if ((lastData!=null)
                &&(getScriptContext()!=null)
                &&(colNames.size()==lastData.size())) {
                //need first column name and value to be blank
                ArrayList<String> lastDataPlus1=new ArrayList<String>();
                lastDataPlus1.add("");  //$NON-NLS-1$
                for (String value:lastData) {
                    lastDataPlus1.add(value);
                }
                //handle sqlplus: column ... new_val...
                getScriptContext().updateColumn(colNamesPlus1.toArray(new String[colNamesPlus1.size()]),
                                                 lastDataPlus1.toArray(new String[lastDataPlus1.size()]), null );
            }
        }
    }
    
 
    
  }

  private void printDataList(int pagesize, String nullDisplay) throws IOException {
    _totalRows = _totalRows  + data.size();
    // force the names into array item 0
    data.add(0,colNames);
   _listPrinter.print(getOutputStream(), this.getConnection(),data,pagesize,nullDisplay,numberCol);
    // clear it out after
    data.clear();
  }

  private int getPageSize() {
    // grab the pagesize from the script context
    int pagesize =0;
    try { 
      pagesize = (Integer) getScriptContext().getProperty(ScriptRunnerContext.SETPAGESIZE);
    } catch(Exception e){
      // default to 0 on any error
    }
    return pagesize;
  }

  @Override
  public boolean allowsLobs() {
    return true;
  }

  @Override
  public void setTableName(String tName) {
    // not needed
  }


  @Override
  public boolean isInternalOnly() {
    // this will hide it from drop lists
    return true;
  }

  public static String join  (List<String> s, String delimiter)
  {
    if (s == null || s.isEmpty()) return "";
    Iterator<String> iter = s.iterator();
    StringBuilder builder = new StringBuilder(iter.next());
    while( iter.hasNext() ) {
      builder.append(delimiter).append(iter.next());
    }
    return builder.toString();
  }
  
  static public void setListPrinter(IListPrinter listprinter){
  	_listPrinter = listprinter;
  }
  public Boolean isCandidateForSpoolMax() {
	  return true;
  }
}
