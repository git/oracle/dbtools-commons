/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.sql.Clob;
import java.sql.Types;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.dbtools.raptor.utils.NLSUtils;

/**
 *
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=DelimitedFormatter.java"
 *         >Barry McGillin</a>
 *
 */
public class DelimitedFormatter extends ResultsFormatter {
  public static final String TYPE = "Delimited"; //$NON-NLS-1$

  public static final String EXT = "dsv"; //$NON-NLS-1$

  public static final String DEFAULT_DELIMITER = ","; //$NON-NLS-1$

  public static final String KEY_HEADER = "EXPORT_DLM_HEADER";

  public static final String KEY_DELIMITER = "EXPORT_DLM_DELIMITER"; //$NON-NLS-1$

  public static final String KEY_REC_TERM = "EXPORT_DLM_REC_TERM"; //$NON-NLS-1$

  public static final String KEY_ENCLOSURES = "EXPORT_DLM_ENCLOSURES"; //$NON-NLS-1$

  public static final String KEY_ENCL_LEFT = "EXPORT_DLM_ENCL_LEFT"; //$NON-NLS-1$

  public static final String KEY_ENCL_RIGHT = "EXPORT_DLM_ENCL_RIGHT"; //$NON-NLS-1$

  public static final String KEY_ENCL_RIGHT_DOUBLE = "EXPORT_DLM_ENCL_RIGHT_DOUBLE"; //$NON-NLS-1$

  private static String s_delim = DEFAULT_DELIMITER;

  private boolean _isHeader;
  
  private ArrayList<String> m_colnames = null;
  
  private ArrayList<String> m_currRow = null;

  public DelimitedFormatter() {
    super(TYPE, Messages.getString("FixedFormatter.4"), EXT); //$NON-NLS-1$ 
  }

  public DelimitedFormatter(String type, String filter, String ext) {
    super(type, filter, ext); // pass from extenders
  }

  public void start() throws IOException {
	  
		m_colnames = new ArrayList<String>();
		for (int i = 0; i < getColumnCount(); i++) {
			m_colnames.add(getColumnName(i));
		}

	  if (getScriptContext()!=null){
		  String[] formatterOptions = (String[]) getScriptContext().getProperty(ScriptRunnerContext.SQL_FORMAT_FULL);
		  if ( formatterOptions!= null && formatterOptions.length >3 ){
			  _delimiter = formatterOptions[3];
		  }
		  if ( formatterOptions!= null && formatterOptions.length >4 ){
			  _enclosureLeft = formatterOptions[4];
		  }
		  if ( formatterOptions!= null && formatterOptions.length >5 ){
			  _enclosureRight = formatterOptions[5];
		  }
	  }
	  if (_delimiter == null) {
		  _delimiter = s_delim;
	  }
	  String enclosureLeft = _isEnclosed ? _enclosureLeft : "";
	  String enclosureRight = _isEnclosed ? _enclosureRight : "";

	  if (isHeader()) {
		  int size = getColumnCount();
		  for (int i = 0; i < size; i++) {
			  String name = getColumnName(i);
			  if (i < size - 1) {
				  write(enclosureLeft + name + enclosureRight + _delimiter); //$NON-NLS-1$ //$NON-NLS-2$

			  } else {
				  write(enclosureLeft + name + enclosureRight); //$NON-NLS-1$ //$NON-NLS-2$
			  }
		  }
		  write(getLineTerminator()); //$NON-NLS-1$
	  }
  }

  public void startRow() throws IOException {
	  m_currRow = new ArrayList<String>();
  }

  public void printColumn(Object col, int viewIndex, int modelIndex) throws IOException {
    String colm = ""; //$NON-NLS-1$
    String enclosureLeft = _enclosureLeft;
    String enclosureRight = _enclosureRight;
    String delimiter = _delimiter == null ? s_delim : _delimiter;
    int type = this.getDataType(viewIndex);
    boolean isEnclosureRightDouble = false;

    if ((type == Types.CHAR && _isEnclosed) || (type == Types.LONGNVARCHAR) || (type == Types.LONGVARCHAR) || (type == Types.NCHAR) || (type == Types.NVARCHAR) || (type == Types.VARCHAR)
        || (type == Types.CLOB)) {
      if (_isEnclosureRightDoubled) {
        isEnclosureRightDouble = true;
      }
    } else {
      enclosureLeft = "";
      enclosureRight = "";
    }

    
    Object val = (type == Types.BLOB)?"(BLOB)":NLSUtils.format(this.getConnection(), col);//  retValue(col);
    if (val != null) {
      //colm = delimiter + enclosureLeft + getValue(col).toString().replaceAll("\"", "\"\"") + enclosureRight; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
      if (col != null) {
        String value = isEnclosureRightDouble ? enclosureRightDouble(val.toString()) : val.toString();
        colm = delimiter + enclosureLeft + value + enclosureRight;
      } else {
        enclosureLeft = "";
        enclosureRight = "";
        colm = delimiter + val.toString();
      }
      m_currRow.add((String)val);
    } else {
      colm = delimiter + enclosureLeft + enclosureRight; //$NON-NLS-1$

    }

    if (viewIndex == 0) {
      write(enclosureLeft + ((enclosureLeft.trim().length() > 0) ? colm.substring(delimiter.length() + 1) : colm.substring(delimiter.length()))); //$NON-NLS-1$

    } else {
      write(colm);
    }

  }

  public void endRow() throws IOException {
    write(getLineTerminator());
  }

  public void end() throws IOException {
		ArrayList<String> lastData = null;
		if ((m_currRow != null) && (!m_currRow.isEmpty())) {
			lastData = new ArrayList<String>();
			lastData.add("");
			for(int i = 0; i < m_currRow.size(); i++) {
				lastData.add(m_currRow.get(i));
			}
		}
		
		if ((getScriptContext() != null) && (getScriptContext().getColumnMap() != null) && 
			(!getScriptContext().getColumnMap().isEmpty()) && m_colnames != null && lastData != null) {
            ArrayList<String> columns = new ArrayList<String>();
            columns.add("");
            for(int i = 0; i < m_colnames.size(); i++) {
            	columns.add(m_colnames.get(i));
			}
            
			if ((lastData != null) && (getScriptContext() != null) && (columns.size() == lastData.size())) {
				// handle sqlplus: column ... new_val...
				getScriptContext().updateColumn(columns.toArray(new String[columns.size()]), lastData.toArray(new String[lastData.size()]), null);
			}
		}
		
		
  }

  public void setTableName(String tName) {
  }

  @Override
  public boolean allowsLobs() {
    return true;
  }

  public boolean isHeaderOptionSupported() {
    return true;
  }

  public boolean isLineTerminatorSupported() {
    return true;
  }

  public boolean isDelimiterSupported() {
    return true;
  }

  public boolean isDelimiterConfigurable() {
    return true;
  }

  public boolean isEnclosuresSupported() {
    return true;
  }

  public void isHeader(boolean isHeader) {
    _isHeader = isHeader;
  }

  public boolean isHeader() {
    return _isHeader;
  }

  public String getHeaderConfigKey() {
    return KEY_HEADER;
  }

  public String getLineTerminatorConfigKey() {
    return KEY_REC_TERM;
  }

  public String getDelimiterConfigKey() {
    return KEY_DELIMITER;
  }

  public String getEnclosuresConfigKey() {
    return KEY_ENCLOSURES;
  }

  public String getEnclosureLeftConfigKey() {
    return KEY_ENCL_LEFT;
  }

  public String getEnclosureRightConfigKey() {
    return KEY_ENCL_RIGHT;
  }

  public String getEnclosureRightDoubleConfigKey() {
    return KEY_ENCL_RIGHT_DOUBLE;
  }

  public String getLineTerminator() {
    if (_EOLChars == null) {
      super.getLineTerminator();
    }
    return _EOLChars;
  }
  public Boolean isCandidateForSpoolMax() {
	  return true;
  }
}
