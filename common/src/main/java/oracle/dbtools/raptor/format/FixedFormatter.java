/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.sql.Clob;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.utils.DataTypesUtil;

/**
 * @author Barry McGillin
 * 
 *
 */
public class FixedFormatter extends ResultsFormatter {
  public static final String TYPE = "fixed"; //$NON-NLS-1$
  public static final String EXT = "txt"; //$NON-NLS-1$
  public static final String KEY_HEADER = "EXPORT_FIX_HEADER"; //$NON-NLS-1$
  public static final String KEY_REC_TERM = "EXPORT_FIX_REC_TERM"; //$NON-NLS-1$
	
  private final static int FIXED_PAD_LENGTH = 30;	
  private boolean _isHeader;
  /**
   * Fixed format Constructor
   */
  public FixedFormatter() {
    super(TYPE, Messages.getString("FixedFormatter.1"), EXT); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  }

  /**
   * @see oracle.dbtools.raptor.format.IResultFormatter#end()
   */
  
  public void end() throws IOException {}

  /**
   * @see oracle.dbtools.raptor.format.IResultFormatter#endRow()
   */
  
  public void endRow() throws IOException {
    write(getLineTerminator());
  }

  /* (non-Javadoc)
   * @see oracle.dbtools.raptor.format.IResultFormatter#printColumn(java.lang.Object, int, int)
   */
  
  public void printColumn(Object col, int viewIndex, int modelIndex) throws IOException {
	// Get Value
	Object val;
	if (col instanceof Clob) {
		Integer size=null;
		if (this.getScriptContext()!=null){
			size=(Integer)(this.getScriptContext().getProperty(ScriptRunnerContext.SETLONG));
		}
		if (size!=null) {
			val=DataTypesUtil.stringValue(col,this.getConnection(),size);
		} else {
			val=DataTypesUtil.stringValue(col,this.getConnection());
		}
	} else {
		val = getValue(col);
	}
	
	
	String colm;
	if (col == null && val != null) {	
	        colm = val.toString();
	} else {
	        String cleanString = (val != null) ? cleanString(val.toString()) : "";
	        colm = "\""+ cleanString +"\"";  //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	// pad (== width) should always be a non-negative number.
	write(rightpad( colm, FIXED_PAD_LENGTH ));
  }

  /**
   * @see oracle.dbtools.raptor.format.IResultFormatter#setTableName(java.lang.String)
   */
  
  public void setTableName(String name) {}

  /**
   * @see oracle.dbtools.raptor.format.IResultFormatter#start()
   */
  
  public void start() throws IOException {
    int pad = FIXED_PAD_LENGTH ;
	int columnCount = getColumnCount();
	if (isHeader()){
		for (int i = 0; i < columnCount; i++) {
			String cleanString = cleanString(getColumnName( i ));
			int cleanStringLength = cleanString.length();    
			String name = "\""+ cleanString +"\"";
			write( rightpad(name, pad )); //$NON-NLS-1$ //$NON-NLS-2$
		}
		write(getLineTerminator());
	}
  }

   private String rightpad(final String name, final int pad) {	
    String returnStr = (name != null)? name.trim() : name;
    String formatStr = "%1$-" + pad + "s";
    if ( (name != null) /*&& ( !name.trim().equals("") && ( !name.trim().equals("\"\"") ))*/ ) {
    	returnStr = String.format( formatStr, name);  //$NON-NLS-1$ //$NON-NLS-2$
    } else {
    		returnStr = String.format( formatStr, "\"\"");
    }
    return returnStr;
  }

  /**
   * @see oracle.dbtools.raptor.format.IResultFormatter#startRow()
   */
  
  public void startRow() throws IOException {}
  
  @Override
  public boolean allowsLobs() {
      return true;
  }
  public boolean isHeaderOptionSupported(){
  	return true;
  }
  public boolean isLineTerminatorSupported(){
		return true;
  }
  public void isHeader (boolean isHeader){
  	_isHeader = isHeader;
  }
  public boolean isHeader(){
  	return _isHeader;
  }
  public String getHeaderConfigKey(){
  	return KEY_HEADER;
  }
  public String getLineTerminatorConfigKey(){
  	return KEY_REC_TERM;
  }
  public Boolean isCandidateForSpoolMax() {
	  return true;
  }
}
