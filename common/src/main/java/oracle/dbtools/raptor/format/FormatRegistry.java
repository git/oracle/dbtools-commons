/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * The format registry is a registry where we can register formatters
 * <code>IResultFormatter</code>
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=FormatRegistry.java"
 *         >Barry McGillin</a>
 * 
 */
public class FormatRegistry {

	// private static List<IResultFormatter> _registry = new
	// ArrayList<IResultFormatter>();
	private static Map<String, Class> s_registry = new TreeMap<String, Class>();
	private static ArrayList<String> s_internalOnly = new ArrayList<String>();
	private static String m_lineTerminator = System.getProperty("line.terminator");

	// register new formatters
	synchronized public static void registerFormater(IResultFormatter clazz) {
		s_registry.put(clazz.getType().toLowerCase(), clazz.getClass());
		if(clazz.isInternalOnly()){
			s_internalOnly.add(clazz.getType().toLowerCase());
		}
	}

	// remove formatters
	synchronized public static void unregisterFormater(IResultFormatter clazz) {
		s_registry.remove(clazz.getType().toLowerCase());
	}

	// get a formatter based on the string from the classes
	synchronized public static IResultFormatter getFormatter(String type) {
	   if (s_registry.isEmpty())
	      registerBasics();
		try {
			return (IResultFormatter) s_registry.get(type.toLowerCase()).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	// get the types of formatters
	synchronized public static List<String> getTypes() {
		if (s_registry.isEmpty())
			registerBasics();
		List<String> types = new ArrayList<String>();
		Set<String> keys = s_registry.keySet();
		for(String key:keys){
			if(!isInternal(key)){
			    types.add(key);
			}
		}
		return types;
	}

  private static boolean isInternal(String key) {
		for(String internalOnly:s_internalOnly){
			if(internalOnly.equals(key)){
				return true;
			}
		}
		return false;
	}

	// get the types of formatters
  synchronized public static List<String> getAllTypes() {
    if (s_registry.isEmpty())
      registerBasics();
    List<String> types = new ArrayList<String>();
    types.addAll(s_registry.keySet());
    return types;
  }

  public static void setLineTerminator(String terminator) {
		m_lineTerminator = terminator;
	}

	/**
	 * getLineTerminator FormatRegistry
	 */
	public static String getLineTerminator() {
		return m_lineTerminator;
	}

	public static void registerBasics() {
		if (s_registry.isEmpty()) {
			try {
				FormatRegistry.registerFormater(new TxtFormatter());
				FormatRegistry.registerFormater(new CSVFormatter());
				FormatRegistry.registerFormater(new DelimitedFormatter());
				FormatRegistry.registerFormater(new InsertFormatter());
				FormatRegistry.registerFormater(new FixedFormatter());
				FormatRegistry.registerFormater(new XMLFormatter());
				FormatRegistry.registerFormater(new HTMLFormatter());
				FormatRegistry.registerFormater(new LoaderFormatter());
				FormatRegistry.registerFormater(new ANSIConsoleFormatter());
				FormatRegistry.registerFormater(new JSONFormatter());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
