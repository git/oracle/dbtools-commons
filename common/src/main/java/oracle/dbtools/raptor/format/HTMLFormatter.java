/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.sql.Clob;
import java.sql.Types;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.utils.DataTypesUtil;

/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=HTMLFormatter.java">Barry McGillin</a> 
 *
 */
public class HTMLFormatter extends ResultsFormatter {
    public static final String TYPE = "HTML"; //$NON-NLS-1$
    public static final String EXT = "htm"; //$NON-NLS-1$
	public static final String KEY_REC_TERM = 	"EXPORT_HTM_REC_TERM"; //$NON-NLS-1$

	private boolean _result = true;

	public HTMLFormatter() {
		super(
				TYPE, "HTML File", EXT);
	}
	final private static String MIME ="text/html";
  public String getMimeType(){
    return MIME;
  }
  
    public boolean isTextEditorReadable(){
    	return false;
    }
	
	public void start() throws IOException {
		write(HEADER.replace("@@ENCODING@@", getEncode()) + getLineTerminator()); //$NON-NLS-1$
    int size = getColumnCount();

		for (int i = 0; i < size; i++) {
      String name = getColumnName(i);
      write("td:nth-of-type("+i+"):before { content: \"" +name +"\"; }" + getLineTerminator()); //$NON-NLS-1$ //$NON-NLS-2$
    }
    
    write(HEADER_PART2+ getLineTerminator()); //$NON-NLS-1$
		write("<table><thead><tr>"); //$NON-NLS-1$
		for (int i = 0; i < size; i++) {
			String name = getColumnName(i);
			write("\t<th>" + name + "</th>" + getLineTerminator()); //$NON-NLS-1$ //$NON-NLS-2$
		}
		write("</tr></thead>\n<tbody id=\"data\">\n"+getLineTerminator()); //$NON-NLS-1$
	}

	public void startRow() throws IOException {
		write("\t<tr>" + getLineTerminator()); //$NON-NLS-1$
	}

	public void printColumn(Object col, int viewIndex, int modelIndex)
			throws IOException {
		if(super.getDataType(viewIndex) == Types.NUMERIC){
			write("<td align=\"right\">"); //$NON-NLS-1$
		} else {
			write("<td>"); //$NON-NLS-1$
		}
                if (col instanceof List<?>) {
	            String str = "<table><tr>"; //$NON-NLS-1$
	            List<?> l = (List<?>) col;
	            List<?> zeroThRow = (List<?>) l.get(0);
                int c = 0;
                for (Object fld : (List<?>) zeroThRow) {
                    str += "<th>"+zeroThRow.get(c)+"</th>";
                    c++;
                }
                str += "</tr>"; //$NON-NLS-1$
                int i = 0;
	            for (Object row : l) {
	                if (i > 0) {
	                    str += "<tr>"; //$NON-NLS-1$
	                    int j = 0;
	                    for (Object fld : (List<?>) row) {
	                        str += "<td>" + escape(getValue(fld).toString()) + "</td>"; //$NON-NLS-1$ //$NON-NLS-2$
	                        j++;
	                    }
	                    str += "</tr>"; //$NON-NLS-1$
	                }
	                i++;
	            }
	            str += "</table>"; //$NON-NLS-1$
                write(str);
	        } else {
	            write(escape(getValue(col)));
	        }
		write("</td>" + getLineTerminator()); //$NON-NLS-1$
	}

	private String escape(String string) {
        	if (string != null) {
			return string.replace("<", "&lt;").replace(">", "&gt;"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        	} else {
			return "&nbsp;";
		}
	}

	public void endRow() throws IOException {
		write("\t</tr>" + getLineTerminator()); //$NON-NLS-1$
	}

	public void end() throws IOException {
		write("</tbody></table><!-- SQL:\n"+ getWrapper().getSQL() + "--></body></html>\n"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public void setResult(boolean result) {
		this._result = result;
	}

	public void setTableName(String tName) {

	}

	final private static String HEADER = "<!DOCTYPE html>\n" + 
	    "<html>\n" + 
	    "\n" + 
	    "<head>\n" + 
	    "  <meta charset='@@ENCODING@@'>\n" + 
	    "  \n" + 
	    "  <title>Responsive Table</title>\n" + 
	    "  \n" + 
	    "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" + 
	    "\n" + 
	    "  \n" + 
	    "  <style>\n" + 
	    "  * { \n" + 
	    "    margin: 0; \n" + 
	    "    padding: 0; \n" + 
	    "  }\n" + 
	    "  body { \n" + 
	    "    font: 14px/1.4 Georgia, Serif; \n" + 
	    "  }\n" + 
	    "  \n" + 
	    "  /* \n" + 
	    "  Generic Styling, for Desktops/Laptops \n" + 
	    "  */\n" + 
	    "  table { \n" + 
	    "    width: 100%; \n" + 
	    "    border-collapse: collapse; \n" + 
	    "  }\n" + 
	    "  /* Zebra striping */\n" + 
	    "  tr:nth-of-type(odd) { \n" + 
	    "    background: #eee; \n" + 
	    "  }\n" + 
	    "  th { \n" + 
	    "    background: #333; \n" + 
	    "    color: white; \n" + 
	    "    font-weight: bold; \n" + 
	    "  }\n" + 
	    "  td, th { \n" + 
	    "    padding: 6px; \n" + 
	    "    border: 1px solid #9B9B9B; \n" + 
	    "    text-align: left; \n" + 
	    "  }\n" + 
	    "  @media \n" + 
	    "  only screen and (max-width: 760px),\n" + 
	    "  (min-device-width: 768px) and (max-device-width: 1024px)  {\n" + 
	    "    table, thead, tbody, th, td, tr { display: block; }\n" + 
	    "    thead tr { position: absolute;top: -9999px;left: -9999px;}\n" + 
	    "    tr { border: 1px solid #9B9B9B; }\n" + 
	    "    td { border: none;border-bottom: 1px solid #9B9B9B; position: relative;padding-left: 50%; }\n" + 
	    "    \n" + 
	    "    td:before { position: absolute;top: 6px;left: 6px;width: 45%; padding-right: 10px; white-space: nowrap;}\n" + 
	    "    \n" + 
	    "    /*\n" + 
	    "    Label the data\n" + 
	    "    */";

	final private static String HEADER_PART2 ="  }\n" + 
	    "  \n" + 
	    "  /* Smartphones (portrait and landscape) ----------- */\n" + 
	    "  @media only screen\n" + 
	    "  and (min-device-width : 320px)\n" + 
	    "  and (max-device-width : 480px) {\n" + 
	    "    body { \n" + 
	    "      padding: 0; \n" + 
	    "      margin: 0; \n" + 
	    "      width: 320px; }\n" + 
	    "    }\n" + 
	    "  \n" + 
	    "  /* iPads (portrait and landscape) ----------- */\n" + 
	    "  @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {\n" + 
	    "    body { \n" + 
	    "      width: 495px; \n" + 
	    "    }\n" + 
	    "  }\n" + 
	    "  \n" + 
	    "  </style>\n" + 
	    "  <!--<![endif]-->\n" + 
	    "<script type=\"text/javascript\">\n" + 
	    "\n" + 
	    "lsearch = function(){\n" + 
	    "  //this.term = document.getElementById('S').value.toUpperCase();\n" + 
	    "  var s = document.getElementById('search').value.toLowerCase();\n" + 
	    "  rows = document.getElementById('data').getElementsByTagName('TR');\n" + 
	    "  for(var i=0;i<rows.length;i++){\n" + 
	    "    if (s ==\"\" ){\n" + 
	    "      rows[i].style.display ='';\n" + 
	    "    } else if ( rows[i].innerText.toLowerCase().indexOf(s) != -1 ) {\n" + 
	    "      rows[i].style.display ='';\n" + 
	    "    } else {\n" + 
	    "      rows[i].style.display ='none';\n" + 
	    "    }\n" + 
	    "  }\n" + 
	    "  this.time = false;\n" + 
	    "}\n" + 
	    "\n" + 
	    "</script>\n" + 
	    "</head>\n" + 
	    "\n" + 
	    "<body>\n" + 
	    "<div><input type=\"text\" size=\"30\" maxlength=\"1000\" value=\"\" id=\"search\" onkeyup=\"lsearch();\" /><input type=\"button\" value=\"Go\" onclick=\"lsearch();\"/> </div>";
    @Override
    public boolean allowsLobs() {
        return true;
    }
	public boolean isLineTerminatorSupported(){
		return true;
	}
    public String getLineTerminatorConfigKey(){
    	return KEY_REC_TERM;
    }

    protected String getValue(Object col) {
        Object val = null;
        
        try {
            if (col instanceof Clob) {
                Integer size=null;
                if (this.getScriptContext()!=null){
                    size=(Integer)(this.getScriptContext().getProperty(ScriptRunnerContext.SETLONG));
                }
                if (size!=null) {
                    val=DataTypesUtil.stringValue(col,this.getConnection(),size);
                } else {
                    val=DataTypesUtil.stringValue(col,this.getConnection());
                }
            } else {                
                val = super.getValue(col);
            }
        } catch (Exception e){
                Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING,
                                e.getStackTrace()[0].toString(), e);
        }
        
        return (val != null) ? val.toString() : null;
    }
    public Boolean isCandidateForSpoolMax() {
  	  return true;
    }
}
