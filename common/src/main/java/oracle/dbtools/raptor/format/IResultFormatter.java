/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.util.ZipOutputHandler;

/**
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=IResultFormatter.java"
 *         >Barry McGillin</a>
 *
 */
public interface IResultFormatter extends IExportFormatOptions {

  public String getType();

  public String getExt();
  
  public String getMimeType();

  public void setOutWriter(Writer out);

  public Writer getOutWriter();

  public void setOutputStream(OutputStream out);

  public OutputStream getOutputStream();

  /**
   * For now zipping is only supported in text-based formats. In which case,
   * outStream/OutWriter is not used.
   * 
   * @param zipper
   */
  public void setZipper(ZipOutputHandler zipper);

  public ZipOutputHandler getZipper();

  /**
   * Is output requested as a single file?
   * 
   * Note for sqlloader format, the preferred method of generation is to have
   * the control file and data generated as separate files. However, if user
   * requests a single file, we will include the data with the control file.
   * (Further note, for some datatypes separate files are generated for each
   * column and these files are not managed centrally (bad).
   * 
   * @return
   */
  public boolean isSeparateDataFile();

  /**
   * Set output requested as a single file.
   */
  public void isSeparateDataFile(boolean value);

  public void setEncode(String enc);

  public String getEncode();

  /*
   * Determine if generation for cloud deployment
   * 
   * @return true if deploying to cloud
   */
  public boolean isDeployCloud();

  /*
   * Indicate if generation for cloud deployment
   */
  public void isDeployCloud(boolean value);
  
  public Boolean isCandidateForSpoolMax();

  public Boolean getPromptForTable();

  public void setPromptForTable(Boolean prompt);

  public void setTableName(String tName);

  public String getFilter();

  public Object getDbObject();

  public void setDbObject(Object dbObject);

  // This method is called at the beginning of an export. It is only called once
  // even when multiple objects are being exported. This allows the formatters
  // to do startup processing.
  public void startExport();

  // This method is called at the end of an export. It is only called once
  // even when multiple objects are being exported. This allows the formatters
  // to do cleanup processing.
  public void finishExport();

  public void start() throws IOException;

  public void startRow() throws IOException;

  public void printColumn(Object col, int viewIndex, int modelIndex) throws IOException;

  public void endRow() throws IOException;

  public void end() throws IOException;

  public void log(String s);

  public boolean isTextEditorReadable();

  public boolean isStandardEncodings();

  public String[] getEncodings();

  public boolean allowsLobs();

  public boolean isHeaderOptionSupported();

  public boolean isHeader();

  public void isHeader(boolean value);

  public String getHeaderConfigKey();

  public boolean isStreamHandler();

  public void flushStream();

  public void closeStream();

  public int validateOptions();

  public String getValidationMessage();

  public void setValidationMessage(String value);

  public void setDataProvider(IResultsFormatterWrapper wrapper);

  public int print();

  public void setScriptContext(ScriptRunnerContext scriptRunnerContext);

  public ScriptRunnerContext getScriptContext();
  
  /**
   *  This will exclude the formatter from the FormatRegistry.getTypes()
   * @return
   */
  public boolean isInternalOnly();

}
