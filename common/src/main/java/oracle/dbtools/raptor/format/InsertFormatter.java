/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.jdbc.OracleBlob;
import oracle.jdbc.OracleCallableStatement;
//Note if we try to use java.sql.Clob, data is returned with escape characters as \/ for /
//import java.sql.Clob;
//Also note oracle.sql.CLOB deprecated in 12.1
//But OracleClob appears to do what we need
import oracle.jdbc.OracleClob;
import oracle.jdbc.OracleTypes;
import oracle.sql.BFILE;
import oracle.sql.BLOB;
import oracle.sql.RAW;

/*
 * Insert Formatter
 * 
 * ResultsFormatter implementation for sql insert generation
 * 
 * 
 * 
 */
public class InsertFormatter extends ResultsFormatter {
	private static final Logger LOGGER = Logger.getLogger(InsertFormatter.class.getName());

    public static final String TYPE = "INSERT"; //$NON-NLS-1$
    public static final String EXT = "sql"; //$NON-NLS-1$
	public static final String KEY_REC_TERM = 	"EXPORT_INS_REC_TERM"; //$NON-NLS-1$
	public static final String COMMIT = 	"COMMIT"; //$NON-NLS-1$
	public static final String COMMIT_ROWS = 	"COMMIT_ROWS"; //$NON-NLS-1$

	StringBuffer sb = new StringBuffer();

	String tableName = null;

	boolean  isTruncate=false;
	
	boolean  isCommit=false;
	
	int commitRows=0;
	
	boolean isDecSepComma=false;
	
	long rowCount=0;
	float block;

	public InsertFormatter() {
		//super(exportTypeName, "SQL File", "sql"); //$NON-NLS-1$ //$NON-NLS-2$
		super(TYPE,"SQL File",EXT); //$NON-NLS-1$ 
	}

	public String getString() {
		return sb.toString();
	}

	// No column header for insert generation
    public boolean allowsHeader(){
    	return false;
    }
    
    // table being exported, or for grid exports, user provided name
	public void setTableName(String tName) {
		tableName = tName;
	}

	public String getTableName() {
		return this.tableName;
	}

	// start
	//
	// generate comment block to indicate start of export 
	public void start() throws IOException {
		sb = new StringBuffer();
		write("REM INSERTING into " + getTableName()); //$NON-NLS-1$
		write(getLineTerminator());
		write("SET DEFINE OFF;"); //$NON-NLS-1$
		write(getLineTerminator());
		isDecSepComma = NLSProvider.getProvider(getWrapper().getConnection()).getDecimalSeparator() == ',';
		block = 0;
		rowCount=0;
	}

	// Start row - generates the insert into and column list
	//
	public void startRow() throws IOException {
		int size = getColumnCount();
		StringBuffer header = new StringBuffer();
		int x = 0;
		//if (getColumnName(0).equals("Row")) //$NON-NLS-1$ //This string is indeed translated.  Causing bug 7431760
		if (getColumnName(0).equals("Row"))
			x = 1;
		if (size > x) {
			header.append(DBUtil.addDoubleQuote(getColumnName(x)));
			x++;
		}
		for (int i = x; i < size; i++) {
			header.append(',').append(DBUtil.addDoubleQuote(getColumnName(i)));
		}
		write("Insert into "); //$NON-NLS-1$
		if (getTableName().indexOf(".") > -1) //$NON-NLS-1$
			write(getTableName());
		else
			write(DBUtil.addDoubleQuote(getTableName()));
		write(" (" + header + ") values ("); //$NON-NLS-1$ //$NON-NLS-2$
		++rowCount;
		++block;
	}

	/* Generate the column values formatted for insert
	 * (non-Javadoc)
	 * @see oracle.dbtools.raptor.format.IResultFormatter#printColumn(java.lang.Object, int, int)
	 */
	public void printColumn(Object col, int viewIndex, int modelIndex)
			throws IOException {
		String colm = ""; //$NON-NLS-1$
		
		String sep = viewIndex==0?"":",";
		if (col != null) {
			int type = getDataType(viewIndex);
			if (type == OracleTypes.DATE) {
				if (getDateFormat() == null) {
					colm = sep
							+ "'" + cleanString(getValue(col).toString()) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					colm = sep
							+ "to_date('" + cleanString(getValue(col).toString()) + "','" + getDateFormat() + "')"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
			} else if (type == OracleTypes.INTEGER
					|| type == OracleTypes.BIGINT || type == OracleTypes.FLOAT
					|| type == OracleTypes.REAL || type == OracleTypes.DOUBLE
					|| type == OracleTypes.NUMERIC
					|| type == OracleTypes.NUMBER
					|| type == OracleTypes.TINYINT
					|| type == OracleTypes.DECIMAL ) {
					//Bug 10007179 - REDUNDANT '' WHEN UNLOADING NESTED TABLE 
					// Don't double the quote for structure.
		
				colm = cleanString(getValue(col).toString());
				if (isDecSepComma && !colm.startsWith("\'")){  // quote numbers if decimal point is comma
					colm = "\'" + colm + "\'";
				}
				colm = sep + colm;
			} else if (type == OracleTypes.TIMESTAMP
					|| type == OracleTypes.TIMESTAMPLTZ) {
				colm = sep
						+ "to_timestamp('" + getValue(col) + "','" + this.getTimeStampFormat() + "')"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			} else if (type == OracleTypes.TIMESTAMPTZ) {
				colm = sep
						+ "to_timestamp_tz('" + getValue(col) + "','" + this.getTimeStampTZFormat() + "')"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			} else if (type == OracleTypes.CLOB) {

				try {
					String str = getClobString(col, "TO_CLOB");
					colm = sep + str; //$NON-NLS-1$
				} catch (SQLException sqle) {
					//throw new IOException(sqle.getMessage());
					colm = sep + " EMPTY_CLOB()"; //$NON-NLS-1$ just give up on the clob, not everything
				}

			} else if (type == OracleTypes.NCLOB) {

				try {
					String str = getClobString(col, "TO_NCLOB");
					colm = sep + str; //$NON-NLS-1$
				} catch (SQLException sqle) {
					//throw new IOException(sqle.getMessage());
					colm = sep + " EMPTY_NCLOB()"; //$NON-NLS-1$ just give up on the clob, not everything
				}

			}  
			else if (type == OracleTypes.BLOB) {
				try {
					String str = getUUEncodedBlob(col);
					colm = sep + "'" + str + "'"; //$NON-NLS-1$
					colm = sep + str; //$NON-NLS-1$

				} catch (SQLException e) {
					//throw new IOException(e.getMessage());
					colm = sep + " EMPTY_BLOB()"; //$NON-NLS-1$  just give up on the blob UUencode only works for oracle
				}
			} else if (col instanceof byte[] && (type == OracleTypes.RAW || 
					type == OracleTypes.BINARY ||
					type == OracleTypes.VARBINARY )) {
						String str = (String)byteArrayToHexValue(col);
						colm = sep + "'" + str + "'"; //$NON-NLS-1$
			} else if (type == OracleTypes.BFILE) {
				try {
					colm = sep + " BFILENAME ('" + ((BFILE) col).getDirAlias()
							+ "', '" + ((BFILE) col).getName() + "')"; //$NON-NLS-1$
				} catch (SQLException e) {
					throw new IOException(e.getMessage());
				}
			} else if (type == 2003) { // vasan:This is a nested table case. I
										// don't know what is the OracleTypes
										// constant for this
				colm = sep + getValue(col).toString();
			} else if (type == OracleTypes.STRUCT){
				colm = sep + getValue(col).toString(); 
			} else {
				colm = sep + "'" + cleanString(getValue(col).toString()) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			}
		} else {
			int type = getDataType(viewIndex);
			if (type == OracleTypes.CLOB) {
				colm = sep + " EMPTY_CLOB()"; //$NON-NLS-1$
			} else if (type == OracleTypes.BLOB) {
				colm = sep + " EMPTY_BLOB()"; //$NON-NLS-1$
			} else {
				colm = sep + getNullDisplay(); //$NON-NLS-1$        	
			}
		}
		write(colm);
	}

	private String getNullDisplay() {
		Object nullValue = super.getValue(null);
		
		return (nullValue != null) ? nullValue.toString() : "null";
	}

	public String cleanString(String str) {
		String newStr = ""; //$NON-NLS-1$
		Character ch;
		Character qt = '\'';
		for (int i = 0; i < str.length(); i++) {
			ch = str.charAt(i);
			if (ch.equals(qt))
				newStr = newStr + "\'\'"; //$NON-NLS-1$
			else
				newStr = newStr + ch;
		}
		return newStr;
	}

	/* endRow - generate the close paren and semi-colon
	 * (non-Javadoc)
	 * @see oracle.dbtools.raptor.format.IResultFormatter#endRow()
	 */
	public void endRow() throws IOException {
		write(");" + getLineTerminator()); //$NON-NLS-1$
		if (isCommit && commitRows>0 && block == commitRows){
			write("commit;" + getLineTerminator()); //$NON-NLS-1$
			block=0;
		}
	}

	public void end() throws IOException {
		if (isCommit && block!= 0){
			write("commit;" + getLineTerminator()); //$NON-NLS-1$
		}
	}

	public Boolean getPromptForTable() {
		return true;
	}

	protected void write(String s) throws IOException {
		if (s != null) {
			if (_out != null || _zipper != null)
				super.write(s);
			else
				sb.append(s);

			checkAndFlush();
		}
	}


    @Override
    public boolean allowsLobs() {
        return false;
    }
     
	private String getClobString(Object clob, String function) throws SQLException {
		long length = 0;
		if (clob instanceof OracleClob){ 
			length = ((OracleClob) clob).length();
		} else {
			length = ((Clob) clob).length(); 	
		}
		
		if (length>500){
			// chunk it up

			StringBuilder work = new StringBuilder();
			Reader dataReader = null;
			if (clob instanceof OracleClob){ 
				dataReader = ((OracleClob) clob).getCharacterStream();
			} else {
				dataReader = ((Clob) clob).getCharacterStream();		
			}
			char[] cbuf = new char[500];
			
			int charsRead=0;
			
			String savedQuote="";
			String CONCAT=getLineTerminator() + "|| ";
			String concat="";
			try {
				while ((charsRead = dataReader.read(cbuf,0,500))!= -1) {
					// Make sure we will not try to split up the doubled single quote
					// this will contain the saved quote if needed
					work.append(concat+function+"('" + savedQuote);
					for (int i=0;i<charsRead;++i){
						
						if (cbuf[i] == '\''){
							work.append("'");	// double any single quotes found.
						} 
						work.append(cbuf[i]);
					}
					work.append("')");
					// Make sure we will not try to split up the doubled single quote
					if (work.charAt(charsRead-1) == '\''){
						if (charsRead >1 && !(work.charAt(charsRead-2) == '\'')){
							work.deleteCharAt(charsRead-1); // force it into the next chunk
							savedQuote="'";
						}
					}
					concat=CONCAT;
				}
			} catch (IOException e){
				LOGGER.log(Level.WARNING, e.getMessage(), e);
				return " EMPTY_CLOB()";
			}

	
			return work.toString();
		} else if (length > 0){
			return "'" + cleanString(getValue(clob).toString()) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		return ""; //$NON-NLS-1$
		
	}

	private String getUUEncodedBlob(Object aBlob) throws SQLException {
		String encodedString = null;
		long aBlobLength = 0L;
		byte[] aBlobBytes = null;

		Connection conn = getConnection();
		OracleCallableStatement ocst = (OracleCallableStatement) conn
				.prepareCall("{ ? = call utl_encode.uuencode(utl_raw.cast_to_raw(?),'1','uuencode.buf') }");
		ocst.registerOutParameter(1, OracleTypes.RAW);

		if (aBlob instanceof OracleBlob) {
			aBlobLength = ((OracleBlob) aBlob).length();
			aBlobBytes = ((OracleBlob) aBlob).getBytes(1, (int) aBlobLength);
		} else if (aBlob instanceof Blob) {
			aBlobLength = ((Blob) aBlob).length();
			aBlobBytes = ((Blob) aBlob).getBytes(1, (int) aBlobLength);
		}

		ocst.setString(2, new String(aBlobBytes));
		// ocst.setString(2, aBlob.toString());

		// Run the procedure.
		ocst.execute();

		// get the value.
		RAW rawEncoded = ocst.getRAW(1);

		// get a string representation.
		encodedString = rawEncoded.stringValue();

		// close the statement.
		ocst.close();

		// chunk it up as we do for clobs
		
		
		if (encodedString.length()>500){
			// chunk it up

			StringBuilder work = new StringBuilder();
			Reader dataReader = new StringReader(encodedString);
			char[] cbuf = new char[500];
			
			int charsRead=0;
			
			String CONCAT=getLineTerminator() + "|| ";
			String concat="";
			try {
				while ((charsRead = dataReader.read(cbuf,0,100))!= -1) {
					work.append(concat+"TO_BLOB(HEXTORAW('");
					for (int i=0;i<charsRead;++i){
						work.append(cbuf[i]);
					}
					work.append("'))");
				
					concat=CONCAT;
				}
			} catch (IOException e){
				LOGGER.log(Level.WARNING, e.getMessage(), e);
				// Then allow to return normally
			}

	
			return work.toString();
		}
		
		
		
		// return it back encoded string.
		return "'"+encodedString+"'";
	}
	private String byteArrayToHexValue(Object col){
		byte[] b = (byte[])col;  
		String result = "";
		for (int i=0; i < b.length; i++) {
			result +=
				Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
		}
		return result;
	}



	/*
	 * protected Object getValue(Object obj) { if ((obj instanceof Blob) || (obj
	 * instanceof BLOB)) { return obj; } else { return super.getValue(obj); } }
	 */
	public boolean isLineTerminatorSupported(){
		return true;
	}
	public boolean isTableNameSupported(){
		return true;
	}
    public String getLineTerminatorConfigKey(){
    	return KEY_REC_TERM;
    }
 
    public boolean isTruncateSupported(){
		return true;
	}
    
    public boolean isTruncate(){
    	return isTruncate;
    }
    public void isTruncate(boolean value){
    	isTruncate = value;
    }
 
    public boolean isCommitSupported(){
		return true;
	}
    public boolean isCommit(){
    	return isCommit;
    }
    public void isCommit(boolean value){
    	isCommit=value;
    }
    
    public int getCommitRows(){
    	return commitRows;
    }
    public void setCommitRows(int value){
    	commitRows=value;
    }
    
    public String getCommitConfigKey(){
    	return COMMIT; //$NON-NLS-1$
    }
    
    public String getCommitRowsConfigKey(){
    	return COMMIT_ROWS; //$NON-NLS-1$
    }
    public Boolean isCandidateForSpoolMax() {
  	  return true;
    }
}
