/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.jdbc.OracleTypes;

/**
 * This is a json format that mimics that the apex listener has 
 * for a json format:
 * Example:
select * from user_objects 
where rownum <3

Results:
{"items":[
{"object_name":"ALL_UT_LIB_DYN_QUERIES","subobject_name":"null","object_id":"23861","data_object_id":"null","object_type":"VIEW","created":"11-SEP-12","last_ddl_time":"11-SEP-12","timestamp":"2012-09-11:15:36:07","status":"VALID","temporary":"N","generated":"N","secondary":"N","namespace":"1","edition_name":"null"}{"object_name":"ALL_UT_LIB_STARTUPS","subobject_name":"null","object_id":"23858","data_object_id":"null","object_type":"VIEW","created":"11-SEP-12","last_ddl_time":"11-SEP-12","timestamp":"2012-09-11:15:36:06","status":"VALID","temporary":"N","generated":"N","secondary":"N","namespace":"1","edition_name":"null"}]}


 * @author klrice
 *
 */
public class JSONFormatter extends ResultsFormatter {
	class Col {
		private String _name;
		private Object _val;
		private int _type;

		Col(String name, Object val, int type) {
			_name = name;
			_val = val;
			_type = type;
		}
		final private static String MIME ="application/json";
    public String getMimeType(){
      return MIME;
    }
    
		public String getName() {
			return q(_name);
		}

		public Object getValue() throws IOException {
			if ((_val != null) && (_type == OracleTypes.INTEGER || _type == OracleTypes.BIGINT || _type == OracleTypes.FLOAT || _type == OracleTypes.REAL || _type == OracleTypes.DOUBLE || _type == OracleTypes.NUMERIC || _type == OracleTypes.NUMBER || _type == OracleTypes.TINYINT || _type == OracleTypes.DECIMAL)) {
				return _val.toString();
			} else if (_val != null && _type == OracleTypes.CURSOR) {
				JSONFormatter json = new JSONFormatter();
				ResultSet rs = (java.sql.ResultSet) _val;
				json.setDataProvider(new ResultSetFormatterWrapper(rs, ResultSetFormatterWrapper.ObjectType.GENERIC));
				json.setTopLevel(false);
				StringWriter writer = new StringWriter();
				json.print(writer);
				StringBuffer sb = writer.getBuffer();
				return sb.toString();
			} else if (_val != null) {
				return q(_val.toString());
			}
			return _val;
		}

		public boolean isNullValue() {
			return _val == null;
		}

		public int getType() {
			return _type;
		}
	}
	
	public static final String TYPE = "JSON"; //$NON-NLS-1$
	public static final String EXT = "json"; //$NON-NLS-1$
	private ArrayList<Col> jsonRowData = new ArrayList<Col>();
	private boolean _topLevel = true;

	public JSONFormatter() {
		super(TYPE, Messages.getString("JSONFormatter.0"), EXT); //$NON-NLS-1$
	}

	public void setTopLevel(boolean b) {
		_topLevel = b;
	}

	@Override
	public void setTableName(String tName) {
	}

	@Override
	public void start() throws IOException {
		if (_topLevel) {
			try {
				write(getColumnMetadata());
			} catch (Exception e) {
				//ignore
			}
			write(",\"items\":\n");
		}
		write("[\n");
	}

	private String getColumnMetadata() throws SQLException {
		StringBuffer sb = new StringBuffer();
		if (getScriptContext().isJSONOutput()) {
			sb.append("],");
		} else {
			sb.append("{");
		}
		sb.append("\"results\":[{");
		sb.append(q("columns") + ":[");
		for (int i = 0; i < getWrapper().getColumnCount(); i++) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append("{" + q("name") + ":" + q(getWrapper().getColumnName(i)));
			sb.append(",");
			
			// 25662771 WRONG COLUMN TYPE WHEN SQLFORMAT IS JSON. 
			// Do not convert the names here. Internally, date is id-ed as timestamp. It will return 93 (timestamp) instead of 91 (date).
			// Use the datatype name directly from the wrapper.  10/20/2017 -- sroychow.
			//sb.append(q("type") + ":" + q(getDataTypeName(getWrapper().getDataType(i))) + "}");
			//int dataTypeInteger = getWrapper().getDataType(i);
			//int dataTypeInteger = getDataType(i);
			//String dataTypeName = getDataTypeName(dataTypeInteger);
			
			String dataTypeName = getDataTypeName(i);
			sb.append(q("type") + ":" + q(dataTypeName) + "}");
		}
		sb.append("]");
		return sb.toString();
	}

	// 25662771 WRONG COLUMN TYPE WHEN SQLFORMAT IS JSON. 
	// Do not convert the names here. Internally, date is id-ed as timestamp.
	// Use the datatype name directly from the wrapper. 10/20/2017 -- sroychow.
	/*
	private String getDataTypeName(int dataType) {
		switch(dataType) {
		case Types.VARCHAR:
			return "VARCHAR2";
		case Types.CHAR:
			return "CHAR";
		case Types.NCHAR:
			return "NCHAR";
		case Types.NVARCHAR:
			return "NVARCHAR";
		case Types.DATE:
		    return "DATE";
		case Types.BLOB:
			return "BLOB";
		case Types.CLOB:
			return "CLOB";
		case Types.NCLOB:
			return "NCLOB";
		case Types.TIMESTAMP:
			return "TIMESTAMP";
		default:
			return "NUMBER";
		}	
	}
	*/

	@Override
	public void startRow() throws IOException {
		long row = this.getResultsFormatterWrapper().getRowNum();
		jsonRowData.clear();

		if (row > 1) {
			write(",");
		}
	}

	@Override
	public void printColumn(Object col, int viewIndex, int modelIndex) {
		int type = getDataType(viewIndex);
		String name = getColumnName(viewIndex).toLowerCase();
		Object val = null;
		if (type == OracleTypes.CLOB) {
			Integer size=null;
			if (getScriptContext()!=null){
				size=(Integer)(getScriptContext().getProperty(ScriptRunnerContext.SETLONG));
			}
			String store=null;
			if (size!=null) {
				store=DataTypesUtil.stringValue(col,getScriptContext().getCurrentConnection(),size);
			} else {
				store=DataTypesUtil.stringValue(col,getScriptContext().getCurrentConnection());
			}
			val=store;
		}else {
			if (type != OracleTypes.CURSOR) { 
				val = getValue(col);
			} else {
				val = col;
			}
		}
		Col c = new Col(name, val, type);
		jsonRowData.add(c);
	}

	@Override
	public void endRow() {
		try {
			write("{");
			boolean first = true;

			for (Col col : jsonRowData) {
				if (!first && !col.isNullValue()) {
					write(",");
				}
				if (!col.isNullValue()) {
					write(col.getName() + ":" + col.getValue());
					first = false;
				}
			}
			write("}"+System.lineSeparator()); //Quick fix - newline each row. Should use StringBuilder customers are exporting millions of rows with this.
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void end() throws IOException {
		write("]");
		if (_topLevel) {
			write("}");
		}
		if (getScriptContext().isJSONOutput()) {
			getScriptContext().setJSONQueryRun(true);
		} else {
			write("]}");
		}
	}

	public String q(String string) {
		if (string == null || string.length() == 0) {
			return "\"\"";
		}
		int len = string.length();
		StringBuilder sb = new StringBuilder(len + 4);
		sb.append('"');
		sb.append(createValidJSON(string));
		sb.append('"');
		return sb.toString();
	}

	private StringBuffer createValidJSON(String string) {
		char c = 0;
		int i;
		int len = string.length();
		String t;
		StringBuffer sb = new StringBuffer();
		for (i = 0; i < len; i += 1) {
			c = string.charAt(i);
			switch (c) {
			case '\\':
			case '"':
				sb.append('\\');
				sb.append(c);
				break;
			case '/':
				sb.append('\\');
				sb.append(c);
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\r':
				sb.append("\\r");
				break;
			default:
				if (c < ' ') {
					t = "000" + Integer.toHexString(c);
					sb.append("\\u" + t.substring(t.length() - 4));
				} else {
					sb.append(c);
				}
			}
		}
		return sb;
	}
	public Boolean isCandidateForSpoolMax() {
		return true;
	}
}
