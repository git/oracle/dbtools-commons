/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import oracle.dbtools.raptor.nls.FormatType;
import oracle.dbtools.raptor.utils.GuidGen;
import oracle.dbtools.raptor.utils.NLSUtils;
import oracle.sql.ArrayDescriptor;
import oracle.sql.BFILE;
import oracle.sql.DATE;
import oracle.sql.Datum;
import oracle.sql.OPAQUE;
import oracle.sql.StructDescriptor;
import oracle.sql.TIMESTAMP;
import oracle.sql.TIMESTAMPLTZ;
import oracle.sql.TIMESTAMPTZ;
import oracle.xdb.XMLType;

public class LoaderFormatter extends ResultsFormatter {
    private static final Logger LOGGER = Logger.getLogger(LoaderFormatter.class.getName());
    public static final String TYPE = "LOADER"; //$NON-NLS-1$
    public static final String EXT = "ctl"; //$NON-NLS-1$
    public static final String EXT_DAT = "ldr"; //$NON-NLS-1$
    public static final String KEY_SEPARATE_DATA_FILE = "SEPARATE_DATA_FILE"; //$NON-NLS-1$
    public static final String KEY_DELIMITER = "EXPORT_LDR_DELIMITER"; //$NON-NLS-1$
    public static final String KEY_REC_TERM = "EXPORT_LDR_REC_TERM"; //$NON-NLS-1$
    public static final String KEY_ENCLOSURES = "EXPORT_LDR_ENCLOSURES"; //$NON-NLS-1$
    public static final String KEY_ENCL_LEFT = "EXPORT_LDR_ENCL_LEFT"; //$NON-NLS-1$
    public static final String KEY_ENCL_RIGHT = "EXPORT_LDR_ENCL_RIGHT"; //$NON-NLS-1$
    public static final String KEY_ENCL_RIGHT_DOUBLE = "EXPORT_LDR_ENCL_RIGHT_DOUBLE"; //$NON-NLS-1$

    boolean do_header = false;
    boolean blobClobWarning = false;
    boolean rowWritten=false;
    String tableName = null;
    StringBuilder header;
    StringBuilder row;
    Writer _sepDataWriter;
    private Set<String> s = new HashSet<String>();
    private List<Object> colIsNullObject = new ArrayList<Object>();
    private int indentNum;
    private boolean columnOutputted = false;
    private boolean headerStarted = false;
    private boolean _isSeparateDataFile=false;
    private String ldr_delim = "|"; //$NON-NLS-1$
    private String ldr_leftEnc = "\""; //$NON-NLS-1$
    private String ldr_rightEnc = "\""; //$NON-NLS-1$
	private String _ldrEOLChars;
	private String _ctlEOLChars;
	private Integer numRows = 0;
    /**
	 * An array list of the file I have created.  I'll use this for the controlling script
	 */
	private ArrayList<String> _createdFiles = new ArrayList<String>();



	int spin = 0;

	public LoaderFormatter() {
		super(TYPE, Messages.getString("LoaderFormatter.3"), EXT_DAT); //$NON-NLS-1$
	}

	/**
	 * Set table name for ctl file generation.
	 */
	public void setTableName(String tName) {
		tableName = tName;
	}

	public String getTableName() {
		return this.tableName;
	}

	/**
	 * No header support for sqlloader
	 * 
	 * @return
	 */
	public boolean allowsHeader() {
		return false;
	}

	/**
	 * Some special handling for line terminator because the control file should
	 * be build with standard line terminator. We won't set _EOLChars to the
	 * user specified value until after the control file is generated. This way,
	 * the data file should have those terminators. Note, we expect this to be
	 * used when standard terminators are embedded in the data.
	 */
	public void setLineTerminator(String terminator) {
		_ldrEOLChars = terminator;
	}

	/*
	 * At this point, all we can do is some initializing
	 * 
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.format.IResultFormatter#start()
	 */
	public void start() throws IOException {
		do_header = true;
		rowWritten = false;
		header = new StringBuilder();
		blobClobWarning = false;
		if (_delimiter != null) {
			ldr_delim = _delimiter;
		}
		if (_enclosureLeft != null) {
			ldr_leftEnc = _enclosureLeft;
		}
		if (_enclosureRight != null) {
			ldr_rightEnc = _enclosureRight;
		}

		s = new HashSet<String>();
		spin = 0;
		int size = getColumnCount();
		for (int i = 0; i < size; i++) {
			// add to upper case - false positives ok.
			s.add(this.getColumnName(i).replaceAll("\"", //$NON-NLS-1$
					"").toUpperCase()); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * name not used simple L_spinnumber as multibyte may cause problems with
	 * maxing out to 30 characters rather than 30 bytes.
	 * 
	 * @param name
	 *            the columns name.
	 * @return L_nnn for a filler column holding reference to lob file name (or
	 *         null).
	 */
	private String getNewName(String name) {
		// set spin globally to cut down on s.contains
		// int spin = 0;
		String longName = null;
		while (true) {
			longName = "L" + "_" + spin; //$NON-NLS-1$ //$NON-NLS-2$
			if (!(s.contains(longName))) {
				break;
			}
			spin++;
		}
		s.add(longName);
		spin++;
		return longName;
	}

	/*
	 * Handling the .ctl file generation needs for Varrays
	 */
	private String getVarrayInfo(String name, Object data,
			List<Object> thisArray) {
		String indentation = ""; //$NON-NLS-1$
		for (int i = 0; i < indentNum; i++) {
			indentation = indentation + "  "; //$NON-NLS-1$
		}
		String typeData = indentation + "(" + getLineTerminator(); //$NON-NLS-1$
		try {
			thisArray.add(0, new Integer(0)); // Init to not null. Only 1
												// element in Array
			oracle.sql.ARRAY varr = (oracle.sql.ARRAY) data;
			Datum[] values = varr.getOracleArray();
			int dt1 = varr.getBaseType();
			if (dt1 == 2002) { // array of structs so assume not null
				if (values[0] != null) {
					typeData = typeData
							+ indentation
							+ "  " + name + " COLUMN OBJECT" + getLineTerminator(); //$NON-NLS-1$ //$NON-NLS-2$
					indentNum = indentNum + 1;
					List<Object> arrayofStruct = new ArrayList<Object>();
					typeData = typeData
							+ getColumnObjectInfo((Object) values[0],
									arrayofStruct);
					thisArray.add(0, arrayofStruct); // Init to not null. Only 1
														// element in Array
					indentNum = indentNum - 1;
				} else {
					thisArray.add(0, new Integer(1));
				}
			} else if (dt1 == 2003) { // varray. Not allowed yet but code it for
										// when it is
				if (values[0] != null) {
					ArrayDescriptor varrDescriptor = varr.getDescriptor();
					int varrType = varrDescriptor.getArrayType();
					String varrTypeName = "";
					if (varrType == ArrayDescriptor.TYPE_NESTED_TABLE) {
						varrTypeName = "NESTED TABLE";
					} else {
						varrTypeName = "VARRAY";
					}
					typeData = typeData + indentation + "  " + name + " "
							+ varrTypeName + " TERMINATED BY '" + ldr_delim + //$NON-NLS-1$ //$NON-NLS-2$
							"/'" + getLineTerminator(); //$NON-NLS-1$
					indentNum = indentNum + 1;
					List<Object> arrayofArray = new ArrayList<Object>();
					typeData = typeData
							+ getVarrayInfo(name, values[0], arrayofArray);
					thisArray.add(0, arrayofArray); // Init to not null. Only 1
													// element in Array
					indentNum = indentNum - 1;
				} else {
					thisArray.add(0, new Integer(1));
				}
			} else {
				typeData = typeData
						+ indentation
						+ "  " + name + getDateFormat(dt1) + getLineTerminator(); //$NON-NLS-1$
			}

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}
		return typeData + indentation + ")" + getLineTerminator(); //$NON-NLS-1$
	}

	/*
	 * Handling the .ctl file needs for Structs
	 */
	private String getColumnObjectInfo(Object data, List<Object> thisStruct) {
		String indentation = ""; //$NON-NLS-1$
		boolean elementOutput = false;
		for (int i = 0; i < indentNum; i++) {
			indentation = indentation + "  "; //$NON-NLS-1$
		}
		String typeData = indentation + "(" + getLineTerminator(); //$NON-NLS-1$
		try {
			oracle.sql.STRUCT struct = (oracle.sql.STRUCT) data;
			Object sCols[] = struct.getAttributes();
			StructDescriptor desc = struct.getDescriptor();
			ResultSetMetaData metadata = desc.getMetaData();
			for (int md1 = 0; md1 < metadata.getColumnCount(); md1++) {
				Object obj2 = sCols[md1];
				int dt1 = metadata.getColumnType(md1 + 1);
				String colName = metadata.getColumnName(md1 + 1);
				thisStruct.add(md1, new Integer(0)); // md1 is not a null
														// structure
				if (dt1 == 2002) {
					if (obj2 != null) {
						if (elementOutput == true) {
							typeData = typeData + "," + getLineTerminator(); //$NON-NLS-1$
						}
						elementOutput = true;
						typeData = typeData
								+ indentation
								+ "  " + colName + " COLUMN OBJECT" + getLineTerminator(); //$NON-NLS-1$ //$NON-NLS-2$
						indentNum = indentNum + 1;
						List<Object> internalStruct = new ArrayList<Object>(); // have
																				// recursive
																				// struct
																				// so
																				// get
																				// its
																				// structure
																				// too
						typeData = typeData
								+ getColumnObjectInfo(obj2, internalStruct);
						thisStruct.add(md1, internalStruct); // md1 is a
																// structure
						indentNum = indentNum - 1;
					} else {
						thisStruct.add(md1, new Integer(1)); // md1 is a null
																// structure
					}
				} else if (dt1 == 2003) {
					if (obj2 != null) {
						if (elementOutput == true) {
							typeData = typeData + "," + getLineTerminator(); //$NON-NLS-1$
						}
						elementOutput = true;
						oracle.sql.ARRAY varr = (oracle.sql.ARRAY) obj2;
						ArrayDescriptor varrDescriptor = varr.getDescriptor();
						int varrType = varrDescriptor.getArrayType();
						String varrTypeName = "";
						if (varrType == ArrayDescriptor.TYPE_NESTED_TABLE) {

							varrTypeName = "NESTED TABLE";
						} else {
							varrTypeName = "VARRAY";
						}
						typeData = typeData + indentation + "  " + colName
								+ " " + varrTypeName + " TERMINATED BY '" +
								//$NON-NLS-1$ //$NON-NLS-2$
								ldr_delim + "/'" + getLineTerminator(); //$NON-NLS-1$
						List<Object> internalArray = new ArrayList<Object>(); // have
																				// array
																				// so
																				// get
																				// its
																				// structure
																				// too
						indentNum = indentNum + 1;
						typeData = typeData
								+ getVarrayInfo(colName, obj2, internalArray);
						thisStruct.add(md1, internalArray); // md1 is a
															// structure
						indentNum = indentNum - 1;

					} else {
						thisStruct.add(md1, new Integer(1)); // md1 is a null
																// array
					}
				} else {
					if (elementOutput == true) {
						typeData = typeData + "," + getLineTerminator(); //$NON-NLS-1$
					}
					elementOutput = true;
					typeData = typeData + indentation
							+ "  " + colName + getDateFormat(dt1); //$NON-NLS-1$
				}
			}
			typeData = typeData + getLineTerminator();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}
		return typeData + indentation + ")" + getLineTerminator(); //$NON-NLS-1$
	}

	private String getColumnInfo(Object data, int col) {
		indentNum = 1;
		colIsNullObject.add(col, new Integer(0)); // col is not a null structure
		String colName = getColumnName(col);
		String name = colName.startsWith("\"") ? colName : "\"" + colName
				+ "\"";
		// String name = "\"" + getColumnName(col)+ "\"";
		int dt = getDataType(col);

		// We do not output columns that have null structs as defined in their
		// first record.
		// Since structs can be recursive too then the same applies to elements
		// of the inner struct
		// An ArrayList will be indexed by column position (or struct element
		// position) and have the values
		// 0 means its a populated struct, 1 means its null, a recursive
		// ArrayList means recursive structures

		if (dt == 2002) { // STRUCTS or Object Types. Null first record means
							// miss out that column
			if (data == null) {
				colIsNullObject.add(col, new Integer(1)); // col is a null
															// structure
				return null;
			} else {
				List<Object> internalStruct = new ArrayList<Object>();
				String theObject = getColumnObjectInfo(data, internalStruct);
				colIsNullObject.add(col, internalStruct); // col is a structure
				return (name + " COLUMN OBJECT" + getLineTerminator() + theObject); //$NON-NLS-1$ }
			}
		} else if (dt == 2003) { // VARRAY
			if (data == null) {
				colIsNullObject.add(col, new Integer(1)); // col is a null array
				return null;
			} else {
				String varrTypeName = "";
				try {
					oracle.sql.ARRAY varr = (oracle.sql.ARRAY) data;
					ArrayDescriptor varrDescriptor = varr.getDescriptor();
					int varrType = varrDescriptor.getArrayType();
					if (varrType == ArrayDescriptor.TYPE_NESTED_TABLE) {
						varrTypeName = "NESTED TABLE";
					} else {
						varrTypeName = "VARRAY";
					}
				} catch (SQLException e) {
					LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
				}
				String varray = name
						+ " " + varrTypeName + " TERMINATED BY '" + ldr_delim + "/'" + getLineTerminator(); //$NON-NLS-1$ //$NON-NLS-2$
				indentNum = indentNum + 1;
				List<Object> internalArray = new ArrayList<Object>();
				varray = varray + getVarrayInfo(name, data, internalArray);
				colIsNullObject.add(col, internalArray); // col is a array
				indentNum = indentNum - 1;
				return (varray);
			}
		} else if ((dt == 2004) || (dt == 2005)
				|| (dt == 2007 && isXMLType(data))) { // blob || (n)clob ||
														// XMLType
			String newName = getNewName(name);
			return newName + " FILLER char," + getLineTerminator() + //$NON-NLS-1$
					name + " LOBFILE( " + newName
					+ ") TERMINATED BY EOF NULLIF " + newName + //$NON-NLS-1$ //$NON-NLS-2$
					" = 'null'"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		} else if ((dt == -13)) { // BFile
			int colidx = col + 1;
			String dname = "dname_" + colidx + " FILLER char(20)" + "," + getLineTerminator(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			String fname = "fname_" + colidx + " FILLER char(30)" + "," + getLineTerminator(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			String newName = name
					+ " BFILE(" + "dname_" + colidx + ",fname_" + colidx + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			return dname + fname + newName;
		} else if (dt == Types.CHAR ||
					dt == Types.VARCHAR ||
					dt == Types.NCHAR ||
					dt == Types.NVARCHAR //||
			   		// The length of tlong varchar is 2 Gig, so let's
	        		// not do this for now and let it do what it always did.
	        		// LONG VARCHAR has been discouraged since 8.0 and LONG NVARCHAR is not supported.
	     
					//dt == Types.LONGVARCHAR
					){
			return name + (" CHAR (")+ getPrecision(col) + (")");
			
		} else {
	
			// handle non sdo
			// header.append( name + ":" + dt);
			return name + getDateFormat(dt);
		}
	}

	/*
	 * Generate the control file contents in header (StringBuffer).
	 * 
	 * The control file contents will actually be written in end() so that we
	 * can generate a reasonable number for OPTIONS(ERRORS=nnn) value. We will
	 * determine this as 50 rows or 10% of the file written whichever is larger.
	 * Previously the ERRORS was defaulted to 50.
	 * 
	 * Note, generating a single file is deprecated in 3.2.1
	 */
	public void start2(Object data, int col) throws IOException {
		int size = getColumnCount();
		// Bug 8781333 - EXPORT DATA > LOADER, THE RESULT FILE IS NOT CORRECT
		// Some data is generated as quoted, depending on the datatype
		// Add the OPTIONALLY ENCLOSED clause
		// String sepDataFileName = getNewFileName("ldr","");
		if (col == 0) {
			_EOLChars = null;
			header.append(isDeployCloud()?"LOAD DATA CHARACTERSET \"AL32UTF8\" LENGTH CHAR" + getLineTerminator(): "LOAD DATA "+ getLineTerminator()); //$NON-NLS-1$
			if (!isSeparateDataFile()) {
				header.append("INFILE *" + getLineTerminator()); //$NON-NLS-1$
			} else {
				header.append("INFILE '" + getFileName() + "' " + "\""
						+ "str '" + getLineTerminatorString() + "'" + "\""
						+ getLineTerminator());
			}

			header.append("APPEND" + getLineTerminator()); //$NON-NLS-1$	// handle truncate as ddl (by exportapi)
			header.append("CONTINUEIF NEXT(1:1) = '" + "#" + "'" +
			//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$         	
			// Note, we thought this little change will let us load data with
			// embedded line terminators
			// but it didn't work
			//header.append("CONTINUEIF LAST != '" + ldr_delim + "'" +  //$NON-NLS-1$ //$NON-NLS-2$ 
					getLineTerminator()); //$NON-NLS-1$

			String[] names = tableName.split("\\."); //$NON-NLS-1$
			if (names.length == 1) {
				if (!names[0].startsWith("\"")) {
					header.append("INTO TABLE \"" + names[0] + "\"" + getLineTerminator()); //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					header.append("INTO TABLE " + names[0] + //$NON-NLS-1$
							getLineTerminator());
				}
			} else {
				if (!names[0].startsWith("\"")) {
					header.append("INTO TABLE \"" + names[0] + "\"" + "."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				} else {
					header.append("INTO TABLE " + names[0] + "."); //$NON-NLS-1$ //$NON-NLS-2$
				}
				if (!names[1].startsWith("\"")) {
					header.append("\"" + names[1] + "\"" + //$NON-NLS-1$ //$NON-NLS-2$
							getLineTerminator());
				} else {
					header.append(names[1] + getLineTerminator());
				}
				// header.append("INTO TABLE \"" + names[0] + "\"" + ".\"" +
				// names[1] + "\"" +
				// header.append("INTO TABLE " + names[0] + "." + names[1] +
				//$NON-NLS-1$ //$NON-NLS-2$ 
				// getLineTerminator());
			}
			header.append("FIELDS TERMINATED BY" + "'" + ldr_delim + "'" +
			//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					getLineTerminator()); //$NON-NLS-1$
			if (ldr_leftEnc.length() != 0) {
				header.append("OPTIONALLY ENCLOSED BY '"
						+ handleSpecialEnclosure(ldr_leftEnc) + "'"
						+ " AND "
						+ "'"
						+
						//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						handleSpecialEnclosure(ldr_rightEnc)
						+ "'" + getLineTerminator()); //$NON-NLS-1$
			}
			header.append("TRAILING NULLCOLS ( " + getLineTerminator()); //$NON-NLS-1$
			headerStarted = false;
			colIsNullObject.clear();
		}
		String next = getColumnInfo(data, col);
		if (next != null) {
			if (headerStarted) {
				header.append("," + getLineTerminator()); //$NON-NLS-1$}
			}
			header.append(next);
			headerStarted = true;
		}
		if (col == (size - 1)) {
			header.append(")" + getLineTerminator()); //$NON-NLS-1$

			// Defer writing the control file until end() to allow us to
			// generate a reasonable
			// ERRORS = value.
			//
			// if (!isSeparateDataFile()){
			//            	header.append("begindata" + getLineTerminator()); //$NON-NLS-1$
			// write(header.toString());
			// } else {
			// Separate data file which will be handled here instead of
			// GenerationOutputStream
			// But first write the .ctl file...
			// write(header.toString());

			// if (_zipper !=null){
			// _zipper.closeEntry();
			// _zipper.openEntry(sepDataFileName); // open entry for data file
			// } else {
			// _sepDataWriter=getNewWriter(sepDataFileName); // just simple
			// writes
			// }
			// }
			_ctlEOLChars=_EOLChars;
			_EOLChars = _ldrEOLChars; // establish correct record terminator for
										// data.
			
		}
	}

    @Override
    public boolean allowsLobs() {
        return true;
    }

    private String getDateFormat(int dt) {
        String mask = "";
        if (dt == 91) {
            mask = NLSUtils.getDateFormat(getConnection(), FormatType.LOADER);
            return " DATE \"" + mask + "\" "; //$NON-NLS-1$ //$NON-NLS-2$
        } else if (dt == 93) {
            mask = NLSUtils.getTimeStampFormat(getConnection(), FormatType.LOADER);
            return " TIMESTAMP \"" + mask + "\" "; //$NON-NLS-1$ //$NON-NLS-2$
        } else if (dt == -101) {
            mask = NLSUtils.getTimeStampWithTimeZoneFormat(getConnection(), FormatType.LOADER);
            return " TIMESTAMP WITH TIME ZONE \"" + mask + "\" "; //$NON-NLS-1$ //$NON-NLS-2$
        } else if (dt == -102) {
            mask = NLSUtils.getTimeStampWithTimeZoneFormat(getConnection(), FormatType.LOADER);
            //return " TIMESTAMP WITH LOCAL TIME ZONE \"" + mask + "\" "; //$NON-NLS-1$ //$NON-NLS-2$
            return " TIMESTAMP WITH TIME ZONE \"" + mask + "\" "; //$NON-NLS-1$ //$NON-NLS-2$
        }

        return " "; //$NON-NLS-1$*/
        /*        String dFormat = NLSUtils.getDateFormat(getConnection());
        if (dt == 93)
            return " timestamp \"" + dFormat + "\" "; //$NON-NLS-1$ //$NON-NLS-2$
        else
            return " "; //$NON-NLS-1$*/
    }

    
    
    public void startRow() throws IOException {
        //header = new StringBuilder();
        row = new StringBuilder();
        row.append(" "); // because we are using continuation in first field //$NON-NLS-1$
    }

    private String getStruct(Object data, List<Object> theStructNullIndicator) {
        String colm = ""; //$NON-NLS-1$
        Object structObj = null;
        try {
            oracle.sql.STRUCT struct = (oracle.sql.STRUCT)data;
            Object sCols[] = struct.getAttributes();
            StructDescriptor desc = struct.getDescriptor();
            ResultSetMetaData metadata = desc.getMetaData();
            for (int md1 = 0; md1 < metadata.getColumnCount(); md1++) {
                boolean processStruct = true;
                int dt1 = metadata.getColumnType(md1 + 1);
                if (theStructNullIndicator != null) {
                    structObj = theStructNullIndicator.get(md1);
                    if (structObj instanceof Integer && (Integer)structObj == 1) {
                        // null struct
                        // do nothing
                        processStruct = false;
                    }
                }
                if (processStruct == true) {
                    if (sCols[md1] == null) {
                        if (dt1 == 2002) { // empty struct
                            colm = colm + ldr_delim;
                        } else if (dt1 == 2003) {
                            colm = colm + ldr_delim + "/"; //$NON-NLS-1$
                        } else if ((dt1 == 2004) || (dt1 == 2005)) { //blob or (n)clob
                            colm =
colm + getLeftEnc(dt1) + "null" + getRightEnc(dt1) + ldr_delim; //$NON-NLS-1$
                        } else {
                            colm = colm + getLeftEnc(dt1) + getRightEnc(dt1) + ldr_delim;
                        }
                    } else {
                        if (dt1 == 2002) {
                            Object obj2 = sCols[md1];
                            colm = colm + getStruct(obj2, (List<Object>)structObj); //$NON-NLS-1$
                        } else if (dt1 == 2003) {
                            Object obj2 = sCols[md1];
                            colm = colm + getVarray(obj2, (List<Object>)structObj); //$NON-NLS-1$
                        } else {
                            colm =
colm + getLeftEnc(dt1) + cleanString(getValue(sCols[md1],dt1).toString()) + getRightEnc(dt1) +
 ldr_delim; //$NON-NLS-1$
                        }
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
        }
        return colm;
    }

    private String getVarray(Object data, List<Object> theArrayNullIndicator) {
        String colm = ""; //$NON-NLS-1$
        Object arrayObj = null;
        try {
            boolean processArray = true;
            oracle.sql.ARRAY varr = (oracle.sql.ARRAY)data;
            int baseTyp = varr.getBaseType();
            Datum[] values = varr.getOracleArray();
            if (theArrayNullIndicator != null) {
                arrayObj = theArrayNullIndicator.get(0);
                if (arrayObj instanceof Integer && (Integer)arrayObj == 1) {
                    // null array
                    // do nothing
                    processArray = false;
                }
            }
            int elementCount = 0;
            if (processArray == true) {
                for (int x = 0; x < varr.length(); x++) {
                    if (values[x] == null) {
                        if (baseTyp == 2002) { // null struct
                            colm = colm + ldr_delim;
                        } else if (baseTyp == 2003) {
                            colm = colm + ldr_delim;
                        } else if ((baseTyp == 2004) || (baseTyp == 2005)) { //blob or (n)clob
                            colm =
colm + getLeftEnc(baseTyp) + "null" + getRightEnc(baseTyp) + ldr_delim; //$NON-NLS-1$
                    } else {
                            colm = colm + getLeftEnc(baseTyp) + getRightEnc(baseTyp) + ldr_delim;
                        }
                    } else {
                        elementCount = elementCount + 1;
                        // output 5 elements per line
                        if (elementCount >= 10) {
                            colm = colm + getLineTerminator() + "#"; //$NON-NLS-1$
                            elementCount = 0;
                        }
                        if (baseTyp == 2002) { // array of object types
                            colm =
colm + getStruct(values[x], (List<Object>)arrayObj); //$NON-NLS-1$
                        } else if (baseTyp == 2003) {
                            colm =
colm + getVarray(values[x], (List<Object>)arrayObj); //$NON-NLS-1$}
                        } else {
                            colm =
colm + getLeftEnc(baseTyp) + cleanString(getValue(values[x],baseTyp).toString()) + getRightEnc(baseTyp) +
 ldr_delim; //$NON-NLS-1$
                        }

                    }
                }
                colm = colm + "/"; //$NON-NLS-1$
            } else {
                colm = colm + ldr_delim + "/"; //$NON-NLS-1$
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
        }
        return colm;
    }

    public void printColumn(Object col, int viewIndex, int modelIndex) throws IOException {
        Object obj = null;
        // build the contents of the control file now, but write it out in end() processing
        if (do_header) {				
            start2(col, viewIndex);
        }
        int dt = getDataType(viewIndex);
        if (dt == 2002 || dt == 2003) { // struct or array therefor check for null
            obj = colIsNullObject.get(viewIndex);
            if (obj instanceof ArrayList<?>) {
                // have a non null structure
                // so OK to output members in next block
            } else if ((Integer)colIsNullObject.get(viewIndex) == 1) { //top level null Object
                // dont output the column
                return;
            }
        }
        String colm = ""; //$NON-NLS-1$

        //        if (columnOutputted) {
        //            colm = colm + ldr_delim; //$NON-NLS-1$
        //        }
        columnOutputted = true; // this is set so that the next record will be delimited
	if (col == null) {
            colm = colm + getDelimNullDisplay(dt) + ldr_delim;
            if (dt == 2003) { // varray
                colm = colm + "/"; //$NON-NLS-1$
            }
        } else {
            if (dt == 2002) {
                colm = colm + getStruct(col, (List<Object>)obj);
            } else if (dt == 2003) {
                colm = colm + getVarray(col, (List<Object>)obj);
            } else if (dt == -13) {
                    try {
                        colm =
    ((BFILE)col).getDirAlias() + ldr_delim + ((BFILE)col).getName() + ldr_delim; //$NON-NLS-1$
                    } catch (SQLException e) {
                        throw new IOException(e.getMessage());
                    }
            } else {
                colm =
colm + getLeftEnc(dt) + cleanString(getValue(col,dt).toString()) + getRightEnc(dt) + ldr_delim;
            }
        }
        row.append(colm);
    }

    private String getLeftEnc(int datatype) {
        // don't quote NUMBER or a blob or a clob (filler file name)
    	// 2=number         || 12=varchar     || 93 = timestamp || 2004 = blob      || 20045 = clob 
        /*if (datatype == 2 || datatype == 12 || datatype == 93 || datatype == 2004 ||
            datatype == 2005)
            return ""; //$NON-NLS-1$
        else
            return ldr_leftEnc; //$NON-NLS-1$*/
    	if (_isEnclosed && (datatype == Types.CHAR || datatype == Types.VARCHAR
    			|| datatype == Types.TIMESTAMP 
        		|| datatype == Types.NCHAR || datatype == Types.NVARCHAR 
        		|| datatype == Types.LONGNVARCHAR || datatype == Types.LONGVARCHAR ))
        {
    		return ldr_leftEnc;
        } else {
        	return "";
        }
    }

    private String getRightEnc(int datatype) {
        // don't quote NUMBER or a blob or a clob (filler file name)
    	// 2=number         || 12=varchar     || 93 = timestamp || 2004 = blob      || 20045 = clob 
        /*if (datatype == 2 || datatype == 12 || datatype == 93 || datatype == 2004 ||
            datatype == 2005)
            return ""; //$NON-NLS-1$
        else
            return ldr_rightEnc; //$NON-NLS-1$*/
    	if (_isEnclosed && (datatype == Types.CHAR || datatype == Types.VARCHAR
    			|| datatype == Types.TIMESTAMP 
        		|| datatype == Types.NCHAR || datatype == Types.NVARCHAR 
        		|| datatype == Types.LONGNVARCHAR || datatype == Types.LONGVARCHAR ))        {
    		return ldr_rightEnc;
        } else {
        	return "";
        }
    }
    // placed here in case cleaning becomes required

    public String cleanString(String str) {
        String newStr = ""; //$NON-NLS-1$
        Character ch;
        //        Character qt = '\'';
        for (int i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            //                 if (ch.equals(qt))
            //                    newStr = newStr+"\'\'";
            //                 else
            newStr = newStr + ch;
        }
        return newStr;
    }

	private String getNullorNullDisplay() {
		Object nullValue = super.getValue(null);
		
		return (nullValue != null) ? nullValue.toString() : "null";
	}
	
	private String getEmptyorNullDisplay() {
		Object nullValue = super.getValue(null);
		
		return (nullValue != null) ? nullValue.toString() : "";
	}
	
	private String getDelimNullDisplay(int datatype) {
		switch (datatype) {
			case 2004: // blob
			case 2005: // clob
			case 2007: // opaque
				return getNullorNullDisplay();
			default:
                        	return getEmptyorNullDisplay();
		}
	}
	
	public void endRow() throws IOException {

		rowWritten = true;
		++numRows;

		if (_zipper != null) {
			_zipper.writeEntryText(row.toString());
			_zipper.writeEntryText(getLineTerminator());
		} else if (_out != null) {
			_out.write(row.toString());
			_out.write(getLineTerminator());
			if (_writeTracker < FLUSH_EVERY_N_WRITE) {
				_writeTracker++;
			} else {
				if (_out != null) {
					_out.flush();
				}
				_writeTracker = 0;
			}
		}

		do_header = false;
		columnOutputted = false;
	}

    /*
     * getValue - does the main work of picking up the value and getting it into
     * form needed for sqlloader.
     * 
     * This primarily relies on DataTypesUtil.stringValue, but handles clobs
     * and blobs here so that the complete values can be written into the
     * separate files, not as truncated strings which is the standard thing
     * DataTYpeUtils does.
     * 
     * Note, if col is null we don't even get here, instead we returned "null"
     * so that loader would recognize it as null value.
     * 
     */
    protected Object getValue(Object obj, int type) {
        if ((this.getFileName() == null) && ((obj instanceof Blob) || (obj instanceof Clob))) {
            return "null"; //$NON-NLS-1$
        }
        if (obj instanceof Clob) {
            Clob clob = (Clob)obj;
            String opfile = ""; //$NON-NLS-1$
                        
            boolean isNull = false;
            
            try {
            	Reader clobRdr = clob.getCharacterStream();
            	opfile=getNewFileName(EXT_DAT);
            	_createdFiles.add(opfile);

            	try {
            		isNull = writeFile(opfile, clobRdr);
            	} catch (Exception e) {
            		LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
            		isNull=true;
            	} finally {
    				try {
    					clobRdr.close();
    				} catch (Exception e) {
    					; // ignore
    				}
    			}

            } catch (Exception e) {
            	Logger.getLogger(LoaderFormatter.class.getName()).log(
            			Level.WARNING, e.getStackTrace()[0].toString(), e);
            	isNull=true;
            }
            
            if (isNull) {
                return "null"; //$NON-NLS-1$
            }
            return new File(opfile).getName();
        }
        if (obj instanceof Blob) {
            Blob blob = (Blob)obj;
            String opfile = ""; //$NON-NLS-1$
            BufferedOutputStream bos = null;
            InputStream is = null;
            boolean isNull = false;
            try {
            	opfile = getNewFileName(EXT_DAT);
            	is = blob.getBinaryStream();
            	if (is != null) {
            		_createdFiles.add(opfile);
            		bos = new BufferedOutputStream(new FileOutputStream(opfile));
            		byte[] buffer = new byte[4096];
            		int length = 0;
            		while ((length = is.read(buffer)) != -1) {
            			bos.write(buffer, 0, length);
            		}

            	} else {
            		isNull = true;
            	}
            } catch (FileNotFoundException e) {
            	LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
            } finally {
                if (bos != null) {
                    try {
                        bos.close();
                    } catch (IOException e) {
                    }
                }
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                    }
                }
            }
            if (isNull) {
                return "null"; //$NON-NLS-1$
            }
            return new File(opfile).getName();
            
        }  else if (obj instanceof OPAQUE) {
        	String str = "";
        	boolean isNull = false;
        	String opfile = ""; //$NON-NLS-1$
        	try {
        		OPAQUE opaque = (OPAQUE) obj;
        		str = opaque.getSQLTypeName().trim();
        		if (str.equals("SYS.XMLTYPE")) { //$NON-NLS-1$
        			
         			XMLType xmltype = XMLType.createXML(opaque);
        			Reader clobRdr = xmltype.getClobVal().getCharacterStream();

        			opfile=getNewFileName(EXT_DAT);
        			_createdFiles.add(opfile);

        			try {
        				isNull = writeFile(opfile,clobRdr);
        			} catch (Exception e){
        				isNull=true;
        			} finally {
        				try {
        					clobRdr.close();
        				} catch (Exception e) {
        					; // ignore
        				}
        			}
        		}
			} catch (Exception e) {
		        Logger.getLogger(LoaderFormatter.class.getName()).log(
                        Level.WARNING, e.getStackTrace()[0].toString(), e);
		        isNull = true;
			}
			if (isNull) {
                return "null"; //$NON-NLS-1$
            }
            return new File(opfile).getName();
        }
			
        if (_isEnclosed && (type == Types.CHAR
         		|| type == Types.LONGNVARCHAR || type == Types.LONGVARCHAR 
        		|| type == Types.NCHAR || type == Types.NVARCHAR || type == Types.VARCHAR))
        {
        	//return ldr_leftEnc + enclosureRightDouble(getResultsFormatterWrapper().getValue(obj).toString() + ldr_rightEnc);
        	return enclosureRightDouble(getResultsFormatterWrapper().getValue(obj).toString());
        }
        if (obj instanceof DATE  
        		|| (obj instanceof TIMESTAMPTZ)
				|| (obj instanceof TIMESTAMPLTZ) 
                || (obj instanceof TIMESTAMP)) {
        	return this.getResultsFormatterWrapper().getValue(obj, FormatType.LOADER);
        }
        return this.getResultsFormatterWrapper().getValue(obj);
    }
     
    private boolean isXMLType(Object obj){
    	if (obj instanceof OPAQUE) {
         	try {
        		OPAQUE opaque = (OPAQUE) obj;
        		 if (opaque.getSQLTypeName().trim().equals("SYS.XMLTYPE")){
        			 return true;
        		 }
        	} catch (Exception e){
        		// just don't recognize it as XMLType
        	}
    	}
    	return false;
    }
    private String getNewFileName(String ext){
    	return getNewFileName(ext,GuidGen.toString(GuidGen.uuidCreate()).replaceAll("\\" + ldr_delim, "_"));//$NON-NLS-1$ //$NON-NLS-2$
    }
    private String getNewFileName(String ext, String opfile){
    	String fileName = getFileName();
    	if (fileName==null) return "";
    	int extIndex = fileName.lastIndexOf(".");
    	String baseFileName = extIndex != -1?fileName.substring(0,extIndex):  // keep the dot
    		fileName;
    	//I was going to have if windows do this if unix do the other
    	//but just append guid is ok too.
    	int spin = 0;
    	fileName = baseFileName + opfile + "." + ext;
    	if (!new File(fileName).exists()) return fileName;
    	
    	while (new File(fileName).exists()) { 
    		spin++;
    		fileName = baseFileName + opfile + "_" + spin + "." + ext;
    	}
    	return fileName; 
    }   
    
    private Writer getNewWriter(String file){
    	FileOutputStream fos = null;
		OutputStreamWriter osw = null;
		BufferedWriter bw = null;
    	try {
    		fos = new FileOutputStream(file);
    		try {
    			osw = new OutputStreamWriter(fos, getEncode());
    		} catch (UnsupportedEncodingException e1) {
    			LOGGER.log(Level.WARNING, e1.getStackTrace()[0].toString(), e1);
    			osw = new OutputStreamWriter(fos);
    		}
    		bw = new BufferedWriter(osw);
    		
    	} catch (FileNotFoundException e) {
    		LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    	}	
    	return bw;
    }
    
    // writeFile
    // Writes the file using the specified reader
    // returns true if nothing is written to the file (ie. the data is null)
    
    private boolean writeFile(String file, Reader reader ){
		Writer writer = getNewWriter(file);
		boolean isNull = true;
		if (writer!=null){
			try {
				char[] buffer = new char[4096];
				int length = 0;
				while ((length = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, length);
					isNull=false;
				}
			} catch (FileNotFoundException e) {
				LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
			} finally {
				try {
					writer.close();
				} catch (IOException e) {
				}
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
					}
				}
			}
		}
		return isNull;
    }
/*    private void writeFile(String file, Reader reader ){
    	FileOutputStream fos = null;
		OutputStreamWriter osw = null;
		BufferedWriter bw = null;
    	try {
    		fos = new FileOutputStream(file);
    		try {
    			osw = new OutputStreamWriter(fos, getEncode());
    		} catch (UnsupportedEncodingException e1) {
    			LOGGER.log(Level.WARNING, e1.getStackTrace()[0].toString(), e1);
    			osw = new OutputStreamWriter(fos);
    		}
    		bw = new BufferedWriter(osw);
    		char[] buffer = new char[4096];
    		int length = 0;
    		while ((length = reader.read(buffer)) != -1) {
    			bw.write(buffer, 0, length);
    		}
    	} catch (FileNotFoundException e) {
    		LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    	} catch (IOException e) {
    		LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    	} finally {
    		if (bw != null) {
    			try {
    				bw.close();
    			} catch (IOException e) {
    			}
    		}
    		if (osw != null) {
    			try {
    				osw.close();
    			} catch (IOException e) {
    			}
    		}
    		if (reader != null) {
    			try {
    				reader.close();
    			} catch (IOException e) {
    			}
    		}
    	}
    }
*/
    /*public void close() {
    	super.close();
        try {
        	end();
        } catch (IOException e){
        	LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
        }
    }*/
    
   
	// At this point we have created the control file which we let
	// GenerationOutputStream handle.
	// We have also created zero or more files for blobs/clobs which we have
	// handled.
	//
	// For !isSeparateDataFile, we have included the syntax for the data in the
	// control file but otherwise
	// we still need to save the data file.
	//
	// if using compression add the data file and any
	// blob/clob files to the zip.

	// Note we always used to create a control file that contained both the
	// control info and the data, but after learning of row length limitations
	// with that, we can generate
	// them in separate files as long as single file is not selected.

	// All these extra files are hidden (bad) from generationOutputStream.
	// And we had ot add a direct interface to ExportGenerationOutputStream to add the .ctl file
	// 	to the control file.

	public void end() throws IOException {
		if (!rowWritten) {
			return;
		}
		String controlFileName = getNewFileName(EXT,"");
		if (numRows > 500) {
			numRows = numRows/10;
		} else {
			numRows=50;
		}
		String errors = "OPTIONS (ERRORS=" + numRows.toString()+")"+_ctlEOLChars; //$NON_NLS-1$ //$NON-NLS-2 
		if (_zipper == null) {
			if (!controlFileName.equals("")) {
				Writer ctlWriter = getNewWriter(controlFileName);
				addToControllingFile(controlFileName);
				ctlWriter.write(errors);
				ctlWriter.write(header.toString());
				ctlWriter.close();
			}		
			
		} else if (_zipper != null) {
			
			// The data file is complete now
			_zipper.closeEntry();
			_zipper.openEntry(controlFileName); // open entry for control file
			write (errors);
			write(header.toString());
			_zipper.closeEntry();

			// Then add all the lob files we created.  But we will not put these in the controlling file.
			for (String s : _createdFiles) {
				// _zipper.openEntry(s);
				_zipper.writeFileEntry(s);
				File file = new File(s); // don't need anymore...
				file.delete();
			}
		}

		// Separate data file which will be handled here instead of
		// GenerationOutputStream
		// But first write the .ctl file...
/*		write(header.toString()); 

//		if (_zipper != null) {
//			_zipper.closeEntry();
//			_zipper.openEntry(sepDataFileName); // open entry for data file
//		} else {
//			_sepDataWriter = getNewWriter(sepDataFileName); // just simple
															// writes
//		}
		*/

		_createdFiles = new ArrayList<String>();

	}
    
    // At this point we have created the base data file and zero or more files for blobs/clobs.
    // If we are using compression (zipper), then the base data file is already added.
    //
    // For !isSeparateDataFile, we have included the syntax for the control file with the data file, but otherwise
    // we still need to save the control file.
    //
    // if using compression add the control file and any
    // blob/clob files to the zip.  
    
    // Note we always used to create a control file that contained both the
    // control info and the data, but after learning of row length limitations with that, we will generate
    // them in separate files as long as single file is not selected.  
    
    // All these extra files are hidden (bad) from generationOutputStream.
    
/*    public void OLDend() throws IOException {
    	String ctlFile = getNewControlFileName(); //$NON-NLS-1$

    	if(_zipper == null && isSeparateDataFile() ){
    		writeFile(ctlFile,new StringReader(header.toString()));
    	} else {
    		// We assume that the data file is complete before this
    		// is done because the open entry will close the entry for it
    		// First do the control file
    		if (isSeparateDataFile()){
    			_zipper.openEntry(ctlFile);
    			_zipper.writeEntryCharStream(new StringReader(header.toString()));
    		}
    		// Then add all the lob files we created.
    		for (String s : _createdFiles) {
    			//_zipper.openEntry(s);
    			_zipper.writeFileEntry(s); 
    			File file = new File (s);	// don't need anymore...
    			file.delete();
    		}
    		_createdFiles = new ArrayList<String>();
    	}

    }
*/

    public Boolean getPromptForTable() {
        return true;
    }

    public boolean isLineTerminatorSupported() {
        return true;
    }
    
	public boolean isTableNameSupported(){
		return true;
	}

    public boolean isDelimiterSupported() {
        return true;
    }

    public boolean isDelimiterConfigurable() {
        return true;
    }

    public boolean isEnclosuresSupported() {
        return true;
    }

    public String getLineTerminatorConfigKey() {
        return KEY_REC_TERM;
    }

    public String getDelimiterConfigKey() {
        return KEY_DELIMITER;
    }

    // The single quote is special character for loader so add backslash
    public String handleSpecialEnclosure(String value){
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0; i < value.length();++i){
    		char thisChar=value.charAt(i);
    		if (thisChar == '\''){
    			sb.append('\\');
    			sb.append(thisChar);		
    		} else {
    			sb.append(thisChar);
    		}
    	}
    	return sb.toString();
    }
    
    public String getEnclosuresConfigKey() {
        return KEY_ENCLOSURES;
    }

    public String getEnclosureLeftConfigKey() {
        return KEY_ENCL_LEFT;
    }

    public String getEnclosureRightConfigKey() {
        return KEY_ENCL_RIGHT;
    }
    // For SQL*Loader, Enclosure Right is always doubled if embedded in data
    // show its doubled, but don't let user configure it..
    public boolean isEnclosureRightDoubleConfigurable(){
    	return false;
    }
    public String getEnclosureRightDoubleConfigKey() {
        return KEY_ENCL_RIGHT_DOUBLE;
    }
    
    public boolean allowLobs(){
    	return true;
    }
    // Not really readable because now we generate separate files
    public boolean isTextEditorReadable() {
        return false;
    }
    
    public boolean isSeparateDataFileSupported() {
        return true;
    }
    public String getSeparateDataFileConfigKey() {
        return KEY_SEPARATE_DATA_FILE;
    }
    public void isSeparateDataFile(boolean value){
    	_isSeparateDataFile=value;
    }
    public boolean isSeparateDataFile(){
    	return _isSeparateDataFile;
    }
    public Boolean isCandidateForSpoolMax() {
  	  return true;
    }
}
