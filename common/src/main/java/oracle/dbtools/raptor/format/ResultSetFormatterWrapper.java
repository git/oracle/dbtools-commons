/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;


import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.ResultSetFormatter;
import oracle.dbtools.raptor.backgroundTask.IRaptorTaskProgressUpdater;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.utils.ExceptionHandler;
import oracle.jdbc.OracleResultSet;

public class ResultSetFormatterWrapper extends ResultsFormatterWrapper {
    private ResultSet m_rs;
    private ResultSetMetaData m_meta;
    private String m_sql;
    private int[] m_dataTypes;
    private ObjectType m_objectType = ObjectType.PROPRIETARY;
    private int _maxRows = 0;

    /*
     * Constructors require the result set
     *
     * Optionally called with metadata details.
     *
     */
    //------------------------------------------------------------------------------------------------------
    // vasan: Following members are used only when export of a ResultSetTable is attempted.  Use it with care.
    private ArrayList<Integer> m_exportCols = null;
    //-------------------------------------------------------------------------------------------------------
    protected ResultSetFormatterWrapper() {
        m_objectType = ObjectType.PROPRIETARY;
    }

    public ResultSetFormatterWrapper(ResultSet rs,int maxRows) {
        this(rs, ObjectType.PROPRIETARY);
        _maxRows = maxRows;
    }

    public ResultSetFormatterWrapper(ResultSet rs, ObjectType objectType) {
        setResultSet(rs ,objectType);
    }

    /*
     */
    public ResultSetFormatterWrapper(ResultSet rs,
    		ArrayList<Integer> exportCols,
    		List<String> exportColNames,
    		int [] exportSizes,
    		int [] exportDataTypes,
    		String sql)
    {
    	m_rs = rs;
    	m_sql=sql;
       m_exportCols = exportCols;
       setColumnNames(exportColNames);
       setColumnSizes(exportSizes);
       setPrecisions(exportSizes);
       m_dataTypes=exportDataTypes;
    }

    public ResultSetFormatterWrapper(ResultSet rs, ArrayList<Integer> exportCols, String sql)
    {
       m_sql=sql;
       m_exportCols = exportCols;
   	   setResultSet(rs);
    }

    public ResultSetFormatterWrapper(ResultSet rs, ArrayList<Integer> exportCols)
    {
       m_exportCols = exportCols;
   	   setResultSet(rs);
    }

    /*
     * setResultSet
     *
     * Save the result set and set the column properties
     * Note that colsize (for historical reasons is the default size
     * of the column as it appears in a table.
     * Added later, precision and scale which we hope will reflect
     * the size (or largest possible size for var fields) to provide
     * more accurate exports/generations.
     *
     */
    protected void setResultSet(ResultSet rs) {
        m_rs = rs;
        String columnName = ""; //$NON-NLS-1$
        try {
            m_meta = m_rs.getMetaData();
            int[] colSizes = (m_exportCols == null)? new int[ m_meta.getColumnCount() + 1 ]:new int[ m_exportCols.size() + 1 ];
            int[] precisions = (m_exportCols == null)? new int[ m_meta.getColumnCount() + 1 ]:new int[ m_exportCols.size() + 1 ];
            int[] scales = (m_exportCols == null)? new int[ m_meta.getColumnCount() + 1 ]:new int[ m_exportCols.size() + 1 ];

            List<String> colNames = new ArrayList<String>();

            int colCount = 0;
            if (m_exportCols == null)
            {
            	colCount = m_meta.getColumnCount();
            }
            else
            {
            	colCount = m_exportCols.size();
            }

            for (int i = 0; i < colCount; i++)
            {
            	if (m_exportCols == null)
            	{
	                columnName = m_meta.getColumnName(i+1);
	                colNames.add(columnName);
	                switch (m_meta.getColumnType(i+1)) {
	                    case 91:
	                    case 92:
	                    case 93:
	                        colSizes[ i ] = 25;
	                        break;
	                    default:
	                        colSizes[ i ] = columnName.length();
	                }

	                scales[ i ] = m_meta.getScale(i+1);
	                precisions[ i] = m_meta.getPrecision(i+1);
            	}
            	else
            	{
	                columnName = m_meta.getColumnName(m_exportCols.get(i)+1);
	                colNames.add(columnName);
	                switch (m_meta.getColumnType(m_exportCols.get(i)+ 1)) {
	                    case 91:
	                    case 92:
	                    case 93:
	                        colSizes[ i ] = 25;
	                        break;
	                    default:
	                        colSizes[ i ] = columnName.length();
	                }

	                scales[ i ] = m_meta.getScale(m_exportCols.get(i)+1);
            		precisions[ i ] = m_meta.getPrecision(m_exportCols.get(i)+1);
            	}
            }

            setColumnNames(colNames);
            setColumnSizes(colSizes);
            setScales(scales);
            setPrecisions(precisions);
        } catch (SQLException e) {
            ExceptionHandler.handleException(e);
        }
    }
    /*
     * setResultSet
     *
     * Save the result set and set the column properties
     * Note that colsize (for historical reasons is the default size
     * of the column as it appears in a table.
     * Added later, precision and scale which we hope will reflect
     * the size (or largest possible size for var fields) to provide
     * more accurate exports/generations.
     *
     */
    protected void setResultSet(ResultSet rs, ObjectType objectType) {
        m_objectType = objectType;
        setResultSet(rs);
    }

    @Override
    /*
     * get the connection from the result set
     */
    public Connection getConnection() {
        Connection conn = null;
        try {
            conn = m_rs.getStatement().getConnection();
        } catch (SQLException ex) {

        }
        return conn;
    }

    @Override
    public int getDataType(int col) throws SQLException {
    	//vasan: I always expect 0 based calls from the respective formatters.  Hence increase the offset by 1
    	//fix bug:Bug 8775448 - DATABASE EXPORT> EXPORTED FILE IS NOT CORRECT
    	if (m_exportCols == null) {
    		String colTypeName = m_meta.getColumnTypeName(col+1);
    		return m_meta.getColumnType(col + 1);
    	} else if (m_dataTypes==null){
    		return m_meta.getColumnType(m_exportCols.get(col) + 1);
    	} else {
    		return m_dataTypes[col];
    	}
    }
    
    @Override
    public String getDataTypeName(int col) throws SQLException {
    	if (m_exportCols == null) {
    		String colTypeName = m_meta.getColumnTypeName(col+1);
    		return colTypeName;
    	} else if (m_dataTypes==null){
    		return m_meta.getColumnTypeName(m_exportCols.get(col) + 1);
    	} else {
    		return m_meta.getColumnTypeName(m_dataTypes[col]+1);
    	}
    }


    @Override
    public int print() throws IOException {
        int ret = 0;
        try {
            ret = print(null);
        } catch ( ExecutionException e ) {

        }
        return ret;
    }

    public String getSQL() {
        return m_sql;
    }


    @Override
    public int print(IRaptorTaskProgressUpdater updater) throws IOException, ExecutionException {
        int ret = 0;
        m_formatter.start();
        try {
          m_rowNum=0;
            while (m_rs != null && (
            		(m_formatter.isCandidateForSpoolMax()&&
            				ResultSetFormatter.replaceNext(m_rowNum, _maxRows, m_formatter.getScriptContext(), m_formatter.getOutputStream(), m_rs, null, m_formatter)
            				)
            		||(!m_formatter.isCandidateForSpoolMax()&&m_rs.next()))&& (_maxRows ==0 ||
            		(m_rowNum <_maxRows)||ResultSetFormatter.maxRowsSpoolOnly(m_formatter.getScriptContext()))) {
                if ( updater != null ) {
                    updater.checkCanProceed();
                } else if ( isInterrupted()) {
                	m_rs.getStatement().cancel();
                    break;
                }
                m_rowNum++;
                m_formatter.log(Messages.getString("ResultSetFormatterWrapper.1") + m_rs.getRow()); //$NON-NLS-1$
                m_formatter.startRow();
                Object o = null;
                if (m_exportCols == null)
                {
	                for (int ii = 0; ii < m_meta.getColumnCount(); ii++) {
	                	if (m_objectType == ObjectType.PROPRIETARY && m_rs instanceof OracleResultSet) {
	                		try {
	                        o = ScriptUtils.getOracleObjectWrap((OracleResultSet) m_rs,ii + 1);
	                		} catch (SQLException e) {
	                			o =m_rs.getObject(ii+1);
	                		}
	                    } else {
	                        o = m_rs.getObject(ii + 1);
	                    }
	                	//m_formatter.printColumn(m_rs.getObject(ii + 1), ii, ii);

	                	m_formatter.printColumn(o, ii, ii);
	                }
                }
                else
                {
                	for (int ii=0; ii < m_exportCols.size(); ii++){
	                	if (m_objectType == ObjectType.PROPRIETARY && m_rs instanceof OracleResultSet) {
	                        o = ScriptUtils.getOracleObjectWrap((OracleResultSet) m_rs, m_exportCols.get(ii)+1);
	                    } else {
	                        o = m_rs.getObject(m_exportCols.get(ii)+1);
	                    }
                		//m_formatter.printColumn(m_rs.getObject(m_exportCols.get(ii)+1), ii, ii);
	                	m_formatter.printColumn(o, ii, ii);
                	}
                }
                m_formatter.endRow();
                ret = m_rs.getRow();
            }
        }
        catch(ExecutionException e)
        {
        	try
        	{
	        	if (m_rs != null)
	        		m_rs.getStatement().cancel();
        	}
        	catch(Exception ex)
        	{
        		ExceptionHandler.handleException(ex);
        	}
        }
        catch (SQLException e) {
            ExceptionHandler.handleException(e);
        } catch (IOException e) {
            ExceptionHandler.handleException(e);
        }

        m_formatter.end();
        return ret;
    }

    /*
     * (non-Javadoc)
     * @see oracle.dbtools.raptor.format.ResultsFormatterWrapper#close()
     * close the resultset
     */
    public void close() {
        if (m_rs != null) {
        	DBUtil.closeResultSet(m_rs);

        }
    }

    public static enum ObjectType {
        GENERIC,
        PROPRIETARY;
    }
}
