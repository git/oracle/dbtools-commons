/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.backgroundTask.IRaptorTaskProgressUpdater;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.utils.NLSUtils;
import oracle.dbtools.util.ZipOutputHandler;

public abstract class ResultsFormatter implements IResultFormatter {
  private static final Logger LOGGER = Logger.getLogger(ResultsFormatter.class.getName());

  protected final int FLUSH_EVERY_N_WRITE = 20;

  public static final int CONTINUE = 0;

  public static final int CANCEL_WITH_MSG = 1;

  public static final int CANCEL = 2;

  protected int _writeTracker = 0;

  private ResultsFormatterWrapper m_wrapper;

  protected int _format;

  protected Writer _out;

  protected ZipOutputHandler _zipper;

  /**
   * An array list of the files that need to be added to the controlling file.
   * These files were added by the formatters. Some work files (eg clob files)
   * may be created by the formatters but not added to the control file.
   */
  private ArrayList<String> m_addToControllingFile = new ArrayList<String>();;

  protected String _type;

  protected String _filter;

  protected String _ext;

  protected Boolean _prompt = false;

  protected Object _dbObject;

  protected String _EOLChars;

  protected String _EOLCharsString;

  protected String _delimiter;

  protected boolean _isEnclosed = true;

  protected String _enclosureLeft = "\""; //$NON-NLS-1$

  protected String _enclosureRight = "\""; //$NON-NLS-1$

  protected String _replacableEnclosureRight = _enclosureRight;

  protected boolean _isEnclosureRightDoubled = true;

  protected String _nullValue = ""; // NORES //$NON-NLS-1$

  protected String _validationMessage;

  public Object getDbObject() {
    return _dbObject;
  }

  public void setDbObject(Object dbobject) {
    _dbObject = dbobject;
  }

  private OutputStream _outStream;

  private String _enc;

  private String _fileName;

  private boolean _isDeployCloud;

  private IRaptorTaskProgressUpdater _updater;

  private ScriptRunnerContext _scriptCtx;

  //
  //
  public ResultsFormatter(String type, String filter, String ext) {
    _type = type;
    _ext = ext;
    _filter = filter;
  }
  
  final private static String MIME ="text/plain";
  public String getMimeType(){
    return MIME;
  }
  
  // This method is called at the beginning of an export. It is only called once
  // even when multiple objects are being exported. Override
  // to do startup processing.
  public void startExport() {

  }

  // This method is called at the end of an export. It is only called once
  // even when multiple objects are being exported. Override
  // to do cleanup processing.
  public void finishExport() {

  }

  // Indicate if the format allows filtering the column list. If this flag
  // is false, the list of columns can not be filtered and will therefore
  // be all of the columns in the table. Copy and Datapump do not allow
  // filtering the column list.
  public boolean isColumnListFilterable() {
    return true;
  }

  // Indicated if the formatted output can be read by a text editor.
  // Known usage is to determine if the file can be opened in a
  // worksheet at end of export.
  public boolean isTextEditorReadable() {
    return true;
  }

  // True if the standard list of encodings is to be used. Otherwise, false
  // and formatter must return a list of valid encodings
  // Known usage is for TimesTen to restrict encodings to UTF-8
  public boolean isStandardEncodings() {
    return true;
  }

  public String[] getEncodings() {
    return null;
  }

  public String getFileName() {
    return _fileName;
  }

  public void setFileName(String fileName) {
    _fileName = fileName;
  }

  public String getEncode() {
    return _enc;
  }

  public void setEncode(String enc) {
    _enc = enc;
  }

  public boolean isDeployCloud() {
    return _isDeployCloud;
  }

  public void isDeployCloud(boolean isDeployCloud) {
    _isDeployCloud = isDeployCloud;
  }

  public Boolean getPromptForTable() {
    return _prompt;
  }

  public void setPromptForTable(Boolean prompt) {
    _prompt = prompt;
  }

  public String getType() {
    return _type;
  }

  public String getExt() {
    return _ext;
  }

  public String getFilter() {
    return _filter;
  }

  public void setDataProvider(IResultsFormatterWrapper wrapper) {
    m_wrapper = (ResultsFormatterWrapper) wrapper;
    m_wrapper.setParent(this);
  }

  protected ResultsFormatterWrapper getWrapper() {
    return m_wrapper;
  }

  protected String getColumnName(int col) {
    return m_wrapper.getColumnName(col);
  }

  protected int getColumnSize(int col) {
    return m_wrapper.getColumnSize(col);
  }

  protected int getPrecision(int col) {
    return m_wrapper.getPrecision(col);
  }

  protected int getScale(int col) {
    return m_wrapper.getScale(col);
  }

  protected int getColumnCount() {
    return m_wrapper.getColumnCount();
  }

  public void print(Writer out) {
    print(out, null);
  }

  public void print(Writer out, IRaptorTaskProgressUpdater taskUpdater) {
    _out = out;
    _updater = taskUpdater;
    print();
  }

  // Some formatters may need to close the stream, beware that single file
  // export must be disabled when exporting ddl and data for these formats.
  public boolean isStreamHandler() {
    return false;
  }

  public void flushStream() {
  }

  public void closeStream() {
  }

  // validateOptions gives formatter the opportunity to validate options
  // Overide this method to implement formatter validation.
  //
  /*
   * Validate the target save as type and save as (file or directory)
   * 
   * Returns an action code: CONTINUE: The validation is successful, no
   * additional action needed. CANCEL_WITH_MSG: Do not continue, msg contains
   * details of error CANCEL: Do not continue, user has requested cancel.
   * 
   * Messages are returned via getValidationMessage
   */
  public int validateOptions() {
    return CONTINUE;
  }

  // Get the validation Message from the validateSaveAs
  //
  public String getValidationMessage() {
    return this._validationMessage;
  }

  // Get the validation Message from the validateSaveAs
  //
  public void setValidationMessage(String value) {
    _validationMessage = value;
  }

  public boolean allowsLobs() {
    return false;
  }

  public boolean isHeaderOptionSupported() {
    return false;
  }

  public void isHeader(boolean isHeader) {
  }

  public boolean isHeader() {
    return false;
  }

  public String getHeaderConfigKey() {
    return ""; //$NON-NLS-1$
  }

  /**
   * Is a separate data file requested for the object?
   * 
   * Note for sqlloader format, the preferred method of generation is to have
   * the control file and data generated as separate files. However, if user
   * requests a separate file, we will include the data with the control file.
   * (Further note, for some datatypes separate files are generated for each
   * column and these files are not managed centrally (bad).
   * 
   * @return
   */
  public boolean isSeparateDataFileSupported() {
    return false;
  }

  public boolean isSeparateDataFile() {
    return false;
  }

  public void isSeparateDataFile(boolean value) {
  }

  public String getSeparateDataFileConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public boolean isCommitSupported() {
    return false;
  }

  public boolean isCommit() {
    return false;
  }

  public int getCommitRows() {
    return 0; //$NON-NLS-1$
  }

  public void setCommitRows(int value) {
  }

  public void isCommit(boolean value) {
  }

  public String getCommitConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public String getCommitRowsConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public boolean isLineTerminatorSupported() {
    return false;
  }

  public void setLineTerminator(String terminator) {
    _EOLChars = terminator;
  }

  public void setLineTerminatorString(String terminator) {
    _EOLCharsString = terminator;
  }

  public String getLineTerminatorString() {
    return _EOLCharsString;
  }

  public String getLineTerminatorConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public boolean isDelimiterSupported() {
    return false;
  }

  public boolean isDelimiterConfigurable() {
    return false;
  }

  // Extenders return the default delimiter if it is not configurable.
  public String getDefaultDelimiter() {
    return ""; //$NON-NLS-1$
  }

  public String getDelimiter() {
    return _delimiter;
  }

  public void setDelimiter(String delimiter) {
    _delimiter = delimiter;
  }

  public String getDelimiterConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public boolean isEnclosuresSupported() {
    return false;
  }

  public void setEnclosures(boolean isEnclosed, String enclosureLeft, String enclosureRight, boolean isEnclosureRightDoubled) {
    _isEnclosed = isEnclosed;
    _enclosureLeft = enclosureLeft;
    _enclosureRight = enclosureRight;
    _replacableEnclosureRight = handleSpecialEnclosure(enclosureRight);
    _isEnclosureRightDoubled = isEnclosureRightDoubled;;
  }

  public void isEnclosed(boolean isEnclosed) {
    _isEnclosed = isEnclosed;
  }

  public boolean isEnclosed() {
    return _isEnclosed;
  }

  public String getEnclosureLeft() {
    return _enclosureLeft;
  }

  public void setEnclosureLeft(String enclosureLeft) {
    _enclosureLeft = enclosureLeft;
  }

  public String getEnclosureRight() {
    return _enclosureRight;
  }

  public void setEnclosureRight(String enclosureRight) {
    _enclosureRight = enclosureRight;
  }

  public String getEnclosuresConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public String getEnclosureLeftConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public String getEnclosureRightConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public String getEnclosureRightDoubleConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public boolean isEnclosureRightDoubleConfigurable() {
    return true;
  }

  public boolean isTableNameSupported() {
    return false;
  }

  public boolean isDataNameSupported() {
    return false;
  }

  public String getDataName() {
    return ""; //$NON-NLS-1$
  }

  public void setDataName(String name) {
  }

  public String getDataNameConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public boolean isQuerySupported() {
    return false;
  }

  public boolean isQuerySaved() {
    return false;
  }

  public void isQuerySaved(boolean value) {
  }

  public String getQueryName() {
    return ""; //$NON-NLS-1$
  }

  public void setQueryName(String name) {
  }

  public String getQueryConfigKey() {
    return ""; //$NON-NLS-1$
  }

  public String getQueryNameConfigKey() {
    return ""; //$NON-NLS-1$
  }

  // isTruncate currently used by DBCart. There are no preferences for is yet,
  // so no ConfigKey etc.
  // Option is not yet visible in Export, but can be added.
  public boolean isTruncateSupported() {
    return false;
  }

  public boolean isTruncate() {
    return false;
  }

  public void isTruncate(boolean value) {
  }

  // public void printWithDialog() {
  // final ResultsFormatter finalFormatter = this;
  // // final DataOutputStream out = _out;
  // final ProgressRunnable runner = new ProgressRunnable() {
  //
  // protected void doCancel() {
  // // call super cancel
  // super.doCancel();
  // // set interrupt
  // finalFormatter.interrupt();
  // }
  //
  // @Override
  // protected Object doWork() throws Exception {
  // finalFormatter.print();
  // if (_out!=null){
  // _out.close();
  // }
  // return null;
  // }
  // };
  // //
  // // provide feedback
  // final IProgressFeedback feedback = new IProgressFeedback() {
  //
  // public void feedback(String s) {
  // Log.status(s);
  // }
  // };
  // //
  // // setfeedback
  // finalFormatter.setFeedback(feedback);
  // // set the title
  //        runner.setTitle(Messages.getString("ResultsFormatter.14")); //$NON-NLS-1$
  // // allow a cancel
  // runner.setCancelable(true);
  // // start the export
  // try {
  // runner.start(true);
  // } catch (ProgressException e) {
  // // export was cancelled
  // LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
  // }
  // }

  public int print() {
    int ret = 0;
    try {
      if (_updater != null)
      ret =  m_wrapper.print(_updater);
      else
      ret = m_wrapper.print();

    } catch (ExecutionException e1) {
      if (e1.getCause() instanceof CancellationException)
        LOGGER.log(Level.INFO, Messages.getString("ResultsFormatter.15")); //$NON-NLS-1$
      else
        LOGGER.log(Level.WARNING, e1.getStackTrace()[0].toString(), e1);
      // e1.printStackTrace();
    } catch (Exception e) {
      // Errors during export should only be logged
      LOGGER.log(Level.WARNING, e.getMessage(), e);
    } finally {
      if (_out != null)
        try {
          _out.flush();
        } catch (IOException e) {
        }
    }
    return ret;
  }

  protected void write(String s) throws IOException {
    if (s != null) {
      // The SQL History is written and read in UTF-8 encoding.
      // In this way all characters typed into the sql worksheet can be saved in
      // a standard format
      // and not limited to the JDev specified encoding. Users should not be
      // view/editing the sqlhistory file directly.
      if (_zipper != null) {
        _zipper.writeEntryText(s);
      } else if (_out != null) {
        _out.write(s);
      }
      checkAndFlush();

    }

  }

  protected void checkAndFlush() throws IOException {
    if (_writeTracker < FLUSH_EVERY_N_WRITE) {
      _writeTracker++;
    } else {
      if (_out != null) {
        _out.flush();
      }
      _writeTracker = 0;
    }
  }

  /*
   * Add a file to the list of files that should be included in the controlling
   * file generated by the export. These files were added manually by the
   * formatter and are not yet
   */
  protected void addToControllingFile(String fileName) {
    m_addToControllingFile.add(fileName);
  }

  /**
   * getAddToControllingFile get the list of files that should be included in
   * the controlling file generated by the export. These files were added
   * manually by the formatter and are not yet known to
   * ExportGenerationOutputStream..
   * 
   * @return - list of files to add
   */
  public ArrayList<String> getAddToControllingFile() {
    ArrayList<String> rtn = m_addToControllingFile;
    m_addToControllingFile = new ArrayList<String>();
    return rtn;
  }

  public void log(String s) {
    if (_updater != null)
      _updater.getDescriptor().setMessage(s);
  }

  public Writer getOutWriter() {
    return _out;
  }

  // Allow user to handle own Writer
  // This allows export to control all of that fancy file handling
  public void setOutWriter(Writer out) {
    _zipper = null;
    _out = out;
  }

  public OutputStream getOutputStream() {
    return _outStream;
  }

  public void setOutputStream(OutputStream out) {
    _outStream = out;
    // If caller did not set a Writer, create one now.
    if (_out == null) {
      if (_enc != null) {
        try {
          _out = new OutputStreamWriter(out, _enc);
        } catch (UnsupportedEncodingException e) {
          _out = new OutputStreamWriter(out);
        }
      }
    }
  }

  public void setZipper(ZipOutputHandler zipper) {
    _zipper = zipper;
    _out = null;
  }

  public ZipOutputHandler getZipper() {
    return _zipper;
  }

  public void setUpdater(IRaptorTaskProgressUpdater updater) {
    _updater = updater;
  }

  public void interrupt() {
    // _interrupt = true;
  }

  public ResultsFormatterWrapper getResultsFormatterWrapper() {
    return m_wrapper;
  }

  protected Object getValue(Object obj) {
    return m_wrapper.getValue(obj);
  }

  public int getDataType(int colIdx) {
    int ret = -1;
    try {
      // if (_table != null && _table.getModel() instanceof ResultSetTableModel)
      // {
      // ret = ((ResultSetTableModel) _table.getModel()).getType(colIdx - 1);
      // } else if (_rs != null) {
      // ret = _rs.getMetaData().getColumnType(colIdx);
      // }
      ret = m_wrapper.getDataType(colIdx);
    } catch (SQLException e) {
    }
    return ret;
  }
  
  public String getDataTypeName (int colIdx) {
	  String ret = "";
	  try {
		  ret = m_wrapper.getDataTypeName(colIdx);
	  } 
	  catch (SQLException e) {
	  }
	  return ret;
  }


  public String getTimeStampFormat() {
    return NLSUtils.getTimeStampFormat(m_wrapper.getConnection());
  }

  public String getTimeStampTZFormat() {
    return NLSUtils.getTimeStampWithTimeZoneFormat(m_wrapper.getConnection());
  }

  public String getDateFormat() {
    return NLSUtils.getDateFormat(m_wrapper.getConnection());
  }

  public String getFormattedDATE(Object obj) {
    return NLSUtils.format(m_wrapper.getConnection(), obj);
  }

  public String enclosureRightDouble(String value) {
    return value.replaceAll(_replacableEnclosureRight, _replacableEnclosureRight + _replacableEnclosureRight);
  }

  // The paren is special character for regex so add backslash
  public String handleSpecialEnclosure(String value) {
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < value.length(); ++i) {
      char thisChar = value.charAt(i);
      if (thisChar == ')') {
        sb.append('\\');
        sb.append(thisChar);
      } else {
        sb.append(thisChar);
      }
    }
    return sb.toString();
  }

  public Connection getConnection() {
    return m_wrapper.getConnection();
  }

  public String getLineTerminator() {
    if (_EOLChars == null) {
      _EOLChars = FormatRegistry.getLineTerminator();
    }
    return _EOLChars;
  }

  public boolean doPrint(IRaptorTaskProgressUpdater taskProgressUpdater) {
    final ResultsFormatter finalFormatter = this;
    boolean success = false;
    try {
      finalFormatter.setUpdater(taskProgressUpdater);
      if (taskProgressUpdater != null) {
        taskProgressUpdater.checkCanProceed();
      }
      finalFormatter.print();
      // _out.close();
      // } catch (IOException e) {
      // Logger.getLogger(getClass().getName()).log(Level.WARNING,
      // e.getStackTrace()[ 0 ].toString(), e);
    } catch (ExecutionException e1) {
      if (e1.getCause() instanceof CancellationException)
        LOGGER.log(Level.INFO, Messages.getString("ResultsFormatter.15")); //$NON-NLS-1$
      else
        LOGGER.log(Level.WARNING, e1.getStackTrace()[0].toString(), e1);
      // e1.printStackTrace();
    }
    return success;
  }

  public String cleanString(String str) {
    StringBuilder newStr = new StringBuilder(""); //$NON-NLS-1$
    Character ch;
    Character qt = '\"';
    for (int i = 0; i < str.length(); i++) {
      ch = str.charAt(i);
      if (ch.equals(qt))
        newStr = newStr.append("\"\""); //$NON-NLS-1$
      else
        newStr = newStr.append(ch);
    }
    return newStr.toString();
  }

  public void close() {
    if (m_wrapper != null) {
      m_wrapper.close();
    }
  }
  public void setScriptContext(ScriptRunnerContext scriptRunnerContext){
    _scriptCtx = scriptRunnerContext;
  }
  
  public ScriptRunnerContext getScriptContext(){
    return _scriptCtx;
  }


  @Override
  public boolean isInternalOnly() {
    //limits what shows in drop lists for choices
    return false;
  }

  public Boolean isCandidateForSpoolMax() {
	  return false;
  }
}
