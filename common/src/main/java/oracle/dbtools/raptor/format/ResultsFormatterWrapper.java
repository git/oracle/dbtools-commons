/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import oracle.dbtools.raptor.backgroundTask.IRaptorTaskProgressUpdater;
import oracle.dbtools.raptor.nls.FormatType;
import oracle.dbtools.raptor.utils.DataTypesUtil;

public abstract class ResultsFormatterWrapper implements IResultsFormatterWrapper
{
    private List<String> m_colNames;
    private int[] m_colSizes; // default size for columns in a grid
    private int[] m_precisions; // precision of columns
    private int[] m_scales; // scale of columns
    private transient boolean m_interrupted;
    protected IResultFormatter m_formatter;
    protected long m_rowNum=0;
    
    protected ResultsFormatterWrapper()
    {
    }
        
    void setParent( IResultFormatter parent )
    {
        m_formatter = parent;
    }
    
    protected void setColumnNames( List<String> names )
    {
        m_colNames = names;
    }
    
    /**
     * setColumnSizes
     * Set the default size of the columns as they should appear in a grid
     * @param sizes
     */
    protected void setColumnSizes( int[] sizes )
    {
        m_colSizes = sizes;
    }
    
    /**
     * set Precisions
     * Set the precision of the columns
     * @param sizes
     */
    protected void setPrecisions( int[] sizes )
    {
        m_precisions = sizes;
    }
    /**
     * set Scales
     * Set the scales of the columns
     * @param sizes
     */
    protected void setScales( int[] sizes )
    {
        m_scales = sizes;
    }
    
    public abstract Connection getConnection();
    
    public void close() {
        
    }
    
    public abstract int getDataType( int col ) throws SQLException;
    
    public String getDataTypeName ( int col ) throws SQLException {
    	return "";
    }
    
    public abstract int print() throws IOException;
    
    public abstract int print(IRaptorTaskProgressUpdater updater) throws IOException, ExecutionException;
    
    public String getColumnName( int col )
    {
    	try {
    		return m_colNames.get( col );
    	} catch (Exception e) {
    		return ""; //$NON-NLS-1$
    	}
    }
    /**
     * getColumnSize 
     * Get the default size that the column 
     * Assumes that the column sizes array has been set with the
     * precision of the column.
     * @param col
     * @return
     */
    public int getColumnSize( int col )
    {
        return m_colSizes[ col ];
    }
    
    /**
     * getPrecision 
     * Get the Precision of the column
     * assumes that the precisions array has been set with the
     * precision of the column, otherwise returns colSize.
     * @param col
     * @return
     */
    public int getPrecision( int col )
    {
        return m_precisions==null? m_colSizes[ col ]:m_precisions [ col ];
    }
    /**
     * getScale 
     * Get the scale of the column
     * assumes that the scale array has been set with the
     * scale of the column, otherwise returns 0.
     * @param col
     * @return
     */
    public int getScale( int col )
    {
        return m_scales==null? 0:m_scales [ col ];
    }
    
    public int getColumnCount()
    {
        return m_colNames.size();
    }
    
    public List<String> getColumnNames()
    {
        return Collections.unmodifiableList( m_colNames );
    }
    
    public void interrupt()
    {
        m_interrupted = true;
    }
    
    protected boolean isInterrupted()
    {
        return m_interrupted;
    }

    public Object getValue(Object obj)
    {
        try 
        {
            return DataTypesUtil.stringValue( obj, getConnection());
        }
        catch (Exception e) {}
        return obj;
    }
    
    public Object getValue(Object obj, FormatType formatType)
    {
        try 
        {
            return DataTypesUtil.stringValue( obj, getConnection(), formatType);
        }
        catch (Exception e) {}
        return obj;
    }
    
    public abstract String getSQL();

    public long getRowNum() {
      return m_rowNum;
    }

}
