/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

import oracle.dbtools.db.ConnectionResolver;
import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
/*
 * SQLFormatterWrapper - provides extension for ResultSetFormatterWrapper for cases
 * where string containing sql select can be provided. 
 * 
 * As example of use is for exporting a table from nav.  The appropriate sql is generated
 * by the extract framework.  ExportAPI uses SQLFormatter wrapper to create ResultSetWrapper 
 * for reading the data.
 * 
 * 
 */
public class SQLFormatterWrapper extends ResultSetFormatterWrapper {
	private Connection m_conn;
    private String m_connName;
    private String m_sql;
    
    public SQLFormatterWrapper(String conn, String sql) {
     this(conn,sql,null);
    }

    @SuppressWarnings("resource")
	public SQLFormatterWrapper(String connName, String sql,Map<String,?> bindMap) {
        m_connName = connName;
        m_sql = sql;

        Connection conn = getConnection();
        if (LockManager.lock(conn)) {
            try {
            	ResultSet rs;
              if(bindMap == null){ //do this so the exact same code path is used as we are close to production
            	rs = DBUtil.getInstance(conn).executeQuery(sql, (List) null);
              } else {
                rs = DBUtil.getInstance(conn).executeQuery(sql, bindMap);
              }
                if (rs!=null){
                	setResultSet(rs);
                }	
            } finally {
                LockManager.unlock(conn);
            }
        } else {
        	java.lang.System.out.println("Unable to get lock for connection"); //$NON-NLS-1$
        }
    }

    @Override
    public Connection getConnection() {
        /* If ConnectionResolver called everytime, the fix for Bug 24396124 
           will slow up performance by triggering an isConnectionAlive  
           call. Therefore, need to cache connection for each instance. 
        */
    	if (m_conn == null) { 
          try {
              m_conn = ConnectionResolver.getConnection(m_connName);
          } catch (Exception ex) {

          }
        }
        return m_conn;
    }

    /* 
     * SQL to select data for resultset.
     * (non-Javadoc)
     * @see oracle.dbtools.raptor.format.ResultSetFormatterWrapper#getSQL()
     */
    public String getSQL() {
        return m_sql;
    }
}
