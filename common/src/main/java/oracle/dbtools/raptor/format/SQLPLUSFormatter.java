/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.util.Arrays;

/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=SQLPLUSFormatter.java">Barry McGillin</a> 
 *
 */
public class SQLPLUSFormatter extends ResultsFormatter {

	public SQLPLUSFormatter() {
		super("SqlPlus","SqlPlus File","sql"); //$NON-NLS-1$ //$NON-NLS-3$
	}

	public void start() throws IOException {
		
		StringBuffer header = new StringBuffer();
                int count = getColumnCount() + 1;
		for (int i = 1; i < count; i++) {			
                    char[] chars = new char[ getColumnSize( i ) ];
                    Arrays.fill( chars, ' ' );
                    header.append( chars );
                    header.append("|"); //$NON-NLS-1$
		}
		write(header.toString());
		write("\n"); //$NON-NLS-1$
		for (int i = 0, ii = header.length(); i < ii; i++) {
			write("-"); //$NON-NLS-1$
		}
	}

	public void startRow() throws IOException {
		
		
	}

	public void printColumn(Object col,int viewIndex, int modelIndex) throws IOException {
            StringBuffer temp = new StringBuffer();
            if ( col != null )
            {
                temp.append( getValue(col).toString() );
            }
                
            int colSize = getColumnSize( viewIndex );
            
		if (temp.length() < colSize) {
                    char[] chars = new char[ colSize - temp.length() ];
                    Arrays.fill( chars, ' ' );
                    temp.append( chars );
		}
		temp.append( " " ); //$NON-NLS-1$
		write(temp.toString());
	}

	public void endRow() throws IOException {
		
		write("\n"); //$NON-NLS-1$
	}

	public void end() throws IOException {
		
		
	}

	public void setTableName(String tName) {
		
		
	}
	public Boolean isCandidateForSpoolMax() {
		return true;
	}

}
