/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.sql.Clob;
import java.sql.Types;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.utils.DataTypesUtil;

/**
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=TxtFormatter.java"
 *         >Barry McGillin</a>
 * 
 */
public class TxtFormatter extends ResultsFormatter {
	public static final String TYPE = "TEXT"; //$NON-NLS-1$
	public static final String EXT = "tsv"; //$NON-NLS-1$
	public static final String DEFAULT_DELIMITER = "\t"; //$NON-NLS-1$
	public static final String KEY_HEADER = "EXPORT_TXT_HEADER"; //$NON-NLS-1$
	public static final String KEY_DELIMITER = "EXPORT_TXT_DELIMITER"; //$NON-NLS-1$
	public static final String KEY_REC_TERM = "EXPORT_TXT_REC_TERM"; //$NON-NLS-1$
	public static final String KEY_ENCLOSURES = "EXPORT_TXT_ENCLOSURES"; //$NON-NLS-1$
	public static final String KEY_ENCL_LEFT = "EXPORT_TXT_ENCL_LEFT"; //$NON-NLS-1$
	public static final String KEY_ENCL_RIGHT = "EXPORT_TXT_ENCL_RIGHT"; //$NON-NLS-1$
	public static final String KEY_ENCL_RIGHT_DOUBLE = "EXPORT_TXT_ENCL_RIGHT_DOUBLE"; //$NON-NLS-1$

	private boolean _isHeader;

	public TxtFormatter() {
		super(TYPE, Messages.getString("TxtFormatter.0"), EXT); //$NON-NLS-1$
	}

	public void start() throws IOException {
		if (isHeader()) {
			int size = getColumnCount();
			for (int i = 0; i < size; i++) {
				String name = getColumnName(i);
				if (i < size - 1) {
					write(getEnclosureLeft() + name + getEnclosureRight() + getDelimiter()); //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					write(getEnclosureLeft() + name + getEnclosureRight()); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
			write(getLineTerminator());
		}
	}

	public void startRow() throws IOException {

	}

	public void printColumn(Object col, int viewIndex, int modelIndex) throws IOException {
		String colm = ""; //$NON-NLS-1$

		String lEncl = null;
		String rEncl = null;
		int type = this.getDataType(viewIndex);

		if ((type == Types.CHAR) || (type == Types.LONGNVARCHAR) || (type == Types.LONGVARCHAR)
		        || (type == Types.NCHAR) || (type == Types.NVARCHAR) || (type == Types.VARCHAR) || (type == Types.CLOB)) {
			lEncl = getEnclosureLeft();
			rEncl = getEnclosureRight();
		} else {
			lEncl = ""; //$NON-NLS-1$
			rEncl = ""; //$NON-NLS-1$
		}

		if (col != null && viewIndex == 0) {
			colm = lEncl + cleanString(getValueClobCheck(col)) + rEncl;
		} else if (col == null && viewIndex == 0) {
			colm = lEncl + rEncl;
		} else if (col != null && viewIndex > 0) {
			colm = getDelimiter() + lEncl + cleanString(getValueClobCheck(col)) + rEncl; //$NON-NLS-1$ //$NON-NLS-2$
		} else if (col == null && viewIndex > 0) {
			colm = getDelimiter() + lEncl + rEncl;
		}
		write(colm);
	}
	String getValueClobCheck(Object obj) {
        String val=null;
        if (obj instanceof Clob) {
            Integer size=null;
            if (this.getScriptContext()!=null){
                size=(Integer)(this.getScriptContext().getProperty(ScriptRunnerContext.SETLONG));
            }
            if (size!=null) {
                val=DataTypesUtil.stringValue(obj,this.getConnection(),size);
            } else {
                val=DataTypesUtil.stringValue(obj,this.getConnection());
            }
        } else {
            val=getValue(obj).toString();
        }
        return val;
	}
	public void endRow() throws IOException {
		write(getLineTerminator());
	}

	public void end() throws IOException {
	}

	public void setTableName(String tName) {
	}

	@Override
	public boolean allowsLobs() {
		return true;
	}

	public boolean isHeaderOptionSupported() {
		return true;
	}

	public void isHeader(boolean isHeader) {
		_isHeader = isHeader;
	}

	public boolean isHeader() {
		return _isHeader;
	}

	public boolean isLineTerminatorSupported() {
		return true;
	}

	public boolean isDelimiterSupported() {
		return true;
	}

	public boolean isDelimiterConfigurable() {
		return false;
	}

	public String getDefaultDelimiter() {
		return DEFAULT_DELIMITER;
	}

	public boolean isEnclosuresSupported() {
		return true;
	}

	public String getHeaderConfigKey() {
		return KEY_HEADER;
	}

	public String getLineTerminatorConfigKey() {
		return KEY_REC_TERM;
	}

	public String getDelimiterConfigKey() {
		return KEY_DELIMITER;
	}

	public String getEnclosuresConfigKey() {
		return KEY_ENCLOSURES;
	}

	public String getEnclosureLeftConfigKey() {
		return KEY_ENCL_LEFT;
	}

	public String getEnclosureRightConfigKey() {
		return KEY_ENCL_RIGHT;
	}

	public String getEnclosureRightDoubleConfigKey() {
		return KEY_ENCL_RIGHT_DOUBLE;
	}
	public Boolean isCandidateForSpoolMax() {
		return true;
	}
}
