/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.format;

import java.io.IOException;
import java.sql.Clob;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.utils.DataTypesUtil;



public class XMLFormatter extends ResultsFormatter {
    public static final String TYPE = "XML"; //$NON-NLS-1$
    public static final String EXT = "xml"; //$NON-NLS-1$
	public static final String KEY_REC_TERM = 	"EXPORT_XML_REC_TERM"; //$NON-NLS-1$

    private boolean _result = true;
	
    public XMLFormatter() {
    	super(TYPE,Messages.getString("XMLFormatter.0"),EXT);  //$NON-NLS-1$
    }
    final private static String MIME ="text/xml";
    public String getMimeType(){
      return MIME;
    }
    
    public void setResult(boolean result){
        this._result = result;
    }

	public void start() throws IOException {
		write("<?xml version='1.0'  encoding='"+getEncode()+"' ?>" + getLineTerminator()); //$NON-NLS-1$ //$NON-NLS-2$
		write("<RESULTS>" + getLineTerminator()); //$NON-NLS-1$
	}

	public void startRow() throws IOException {
		write("\t<ROW>" + getLineTerminator()); //$NON-NLS-1$
	}

	public void printColumn(Object col, int viewIndex, int modelIndex) throws IOException {                
        String name = getColumnName(viewIndex);
        write("\t\t<COLUMN NAME=\"" + name.replace(' ','_').replace('/', '_') + "\">"); //$NON-NLS-1$ //$NON-NLS-2$
  	    write("<![CDATA["); //$NON-NLS-1$
  	    String val = getValue(col);
            if ( val != null)
                write(val); 
            write("]]>"); //$NON-NLS-1$
            write("</COLUMN>" + getLineTerminator()); //$NON-NLS-1$
	}

	public void endRow() throws IOException {
		write("\t</ROW>" +getLineTerminator()); //$NON-NLS-1$
	}

	public void end() throws IOException {
		write("</RESULTS>"); //$NON-NLS-1$
	}
	
    @Override
    public boolean allowsLobs() {
        return true;
    }

	public void setTableName(String tName) {
		
		
	}
	public boolean isLineTerminatorSupported(){
		return true;
	}
    public String getLineTerminatorConfigKey(){
    	return KEY_REC_TERM;
    }
    
    
    /*
     * Provide own  getValue to handle Clob which will get the
     * entire value rather than being limited by a fixed size.
     * 
     * Unlike super.getValue - this returns a string or null 
     * 
     * return String containing value
     * 		  null to indicate no value to write (exception) or the 
     * 		  write was handled here.	
     * 
     * @see oracle.dbtools.raptor.format.ResultsFormatter#getValue(java.lang.Object)
     */
    
    protected String getValue(Object col) {
    	try {
    		if (col instanceof Clob) {
    			Clob clob = (Clob)col;
    			Long remaining = clob.length();
    			int length = 4000;
    			String chunk="";
    			for (long pos=1;remaining>0;pos = pos+chunk.length()){
    				write(chunk=clob.getSubString(pos, length));
    				remaining = remaining - chunk.length();
      			}
    			return null; // doing the write directly so no value to return.
    		} 
  		
    		Object val = super.getValue(col);
            
                return (val != null) ? val.toString() : null;
    	
    	} catch (Exception e){
    		Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING,
    				e.getStackTrace()[0].toString(), e);
    		return null;
    	}
    }
    public Boolean isCandidateForSpoolMax() {
  	  return true;
    }
}
