/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.metadata;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;

import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.db.VersionTracker;
import oracle.dbtools.raptor.query.Bind;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryUtils;
import oracle.dbtools.scripting.Script;

/**
 * Shared definition of Display Model initialised with XML.  Used by the Abstract Factory Pattern implemented
 * by XMLFactory
 * @param <DM> Concrete Display Model class
 * @param <OA> Concrete Object Action class
 */
public abstract class AbstractDisplayModel<DM extends AbstractDisplayModel<DM, OA>, OA extends AbstractObjectAction<?>>
{
    protected List<OA> m_actions = null;
    protected List<DM> m_children = null;
    protected ClassLoader m_cl = null;
    protected Connection m_conn = null;
    protected Object m_customValue = null;
    protected Version m_dbVersion = null;
    protected Script m_deleteScript = null;
    protected String m_description = null;
    protected String m_drillClass = null;
    protected Boolean m_enable = Boolean.TRUE;
    protected Map<String, String> m_extAttrs = null;
    protected Map<String, String> m_extValues = null;
    protected String m_iconName = null;
    protected Icon m_icon = null;
    protected String m_id = null;
    protected Script m_insertScript = null;
    protected String m_name = null;
    protected boolean m_noRefresh = false;
    protected DM m_parent;
    protected String m_path = null;
    protected List<Query> m_queries = null;
    protected List<Bind> m_queriesBinds = null;
    protected Map<String, Object> m_queriesBindMap = null;
    // only supported styles "multidisplay" , "masterdetail", "simple"
    protected String m_style = null;
    protected String m_tooltip = null;
    // only supported types are "vertical" and  "code" and "default" in results
    protected String m_type = null;
    protected Script m_updateScript = null;
    protected String m_connType = "Oracle";
    protected String m_objectType = null;

    /**
     * @param query
     */
    public void addQuery(Query query)
    {
        if (m_queries == null)
        {
            m_queries = new ArrayList<Query>();
        }
        m_queries.add(query);
    }

    /**
     * @param child
     */
    public void addChild(DM child)
    {
        if (getChildren() == null)
        {
            setChildren(new ArrayList<DM>());
        }
        getChildren().add(child);
    }

    /**
     * @return
     */
    public String getName()
    {
        return (m_name == null || m_name.equals("null")) ? "" : m_name;
    }

    /**
     * @param name
     */
    public void setName(String name)
    {
        m_name = name;
    }

    /**
     * @param actions
     */
    public void setActions(List<OA> actions)
    {
        m_actions = actions;
    }

    /**
     * @return
     */
    public List<OA> getActions()
    {
        return m_actions;
    }

    /**
     * @return
     */
    protected Version getDbVersion() {
        return m_dbVersion;
    }

    /**
     * @param conn
     */
    protected void setDbVersion(Connection conn) {
        m_dbVersion = conn != null ? VersionTracker.getDbVersion(DefaultConnectionIdentifier.createIdentifier(conn)) : null;
    }

    /**
     * @return
     */
    public Boolean getEnable()
    {
        return m_enable;
    }

    /**
     * @param enable
     */
    public void setEnable(String enable)
    {
        setEnable(Boolean.parseBoolean(enable));
    }

    /**
     * @param enable
     */
    public void setEnable(Boolean enable)
    {
        m_enable = enable;
    }

    /**
     * @return
     */
    public String getDescription()
    {
        return m_description;
    }

    /**
     * @param description
     */
    public void setDescription(String description)
    {
        m_description = description;
    }

    /**
     * @return
     */
    public String getToolTip()
    {
        return m_tooltip;
    }

    /**
     * @param tooltip
     */
    public void setToolTip(String tooltip)
    {
        m_tooltip = tooltip;
    }

    /**
     * @return
     */
    public Icon getIcon()
    {
        return m_icon;
    }

    /**
     * @param icon
     */
    public void setIcon(Icon icon)
    {
        m_icon = icon;
    }

    /**
     * @return
     */
    public String getDrillClass()
    {
        return m_drillClass;
    }

    /**
     * @param drillClass
     */
    public void setDrillClass(String drillClass)
    {
        m_drillClass = drillClass;
    }

    /**
     * @return
     */
    public String getType()
    {
        if (m_type == null)
        {
            return "Child"; //$NON-NLS-1$
        }
        return m_type;
    }

    /**
     * @param type
     */
    public void setType(String type)
    {
        m_type = type;
    }

    /**
     * @return
     */
    public String getStyle()
    {
        if (m_style == null)
        {
            if (!isChangeable())
            {
                return "Table";
            }
            else
            {
                return "UpdableTable";
            }
        }
        return m_style;
    }

    /**
     * @return
     */
    public boolean isChangeable()
    {
        return m_insertScript != null ||
               m_updateScript != null ||
               m_deleteScript != null;
    }

    /**
     * @return
     */
    public boolean isChild()
    {
        return getType().equals("Child");
    }

    /**
     * @param style
     */
    public void setStyle(String style)
    {
        m_style = style;
    }

    /**
     * @param customValue
     */
    public void setCustomValue(Object customValue)
    {
        m_customValue = customValue;
    }

    /**
     * @return
     */
    public Object getCustomValue()
    {
        return m_customValue;
    }

    /**
     * @return
     */
    public List<Query> getQueries()
    {
        return m_queries;
    }

    /**
     * @param queries
     */
    public void setQueries(List<Query> queries)
    {
        this.m_queries = queries;
    }

    /**
     * @return
     */
    public List<Bind> getQueriesBinds()
    {
        return m_queriesBinds;
    }

    /**
     * @param binds
     */
    public void setQueriesBinds(List<Bind> binds)
    {
        m_queriesBinds = binds;
    }

    /**
     * @return
     */
    public Map<String, Object> getQueriesBindMap()
    {
        return m_queriesBindMap;
    }

    /**
     * @param binds
     */
    public void setQueriesBindMap(Map<String, Object> binds)
    {
        m_queriesBindMap = binds;
    }

    /**
     * @return
     */
    public List<DM> getChildren()
    {
        return m_children;
    }

    /**
     * @return
     */
    public boolean hasChildren()
    {
        List<DM> children = getChildren();
        return children != null && children.size() > 0;
    }

    /**
     * @param children
     */
    public void setChildren(List<DM> children)
    {
        this.m_children = children;
    }

    /**
     * @return
     */
    public Query getQuery()
    {
        return getQuery(false,false,false);
    }

    /**
     * @param prependDBA
     * @param cacheOnlyCheck
     * @param strict if true, only a matching Query will be returned, else null
     *               if false, either a matching Query or the first defined will be returned
     * @return query
     */
    protected Query getQuery(boolean prependDBA, boolean cacheOnlyCheck, boolean strict)
    {
        if (m_queries == null)
        {
            m_queries = getQueries();
        }
        Query query = QueryUtils.getQuery(m_queries, DefaultConnectionIdentifier.createIdentifier(m_conn), prependDBA, cacheOnlyCheck);
        if (!strict && query == null && m_queries != null && m_queries.size() > 0)
        {
            query = m_queries.get(0);
        }
        return query;
    }

    /**
     * @return
     */
    public Map<String, String> getExtAttributes()
    {
        if (m_extAttrs == null)
        {
            m_extAttrs = new HashMap<String, String>();
        }
        return m_extAttrs;
    }

    /**
     * @param attrs
     */
    public void setExtAttributes(Map<String, String> attrs)
    {
        m_extAttrs = attrs;
    }

    /**
     * @return
     */
    public Map<String, String> getExtValues() {
        if (m_extValues == null)
        {
            m_extValues = new HashMap<String, String>();
        }
        return m_extValues;

    }

    /**
     * @param values
     */
    public void setExtValues(Map<String, String> values) {
        m_extValues = values;
    }

    /**
     * @return
     */
    public String getPath()
    {
        return m_path;
    }

    /**
     * @param path
     */
    public void setPath(String path)
    {
        m_path = path;
    }

    /**
     * @return
     */
    public String getID()
    {
        return m_id;
    }

    /**
     * @param id
     */
    public void setID(String id)
    {
        m_id = id;
    }

    /**
     * @param iconName
     */
    public void setIconName(String iconName)
    {
        m_iconName = iconName;
    }

    /**
     * @return
     */
    public String getIconName()
    {
        return m_iconName;
    }

    /**
     * @return
     */
    public DM getParent()
    {
        return m_parent;
    }

    /**
     * @param parent
     */
    public void setParent(DM parent)
    {
        m_parent = parent;
    }

    /**
     * @return
     */
    public Script getInsertScript()
    {
        return m_insertScript;
    }

    /**
     * @param insertScript
     */
    public void setInsertScript(Script insertScript)
    {
        m_insertScript = insertScript;
    }

    /**
     * @return
     */
    public Script getDeleteScript()
    {
        return m_deleteScript;
    }

    /**
     * @param deleteScript
     */
    public void setDeleteScript(Script deleteScript)
    {
        m_deleteScript = deleteScript;
    }

    /**
     * @param updateScript
     */
    public void setUpdateScript(Script updateScript)
    {
        m_updateScript = updateScript;
    }

    /**
     * @return
     */
    public Script getUpdateScript()
    {
        return m_updateScript;
    }

    /**
     * @param m_connType
     */
    public void setConnType(String m_connType)
    {
        this.m_connType = m_connType;
    }

    /**
     * @return
     */
    public String getConnType()
    {
        return m_connType;
    }

    /**
     * @return
     */
    public String getObjectType()
    {
        return m_objectType;
    }

    /**
     * @param m_connType
     */
    public void setObjectType(String m_connType)
    {
        this.m_objectType = m_connType;
    }

    /**
     * @return
     */
    public ClassLoader getCl()
    {
        if (m_cl == null)
        {
            m_cl = getClass().getClassLoader();
        }
        return m_cl;
    }

    /**
     * @return
     */
    public  void setCl(ClassLoader cl)
    {
        m_cl = cl;
    }

    /**
     * @return
     */
    public boolean isNoRefresh()
    {
        return m_noRefresh;
    }

    /**
     * @param b
     */
    public void setNoRefresh(Boolean b)
    {
        m_noRefresh = b == null ? Boolean.FALSE : b;
    }
}
