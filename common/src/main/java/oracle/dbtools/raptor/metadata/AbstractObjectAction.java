/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.metadata;

import java.util.ArrayList;
import java.util.List;

import oracle.dbtools.common.utils.MetaResource;
import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.common.utils.Version;

/**
 * Shared definition of Object Action initialised with XML.  Used by the Abstract Factory Pattern implemented
 * by XMLFactory
 * @param <OAP> Concrete Object Action Promp class
 */
public class AbstractObjectAction<OAP extends AbstractObjectActionPrompt> implements IObjectActionConstants
{
    public static final int SINGLE_SELECT = 1;    
    public static final int MULTI_SELECT = 2;
    public static final int BOTH_SELECT = SINGLE_SELECT|MULTI_SELECT;

    protected Version m_minVersion = null;
    protected Version m_maxVersion = null;
    protected List<OAP> m_prompts = null;
    protected boolean m_reload = false;
    protected boolean m_reloadParent = false;
    protected boolean m_removeFromParent = false;
    protected String m_title = null;
    protected String m_classArgs = null;
    protected String m_connType = null;
    protected int m_selectionMode = 0;
    //protected String m_className = null;
    //protected ClassLoader m_classLoader = null;
    protected float m_weight = 1.0f;
    protected float m_section = 16.0f;
    protected boolean m_toolbar = false;
    protected boolean m_isRenameAction = false;
    protected String m_sql = null;
    protected String m_sqlType = null;
    protected String m_script = null;
    protected String m_scriptLang = null;
    protected String m_iconName = null;
    protected MetaResource m_iconRef = null;
    protected boolean m_sqldevOnly = false;
    protected String m_type = null;
    protected boolean m_connRequired = true;
    protected String m_commandName = null;
    protected String m_confirm = null;
    protected String m_confirmTitle = null;
    protected String m_confirmSql = null;
    protected String m_help = null;
    protected String[] m_reqFeatures = null;
    protected int m_id;
    protected String m_controllerClassName = null;
    protected List<String> m_features;

    public void setMinVersion(String s)
    {
        if (s != null)
        {
            m_minVersion = new Version(s);
        }
    }

    public Version getMinVersion()
    {
        return m_minVersion;
    }

    public void setMaxVersion(String s)
    {
        if (s != null)
        {
            m_maxVersion = new Version(s);
        }
    }

    public Version getMaxVersion()
    {
        return m_maxVersion;
    }

    public List<OAP> getPrompts()
    {
        if (m_prompts == null)
        {
            m_prompts = new ArrayList<OAP>();
        }
        return m_prompts;
    }

    public void setPrompts(List<OAP> prompts)
    {
        m_prompts = prompts;
    }

    public void setReload(boolean reload)
    {
        m_reload = reload;
    }

    public boolean getReload()
    {
        return m_reload;
    }

    public void setReloadParent(boolean reload)
    {
        m_reloadParent = reload;
    }

    public boolean getReloadParent()
    {
        return m_reloadParent;
    }

    public void setRemoveFromParent(boolean remove)
    {
        m_removeFromParent = remove;
    }

    public boolean isRemoveFromParent()
    {
        return m_removeFromParent;
    }

    public int getId()
    {
        return m_id;
    }

    public void setId(int id)
    {
        m_id = id;;
    }

    public String getHelp()
    {
        return m_help;
    }

    public String getType()
    {
        return m_type;
    }

    public String getSql()
    {
        return m_sql;
    }

    public String getSqlType()
    {
        return m_sqlType;
    }

    public String getTitle()
    {
        return m_title;
    }

    public void setTitle(String title)
    {
        m_title = title;
    }

    public String getConfirm()
    {
        return m_confirm;
    }

    public String getConfirmSql()
    {
        return m_confirmSql;
    }

    public String getConfirmTitle()
    {
        return m_confirmTitle;
    }

    public void setClassArgs(String classargs)
    {
        m_classArgs = classargs;
    }

    public String getClassArgs()
    {
        return m_classArgs;
    }

    public void setConnType(String connType)
    {
        m_connType = connType;
    }

    public String getConnType()
    {
        return m_connType == null ? "Oracle" : m_connType;
    }

    public int getSelectionMode()
    {
        return m_selectionMode;
    }

    public boolean isSelectable(int i)
    {
        return (i&m_selectionMode)==i;
    }

    public void setSelectionMode(int s)
    {
        m_selectionMode = s;
    }

    public void setSelectionMode(String s)
    {
        if ( s!= null &&  s.equalsIgnoreCase("MULTI"))
        {
            setSelectionMode(MULTI_SELECT);
        }
        else if ( s!= null &&  s.equalsIgnoreCase("BOTH"))
        {
            setSelectionMode(BOTH_SELECT);
        }
        else 
        {
            setSelectionMode(SINGLE_SELECT);
        }
    }


    public List<String> getDefaults()
    {
        ArrayList<String> ret = new ArrayList<String>();
        for (OAP p : getPrompts())
        {
            ret.add(p.getDefault());
        }
        return ret;
    }

    /**
     * @return
     */
    public boolean isConnectionRequired()
    {
        return m_connRequired;
    }

    /**
     * @return
     */
    public float getWeight()
    {
        return m_weight;
    }

    public void setWeight(float weight)
    {
        m_weight = weight;
    }

    public float getSection()
    {
        return m_section;
    }

    public void setSection(float section)
    {
        m_section = section;
    }

    public boolean isScript()
    {
        return ModelUtil.hasLength(m_script);
    }

    public boolean isSqlDevOnly()
    {
        return m_sqldevOnly;
    }

    public String getScript()
    {
        return m_script;
    }

    public String getScriptLanguage()
    {
        if (m_scriptLang==null)
        {
            m_scriptLang = "js";
        }
        return m_scriptLang;
    }

    public void setToolbar(boolean toolbar)
    {
        m_toolbar = toolbar;
    }

    public boolean isToolbar()
    {
        return m_toolbar;
    }

    public boolean isRenameAction()
    {
        return m_isRenameAction;
    }

    public void setIsRenameAction(boolean isRenameAction)
    {
        m_isRenameAction = isRenameAction;
    }

    public void setSql(String sql)
    {
        m_sql = sql;
    }

    public void setSqlType(String sqlType)
    {
        m_sqlType = sqlType;
    }

    public void setScript(String script)
    {
        m_script = script;
    }

    public void setScriptLang(String scriptLang)
    {
        m_scriptLang = scriptLang;
    }

    public MetaResource getIconRef()
    {
        return m_iconRef;
    }

    public void setIconRef(MetaResource mRes)
    {
        m_iconRef = mRes;
    }

    public void setSqldevOnly(boolean sqldevOnly)
    {
        m_sqldevOnly = sqldevOnly;
    }

    public void setType(String type)
    {
        m_type = type;
    }

    public void setConnRequired(boolean connRequired)
    {
        m_connRequired = connRequired;
    }

    public void setCommandName(String commandName)
    {
        m_commandName = commandName;
    }

    public String getCommandName()
    {
        return m_commandName;
    }

    public void setConfirm(String confirm)
    {
        m_confirm = confirm;
    }

    public void setConfirmTitle(String confirmTitle)
    {
        m_confirmTitle = confirmTitle;
    }

    public void setConfirmSql(String confirmSQL)
    {
        m_confirmSql = confirmSQL;
    }

    public void setHelp(String help)
    {
        m_help  = help;
    }

    public void setReqFeatures(String[] features)
    {
        if (features == null)
        {
            m_features = null;
            return ;
        }
        if (m_features == null)
        {
            m_features = new ArrayList<String>();
        }
        m_features.clear();
        for (String feature : features)
        {
            m_features.add(feature);
        }
    }

    @SuppressWarnings("oracle.jdeveloper.java.null-array-return")
    public String[] getReqFeatures()
    {
        if (m_features == null)
        {
            return null;
        }
        return m_features.toArray(new String[m_features.size()]);
    }
}