/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.metadata;


/**
 * Shared definition of Object Action Prompt initialised with XML.  Used by the Abstract Factory Pattern implemented
 * by XMLFactory
 */
public class AbstractObjectActionPrompt
{
    protected String m_dataType;
    protected String m_default;
    protected String m_prompt;
    protected boolean m_readOnly;
    protected String m_reload;
    protected String m_required;
    protected String m_type;
    protected String m_typeAttribute;
    protected String m_validator;
    protected String m_value;

    /**
     * @param dataType
     */
    public void setDataType(String dataType)
    {
        this.m_dataType = dataType;
    }

    /**
     * @return
     */
    public String getDataType()
    {
        return m_dataType;
    }

    /**
     * @param defaultVal
     */
    public void setDefault(String defaultVal)
    {
        this.m_default = defaultVal;
    }

    /**
     * @return
     */
    public String getDefault()
    {
        return m_default;
    }

    /**
     * @param prompt
     */
    public void setPrompt(String prompt)
    {
        this.m_prompt = prompt;
    }

    /**
     * @return
     */
    public String getPrompt()
    {
        return m_prompt == null ? "" : m_prompt;
    }

    /**
     * @param readOnly
     */
    public void setReadOnly(boolean readOnly)
    {
        this.m_readOnly = readOnly;
    }

    /**
     * @return
     */
    public boolean isReadOnly()
    {
        return m_readOnly;
    }

    /**
     * @param reload
     */
    public void setReload(String reload)
    {
        this.m_reload = reload;
    }

    /**
     * @return
     */
    public String getReload()
    {
        return m_reload;
    }

    /**
     * @return
     */
    public boolean isReloadable()
    {
        return m_reload != null;
    }

    /**
     * @param required
     */
    public void setRequired(String required)
    {
        this.m_required = required;
    }

    /**
     * @return
     */
    public boolean isRequired()
    {
        return m_required != null && m_required.equalsIgnoreCase("true");
    }

    /**
     * @param type
     */
    public void setType(String type)
    {
        this.m_type = type;
    }

    /**
     * @return
     */
    public String getType()
    {
        return m_type == null ? "" : m_type;
    }

    /**
     * @param typeAttribute
     */
    public void setTypeAttribute(String typeAttribute)
    {
        this.m_typeAttribute = typeAttribute;
    }

    /**
     * @return
     */
    public String getTypeAttribute()
    {
        return m_typeAttribute;
    }

    /**
     * @param validator
     */
    public void setValidator(String validator)
    {
        this.m_validator = validator;
    }

    /**
     * @return
     */
    public String getValidator()
    {
        return m_validator;
    }

    /**
     * @param value
     */
    public void setValue(String value)
    {
        this.m_value = value;
    }

    /**
     * @return
     */
    public String getValue()
    {
        return m_value;
    }
}
