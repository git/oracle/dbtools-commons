/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.metadata;

/**
 * Object Action
 * Public string definitions for Element names used in xml defining Object Actions.
 */
public interface IObjectActionConstants
{
    public static final String ATTRIBUTE_MINVERSION = "minversion"; //$NON-NLS-1$
    public static final String ATTRIBUTE_MAXVERSION = "maxversion"; //$NON-NLS-1$
    public static final String ELEMENT_HELP = "help"; //$NON-NLS-1$
    public static final String ELEMENT_PROMPT = "prompt"; //$NON-NLS-1$
    public static final String ELEMENT_CONFIRMATION = "confirmation"; //$NON-NLS-1$
    public static final String ATTRIBUTE_SECTION = "section"; //$NON-NLS-1$
    public static final String ATTRIBUTE_WEIGHT = "weight"; //$NON-NLS-1$
    public static final String ATTRIBUTE_TOOLBAR = "toolbar"; //$NON-NLS-1$
    public static final String ATTRIBUTE_CONNECTION_REQUIRED = "connectionRequired"; //$NON-NLS-1$
    public static final String ATTRIBUTE_CONTROLLER_CLASS_NAME = "controllerClassName"; //$NON-NLS-1$
    public static final String ATTRIBUTE_SELECTION_MODE = "selectionMode"; //$NON-NLS-1$
    public static final String ATTRIBUTE_CLASS_ARGS = "classArgs"; //$NON-NLS-1$
    public static final String ATTRIBUTE_CLASS_NAME = "className"; //$NON-NLS-1$
    public static final String ATTRIBUTE_COMMAND_NAME = "commandName"; //$NON-NLS-1$
    public static final String ATTRIBUTE_CONN_TYPE = "connType"; //$NON-NLS-1$
    public static final String ATTRIBUTE_REMOVE_FROM_PARENT = "removeFromParent"; //$NON-NLS-1$
    public static final String ATTRIBUTE_RELOAD = "reload"; //$NON-NLS-1$
    public static final String ATTRIBUTE_RELOADPARENT = "reloadparent"; //$NON-NLS-1$
    public static final String ATTRIBUTE_SQLDEV_ONLY = "sqldevonly"; //$NON-NLS-1$
    public static final String ELEMENT_ICON_NAME = "iconName"; //$NON-NLS-1$
    public static final String ATTRIBUTE_TYPE = "type"; //$NON-NLS-1$
    public static final String ATTRIBUTE_LANGUAGE = "language"; //$NON-NLS-1$
    public static final String ELEMENT_SCRIPT = "script"; //$NON-NLS-1$
    public static final String ELEMENT_SQL = "sql"; //$NON-NLS-1$
    public static final String ELEMENT_TITLE = "title"; //$NON-NLS-1$
    public static final String ATTRIBUTE_REQUIREDFEATURES = "requiredFeatures"; //$NON-NLS-1$
}
