/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.metadata;


import java.lang.reflect.Field;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.dbtools.common.utils.MetaResource;
import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryCache;
import oracle.dbtools.raptor.utils.XLIFFHelper;
import oracle.dbtools.raptor.utils.XMLHelper;
import oracle.dbtools.util.Logger;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SuppressWarnings("unused")
public abstract class XMLFactory<DM extends AbstractDisplayModel<DM,OA>, OA extends AbstractObjectAction<OAP>, OAP extends AbstractObjectActionPrompt>
{
    public static final String RENAME_ACTION_NAME = "Rename...";

    /**
     * Instantiate a new Display Model object, perform any implementation specific pre-initialisation here.
     * @param domNode may be null
     * @param cl
     * @param xliffhelper
     * @return
     */
    protected abstract DM newDisplayModel(Node domNode, ClassLoader cl, XLIFFHelper xliffhelper);

    /**
     * Instantiate a new Object Action object, perform any implementation specific pre-initialisation here.
     * @param domNode may be null
     * @param cl
     * @param xliffhelper
     * @return
     */
    protected abstract OA newObjectAction(Node domNode, ClassLoader cl, XLIFFHelper xliffhelper);

    /**
     * Instantiate a new Object Action Prompt object, perform any implementation specific pre-initialisation here.
     * @param domNode may be null
     * @param cl
     * @param xliffhelper
     * @return
     */
    protected abstract OAP newObjectActionPrompt(Node domNode, ClassLoader cl, XLIFFHelper xliffhelper);

    // public ICommonQueries createCommonQueries(Node domNode);

    //public  ISource createSource(Node domNode);

    /**
     * Create and initialise a Display Model
     * @param domNode
     * @param cl
     * @param xliffhelper
     * @return
     */
    public DM createDisplayModel(Node domNode, ClassLoader cl, XLIFFHelper xliffhelper)
    {
        DM dm = newDisplayModel(domNode, cl, xliffhelper);
        initDisplayModel(domNode, cl, xliffhelper, dm);
        initDisplayModelPost(domNode, cl, xliffhelper, dm);
        return dm;
    }

    /**
     * create and initialise an Object Action
     * @param domNode
     * @param cl
     * @param xliffhelper
     * @return
     */
    public OA createObjectAction(Node domNode, ClassLoader cl, XLIFFHelper xliffhelper)
    {
        OA objAction = newObjectAction(domNode, cl, xliffhelper);
        initObjectAction(domNode, cl, xliffhelper, objAction);
        initObjectActionPost(domNode, cl, xliffhelper, objAction);
        return objAction;
    }

    /**
     * Create and initialise an Objecte Action Prompt
     * @param domNode
     * @param cl
     * @param xliffhelper
     * @return
     */
    public OAP createObjectActionPrompt(Node domNode, ClassLoader cl, XLIFFHelper xliffhelper)
    {
        OAP objAction = newObjectActionPrompt(domNode, cl, xliffhelper);
        initObjectActionPrompt(domNode, cl, xliffhelper, objAction);
        initObjectActionPromptPost(domNode, cl, xliffhelper, objAction);
        return objAction;
    }

    /**
     * Override to perform implementation dependant initialisation of a Display Model from the information in <code>node</code>.
     * @param node
     * @param cl
     * @param xliffHelper
     * @param dm
     */
    protected void initDisplayModelPost(Node node, ClassLoader cl, XLIFFHelper xliffHelper, DM dm)
    {
    }

    /**
     * Perform implementation independant initialisation of a Display Model from the information in <code>node</code>.
     * @param node
     * @param cl
     * @param xliffHelper
     * @param dm
     */
    protected final void initDisplayModel(Node node, ClassLoader cl, XLIFFHelper xliffHelper, DM dm)
    {
        dm.setCl(cl);
        dm.setName(notNull(XMLHelper.getNodeValue(node, "name")));
        dm.setDescription(notNull(XMLHelper.getNodeValue(node, "description")));
        dm.setToolTip(notNull(XMLHelper.getNodeValue(node, "tooltip")));
        dm.setDrillClass(notNull(XMLHelper.getNodeValue(node, "drillClass")));
        dm.setIconName(notNull(XMLHelper.getNodeValue(node, "iconName")));
        dm.setActions(getActions(node, cl, xliffHelper));
        if (xliffHelper != null)
        {
            dm.setName(xliffHelper.getTranslation(dm.getName()));
            dm.setDescription(xliffHelper.getTranslation(dm.getDescription()));
            dm.setToolTip(xliffHelper.getTranslation(dm.getToolTip()));
        }
        //
        // Attributes
        NamedNodeMap attrs = node.getAttributes();
        Node n = null;
        Map<String, String> extAttrs = null;
        // find attributes
        for (int i = 0; i < attrs.getLength(); i++)
        {
            n = attrs.item(i);
            if (n.getNodeName().equals("type"))
            {
                dm.setType(notNull(XMLHelper.getAttributeNode(node, "type")));
            }
            else if (n.getNodeName().equals("style"))
            {
                dm.setStyle(notNull(XMLHelper.getAttributeNode(node, "style")));
            }
            else if (n.getNodeName().equals("id"))
            {
                dm.setID(notNull(XMLHelper.getAttributeNode(node, "id")));
            }
            else if (n.getNodeName().equals("enable"))
            {
                dm.setEnable(Boolean.parseBoolean(notNullBoolean(XMLHelper.getAttributeNode(node, "enable"))));
            }
            else if (n.getNodeName().equals("noRefresh"))
            {
                dm.setNoRefresh(Boolean.parseBoolean(notNullBoolean(XMLHelper.getAttributeNode(node, "noRefresh"))));
            }
            else if (n.getNodeName().equals("connType"))
            {
                dm.setConnType(XMLHelper.getAttributeNode(node, "connType"));
            }
            else if (n.getNodeName().equals("objectType"))
            {
                dm.setObjectType(XMLHelper.getAttributeNode(node, "objectType"));
            }
            
            else
            {
                // if not one of the above stuff it into the extra attrs bucket
                if (extAttrs == null)
                {
                    extAttrs = new HashMap<String, String>();
                }
                extAttrs.put(n.getNodeName(), n.getNodeValue().intern());
            }
        }
        dm.setExtAttributes(extAttrs);
        
        //populateAttributes(node);
        // add the child elements
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++)
        {
            Node child = children.item(i);
            String name = child.getNodeName();
            if (name.equalsIgnoreCase("CustomValues"))
            {
                NodeList custom = children.item(i).getChildNodes();
                Map<String, String> extValues = null;
                for (int zz = 0; zz < custom.getLength(); zz++)
                {
                    String nn = custom.item(zz).getNodeName();
                    String value = custom.item(zz).getTextContent();
                    if (extValues == null)
                    {
                        extValues = new HashMap<String, String>();
                    }
                    if (!(nn.equals("#text")))
                    {
                        extValues.put(nn, value.intern());
                    }
                }
                dm.setExtValues(extValues);
            }
            else if (name.equalsIgnoreCase("query"))
            {
                dm.addQuery(Query.getQuery(child, xliffHelper, cl));
            }
            else if (name.equalsIgnoreCase("display"))
            {
                DM childDM = createDisplayModel(child, cl, xliffHelper);
                dm.addChild(childDM);
            }
            else if (name.equalsIgnoreCase("queries"))
            {
                dm.setQueries(Query.getQueries(child, cl));
            }
        }

        if (dm.getType() != null && dm.getType().equals("editor"))
        {
            // Bug 25367301 - XML EXTENSIONS AREN'T WORKING 
            // Use corresponding isNullOrEmpty check for notNull(...) handled items
            if (isNullOrEmpty(dm.getName()))
            {
                dm.setName(XMLHelper.getNodeValue(node, "title"));
            }
//            if (dm.getType() == null)
//            {
//                dm.setType(XMLHelper.getAttributeNode(node, "vertical"));
//            }
            if (isNullOrEmpty(dm.getStyle()))
            {
                dm.setStyle(XMLHelper.getAttributeNode(node, "display"));
            }
            if (dm.getQueries() == null || dm.getQueries().size() == 0)
            {
                String queryID = XMLHelper.getAttributeNode(XMLHelper.getChildNode(node, "query"), "id");
                if (queryID != null && !queryID.equals(""))
                {
                    dm.setQueries(QueryCache.getQueries(queryID));
                }
                else
                {
                    // get query
                    dm.setQueries(Query.getQueries(node));
                }
                if (XMLHelper.getChildNode(node, "bottomquery") != null)
                {
                    Node bottomQueryNode = XMLHelper.getChildNode(node, "bottomquery");
                    DM model = newDisplayModel(null, cl, xliffHelper);
                    model.setName(XMLHelper.getNodeValue(XMLHelper.getChildNode(node, "bottomquery"), "title")); //$NON-NLS-1$ //$NON-NLS-2$
                    queryID = XMLHelper.getAttributeNode(XMLHelper.getChildNode(XMLHelper.getChildNode(node, "bottomquery"), "query"), "id"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    if (queryID != null && !queryID.equals(""))
                    {
                        model.setQueries(QueryCache.getQueries(queryID));
                    }
                    else
                    {
                        // get query
                        model.setQueries(Query.getQueries(XMLHelper.getChildNode(node, "bottomquery")));
                    }
                    dm.addChild(model);
                }
                // sub query for child results
                if (XMLHelper.getChildNode(node, "subquery") != null)
                {
                    Node subqueryNode = XMLHelper.getChildNode(node, "subquery");
                    DM model = newDisplayModel(null, cl, xliffHelper);
                    model.setQueries(Query.getQueries(XMLHelper.getChildNode(node, "subquery")));
                    model.setStyle(XMLHelper.getAttributeNode(XMLHelper.getChildNode(node, "subquery"), "type"));
                    dm.addChild(model);
                }
            }
        }
    }

    /**
     * Override to perform implementation dependant initialisation of an Object Action from the information in <code>node</code>.
     * @param node
     * @param cl
     * @param xliff
     * @param oa
     */
    protected void initObjectActionPost(Node node, ClassLoader cl, XLIFFHelper xliff, OA oa)
    {
    }

    /**
     * Perform implementation independant initialisation of an Object Action from the information in <code>node</code>.
     * @param node
     * @param cl
     * @param xliff
     * @param oa
     */
    protected final void initObjectAction(Node node, ClassLoader cl, XLIFFHelper xliff, OA oa)
    {
        oa.setTitle(XMLHelper.getNodeValue(node, IObjectActionConstants.ELEMENT_TITLE));
        oa.setIsRenameAction(RENAME_ACTION_NAME.equals(oa.getTitle()));
        oa.setSql(XMLHelper.getNodeValue(node, IObjectActionConstants.ELEMENT_SQL));
        oa.setScript(XMLHelper.getNodeValue(node, IObjectActionConstants.ELEMENT_SCRIPT));
        oa.setScriptLang(XMLHelper.getAttributeNode(XMLHelper.getChildNode(node, IObjectActionConstants.ELEMENT_SCRIPT), IObjectActionConstants.ATTRIBUTE_LANGUAGE));
        oa.setSqlType(XMLHelper.getAttributeNode(XMLHelper.getChildNode(node, IObjectActionConstants.ELEMENT_SQL), IObjectActionConstants.ATTRIBUTE_TYPE));
        String iconName = XMLHelper.getNodeValue(node, IObjectActionConstants.ELEMENT_ICON_NAME);
        if (ModelUtil.hasLength(iconName))
        {
            oa.setIconRef(new MetaResource(cl, iconName));
        }
        // attributes
        oa.setSqldevOnly(isTrue(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_SQLDEV_ONLY)));
        oa.setReloadParent(isTrue(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_RELOADPARENT)));
        oa.setReload(isTrue(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_RELOAD)));
        oa.setRemoveFromParent(isTrue(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_REMOVE_FROM_PARENT)));
        oa.setType(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_TYPE));
        oa.setConnType(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_CONN_TYPE));

        oa.setClassArgs(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_CLASS_ARGS));
        oa.setSelectionMode(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_SELECTION_MODE));
        oa.setConnRequired(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_CONNECTION_REQUIRED) != null ?
                           Boolean.valueOf(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_CONNECTION_REQUIRED)) : true);
        oa.setToolbar(Boolean.parseBoolean(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_TOOLBAR)));
        oa.setCommandName(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_COMMAND_NAME));
        String weight = XMLHelper.getAttributeNode(node, "weight");
        if (weight != null)
        {
            try
            {
                oa.setWeight(resolveFloatValue(weight, cl));
            }
            catch (Exception e)
            {
                Logger.severe(getClass(), "Menu Weight", e);
            }
        }
        String section = XMLHelper.getAttributeNode(node, "section");
        if (section != null)
        {
            try
            {
                oa.setSection(resolveFloatValue(section, cl));
            }
            catch (NumberFormatException nfe)
            {
                Logger.severe(getClass(), "Menu Section", nfe);
            }
        }

        // nodes
        oa.setConfirm(XMLHelper.getNodeValue(XMLHelper.getChildNode(node, IObjectActionConstants.ELEMENT_CONFIRMATION), IObjectActionConstants.ELEMENT_PROMPT));
        oa.setConfirmTitle(XMLHelper.getNodeValue(XMLHelper.getChildNode(node, IObjectActionConstants.ELEMENT_CONFIRMATION), IObjectActionConstants.ELEMENT_TITLE));
        oa.setConfirmSql(XMLHelper.getNodeValue(XMLHelper.getChildNode(node, IObjectActionConstants.ELEMENT_CONFIRMATION), IObjectActionConstants.ELEMENT_SQL));
        oa.setHelp(XMLHelper.getNodeValue(node, IObjectActionConstants.ELEMENT_HELP));
        // get prompts
        oa.setPrompts(getPrompts(node, cl, xliff));
        //
        // get min/max
        oa.setMaxVersion(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_MAXVERSION));
        oa.setMinVersion(XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_MINVERSION));
        // get required features
        final String reqFeatures = XMLHelper.getAttributeNode(node, IObjectActionConstants.ATTRIBUTE_REQUIREDFEATURES);
        oa.setReqFeatures(reqFeatures != null ? reqFeatures.split(",") : null);

        //
        //
        if (xliff != null)
        {
            oa.setTitle(xliff.getTranslation(oa.getTitle()));
            oa.setConfirm(xliff.getTranslation(oa.getConfirm()));
            oa.setConfirmTitle(xliff.getTranslation(oa.getConfirmTitle()));
            oa.setHelp(xliff.getTranslation(oa.getHelp()));
        }
    }

    /**
     * Override to perform implementation dependant initialisation of an Object Action Prompt from the information in <code>node</code>.
     * @param node
     * @param xliff
     * @param oap
     */
    protected void initObjectActionPromptPost(Node node, ClassLoader cl, XLIFFHelper xliff, OAP oap)
    {
    }

    /**
     * Perform implementation independant initialisation of an Object Action Prompt from the information in <code>node</code>.
     * @param node
     * @param xliff
     * @param oap
     */
    protected final void initObjectActionPrompt(Node node, ClassLoader cl, XLIFFHelper xliff, OAP oap)
    {
        oap.setPrompt(XMLHelper.getNodeValue(node, "label"));
        oap.setValue(XMLHelper.getNodeValue(node, "value"));
        oap.setDefault(XMLHelper.getNodeValue(node, "default"));
        oap.setType(XMLHelper.getAttributeNode(node, "type"));
        oap.setRequired(XMLHelper.getAttributeNode(node, "required"));
        oap.setReadOnly(Boolean.parseBoolean(XMLHelper.getAttributeNode(node, "readonly")));
        oap.setValidator(XMLHelper.getAttributeNode(node, "validator"));
        oap.setReload(XMLHelper.getAttributeNode(node, "reload"));
        oap.setDataType(XMLHelper.getAttributeNode(node, "datatype"));
        oap.setTypeAttribute(XMLHelper.getAttributeNode(node, "type_attrib"));
        if (xliff != null)
        {
            oap.setPrompt(xliff.getTranslation(oap.getPrompt()));
        }
    }

    /**
     * Convenience method for creating Object Action objects from child nodes named <code>"item"</code>
     * @param node
     * @param cl
     * @param xliffHelper
     * @return
     */
    protected List<OA> getActions(Node node, ClassLoader cl, XLIFFHelper xliffHelper)
    {
        List<OA> ret = new ArrayList<OA>();
        NodeList nodes = ((Element) node).getElementsByTagName("item");
        for (int i = 0; i < nodes.getLength(); i++)
        {
            Node child = nodes.item(i);
            OA oa = createObjectAction(child, cl, xliffHelper);
            ret.add(oa);
        }
        return ret;
    }


    /**
     * Convenience method for creating Object Action Prompt objects from child nodes named <code>"prompt"</code>
     * @param node
     * @param xliffhelper
     * @return
     */
    protected List<OAP> getPrompts(Node node, ClassLoader cl, XLIFFHelper xliffhelper)
    {
        List<OAP> ret = new ArrayList<OAP>();
        NodeList nodes = node.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++)
        {
            final Node item = nodes.item(i);
            if (item.getNodeName().equals("prompt"))
            {
                OAP oap = createObjectActionPrompt(item, cl, xliffhelper);
                ret.add(oap);
            }
        }
        return ret;
    }

    /**
     * @param s
     * @return
     */
    protected static boolean isTrue(String s)
    {
        if (s != null && s.equalsIgnoreCase("true"))
            return true;
        else
            return false;
    }

    /**
     * @param value
     * @return
     * @throws NumberFormatException
     */
    protected static Float resolveFloatValue(String value, ClassLoader cl) throws NumberFormatException
    {
        if (null == value)
        {
            return null;
        }
        Float floatValue = Float.MAX_VALUE;
        try
        {
            floatValue = Float.valueOf(value);
        }
        catch (NumberFormatException nfe)
        {
            // See if it's a reference to a constant
            String className = value.substring(0, value.lastIndexOf("."));
            String fieldName = value.substring(value.lastIndexOf(".") + 1);
            try
            {
                Field field = Class.forName(className, true, cl).getDeclaredField(fieldName);
                Object val = field.get(null);
                if (val instanceof Float)
                {
                    floatValue = (Float) val;
                }
            }
            catch (Exception e)
            {
                Logger.warn(XMLFactory.class, e);
                throw nfe;
            }
        }
        return floatValue;
    }

    /**
     * @param value
     * @return
     */
    protected static String notNull(String value)
    {
        if (value == null || value.equals("null"))
            return "";
        else
            return value;
    }

    /**
     * @param value
     * @return
     */
    protected static String notNullBoolean(String value)
    {
        if (value == null || value.equals("null"))
            return "false";
        else
            return "true";
    }
    
    protected static boolean isNullOrEmpty(String value)
    {
        return null == value || value.isEmpty();
    }

    public static String[] getTypesForNode(String nodeType)
    {
        return s_nodeTypeMap.get(nodeType);
    }

    private static Map<String, String[]> s_nodeTypeMap = new HashMap<String, String[]>();
    static
    {
        s_nodeTypeMap.put("TableNode", new String[] { "TABLE" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("ViewNode", new String[] { "VIEW" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("MViewNode", new String[] { "MATERIALIZED_VIEW" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("PlSqlBaseNode", new String[] { "PROCEDURE", "FUNCTION", "PACKAGE", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                                                          "PACKAGE BODY", "TYPE", "TYPE BODY" }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        s_nodeTypeMap.put("DatabaseLinkNode", new String[] { "DATABASE LINK" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("MViewLogNode", new String[] { "MATERIALIZED VIEW LOG" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("TriggerNode", new String[] { "TRIGGER" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("IndexNode", new String[] { "INDEX" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("RecycledObjectNode", new String[] { "RECYCLEBIN" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("DirectoryNode", new String[] { "DIRECTORY" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("QueueNode", new String[] { "QUEUE" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("QueueTableNode", new String[] { "QUEUE TABLE" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("JavaNode", new String[] { "JAVA" }); //$NON-NLS-1$ //$NON-NLS-2$
        s_nodeTypeMap.put("XmlSchemaNode", new String[] { "XML SCHEMA" }); //$NON-NLS-1$ //$NON-NLS-2$
        // QA has an improper display defined, but it's a longstanding part of their test suite so we need a mapping for it
        s_nodeTypeMap.put("FunctionNode", new String[] {"FUNCTION" } ); //$NON-NLS-1$ //$NON-NLS-2$
    }

}