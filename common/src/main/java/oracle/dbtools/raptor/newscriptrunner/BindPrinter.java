/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.query.Bind;

/**
 * BindPrinter to centralize the printing of binds in sqlplus and simpify the
 * way it is being printed.
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=BindPrinter.java">Barry
 *         McGillin</a>
 *
 */
public class BindPrinter {
	protected static final Logger LOGGER = Logger.getLogger(BindPrinter.class.getName());
	private static final String REFCURSOR = "REFCURSOR";
	private static final String VARCHAR2 = "VARCHAR2";
	private static final String NVARCHAR2 = "NVARCHAR2";
	private static final String CHAR = "CHAR";
	private static final String NCHAR = "NCHAR";
	private static final String BINARY_DOUBLE = "BINARY_DOUBLE";
	private static final String NUMBER = "NUMBER";
	private static final String LONG = "LONG";
	private static final String CLOB = "CLOB";
	private static final String NCLOB = "NCLOB";
	private static final String BLOB = "BLOB";
	private static final String BINARY_FLOAT = "BINARY_FLOAT";
	private static final String BFILE = "BFILE";
	private static final String COLON = ":";
	private static final String EMPTY_STRING = "";
	private static final String NL = "\n";
	private static final char NULL = '\0';
	private static final char DASH = '-';
	private static final char SPACE = ' ';

	private ScriptRunnerContext ctx;
	private BufferedOutputStream out;
	private ISQLCommand cmd;
	private int chunkSize = 4000;

	/**
	 * 
	 */
	public BindPrinter(ScriptRunnerContext context, BufferedOutputStream outputStream, ISQLCommand command) {
		ctx = context;
		out = outputStream;
		cmd = command;
	}

	public void runPrint() {
		// too much (over 100000) means output to dos gets ignored - too low =
		// performance issue (flush too often)
		report(EMPTY_STRING);
		cmd.setSql(cmd.getSql().replaceAll(COLON, EMPTY_STRING));
		String[] variablesToPrint = nextWordAndRest(cmd.getSql());
		if (variablesToPrint[1].trim().equals(EMPTY_STRING)) {
			// print all variables
			Map<String, Bind> m = getScriptRunnerContext().getVarMap();
			if (m.size() == 0) {
				report("SP2-0568: No bind variables declared.\n");
				getScriptRunnerContext().errorLog(ctx.getSourceRef(), "SP2-0568: No bind variables declared.\n", cmd.getSql());
				return;
			}
			print(m);
		} else {
			// print value of the variables
			Map<String, Bind> m = getScriptRunnerContext().getVarMap();
			String[] vars = variablesToPrint[1].split("\\s"); //$NON-NLS-1$
			print(m, vars);
		}
	}

	private void print(Map<String, Bind> m, String[] vars) {
		// report("\n");
		for (int i = 0; i < vars.length; i++) {
			String nextKey = vars[i].trim().toUpperCase();

			Bind bind = null;
			bind = m.get(nextKey.toUpperCase());
			if (bind != null) {
				if (bind.getType() != null) {
					FormattedBind formatter = new FormattedBind(bind);
					nextKey = formatter.getName();
					String value = formatter.getValue();
					if (value == null) {
						reportNull(bind, formatter);
					} else {
						reportBind(formatter);
					}
				} else {
					// not found in HashMap
					// throw an error
					report(ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNDECLARED_VARIABLE, nextKey));
					getScriptRunnerContext().errorLog(ctx.getSourceRef(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNDECLARED_VARIABLE, nextKey), cmd.getSql());
					
				}
			} else {
				report(ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNDECLARED_VARIABLE, nextKey));
				getScriptRunnerContext().errorLog(ctx.getSourceRef(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNDECLARED_VARIABLE, nextKey), cmd.getSql());
			}
		}

	}

	private void print(Map<String, Bind> m) {
		Set<String> s = m.keySet();
		synchronized (m) {
			Iterator<String> iter = s.iterator();
			while (iter.hasNext()) {
				String key = iter.next();
				Bind bind = m.get(key.toUpperCase());
				if (bind != null) {
					FormattedBind formatter = new FormattedBind(bind);
					String value = formatter.getValue();
					if (value == null) {
						reportNull(bind, formatter);
					} else {
						reportBind(formatter);
					}
				} else {
					report(ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNDECLARED_VARIABLE, key));
					getScriptRunnerContext().errorLog(ctx.getSourceRef(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNDECLARED_VARIABLE, key), cmd.getSql());
				}
			}
		}

	}

	/**
	 * @param formatter
	 * @param nextKey
	 * @param value
	 */
	private void reportBind(FormattedBind formatter) {
		if (formatter.getName().length() > 0) {
			report(formatter.getName());
			report(formatter.getDashes());
		}
		int len = formatter.getValue().length();
		int last = 0;
		int k = 0;
		for (k = chunkSize; k < len - 2; k = k + chunkSize) {
			getScriptRunnerContext().write(formatter.getValue().substring(last, k));
			try {
				getScriptRunnerContext().getOutputStream().flush();
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
			}
			last = k;
		}
		report(formatter.getValue().substring(last) + NL);

	}

	/**
	 * @param nextKey
	 * @param bind
	 * @param formatter
	 */
	private void reportNull(Bind bind, FormattedBind formatter) {
		String value;
		report(bind.getName());
		String nullSetting = (String) getScriptRunnerContext().getProperty(ScriptRunnerContext.SETNULL);
		if ((nullSetting != null) && (!(nullSetting.equals("")))) {
			report(formatter.getDashes());
			report(nullSetting + NL);
		} else {

			if (bind.getType() != null && bind.getType().startsWith(BFILE)) {
				value = "bfilename(NULL)"; //$NON-NLS-1$
				report(formatter.getDashes());
				report(value + NL); // $NON-NLS-1$
			} else {
				report(formatter.getDashes());
			}
		}
	}

	/**
	 * @param spaces
	 *            String to format column
	 * @param value
	 *            of the bind
	 * @param colsize
	 *            to print
	 * @param type
	 *            of bind
	 * @return fomratted String
	 */
	private String formatNumberDouble(String spaces, String value, int colsize, String type) {
		if ((value == null) || value.trim().length() == 0) {
			return "";
		}
		if (value.trim().equals("0"))
			value = (spaces.substring(0, (spaces.length() - value.length()))) + value;
		else if (value.trim().equalsIgnoreCase("nan")) {
			value = "Nan";
			value = (spaces.substring(0, (spaces.length() - value.length()))) + value;
		} else if (value.trim().equalsIgnoreCase("infinity")) {
			value = "Inf";
			value = (spaces.substring(0, (spaces.length() - value.length()))) + value;
		} else if (type.equalsIgnoreCase("NUMBER")) {
			value = (spaces.substring(0, (spaces.length() - value.length()))) + value;
		} else {
			NumberFormat formatter = new DecimalFormat();
			int minsize = 8;
			char pad = '#';
			if (colsize < minsize) {
				char[] chars = new char[colsize];
				Arrays.fill(chars, pad);
				value = String.valueOf(chars);
			} else {
				// 26639690: NUMBER OF POUND SIGNS(#) IS NOT CORRECTLY WHEN A NUMBER DOES NOT FIT THE COLUMN
				// String mask = "0.0#E000"; //$NON-NLS-1$ -- incorrect mask.
				String mask = "0.0E000{0}";
				int formatMask = colsize - minsize;
				String maskFiller = ""; //$NON-NLS-1$
				if (formatMask > 0) {
					char[] chars = new char[formatMask];
					Arrays.fill(chars, pad);
					maskFiller = String.valueOf(chars);
				} 
				else if (formatMask == 0) {  
					// handle formatMask for 0 case; when the numwidth = 8.
					maskFiller = "";
				}
				else {
					maskFiller = "#"; //$NON-NLS-1$
				}

				String pattern = MessageFormat.format(mask, maskFiller);
				formatter = new DecimalFormat(pattern);
				String s = formatter.format(Double.valueOf(value)); // 2,147484E9
				String[] as = s.split("E"); //$NON-NLS-1$
				String middle = "+"; //$NON-NLS-1$
				if (as.length > 1 && as[1].startsWith("-")) {
					middle = "";
				} else {
					middle = "+"; //$NON-NLS-1$
				}
				value = as[0] + "E" + middle + as[1]; //$NON-NLS-1$
				if (value.length() <= 10)
					value = (spaces.substring(0, (spaces.length() - value.length()))) + value;
			}
		}
		return value;
	}

	/**
	 * next string to character, returning the string and the rest
	 */
	private String[] nextWordAndRest(String in) {
		return nextWordAndRestOrString(in, null);
	}

	/**
	 * next string to character, returning the string and the rest
	 */
	private String[] nextWordAndRestOrString(String in, String extraOne) {
		in = in.trim();
		if (in.length() == 0) {
			return null;
		}
		int i = 0;
		boolean inQuote = false;
		boolean found = false;
		String rest = ""; //$NON-NLS-1$
		String firstWord = ""; //$NON-NLS-1$
		for (i = 0; i < in.length(); i++) {
			if (in.charAt(i) == '"') {
				if (inQuote) {
					inQuote = false;
				} else {
					inQuote = true;
				}
			}
			if ((Character.isWhitespace(in.charAt(i)) || (extraOne != null && (extraOne.indexOf(in.charAt(i)) != -1)))
					&& (inQuote == false)) {
				found = true;
				break;
			}
		}
		if (!(found == false)) {// we have whitespace
			rest = in.substring(i).trim();// trims if whitspace, no ltrim effect
											// if extraone='(' which is what is
											// wanted
			firstWord = in.substring(0, i);
		} else {
			firstWord = in;
		}
		String[] stringArray = { firstWord, rest };
		return stringArray;
	}

	private void report(String string) {
		ScriptUtils.report(ctx, string, out);
	}

	private ScriptRunnerContext getScriptRunnerContext() {
		return ctx;
	}

	class FormattedBind {
		private String dashes = "";
		private String name = "";
		private String value = "";
		private Bind bind;

		public FormattedBind(Bind b) {
			bind = b;
			format();
		}

		public String getName() {
			return name;
		}

		public String getValue() {
			return value;
		}

		public String getDashes() {
			return dashes;
		}

		public void format() {
			name = bind.getName();
			value = bind.getValue();
			dashes = new String(new char[bind.getName().length() <= 10 ? 10 : bind.getName().length()]).replace(NULL,
					DASH);
			String spaces = new String(new char[bind.getName().length() <= 10 ? 10 : bind.getName().length()])
					.replace(NULL, SPACE);

			switch (bind.getType()) {
			case LONG:
				if ((value != null) && (value
						.length() > (Integer) getScriptRunnerContext().getProperty(ScriptRunnerContext.SETLONG))) {
					value = value.substring(0,
							(Integer) getScriptRunnerContext().getProperty(ScriptRunnerContext.SETLONG));
				}
				break;

			case CLOB:
			case NCLOB:
			case BLOB:
				dashes = new String(new char[80]).replace(NULL, DASH);
				// longChunkSizeLen = length of longChunkSize
				int longchunksizeLen = (Integer) getScriptRunnerContext()
						.getProperty(ScriptRunnerContext.SETLONGCHUNKSIZE);

				// longLen = length of long
				int longLen = (Integer) getScriptRunnerContext().getProperty(ScriptRunnerContext.SETLONG);
				if (value != null) {
					// valueLen = length of value
					int valueLen = value.length();

					if (valueLen > longchunksizeLen) {

						// first truncate the data at long value.
						if (longLen < valueLen) {
							value = value.substring(0, longLen);
						}

						int truncatedValueLen = value.length();
						// if longchunksizeLen is less than length of value,
						// then we need to wrap the data set to
						// SETLONGCHUNKSIZE.
						if (longchunksizeLen < truncatedValueLen) {
							StringBuffer sb = new StringBuffer();
							int partLen = (int) Math.ceil(((double) truncatedValueLen) / longchunksizeLen);
							for (int k = 0; k < partLen; k++) {
								int startIndex = longchunksizeLen * k;
								int endIndex = longchunksizeLen * (k + 1);
								if (endIndex >= value.length()) {
									endIndex = value.length();
								}
								String partString = value.substring(startIndex, endIndex);
								sb.append(partString + NL);
							}
							value = sb.toString();
						}
					} else {
						if (longLen < valueLen) {
							value = value.substring(0, longLen);
						}
					}
				}
				break;
			case NUMBER:
			case BINARY_DOUBLE:
			case BINARY_FLOAT:
				int colsize = (int) getScriptRunnerContext().getProperty(ScriptRunnerContext.SETNUMWIDTH);
				spaces = new String(new char[bind.getName().length() <= colsize ? colsize : bind.getName().length()])
						.replace(NULL, SPACE);
				dashes = new String(new char[bind.getName().length() <= colsize ? colsize : bind.getName().length()])
						.replace(NULL, DASH);
				/*
				 * old code: this code was conditionally exiting out for numwidth less than 8. 
				if (value != null && value.length() <= spaces.length() && name != null
						&& name.length() <= spaces.length()) {
					name = (spaces.substring(0, (spaces.length() - name.length()))) + name;
					value = formatNumberDouble(spaces, value, colsize, bind.getType());
				}
				*/
				// bug 26639690: NUMBER OF POUND SIGNS(#) IS NOT CORRECTLY WHEN A NUMBER DOES NOT FIT THE COLUMN.
				name = (spaces.substring(0, (spaces.length() - name.length()))) + name;
				value = formatNumberDouble(spaces, value, colsize, bind.getType());
				
				int nameLen = name.length();
				if ((value != null) && (value.trim().length() >  0)) {
					int valueLen = value.length();				
					if (nameLen > valueLen) {
						value = String.format("%1$"+ nameLen + "s", value);
					}
				}
				
				break;
			case VARCHAR2:
			case CHAR:
			case BFILE:
				dashes = new String(new char[80]).replace(NULL, DASH);
				break;
			case REFCURSOR:
				name = ""; // Refcursors dont show title or bars
				dashes = "";
				break;
			case NCHAR:
				dashes = new String(new char[16]).replace(NULL, DASH);
				break;
			case NVARCHAR2:
				dashes = new String(new char[64]).replace(NULL, DASH);
				break;
			default:

				break;
			}
		}

	}
	
	public static String fixedLengthString(String string, int length) {
	    return String.format("%1$"+length+ "s", string);
	}
}
