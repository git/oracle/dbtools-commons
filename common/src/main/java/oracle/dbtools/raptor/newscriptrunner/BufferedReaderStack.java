/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Stack;

import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * Provides the ability to work with incomplete statements and file templates.
 * 
 * declare v_var1 int;
 * @restOfBLockDefinition.sql
 *
 * It currently does not handle arguments passed to the subblock/template
 * 
 * declare v_var1 int;
 * @restOfBLockDefinition.sql hello
 *
 * @author dermot.oneill@oracle.com
 *
 */
public class BufferedReaderStack extends Stack<BufferedReader> {
	ScriptRunnerContext _scriptRunnerContext = null;
	//have another stack to keep the current directory . required when working with deep @@file references
	Stack<String> _dirs = new Stack<String>();
	public BufferedReaderStack(BufferedReader br) {
		push(br);
	}

	@Override
	public synchronized BufferedReader push(BufferedReader br) {
		return super.push(br);
	}

	@Override
	public synchronized BufferedReader pop() {
		return super.pop();
	}

	public String readLine() throws IOException {
		return readLine(true);
	}
	
	/**
	 * 
	 * @param live when true then work with @file notation and dive into the text of that file. when false, just return the line. handy when working with multiline comments
	 * @return
	 * @throws IOException
	 * @throws PathException 
	 */
	public String readLine(boolean live) throws IOException {
		return readline(live, false, null, false, false);
	}
	
	public String readline(boolean live, boolean soFar, String soFarString, boolean stateKnown, boolean stateOK) throws IOException {
		String line = null;
		if (super.isEmpty()) {//there are no readers left, return null.
			line = null;
		} else {
			line = super.peek().readLine(); //get the next line from the current reader
			if (line == null) {
			  //This reader is exhausted, pop back up to the previous reader and continue reading it
				popStack();
				line = readline(live, soFar, soFarString, stateKnown, stateOK);
			} else {
				if (line.trim().startsWith("@") && live && (_scriptRunnerContext!=null && !_scriptRunnerContext.getRestrictedLevel().isRestricted(Restricted.Level.R3))) {//dont go into files if restricted level is R3
					if ((stateKnown && stateOK)||(!stateKnown&&soFar&&(processSoFar(soFarString, line)))) {//push a new reader to the top
						super.push(getReader(line));
						line = readLine(live);
					}
				}
			}
		}
		return line;
	}

	boolean processSoFar(String soFarString, String line) {
		if (soFarString==null||soFarString.equals("")) {
			return true;
		}
		String both=/*ScriptUtils.checkforContinuationChars*/(soFarString + "\n" +line);
		return(CoreScriptParser.amIInitial(both.substring(0,both.lastIndexOf(line)), true, false));
	}
	private void popStack() {
		if(super.peek()!=null){
			super.pop();//should I close br here ??
		}
		if(!_dirs.isEmpty()){
			_dirs.pop();
		}	
	}

	private BufferedReader getReader(String line) throws FileNotFoundException {
		ScriptParser sp = new ScriptParser(line);
		sp.parse();
		ISQLCommand[] commands = sp.getSqlStatements();
		SQLPLUS sqlplus = new SQLPLUS(commands[0],null);
		sqlplus.setScriptRunnerContext(_scriptRunnerContext);
		// Bug 22752125 checks to avoid NPE.
		if(_dirs.isEmpty() && _scriptRunnerContext.getSourceRef().length() > 0) {
			_dirs.push(getDir(_scriptRunnerContext.getSourceRef()));
		}
		if(_dirs.size() > 0) sqlplus.setDirectory(_dirs.peek());
		ExecutePathCheckReturn retVal= sqlplus.checkMultiplePaths(commands[0]);
		Reader reader = getReader(retVal);
		if(reader==null){
			reportIssue(retVal,commands[0]);
		}
		_dirs.push(getDir(getFile(retVal)));
		return new BufferedReader(reader);
	}

	private String getFile(ExecutePathCheckReturn retVal) {
		if(retVal.getOutputFile()!=null){
			return retVal.getOutputFile();
		}else if (retVal.getUrl()!= null){
			return retVal.getUrl().getFile();
		}else {
			return null;
		}
	}

	private Reader getReader(ExecutePathCheckReturn retVal) {
		if(retVal == null){
			return null;
		} if(retVal.getLiveReader()!=null){
			return retVal.getLiveReader();
		} else if(retVal.getOutputFile()!=null){
			try {
				return new BufferedReader(new InputStreamReader(new FileInputStream(new File(retVal.getOutputFile()))));
			} catch (FileNotFoundException e) {
			}
		} else if(retVal.getUrl()!=null){
			try {
				return new BufferedReader(new InputStreamReader(new FileInputStream(new File(retVal.getUrl().getFile()))));
			} catch (FileNotFoundException e) {
			}
		}
		return null;
	}

	private void reportIssue(ExecutePathCheckReturn retVal,ISQLCommand cmd) throws FileNotFoundException {
		if(retVal == null){
			if(cmd !=null){
				throw new FileNotFoundException(cmd.getSQLOrig());
			} else {
				throw new FileNotFoundException();
			}
		} else {
			if(retVal.getLastException() != null){
				throw new FileNotFoundException(retVal.getLastException().getLocalizedMessage());
			} else {
				if(cmd!=null){
				throw new FileNotFoundException(cmd.getSQLOrig());
				} else {
					throw new FileNotFoundException();
				}
			}
		}
	}

	private String getDir(String sourceRef) {
			File file = new File(sourceRef);
			if(file.isDirectory()){
				return file.getAbsolutePath();
			} else {
				return file.getParentFile().getAbsolutePath();
			}
	}

	public void setContext(ScriptRunnerContext src) {
		_scriptRunnerContext = src;	
	}

	public String readTopLine() throws IOException {
		if(isEmpty()){
			return null;
		}
		String line  =super.peek().readLine(); 
		if (line == null) {
		  //This reader is exhausted, pop back up to the previous reader and continue reading it
	    popStack();
			line = readTopLine();
		}
		return line;//get the next line from the current reader
	}
	
}
