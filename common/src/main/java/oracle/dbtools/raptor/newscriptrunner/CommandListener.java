/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.commands.Help;
import oracle.dbtools.raptor.newscriptrunner.commands.Set;

/**
 * A {@link CommandListener} is a class which when registered with {@link CommandRegistry} will be be used to add functionality to the @link {@link ScriptRunner}.  For example, all commands like @link {@link Help}, @link {@link Host}, @link {@link Set} are handled this way.
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=CommandListener.java"
 *         >Barry McGillin</a>
 * 
 */
public abstract class CommandListener {
	public abstract boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd);

	public abstract void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd);

	public abstract void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd);

	public void beginScript(Connection conn, ScriptRunnerContext ctx) {
	}

	public void endScript(Connection conn, ScriptRunnerContext ctx) {
	}
	
	/**
	 * Tests the full sqlplus command against the user's input
	 * This is case insensative
	 * This take the first 4 + optional others in the matching for example
	 * Passing "information" will return true to : info,inform,informa,... 
	 * 
	 * @param cmd - full sqlplus commant
	 * @param userCmd - user's input
	 * @return
	 */
	public static boolean matches(String cmd,String userCmd){
 	   String baseCmd = cmd.length() > 4 ? cmd.substring(0,4) : cmd;
	   StringBuilder regex = new StringBuilder();
	   regex.append("(?i:"+baseCmd+"(?i:");
	   for(int i=4; i < cmd.length();i++){
	     regex.append("|" + cmd.substring(4, i+1)); 
	   }
	   regex.append("))($| .*)");
	   return userCmd.trim().matches(regex.toString());
	}
	
}
