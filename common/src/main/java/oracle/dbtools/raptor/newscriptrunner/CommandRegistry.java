/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.raptor.newscriptrunner.commands.Accept;
import oracle.dbtools.raptor.newscriptrunner.commands.Attribute;
import oracle.dbtools.raptor.newscriptrunner.commands.ApexCmd;
import oracle.dbtools.raptor.newscriptrunner.commands.ArchiveLogList;
import oracle.dbtools.raptor.newscriptrunner.commands.AutoTrace;
import oracle.dbtools.raptor.newscriptrunner.commands.BottomTitle;
import oracle.dbtools.raptor.newscriptrunner.commands.Break;
import oracle.dbtools.raptor.newscriptrunner.commands.CTAS;
import oracle.dbtools.raptor.newscriptrunner.commands.CdCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.ChangeLoggerLevel;
import oracle.dbtools.raptor.newscriptrunner.commands.ChangeScriptRunnerContextCmd;
import oracle.dbtools.raptor.newscriptrunner.commands.Clear;
import oracle.dbtools.raptor.newscriptrunner.commands.ClearScreen;
import oracle.dbtools.raptor.newscriptrunner.commands.Column;
import oracle.dbtools.raptor.newscriptrunner.commands.Compute;
import oracle.dbtools.raptor.newscriptrunner.commands.DDLCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.DebugLoadQueryFile;
import oracle.dbtools.raptor.newscriptrunner.commands.DescribePrep;
import oracle.dbtools.raptor.newscriptrunner.commands.ErrorOut;
import oracle.dbtools.raptor.newscriptrunner.commands.FormatSQLPlusCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.Help;
import oracle.dbtools.raptor.newscriptrunner.commands.HiddenParameters;
import oracle.dbtools.raptor.newscriptrunner.commands.HostAliasCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.IStoreCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.Info;
import oracle.dbtools.raptor.newscriptrunner.commands.LoadCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.OerrCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.PLSQLLabelListener;
import oracle.dbtools.raptor.newscriptrunner.commands.Pause;
import oracle.dbtools.raptor.newscriptrunner.commands.PingCmd;
import oracle.dbtools.raptor.newscriptrunner.commands.Prompt;
import oracle.dbtools.raptor.newscriptrunner.commands.RePopulateNLSMap;
import oracle.dbtools.raptor.newscriptrunner.commands.SQLPATHShowCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.Set;
import oracle.dbtools.raptor.newscriptrunner.commands.SetAppinfo;
import oracle.dbtools.raptor.newscriptrunner.commands.SetArraysize;
import oracle.dbtools.raptor.newscriptrunner.commands.SetAutoCommit;
import oracle.dbtools.raptor.newscriptrunner.commands.SetAutoPrint;
import oracle.dbtools.raptor.newscriptrunner.commands.SetBlockTerminator;
import oracle.dbtools.raptor.newscriptrunner.commands.SetCloseCursor;
import oracle.dbtools.raptor.newscriptrunner.commands.SetColInvisble;
import oracle.dbtools.raptor.newscriptrunner.commands.SetColsep;
import oracle.dbtools.raptor.newscriptrunner.commands.SetConcat;
import oracle.dbtools.raptor.newscriptrunner.commands.SetCopyCommit;
import oracle.dbtools.raptor.newscriptrunner.commands.SetDDLSettings;
import oracle.dbtools.raptor.newscriptrunner.commands.SetDescribe;
import oracle.dbtools.raptor.newscriptrunner.commands.SetEcho;
import oracle.dbtools.raptor.newscriptrunner.commands.SetErrorLogging;
import oracle.dbtools.raptor.newscriptrunner.commands.SetEscape;
import oracle.dbtools.raptor.newscriptrunner.commands.SetFeedBack;
import oracle.dbtools.raptor.newscriptrunner.commands.SetGetPage;
import oracle.dbtools.raptor.newscriptrunner.commands.SetHeading;
import oracle.dbtools.raptor.newscriptrunner.commands.SetHeadsep;
import oracle.dbtools.raptor.newscriptrunner.commands.SetJavaProperty;
import oracle.dbtools.raptor.newscriptrunner.commands.SetLDAPCon;
import oracle.dbtools.raptor.newscriptrunner.commands.SetLinesize;
import oracle.dbtools.raptor.newscriptrunner.commands.SetLong;
import oracle.dbtools.raptor.newscriptrunner.commands.SetLongChunkSizeCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.SetNet;
import oracle.dbtools.raptor.newscriptrunner.commands.SetNetOverwrite;
import oracle.dbtools.raptor.newscriptrunner.commands.SetNewPage;
import oracle.dbtools.raptor.newscriptrunner.commands.SetNull;
import oracle.dbtools.raptor.newscriptrunner.commands.SetNumberformat;
import oracle.dbtools.raptor.newscriptrunner.commands.SetNumberwidth;
import oracle.dbtools.raptor.newscriptrunner.commands.SetOwa;
import oracle.dbtools.raptor.newscriptrunner.commands.SetPagesize;
import oracle.dbtools.raptor.newscriptrunner.commands.SetPause;
import oracle.dbtools.raptor.newscriptrunner.commands.SetPreAndPostCommands;
import oracle.dbtools.raptor.newscriptrunner.commands.SetSQLFormat;
import oracle.dbtools.raptor.newscriptrunner.commands.SetSecureLiterals;
import oracle.dbtools.raptor.newscriptrunner.commands.SetSecuredCol;
import oracle.dbtools.raptor.newscriptrunner.commands.SetServerOutput;
import oracle.dbtools.raptor.newscriptrunner.commands.SetSpool;
import oracle.dbtools.raptor.newscriptrunner.commands.SetSqlBlanklines;
import oracle.dbtools.raptor.newscriptrunner.commands.SetStatementCache;
import oracle.dbtools.raptor.newscriptrunner.commands.SetTNSAdmin;
import oracle.dbtools.raptor.newscriptrunner.commands.SetTerm;
import oracle.dbtools.raptor.newscriptrunner.commands.SetTiming;
import oracle.dbtools.raptor.newscriptrunner.commands.SetTrimOut;
import oracle.dbtools.raptor.newscriptrunner.commands.SetTrimSpool;
import oracle.dbtools.raptor.newscriptrunner.commands.SetVerify;
import oracle.dbtools.raptor.newscriptrunner.commands.SetWrap;
import oracle.dbtools.raptor.newscriptrunner.commands.SetXMLFormat;
import oracle.dbtools.raptor.newscriptrunner.commands.SetPrelim;
import oracle.dbtools.raptor.newscriptrunner.commands.SetShowMode;
import oracle.dbtools.raptor.newscriptrunner.commands.SetXQuery;
import oracle.dbtools.raptor.newscriptrunner.commands.Show;
import oracle.dbtools.raptor.newscriptrunner.commands.ShowErrors;
import oracle.dbtools.raptor.newscriptrunner.commands.StoreRegistry;
import oracle.dbtools.raptor.newscriptrunner.commands.TimingCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.TopTitle;
import oracle.dbtools.raptor.newscriptrunner.commands.TransparentGateway;
import oracle.dbtools.raptor.newscriptrunner.commands.FindCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.WhichCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.XQuery;
import oracle.dbtools.raptor.newscriptrunner.commands.alias.AliasCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.bridge.BridgeCmd;
import oracle.dbtools.raptor.newscriptrunner.commands.net.NetCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowInstance;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowLogin;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowPDBS;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowRegistry;
import oracle.dbtools.raptor.newscriptrunner.commands.show.Show_Restrict;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.newscriptrunner.restricted.RunnerRestrictedLevel;
import oracle.dbtools.raptor.utils.ExceptionHandler;

public class CommandRegistry {
    private static Map<SQLCommand.StmtSubType, List<Class<? extends CommandListener>>> m_listeners =  new LinkedHashMap<SQLCommand.StmtSubType, List<Class<? extends CommandListener>>>(); //this map is setup when the class is loaded
    private static Map<SQLCommand.StmtSubType, List<Class<? extends CommandListener>>> m_listenersCtx =  new LinkedHashMap<SQLCommand.StmtSubType, List<Class<? extends CommandListener>>>(); //this map is setup when the CommandRegistery is loaded
    
    private static Map<Connection, Map<SQLCommand.StmtSubType, List<CommandListener>>> m_connectionListeners =  Collections.synchronizedMap(new LinkedHashMap<Connection, Map<SQLCommand.StmtSubType, List<CommandListener>>>());
    private static Map<ScriptRunnerContext, Map<SQLCommand.StmtSubType, List<CommandListener>>> m_connectionListenersCtx =  Collections.synchronizedMap(new LinkedHashMap<ScriptRunnerContext, Map<SQLCommand.StmtSubType, List<CommandListener>>>());
    private static Map<String,Map<SQLCommand.StmtSubType, List<CommandListener>>> m_connectionListenersCache =  Collections.synchronizedMap(new LinkedHashMap<String,Map<SQLCommand.StmtSubType, List<CommandListener>>>());

    
    private static ArrayList<IHelp> m_cmdsWithHelp = new ArrayList<IHelp>();
    
    public synchronized static void clearCaches(final Connection connection,final ScriptRunnerContext ctx){
      m_connectionListeners.remove(connection);
      m_connectionListenersCtx.remove(ctx);
      String cacheKey = (connection == null ? "NO_CONN" : connection.hashCode() ) + ":" + (ctx== null? "NO_CTX" : ctx.hashCode() );
      m_connectionListenersCache.remove(cacheKey);
    }
    
    public synchronized static void addListener(Class<? extends CommandListener> listener, SQLCommand.StmtSubType type) {
      // dump all caches
      m_connectionListenersCache.clear();
      
        // check to make sure the arraylist exists
        if (m_listeners.get(type) == null) {
            // init the arraylist
            m_listeners.put(type, new ArrayList<Class<? extends CommandListener>>());
        }
        if (IStoreCommand.class.isAssignableFrom(listener)) {
        	try {
                IStoreCommand istore = (IStoreCommand) listener.newInstance();
                StoreRegistry.register(istore);
              } catch (Exception e) {
            	  Logger.getLogger(CommandRegistry.class.getName()).fine(listener.getName() + e.getMessage());
              }
        }
        if ( IShowCommand.class.isAssignableFrom(listener) ){
          try {
            IShowCommand ishow = (IShowCommand) listener.newInstance();
            ShowRegistry.register(ishow);
          } catch (Exception e) {
        	  Logger.getLogger(CommandRegistry.class.getName()).fine(listener.getName() + e.getMessage());
          }
        }
        if ( IHelp.class.isAssignableFrom(listener) ){          
          try {
            IHelp iHelp = (IHelp) listener.newInstance();
            m_cmdsWithHelp.add(iHelp);
          } catch (InstantiationException e) {
        	  Logger.getLogger(CommandRegistry.class.getName()).fine(listener.getName() + e.getMessage());
          }catch (IllegalAccessException e) {
        	  Logger.getLogger(CommandRegistry.class.getName()).fine(listener.getName() + e.getMessage());          }
        } else {
          Logger.getLogger(CommandRegistry.class.getName()).fine(listener.getName() + " Does not implement IHelp");
        }
        
        m_listeners.get(type).add(0, listener); // always put the latest listener first on the list.
        // that way customer defined listerners get the first crack at handling a statement, not the default built in listener
    }

    public static ArrayList<IHelp> getCommandsWithHelp() {
      return m_cmdsWithHelp;
    }
    
    public static IHelp getCommandWithHelp(String cmd) {
      for(IHelp h: m_cmdsWithHelp){
        if ( h.getCommand().equalsIgnoreCase(cmd) ){
          return h;
        }
      }
      return null;
    }
    
    
    public static void removeListener( SQLCommand.StmtSubType type) {
        if (m_listeners.get(type) == null) {
        	return;
        }
        m_listeners.get(type).clear();
        m_listeners.remove(m_listeners.get(type));
    }
        public static void updateListener(Class<? extends CommandListener> listener, SQLCommand.StmtSubType type) {
        // check to make sure the arraylist exists
        if (m_listeners.get(type) == null) {
            // init the arraylist
            m_listeners.put(type, new ArrayList<Class<? extends CommandListener>>());
        }
        m_listeners.get(type).clear();
        m_listeners.get(type).add(0, listener); // always put the latest listener first on the list.
        // that way customer defined listerners get the first crack at handling a statement, not the default built in listener
    }
    
    public static void addListenerCtx(Class<? extends CommandListener> listener, SQLCommand.StmtSubType type) {
        // check to make sure the arraylist exists
        if (m_listenersCtx.get(type) == null) {
            // init the arraylist
            m_listenersCtx.put(type, new ArrayList<Class<? extends CommandListener>>());
        }
        m_listenersCtx.get(type).add(0, listener); // always put the latest listener first on the list.
        // that way customer defined listerners get the first crack at handling a statement, not the default built in listener
    }
    
    public synchronized static Map<SQLCommand.StmtSubType, List<CommandListener>> getListeners(Connection conn, ScriptRunnerContext ctx) {
        // check if the hash is null
        if (m_connectionListeners.get(conn) == null) {
            m_connectionListeners.put(conn, new LinkedHashMap<SQLCommand.StmtSubType, List<CommandListener>>());
        }
        if (m_connectionListenersCtx.get(ctx) == null) {
            m_connectionListenersCtx.put(ctx, new LinkedHashMap<SQLCommand.StmtSubType, List<CommandListener>>());
        }
        String cacheKey = (conn == null ? "NO_CONN" : conn.hashCode() ) + ":" + (ctx== null? "NO_CTX" : ctx.hashCode() );
        
        if (m_connectionListenersCache.get(cacheKey) != null ){
          return m_connectionListenersCache.get(cacheKey);
        }

        // grab the listeners for the connection
        Map<SQLCommand.StmtSubType, List<CommandListener>> listeners = m_connectionListeners.get(conn);
        Map<SQLCommand.StmtSubType, List<CommandListener>> listenersCtx = m_connectionListenersCtx.get(ctx);
        // get all the keys
        Iterator<SQLCommand.StmtSubType> keys = m_listeners.keySet().iterator();
        SQLCommand.StmtSubType key = null;
        boolean addNew = true;
        while (keys.hasNext()) {
            key = keys.next();
            // get the registered classes
            List<Class<? extends CommandListener>> classes = m_listeners.get(key);
            for (Class<? extends CommandListener> c: classes) {
                // check against the already created listeners for the connection
                if (listeners.get(key) != null) {
                    for (CommandListener connL: listeners.get(key)) {
                        if (connL.getClass().equals(c)) {
                            addNew = false;
                        }
                    }
                } else {
                    listeners.put(key, new ArrayList<CommandListener>());
                }
                // if not found add it now
                if (addNew) {
                    CommandListener l;
                    try {
                        l = c.newInstance();
                        listeners.get(key).add(l);
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                } else {
                  addNew = true; // reset to true for the next loop
                }

            }
        }
        //do the same for context listeners - initially setSpool
        keys = m_listenersCtx.keySet().iterator();
        key = null;
        addNew = true;
        while (keys.hasNext()) {
            key = keys.next();
            // get the regitered classes
            List<Class<? extends CommandListener>> classes = m_listenersCtx.get(key);
            for (Class<? extends CommandListener> c: classes) {
                // check against the already created listeners for the connection
                if (listenersCtx.get(key) != null) {
                    for (CommandListener connL: listenersCtx.get(key)) {
                        if (connL.getClass().equals(c)) {
                            addNew = false;
                        }
                    }
                } else {
                    listenersCtx.put(key, new ArrayList<CommandListener>());
                }
                // if not found add it now
                if (addNew) {
                    CommandListener l;
                    try {
                        l = c.newInstance();
                        listenersCtx.get(key).add(l);
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                }else {
                  addNew = true; // reset to true for the next loop
                }
            }
        }
        //make a copy without duplicating any classes or providing persistent persistence
        Map<SQLCommand.StmtSubType, List<CommandListener>> joinListeners = new LinkedHashMap<SQLCommand.StmtSubType, List<CommandListener>>();
        keys = listeners.keySet().iterator();
        key = null;
        addNew = true;
        while (keys.hasNext()) {
            key = keys.next();
            // get the regitered classes
            List<CommandListener> commandListeners = listeners.get(key);
            for (CommandListener c: commandListeners) {
                // check against the already created listeners for the connection
                if (joinListeners.get(key) != null) {
                    for (CommandListener connL: joinListeners.get(key)) {
                        if (connL.getClass().equals(c)) {
                             addNew = false;
                        }
                    }
                } else {
                    joinListeners.put(key, new ArrayList<CommandListener>());
                }
                // if not found add it now
                if (addNew) {
                    CommandListener l;
                    try {
                        joinListeners.get(key).add(c);
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                } else {
                  addNew = true; // reset to true for the next loop
                }
            }
        }
        //make a copy without duplicating any classes or providing persistent persistence
        Map<SQLCommand.StmtSubType, List<CommandListener>> listenersCtxCopy = new LinkedHashMap<SQLCommand.StmtSubType, List<CommandListener>>();
        keys = listenersCtx.keySet().iterator();
        key = null;
        addNew = true;
        while (keys.hasNext()) {
            key = keys.next();
            // get the regitered classes
            List<CommandListener> commandListeners = listenersCtx.get(key);
            for (CommandListener c: commandListeners) {
                // check against the already created listeners for the context
                if (listenersCtxCopy.get(key) != null) {
                    for (CommandListener connL: listenersCtxCopy.get(key)) {
                        if (connL.getClass().equals(c)) {
                            addNew = false;
                        }
                    }
                } else {
                    listenersCtxCopy.put(key, new ArrayList<CommandListener>());
                }
                // if not found add it now
                if (addNew) {
                    CommandListener l;
                    try {
                        listenersCtxCopy.get(key).add(c);
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                } else {
                  addNew = true; // reset to true for the next loop
                }
            }
        }

        List <CommandListener> preCtx = getForAllListeners(ctx.getRestrictedLevel(),listenersCtxCopy);
        if (preCtx == null) {
            preCtx =  new ArrayList<CommandListener>();
        }
        //take from the array list clone containing real classes
        List <CommandListener> pre =  getForAllListeners(ctx.getRestrictedLevel(),joinListeners);
        if (pre == null) {
            pre =  new ArrayList<CommandListener>();
        }
        //relies on not adding to the existing ArrayList objects
        joinListeners.putAll(listenersCtxCopy);
        //we are adding to the pre arraylist from join listeners which we have recently created
        //G_S_FORALLSTMTS_STMTSUBTYPE is currently only event with multiple classes attached.
        ArrayList<CommandListener> together = new ArrayList<CommandListener>();
        for (CommandListener lis: pre) {
            if (!(together.contains(lis))) {
                together.add(lis);
            }
        }
        for (CommandListener lis: preCtx) {
            if (!(together.contains(lis))) {
                together.add(lis);
            }
        }
        //This is where G_S_FORALLSTMTS_<level> is re keyed as G_S_FORALLSTMTS_R0.
        if (together.size()>0) {
            joinListeners.put(StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE, together); 
        }
        //Map<String,Map<SQLCommand.StmtSubType, List<CommandListener>>> m_connectionListenersCache;
        m_connectionListenersCache.put(cacheKey,joinListeners);
        return joinListeners;
    }

    // handle the command
    public static boolean fireListeners(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        boolean isHandled = false;
        // put the active comp in the ctx

        Map<SQLCommand.StmtSubType, List<CommandListener>> listeners = getListeners(conn, ctx);
        // Allow a G_S_FORALLEVENTS_STMTSUBTYPE to at least see this event.
        if (listeners.containsKey(StmtSubType.G_S_FORALLEVENTS_STMTSUBTYPE)) {
            for (CommandListener lis: listeners.get(StmtSubType.G_S_FORALLEVENTS_STMTSUBTYPE)) {              
                if (!isHandled ) {
                    isHandled = lis.handleEvent(conn, ctx, cmd);
                    // break; //Allow all G_S_FORALLEVENTS_STMTSUBTYPE to be processed.
                }
            }
        }
        if (!isHandled) {
            if (listeners.containsKey(cmd.getStmtId())) {
                for (CommandListener lis: listeners.get(cmd.getStmtId())) {
                    if (!isHandled ) {
                        isHandled = lis.handleEvent(conn, ctx, cmd);
                        break; // only one listener can handle an event. this can kinda be hacked by using before or after listeners.
                    }
                }
            }
        }
        if (!isHandled) {
            //note that the listeners map is created after filtering out G_S_FORALLSTMTS_<level> which are not suitable at this RESTRICT level
            //those that are suitable are changed to G_S_FORALLSTMTS_R0. So even if you registered a listener with G_S_FORALLSTMTS_R3 (as an example). If it is allowable in this runtime RESTRICT LEVEL, it will be keyed with G_S_FORALLSTMTS_R0.
            if (listeners.containsKey(StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE)) {
                for (CommandListener lis: listeners.get(StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE)) {
                    if (!isHandled ) {
                        isHandled = lis.handleEvent(conn, ctx, cmd);
                        // break; // only one listener can handle an event. this can kinda be hacked by using before or after listeners.
                    }
                }
            }
        }

        return isHandled;
    }
    
    public static void fireBeginListeners(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        // put the active comp in the ctx
        Map<SQLCommand.StmtSubType, List<CommandListener>> listeners = getListeners(conn, ctx);
        // Allow a G_S_FORALLEVENTS_STMTSUBTYPE to at least see this event.
        if (listeners.containsKey(StmtSubType.G_S_FORALLEVENTS_STMTSUBTYPE)) {
            for (CommandListener lis: listeners.get(StmtSubType.G_S_FORALLEVENTS_STMTSUBTYPE)) {
                  lis.beginEvent(conn, ctx, cmd);
            }
        }
        if (listeners.containsKey(cmd.getStmtId())) {
            for (CommandListener lis: listeners.get(cmd.getStmtId())) {
                lis.beginEvent(conn, ctx, cmd); // all begin listeners for this type are called
            }
        }
        //note that the listeners map is created after filtering out G_S_FORALLSTMTS_<level> which are not suitable at this RESTRICT level
        //those that are suitable are changed to G_S_FORALLSTMTS_R0. So even if you registered a listener with G_S_FORALLSTMTS_R3 (as an example). If it is allowable in this runtime RESTRICT LEVEL, it will be keyed with G_S_FORALLSTMTS_R0.
        if (listeners.containsKey(StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE)) {
            for (CommandListener lis: listeners.get(StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE)) { //if you restrict a listener forall. does this mean the that command being run is restricted ?? but it listen to loads of commands, which one if the one if would have processed ?
                lis.beginEvent(conn, ctx, cmd); // all begin listeners that work on all statements
            }
        }
        //
        // Added a pre command for user to set some sql/plsql to run before any adhoc 
        // plsql or plsql
        if ( ( cmd.getStmtType() == StmtType.G_C_SQL || cmd.getStmtType() == StmtType.G_C_PLSQL ) 
            && ctx.getProperty(ScriptRunnerContext.PRE_COMMAND) != null && ! ctx.getProperty(ScriptRunnerContext.PRE_COMMAND).equals("") && 
            ( ctx.getProperty("RUNNING_PRE_POST" )== null || ctx.getProperty("RUNNING_PRE_POST" )== Boolean.FALSE ) )   {
          
          // set this so there's not recursion
          ctx.putProperty("RUNNING_PRE_POST", Boolean.TRUE);
          ScriptParser sp = new ScriptParser((String)ctx.getProperty(ScriptRunnerContext.PRE_COMMAND));
          sp.parse();
          
          ISQLCommand[] stmts = sp.getSqlStatements();
          ScriptRunner sr = new ScriptRunner(conn, ctx.getOutputStream(), ctx);          
          try {
           for(ISQLCommand stmt : stmts){
              
              sr.run(stmt);
              
            }
          } catch (IOException e) {
            ctx.write("Could not run Pre Command");
          }
          ctx.putProperty("RUNNING_PRE_POST", Boolean.FALSE);
        }
    }
    

    /**
     * Returns a list of "ForAll" CommandListeners suitable for the restriction level
     * @param runnerRestrictedLevel
     * @param listeners
     * @return
     */
    private static List<CommandListener> getForAllListeners(final RunnerRestrictedLevel.Level runnerRestrictedLevel, final Map<StmtSubType, List<CommandListener>> listeners) {
      List<CommandListener> forAllListeners = new ArrayList<CommandListener>();
     
      if(runnerRestrictedLevel == RunnerRestrictedLevel.Level.NONE){ //do not filter out any listeners
        return listeners.get(StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
      }
      
      for(CommandListener listener:listeners.get(StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE)){
        Restricted.Level level = Restricted.Level.R4; //BY DEFAULT THE RESTRICT LEVEL IS R4
        if ( listener.getClass().getAnnotation(Restricted.class) != null ) { 
          level = listener.getClass().getAnnotation(Restricted.class).level();
        }
        if(!runnerRestrictedLevel.isRestricted(level)){
          forAllListeners.add(listener);
        }
      }
      return forAllListeners;
    }
    
    public static void fireEndListeners(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
      //
      // Added a post command for user to set some sql/plsql to run before any adhoc 
      // plsql or plsql
      if ( ( cmd.getStmtType() == StmtType.G_C_SQL || cmd.getStmtType() == StmtType.G_C_PLSQL )&&
          ctx.getProperty(ScriptRunnerContext.POST_COMMAND) != null && ! ctx.getProperty(ScriptRunnerContext.POST_COMMAND).equals("")&& 
          ( ctx.getProperty("RUNNING_PRE_POST" )== null || ctx.getProperty("RUNNING_PRE_POST" )== Boolean.FALSE ) ) {
        // set this so there's not recursion
        ctx.putProperty("RUNNING_PRE_POST", Boolean.TRUE);
        ScriptParser sp = new ScriptParser((String)ctx.getProperty(ScriptRunnerContext.POST_COMMAND));
        sp.parse();
        ScriptRunner sr = new ScriptRunner(conn, ctx.getOutputStream(), ctx);          
        ISQLCommand[] stmts = sp.getSqlStatements();
        try {
         for(ISQLCommand stmt : stmts){
            sr.run(stmt);
          }
        } catch (IOException e) {
          ctx.write("Could not run Post Command");

        }
        ctx.putProperty("RUNNING_PRE_POST", Boolean.FALSE);
      }
      
        // put the active comp in the ctx
        Map<SQLCommand.StmtSubType, List<CommandListener>> listeners = getListeners(conn, ctx);
        // Allow a G_S_FORALLEVENTS_STMTSUBTYPE to at least see this event.
        if (listeners.containsKey(StmtSubType.G_S_FORALLEVENTS_STMTSUBTYPE)) {
            for (CommandListener lis: listeners.get(StmtSubType.G_S_FORALLEVENTS_STMTSUBTYPE)) {
                lis.endEvent(conn, ctx, cmd);
            }
        }
        if (listeners.containsKey(cmd.getStmtId())) {
            for (CommandListener lis: listeners.get(cmd.getStmtId())) {
                lis.endEvent(conn, ctx, cmd);
            }
        }
        //note that the listeners map is created after filtering out G_S_FORALLSTMTS_<level> which are not suitable at this RESTRICT level
        //those that are suitable are changed to G_S_FORALLSTMTS_R0. So even if you registered a listener with G_S_FORALLSTMTS_R3 (as an example). If it is allowable in this runtime RESTRICT LEVEL, it will be keyed with G_S_FORALLSTMTS_R0.
        if (listeners.containsKey(StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE)) {
            for (CommandListener lis: listeners.get(StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE)) {
                lis.endEvent(conn, ctx, cmd); // all begin listeners that work on all statements
            }
        }
        
    }
    
    public static void fireBeginScriptListeners(Connection conn, ScriptRunnerContext ctx) {
        // put the active comp in the ctx
        Map<SQLCommand.StmtSubType, List<CommandListener>> listeners = getListeners(conn, ctx);
        // Allow a G_S_FORALLEVENTS_STMTSUBTYPE to at least see this event.
        if (listeners.containsKey(StmtSubType.G_S_FORALLEVENTS_STMTSUBTYPE)) {
            for (CommandListener lis: listeners.get(StmtSubType.G_S_FORALLEVENTS_STMTSUBTYPE)) {
                lis.beginScript(conn, ctx);
            }
        }
        if (listeners.containsKey(StmtSubType.G_S_BEFOREAFTER_SCRIPT)) {
            for (CommandListener lis: listeners.get(StmtSubType.G_S_BEFOREAFTER_SCRIPT)) {
                lis.beginScript(conn, ctx); // all begin listeners that work before the script
            }
        }
    }
    
    public static void fireEndScriptListeners(Connection conn, ScriptRunnerContext ctx) {
        // put the active comp in the ctx
        Map<SQLCommand.StmtSubType, List<CommandListener>> listeners = getListeners(conn, ctx);
        // Allow a G_S_FORALLEVENTS_STMTSUBTYPE to at least see this event.
        if (listeners.containsKey(StmtSubType.G_S_FORALLEVENTS_STMTSUBTYPE)) {
            for (CommandListener lis: listeners.get(StmtSubType.G_S_FORALLEVENTS_STMTSUBTYPE)) {
                lis.endScript(conn, ctx);
            }
        }
        if (listeners.containsKey(StmtSubType.G_S_BEFOREAFTER_SCRIPT)) {
            for (CommandListener lis: listeners.get(StmtSubType.G_S_BEFOREAFTER_SCRIPT)) {
                lis.endScript(conn, ctx); // all begin listeners that work before the script
            }
        }
    }
    
    static {
        // add script runner extensions
        // Listen to only a specific statement. These listeners either perform the action itself
        // or they set a property in the context for the SQLCommand to reference.
        // sometimes they modify the SQL of the command in the beginEvent, ex: Describe.
      CommandRegistry.addForAllStmtsListener(SetStatementCache.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
    	CommandRegistry.addListener(Accept.class, StmtSubType.G_S_ACCEPT);
    	CommandRegistry.addListener(Clear.class, StmtSubType.G_S_CLEAR);
    	CommandRegistry.addListener(TopTitle.class, StmtSubType.G_S_TTITLE);
    	CommandRegistry.addListener(BottomTitle.class, StmtSubType.G_S_BTITLE);
    	CommandRegistry.addListener(ClearScreen.class, StmtSubType.G_S_CLEAR_SCREEN);
      CommandRegistry.addListener(Help.class, StmtSubType.G_S_HELP);
		  CommandRegistry.addListener(DDLCommand.class, StmtSubType.G_S_DDL);

        //set echo is a ctx listener as setting survives connects
        CommandRegistry.addListener(SetAppinfo.class, StmtSubType.G_S_SET_APPINFO);
        CommandRegistry.addListener(SetArraysize.class, StmtSubType.G_S_SET_ARRAYSIZE);
        CommandRegistry.addListener(SetCopyCommit.class, StmtSubType.G_S_SET_COPYCOMMIT);
        CommandRegistry.addListener(SetNull.class, StmtSubType.G_S_SET_NULL);
        CommandRegistry.addListener(SetNet.class, StmtSubType.G_S_SET_NET);
        CommandRegistry.addListener(SetNetOverwrite.class, StmtSubType.G_S_SET_NETOVERWRITE);
        CommandRegistry.addListener(SetPagesize.class, StmtSubType.G_S_SET_PAGESIZE);
        CommandRegistry.addListener(SetNewPage.class, StmtSubType.G_S_SET_NEW_PAGE);
        CommandRegistry.addListener(SetErrorLogging.class, StmtSubType.G_S_SET_ERROR_LOGGING);
        CommandRegistry.addListener(SetLinesize.class, StmtSubType.G_S_SET_LINESIZE);
        CommandRegistry.addListener(SetLong.class, StmtSubType.G_S_SET_LONG);
        CommandRegistry.addListener(SetColsep.class, StmtSubType.G_S_SET_COLSEP);
        CommandRegistry.addListener(SetHeading.class, StmtSubType.G_S_SET_HEADING);
        CommandRegistry.addListener(SetHeadsep.class, StmtSubType.G_S_SET_HEADINGSEP);
        CommandRegistry.addListener(SetTrimSpool.class, StmtSubType.G_S_SET_TRIMSPOOL);
        CommandRegistry.addListener(SetTrimOut.class, StmtSubType.G_S_SET_TRIMOUT);
        CommandRegistry.addListener(SetWrap.class, StmtSubType.G_S_SET_WRAP);
        CommandRegistry.addListener(SetPrelim.class, StmtSubType.G_S_SET_PRELIM);
        CommandRegistry.addListener(SetShowMode.class, StmtSubType.G_S_SET_SHOWMODE);
        CommandRegistry.addListener(SetNumberwidth.class, StmtSubType.G_S_SET_NUMBERWIDTH);
        CommandRegistry.addListener(SetNumberformat.class, StmtSubType.G_S_SET_NUMBERFORMAT);
		    CommandRegistry.addListener(SetSecureLiterals.class, StmtSubType.G_S_SET_SECURELITERALS);
        CommandRegistry.addListener(SetTerm.class, StmtSubType.G_S_SET_TERM);
        CommandRegistry.addListener(SetFeedBack.class, StmtSubType.G_S_SET_FEEDBACK);
        CommandRegistry.addListener(Pause.class, StmtSubType.G_S_PAUSE);
        CommandRegistry.addListener(Prompt.class, StmtSubType.G_S_PROMPT);
        CommandRegistry.addListener(Prompt.class, StmtSubType.G_S_DOC_PLUS);
        CommandRegistry.addListener(PLSQLLabelListener.class, StmtSubType.G_S_PLSQLLABEL);
        CommandRegistry.addForAllStmtsListener(TransparentGateway.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(HiddenParameters.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addListener(XQuery.class, StmtSubType.G_S_XQUERY);
        CommandRegistry.addListener(Show.class, StmtSubType.G_S_SHOW);//there are lots of small show options should be covered in the same class
        //CommandRegistry.addListener(CLIDefinePrompter.class, StmtSubType.G_S_UNKNOWN);
        CommandRegistry.addListener(Set.class, StmtSubType.G_S_SET);
        CommandRegistry.addListener(DebugLoadQueryFile.class, StmtSubType.G_S_LOADQUERYFILE);
        CommandRegistry.addListener(ChangeLoggerLevel.class, StmtSubType.G_S_SETLOGLEVEL);
        CommandRegistry.addListener(SetJavaProperty.class, StmtSubType.G_S_SET_PROPERTY);
        CommandRegistry.addListener(SetConcat.class, StmtSubType.G_S_SET_CONCAT);
        CommandRegistry.addListener(SetEscape.class, StmtSubType.G_S_SET_ESCAPE);
        CommandRegistry.addListener(SetBlockTerminator.class, StmtSubType.G_S_SET_BLO);
        CommandRegistry.addListener(SetSqlBlanklines.class, StmtSubType.G_S_SET_SQLBL);
        CommandRegistry.addListener(DescribePrep.class, StmtSubType.G_S_DESCRIBE);
        CommandRegistry.addListener(SetDescribe.class, StmtSubType.G_S_SET_DESCRIBE);
        CommandRegistry.addListener(Column.class, StmtSubType.G_S_COLUMN);
        CommandRegistry.addListener(ArchiveLogList.class, StmtSubType.G_S_ARCHIVE);
        CommandRegistry.addListener(BridgeCmd.class, StmtSubType.G_S_BRIDGE);
        CommandRegistry.addListener(OerrCommand.class, StmtSubType.G_S_OERR);
        CommandRegistry.addListener(AliasCommand.class, StmtSubType.G_S_ALIAS);
        CommandRegistry.addListener(NetCommand.class, StmtSubType.G_S_NET);
        CommandRegistry.addListener(CdCommand.class, StmtSubType.G_S_CD);
        CommandRegistry.addListener(SQLPATHShowCommand.class, StmtSubType.G_S_SQLPATH);
        CommandRegistry.addListener(FormatSQLPlusCommand.class, StmtSubType.G_S_FORMAT);
        CommandRegistry.addListener(Break.class, StmtSubType.G_S_BREAK);
        CommandRegistry.addListener(HostAliasCommand.class, StmtSubType.G_S_HOSTALIAS);
        CommandRegistry.addListener(HostAliasCommand.class, StmtSubType.G_S_HOST);
        CommandRegistry.addListener(Compute.class, StmtSubType.G_S_COMPUTE);
        CommandRegistry.addListener(TimingCommand.class, StmtSubType.G_S_TIMING);
        CommandRegistry.addListener(SetLongChunkSizeCommand.class, StmtSubType.G_S_SET_LONGC);
        CommandRegistry.addListener(CTAS.class,StmtSubType.G_S_CTAS);
        CommandRegistry.addListener(LoadCommand.class, StmtSubType.G_S_LOAD);
        CommandRegistry.addListener(Attribute.class, StmtSubType.G_S_ATTRIBUTE);
        CommandRegistry.addListener(SetSecuredCol.class, StmtSubType.G_S_SET_SECUREDCOL);
        CommandRegistry.addListener(SetXMLFormat.class, StmtSubType.G_S_SET_XMLFORMAT);
        CommandRegistry.addListener(SetXMLFormat.class, StmtSubType.G_S_SET_XMLOPTIMIZATIONCHECK);
        CommandRegistry.addListener(SetCloseCursor.class, StmtSubType.G_S_SET_CLOSECURSOR);
        CommandRegistry.addListener(SetColInvisble.class, StmtSubType.G_S_SET_COLINVISIBLE);//this is used in ShowRegistery and StoreRegistery
        
        CommandRegistry.addListener(FindCommand.class, StmtSubType.G_S_FIND);
        CommandRegistry.addListener(WhichCommand.class, StmtSubType.G_S_WHICH);
        CommandRegistry.addListener(SetXQuery.class, StmtSubType.G_S_SET_XQUERY);
        //Additional commands that need to be addForAllStmtsListener 
        //as they have fire everywhere where on statements. rather than fire for that 'set' line
        CommandRegistry.addForAllStmtsListener(SetXQuery.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(SetDDLSettings.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(SetGetPage.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(SetAutoPrint.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        // Listen to every statement.
        // These Statements perform some action (mostly before or after another command).
        // First, for itself, its status is set in the context, then all other commands check if the status is on.
        CommandRegistry.addForAllStmtsListenerCtx(SetSpool.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(AutoTrace.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(SetOwa.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(SetPause.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);//There is a SET_PAUSE command as well, which one is used?
        CommandRegistry.addForAllStmtsListener(SetServerOutput.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListenerCtx(SetTiming.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(ChangeScriptRunnerContextCmd.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        //set echo is a ctx listener as setting survives connects
        CommandRegistry.addForAllStmtsListenerCtx(SetVerify.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListenerCtx(SetEcho.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListenerCtx(SetAutoCommit.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
       
        CommandRegistry.addForAllStmtsListener(ErrorOut.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(PingCmd.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(Info.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(ApexCmd.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(SetPreAndPostCommands.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(SetSQLFormat.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        
        CommandRegistry.addForAllStmtsListener(SetLDAPCon.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE); 
        
        CommandRegistry.addForAllStmtsListener(SetTNSAdmin.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        CommandRegistry.addForAllStmtsListener(RePopulateNLSMap.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
        
        /** Leaving this in till after testing **/
        CommandRegistry.addForAllStmtsListener(ShowErrors.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE); // this will never be hit, as SHOW command handles the SHOW cmd and Show.java SQLCommand has a registery of SQLCommands which handle the different types of SHOW ..
        CommandRegistry.addForAllStmtsListener(ShowPDBS.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE); // this will never be hit, as SHOW command handles the SHOW cmd and Show.java SQLCommand has a registery of SQLCommands which handle the different types of SHOW ..
        CommandRegistry.addForAllStmtsListener(ShowInstance.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE); // this will never be hit, as SHOW command handles the SHOW cmd and Show.java SQLCommand has a registery of SQLCommands which handle the different types of SHOW ..
        CommandRegistry.addForAllStmtsListener(Show_Restrict.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE); // this will never be hit, as SHOW command handles the SHOW cmd and Show.java SQLCommand has a registery of SQLCommands which handle the different types of SHOW ..
        CommandRegistry.addForAllStmtsListener(ShowLogin.class,StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);// this will never be hit, as SHOW command handles the SHOW cmd and Show.java SQLCommand has a registery of SQLCommands which handle the different types of SHOW ..
    }   
    
  // overload for backwards compatibility
  public static void addForAllStmtsListener(Class<? extends CommandListener> className) {
    CommandRegistry.addListener(className, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
  }
  
  public static void addForAllStmtsListener(Class<? extends CommandListener> className, SQLCommand.StmtSubType subtype) {
    CommandRegistry.addListener(className, subtype);
  }

  private static void addForAllStmtsListenerCtx(Class<? extends CommandListener> className, SQLCommand.StmtSubType subtype) {
    CommandRegistry.addListenerCtx(className, subtype);
  }

}
