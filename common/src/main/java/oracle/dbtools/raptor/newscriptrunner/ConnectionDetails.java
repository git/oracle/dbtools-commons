/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.sql.Connection;

public class ConnectionDetails {
		String m_fullString;
        boolean m_callDialog;
        String m_connectName;
        String m_connectPassword;
        String m_connectDB;
        String m_role;
        boolean m_roleBad;
        int m_at;//if !-1 -> database given or blank
        int m_slash;//if !-1 password given or blank
        Connection m_conn;//allows connection from call dialog to be used instead of closed and reconnect be careful it is used or closed (try/finally block.
        boolean m_callUsage=false;
		private String m_edition;
       

		public ConnectionDetails(String fullString, boolean callDialog, String connectName, String connectPassword, String connectDB, String role, boolean roleBad, int at, int slash, Connection conn, String edition) {
	    	init(fullString,callDialog,connectName,connectPassword,connectDB,role,roleBad,at,slash,conn,edition);

        }

    public ConnectionDetails(String fullString, boolean callDialog, String connectName, String connectPassword, String connectDB, String role, boolean roleBad, int at, int slash, Connection conn) {
    	init(fullString,callDialog,connectName,connectPassword,connectDB,role,roleBad,at,slash,conn,"");
    }

    private void init(String fullString, boolean callDialog, String connectName, String connectPassword,
			String connectDB, String role, boolean roleBad, int at, int slash, Connection conn, String edition) {
    	 m_fullString = fullString;
         m_callDialog = callDialog;
         m_connectName = connectName;
         m_connectPassword = connectPassword;
         m_connectDB = connectDB;
         m_role = role;
         m_roleBad = roleBad;
         m_at = at;//==-1 no active at
         m_slash = slash;//==-1 no active slash
         m_conn = conn; //allows dialog test connection to be use for main connection - -> no cost to call call dialog
         //m_callUsage = ScriptUtils.newConnectionDetailsError(this);
     	m_edition = edition;
	}

	public void setCallDialog(boolean callDialog) {
        m_callDialog = callDialog;
    }

    public void setConnectName(String connectName) {
        m_connectName = connectName;
    }

    public void setConnectPassword(String connectPassword) {
        m_connectPassword = connectPassword;
        m_slash=1;
        rebuildFullText();
    }

    public void setConnectDB(String connectDB) {
        m_connectDB = connectDB;
        m_at=1;
        rebuildFullText();
    }

    public boolean isCallDialog() {
        return m_callDialog;
    }

    public String getFullString() {
        return m_fullString;
    }
    public String getConnectName() {
        return m_connectName;
    }

    public String getConnectPassword() {
        return m_connectPassword;
    }

    public String getConnectDB() {
        return m_connectDB;
    }

    public String getRole() {
        return m_role;
    }

    public boolean getRoleBad() {
        return m_roleBad;
    }

    public int getAt() {
        return m_at;
    }
    public int getSlash() {
        return m_slash;
    }
    public Connection getConnection() {
        return m_conn;
    }
    public void setConnection(Connection conn) {
        m_conn = conn;
    }
    public boolean getCallUsage() {
        return m_callUsage;
    }

    public void setCallUsage(boolean callUsage) {
        m_callUsage = callUsage;
 
    }
    public String getEdition() {
    	//trim " and '
    	m_edition=m_edition.trim();
        if ((m_edition.length() > 2) && (m_edition.startsWith("\"") && m_edition.endsWith("\""))) { //$NON-NLS-1$ //$NON-NLS-2$
            // indouble quotes
            return m_edition.substring(1, m_edition.length() - 1);
        }
        if ((m_edition.length() > 2) && (m_edition.startsWith("'") && m_edition.endsWith("'"))) { //$NON-NLS-1$ //$NON-NLS-2$
            // in single quotes
            return m_edition.substring(1, m_edition.length() - 1);
        }
		return m_edition;
	}

	public void setEdition(String edition) {
		m_edition = edition;
	}
    private void rebuildFullText() {
    	String rebuild="";
    	String[] pre={"","/","@"," "," "};
        Boolean[] test={true,getSlash()!=-1,getAt()!=-1,((getRole()!=null)&&(!(getRole().equals("")))),!getEdition().equals("")};
        String[] val={this.getConnectName(), this.getConnectPassword(), this.getConnectDB(), this.getRole(), this.getEdition()};
        for (int counter=0;counter<pre.length;counter++) {
        	if (test[counter]) {
        		rebuild+=pre[counter]+val[counter];
        	}
        }
        m_fullString=rebuild;
    }
    public void cloneFrom(ConnectionDetails source) {
        m_fullString = source.getFullString();
        m_callDialog = source.isCallDialog();
        m_connectName = source.getConnectName();
        m_connectPassword = source.getConnectPassword();
        m_connectDB = source.getConnectDB();
        m_role = source.getRole();
        m_roleBad = source.getRoleBad();
        m_at = source.getAt();//==-1 no active at
        m_slash = source.getSlash();//==-1 no active slash
        m_conn = source.getConnection(); //allows dialog test connection to be use for main connection - -> no cost to call call dialog
        m_callUsage = source.getCallUsage();
    }
}