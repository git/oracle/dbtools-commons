/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.util.Set;

import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;


/**
 * Core of ScriptParser available as a seperate class - largely for determining state at a 
 * point in a string for @ and & expansion.
 * & - not in comments
 * @ only in initial state (not in quotes aliases or comments).
 * Additional functionality - can lex part of a line (up to where & or @ is)
 * @author totierne
 *
 */
public class CoreScriptParser {
    
    final static int INITIAL = 0;
    final static int INSINGLELINECOMMENT = 1;
    final static int INMULTILINECOMMENT = 2;
    final static int INALIAS = 3;
    final static int INSINGLEQUOTE = 4;
    final static int INQUOTEDQUOTE = 5;
    private int m_state = INITIAL;
    private boolean m_firstLine = true;
    private StringBuilder m_lastLine =new StringBuilder();
    private String m_quoteString = ""; //$NON-NLS-1$
    private String m_leftOver=null;
    private String m_setLeftOver=null;
    
    /**
     * create refine is a mechanism to change what initially looked like ; terminated to / terminated
     * not needed on second 'substitution' parse for finding if & in comment.
     * ie set createRefine==null to skip.
     */
    protected ISQLCommand createRefine=null;
    protected boolean createRefineReturnOnSemicolon=false;
    protected boolean createRefineSemiColonTerminates=false;
    protected StringBuffer createRefineLineSoFar=null;
    public CoreScriptParser() {
        standaloneConfig();
    }

    public void standaloneConfig() {
        initialiseState();
        m_firstLine = true;
        m_quoteString = ""; //$NON-NLS-1$
        m_leftOver=null;
        m_setLeftOver=null;
        m_lastLine.setLength(0);  //$NON-NLS-1$
        
        createRefine=null;//assumption createRefine==null means we are reparsing complete statements 
        //do not have to worry about end of statement - reparse so far used for @ and & search
        createRefineReturnOnSemicolon=false;
        createRefineSemiColonTerminates=false;
        createRefineLineSoFar=null;
    }
    
    /**
     * sustitution '&' check
     * @param inString string up to a qualified &
     * @param concat whether concatenation char
     * @param withNewline whether it is partial or full line
     * @return whether an & at this point is live.
     */
    public boolean incrementalOkForSubst(String inString, boolean concat, boolean withNewline) {
        numberOfQuotes(inString, concat, withNewline);
        return okForSubst();
    }
        /**
         * amIInitial old dead code removed now uses coreScriptParser.
         * 
         * @param inString am I in the initial state after processing this.
         * @param concat take concatenation char into account
         * @returns is (m_state == initial) not initially allow in ' or " or q'< 
         */
    public  static boolean amIInitial(String inString, boolean concat, boolean withNewline) {
        CoreScriptParser coreParse=new CoreScriptParser();
        coreParse.standaloneConfig();//get top 'pauses' on first ; or \ so end char irrelevant
        coreParse.numberOfQuotes(inString, true, withNewline);
        return coreParse.okForAt();
    }

    public static String getEndQuoteString(String inQuote) {
        String retVal = inQuote;
        if (retVal.equals("[")) { //$NON-NLS-1$
            retVal = "]"; //$NON-NLS-1$
        } else if (retVal.equals("{")) { //$NON-NLS-1$
            retVal = "}"; //$NON-NLS-1$
        } else if (retVal.equals("<")) { //$NON-NLS-1$
            retVal = ">"; //$NON-NLS-1$
        } else if (retVal.equals("(")) { //$NON-NLS-1$
            retVal = ")"; //$NON-NLS-1$
        }
        return retVal;
    }
    
    /**
     * Small String/regexp function is called millions of times in apex test install run.
     */
    public String stripLine(String line) {
        String comp = line;
        comp = comp.replaceAll("/\\*.*\\*/", ""); // remove multiline comments
        comp = comp.replaceAll("--.*?\n", ""); // remove single line comments
        comp = comp.replaceAll(" ", ""); //$NON-NLS-1$ //$NON-NLS-2$
        comp = comp.replaceAll("\n", ""); //$NON-NLS-1$ //$NON-NLS-2$
        comp = comp.replaceAll("\r", ""); //$NON-NLS-1$ //$NON-NLS-2$
        comp = comp.replaceAll("\t", ""); //$NON-NLS-1$ //$NON-NLS-2$
        comp = comp.toLowerCase();
        return comp;
    }
    protected SQLCommand getPropertiesWhenMulitpleTokensKnown(String line) {
        String strippedLine = stripLine(line.trim());
        Set<String> keys = SQLStatementTypes.multiToken.keySet();
        for (String key: keys) {
            if (strippedLine.startsWith(key)) {
                return SQLStatementTypes.multiToken.get(key);
            }
        }
        return null;
    }
    /**
     * set firstLine == false if we are not ended by - and do not start with ^--
     * 
     * @param inString
     */
    void setFirstLine(String inString, boolean withNewline) {
        if (m_firstLine&&withNewline) {
            if (!(((m_lastLine+inString).replaceAll("\\s+$", "").endsWith("-")) && (!((m_lastLine+inString).replaceAll("^\\s+", "").startsWith("--"))))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
                m_firstLine = false;
            }
        }
    }
    private static String[][] m_startAndEnd = { { "", "", "0" }, { "--", "\n", "1" }, { "/*", "*/", "2" }, { "\"", "\"", "3" }, { "'", "'", "4" }, { "q'", "'", "5" } }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$

    public void initialiseState() {
        m_state=INITIAL;
    }
    public boolean stateInitial() {
        return m_state==INITIAL;
    }
    public String getLeftOver() {
        return m_leftOver;
    }
    public void setLeftOver(String inLeft) {
        m_leftOver=inLeft;
    }
    public String getSetLeftOver() {
        return m_setLeftOver;
    }
    public void setSetLeftOver(String inLeft) {
        m_setLeftOver=inLeft;
    }

    
    private void createRefineRecalculate() {
        if (createRefine==null) {
            //reparsing for state on substitution & not for end of statement.
            return;
        }
        if (createRefine.getProperty(SQLCommand.CREATEREFINE)==null||
                createRefine.getProperty(SQLCommand.CREATEREFINE).equals(Boolean.FALSE)) {
            return;
        }
        if (!(createRefine.getStmtSubType().equals(StmtSubType.G_S_CREATE_UNKNOWN)||
                ((createRefine.getStmtSubType().equals(StmtSubType.G_S_WITH))
                &&(createRefine.getStmtType().equals(StmtType.G_C_SQL))))){
            return;
        }
        //issues are 1/ create /* comment */ not differenciated and we have to flip from sql to plsql
        //           2/ with /*comment */ function with /*comment*/ procedure we have to flip from sql to plsql
        //           3/ set is late evaluated so always is sql -> OK anyway.
        //Stripping and refine done here on first possibly terminating ;
        String stripSoFar=ScriptUtils.stripFirstN(createRefineLineSoFar.toString(), 400, true, false);
        SQLCommand command=getPropertiesWhenMulitpleTokensKnown(stripSoFar);
        if (command!=null&&(command.isCreatePLSQLCmd()||
                ((command.getStmtSubType().equals(StmtSubType.G_S_WITH))
                        &&(command.getStmtType().equals(StmtType.G_C_PLSQL))))) {
            //reset ; expectations and morph statement
            createRefineReturnOnSemicolon=false;
            createRefineSemiColonTerminates=false;
            createRefine.setStmtType(command.getStmtType());
            createRefine.setStmtClass(command.getStmtClass());
            createRefine.setStmtSubType(command.getStmtSubType());
            createRefine.setResultsType(command.getResultsType());
            createRefine.setExecutable(command.getExecutable());
            createRefine.setRunnable(command.getRunnable());
        }
        createRefine.setProperty(SQLCommand.CREATEREFINE,Boolean.FALSE);
    }
//core additional use reparse of statement 
//(i.e. end of statement already handled)  for seeing if & in 
    /**
     * counts the quotes, if multiline recurses per line. relies on state and firstLine class variables.
     * 
     * @param inString string to be checked
     * @param returnOnSemicolon if I hit a semicolon in initial state I return and give its position to allows a continuation from the left over at
     *        that point.
     * @param concat
     * @return new int[] {noogquotes, do I end with a semicolon, where is a semicolon in initial}
     */
    public int[] numberOfQuotes(String inString/*, boolean returnOnSemicolon*/, boolean concat) {
        return numberOfQuotes(inString, concat, true);
    }
    /**
     * couts the quotes note not ends with newline can be does to check state for substitution .
     * @param inString string to scan
     * @param concat whether concatenation char is to be honoured
     * @param withNewline false only used in coreScriptParser has to track half lines
     * @return new int[] {noogquotes, do I end with a semicolon, where is a semicolon in initial}
     */
        public int[] numberOfQuotes(String inString/*, boolean returnOnSemicolon*/, boolean concat, boolean withNewline) {

        int noOfQuotes = 0;
        int position = 0;
        int embeddedSemicolon = 0;
        int initialSemicolon = -1;
        inString = inString.replaceAll("\r", ""); //$NON-NLS-1$ //$NON-NLS-2$
        if (inString.indexOf('\n') != -1) {
            int retVal[] = null;
            int lengthSoFar = 0;
            // for each line
            // recurse numberOfQuotes
            // on return return on semicolon or keep summing up number of
            // quotes note sometimes we return on semicolon sometimes we do not
            //this.createRefineLineSoFar=new StringBuffer();
            String[] lineArray=(inString).split("\n",-1);//ensure last entry is not discarded.
            for (int lineNo=0;lineNo<lineArray.length;lineNo++) { //$NON-NLS-1$
                String s=lineArray[lineNo];
                //last line we need to pass on withNewline
                if (lineNo==lineArray.length-1) {
                    retVal = numberOfQuotes(s/*, returnOnSemicolon*/, concat, withNewline);  //$NON-NLS-1$
                } else {
                    retVal = numberOfQuotes(s/*, returnOnSemicolon*/, concat);
                }
                noOfQuotes = noOfQuotes + retVal[ 0 ];
                if ((createRefineReturnOnSemicolon) && (retVal[ 2 ] != -1)) {
                    return new int[] { noOfQuotes, retVal[ 1 ], retVal[ 2 ] + lengthSoFar };
                }
                //last length so far irrelevant (off by one)
                lengthSoFar = lengthSoFar + s.length() + "\n".length(); //$NON-NLS-1$
            }
            /**
             * note relies on multiline got from gettop not getting important ; in the middle the non multiline numberofquotes has an ends with ;
             * field
             */
            return new int[] { noOfQuotes, retVal[ 1 ], retVal[ 2 ] };
        }
        while (true) {
            if (m_state == INITIAL) {
                int lowestMatch = 100000;
                /* int[] retArray=lookForNextMatch(inString,position); */
                int matchVal = 0;
                int i = 1;
                // System.out.println(startAndEnd.length);
                for (i = 1; i < m_startAndEnd.length; i++) {
                    int nextMatch = inString.indexOf(m_startAndEnd[ i ][ 0 ], position);
                    if ((nextMatch != -1) && (nextMatch < lowestMatch)) {
                        if (!((i == INQUOTEDQUOTE) && (nextMatch + 2 == inString.length()))) {// not at end of line
                            lowestMatch = nextMatch;
                            matchVal = i;
                        }
                    }
                }
                i = 5;
                int nextMatch = inString.indexOf("Q'", position); //$NON-NLS-1$
                if ((nextMatch != -1) && (nextMatch < lowestMatch)) {
                    if (!((i == INQUOTEDQUOTE) && (nextMatch + 2 == inString.length()))) {// not at end of line
                        lowestMatch = nextMatch;
                        matchVal = i;
                    }
                }
                if ((createRefine!=null)&&(createRefine.getProperty(SQLCommand.CREATEREFINE)!=null&&
                        createRefine.getProperty(SQLCommand.CREATEREFINE).equals(Boolean.TRUE))) { 
                    if (createRefineReturnOnSemicolon) {
                        //morph into a plsql possibility
                        int nextSemi=inString.indexOf(";", position);
                        if ((nextSemi != -1) && (nextSemi < lowestMatch)) {
                            //already added createRefineLineSoFar.append(inString);
                            this.createRefineRecalculate();
                        }
                    }
                }
                if (createRefineReturnOnSemicolon) {
                    nextMatch = inString.indexOf(";", position); //$NON-NLS-1$
                    if ((nextMatch != -1) && (nextMatch < lowestMatch)) {
                        setFirstLine(inString, withNewline);
                        
                        initialSemicolon = nextMatch;
                        updateLastLine(withNewline,m_lastLine,inString);
                        int[] retval = { noOfQuotes, 0, initialSemicolon };
                        return retval;
                    }
                }
                if (matchVal == INQUOTEDQUOTE) {// get character
                    m_quoteString = inString.substring(lowestMatch + 2, lowestMatch + 3);
                    // Leave jdbc to tell them it boolean isValid=isQuoteStringValid(quoteString);
                    m_quoteString = getEndQuoteString(m_quoteString);
                }
                if ((matchVal == INQUOTEDQUOTE)) {
                    noOfQuotes++;
                    m_state = INQUOTEDQUOTE;
                    position = lowestMatch + 3;
                } else if (matchVal == INSINGLEQUOTE) {
                    noOfQuotes++;
                    m_state = INSINGLEQUOTE;
                    position = lowestMatch + 1;
                }
                // do not want to go into insinglelinecomment if
                // firstLine +
                // this is a minus --
                // following line is an insinglelinecomment
                // ^--
                //else if ((matchVal == INSINGLELINECOMMENT) && (m_firstLine) && (inString.trim().equals("--"))) {
                //    position = lowestMatch+2;
                //}
                else if ((matchVal == INSINGLELINECOMMENT) && (m_firstLine) && (withNewline) && ((inString).substring(lowestMatch).replaceAll("\\s+$", "").equals("--")) && (!((m_lastLine+inString).replaceAll("\\s+$", "").replaceAll("^\\s+", "").equals("--")))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$
                    position = lowestMatch + 1;
                } else {
                    if (matchVal == 0) {
                        setFirstLine(inString, withNewline);
                        updateLastLine(withNewline,m_lastLine,inString);
                        int[] retval = { noOfQuotes, 0, initialSemicolon };
                        return retval;
                    }
                    m_state = matchVal;
                    position = lowestMatch + m_startAndEnd[ matchVal ][ 0 ].length();
                }
            } else {
                // look for end chars adding number of quotes and special q'x x' character.
                String endString = m_startAndEnd[ m_state ][ 1 ];
                if (m_state == INQUOTEDQUOTE) {
                    endString = m_quoteString + endString;
                }
                boolean stillGrabbing = false;
                int newPos = inString.indexOf(endString, position);
                // if state == INSINGLEQUOTE eat until string does not end in 
                if ((concat) && (m_firstLine) && (m_state == INSINGLELINECOMMENT)) {
                    if ((newPos == -1) && ((withNewline==false)||
                            (withNewline&&(((m_lastLine+inString).replaceAll("\\s+$", "").endsWith("-")) && (!((m_lastLine+inString).replaceAll("^\\s+", "").startsWith("--"))))))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
                        stillGrabbing = true;
                    }
                    while (newPos != -1) {
                        boolean grabAgain = (((inString.substring(position, newPos)).replaceAll("\\s+$", "").endsWith("-") && (!((m_lastLine+inString).replaceAll("^\\s+", "").startsWith("--"))))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
                        if (grabAgain) {
                            if (newPos + 1 <= inString.length()) {
                                newPos = inString.indexOf(endString, newPos + 1);
                            } else {
                                stillGrabbing = true;
                                newPos = -1;
                            }
                        } else {
                            break;
                        }
                    }
                }
                if (newPos == -1) {
                    if ((m_state == INSINGLELINECOMMENT)&&(withNewline)) {
                        if (!(stillGrabbing)) {
                            m_state = INITIAL;// end of line ends comment if not still grabbing
                        }
                        // if line.trim(endsWith(";")embeddedSemicolon=1;
                        if (inString.trim().endsWith(";")) { //$NON-NLS-1$
                            embeddedSemicolon = 1;
                        }
                    } else if ((m_state != INITIAL)&&(withNewline)) {
                        if (inString.trim().endsWith(";")) { //$NON-NLS-1$
                            embeddedSemicolon = 1;
                        }
                    }
                    setFirstLine(inString, withNewline);
                    updateLastLine(withNewline,m_lastLine,inString);
                    int[] retval = { noOfQuotes, embeddedSemicolon, initialSemicolon };
                    return retval;
                }
                position = newPos + endString.length();
                if ((m_state == INSINGLEQUOTE) || (m_state == INQUOTEDQUOTE)) {
                    noOfQuotes++;
                    m_quoteString = ""; //$NON-NLS-1$
                }
                m_state = INITIAL;
            }
        }
        /*
         * int noOfQuotes=0; if state is initial look for next start string and pick the lowest numbered one that is not -1 to change state and
         * restart if state is not initial look for end character [on quote transtion increase number of quotes] if run out of character or all
         * searches are -1 return
         */
    }
    /**
     * Convenience function due to concatenation char if 
     * withNewline==false lastLine needs to be remembered
     * @param withNewline full line being lexed
     * @param lastLine lastline so far
     * @param inString current line/
     */
    void updateLastLine(boolean withNewline, StringBuilder lastLine, String inString) {
        if (withNewline) {
            lastLine.setLength(0);
        } else {
            lastLine.append(inString);
        }
    }
    /**
     * @ check made a function so easily changed and m_state not externalised
     * @return whether @ at this point is an include file
     */
    boolean okForAt() {
        //maybe where classic == true allow everywhere
        //90% sure if we do not want to @ expand in ' " or q'< certainly not in comments -- or /*
        return (m_state==INITIAL);
    }
    /**
     * & check made a function so easily changed and m_state not externalised
     * @return whether & at this point is a substitution
     */
    boolean okForSubst() {
        //maybe where classic == true allow everywehere
        return ((m_state!=INSINGLELINECOMMENT)&&(m_state!=INMULTILINECOMMENT));
    }
}
