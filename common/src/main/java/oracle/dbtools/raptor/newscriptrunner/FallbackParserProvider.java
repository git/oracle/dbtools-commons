/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FallbackParserProvider implements Iterator<ISQLCommand> {
    private ScriptParser m_parser = null;
    private ISQLCommand m_nextCmd = null;
    // all logging in this class can go through this
    private Logger m_logger = Logger.getLogger(getClass().getName());

    /**
     * Default Constructor takes a reader
     * @param runCtx 
     * 
     * @param reader
     */
    public FallbackParserProvider(ScriptRunnerContext runCtx, Reader reader) {
        m_parser = new ScriptParser(reader);// read all lines
        m_parser.setScriptRunnerContext(runCtx);
    }


    /**
     * Returns true if there is another statement left to iterate over
     */
    public boolean hasNext() {
        ISQLCommand cmd = null;
        try {
            if (m_nextCmd != null) {
                return true;
            } else {
                cmd = getNextValidCmd();
                m_nextCmd = cmd;
            }
        } catch (Exception e) {
            return false;
        }
        if (cmd != null) {
            return true;
        } else {
            return false;
        }
    }

    private ISQLCommand getNextValidCmd() {
    	ISQLCommand cmd = null;
        try {
            cmd = m_parser.next();
        } catch (IOException e1) {
            // always at the very least log an exception, never ignore
            m_logger.log(Level.SEVERE, e1.getStackTrace()[ 0 ].toString(), e1);
        }
        while (cmd != null && !isValidCommand(cmd)) {
            try {
                cmd = m_parser.next();
            } catch (IOException e) {
                // always at the very least log an exception, never ignore
                m_logger.log(Level.SEVERE, e.getStackTrace()[ 0 ].toString(), e);
            }
        }
        if (cmd != null && isValidCommand(cmd)) {
            return cmd;
        } else {
            return null;
        }
    }

    private boolean isValidCommand(ISQLCommand cmd) {
        return true;
    }

    /*
     * Returns the next SQLCommand from the script
     */
    public ISQLCommand next() {
        ISQLCommand sqlCommand = null;
        try {
            if (m_nextCmd != null) {
                sqlCommand = m_nextCmd;
                m_nextCmd = null;
            } else {
                sqlCommand = getNextValidCmd();
            }
        } catch (Exception e) {
            // always at the very least log an exception, never ignore
            m_logger.log(Level.SEVERE, e.getStackTrace()[ 0 ].toString(), e);
        }
        return sqlCommand;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.Iterator#remove()
     */
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
