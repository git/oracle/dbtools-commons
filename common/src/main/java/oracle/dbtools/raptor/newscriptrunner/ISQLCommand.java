/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.util.List;

import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

public interface ISQLCommand {
  public static final String PROP_STATUS_BOOLEAN = "prop_status_boolean"; //$NON-NLS-1$
  public static final String PROP_STRING = "prop_file_string"; //$NON-NLS-1$
  public static final String PROP_PRINTED_STRING = "prop_printed_string";
  public static final String PROP_ERROR_STRING = "prop_error_string";
  public static final String PROP_IS_WRAPPED = "prop_is_wrapped"; //note once declared wrapped you never become unwrapped.//$NON-NLS-1$
  public static final String PROP_DBMS_OUTPUT_CALLED = "prop_dbms_output_called"; //$NON-NLS-1$

  public abstract String getLoweredTrimmedNoWhitespaceSQL();

  public abstract String getLoweredTrimmedSQL();

  /**
     * stops a query from being run by the script runner
     * @return
     */
  public abstract boolean getRunnable();

  /**
   * defines if a command is able to directly run the original sql against the database
   * @return
   */
  public abstract boolean getExecutable();

  /**
   * stops a query from being run by the script runner
   * @param isRunnable
   */
  public abstract void setRunnable(boolean isRunnable);

  /**
   * defines if a command is able to directly run the original sql against the database
   * @param isRunnable
   */
  public abstract void setExecutable(boolean isRunnable);

  public abstract String getSql();

  public abstract void setSql(String sql);

  public abstract SQLCommand.StmtType getStmtClass();

  public abstract void setStmtClass(SQLCommand.StmtType stmtClass);

  public abstract SQLCommand.StmtSubType getStmtId();

  public abstract void setStmtId(SQLCommand.StmtSubType stmtId);

  public abstract int getLines();

  public abstract void setLines(int lines);

  public abstract String toString();

  public abstract SQLCommand.StmtResultType getResultsType();

  public abstract void setResultsType(SQLCommand.StmtResultType resultsType);

  public abstract List<Object> getBinds();

  public abstract void setBinds(List<Object> binds);

  public abstract void addBind(Object o);

  public abstract void setSQLOrig(String sqlOrig);

  public abstract String getSQLOrig();

  public abstract String getSQLOrigWithTerminator();

  public abstract void setModifiedSQL(String modifiedSQL);

  public abstract String getModifiedSQL();

  public abstract void setStartLine(int line);

  public abstract int getStartLine();

  public abstract void setEndLine(int line);

  public abstract int getEndLine();

  public abstract SQLCommand.StmtSubType getStmtSubType();

  public abstract SQLCommand.StmtType getStmtType();

  public abstract void setStmtSubType(SQLCommand.StmtSubType stmtSubType);

  public abstract void setStmtType(SQLCommand.StmtType stmtType);

  public abstract String getStmtProperty();

  public boolean isSqlPlusSetCmd();

  public boolean isCreateCmd();

  public Object getProperty(String key);

  public void setProperty(String key, Object value);

  public abstract boolean isCreateSQLCmd();

  public abstract boolean isCreatePLSQLCmd();

  public String getStatementTerminator();

  public boolean equals(Object sqlCommand);

  public long getTiming();

  public void setTiming(long elapsedTime);

  public boolean isTopLevel();

  public void setTopLevel(boolean topLevel);

  public abstract boolean isComplete();

  public abstract void setFail();

  public abstract boolean isFail();

  public abstract String getSqlWithTerminator();

  public boolean isSetCompoundCmd();

  public String getCompoundSetcmd();

  public String getReadableType();

  public abstract int getdepth();

  public abstract void setDepth(int depth);

  public void setBatchStatements(List<String> stmts);

  public List<String> getBatchStatements();

  public Restricted.Level getRestrictedLevel();
  
}