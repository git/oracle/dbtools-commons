/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;

import oracle.sql.CharacterSet;

/**
 * This will be used when the result of a script is to be returned in JSON format.
 * All characters written to the output will come through here.
 * Some will already be in JSON format (like the query results)
 * Some will not have any JSON formatting (like DML, DDL statements). These will be formatted to JSON before writing to the out stream.
 * As JSON results typically require more information than standard results (like the orig statement). 
 * There will be some member variables to hold additional information and this information will be inserted into the output in json format.
 * This class will also reference the current command being run
 */
public class JSONWrapBufferedOutputStream extends WrapListenBufferOutputStream {
	OutputStream _out = null;
	boolean _firstDecoratedLine = true;
	ArrayList<String> _extraStmtInfo = new ArrayList<String>();
	ArrayList<String> _dbmsOutput = new ArrayList<String>();
	

	public JSONWrapBufferedOutputStream(OutputStream out, ScriptRunnerContext ctx) {
		super(out, ctx);
		_out = out;
	}

	/**
	 * call this before a statement is run with the script runner.
	 * its a flag to help format the json output between statements
	 */
	public void newStmt(){
		_firstDecoratedLine =true;
	}
	
	/**
	 * chance to decorate a "string" or "line" or array of bytes.
	 * decoration just puts the "line" withing quotes.
	 * this makes the list of strings within the RESPONSE item in the JSON result
	 * @param b
	 * @param decorate
	 * @throws IOException
	 */
	public void write(byte[] b, boolean decorate) throws IOException {
		checkForExit();
		if (decorate && shouldDecorate(b)) {
			super.write(decorate(b));
		} else {
			super.write(b);
		}
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		checkForExit();
		byte [] subArray = Arrays.copyOfRange(b, off, len);
		if (shouldDecorate(subArray)) {
			byte[] decorated = decorate(subArray);
			super.write(decorated,0,decorated.length);
		} else {
			super.write(b,off,len);
		}
	}
	
	/**
	 * Do not decorate FORCE_PRINT or FORCE_PRINT_BYTES
	 * @param b
	 * @return
	 */
	private boolean shouldDecorate(byte[] b) {
		try {
			if (Arrays.equals(ScriptRunner.FORCE_PRINT.getBytes("UTF8"), b) || Arrays.equals(ScriptRunner.FORCE_PRINT_BYTES, b)) {
				return false;
			}
		} catch (UnsupportedEncodingException e) {
			//ignore
		}
		return true;
	}

	@Override
	public void write(byte[] b) throws IOException {
		write(b, true);
	}

	/**
	 * Add quotes around an array of bytes.
	 * put a , first if it is not the first line for a statement. 
	 * Its important to set newStmt() so that we know when a , is required
	 * @param b
	 * @return
	 */
	private byte[] decorate(byte[] b) {
		try {
			if(_firstDecoratedLine){
				_firstDecoratedLine = false;
				return (quote(new String(b, ScriptRunnerContext.ALWAYSUTF8), true)).getBytes(ScriptRunnerContext.ALWAYSUTF8);
			} else {
				return (","+quote(new String(b, ScriptRunnerContext.ALWAYSUTF8), true)).getBytes(ScriptRunnerContext.ALWAYSUTF8);
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	@Override
	public void close() throws IOException {
		super.flush();
		if (!(boolean) getScriptRunnerContext().getProperty(ScriptRunnerContext.SYSTEM_OUT)) {
			super.close();
		}
	}
	/**
  * Add some extra JSON information to be written out at a later time when writeExtraInfo is called.
  * @param errorInfo
  */
	public void addStmtInfo(String errorInfo) {
		if(!_extraStmtInfo.contains(errorInfo)){
			_extraStmtInfo.add(errorInfo);
		}
	}

	/**
	 * any information added with addStmtInfo, which was buffered, is now written to the output and the buffer cleared
	 */
  public void writeExtraInfo() {
    try {
        for (String extraStmtInfo : _extraStmtInfo) {
          //this stops writing a "," with nothing after it
          if(extraStmtInfo != null && !extraStmtInfo.trim().equals("")){
            write(",".getBytes(ScriptRunnerContext.ALWAYSUTF8), false);
            write(extraStmtInfo.getBytes(ScriptRunnerContext.ALWAYSUTF8), false);
          } 
      }
    } catch (UnsupportedEncodingException e) {
      // ignore
    } catch (IOException e) {
      // ignore
    } finally {
      _extraStmtInfo.clear();
    }
  }
	
	/**
	 * Take a string and quote it for JSON . So internal quotes need to be escaped and so on
	 * @param string
	 * @param addQuotes
	 * @return
	 */
	public static String quote(String string, boolean addQuotes) {
		if (string == null || string.length() == 0) {
			if (addQuotes) {
				return "\"\"";
			} else {
				return "";
			}
		}

		char c = 0;
		int i;
		int len = string.length();
		StringBuilder sb = new StringBuilder(len + 4);
		String t;
    //dont wrap in quotes if it already starts and ends with quotes
		if(addQuotes && string.charAt(0)=='"' && string.charAt(string.length()-1)=='"'){
		  addQuotes = false;//already quoted
		}
		if (addQuotes) {
			sb.append('"');
		}
		for (i = 0; i < len; i += 1) {
			c = string.charAt(i);
			switch (c) {
			case '\\':
			case '"':
			  if(addQuotes){ //dont escape outer quotes if add quotes is on and it has quotes
			    sb.append('\\');
			  }
				sb.append(c);
				break;
			case '/':
				sb.append('\\');
				sb.append(c);
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\r':
				sb.append("\\r");
				break;
			default:
				if (c < ' ') {
					t = "000" + Integer.toHexString(c);
					sb.append("\\u" + t.substring(t.length() - 4));
				} else {
					sb.append(c);
				}
			}
		}
		if (addQuotes) {
			sb.append('"');
		}
		return sb.toString();
	}
	
}