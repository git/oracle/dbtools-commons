/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.Writer;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.MetaResource;
import oracle.dbtools.common.utils.StringUtils;
import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.ConnectionResolver;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.db.VersionTracker;
import oracle.dbtools.logging.Timer;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.raptor.compiler.OraSubmitResults;
import oracle.dbtools.raptor.compiler.PlSqlErrorInfo;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.datatypes.impl.DataTypeImpl;
import oracle.dbtools.raptor.datatypes.values.ResultSetValue;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.commands.SetServerOutput;
import oracle.dbtools.raptor.query.Bind;
import oracle.dbtools.raptor.query.Bind.Mode;
import oracle.dbtools.raptor.query.Parser;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryXMLSupport;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.dbtools.raptor.utils.ExceptionHandler;
import oracle.dbtools.raptor.utils.oerr.Oerr;
import oracle.dbtools.raptor.utils.oerr.OerrException;
import oracle.dbtools.util.Debug;
import oracle.dbtools.util.Service;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.CLOB;

public class PLSQL extends SQLCommandRunner {
	SQLWarning warn = null;
	public boolean errorToWorksheet = false;

	public PLSQL(ISQLCommand cmd, BufferedOutputStream out) {
		super(cmd, out);
	}

	public void run() {
		// get the lock
		if (!LockManager.lock(conn)) {
			return;
		}
		Debug.debug(Messages.getString("PLSQL.0") + cmd.getSql()); //$NON-NLS-1$
		getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, null);
		try {
			Timer timer = new Timer();
			timer.start();

			errorToWorksheet = false;
			if (cmd.isCreatePLSQLCmd()) {
				runCreatePLSQL();
				reportfinish();
			} else {
				/*
				 * report finish moved into runNonCreatePLSQL so it is before
				 * internal result sets.
				 */
				runNonCreatePLSQL();
			}
			Debug.debug(Messages.getString("PLSQL.1")); //$NON-NLS-1$

			timer.end();
			cmd.setTiming(timer.duration());
		} catch (SQLException e) {
			cmd.setFail();
			handleException(e);
		} finally {
			if (cstmt != null) {
				try {
					cstmt.close();
				} catch (SQLException e) {
				}
			}
			;
			if (callStmt != null) {
				try {
					callStmt.close();
				} catch (SQLException e) {
				}
			}
			;
			// release the lock
			LockManager.unlock(conn);
		}
	}

	private void handleException(SQLException e) {
		cmd.setFail();
		String toSend = e.getMessage();
		ExceptionHandler.handleException(e, connName, true, false);
		// stuff in the ctx that an err happened
		getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_ENCOUNTERED, Boolean.TRUE);
		getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_FOR_AUTOCOMMIT, Boolean.TRUE);
		getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, e.getMessage());
		getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_SQLCODE, e.getErrorCode());
		doWhenever(true);
		toSend = adjustColError(toSend);
		getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, toSend);
		if (conn instanceof OracleConnection) {
			try {
				Oerr oerr = new Oerr();
				toSend += (oerr.oerr(toSend));
			} catch (OerrException oerre) {
				// ignore
			}
		}
		if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) != null
				&& Boolean.parseBoolean(
						getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
			String[] lines = cmd.getSQLOrig().split("\n"); //$NON-NLS-1$
			String line = null;
			int lineNo = getLineNo(toSend);
			int colNo = getColNo(toSend);
			boolean isExecStmt = false;
			if (checkForExecuteStmt(lines)) {
				line = cmd.getModifiedSQL();
				lines = cmd.getModifiedSQL().split("\n"); //$NON-NLS-1$
				isExecStmt = true;
			} else
				line = cmd.getSQLOrig();

			if (lines.length > 0 && lineNo > 0 && lineNo <= lines.length) {
				line = lines[lineNo - 1];
			}

			String asterix = "";
			if (colNo <= 1) {
				asterix = "*";
			} else {
				asterix = new String(new char[colNo]).replace('\0', ' ');
				asterix = asterix.substring(0, asterix.length() - 1) + "*"; //$NON-NLS-1$
			}
			asterix = "\n" + asterix + "\n"; //$NON-NLS-1$ //$NON-NLS-2$
			String msg = Messages.getString("PLSQL.9") + lineNo + ":\n" + adjustColError(e.getMessage()) + "\n"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			if (!isExecStmt) {
				report(getScriptRunnerContext(), line + asterix + msg);
			} else 
				report(getScriptRunnerContext(), line + "\n" + asterix + msg);
		} else {
			report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1),
					cmd.getSQLOrig(), toSend, this.getScriptRunnerContext()));
		}
		getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), e.getMessage(), cmd.getSql());
	}

	private boolean checkForExecuteStmt(String[] lines) {
		String exec = lines[0];
		if ((exec.trim().toLowerCase().startsWith("exec")) || exec.trim().toLowerCase().startsWith("execute")) { //$NON-NLS-1$ //$NON-NLS-2$
			return true;
		}
		return false;
	}

	/**
	 * Replacing binds with literals warrants column adjustment
	 */
	private String adjustColError(String msg) {
		String sqlOrig = cmd.getSQLOrig();
		String[] linesOrig = sqlOrig.split("\n"); //$NON-NLS-1$
		String origLine = sqlOrig;
		String sqlMod = cmd.getModifiedSQL();
		String[] linesMod = sqlMod.split("\n"); //$NON-NLS-1$
		String modLine = sqlMod;
		List<Long> lineCols = getLineCols(msg);
		for (long lineCol : lineCols) {
			int lineNo = Service.lX(lineCol);
			int colNo = Service.lY(lineCol);
			if (linesOrig.length > 0 && lineNo > 0 && lineNo <= linesOrig.length) {
				origLine = linesOrig[lineNo - 1];
			}
			if (linesMod.length > 0 && lineNo > 0 && lineNo <= linesMod.length) {
				modLine = linesMod[lineNo - 1];
			}
			if (colNo < modLine.length())
				modLine = modLine.substring(0, colNo);
			int adjust = calcAdjValue(origLine, modLine);
			int fixedCol = colNo + adjust;
			if (doubleBindPrepareErrorOffset < colNo + adjust) {
				fixedCol -= doubleBindPrepareErrorOffset;
				fixedCol = calcDoubleBindAdjustmentOffset(fixedCol);
			}
			msg = msg.replace("line " + lineNo + ", column " + colNo, "line " + lineNo + ", column " + fixedCol);
		}
		return msg;
	}

	private static int calcAdjValue(String origLine, String modLine) {
		List<LexerToken> orig = LexerToken.parse(origLine);
		List<LexerToken> mod = LexerToken.parse(modLine);
		int mpos = 0;
		int opos = -1;
		int adjust = 0;
		for (LexerToken ot : orig) {
			opos++;
			if (mod.size() <= mpos)
				break;
			LexerToken mt = mod.get(mpos);
			if (mt.content.equals(ot.content)) {
				mpos++;
				continue;
			}
			int start = mt.begin;
			mpos = mpos + 4;
			if (mod.size() <= mpos)
				break;
			mt = mod.get(mpos);
			adjust = ot.content.length() - (mt.end - start);
			mpos++;
		}
		return adjust;
	}

	/**
	 * Return the line number of a plsql error in the command that was run
	 * Format of a PLSQL error. ORA-06550: line 3, column 1: PLS-00201:
	 * identifier 'CRAPOLA' must be declared ORA-06550: line 3, column 1:
	 * 
	 * @param message
	 * @return line number
	 */
	private int getLineNo(String message) {
		String[] lines = message.split("\n"); //$NON-NLS-1$
		if (lines.length > 0 && lines[0].contains("ORA-06550") && lines[0].contains("line")) { //$NON-NLS-1$ //$NON-NLS-2$
			String[] bits = lines[0].split("\\s+"); //$NON-NLS-1$
			if (bits.length > 3 && bits[2].endsWith(",")) { //$NON-NLS-1$
				String lineString = bits[2].substring(0, bits[2].length() - 1);
				try {
					int x = Integer.parseInt(lineString);
					return x;
				} catch (NumberFormatException e) {
				}
			}
		}
		// No idea what it is so give back 1
		return 1;
	}

	/**
	 * Return the line number of a plsql error in the command that was run
	 * Format of a PLSQL error. ORA-06550: line 3, column 1: PLS-00201:
	 * identifier 'CRAPOLA' must be declared ORA-06550: line 3, column 1:
	 * 
	 * @param message
	 * @return line number
	 */

	private int getColNo(String message) {
		String[] lines = message.split("\n"); //$NON-NLS-1$
		if (lines.length > 0 && lines[0].contains("ORA-06550") && lines[0].contains("column")) { //$NON-NLS-1$ //$NON-NLS-2$
			String[] bits = lines[0].split("\\s+"); //$NON-NLS-1$
			if (bits.length > 4 && bits[2].endsWith(",")) { //$NON-NLS-1$
				String lineString = bits[4].substring(0, bits[4].length() - 1);
				try {
					int y = Integer.parseInt(lineString);
					return y;
				} catch (NumberFormatException e) {
				}
			}
		}
		// No idea what it is so give back 1
		return 1;
	}

	/**
	 * There can be more than one ORA-06550
	 * 
	 * @param message
	 * @return Service.pair(line, col)
	 */
	private List<Long> getLineCols(String message) {
		List<Long> ret = new LinkedList<Long>();
		String[] lines = message.split("\n"); //$NON-NLS-1$
		for (String line : lines) {
			if (line.contains("ORA-06550") && line.contains("column") && line.contains("line")) {
				String[] bits = line.split("\\s+"); //$NON-NLS-1$
				if (bits.length > 4 && bits[2].endsWith(",")) { //$NON-NLS-1$
					String lineString = bits[2].substring(0, bits[2].length() - 1);
					String colString = bits[4].substring(0, bits[4].length() - 1);
					try {
						int x = Integer.parseInt(lineString);
						int y = Integer.parseInt(colString);
						ret.add(Service.lPair(x, y));
					} catch (NumberFormatException e) {
					}
				}
			}
		}
		return ret;
	}

	private void reportfinish() {
		String objectType = "anon"; //$NON-NLS-1$
		String objectName = null;
		parseNameAndType(cmd.getSql());
		objectType = type != null ? type : objectType;
		objectName = name;
		// Hack to get java output right.
		if (getScriptRunnerContext().isSQLPlusClassic() && objectType.toLowerCase().startsWith("java"))
			objectType = "Java";

		boolean errors = false;
		if (objectType.equals("anon") && (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF)) { //$NON-NLS-1$
			report(getScriptRunnerContext(), Messages.getString("PLSQL.4")); //$NON-NLS-1$
		} else if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF) {
			if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SILENT) == null) {
				if ((getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) != null
						&& Boolean.parseBoolean(getScriptRunnerContext()
								.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString()))) {
					if (warn != null) {
						report(getScriptRunnerContext(), MessageFormat.format(Messages.getString("PLSQL.101"), //$NON-NLS-1$
								new Object[] { StringUtils.initCap(objectType) }));
					} else {
						report(getScriptRunnerContext(), MessageFormat.format(Messages.getString("PLSQL.102"), //$NON-NLS-1$
								new Object[] { StringUtils.initCapSingle(objectType) }));
					}
					if ((objectName != null) && (!(objectName.equals("")))) { //$NON-NLS-1$
						if (ScriptUtils.isHttpCon(this.getConn(), getScriptRunnerContext())) {
							errors = false;// show error requires binds
						} else {						  
							errors = reportErrors(objectName, true);
						}
					}
					return;
				} else {
					if (objectType.toUpperCase().equals("JAVA SOURCE")) { //$NON-NLS-1$
						report(getScriptRunnerContext(), MessageFormat.format(Messages.getString("PLSQL.5Created"), //$NON-NLS-1$
								StringUtils.initCap(objectType), objectName));
					} else {
						report(getScriptRunnerContext(), MessageFormat.format(Messages.getString("PLSQL.5"), //$NON-NLS-1$
								StringUtils.initCap(objectType), objectName));
					}
				}
				if ((objectName != null) && (!(objectName.equals("")))) { //$NON-NLS-1$
					if (!(ScriptUtils.isHttpCon(this.getConn(), getScriptRunnerContext()))) {
						errors = reportErrors(objectName, true);
					}
				}
			}
		} else if ((objectName != null) && (!(objectName.equals("")))) { //$NON-NLS-1$
			// we want to have object details for later "show errors" command.
			// and mask the happy days created message when feedback off.
			errors = reportErrors(objectName, false);
		}

		if (errors) {
			report(getScriptRunnerContext(), Messages.getString("PLSQL.29")); //$NON-NLS-1$
		} else if (warn != null && !(ConnectionResolver.isOracle(conn) && warn.getErrorCode() == 17110)
				&& warn.getMessage() != null) {
			reportWarning(warn);
		}
	}

	/**
	 * checks for double binds - sets doubleBind if any found.
	 */
	void checkForDoubleBind(String text) {
		DBUtil dbUtil = DBUtil.getInstance(conn);
		List<String> binds = DBUtil.getBindNames(text, true);
		HashSet<String> uniqueBinds = new HashSet<String>();
		HashSet<String> doubleBindsLocal = new HashSet<String>();
		boolean doubleBindFound = false;
		// Edge case - user is supplying their own bind list - do not want to
		// apply double binds fix, as double binds adds an extra bind per double
		// bind.
		
		if ((cmd.getBinds() != null) && (cmd.getBinds().size() > 0)) {
			doubleBinds = null;
			return;
		}
		/*
		 * check for dups hashset? - check if its in a list no put it in already
		 * there -> duplicate need to handle upper lower case (yes)?
		 */
		for (String bind : binds) {
			// Due to auto set bind to null - treat all binds as double binds
			// SQLDEv specific bug: Bug 23244020 - BIND VARIABLES NOT PERSISTING
			// THROUGHOUT SESSION
			// Old bug - jdbc says not a bug 16471439 - 12C BIND ISSUE IN
			// WORKSHEET
			// Removedboolean
			// inAlready=(!uniqueBinds.add(bind.toUpperCase(Locale.US)));
			if (true/* inAlready */) {
				doubleBindFound = true;
				doubleBindsLocal.add(bind.toUpperCase(Locale.US));
			}
		}
		if (doubleBindFound == true) {
			/* rewrite sql */
			doubleBinds = doubleBindsLocal;
		}
	}

	/*
	 * like get setbinds but taking into account length and done one bind at a
	 * time
	 */
	protected String getBindType(String bindName) throws SQLException {
		boolean setBind = true;
		String type = null;
		String typeBracket = null;
		boolean bindSetInCommand = false;
		int bindIdx = 1;
		Map<String, Object> popupVars = (Map<String, Object>) getScriptRunnerContext()
				.getProperty(ScriptRunnerContext.POPUPBINDS);
		Map<String, Bind> vars = getScriptRunnerContext().getVarMap();
		// try and bind with a bindmap passed in (like from reports)
		Map<String, Bind> externalVars = (Map<String, Bind>) getScriptRunnerContext().getProperty("BINDMAP"); //$NON-NLS-1$
		if (externalVars == null) {
			externalVars = new HashMap<String, Bind>();
		}
		boolean external = false;
		boolean local = false;
		boolean popup = false;
		if (popupVars.containsKey(bindName)) {
			popup = true;
		}
		if (vars.containsKey(bindName.toUpperCase())) {
			local = true;
		}
		if (externalVars.containsKey(bindName)) {
			external = true;
		}
		//local is used by SQL*Plus as well as ORDS restSQL
		if ((popup)||  (local) || (external)) {
			Bind bind = null;
			String value = null;
			if (popup) {
				if ((popupVars.get(bindName)).equals(DBUtil.NULL_VALUE)) {
					value = null;
				} else {
					value = (String) (popupVars.get(bindName));
				}
				type = "VARCHAR2"; //$NON-NLS-1$ //safe to default to (32767)
			} else if (local) {
				bind = vars.get(bindName.toUpperCase());
				type = bind.getType();
				typeBracket = bind.getBracket();
				if (typeBracket != null) {
					type = typeBracket;
				}
				value = bind.getValue();
			} else {
				bind = externalVars.get(bindName);
				type = bind.getType();
				typeBracket = bind.getBracket();
				if (typeBracket != null) {
					type = typeBracket;
				}
				value = bind.getValue();
			}
      if (type != null) {
        if (type.indexOf("(") == -1) { //$NON-NLS-1$
          if (type.toUpperCase().indexOf("NCHAR") > -1) { //$NON-NLS-1$
            type = "NVARCHAR2(32767)"; //$NON-NLS-1$
          } else if ((type.toUpperCase().indexOf("CHAR") > -1) && (type.toUpperCase().indexOf("CHAR2") == -1)) { //$NON-NLS-1$ //$NON-NLS-2$
            // ie char but not nvarchar2 or varchar2
            type = "VARCHAR2(32767)"; //$NON-NLS-1$
          } else if (type.toUpperCase().indexOf("NCHAR") > -1 || type.toUpperCase().indexOf("NVARCHAR") > -1 //$NON-NLS-1$ //$NON-NLS-2$
              || type.toUpperCase().indexOf("CHAR") > -1 || type.toUpperCase().indexOf("VARCHAR") > -1) { //$NON-NLS-1$ //$NON-NLS-2$
            type = type + "(32767)"; //$NON-NLS-1$
          } else if (type.toUpperCase().equals("REFCURSOR")) { //$NON-NLS-1$
            type = "SYS_REFCURSOR"; //$NON-NLS-1$
          }
        }
      }
			// bindIdx++;
			// } else if (cmd.getBinds().size() > 0) {//TODO do not know what
			// this was for.
			// bindSetInCommand = true;
		} else {
			// Bind variable used is not in the Var Map. not declared. This will
			// error our in set bind do not report twice
			type = null;
		}
		return type;
	}

	public void setMyUniq(String anonPLSQL) {
		String localMyUniq = myUniq.toUpperCase(Locale.US);
		String anonUpper = anonPLSQL.toUpperCase(Locale.US);
		int count = 1;
		while ((anonUpper.contains(localMyUniq + count + "Z"))) { //$NON-NLS-1$
			count++;
		}
		myUniq = myUniq + count + "Z"; //$NON-NLS-1$
	}

	private int doubleBindPrepareErrorOffset = 0;

	public final String doubleBindPrepareOracleSql(String query) throws SQLException {
		// PreparedStatement pstmt = null;
		setMyUniq(query);
		List<Bind> parserdBinds = Parser.getInstance().getBinds(query, true);
		HashMap<String, Integer> lookUpCount = new HashMap<String, Integer>();
		String sql = query;
		int count = 1;
		HashSet<String> alreadySkipped = new HashSet<String>();
		// put the headere in one line so error numbers line wise or ok- so
		// line/column errors only busted :
		// if double bind is used in the first line which seams like a good
		// trade off (then column will be off)
		// also if the error is in the header or footer it could confuse as
		// where it is will be invisible.
		// List<String> parserdBindsString = new ArrayList<String>();
		// for (Bind b: parserdBinds) {
		// parserdBindsString.add(b.getName());
		// }
		/*
		 * ensure longest bind first so :blahb does not get confused with :blah
		 * TODO write a testcase to verify this
		 */
		// Collections.sort(parserdBindsString);
		//Collections.reverse(parserdBinds); Do not change list - might be cached/shared
		String header = "DECLARE "; //$NON-NLS-1$
		String footer = ""; //$NON-NLS-1$
		if (parserdBinds!=null) {
			for (int curBind=(parserdBinds.size()-1);curBind>=0;curBind--) {
				Bind b=parserdBinds.get(curBind);
				String bString = b.getName();
				String bindUpper = bString.toUpperCase(Locale.US);
				if (this.doubleBinds.contains(bindUpper)) {
					// treat differently all go to ZSQlDevUnIq - plsql vars first
					// time add to header
					// used to blow up if :b.getName is in a comment - now using
					// char offsets from the lexer.
					// sql = sql.replace(":" + bString, myUniq+"_" + bindUpper);
					// //$NON-NLS-1$ //$NON-NLS-2$
					if ((alreadySkipped.add(bindUpper))) {
						lookUpCount.put(bindUpper, count);
						String bindType = getBindType(bString);
						if (bindType == null) {
							doubleBindPrepareErrorOffset = 0;
							return null;
						}
						if (bindType.equalsIgnoreCase("sys_refcursor")) {// cannot //$NON-NLS-1$
																			// be
																			// set
																			// apparently
							header = header + myUniq + "_" + count + " " + bindType + ":=null; "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						} else {
							header = header + myUniq + "_" + count + " " + bindType + ":=:" + myUniq + "Init" + count //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
									+ "; "; //$NON-NLS-1$
							lookUpMangle.put(myUniq + "Init" + count, bString); //$NON-NLS-1$
						}
						footer = footer + ":" + b.getName() + ":=" + myUniq + "_" + count + "; "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					}
					sql = replaceBind(b, " " + myUniq + "_" + lookUpCount.get(bindUpper), sql); //$NON-NLS-1$ //$NON-NLS-2$
																								// //need
																								// to
																								// ensure
																								// it
																								// starts
																								// with
																								// whitespace
					// only need to mangle the init made up input bind.
					// lookUpMangle.put(myUniq+"_" + bindUpper, bString);
				} else {
					// single reference -> ok
				}
				count++;
			}
		}
		String ret = header + " BEGIN " + sql + " " + footer + " END;";
		doubleBindPrepareErrorOffset = ret.indexOf(sql);
		return ret; // $NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	private Map<Integer, Integer> doubleBindAdjustmentsOffsets = new HashMap<Integer, Integer>();

	private int calcDoubleBindAdjustmentOffset(int pos) {
		int ret = pos;
		for (int p : doubleBindAdjustmentsOffsets.keySet())
			if (p < pos)
				ret -= doubleBindAdjustmentsOffsets.get(p);
		return ret;
	}

	private String replaceBind(Bind b, String replacement, String sql) {
		doubleBindAdjustmentsOffsets.put(b.getBegin(), replacement.length() - (b.getEnd() - b.getBegin() + 1));
		// too cases at
		// at the end:
		if (sql.length() == (b.getEnd())) {
			return (sql.substring(0, b.getBegin()) + replacement);
		} else {// bind is in the middle
			return (sql.substring(0, b.getBegin()) + replacement + sql.substring(b.getEnd()));
		}
	}

	private void runNonCreatePLSQL() throws SQLException {
		DBUtil dbUtil = DBUtil.getInstance(conn);
		;
		if ((conn instanceof OracleConnection) || (ScriptUtils.isHttpCon(conn, this.getScriptRunnerContext()))) {
			checkForDoubleBind(cmd.getSql());
			String alteredSql = null;
			if (doubleBinds != null) {
				// might need to lock? no as prepareCall is not explicitly
				// locked
				altSQL = doubleBindPrepareOracleSql(cmd.getSql());
				if (altSQL == null) {// bind types missing go down non double
										// bind path for reporting
					doubleBinds = null;
				} else {
					alteredSql = altSQL;
				}
			}
			if (doubleBinds == null) {
				alteredSql = dbUtil.prepareOracleSql(cmd.getSql());
			}
			callStmt = conn.prepareCall(alteredSql);
		} else {
			callStmt = conn.prepareCall(dbUtil.prepareNonOracleSql(cmd.getSql()));
		}
		callStmt.setEscapeProcessing(false);
		List<String> binds = null;
		if ( cmd.getSql().indexOf(":")>0 ) {
		  binds = DBUtil.getBindNames(alteredSQL(doubleBinds, cmd.getSql(), altSQL), true);
		}
		if (binds != null && binds.size() > 0) {
			if (setBinds(callStmt)) {
				registerOutParameters();
				callStmt.getParameterMetaData().getParameterCount();
				try {
					callStmt.execute();
					fetchOutputParameterValues();
				} finally {
					SetServerOutput.coreEndWatcher(conn, getScriptRunnerContext(),cmd);
				}
				reportfinish();
			}
		} else {
			try {
				callStmt.execute();
			} finally {
				SetServerOutput.coreEndWatcher(conn, getScriptRunnerContext(),cmd);
			}
			reportfinish();
		}
		processImplicitResultSets(callStmt);
		warn = callStmt.getWarnings();
	}

	private void processImplicitResultSets(CallableStatement callStmt) {
		try {
			int i = 0;
			try{
				while (callStmt.getMoreResults()) {
					i++;
					report(getScriptRunnerContext(), "ResultSet #" + i + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
					ResultSet rs = null;
					try {
						rs=callStmt.getResultSet();
						SQL sqlProcessing = new SQL(cmd, out);
						sqlProcessing.setScriptRunnerContext(getScriptRunnerContext());
						sqlProcessing.setConn(getConn());
						sqlProcessing.processResultSet(rs);
					}finally {
						if (rs != null) {
							try {
								rs.close();
							} catch (Exception ex) {
							}
						}
					}
				}
			} catch (Exception ee) {
				if (ee instanceof SQLException) {
					throw (SQLException) ee;
				}
				if (ee instanceof IOException) {
					throw (IOException) ee;
				}
				//might be not implemented yet exception from rest
				Logger.getLogger(getClass().getName()).log(Level.WARNING, ee.getStackTrace()[0].toString(), ee);
			}
			
		} catch (SQLException e) {
			cmd.setFail();
			Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		} catch (IOException e) {
			Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}
	}

	private void runCreatePLSQL() throws SQLException {
		PreparedStatement pstmt = null;
		try {
			boolean useExecuteImmediate = false;
			StmtSubType stmtId = cmd.getStmtId();
			if (((stmtId == SQLCommand.StmtSubType.G_S_CREATE_FUNCTION)
					|| (stmtId == SQLCommand.StmtSubType.G_S_CREATE_PROCEDURE)
					|| (stmtId == SQLCommand.StmtSubType.G_S_CREATE_PACKAGE_BODY)
					|| (stmtId == SQLCommand.StmtSubType.G_S_CREATE_PACKAGE_HEADER)
					|| (stmtId == SQLCommand.StmtSubType.G_S_CREATE_JAVA)
					|| (stmtId == SQLCommand.StmtSubType.G_S_CREATE_TRIGGER)
					|| (stmtId == SQLCommand.StmtSubType.G_S_CREATE_TYPE)/**
																			 * surely
																			 * TYPE_BODY
																			 * AS
																			 * WELL?
																			 **/
			)) {
				// set skip through more intensive parsing
				String localCmd = cmd.getSQLOrig();
				if (localCmd.length() > 1001) {
					localCmd = localCmd.substring(0, 999);
				}
				localCmd = localCmd.toLowerCase();
				if (localCmd.indexOf("wrapped") != -1) { //$NON-NLS-1$
					localCmd = localCmd.replaceAll("\\s+or\\s+replace\\s+", " "); //$NON-NLS-1$ //$NON-NLS-2$
					localCmd = localCmd.replaceAll("\\s+noneditionable\\s+", " "); //$NON-NLS-1$ //$NON-NLS-2$
					localCmd = localCmd.replaceAll("\\s+editionable\\s+", " "); //$NON-NLS-1$ //$NON-NLS-2$
					String[] words = localCmd.trim().split("\\s+"); //$NON-NLS-1$
					if ((words != null) && (words.length > 5)) {
						if ((words[3].equals("wrapped") //$NON-NLS-1$
								|| (words[4].equals("wrapped") && (words[2].equals("body"))))) { //$NON-NLS-1$ //$NON-NLS-2$
							useExecuteImmediate = true;
						}
					}
					if (useExecuteImmediate == false) {
						// try with comments stripped:
						localCmd = ScriptUtils.stripFirstN(localCmd, 1000,
								(cmd.getProperty(SQLCommand.STRIPPED_CONTINUATION) == null), false);
						localCmd = localCmd.replaceAll("\\s+or\\s+replace\\s+", " "); //$NON-NLS-1$ //$NON-NLS-2$
						localCmd = localCmd.replaceAll("\\s+noneditionable\\s+", " "); //$NON-NLS-1$ //$NON-NLS-2$
						localCmd = localCmd.replaceAll("\\s+editionable\\s+", " "); //$NON-NLS-1$ //$NON-NLS-2$
						words = localCmd.trim().split("\\s+"); //$NON-NLS-1$
						if ((words != null) && (words.length > 5)) {
							if ((words[3].equals("wrapped") //$NON-NLS-1$
									|| (words[4].equals("wrapped") && (words[2].equals("body"))))) { //$NON-NLS-1$ //$NON-NLS-2$
								useExecuteImmediate = true;
							}
						}
					}
				}
			}

			if (useExecuteImmediate &&
			// This check is added for lrgsrg regressions. The Database in this
			// case is created and all the necessary packages(catalog)
			// are loaded including DBMS_SQL package so we cannot use this code
			// for Oracle Database Versions > 11g.
			// For Oracle Database Versions < 11g, where Database is created and
			// loaded with catalog packages, this will FAIL.
					VersionTracker.getDbVersion(DefaultConnectionIdentifier.createIdentifier(conn))
							.compareTo(new Version("11.1")) < 0) {
				// if I am create or replace call as bind
				// execute immediatte fails for string over 32k (like a big
				// wrapped string) for <11g
				oracle.sql.CLOB oraclob = null;
				try {
					oraclob = CLOB.createTemporary(conn, true, CLOB.DURATION_SESSION);
					oraclob.open(CLOB.MODE_READWRITE);
					Writer targetWriter = oraclob.setCharacterStream(1L);
					targetWriter.write(cmd.getSql() + "\n\n", 0, (cmd.getSql() + "\n\n").length()); //$NON-NLS-1$ //$NON-NLS-2$
					targetWriter.flush();
					targetWriter.close();
					pstmt = conn.prepareStatement("  declare\n" + //$NON-NLS-1$
							"    v_large_sql  CLOB;\n" + //$NON-NLS-1$
							"    v_num        NUMBER := 0;\n" + //$NON-NLS-1$
							"    v_upperbound NUMBER;\n" + //$NON-NLS-1$
							"    v_sql        DBMS_SQL.VARCHAR2S;\n" + //$NON-NLS-1$
							"    v_cur        INTEGER;\n" + //$NON-NLS-1$
							"    v_ret        NUMBER;\n" + //$NON-NLS-1$
							"  begin\n" + //$NON-NLS-1$
							"    v_large_sql := :1;\n" + //$NON-NLS-1$
							"    v_upperbound := CEIL(DBMS_LOB.GETLENGTH(v_large_sql)/256);\n" + //$NON-NLS-1$
							"    FOR i IN 1..v_upperbound\n" + //$NON-NLS-1$
							"    LOOP\n" + //$NON-NLS-1$
							"      v_sql(i) := DBMS_LOB.SUBSTR(v_large_sql\n" + //$NON-NLS-1$
							"                                 ,256 -- amount\n" + //$NON-NLS-1$
							"                                 ,((i-1)*256)+1 -- offset\n" + //$NON-NLS-1$
							"                                 );\n" + //$NON-NLS-1$
							"    END LOOP;\n" + //$NON-NLS-1$
							"    v_cur := DBMS_SQL.OPEN_CURSOR;\n" + //$NON-NLS-1$
							"    DBMS_SQL.PARSE(v_cur, v_sql, 1, v_upperbound, FALSE, DBMS_SQL.NATIVE);\n" + //$NON-NLS-1$
							"    v_ret := DBMS_SQL.EXECUTE(v_cur);\n" + //$NON-NLS-1$
							"    DBMS_SQL.CLOSE_CURSOR(v_cur);\n" + //$NON-NLS-1$
							"  EXCEPTION   \n" + //$NON-NLS-1$
							"    when others then\n" + //$NON-NLS-1$
							"      dbms_sql.close_cursor(v_cur);\n" + //$NON-NLS-1$
							"    raise;\n" + //$NON-NLS-1$
							"  END;\n" //$NON-NLS-1$
					);
					pstmt.setClob(1, oraclob);
					try {
						pstmt.executeUpdate();
					} finally {
						// empty dbms_output and print if necessary (might be
						// needed on other commands, this is for insert)
						SetServerOutput.coreEndWatcher(conn, this.getScriptRunnerContext(),cmd);
					}
					warn = pstmt.getWarnings();
					errorToWorksheet = true;
				} catch (IOException e) {
					Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
				} finally {
					try {
						oraclob.freeTemporary();
					} catch (Exception e) {
						Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
					}
				}
			} else {
				cstmt = conn.createStatement();
				cstmt.setEscapeProcessing(false);
				// if I am create or replace
				try {
					cstmt.executeUpdate(cmd.getSql());
				} finally {
					// empty dbms_output and print if necessary (might be needed
					// on other commands, this is for insert)
					SetServerOutput.coreEndWatcher(conn, this.getScriptRunnerContext(),cmd);
				}
				warn = cstmt.getWarnings();
			}
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	private void reportWarning(SQLWarning warn) {
		getScriptRunnerContext().putProperty(ScriptRunnerContext.LAST_ERR_TYPE,
				type == null ? "UNKNOWN" : type.toUpperCase()); //$NON-NLS-1$
		getScriptRunnerContext().putProperty(ScriptRunnerContext.LAST_ERR_NAME, name);
		report(getScriptRunnerContext(), warn.getMessage());
	}

	private String type = null;
	private String name = null;
	private String owner = null;

	private String[] parseNameAndType(String sql) {
		String[] retVal = ScriptUtils.parseNameAndTypeUtil(sql);
		if (retVal != null) {
			owner = retVal[0];
			name = retVal[1];
			type = retVal[2];
			return new String[] { retVal[1], retVal[2] };
		}
		return new String[] { "", "" }; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * @return true if there are errors
	 */
	public boolean reportErrors(String objectName, boolean feedbackWanted) {
		fireListenersRegister(getScriptRunnerContext());
	  String ret = DBUtil.getInstance(conn).executeReturnOneCol("select count(1) cnt from user_errors");
    if ( ret.equals("0")){
      return false;
    }
		MetaResource mr = new MetaResource(QueryXMLSupport.class.getClassLoader(),
				"oracle/dbtools/raptor/compiler/source.xml");//$NON-NLS-1$
		QueryXMLSupport s_xml = QueryXMLSupport.getQueryXMLSupport(mr);
		Query q = s_xml.getQuery("COMPILER_ERRORS", conn); //$NON-NLS-1$

		Map<String, String> binds = new HashMap<String, String>();
		binds.put("NAME", name); //$NON-NLS-1$
		binds.put("TYPE", type); //$NON-NLS-1$
		if (owner == null) {
			if (conn instanceof OracleConnection) {
				owner = DBUtil.getInstance(conn)
						.executeReturnOneCol("select UPPER(sys_context('USERENV', 'CURRENT_SCHEMA')) from dual"); //$NON-NLS-1$
			} else {
				try {
					owner = conn.getMetaData().getUserName(); // $NON-NLS-1$
				} catch (SQLException e1) {
				}
			}

		}
		binds.put("OWNER", owner); //$NON-NLS-1$

		ResultSet rset = null;
		OraSubmitResults submitResults = new OraSubmitResults();
		try {
			DBUtil dbUtil = DBUtil.getInstance(conn);
			rset = dbUtil.executeQuery(q.getSql(), binds);

			while (rset.next()) {
				int line = rset.getInt(1);
				int col = rset.getInt(2);
				String errMsg = rset.getString(3);
				String attr = rset.getString(4);
				boolean error = "ERROR".equals(attr); // NOTRANS - //$NON-NLS-1$
														// data dictionary value
				submitResults.addError(new PlSqlErrorInfo(line, col, errMsg, !error));
			}
		} catch (SQLException e) {
			fireListenerOutputLog(
					MessageFormat.format(Messages.getString("PLSQL.19"), new Object[] { e.getMessage() })); //$NON-NLS-1$
		} finally {
			try {
				if (rset != null) {// need to close the associated statement
									// done in dbutil
					DBUtil.closeResultSet(rset);
				}
			} catch (Exception ex) {
				// ignore
			}
			// this is not the associated statement, this is the overall stmt.
			// mistake?
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception ex) {
			}
		}

		List<PlSqlErrorInfo> results = new ArrayList<PlSqlErrorInfo>();

		// check the return to see if there was an error
		boolean warnings = false;
		boolean errors = false;
		List<PlSqlErrorInfo> resultList = submitResults.getErrorList();
		results.addAll(resultList);
		// The resultset will be null if there were no errors returned
		// from the compilation.

		if (null != resultList) {
			if ((this.errorToWorksheet) && (resultList.size() != 0)) {
				report(getScriptRunnerContext(), MessageFormat.format(Messages.getString("CREATED"), type, name)); //$NON-NLS-1$
			}
			// IdeStorage ideStorage = new IdeStorage(plsqlNode);
			for (PlSqlErrorInfo ei : resultList) {
				if (ei.isWarning()) {
					fireListenerWarning(ei.getLineNumber(), ei.getColumn(), 0, ei.getErrorMessage());
				} else {
					fireListenerError(ei.getLineNumber(), ei.getColumn(), 0, ei.getErrorMessage());
				}
			}
			if (resultList.size() == 20) {
				fireListenerWarning(1, 1, 0, Messages.getString("PLSQL.20")); //$NON-NLS-1$
			}
		}
		errorToWorksheet = false;
		// Only reset the dirty flag if the submit succeeded. A successful

		for (PlSqlErrorInfo err : results) {
			if (!err.isWarning()) {
				errors = true;
			} else {
				warnings = true;
			}
		}
		// Force the overlays to refresh
		// DatabaseOverlayClient.updateOverlay( this );

		// trans.setLog( null );
		fireListenerunRegisterAsynchronousLogPage();
		if (errors && warnings)
			fireListenerOutputLog(MessageFormat.format(Messages.getString("PLSQL.21"), new Object[] { objectName })); //$NON-NLS-1$
		else if (errors)
			fireListenerOutputLog(MessageFormat.format(Messages.getString("PLSQL.22"), new Object[] { objectName })); //$NON-NLS-1$
		else if (warnings)
			fireListenerOutputLog(MessageFormat.format(Messages.getString("PLSQL.23"), new Object[] { objectName })); //$NON-NLS-1$
		else
			fireListenerOutputLog(MessageFormat.format(Messages.getString("PLSQL.24"), new Object[] { objectName })); //$NON-NLS-1$
	
		// We decided to always "show the error" when there is an issue.
		// use sqlplus classic mode if you do not want to see the error information
		// note that this is not exactly the same results as running a SHOW ERRORS
		// command, but it is close enough, no need to run another command
		if (!getScriptRunnerContext().isSQLPlusClassic()) {
			int maxLength = 8;// 8 is the minimum size of the line/col column
			for (PlSqlErrorInfo err : results) {
				int lineNumLength = String.valueOf(err.getLineNumber()).length();
				int colNumLength = String.valueOf(err.getColumn()).length();
				if (maxLength < lineNumLength + colNumLength) {
					maxLength = lineNumLength + colNumLength;
				}
			}
			if (results.size() > 0) {
				report(getScriptRunnerContext(), "LINE/COL  ERROR");
				report(getScriptRunnerContext(),
						rpad("", '-', maxLength) + " -------------------------------------------------------------");
			}
			for (PlSqlErrorInfo err : results) {
				report(getScriptRunnerContext(), rpad(err.getLineNumber() + "/" + err.getColumn(), ' ', maxLength) + " "
						+ err.getErrorMessage());
			}
		}

		getScriptRunnerContext().putProperty(ScriptRunnerContext.LAST_ERR_TYPE, type.toUpperCase());
		if ((owner != null) && (!(owner.equals("")))) { //$NON-NLS-1$
			getScriptRunnerContext().putProperty(ScriptRunnerContext.LAST_ERR_NAME, owner + "." + name); //$NON-NLS-1$
		} else {
			getScriptRunnerContext().putProperty(ScriptRunnerContext.LAST_ERR_NAME, name);
		}
		return errors;
	}

	private String rpad(String stringToPad, char filler, int length) {
		if (stringToPad.length() >= length) {
			return stringToPad;
		} else {
			StringBuffer padding = new StringBuffer();
			for (int i = stringToPad.length(); i <= length; i++) {
				padding.append(filler);
			}
			return stringToPad + padding.toString();
		}
	}

	private void fetchOutputParameterValues() throws SQLException {
		int bindIdx = 1;
		Map<String, Bind> m = getScriptRunnerContext().getVarMap();
		List<String> al = DBUtil.getBindNames(alteredSQL(doubleBinds, cmd.getSql(), altSQL), true);
		bindIdx = al.size();

		if (m.size() > 0) {
			for (int i = al.size() - 1; i >= 0; i--) {
				if (bindIdx > 0) {
					String bindName = null;
					if (doubleBinds != null) {
						bindName = this.lookUpMangle.get(al.get(i));
						if (bindName == null) {// not an init bind
							bindName = al.get(i);
						}
						if (al.get(i).contains(this.myUniq + "Init")) { //$NON-NLS-1$
							bindIdx--;
							continue;
						}
						if (bindName == null) {
							bindName = al.get(i);
						}
					} else {
						bindName = al.get(i);
					}
					Bind bind = m.get(bindName.toUpperCase());
					String type = bind.getType();
					//handle DataTypes
					if(bind.getDataType()!=null){
					  if((bind.getMode()==Mode.OUT || bind.getMode()==Mode.INOUT)){
					    bind.setDataValue(bind.getDataType().getDataValue(callStmt.getObject(bindIdx)));
					  }
					} else {
					if (type.toUpperCase().indexOf("CHAR") > -1) //$NON-NLS-1$
						try {
							bind.setValue(callStmt.getString(bindIdx));
						} catch (SQLException e) {
						}
					else if (type.toUpperCase().equals("NUMBER")) {//$NON-NLS-1$
						// Using getObject() instead of getInt() . getInt()
						// returns 0 if the result is null.
						Object o = null;
						try {
							o = callStmt.getObject(bindIdx);
						} catch (NullPointerException e) {
						} catch (SQLException e) {
						}
						bind.setValue(o == null ? null : o.toString());
					} else if (type.toUpperCase().indexOf("CLOB") > -1) //$NON-NLS-1$
						try {
							bind.setValue(
									DataTypesUtil.stringValue(callStmt.getObject(bindIdx), conn, Integer.MAX_VALUE));
						} catch (SQLException e) {
						}
					else if (type.toUpperCase().equals("REFCURSOR")) {//$NON-NLS-1$
						try {
							ScriptRunnerContext.set_storedContext(getScriptRunnerContext());
							bind.setValue(DataTypesUtil.stringValue(callStmt.getObject(bindIdx), conn));
							ScriptRunnerContext.set_storedContext(null);
						} catch (SQLException e) {
						}
					} else if (type.toUpperCase().indexOf("BINARY_") > -1) { //$NON-NLS-1$
						try {
							bind.setValue(DataTypesUtil.stringValue(callStmt.getObject(bindIdx), conn)); // $NON-NLS-1$
						} catch (SQLException e) {
						}
					} else if (type.toUpperCase().equals("BLOB")) { //$NON-NLS-1$
						Blob b = null;
						try {
							b = (Blob) callStmt.getObject(bindIdx);
						} catch (SQLException e) {
						}
						String s = null;
						try{
							if (b != null) {
								byte[] bdata = null;
								bdata = b.getBytes(1, (int) b.length());
								s = new String(ScriptUtils.bytesToHex(bdata)).toUpperCase();
							}
						} catch (SQLException e) {
						//note blob could be freed already which is valid to ignore here check sqlcode?
						//Do not log for now	Logger.getLogger(PLSQL.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
						}
						bind.setValue(s); // $NON-NLS-1$
						bind.setValueObj(b);
					}
				}
				m.put(bindName.toUpperCase(), bind); 
				bindIdx--;
			}
			}
		} else {
			List<Object> binds = cmd.getBinds();
		}
	}

	private void registerOutParameters() throws SQLException {
		int bindIdx = 1;
		List<String> al = null;
		al = DBUtil.getBindNames(alteredSQL(doubleBinds, cmd.getSql(), altSQL), true);
		Map<String, Bind> m = getScriptRunnerContext().getVarMap();
		String bindName = null;
		for (int i = 0; i < al.size(); i++) {
			bindName = null;
			if (this.doubleBinds != null) {
				if (al.get(i).contains(myUniq + "Init")) { //$NON-NLS-1$
					// skip it its an additional input parameter used for
					// initialising plsql var.
					bindIdx++;// skip this one its set only
					continue;
				}
			}
			bindName = al.get(i);
			if (m.containsKey(bindName.toUpperCase())) {
				Bind bind = m.get(bindName.toUpperCase());
				//only register out binds
			  if (bind.getMode() == Bind.Mode.OUT||bind.getMode() == Bind.Mode.INOUT ||bind.getMode()==Bind.Mode.UNKNOWN) {
  				String type = bind.getType();
  				//handle DataType/DataValue
  				if(bind.getDataType() != null){
  				  callStmt.registerOutParameter(bindIdx, bind.getDataType().getSqlDataType(ValueType.JDBC));
  				} else {
  				if (type.toUpperCase().indexOf("NCHAR") > -1 || type.toUpperCase().indexOf("NVARCHAR") > -1 //$NON-NLS-1$ //$NON-NLS-2$
  						|| type.toUpperCase().indexOf("NCLOB") > -1) //$NON-NLS-1$
  					((OracleCallableStatement) callStmt).setFormOfUse(bindIdx, OraclePreparedStatement.FORM_NCHAR);
  				if (type.toUpperCase().indexOf("CHAR") > -1) //$NON-NLS-1$
  					callStmt.registerOutParameter(bindIdx, java.sql.Types.VARCHAR);
  				else if (type.toUpperCase().equals("NUMBER")) //$NON-NLS-1$
  					//
  					// Bug 13790813: need to use OracleTypes.NUMBER here
  					// using java.sql.Types.INTEGER that was used before
  					// truncated all the decimal places.
					//
					callStmt.registerOutParameter(bindIdx, OracleTypes.NUMBER);
				else if (type.toUpperCase().indexOf("CLOB") > -1) //$NON-NLS-1$
					callStmt.registerOutParameter(bindIdx, java.sql.Types.CLOB);
				else if (type.toUpperCase().equals("REFCURSOR")) //$NON-NLS-1$
					callStmt.registerOutParameter(bindIdx, OracleTypes.CURSOR);
				else if (type.toUpperCase().equals("BFILE")) //$NON-NLS-1$
					callStmt.registerOutParameter(bindIdx, OracleTypes.BFILE);
				else if (type.toUpperCase().equals("BLOB")) //$NON-NLS-1$
					callStmt.registerOutParameter(bindIdx, OracleTypes.BLOB);
				else if (type.toUpperCase().equals("BINARY_DOUBLE")) //$NON-NLS-1$
					callStmt.registerOutParameter(bindIdx, OracleTypes.BINARY_DOUBLE);
				else if (type.toUpperCase().equals("BINARY_FLOAT")) //$NON-NLS-1$
					callStmt.registerOutParameter(bindIdx, OracleTypes.BINARY_FLOAT);
				
				}           
			} 
		  bindIdx++;
			}
		}
	}

	private String capitalize(String line) {
		String[] arr = line.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1).toLowerCase()).append(" ");
		}
		return sb.toString().trim();
	}

	public static void main(String[] args) {
		String orig = "  dbms_output.put_line ('typ_e1 ' || ty.c1 || ty.c2);";
		String mod = "  dbms_output.put_line (TO_CHAR(:2) || ty.c1 || ty.";
		System.out.println(calcAdjValue(orig, mod));
	}

}
