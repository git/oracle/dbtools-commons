/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


import oracle.dbtools.raptor.utils.ExceptionHandler;


public class ResetPasswordHelper {
	//taking ResetPasswordAction into sqlcl with as few alterations/extra bugs as possible.
	//at the cost of some verbose ness and duplication.

    public static final String NEW_PWD = "OCINewPassword"; //$NON-NLS-1$
    public static final String THIN_NEW_PWD = "oracle.jdbc.newPassword"; //$NON-NLS-1$

    private static final Logger LOGGER = Logger.getLogger(ResetPasswordHelper.class.getName());
    
	public ResetPasswordHelper() {
		
	}
//Try 12.2 thin
//try thick - sqldev seems to go to the trouble to check if it to enable ui..

	//setPasswordHelper.resetAndReconnect(getScriptRunnerContext(), username, URLString, props, passArray[2],e);

    public Connection resetAndReconnect(ScriptRunnerContext ctx, String username, String urlOrig, Properties props, String newPassword, Throwable firstError) throws Throwable {
    	//Bug 15868330 - RESET PASSWORD CONNECTION CONTEXT MENU SHOULD NOT BE DISABLED ON ACTIVE CONNECTION
    	//               so launch succeeds for connections that are: 1) active; 2)inactive + OCI available
    	String url=urlOrig;
    	boolean reset_ok = false;

        
        //if url = thin -> thin, thick
        //if url = thick -> thick
        boolean reportFirstError=true;
        boolean alsoTryThick = false;
        if (url.toLowerCase(Locale.US).startsWith("jdbc:oracle:thin")) { //$NON-NLS-1$
        	alsoTryThick = true;
        	Driver d=DriverManager.getDriver(url);
        	//sqlexception if thrown will be thrown outside the function.
        	//only 12.2
        	if (!((d.getMajorVersion()>12)
        			||((d.getMajorVersion()==12)&&(d.getMinorVersion()>=2)))) {
        		//flick to thick also try thick to false
        		url=url.replaceFirst("jdbc:oracle:thin", "jdbc:oracle:oci8");  //$NON-NLS-1$  //$NON-NLS-2$
        		alsoTryThick=false;
        		reportFirstError=false;//oci8 worth a try but do not report oci8 no available error.
        	}
        } 
        SQLException toReport=null;

        String[] tryMultiple=null;
        if (alsoTryThick) {
        	tryMultiple=new String[] {url,url.replaceFirst("jdbc:oracle:thin", "jdbc:oracle:oci8")}; //$NON-NLS-1$  //$NON-NLS-2$
        } else {
        	tryMultiple=new String[] {url};
        }
        Properties localProps = new Properties();
        localProps.putAll(props);
        Connection conn = null;
        
        for (String current:tryMultiple) {
        //       RaptorConnectionCreator creator = new RaptorConnectionCreator();
        		String newPwd = newPassword;
        		localProps.setProperty(NEW_PWD, newPassword);
            	try {
            		if ( current.startsWith("jdbc:oracle:thin")) {  //$NON-NLS-1$
	            		// Reset using thin in 12.2+
	            		localProps.remove(NEW_PWD);
	            		localProps.setProperty(THIN_NEW_PWD, newPwd);
	            	} else {
	            		localProps.remove(THIN_NEW_PWD);
	            		localProps.setProperty(NEW_PWD, newPwd);
	            	}
	            		conn = DriverManager.getConnection(current, localProps);
	                
	                // If the reset worked
	                reset_ok = true;
	                break;
                } catch ( SQLException e) {
                	if ((reportFirstError)&&(toReport==null)) {
                		toReport=e;
                	}
                } finally {
                	if ( conn != null ) { try {conn.close();conn=null; } catch (Exception ex) {} }
                }
        }
        if ( reset_ok ) {
        	// Let's test the new connection
        	Properties testProps = new Properties();
        	testProps.putAll(props);
        	testProps.remove(NEW_PWD);
        	testProps.remove(THIN_NEW_PWD);
        	testProps.setProperty("password", newPassword); //$NON-NLS-1$
        	
        	try {
        		if ( ctx != null ){
        			ctx.putProperty(ScriptRunnerContext.CLI_CONN_URL, urlOrig);
        			/**need to store this as password has changed*/
        			ctx.putProperty(ScriptRunnerContext.CLI_CONN_PROPS, testProps);
        		}
        		conn = DriverManager.getConnection(urlOrig,testProps);
        	} catch (SQLException ex) {
        		toReport=ex;
        		reset_ok = false;
        	}
        }
        if ((conn!=null)&&(!(conn.isClosed()))) {
        	return conn;
        }else {
        	if (toReport!=null) {
        		throw toReport;
        	} else {
        		throw firstError;
        	}
        }
    }
}
