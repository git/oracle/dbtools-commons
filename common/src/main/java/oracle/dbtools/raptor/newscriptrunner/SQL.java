/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLRecoverableException;
import java.sql.SQLWarning;
import java.sql.Savepoint;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.ResultSetFormatter;
import oracle.dbtools.db.SQLPLUSCmdFormatter;
import oracle.dbtools.logging.Timer;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.commands.SetServerOutput;
import oracle.dbtools.raptor.query.Bind;
import oracle.dbtools.raptor.query.Bind.Mode;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.dbtools.raptor.utils.ExceptionHandler;
import oracle.dbtools.raptor.utils.oerr.Oerr;
import oracle.dbtools.raptor.utils.oerr.OerrException;
import oracle.dbtools.util.Debug;
import oracle.dbtools.util.Logger;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleTypes;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.ValueType;

public class SQL extends SQLCommandRunner {
    private String _format;
    private boolean warningReported = false;

    public SQL(ISQLCommand cmd, BufferedOutputStream out) {
        super(cmd, out);
    }

    public void setFormat(String format){
      _format = format;
    }
    
    public void run() {
        ScriptRunnerContext sr_ctx = getScriptRunnerContext();
        if (sr_ctx!=null) {
            sr_ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, null);
        }
        //lock should be held by the script runner. What is the affect of trying to lock/unlock it again for each statement?
        // get the lock
        if (!lockConnection()) return;
        try {
            Timer timer = new Timer();
            timer.start();
            Debug.debug(cmd);
            if (cmd.getProperty(SQLCommand.STRIPPED_CONTINUATION)==null){
                //do not do it twice.
                cmd.setSql(ScriptUtils.checkforContinuationChars(cmd.getSql()));
            }
            if (cmd.getResultsType() == null) {
                execute(true);
            } else if (cmd.getResultsType().equals(SQLCommand.StmtResultType.G_R_DML)) {
              if (isBatchDML()) {
                reportDML(cmd.getStmtId(), processBatchDML());
              } else {
                reportDML(cmd.getStmtId(), processDML());
              }
            } else if (cmd.getResultsType().equals(SQLCommand.StmtResultType.G_R_QUERY) || cmd.getResultsType().equals(SQLCommand.StmtResultType.G_R_SCRIPTEDQUERY)) { 
                if(cmd.getRunnable()){//only used by ScriptRunner when using the DataGrids
                    executeQuery();
                 }
            } else if (cmd.getStmtId().equals(SQLCommand.StmtSubType.G_S_COMMIT)) {
                String[] splitCommit = cmd.getSql().trim().split("[ \t\n\r\f]+"); //$NON-NLS-1$
                int withWork=0;
                if ((splitCommit.length >1)&&splitCommit[1].equalsIgnoreCase("work")) { //$NON-NLS-1$
                    withWork=1;
                }
                boolean bad = false;
                if ((splitCommit.length>(1+withWork))&&(splitCommit[1+withWork].equalsIgnoreCase("force"))) { //$NON-NLS-1$
                    String toExe = cmd.getSql().trim();
                    //commit [work] force string [,integer] , commit [work] force CORRUPT_XID 'STRING', commit [work] CORRUPT_XID_ALL 
                    if ((splitCommit.length>(2+withWork))&&(splitCommit[2+withWork].equalsIgnoreCase("CORRUPT_XID_ALL"))) { //$NON-NLS-1$
                        toExe = toExe.substring(0,toExe.toUpperCase().indexOf("CORRUPT_XID_ALL"))+"CORRUPT_XID_ALL"; //$NON-NLS-1$  //$NON-NLS-2$
                    } else if (((splitCommit.length>(2+withWork))&&(splitCommit[2+withWork].equalsIgnoreCase("CORRUPT_XID")))  //$NON-NLS-1$
                            ||((splitCommit.length>(2+withWork))&&(splitCommit[2+withWork].startsWith("'")))) {  //$NON-NLS-1$
                        //up to first string
                        int counter = 0;
                        counter = cmd.getSql().indexOf("'");  //$NON-NLS-1$
                        if ((counter == -1)||(counter == cmd.getSql().length() -1)) {
                            bad = true;
                        } else {
                            while (true) {
                                counter = cmd.getSql().indexOf("'",counter+1);  //$NON-NLS-1$
                                if (counter == -1) {
                                    bad = true;
                                    break;
                                }
                                if (counter == cmd.getSql().length() -1) {
                                    break;
                                }
                                if ((cmd.getSql().charAt(counter+1)=='\'')){
                                    counter++;
                                } else {
                                    break;
                                }
                            }
                        }
                        if (!bad) {
                            toExe = cmd.getSql().substring(0,counter+1);
                        }
                        if ((splitCommit.length>(2+withWork))&&(splitCommit[2+withWork].startsWith("'"))) { //$NON-NLS-1$
                            //string as above + optional , integer
                            //+, int
                            String numbers=null;
                            if (!bad) {
                                // toExe = cmd.getSql().substring(0,counter+1);
                                counter=counter+1;
                                if ((counter < cmd.getSql().length())&&(cmd.getSql().substring(counter).trim().startsWith(","))) {  //$NON-NLS-1$
                                    if (cmd.getSql().substring(counter).trim().length()>1) {
                                        numbers=cmd.getSql().substring(counter).trim().substring(1).trim().split("[^0-9]")[0];  //$NON-NLS-1$
                                        if (numbers.equals("")) { //$NON-NLS-1$
                                            bad = true;
                                        }
                                    } 
                                }
                                if (!bad) {
                                    toExe = cmd.getSql().substring(0,counter);
                                    if (numbers!=null) {
                                        toExe = toExe + "," + numbers;  //$NON-NLS-1$
                                    }
                                }
                            }
                        }
                    } else {
                        bad = true;
                    }
                    if (!bad) {
                        Statement stmt = null; 
                        try {
                            stmt=conn.createStatement();
                            stmt.execute(toExe);
                        } finally {
                            if (stmt!=null) {
                                try {
                                    stmt.close();
                                } catch (Exception e) {
                                }
                            }
                        }
                    }
                } else {
                    conn.commit();
                }
                if (bad) {
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNKNOWN_COMMAND_ARG,cmd.getSql()));
                } else {
                    if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF | 
                            getScriptRunnerContext().getProperty(ScriptRunnerContext.SILENT)==null) {
                            report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COMMITCOMPLETE)); //$NON-NLS-1$
                    }
                }
            } else if (cmd.getStmtId().equals(SQLCommand.StmtSubType.G_S_ROLLBACK_SQL)) {
                String[] tokens = getStringToTokens(cmd.getSql()); 
                Savepoint localSavepoint=null;
                int withWork=0;
                boolean unknownCommand = false;
                String[] splitRollback = cmd.getSql().trim().split("[ \t\n\r\f]+"); //$NON-NLS-1$
                if ((tokens.length >1)&&(tokens[1].equalsIgnoreCase("work"))) { //$NON-NLS-1$
                    withWork=1;
                }
                if (tokens.length > (3+withWork) && tokens[1+withWork].toLowerCase().equals("to") && tokens[2+withWork].toLowerCase().equals("savepoint")) {  //$NON-NLS-1$  //$NON-NLS-2$
                    localSavepoint=sr_ctx.getSavepoint(tokens[3+withWork]);
                    if (localSavepoint==null) {
                        throw new SQLException(ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNKNOWN_SAVEPOINT,tokens[3+withWork]));
                    }
                    conn.rollback(localSavepoint);
                } else if (tokens.length > (2+withWork) && tokens[1+withWork].toLowerCase().equals("to")) {  //$NON-NLS-1$
                    localSavepoint=sr_ctx.getSavepoint(tokens[2+withWork]);
                    if (localSavepoint==null) {
                        throw new SQLException(ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNKNOWN_SAVEPOINT,tokens[2+withWork]));
                    }
                    conn.rollback(localSavepoint);
                //} else if ((splitRollback.length==1)||((splitRollback.length==2)&&(splitRollback[1].equalsIgnoreCase("work")))){ //$NON-NLS-1$
                //    conn.rollback();
                } else if ((splitRollback.length>(2+withWork))&&(splitRollback[1+withWork].equalsIgnoreCase("force"))) {//bug was to allow  rollback force '3.14.368850';  //$NON-NLS-1$
                    //get the string which is 'blah' where blah can contain '' and spaces want to keep the '' as that is required in the statement.
                    int counter = 0;
                    boolean bad = false;
                    counter = cmd.getSql().indexOf("'"); //$NON-NLS-1$
                    if ((counter == -1)||(counter == cmd.getSql().length() -1)) {
                        bad = true;
                    } else {
                        while (true) {
                            counter = cmd.getSql().indexOf("'",counter+1); //$NON-NLS-1$
                            if (counter == -1) {
                                bad = true;
                                break;
                            }
                            if (counter == cmd.getSql().length() -1) {
                                break;
                            }
                            if ((cmd.getSql().charAt(counter+1)=='\'')){
                                counter++;
                            } else {
                                break;
                            }
                        }
                    }
                    if (!bad) {
                        Statement stmt = null; 
                        try {
                            stmt=conn.createStatement();
                            stmt.execute(cmd.getSql().substring(0,counter+1));
                        } finally {
                            if (stmt!=null) {
                                try {
                                    stmt.close();
                                } catch (Exception e) {
                                }
                            }
                        }
                    } else {
                        unknownCommand = true;
                    }
                } else {
                    conn.rollback();
                }
                if (unknownCommand) {
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNKNOWN_COMMAND_ARG,cmd.getSql()));
                } else {
                    if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF |
                            getScriptRunnerContext().getProperty(ScriptRunnerContext.SILENT)==null) {
                        report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ROLLED_BACK)); //$NON-NLS-1$
                    }
                }
            } else if (cmd.getStmtId().equals(SQLCommand.StmtSubType.G_S_SAVEPOINT)) {
                String[] tokens = getStringToTokens(cmd.getSql()); 
                if (tokens.length>1) {
                Savepoint sp = conn.setSavepoint(tokens[1]);
                sr_ctx.addSavepoint(sp);
                if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF |
                        getScriptRunnerContext().getProperty(ScriptRunnerContext.SILENT)==null) {
                    report(getScriptRunnerContext(), processMessage(cmd.getSql())); 
                  }
                } else {
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ORA03001));
                }
            } else if (cmd.getResultsType().equals(SQLCommand.StmtResultType.G_R_THREE_WORD)) {
                execute(false);
                if ((getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF | getScriptRunnerContext().getProperty(ScriptRunnerContext.SILENT)==null) && !warningReported ) {
                        report(getScriptRunnerContext(), processMessage(cmd.getSql()));
                }
            } else {
                // should never hit this
                // Debug.error("else in SQL was reached with " + cmd);
                if ((this.conn != null) && 
                    (this.conn instanceof OracleConnection) &&
                    (cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_UNKNOWN))) {
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNKNOWN_COMMAND_ARG,cmd.getSql()));
                } else {
                    execute(true);//execute G_S_UNKNOWN commands when they are not Oracle connections as our parser does nor recognize all thirdparty syntax.
                }
            }
            timer.end();
            cmd.setTiming(timer.duration());
        } catch (SQLException se) {
            cmd.setFail();
           handleSQLException(se,sr_ctx);
        } catch (IOException ioe) {
            if (sr_ctx != null) sr_ctx.putProperty(ScriptRunnerContext.ERR_ENCOUNTERED, Boolean.TRUE);
            if (sr_ctx != null) sr_ctx.putProperty(ScriptRunnerContext.ERR_FOR_AUTOCOMMIT, Boolean.TRUE);
            report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine()), cmd.getSQLOrig(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.IO_ERROR, ioe.getMessage()), this.getScriptRunnerContext()));
            // TODO check if this is an interrupt before printing
            // ioe.printStackTrace();
            if (sr_ctx != null) sr_ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE,
                                                   ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine()), cmd.getSQLOrig(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.IO_ERROR, ioe.getMessage()), this.getScriptRunnerContext()));
                        doWhenever(false);
        } finally {
            // release the lock
            unlockConnection();
        }
    }
    
    private void handleSQLException(SQLException se,ScriptRunnerContext sr_ctx) {
        ExceptionHandler.handleException(se, connName, true, false);
        if (sr_ctx != null) {
            sr_ctx.putProperty(ScriptRunnerContext.ERR_ENCOUNTERED, Boolean.TRUE);
            sr_ctx.putProperty(ScriptRunnerContext.ERR_FOR_AUTOCOMMIT, Boolean.TRUE);
            sr_ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE,se.getMessage());
            sr_ctx.putProperty(ScriptRunnerContext.ERR_SQLCODE, se.getErrorCode());
        }
        //Adding errorlog for sql here. This will che
        sr_ctx.errorLog(sr_ctx.getSourceRef(),se.getMessage(),cmd.getSql());
        boolean defaultErrorHandling = true;
        StmtSubType stmtId = cmd.getStmtId();
        int startLine = cmd.getStartLine();
        // Fetch error offsets for DML statements, but not DDL statements
        if ((SQLCommand.StmtResultType.G_R_QUERY.equals(cmd.getResultsType()) || stmtId.equals(SQLCommand.StmtSubType.G_S_INSERT) || 
                stmtId.equals(SQLCommand.StmtSubType.G_S_INSERT) || stmtId.equals(SQLCommand.StmtSubType.G_S_UPDATE) || stmtId.equals(SQLCommand.StmtSubType.G_S_DELETE) || stmtId.equals(SQLCommand.StmtSubType.G_S_SELECT)
                || stmtId.equals(SQLCommand.StmtSubType.G_S_MERGE) || stmtId.equals(SQLCommand.StmtSubType.G_S_WITH))
                && (!(cmd.getSQLOrig().toUpperCase().startsWith("DESC")))) { //$NON-NLS-1$
            int offset =0;
            if (! (se instanceof SQLRecoverableException)) { 
             //if (ScriptUtils.isHttpCon(conn, this.getScriptRunnerContext())){
             //    offset = 0;
             //} else {
                 offset = DBUtil.getInstance(conn).getErrorOffset(cmd.getSql());
             //}
            }
            int col = 0;
            int line = 0;
            String[] lines = null;
            if (offset >= 0) {
                String sql = cmd.getSql();
                if(sql.length() >= offset){//I dont think this offset is consistently returned, not sure if the DB really takes new lines into account
                    String sqlSubSet = ""; //$NON-NLS-1$
                    if (sql.length() >= offset + 1) {
                        sqlSubSet = sql.substring(0,offset+1);
                        //offset starts with zero - we want first error (byte? might break in multi byte) not last good (byte)
                        //Last good byte was complicated by: '--' comments being stripped and 
                        //Current complication with:
                        //    continuation character '-' to continue first line , so first line = first 2 lines.
                        //    error on incomplete statement (defaults to last good char as before)
                    } else {
                        sqlSubSet = sql.substring(0,offset);
                    }
                    lines = sqlSubSet.split("\n"); //$NON-NLS-1$
                } else {
                    lines = new String[] {sql};
                }
                col = lines[ lines.length - 1 ].length();
                line = lines.length;
            }
            if (offset >= 0) {
                String toSend = se.getMessage();
                int ls = (int) sr_ctx.getProperty(ScriptRunnerContext.SETLINESIZE);
                StringBuffer sb = new StringBuffer(toSend);
                toSend = ScriptUtils.wordwrap(1, ls , sb, 1).toString();

                if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null && Boolean.parseBoolean(getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
                    StringBuilder error= new StringBuilder();
                    if (line>0) {
                        error.append(cmd.getSql().split("\n")[line-1] + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                        if (col>1) {
                            String pad = new String(new char[col]).replace('\0', ' ');
                            error.append(pad.substring(0, pad.length()-1)+"*\n"); //$NON-NLS-1$
                        }
                        error.append(MessageFormat.format(Messages.getString("SQL.5"), line)); //$NON-NLS-1$
                    } else {
                        error.append(cmd.getSql() + Messages.getString("SQL.6")); //$NON-NLS-1$
                    }
                    error.append(toSend+"\n"); //$NON-NLS-1$
                    report(getScriptRunnerContext(), error.toString());
                    if (sr_ctx != null) {
                        sr_ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE, error);
                    }

                } else {
                    Oerr oerr = new Oerr();
                    try {
                        toSend += (oerr.oerr(toSend));
                    } catch (OerrException e) {
                        // ignore
                    }
                    Object[] oarray = { new Integer(startLine+1), cmd.getSql()/*
                     * may have substitutions made
                     */, new Integer(startLine + 1  + (line -1)), new Integer(col), ScriptRunnerDbArb.format(ScriptRunnerDbArb.SQL_ERROR, toSend) };
                    String error = ScriptRunnerContext.lineColErr(oarray[0], oarray[1], oarray[2], oarray[3], oarray[4], sr_ctx); 
                    report(getScriptRunnerContext(), error);
                    if (sr_ctx != null) {
                        sr_ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE, error);
                    }
                } 
            } else { // offset can be negative number
                String toSend = se.getMessage();
                int ls = (int) sr_ctx.getProperty(ScriptRunnerContext.SETLINESIZE);
                StringBuffer sb = new StringBuffer(toSend);
                toSend = ScriptUtils.wordwrap(1, ls , sb,1).toString();
                if (!(sr_ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null &&Boolean.parseBoolean(sr_ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString()))) {
                    report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(startLine+1), cmd.getSQLOrig(), toSend, this.getScriptRunnerContext()));
                    if (sr_ctx != null) 
                        sr_ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE, ScriptRunnerContext.lineErr(new Integer(startLine+1), cmd.getSQLOrig(), toSend, this.getScriptRunnerContext()));
                } else { 
                    report(getScriptRunnerContext(), MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.LINE_COMMAND_ERR_CLASSIC),
                            new Object[] {ScriptUtils.getErrorStringAtLine(cmd,se), ScriptUtils.getErrorAsterixFromException(cmd,se),ScriptUtils.getErrorLineFromException(cmd, se),toSend})); 
                    if (sr_ctx != null) 
                        sr_ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE, ScriptRunnerContext.lineErr(new Integer(startLine+1), cmd.getSQLOrig(), toSend, this.getScriptRunnerContext()));
                } 
            }
            defaultErrorHandling = false;

        }
        if (defaultErrorHandling == true) {
            StringBuffer toSend = new StringBuffer(se.getMessage());
            toSend = ScriptUtils.wordwrap(1, (int)sr_ctx.getProperty(ScriptRunnerContext.SETLINESIZE), toSend, 1);
            if (conn instanceof OracleConnection) {
                Oerr oerr = new Oerr();
                try {
                    if (!(sr_ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null &&Boolean.parseBoolean(sr_ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString()))) {
                        toSend.append(oerr.oerr(toSend.toString()));
//                        toSend = ScriptUtils.wordwrap(1, (int)sr_ctx.getProperty(ScriptRunnerContext.SETLINESIZE), toSend, 1);
                    }
                } catch (OerrException e) {
                    // ignore
                }
            }
            //BASE0 , example of where we add 1 to the start line number for reporting
            if (!(sr_ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null &&Boolean.parseBoolean(sr_ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString()))) {
                report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(startLine+1), cmd.getSQLOrig(), toSend, this.getScriptRunnerContext()));
                if (sr_ctx != null) sr_ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE, ScriptRunnerContext.lineErr(new Integer(startLine+1), cmd.getSQLOrig(), 
                                                                                                                    toSend, this.getScriptRunnerContext()));
                
            } else { //se=se;  SyntaxError a = SyntaxError.checkSQLQuerySyntax(cmd.getSQLOrig()); a.getSuggestions()
                 report(getScriptRunnerContext(), MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.LINE_COMMAND_ERR_CLASSIC),new Object[] {ScriptUtils.getErrorStringAtLine(cmd,se), ScriptUtils.getErrorAsterixFromException(cmd,se),ScriptUtils.getErrorLineFromException(cmd, se),toSend})); 
            if (sr_ctx != null) sr_ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE, ScriptRunnerContext.lineErr(new Integer(startLine+1), //cmd.getSQLOrig(), 
                                                                                                                cmd.getSQLOrig(), toSend, this.getScriptRunnerContext()));
            } 
        }
        doWhenever(true);    
    }

    private boolean isBatchDML() {
      if(getCmd().getBatchStatements()!= null && getCmd().getBatchStatements().size()>0){
        return true;
      }
      Map<String, List<DataValue>> binds = getScriptRunnerContext().getBatchVarMap();
      if (binds != null && binds.size() > 0) {
        return true;
      }
      return false;
    }   
    
     private int[] processBatchDML() throws SQLException {
      if(cmd.getBatchStatements()!= null && cmd.getBatchStatements().size()>0){
        //batch statements
        return processBatchStatements();
      } else {
        //batch binds
        return processBatchBinds();
      }
    }
    
     private int[] processBatchStatements() throws SQLException {
       int rowsEffected[] = null;
       boolean connAutoCommit = conn.getAutoCommit();
       Statement statement = null;
       try {
         statement = conn.createStatement();
         conn.setAutoCommit(false);
         for(String batchStmt:cmd.getBatchStatements()){
           statement.addBatch(batchStmt);
         }
         try{
           rowsEffected = statement.executeBatch();
         } finally {
           //empty dbms_output and print if necessary (might be needed on other commands, this is for insert)
           SetServerOutput.coreEndWatcher(conn, this.getScriptRunnerContext(),cmd);
         }
       } catch (SQLException se) {
         cmd.setFail();
         if (statement != null) {
           statement.close();
         }
         throw se;
       } finally {
         conn.setAutoCommit(connAutoCommit);
         if (statement != null) {
           statement.close();
         }
       }
       return rowsEffected;
    }

    private int[] processBatchBinds() throws SQLException {
       int rowsEffected[] = null;
       try {
         stmt = conn.prepareStatement(cmd.getSql());
         stmt.setEscapeProcessing(false);
         try {
           setBatchBinds((OraclePreparedStatement) stmt);
           rowsEffected = stmt.executeBatch();
         } finally {
           //empty dbms_output and print if necessary (might be needed on other commands, this is for insert)
           SetServerOutput.coreEndWatcher(conn, this.getScriptRunnerContext(),cmd);
         }
       } catch (SQLException se) {
         cmd.setFail();
         if (stmt != null) {
           stmt.close();
         }
         throw se;
       } finally {
         if (stmt != null) {
           stmt.close();
         }
       }
       return rowsEffected;
    }

    public void setBatchBinds(OraclePreparedStatement pstmt) throws SQLException {
      Map<String, List<DataValue>> batchBinds = getScriptRunnerContext().getBatchVarMap();
      Set<String> bindKeys = batchBinds.keySet();
      int batchSize = batchBinds.get(bindKeys.iterator().next()).size();
      for (int index = 0; index < batchSize; index++) {
        for (String bindKey : bindKeys) {
          List bindValueList = batchBinds.get(bindKey);
          pstmt.setObjectAtName(bindKey, ((DataValue) bindValueList.get(index)).getTypedValue());
        }
        pstmt.addBatch();
      }
    }
    
    
    /**
     * @param dmlType
     * @param num
     * @throws IOException
     */
    public void reportDML(SQLCommand.StmtSubType dmlType, int num) throws IOException {
      int[] nums = { num };
      reportDML(dmlType, nums);
    }

    /**
     * @param dmlType
     * @param num
     * @throws IOException
     */
    public void reportDML(SQLCommand.StmtSubType dmlType, int[] nums) throws IOException {
      int count = 0;
      getScriptRunnerContext().setDMLResult(nums);
      for (int num : nums) {
        count++;
        String action = null;
        boolean classic = getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) != null && Boolean.parseBoolean(getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString());
        if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF) {
          if (dmlType.equals(SQLCommand.StmtSubType.G_S_INSERT)) {
            if (classic) {
              action = MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CREATED), new Object[] { "" }).trim(); //$NON-NLS-1$
            } else {
              action = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.INSERTED);
            }
          } else if (dmlType.equals(SQLCommand.StmtSubType.G_S_UPDATE)) {
            action = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.UPDATED);
          } else if (dmlType.equals(SQLCommand.StmtSubType.G_S_DELETE)) {
            action = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DELETED);
          } else if (dmlType.equals(SQLCommand.StmtSubType.G_S_MERGE)) {
            action = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.MERGED);
          }
          if (num == 1) {
            action = MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ACTION_RESULT1), new Object[] { num, action }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          } else {
            if (classic) {
              action = MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ACTION_RESULT), new Object[] { String.valueOf(num), action }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            } else {
              action = MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ACTION_RESULT), new Object[] { num, action }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            }
          }
          report(getScriptRunnerContext(), "\n" + action + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
        }
      }
    }
    
    /**
     * @return the number of rows affected
     * @throws SQLException
     */
    public int processDML() throws SQLException {
        int rowsEffected = 0;
        try {
            stmt = conn.prepareStatement(cmd.getSql());
            stmt.setEscapeProcessing(false);
            // dbUtil.bind(stmt, cmd.getBinds());
            try {
                if (DBUtil.getBindNames(cmd.getSql(), true) != null && DBUtil.getBindNames(cmd.getSql(), true).size() > 0) {
                    if (setBinds(stmt)){
                    	 registerReturningBinds();
	                     rowsEffected = stmt.executeUpdate();
	                     fetchReturningValues();
                    }
                } else
                    rowsEffected = stmt.executeUpdate();
            } finally {
                //empty dbms_output and print if necessary (might be needed on other commands, this is for insert)
                SetServerOutput.coreEndWatcher(conn,this.getScriptRunnerContext(),cmd);
            }
        } catch (SQLException se) {
            cmd.setFail();
            if (stmt != null) stmt.close();
            throw se;
        } finally {
            if (stmt != null) stmt.close();
        }
        return rowsEffected;
    }
    
  private void registerReturningBinds() throws SQLException {
    int bindIdx = 1;
    List<Bind> al = DBUtil.getBinds(alteredSQL(doubleBinds, cmd.getSql(), altSQL), true);
    Map<String, Bind> m = getScriptRunnerContext().getVarMap();
    String bindName = null;
    for (int i = 0; i < al.size(); i++) {
      bindName = null;
      if (this.doubleBinds != null) {
        if (al.get(i).getName().contains(myUniq + "Init")) { //$NON-NLS-1$
          // skip it its an additional input parameter used for
          // initialising plsql var.
          bindIdx++;// skip this one its set only
          continue;
        }
      }
      Bind bindFromStatement = al.get(i);
      bindName = bindFromStatement.getName();
      if (m.containsKey(bindName.toUpperCase())) {
        if (bindFromStatement.getMode() == Bind.Mode.RETURNING) {
          Bind bind = m.get(bindName.toUpperCase());
          String type = bind.getType();
          if (type == null) {
            DataType dataType = bind.getDataType();
            ((OraclePreparedStatement) stmt).registerReturnParameter(bindIdx, dataType.getSqlDataType(ValueType.JDBC));
          } else {
            if (type.toUpperCase().indexOf("NCHAR") > -1 || type.toUpperCase().indexOf("NVARCHAR") > -1 //$NON-NLS-1$ //$NON-NLS-2$
                || type.toUpperCase().indexOf("NCLOB") > -1) //$NON-NLS-1$
              ((OracleCallableStatement) stmt).setFormOfUse(bindIdx, OraclePreparedStatement.FORM_NCHAR);
            if (type.toUpperCase().indexOf("CHAR") > -1) //$NON-NLS-1$
              ((OraclePreparedStatement) stmt).registerReturnParameter(bindIdx, java.sql.Types.VARCHAR);
            else if (type.toUpperCase().equals("NUMBER")) //$NON-NLS-1$
              //
              // Bug 13790813: need to use OracleTypes.NUMBER here
              // using java.sql.Types.INTEGER that was used before
              // truncated all the decimal places.
              //
              ((OraclePreparedStatement) stmt).registerReturnParameter(bindIdx, OracleTypes.NUMBER);
            else if (type.toUpperCase().indexOf("CLOB") > -1) //$NON-NLS-1$
              ((OraclePreparedStatement) stmt).registerReturnParameter(bindIdx, java.sql.Types.CLOB);
            else if (type.toUpperCase().equals("REFCURSOR")) //$NON-NLS-1$
              ((OraclePreparedStatement) stmt).registerReturnParameter(bindIdx, OracleTypes.CURSOR);
            else if (type.toUpperCase().equals("BLOB")) //$NON-NLS-1$
              ((OraclePreparedStatement) stmt).registerReturnParameter(bindIdx, OracleTypes.BLOB);
            else if (type.toUpperCase().equals("BINARY_DOUBLE")) //$NON-NLS-1$
              ((OraclePreparedStatement) stmt).registerReturnParameter(bindIdx, OracleTypes.BINARY_DOUBLE);
            else if (type.toUpperCase().equals("BINARY_FLOAT"))
              ((OraclePreparedStatement) stmt).registerReturnParameter(bindIdx, OracleTypes.BINARY_FLOAT); // $NON-NLS-1$
          }
        }
        bindIdx++;
      }
    }
  }
    
	private void fetchReturningValues() throws SQLException {
		int bindIdx = 1;
		Map<String, Bind> m = getScriptRunnerContext().getVarMap();
		List<Bind> al = DBUtil.getBinds(alteredSQL(doubleBinds, cmd.getSql(), altSQL), true);
		bindIdx = al.size();
		for (int i = al.size() - 1; i >= 0; i--) {
			if (al.get(i).getMode() != Bind.Mode.RETURNING) {
				bindIdx++;
				continue;
			}
			if (bindIdx > 0) {
				String bindName = null;
				if (doubleBinds != null) {
					bindName = this.lookUpMangle.get(al.get(i).getName());
					if (bindName == null) {// not an init bind
						bindName = al.get(i).getName();
					}
					if (al.get(i).getName().contains(this.myUniq + "Init")) { //$NON-NLS-1$
						bindIdx--;
						continue;
					}
					if (bindName == null) {
						bindName = al.get(i).getName();
					}
				} else {
					bindName = al.get(i).getName();
				}
				Bind bind = m.get(bindName.toUpperCase());
				String type = bind.getType();

				if(bind.getDataType()!=null){
          if((bind.getMode()==Mode.OUT || bind.getMode()==Mode.INOUT)){
            bind.setDataValue(bind.getDataType().getDataValue(getReturnValue(stmt, bindIdx)));
          }
        } else {
				
				if (type.toUpperCase().indexOf("CHAR") > -1) //$NON-NLS-1$
					try {
						bind.setValue((String) getReturnValue(stmt, bindIdx));
					} catch (SQLException e) {
					}
				else if (type.toUpperCase().equals("NUMBER")) {//$NON-NLS-1$
					// Using getObject() instead of getInt() . getInt() returns
					// 0 if the result is null.
					Object o = null;
					try {
						o = getReturnValue(stmt, bindIdx);
					} catch (NullPointerException e) {
					} catch (SQLException e) {
					}
					bind.setValue(o == null ? null : o.toString());
				} else if (type.toUpperCase().indexOf("CLOB") > -1) //$NON-NLS-1$
					try {
						bind.setValue(
								DataTypesUtil.stringValue(getReturnValue(stmt, bindIdx), conn, Integer.MAX_VALUE));
					} catch (SQLException e) {
					}
				else if (type.toUpperCase().equals("REFCURSOR")) {//$NON-NLS-1$
					try {
						bind.setValue(DataTypesUtil.stringValue(getReturnValue(stmt, bindIdx), conn));
					} catch (SQLException e) {
					}
				} else if (type.toUpperCase().indexOf("BINARY_") > -1) { //$NON-NLS-1$
					try {
						bind.setValue(DataTypesUtil.stringValue(getReturnValue(stmt, bindIdx), conn)); // $NON-NLS-1$
					} catch (SQLException e) {
					}
				} else if (type.toUpperCase().equals("BLOB")) { //$NON-NLS-1$
					Blob b = null;
					try {
						b = (Blob) getReturnValue(stmt, bindIdx);
					} catch (SQLException e) {
					}
					String s = null;
					if (b != null) {
						byte[] bdata = null;
						bdata = b.getBytes(1, (int) b.length());
						s = new String(ScriptUtils.bytesToHex(bdata)).toUpperCase();
					}
					bind.setValue(s); // $NON-NLS-1$
				}
        }
				m.put(bindName.toUpperCase(), bind);
				bindIdx--;
			}
		}
	}

    private Object getReturnValue(PreparedStatement stmt, int bindIdx) throws SQLException {
			ResultSet rs = ((OraclePreparedStatement)stmt).getReturnResultSet();
			rs.next();
			return rs.getObject(1); //maybe more columns ?
	}

	public void executeQuery() throws SQLException, IOException {
        ResultSet rset = null;
        boolean run = true; 
        try {
            stmt = conn.prepareStatement(cmd.getSql());
           // stmt.setEscapeProcessing(false);
            // dbUtil.bind(stmt, cmd.getBinds());
            if (DBUtil.getBindNames(cmd.getSql(), true) != null && DBUtil.getBindNames(cmd.getSql(), true).size() > 0) {
                if (!setBinds(stmt)) run = false;
            }
            if (run) {
                rset = stmt.executeQuery();
                processResultSet(rset);
            }
        } finally {
            if (rset != null) {
                try {
                    rset.close();
                } catch (Exception ex) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                }
            }
        }
    }
    
     public void processResultSet(ResultSet rset) throws SQLException, IOException {
        //if original command is not is a command that becomes a query describe or xquery
        //leave out ScriptRunnerContext which skips column new_val and set null features
        String amISelectString = null;
        if (cmd.getSQLOrig().length()>10) {
            amISelectString=cmd.getSQLOrig().substring(0,8).toLowerCase();
        } else {
            amISelectString=cmd.getSQLOrig().toLowerCase();
        }
        boolean originalSelect = true;
        // Bug 21374726 XQuery cannot have a separate context and must use 
        // the same context as SQL.
        if (amISelectString.startsWith("desc")) { //$NON-NLS-1$
            originalSelect = false;
        }
        ResultSetFormatter rFormat = null;
        if (originalSelect) {//use context for column and set null
            if(conn instanceof OracleConnection)
                rFormat = new SQLPLUSCmdFormatter(getScriptRunnerContext());
            else
                rFormat = new ResultSetFormatter(getScriptRunnerContext());
        } else {
            if(conn instanceof OracleConnection) { // Describe command 
               // this is a Classic SQLPlus mode : column format is based on linesize
               if (getScriptRunnerContext()!=null && ( isClassicMode() || getScriptRunnerContext().isJSONOutput()) ) {
                   rFormat = new SQLPLUSCmdFormatter(getScriptRunnerContext());
               } else {
                   // this will call rset2sqlplusShrinkToSize() for SQLCl format
                   rFormat = new SQLPLUSCmdFormatter();
               }
            }
            else 
               rFormat = new ResultSetFormatter();
        }
        rFormat.setProgressUpdater(getScriptRunnerContext().getTaskProgressUpdater());
        int cnt = 0;
        Properties props = getConnectionInfo(); //this is actually the base connection, not the current connection
        // ObjectFactory factory = ObjectFactory.getFactory(resolveConnectionName(), conn);
        // ConnectionTypeDetails ctd = factory.getConnectionTypeDescriptor().getImpl();
        Integer fs = null;
        try {
          //doneill:see bug 8533854. the props are for the base connection, not the current connection
            //as the base connection is always a SQL Developer connection it will always have properties.
            if (getScriptRunnerContext().getProperty(ScriptRunnerContext.ARRAYSIZE)!=null) {
                 fs = Integer.valueOf((String) getScriptRunnerContext().getProperty(ScriptRunnerContext.ARRAYSIZE));
             } else {
                 if(props !=null){
                     String prefFetchSize = props.getProperty(DBUtil.PREFERRED_FETCH_SIZE);         
                     fs = Integer.valueOf(prefFetchSize);
                 } else {
                     fs  =2;
                 }
             }
        } catch (NumberFormatException e) {
            // Ignore, fs is null
        }
        
        
        //if ((fs != null)&&(!ScriptUtils.isHttpCon(conn,getScriptRunnerContext()))) {
        if(fs!=null){
            rset.setFetchSize(fs.intValue());
        }
        
        //}
        
        // This is useful when COMPUTE command is used. SQLPLUSCmdFormatter needs to know
        // the source sql otherwise it is impossible to do Computations.
        if(conn instanceof OracleConnection && rFormat instanceof SQLPLUSCmdFormatter) {
            ((SQLPLUSCmdFormatter)rFormat).setSQL(cmd.getSql());
        }
        // centralized the code that was here over to the formatter class
        cnt = rFormat.formatResults(out, rset, cmd);
        
        int feedback = getScriptRunnerContext().getFeedback();
        //output suppressed now done via WrapListenBufferOutput
        Object silentMode = getScriptRunnerContext().getProperty(ScriptRunnerContext.SILENT);
        boolean feedOn = ( feedback != ScriptRunnerContext.FEEDBACK_OFF | silentMode != null ) && !getScriptRunnerContext().isJSONOutput();
        if (feedOn && feedback!=ScriptRunnerContext.FEEDBACK_OFF) {
            if ( cnt >= feedback ){
                if (cnt==1)
                  report(getScriptRunnerContext(), MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.SELECTONE), new Object[] {cnt}));
                else {
                    if (isClassicMode()) {
                      report(getScriptRunnerContext(), MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.SELECTED), new Object[] {String.valueOf(cnt)}));
                    } else {
                        report(getScriptRunnerContext(), MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.SELECTED), new Object[] {cnt}));
                    }
                }
            } else if ( cnt == 0 ){
              report(getScriptRunnerContext(), MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOROWS_SELECTED), new Object[] {cnt}));
            } else {
                report(getScriptRunnerContext(), ""); //$NON-NLS-1$
            }
        }
    }


    private boolean isClassicMode() {
        return getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null &&  Boolean.parseBoolean(getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString());
        }

        public void execute(boolean status) throws SQLException, IOException {
        String owner=""; //$NON-NLS-1$
        String name=""; //$NON-NLS-1$
        String type=""; //$NON-NLS-1$
        // generic execute
        try {
            if (cmd.getSql().toLowerCase().startsWith("alter")) {//want to set name/type //$NON-NLS-1$
                String[] retVal=ScriptUtils.parseNameAndTypeUtil(cmd.getSql());
                //get last alter..compule name and type for subsequent show errors
                if (retVal!=null) {
                    owner=retVal[0];
                    name=retVal[1];
                    type=retVal[2];
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.LAST_ERR_TYPE, type ==null?"UNKNOWN":type.toUpperCase()); //$NON-NLS-1$
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.LAST_ERR_NAME, name);
                }
            }
            st = conn.createStatement();
            st.setEscapeProcessing(false);
            try {
                st.execute(cmd.getSql().trim());
            } finally {//give dbmsoutput immediately even before error
                SetServerOutput.coreEndWatcher(conn,getScriptRunnerContext(),cmd);
            }
            boolean isOracleConnection = getConn() instanceof OracleConnection;
            if (!isOracleConnection && st.getResultSet() != null) {
            //We ran this as a stmt because it was an unknown command (3rd party)
            //but it return a result set. Lets use it.
                ResultSet rset = st.getResultSet();
                processResultSet(rset);           
                if (rset != null) {
                    try {
                        rset.close();
                    } catch (Exception ex) {
                    }
                }
            } else {
                SetServerOutput.coreEndWatcher(conn,this.getScriptRunnerContext(),cmd);

                SQLWarning warn = st.getWarnings();
                if (warn == null && status == true ) {
                    if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF)
                        report(getScriptRunnerContext(), processMessage(cmd.getSql()));
                } else if (warn != null) {//assumption report warning if any warnings != 17110.
                    if (isOracleConnection) {
                        String warnText = ScriptRunnerDbArb.format(ScriptRunnerDbArb.SQL_COMMAND, processMessage(cmd.getSql(), false));
                        String toSend = warn.getMessage();
                        if (conn instanceof OracleConnection) {
                            Oerr oerr = new Oerr();
                            try {
                                toSend += (oerr.oerr(toSend));
                            } catch (OerrException e) {
                                // ignore
                            }
                        }
                        boolean realWarning=false;
                        boolean reportCompletedWithWarnings=false;
                        while (warn != null) {
                            if (!((isOracleConnection && (warn.getErrorCode()==17110||warn.getErrorCode()==24344)))) {
                                warnText += "\n" + ScriptRunnerDbArb.format(ScriptRunnerDbArb.FAILED, toSend); //$NON-NLS-1$
                                realWarning=true;
                            } else {
                                if (!((isOracleConnection && (warn.getErrorCode()==17110||warn.getErrorCode()==24344)&&(!(cmd.getStmtSubType().equals(StmtSubType.G_S_ALTER)))))) {
                                    //alter java source wants to show warning, Bug 19788777 - JAVA SOURCE IS NOT RESOLVED CORRECTLY
                                    //previous "do not show this warning" bug was create table as select. 18825838
                                    if (isOracleConnection && (warn.getErrorCode()==17110||warn.getErrorCode()==24344)&&((cmd.getStmtSubType().equals(StmtSubType.G_S_ALTER)))) {
                                        if (cmd.getSQLOrig().toLowerCase().trim().endsWith("compile")&&(type!=null&&type.equals("JAVA SOURCE"))) {
                                            warningReported=true;
                                            report(getScriptRunnerContext(), Messages.getString("ALTER_JAVA_COMPILE"));
                                            return;
                                        }
                                    }
                                    boolean classic = getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null && Boolean.parseBoolean(getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString());
                                    if (classic) {
                                        String returnString=""; //$NON-NLS-1$

                                        if (cmd.getSQLOrig().toLowerCase().replaceAll("\\s+","").contains("view")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                            returnString=Messages.getString("SQL.20"); //$NON-NLS-1$
                                            warningReported=true;
                                            report(getScriptRunnerContext(), returnString);
                                            return;

                                        } else if (cmd.getSQLOrig().toLowerCase().replaceAll("\\s+","").contains("pluggable")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                            returnString=Messages.getString("SQL.24"); //$NON-NLS-1$
                                            if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF)
                                            	report(getScriptRunnerContext(), returnString);
                                            warningReported=true;
                                            return;
                                        } else {
                                            warnText += "\n" + toSend; //$NON-NLS-1$
                                            reportCompletedWithWarnings=true;                                            
                                        }
                                    } else {
                                    warnText += "\n" + toSend; //$NON-NLS-1$
                                    reportCompletedWithWarnings=true;
                                    }
                                } else if (isOracleConnection && warn.getErrorCode()==17110 && cmd.getStmtSubType().equals(StmtSubType.G_S_CREATE_VIEW)) {
                                    if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF)
                                    	report(getScriptRunnerContext(), Messages.getString("SQL.25")); //$NON-NLS-1$
                                    return;
                                } else if (isOracleConnection && warn.getErrorCode()==17710 && cmd.getSQLOrig().replaceAll("\\s+|\n", "").toLowerCase().startsWith("alterpluggabledatabase")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                    if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF)
                                    	report(getScriptRunnerContext(), Messages.getString("SQL.29")); //$NON-NLS-1$
                                    return;
                                }
                            }
                            warn = warn.getNextWarning();
                        }
                        if (realWarning) {
                            if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF)
                            	report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine()+1), cmd.getSQLOrig(), warnText, this.getScriptRunnerContext()));
                        } else {
                            if (reportCompletedWithWarnings) {
                                if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF)
                                	report(getScriptRunnerContext(), toSend);
                            } else {
                                if (status) {
                                    if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF)
                                    	report(getScriptRunnerContext(), processMessage(cmd.getSql()));
                                }
                            }
                        }
                    } else {
                        String warnText = warn.getMessage();
                        if (getScriptRunnerContext().getFeedback() != ScriptRunnerContext.FEEDBACK_OFF)
                        	report(getScriptRunnerContext(), warnText);
                    }
                }
            }
        } catch (SQLException se)
        // Here we need to catch and rethrow the SQLException as we need to
        // manage
        // our cursor usage here and let the caller manage the rest of the
        // exception procedure.
        {
            cmd.setFail();
            if (st != null)
                st.close();
            throw se;
        } finally {
            if (st != null)
                st.close();
        }
    }
    
    public void reportObjectAction() {
    }
    

    private String processMessage(String cmd) {
      return processMessage(cmd, true);
    }
    public String processMessage(String cmdText, boolean action) {
    	      // Bug 27154890 Third party database keywords to look. Just a temporary fix. We need a better way 
    	      // to address the messages we display for 3rd party drivers.
    	      // Bug 27447288
    	      String[] thirdPartyKeywords = {"CREATE", "SCHEMA", "AUTHORIZATION", "IF", "NOT", "EXISTS", "DROP", "TABLE", "ALTER"};
          cmdText = ScriptUtils.stripFirstN(cmdText, 1000, (cmd.getProperty(SQLCommand.STRIPPED_CONTINUATION)==null), false);
          cmdText = cmdText.replaceAll("[oO][rR]\\s+[rR][eE][pP][lL][Aa][Cc][Ee]", " "); //$NON-NLS-1$ //$NON-NLS-2$
          cmdText = cmdText.replaceAll("\\s+[nN][oO][nN][eE][dD][iI][tT][iI][oO][nN][aA][bB][lL][eE]\\s", " "); //$NON-NLS-1$ //$NON-NLS-2$
          cmdText = cmdText.replaceAll("\\s+[eE][dD][iI][tT][iI][oO][nN][aA][bB][lL][eE]\\s", " "); //$NON-NLS-1$ //$NON-NLS-2$
          cmdText = cmdText.replaceAll("\\s+[eE][dD][iI][tT][iI][oO][nN][iI][nN][gG]\\s", " "); //$NON-NLS-1$ //$NON-NLS-2$
          String[] tokens = getStringToTokens(cmdText); 
          String result = ""; //$NON-NLS-1$
          if(tokens == null) {
              return cmdText;
          } else if (tokens.length < 3 &&    !tokens[0].equalsIgnoreCase("savepoint") && !tokens[0].equalsIgnoreCase("noaudit")) {
              if (tokens.length==2) {
                //first word verb - second word thing
                  result = tokens[1];
              }  else
               return cmdText;
          } else {
          tokens[0] = capitalize(tokens[0].toLowerCase());
          if (tokens[0].equalsIgnoreCase("explain")) {
              
          } else if (tokens[0].equalsIgnoreCase("comment")) { //$NON-NLS-1$ //$NON-NLS-2$
              result = tokens[0].toLowerCase(); //$NON-NLS-1$
          }else if ((tokens[0].equalsIgnoreCase("restore")||tokens[0].equalsIgnoreCase("recover")) && tokens[1].equalsIgnoreCase("database") ) { //$NON-NLS-1$ //$NON-NLS-2$
              result = tokens[1].toLowerCase(); //$NON-NLS-1$
          } else if (tokens[0].toLowerCase().equals("grant") || tokens[0].toLowerCase().equals("revoke")  || tokens[0].toLowerCase().equals("audit")  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                  || tokens[0].toLowerCase().equals("lock")|| tokens[0].toLowerCase().equals("noaudit")|| tokens[0].toLowerCase().equals("call") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                  || tokens[0].toLowerCase().equals("flashback")) { //$NON-NLS-1$
            result = tokens[0] ;
          } else if (tokens[1].toLowerCase().equals("restore") && tokens[2].toLowerCase().equals("point")) { //$NON-NLS-1$
              result = tokens[1] +" "+tokens[2] +" " + getQuotedName(tokens[3], cmdText); //$NON-NLS-1$ //$NON-NLS-2$
          } else if (tokens[0].toLowerCase().equals("rename")) { //$NON-NLS-1$
                result = ""; //$NON-NLS-1$
          } else if (tokens[0].toLowerCase().equals("savepoint")) { //$NON-NLS-1$
              result = tokens[0].toLowerCase(); //$NON-NLS-1$
          } else if (tokens[1].toLowerCase().equals("on")) { //$NON-NLS-1$
            result = tokens[0] + " " + tokens[1].toLowerCase() + " " + tokens[2].toLowerCase() + " " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                + tokens[3].toLowerCase() + " " + getQuotedName(tokens[5], cmdText); //$NON-NLS-1$
          } else if (tokens[0].toLowerCase().equals("purge")|| tokens[1].toLowerCase().equals("session")) { //$NON-NLS-1$ //$NON-NLS-2$
            result = tokens[1].toLowerCase();
          } else if (tokens[1].toLowerCase().equals("tablespace")|| tokens[2].toLowerCase().equals("tablespace")) { //$NON-NLS-1$ //$NON-NLS-2$
              if (tokens[1].toLowerCase().equals("tablespace")) { //$NON-NLS-1$
                  result = tokens[1] + " " + getQuotedName(tokens[2], cmdText); //$NON-NLS-1$
              } else {
                  result = tokens[2] + " " + getQuotedName(tokens[3], cmdText); //$NON-NLS-1$
              }
          }else {
            if (tokens[1].toLowerCase().equals("materialized") || tokens[1].toLowerCase().equals("bitmap")|| tokens[1].toLowerCase().equals("search") //$NON-NLS-1$ //$NON-NLS-2$
                || tokens[1].toLowerCase().equals("unique") || tokens[1].toLowerCase().equals("public") //$NON-NLS-1$ //$NON-NLS-2$
                || tokens[1].toLowerCase().equals("database") || tokens[1].toLowerCase().equals("force") ||tokens[1].toLowerCase().equals("noaudit") //$NON-NLS-1$ //$NON-NLS-2$
                || tokens[1].toLowerCase().equals("shared") //$NON-NLS-1$
                || tokens[1].toLowerCase().equals("pluggable") || tokens[1].equalsIgnoreCase("rollback")//$NON-NLS-1$
                || tokens[2].toLowerCase().equals("body")||tokens[1].toLowerCase().equals("global")) { //$NON-NLS-1$ //$NON-NLS-2$
              if (tokens.length<4) {//alter database mount or alter database open
                  if ((tokens.length==3)&&(tokens[0].toLowerCase().equals("alter")&& //$NON-NLS-1$
                          tokens[1].toLowerCase().equals("database")&& //$NON-NLS-1$
                          (tokens[2].toLowerCase().equals("mount")||tokens[2].toLowerCase().equals("open")))) { //$NON-NLS-1$ //$NON-NLS-2$
                      return "\n"+capitalize(ScriptRunnerDbArb.format(ScriptRunnerDbArb.ALTERED, "Database"))+"\n"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                  }
                  return cmdText;
              }
              if (tokens[1].toLowerCase().equals("database")&&tokens[2].toLowerCase().equals("link")) {
            	  result = tokens[1].toLowerCase() +" "+ tokens[2].toLowerCase()+" "+ getQuotedName(tokens[3], cmdText);
              }else if (tokens[1].toLowerCase().equals("database")||tokens[2].toLowerCase().equals("flashback")) {
            	  result = tokens[1].toLowerCase();
              }else if (tokens[1].equalsIgnoreCase("rollback")&& tokens[2].equalsIgnoreCase("segment")) {
                  result=tokens[1].toLowerCase() + " " + tokens[2].toLowerCase() + " " + getQuotedName(tokens[3], cmdText);
              } else if (tokens[1].equalsIgnoreCase("database")&&(tokens[2].equalsIgnoreCase("backup")||tokens[2].equalsIgnoreCase("add"))) {
                  result = tokens[1].toLowerCase();
              } else if (tokens[3].toLowerCase().equals("log")||tokens[3].toLowerCase().equals("temporary")) { //$NON-NLS-1$ //$NON-NLS-2$
                result = tokens[1].toLowerCase() + " " + tokens[2].toLowerCase() + " " + tokens[3].toLowerCase() + " " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    + getQuotedName(tokens[5], cmdText);
              } else if (tokens[2].toLowerCase().equals("database") &&tokens[3].toLowerCase().equals("link")) { //$NON-NLS-1$ //$NON-NLS-2$
                  result = tokens[2].toLowerCase() + " " + tokens[3].toLowerCase() + " " //$NON-NLS-1$ //$NON-NLS-2$
                          + getQuotedName(tokens[4], cmdText);  
              } else if( tokens[2].equalsIgnoreCase("index")||tokens[3].equalsIgnoreCase("index")) { //$NON-NLS-1$ //$NON-NLS-2$
                  if (tokens[2].equalsIgnoreCase("index")) { //$NON-NLS-1$
                      result =   result = tokens[2] + " " + getQuotedName(tokens[3], cmdText);   //$NON-NLS-1$
                  } else {
                      result =   result = tokens[3] + " " + getQuotedName(tokens[4], cmdText); //$NON-NLS-1$
                  }
              }else if (tokens[3].toLowerCase().equals("link")) { //$NON-NLS-1$
                  result = tokens[1].toLowerCase() + " " + tokens[2].toLowerCase() + " " + tokens[3].toLowerCase() + " " + getQuotedName(tokens[4], cmdText);   //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
              } else if (tokens[1].toLowerCase().equals("pluggable")&&tokens[2].toLowerCase().equals("database")) { //$NON-NLS-1$ //$NON-NLS-2$
                  result = tokens[1].toLowerCase() + " " + tokens[2].toLowerCase() + " " + getQuotedName(tokens[3], cmdText); //$NON-NLS-1$ //$NON-NLS-2$
              } else if ((tokens[1].toLowerCase().equals("public")&&tokens[2].toLowerCase().equals("synonym"))||(tokens[1].toLowerCase().equals("synonym"))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                  if (tokens[1].toLowerCase().equals("public")) { //$NON-NLS-1$
                      result = tokens[2] + " " + getQuotedName(tokens[3], cmdText);   //$NON-NLS-1$
                  } else {
                      result = tokens[1] + " " + getQuotedName(tokens[2], cmdText); //$NON-NLS-1$
                  }
              } else {
                  if (tokens[1].toLowerCase().equals("force")) { //$NON-NLS-1$
                      result = tokens[2].toLowerCase() + " " + getQuotedName(tokens[3], cmdText); //$NON-NLS-1$
                  } else {
                result = tokens[1].toLowerCase() + " " + tokens[2].toLowerCase() + " " + getQuotedName(tokens[3], cmdText); //$NON-NLS-1$ //$NON-NLS-2$
                  }
              }
				} else {
					if (conn instanceof OracleConnection)
						result = tokens[1].toLowerCase() + " " + getQuotedName(tokens[2], cmdText); //$NON-NLS-1$
					else {
						// Bug 27154890
						List<String> ftk = Arrays.asList(tokens);
						for (String tpk : thirdPartyKeywords) {
							ftk = ftk.stream().filter(id -> !tpk.equalsIgnoreCase(id)).collect(Collectors.toList());
						}
						result = tokens[1].toLowerCase() + " " + getQuotedName(ftk.get(0), cmdText).replaceAll("\\{", "");
					}
            }
          }
          }
          String verb = getVerb(tokens, action);
          result = verb!=null?MessageFormat.format(getVerb(tokens, action), new Object[] {result.trim()}):result.trim();
          result = "\n"+capitalize(result.trim())+"\n"; //$NON-NLS-1$ //$NON-NLS-2$
         return result;
        }    
    private String capitalize(String line)
    {
        boolean classic = getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null && Boolean.parseBoolean(getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString());
        if (classic) 
            return Character.toUpperCase(line.charAt(0)) + line.substring(1).toLowerCase();
        else
            return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }
    private  String getQuotedName(String nameToken, String cmd) {
        ScriptRunnerContext ctx = getScriptRunnerContext();
        boolean classic = ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null && Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString());
        if (classic) return ""; // Dont return names for classic sqlplus emulation //$NON-NLS-1$
        String retVal = getQuotedNameComma(nameToken, cmd);
        if (retVal.endsWith(",")) {  //$NON-NLS-1$
            retVal = retVal.substring(0,retVal.length()-1);
        }
        return retVal;
    }
    private static String getQuotedNameComma(String nameToken, String cmd) {
        if (nameToken.contains("\"")||nameToken.contains("[")||nameToken.contains("`")) {  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
            //'[' is for SQL Server    //` mysql
            boolean braces = nameToken.contains("[");  //$NON-NLS-1$
            String startQuoteBrace=!braces?"\"":"[";  //$NON-NLS-1$ //$NON-NLS-2$
            String endQuoteBrace=!braces?"\"":"]";  //$NON-NLS-1$ //$NON-NLS-2$
            if (nameToken.contains("`")) {  //$NON-NLS-1$
                startQuoteBrace="`";  //$NON-NLS-1$
                endQuoteBrace="`";  //$NON-NLS-1$
            }
            StringTokenizer st = new StringTokenizer(cmd, "[]\"`");  //$NON-NLS-1$
            String[] tokens = new String[ st.countTokens() ];
            int i = 0;
            while (st.hasMoreTokens()) {
                tokens[ i ] = st.nextToken();
                i++;
            }
            //"schema"."objectname"
            if ((i > 3) && (tokens[ 2 ].trim().equals("."))) { //$NON-NLS-1$
                return startQuoteBrace + tokens[ 1 ] + endQuoteBrace +"."+startQuoteBrace + tokens[ 3 ] + endQuoteBrace; //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
            }
            //"schema".objectname
            if ((i > 2) && (tokens[ 2 ].trim().startsWith(".")) && (tokens[ 2 ].trim().length() > 1)) { //$NON-NLS-1$
                return startQuoteBrace + tokens[ 1 ] + endQuoteBrace + "." + tokens[ 2 ].trim().substring(1).trim().split("[ \r\t\n]")[ 0 ].toUpperCase(Locale.US); //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
            }
            // token[1] could be the second string (table name) schema."objectname"
            if ((i > 1) && (tokens[ 0 ].trim().endsWith(".")) && (tokens[ 0 ].trim().indexOf(" ") != -1)) { //$NON-NLS-1$  //$NON-NLS-2$
                String removeDot = tokens[ 0 ].trim();
                removeDot = removeDot.substring(0,removeDot.length()-1).trim();
                String[] alreadySplit = removeDot.split("[ \r\t\n]"); //$NON-NLS-1$
                String lastString=alreadySplit[alreadySplit.length-1];
                return lastString.toUpperCase(Locale.US) + "." + startQuoteBrace + tokens[ 1 ] + endQuoteBrace;  //$NON-NLS-1$  //$NON-NLS-2$
            }
            //special schema.object cases have been handled 
            //default to something that covers the most likely case a single double quoted reference
            return startQuoteBrace + tokens[ 1 ] + endQuoteBrace;  //$NON-NLS-1$  //$NON-NLS-2$
        } else {
            String retVal = nameToken;
            if (retVal!=null) {
                retVal = nameToken.toUpperCase(Locale.US);
            }
            return retVal;
        }
    }
    private String[] getStringToTokens(String cmd) {
        StringTokenizer st = new StringTokenizer(cmd," \t\n\r\f(");  //$NON-NLS-1$
        String[] tokensOversize = new String[st.countTokens()];
        int quoteCount = 0;
        boolean append = false;
        int i = 0;
        while (st.hasMoreTokens()) {
          String next = st.nextToken();
          if (append) {
              tokensOversize[i] += " "+next;  //$NON-NLS-1$
              append = false;
          } else {
              tokensOversize[i] = next;
          }
          int posQuote=0;
          while ((posQuote = next.indexOf("'",posQuote))!=-1) {  //$NON-NLS-1$
              posQuote++;
              quoteCount++;
              if (posQuote==next.length()) {
                  break;
              }
          }
          if (((quoteCount%2)==1)&&(st.hasMoreTokens())) {
              append = true;
          } else {
              i++;
          }
        }
        if (append == true) {
            i++;
        }
        String [] tokens = new String[i];
        for (int count=0; count<i; count++) {
            tokens[count] = tokensOversize[count];
        }
        return tokens;
    }

    /**
     * @param action 
     * @param lowerCase
     * @return
     */
    private String getVerb(String[] string, boolean action) {
      if (action) {
          if (string[0].toLowerCase().equals("comment")||string[0].toLowerCase().equals("savepoint")) { //$NON-NLS-1$ //$NON-NLS-2$
              return ScriptRunnerDbArb.get(ScriptRunnerDbArb.CREATED);
          } else if (string[0].toLowerCase().equals("create")) { //$NON-NLS-1$
        String t2 = string[1].toLowerCase();
        if (t2.equals("procedure") || t2.equals("function") || t2.equals("package") || t2.equals("trigger")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
          return ScriptRunnerDbArb.get(ScriptRunnerDbArb.COMPILED);
        } else {
            return ScriptRunnerDbArb.get(ScriptRunnerDbArb.CREATED);
        }
      } else if (string[0].toLowerCase().equals("drop")) { //$NON-NLS-1$
        return ScriptRunnerDbArb.get(ScriptRunnerDbArb.DROPPED);
      } else if (string[0].toLowerCase().equals("alter")) { //$NON-NLS-1$
          if (string.length>3 && string[3].equalsIgnoreCase("truncate"))
              return ScriptRunnerDbArb.get(ScriptRunnerDbArb.TRUNCATED);
          else
              return ScriptRunnerDbArb.get(ScriptRunnerDbArb.ALTERED);
      } else if (string[0].toLowerCase().equals("truncate")) { //$NON-NLS-1$
        return ScriptRunnerDbArb.get(ScriptRunnerDbArb.TRUNCATED);
      } else if (string[0].toLowerCase().equals("analyze")) { //$NON-NLS-1$
        return ScriptRunnerDbArb.get(ScriptRunnerDbArb.ANALYZED);
      } else if (string[0].toLowerCase().equals("purge")) { //$NON-NLS-1$
        return ScriptRunnerDbArb.get(ScriptRunnerDbArb.PURGED);
      } else if(string[0].toLowerCase().equals("call")) { //$NON-NLS-1$
          return ScriptRunnerDbArb.get(ScriptRunnerDbArb.COMPLETED);
      } else if(string[0].toLowerCase().equals("rename")) { //$NON-NLS-1$
          return ScriptRunnerDbArb.get(ScriptRunnerDbArb.RENAMED);
      } else if(string[0].toLowerCase().equals("restore")) { //$NON-NLS-1$
          return ScriptRunnerDbArb.get(ScriptRunnerDbArb.RESTORED);
      } else if(string[0].toLowerCase().equals("recover")) { //$NON-NLS-1$
          return ScriptRunnerDbArb.get(ScriptRunnerDbArb.RECOVERED);
      } else if(string[0].toLowerCase().equals("explain")) { //$NON-NLS-1$
          return ScriptRunnerDbArb.get(ScriptRunnerDbArb.EXPLAINED);
      }else
        return ScriptRunnerDbArb.get(ScriptRunnerDbArb.SUCCEEDED);
      }
      return null;
    }
public static void main(String[] args) {
	SQLCommand s = new SQLCommand("alter type body whatever compile");
	SQL sql =new SQL(s,null); //null outputstream as we dont need it today.
	ScriptRunnerContext ctx = new ScriptRunnerContext();
	ctx.putProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE, true);
	sql.setScriptRunnerContext(ctx);
/*	System.out.println(sql.processMessage("CREATE TYPE data_typ1 AS OBJECT   ( year NUMBER,     MEMBER FUNCTION prod(invent NUMBER) RETURN NUMBER   )", true));
	System.out.println(sql.processMessage("CREATE TYPE BODY data_typ1 IS  MEMBER FUNCTION prod (invent NUMBER) RETURN NUMBER IS BEGIN              RETURN (year + invent);         END;      END; ", true));
	System.out.println(sql.processMessage("alter type body whatever compile", true));
	System.out.println(sql.processMessage("CREATE RESTORE POINT BLAH GUARANTEE FLASHBACK DATABASE", true));
	System.out.println(sql.processMessage("RESTORE DATABASE CHECK READONLY", true));
	System.out.println(sql.processMessage("RECOVER DATABASE DELETE ARCHIVELOG", true));
	System.out.println(sql.processMessage("FLASHBACK TABLE EMP TO SCN 123456", true));
	System.out.println(sql.processMessage("FLASHBACK TABLE table_name TO TIMESTAMP timestamp ENABLE TRIGGERS", true));f
	System.out.println(sql.processMessage("alter database flashback on", true));
	System.out.println(sql.processMessage("alter database flashback off", true));
	System.out.println(sql.processMessage("alter database open", true));
	System.out.println(sql.processMessage("alter database open resetlogs", true));
	System.out.println(sql.processMessage("alter database open mount", true)); */

}
}
