/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.dbtools.common.utils.StringUtils;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

public class SQLCommand implements ISQLCommand {
	public static final String SCRIPTTABLE = "/*SQLDEV:SCRIPTTABLE*/";
	public static final String CREATEREFINE = "CREATEREFINE";
	public static final String STRIPPED_CONTINUATION = "STRIPPED_CONTINUATION";
	private StmtType m_stmtType = null;
	private StmtSubType m_stmtSubType = null;
	private StmtResultType m_stmtResultType = null;
	private String m_stmtProperty = null;
	private boolean m_isExecutable = true;// defines if a command is able to
											// directly run the original sql
											// against the database
	private Restricted.Level m_restrictedLevel = Restricted.Level.R4;
	private boolean m_isRunnable = true;// stops a query from being run by the
										// script runner
	private int m_origStartLine = -1;
	private int m_origEndLine = -1;
	private String m_origSQL = null;
	private String m_modifiedSQL = null;
	private List<Object> m_binds = new ArrayList<Object>();
	private Properties m_properties = new Properties();
	private String m_stmtTerminator = null;

	private long m_time = 0;
	private boolean m_topLevelCommand = false;
	private boolean _isComplete = true;
	private boolean failed = false;
	private boolean m_setCompoundCmd = false;
	private String m_compoundSetCmd = null;
	private int scriptDepth = 0;
	private List<String> batchStatements = null;
  private String _loweredTrimmed;
  private String _loweredTrimmedNOWS;

	public SQLCommand(String origSQL) {
		m_origSQL = origSQL;
	}
	 
	public SQLCommand(StmtType stmtType, StmtSubType stmtSubType, StmtResultType stmtResultType, boolean stmtExecutable, Restricted.Level restrictedLevel) {
		m_stmtType = stmtType;
		m_stmtSubType = stmtSubType;
		m_stmtResultType = stmtResultType;
		m_isExecutable = stmtExecutable;
		m_restrictedLevel = restrictedLevel;
	}

	public SQLCommand(SQLCommand cmd) {
		m_stmtType = cmd.getStmtType();
		m_stmtSubType = cmd.getStmtSubType();
		m_stmtResultType = cmd.getResultsType();
		m_isExecutable = cmd.getExecutable();
		m_restrictedLevel = cmd.getRestrictedLevel();
	}

	@Override
	public Restricted.Level getRestrictedLevel() {
     return m_restrictedLevel;
  }

  /**
	 * @return the stmtResultType
	 */
	public StmtResultType getStmtResultType() {
		return m_stmtResultType;
	}

	/**
	 * @param stmtResultType
	 *            the stmtResultType to set
	 */
	public void setStmtResultType(StmtResultType stmtResultType) {
		m_stmtResultType = stmtResultType;
	}

	/**
	 * @return the stmtRunStmtExecutable
	 */
	public boolean isStmtExecutable() {
		return m_isExecutable;
	}

	/**
	 * @param stmtRunStmtExecutable
	 *            the stmtRunStmtExecutable to set
	 */
	public void setStmtExecutable(boolean isExecutable) {
		m_isExecutable = isExecutable;
	}

	/**
	 * @return the stmtSubType
	 */
	public StmtSubType getStmtSubType() {
		return m_stmtSubType;
	}

	/**
	 * @param stmtSubType
	 *            the stmtSubType to set
	 */
	public void setStmtSubType(StmtSubType stmtSubType) {
		m_stmtSubType = stmtSubType;
	}

	/**
	 * @return the stmtType
	 */
	public StmtType getStmtType() {
		return m_stmtType;
	}

	/**
	 * @param stmtType
	 *            the stmtType to set
	 */
	public void setStmtType(StmtType stmtType) {
		m_stmtType = stmtType;
	}

	public static enum StmtType {
		G_C_UNKNOWN, // = "1";
		G_C_SQL, // = "2";
		G_C_PLSQL, // = "3";
		G_C_SQLPLUS, // = "4";
		G_C_COMMENT, // = "5";
		G_C_EMPTYLINE, // = "6";
		G_C_MULTILINECOMMENT, // = "7";
		G_C_OLDCOMMENT, // = "8";
		G_C_USERDEFINED, //
		G_C_NOT_CONNECTED, //
		G_C_SCRIPT,
		G_C_JDBC,//
		// G_C_UNKNOWN_SET, // = "9";
		// G_C_SET,
		// G_C_UNKNOWN_CREATE
	};

	public static enum StmtSubType {
		G_S_UNKNOWN, // ="1"; /* only returned for G_C_UNKNOWN */
		G_S_ACCEPT, // ="2";
		G_S_ALTER, // ="3";
		G_S_ANALYZE, // ="4";
		G_S_APPEND, // ="5";
		G_S_ARCHIVE, // ="6";
		G_S_ASSOCIATE, // ="7";
		G_S_AT, // ="8"; /* i.e. "@" */
		G_S_ATNESTED, // ="9"; /* i.e. "@@" */
		G_S_ATTRIBUTE, // ="10";
		G_S_AUDIT, // ="11";
		G_S_BEGIN, // ="12";
		G_S_BLOCKTERMINATOR, // ="13"; /* Period ending SQL and PL/SQL */
		G_S_BREAK, // ="14";
		G_S_BTITLE, // ="15";
		G_S_CALL, // ="16";
		G_S_CHANGE, // ="17";
		G_S_CLEAR, // ="18";
		G_S_COLUMN, // ="19";
		G_S_COMMENT_SQL, // ="20"; /* SQL table COMMENT command */
		G_S_COMMIT, // ="21";
		G_S_COMMENT_PLUS, // ="22"; /* REM, --, or slash-asterisk */
		G_S_COMPUTE, // ="23";
		G_S_CONNECT, // ="24";
		G_S_COPY, // ="25";
		G_S_DECLARE, // ="28";
		G_S_DEFINE, // ="29";
		G_S_DEL_PLUS, // ="30"; /* SQL*Plus line-editor DEL line deletion */
		G_S_DELETE, // ="31"; /* SQL data deletion */
		G_S_DESCRIBE, // ="32";
		G_S_DISASSOCIATE, // ="33";
		G_S_DISCONNECT, // ="34";
		G_S_DROP, // ="35";
		G_S_EDIT, // ="36";
		G_S_EXECUTE, // =G_S_EXECUTE;
		G_S_EXPAND, // ="38";
		G_S_EXIT, // ="39";
		G_S_EXPLAIN, // ="40";
		G_S_FIND, //
		G_S_FLASHBACK, // ="41";
		G_S_GET, // ="42";
		G_S_GRANT, // ="43";
		G_S_HOST, // ="44";
		G_S_HOSTALIAS, // ="45"; /* '!' or '$ */
		G_S_HELP, // ="46"; /* HELP or "?" */
		G_S_INPUT, // ="47";
		G_S_INSERT, // ="48";
		G_S_LIST, // ="49";
		G_S_LOCK, // ="50";
		G_S_MERGE, // ="51";
		G_S_NEWPAGE, // ="52";
		G_S_NOAUDIT, // ="53";
		G_S_ORADEBUG, // ="54";
		G_S_PASSWORD, // ="55";
		G_S_PLSQLLABEL, // ="57"; /* <<mylabel>> begin ... end; */
		G_S_PAUSE, // ="56";
		G_S_PRINT, // ="58";
		G_S_PWD, //
		G_S_SET_PROPERTY, //
		G_S_PROMPT, // ="59";
		G_S_PURGE, // ="60";
		G_S_QUIT, // ="61";
		G_S_RENAME, // ="63";
		G_S_RECOVER, // ="62";
		G_S_REPFOOTER, // =G_S_REPFOOTER;
		G_S_REPHEADER, // ="65";
		G_S_REVOKE, // ="66";
		G_S_ROLLBACK_PLUS, // ="67"; /* ROLLBAC with no 'K' */
		G_S_ROLLBACK_SQL, // ="68";
		G_S_RUN, // ="69";
		G_S_SAVE, // ="70";
		G_S_SAVEPOINT, // ="71";
		G_S_SELECT, // ="72"; /* SELECT or "(SELECT...)" */
		// G_S_SET_PLUS, // ="73"; /* Client-side SET command */
		// G_S_SET_SQL, // ="74"; /* Server-side SET e.g. SET TRANSACTION */
		G_S_SHOW, // ="75";
		G_S_SHUTDOWN, // ="76";
		G_S_SLASH, // ="77"; /* "/" to re-execute previous SQL or PL/SQL */
		G_S_SPOOL, // ="78";
		G_S_SQLPLUSPREFIX, // ="79"; /* "#" */
		G_S_SQLTERMINATOR, // ="80"; /* ";" or "/" to end SQL*Plus, SQL and
							// PL/SQL */
		G_S_START, // ="81";
		G_S_STARTUP, // ="82";
		G_S_STORE, // ="83";
		G_S_TIMING, // ="84";
		G_S_TRUNCATE, // ="85";
		G_S_TTITLE, // ="86";
		G_S_UNDEFINE, // ="87";
		G_S_UPDATE, // ="88";
		G_S_VALIDATE, // ="89";
		G_S_VARIABLE, // ="90";
		G_S_WHENEVER, // ="91";
		G_S_WHICH, //
		G_S_WITH, // ="92";
		G_S_SET, //
		G_S_XQUERY, //
		G_S_SETLOGLEVEL, //
		G_S_LOADQUERYFILE, //
		G_S_SET_ARRAYSIZE, //
		G_S_SET_APPINFO, //
		G_S_SET_AUTOCOMMIT, //
		G_S_SET_AUTOPRINT, //
		G_S_SET_AUTOTRACE, //
		G_S_SET_COPYCOMMIT, //
		G_S_SET_CONCAT, //
		G_S_SET_EMBEDDED, //
		G_S_SET_ESCAPE, //
		G_S_SET_EXITCOMMIT, //
		G_S_SET_FEEDBACK, //
		G_S_SET_OWA, //
		G_S_SET_GETPAGE, //
		G_S_SET_SERVEROUTPUT, //
		G_S_SET_SPOOL, //
		G_S_SET_TERM, //
		G_S_SET_TIMING, //
		G_S_SET_VERIFY, //
		G_S_SET_XQUERY, //
		G_S_SET_CONSTRAINT, //
		G_S_SET_ROLE, //
		G_S_SET_TRANSACTION, //
		G_S_SET_PAUSE, //
		G_S_SET_ECHO, //
		G_S_SET_DEFINE, //
		G_S_SET_DESCRIBE, //
		G_S_SET_SCAN, //
		G_S_SET_SUFFIX, //
		G_S_SET_EDITFILE, //
		G_S_SET_NULL, //
		G_S_SET_NET, //
		G_S_SET_NEW_PAGE, //
		G_S_SET_NETOVERWRITE, //
		G_S_SET_OBSOLETE, // Used for Obsoleted Set commands.
		G_S_SET_UNKNOWN, // thisisasetcommand,butwehaventrecognized it fully
		G_S_SET_PAGESIZE, //
		G_S_SET_LINESIZE, G_S_SET_LONG, //
		G_S_SET_LONGC, //
		G_S_SET_COLSEP, //
		G_S_SET_SECUREDCOL, //
		G_S_SET_HEADING, //
		G_S_SET_HEADINGSEP, //
		G_S_SET_SECURELITERALS, //
		G_S_SET_TAB, // Added this to recognize the tab command otherwise this is obsolete.
		G_S_SET_TRIMSPOOL, //
		G_S_SET_TRIMOUT, //
		G_S_SET_WRAP, //
		G_S_SET_NUMBERWIDTH, //
		G_S_SET_NUMBERFORMAT, //
		G_S_CREATE_PACKAGE_HEADER, //
		G_S_CREATE_PACKAGE_BODY, //
		G_S_CREATE_PROCEDURE, //
		G_S_CREATE_TRIGGER, //
		G_S_CREATE_FUNCTION, //
		G_S_CREATE_TYPE, //
		G_S_CREATE_LIBRARY, //
		G_S_CREATE_JAVA, //
		G_S_CREATE_TABLE, //
		G_S_CREATE_VIEW, //
		G_S_CREATE_DATABASE, //
		G_S_CREATE_CONN, //
		G_S_CREATE_UNKNOWN, //
		G_S_FORALLEVENTS_STMTSUBTYPE, //
		G_S_FORALLSTMTS_STMTSUBTYPE, //This will listen to every command that has not been handled by a registered SQLCommand
		G_S_BEFOREAFTER_SCRIPT, //
		G_S_SET_WORKSHEETNAME, //
		G_S_BRIDGE, //
		G_S_DOC_PLUS, //
		G_S_SET_SQLPROMPT, // DOC
		G_S_OERR, //
		G_S_DDL, //
		G_S_CLEAR_SCREEN, //
		G_S_SET_TIME, //
		G_S_HISTORY, //
		G_S_ALIAS, //
		G_S_NET, //
		G_S_CD, //
		G_S_SQLPATH, // Only for registering for show, no command of this type.
		G_S_FORMAT, // ,G_S_SET_COLOR
		G_S_ADMINISTER, // ADMINISTER KEY MANAGEMENT
		G_S_CTAS, // CTAS
		G_S_SSHTUNNEL, // set up a tunnel to piggy back jdbc connections on
		G_S_SET_DDL, //
		G_S_LOAD, //
		G_S_SET_SQLBL, // Allow blank lines in the editor and make the terminator end the edit session.
		G_S_SET_BLO, // Sets the character used to end PL/SQL blocks to c.
		G_S_SET_CLEARSCREEN, // Set the mode for clear screen, top middle or bottom
		G_S_SET_ENCODING, // Set the encoding to read files in
		G_S_SCRIPT, // set for scripting inline
		G_S_SET_SQLPLUS_CLASSIC_MODE, // set for classix mode in sqlplus to allow us to be 100% compatible.
		G_S_SET_IGNORE_SETTING_MODE, // set this for ignoring settings in QA testing.
		G_S_SET_PRELIM, // Added to just set on/off
		G_S_SET_SHOWMODE, // Added to just set on/off
		G_S_SET_XMLFORMAT, //
		G_S_SET_CLOSECURSOR, //
		G_S_SET_SQLCOMPATABILITY, //
		G_S_SET_XMLOPTIMIZATIONCHECK,// 
		G_S_SET_HISTORY, //
		G_S_VERSION,//
		G_S_SET_COLINVISIBLE, // 
		G_S_NLSLANG,
		G_S_JDBC,
		G_S_SET_ERROR_LOGGING
	};

	public static enum StmtResultType {
		G_R_QUERY, // = "1";
		G_R_DML, // = "2";
		// this is for a 3 word command followed by the name of an object
		// such as alter table foo
		G_R_THREE_WORD, // = "3";
		// this is for a 1 word command followed by the name of an object
		// such as rename foo to foo2;
		G_R_ONE_WORD, // = "4";
		// grant or revoke
		G_R_PERMISSION, //
		G_R_NONE, //
		G_R_UNKNOWN, //
		G_R_SCRIPTEDQUERY;
	}

	public String getModifiedSQL() {
		return m_modifiedSQL;
	}

	public void setModifiedSQL(String modifiedSQL) {
		modifiedSQL = removeHints(modifiedSQL);
		m_modifiedSQL = modifiedSQL;
	}

	private String removeHints(String modifiedSQL) {
		if (modifiedSQL == null ) {
			return null;
		}
		if ( modifiedSQL.indexOf("/*sqldev") >0  ) {
  		//Here, instead of a blind trim, we are keeping the spaces on the left of the first line.
      int spaceOnLeft = StringUtils.rtrim(modifiedSQL).length() - modifiedSQL.trim().length();
  		for (String hint : SQLStatementTypes.hintTokens.keySet()) {
  			if (modifiedSQL.trim().toLowerCase().startsWith(hint.toLowerCase())) {
  				modifiedSQL = modifiedSQL.substring(hint.length()+spaceOnLeft);
  			}
  		}
		}
		return modifiedSQL;
	}

	public int getOrigEndLine() {
		return m_origEndLine;
	}

	public void setOrigEndLine(int origEndLine) {
		m_origEndLine = origEndLine;
	}

	public String getOrigSQL() {
	  if(m_origSQL == null){
	    return null;
	  } else {
	    return StringUtils.rtrim(m_origSQL);
	  }
	}

	public void setOrigSQL(String origSQL) {
		m_origSQL = origSQL;
	}

	public int getOrigStartLine() {
		return m_origStartLine;
	}

	public void setOrigStartLine(int origStartLine) {
		m_origStartLine = origStartLine;
	}

	public List<Object> getBinds() {
		return m_binds;
	}

	public void setBinds(List<Object> binds) {
		m_binds = binds;
	}

	// for the moment I want to be able to use both the orginal and new script
	// runner at the same time,
	// SQLCommand is a join point between the two script runners and the
	// outside.
	// using the ISQLCommand interface (based of the old SQLCommand) to
	// facilitate this.
	// TODO remove these methods once we get rid of the old Script Runner
	public void addBind(Object o) {
		m_binds.add(o);
	}

	public int getEndLine() {
		return getOrigEndLine();
	}

	public int getLines() {
		return getOrigEndLine() - getOrigStartLine();
	}

	public StmtResultType getResultsType() {
		return getStmtResultType();
	}

	public boolean getRunnable() {
		return m_isRunnable;
	}

	public String getSQLOrig() {
		return getOrigSQL();
	}

	public String getSQLOrigWithTerminator() {
		return m_origSQL + getStatementTerminator();
	}

	public String getSql() {
		return getModifiedSQL();
	}

	public String getSqlWithTerminator() {
		String seperator = "";
		if (getStatementTerminator().equals("/")) {
			seperator = "\n";
		}
		return getModifiedSQL() + seperator + getStatementTerminator();
	}

	public int getStartLine() {
		return getOrigStartLine();
	}

	public StmtType getStmtClass() {
		return getStmtType();
	}

	public StmtSubType getStmtId() {
		return getStmtSubType();
	}

	public void setEndLine(int line) {
		setOrigEndLine(line);
	}

	public void setLines(int lines) {
		// dont know what to do here. log it incase anyone calls it
	}

	public void setResultsType(StmtResultType resultsType) {
		setStmtResultType(resultsType);
	}

	public void setRunnable(boolean isRunnable) {
		m_isRunnable = isRunnable;
	}

	public void setExecutable(boolean isExecutable) {
		m_isExecutable = isExecutable;
	}

	public boolean getExecutable() {
		return m_isExecutable;
	}

	public void setSQLOrig(String sqlOrig) {
		setOrigSQL(sqlOrig);
	}

	public void setSql(String sql) {
		setModifiedSQL(sql);
	}

	public void setStartLine(int line) {
		setOrigStartLine(line);
	}

	public void setStmtClass(StmtType stmtClass) {
		setStmtType(stmtClass);
	}

	public void setStmtId(StmtSubType stmtId) {
		setStmtSubType(stmtId);
	}

	public String getStmtProperty() {
		return m_stmtProperty;
	}

	public void setStmtProperty(String stmtProperty) {
		m_stmtProperty = stmtProperty;
	};

	public boolean isSqlPlusSetCmd() {
		// this saves me from specifying this everything I add a new create
		// is sqlplus also includes G_S_SET command - which is compound set
		// which may not be processed yet.
		if (getStmtType() == StmtType.G_C_SQLPLUS && getStmtSubType().toString().startsWith("G_S_SET")) { //$NON-NLS-1$
			return true;
		}
		return false;
	}

	public boolean isCreateSQLCmd() {
		// this saves me from specifying this everything I add a new create
		if (getStmtType() == StmtType.G_C_SQL && getStmtSubType().toString().startsWith("G_S_CREATE_")) { //$NON-NLS-1$
			return true;
		}
		return false;
	}

	public boolean isCreatePLSQLCmd() {
		// this saves me from specifying this everything I add a new create
		if (getStmtType() == StmtType.G_C_PLSQL && getStmtSubType().toString().startsWith("G_S_CREATE_")) { //$NON-NLS-1$
			return true;
		}
		return false;
	}

	public boolean isCreateCmd() {
		// this saves me from specifying this everything I add a new create
		if (getStmtSubType().toString().startsWith("G_S_CREATE_")) { //$NON-NLS-1$
			return true;
		}
		return false;
	}

	public Object getProperty(String key) {
		return m_properties.get(key);
	}

	public void setProperty(String key, Object value) {
		m_properties.put(key, value);

	}

	public void setStatementTerminator(String stmtTerminator) {
		m_stmtTerminator = stmtTerminator;
	}

	public String getStatementTerminator() {
		return m_stmtTerminator != null ? m_stmtTerminator : "";
	}

	public boolean equals(Object sqlcommand) {
		if (!(sqlcommand instanceof SQLCommand)) {
			return false;
		}
		SQLCommand sqlCommand = (SQLCommand) sqlcommand;
		if (sqlCommand.getStartLine() == getStartLine() && sqlCommand.getEndLine() == getEndLine() && sqlCommand.getOrigSQL().equals(getOrigSQL())
				&& sqlCommand.getStmtType() == getStmtType() && sqlCommand.getStmtSubType() == getStmtSubType()
				&& sqlCommand.getStmtResultType() == getStmtResultType()) {
			return true;
		} else {
			return false;
		}
	}

	// private static StmtSubType[] NoLogPermissibleStmtSubType = {
	// StmtSubType.G_S_AT, StmtSubType.G_S_COLUMN, StmtSubType.G_S_SPOOL,
	// StmtSubType.G_S_SET_PAGESIZE, StmtSubType.G_S_SET_LINESIZE,
	// StmtSubType.G_S_SET_LONG, StmtSubType.G_S_SET_COLSEP,
	// StmtSubType.G_S_SET_HEADINGSEP, StmtSubType.G_S_SET_HEADING,
	// StmtSubType.G_S_SET_WRAP, StmtSubType.G_S_SET_NUMBERFORMAT,
	// StmtSubType.G_S_SET_NUMBERWIDTH, StmtSubType.G_S_CONNECT };
	//
	//
	// public static boolean checkNologCommands(StmtSubType stmtSubType,
	// ScriptRunnerContext context) {
	// if (context.getProperty(ScriptRunnerContext.NOLOG) != null
	// && context.getProperty(ScriptRunnerContext.NOLOG).toString()
	// .toLowerCase().equals("true")) {
	// if (stmtSubType == null) {
	// return true;
	// }
	// for (StmtSubType currentStmtSubType : NoLogPermissibleStmtSubType) {
	// if (currentStmtSubType == stmtSubType) {
	// return true;
	// }
	// }
	// }
	// return false;
	// }
	private static StmtSubType[] NoLogExecutableStmtSubType = { StmtSubType.G_S_ATNESTED, StmtSubType.G_S_AT, StmtSubType.G_S_START, StmtSubType.G_S_HOSTALIAS,
			StmtSubType.G_S_HELP, StmtSubType.G_S_COMMENT_PLUS, StmtSubType.G_S_SPOOL, StmtSubType.G_S_SET_PAGESIZE, StmtSubType.G_S_SET_LINESIZE,
			StmtSubType.G_S_SET_LONG, StmtSubType.G_S_SET_COLSEP, StmtSubType.G_S_SET_HEADINGSEP, StmtSubType.G_S_SET_HEADING, StmtSubType.G_S_SET_WRAP,
			StmtSubType.G_S_SET_NUMBERFORMAT, StmtSubType.G_S_SET_NUMBERWIDTH, StmtSubType.G_S_COLUMN, StmtSubType.G_S_EXIT, StmtSubType.G_S_QUIT,
			StmtSubType.G_S_CONNECT, StmtSubType.G_S_SET_SQLPROMPT, StmtSubType.G_S_SET_NET, StmtSubType.G_S_SET_NETOVERWRITE, StmtSubType.G_S_NET,
			StmtSubType.G_S_FORMAT, StmtSubType.G_S_SHOW
			/* for now obvious omissions??? startup (shutdown when connected) */
	};

	public static boolean isNoLogExecutable(ISQLCommand cmd) {
		if (cmd == null || cmd.getStmtSubType() == null) {
			return true;
		}
		for (StmtSubType currentStmtSubType : NoLogExecutableStmtSubType) {
			if (currentStmtSubType == cmd.getStmtSubType()) {
				return true;
			}
		}
		return false;
	}

	private static StmtSubType[] NonOracleExecutableStmtSubType = { StmtSubType.G_S_ATNESTED, StmtSubType.G_S_AT, StmtSubType.G_S_START, StmtSubType.G_S_SLASH,
			StmtSubType.G_S_HOSTALIAS, StmtSubType.G_S_HELP, StmtSubType.G_S_COMMENT_PLUS, StmtSubType.G_S_COLUMN, StmtSubType.G_S_SPOOL,
			StmtSubType.G_S_SET_PAGESIZE, StmtSubType.G_S_SET_LINESIZE, StmtSubType.G_S_SET_LONG, StmtSubType.G_S_SET_COLSEP, StmtSubType.G_S_SET_HEADINGSEP,
			StmtSubType.G_S_SET_HEADING, StmtSubType.G_S_SET_WRAP, StmtSubType.G_S_SET_NUMBERFORMAT, StmtSubType.G_S_SET_NUMBERWIDTH, StmtSubType.G_S_EXIT,
			StmtSubType.G_S_QUIT, StmtSubType.G_S_CONNECT };

	public static boolean isNonOracleExecutable(ISQLCommand cmd) {
		if (cmd == null || cmd.getStmtSubType() == null) {
			return true;
		}
		for (StmtSubType currentStmtSubType : NonOracleExecutableStmtSubType) {
			if (currentStmtSubType == cmd.getStmtSubType()) {
				return true;
			}
		}
		// SHOW is a SQL Command that should not be run by SQL*Plus against non
		// Oracle connections, but SHOW JDBC can...
		if (isSHOWJDBC(cmd)) {
			return true;
		}
		return false;
	}

	private static boolean isSHOWJDBC(ISQLCommand cmd) {
		if (cmd.getStmtSubType() == StmtSubType.G_S_SHOW) {
			try {
				String showType = cmd.getSql().trim().toLowerCase().replaceAll("^show?\\s+", ""); //$NON-NLS-1$ //$NON-NLS-2$
				if (showType != null && showType.startsWith("jdbc")) {
					return true;
				}
			} catch (Exception e) {
			}
		}
		return false;
	}

	@Override
	public long getTiming() {
		return m_time;
	}

	@Override
	public void setTiming(long elapsedTime) {
		m_time = elapsedTime;
	}

	@Override
	public boolean isTopLevel() {
		return m_topLevelCommand;
	}

	@Override
	public void setTopLevel(boolean topLevel) {
		m_topLevelCommand = topLevel;
	}

	public void setIsComplete(boolean complete) {
		_isComplete = complete;
	}

	@Override
	public boolean isComplete() {
		return _isComplete;
	}

	/**
	 * @return the failed
	 */
	public boolean isFailed() {
		return failed;
	}

	/**
	 * @param failed
	 *            the failed to set
	 */
	public void setFail() {
		failed = true;
	}

	@Override
	public boolean isFail() {
		return failed;

	}

	public void setIsSetCompoundCmd(boolean compoundCmd) {
		m_setCompoundCmd = compoundCmd;
	}

	public boolean isSetCompoundCmd() {
		return m_setCompoundCmd;
	}

	public void setCompoundSetcmd(String cmd) {
		m_compoundSetCmd = cmd;
	}

	public String getCompoundSetcmd() {
		return m_compoundSetCmd;
	}

	public String getReadableType() {
		// G_C_UNKNOWN,G_C_SQL,G_C_PLSQL,G_C_SQLPLUS,G_C_COMMENT,G_C_EMPTYLINE,G_C_MULTILINECOMMENT,G_C_OLDCOMMENT,G_C_USERDEFINED,G_C_NOT_CONNECTED,G_C_SCRIPT
		if (getStmtClass() == null) {
			return OTHER;
		} else if (getStmtClass() == StmtType.G_C_PLSQL) {
			return PLSQL;
		} else if (getStmtClass() == StmtType.G_C_SQLPLUS) {
			return SQLPLUS;
		} else if (getStmtClass() == StmtType.G_C_UNKNOWN) {
			return OTHER;
		} else if (getStmtClass() == StmtType.G_C_COMMENT || getStmtClass() == StmtType.G_C_MULTILINECOMMENT || getStmtClass() == StmtType.G_C_EMPTYLINE
				|| getStmtClass() == StmtType.G_C_OLDCOMMENT) {
			return IGNORE;
		} else if (getStmtClass() == StmtType.G_C_USERDEFINED) { // create connection, set worksheet name
			return SQLPLUS;
		} else if (getStmtClass() == StmtType.G_C_NOT_CONNECTED) { // not used anymore
			return SQLPLUS;
		} else if (getStmtClass() == StmtType.G_C_SCRIPT) { // not sure what this is. check with turloch
			return OTHER;
		} else { // most likely G_C_SQL , but would like to break this down further
			if (getResultsType() != null && getStmtSubType() != null) {
				if (getResultsType() == StmtResultType.G_R_QUERY) {
					return QUERY;
				} else if (getResultsType() == StmtResultType.G_R_DML) {
					return DML;
				} else if (getResultsType() == StmtResultType.G_R_SCRIPTEDQUERY) {
					return SQLPLUS;
				} else if (getStmtSubType() == StmtSubType.G_S_COMMIT || getStmtSubType() == StmtSubType.G_S_ROLLBACK_SQL
						|| getStmtSubType() == StmtSubType.G_S_SAVEPOINT || getStmtSubType() == StmtSubType.G_S_SET_TRANSACTION) {
					return TRANS;
				} else if (getStmtSubType() == StmtSubType.G_S_SET_ROLE) {
					return SESSION;
				} else if (getStmtSubType() == StmtSubType.G_S_ALTER) {
					if (getOrigSQL().indexOf("SESSION") != -1) {
						return SESSION;
					} else if (getOrigSQL().indexOf("SYSTEM") != -1) {
						return SYSTEM;
					}
				} else if (getStmtSubType() == StmtSubType.G_S_CALL || getStmtSubType() == StmtSubType.G_S_EXPLAIN
						|| getStmtSubType() == StmtSubType.G_S_LOCK) {
					return DML;
				} else if (getStmtSubType() == StmtSubType.G_S_JDBC){
					return JDBC;
				} else {
					return DDL;
				}
			} else {
				return OTHER;
			}
		}
		return OTHER;
	}

	private static final String DDL = "ddl";
	private static final String DML = "dml";
	private static final String QUERY = "query";
	private static final String PLSQL = "plsql";
	private static final String SQLPLUS = "sqlplus";
	private static final String TRANS = "transaction-control";
	private static final String SESSION = "session-control";
	private static final String SYSTEM = "system-control";
	private static final String OTHER = "other";
	private static final String IGNORE = "ignore";
	private static final String JDBC="jdbc";

	@Override
	public int getdepth() {
		if (scriptDepth > 0) {
			return scriptDepth;
		}
		return 0;
	}
	/**
	 * Set the depth of the script we are in. If its not set we're top level
	 */
	@Override
	public void setDepth(int depth) {
		scriptDepth = depth;
	}

  @Override
  public void setBatchStatements(List<String> stmts) {
    batchStatements = stmts;
  }

  @Override
  public List<String> getBatchStatements() {
    return batchStatements;
  }
  
  public String getLoweredTrimmedSQL(){
    if (_loweredTrimmed == null ) {
      _loweredTrimmed = getSql().trim().toLowerCase();
    }
    return _loweredTrimmed;
  }
  public String getLoweredTrimmedNoWhitespaceSQL(){
    if (_loweredTrimmedNOWS == null ) {
      _loweredTrimmedNOWS = getLoweredTrimmedSQL().replaceAll(" ", "");
    }
    return _loweredTrimmedNOWS;
  }
}
