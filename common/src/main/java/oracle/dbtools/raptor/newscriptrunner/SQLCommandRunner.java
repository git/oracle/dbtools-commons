/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.db.ConnectionResolver;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.ValueType;
import oracle.dbtools.raptor.query.Bind;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.dbtools.util.Debug;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleTypes;

public abstract class SQLCommandRunner  {
    protected static final Logger LOGGER = Logger.getLogger(SQLCommandRunner.class.getName());
    private ScriptRunnerContext scriptRunnerContext = null;
    protected ISQLCommand cmd;
    protected Connection conn;
    protected String connName;
    protected BufferedOutputStream out;
    protected PreparedStatement stmt;
    protected CallableStatement callStmt;
    Statement st, cstmt;
    protected Debug debug = new Debug();
    protected String statusLine = null;
    //Programs in sqldeveloper proper may refer to these static final ints.
    public static final int CONTINUE = ScriptUtils.CONTINUE;
    public static final int EXITMASK = ScriptUtils.EXITMASK;
    public static final int ACTIONMASK = ScriptUtils.ACTIONMASK;
    public static final int ACTIONNONE = ScriptUtils.ACTIONNONE;
    public static final int ACTIONCOMMIT = ScriptUtils.ACTIONCOMMIT;
    public static final int ACTIONROLLBACK = ScriptUtils.ACTIONROLLBACK;
    public static final int EXIT=ScriptUtils.EXIT;
    public static final String EXECUTEFAILED = ScriptUtils.EXECUTEFAILED;
    /* double binds start */
    protected HashSet<String> doubleBinds = null;
    /* look up mangled name - made up initialisation bind to real bind mapping */
    protected HashMap<String, String> lookUpMangle = new HashMap<String,String>();
    /* altered sql containing additional declare header and altered body */
    protected String altSQL = null;
    /* base of myuniq concatenated with count+"Z" for unique value. */
    protected String myUniq = "SqlDevBind"; //$NON-NLS-1$
	private static ArrayList<ScriptCommandListener> commandlisteners = new ArrayList<ScriptCommandListener>();

    /* convienience function to avooid 5 repeated if then elses in the code */
    protected String alteredSQL(HashSet<String> inDoubleBinds, String cmdSQL, String inAltSQL) {
        String retSQL=null;
        if (inDoubleBinds!=null) {
            retSQL = inAltSQL;
        } else {
            retSQL = cmdSQL;
        }
        return retSQL;
    }
    /* double binds end */
    /**
     * See SQLPLUS.interupt for interupting a file execution
     */
    public void interrupt() {
    	  if(getScriptRunnerContext()!=null){
    	    // The "writer" has access to the script runner context. 
    	  	//So interrupt here will kill the command, but the setting the flag in the context will allow the writer/printing of the result set to stop
    		  getScriptRunnerContext().setInterrupted(true);
    	  }
        try {
            if (stmt != null && !stmt.isClosed()) {
                stmt.cancel();
            }
        } catch (SQLFeatureNotSupportedException e) {
        	assert (e != null); // just to force compile
        	LOGGER.log(Level.WARNING, "Unsupported Feature");
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].getMethodName() +"Line:"+e.getStackTrace()[ 0 ].getLineNumber()+":"+e.getMessage());
        }
        try {
            if (st != null  && !st.isClosed()) {
                st.cancel();
            }
        } catch (SQLFeatureNotSupportedException e) {
            LOGGER.log(Level.WARNING, "Unsupported Feature");
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].getMethodName() +"Line:"+e.getStackTrace()[ 0 ].getLineNumber()+":"+e.getMessage());
        }
        try {
            if (callStmt != null && !callStmt.isClosed()) { // cancel pl/sql
                callStmt.cancel();
            }
        } catch (SQLFeatureNotSupportedException e) {
            LOGGER.log(Level.WARNING, "Unsupported Feature");
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].getMethodName() +"Line:"+e.getStackTrace()[ 0 ].getLineNumber()+":"+e.getMessage());
        }
        try {
            if (cstmt != null  && !cstmt.isClosed()) {
                cstmt.cancel();
            }
        } catch (SQLFeatureNotSupportedException e) {
            LOGGER.log(Level.WARNING, "Unsupported Feature");
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].getMethodName() +"Line:"+e.getStackTrace()[ 0 ].getLineNumber()+":"+e.getMessage());
        }
    }

    public SQLCommandRunner(ISQLCommand cmd, BufferedOutputStream out) {
        this.cmd = cmd;
        this.out = out;
    }

    //moved to scriptUtils stub left behind 
    protected void report(ScriptRunnerContext ctx, String s) {
        ScriptUtils.report(ctx, s, out);
    }

    //moved to scriptUtils stub left behins
    public void doWhenever(boolean SqlError) {// rollback commit and or set
        ScriptUtils.doWhenever(scriptRunnerContext, cmd, conn, SqlError);
    }

    public ISQLCommand getCmd() {
        return cmd;
    }

    public void setCmd(ISQLCommand cmd) {
        this.cmd = cmd;
    }

    public String getConnName() {
        return connName;
    }

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
        this.connName = ConnectionResolver.getConnectionName(conn);
    }

    public void setScriptRunnerContext(ScriptRunnerContext scriptRunnerContext) {
        this.scriptRunnerContext = scriptRunnerContext;
    }

    public void write(String s) throws IOException {
        out.write(this.getScriptRunnerContext().stringToByteArrayForScriptRunnerNonStatic(s));
    }

    public void setConnName(String connName) {
        this.connName = connName;
        try {
            conn = ConnectionResolver.getConnection(connName);
        } catch (Exception ex) {
        }
    }

//    public String getStatusLine() {
//        return Ide.getStatusBar().getText();
//    }

//    public void setStatusLine(String statusLine) {
//        Ide.getStatusBar().setText(statusLine);
//    }

    public ScriptRunnerContext getScriptRunnerContext() {
        return scriptRunnerContext;
    }

    public boolean setBinds(PreparedStatement stmt) throws SQLException {
        boolean setBind = true;
        boolean bindSetInCommand = false;
        List<String> al = DBUtil.getBindNames(alteredSQL(doubleBinds, cmd.getSql(), altSQL), true);
        int bindIdx = al.size();
        Map<String, Object> popupVars =  (Map <String, Object>) getScriptRunnerContext()
            .getProperty(ScriptRunnerContext.POPUPBINDS);
        Map<String, Bind> vars = getScriptRunnerContext().getVarMap();
        //try and bind with a bindmap passed in (like from reports)
        Map <String,Bind> externalVars = (Map<String,Bind>)getScriptRunnerContext().getProperty("BINDMAP");
        if (externalVars==null) {
            externalVars = new HashMap<String,Bind>();
        }
        //if(externalVars!=null && externalVars.size() > 0){
        //    externalVars.putAll(vars);//this way locally defined vars override any external defined vars (from master report)
        //    vars = externalVars;//the rest of the code uses vars, so reassign it.
        //}
        for (int i = al.size()-1; i >= 0; i--) {
            String bindName = null;
            if (doubleBinds!=null) {
                bindName = lookUpMangle.get(al.get(i));
                if (bindName == null) {//not an init bind
                    bindName = al.get(i);
                }
            } else {
                
                bindName = al.get(i);
            }
            boolean external = false;
            boolean local = false;
            boolean popup = false;
            Bind qbind=null;
            if (popupVars.containsKey(bindName)) {
                popup = true;
            }
            if (vars.containsKey(bindName.toUpperCase())) {
                local = true;
            }
            if (externalVars.containsKey(bindName)) {
                external = true;
            }
            if(local && vars.get(bindName.toUpperCase()).getDataType()!=null){
              Bind bind = vars.get(bindName.toUpperCase());
              DataValue dataValue = bind.getDataValue();
              DataType dataType = bind.getDataType();
              Object typedValue = (dataValue != null) ? dataValue.getTypedValue( ValueType.DATUM) : null;
              int type =dataType.getSqlDataType(ValueType.DATUM);
              
              if(typedValue instanceof oracle.sql.CHAR &&
                 ! (typedValue instanceof oracle.dbtools.raptor.datatypes.oracle.sql.CHAR)){
                  typedValue = dataValue.getTypedValue(ValueType.JAVA);
             }
              stmt.setObject(bindIdx, typedValue,type);
              bindIdx--;
            }else if ((popup)||(local)||(external)) {
                Bind bind = null;
                String type = null;
                String value = null;
                if (popup) {
                    if ((popupVars.get(bindName)).equals(DBUtil.NULL_VALUE)) {
                       value = null;
                    } else {
                       value = (String) (popupVars.get(bindName));
                    }
                    type = "VARCHAR2";
                } else if (local) {
                    bind = vars.get(bindName.toUpperCase());
                    qbind=bind;
                    type = bind.getType();
                    value = bind.getValue();
                } else {
                    bind = externalVars.get(bindName);
                    qbind=bind;
                    type = bind.getType();
                    value = bind.getValue();
                }
                if (popup) {//popup just has a string value type is wrong and nvarchar will be wrong.
                    if (value != null) {
                        stmt.setString(bindIdx, value);
                    } else {
                        stmt.setNull(bindIdx, java.sql.Types.VARCHAR);
                    }
                } else {
                    if (type.toUpperCase().indexOf("NCHAR") > -1 || type.toUpperCase().indexOf("NVARCHAR") > -1 || type.toUpperCase().indexOf("NCLOB") > -1) {
                        ((OraclePreparedStatement) stmt).setFormOfUse(bindIdx, OraclePreparedStatement.FORM_NCHAR);
                    }
                    if (type.toUpperCase().indexOf("CHAR") > -1) {
                        if (value != null) {
                            stmt.setString(bindIdx, value);
                        } else {
                            stmt.setNull(bindIdx, java.sql.Types.VARCHAR);
                        }
                    } else if (type.toUpperCase().equals("NUMBER")) { //$NON-NLS-1$
                            try {
                                if (value != null) {
                                    stmt.setBigDecimal(bindIdx, new BigDecimal(value));
                                } else {
                                    stmt.setNull(bindIdx, java.sql.Types.NUMERIC);
                                }
                            } catch (NumberFormatException e) {
                                // setObject() here does not work. throws Parameter Type Conflict Exception.
                                stmt.setBigDecimal(bindIdx, new BigDecimal(0));
                            }
                    } else  if (type.toUpperCase().indexOf("CLOB") > -1) {
                                if (value != null) {
                                    stmt.setClob(bindIdx, DataTypesUtil.getCLOB(value, conn));
                                } else {
                                    stmt.setNull(bindIdx, java.sql.Types.CLOB);
                                }
                    } else  if (type.toUpperCase().equals("REFCURSOR")) {
                                    /* stmt.setObject(bindIdx, value) */; // Raghu: probably cant set a refcursor
                    } else  if (type.toUpperCase().equals("BINARY_DOUBLE")) { //$NON-NLS-1$
                                        try {
                                            if (value != null) {
                                                ((OraclePreparedStatement) stmt).setBinaryDouble(bindIdx, Double.parseDouble(value));
                                            } else {
                                                stmt.setNull(bindIdx, OracleTypes.BINARY_DOUBLE);
                                            }
                                        } catch (NumberFormatException e) {
                                            stmt.setString(bindIdx, value);
                                        }
                   } else if (type.toUpperCase().equals("BINARY_FLOAT")) { //$NON-NLS-1$
                                            try {
                                                if (value != null) {
                                                    ((OraclePreparedStatement) stmt).setBinaryFloat(bindIdx, Float.parseFloat(value));
                                                } else {
                                                    stmt.setNull(bindIdx, OracleTypes.BINARY_FLOAT);
                                                }
                                            } catch (NumberFormatException e) {
                                                stmt.setString(bindIdx, value);
                                            }
                    } else if (type.toUpperCase().equals("BLOB")) {
                        try {
                            if ((qbind!=null)&&(qbind.isLastSetObj()==true)) {
                                if (qbind.getValueObj()!=null) {
                                    stmt.setBlob(bindIdx, (Blob)qbind.getValueObj());
                                } else {
                                      stmt.setNull(bindIdx, OracleTypes.BLOB);
                                }
                            } else {
                                if (value!=null) {
                                    try {
                                        stmt.setBlob(bindIdx, DataTypesUtil.getBLOB(value.toCharArray(), conn));
                                    } catch (IOException e) {
                                       stmt.setNull(bindIdx, OracleTypes.BLOB);
                                    }
                                } else {
                                      stmt.setNull(bindIdx, OracleTypes.BLOB);
                                }
                            }
                        } catch (SQLException e) {
                            LOGGER.log(Level.FINE,e.getMessage(),e);
                            try {
                                stmt.setNull(bindIdx, OracleTypes.BLOB);
                            } catch (Exception ee) {
                            }
                        }
                    }
                }
                bindIdx--;
            } else if (cmd.getBinds().size() > 0) {
                //should never get here with doublebind fix flagged - skipped as difficult to sync altering the sql & binds
                //with the exteral to isqlcommandrunner/plsql/scriptrunercontext 3rd party programmatically getting and setting binds
                bindSetInCommand = true;
            } else {
                    // Bind variable used is not in the Var Map. not declared.
                    report(scriptRunnerContext, ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNDECLARED_VARIABLE, bindName.toUpperCase(Locale.US)));
                    setBind = false;
                    break;
            }
        }
        if (bindSetInCommand) {
            DBUtil.bind(stmt, cmd.getBinds());
            setBind = true;
        }
        return setBind;
    }

    protected void unlockConnection() {
        try {
            LockManager.unlock(conn);
        } catch (Exception e) {
        }
    }

    protected boolean lockConnection() {
        return LockManager.lock(conn);
    }

    /**
     * returns the base connection name, not the current connection.
     * The current connection name may not be a SQL Developer connection but a java.sql.Connection used when "CONNECT" statement is performed by the script runner
     * @return
     */
    protected String resolveConnectionName() { // use getBaseConnection from scriptRunnerContext as
        // we may have Connected down to a sub connection.//Base connection null check done in getConnectionInfo
        return ConnectionResolver.getConnectionName(this.getScriptRunnerContext().getBaseConnection());
    }

    /**
     * returns the properties of the base connection . Not the current connection.
     * @return
     */
    protected Properties getConnectionInfo() {
        if (this.getScriptRunnerContext().getBaseConnection()==null) {
            return null;
        }
        return ConnectionResolver.getConnectionInfo(resolveConnectionName());
    }
	public static void registerListener(ScriptCommandListener listener) {
		if (!commandlisteners.contains(listener)) {
			commandlisteners.add(listener);
		}
	}

	public void unregisterListener(ScriptCommandListener listener) {
		if (commandlisteners.contains(listener)) {
			commandlisteners.remove(listener);
		}
	}

	public void fireListenersRegister(ScriptRunnerContext context) {
		for (Iterator<ScriptCommandListener> iterator = commandlisteners.iterator(); iterator.hasNext();) {
			ScriptCommandListener listener = iterator.next();
			listener.registerdisplay(context);
		}
	}

	/**
	 * fireListenerOutputLog
	 * 
	 * @param format
	 *            PLSQL
	 */
	public void fireListenerOutputLog(String format) {
		for (Iterator<ScriptCommandListener> iterator = commandlisteners.iterator(); iterator.hasNext();) {
			ScriptCommandListener listener = iterator.next();
			listener.outputLog(format);
		}
	}

	/**
	 * fireListenerWarning
	 * 
	 * @param lineNumber
	 * @param column
	 * @param i
	 * @param errorMessage
	 *            PLSQL
	 */
	public void fireListenerWarning(int lineNumber, int column, int i, String errorMessage) {
		for (Iterator<ScriptCommandListener> iterator = commandlisteners.iterator(); iterator.hasNext();) {
			ScriptCommandListener listener = iterator.next();
			listener.warning(lineNumber, column, i, errorMessage);
		}

	}

	/**
	 * fireListenerError
	 * 
	 * @param lineNumber
	 * @param column
	 * @param i
	 * @param errorMessage
	 *            PLSQL
	 */
	public void fireListenerError(int lineNumber, int column, int i, String errorMessage) {
		for (Iterator<ScriptCommandListener> iterator = commandlisteners.iterator(); iterator.hasNext();) {
			ScriptCommandListener listener = iterator.next();
			listener.error(lineNumber, column, i, errorMessage);
		}

	}

	public void fireListenerunRegisterAsynchronousLogPage() {
		for (Iterator<ScriptCommandListener> iterator = commandlisteners.iterator(); iterator.hasNext();) {
			ScriptCommandListener listener = iterator.next();
			listener.unregisterdisplay();
		}

	}
}
