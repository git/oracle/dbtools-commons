/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import oracle.dbtools.app.Literals2Binds;
import oracle.dbtools.common.utils.FileUtils;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.plusplus.IBuffer;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext.SqlplusVariable;
import oracle.dbtools.raptor.newscriptrunner.commands.HelpMessages;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.commands.SetAppinfo;
import oracle.dbtools.raptor.newscriptrunner.commands.alias.Aliases;
import oracle.dbtools.raptor.newscriptrunner.commands.net.NetCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.net.NetEntries;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowLogin;
import oracle.dbtools.raptor.nls.DefaultNLSProvider;
import oracle.dbtools.raptor.nls.NLSLang;
import oracle.dbtools.raptor.nls.OracleNLSProvider;
import oracle.dbtools.raptor.query.Bind;
import oracle.dbtools.raptor.query.Parser;
import oracle.dbtools.raptor.utils.TCPTNSEntry;
import oracle.dbtools.raptor.utils.TNSHelper;
import oracle.dbtools.util.Pair;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.BLOB;
import oracle.sql.CLOB;

public class SQLPLUS extends SQLCommandRunner {
    public static String PRODUCT_NAME = "SQL Developer"; //$NON-NLS-1$
    // PRODUCT_NAME goes into the database should thus be internationaized?

    protected static final String m_lineSeparator = System.getProperty("line.separator"); //$NON-NLS-1$

    private String directory = null;
    public static ProviderForSQLPATH sqlpathProvider = null;

    private ScriptExecutor _runner = null;

    public SQLPLUS(ISQLCommand cmd, BufferedOutputStream out) {
        super(cmd, out);
    }

    public static void setProductName(String newName) {
        PRODUCT_NAME = newName;
    }

    public void interrupt() {
        if (_runner != null) {
            _runner.interrupt();
        }
    }

    private void runGoToLine(String sql) {
        int lineNo = -1;
        try {
            lineNo = Integer.parseInt(sql.split("\\s+")[0]); //$NON-NLS-1$
        } catch (NumberFormatException e) {
            getScriptRunnerContext().write(MessageFormat.format(
                    Messages.getString("SQLPLUS.1"), new Object[] { sql })); //$NON-NLS-1$
            getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), MessageFormat.format(
                    Messages.getString("SQLPLUS.1"), new Object[] { sql }), sql);
        }
        if (getScriptRunnerContext().getSQLPlusBuffer().getBufferSafe().linesInBuffer() > 0 && lineNo >= 0) {
            getScriptRunnerContext()
                    .write(getScriptRunnerContext().getSQLPlusBuffer().getBufferSafe().list(lineNo, false));
        } else {
            getScriptRunnerContext().write("\nSP2-0223: No lines in SQL Buffer."); //$NON-NLS-1$
            getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), "SP2-0223: No lines in SQL Buffer.",cmd.getSql());
        }
    }

    public void run() {
        // first remove last semicolon
        String sql = cmd.getSql().trim();
        if (sql.endsWith(";")) { //$NON-NLS-1$
            // remove last character only matters if length>1
            if (sql.length() > 1) {
                cmd.setSql(sql.substring(0, sql.length() - 1));
            }
        }
        if (cmd.isSqlPlusSetCmd()) {
            runSetPlus(); // TODO: we have much finer grained parsing of the SET
                            // commands now.
            // at the moment run the same code as 1.5.3, but we should break
            // this into SET DEFINE, SET SPOOL ....
        }
        switch (cmd.getStmtSubType()) {
        case G_S_UNKNOWN:
            try {
                if (!runAliases()) {
                    if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) != null
                            && Boolean.parseBoolean(getScriptRunnerContext()
                                    .getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
                        if (justANumber(sql)) {
                            // Need to get the buffer safe and set the current
                            // line there, if it exists.
                            runGoToLine(sql);
                        } else if (cmd.getSQLOrig().length() < 10) {
                            report(getScriptRunnerContext(), MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.SQLPLUS_UNKNOWN),
                                    cmd.getSQLOrig()));
                           this.getScriptRunnerContext().addUnknown();
                        } else {
                            report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.SQLPLUS_UNKNOWN_BEGINING2,
                                    cmd.getSQLOrig().substring(0, 10) + "...")); //$NON-NLS-1$
                            this.getScriptRunnerContext().addUnknown();

                        }
                    } else {

                        if (justANumber(sql)) {
                            runGoToLine(sql);
                        } else {
                            report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                                    ScriptRunnerDbArb.getString(ScriptRunnerDbArb.UNKNOWN_COMMAND),
                                    this.getScriptRunnerContext()) + "\n"); //$NON-NLS-1$
                            this.getScriptRunnerContext().addUnknown();

                            cmd.setFail();
                            if (cmd.getSQLOrig().contains("\u200B")) { //$NON-NLS-1$
                                report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.ORIGINAL_TEXT_CONTAINED,
                                        "Unicode u200B")); //$NON-NLS-1$
                            }
                        }

                    }
                    cmd.setProperty(EXECUTEFAILED, EXECUTEFAILED);
                }
            } catch (ArgumentQuoteException aqe) {
                report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                        ScriptRunnerDbArb.format(ScriptRunnerDbArb.QUOTEMISMATCH, aqe.getMessage()),
                        this.getScriptRunnerContext()));
                doWhenever(false);
            }
            if (this.getScriptRunnerContext().isMaxUnknown()) {
            	this.getScriptRunnerContext().write(Messages.getString("SQLPLUS.3")); //$NON-NLS-1$
            	getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), Messages.getString("SQLPLUS.3"), cmd.getSql());
            	this.getScriptRunnerContext().resetUnknown();
            }
        
            break;
        case G_S_EXECUTE:
            runExecute();
            break;
        case G_S_VARIABLE:
            runVariable();
            break;
        case G_S_PRINT:
            runPrint();
            break;
        case G_S_DOC_PLUS:
        case G_S_PROMPT:
            runPrompt();
            break;
        case G_S_START:
        case G_S_AT:
        case G_S_ATNESTED:
            runExecuteFile();
            break;
        case G_S_EXIT:
        case G_S_QUIT:
            runQuit();
            break;
        case G_S_CONNECT:
            runConnect();
            break;
        case G_S_WHENEVER:
            runWhenever();
            break;
        case G_S_DEFINE:
            runDefine();
            break;
        case G_S_UNDEFINE:
            runUndefine();
            break;
        case G_S_ACCEPT:
            // runAccept();
            break;
        case G_S_COLUMN:
            runColumn();
            break;
        case G_S_PASSWORD:
            runPassword();
            break;
        case G_S_COPY:
            runCopy();
            break;
        case G_S_SET_CONSTRAINT:
        case G_S_SET_ROLE:
            // case G_S_SET_TRANSACTION:
            // runSetPlus();
            break;
        case G_S_DISCONNECT:
            runDisconnect();
            break;
        case G_S_COMMENT_PLUS:
            break;
        case G_S_SLASH:
            runBuffer();
            break;
        case G_S_RUN:
            if (getScriptRunnerContext().getProperty(ScriptRunnerContext.JLINE_SETTING) != null) {
                getScriptRunnerContext().write(getScriptRunnerContext().getSQLPlusBuffer().getBufferSafe().list(false));
            }
            runBuffer();
            break;
        case G_S_ROLLBACK_PLUS:

            try {
                conn.rollback();
                report(getScriptRunnerContext(), "Rollback complete.");//$NON-NLS-1$
            } catch (SQLException e) {
                e.printStackTrace();
            }
        default:
            ;
            break;
        }
    }

    private boolean justANumber(String sql) {
        Pattern pattern = Pattern.compile("^(-?\\d+.*)"); //$NON-NLS-1$
        if (pattern.matcher(sql).matches())
            return true;
        return false;
    }

    private void runBuffer() {
        if ((conn != null) && (!isConnectionClosed(conn))) {
            if (getScriptRunnerContext().getSQLPlusBuffer() != null) {
                IBuffer buf = getScriptRunnerContext().getSQLPlusBuffer().getBufferSafe();
                if (buf.getBufferString().trim().length() > 0) {
                    String toRun = ""; //$NON-NLS-1$
                    if (buf.getLine(buf.size()).trim().equals(".")) { //$NON-NLS-1$
                        toRun = buf.getBuffer().trim();
                        toRun = toRun.replace(toRun.substring(toRun.length() - 1), ""); //$NON-NLS-1$
                    } else {
                        toRun = buf.getBuffer();
                    }
                    _runner = new ScriptExecutor(toRun, conn);
                    _runner.setScriptRunnerContext(getScriptRunnerContext());
                    Boolean oldInSlash = (Boolean) (getScriptRunnerContext()
                            .getProperty(ScriptRunnerContext.ISSLASHSTATEMENT));
                    if (oldInSlash == null) {
                        oldInSlash = Boolean.FALSE;
                    }
                    try {
                        // need to know I am in slash as I might not want to be
                        // echoed.
                        getScriptRunnerContext().putProperty(ScriptRunnerContext.ISSLASHSTATEMENT, Boolean.TRUE);
                        _runner.run();
                    } finally {
                        getScriptRunnerContext().putProperty(ScriptRunnerContext.ISSLASHSTATEMENT, oldInSlash);
                    }
                } else {
                    getScriptRunnerContext().write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.SQLPLUS_BUFFER_EMPTY));
                    try {
                        getScriptRunnerContext().getOutputStream().flush();
                    } catch (IOException e) {
                    }
                }
            }
        }
    }

    public static void disconnectMessage(Connection conn, ScriptRunnerContext src, BufferedOutputStream out) {
        if ((conn != null) && (!isConnectionClosed(conn))) {
            String localOVersion = src.doPromptReplaceSqlplusVar(
                    ScriptRunnerContext.SqlplusVariable._O_VERSION.toString(),
                    ScriptRunnerContext.SqlplusVariable._O_VERSION);
            // if !jline
            // close conn if transient connection
            // if jline RealBase = null if realbase = currentconn
            // close current conn
            if (!(src.getProperty(ScriptRunnerContext.SILENT) != null)
                    ^ (src.getProperty(ScriptRunnerContext.HIDDENOPTIONX) != null)) {
                src.write("\n" + ScriptRunnerDbArb.format(ScriptRunnerDbArb.DISCONNECTED_FROM, localOVersion) + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                try {
                    src.getOutputStream().flush();
                } catch (IOException e) {
                    /* eatme */}
            }
        }
    }

    private void runDisconnect() {
        if ((conn != null) && (!isConnectionClosed(conn))) {
            if ((getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH) == null)
                    || (((Boolean) (getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH)))
                            .equals(Boolean.FALSE))) {
                disconnectMessage(conn, getScriptRunnerContext(), out);
                commitConnection(conn);
            }
            configureOldConnections(conn);
        }
        conn = null;// say conn = null is no log to save on .isclosed
        // locking
        getScriptRunnerContext().getMap().put(ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER.toString(), ""); //$NON-NLS-1$
        getScriptRunnerContext().getMap().put(ScriptRunnerContext.SqlplusVariable._USER.toString(), ""); //$NON-NLS-1$
        getScriptRunnerContext().setCurrentConnection(conn);
        getScriptRunnerContext().putProperty(ScriptRunnerContext.NOLOG, true);
    }

    /**
     * SQLPLUS copy command eg: copy from totierne/totierne@XE create
     * xx123blobclob (N,V,extra,myblob,myclob) using select N,V, '123'
     * extra,myblob,myclob from fredzzblobclob;
     */
    @SuppressWarnings("deprecation")
    private void runCopy() {
        cmd.setSql(removeDashNewline(cmd, cmd.getSql()));
        // COPY {FROM database | TO database | FROM database TO database}
        // {APPEND|CREATE|INSERT|REPLACE} destination_table [(column, column,
        // column, ...)] USING query
        // can I regexp the start. eat it in chunks
        String mySQL = cmd.getSql().trim();
        String copyFrom = null;
        String copyTo = null;
        String toType = null;
        String destinationTable = null;
        String destColumns = null;
        String query = null;
        String stringArray[] = nextWordAndRest(mySQL);
        String errorString = null;
        boolean useByte = false;
        int maxSrcCols = 0;
        boolean copySucceeded = false;
        if (stringArray != null && (stringArray[0] != null) && (stringArray[0].equalsIgnoreCase("COPY")) //$NON-NLS-1$
                && ((stringArray.length == 2) && (stringArray[1] != null))) {
            // copy checked for.
            stringArray = nextWordAndRest(stringArray[1].trim());
            if ((stringArray != null) && (stringArray[0] != null) && (stringArray[0].equalsIgnoreCase("FROM")) //$NON-NLS-1$
                    && ((stringArray.length == 2) && (stringArray[1] != null))) {
                stringArray = nextWordAndRest(stringArray[1].trim());
                if (stringArray != null && (stringArray[0] != null)
                        && ((stringArray.length == 2) && (stringArray[1] != null))) {
                    copyFrom = stringArray[0];
                    stringArray = nextWordAndRest(stringArray[1].trim());
                } else {
                    errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);
                }
            }
            if (errorString == null && stringArray != null && (stringArray[0] != null)
                    && (stringArray[0].equalsIgnoreCase("TO")) //$NON-NLS-1$
                    && ((stringArray.length == 2) && (stringArray[1] != null))) {
                stringArray = nextWordAndRest(stringArray[1].trim());
                if (stringArray != null && (stringArray[0] != null)
                        && ((stringArray.length == 2) && (stringArray[1] != null))) {
                    copyTo = stringArray[0];
                    stringArray = nextWordAndRest(stringArray[1].trim());
                } else {
                    errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);
                }
            }
            if (errorString == null && stringArray != null && (stringArray[0] != null)
                    && (stringArray[0].equalsIgnoreCase("FROM")) //$NON-NLS-1$
                    && ((stringArray.length == 2) && (stringArray[1] != null))) {
                stringArray = nextWordAndRest(stringArray[1].trim());
                if (stringArray != null && (stringArray[0] != null)
                        && ((stringArray.length == 2) && (stringArray[1] != null))) {
                    copyFrom = stringArray[0];
                    stringArray = nextWordAndRest(stringArray[1].trim());
                } else {
                    errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);
                }
            }
            if (errorString == null && stringArray != null && (stringArray[0] != null)
                    && (stringArray[0].equalsIgnoreCase("TO")) //$NON-NLS-1$
                    && ((stringArray.length == 2) && (stringArray[1] != null))) {
                stringArray = nextWordAndRest(stringArray[1].trim());
                if (stringArray != null && (stringArray[0] != null)
                        && ((stringArray.length == 2) && (stringArray[1] != null))) {
                    copyTo = stringArray[0];
                    stringArray = nextWordAndRest(stringArray[1].trim());
                } else {
                    errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);
                }
            }
            if (copyFrom != null && copyFrom.toLowerCase().contains("as sysdba")) { //$NON-NLS-1$
                getScriptRunnerContext().write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_ERR_SYSDBA));
                return;
            }
            if (errorString == null && stringArray != null && (stringArray[0] != null) &&
                    // APPEND|CREATE|INSERT|REPLACE
                    ((stringArray[0].equalsIgnoreCase("APPEND")) || //$NON-NLS-1$
                            (stringArray[0].equalsIgnoreCase("APPEND_BYTE")) || //$NON-NLS-1$
                            (stringArray[0].equalsIgnoreCase("CREATE")) || //$NON-NLS-1$
                            (stringArray[0].equalsIgnoreCase("CREATE_BYTE")) || //$NON-NLS-1$
                            (stringArray[0].equalsIgnoreCase("INSERT")) || //$NON-NLS-1$
                            (stringArray[0].equalsIgnoreCase("REPLACE_BYTE")) || //$NON-NLS-1$
                            (stringArray[0].equalsIgnoreCase("REPLACE"))) //$NON-NLS-1$
                    && ((stringArray.length == 2) && (stringArray[1] != null))) {
                toType = stringArray[0].toUpperCase();
                if (toType.equals("CREATE_BYTE")) { //$NON-NLS-1$
                    useByte = true;
                    toType = "CREATE"; //$NON-NLS-1$
                }
                if (toType.equals("APPEND_BYTE")) { //$NON-NLS-1$
                    useByte = true;
                    toType = "APPEND"; //$NON-NLS-1$
                }
                if (toType.equals("REPLACE_BYTE")) { //$NON-NLS-1$
                    useByte = true;
                    toType = "REPLACE"; //$NON-NLS-1$
                }
            } else {
                errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);
            }
            if ((errorString == null) && (stringArray != null)) {
                // ensure ( not in a "" is breaks the string
                stringArray = nextWordAndRestOrString(stringArray[1].trim(), "("); //$NON-NLS-1$
            }
            if (errorString == null && stringArray != null && (stringArray[0] != null)
                    && ((stringArray.length == 2) && (stringArray[1] != null))) {
                destinationTable = stringArray[0];
                if ((destinationTable.length() > 2)
                        && (destinationTable.startsWith("\"") && destinationTable.endsWith("\""))) { //$NON-NLS-1$ //$NON-NLS-2$
                    // indouble quotes
                    destinationTable = destinationTable.substring(1, destinationTable.length() - 1);
                } else {
                    destinationTable = destinationTable.toUpperCase();
                }
            } else {
                errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);
            }
            if ((errorString == null) && (stringArray != null)) {
                String maybeBracketted = stringArray[1].trim();
                if (maybeBracketted.startsWith("(") && maybeBracketted.indexOf(")") != -1) { //$NON-NLS-1$ //$NON-NLS-2$
                    destColumns = maybeBracketted.substring(1, maybeBracketted.indexOf(")")); //$NON-NLS-1$
                    try {
                        maybeBracketted = maybeBracketted.substring(maybeBracketted.indexOf(")") + 1); //$NON-NLS-1$
                    } catch (Exception e) {
                        errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);// ran
                                                                                                        // off
                                                                                                        // the
                                                                                                        // edge
                                                                                                        // of
                                                                                                        // the
                                                                                                        // string
                    }
                    maybeBracketted = maybeBracketted.trim();
                }
                if ((errorString == null) && (stringArray != null))
                    stringArray = nextWordAndRest(maybeBracketted);
                if ((stringArray == null) || ((!((stringArray != null) && (stringArray.length > 0)
                        && (stringArray[0] != null) && (stringArray[0].equalsIgnoreCase("USING")))))) { //$NON-NLS-1$
                    // Bug 21624971 - GET WRONG ERROR WHEN COPY COMMAND MISSING
                    // USING KEYWORD
                    getScriptRunnerContext()
                            .write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_USING_NOT_DESCRIBED));
                    return;
                }
                if ((errorString == null) && stringArray != null && (stringArray[0] != null)
                        && (stringArray[0].equalsIgnoreCase("USING")) //$NON-NLS-1$
                        && ((stringArray.length == 2) && (stringArray[1] != null))) {
                    query = stringArray[1];
                    ArrayList<String> destArray = new ArrayList<String>();
                    if (destColumns != null) {
                        String[] interrum = destColumns.trim().split("\\s*,\\s*"); //$NON-NLS-1$
                        for (String acol : interrum) {
                            String thisCol = null;
                            thisCol = acol;
                            if ((thisCol.length() > 2) && (thisCol.startsWith("\"") && thisCol.endsWith("\""))) { //$NON-NLS-1$ //$NON-NLS-2$
                                // indouble quotes
                                thisCol = thisCol.substring(1, thisCol.length() - 1);
                            } else {
                                thisCol = thisCol.toUpperCase();
                            }
                            destArray.add(thisCol);
                        }
                    }
                    Connection[] fromToConnection = { null, null };
                    String[] fromToString = { copyFrom, copyTo };
                    for (int i = 0; i < 2; i++) {
                        if ((errorString == null) && fromToString[i] != null) {
                            boolean wasThirdParty = false;
                            /**
                             * remove outermost double quote if no double quotes
                             * contained
                             */
                            String toCon = fromToString[i];
                            String retVal = toCon;
                            if ((toCon != null)
                                    && (toCon.startsWith("\"") && toCon.endsWith("\"") && toCon.length() > 2)) { //$NON-NLS-1$ //$NON-NLS-2$
                                String candidate = toCon.substring(1, toCon.length() - 1);
                                int at = retVal.lastIndexOf("@"); //$NON-NLS-1$
                                int space = retVal.indexOf(" "); //$NON-NLS-1$
                                if ((candidate.indexOf("\"") != -1) || (at == -1 && space != -1) //$NON-NLS-1$
                                        || (at != -1 && space != -1 && space < at)) {
                                    // "copy connect string contains embedded
                                    // double quote or space, refusing to remove
                                    // outer double quote."); //$NON-NLS-1$
                                    // leave retVal unchanged
                                } else {
                                    retVal = candidate;
                                }
                            }
                            // edge cases not handled:
                            // 1/space in all quoted username/password
                            // - would have to be worked around in subsequent
                            // prompting calls as well.
                            // 2/seems like usage output needs a flag for
                            // whether it is connect or
                            // copy (or from first call from shell prompt)
                            ConnectionDetails cd = ScriptUtils.getConnectionDetails("connect " + retVal); //$NON-NLS-1$
                            fromToString[i] = retVal;// reporting does not
                                                        // include double
                                                        // quotes.
                            // catch it here - capture username/password@jdbc.
                            // []jdbc. and jdbc. do not accept third party to
                            // copy
                            // copy is oracle specific - if that is to be
                            // changed raise an ER - or use copy to
                            // oracle/bridge
                            // it could be generic - but that would raise
                            // testing/bug issues, for advanced types for
                            // example.
                            if (cd != null && cd.getCallUsage()) {
                                errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);
                            } else {
                                // Bug 21609843 - COPY COMMAND SHOULD FAIL
                                // IMMEDIATELY WHEN USERNAME IS NOT SPECIFIED
                                if (cd.getConnectName().equals("")) //$NON-NLS-1$
                                    errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);

                                if ((cd != null) && (cd.getConnectDB() != null) && (!(cd.getConnectDB().equals("")))) { //$NON-NLS-1$
                                    oracle.dbtools.db.ConnectionDetails cd3 = null;
                                    try {
                                        if (!hasUrlStub(cd.getConnectDB())) {
                                            cd3 = new oracle.dbtools.db.ConnectionDetails(cd.getConnectDB());
                                        }
                                    } catch (Exception e) {
                                        // do not want to fail badly if 3rd
                                        // party connection parsing fails
                                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
                                    }
                                    if (cd3 != null && cd3.getDriver() != null) {
                                        wasThirdParty = true;
                                    }
                                }
                                boolean cancelPressed = false;
                                Connection myconn = null;
                                ArrayList<String> urlMessage = new ArrayList<String>();
                                try {// make sure connectCallDialog connection
                                        // is always closed.
                                    if (!wasThirdParty) {
                                        if (cd != null) {
                                            cancelPressed = !connectCallDialog(cd); // returns
                                                                                    // false
                                                                                    // if
                                                                                    // canceled
                                                                                    // pressed.
                                                                                    // so
                                                                                    // need
                                                                                    // to
                                                                                    // negate
                                                                                    // it
                                        }
                                    }
                                    if (getScriptRunnerContext().getExited() != true) {
                                        if (cd != null && cancelPressed == false && !wasThirdParty) {
                                            if (cd.getConnection() == null) {
                                                myconn = getConnection(urlMessage, cd.getConnectName(),
                                                        cd.getConnectPassword(), cd.getConnectDB(), cd.getRole());
                                            } else {
                                                myconn = cd.getConnection();
                                                cd.setConnection(null);
                                            }
                                        }
                                    }
                                } finally {
                                    if (cd != null && cd.getConnection() != null) {
                                        try {
                                            cd.getConnection().close();
                                        } catch (SQLException e) {
                                            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                    e.getStackTrace()[0].toString(), e);
                                        }
                                    }
                                }
                                if (getScriptRunnerContext().getExited() != true) {
                                    if (cd != null && cancelPressed == false && !wasThirdParty) {
                                        if (myconn == null) {
                                            for (String outln : urlMessage) {
                                                if (getScriptRunnerContext().isSQLPlusClassic()) {
                                                    if (outln == null || (outln.equals(""))) { //$NON-NLS-1$
                                                        // first non ""
                                                        continue;
                                                    }
                                                }
                                                if (getScriptRunnerContext().getOutputStream() != null) {
                                                    getScriptRunnerContext().write(outln);
                                                } else {
                                                    System.out.print(outln);// sqlcli
                                                                            // not
                                                                            // initiated
                                                                            // yet
                                                                            // //authorized
                                                }
                                                if (getScriptRunnerContext().isSQLPlusClassic()) {
                                                    if (outln != null && (!outln.equals(""))) { //$NON-NLS-1$
                                                        // first non blank
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (myconn != null) {
                                        fromToConnection[i] = myconn;
                                    }
                                }

                                if (myconn == null) {
                                    if (fromToConnection[0] != null) {
                                        try {
                                            fromToConnection[0].close();
                                        } catch (SQLException e) {
                                            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                    e.getStackTrace()[0].toString(), e);
                                        }
                                    }
                                    errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_CONNECTION_FAILED);
                                }
                            }
                        }
                    }
                    // might have " removed
                    if (errorString == null) {
                        if (fromToString[0] != null) {
                            copyFrom = fromToString[0];
                        }
                        if (fromToString[1] != null) {
                            copyTo = fromToString[1];
                        }
                    }
                    if ((errorString == null) && (fromToConnection[0] == null) && (fromToConnection[1] == null)) {
                        errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_EITHER_FROM_OR_TO);
                    }
                    if (errorString == null) {
                        Connection fromConnection = fromToConnection[0];
                        Connection toConnection = fromToConnection[1];
                        // do not set property on null ie existing connection
                        Boolean getCommit = (Boolean) getScriptRunnerContext()
                                .getProperty(ScriptRunnerContext.CHECKBOXAUTOCOMMIT);
                        try {
                            if ((getCommit == null) || (getCommit.equals(Boolean.FALSE))) {
                                if (toConnection != null) {
                                    toConnection.setAutoCommit(false);
                                }
                                if (fromConnection != null) {
                                    fromConnection.setAutoCommit(false);
                                }
                            } else {
                                if (toConnection != null) {
                                    toConnection.setAutoCommit(true);
                                }
                                if (fromConnection != null) {
                                    fromConnection.setAutoCommit(true);
                                }
                            }
                        } catch (SQLException e) {
                            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
                                    e);
                        }
                        if (fromConnection == null) {
                            fromConnection = getScriptRunnerContext().getCurrentConnection();
                        }
                        if (toConnection == null) {
                            toConnection = getScriptRunnerContext().getCurrentConnection();
                        }
                        int copyCommit = 0;
                        String cc = (String) getScriptRunnerContext().getProperty(ScriptRunnerContext.COPYCOMMIT);
                        if (cc != null) {
                            try {
                                copyCommit = Integer.parseInt(cc);
                            } catch (NumberFormatException e) {
                                // ignore default to commit at end.
                                copyCommit = 0;
                            }
                        }
                        Integer fs = null;
                        if (getScriptRunnerContext().getProperty(ScriptRunnerContext.ARRAYSIZE) != null) {
                            fs = Integer.valueOf(
                                    (String) getScriptRunnerContext().getProperty(ScriptRunnerContext.ARRAYSIZE));
                        } else {
                            Properties props = getConnectionInfo();
                            try {
                                if (props != null) {
                                    fs = Integer.valueOf(props.getProperty(DBUtil.PREFERRED_FETCH_SIZE));
                                }
                            } catch (NumberFormatException e) {
                                fs = null;
                            }
                        }
                        String fsString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_DEFAULT);
                        if (fs != null) {
                            fsString = fs.toString();
                        }
                        Integer reportFs = fs;
                        if (fs == null) {
                            reportFs = 0;
                        }
                        // getScriptRunnerContext().write(ScriptRunnerDbArb.format(ScriptRunnerDbArb.COPY_NUMBER_OF_ROWS,
                        // new Integer(noOfRows).toString())+"\n");
                        // //$NON-NLS-1$
                        if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) != null
                                && Boolean.parseBoolean(getScriptRunnerContext()
                                        .getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
                            report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.COPY_ARRAY_CLASSIC,
                                    new Object[] { new Integer(reportFs).toString(), fsString }));
                        } else {
                            report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.COPY_ARRAY,
                                    new Object[] { new Integer(reportFs).toString(), fsString }));
                        }
                        if (copyCommit == 0) {
                            report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_COMMIT_WHEN_DONE));
                        } else {
                            report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.COPY_COMMIT,
                                    new Object[] { new Integer(copyCommit).toString() }));
                        }
                        getScriptRunnerContext();
                        // Bug 12551986
                        report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.COPY_LONG, new Object[] {
                                getScriptRunnerContext().getProperty(ScriptRunnerContext.SETLONG).toString() }));

                        /*
                         * APPEND
                         * 
                         * Inserts the rows from query into destination_table if
                         * the table exists. If destination_table does not
                         * exist, COPY creates it.
                         * 
                         * CREATE
                         * 
                         * Inserts the rows from query into destination_table
                         * after first creating the table. If destination_table
                         * already exists, COPY returns an error.
                         * 
                         * INSERT
                         * 
                         * Inserts the rows from query into destination_table.
                         * If destination_table does not exist, COPY returns an
                         * error. When using INSERT, the USING query must select
                         * one column for each column in destination_table.
                         * 
                         * REPLACE
                         * 
                         * Replaces destination_table and its contents with the
                         * rows from query. If destination_table does not exist,
                         * COPY creates it. Otherwise, COPY drops the existing
                         * table and replaces it with a table containing the
                         * copied data.
                         */

                        // need a create table
                        // test 1/does number of dest columns = real number of
                        // columns.
                        Statement s = null;
                        ResultSet rs = null;
                        PreparedStatement ps = null;
                        boolean amILocked = false;
                        boolean commitMade = false;
                        ArrayList<Blob> blobs = new ArrayList<Blob>();
                        ArrayList<Clob> clobs = new ArrayList<Clob>();
                        try {
                            if (fromToConnection[0] == null || fromToConnection[1] == null) {
                                if (getScriptRunnerContext().getCurrentConnection() != null) {
                                    amILocked = LockManager.lock(getScriptRunnerContext().getCurrentConnection());
                                }
                                // bug - sdcli had no BaseConnection - so lock
                                // current connection
                                if (amILocked == false) {
                                    errorString = ScriptRunnerDbArb
                                            .getString(ScriptRunnerDbArb.COPY_FAILED_TO_LOCK_CONNECTION);
                                }
                            }
                            boolean destTableNotExist = false;

                            ArrayList<String> toColName = new ArrayList<String>();
                            ArrayList<String> toColTypeName = new ArrayList<String>();
                            ArrayList<Integer> toColPrecision = new ArrayList<Integer>();
                            ArrayList<Integer> toColScale = new ArrayList<Integer>();
                            ArrayList<Integer> fromColTypeInt = new ArrayList<Integer>();
                            ArrayList<String> fromColName = new ArrayList<String>();
                            ArrayList<String> fromColTypeName = new ArrayList<String>();
                            ArrayList<Integer> fromColPrecision = new ArrayList<Integer>();
                            ArrayList<Integer> fromColScale = new ArrayList<Integer>();
                            ArrayList<Boolean> fromColNullable = new ArrayList<Boolean>();
                            StringBuilder createBuilder = new StringBuilder(
                                    "create table \"" + destinationTable + "\" ("); //$NON-NLS-1$ //$NON-NLS-2$
                            String createString = null;
                            try {
                                if (errorString == null) {
                                    commitIgnoreAutocommitError(toConnection);
                                    commitMade = true;
                                    s = fromConnection.createStatement();
                                    rs = s.executeQuery(query);

                                    ResultSetMetaData rsmd = rs.getMetaData();
                                    maxSrcCols = rsmd.getColumnCount();
                                    if ((maxSrcCols != destArray.size()) && (destArray.size() != 0)) {
                                        errorString = ScriptRunnerDbArb
                                                .getString(ScriptRunnerDbArb.COPY_NUMBER_OF_COLUMNS_MISMATCH);
                                    } else {
                                        for (int i = 1; i <= maxSrcCols; i++) {
                                            fromColTypeInt.add(rsmd.getColumnType(i));
                                            fromColName.add(rsmd.getColumnName(i));
                                            fromColTypeName.add(rsmd.getColumnTypeName(i));
                                            if (rsmd.isNullable(i) != DatabaseMetaData.columnNullable) {
                                                fromColNullable.add(Boolean.FALSE);
                                            } else { // nullable for do not
                                                        // know.
                                                fromColNullable.add(Boolean.TRUE);
                                            }
                                            Integer reportedPrecision = rsmd.getPrecision(i);
                                            // if precision =0 like it was from
                                            // a select null
                                            // fake it where necessary seems to
                                            // report as varchar
                                            // will fake it to length 200 for
                                            // varchar, nvarchar, char, nchar

                                            if ((reportedPrecision == 0) || (reportedPrecision == -1)) {
                                                // and type requires length
                                                if (fromColTypeName.get(i - 1).equalsIgnoreCase("VARCHAR2") || //$NON-NLS-1$
                                                        fromColTypeName.get(i - 1).equalsIgnoreCase("CHAR") || //$NON-NLS-1$
                                                        fromColTypeName.get(i - 1).equalsIgnoreCase("NVARCHAR2") || //$NON-NLS-1$
                                                        fromColTypeName.get(i - 1).equalsIgnoreCase("NCHAR") || //$NON-NLS-1$
                                                        fromColTypeName.get(i - 1).equalsIgnoreCase("RAW")) { //$NON-NLS-1$
                                                    // fake it to something
                                                    // sqlplus implies 1
                                                    reportedPrecision = 1;
                                                }
                                            }
                                            fromColPrecision.add(reportedPrecision);
                                            fromColScale.add(rsmd.getScale(i));
                                            // if precision = 0 fo not print it
                                            // if scale = 0 do not print it.
                                            String createColName = fromColName.get(i - 1);
                                            // ! if destarray.size()== 0 it
                                            // should be got from fromColName
                                            if (destArray.size() > 0) {
                                                createColName = destArray.get(i - 1);
                                            }

                                            // warp type for internal:
                                            // intervalds = interval
                                            // [(day_precision if not -1)] TO
                                            // SEcond [(scale if not -1?? check
                                            // what is the default 6 passed in)]
                                            // intervalym = interval
                                            // [(year_precision if not -1)] to
                                            // month
                                            // timestamp precision is expanded
                                            // to 6 - I am ok with that.
                                            if (fromColTypeName.get(i - 1).equalsIgnoreCase("INTERVALDS")) { //$NON-NLS-1$
                                                String precision = ""; //$NON-NLS-1$
                                                if (fromColPrecision.get(i - 1) > -1) {
                                                    precision = " (" + fromColPrecision.get(i - 1) + ")"; //$NON-NLS-1$ //$NON-NLS-2$
                                                }
                                                String scale = ""; //$NON-NLS-1$
                                                if (fromColScale.get(i - 1) > -1) {
                                                    scale = "(" + fromColScale.get(i - 1) + ") "; //$NON-NLS-1$ //$NON-NLS-2$
                                                }
                                                createBuilder.append("\"" + createColName + "\" " + "INTERVAL DAY" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                                        + precision + " TO SECOND " + scale); //$NON-NLS-1$
                                            } else if (fromColTypeName.get(i - 1).equalsIgnoreCase("INTERVALYM")) { //$NON-NLS-1$
                                                String precision = ""; //$NON-NLS-1$
                                                if (fromColPrecision.get(i - 1) > -1) {
                                                    precision = " (" + fromColPrecision.get(i - 1) + ")"; //$NON-NLS-1$ //$NON-NLS-2$
                                                }
                                                createBuilder.append("\"" + createColName + "\" " + "INTERVAL YEAR" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                                        + precision + " TO MONTH "); //$NON-NLS-1$
                                            } else if (fromColTypeName.get(i - 1).equalsIgnoreCase("BINARY_DOUBLE")) { //$NON-NLS-1$
                                                createBuilder.append("\"" + createColName + "\" " + "BINARY_DOUBLE"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                            } else {
                                                createBuilder.append("\"" + createColName + "\" "); //$NON-NLS-1$ //$NON-NLS-2$
                                                if (fromColPrecision.get(i - 1) != 0
                                                        && fromColPrecision.get(i - 1) != -1) {
                                                    if (fromColTypeName.get(i - 1).equalsIgnoreCase("NUMBER") //$NON-NLS-1$
                                                            && fromColPrecision.get(i - 1) == 126) {
                                                        if (fromColTypeInt.get(i - 1) == 2) {
                                                            fromColTypeName.set(i - 1, "FLOAT"); //$NON-NLS-1$
                                                        } else {
                                                            // Code for binary
                                                            // float is 21
                                                            fromColTypeName.set(i - 1, "BINARY_FLOAT"); //$NON-NLS-1$
                                                        }
                                                        createBuilder.append(fromColTypeName.get(i - 1));
                                                    } else
                                                        // blob clob report
                                                        // precision as -1 long
                                                        // reports as 2Gig
                                                        if (!(fromColTypeName.get(i - 1).toUpperCase()
                                                                .startsWith("LONG"))) { //$NON-NLS-1$
                                                        createBuilder.append(fromColTypeName.get(i - 1));
                                                        createBuilder.append("(" + fromColPrecision.get(i - 1)); //$NON-NLS-1$
                                                        if (fromColTypeName.get(i - 1).equalsIgnoreCase("VARCHAR2") || //$NON-NLS-1$
                                                                fromColTypeName.get(i - 1).equalsIgnoreCase("CHAR")) { //$NON-NLS-1$
                                                            if (useByte) {
                                                                createBuilder.append(" BYTE"); //$NON-NLS-1$
                                                            } else {
                                                                createBuilder.append(" CHAR"); //$NON-NLS-1$
                                                            }
                                                        }
                                                        if (fromColScale.get(i - 1) != 0) {
                                                            // numeric scale can
                                                            // be negative -
                                                            // defaults to 0
                                                            createBuilder.append(", " + fromColScale.get(i - 1)); //$NON-NLS-1$
                                                        }
                                                        createBuilder.append(") "); //$NON-NLS-1$
                                                    } else {
                                                        createBuilder.append(fromColTypeName.get(i - 1));
                                                    }
                                                } else {
                                                    createBuilder.append(fromColTypeName.get(i - 1));
                                                }
                                            }
                                            if (fromColNullable.equals(Boolean.FALSE)) {
                                                createBuilder.append(" NOT NULL, "); //$NON-NLS-1$
                                            } else {
                                                createBuilder.append(", "); //$NON-NLS-1$
                                            }
                                            // colname typename(precision,scale)
                                            // not null if
                                            // nullable.equalse(Boolean.FALSE)
                                        }
                                        createString = createBuilder.substring(0, createBuilder.lastIndexOf(",")) + ")"; //$NON-NLS-1$ //$NON-NLS-2$
                                    }
                                }
                            } catch (SQLException e) {
                                errorString = e.getLocalizedMessage();
                            } finally {
                                if (rs != null) {
                                    try {
                                        rs.close();
                                    } catch (SQLException e) {
                                        Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                e.getStackTrace()[0].toString(), e);
                                    }
                                }
                                if (s != null) {
                                    try {
                                        s.close();
                                    } catch (SQLException e) {
                                        Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                e.getStackTrace()[0].toString(), e);
                                    }
                                }
                            }
                            if ((errorString == null) && (destArray.size() == 0)) {
                                // create destarray list from from select. This
                                // is not to be uppercased or dedouble quoted.
                                for (int i = 1; i <= maxSrcCols; i++) {
                                    destArray.add(fromColName.get(i - 1));
                                }
                            }
                            Statement s2 = null;
                            ResultSet rs2 = null;
                            try {
                                if (errorString == null) {
                                    s2 = toConnection.createStatement();
                                    rs2 = s2.executeQuery("select * from \"" + destinationTable + "\" where 1=2"); //$NON-NLS-1$ //$NON-NLS-2$
                                    ResultSetMetaData rsmd = rs2.getMetaData();
                                    int maxToCols = rsmd.getColumnCount();
                                    // if ((maxToCols != destArray.size()) &&
                                    // (destArray.size() != 0)) {
                                    if (maxSrcCols < maxToCols)
                                        errorString = ScriptRunnerDbArb
                                                .getString(ScriptRunnerDbArb.COPY_NUMBER_OF_SRC_COLUMNS_LESS_THAN_DEST);
                                    else if (maxToCols < maxSrcCols) {
                                        errorString = ScriptRunnerDbArb
                                                .getString(ScriptRunnerDbArb.COPY_NUMBER_OF_SRC_COLUMNS_MORE_THAN_DEST);
                                    }
                                    // errorString =
                                    // ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_NUMBER_OF_COLUMNS_MISMATCH);
                                    // }
                                }
                            } catch (SQLException e) {
                                destTableNotExist = true;
                                if (toType.equals("INSERT")) { //$NON-NLS-1$
                                    errorString = ScriptRunnerDbArb
                                            .getString(ScriptRunnerDbArb.COPY_DESTINATION_DOES_NOT_EXIST);
                                }
                            } finally {
                                if (rs2 != null) {
                                    try {
                                        rs2.close();
                                    } catch (SQLException e) {
                                        Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                e.getStackTrace()[0].toString(), e);
                                    }
                                }
                                if (s2 != null) {
                                    try {
                                        s2.close();
                                    } catch (SQLException e) {
                                        Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                e.getStackTrace()[0].toString(), e);
                                    }
                                }
                            }
                            if ((errorString == null) && (destTableNotExist == false) && (toType.equals("CREATE"))) { //$NON-NLS-1$
                                errorString = ScriptRunnerDbArb
                                        .getString(ScriptRunnerDbArb.COPY_DESTINATION_DOES_EXIST);
                            }
                            String reportCommand = ""; //$NON-NLS-1$
                            try {
                                if ((errorString == null)
                                        && (((toType.equals("REPLACE")) && (destTableNotExist == false)))) { //$NON-NLS-1$
                                    // if table exists drop it
                                    reportCommand = ("drop table \"" + destinationTable + "\""); //$NON-NLS-1$ //$NON-NLS-2$
                                    s = toConnection.createStatement();
                                    s.execute(reportCommand);
                                    s.close();
                                }
                                if ((errorString == null) && (toType.equals("APPEND") && (destTableNotExist == true)) //$NON-NLS-1$
                                        || (toType.equals("REPLACE")) //$NON-NLS-1$
                                        || (toType.equals("CREATE") && (destTableNotExist == true))) { //$NON-NLS-1$
                                    // create it
                                    s = toConnection.createStatement();
                                    reportCommand = createString;
                                    s.execute(reportCommand);
                                    report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.COPY_TABLE_CREATED,
                                            destinationTable));

                                }
                            } catch (SQLException e) {
                                destTableNotExist = true;
                                errorString = ScriptRunnerDbArb.format(ScriptRunnerDbArb.COPY_CREATE_ERROR,
                                        reportCommand, e.getLocalizedMessage(),
                                        ScriptRunnerDbArb.COPY_DESTINATION_WAS_NOT_CREATED);
                            } finally {
                                if (rs != null) {
                                    try {
                                        rs.close();
                                    } catch (SQLException e) {
                                        Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                e.getStackTrace()[0].toString(), e);
                                    }
                                }
                                if (s != null) {
                                    try {
                                        s.close();
                                    } catch (SQLException e) {
                                        Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                e.getStackTrace()[0].toString(), e);
                                    }
                                }
                            }
                            if (errorString == null) {
                                try {
                                    int maxCols = 0;
                                    int longCol = 0;
                                    StringBuilder allCols = new StringBuilder(""); //$NON-NLS-1$
                                    StringBuilder getAll = new StringBuilder(""); //$NON-NLS-1$
                                    String allColsString = ""; //$NON-NLS-1$
                                    String getAllString = ""; //$NON-NLS-1$

                                    try {
                                        s = toConnection.createStatement();
                                        if (destArray.size() == 0) {
                                            rs = s.executeQuery(
                                                    "select * from \"" + destinationTable + "\"  where 1=2"); //$NON-NLS-1$ //$NON-NLS-2$
                                        } else {
                                            int i = 0;
                                            for (String theCol : destArray) {
                                                i++;
                                                // dest array is already de
                                                // double quoted or got from
                                                // result set metadata
                                                allCols.append("\"" + theCol + "\"" + ", "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                                getAll.append(":" + i + ", "); //$NON-NLS-1$ //$NON-NLS-2$
                                            }
                                            allColsString = allCols.substring(0, allCols.lastIndexOf(",")); //$NON-NLS-1$
                                            getAllString = getAll.substring(0, getAll.lastIndexOf(",")); //$NON-NLS-1$
                                            rs = s.executeQuery("select " + allColsString + " from \"" //$NON-NLS-1$ //$NON-NLS-2$
                                                    + destinationTable + "\" where 1 = 2"); //$NON-NLS-1$
                                        }

                                        ResultSetMetaData rsmd = rs.getMetaData();
                                        maxCols = rsmd.getColumnCount();
                                        if (((maxCols != destArray.size()) && (destArray.size() != 0))) {
                                            errorString = ScriptRunnerDbArb
                                                    .getString(ScriptRunnerDbArb.COPY_NUMBER_OF_COLUMNS_MISMATCH);
                                        } else {

                                            for (int i = 1; i <= maxCols; i++) {
                                                toColName.add(rsmd.getColumnName(i));
                                                toColTypeName.add(rsmd.getColumnTypeName(i));
                                                toColPrecision.add(rsmd.getPrecision(i));
                                                toColScale.add(rsmd.getScale(i));
                                                if (destArray.size() == 0) {
                                                    getAll.append(":" + i + ", "); //$NON-NLS-1$ //$NON-NLS-2$
                                                }
                                            }
                                            if ((destArray.size() == 0) && (getAll.length() != 0)) {
                                                getAllString = getAll.substring(0, getAll.lastIndexOf(",")); //$NON-NLS-1$
                                            }
                                        }
                                    } catch (SQLException e) {
                                        destTableNotExist = true;
                                        // need dest table does not contain
                                        // columns depending on error
                                        // ORA-06553: PLS-306: wrong number or
                                        // types of arguments in call to 'V' -
                                        // there was a function V not sure if I
                                        // should code for it -> change testcase
                                        // to avoid.
                                        if (e.getErrorCode() == 904) {
                                            // SQL Error: ORA-00904: "VVV":
                                            // invalid identifier
                                            // ORA-06553: PLS-306: wrong number
                                            // or types of arguments in call to
                                            // 'V'
                                            errorString = ScriptRunnerDbArb
                                                    .getString(ScriptRunnerDbArb.COPY_COL_NAME_MISMATCH);
                                        } else {
                                            errorString = ScriptRunnerDbArb
                                                    .getString(ScriptRunnerDbArb.COPY_DESTINATION_DOES_NOT_EXIST);
                                        }
                                    } finally {
                                        if (rs != null) {
                                            try {
                                                rs.close();
                                            } catch (SQLException e) {
                                                Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                        e.getStackTrace()[0].toString(), e);
                                            }
                                        }
                                        if (s != null) {
                                            try {
                                                s.close();
                                            } catch (SQLException e) {
                                                Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                        e.getStackTrace()[0].toString(), e);
                                            }
                                        }
                                    }
                                    if (errorString == null) {
                                        s = fromConnection.createStatement();
                                        rs = s.executeQuery(query);
                                        // rs.setFetchSize(100);

                                        if (fs != null) {
                                            rs.setFetchSize(fs);
                                        }
                                        ResultSetMetaData rsmd = rs.getMetaData();
                                        maxCols = rsmd.getColumnCount();
                                        if ((maxCols != destArray.size()) && (destArray.size() != 0)) {
                                            errorString = ScriptRunnerDbArb
                                                    .getString(ScriptRunnerDbArb.COPY_NUMBER_OF_COLUMNS_MISMATCH);
                                        } else {
                                            /**
                                             * copy does not support nchar
                                             * nvarchar or nclob so error out on
                                             * these
                                             */
                                            for (int i = 1; i <= maxCols; i++) { // we
                                                                                    // have
                                                                                    // the
                                                                                    // type
                                                                                    // data
                                                                                    // already
                                                /*
                                                 * NVARCHAR restriction removed
                                                 * set form of use later
                                                 */
                                                if ((fromColTypeName.get(i - 1).toUpperCase().equals("LONG")) || //$NON-NLS-1$
                                                        (fromColTypeName.get(i - 1).toUpperCase().equals("LONG RAW")) || //$NON-NLS-1$
                                                        (fromColTypeName.get(i - 1).toUpperCase().equals("BLOB")) || //$NON-NLS-1$
                                                        (fromColTypeName.get(i - 1).toUpperCase().equals("NCLOB")) || //$NON-NLS-1$
                                                        (fromColTypeName.get(i - 1).toUpperCase().equals("CLOB"))) { //$NON-NLS-1$
                                                    longCol++;
                                                }
                                            }

                                            /*
                                             * sqlplus copy does not check size
                                             * just continues until error for
                                             * (int i = 1; i <= maxCols; i++) {
                                             * // we have the type data already
                                             * captured above boolean
                                             * bothStrings =
                                             * ((fromColTypeName.get(i-1).
                                             * toUpperCase().equals("VARCHAR2")|
                                             * |fromColTypeName.get(i-1).
                                             * toUpperCase().equals("CHAR")) &&
                                             * (toColTypeName.get(i-1).
                                             * toUpperCase().equals("VARCHAR2")|
                                             * |toColTypeName.get(i-1).
                                             * toUpperCase().equals("CHAR")));
                                             * if (!((fromColTypeName.get(i -
                                             * 1).equalsIgnoreCase(toColTypeName
                                             * .get(i - 1)))||bothStrings)) {
                                             * errorString = ("error"); } else {
                                             * if ((fromColPrecision.get(i -
                                             * 1)>(toColPrecision.get(i - 1))))
                                             * { errorString = ("error"); } else
                                             * { if ((fromColScale.get(i -
                                             * 1)>(toColScale.get(i - 1)))) {
                                             * errorString = ("error"); } } } }
                                             */
                                            /*
                                             * destarray.size()==0 - from = to
                                             * else destarray = to (will match
                                             * if create - if not create ok to
                                             * error out as db not changed yet
                                             */
                                            if ((destArray.size() == 0) && (errorString == null)
                                                    && (fromColName.size() > 0 && toColName.size() > 0)) {
                                                boolean problem = false;
                                                // no named column - source =
                                                // dest
                                                for (int i = 1; i <= maxCols; i++) {
                                                    // already from the database
                                                    // so normalised not double
                                                    // quoted
                                                    if (!(fromColName.get(i - 1).equals(toColName.get(i - 1)))) {
                                                        problem = true;
                                                        break;
                                                    }
                                                }
                                                if (problem) {
                                                    errorString = ScriptRunnerDbArb
                                                            .getString(ScriptRunnerDbArb.COPY_COL_NAME_MISMATCH);
                                                }
                                            }

                                            // insert into desttable (n1,n2,n3)
                                            // values (:1,:2,:3)
                                            if (errorString == null) {
                                                String myInsert = ""; //$NON-NLS-1$
                                                if (allColsString.equals("")) { //$NON-NLS-1$
                                                    myInsert = "insert into \"" + destinationTable + "\" values (" //$NON-NLS-1$ //$NON-NLS-2$
                                                            + getAllString + ")"; //$NON-NLS-1$
                                                } else {
                                                    myInsert = "insert into \"" + destinationTable + "\" (" //$NON-NLS-1$ //$NON-NLS-2$
                                                            + allColsString + ") values (" + getAllString + ")"; //$NON-NLS-1$ //$NON-NLS-2$
                                                }
                                                ps = toConnection.prepareStatement(myInsert);
                                                for (int i = 1; i <= maxCols; i++) {
                                                    // fixed this to use from
                                                    // result so do not go off
                                                    // the array.
                                                    // seems like it is
                                                    // reporting nvarchar2 as
                                                    // varchar - and int as
                                                    // Types.VARCHAR = 12.
                                                    if ((fromColTypeName.get(i - 1).toLowerCase().equals("nvarchar2") || //$NON-NLS-1$
                                                            fromColTypeName.get(i - 1).toLowerCase().equals("nvarchar") //$NON-NLS-1$
                                                            || fromColTypeName.get(i - 1).toLowerCase().equals("nchar") //$NON-NLS-1$
                                                            || fromColTypeName.get(i - 1).toLowerCase()
                                                                    .equals("nclob"))) { //$NON-NLS-1$
                                                        oracle.jdbc.OraclePreparedStatement pstmt = (oracle.jdbc.OraclePreparedStatement) ps;
                                                        pstmt.setFormOfUse(i,
                                                                oracle.jdbc.OraclePreparedStatement.FORM_NCHAR);
                                                    }
                                                }
                                                int noOfRows = 0;
                                                int noOfRowsSinceCommit = 0;
                                                int noOfCommittedRows = 0;
                                                int noOfInsertedRows = 0;
                                                int myfs = 200;
                                                if (fs != null) {
                                                    myfs = fs;
                                                }
                                                if ((longCol > 0) && (myfs > (50 / longCol))) {
                                                    myfs = 50 / longCol;
                                                    // dont batch up too much if
                                                    // long, long raw, blob or
                                                    // clob
                                                    if (myfs == 0) {// over 50
                                                                    // long&blobs
                                                                    // do not
                                                                    // want a
                                                                    // divide by
                                                                    // 0 later.
                                                        myfs = 1;
                                                    }
                                                } else {
                                                    if (myfs > 200) {
                                                        // dont batch up too
                                                        // much
                                                        myfs = 200;
                                                    }
                                                }
                                                int arrayBinds = 0;

                                                while (rs.next()) {
                                                    for (int i = 1; i <= maxCols; i++) {
                                                        Object gotObject = rs.getObject(i);
                                                        if (rs.wasNull()) {
                                                            ps.setNull(i, fromColTypeInt.get(i - 1));
                                                        } else {
                                                            int BUFFERSIZE = 2048;
                                                            Object o = gotObject;
                                                            if (o instanceof BigInteger) {
                                                                oracle.sql.NUMBER oNUM = new oracle.sql.NUMBER(
                                                                        (BigInteger) o);
                                                                ((OraclePreparedStatement) ps).setNUMBER(i, oNUM);
                                                            } else if (o instanceof Blob) {
                                                                Blob b = null;
                                                                BufferedInputStream bis = null;
                                                                BufferedOutputStream bos = null;
                                                                InputStream inp = null;
                                                                OutputStream os = null;
                                                                try {
                                                                    b = BLOB.createTemporary(toConnection, false,
                                                                            BLOB.DURATION_SESSION);
                                                                    blobs.add(b);
                                                                    ((BLOB) b).open(BLOB.MODE_READWRITE);
                                                                    inp = ((Blob) o).getBinaryStream();
                                                                    bis = new BufferedInputStream(inp);
                                                                    byte[] buffer = new byte[BUFFERSIZE];
                                                                    os = b.setBinaryStream(1L);
                                                                    int lastRead = 0;
                                                                    bos = new BufferedOutputStream(os);
                                                                    while ((lastRead = bis.read(buffer, 0,
                                                                            buffer.length)) != -1) {
                                                                        bos.write(buffer, 0, lastRead);
                                                                    }
                                                                    bos.flush();
                                                                } catch (IOException io) {
                                                                    throw new SQLException(io);
                                                                } finally {
                                                                    if (inp != null) {
                                                                        try {
                                                                            inp.close();
                                                                        } catch (IOException io) {
                                                                        }
                                                                    }
                                                                    if (os != null) {
                                                                        try {
                                                                            os.close();
                                                                        } catch (IOException io) {
                                                                        }
                                                                    }
                                                                    if (bis != null) {
                                                                        try {
                                                                            bis.close();
                                                                        } catch (IOException io) {
                                                                        }
                                                                    }
                                                                    if (bos != null) {
                                                                        try {
                                                                            bos.close();
                                                                        } catch (IOException io) {
                                                                        }
                                                                    }
                                                                }
                                                                // SQLException
                                                                // is passed out
                                                                ps.setObject(i, b);
                                                            } else if (o instanceof Clob) {
                                                                Clob c = null;
                                                                BufferedReader binp = null;
                                                                BufferedWriter bwr = null;
                                                                Reader inp = null;
                                                                Writer wr = null;
                                                                try {
                                                                    if (fromColTypeName.get(i - 1).toUpperCase()
                                                                            .equals("NCLOB")) { //$NON-NLS-1$
                                                                        c = CLOB.createTemporary(toConnection, false,
                                                                                CLOB.DURATION_SESSION,
                                                                                oracle.jdbc.OraclePreparedStatement.FORM_NCHAR);
                                                                    } else {
                                                                        c = CLOB.createTemporary(toConnection, false,
                                                                                CLOB.DURATION_SESSION);
                                                                    }
                                                                    clobs.add(c);
                                                                    ((CLOB) c).open(CLOB.MODE_READWRITE);
                                                                    inp = ((Clob) o).getCharacterStream();
                                                                    binp = new BufferedReader(inp);
                                                                    char[] myc = new char[BUFFERSIZE];
                                                                    wr = c.setCharacterStream(1L);
                                                                    int lastRead = 0;
                                                                    bwr = new BufferedWriter(wr);
                                                                    while ((lastRead = binp.read(myc, 0,
                                                                            myc.length)) != -1) {
                                                                        bwr.write(myc, 0, lastRead);
                                                                    }
                                                                    bwr.flush();
                                                                } catch (IOException io) {
                                                                    throw new SQLException(io);
                                                                } finally {
                                                                    if (inp != null) {
                                                                        try {
                                                                            inp.close();
                                                                        } catch (IOException io) {
                                                                        }
                                                                    }
                                                                    if (wr != null) {
                                                                        try {
                                                                            wr.close();
                                                                        } catch (IOException io) {
                                                                        }
                                                                    }
                                                                    if (binp != null) {
                                                                        try {
                                                                            binp.close();
                                                                        } catch (IOException io) {
                                                                        }
                                                                    }
                                                                    if (bwr != null) {
                                                                        try {
                                                                            bwr.close();
                                                                        } catch (IOException io) {
                                                                        }
                                                                    }
                                                                }
                                                                ps.setObject(i, c);
                                                            } else {
                                                                ps.setObject(i, gotObject);
                                                            }
                                                        }
                                                    }
                                                    noOfRows++;
                                                    noOfRowsSinceCommit++;
                                                    ps.addBatch();
                                                    if (myfs > 0) {
                                                        if ((noOfRows % myfs) == 0) {
                                                            ps.executeBatch();
                                                            arrayBinds++;
                                                            noOfInsertedRows = noOfRows;
                                                            for (Blob b : blobs) {
                                                                try {
                                                                    ((BLOB) b).freeTemporary();
                                                                } catch (SQLException e) {
                                                                }
                                                            }
                                                            blobs = new ArrayList<Blob>();
                                                            for (Clob c : clobs) {
                                                                try {
                                                                    ((CLOB) c).freeTemporary();
                                                                } catch (SQLException e) {
                                                                }
                                                            }
                                                            clobs = new ArrayList<Clob>();
                                                        }
                                                    }
                                                    if (copyCommit != 0) {
                                                        if ((arrayBinds % copyCommit) == 0) {
                                                            ps.executeBatch();
                                                            commitIgnoreAutocommitError(toConnection);
                                                            noOfCommittedRows += noOfRowsSinceCommit;
                                                            noOfRowsSinceCommit = 0;
                                                            this.freelobs(blobs, clobs);
                                                        }
                                                    }
                                                }
                                                ps.executeBatch();
                                                noOfInsertedRows = noOfRows;
                                                commitIgnoreAutocommitError(toConnection);
                                                noOfCommittedRows += noOfRowsSinceCommit;
                                                copySucceeded = true;
                                                String reportTo = ""; //$NON-NLS-1$
                                                this.freelobs(blobs, clobs);
                                                if (copyTo == null) {
                                                    reportTo = ScriptRunnerDbArb
                                                            .getString(ScriptRunnerDbArb.COPY_DEFAULT_HOST);
                                                } else {// use copy to with
                                                        // password removed
                                                    // use stanard command so it
                                                    // is broken in one place...
                                                    ConnectionDetails cd = ScriptUtils
                                                            .getConnectionDetails("connect " + copyTo); //$NON-NLS-1$
                                                    String unknown = ScriptRunnerDbArb
                                                            .get(ScriptRunnerDbArb.COPY_UNKNOWN);
                                                    String name = cd.getConnectName();
                                                    String db = cd.getConnectDB();
                                                    if ((name == null) || (name.equals(""))) { //$NON-NLS-1$
                                                        name = unknown;
                                                    }
                                                    reportTo = name;
                                                    if ((db != null) && (!(db.equals("")))) { //$NON-NLS-1$
                                                        reportTo += "@" + db; //$NON-NLS-1$
                                                    }
                                                }
                                                String reportFrom = ""; //$NON-NLS-1$
                                                if (copyFrom == null) {
                                                    reportFrom = ScriptRunnerDbArb
                                                            .getString(ScriptRunnerDbArb.COPY_DEFAULT_HOST);
                                                } else {// use copy to with
                                                        // password removed
                                                    // use stanard command so it
                                                    // is broken in one place...
                                                    ConnectionDetails cd = ScriptUtils
                                                            .getConnectionDetails("connect " + copyFrom); //$NON-NLS-1$
                                                    String unknown = ScriptRunnerDbArb
                                                            .get(ScriptRunnerDbArb.COPY_UNKNOWN);
                                                    String name = cd.getConnectName();
                                                    String db = cd.getConnectDB();
                                                    if ((name == null) || (name.equals(""))) { //$NON-NLS-1$
                                                        name = unknown;
                                                    }
                                                    reportFrom = name;
                                                    if ((db != null) && (!(db.equals("")))) { //$NON-NLS-1$
                                                        reportFrom += "@" + db; //$NON-NLS-1$
                                                    }
                                                }

                                                String in4Spaces = "    "; //$NON-NLS-1$
                                                report(getScriptRunnerContext(), in4Spaces + ScriptRunnerDbArb.format(
                                                        ScriptRunnerDbArb.COPY_ROWS_SELECTED,
                                                        new Object[] { new Integer(noOfRows).toString(), reportFrom }));
                                                report(getScriptRunnerContext(), in4Spaces
                                                        + ScriptRunnerDbArb
                                                                .format(ScriptRunnerDbArb.COPY_ROWS_INSERTED,
                                                                        new Object[] {
                                                                                new Integer(noOfInsertedRows)
                                                                                        .toString(),
                                                                        destinationTable }));
                                                report(getScriptRunnerContext(), in4Spaces
                                                        + ScriptRunnerDbArb
                                                                .format(ScriptRunnerDbArb.COPY_ROWS_COMMITED_TWO_ARG,
                                                                        new Object[] {
                                                                                new Integer(noOfCommittedRows)
                                                                                        .toString(),
                                                                                destinationTable, reportTo }));
                                                /**
                                                 * } typical copy output: Array
                                                 * fetch/bind size is 15.
                                                 * (arraysize is 15) Will commit
                                                 * after every 15 array binds.
                                                 * (copycommit is 15) Maximum
                                                 * long size is 80. (long is 80)
                                                 * Table DEST created. 2 rows
                                                 * selected from DEFAULT HOST
                                                 * connection. 2 rows inserted
                                                 * into DEST. 2 rows committed
                                                 * into DEST at totierne@XE.
                                                 */
                                            }
                                        }
                                    }
                                } catch (SQLException e) {
                                    errorString = e.getLocalizedMessage();
                                }
                            }
                        } finally {
                            try {
                                this.freelobs(blobs, clobs);
                                if (s != null)
                                    try {
                                        s.close();
                                    } catch (Exception e) {
                                    }
                                if (ps != null) {
                                    try {
                                        ps.close();
                                    } catch (SQLException e) {
                                        Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                e.getStackTrace()[0].toString(), e);
                                    }
                                }
                                try {
                                    if ((errorString != null) && (commitMade)) {
                                        toConnection.rollback();
                                    }
                                } catch (SQLException e) {
                                    // already in an error condition
                                    Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                            e.getStackTrace()[0].toString(), e);
                                }
                                if (fromToConnection[0] != null) {
                                    try {
                                        fromToConnection[0].close();
                                    } catch (SQLException e) {
                                        Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                e.getStackTrace()[0].toString(), e);
                                    }
                                }
                                if (fromToConnection[1] != null) {
                                    try {
                                        fromToConnection[1].close();
                                    } catch (SQLException e) {
                                        Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                e.getStackTrace()[0].toString(), e);
                                    }
                                }
                            } finally {
                                // amilocked is true if we get here at all.[put
                                // the check in anyway]
                                if (amILocked) {
                                    try {
                                        LockManager.unlock(getScriptRunnerContext().getCurrentConnection());
                                    } catch (Exception e) {
                                        Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                                e.getStackTrace()[0].toString(), e);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if ((errorString == null) && (stringArray == null)) {
            errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);
        }
        if ((errorString == null) && (copySucceeded == false)) {
            errorString = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.COPY_SYNTAX_ERROR);
        }
        if (errorString != null) {
            report(getScriptRunnerContext(), errorString);
            doWhenever(true);
        }
        // dowhenever??? error checking last...
    }

    private void commitIgnoreAutocommitError(Connection conn) throws SQLException {
        if (!conn.getAutoCommit()) {
            conn.commit();
        }
    }

    @SuppressWarnings("deprecation")
    private void freelobs(ArrayList<Blob> blobs, ArrayList<Clob> clobs) {
        for (Blob b : blobs) {
            try {
                ((oracle.sql.BLOB) b).freeTemporary();
            } catch (SQLException e) {
            }
        }
        blobs.clear();
        for (Clob c : clobs) {
            try {
                ((oracle.sql.CLOB) c).freeTemporary();
            } catch (SQLException e) {
            }
        }
        clobs.clear();
    }

    private void runSetPlus() {
        // need to handle set define on set scan on, set define off set scan off
        // set define c where c is a character
        //
        // SQL> set define x
        // SP2-0272: define character cannot be alphanumeric or whitespace
        // SQL> set define '!'
        // SQL> set define "!"
        // SQL> SET DEFINE "!"
        // if recognised, removeDashNewline already done by compound set
        cmd.setSql(cmd.getSql().toLowerCase());
        String[] retVal = nextWordAndRest(cmd.getSql());
        if (retVal != null) {
            if (cmd.getStmtSubType().equals(StmtSubType.G_S_SET_UNKNOWN)) {
                // report(ScriptRunnerDbArb.format(ScriptRunnerDbArb.SQLPLUS_COMMAND_SKIPPED,
                // new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig()));
                StringTokenizer st = new StringTokenizer(cmd.getSql());

                // bug 19659881. Initially the condition was '>2'. Now it is
                // '>=2' to include the next token (such as 'set repheader') to
                // test its validity.
                if (st.countTokens() >= 2) {
                    st.nextToken();
                    String token = st.nextToken();
                    if (token.length() < 10)
                        report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.SQLPLUS_UNKNOWN_SET_COMMAND, token));
                    else {
                        report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.SQLPLUS_UNKNOWN_SET_COMMAND_BEGINNING,
                                token.substring(0, 10) + "...")); //$NON-NLS-1$
                    }
                    cmd.setFail();
                }
            } else if (cmd.getStmtSubType().equals(StmtSubType.G_S_SET_OBSOLETE)) {
                report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.SQLPLUS_COMMAND_OBSOLETE,
                        new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig()));
            }
            // else if ((retVal !=
            // null)&&(!(cmd.getStmtSubType().equals(StmtSubType.G_S_SET_UNKNOWN))))
            // { //check subtype as it could be a busted define verify etc
            else { // check subtype as it could be a busted define verify etc
                retVal = nextWordAndRest(retVal[1]);
                if (retVal != null) {
                    String command = retVal[0];
                    if (((retVal[0].equals("def")) || (retVal[0].equals("defi")) || (retVal[0].equals("defin")) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                            || (retVal[0].equals("define")) || (retVal[0].equals("verify")) || (retVal[0].equals("ver")) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                            || (retVal[0].equals("scan")))) { //$NON-NLS-1$
                        retVal = nextWordAndRest(retVal[1]);
                        if (retVal != null) {
                            String setChar = retVal[0];
                            // retVal[1] can now be <a_value> '<a_value>'
                            // "<a_value>" on off
                            // a_value cannot be alphanumeric or whitespace
                            boolean quoted = false;
                            String originalSetChar = setChar;
                            if ((setChar.startsWith("\'")) && (setChar.endsWith("\'")) && (setChar.length() == 3)) { //$NON-NLS-1$ //$NON-NLS-2$
                                setChar = setChar.substring(1, 2);
                                quoted = true;
                            }
                            if ((setChar.startsWith("\"")) && (setChar.endsWith("\"")) && (setChar.length() == 3)) { //$NON-NLS-1$ //$NON-NLS-2$
                                setChar = setChar.substring(1, 2);
                                quoted = true;
                            }
                            if (setChar.equals("on")) { //$NON-NLS-1$
                                if (command.startsWith("def")) { //$NON-NLS-1$
                                    getScriptRunnerContext().setSubstitutionOn(true);
                                    getScriptRunnerContext().setSubstitutionChar('&');
                                    return;
                                } else {
                                    getScriptRunnerContext().setScanOn(true);
                                    return;
                                }
                            } else if (setChar.equals("off")) { //$NON-NLS-1$
                                if (command.startsWith("def")) { //$NON-NLS-1$
                                    getScriptRunnerContext().setSubstitutionOn(false);
                                    return;
                                } else {
                                    getScriptRunnerContext().setScanOn(false);
                                    return;
                                }
                            } else if ((setChar.length() == 1) && (!(Character.isWhitespace(setChar.charAt(0))))
                                    && (!(Character.isLetterOrDigit(setChar.charAt(0)))) && (!(command.equals("scan"))) //$NON-NLS-1$
                                    && (!((setChar.charAt(0) == '"') && (quoted == false)))
                                    && (!((setChar.charAt(0) == '\'') && (quoted == false)))
                                    && (!(originalSetChar.equals("\"\"\""))) && (!(originalSetChar.equals("'''")))) { //$NON-NLS-1$ //$NON-NLS-2$
                                getScriptRunnerContext().setSubstitutionOn(true);
                                getScriptRunnerContext().setSubstitutionChar(setChar.charAt(0));
                                return;
                            }
                            String errorReport = ScriptRunnerDbArb.format(ScriptRunnerDbArb.INVALID_NAME_COMMAND,
                                    "set " + command); //$NON-NLS-1$
                            report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                                    errorReport, this.getScriptRunnerContext()));
                            return;
                        }
                    }
                }
            }
        }
        // report(ScriptRunnerDbArb.format(ScriptRunnerDbArb.SQLPLUS_COMMAND_SKIPPED,
        // new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig()));
    }

    private void runColumn() {
        // column new val handled in Column.sql as a 'listener' command, if it
        // reaches here, erroring out:
        // report(ScriptRunnerDbArb.format(ScriptRunnerDbArb.SQLPLUS_COMMAND_SKIPPED_ONLY_COLUMN_NEW_VAL,
        // new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig()));
        // cmd.setProperty(EXECUTEFAILED,EXECUTEFAILED);
    }

    private void runUndefine() {
        // eat define
        cmd.setSql(removeDashNewline(cmd, cmd.getSql()));
        String[] stringArray = nextWordAndRest(cmd.getSql());
        if (stringArray[1].trim().equals("")) { //$NON-NLS-1$
            // noop
        } else {
            // for each word, uppercase and undefine
            String theRest = stringArray[1].trim();
            Map<String, String> m = getScriptRunnerContext().getMap();
            while ((!(theRest.trim().equals("")))) { //$NON-NLS-1$
                stringArray = nextWordAndRest(theRest);
                String localVarName = stringArray[0].toUpperCase();
                theRest = stringArray[1];
                m.remove(localVarName);
            }
        }
    }

    private void runDefine() {
        // eat define
        String[] stringArray = nextWordAndRest(cmd.getSql());
        if (stringArray[1].trim().equals("")) { //$NON-NLS-1$
            // print all defines
            for (ScriptRunnerContext.SqlplusVariable valueVar : ScriptRunnerContext.SqlplusVariable.values()) {
                String value = getScriptRunnerContext().doPromptReplaceSqlplusVar(valueVar.toString(), valueVar);
                String nextKey = valueVar.toString();
                // Bug 26625284 - VARIABLES SHOWED BY DEFINE COMMAND SHOULD ALWAYS BE OF TYPE CHAR 
                //boolean isDigit = value.equals("") ? false : Pattern.matches("^\\d*$", value); //$NON-NLS-1$ //$NON-NLS-2$
                //if (isDigit) {
                //    report(getScriptRunnerContext(), "DEFINE " + nextKey + " =  " + value + " (NUMBER)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                //} else {
                    report(getScriptRunnerContext(), "DEFINE " + nextKey + " =  \"" + value + "\"" + " (CHAR)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                //}
            }
            Map<String, String> m = getScriptRunnerContext().getMap();
            Set<String> s = m.keySet();
            synchronized (m) {
                Iterator<String> i = s.iterator(); // Must be in synchronized
                                                    // block
                while (i.hasNext()) {
                    String nextKey = i.next();
                    boolean printedAlready = true;
                    try {
                        ScriptRunnerContext.SqlplusVariable.valueOf(nextKey.toUpperCase());
                    } catch (IllegalArgumentException iae) {
                        printedAlready = false;
                    }
                    if (!printedAlready) {
                        String value = m.get(nextKey);
                        boolean isDigit = value.equals("") ? false : Pattern.matches("^\\d*$", value); //$NON-NLS-1$ //$NON-NLS-2$
                        // binary float either e+- or dot
                        boolean isBF1 = value.equals("") ? false //$NON-NLS-1$
                                : Pattern.matches("^[-+]?[0-9]*\\.?[0-9]+([eE][-+]?)([0-9]+)?$|^[+-][iI][nN][fF]$", //$NON-NLS-1$
                                        value);
                        boolean isBF2 = value.equals("") ? false //$NON-NLS-1$
                                : Pattern.matches("^[-+]?[0-9]*\\.[0-9]+([eE][-+]?[0-9]+)?$|^[+-][iI][nN][fF]$", value); //$NON-NLS-1$
                        if (isBF1 || isBF2) {
                            report(getScriptRunnerContext(), "DEFINE " + nextKey + " =  " + value + " (BINARY_FLOAT)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                        } else if (isDigit) {
                            report(getScriptRunnerContext(), "DEFINE " + nextKey + " =  " + value + " (NUMBER)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                        } else {
                            report(getScriptRunnerContext(), "DEFINE " + nextKey + " =  \"" + value + "\"" + " (CHAR)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                        }
                    }
                }
            }
        } else {
            String localSubName = ""; //$NON-NLS-1$
            if (stringArray[1].indexOf("=") == -1) { //$NON-NLS-1$
                // print define of stringArray[1].trim().toUpperCase()
                Map<String, String> m = getScriptRunnerContext().getMap();
                String nextKey = stringArray[1].trim().toUpperCase();
                String value = m.get(nextKey);
                
                // Bug 25030438 getting the correct data type for the nextKey.
                Map<String, String> svt = getScriptRunnerContext().getSubVarTypeMap();
                String dataType = svt.get(nextKey);
                
                // _Privilege is special as "NOTSYSDBA" needs to be converted to
                // ""
                // _DATE is dynamic
                if ((value == null) || (nextKey.equals(SqlplusVariable._PRIVILEGE.toString()))
                        || (nextKey.equals(SqlplusVariable._DATE.toString()))) { // might
                                                                                    // be
                                                                                    // an
                                                                                    // unset
                                                                                    // yet,
                                                                                    // sqlplus
                                                                                    // variable
                    // print all defines
                    boolean found = true;
                    SqlplusVariable valueVar = null;
                    try {
                        valueVar = ScriptRunnerContext.SqlplusVariable.valueOf(nextKey);
                    } catch (IllegalArgumentException iae) {
                        found = false;
                    }
                    if (found) {
                        value = getScriptRunnerContext().doPromptReplaceSqlplusVar(nextKey.toString(), valueVar);
                    }
                }
                if (value == null) {
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNDEFINED_SYMBOL,
                            nextKey.toLowerCase(Locale.US)));
                } else {
                    int maxvarlen = 15;
                    int keyLen = nextKey.length();
                    int pad = 0;
                    StringBuffer spacePad = new StringBuffer();
                    if (keyLen < maxvarlen) {
                        pad = maxvarlen - keyLen;
                        for (int j = 0; j < pad; j++)
                            spacePad.append(" "); //$NON-NLS-1$
                    } else {
                        spacePad.append(""); //$NON-NLS-1$
                    }
                    boolean forceChar=false;
                    for (ScriptRunnerContext.SqlplusVariable valueVar : ScriptRunnerContext.SqlplusVariable.values()) {
                    		if (valueVar.equals(nextKey.toUpperCase())) {
                    			forceChar=true;
                    			break;
                    		}
                    }
                    // Bug 25030438 Making sure valid data type from  the select statement is reported.
                    if ((forceChar)||(nextKey.equals(value) && (nextKey.equals(SqlplusVariable._PRIVILEGE.toString())
                            || nextKey.equals(SqlplusVariable._USER.toString())))) {
                           report(getScriptRunnerContext(), "DEFINE " + nextKey + spacePad.toString() + " = \"" + "\"" + " (CHAR)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                    } else {
                    	if(dataType != null && dataType.length() > 0) {
                    		// Bug 26625454
                    	    if(dataType.equalsIgnoreCase("CHAR") || dataType.equalsIgnoreCase("VARCHAR") || dataType.equalsIgnoreCase("VARCHAR2")) //$NON-NLS-1$ //$NON-NLS-2$
                    	    	report(getScriptRunnerContext(), "DEFINE " + nextKey + spacePad.toString() + " = \"" + value + "\"" + " (" + dataType +  ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
                    	    else
                    		   report(getScriptRunnerContext(), "DEFINE " + nextKey + spacePad.toString() + " = " + value + " (" + dataType +  ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                    	}
                    	else
                    	   report(getScriptRunnerContext(), "DEFINE " + nextKey + spacePad.toString() + " = \"" + value + "\"" + " (CHAR)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                    }
                }
            } else {
                localSubName = stringArray[1].substring(0, stringArray[1].indexOf("=")).trim().toUpperCase(); //$NON-NLS-1$
                String setValue = ""; //$NON-NLS-1$
                boolean errorOut = false;
                if ((stringArray[1].indexOf("=")) != (stringArray[1].length() - 1)) { //$NON-NLS-1$
                    setValue = stringArray[1].substring(stringArray[1].indexOf("=") + 1).trim(); //$NON-NLS-1$
                    if (setValue.startsWith("\"")) { //$NON-NLS-1$
                        setValue = getDefineValue('"', setValue); // $NON-NLS-1$
                    } else if (setValue.startsWith("'")) { //$NON-NLS-1$
                        setValue = getDefineValue('\'', setValue); // $NON-NLS-1$
                    }
                    // set the define
                    if (setValue != null) {
                        Map<String, String> map = getScriptRunnerContext().getMap();
                        map.put(localSubName, setValue);
                    } else {
                        errorOut = true;
                    }
                }
                if (errorOut) {
                    String errorReport = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DEFINE_REQUIRES_VALUE);
                    if (setValue == null) {
                        errorReport = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DEFINE_INVALID);
                    }
                    report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                            errorReport, this.getScriptRunnerContext()));
                }
            }
        }
    }

    /**
     * return error message as array[1] on error - at least return {"",null}
     * otherwise.
     */
    String getDefineValue(Character specialCharacter, String containsSpecial) {
        int lastEscaped = -1;
        String retString = null;
        String extra = containsSpecial + " ";// so we can run off //$NON-NLS-1$
                                                // the end
        // Character escapedBy = specialCharacter;
        StringBuilder retVal = new StringBuilder(""); //$NON-NLS-1$
        char last = ' ';
        char current = ' ';
        char next = ' ';
        for (int i = 1; i < containsSpecial.length(); i++) {
            last = current;
            current = containsSpecial.charAt(i);
            next = extra.charAt(i + 1);
            if (!(specialCharacter.equals(current))) {
                retVal.append(current);
                if (i == (containsSpecial.length() - 1)) {
                    // we have reached the end without ending the quote
                    retVal = null;
                }
                continue;
            } else // either its a " the start of a "" or the end of a ""
                if ((!(specialCharacter.equals(last)) || (lastEscaped == i - 1)) && (specialCharacter.equals(current))
                        && !(specialCharacter.equals(next))) {
                break;
            } else if (((!(specialCharacter.equals(last))) || (lastEscaped == i - 1))
                    && (specialCharacter.equals(current)) && (specialCharacter.equals(next))) {
                lastEscaped = i + 1;
            } else
                if (((specialCharacter.equals(last))) && (specialCharacter.equals(current)) && (lastEscaped != i - 1)) {
                retVal.append(current);
                if (i == (containsSpecial.length() - 1)) {
                    // we have reached the end without ending the quote
                    retVal = null;
                }
            } else {
                // error? should not happen [force write error by setting output
                // to null]
                retVal = null;
                break;
            }
        }
        if (containsSpecial.length() == 1) {// ie just a ' or just a "
            retString = null;
        } else {
            if (retVal == null) {
                retString = null;
            } else {
                retString = retVal.toString();
            }
        }
        return retString;
    }

    private void runWhenever() {
        boolean showUsage = false;
        final String SUCCESS = "SUCCESS"; //$NON-NLS-1$
        final String FAILURE = "FAILURE"; //$NON-NLS-1$
        final String WARNING = "WARNING"; //$NON-NLS-1$
        // Strip comments functionality is required for create or replace. in
        // create or replace could simply stop at first ' or " to allow parsing
        // up to possible double quoted name.
        String removeComments = cmd.getSql().toUpperCase();
        // need to coalesee continuation character
        // this may be a cheaper 'weaker' one (there are 2, one in core script
        // parser).
        String stripped = ScriptUtils.stripFirstN(removeComments, 9999,
                cmd.getProperty(SQLCommand.STRIPPED_CONTINUATION) == null, true, true);
        if (stripped == null) {
            showUsage = true;
        } else {
            removeComments = stripped;
        }
        boolean localMatchFound = false;
        boolean localSqlError = false;
        int localFlags = 0;
        if (!showUsage) {
            // parse first act later
            String cmdUpper = removeComments;// Do toUpperCase once
            if ((cmdUpper.length() > 1) && ((cmdUpper.endsWith(";") || (cmdUpper.endsWith("/"))))) { //$NON-NLS-1$ //$NON-NLS-2$
                cmdUpper = cmdUpper.substring(0, cmdUpper.length() - 1);
            }
            String[] stringArray = nextWordAndRest(cmdUpper);
            if (!showUsage && (stringArray != null) && (!(stringArray[1].equals("")))) { //$NON-NLS-1$
                String nextWord = stringArray[0];
                String theRest = stringArray[1];
                // next word is whenever
                stringArray = nextWordAndRest(theRest);
                if ((stringArray != null) && (!(stringArray[1].equals("")))) { //$NON-NLS-1$
                    nextWord = stringArray[0];
                    theRest = stringArray[1];
                    if ((nextWord.equals("SQLERROR")) || (nextWord.equals("OSERROR"))) { //$NON-NLS-1$ //$NON-NLS-2$
                        if ((nextWord.equals("SQLERROR"))) { //$NON-NLS-1$
                            localSqlError = true;
                        } else {
                            localSqlError = false;// OSERROR
                        }
                        stringArray = nextWordAndRest(theRest);
                        if (stringArray != null) {
                            nextWord = stringArray[0];// can be continue or exit
                            theRest = stringArray[1];// can end with commit
                                                        // rollback or none
                            if ((nextWord.equals("CONTINUE")) || (nextWord.equals("EXIT"))) { //$NON-NLS-1$ //$NON-NLS-2$
                                if (nextWord.equals("EXIT")) { //$NON-NLS-1$
                                    localFlags = ScriptUtils.EXIT;
                                } else {
                                    localFlags = 0;
                                }
                                if ((theRest == null) || (theRest.equals(""))) { //$NON-NLS-1$
                                    theRest = ""; //$NON-NLS-1$
                                } else {
                                    int value = 1;
                                    boolean wasSQLCODE=false;
                                    try {
                                        stringArray = nextWordAndRest(theRest);
                                        if (stringArray != null) {
                                            nextWord = stringArray[0];// can be
                                                                        // continue
                                                                        // or
                                                                        // exit
                                            if (nextWord.equals(SUCCESS) || nextWord.equals(FAILURE)
                                                    || nextWord.equals(WARNING)) {

                                                switch (nextWord) {
                                                case SUCCESS:
                                                    value = 0;
                                                    break;
                                                case WARNING:
                                                    value = 1;
                                                    break;
                                                case FAILURE:
                                                    value = 2;
                                                    break;
                                                default:
                                                    value = 1;
                                                }
                                            } else if (nextWord.equals("SQL.SQLCODE")){ //$NON-NLS-1$
                                               wasSQLCODE=true;
                                               value=2;
                                            }  else {
                                                value = Integer.parseInt(nextWord);
                                            }
                                            theRest = stringArray[1];// can end
                                                                        // with
                                                                        // commit
                                                                        // rollback
                                                                        // or
                                                                        // none
                                            if (theRest == null) {
                                                theRest = ""; //$NON-NLS-1$
                                            }
                                        }
                                    } catch (NumberFormatException e) {// was
                                                                        // not a
                                                                        // number
                                        value = 1;
                                    }
                                    getScriptRunnerContext().putProperty(ScriptRunnerContext.EXIT_INT_WHENEVER, value);
                                    getScriptRunnerContext().putProperty(ScriptRunnerContext.EXIT_INT_WHENEVER_WAS_SQLCODE, wasSQLCODE);
                                }
                                String action = "NONE"; //$NON-NLS-1$
                                String[] arrayWords = theRest.trim().split("\\s+");//$NON-NLS-1$
                                if (localFlags == ScriptUtils.EXIT) {
                                    action = "COMMIT"; //$NON-NLS-1$
                                }
                                if (arrayWords != null) {
                                    int count = 0;
                                    for (String word : arrayWords) {
                                        if (localFlags != ScriptUtils.EXIT) {
                                            count++;
                                            if (count > 1) {
                                                showUsage = true;
                                                break;
                                            }
                                            if (word.equals("NONE")) { //$NON-NLS-1$
                                                action = word;
                                                break;
                                            }
                                        } else {
                                            count++;
                                            if (count > 1) {
                                                showUsage = true;
                                                break;
                                            }
                                        }
                                        if (word.equals("COMMIT") || word.equals("ROLLBACK") || word.equals("NONE")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                            action = word;
                                            break;
                                        }
                                    }
                                }

                                if (action.equals("COMMIT")) { //$NON-NLS-1$
                                    localFlags = localFlags | ScriptUtils.ACTIONCOMMIT;
                                } else if (action.equals("ROLLBACK")) { //$NON-NLS-1$
                                    localFlags = localFlags | ScriptUtils.ACTIONROLLBACK;
                                } else {// do not commit or rollback on sqlerror
                                        // "NONE"
                                    localFlags = localFlags | ScriptUtils.ACTIONNONE;
                                }
                                // else can be none default, problem if bind
                                // variable ends with
                                // rollback or commit like :myrollback
                                // put in space tab newline or end of comment
                                // whitespace
                                localMatchFound = true;
                            }
                        }
                    }
                }
            }
        }
        if ((localMatchFound == true) && !showUsage) {// we have the values set
            if (localSqlError == true) {
                getScriptRunnerContext().setSqlError(localFlags);
            } else {
                getScriptRunnerContext().setOsError(localFlags);
            }
        } else {
            report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                    Messages.getString("WHENEVER"), this.getScriptRunnerContext())); //$NON-NLS-1$
        }
        /*
         * //WHENEVER SQLERROR // { // EXIT // [SUCCESS | FAILURE | WARNING | n
         * | variable | :BindVariable] // [COMMIT | ROLLBACK] // | CONTINUE
         * [COMMIT | ROLLBACK | NONE] // } if SQLERROR set a flag for action on
         * sqlerror and check and act on this action every sqlerror[sqlerror in
         * sql or plsql] //WHENEVER OSERROR // { // EXIT // [SUCCESS | FAILURE |
         * n | variable | :BindVariable] // [COMMIT | ROLLBACK] // | CONTINUE
         * [COMMIT | ROLLBACK | NONE] // } if OSError set a flag and action on
         * oserror what sort of flag, should I throw an exception to do this for
         * now ignore the SUCCESS | FAILURE | n | variable | :BindVariable the
         * os error is start @ @@
         */
    }

    private void runPassword() {
        Boolean haveILocked = false;
        Statement s = null;
        ResultSet rs = null;
        try {
            if (getScriptRunnerContext().getBaseConnection() == null) {
            	  if ((getScriptRunnerContext() != null)
                          && (getScriptRunnerContext().getProperty(ScriptRunnerContext.NOLOG) != null)
                          && (((Boolean) (getScriptRunnerContext().getProperty(ScriptRunnerContext.NOLOG))
                                  .equals(Boolean.TRUE)))) {
            		  report(getScriptRunnerContext(),Messages.getString("SQLPLUS.18")); //$NON-NLS-1$
            		  getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), Messages.getString("SQLPLUS.18"), cmd.getSql());
            		  report(getScriptRunnerContext(),Messages.getString("SQLPLUS.19")); //$NON-NLS-1$
            		  getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), Messages.getString("SQLPLUS.19"), cmd.getSql());
                  } else {
                	  report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.FAILED_TO_ALTER_USER));
                  }
                
                doWhenever(true);
            } else if (conn == null) {
                report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOT_CONNECTED));
                return;
            } else if (haveILocked = LockManager.lock(getScriptRunnerContext().getBaseConnection())) {
                String splitString = null;
                String toprocess = cmd.getSql().replaceAll("^\\s+", ""); //$NON-NLS-1$ //$NON-NLS-2$
                if (toprocess.split("\\s+").length > 1) { //$NON-NLS-1$
                    splitString = toprocess.replaceFirst("^[^\\s]+\\s*", "").trim(); //$NON-NLS-1$ //$NON-NLS-2$
                }
                s = conn.createStatement();
                rs = s.executeQuery("Select user from dual"); //$NON-NLS-1$
                rs.next();
                String currentUserid = rs.getString(1);
                rs.close();
                s.close();
                String alterUserId = null;
                if (splitString != null && !(splitString.equals(""))) { //$NON-NLS-1$
                    alterUserId = splitString;
                    if ((alterUserId.startsWith("\"") && (alterUserId.endsWith("\"")) && (alterUserId.length() > 2))) { //$NON-NLS-1$ //$NON-NLS-2$
                        alterUserId = alterUserId.substring(1, alterUserId.length() - 1);
                    } else {
                        alterUserId = alterUserId.toUpperCase();
                    }
                }
                if (alterUserId == null) {
                    alterUserId = currentUserid;
                }
                // if currentUSerID = alter User id, then ask for current
                // password.
                Pair<String, String> passwords = getScriptRunnerContext().getPasswordFieldsProvider()
                        .passwordGetPassword(getScriptRunnerContext(), currentUserid, alterUserId);
                if (passwords != null) {
                    // use the string
                    // ArrayList<Object> al = new ArrayList<Object>();
                    // al.add(newPassword);
                    s = conn.createStatement();
                    s.execute("alter user \"" + alterUserId + "\" identified by \"" + passwords.first() //$NON-NLS-1$ //$NON-NLS-2$
                            + "\" replace \"" + passwords.second() + "\""); //$NON-NLS-1$ //$NON-NLS-2$
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.PASSWORD_CHANGED));
                } else {
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.FAILED_TO_ALTER_USER));
                    doWhenever(true);
                }
            }
        } catch (SQLException e) {
            // report(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.FAILED_TO_ALTER_USER));
            // ^^^^ this is not informative, custom password function could
            // throw meaningful message why it failed (16850418)
            if (e.getErrorCode() != 65066) {
                report(getScriptRunnerContext(), e.getMessage());
            } else {
                report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.PDBPASSWORDERROR));
            }
            report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.FAILED_TO_ALTER_USER));
            doWhenever(true);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
            }
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                }
            }
            if (haveILocked) {
                LockManager.unlock(getScriptRunnerContext().getBaseConnection());
            }
        }
    }

    /**
     * very similar code for disconnect, connect or failed connections, existing
     * connections have to be wiped issues handled by logging an error lower
     * down. TODO pass the exceptions up so they can be try/catched in the
     * calling code.
     */
    private void configureOldConnections(Connection conn) {
        Object jline = getScriptRunnerContext().getProperty(ScriptRunnerContext.JLINE_SETTING);
        if (jline == null) {
            if (!(isConnectionClosed(conn))) {
                closeOldConnection(conn);
            }
            getScriptRunnerContext().setCloseConnection(true); // all future
            // connections
            // will be
            // transiant
        } else {
            Boolean doNullBase = ((getScriptRunnerContext().getBaseConnection() != null) && (conn != null)
                    && getScriptRunnerContext().getBaseConnection().equals(conn));
            getScriptRunnerContext().setCloseConnection(true);
            if (!(isConnectionClosed(conn))) {
                closeOldConnection(conn);// want to close in jline mode for
                // each connection
            }
            if (doNullBase) {
                getScriptRunnerContext().setBaseConnection(null);
            }
            getScriptRunnerContext().setCloseConnection(false); // do not
            // want to
            // close for
            // each
            // script
            // if command line
            // cleaned
            // up at end
        }
    }

    private void runConnect() {
        cmd.setSql(ScriptUtils.checkforContinuationChars(cmd.getSql()));
        String connectIdent = null;
        ArrayList<String> urlMessage = new ArrayList<String>();
        // connect scott/tiger@TNSNAMES_ORA_ENTRY //look up via anils code
        // connect scott@TNSNAMES_ORA_ENTRY //pop up Password dialog
        // OLD:connect scott //look up TWO_TASK, ORACLE_SID
        // OLD:connect //pop up user/password //look up TWO_TASK, ORACLE_SID
        // NEW:If database is not given use url from:
        // Connection con=getScriptRunnerContext().getBaseConnection()
        // oracle.jdbc.OracleConnection oc=
        // (oracle.jdbc.OracleConnection) con;
        // System.out.println("ZZ"+oc.getURL());
        // how do I handle the connection: this is in kris flux actually a new
        // connection for that thread etc qould be ok as long as it is taken
        // down
        // on exit quit error end.
        // TCPTNSEntry[] entries = TNSHelper.getTNSEntries(); gets TNSNAMES
        // ENTRIES does this take care of TNS_ADMIN
        // what about TWO_TASK being set to tns_entry string (with brackets)?
        // ignore for now.
        // TWO_TASK is called LOCAL in windows
        String localSql = cmd.getSql().trim();
        // next line returns null if sqlplus scott/tiger@<blank>
        ConnectionDetails cd = ScriptUtils.getConnectionDetails(localSql);
        // SQLPLUS.1=Usage: CONN[ECT] [{logon|/|proxy}
        // [AS\n{SYSDBA|SYSOPER|SYSASM|SYSBACKUP|SYSDG|SYSKM}]
        // SQLPLUS.INVALID_OPTION=SP2-0306: Invalid option.
        // SQLPLUS.LOGON_PROXY=where <logon> ::=
        // <username>[/<password>][@<connect_identifier>]\n\
        // \ <proxy> ::=
        // <proxyuser>[<username>][/<password>][@<connect_identifier>]

        if (cd != null && (cd.getCallUsage())) { // username/password with a
                                                    // space, and not double
                                                    // quoted - likely garbage
                                                    // in.
            Boolean inSQLCLLogin=(Boolean)getScriptRunnerContext().getProperty(ScriptRunnerContext.IN_INITIAL_SQLCLCONNECT);

            report(getScriptRunnerContext(), "SP2-0306: Invalid option."); //$NON-NLS-1$ 
            String toReport="";
            if ((inSQLCLLogin==null||inSQLCLLogin.equals(Boolean.FALSE))) {
                //connect from SQL> prompt error message
                //
                toReport=oracle.dbtools.raptor.newscriptrunner.Messages.getString("SQLPLUS.1")+"\n"  //$NON-NLS-1$  //$NON-NLS-2$
                +oracle.dbtools.raptor.newscriptrunner.Messages.getString("SQLPLUS.LOGON_PROXY");  //$NON-NLS-1$
                //"Usage: CONN[ECT] [{logon|/|proxy} [AS {SYSDBA|SYSOPER|SYSASM|SYSBACKUP|SYSDG|SYSKM|SYSRAC}] [edition=value]]\n"+
                //        "where <logon> ::= <username>[/<password>][@<connect_identifier>]\n"+
                //        "      <proxy> ::= <proxyuser>[<username>][/<password>][@<connect_identifier>]";
            }
            report(getScriptRunnerContext(), toReport); //$NON-NLS-1$
           // report(""); //$NON-NLS-1$ .
            // issues - could be from sql rather than connect command
            return;
        }
        Connection myconn = null;
        try {// make sure connectCallDialog connection is closed
            boolean cancelPressed = false;
            if (cd != null) {
                cancelPressed = !connectCallDialog(cd); // returns false if
                                                        // canceled pressed. so
                                                        // need to negate it
            }
            if (getScriptRunnerContext().getExited() != true) {
                if (cd != null && cancelPressed == false) {
                    if (cd.getConnection() != null) {
                        myconn = cd.getConnection();
                        cd.setConnection(null);
                    } else {
                        myconn = getConnection(urlMessage, cd.getConnectName(), cd.getConnectPassword(),
                                cd.getConnectDB(), cd.getRole());
                    }
                }
            }
        } finally {
            if (cd != null && cd.getConnection() != null) {
                try {
                    cd.getConnection().close();
                } catch (SQLException e) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
                }
            }

            if (myconn != null) {
            	String url = ((String) (getScriptRunnerContext().getProperty(ScriptRunnerContext.CLI_CONN_URL)));
                ScriptUtils.setHttpCon(myconn, getScriptRunnerContext(),((String) (getScriptRunnerContext().getProperty(ScriptRunnerContext.CLI_CONN_URL)))!=null&&
                        (url.startsWith("http:") || url.startsWith("https:"))); //$NON-NLS-1$ //$NON-NLS-2$ .
                if (cd.getEdition()!=null&&cd.getEdition().trim().length()>0) {
                	try {
						myconn.createStatement().execute(MessageFormat.format("alter session set edition={0}", cd.getEdition())); //$NON-NLS-1$
					} catch (SQLException e) {
						try {
							myconn.close();
							myconn = null;
						} catch (SQLException e2) {
							 Logger.getLogger(getClass().getName()).log(Level.WARNING, e2.getStackTrace()[0].toString(), e2);
						}
						getScriptRunnerContext().write(e.getLocalizedMessage()+"\n");  //$NON-NLS-1$
						//likely edition does not exist - nice if this was reported better see sqlcl set edition 
						//at least this errors and marks connection as failed..
	                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
					}
                }
            }
        }
        if (getScriptRunnerContext().getExited() != true) {
            if (myconn != null) {
                if (myconn instanceof OracleConnection) {
                    try {
                        int pref=0;
                        try {
                            pref=Integer.valueOf((String) getScriptRunnerContext().getProperty(ScriptRunnerContext.ARRAYSIZE));
                        } catch (Exception e) {
                        }
                        ((OracleConnection)myconn).setDefaultRowPrefetch(Math.max(pref, 50));
                    } catch (Exception e) {
                        Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
                    }
                	}
                connectIdent = cd.getConnectDB();
                if (connectIdent == null || connectIdent.equals("")) { //$NON-NLS-1$
                    if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
                        connectIdent = System.getenv("LOCAL"); //$NON-NLS-1$
                    } else {
                        connectIdent = System.getenv("TWO_TASK"); //$NON-NLS-1$
                    }
                }
                // we have a new connection, so commit the old connection and
                // close if its a transiant (scripted connection)
                if (conn != null) {
                    if ((conn instanceof OracleConnection) && (getScriptRunnerContext().getBaseConnection() != null)
                            && (conn.equals(getScriptRunnerContext().getBaseConnection()))
                            && (getScriptRunnerContext().getMap()
                                    .get(ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER.toString()) != "")) { //$NON-NLS-1$
                        getScriptRunnerContext().putProperty(ScriptRunnerContext.BASECONNECTIONID,
                                getScriptRunnerContext().doPromptReplaceSqlplusVar(
                                        ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER.toString(),
                                        ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER));
                    }

                }
                if (conn != null) {// from command line we may not have a
                                    // connection yet.
                    commitConnection(conn);
                    configureOldConnections(conn);
                    getScriptRunnerContext().setCurrentConnection(myconn);
                } else {
                    if (getScriptRunnerContext().getBaseConnection() == null) {
                        getScriptRunnerContext().setBaseConnection(myconn);
                    }
                    getScriptRunnerContext().setCurrentConnection(myconn);
                }
                // role options - null or "" - unknown (jdev connection
                // NOTSYSDBA - as xxx was not there report this in
                // scriptrunnercontext as ""
                // as xxx trimmed and to uppered - if set at all 99% will be as
                // sysdba
                String cleanRole = cd.getRole();
                if (cleanRole == null) {
                    cleanRole = ScriptRunnerContext.NOTSYSDBA;
                }
                cleanRole = cleanRole.trim().toUpperCase();
                if (cleanRole.equals("")) { //$NON-NLS-1$
                    cleanRole = ScriptRunnerContext.NOTSYSDBA;
                }
                getScriptRunnerContext().getMap().put(ScriptRunnerContext.SqlplusVariable._PRIVILEGE.toString(),
                        cleanRole);
                // _CONNECT_IDENTIFIER set - on demand if null, on connect, (or
                // by the user typically in login.sql) Note we use a select to
                // get info not strictly connection identifier.
                if (connectIdent != null && connectIdent.length() > 1 && connectIdent.length() < 81) {
                    getScriptRunnerContext().getMap()
                            .put(ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER.toString(), connectIdent);
                } else {
                    getScriptRunnerContext().getMap()
                            .put(ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER.toString(), ""); //$NON-NLS-1$
                    // want to set the new id (before it is possibly reset in
                    // login .sql)
                    getScriptRunnerContext().doPromptReplaceSqlplusVar(myconn,
                            ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER.toString(),
                            ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER);
                }
                conn = myconn;

                // REPORT CONNECTED HERE.
                if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CMDLINE_LOGIN) != null) {
                    if ((getScriptRunnerContext().getProperty(ScriptRunnerContext.HIDDENOPTIONX) == null)) {
                        if ((getScriptRunnerContext().getProperty(ScriptRunnerContext.SILENT) == null)) {
                            if ((getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH) == null)
                                    || (getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH)
                                            .equals(Boolean.FALSE))) {
                                if (getScriptRunnerContext().getProperty(ScriptRunnerContext.LASTLOGINTIME) == null) {
                                    String lastLogin = DBUtil.getInstance(getConn()).executeReturnOneCol(
                                            "select INITCAP(TO_CHAR(last_login ,'DY MON DD YYYY HH24:MI:SS TZH:TZM')) from dba_users where username=USER"); //$NON-NLS-1$
                                    if (lastLogin != null)
                                        report(getScriptRunnerContext(), MessageFormat.format("Last Successful login time: {0}\n", //$NON-NLS-1$
                                                new Object[] { lastLogin })); // $NON-NLS-2$
                                }
                                String x = getScriptRunnerContext().doPromptReplaceSqlplusVar(
                                        ScriptRunnerContext.SqlplusVariable._O_VERSION.toString(),
                                        ScriptRunnerContext.SqlplusVariable._O_VERSION);
                                report(getScriptRunnerContext(), MessageFormat.format(Messages.getString("SQLPLUS.6"), new Object[] { x }) //$NON-NLS-1$
                                        + "\n"); //$NON-NLS-1$
                            }
                        }
                    }
                    getScriptRunnerContext().getProperties().remove(ScriptRunnerContext.SQLPLUS_CMDLINE_LOGIN);
                } else if ((getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH) != null)
                        && (getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH)
                                .equals(Boolean.FALSE))) {
                    if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SILENT) == null)
                        report(getScriptRunnerContext(), MessageFormat.format("{0}.", //$NON-NLS-1$
                                new Object[] { ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CONNECTED) }));
                }
                if ((getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH) != null)
                        && (getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH).equals(Boolean.TRUE))
                        && (getScriptRunnerContext().getProperty(ScriptRunnerContext.SILENT) == null)) {
                    if (getScriptRunnerContext().isSQLPlusClassic()) {
                        report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CONNECTIDLECLASSIC));
                    } else {
                        report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CONNECTIDLE));
                    }
                    // "Connected to an Idle instance");
                }
                Boolean getCommit = (Boolean) getScriptRunnerContext()
                        .getProperty(ScriptRunnerContext.CHECKBOXAUTOCOMMIT);

                try {
                    if ((getCommit == null) || (getCommit.equals(Boolean.FALSE))) {
                        conn.setAutoCommit(false);
                    } else {
                        conn.setAutoCommit(true);
                    }
                } catch (SQLException e) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
                } // i.e. autocommit is usually off for connect.
                    // set echo off for login/glogin - user has to specifically
                    // set it if they want to debug these files
                    // not enough to set echo fale

                Boolean oldEcho = getScriptRunnerContext().isEchoOn();
                try {
                    forceEcho(false, false);
                    Boolean resetToDefault = (Boolean) getScriptRunnerContext()
                            .getProperty(ScriptRunnerContext.COMMANDLINECONNECT);
                    if ((resetToDefault != null) && (resetToDefault.equals(Boolean.TRUE))) {
                        getScriptRunnerContext().putProperty(ScriptRunnerContext.COMMANDLINECONNECT, Boolean.FALSE);
                        // getScriptRunnerContext().setEscape(true);
                        getScriptRunnerContext().setSubstitutionOn(true);
                    }
                    // Check NLS_LANG, if its there, test it and set it up
                    if (NLSLang.isNLSLANGSet()) {
                    	NLSLang.setupNLSSession(getScriptRunnerContext(),conn);
                    }
                    // we have a new connection, run login.sql
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.NOLOG, false);
                    Object glogin = getScriptRunnerContext().getProperty(ScriptRunnerContext.DBCONFIG_GLOGIN);
                    Object gloginFile = getScriptRunnerContext().getProperty(ScriptRunnerContext.DBCONFIG_GLOGIN_FILE);
                    if ((glogin != null) && (((Boolean) glogin).equals(new Boolean("true")))) { //$NON-NLS-1$
                        runGLoginSQL();
                        runUserLoginSQL();
                    }
                } finally {
                    // force back to old unless explicit change if
                    // (!((oldEcho!=null)&&(oldEcho.equals(Boolean.FALSE)))) {
                    forceEcho(oldEcho, true);
                    // }
                }
            } else {// connection failed commit and exit
                // try {
                String failed = ""; //$NON-NLS-1$
                for (String outln : urlMessage) {
                    if (getScriptRunnerContext().isSQLPlusClassic()) {
                        if (outln != null && (!outln.equals(""))) { //$NON-NLS-1$
                            // first non ""
                            failed = outln;
                            break;
                        }
                    } else {
                        failed += outln;
                    }
                }
                if (getScriptRunnerContext().isCommandLine()
                        && (getScriptRunnerContext().getTopLevel() || getScriptRunnerContext().isSQLPlusClassic())) {// less
                                                                                                                        // noisy
                                                                                                                        // /
                                                                                                                        // not
                                                                                                                        // clickable
                                                                                                                        // connect
                                                                                                                        // fail
                                                                                                                        // error
                                                                                                                        // for
                                                                                                                        // command
                                                                                                                        // line.
                    if (!(getScriptRunnerContext().getSqlError() != 0 || getScriptRunnerContext().getOsError() != 0)) { // Checking
                                                                                                                        // for
                                                                                                                        // whenever
                        if (!( (getScriptRunnerContext().getProperty(ScriptRunnerContext.NOLOG) != null)
                              && (((Boolean) (getScriptRunnerContext().getProperty(ScriptRunnerContext.NOLOG))
                                      .equals(Boolean.TRUE))))) {
                            if (!failed.equals("")) { //$NON-NLS-1$
                                failed = failed + "\n" + ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NO_LONGER_CONNECTED); //$NON-NLS-1$
                            } else {
                                failed = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NO_LONGER_CONNECTED);
                            }
                        }
                    }
                    report(getScriptRunnerContext(), failed);
                } else {
                    if (!failed.equals("")) { //$NON-NLS-1$
                        failed = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CONNECTION_FAILED) + "\n" + failed; //$NON-NLS-1$
                    } else {
                        failed = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CONNECTION_FAILED);
                    }
                    //Bug 25129277 - CONNECT STATEMENT DISPLAYS PASSWORD WHEN CONNECTION FAILED. 
                    report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), "  connect ...", failed, //$NON-NLS-1$
                            getScriptRunnerContext()).trim());
                }
                doWhenever(true);
                if ((conn != null) && (!isConnectionClosed(conn))) {
                    if (LockManager.lock(conn)) {
                        try {
                            // if !jline
                            // close conn if transient connection
                            // if jline RealBase = null if realbase =
                            // currentconn
                            // close current conn
                            if (conn != null) {// from command line we may not
                                                // have a connection yet.
                                commitConnection(conn);
                                configureOldConnections(conn);// see no reason
                                                                // not to
                                                                // setCurrentConnection
                                                                // here - it is
                                                                // done
                                                                // eventually
                                                                // though
                            } else {
                                if (getScriptRunnerContext().getBaseConnection() == null) {
                                    getScriptRunnerContext().setBaseConnection(myconn);
                                }
                                getScriptRunnerContext().setCurrentConnection(myconn);
                            }
                            if (!((getScriptRunnerContext().isCommandLine() && getScriptRunnerContext().getTopLevel())
                                    || (conn == null))) {
                                // kris wants less noisy error message.
                                if (!(getScriptRunnerContext().isSQLPlusClassic())) {
                                    report(getScriptRunnerContext(), "Commit"); //$NON-NLS-1$
                                }
                            }
                        } finally {
                            LockManager.unlock(conn);
                        }
                    }
                }
                conn = null;// say conn = null is no log to save on .isclosed
                            // locking.
                getScriptRunnerContext().getMap()
                        .put(ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER.toString(), ""); //$NON-NLS-1$
                getScriptRunnerContext().getMap().put(ScriptRunnerContext.SqlplusVariable._USER.toString(), ""); //$NON-NLS-1$
                getScriptRunnerContext().setCurrentConnection(conn);
                // } catch (SQLException e) {
                // report(ScriptRunnerDbArb.format(ScriptRunnerDbArb.ERROR_ON,
                // "Commit")); //$NON-NLS-1$
                // }
                getScriptRunnerContext().putProperty(ScriptRunnerContext.NOLOG, true);
            }
        }
        getScriptRunnerContext().putProperty(ScriptRunnerContext.AUTOTRACE_CTXTRACESTATE, false);
        getScriptRunnerContext().putProperty(ScriptRunnerContext.AUTOTRACE_CTXTRACETYPE,
                ScriptRunnerContext.AUTOTRACE_AUTO_NONE);
    }

    private void forceEcho(boolean newValue, boolean isReset) {
        // need to do more than this - set listener off?
        if (isReset == false) {
            getScriptRunnerContext().putProperty(ScriptRunnerContext.EXPLICITSETECHO, Boolean.FALSE);
        } else {
            Boolean prop = (Boolean) getScriptRunnerContext().getProperty(ScriptRunnerContext.EXPLICITSETECHO);
            if ((prop != null) && (prop.equals(Boolean.TRUE))) {
                // been explicitlty set do not reset
                return;
            }
        }
        getScriptRunnerContext().setEcho(newValue);
    }

    private void commitConnection(Connection conn) {
        try {
            if (conn != null && !isConnectionClosed(conn)) {
                if (LockManager.lock(conn)) {
                    try {
                        conn.commit();
                    } finally {
                        LockManager.unlock(conn);
                    }
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
        }
    }

    private static boolean isConnectionClosed(Connection conn) {
        boolean closed = false;
        try {
            if (conn != null) {
                if (LockManager.lock(conn)) {
                    try {
                        closed = conn.isClosed();
                    } finally {
                        LockManager.unlock(conn);
                    }
                }
            }
        } catch (SQLException e) {
            Logger.getLogger("SQLPLUS").log(Level.WARNING, e.getStackTrace()[0].toString(), e); //$NON-NLS-1$
        }
        return closed;
    }

    private void closeOldConnection(Connection conn) {
        if (getScriptRunnerContext().getCloseConnection() == true) {
            try {
                conn.close();
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
            }
        }
    }

    private void runUserLoginSQL() {
    	ArrayList<String> logins = ShowLogin.getLocations(getScriptRunnerContext(), "login.sql", true); //$NON-NLS-1$
        String loginFile = ShowLogin.firstOrNull(logins); //$NON-NLS-1$
        if (loginFile != null) {
            File f = new File((String) loginFile);
            if (f.exists()) {
                getScriptRunnerContext().putProperty(ScriptRunnerContext.SQLCLI_LOGIN_SQL, Boolean.TRUE);
                if (FileUtils.isFileOnCWD(f,getScriptRunnerContext())) {
                	if (!FileUtils.isFileOnSQLPATH(f, getScriptRunnerContext())) {
                		getScriptRunnerContext().write(Messages.getString("SQLPLUS.24")); //$NON-NLS-1$
                		getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), Messages.getString("SQLPLUS.24"), "");
                	}
                }
                runLoginSQLFile(f);
                getScriptRunnerContext().putProperty(ScriptRunnerContext.SQLCLI_LOGIN_SQL, Boolean.FALSE);
            }
        }
    }

    private void runGLoginSQL() {
        String gloginFile = ShowLogin
                .firstOrNull(ShowLogin.getLocations(getScriptRunnerContext(), "glogin.sql", false)); //$NON-NLS-1$
        if (gloginFile != null) {
            File file = new File(gloginFile);
            if (file.exists() && file.canRead()) {
                // Get the glogin if its there
                getScriptRunnerContext().putProperty(ScriptRunnerContext.SQLCLI_GLOGIN_SQL, Boolean.TRUE);
                runLoginSQLFile(file);
                getScriptRunnerContext().putProperty(ScriptRunnerContext.SQLCLI_GLOGIN_SQL, Boolean.FALSE);
            }
        }
    }

    private void runLoginSQLFile(File file) {
        String directoryRemoved = file.getName();
        Object gloginFile = "login.sql"; //$NON-NLS-1$
        try {
            gloginFile = file.getCanonicalPath();
        } catch (IOException ioe) {
            // should use Brians logger this logger resolces to the default
            Logger.getLogger(getClass().getName()).log(Level.WARNING, ioe.getStackTrace()[0].toString(), ioe);
        }
        if (FileUtils.isFileOnCWD(file,getScriptRunnerContext())) {
        	if (!FileUtils.isFileOnSQLPATH(file, getScriptRunnerContext())) {
        	getScriptRunnerContext().putProperty(ScriptRunnerContext.LOGIN_FILE_ON_CWD, Boolean.TRUE);
        	}
        }
        String prevFile = getScriptRunnerContext().getSourceRef();
        
        Integer localDepth = 0;
        try {
            if ((getScriptRunnerContext().getTopLevel() == true)
                    || (getScriptRunnerContext().getProperty(ScriptRunnerContext.SCRIPT_DEPTH)) == null) {
                localDepth = 1;
            } else {
                localDepth = (Integer) getScriptRunnerContext().getProperty(ScriptRunnerContext.SCRIPT_DEPTH);
            }
            int oneOff = 0;
            String theName = ""; //$NON-NLS-1$
            String theGLogin = ShowLogin
            		.firstOrNull(ShowLogin.getLocations(getScriptRunnerContext(), "glogin.sql", false)); //$NON-NLS-1$
            if (theGLogin != null) {
                int slash = theGLogin.lastIndexOf("/"); //$NON-NLS-1$
                int bslash = theGLogin.lastIndexOf("\\"); //$NON-NLS-1$
                if (slash > bslash) {
                    bslash = slash;
                }
                if (bslash != -1) {
                    theName = theGLogin.substring(bslash + 1);
                } else {
                    theName = theGLogin;
                }
            }
            if (directoryRemoved != null && directoryRemoved.equals(theName)) {
                oneOff = 1;
            }
            if ((localDepth + oneOff) < 20) {
                getScriptRunnerContext().putProperty(ScriptRunnerContext.LOGIN_FILE, Boolean.TRUE);
                _runner = new ScriptExecutor(new FileInputStream(file), conn);
                getScriptRunnerContext().putProperty("runner", _runner); //$NON-NLS-1$
                _runner.setOut(out);
                boolean localTopLevel = getScriptRunnerContext().getTopLevel();
                getScriptRunnerContext().setTopLevel(false);

                getScriptRunnerContext().putProperty(ScriptRunnerContext.SCRIPT_DEPTH, localDepth + 1);
                _runner.setScriptRunnerContext(getScriptRunnerContext());
                String pathval = file.getCanonicalPath();
                getScriptRunnerContext().setSourceRef(pathval);
                int ii = pathval.lastIndexOf(File.separator);
                pathval = pathval.substring(0, ii);
                _runner.setDirectory(pathval);
                ArrayList<String> scrArrayList = null;
                try {
                    scrArrayList = ((ArrayList<String>) getScriptRunnerContext()
                            .getProperty(ScriptRunnerContext.APPINFOARRAYLIST));

                    scrArrayList.add(directoryRemoved);
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
                    if (scrArrayList.size() != 0) {
                        SetAppinfo.setAppinfo(getScriptRunnerContext(), getScriptRunnerContext().getCurrentConnection(),
                                scrArrayList.get(scrArrayList.size() - 1), scrArrayList.size());
                    } else {
                        // should never happen we have just added to the list
                    }

                    _runner.run();
                } finally {
                    setScriptRunnerContext(_runner.getScriptRunnerContext());
                    getScriptRunnerContext().setTopLevel(localTopLevel);
                    this.setConn(_runner.getScriptRunnerContext().getCurrentConnection());// return
                                                                                            // the
                                                                                            // current
                                                                                            // connection,
                                                                                            // which
                                                                                            // could
                                                                                            // be
                                                                                            // a
                                                                                            // connection
                                                                                            // created
                                                                                            // within
                                                                                            // the
                                                                                            // file
                    scrArrayList = (ArrayList<String>) getScriptRunnerContext()
                            .getProperty(ScriptRunnerContext.APPINFOARRAYLIST);
                    if (scrArrayList.size() != 0) {
                        scrArrayList.remove(scrArrayList.size() - 1);
                    }
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
                    if (scrArrayList.size() != 0) {
                        SetAppinfo.setAppinfo(getScriptRunnerContext(), getScriptRunnerContext().getCurrentConnection(),
                                scrArrayList.get(scrArrayList.size() - 1), scrArrayList.size());
                    } else {
                        SetAppinfo.setAppinfo(getScriptRunnerContext(), getScriptRunnerContext().getCurrentConnection(),
                                PRODUCT_NAME, // $NON-NLS-1$p
                                0);
                    }
                }
            } else {
                if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) != null
                        && Boolean.parseBoolean(getScriptRunnerContext()
                                .getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.MAX_NEST_CLASSIC));
                } else {
                    // does not appear to bomb out on whenever sqlerror|oserror
                    // exit
                    report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                            ScriptRunnerDbArb.getString(ScriptRunnerDbArb.MAX_NEST), this.getScriptRunnerContext()));
                }
            }
        } catch (FileNotFoundException e) {
            report(getScriptRunnerContext(), "No " + (String) gloginFile + " found"); //$NON-NLS-1$ //$NON-NLS-2$
        } catch (IOException e) {
            report(getScriptRunnerContext(), "No " + (String) gloginFile + " found"); //$NON-NLS-1$ //$NON-NLS-2$
        } finally {
            getScriptRunnerContext().setSourceRef(prevFile);
            getScriptRunnerContext().removeProperty(ScriptRunnerContext.LOGIN_FILE);
            getScriptRunnerContext().putProperty(ScriptRunnerContext.SCRIPT_DEPTH, localDepth);
        }

    }

    private boolean connectCallDialog(/* in out */ConnectionDetails cd) {
        // catch it here - capture username/password@jdbc. []jdbc. and jdbc. do
        // not accept third party to retry
        if ((cd != null) && (cd.getConnectDB() != null) && (!(cd.getConnectDB().equals("")))) { //$NON-NLS-1$
            oracle.dbtools.db.ConnectionDetails cd3 = null;
            try {
                if (!hasUrlStub(cd.getConnectDB())) {
                    cd3 = new oracle.dbtools.db.ConnectionDetails(cd.getConnectDB());
                }
            } catch (Exception e) {
                // do not want to fail badly if 3rd party connection parsing
                // fails
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
            }
            if (cd3 != null && cd3.getDriver() != null) {
                // do not want 3rd party in retry - leave it to the level above
                // to try once.
                return true;
            }
        }
        for (int ii = 0; ii < 3; ii++) {
            // call dialog with connectName connectPassword connectDB
            // test connection if it works break
            // if we have at least / (the first time) try it without prompting.
            if (!((ii == 0) && (cd.getSlash() != -1) && (cd.getConnectName() != null)
                    && (cd.getConnectPassword() != null) && (cd.getConnectDB() != null))) {
                ConnectionDetails threeFields = getScriptRunnerContext().getConnectFieldsProvider()
                        .get3Fields(getScriptRunnerContext(), cd, (ii > 0 ? true : false));
                if (threeFields == null) {
                    return false; // cancel pressed
                }
                cd.cloneFrom(threeFields);
            }
            String totalErrors = ""; //$NON-NLS-1$
            ArrayList<String> urlMessage = new ArrayList<String>();
            Boolean threeTimes = (Boolean) this.getScriptRunnerContext().getProperty(ScriptRunnerContext.THREETIMES);
            if (threeTimes == null) {
                threeTimes = Boolean.FALSE;
            }
            if ((ii == 2) || (threeTimes.equals(Boolean.FALSE))) {
                // let main connection error out with new details
                // cd.getConnection() is null on return.
                break;
            }
            Connection myconn = getConnection(urlMessage, cd.getConnectName(), cd.getConnectPassword(),
                    cd.getConnectDB(), cd.getRole());
            if (myconn != null) {
                cd.setConnection(myconn);// danger confirm it gets closed all
                                            // calls in try/finally cleanup blog
                                            // (copy and connect)
                myconn = null;
                break;
            } else {
                for (String outln : urlMessage) {
                    if (getScriptRunnerContext().isSQLPlusClassic()) {
                        if (outln != null && (!outln.equals(""))) { //$NON-NLS-1$
                            // first non ""
                            totalErrors = outln;
                            break;
                        }
                    } else {
                        totalErrors += outln;
                    }
                }
                doWhenever(true);
                if (getScriptRunnerContext().getExited() == true) {
                    break;
                }
            }
            if (getScriptRunnerContext().isCommandLine() && getScriptRunnerContext().getTopLevel()) {// reduce
                                                                                                        // error
                                                                                                        // size
                                                                                                        // in
                                                                                                        // command
                                                                                                        // line
                                                                                                        // -
                                                                                                        // note
                                                                                                        // RETRYING
                                                                                                        // is
                                                                                                        // in
                                                                                                        // the
                                                                                                        // prompt
                                                                                                        // for
                                                                                                        // usernam/password/connection
                report(getScriptRunnerContext(), ("Z" + totalErrors).trim() //$NON-NLS-1$
                        .substring(1)/*
                                         * Z is for rtrim assumed in message
                                         * file does not end with newline
                                         */);
            } else {
                report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                        ScriptRunnerDbArb.format(ScriptRunnerDbArb.CONNECTION_FAILED_RETRY_ARG, ("Z" + totalErrors) //$NON-NLS-1$
                                .trim().substring(1)/*
                                                     * assumed in message file
                                                     * does not end with newline
                                                     */),
                        this.getScriptRunnerContext()).trim());
            }
        }
        return true;

    }

    private void runQuit() {
        // does trim to UpperCase endsWith Rollback or endswith commit
        // how does one exit nicely time to look at sqlplus code methinks;
        {// ignoring reporting and variables for now.
            String localSql = cmd.getSql().toUpperCase();
            if (localSql.endsWith(";")) { //$NON-NLS-1$
                localSql = (localSql.substring(0, (localSql.length() - 1))).trim();
            }
            if ((conn != null) && (!(isConnectionClosed(conn)))
                    && (!((getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH) == null)
                            || ((Boolean) (getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH)))
                                    .equals(Boolean.TRUE)))) {
                if (localSql.endsWith("ROLLBACK")) { //$NON-NLS-1$
                    try {
                        conn.rollback();
                    } catch (SQLException e) {
                        if ((!(conn instanceof OracleConnection))||(DBUtil.isOracleConnectionAlive(conn))) {
                            report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                        
                                ScriptRunnerDbArb.format(ScriptRunnerDbArb.ERROR_ON, "Rollback"), //$NON-NLS-1$
                                this.getScriptRunnerContext()));
                        }
                    }
                } else {
                    try {
                        if (getScriptRunnerContext().getProperty(ScriptRunnerContext.EXITCOMMIT) != null
                                && getScriptRunnerContext().getProperty(ScriptRunnerContext.EXITCOMMIT).equals("OFF")) { //$NON-NLS-1$
                            conn.rollback();
                        } else {
                            conn.commit();
                        }
                    } catch (SQLException e) {
                        if ((!(conn instanceof OracleConnection))||(DBUtil.isOracleConnectionAlive(conn))) {
                            report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                                ScriptRunnerDbArb.format(ScriptRunnerDbArb.ERROR_ON, "Rollback"), //$NON-NLS-1$
                                this.getScriptRunnerContext()));
                        }
                    }
                }
            }
            getScriptRunnerContext().setExited(true);
            String exitArgs = ""; //$NON-NLS-1$
            if (localSql.length() > 2) {
                exitArgs = localSql.substring(4).trim();
                exitArgs = exitArgs.replaceAll("ROLLBACK", "").replaceAll("COMMIT", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            }
            String[] allExitArgs = exitArgs.trim().split("\\s+"); //$NON-NLS-1$
            if (allExitArgs[0].toUpperCase().equals("FAILURE")) { //$NON-NLS-1$
                getScriptRunnerContext().putProperty(ScriptRunnerContext.EXIT_INT, 1);
            } else if (allExitArgs.length > 1) {
                report(getScriptRunnerContext(), ScriptRunnerDbArb.get(ScriptRunnerDbArb.EXIT_JUNK2));
            } else {
                try {
                    // print value of the variables
                    // Map m=getScriptRunnerContext().getVarMap();
                    if (allExitArgs[0].startsWith(":")) { //$NON-NLS-1$
                        String bindName = exitArgs.substring(1).trim();
                        Map<String, Bind> m = getScriptRunnerContext().getVarMap();
                        synchronized (m) {
                            Bind b = m.get(bindName.toUpperCase());
                            if (b != null) {
                                String value = b.getValue();
                                if ((value != null) && (!value.equals("")) //$NON-NLS-1$
                                        && !(value.equalsIgnoreCase("infinity") || value.equalsIgnoreCase("nan")) //$NON-NLS-1$ //$NON-NLS-2$
                                        && (value.length() > 0 && Character.isDigit(value.charAt(0)))) {
                                    allExitArgs[0] = value;
                                } else {
                                    allExitArgs[0] = "1"; //$NON-NLS-1$
                                    report(getScriptRunnerContext(), ScriptRunnerDbArb.get(ScriptRunnerDbArb.INTERNAL_NUMBER_ERROR));
                                    report(getScriptRunnerContext(), ScriptRunnerDbArb.get(ScriptRunnerDbArb.EXIT_JUNK2));
                                }
                            } else {
                                // Bind doesnt exist, so call it 1 to mirror
                                // SQL*Plus
                                allExitArgs[0] = "1"; //$NON-NLS-1$
                                report(getScriptRunnerContext(), ScriptRunnerDbArb.get(ScriptRunnerDbArb.EXIT_JUNK2));
                            }
                        }
                    } else if (allExitArgs[0] != null && (allExitArgs[0].toUpperCase().equals("SQLCODE") //$NON-NLS-1$
                            || allExitArgs[0].toUpperCase().equals("SQL.SQLCODE"))) { //$NON-NLS-1$
                        String sqlCodeString = (String) getScriptRunnerContext()
                                .getProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE);
                        int codeNum = 0;
                        if (sqlCodeString != null) {
                            try {
                                String firstLine = sqlCodeString.split("\n")[0].trim(); //$NON-NLS-1$
                                if (firstLine.matches("^[^\\-]*\\-[0-9]+.*$")) { //$NON-NLS-1$
                                    codeNum = Integer.parseInt(firstLine.replaceAll("^[^\\-]*\\-([0-9]+).*$", "$1")); //$NON-NLS-1$ //$NON-NLS-2$
                                } else {
                                    codeNum = -1;
                                }
                            } catch (NumberFormatException e) {
                                codeNum = -1;
                            } catch (PatternSyntaxException e) {
                                codeNum = -1;
                            }
                        }
                        allExitArgs[0] = String.valueOf(codeNum);
                    }
                    String val = allExitArgs[0];
                    if (val != null) {
                        val = val.trim().toUpperCase();
                    }
                    if ((allExitArgs[0] != null) && !val.equals("SUCCESS") && //$NON-NLS-1$
                            !val.equals("FAILURE") && //$NON-NLS-1$
                            !val.equals("WARNING") && //$NON-NLS-1$
                            !val.equals("COMMIT") && //$NON-NLS-1$
                            !val.equals("ROLLBACK") && //$NON-NLS-1$ //$NON-NLS-2$
                                                        // $NON-NLS-1$
                                                        // $NON-NLS-1$
                                                        //$NON-NLS-1$ //$NON-NLS-3$
                                                        // $NON-NLS-1$
                                                        // $NON-NLS-1$
                                                        //$NON-NLS-1$ //$NON-NLS-4$
                                                        // $NON-NLS-1$
                                                        // $NON-NLS-1$
                                                        //$NON-NLS-1$ //$NON-NLS-5$
                    !val.startsWith("/*") && //$NON-NLS-1$
                            !val.startsWith("--") && !val.equals("")//$NON-NLS-1$ //$NON-NLS-2$
                    ) { // $NON-NLS-1$ //$NON-NLS-2$
                        try { // 5e3
                            Double.parseDouble(allExitArgs[0]);
                            DecimalFormat format = new DecimalFormat();
                            format.setDecimalSeparatorAlwaysShown(false);
                            Double d = Double.parseDouble(allExitArgs[0]);
                            if (d > Integer.MAX_VALUE || d < 1) {
                                getScriptRunnerContext().putProperty(ScriptRunnerContext.EXIT_INT, 0);
                            } else {
                                int is = (int) new BigDecimal(d).setScale(0, RoundingMode.HALF_DOWN)
                                        .intValue();
                                getScriptRunnerContext().putProperty(ScriptRunnerContext.EXIT_INT, is);
                            }
                        } catch (NumberFormatException e) {
                            int i = 0;
                            // Check that this is a sqlplus variable
                            Map<String, String> m = getScriptRunnerContext().getMap();
                            Set<String> s = m.keySet();
                            synchronized (m) {
                                Iterator<String> it = s.iterator();
                                boolean found = false;
                                while (it.hasNext()) {
                                    String nextKey = it.next();
                                    if (val.equals(nextKey)) {
                                        try {
                                            String value = m.get(nextKey);
                                            try {
                                                i = Integer.parseInt(value);
                                                found = true;
                                            } catch (NumberFormatException e2) {

                                            }
                                        } catch (IllegalArgumentException iae) {
                                        }

                                    }
                                }
                                // Check for sqlcode
                                if (val.equalsIgnoreCase("sqlcode") || val.equalsIgnoreCase("sql.sqlcode")) { //$NON-NLS-1$ //$NON-NLS-2$
                                    String sqlCodeString = (String) getScriptRunnerContext()
                                            .getProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE);
                                    int codeNum = 0;
                                    if (sqlCodeString != null) {
                                        try {
                                            String firstLine = sqlCodeString.split("\n")[0].trim(); //$NON-NLS-1$
                                            if (firstLine.matches("^[^\\-]*\\-[0-9]+.*$")) { //$NON-NLS-1$
                                                codeNum = Integer
                                                        .parseInt(firstLine.replaceAll("^[^\\-]*\\-([0-9]+).*$", "$1")); //$NON-NLS-1$ //$NON-NLS-2$
                                            } else {
                                                codeNum = -1;
                                            }
                                        } catch (NumberFormatException e1) {
                                            codeNum = -1;
                                        } catch (PatternSyntaxException e2) {
                                            codeNum = -1;
                                        }
                                        getScriptRunnerContext().putProperty(ScriptRunnerContext.EXIT_INT, codeNum);
                                        found = true;
                                    }
                                }
                                if (!found) {
                                    i = 1; // numbers not found so error out
                                    report(getScriptRunnerContext(), MessageFormat.format(ScriptRunnerDbArb.get(ScriptRunnerDbArb.EXIT_JUNK),
                                            new Object[] { exitArgs != null ? exitArgs.trim() : exitArgs }));
                                    report(getScriptRunnerContext(), ScriptRunnerDbArb.get(ScriptRunnerDbArb.EXIT_JUNK2));
                                }

                            }
                            getScriptRunnerContext().putProperty(ScriptRunnerContext.EXIT_INT, i);
                        }

                    } else {
                        int value = 0;
                        switch (val) {
                        case "WARNING": //$NON-NLS-1$
                            value = 2;
                            break;
                        case "FAILURE": //$NON-NLS-1$
                            value = 1;
                            break;
                        default:
                            break;
                        }
                        getScriptRunnerContext().putProperty(ScriptRunnerContext.EXIT_INT, value);
                    }
                } catch (Exception e) {
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.EXIT_VALUE_NOT_CALCULATED, exitArgs));
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.EXIT_INT, 1);
                    // ignore for now, not handling variable or bind variable in
                    // exit status
                }
            }

            if ((this.getScriptRunnerContext().getProperty(ScriptRunnerContext.SPOOLOUTBUFFER)) != null) {
                this.getScriptRunnerContext().stopSpool();
                // so disconnected message will not appear in spool.
            }
            if (conn != null) {
                runDisconnect();
            }
        }
    }

    ExecutePathCheckReturn checkPath(boolean tryUrlAsDir, boolean multiplePath, boolean noPrefix, String prefixString,
            URL prefixURL, String inputFile, String encodingOverride, PathException lastException) {
        ExecutePathCheckReturn retVal = new ExecutePathCheckReturn();
        // need to check prefix string could be ;: separated or prefix url or
        // noprefix
        // or http or ftp ...file: or / or \
        // assumption ..file: does not come from multiple=true ie SQLPATH
        // provider:
        Reader reader = null;
        InputStream is = null;
        int NOPREFIX = 1, PREFIXSTRING = 2, PREFIXURL = 3;
        int state = 0;
        // 1=noprefix 2=prefixFile 3=prefix URL;
        if (noPrefix) {
            state = NOPREFIX;
        } else if (prefixString != null) {
            state = PREFIXSTRING;
        } else if (prefixURL != null) {
            state = PREFIXURL;
        } else {
            if (lastException != null) {
                // on null bad state propagate last exception if opossible
                retVal.setLastException(lastException);
            } else {// prefix was invalid do not report exception
                // retVal.setLastException(new PathException("Invalid state
                // nu"));
            }
            return retVal;
        }
        try {
            if (state == PREFIXURL) {
                if (prefixURL != null) {
                    // if (!urlWasNotDir) {
                    // TODO may need to handle this
                    // }
                    URL returnURL = joinUrl(tryUrlAsDir, prefixURL, inputFile);
                    if ((returnURL != null) && (this.haveIBytes(returnURL))) {
                        URLConnection uc = null;
                        InputStream isInner = null;
                        try {
                            uc = returnURL.openConnection();
                            isInner = uc.getInputStream();
                            inputFile = returnURL.toString();
                        } catch (IOException e) {
                            returnURL = null;
                        } finally {
                            if (isInner != null) {
                                try {
                                    isInner.close();
                                } catch (IOException e) {
                                }
                            }
                        }
                        retVal.setUrl(returnURL);
                    }
                }
            } else {
                String setting = null;
                if (state == NOPREFIX) {
                    setting = inputFile;
                } else {
                    setting = prefixString;
                }
                if ((setting != null) && (!setting.equals(""))) { //$NON-NLS-1$
                    String[] pathArray = new String[] { setting };
                    if (multiplePath) {
                        pathArray = setting.split(File.pathSeparator);
                    }
                    // loop through all arrays with / and \ for pathArray + "\"
                    // + inputfile
                    try {
                        boolean prependHttp = false;
                        boolean prependFtp = false;
                        String matchedInputFile = null;
                        for (String singlePath : pathArray) {
                            if (singlePath == null) {
                                continue;
                            }
                            if (File.pathSeparator.equals(":")) { //$NON-NLS-1$
                                if (prependHttp) {
                                    singlePath = "http:" + singlePath; //$NON-NLS-1$
                                }
                                if (prependFtp) {
                                    singlePath = "ftp:" + singlePath; //$NON-NLS-1$
                                }
                                prependHttp = false;
                                prependHttp = false;
                                if (singlePath.toLowerCase().equals("http")) { //$NON-NLS-1$
                                    prependHttp = true;
                                    continue;
                                }
                                if (singlePath.toLowerCase().equals("ftp")) { //$NON-NLS-1$
                                    prependFtp = true;
                                    continue;
                                }
                                prependHttp = false;
                                prependHttp = false;
                            }
                            if (FileUtils.startsWithHttpOrFtp(singlePath)) {
                                String path;
                                if (state != NOPREFIX) {
                                    if (singlePath.endsWith("\\") || singlePath.endsWith("/")) { //$NON-NLS-1$ //$NON-NLS-2$
                                        path = singlePath + inputFile;
                                    } else {
                                        path = singlePath + "/" + inputFile; //$NON-NLS-1$
                                    }
                                } else {
                                    path = singlePath;
                                }
                                if (FileUtils.haveIBytes(path, getScriptRunnerContext())) {
                                    matchedInputFile = path;
                                    break;
                                }
                                if (state != NOPREFIX) {
                                    if (singlePath.endsWith("\\") || singlePath.endsWith("/")) { //$NON-NLS-1$ //$NON-NLS-2$
                                        path = singlePath + inputFile;
                                    } else {
                                        path = singlePath + "\\" + inputFile; //$NON-NLS-1$
                                    }
                                } else {
                                    path = singlePath;
                                }
                                if (FileUtils.haveIBytesRaw(path)) {
                                    matchedInputFile = path.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
                                    break;
                                }
                            } else {
                                String combined = null;
                                if (singlePath.endsWith("\\") || singlePath.endsWith("/")) { //$NON-NLS-1$ //$NON-NLS-2$
                                    combined = singlePath + inputFile;
                                } else {
                                    combined = singlePath + "/" + inputFile; //$NON-NLS-1$
                                }
                                if (state == NOPREFIX) {
                                    combined = singlePath;
                                }
                                if (new File((combined).replace("/", "\\")).exists()) { //$NON-NLS-1$ //$NON-NLS-2$
                                                                                        // $NON-NLS-1$
                                                                                        // $NON-NLS-1$
                                                                                        //$NON-NLS-1$ //$NON-NLS-3$
                                    matchedInputFile = (combined).replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
                                                                                        // $NON-NLS-1$
                                                                                        // $NON-NLS-1$
                                                                                        //$NON-NLS-1$ //$NON-NLS-3$
                                    break;
                                }
                                if (new File((combined).replace("\\", "/")).exists()) { //$NON-NLS-1$ //$NON-NLS-2$
                                                                                        // $NON-NLS-1$
                                                                                        // $NON-NLS-1$
                                                                                        //$NON-NLS-1$ //$NON-NLS-3$
                                    matchedInputFile = (combined).replace("\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$
                                                                                        // $NON-NLS-1$
                                                                                        // $NON-NLS-1$
                                                                                        //$NON-NLS-1$ //$NON-NLS-3$
                                    break;
                                } 
                            }
                        }
                        if (matchedInputFile != null) {
                            if (FileUtils.startsWithHttpOrFtp(matchedInputFile)
                                    && ((FileUtils.haveIBytes(matchedInputFile, getScriptRunnerContext())
                                            || FileUtils.haveIBytesRaw(matchedInputFile)))) {
                                matchedInputFile.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
                                if (getScriptRunnerContext().getProperty(ScriptRunnerContext.HTTP_PROXY) != null) {
                                    try {
                                        Proxy proxy = new Proxy(Proxy.Type.HTTP,
                                                new InetSocketAddress(getScriptRunnerContext()
                                                        .getProperty(ScriptRunnerContext.HTTP_PROXY_HOST).toString(),
                                                Integer.parseInt(getScriptRunnerContext()
                                                        .getProperty(ScriptRunnerContext.HTTP_PROXY_PORT).toString())));
                                        HttpURLConnection connection = (HttpURLConnection) new URL(matchedInputFile)
                                                .openConnection(proxy);
                                        is = connection.getInputStream();
                                    } catch (Exception e) {
                                        write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.SQLPLUS_PROXY_ERROR));
                                    }
                                } else {
                                    is = getInputStream(matchedInputFile);
                                }
                                if (is != null) {
                                    inputFile = matchedInputFile;
                                    reader = new BufferedReader(new InputStreamReader(is, encodingOverride));
                                    retVal.setOutputFile(inputFile);
                                }
                            } else {
                                inputFile = matchedInputFile;
                                reader = new BufferedReader(
                                        new InputStreamReader(new FileInputStream(inputFile), encodingOverride));
                                retVal.setOutputFile(inputFile);
                            }
                        }
                    } catch (FileNotFoundException ee) {
                        throw ee;
                    }
                }
            }
        } catch (Exception e) {
            retVal.setLastException(new PathException(e));
        } finally {
            if (reader == null) {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException ioe) {
                        LOGGER.log(Level.WARNING, ioe.getStackTrace()[0].toString(), ioe);
                    }
                }
                if (retVal.getLastException() != null) {
                    if (lastException != null) {
                        // on null bad state propagate last exception if
                        // opossible
                        retVal.setLastException(lastException);
                    } else {
                        retVal.setLastException(new PathException(
                                ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNABLE_TO_OPEN_FILE, inputFile)));
                    }
                }
            } else {
                retVal.setLiveReader(reader);
                // this is SUCCESS
            }
            // really clear up stuff the only closable externally stuff is the
            // reader.
        }
        return retVal;
    }

    public ExecutePathCheckReturn checkMultiplePaths(ISQLCommand cmd) {
        this.cmd = cmd;
        return checkMultiplePaths();
    }

    public ExecutePathCheckReturn checkMultiplePaths() {
        String preAppendInputFile = ""; //$NON-NLS-1$
        String arguments = null;
        ExecutePathCheckReturn retVal = null;
        String encodingOverride = getScriptRunnerContext().getEncoding();

        URL baseURL = (URL) getScriptRunnerContext().getProperty(ScriptRunnerContext.BASE_URL);
        if (getScriptRunnerContext().getTopLevel()) {
            getScriptRunnerContext().putProperty(ScriptRunnerContext.TOP_BASE_URL, baseURL);
        }
        URL topURL = (URL) getScriptRunnerContext().getProperty(ScriptRunnerContext.TOP_BASE_URL);
        // String oh = EnvironmentVariables.getVariable("ORACLE_HOME");
        // report ("ORACLE_HOME..."+oh);
        // need to grab a file which may be relative absolute
        // or ORACLE _HOME based ie start ?/file.sql or @?/file.sql
        // file start file tries file.sql start file.sqlx tries file file.sqlx
        boolean callFile = false;
        // String arguments="";
        String localSql = ""; //$NON-NLS-1$
        int i = 0;
        if (cmd.getStmtId() == SQLCommand.StmtSubType.G_S_START) {
            localSql = cmd.getSql();
            localSql = localSql.trim();
            if (localSql.toLowerCase().equals("start")) { //$NON-NLS-1$
                report(getScriptRunnerContext(), ""); //$NON-NLS-1$
            } else {
                boolean found = false;
                for (i = 0; i < localSql.length(); i++) {
                    char nextChar = localSql.charAt(i);
                    if ((Character.isWhitespace(nextChar)) || (nextChar == '\'') || (nextChar == '\"')) {
                        found = true;
                        break;
                    }
                }
                if (found == false) {
                    // noop report("");
                } else {// we have whitespace
                    // get first argument if the argument has space
                    // in the name or is quoted this will fail
                    localSql = localSql.substring(i).trim();
                    callFile = true;
                }
            }
        } else if (cmd.getStmtId() == SQLCommand.StmtSubType.G_S_AT) {
            if (cmd.getSql().trim().startsWith("@@")) { //$NON-NLS-1$
                cmd.setStmtId(SQLCommand.StmtSubType.G_S_ATNESTED);
                // second at could be a substitution variable like in apex
            } else {
                localSql = cmd.getSql();
                localSql = localSql.trim();
                // eat at trim again
                if (localSql.length() == 1) {
                    report(getScriptRunnerContext(), ""); //$NON-NLS-1$
                } else {
                    localSql = localSql.substring(1).trim();
                    if (localSql.startsWith("~")) //$NON-NLS-1$
                        localSql = localSql.replaceFirst("~", System.getProperty("user.home")); //$NON-NLS-1$ //$NON-NLS-2$
                    callFile = true;
                }
            }
        }
        if (cmd.getStmtId() == SQLCommand.StmtSubType.G_S_ATNESTED) {
            localSql = cmd.getSql();
            localSql = localSql.trim();
            // eat at trim again
            if (localSql.length() == 2) {
                // noop
            } else {
                localSql = localSql.substring(2).trim();
                callFile = true;
            }
        }
        if (callFile == true) {
            boolean found = false;
            String unQuotedFile = null;
            String endString = ""; //$NON-NLS-1$
            if ((localSql.length() > 1) && (localSql.startsWith("\"") || localSql.startsWith("\'"))) { //$NON-NLS-1$ //$NON-NLS-2$
                endString = new Character(localSql.charAt(0)).toString();
                int indexOfSecond = localSql.substring(1).indexOf(endString);
                if (indexOfSecond != -1) {
                    unQuotedFile = localSql.substring(1, indexOfSecond + 1);
                    i = indexOfSecond + 2;
                }
            } else {
                for (i = 0; i < localSql.length(); i++) {
                    if (Character.isWhitespace(localSql.charAt(i))) {
                        found = true;
                        break;
                    }
                }
            }
            if ((found == false) && (localSql.length() == 0)) {
                // noop report("");
            } else {

                String inputFile = ""; //$NON-NLS-1$
                if ((found != false) || (unQuotedFile != null)) {
                    if (unQuotedFile != null) {
                        inputFile = unQuotedFile;
                        if (localSql.length() > i) {// i itself may be one
                                                    // character data
                            arguments = localSql.substring(i).trim();
                        }
                    } else {
                        inputFile = localSql.substring(0, i);
                        if (localSql.length() > i + 1) {// i itself is a space
                            arguments = localSql.substring(i).trim();
                        }
                    }
                } else {
                    inputFile = localSql;
                }
                if (inputFile.contains("$") || inputFile.contains("%")) { //$NON-NLS-1$ //$NON-NLS-2$
                    inputFile = ScriptUtils.replaceEnvVars(inputFile);
                }
                if (inputFile.startsWith("?")) { //$NON-NLS-1$
                    if (inputFile.length() > 1) {
                        String OH = java.lang.System.getenv("ORACLE_HOME"); //$NON-NLS-1$
                        if (OH == null) {
                            OH = "?"; //$NON-NLS-1$
                        }
                        if (OH.endsWith(File.separator)) {
                            OH = OH.substring(0, OH.length() - 1);
                        }
                        inputFile = OH + inputFile.substring(1);
                    }
                }
                int filesep = inputFile.lastIndexOf("/"); //$NON-NLS-1$
                int filesepBack = inputFile.lastIndexOf("\\"); //$NON-NLS-1$
                if (filesepBack > filesep) {
                    filesep = filesepBack;
                }
                int dot = inputFile.lastIndexOf('.');
                if (dot <= filesep) {// may both be -1
                    if (!getScriptRunnerContext().getProperty(ScriptRunnerContext.SUFFIX).equals("")) //$NON-NLS-1$
                        inputFile = inputFile + "." + getScriptRunnerContext().getProperty(ScriptRunnerContext.SUFFIX); //$NON-NLS-1$
                }
                // Remember input file before any directory or URL are added.
                // Oracle home is ok as input files are not really going to
                // begin
                // with ? .
                preAppendInputFile = inputFile;
                // if inputFile does not start with File.separator, in@@ case
                // use directory+File.separator;
                // what about dos starts with a drive letter
                // if windows and chars 2,3 is :\ if not windows and starts with
                // /
                boolean prependDirectory = false;
                if (!(FileUtils.startsWithHttpOrFtp(inputFile))) {
                    //if starts with / or \ could be full unc path (windows) or full path Unix - prepend=false
                    if (!((inputFile.startsWith("/")) || (inputFile.startsWith("\\")))) { //$NON-NLS-1$ //$NON-NLS-2$
                        if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
                            if (!((inputFile.length() > 3) && (((inputFile.charAt(1) == ':')
                                         && ((inputFile.charAt(2) == '/') || (inputFile.charAt(2) == '\\')))
                                        || ((inputFile.charAt(0) == '/') && (inputFile.charAt(2) == ':')
                                                && (inputFile.charAt(3) == '/'))))) { // $NON-NLS-1$
                                prependDirectory = true;
                            }
                        } else {
                            if (!((inputFile.startsWith("/")) || (inputFile.startsWith("\\")))) { //$NON-NLS-1$ //$NON-NLS-2$
                                prependDirectory = true;
                            }
                        }
                    }
                }
                if (cmd.getStmtId() == SQLCommand.StmtSubType.G_S_ATNESTED) {
                    String uRLString = null;
                    boolean slashCase = false;
                    // if I have a directory and I am not c:/ or / I should be
                    // treated as a normal script
                    if (prependDirectory == true) {
                        // allow prepend directory if it exists even if / in
                        // file path - not checking http etc for now.
                        String possibleMatch = null;
                        if ((getDirectory() != null)) {
                            if ((getDirectory().endsWith("/") || getDirectory().endsWith("\\"))) { //$NON-NLS-1$ //$NON-NLS-2$
                                possibleMatch = getDirectory() + inputFile;
                            } else {
                                possibleMatch = getDirectory() + File.separator + inputFile;
                            }
                        }
                        if ((possibleMatch != null) && FileUtils.startsWithHttpOrFtp(possibleMatch)) {
                            if (FileUtils.haveIBytes(possibleMatch, getScriptRunnerContext())) {
                                uRLString = possibleMatch.replace("\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$
                            } else if ((getDirectory().indexOf("\\") != -1) && //$NON-NLS-1$
                                    FileUtils.haveIBytesRaw(possibleMatch.replace("/", "\\"))) { //$NON-NLS-1$ //$NON-NLS-2$
                                uRLString = possibleMatch.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
                            } else {
                                // always check normal path if not found
                                prependDirectory = false;
                                slashCase = true;
                                // }
                            }
                        } else {
                            if ((possibleMatch != null) && (new File(possibleMatch.replace("/", "\\")).exists())) { //$NON-NLS-1$ //$NON-NLS-2$
                                prependDirectory = true;
                            } else
                                if ((possibleMatch != null) && (new File(possibleMatch.replace("\\", "/")).exists())) { //$NON-NLS-1$ //$NON-NLS-2$
                                prependDirectory = true;
                            } else {
                                // always check normal path if not found
                                prependDirectory = false;
                                slashCase = true;
                                // }
                            }
                        }
                    }
                    if (uRLString != null) {
                        inputFile = uRLString;
                    } else {
                        if ((prependDirectory == true) && (getDirectory() != null)) {
                            if (!getDirectory().equals("")) { //$NON-NLS-1$
                                if ((getDirectory().endsWith("/") || getDirectory().endsWith("\\"))) { //$NON-NLS-1$ //$NON-NLS-2$
                                    inputFile = getDirectory() + inputFile;
                                } else {
                                    inputFile = getDirectory() + File.separator + inputFile;
                                }
                            }
                        }
                    }
                    prependDirectory = false;
                    if (slashCase == true) {
                        // @@xx/xx or @@xx failed try @
                        prependDirectory = true;
                    }
                }

                if (prependDirectory) {
                    String encoding = encodingOverride;
                    // if altering this path the SQLPATHShowCommand code will
                    // also have to be altered.
                    retVal = checkPath(false, false, false,
                            (String) getScriptRunnerContext().getProperty(ScriptRunnerContext.CDPATH), null, inputFile,
                            encoding, null);

                    // need to check url if passing in a base url, otherwise a
                    // reader is returned
                    if ((retVal.getLiveReader() == null) && (retVal.getUrl() == null)) {
                        retVal = checkPath(false, false, false, null, baseURL, inputFile, encoding, null);
                    }
                    if ((retVal.getLiveReader() == null) && (retVal.getUrl() == null)) {
                        retVal = checkPath(false, false, false, null, topURL, inputFile, encoding,
                                (PathException) retVal.getLastException());
                    }
                    if ((retVal.getLiveReader() == null) && (retVal.getUrl() == null)) {
                        retVal = checkPath(false, false, false, null,
                                useIfNotDirectoryOrNotFile(getScriptRunnerContext().getLastNodeForDirNameURL()),
                                inputFile, encoding, retVal.getLastException());
                    }
                    // the below is reinserted, it is checking
                    // getLastNodeForDirNameURL().getPATH().. i.e. what if
                    // dirnameurl is a directory itselt
                    if ((retVal.getLiveReader() == null) && (retVal.getUrl() == null)) {
                        retVal = checkPath(true, false, false, null,
                                useIfDirectoryOrNotFile(getScriptRunnerContext().getLastNodeForDirNameURL()), inputFile,
                                encoding, (PathException) retVal.getLastException());
                    }
                    if ((retVal.getLiveReader() == null) && (retVal.getUrl() == null)) {
                        retVal = checkPath(false, false, false, null,
                                useIfNotDirectoryOrNotFile(getScriptRunnerContext().getLastDirNameURL()), inputFile,
                                encoding, retVal.getLastException());
                    } // the below is checking if get LastDirNameURL was a
                        // directory
                    if ((retVal.getLiveReader() == null) && (retVal.getUrl() == null)) {
                        retVal = checkPath(true, false, false, null,
                                useIfDirectoryOrNotFile(getScriptRunnerContext().getLastDirNameURL()), inputFile,
                                encoding, retVal.getLastException());
                    }
                    if ((retVal.getLiveReader() == null) && (retVal.getUrl() == null)) {
                        retVal = checkPath(false, true, false,
                                (String) getScriptRunnerContext()
                                        .getProperty(ScriptRunnerContext.DBCONFIG_DEFAULT_PATH),
                                null, inputFile, encoding, retVal.getLastException());
                    }
                    if ((retVal.getLiveReader() == null) && (retVal.getUrl() == null)
                            && (SQLPLUS.getSqlpathProvider() != null)) {
                        retVal = checkPath(false, true, false, SQLPLUS.getSqlpathProvider().getSQLPATHsetting(), null,
                                inputFile, encoding, (PathException) retVal.getLastException());
                    }
                    // plus I guess try as from cwd ie try thefile.sql i.e.
                    // ./thefile.sql
                    if (getScriptRunnerContext().getProperty(ScriptRunnerContext.JLINE_SETTING) != null) {
                        if ((retVal.getLiveReader() == null) && (retVal.getUrl() == null)) {
                            retVal = checkPath(false, false, true, null, null, inputFile, encoding,
                                    (PathException) retVal.getLastException());
                        }
                        if ((retVal.getLiveReader() == null) && (retVal.getUrl() == null)) {
                            retVal = checkPath(false, false, true, null, null, inputFile, encoding,
                                    (PathException) retVal.getLastException());
                        }
                    }
                }
                if (retVal == null) {// no prefix
                    retVal = checkPath(false, false, true, null, null, inputFile, encodingOverride, null);
                }
            }
        }
        if (retVal != null) {
            retVal.setArguments(arguments);
            retVal.setPreAppendInputFile(preAppendInputFile);
        }
        return retVal;
    }

    URL useIfDirectoryOrNotFile(URL inURL) {
        if (inURL == null) {
            return null;
        }
        if (!((((URL) inURL).getProtocol() != null) && ((URL) inURL).getProtocol().toLowerCase().endsWith("file"))) { //$NON-NLS-1$
            return inURL;
        }
        try {
            if (new File(inURL.getFile()).isDirectory()) {
                return inURL;
            } else {
                return null;
            }
        } catch (NullPointerException e) {
            return null;
        }
    }

    URL useIfNotDirectoryOrNotFile(URL inURL) {
        if (inURL == null) {
            return null;
        }
        if (!((((URL) inURL).getProtocol() != null) && ((URL) inURL).getProtocol().toLowerCase().endsWith("file"))) { //$NON-NLS-1$
            return inURL;
        }
        try {
            if (new File(inURL.getFile()).isDirectory()) {
                return null;
            } else {
                return inURL;
            }
        } catch (NullPointerException e) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private void runExecuteFile() {
        if (getScriptRunnerContext().isStdin()) {
            getScriptRunnerContext().putProperty(ScriptRunnerContext.SQLCLI_STDIN_WAS_ON, Boolean.TRUE);
        }
        getScriptRunnerContext().putProperty(ScriptRunnerContext.SQLCLI_STDIN, Boolean.FALSE);

        getScriptRunnerContext().putProperty(ScriptRunnerContext.SQLPLUS_EXECUTE_FILE, Boolean.TRUE);
        if(getScriptRunnerContext().getRestrictedLevel().getLevel() > 2){
          getScriptRunnerContext().write(MessageFormat.format(Messages.getString("SQLPLUS.9"), "start")); //$NON-NLS-1$ //$NON-NLS-2$
          getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), MessageFormat.format(Messages.getString("SQLPLUS.9"), "start"), cmd.getSql());
          return;
        }
        Integer localDepth = 0;
        boolean localTopLevel = getScriptRunnerContext().getTopLevel();
        if ((getScriptRunnerContext().getTopLevel() == true)
                || (getScriptRunnerContext().getProperty(ScriptRunnerContext.SCRIPT_DEPTH)) == null) {
            localDepth = 1;
        } else {
            localDepth = (Integer) getScriptRunnerContext().getProperty(ScriptRunnerContext.SCRIPT_DEPTH);
        }
        URL baseURL = (URL) getScriptRunnerContext().getProperty(ScriptRunnerContext.BASE_URL);
        String previousInputFile = getScriptRunnerContext().getSourceRef(); // $NON-NLS-1$
        ExecutePathCheckReturn retVal = checkMultiplePaths(cmd);
        if (retVal != null && (retVal.getLiveReader() != null || retVal.getUrl() != null)) {
            try {
                if (retVal.getLiveReader() != null) {
                    _runner = new ScriptExecutor(retVal.getLiveReader(), conn);
                } else {
                    if (getScriptRunnerContext().getEncoding() != null) {
                        // use encoding if set
                        _runner = new ScriptExecutor(retVal.getUrl(), conn, getScriptRunnerContext().getEncoding());
                    } else {
                        _runner = new ScriptExecutor(retVal.getUrl(), conn);
                    }
                }

                // parse args taking into account ' " and whitespace.
                String[] parserArgs = ScriptUtils.executeArgs(retVal.getArguments());
                if (parserArgs != null) {
                    for (int argCount = 0; argCount < parserArgs.length; argCount++) {
                        Map<String, String> m = getScriptRunnerContext().getMap();
                        m.put(Integer.toString(argCount + 1), parserArgs[argCount]);
                    }
                }
                _runner.setOut(out);

                if (localDepth < 20) {
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.SCRIPT_DEPTH, localDepth + 1);
                    getScriptRunnerContext().setTopLevel(false);
                    _runner.setScriptRunnerContext(getScriptRunnerContext());

                    String pathval = null;
                    String inputFile = retVal.getOutputFile();
                    if (retVal.getUrl() == null) {
                        inputFile = retVal.getOutputFile();
                    } else {
                        inputFile = retVal.getUrl().toString();
                    }
                    setExecutorPath(_runner, inputFile, getScriptRunnerContext());
                    ArrayList<String> scrArrayList = ((ArrayList<String>) getScriptRunnerContext()
                            .getProperty(ScriptRunnerContext.APPINFOARRAYLIST));
                    scrArrayList.add(retVal.getPreAppendInputFile());
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
                    if (scrArrayList.size() != 0) {
                        SetAppinfo.setAppinfo(getScriptRunnerContext(), getScriptRunnerContext().getCurrentConnection(),
                                scrArrayList.get(scrArrayList.size() - 1), scrArrayList.size());
                    } else {
                        // should never happen we have just added to the list
                    }
                    _runner.run();
                    getScriptRunnerContext().removeProperty(ScriptRunnerContext.SQLPLUS_EXECUTE_FILE);
                    if (baseURL != null) {
                        getScriptRunnerContext().putProperty(ScriptRunnerContext.BASE_URL, baseURL);
                    }
                    scrArrayList = (ArrayList<String>) getScriptRunnerContext()
                            .getProperty(ScriptRunnerContext.APPINFOARRAYLIST);
                    if (scrArrayList.size() != 0) {
                        scrArrayList.remove(scrArrayList.size() - 1);
                    }
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
                    if (scrArrayList.size() != 0) {
                        SetAppinfo.setAppinfo(getScriptRunnerContext(), getScriptRunnerContext().getCurrentConnection(),
                                scrArrayList.get(scrArrayList.size() - 1), scrArrayList.size());
                    } else {
                        SetAppinfo.setAppinfo(getScriptRunnerContext(), getScriptRunnerContext().getCurrentConnection(),
                                PRODUCT_NAME, // $NON-NLS-1$p
                                0);
                    }
                    setScriptRunnerContext(_runner.getScriptRunnerContext());
                    this.setConn(_runner.getScriptRunnerContext().getCurrentConnection());// return
                                                                                            // the
                                                                                            // current
                                                                                            // connection,
                                                                                            // which
                                                                                            // could
                                                                                            // be
                                                                                            // a
                                                                                            // connection
                                                                                            // created
                                                                                            // within
                                                                                            // the
                                                                                            // file
                } else {
                    // does not appear to bomb out on whenever sqlerror|oserror
                    // exit
                    report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                            ScriptRunnerDbArb.getString(ScriptRunnerDbArb.MAX_NEST), this.getScriptRunnerContext()));
                }
            } catch (FileNotFoundException fnfe) {
                report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                        ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNABLE_TO_OPEN_FILE, retVal.getPreAppendInputFile()),
                        this.getScriptRunnerContext()));
                doWhenever(false);
            } catch (IOException ioe) {
                report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                        ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNABLE_TO_OPEN_FILE, retVal.getPreAppendInputFile()),
                        this.getScriptRunnerContext()));
                doWhenever(false);
            } catch (ArgumentQuoteException aqe) {
                report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                        ScriptRunnerDbArb.format(ScriptRunnerDbArb.QUOTEMISMATCH, aqe.getMessage()),
                        this.getScriptRunnerContext()));
                doWhenever(false);
            } finally {
                getScriptRunnerContext().setTopLevel(localTopLevel);
                getScriptRunnerContext().putProperty(ScriptRunnerContext.SCRIPT_DEPTH, localDepth);
                getScriptRunnerContext().setSourceRef(previousInputFile);// set
                                                                            // source
                                                                            // ref
                                                                            // back
                                                                            // to
                                                                            // original
                                                                            // even
                                                                            // on
                                                                            // error
                if (retVal != null && retVal.getLiveReader() != null) {
                    try {
                        retVal.getLiveReader().close();
                    } catch (IOException e) {
                        Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
                    }
                }
            }
        } else {
            if (retVal != null) {
                if (retVal.getLastException() != null) {
                    // gone to the trouble of tracking the exception better log
                    // it.
                    LOGGER.log(Level.FINE, retVal.getLastException().getStackTrace()[0].toString(),
                            retVal.getLastException());
                }
                report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                        ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNABLE_TO_OPEN_FILE, retVal.getPreAppendInputFile()),
                        this.getScriptRunnerContext()));
                doWhenever(false);
            } else {
                if (cmd.getModifiedSQL().toLowerCase().equals("@@")) { //$NON-NLS-1$
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.INVALID_FILE_NAME));
                } else {
                    getScriptRunnerContext()
                            .write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.INVALID_ARGUMENTS) + m_lineSeparator);
                }
            }
        }
        getScriptRunnerContext().removeProperty(ScriptRunnerContext.SQLPLUS_EXECUTE_FILE);
    }

    public static void setExecutorPath(ScriptExecutor runner, String inputFileOrUrl, ScriptRunnerContext ctx)
            throws IOException {
        String inputFile = inputFileOrUrl;
        String pathval;
        if (new File(inputFile).exists()) {
            pathval = (new File(inputFile)).getCanonicalPath();
            int ii = pathval.lastIndexOf("/"); //$NON-NLS-1$
            int iislash = pathval.lastIndexOf("\\"); //$NON-NLS-1$
            if (iislash > ii) {
                ii = iislash;
            }
            runner.getScriptRunnerContext().setSourceRef(pathval);
            pathval = pathval.substring(0, ii);
            runner.setDirectory(pathval);
        } else {// may be a url? handling urls with @@ could be a problem.
            if (FileUtils.startsWithHttpOrFtp(inputFile) && (FileUtils.haveIBytes(inputFile, ctx))) {
                String localInputFile = inputFile.replace("\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$
                runner.getScriptRunnerContext().setSourceRef(localInputFile);
                int ii = localInputFile.lastIndexOf("/"); //$NON-NLS-1$
                int iib = localInputFile.lastIndexOf("\\"); //$NON-NLS-1$
                if (iib > ii) {
                    ii = iib;
                }
                localInputFile = localInputFile.substring(0, ii);
                runner.setDirectory(localInputFile);
            } else if (FileUtils.startsWithHttpOrFtp(inputFile) && (FileUtils.haveIBytesRaw(inputFile))) {
                String localInputFile = inputFile.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
                runner.getScriptRunnerContext().setSourceRef(localInputFile);
                int ii = localInputFile.lastIndexOf("/"); //$NON-NLS-1$
                int iib = localInputFile.lastIndexOf("\\"); //$NON-NLS-1$
                if (iib > ii) {
                    ii = iib;
                }
                localInputFile = localInputFile.substring(0, ii);
                runner.setDirectory(localInputFile);
            } else {
                runner.getScriptRunnerContext().setSourceRef(""); //$NON-NLS-1$
                runner.setDirectory(""); //$NON-NLS-1$
            }
        }
    }

    private URL joinUrl(boolean tryUrlAsDir, URL inURL, String inputf) {
        try {
            if ((inURL == null) || (inputf == null)) {
                return null;
            }
            String urlString = inURL.toString();
            int lasts = urlString.lastIndexOf("/"); //$NON-NLS-1$
            int lastbs = urlString.lastIndexOf("\\"); //$NON-NLS-1$
            if (tryUrlAsDir) {// try as directory
                if (lastbs > lasts) {
                    urlString = urlString + "\\" + inputf.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                } else if (lasts > lastbs) {
                    urlString = urlString + "/" + inputf.replace("\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                } else { // both -1
                    return null;
                }
            } else {// remove/replace lead reference
                if (lastbs > lasts) {
                    urlString = urlString.substring(0, lastbs + 1) + inputf.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
                } else if (lasts > lastbs) {
                    urlString = urlString.substring(0, lasts + 1) + inputf.replace("\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$
                } else { // both -1
                    return null;
                }
            }
            return new URL(urlString);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean haveIBytes(URL tryNextURL) {
        if (tryNextURL != null) {
            InputStream is = null;
            URLConnection c = null;
            try {
                c = tryNextURL.openConnection(); // $NON-NLS-1$ //$NON-NLS-2$
                is = c.getInputStream();
                if (is.read() != -1) {
                    return true;
                }
            } catch (MalformedURLException e1) {
                return false;
            } catch (IOException e1) {
                return false;
            } catch (Exception e2) {// was getting illegal argument exception on
                                    // !"XYZ$% file input
                return false;
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (Exception e) {
                        // ignore.
                    }
                }
            }
            return false;
        }
        return false;
    }

    private InputStream getInputStream(String base) {
        if (FileUtils.startsWithHttpOrFtp(base) == false) {
            return null;
        }
        InputStream is = null;
        URLConnection c = null;
        try {
            String lower = base.toLowerCase();
            if (FileUtils.containsFile(lower)) { // $NON-NLS-1$
                base = base.substring(lower.indexOf("file:")); //$NON-NLS-1$
            }
            boolean wasException = false;
            try {
                c = new URL(base.replace("\\", "/")).openConnection(); //$NON-NLS-1$ //$NON-NLS-2$
            } catch (MalformedURLException e1) {
                wasException = true;
            } catch (IOException e1) {
                wasException = true;
            }
            if (wasException) {
                c = new URL(base.replace("//", "\\")).openConnection(); //$NON-NLS-1$ //$NON-NLS-2$
            }
            is = c.getInputStream();
        } catch (MalformedURLException e1) {
            return null;
        } catch (IOException e1) {
            return null;
        }
        return is;
    }

    private void runPrompt() {
        String localSql = cmd.getSql();
        // remove starting whitespace
        // find first whitespace character
        // assume prompt is the first word so then all you do is print the rest
        localSql = localSql.trim();
        boolean found = false;
        int i = 0;
        for (i = 0; i < localSql.length(); i++) {
            if (Character.isWhitespace(localSql.charAt(i))) {
                found = true;
                break;
            }
        }
        if (found == true) {
            report(getScriptRunnerContext(), localSql.substring(i).trim());
        } else {
            report(getScriptRunnerContext(), ""); //$NON-NLS-1$
        }
    }

    private void runPrint() {
        BindPrinter printer = new BindPrinter(getScriptRunnerContext(), out, getCmd());
        printer.runPrint();
    }

    private int indexOfFirstWhitespace(String value) {
        // throws null pointer if value is null
        for (int i = 0; i < value.length(); i++) {
            if (Character.isWhitespace(value.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    private void runVariable() {
        String[] stringArray = nextWordAndRest(cmd.getSql());
        if (stringArray[1].trim().equals("")) { //$NON-NLS-1$
            // print all variable names along with data types.
            Map<String, Bind> m = getScriptRunnerContext().getVarMap();
            Set<String> s = m.keySet();
            synchronized (m) { // Must be in synchronized block ??
                Iterator<String> i = s.iterator();
                if (i.hasNext()) {
                    while (i.hasNext()) {
                        String nextKey = i.next();
                        // String nextKey=(String) iter.next();
                        Bind bind = m.get(nextKey);
                        String type = bind.getType();
                        String datatypeBracket = bind.getBracket();
                        String type2Show = datatypeBracket != null ? datatypeBracket : type;
                        String msg = "variable {0}\ndatatype {1}\n"; //$NON-NLS-1$
                        msg = MessageFormat.format(msg, new Object[] { bind.getPrintBind(), type2Show });
                        report(getScriptRunnerContext(), msg);
                    }
                } else {
                    report(getScriptRunnerContext(), "SP2-0568: No bind variables declared.\n"); //$NON-NLS-1$
                    getScriptRunnerContext().errorLog("", "SP2-0568: No bind variables declared.", cmd.getSql());
                }
            }
        } else {
            String localSubName = ""; //$NON-NLS-1$
            if (indexOfFirstWhitespace(stringArray[1]) == -1) { // only one
                                                                // word, assume
                                                                // variable name
                Map<String, Bind> m = getScriptRunnerContext().getVarMap();
                Set<String> s = m.keySet();
                Iterator<String> i = s.iterator();
                boolean found = false;
                while (i.hasNext()) {
                    String nextKey = i.next();
                    if (stringArray[1].toLowerCase().equals(nextKey.toLowerCase())) {
                        Bind bind = m.get(nextKey);
                        String type = bind.getType();
                        String datatypeBracket = bind.getBracket();
                        String type2Show = datatypeBracket != null ? datatypeBracket : type;
                        String msg = "variable {0}\ndatatype {1}\n"; //$NON-NLS-1$
                        msg = MessageFormat.format(msg, new Object[] { bind.getPrintBind(), type2Show });
                        report(getScriptRunnerContext(), msg);
                        found = true;
                    }
                }
                if (!found) {
                	String msg = ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNDECLARED_VARIABLE, stringArray[1]);
                    report(getScriptRunnerContext(), msg);
                    getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), msg, cmd.getSql());

                }
            } else {
                localSubName = stringArray[1].substring(0, indexOfFirstWhitespace(stringArray[1])).trim();
                String dataType = ""; //$NON-NLS-1$
                dataType = stringArray[1].substring(indexOfFirstWhitespace(stringArray[1]) + 1).trim();
                String dataTypeBracket = validVariableDatatype(dataType, true);
                dataType = validVariableDatatype(dataType, false);

                if (Character.isLetter(localSubName.charAt(0))) {
                    try {
                        if ((dataTypeBracket == null) || (dataType == null)
                                || (dataTypeBracket.replace(dataType, "").trim().equals("(0)"))) { //$NON-NLS-1$ //$NON-NLS-2$
                        	String msg =ScriptRunnerDbArb.format(ScriptRunnerDbArb.ILLEGAL_USAGE, localSubName);
                            report(getScriptRunnerContext(), msg);
                            getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), msg, cmd.getSql());
                            return;
                        }
                        if ((dataTypeBracket != null) || (dataType != null)) {
                            String num_dtype = dataTypeBracket.replaceFirst(dataType, "").replace("(", "") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                    .replace(")", "").trim(); //$NON-NLS-1$ //$NON-NLS-2$
                            String[] dtype = num_dtype.split(" "); //$NON-NLS-1$
                            // was erroring out on no number types
                            if (dataTypeBracket.indexOf("(") != -1 && dtype.length > 0) { //$NON-NLS-1$
                                long size = Long.parseLong(dtype[0].trim());
                                // Bug 21430002 - GET NO ERROR WHEN DEFINE
                                // VARIABLE OF TYPE CHAR WITH WRONG LENGTH
                                // QUALIFIERS
                                if (dtype.length > 1) {
                                    String dataTypeLower=dataType.toLowerCase(Locale.US);
                                    String second=dtype[1];
                                    if (second!=null) {
                                        second=second.toLowerCase(Locale.US);
                                    } else {
                                        second="";  //$NON-NLS-1$ 
                                    }
                                    if (dtype.length>2||(!(((dataTypeLower.equals("char"))||(dataTypeLower.equals("varchar2")))&&((second.equals("byte"))||(second.equals("char")))))) { //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
                                        /* this is just plain wrong will mitigate by skipping if second argument byte or char - and char or varchar2. */
                                        //if (validVariableDatatype(dtype[1].trim(), false) == null) {
                                        report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.ILLEGAL_USAGE, localSubName));
                                        getScriptRunnerContext().errorLog(getScriptRunnerContext().getSourceRef(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.ILLEGAL_USAGE, localSubName), cmd.getSql());
                                        return;
                                    }
                                    if (dtype[1].trim().equalsIgnoreCase("char") //$NON-NLS-1$
                                            || dtype[1].trim().equalsIgnoreCase("byte")) { //$NON-NLS-1$
                                        // NLS_LENGTH_SEMANTICS to be resolved.
                                        if (size > 2000) {
                                            getScriptRunnerContext().write(ScriptRunnerDbArb.getString(
                                                    ScriptRunnerDbArb.INVALID_VARIABLE_LENGTH) + m_lineSeparator);
                                            return;
                                        } else if (size < 1) {
                                            report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.ILLEGAL_USAGE,
                                                    localSubName));
                                            return;
                                        }
                                    }
                                }
                            }
                            Bind bind = new Bind(localSubName.toUpperCase(), dataType.toUpperCase(),
                                    dataTypeBracket.toUpperCase(), localSubName, null, localSubName);
                            bind.setPrintBind(localSubName);
                            Map<String, Bind> m = getScriptRunnerContext().getVarMap();
                            m.put(localSubName.toUpperCase(), bind);
                        }
                    } catch (NumberFormatException e) {
                        getScriptRunnerContext()
                                .write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.INVALID_VARIABLE_LENGTH)
                                        + m_lineSeparator);
                    }
                } else {
                    report(getScriptRunnerContext(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.ILLEGAL_VARIABLE, localSubName));
                }
            }
        }
    }

    private boolean runAliases() throws ArgumentQuoteException {
        String localSql = cmd.getSql();
        String[] tokens = localSql.split("\\s+"); //$NON-NLS-1$
        String command = ""; //$NON-NLS-1$
        String args = ""; //$NON-NLS-1$
        if (tokens.length > 0) {
            String theText = ""; //$NON-NLS-1$
            if ((tokens.length > 1) && Aliases.getInstance().contains(tokens[0] + tokens[1])) {
                command = tokens[0] + tokens[1];
                if (tokens.length > 2) {
                    args = cmd.getSql().trim().replaceAll("^\\s*" + tokens[0] + "\\s+" + tokens[1] + "\\s+", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                }

                // removefirst two words, then use rest as substitutions
                theText = Aliases.getInstance().get(command).getQuery();
            } else if (Aliases.getInstance().contains(tokens[0])) {
                command = tokens[0];
                if (tokens.length > 1) {
                    args = cmd.getSql().trim().replaceAll("^\\s*" + tokens[0] + "\\s+", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                }
                // remove first words, then use rest as substitutions
                theText = Aliases.getInstance().get(command).getQuery();
            }
            if ((theText != null) && (!theText.equals(""))) { //$NON-NLS-1$
                ArrayList<String> bindsToBeRemoved = new ArrayList<String>();
                String errorMessage = ""; //$NON-NLS-1$
                // this is the efault and only for now:

                // parse statement - assumption no subscripts nested aliases
                // set binds not already set with binds in argument sequence
                // at end remove these set binds.
                Integer localDepth = 0;
                String localSourceRef = getScriptRunnerContext().getSourceRef();
                boolean localTopLevel = getScriptRunnerContext().getTopLevel();
                if ((getScriptRunnerContext().getTopLevel() == true)
                        || (getScriptRunnerContext().getProperty(ScriptRunnerContext.SCRIPT_DEPTH)) == null) {
                    localDepth = 1;
                } else {
                    localDepth = (Integer) getScriptRunnerContext().getProperty(ScriptRunnerContext.SCRIPT_DEPTH);
                }
                Boolean lastEOF=(Boolean)getScriptRunnerContext().getProperty(ScriptRunnerContext.ALIAS_END_ON_EOF);
                if (lastEOF==null) {
                    lastEOF=Boolean.FALSE;
                }
                try {
                	getScriptRunnerContext().putProperty(ScriptRunnerContext.ALIAS_END_ON_EOF,Boolean.TRUE);
                    ScriptParser sp = new ScriptParser(theText);
                    if (getScriptRunnerContext().isCommandLine()) {
                        sp.setScriptRunnerContext(getScriptRunnerContext());
                    }
                    // parse args taking into account ' " and whitespace.
                    String[] parserArgs = ScriptUtils.executeArgs(args);
                    if (parserArgs == null) {
                        // returns null for none I assume none = empty array.
                        parserArgs = new String[] {};
                    }
                    int nextArg = 0;
                    ISQLCommand icmd = null;
                    while ((icmd = sp.next()) != null) {

                        try {
                            // TODO nice if listing binds of an alias were
                            // visible to the user.
                            // TODO - we may need to restrict the commands
                            // parsed. First cut try get binds everywhere and
                            // ignore/log exceptions.
                            List<Bind> parserdBinds = Parser.getInstance().getBinds(icmd.getSql(), false);
                            for (Bind b : parserdBinds) {
                                if (!getScriptRunnerContext().getVarMap().containsKey(b.getName().toUpperCase())) {
                                    if (nextArg < parserArgs.length) {
                                        // TODO nice if argument value could be
                                        // a bind - not sure what level to
                                        // change that in, does :fred as a bind
                                        // argument = ':fred', or the the value
                                        // of fred variable.
                                        // cludge workaround for now - pass
                                        // :fred is as a substitution variable
                                        // and refer to it as &&1
                                        Bind bind = new Bind(b.getName().toUpperCase(), "VARCHAR2", "VARCHAR2(1000)", //$NON-NLS-1$ //$NON-NLS-2$
                                                b.getName().toUpperCase(), parserArgs[nextArg],
                                                b.getName().toUpperCase());
                                        nextArg++;
                                        getScriptRunnerContext().getVarMap().put(b.getName().toUpperCase(), bind);
                                        bindsToBeRemoved.add(b.getName().toUpperCase());
                                    } else {
                                        errorMessage = ScriptRunnerDbArb
                                                .getString(ScriptRunnerDbArb.ALIASWITHBINDS_NOTENOUGH); // $NON-NLS-1$
                                    }
                                }
                            }
                        } catch (Throwable t) {// warn and keep going - getbinds
                                                // might barf on say sqlplus
                                                // command
                            Logger.getLogger(this.getClass().getName()).log(Level.INFO, t.getMessage());
                        }
                    }
                    if (nextArg != parserArgs.length) {
                        report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ALIASWITHBINDS_TOOMANY)); // $NON-NLS-1$
                    }

                    if (localDepth > 20) {
                        if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) != null
                                && Boolean.parseBoolean(getScriptRunnerContext()
                                        .getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
                            errorMessage = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.MAX_NEST_CLASSIC);
                        } else {
                            // does not appear to bomb out on whenever
                            // sqlerror|oserror exit
                            errorMessage = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.MAX_NEST);
                        }
                    }
                    // doit:
                    if (errorMessage.equals("")) { //$NON-NLS-1$
                        // mild duplication same as non binds except substition
                        // arguments &&1 &&2 not set
                        getScriptRunnerContext().putProperty(ScriptRunnerContext.SCRIPT_DEPTH, localDepth + 1);
                        getScriptRunnerContext().setTopLevel(false);
                        _runner = new ScriptExecutor(theText, conn);
                        _runner.setOut(out);
                        getScriptRunnerContext().setSourceRef("/ALIAS/" + tokens[0]); //$NON-NLS-1$
                        _runner.setScriptRunnerContext(getScriptRunnerContext());
                        _runner.setDirectory(this.getDirectory());
                        _runner.run();
                        setScriptRunnerContext(_runner.getScriptRunnerContext());
                        this.setConn(_runner.getScriptRunnerContext().getCurrentConnection());// return
                                                                                                // the
                                                                                                // current
                                                                                                // connection,
                                                                                                // which
                                                                                                // could
                                                                                                // be
                                                                                                // a
                                                                                                // connection
                                                                                                // created
                                                                                                // within
                                                                                                // the
                                                                                                // file
                    } else {
                        report(getScriptRunnerContext(), errorMessage);
                        errorMessage = ""; //$NON-NLS-1$
                    }
                    // always undo what you did if you did nothing there is
                    // nothing to undo.
                    for (String bindName : bindsToBeRemoved) {
                        getScriptRunnerContext().getVarMap().remove(bindName);
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ioe.getLocalizedMessage());
                } finally {
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.ALIAS_END_ON_EOF, lastEOF);
                    for (String bindName : bindsToBeRemoved) {
                        getScriptRunnerContext().getVarMap().remove(bindName);
                    }
                    getScriptRunnerContext().setTopLevel(localTopLevel);
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.SCRIPT_DEPTH, localDepth);
                    getScriptRunnerContext().setSourceRef(localSourceRef);
                }
                if ((errorMessage != null) && (!errorMessage.equals(""))) { //$NON-NLS-1$
                    report(getScriptRunnerContext(), errorMessage);
                }
                return true;
            }
        }
        return false;
    }

    private void runExecute() {
        // handle continuation
        cmd.setSql(SQLPLUS.removeDashNewline(cmd, cmd.getSql()));
        // convert exec execu execut execute into begin cmd.getsql; end;
        String localSql = cmd.getSql();
        // remove starting whitespace
        // find first whitespace character
        // assume execute is the first word so then all you do is print the rest

        localSql = localSql.trim();
        boolean found = false;
        for (int i = 0; i < localSql.length(); i++) {
            if (Character.isWhitespace(localSql.charAt(i))) {
                found = true;
                localSql = localSql.substring(i);
                break;
            }
        }
        if (found == true) {
            String anonymousBlock = "BEGIN" + localSql + //$NON-NLS-1$
                    (localSql.endsWith(";") ? "" : ";") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    + " END;"; //$NON-NLS-1$
            Literals2Binds bp = null;
            if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SECURELITERALS)==null) {
                if ((cmd.getEndLine()-cmd.getStartLine())<20) {
                    bp = Literals2Binds.bindAid(anonymousBlock);
                } else {
                    //off
                    bp = Literals2Binds.Literals2BindsNull(anonymousBlock);
                }
            } else {
                if (getScriptRunnerContext().getProperty(ScriptRunnerContext.SECURELITERALS).equals("ON")) { //$NON-NLS-1$
                    bp = Literals2Binds.bindAid(anonymousBlock);
                }else {
                    bp = Literals2Binds.Literals2BindsNull(anonymousBlock);
                }
            }
            cmd.setSql(bp.getSql());
            cmd.setSQLOrig(anonymousBlock);
            PLSQL plsql = new PLSQL(cmd, out);
            Map<String, String> bindsMap = bp.getBinds();
            for (String bindName : bindsMap.keySet()) {
                Bind b = new Bind();
                b.setName(bindName);
                b.setType("VARCHAR2"); //$NON-NLS-1$
                b.setValue(bindsMap.get(bindName));
                getScriptRunnerContext().getVarMap().put(bindName, b);
            }
            plsql.setScriptRunnerContext(getScriptRunnerContext());
            plsql.setConn(conn);
            // plsql.setStatusLine(statusLine);
            try {
                plsql.run();
                // plsql.start();
            } finally {
                for (String bindName : bindsMap.keySet())
                    getScriptRunnerContext().getVarMap().remove(bindName);
                plsql = null;
            }
        } else {
            if (localSql.trim().toLowerCase().equals("exec") || localSql.trim().toLowerCase().equals("execute")) { //$NON-NLS-1$ //$NON-NLS-2$
                report(getScriptRunnerContext(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.EXEC_COMMAND));
            } else
                report(getScriptRunnerContext(), ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                        ScriptRunnerDbArb.format(ScriptRunnerDbArb.MALFORMED_EXECUTE_CONTENT, cmd.getSql()),
                        this.getScriptRunnerContext()));
        }
    }

    /**
     * added with bracket which returns length if available i.e. for character
     * types.
     */
    protected String validVariableDatatype(String str, boolean withBracket) {
        String dataTypeRegex = null;
        StringBuilder sb = new StringBuilder();
        sb.append("NUMBER"); //$NON-NLS-1$
        sb.append("|CHAR([\\s]*\\([\\s]*\\d+[\\s]*[CHAR|BYTE]*[\\s]*\\))*"); //$NON-NLS-1$
        sb.append("|NCHAR([\\s]*\\([\\s]*\\d+[\\s]*\\))*"); //$NON-NLS-1$
        sb.append("|VARCHAR2([\\s]*\\([\\s]*\\d+[\\s]*[CHAR|BYTE]*[\\s]*\\))*"); //$NON-NLS-1$
        sb.append("|NVARCHAR2([\\s]*\\([\\s]*\\d+[\\s]*\\))*"); //$NON-NLS-1$
        sb.append("|CLOB"); //$NON-NLS-1$
        sb.append("|NCLOB"); //$NON-NLS-1$
        sb.append("|BLOB"); //$NON-NLS-1$
        sb.append("|BFILE"); //$NON-NLS-1$
        sb.append("|REFCURSOR"); //$NON-NLS-1$
        sb.append("|BINARY_FLOAT"); //$NON-NLS-1$
        sb.append("|BINARY_DOUBLE"); //$NON-NLS-1$
        dataTypeRegex = sb.toString();
        Pattern pat = Pattern.compile(dataTypeRegex);
        Matcher matcher = pat.matcher(str.toUpperCase());
        if (matcher.matches()) {
            String ret = matcher.group();
            if (withBracket && (ret.indexOf(")") > 0)) { //$NON-NLS-1$
                return ret.substring(0, ret.indexOf(")") + 1).trim().toUpperCase(); //$NON-NLS-1$
            } else if (ret.indexOf("(") > 0) {//$NON-NLS-1$
                return ret.substring(0, ret.indexOf("(")).trim().toUpperCase(); //$NON-NLS-1$
            } else {
                return ret.trim().toUpperCase();
            }
        }
        return null;
    }

    /**
     * need to add hostname connection paths: if get tnsnames.ora entry fails
     * 
     * @fred (already there and works if default service set)
     * @fred:1521/fred
     * @fred:1521/yourmachine.yourdomain not considered sid or specifying not 1521
     *                               Most common easiest usecase machine ->
     *                               machine:1521/ default listener
     * @param connectName
     * @param connectPassword
     * @param connectDB
     * @param role
     * @return
     */
    private Connection getConnection(ArrayList<String> urlMessage, String connectName, String connectPassword,
            String connectDB, String role) {
        Connection myconn = null;
        String successfulAttempt = null;
        Properties props = new Properties();
        props.setProperty("v$session.program", SQLPLUS.PRODUCT_NAME); //$NON-NLS-1$ //$NON-NLS-2$
        if (connectName != null) {
            if (connectName.length() > 0) {
                // might conflict with barries no username fix
                String altered = connectName;
                if (role != null && role.length() > 1) {
                    String retVal[] = nextWordAndRest(role);
                    if (retVal[0].trim().equalsIgnoreCase("as")) { //$NON-NLS-1$
                        if (!((retVal[1].trim().equalsIgnoreCase("sysdba") //$NON-NLS-1$
                                || (retVal[1].trim().equalsIgnoreCase("sysoper")) //$NON-NLS-1$
                                || (retVal[1].trim().equalsIgnoreCase("sysasm"))))) { // handled //$NON-NLS-1$
                                                                                        // as
                                                                                        // internal
                                                                                        // login
                            altered += " as " + retVal[1].trim().toLowerCase(); //$NON-NLS-1$
                        }
                    }
                }
                props.setProperty("user", altered); //$NON-NLS-1$
            }
        }
        if (connectPassword != null) {
            if (connectPassword.length() > 0)
                props.setProperty("password", connectPassword); //$NON-NLS-1$
        }
        String thirdURL = connectDB;

        if ((connectName != null)) { // $NON-NLS-1$
            if ((connectPassword != null)) { // $NON-NLS-1$
                thirdURL = "PASSWORD=" + connectPassword + "]" + thirdURL; //$NON-NLS-1$ //$NON-NLS-2$
            } else {// need password field - if this breaks supply full url with
                    // [] youself
                thirdURL = "PASSWORD=]" + thirdURL; //$NON-NLS-1$ //$NON-NLS-2$
            }
            thirdURL = "[USERNAME=" + connectName + "," + thirdURL; //$NON-NLS-1$ //$NON-NLS-2$
        }
        // Note design decision - blank username passwords are reported as "" if
        // you use username/password@url
        // provide your own bridge style urls including username password if
        // that does not suit,
        // optionally prefixed with [].
        // note two connectionDetails as needed to specify this one explicitly
        // try 3rd party - will not set driver string if url is not 3rd party.
        oracle.dbtools.db.ConnectionDetails cd = null;
        try {
            if (!hasUrlStub(connectDB)) {
                cd = new oracle.dbtools.db.ConnectionDetails(thirdURL);
            }
        } catch (Exception e) {
            // do not want to fail badly if 3rd party connection parsing fails
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        }
        if (cd != null && cd.getDriver() != null) {
            myconn = cd.getConn();
        } else {
            if (role != null && role.length() > 1) {
                String retVal[] = nextWordAndRest(role);
                if (retVal[0].trim().equalsIgnoreCase("as") && retVal[1].trim().equalsIgnoreCase("sysdba")) //$NON-NLS-1$ //$NON-NLS-2$
                    props.setProperty("internal_logon", "SYSDBA"); //$NON-NLS-1$ //$NON-NLS-2$
                if (retVal[0].trim().equalsIgnoreCase("as") && retVal[1].trim().equalsIgnoreCase("sysoper")) //$NON-NLS-1$ //$NON-NLS-2$
                    props.setProperty("internal_logon", "SYSOPER"); //$NON-NLS-1$ //$NON-NLS-2$
                if (retVal[0].trim().equalsIgnoreCase("as") && retVal[1].trim().equalsIgnoreCase("sysasm")) //$NON-NLS-1$ //$NON-NLS-2$
                    props.setProperty("internal_logon", "SYSASM"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            Boolean useOci = (Boolean) this.getScriptRunnerContext()
                    .getProperty(ScriptRunnerContext.DBCONFIG_USE_THICK_DRIVER);
            if ((connectDB == null) || (connectDB.equals(""))) { //$NON-NLS-1$
                if (getScriptRunnerContext().getProperty(ScriptRunnerContext.JLINE_SETTING) != null) {
                    // only check environmental variables if in commend line
                    // mode - i.e. do not change existing behaviour
                    String envToCheck = "TWO_TASK"; //$NON-NLS-1$
                    if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) { //$NON-NLS-1$ //$NON-NLS-2$
                        envToCheck = "LOCAL"; //$NON-NLS-1$
                    }
                    String envValue = System.getenv(envToCheck);
                    if ((envValue != null) && (!envValue.equals(""))) { //$NON-NLS-1$
                        connectDB = envValue;
                    }
                }
            }
            if ((connectDB == null) || (connectDB.equals(""))) { //$NON-NLS-1$
                try {
                    Connection lastConn = getScriptRunnerContext().getBaseConnection();
                    if (getScriptRunnerContext().getCurrentConnection() != null) {
                        lastConn = getScriptRunnerContext().getCurrentConnection();
                        // if disconnected we want to try bequeath.
                        if ((getScriptRunnerContext() != null)
                                && (getScriptRunnerContext().getProperty(ScriptRunnerContext.NOLOG) != null)
                                && (((Boolean) (getScriptRunnerContext().getProperty(ScriptRunnerContext.NOLOG))
                                        .equals(Boolean.TRUE)))) {
                            lastConn = null;
                        }
                        //if ScriptUtils.inhttp -> do not try loopback
                        if (lastConn!=null&&ScriptUtils.isHttpCon(lastConn, getScriptRunnerContext())) {
                            lastConn = null;
                        }
                    }

                    // try url if you are connected and not (classic and command
                    // line)
                    if ((lastConn != null) && (!(getScriptRunnerContext().isCommandLine()
                            && (getScriptRunnerContext().isSQLPlusClassic())))) { // $NON-NLS-1$
                        String url = ""; //$NON-NLS-1$
                        if (LockManager.lock(lastConn)) {
                            try {
                                url = (lastConn).getMetaData().getURL();
                            } finally {
                                LockManager.unlock(lastConn);
                            }
                        }
                        myconn = logConnectionURL(urlMessage, connectName, url, props, true);

                    } else {
                        myconn = null;
                        // try beq even if oci on
                        try {
                            if (useOci) {
                                String url = "jdbc:oracle:oci8:@"; //$NON-NLS-1$
                                myconn = logConnectionURL(urlMessage, connectName, url, props, useOci, true);// trying
                                                                                                                // bequeath
                                                                                                                // anyway
                                                                                                                // -
                                                                                                                // but
                                                                                                                // do
                                                                                                                // not
                                                                                                                // report
                                                                                                                // error
                                                                                                                // if
                                                                                                                // oci
                                                                                                                // not
                                                                                                                // flgged
                            }
                        } catch (Throwable sqle) {
                            // to report this would be too noisy - we print out
                            // the url of eventual connection.
                        }
                        if (myconn == null) {
                            try {
                                // Oracle_sid - does not seem to be a default
                                // service name - in the registry perhaps
                                String oracleSid = System.getenv("ORACLE_SID"); //$NON-NLS-1$
                                if ((oracleSid != null) && (!oracleSid.equals(""))) { //$NON-NLS-1$
                                    String url = "jdbc:oracle:thin:@127.0.0.1:1521:" + System.getenv("ORACLE_SID"); //$NON-NLS-1$ //$NON-NLS-2$
                                    myconn = logConnectionURL(urlMessage, connectName, url, props, true);
                                }
                            } catch (Throwable sqle2) {
                            }
                        }
                        if (myconn == null) {
                            try {
                                String url = "jdbc:oracle:thin:@localhost:1521/orcl"; //$NON-NLS-1$
                                myconn = logConnectionURL(urlMessage, connectName, url, props, false);
                            } catch (Throwable sqle2) {
                            }
                        }
                        if (myconn == null) {
                            try {
                                String url = "jdbc:oracle:thin:@localhost:1521/xe"; //$NON-NLS-1$
                                myconn = logConnectionURL(urlMessage, connectName, url, props, false);
                            } catch (Throwable sqle2) {
                            }
                        }
                    }
                } catch (SQLException e) {
                    getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, e.getMessage());
                    try {
                        if ((myconn != null) && (!myconn.isClosed())) {
                            myconn.close();
                        }
                    } catch (SQLException ee) {
                        // do not report secondary error;
                    }
                    myconn = null;
                } catch (Throwable e) {
                    myconn = null;
                }
            } else {
                int match = -1;
                myconn = null;
                // assume if set LDAPCON is set or environmental variable
                // LDAPCON is set you want to try this first.
                String tryLDAP = null;
                if ((getScriptRunnerContext().getProperty(ScriptRunnerContext.LDAPCON) != null)
                        && ((((String) getScriptRunnerContext().getProperty(ScriptRunnerContext.LDAPCON))
                                .contains("#ENTRY#")))) { //$NON-NLS-1$
                    tryLDAP = ((String) getScriptRunnerContext().getProperty(ScriptRunnerContext.LDAPCON))
                            .replace("#ENTRY#", connectDB); //$NON-NLS-1$
                }
                if ((tryLDAP == null)
                        && ((System.getenv("LDAPCON") != null) && ((System.getenv("LDAPCON").contains("#ENTRY#"))))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    tryLDAP = System.getenv("LDAPCON").replace("#ENTRY#", connectDB); //$NON-NLS-1$ //$NON-NLS-2$
                }
                if (tryLDAP != null) {
                    try {
                        myconn = logConnectionURL(urlMessage, connectName, tryLDAP, props, true);
                    } catch (Throwable e) {
                        // we have filled urlMessage - and myconn=null
                    }
                }
                // myconn==null if ldap did not work (assumption ldap #entry#
                // lookup is not going to be in tnsnames.ora {
                //need to remember pre tnsnames lookup as oci wallet auth uses it as a key
                String preExpand=null;
                ArrayList<TCPTNSEntry> myTCPEntry = null;
                if (myconn == null) {
                    myTCPEntry = TNSHelper.getTNSEntries();
                    if (myTCPEntry != null) {
                        for (int i = 0; i < myTCPEntry.size(); i++) {
                            // a problem assume it is up to the first . if there
                            // is a dot
                            if ((myTCPEntry.get(i).getName().toUpperCase().equals(connectDB.toUpperCase()))) {
                                preExpand=connectDB;
                                match = i;
                                break;
                            }
                        }
                    }
                }
                if ((match == -1) && (/* not easy connect */connectDB.indexOf("/") == -1) //$NON-NLS-1$
                        && (connectDB.indexOf("\\") == -1) //$NON-NLS-1$
                        && (connectDB.indexOf(":") == -1)) { //$NON-NLS-1$
                    String connectDBnoDot = connectDB;
                    int indexdot = connectDBnoDot.indexOf("."); //$NON-NLS-1$
                    if (indexdot != -1) {
                        connectDBnoDot = connectDBnoDot.substring(0, indexdot);
                    }
                    if (myTCPEntry != null) {
                        for (int i = 0; i < myTCPEntry.size(); i++) {
                            // a problem assume it is up to the first . if there
                            // is a dot
                            String theEntry = myTCPEntry.get(i).getName();
                            int indexdot2 = theEntry.indexOf("."); //$NON-NLS-1$
                            if (indexdot2 != -1) {
                                theEntry = theEntry.substring(0, indexdot2);
                            }
                            if ((theEntry.toUpperCase().equals(connectDBnoDot.toUpperCase()))) {
                                match = i;
                                break;
                            }
                        }
                    }
                    if ((match == -1) && getScriptRunnerContext().isCommandLine()
                            && ((getScriptRunnerContext().getProperty(
                                    ScriptRunnerContext.SETNET) == null)
                            || ((String) getScriptRunnerContext().getProperty(ScriptRunnerContext.SETNET) != null
                                    && (!((String) getScriptRunnerContext().getProperty(ScriptRunnerContext.SETNET))
                                            .equals(NetCommand.OFF))))) {
                        String netVal = NetEntries.getInstance().get(connectDB);
                        if (netVal != null) {
                            String lookUp = netVal;
                            if (hasUrlStub(lookUp)) {
                                try {
                                    myconn = logConnectionURL(urlMessage, connectName, lookUp, props, true);
                                } catch (Throwable ee) {
                                    myconn = null;
                                    getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE,
                                            ee.getMessage());

                                }
                            } else {
                                try {
                                    if (useOci) {
                                        String url = "jdbc:oracle:oci8:@" + lookUp; //$NON-NLS-1$
                                        myconn = logConnectionURL(urlMessage, connectName, url, props, true);
                                    } else {
                                        throw new Exception();
                                    }
                                } catch (Throwable e) {
                                    try {
                                        String url = "jdbc:oracle:thin:@" + lookUp; //$NON-NLS-1$
                                        myconn = logConnectionURL(urlMessage, connectName, url, props, true);
                                    } catch (Throwable ee) {
                                        myconn = null;
                                        getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE,
                                                ee.getMessage());

                                    }
                                }
                            }
                        }
                    }
                }

                if (useOci == null) {
                    useOci = Boolean.FALSE;
                }
                if ((match == -1)
                        && (myconn == null)) /*
                                                 * notnsnames entry - note
                                                 * possible . confusion
                                                 */ {
                    if (hasUrlStub(connectDB)) {
                        try {
                            myconn = logConnectionURL(urlMessage, connectName, connectDB, props, true);
                        } catch (Throwable ee) {
                            myconn = null;
                            getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE,
                                    ee.getMessage());

                        }
                    } else {
                        try {
                            if (useOci) {
                                String url = "jdbc:oracle:oci8:@" + connectDB; //$NON-NLS-1$
                                myconn = logConnectionURL(urlMessage, connectName, url, props, true);
                                successfulAttempt = connectDB;
                            } else {
                                throw new Exception();
                            }
                        } catch (Throwable e) {
                            try {
                                String url = "jdbc:oracle:thin:@" + connectDB; //$NON-NLS-1$
                                myconn = logConnectionURL(urlMessage, connectName, url, props, true);
                                successfulAttempt = connectDB;
                            } catch (Throwable ee) {
                                myconn = null;
                                getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE,
                                        ee.getMessage());

                            }
                        }
                    }
                    if (myconn == null) {
                        // new last chance put in port1521
                        if ((!this.hasUrlStub(connectDB)) && connectDB.indexOf("(") == -1) { //$NON-NLS-1$
                            String startConnectDB = connectDB;
                            // rewrite host/service as host:1521/service
                            if (!connectDB.matches("^[^:]+\\:[0-9]+\\/.*") && ((connectDB.indexOf("/") != -1) //$NON-NLS-1$ //$NON-NLS-2$
                                    && connectDB.substring(connectDB.indexOf("/") + 1).indexOf("/") == -1)) { //$NON-NLS-1$ //$NON-NLS-2$
                                // no port number and only one /
                                connectDB = connectDB.replaceAll("/", ":1521/"); //$NON-NLS-1$ //$NON-NLS-2$
                                try {
                                    if (useOci) {
                                        String url = "jdbc:oracle:oci8:@" + connectDB; //$NON-NLS-1$
                                        myconn = logConnectionURL(urlMessage, connectName, url, props, true);
                                        successfulAttempt = connectDB;
                                    } else {
                                        throw new Exception();
                                    }
                                } catch (Throwable e) {
                                    try {
                                        String url = "jdbc:oracle:thin:@" + connectDB; //$NON-NLS-1$
                                        myconn = logConnectionURL(urlMessage, connectName, url, props, true);
                                        successfulAttempt = connectDB;
                                    } catch (Throwable ee) {
                                        myconn = null;
                                    }
                                }
                            }
                            // issue ipv6 has multiple :
                            if ((myconn == null) && ((startConnectDB.indexOf(":") == -1) || //$NON-NLS-1$
                                    ((!startConnectDB.endsWith(":")) //$NON-NLS-1$ //$NON-NLS-2$
                                            && (startConnectDB.substring(startConnectDB.indexOf(":") + 1) //$NON-NLS-1$
                                                    .indexOf(":") != -1)) //$NON-NLS-1$
                                            && (startConnectDB.indexOf("/") == -1))) { //$NON-NLS-1$ //$NON-NLS-2$
                                // need to try as index of . and as raw if
                                // indexOf . != -1
                                // then try a resolve of startConnectDB domain
                                // name.
                                String noDot = null;
                                String withDot = null;
                                String startConnectDB2 = null;
                                try {
                                    // go name -> address -> name so we get the
                                    // yourdomain
                                    InetAddress addr = InetAddress
                                            .getByAddress(((InetAddress.getByName(startConnectDB)).getAddress()));
                                    startConnectDB2 = addr.getCanonicalHostName();
                                } catch (UnknownHostException uhe) {
                                    startConnectDB2 = null;
                                }
                                // try both name and resolved name
                                String[] both = new String[] { startConnectDB, startConnectDB2 };
                                for (String ident : both) {
                                    if (ident == null) {
                                        continue;
                                    }

                                    if (myconn != null) {
                                        break;
                                    }
                                    if (ident.indexOf(".") == -1) { //$NON-NLS-1$
                                        noDot = ident;
                                    } else {
                                        noDot = ident.substring(0, ident.indexOf(".")); //$NON-NLS-1$
                                    }
                                    boolean onlyNumber = true;
                                    try {
                                        Integer.parseInt(noDot);
                                    } catch (NumberFormatException nfe) {
                                        onlyNumber = false;
                                    }
                                    try {
                                        if (!onlyNumber) {
                                            String url = "jdbc:oracle:thin:@" + ident + ":1521/" + noDot; //$NON-NLS-1$ //$NON-NLS-2$

                                            myconn = logConnectionURL(urlMessage, connectName, url, props, false);
                                            successfulAttempt = ident + ":1521/" + noDot; //$NON-NLS-1$
                                        }
                                    } catch (Throwable ee) {
                                        myconn = null;
                                    }
                                    if ((myconn == null) && (ident.indexOf(".") != -1)) { //$NON-NLS-1$
                                        withDot = ident;
                                        try {
                                            if (!onlyNumber) {
                                                String url = "jdbc:oracle:thin:@" + ident + ":1521/" + withDot; //$NON-NLS-1$ //$NON-NLS-2$
                                                myconn = logConnectionURL(urlMessage, connectName, url, props, false);
                                                successfulAttempt = ident + ":1521/" + withDot; //$NON-NLS-1$
                                            }
                                        } catch (Throwable ee) {
                                            myconn = null;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (myconn == null) {
                    if (hasUrlStub(myTCPEntry.get(match).getSpecUrl())) {
                        try {
                            myconn = logConnectionURL(urlMessage, connectName, myTCPEntry.get(match).getSpecUrl(),
                                    props, true);
                        } catch (Throwable ee) {
                            myconn = null;
                            getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE,
                                    ee.getMessage());
                        }
                    } else {
                        try {
                            if (useOci) {
                                /*
                                 * String url =
                                 * myTCPEntry.get(match).getJDBCUrl();
                                 */
                                // Make sure we have a JDBC oci url here. We are
                                // using TNS connection.
                                String url = "";  //$NON-NLS-1$
                                //wallet uses the alias name as a key to look up authenticated username/password.
                                if (preExpand==null||preExpand.equals("")) {  //$NON-NLS-1$
                                    url = "jdbc:oracle:oci8:@" + myTCPEntry.get(match).getSpecUrl(); //$NON-NLS-1$
                                } else {
                                    url="jdbc:oracle:oci8:@" + preExpand; //$NON-NLS-1$;
                                }
                                myconn = logConnectionURL(urlMessage, connectName, url, props, true);
                            } else {
                                throw new Exception();
                            }
                        } catch (Throwable e) {
                            try {
                                // thin has allowed @(DESCRIPTION... for a
                                // couple of years no longer need to use
                                // @host:port:SID syntax
                                String url = "jdbc:oracle:thin:@" + myTCPEntry.get(match).getSpecUrl(); //$NON-NLS-1$
                                myconn = logConnectionURL(urlMessage, connectName, url, props, true);
                            } catch (SQLException e2) {
                                // it is likely OCI is not linked in...try thin
                                // via hostname port sid
                                if (useOci) {
                                    Logger.getLogger(getClass().getName()).log(Level.WARNING,
                                            ScriptRunnerDbArb.getString(ScriptRunnerDbArb.OCI_FAILED_TRYING_THIN));
                                }
                                getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE,
                                        e2.getMessage());
                                if (myconn != null) {
                                    try {
                                        myconn.close();
                                    } catch (SQLException e1) {
                                    }
                                    myconn = null;
                                }
                            } catch (Throwable ee) {
                                myconn = null;
                            }
                        }
                    }
                }
            }
        }
        Boolean prelim = (Boolean) getScriptRunnerContext().getProperty(ScriptRunnerContext.PRELIMAUTH);
        if ((prelim == null) || (prelim.equals(Boolean.FALSE))) {// prelim
                                                                    // connection
                                                                    // will fail
                                                                    // lots of
                                                                    // sql
            setupNLS(myconn, getScriptRunnerContext()); 
        }
        if (myconn != null) {
            if ((successfulAttempt != null) && (getScriptRunnerContext().isCommandLine())
                    && ((getScriptRunnerContext().getProperty(ScriptRunnerContext.SETNET) == null)
                            || ((String) getScriptRunnerContext().getProperty(ScriptRunnerContext.SETNET) != null
                                    && (!((String) getScriptRunnerContext().getProperty(ScriptRunnerContext.SETNET))
                                            .equals(NetCommand.READONLY))))) {
                NetCommand.add(getScriptRunnerContext(),
                        (String) getScriptRunnerContext().getProperty(ScriptRunnerContext.SETNETOVERWRITE),
                        successfulAttempt, successfulAttempt, false);
            }
            getScriptRunnerContext().putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, null);
        }
        return myconn;
    }

		/**
     * This is only meant for use with sdsql command line for when a cloned
     * connection is needed
     * 
     * @param url
     * @param props
     * @return
     * @throws SQLException
     */
    private Connection connect(String urlin, Properties props) throws SQLException {
        String url=urlin;
        //if http force orest driver
        if (getScriptRunnerContext().getProperty(ScriptRunnerContext.JLINE_SETTING) != null) {
            if ((url!=null)&&((url.startsWith("http:")||(url.startsWith("https:"))))) {  //$NON-NLS-1$ //$NON-NLS-2$ 
                url="jdbc:oracle:orest@"+url; //$NON-NLS-1$ 
            }
        }
        if ((url != null) && (url.startsWith("http:") || url.startsWith("https:") || url.startsWith("jdbc:oracle:orest"))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            if ((!(url.endsWith("/")))&&(url.indexOf('?')==-1)) {  //$NON-NLS-1$ 
                url+="/";  //$NON-NLS-1$ 
            }
        }
    	ScriptRunnerContext ctx = getScriptRunnerContext();
        if (ctx != null) {
            ctx.putProperty(ScriptRunnerContext.CLI_CONN_URL, url);
            ctx.putProperty(ScriptRunnerContext.CLI_CONN_PROPS, props);
        }
        boolean doDebug = false;
        if ((url != null) && (url.startsWith("http:") || url.startsWith("https:") || url.startsWith("jdbc:oracle:orest"))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            doDebug = true;
        }
        if (doDebug) {
            try {
                /*
                 * Temporarily needed - this can be done declaritively in the
                 * jar 
                 */
                //Class.forName("oracle.dbtools.jdbc.Driver"); //$NON-NLS-1$
                //oracle.dbtools.util.Logger.info(this.getClass(),"class.forname_succeeded"); //$NON-NLS-1$
            } catch (Exception e) {
                oracle.dbtools.util.Logger.warn(this.getClass(),e);
                throw new SQLException(e);
            }
            if (url.startsWith("jdbc:oracle:orest")) { //$NON-NLS-1$
                try {
                    /*
                     * OracleConnection version
                     * Temporarily needed - this can be done declaritively in the
                      * jar 
                    */
                    //Class.forName("oracle.dbtools.jdbc.orest.Driver"); //$NON-NLS-1$
                    //oracle.dbtools.util.Logger.info(this.getClass(),"class.forname_succeeded"); //$NON-NLS-1$
             } catch (Exception e) {
                    oracle.dbtools.util.Logger.warn(this.getClass(),e);
                    throw new SQLException(e);
                }
            }
        }
        Connection forReturn=DriverManager.getConnection(url, props);;
        if (doDebug) {
            oracle.dbtools.util.Logger.info(this.getClass(),"Connection got, class:" + forReturn.getClass()); //$NON-NLS-1$
        }
        return forReturn;
    }

    private Connection preDebconnect(String url, Properties props) throws SQLException {
        ScriptRunnerContext ctx = getScriptRunnerContext();
        if (ctx != null) {
            ctx.putProperty(ScriptRunnerContext.CLI_CONN_URL, url);
            ctx.putProperty(ScriptRunnerContext.CLI_CONN_PROPS, props);
        }
        return DriverManager.getConnection(url, props);
    }

    private Connection logConnectionURL(ArrayList<String> urlMessage, String username, String URLString,
            Properties props, boolean tryPrelim) throws Throwable {
        return logConnectionURL(urlMessage, username, URLString, props, tryPrelim, true);
    }

    private boolean hasUrlStub(String url) {
        if (url != null) {
            for (String stub : new String[] { "jdbc:oracle:thin", "jdbc:oracle:oci", "jdbc:oracle:kprb", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    "jdbc:default:connection", //$NON-NLS-1$
                    "jdbc:timesten:direct", "jdbc:timesten:client", "jdbc:oracle:orest","http:" ,"https:"}) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
                if (url.startsWith(stub)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Connection prelimAuthTryReopen(Connection preconn, ScriptRunnerContext ctx) {
        try {// warning near duplicate code to workaround database auto closed
                // between connect and startup
            Boolean prelim = (Boolean) ctx.getProperty(ScriptRunnerContext.PRELIMAUTH);
            Integer prelimError = (Integer) ctx.getProperty(ScriptRunnerContext.PRELIMAUTHPREERROR);
            if ((preconn != null) && (preconn.isClosed() && (prelim != null) && (prelim.equals(Boolean.TRUE)
                    && (prelimError != null)/*
                                             * &&(prelimError.equals(new
                                             * Integer(1090)))
                                             */))) {
                // try an idle login.//MIGHT WANT OTHER CHECKS LIKE SPECIFIC ORA
                // OR NUT USERNAME/PASSWORD ERROR
                Properties duplicateProperties = new Properties();
                Properties props = (Properties) ctx.getProperty(ScriptRunnerContext.CLI_CONN_PROPS);
                String internalLogin = (String) props.getProperty("internal_logon"); //$NON-NLS-1$
                String userName = props.getProperty("user"); //$NON-NLS-1$
                if (userName != null) {
                    duplicateProperties.setProperty("user", userName); //$NON-NLS-1$
                }
                String password = props.getProperty("password"); //$NON-NLS-1$
                if (password != null) {
                    duplicateProperties.setProperty("password", password); //$NON-NLS-1$
                }
                duplicateProperties.setProperty("prelim_auth", "true"); //$NON-NLS-1$ //$NON-NLS-2$
                duplicateProperties.setProperty("internal_logon", internalLogin); //$NON-NLS-1$ //$NON-NLS-2
                try {
                    Connection conn = DriverManager.getConnection(
                            (String) ctx.getProperty(ScriptRunnerContext.CLI_CONN_URL), duplicateProperties);
                    try {
                        Logger.getLogger("SQLPLUS").log(Level.INFO, "(prelim auth) conn.isClosed()=" + conn.isClosed()); //$NON-NLS-1$ //$NON-NLS-2$
                    } catch (SQLException ee) {
                        Logger.getLogger("SQLPLUS").log(Level.INFO, "(prelim auth) conn.isClosed()_exception", ee); //$NON-NLS-1$ //$NON-NLS-2$
                    }
                    return conn;
                } catch (Throwable t2) {
                    ctx.write(t2.getLocalizedMessage() + "\n"); //$NON-NLS-1$
                }
            }
        } catch (Exception e) {
            ctx.write(e.getLocalizedMessage() + "\n"); //$NON-NLS-1$
        }
        return null;
    }

    private Connection logConnectionURL(ArrayList<String> urlMessage, String username, String URLString,
            Properties props, boolean tryPrelim, boolean doLog) throws Throwable {

        String user = ""; //$NON-NLS-1$
        boolean prelimAuthThrown = false;
        Connection conn = null;/*
                                 * leave out username in case that is sensitive
                                 * to log
                                 */
        if (doLog) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO,
                    ScriptRunnerDbArb.format(ScriptRunnerDbArb.CONNECTING, URLString));
        }
        String feedback = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NO_ERROR);
        try {
            try {
                conn = connect(URLString, props);
            } catch (SQLException e) {
                if (e.getErrorCode() == 28001) {
                    // get provider
                    report(getScriptRunnerContext(), e.getLocalizedMessage());
                    // issue will get this twice in some circumstance - new
                    // password not given or thin only and <12.2)
                    IGetPromptedPasswordFieldsProvider passProvider = getScriptRunnerContext()
                            .getPasswordFieldsProvider();
                    if (passProvider != null) {
                        // you are delaying for no reson hardcode first and
                        // decide which whether password to use latrer
                        Pair<String, String> passPair = passProvider.passwordGetPassword(getScriptRunnerContext(),
                                username + "donotdisplay", username); //$NON-NLS-1$
                        // array==null on error otherise string[2] = new
                        // password
                        if ((passPair != null) && (passPair.first() != null)) {
                            conn = new ResetPasswordHelper().resetAndReconnect(getScriptRunnerContext(), username,
                                    URLString, props, passPair.first(), e);
                        } else {
                            throw e;
                        }
                    }
                } else {
                    throw e;
                }
            }
            getScriptRunnerContext().putProperty(ScriptRunnerContext.PRELIMAUTH, Boolean.FALSE);
            if (conn != null) {
                try {
                    SQLWarning sw = conn.getWarnings();
                    if (sw != null) {
                        report(getScriptRunnerContext(), "ERROR:"); //$NON-NLS-1$
                    }
                    while (sw != null) {
                        String w = sw.getLocalizedMessage();
                        if (w != null) {
                            report(getScriptRunnerContext(), w); // $NON-NLS-1$
                        }
                        sw = sw.getNextWarning();
                    }
                    conn.clearWarnings();
                } catch (SQLException e) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
                }
            }
            // if sysdba try an idle connection
        } catch (Throwable e) {
            feedback = e.getLocalizedMessage();
            if (feedback == null) {
                feedback = e.getMessage();
            }
            if (feedback != null) {// you never know...
                feedback = feedback.trim();// I assume it has no ending newlines
                                            // later
            } else {
                feedback = ""; //$NON-NLS-1$
            }
            user = props.getProperty("user"); //$NON-NLS-1$
            if (user == null) {
                user = ""; //$NON-NLS-1$
            }
            try {
                String internalLogin = (String) props.getProperty("internal_logon"); //$NON-NLS-1$
                if (tryPrelim && (getScriptRunnerContext().isCommandLine()) && (e instanceof SQLException
                        && ((((SQLException) e).getErrorCode() == 1034) || ((SQLException) e).getErrorCode() == 1090) || ((SQLException) e).getErrorCode() == 1033
                        && (cmd.getStmtSubType().equals(StmtSubType.G_S_CONNECT)
                                && ((internalLogin != null) && (internalLogin.equalsIgnoreCase("SYSDBA") //$NON-NLS-1$
                                        || internalLogin.equalsIgnoreCase("SYSOPER")  //$NON-NLS-1$
                                        || internalLogin.equalsIgnoreCase("SYSASM")))))) { //$NON-NLS-1$
                    // try an idle login.//MIGHT WANT OTHER CHECKS LIKE SPECIFIC
                    // ORA OR NUT USERNAME/PASSWORD ERROR
                    Properties duplicateProperties = new Properties();
                    String userName = props.getProperty("user"); //$NON-NLS-1$
                    if (userName != null) {
                        duplicateProperties.setProperty("user", userName); //$NON-NLS-1$
                    }
                    String password = props.getProperty("password"); //$NON-NLS-1$
                    if (password != null) {
                        duplicateProperties.setProperty("password", password); //$NON-NLS-1$
                    }

                    duplicateProperties.setProperty("prelim_auth", "true"); //$NON-NLS-1$ //$NON-NLS-2$
                    duplicateProperties.setProperty("internal_logon", internalLogin); //$NON-NLS-1$ //$NON-NLS-2$
                    ScriptRunnerContext ctx = getScriptRunnerContext();
                    try {
                        conn = DriverManager.getConnection(URLString, duplicateProperties);
                        try {
                            Logger.getLogger(getClass().getName()).log(Level.INFO,
                                    "(prelim auth) conn.isClosed()=" + conn.isClosed()); //$NON-NLS-1$
                        } catch (SQLException ee) {
                            Logger.getLogger(getClass().getName()).log(Level.INFO,
                                    "(prelim auth) conn.isClosed()_exception", ee); //$NON-NLS-1$
                        }
                        getScriptRunnerContext().putProperty(ScriptRunnerContext.PRELIMAUTH, Boolean.TRUE);
                        getScriptRunnerContext().putProperty("PRELIMAUTHPREERROR", ((SQLException) e).getErrorCode()); //$NON-NLS-1$
                        if (ctx != null) {
                            ctx.putProperty(ScriptRunnerContext.CLI_CONN_URL, URLString);
                            ctx.putProperty(ScriptRunnerContext.CLI_CONN_PROPS, props);
                            // might change to be connected properly on
                            // startup?? This has not got prelim set
                        }
                    } catch (Throwable t2) {
                        if (doLog) {
                            if (!getScriptRunnerContext().isSQLPlusClassic()) {
                                urlMessage.add(ScriptRunnerDbArb.format(ScriptRunnerDbArb.URL_MESSAGE, user, URLString,
                                        feedback));
                            } else {
                                classicUrlMessageAdd(urlMessage, user, URLString, feedback);
                            }
                        }
                        feedback = ""; //$NON-NLS-1$
                        feedback = t2.getLocalizedMessage();
                        if (feedback == null) {
                            feedback = e.getMessage();
                        }
                        if (feedback != null) {// you never know...
                            feedback = feedback.trim();// I assume it has no
                                                        // ending newlines later
                        } else {
                            feedback = ""; //$NON-NLS-1$
                        }
                        prelimAuthThrown = true;
                        throw t2;
                    }
                } else {// not trying prelim
                    throw e;
                }
            } finally {
                if ((feedback != null) && (!(feedback.equals("")))) { //$NON-NLS-1$
                    user = props.getProperty("user"); //$NON-NLS-1$
                    if (user == null) {
                        user = ""; //$NON-NLS-1$
                    }
                    if (prelimAuthThrown) {
                        user = user + " prelim_auth"; //$NON-NLS-1$
                    }
                    if (doLog) {
                        if (!getScriptRunnerContext().isSQLPlusClassic()) {
                            urlMessage.add(
                                    ScriptRunnerDbArb.format(ScriptRunnerDbArb.URL_MESSAGE, user, URLString, feedback));
                        } else {
                            classicUrlMessageAdd(urlMessage, user, URLString, feedback);
                        }
                    }
                }
            }
        }

        return conn;
    }

    public static void classicUrlMessageAdd(ArrayList<String> urlMessage, String user, String URLString,
            String feedback) {
        if (urlMessage
                .size() == 0) {/* 0 - last beq/ipc- 1-first thin 2-first */
            urlMessage.add(""); //$NON-NLS-1$
            urlMessage.add(""); //$NON-NLS-1$
            urlMessage.add(""); //$NON-NLS-1$
            urlMessage.set(2, ScriptRunnerDbArb.format(ScriptRunnerDbArb.URL_MESSAGE_CLASSIC, feedback));

        }
        // considered skipping the prelim auth connection attempt.
        if (urlMessage.get(1).equals("") && URLString.startsWith("jdbc:oracle:thin")) { //$NON-NLS-1$ //$NON-NLS-2$
            urlMessage.set(1, ScriptRunnerDbArb.format(ScriptRunnerDbArb.URL_MESSAGE_CLASSIC, feedback));
        }
        // bump beq or PROTOCOL=IPC to the top
        String urlLower = URLString.toLowerCase(Locale.US).replaceAll("\\s+", ""); //$NON-NLS-1$ //$NON-NLS-2$
        if ((urlLower.startsWith("jdbc:oracle:oci") //$NON-NLS-1$
                && urlLower.endsWith("@"))// ie 99% chance beq //$NON-NLS-1$
                                            // could be with or without 8 with
                                            // or without ends with :
                || (urlLower.indexOf("protocol=ipc") != -1)) { // 99% //$NON-NLS-1$
                                                                // ipc
            urlMessage.set(0, ScriptRunnerDbArb.format(ScriptRunnerDbArb.URL_MESSAGE_CLASSIC, feedback));
        }
        urlMessage.add(ScriptRunnerDbArb.format(ScriptRunnerDbArb.URL_MESSAGE_CLASSIC, feedback));
    }

    public static void setupNLS(Connection myconn, ScriptRunnerContext src) {
        // making static so it can be called from startup (prelim - db not up)
        // to successfully started up
        Map<String, String> nlsMap = src.getNLSMap();
        if (myconn != null && nlsMap != null && !nlsMap.isEmpty()) {
            DefaultNLSProvider provider = null;
            provider = new OracleNLSProvider(myconn);
            provider.initConnection();
            provider.updateDefaults(nlsMap);
        }
    }

    /**
     * next string to character, returning the string and the rest
     */
    private String[] nextWordAndRest(String in) {
        return nextWordAndRestOrString(in, null);
    }

    /**
     * next string to character, returning the string and the rest
     */
    private String[] nextWordAndRestOrString(String in, String extraOne) {
        in = in.trim();
        if (in.length() == 0) {
            return null;
        }
        int i = 0;
        boolean inQuote = false;
        boolean found = false;
        String rest = ""; //$NON-NLS-1$
        String firstWord = ""; //$NON-NLS-1$
        for (i = 0; i < in.length(); i++) {
            if (in.charAt(i) == '"') {
                if (inQuote) {
                    inQuote = false;
                } else {
                    inQuote = true;
                }
            }
            if ((Character.isWhitespace(in.charAt(i)) || (extraOne != null && (extraOne.indexOf(in.charAt(i)) != -1)))
                    && (inQuote == false)) {
                found = true;
                break;
            }
        }
        if (!(found == false)) {// we have whitespace
            rest = in.substring(i).trim();// trims if whitspace, no ltrim effect
                                            // if extraone='(' which is what is
                                            // wanted
            firstWord = in.substring(0, i);
        } else {
            firstWord = in;
        }
        String[] stringArray = { firstWord, rest };
        return stringArray;
    }

    public static String removeDashNewline(ISQLCommand cmd, String in) {
        if (cmd.getProperty(SQLCommand.STRIPPED_CONTINUATION) != null) {
            return in;
        }
        return removeDashNewline(in);
    }

    static boolean specialNoContinuation(String s, int match) {
        int index = s.lastIndexOf('\n', match);
        if (index != -1) {
            return (s.substring(index + 1, match + 1).matches("^\\s*--")); //$NON-NLS-1$
        }
        return false;
    }

    // to be called when setString(return value) not known
    public static String removeDashNewline(String in) {

        // find newline, trim to newline if there it ends with dash add in the
        // rest
        // newline can be \r\n \n or \r so maybe find - eat
        StringBuffer op = new StringBuffer();

        /*
         * // cover basic case. in = in.trim(); if (in.endsWith("-")) {
         * //$NON-NLS-1$ in = in.substring(0, in.length()-1); } if
         * (in.startsWith("-")) { //$NON-NLS-1$ in = in.substring(1,
         * in.length()); }
         * 
         * // You do not want to remove dashes that are in the quotes, but the
         * ones that are outside the quotes. boolean inQuotes = false;
         * 
         * for (int i = 0; i < in.length(); i++) {
         * 
         * char ch = in.charAt(i);
         * 
         * if ( ((ch == '\'') || (ch == '\"')) && (inQuotes == false) ) {
         * op.append(ch); inQuotes = true; continue; } if ( inQuotes == true ) {
         * 
         * String charAsString = "" + ch; //$NON-NLS-1$ if
         * (!charAsString.equals(m_lineSeparator)) { op.append(ch); } if ((ch ==
         * '\'') || (ch == '\"')) { inQuotes = false; } } else { if (ch != '-')
         * { String charAsString = "" + ch; //$NON-NLS-1$ if
         * (!charAsString.equals(m_lineSeparator)) { op.append(ch); } } } }
         */
        /***********************************************************************************************
         * I have commented the following code to make it bit simpler. --
         * sroychow - 03/23/2015 Please see bug 19649168: UNABLE TO PRINT VALUE
         * OF SQL.XX (SCAN) for more details. totierne put the commented code
         * back in need a testcase for any additional fix (quote counting was
         * inappropriate) 21286099 EVERT BACK TO 'OLD' SQLPLUS.XXX CONCATENATION
         * CHAR
         ***********************************************************************************************/
        int currentlyAt = 0;
        int lastnewlineMatch = -1;
        int newlineMatch = -1;
        while (true) {
            int match = in.indexOf("-", currentlyAt); //$NON-NLS-1$
            if (match == -1) {
                if (currentlyAt == 0) {
                    return in;
                }
                op.append(in.substring(currentlyAt));
                break;
            }
            int eatWhite = match + 1;
            for (eatWhite = match + 1; eatWhite < in.length() && ((in.substring(eatWhite, eatWhite + 1).equals(" ")) //$NON-NLS-1$
                    || (in.substring(eatWhite, eatWhite + 1).equals("\t"))); eatWhite++) { //$NON-NLS-1$
            }
            if (eatWhite == in.length() && !specialNoContinuation(in, match)) {
                // extra concatted blank line could have been trimmed.
                op.append(in.substring(currentlyAt, match));
                break;
            }

            if ((eatWhite < in.length())
                    && (((in.substring(eatWhite, eatWhite + 1).equals("\r")) //$NON-NLS-1$
                            || (in.substring(eatWhite, eatWhite + 1).equals("\n")))) //$NON-NLS-1$
                    && !specialNoContinuation(in, match)) {
                // match found
                op.append(in.substring(currentlyAt, match));
                // maybe /r/n
                eatWhite++;
                if ((eatWhite < in.length()) && (((in.substring(eatWhite, eatWhite + 1).equals("\r")) //$NON-NLS-1$
                        || (in.substring(eatWhite, eatWhite + 1).equals("\n"))))) { //$NON-NLS-1$
                    eatWhite++;
                }
                currentlyAt = eatWhite;
            } else {
                // no match found
                op.append(in.substring(currentlyAt, eatWhite));
                currentlyAt = eatWhite;
            }
            if (eatWhite == in.length()) {
                break;
            }
        }
        /*
         * End of sroychow comment out
         ***********************************************************************************************/
        return op.toString();
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public static ProviderForSQLPATH getSqlpathProvider() {
        return sqlpathProvider;
    }

    public static void setSqlpathProvider(ProviderForSQLPATH sqlpathProvder) {
        SQLPLUS.sqlpathProvider = sqlpathProvder;
    }
}
