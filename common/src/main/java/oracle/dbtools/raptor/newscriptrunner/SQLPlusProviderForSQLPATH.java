/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;

import oracle.dbtools.common.utils.PlatformUtils;
import oracle.dbtools.common.utils.UtilResources;
import oracle.dbtools.raptor.newscriptrunner.ProviderForSQLPATH;
import oracle.dbtools.raptor.utils.WindowsUtility;
import oracle.dbtools.util.Logger;

public class SQLPlusProviderForSQLPATH implements ProviderForSQLPATH {

	public SQLPlusProviderForSQLPATH() {
	}

	@Override
	public String getSQLPATHsetting() {
		String sqlPath = ".";
		String fromDBConfig = null;// command line switch maybe?
									// DBConfig.getInstance().getString(DBConfig.SQL_PATH_OVERRIDE);
		if (fromDBConfig == null || (fromDBConfig.equals(""))) { //$NON-NLS-1$
			// System.getProperty(key)
			// get it from SQLPATH environment variable or the registry (windows
			// only)
			String envSQLPATH = System.getenv("SQLPATH"); //$NON-NLS-1$
			if (envSQLPATH != null && !envSQLPATH.equals("")) { //$NON-NLS-1$
				if (sqlPath!=null) sqlPath += File.pathSeparator + envSQLPATH;
				return sqlPath;
			}
			fromDBConfig = this.getOracleEnv("SQLPATH"); //$NON-NLS-1$

		}
		String path="";
		if(fromDBConfig!=null)
			path +=fromDBConfig;
		if (sqlPath!=null) {
			if (path.length()>0)
				path = sqlPath + File.pathSeparator + path;
			else 
				path += sqlPath;
		}
		String startDir = System.getProperties().getProperty("startup.directory");
		if (startDir!=null && startDir.trim().length()>0) {
			path = startDir + File.pathSeparator + path;
		}
		return path;
	}

	/**
	 * look up oracle software then oracle home then key in = SQL:PATH for this
	 * example in this case take the last ORACLE_HOME's sqlpath.
	 */
	String getOracleEnv(String in) {
		String sqlPath = null;
		if (sqlPath == null && PlatformUtils.isWindows()) {
			String key = "SOFTWARE\\ORACLE"; //$NON-NLS-1$
			try {
				java.util.logging.Logger l = java.util.logging.Logger.getLogger(""); //$NON-NLS-1$
				Level level=l.getLevel();
				List<String> oracle_keys = null;
				try{
					l.setLevel(Level.OFF); //$NON-NLS-1$
				    oracle_keys = WindowsUtility.readStringSubKeys(WindowsUtility.HKEY_LOCAL_MACHINE, key);
				} finally {
					l.setLevel(level);
				}
				String homeKey = null;
				HashSet<String> removeDuplicates=new HashSet<String>();
				if (oracle_keys!=null) {
					for (String oracle_key : oracle_keys) {
						removeDuplicates.add(oracle_key);
					}
				}
				if (!removeDuplicates.isEmpty()) {
					for (String oracle_key : removeDuplicates) {
						String temp = key + "\\" + oracle_key; //$NON-NLS-1$
						homeKey = WindowsUtility.readString(WindowsUtility.HKEY_LOCAL_MACHINE, temp, "ORACLE_HOME_KEY"); //$NON-NLS-1$
						Logger.info(this.getClass(), UtilResources.getString(UtilResources.TNSHelper_24) + homeKey);
						if (homeKey != null) {
							sqlPath = WindowsUtility.readString(WindowsUtility.HKEY_LOCAL_MACHINE, homeKey, in); //$NON-NLS-1$
							Logger.info(this.getClass(), UtilResources.getString(UtilResources.TNSHelper_29) + sqlPath);
						}
					}
				}
			} catch (Exception e) {
				Logger.finer(this.getClass(), e.getMessage(), e.getCause() );
			}
		}
		return sqlPath;
	}

}
