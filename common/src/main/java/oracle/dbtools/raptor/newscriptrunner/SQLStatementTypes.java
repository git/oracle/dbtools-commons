/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtResultType.G_R_DML;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtResultType.G_R_NONE;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtResultType.G_R_ONE_WORD;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtResultType.G_R_PERMISSION;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtResultType.G_R_QUERY;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtResultType.G_R_SCRIPTEDQUERY;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtResultType.G_R_THREE_WORD;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType.*;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_COMMENT;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_EMPTYLINE;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_MULTILINECOMMENT;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_OLDCOMMENT;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_PLSQL;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_SQL;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_SCRIPT;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_SQLPLUS;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_JDBC;

import java.util.HashMap;
import java.util.LinkedHashMap;

import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

public class SQLStatementTypes {
    public static final String CREATE_UNKNOWN = "create unknown"; //$NON-NLS-1$
    
    /*
     * stmts is for statements which require no following whitespace {token,class,statement,runnable}
     */
    public static final HashMap<String, SQLCommand> noSpaceRequiredTokens = new HashMap<String, SQLCommand>() {
        /**
		 * 
		 */
		private static final long serialVersionUID = 8259085550401643399L;

		{
            put("emptyline", new SQLCommand(G_C_EMPTYLINE, G_S_UNKNOWN, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("--", new SQLCommand(G_C_COMMENT, G_S_COMMENT_PLUS, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("/*", new SQLCommand(G_C_MULTILINECOMMENT, G_S_COMMENT_PLUS, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("@@", new SQLCommand(G_C_SQLPLUS, G_S_ATNESTED, G_R_NONE, false,Restricted.Level.R3)); //$NON-NLS-1$
            put("@", new SQLCommand(G_C_SQLPLUS, G_S_AT, G_R_NONE, true,Restricted.Level.R3)); //$NON-NLS-1$
            put("/", new SQLCommand(G_C_SQLPLUS, G_S_SLASH, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("!", new SQLCommand(G_C_SQLPLUS, G_S_HOSTALIAS, G_R_NONE, false,Restricted.Level.R1)); //$NON-NLS-1$
            put("$", new SQLCommand(G_C_SQLPLUS, G_S_HOSTALIAS, G_R_NONE, false,Restricted.Level.R1)); //$NON-NLS-1$
            put("?", new SQLCommand(G_C_SQLPLUS, G_S_HELP, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("(", new SQLCommand(G_C_SQL, G_S_SELECT, G_R_QUERY, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("<<", new SQLCommand(G_C_PLSQL, G_S_PLSQLLABEL, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("c/", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c@", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c#", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c>", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c<", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c?", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c.", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c,", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c:", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c;", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c|", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("c~", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("{"  ,new SQLCommand(G_C_JDBC, G_S_JDBC, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
        }
        
        public SQLCommand get(Object key) {
            SQLCommand cmd = super.get(key);
            return new SQLCommand(cmd); // us a copy constructor to create a new object, as this object will contain line info and possibly even
            // change types
        }
    };

    
    public static final HashMap<String, SQLCommand> hintTokens = new HashMap<String, SQLCommand>() {
        {
            put("/*sqldev:query*/", new SQLCommand(G_C_SQL, G_S_SELECT, G_R_QUERY, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("/*sqldev:stmt*/", new SQLCommand(G_C_SQL, G_S_CREATE_UNKNOWN, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
        }
        
        public SQLCommand get(Object key) {
            SQLCommand cmd = super.get(key);
            return new SQLCommand(cmd); // us a copy constructor to create a new object, as this object will contain line info and possibly even
            // change types
        }
    };

    
    /*
     * tokenStmts is for command which require whitespace following the token format of the array is as follows {token, class,
     */
    public static final HashMap<String, SQLCommand> spaceRequiredTokens = new HashMap<String, SQLCommand>() {
        {
           
            put("select", new SQLCommand(G_C_SQL, G_S_SELECT, G_R_QUERY, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("bridge",new SQLCommand(G_C_SQL,G_S_BRIDGE,G_R_SCRIPTEDQUERY,true,Restricted.Level.R4));//$NON-NLS-1$
            put("administer", new SQLCommand(G_C_SQL, G_S_ADMINISTER, G_R_NONE,true,Restricted.Level.NONE));
            // DML
            put("insert", new SQLCommand(G_C_SQL, G_S_INSERT, G_R_DML, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("update", new SQLCommand(G_C_SQL, G_S_UPDATE, G_R_DML, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("delete", new SQLCommand(G_C_SQL, G_S_DELETE, G_R_DML, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("merge", new SQLCommand(G_C_SQL, G_S_MERGE, G_R_DML, true,Restricted.Level.NONE)); //$NON-NLS-1$
            //
            put("purge", new SQLCommand(G_C_SQL, G_S_PURGE, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("analyze", new SQLCommand(G_C_SQL, G_S_ANALYZE, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("rename", new SQLCommand(G_C_SQL, G_S_RENAME, G_R_ONE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("grant", new SQLCommand(G_C_SQL, G_S_GRANT, G_R_PERMISSION, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("revoke", new SQLCommand(G_C_SQL, G_S_REVOKE, G_R_PERMISSION, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("flashback", new SQLCommand(G_C_SQL, G_S_FLASHBACK, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("commit", new SQLCommand(G_C_SQL, G_S_COMMIT, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("rollback", new SQLCommand(G_C_SQL, G_S_ROLLBACK_SQL, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("call", new SQLCommand(G_C_SQL, G_S_CALL, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("lock", new SQLCommand(G_C_SQL, G_S_LOCK, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("explain", new SQLCommand(G_C_SQL, G_S_EXPLAIN, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("validate", new SQLCommand(G_C_SQL, G_S_VALIDATE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("savepoint", new SQLCommand(G_C_SQL, G_S_SAVEPOINT, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("audit", new SQLCommand(G_C_SQL, G_S_AUDIT, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("noaudit", new SQLCommand(G_C_SQL, G_S_NOAUDIT, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("associate", new SQLCommand(G_C_SQL, G_S_ASSOCIATE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("disassociate", new SQLCommand(G_C_SQL, G_S_DISASSOCIATE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("expand", new SQLCommand(G_C_SQL, G_S_EXPAND, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("comment", new SQLCommand(G_C_SQL, G_S_COMMENT_SQL, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
            // plsql commands
            put("begin", new SQLCommand(G_C_PLSQL, G_S_BEGIN, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("declare", new SQLCommand(G_C_PLSQL, G_S_BEGIN, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("xquery", new SQLCommand(G_C_PLSQL, G_S_XQUERY, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            // script commands
            put("scri", new SQLCommand(G_C_SCRIPT , G_S_SCRIPT, G_R_NONE, true,Restricted.Level.R4_ABORT_SCRIPT));
            put("scrip", new SQLCommand(G_C_SCRIPT , G_S_SCRIPT, G_R_NONE, true,Restricted.Level.R4_ABORT_SCRIPT));
            put("script", new SQLCommand(G_C_SCRIPT , G_S_SCRIPT, G_R_NONE, true,Restricted.Level.R4_ABORT_SCRIPT));
            //
            // sqlplus commands
            put("ddl", new SQLCommand(G_C_SQLPLUS, G_S_DDL, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("load", new SQLCommand(G_C_SQLPLUS, G_S_LOAD, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("acc", new SQLCommand(G_C_SQLPLUS, G_S_ACCEPT, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("acce", new SQLCommand(G_C_SQLPLUS, G_S_ACCEPT, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("accep", new SQLCommand(G_C_SQLPLUS, G_S_ACCEPT, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("accept", new SQLCommand(G_C_SQLPLUS, G_S_ACCEPT, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("rem", new SQLCommand(G_C_SQLPLUS, G_S_COMMENT_PLUS, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("col", new SQLCommand(G_C_SQLPLUS, G_S_COLUMN, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("colu", new SQLCommand(G_C_SQLPLUS, G_S_COLUMN, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("colum", new SQLCommand(G_C_SQLPLUS, G_S_COLUMN, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("column", new SQLCommand(G_C_SQLPLUS, G_S_COLUMN, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("var", new SQLCommand(G_C_SQLPLUS, G_S_VARIABLE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("vari", new SQLCommand(G_C_SQLPLUS, G_S_VARIABLE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("varia", new SQLCommand(G_C_SQLPLUS, G_S_VARIABLE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("variab", new SQLCommand(G_C_SQLPLUS, G_S_VARIABLE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("variabl", new SQLCommand(G_C_SQLPLUS, G_S_VARIABLE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("variable", new SQLCommand(G_C_SQLPLUS, G_S_VARIABLE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("pro", new SQLCommand(G_C_SQLPLUS, G_S_PROMPT, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("prom", new SQLCommand(G_C_SQLPLUS, G_S_PROMPT, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("promp", new SQLCommand(G_C_SQLPLUS, G_S_PROMPT, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("prompt", new SQLCommand(G_C_SQLPLUS, G_S_PROMPT, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("exec", new SQLCommand(G_C_SQLPLUS, G_S_EXECUTE, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("execu", new SQLCommand(G_C_SQLPLUS, G_S_EXECUTE, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("execut", new SQLCommand(G_C_SQLPLUS, G_S_EXECUTE, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("execute", new SQLCommand(G_C_SQLPLUS, G_S_EXECUTE, G_R_NONE, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("sho", new SQLCommand(G_C_SQLPLUS, G_S_SHOW, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("show", new SQLCommand(G_C_SQLPLUS, G_S_SHOW, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("whenever", new SQLCommand(G_C_SQLPLUS, G_S_WHENEVER, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("spo", new SQLCommand(G_C_SQLPLUS, G_S_SPOOL, G_R_NONE, false,Restricted.Level.R2)); //$NON-NLS-1$
            put("spoo", new SQLCommand(G_C_SQLPLUS, G_S_SPOOL, G_R_NONE, false,Restricted.Level.R2)); //$NON-NLS-1$
            put("spool", new SQLCommand(G_C_SQLPLUS, G_S_SPOOL, G_R_NONE, false,Restricted.Level.R2)); //$NON-NLS-1$
            put("def", new SQLCommand(G_C_SQLPLUS, G_S_DEFINE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("defi", new SQLCommand(G_C_SQLPLUS, G_S_DEFINE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("defin", new SQLCommand(G_C_SQLPLUS, G_S_DEFINE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("define", new SQLCommand(G_C_SQLPLUS, G_S_DEFINE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("sta", new SQLCommand(G_C_SQLPLUS, G_S_START, G_R_NONE, true,Restricted.Level.R3)); //$NON-NLS-1$
            put("star", new SQLCommand(G_C_SQLPLUS, G_S_START, G_R_NONE, true,Restricted.Level.R3)); //$NON-NLS-1$
            put("start", new SQLCommand(G_C_SQLPLUS, G_S_START, G_R_NONE, true,Restricted.Level.R3)); //$NON-NLS-1$
            put("bre", new SQLCommand(G_C_SQLPLUS, G_S_BREAK, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("brea", new SQLCommand(G_C_SQLPLUS, G_S_BREAK, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("break", new SQLCommand(G_C_SQLPLUS, G_S_BREAK, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("comp", new SQLCommand(G_C_SQLPLUS, G_S_COMPUTE, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("compu", new SQLCommand(G_C_SQLPLUS, G_S_COMPUTE, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("comput", new SQLCommand(G_C_SQLPLUS, G_S_COMPUTE, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("compute", new SQLCommand(G_C_SQLPLUS, G_S_COMPUTE, G_R_NONE, false,Restricted.Level.R4)); //$NON-NLS-1$
            put("conn", new SQLCommand(G_C_SQLPLUS, G_S_CONNECT, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("conne", new SQLCommand(G_C_SQLPLUS, G_S_CONNECT, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("connec", new SQLCommand(G_C_SQLPLUS, G_S_CONNECT, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("connect", new SQLCommand(G_C_SQLPLUS, G_S_CONNECT, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("exit", new SQLCommand(G_C_SQLPLUS, G_S_EXIT, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("quit", new SQLCommand(G_C_SQLPLUS, G_S_QUIT, G_R_NONE, false,Restricted.Level.R4_ABORT_SCRIPT)); //$NON-NLS-1$
            put("desc", new SQLCommand(G_C_SQLPLUS, G_S_DESCRIBE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("descr", new SQLCommand(G_C_SQLPLUS, G_S_DESCRIBE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("descri", new SQLCommand(G_C_SQLPLUS, G_S_DESCRIBE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("describ", new SQLCommand(G_C_SQLPLUS, G_S_DESCRIBE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("describe", new SQLCommand(G_C_SQLPLUS, G_S_DESCRIBE, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("copy", new SQLCommand(G_C_SQLPLUS, G_S_COPY, G_R_NONE, false,Restricted.Level.NONE)); //$NON-NLS-1$
            put("disc", new SQLCommand(G_C_SQLPLUS, G_S_DISCONNECT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("disco", new SQLCommand(G_C_SQLPLUS, G_S_DISCONNECT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("discon", new SQLCommand(G_C_SQLPLUS, G_S_DISCONNECT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("disconn", new SQLCommand(G_C_SQLPLUS, G_S_DISCONNECT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("disconne", new SQLCommand(G_C_SQLPLUS, G_S_DISCONNECT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("disconnec", new SQLCommand(G_C_SQLPLUS, G_S_DISCONNECT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("disconnect", new SQLCommand(G_C_SQLPLUS, G_S_DISCONNECT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("timi", new SQLCommand(G_C_SQLPLUS, G_S_TIMING, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("timin", new SQLCommand(G_C_SQLPLUS, G_S_TIMING, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("timing", new SQLCommand(G_C_SQLPLUS, G_S_TIMING, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("get", new SQLCommand(G_C_SQLPLUS, G_S_GET, G_R_NONE, false, Restricted.Level.R3 )); //$NON-NLS-1$
            put("help", new SQLCommand(G_C_SQLPLUS, G_S_HELP, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("h", new SQLCommand(G_C_SQLPLUS, G_S_HISTORY , G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("hi", new SQLCommand(G_C_SQLPLUS, G_S_HISTORY , G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("his", new SQLCommand(G_C_SQLPLUS, G_S_HISTORY , G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("hist", new SQLCommand(G_C_SQLPLUS, G_S_HISTORY , G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("histo", new SQLCommand(G_C_SQLPLUS, G_S_HISTORY , G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("histor", new SQLCommand(G_C_SQLPLUS, G_S_HISTORY , G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("history", new SQLCommand(G_C_SQLPLUS, G_S_HISTORY , G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("print", new SQLCommand(G_C_SQLPLUS, G_S_PRINT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("undef", new SQLCommand(G_C_SQLPLUS, G_S_UNDEFINE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("undefi", new SQLCommand(G_C_SQLPLUS, G_S_UNDEFINE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("undefin", new SQLCommand(G_C_SQLPLUS, G_S_UNDEFINE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("undefine", new SQLCommand(G_C_SQLPLUS, G_S_UNDEFINE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("tti", new SQLCommand(G_C_SQLPLUS, G_S_TTITLE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("ttit", new SQLCommand(G_C_SQLPLUS, G_S_TTITLE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("ttitl", new SQLCommand(G_C_SQLPLUS, G_S_TTITLE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("ttitle", new SQLCommand(G_C_SQLPLUS, G_S_TTITLE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("bti", new SQLCommand(G_C_SQLPLUS, G_S_BTITLE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("btit", new SQLCommand(G_C_SQLPLUS, G_S_BTITLE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("btitl", new SQLCommand(G_C_SQLPLUS, G_S_BTITLE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("btitle", new SQLCommand(G_C_SQLPLUS, G_S_BTITLE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("attr", new SQLCommand(G_C_SQLPLUS, G_S_ATTRIBUTE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("attri", new SQLCommand(G_C_SQLPLUS, G_S_ATTRIBUTE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("attrib", new SQLCommand(G_C_SQLPLUS, G_S_ATTRIBUTE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("attribu", new SQLCommand(G_C_SQLPLUS, G_S_ATTRIBUTE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("attribut", new SQLCommand(G_C_SQLPLUS, G_S_ATTRIBUTE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("attribute", new SQLCommand(G_C_SQLPLUS, G_S_ATTRIBUTE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("store", new SQLCommand(G_C_SQLPLUS, G_S_STORE, G_R_NONE, false, Restricted.Level.R2 )); //$NON-NLS-1$
            put("r", new SQLCommand(G_C_SQLPLUS, G_S_RUN, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("ru", new SQLCommand(G_C_SQLPLUS, G_S_RUN, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("run", new SQLCommand(G_C_SQLPLUS, G_S_RUN, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("sav", new SQLCommand(G_C_SQLPLUS, G_S_SAVE, G_R_NONE, false, Restricted.Level.R2 )); //$NON-NLS-1$
            put("save", new SQLCommand(G_C_SQLPLUS, G_S_SAVE, G_R_NONE, false, Restricted.Level.R2)); //$NON-NLS-1$
            put("oradebug", new SQLCommand(G_C_SQLPLUS, G_S_ORADEBUG, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("passw", new SQLCommand(G_C_SQLPLUS, G_S_PASSWORD, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("passwo", new SQLCommand(G_C_SQLPLUS, G_S_PASSWORD, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("passwor", new SQLCommand(G_C_SQLPLUS, G_S_PASSWORD, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("password", new SQLCommand(G_C_SQLPLUS, G_S_PASSWORD, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("pau", new SQLCommand(G_C_SQLPLUS, G_S_PAUSE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("paus", new SQLCommand(G_C_SQLPLUS, G_S_PAUSE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("pause", new SQLCommand(G_C_SQLPLUS, G_S_PAUSE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("rema", new SQLCommand(G_C_SQLPLUS, G_S_COMMENT_PLUS, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("remar", new SQLCommand(G_C_SQLPLUS, G_S_COMMENT_PLUS, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("remark", new SQLCommand(G_C_SQLPLUS, G_S_COMMENT_PLUS, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("ho", new SQLCommand(G_C_SQLPLUS, G_S_HOST, G_R_NONE, false, Restricted.Level.R1)); //$NON-NLS-1$
            put("hos", new SQLCommand(G_C_SQLPLUS, G_S_HOST, G_R_NONE, false, Restricted.Level.R1 )); //$NON-NLS-1$
            put("host", new SQLCommand(G_C_SQLPLUS, G_S_HOST, G_R_NONE, false, Restricted.Level.R1 )); //$NON-NLS-1$
            put("acc", new SQLCommand(G_C_SQLPLUS, G_S_ACCEPT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("acce", new SQLCommand(G_C_SQLPLUS, G_S_ACCEPT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("accep", new SQLCommand(G_C_SQLPLUS, G_S_ACCEPT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("accept", new SQLCommand(G_C_SQLPLUS, G_S_ACCEPT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("ed", new SQLCommand(G_C_SQLPLUS, G_S_EDIT, G_R_NONE, false, Restricted.Level.R1 )); //$NON-NLS-1$
            put("edi", new SQLCommand(G_C_SQLPLUS, G_S_EDIT, G_R_NONE, false, Restricted.Level.R1 )); //$NON-NLS-1$
            put("edit", new SQLCommand(G_C_SQLPLUS, G_S_EDIT, G_R_NONE, false, Restricted.Level.R1 )); //$NON-NLS-1$
            put("cl", new SQLCommand(G_C_SQLPLUS, G_S_CLEAR, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("cle", new SQLCommand(G_C_SQLPLUS, G_S_CLEAR, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("clea", new SQLCommand(G_C_SQLPLUS, G_S_CLEAR, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("clear", new SQLCommand(G_C_SQLPLUS, G_S_CLEAR, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("i", new SQLCommand(G_C_SQLPLUS, G_S_INPUT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("in", new SQLCommand(G_C_SQLPLUS, G_S_INPUT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("inp", new SQLCommand(G_C_SQLPLUS, G_S_INPUT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("inpu", new SQLCommand(G_C_SQLPLUS, G_S_INPUT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("input", new SQLCommand(G_C_SQLPLUS, G_S_INPUT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("l", new SQLCommand(G_C_SQLPLUS, G_S_LIST, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("li", new SQLCommand(G_C_SQLPLUS, G_S_LIST, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("lis", new SQLCommand(G_C_SQLPLUS, G_S_LIST, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("list", new SQLCommand(G_C_SQLPLUS, G_S_LIST, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("del", new SQLCommand(G_C_SQLPLUS, G_S_DEL_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("archive", new SQLCommand(G_C_SQLPLUS, G_S_ARCHIVE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("shu", new SQLCommand(G_C_SQLPLUS, G_S_SHUTDOWN, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("shut", new SQLCommand(G_C_SQLPLUS, G_S_SHUTDOWN, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("shutd", new SQLCommand(G_C_SQLPLUS, G_S_SHUTDOWN, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("shutdow", new SQLCommand(G_C_SQLPLUS, G_S_SHUTDOWN, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("shutdown", new SQLCommand(G_C_SQLPLUS, G_S_SHUTDOWN, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("startup", new SQLCommand(G_C_SQLPLUS, G_S_STARTUP, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("recover", new SQLCommand(G_C_SQLPLUS, G_S_RECOVER, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("roll", new SQLCommand(G_C_SQLPLUS, G_S_ROLLBACK_PLUS, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("rollb", new SQLCommand(G_C_SQLPLUS, G_S_ROLLBACK_PLUS, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("rollba", new SQLCommand(G_C_SQLPLUS, G_S_ROLLBACK_PLUS, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("rollbac", new SQLCommand(G_C_SQLPLUS, G_S_ROLLBACK_PLUS, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("newpage", new SQLCommand(G_C_SQLPLUS, G_S_NEWPAGE, G_R_NONE, false, Restricted.Level.R4)); //$NON-NLS-1$
            // old school comments
            put("doc", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("#doc", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("docu", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("#docu", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("docum", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("#docum", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("docume", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("#docume", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("documen", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("#documen", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("document", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("#document", new SQLCommand(G_C_OLDCOMMENT, G_S_DOC_PLUS, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("a", new SQLCommand(G_C_SQLPLUS, G_S_APPEND, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("ap", new SQLCommand(G_C_SQLPLUS, G_S_APPEND, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("app", new SQLCommand(G_C_SQLPLUS, G_S_APPEND, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("appe", new SQLCommand(G_C_SQLPLUS, G_S_APPEND, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("appen", new SQLCommand(G_C_SQLPLUS, G_S_APPEND, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("append", new SQLCommand(G_C_SQLPLUS, G_S_APPEND, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("c", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("ch", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("cha", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("chan", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("chang", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("change", new SQLCommand(G_C_SQLPLUS, G_S_CHANGE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            //can be StmtType.G_C_SQL if it contains =
            put("alias", new SQLCommand(StmtType.G_C_SQLPLUS,G_S_ALIAS,G_R_NONE,false, Restricted.Level.R4));//$NON-NLS-1$
            put("net", new SQLCommand(StmtType.G_C_SQLPLUS,G_S_NET,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("cd", new SQLCommand(StmtType.G_C_SQLPLUS,G_S_CD,G_R_NONE,false,Restricted.Level.R4_ABORT_SCRIPT));//$NON-NLS-1$
            put("oerr", new SQLCommand(StmtType.G_C_SQLPLUS,G_S_OERR,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            put("format", new SQLCommand(StmtType.G_C_SQLPLUS /* originally set as G_C_SQL to get end by ; and multiline */ ,G_S_FORMAT,G_R_NONE,false,Restricted.Level.R4));//$NON-NLS-1$
            // set commands
            // put("set", new SQLCommand(G_C_UNKNOWN_SET, G_S_UNKNOWN, G_R_NONE, false, Restricted.Level. ));
            // these are now recognized by multiToken
            // create duh
            // put("create", new SQLCommand(G_C_UNKNOWN_CREATE, G_S_UNKNOWN, G_R_NONE, false, Restricted.Level. ));
            // sql dev only
            put("setloglevel", new SQLCommand(G_C_SQL, SQLCommand.StmtSubType.G_S_SETLOGLEVEL, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("loadqueryfile", new SQLCommand(G_C_SQL, SQLCommand.StmtSubType.G_S_LOADQUERYFILE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("ctas", new SQLCommand(G_C_SQLPLUS, SQLCommand.StmtSubType.G_S_CTAS, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("sshtunnel", new SQLCommand(G_C_SQLPLUS, SQLCommand.StmtSubType.G_S_SSHTUNNEL, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("find", new SQLCommand(G_C_SQLPLUS, SQLCommand.StmtSubType.G_S_FIND, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("which", new SQLCommand(G_C_SQLPLUS, SQLCommand.StmtSubType.G_S_WHICH, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("pwd", new SQLCommand(G_C_SQLPLUS, SQLCommand.StmtSubType.G_S_PWD, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("version", new SQLCommand(G_C_SQLPLUS, SQLCommand.StmtSubType.G_S_VERSION, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            
        }
        
        public SQLCommand get(Object key) {
            SQLCommand cmd = super.get(key);
            if (cmd == null) {
                return null;
            }
            return new SQLCommand(cmd); // us a copy constructor to create a new object, as this object will contain line info and possibly even
            // change types
        }
    };
    
    public static final String[] sqlCreate = {};
    public static final LinkedHashMap<String, SQLCommand> setCommandsExact = new LinkedHashMap<String, SQLCommand>() {
        {
            put("ddl", new SQLCommand(G_C_SQLPLUS, G_S_SET_DDL, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("pagesize", new SQLCommand(G_C_SQLPLUS, G_S_SET_PAGESIZE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("pagesiz", new SQLCommand(G_C_SQLPLUS, G_S_SET_PAGESIZE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("pagesi", new SQLCommand(G_C_SQLPLUS, G_S_SET_PAGESIZE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("pages", new SQLCommand(G_C_SQLPLUS, G_S_SET_PAGESIZE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("serveroutput", new SQLCommand(G_C_SQLPLUS, G_S_SET_SERVEROUTPUT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1
            put("serveroutpu", new SQLCommand(G_C_SQLPLUS, G_S_SET_SERVEROUTPUT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("serveroutp", new SQLCommand(G_C_SQLPLUS, G_S_SET_SERVEROUTPUT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("serverout", new SQLCommand(G_C_SQLPLUS, G_S_SET_SERVEROUTPUT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            
            put("appi", new SQLCommand(G_C_SQLPLUS, G_S_SET_APPINFO, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("appin", new SQLCommand(G_C_SQLPLUS, G_S_SET_APPINFO, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("appinf", new SQLCommand(G_C_SQLPLUS, G_S_SET_APPINFO, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("appinfo", new SQLCommand(G_C_SQLPLUS, G_S_SET_APPINFO, G_R_NONE, false, Restricted.Level.NONE)); //$NON-NLS-1$

            put("auto", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("autoc", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("autoco", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("autocom", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("autocomm", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("autocommi", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$
            put("autocommit", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT )); //$NON-NLS-1$

            put("autop", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOPRINT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("autopr", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOPRINT, G_R_NONE, false, Restricted.Level.NONE)); //$NON-NLS-1$
            put("autopri", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOPRINT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("autoprin", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOPRINT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("autoprint", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOPRINT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("array", new SQLCommand(G_C_SQLPLUS, G_S_SET_ARRAYSIZE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("arrays", new SQLCommand(G_C_SQLPLUS, G_S_SET_ARRAYSIZE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("arraysi", new SQLCommand(G_C_SQLPLUS, G_S_SET_ARRAYSIZE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("arraysiz", new SQLCommand(G_C_SQLPLUS, G_S_SET_ARRAYSIZE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("arraysize", new SQLCommand(G_C_SQLPLUS, G_S_SET_ARRAYSIZE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$

            put("autot", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOTRACE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("autotr", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOTRACE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("autotra", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOTRACE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("autotrac", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOTRACE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("autotrace", new SQLCommand(G_C_SQLPLUS, G_S_SET_AUTOTRACE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$

            put("trim", new SQLCommand(G_C_SQLPLUS, G_S_SET_TRIMOUT, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$            
            put("trimo", new SQLCommand(G_C_SQLPLUS, G_S_SET_TRIMOUT, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$            
            put("trimou", new SQLCommand(G_C_SQLPLUS, G_S_SET_TRIMOUT, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$            
            put("trimout", new SQLCommand(G_C_SQLPLUS, G_S_SET_TRIMOUT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            
            put("sqlp", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLPROMPT, G_R_NONE, false, Restricted.Level.NONE)); //$NON-NLS-1$
            put("sqlpr", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLPROMPT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("sqlpro", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLPROMPT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("sqlprom", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLPROMPT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("sqlpromp", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLPROMPT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("sqlprompt", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLPROMPT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            
            put("con", new SQLCommand(G_C_SQLPLUS, G_S_SET_CONCAT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("conc", new SQLCommand(G_C_SQLPLUS, G_S_SET_CONCAT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("conca", new SQLCommand(G_C_SQLPLUS, G_S_SET_CONCAT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("concat", new SQLCommand(G_C_SQLPLUS, G_S_SET_CONCAT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            
            put("def", new SQLCommand(G_C_SQLPLUS, G_S_SET_DEFINE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("defi", new SQLCommand(G_C_SQLPLUS, G_S_SET_DEFINE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("defin", new SQLCommand(G_C_SQLPLUS, G_S_SET_DEFINE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("define", new SQLCommand(G_C_SQLPLUS, G_S_SET_DEFINE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$

            put("xquery", new SQLCommand(G_C_SQLPLUS, G_S_SET_XQUERY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$

            put("errorl", new SQLCommand(G_C_SQLPLUS, G_S_SET_ERROR_LOGGING, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("errorlo", new SQLCommand(G_C_SQLPLUS, G_S_SET_ERROR_LOGGING, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("errorlog", new SQLCommand(G_C_SQLPLUS, G_S_SET_ERROR_LOGGING, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("errorlogg", new SQLCommand(G_C_SQLPLUS, G_S_SET_ERROR_LOGGING, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("errorloggi", new SQLCommand(G_C_SQLPLUS, G_S_SET_ERROR_LOGGING, G_R_NONE, false, Restricted.Level.NONE)); //$NON-NLS-1$
            put("errorloggin", new SQLCommand(G_C_SQLPLUS, G_S_SET_ERROR_LOGGING, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("errorlogging", new SQLCommand(G_C_SQLPLUS, G_S_SET_ERROR_LOGGING, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$

            
        }
        public SQLCommand get(Object key) {
            SQLCommand cmd = super.get(key);
            return new SQLCommand(cmd); // use a copy constructor to create a new object, 
            //as this object will contain line info and possibly eve 
            // change types
         }
    };

    public static final LinkedHashMap<String, SQLCommand> setCommands = new LinkedHashMap<String, SQLCommand>() {
        {
        
            // set sql
            put("constraint", new SQLCommand(G_C_SQL, SQLCommand.StmtSubType.G_S_SET_CONSTRAINT, G_R_NONE, false, Restricted.Level.NONE)); //$NON-NLS-1$
            put("constraints", new SQLCommand(G_C_SQL, SQLCommand.StmtSubType.G_S_SET_CONSTRAINT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("role", new SQLCommand(G_C_SQL, SQLCommand.StmtSubType.G_S_SET_ROLE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("transaction", new SQLCommand(G_C_SQL, SQLCommand.StmtSubType.G_S_SET_TRANSACTION, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            // set sqlplus

            put("copyc", new SQLCommand(G_C_SQLPLUS, G_S_SET_COPYCOMMIT, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            //put("color", new SQLCommand(G_C_SQLPLUS, G_S_SET_COLOR, G_R_NONE, false, Restricted.Level. )); //$NON-NLS-1$
            put("emb", new SQLCommand(G_C_SQLPLUS, G_S_SET_EMBEDDED, G_R_NONE, false, Restricted.Level.R4 )); //
            put("embe", new SQLCommand(G_C_SQLPLUS, G_S_SET_EMBEDDED, G_R_NONE, false, Restricted.Level.R4 )); //
            put("embed", new SQLCommand(G_C_SQLPLUS, G_S_SET_EMBEDDED, G_R_NONE, false, Restricted.Level.R4 )); //
            put("embedd", new SQLCommand(G_C_SQLPLUS, G_S_SET_EMBEDDED, G_R_NONE, false, Restricted.Level.R4 )); //
            put("embedde", new SQLCommand(G_C_SQLPLUS, G_S_SET_EMBEDDED, G_R_NONE, false, Restricted.Level.R4 )); //
            put("embedded", new SQLCommand(G_C_SQLPLUS, G_S_SET_EMBEDDED, G_R_NONE, false, Restricted.Level.R4 )); //
            put("escchar", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4)); // set sqlplus obsolete commands  //$NON-NLS-1$
            put("esc", new SQLCommand(G_C_SQLPLUS, G_S_SET_ESCAPE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("feedback", new SQLCommand(G_C_SQLPLUS, G_S_SET_FEEDBACK, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("feed", new SQLCommand(G_C_SQLPLUS, G_S_SET_FEEDBACK, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("owa", new SQLCommand(G_C_SQLPLUS, G_S_SET_OWA, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("getpage", new SQLCommand(G_C_SQLPLUS, G_S_SET_GETPAGE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("spool", new SQLCommand(G_C_SQLPLUS, G_S_SET_SPOOL, G_R_NONE, false, Restricted.Level.R2 )); //$NON-NLS-1$
            put("term", new SQLCommand(G_C_SQLPLUS, G_S_SET_TERM, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("ver", new SQLCommand(G_C_SQLPLUS, G_S_SET_VERIFY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("describe", new SQLCommand(G_C_SQLPLUS, G_S_SET_DESCRIBE, G_R_NONE, false, Restricted.Level.NONE ));  //$NON-NLS-1$
            put("scan",new SQLCommand(G_C_SQLPLUS, G_S_SET_SCAN, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("null",new SQLCommand(G_C_SQLPLUS, G_S_SET_NULL, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("net",new SQLCommand(G_C_SQLPLUS, G_S_SET_NET, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("newp",new SQLCommand(G_C_SQLPLUS, G_S_SET_NEW_PAGE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("newpage",new SQLCommand(G_C_SQLPLUS, G_S_SET_NEW_PAGE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("pau",new SQLCommand(G_C_SQLPLUS, G_S_SET_PAUSE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("noverwrite",new SQLCommand(G_C_SQLPLUS, G_S_SET_NETOVERWRITE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("timi", new SQLCommand(G_C_SQLPLUS, G_S_SET_TIMING, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("ti", new SQLCommand(G_C_SQLPLUS, G_S_SET_TIME, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("time", new SQLCommand(G_C_SQLPLUS, G_S_SET_TIME, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("echo",  new SQLCommand(G_C_SQLPLUS, G_S_SET_ECHO, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("worksheetname",new SQLCommand(StmtType.G_C_USERDEFINED,G_S_SET_WORKSHEETNAME,G_R_NONE,false,Restricted.Level.R4));
            put("lin", new SQLCommand(G_C_SQLPLUS, G_S_SET_LINESIZE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("longchunksize", new SQLCommand(G_C_SQLPLUS, G_S_SET_LONGC, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("longc", new SQLCommand(G_C_SQLPLUS, G_S_SET_LONGC, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("long", new SQLCommand(G_C_SQLPLUS, G_S_SET_LONG, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$
            put("colsep", new SQLCommand(G_C_SQLPLUS, G_S_SET_COLSEP, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("securedcol", new SQLCommand(G_C_SQLPLUS, G_S_SET_SECUREDCOL, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("xmlfo", new SQLCommand(G_C_SQLPLUS, G_S_SET_XMLFORMAT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("xmlfor", new SQLCommand(G_C_SQLPLUS, G_S_SET_XMLFORMAT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("xmlform", new SQLCommand(G_C_SQLPLUS, G_S_SET_XMLFORMAT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("xmlforma", new SQLCommand(G_C_SQLPLUS, G_S_SET_XMLFORMAT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("xmlformat", new SQLCommand(G_C_SQLPLUS, G_S_SET_XMLFORMAT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("xmloptimizationcheck", new SQLCommand(G_C_SQLPLUS, G_S_SET_XMLOPTIMIZATIONCHECK, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("closecursor", new SQLCommand(G_C_SQLPLUS, G_S_SET_CLOSECURSOR, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("space", new SQLCommand(G_C_SQLPLUS, G_S_SET_COLSEP, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("heads", new SQLCommand(G_C_SQLPLUS, G_S_SET_HEADINGSEP, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("hea", new SQLCommand(G_C_SQLPLUS, G_S_SET_HEADING, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)

            put("trims", new SQLCommand(G_C_SQLPLUS, G_S_SET_TRIMSPOOL, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("wra", new SQLCommand(G_C_SQLPLUS, G_S_SET_WRAP, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("tru", new SQLCommand(G_C_SQLPLUS, G_S_SET_WRAP, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("trun", new SQLCommand(G_C_SQLPLUS, G_S_SET_WRAP, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("trunc", new SQLCommand(G_C_SQLPLUS, G_S_SET_WRAP, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("trunca", new SQLCommand(G_C_SQLPLUS, G_S_SET_WRAP, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("truncat", new SQLCommand(G_C_SQLPLUS, G_S_SET_WRAP, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("truncate", new SQLCommand(G_C_SQLPLUS, G_S_SET_WRAP, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("numf", new SQLCommand(G_C_SQLPLUS, G_S_SET_NUMBERFORMAT, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("num", new SQLCommand(G_C_SQLPLUS, G_S_SET_NUMBERWIDTH, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("property", new SQLCommand(G_C_SQLPLUS, G_S_SET_PROPERTY, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("sqlbl", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLBL, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$)
            put("sqlblanklines", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLBL, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$)
            put("blo", new SQLCommand(G_C_SQLPLUS, G_S_SET_BLO, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$)
            put("blockterminator", new SQLCommand(G_C_SQLPLUS, G_S_SET_BLO, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$)
            put("_prelim", new SQLCommand(G_C_SQLPLUS, G_S_SET_PRELIM, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("showmode", new SQLCommand(G_C_SQLPLUS, G_S_SET_SHOWMODE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$)
            put("hist", new SQLCommand(G_C_SQLPLUS, G_S_SET_HISTORY, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)
            put("history", new SQLCommand(G_C_SQLPLUS, G_S_SET_SHOWMODE, G_R_NONE, false, Restricted.Level.R4 )); //$NON-NLS-1$)


            put("editf", new SQLCommand(G_C_SQLPLUS, G_S_SET_EDITFILE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("editfile", new SQLCommand(G_C_SQLPLUS, G_S_SET_EDITFILE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("suf", new SQLCommand(G_C_SQLPLUS, G_S_SET_SUFFIX, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("suffix", new SQLCommand(G_C_SQLPLUS, G_S_SET_SUFFIX, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$


            put("_12.1_pdb", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLPLUS_CLASSIC_MODE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("_12.1_longident", new SQLCommand(G_C_SQLPLUS, G_S_SET_IGNORE_SETTING_MODE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("classic", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLPLUS_CLASSIC_MODE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("classicmode", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLPLUS_CLASSIC_MODE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("sqlpluscompat", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLCOMPATABILITY, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("sqlpluscompatibility", new SQLCommand(G_C_SQLPLUS, G_S_SET_SQLCOMPATABILITY, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("colinvi", new SQLCommand(G_C_SQLPLUS, G_S_SET_COLINVISIBLE, G_R_NONE, false, Restricted.Level.NONE));  //$NON-NLS-1$
            put("colinvisible", new SQLCommand(G_C_SQLPLUS, G_S_SET_COLINVISIBLE, G_R_NONE, false, Restricted.Level.NONE ));  //$NON-NLS-1$
            
            
            // set sqlplus obsolete commands
            put("sqlc", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("sqlco", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("desc", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
           
            put("sqlt", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("sqlprefix", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            
            put("flush", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("document", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("compatibility", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("maxdata", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$

            put("doc", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("document", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("clear", new SQLCommand(G_C_SQLPLUS, G_S_SET_CLEARSCREEN, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$       
            put("encoding", new SQLCommand(G_C_SQLPLUS, G_S_SET_ENCODING, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$       
            put("cmds", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.NONE ));  //$NON-NLS-1$        
            put("cmdsep", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("tab", new SQLCommand(G_C_SQLPLUS, G_S_SET_TAB, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$ still an obsolete command.

            put("flagger", new SQLCommand(G_C_SQLPLUS, G_S_SET_OBSOLETE, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            
            put("exitc", new SQLCommand(G_C_SQLPLUS, G_S_SET_EXITCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT ));  //$NON-NLS-1$
            put("exitcom", new SQLCommand(G_C_SQLPLUS, G_S_SET_EXITCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT ));  //$NON-NLS-1$
            put("exitcomm", new SQLCommand(G_C_SQLPLUS, G_S_SET_EXITCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT ));  //$NON-NLS-1$
            put("exitcommit", new SQLCommand(G_C_SQLPLUS, G_S_SET_EXITCOMMIT, G_R_NONE, false, Restricted.Level.R4_ABORT_SCRIPT ));  //$NON-NLS-1$
            put("secureliteral", new SQLCommand(G_C_SQLPLUS, G_S_SET_SECURELITERALS, G_R_NONE, false, Restricted.Level.R4 ));  //$NON-NLS-1$
            put("xquery", new SQLCommand(G_C_SQLPLUS, StmtSubType.G_S_SET_XQUERY, G_R_NONE, false, Restricted.Level.NONE ));  //$NON-NLS-1$
        }
            public SQLCommand get(Object key) {
               SQLCommand cmd = super.get(key);
               return new SQLCommand(cmd); // use a copy constructor to create a new object, 
               //as this object will contain line info and possibly eve 
               // change types
            }
        };
        
    public static final LinkedHashMap<String, SQLCommand> multiToken = new LinkedHashMap<String, SQLCommand>() {
        {
            // set sql
            put("set", new SQLCompoundCommand(G_C_SQL, SQLCommand.StmtSubType.G_S_SET, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            // create view
            put("createorreplaceview", new SQLCommand(G_C_SQL, G_S_CREATE_VIEW, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceforceview", new SQLCommand(G_C_SQL, G_S_CREATE_VIEW, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createview", new SQLCommand(G_C_SQL, G_S_CREATE_VIEW, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            // create pl/sql
            put("createorreplacepackageheader", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacepackage", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createpackage", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacepackagebody", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_BODY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceprocedure", new SQLCommand(G_C_PLSQL, G_S_CREATE_PROCEDURE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacetrigger", new SQLCommand(G_C_PLSQL, G_S_CREATE_TRIGGER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createtrigger"         , new SQLCommand(G_C_PLSQL, G_S_CREATE_TRIGGER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacefunction", new SQLCommand(G_C_PLSQL, G_S_CREATE_FUNCTION, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacetype", new SQLCommand(G_C_PLSQL, G_S_CREATE_TYPE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createpackageheader", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createpackagebody", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_BODY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createprocedure", new SQLCommand(G_C_PLSQL, G_S_CREATE_PROCEDURE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createfunction", new SQLCommand(G_C_PLSQL, G_S_CREATE_FUNCTION, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createtype", new SQLCommand(G_C_PLSQL, G_S_CREATE_TYPE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createlibrary", new SQLCommand(G_C_PLSQL, G_S_CREATE_LIBRARY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacelibrary", new SQLCommand(G_C_PLSQL, G_S_CREATE_LIBRARY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            //+editionable and non editionable 1/normal + editionable 2/ normal + non editionable 3/orreplace editionable 4/orreplacew non editionable
            //1/normal + editionable
            //note looks like package is overloaded - but I do not think we care if header and body get confused. Both are PLSQL not returning result set
            //Assumption: create unknown will pick up edition synonym.
            put("createeditionablepackageheader", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createeditionablepackage", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createeditionablepackagebody", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_BODY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createeditionableprocedure", new SQLCommand(G_C_PLSQL, G_S_CREATE_PROCEDURE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createeditionabletrigger", new SQLCommand(G_C_PLSQL, G_S_CREATE_TRIGGER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createeditionablefunction", new SQLCommand(G_C_PLSQL, G_S_CREATE_FUNCTION, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createeditionabletype", new SQLCommand(G_C_PLSQL, G_S_CREATE_TYPE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createeditionablelibrary", new SQLCommand(G_C_PLSQL, G_S_CREATE_LIBRARY, G_R_NONE, false, Restricted.Level.NONE)); //$NON-NLS-1$
            //2/ normal + non editionable 
            put("createnoneditionablepackageheader", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createnoneditionablepackage", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createnoneditionablepackagebody", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_BODY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createnoneditionableprocedure", new SQLCommand(G_C_PLSQL, G_S_CREATE_PROCEDURE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createnoneditionabletrigger", new SQLCommand(G_C_PLSQL, G_S_CREATE_TRIGGER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createnoneditionablefunction", new SQLCommand(G_C_PLSQL, G_S_CREATE_FUNCTION, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createnoneditionabletype", new SQLCommand(G_C_PLSQL, G_S_CREATE_TYPE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createnoneditionablelibrary", new SQLCommand(G_C_PLSQL, G_S_CREATE_LIBRARY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            //3/ orreplace editionable 
            put("createorreplaceeditionablepackageheader", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceeditionablepackage", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceeditionablepackagebody", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_BODY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceeditionableprocedure", new SQLCommand(G_C_PLSQL, G_S_CREATE_PROCEDURE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceeditionabletrigger", new SQLCommand(G_C_PLSQL, G_S_CREATE_TRIGGER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceeditionablefunction", new SQLCommand(G_C_PLSQL, G_S_CREATE_FUNCTION, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceeditionabletype", new SQLCommand(G_C_PLSQL, G_S_CREATE_TYPE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceeditionablelibrary", new SQLCommand(G_C_PLSQL, G_S_CREATE_LIBRARY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            //4/orreplace non editionable
            put("createorreplacenoneditionablepackageheader", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacenoneditionablepackage", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_HEADER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacenoneditionablepackagebody", new SQLCommand(G_C_PLSQL, G_S_CREATE_PACKAGE_BODY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacenoneditionableprocedure", new SQLCommand(G_C_PLSQL, G_S_CREATE_PROCEDURE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacenoneditionabletrigger", new SQLCommand(G_C_PLSQL, G_S_CREATE_TRIGGER, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacenoneditionablefunction", new SQLCommand(G_C_PLSQL, G_S_CREATE_FUNCTION, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacenoneditionabletype", new SQLCommand(G_C_PLSQL, G_S_CREATE_TYPE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacenoneditionablelibrary", new SQLCommand(G_C_PLSQL, G_S_CREATE_LIBRARY, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            //create sql
            put("createtable", new SQLCommand(G_C_SQL, G_S_CREATE_TABLE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createdatabase", new SQLCommand(G_C_SQL, G_S_CREATE_DATABASE, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put(CREATE_UNKNOWN, new SQLCommand(G_C_SQL, G_S_CREATE_UNKNOWN, G_R_NONE, false, Restricted.Level.NONE )); //this is a dummy for unknown thrid party create statements
            //create java
            put("createjava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createnoforcejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplacenoforcejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceandresolvejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceandcompilejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceandresolvenoforcejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createorreplaceandcompilenoforcejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createandresolvejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createandcompilejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createandresolvenoforcejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createandcompilenoforcejava", new SQLCommand(G_C_PLSQL, G_S_CREATE_JAVA, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("createconn", new SQLCommand(StmtType.G_C_USERDEFINED,G_S_CREATE_CONN,G_R_NONE,false, Restricted.Level.NONE));
            put("withfunction", new SQLCommand(G_C_PLSQL, G_S_WITH, G_R_QUERY, true, Restricted.Level.NONE)); //$NON-NLS-1$
            put("withprocedure", new SQLCommand(G_C_PLSQL, G_S_WITH, G_R_QUERY, true, Restricted.Level.NONE)); //$NON-NLS-1$
            put("with", new SQLCommand(G_C_SQL, G_S_WITH, G_R_QUERY, true, Restricted.Level.NONE)); //$NON-NLS-1$
            put("altersession", new SQLCommand(G_C_SQL, G_S_ALTER, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("altersystem", new SQLCommand(G_C_SQL, G_S_ALTER, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$ 
            put("createcontrolfile", new SQLCommand(G_C_SQL, StmtSubType.G_S_CREATE_UNKNOWN, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$ 
            //CREATE , ALTER , DROP
            createAlterDrop("analytic",Restricted.Level.NONE);
            createAlterDrop("attributedimension",Restricted.Level.NONE);
            createAlterDrop("auditpolicy",Restricted.Level.NONE);
            createAlterDrop("altercluster",Restricted.Level.NONE);
            alterDrop("database",Restricted.Level.NONE);
            createAlterDrop("databaselink",Restricted.Level.NONE);
            createAlterDrop("dimension",Restricted.Level.NONE);
            createAlterDrop("diskgroup",Restricted.Level.NONE);
            createAlterDrop("flashback",Restricted.Level.NONE);
            alterDrop("function",Restricted.Level.NONE);
            createAlterDrop("hierarchy",Restricted.Level.NONE);
            createAlterDrop("index",Restricted.Level.NONE);
            createAlterDrop("indextype",Restricted.Level.NONE);
            createAlterDrop("inmemory",Restricted.Level.NONE);
            alterDrop("java",Restricted.Level.NONE);
            alterDrop("library",Restricted.Level.NONE);
            createAlterDrop("lockdown",Restricted.Level.NONE);
            createAlterDrop("materializedview",Restricted.Level.NONE);
            createAlterDrop("materializedzonemap",Restricted.Level.NONE);
            createAlterDrop("operator",Restricted.Level.NONE);
            createAlterDrop("outline",Restricted.Level.NONE);
            alterDrop("package",Restricted.Level.NONE);
            createAlterDrop("pluggabledatabase",Restricted.Level.NONE);
            alterDrop("procedure",Restricted.Level.NONE);
            createAlterDrop("profile",Restricted.Level.NONE);
            createAlterDrop("resourcecost",Restricted.Level.NONE);
            createAlterDrop("role",Restricted.Level.NONE);
            createAlterDrop("rollbacksegment",Restricted.Level.NONE);
            createAlterDrop("sequence",Restricted.Level.NONE);
            createAlterDrop("synonym",Restricted.Level.NONE);
            alterDrop("table",Restricted.Level.NONE);
            createAlterDrop("tablespace",Restricted.Level.NONE);
            createAlterDrop("tablespaceset",Restricted.Level.NONE);
            alterDrop("trigger",Restricted.Level.NONE);
            alterDrop("type",Restricted.Level.NONE);
            createAlterDrop("user",Restricted.Level.NONE);
            alterDrop("view",Restricted.Level.NONE);
            createDrop("context",Restricted.Level.NONE);
            createDrop("directory",Restricted.Level.NONE);
            put("dropeditionlibrary", new SQLCommand(G_C_SQL, StmtSubType.G_S_DROP, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$ 
            put("dropedition", new SQLCommand(G_C_SQL, StmtSubType.G_S_DROP, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$ 
            put("createpfile", new SQLCommand(G_C_SQL, StmtSubType.G_S_CREATE_UNKNOWN, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$ 
            put("createrestorepoint", new SQLCommand(G_C_SQL, StmtSubType.G_S_CREATE_UNKNOWN, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$ 
            put("createschema", new SQLCommand(G_C_SQL, StmtSubType.G_S_CREATE_UNKNOWN, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$ 
            put("createspfile", new SQLCommand(G_C_SQL, StmtSubType.G_S_CREATE_UNKNOWN, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$ 
            put("truncatetable", new SQLCommand(G_C_SQL, G_S_TRUNCATE, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("truncatecluster", new SQLCommand(G_C_SQL, G_S_TRUNCATE, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$
            
        }

        public SQLCommand get(Object key) {
            SQLCommand cmd = super.get(key);
            if (cmd instanceof SQLCompoundCommand) {
                return new SQLCompoundCommand(cmd);
            }
            return new SQLCommand(cmd); // us a copy constructor to create a new object, as this object will contain line info and possibly even
            // change types
        }

        private void createAlterDrop(String commandName, Restricted.Level restrictLevel) {
          put("create"+commandName, new SQLCommand(G_C_SQL, StmtSubType.G_S_CREATE_UNKNOWN, G_R_THREE_WORD, true,restrictLevel)); //$NON-NLS-1$
          put("alter" +commandName, new SQLCommand(G_C_SQL, G_S_ALTER, G_R_THREE_WORD, true,restrictLevel)); //$NON-NLS-1$
          put("drop"+commandName, new SQLCommand(G_C_SQL, G_S_DROP, G_R_THREE_WORD, true,restrictLevel)); //$NON-NLS-1$ 
          
        }
       
        private void alterDrop(String commandName, Restricted.Level restrictLevel) {
          put("alter" +commandName, new SQLCommand(G_C_SQL, G_S_ALTER, G_R_THREE_WORD, true,restrictLevel)); //$NON-NLS-1$
          put("drop"+commandName, new SQLCommand(G_C_SQL, G_S_DROP, G_R_THREE_WORD, true,restrictLevel)); //$NON-NLS-1$
        }
        
        private void createDrop(String commandName, Restricted.Level restrictLevel) {
          put("create"+commandName, new SQLCommand(G_C_SQL, StmtSubType.G_S_CREATE_UNKNOWN, G_R_THREE_WORD, true,restrictLevel)); //$NON-NLS-1$
          put("drop"+commandName, new SQLCommand(G_C_SQL, G_S_DROP, G_R_THREE_WORD, true,restrictLevel)); //$NON-NLS-1$
        }
    };
    
    public static final HashMap<String, SQLCommand> halfRecognizedMultiToken = new HashMap<String, SQLCommand>() {
        {
            put("create", new SQLCommand(G_C_SQL, G_S_CREATE_UNKNOWN, G_R_NONE, false, Restricted.Level.NONE )); //$NON-NLS-1$
            put("set", new SQLCommand(G_C_SQLPLUS, G_S_SET_UNKNOWN, G_R_NONE, false, Restricted.Level.NONE ));//sqlplus means i //$NON-NLS-1$
            put("alter", new SQLCommand(G_C_SQL, G_S_ALTER, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$
            put("drop", new SQLCommand(G_C_SQL, G_S_DROP, G_R_THREE_WORD, true,Restricted.Level.NONE)); //$NON-NLS-1$
        }
        public SQLCommand get(Object key) {
            SQLCommand cmd = super.get(key);
            return new SQLCommand(cmd); // us a copy constructor to create a new object, as this object will contain line info and possibly even
            // change types
        }
    };
    
}
