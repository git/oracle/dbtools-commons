/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.datatypes.oracle.plsql.BOOLEAN;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtResultType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.util.BOMSkipper;


/**
 * @author doneill
 * ScriptExecutor is used to run scripts by features outside of the ScriptRunner in the worksheet.
 * For example the migration workbench may want to run a script to create the repository.
 * It calls this class to run the script.
 * At the moment it has been left as a Thread but will have to be refactored into the new threading model.
 * This class exists because running a SQLCommand and running a script have been seperated out into different classes.
 * ScriptExecutor handles the entire script and ScriptRunner handles just 1 command.
 */
public class ScriptExecutor extends Thread {// TODO refactor to use new threading framework
	private static ICommandHistory history = null;
    private Reader m_rdrForParser = null;
    private Iterator<ISQLCommand> m_parser = null;
    private Connection m_conn = null;
    private ISQLCommand m_cmd = null;
    private ScriptRunner m_runner = null;
    private boolean m_interrupted = false;
    private ScriptRunnerContext m_scriptRunnerContext = null;
    private BufferedOutputStream m_out = null;
    private static final Logger LOGGER = Logger.getLogger(ScriptExecutor.class.getName());
    private URL m_baseURL = null;
    public boolean m_finished = false;
    /* directory root value for @@ */
    private String m_directory = null;

    /**
     * Option to use encoding.
     * @param url could be file http ftp
     * @param conn oracle connection 
     * @param encoding preferred encoding
     */
    public ScriptExecutor(URL url, Connection conn, String encoding) {
        try {
            setConn(conn);
            setStmt(new BufferedReader(new InputStreamReader(url.openStream(), encoding)));
            m_baseURL = url;
        } catch (IOException e) {
            LOGGER.severe(Messages.getString("ScriptExecutor.0") + url.toString()); //$NON-NLS-1$
        }
    }
    public ScriptExecutor(URL url, Connection conn) {
        try {
            setConn(conn);
            setStmt(url.openStream());
            m_baseURL = url;
        } catch (IOException e) {
            LOGGER.severe(Messages.getString("ScriptExecutor.0") + url.toString()); //$NON-NLS-1$
        }
    }

    public ScriptExecutor(InputStream in, Connection conn) {
        setConn(conn);
        setStmt(in);
    }

    public ScriptExecutor(String s, Connection conn) {
        setConn(conn);
        setStmt(s);
    }

    public ScriptExecutor(Reader rdr, Connection conn) {
        setConn(conn);
        setStmt(rdr);
    }

    public ScriptExecutor(Connection conn) {
        setConn(conn);
    }

    public void setConn(Connection conn) {
        m_conn = conn;
    }

    public Connection getConn() {
        return m_conn;
    }

    public void setStmt(String s) {
        m_rdrForParser = new StringReader(s);
        BOMSkipper.skip(m_rdrForParser);
    }

    public void setStmt(InputStream in) {
        setStmt(new InputStreamReader(in));
    }

    private void setStmt(Reader rdr) {
        m_rdrForParser = rdr;
        BOMSkipper.skip(m_rdrForParser);
    }

    public void interrupt() {
        m_interrupted = true;     
        if (m_runner != null) {
        	m_runner.cancel();
        }
    }
    
    /**
     * Need this as the ScriptExecutor is now reused between commands in SQLcl. 
     * If you cancel one command, doesn't mean you want all future commands canceled. 
     * Hence we need the ability to reset it
     */
    public void setInterrupted(boolean interrupted){
    	m_interrupted = interrupted;
    }

    public ScriptRunnerContext getScriptRunnerContext() {
        return m_scriptRunnerContext;
    }

    public void setScriptRunnerContext(ScriptRunnerContext context) {
        m_scriptRunnerContext = context;
    }

    public void setOut(BufferedOutputStream out) {
        m_out = out;
    }

    public void setBaseURL(URL url){
    	m_baseURL = url;
    }
    
    public void run() {

        if (m_scriptRunnerContext == null) m_scriptRunnerContext = new ScriptRunnerContext();
        m_scriptRunnerContext.setInterrupted(false);//reset the interrupted flag, as this class is reused
        if (m_scriptRunnerContext.getTopLevel() == true) {
            m_scriptRunnerContext.setBaseConnection(m_conn);
            m_scriptRunnerContext.setCurrentConnection(m_conn);
        }
        if (m_baseURL != null) {// Bug No: 5585711
            m_scriptRunnerContext.setLastUrl(m_baseURL);
        }
        m_scriptRunnerContext.putProperty(ScriptRunnerContext.BASE_URL, m_baseURL);
        if (m_out == null) {
            if (m_scriptRunnerContext.getOutputStream() != null)
                m_out = m_scriptRunnerContext.getOutputStream().getMainStream();
            else
                m_out = new BufferedOutputStream(System.out);
            m_scriptRunnerContext.putProperty(ScriptRunnerContext.SYSTEM_OUT, Boolean.TRUE);
        }
        try {
            m_parser = SqlParserProvider.getScriptParserIterator(getScriptRunnerContext(), m_rdrForParser);
            // System.out.println(cmds.length);
            int i = 0;
            m_scriptRunnerContext.putProperty(ScriptRunnerContext.ERR_ANY_CMDS_IN_SCRIPT, Boolean.FALSE);
            m_scriptRunnerContext.putProperty(ScriptRunnerContext.ERR_ENCOUNTERED, Boolean.FALSE);
            m_scriptRunnerContext.putProperty(ScriptRunnerContext.RESULTS, null);
        	Boolean amICommandLineConnect=(Boolean)m_scriptRunnerContext.getProperty(ScriptRunnerContext.COMMANDLINECONNECT);
        	while (m_parser.hasNext()) { 
                CommandRegistry.fireBeginScriptListeners(m_conn, getScriptRunnerContext());
           	  	m_cmd = m_parser.next();
           	  	//do not skip first line if we are a connect from the command line converted into a scriptExecutor.
           	  	Boolean commandLineConnect=(Boolean)(m_scriptRunnerContext.getProperty(ScriptRunnerContext.COMMANDLINECONNECT));
           	  	if (((commandLineConnect==null)||(commandLineConnect.equals(Boolean.FALSE)))&&(m_scriptRunnerContext.getProperty(ScriptRunnerContext.LOGIN_FILE)==null && m_scriptRunnerContext.getProperty("SQLCLI_IGNORE_FIRST_LINE")!=null && m_scriptRunnerContext.getProperty("SQLCLI_IGNORE_FIRST_LINE").equals(true))) {
           	  		m_scriptRunnerContext.putProperty("SQLCLI_IGNORE_FIRST_LINE",false);
           	  		if (m_parser.hasNext()) {
           	  			m_cmd = m_parser.next();
           	  		} else  {
           	  			//if nothing is left, then go around and jump out.
           	  			continue;
           	  		}
           	  	}
           	  	//if its not a complete statement (and its the last statement), dont run it. leave it to the sqlcli loop. 	  	
                if(m_scriptRunnerContext.isCommandLine() && isFile(m_scriptRunnerContext) && !m_cmd.isComplete() && !m_parser.hasNext() && !m_scriptRunnerContext.getTopLevel()){
               	 if (!(m_scriptRunnerContext.getProperty(ScriptRunnerContext.ISSLASHSTATEMENT)!=null && Boolean.parseBoolean(m_scriptRunnerContext.getProperty(ScriptRunnerContext.ISSLASHSTATEMENT).toString()))) {
               		 // if this is a slash, just run it. its come from the buffer
               		 m_finished = true;
                 	 m_scriptRunnerContext.putProperty(ScriptRunnerContext.INCOMPLETE, m_cmd);
                	 break;
               	 }
               	 if (m_scriptRunnerContext.getProperty(ScriptRunnerContext.ISSLASHSTATEMENT)!=null && Boolean.parseBoolean(m_scriptRunnerContext.getProperty(ScriptRunnerContext.ISSLASHSTATEMENT).toString())) {
               		 //if its /, just run it.
               		 m_cmd.setExecutable(Boolean.TRUE);
               		 m_cmd.setRunnable(Boolean.TRUE);
               	 }

                }
                i++;
                try {
                    // init to no errs on every iteration.
                	if ((Boolean)m_scriptRunnerContext.getProperty(ScriptRunnerContext.ERR_ENCOUNTERED)){
                		m_scriptRunnerContext.putProperty(ScriptRunnerContext.ERR_ANY_CMDS_IN_SCRIPT, Boolean.TRUE);
                	}
                    m_scriptRunnerContext.putProperty(ScriptRunnerContext.ERR_ENCOUNTERED, Boolean.FALSE);
                    m_scriptRunnerContext.putProperty(ScriptRunnerContext.ERR_MESSAGE, null); // clear the err message

                    if (m_interrupted) {
                        break;
                    }
                    m_scriptRunnerContext.setOutputStreamWrapper(m_out);
                    m_scriptRunnerContext.putProperty(ScriptRunnerContext.CURRENT_CMD_COUNT, i);
                    //a semi colon on its own, is treated as the LIST command
                    if(m_cmd.getSQLOrig().trim().equals("") && m_cmd.getStatementTerminator()!=null && m_cmd.getStatementTerminator().equals(";")){
                    	m_cmd = changeToListCmd(m_cmd);
                    }
                    if (!(getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLCLI_LOGIN_SQL)!=null && getScriptRunnerContext().getProperty(ScriptRunnerContext.SQLCLI_LOGIN_SQL).equals(true) 
                    		&& getScriptRunnerContext().getProperty(ScriptRunnerContext.LOGIN_FILE_ON_CWD)!=null && (m_cmd.getStmtType().equals(StmtType.G_C_PLSQL)||m_cmd.getStmtType().equals(StmtType.G_C_SQL)))) {
                    	run(m_cmd);	
                    }
                    

                    // check for errors and log it if you have a valid error writer

                    if (m_scriptRunnerContext != null && ((Boolean) m_scriptRunnerContext.getProperty(ScriptRunnerContext.ERR_ENCOUNTERED)).booleanValue()) {
                        PrintWriter pw = m_scriptRunnerContext.getErrWriter();
                        if (pw != null) {
                            pw.println(m_cmd);
                            pw.println();
                            String err = (String) m_scriptRunnerContext.getProperty(ScriptRunnerContext.ERR_MESSAGE);
                            err = err.replaceAll("\n", "\nREM "); //$NON-NLS-1$ //$NON-NLS-2$
                            pw.println(err);
                            pw.println('\n');
                        }
                    }

                    // check for any dbms_output if oracle connection
                    // if (conn instanceof OracleConnection) {
                    // String s = DBUtil.getInstance().getDBMSOUTPUT(conn);
                    // if (!s.equals("\n"))
                    // out.write(s.getBytes());
                    // }
                    if (m_scriptRunnerContext.getExited() == true) {
                        break;
                    }
                    
                } catch (Exception e) {
                  Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getStackTrace()[0].toString(), e);
                  Boolean systemOut=((Boolean) m_scriptRunnerContext.getProperty(ScriptRunnerContext.SYSTEM_OUT));
                  try {
                      m_out.flush();
                  } catch (Exception ex) {
                      Logger.getLogger(getClass().getName()).log(Level.SEVERE, ex.getStackTrace()[0].toString(), ex);
                  }
                  if (systemOut==null||systemOut.equals(Boolean.FALSE)) {
                    try {
                      m_out.close();
                    } catch (Exception ex) {
                      Logger.getLogger(getClass().getName()).log(Level.SEVERE, ex.getStackTrace()[0].toString(), ex);
                    }
                  } 
                }
                if (m_scriptRunnerContext.getTopLevel()==true) {
                    if ((amICommandLineConnect==null)||(amICommandLineConnect.equals(Boolean.FALSE))) {
                    	if (!m_scriptRunnerContext.isLoginSQL())
                        saveHistoryItem(m_cmd);
                    }
                }
                m_finished = true;
                CommandRegistry.fireEndScriptListeners(m_conn, getScriptRunnerContext());
            }
        } finally {
            if (m_rdrForParser != null) {
                try {
                    m_rdrForParser.close();
                } catch (IOException io) {

                }
            }
        }
        if ((m_scriptRunnerContext.getTopLevel() == true) && (m_scriptRunnerContext.getCloseConnection() == true)) {
            //should this only fire on graceful exited (m_interrupted is set to false above.)
            m_scriptRunnerContext.writeDisconnectWarning();
        }
        if ((m_scriptRunnerContext.getTopLevel() == true) && (m_scriptRunnerContext.getCloseConnection() == true)) {
            // there has been a successful connection via sqlplus connect
            // so the connection has to be closed.
            m_scriptRunnerContext.closeCurrentConnection();
        }

        try {
            m_out.flush();
        } catch (IOException e) {
            if (!m_interrupted) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            } else {
            	m_interrupted = false;
            }
        }
        
        if ((m_scriptRunnerContext.getTopLevel() == true)) {
            if (m_scriptRunnerContext.getExited() == true) {
                m_scriptRunnerContext.reInitOnExit();
            } else {
                m_scriptRunnerContext.reInitNoExit();
                // holds whenever and substitution variables.
            }
        }
    }

    private ISQLCommand changeToListCmd(ISQLCommand cmd){
    	cmd.setSql("list");
    	cmd.setSQLOrig("list");
    	cmd.setModifiedSQL("list");
    	cmd.setStmtId(StmtSubType.G_S_LIST);
    	cmd.setResultsType(StmtResultType.G_R_NONE);
    	cmd.setStmtClass(StmtType.G_C_SQLPLUS);       
    	cmd.setExecutable(false);
    	cmd.setRunnable(true);
    	return cmd;
    }
    @SuppressWarnings("unchecked")
	private boolean isFile(ScriptRunnerContext scriptRunnerContext) {
    	if(scriptRunnerContext.getProperty(ScriptRunnerContext.APPINFOARRAYLIST)!=null && 
    	  ((ArrayList<String>)scriptRunnerContext.getProperty(ScriptRunnerContext.APPINFOARRAYLIST)).size() >0 ){
    		return true;
    	} else {
    		return false;
    	}
		}
		private void run(ISQLCommand command) throws IOException {
        if (m_runner == null) {
            m_runner = new ScriptRunner(m_conn, m_out, m_scriptRunnerContext);
            m_runner.setDirectory(getDirectory()); 
        }
        m_runner.run(command);
        if (getScriptRunnerContext().isDbmsEnabled()) {
            int defaultEnable=20000;
            Integer i = (Integer) getScriptRunnerContext().getProperty(ScriptRunnerContext.LASTSETSERVEROUTPUT);
            if ((i!=null)&&(i.intValue()>defaultEnable)) {
                defaultEnable=i.intValue();
            }
			enableDbmsOutput(getScriptRunnerContext().getBaseConnection(),defaultEnable);
			getScriptRunnerContext().clearDbmsFlags();
		} 

    }

    public void setDirectory(String dir) {
        m_directory = dir;
    }
    
    public String getDirectory() {
        return m_directory;
    }

    public static void enableDbmsOutput(Connection conn, int bufferSize) {
    	String  plsqlString = "BEGIN DBMS_OUTPUT.ENABLE(" + bufferSize + "); END;"; //$NON-NLS-1$ //$NON-NLS-2$
    	PreparedStatement stmt = null;
    	try {
    		if (conn == null) {
    				return ;
    		}
    		if(conn.isClosed()){
    				return;
    		}
    		stmt = conn.prepareCall(plsqlString);
    		stmt.execute();
    		
    	} catch(Exception e){
    		Logger.getLogger("ScriptExecutor").log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);		
    	}
    }
	/**
	 * @param history the history to set
	 */
	public static void setHistory(ICommandHistory history) {
		ScriptExecutor.history = history;
	}
	private void saveHistoryItem(ISQLCommand cmd) {
		if (history!=null) {
			if (!Boolean.parseBoolean(getScriptRunnerContext().getProperty(ScriptRunnerContext.NOHISTORY).toString()))
				if (getScriptRunnerContext().getTopLevel())
				history.saveCommand(cmd);
		}
	}
}

	