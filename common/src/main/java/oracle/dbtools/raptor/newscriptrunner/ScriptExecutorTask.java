/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.backgroundTask.IRaptorTaskRunMode;
import oracle.dbtools.raptor.backgroundTask.Messages;
import oracle.dbtools.raptor.backgroundTask.RaptorTask;
import oracle.dbtools.raptor.backgroundTask.TaskException;
//import oracle.dbtools.raptor.backgroundTask.utils.DatabaseQueryTask;

/**
 * @author jscapicc
 * ScriptExecutorTask is used to run scripts by features outside of the ScriptRunner in the worksheet.
 * It has been refactored from Script Executor to use RaptorTask
 * For example, Import Data uses this script to create tables and execute
 * Insert commands.
 */ 
public class ScriptExecutorTask  /*extends DatabaseQueryTask<Void>  {*/ 
			extends RaptorTask  <Void> {
    private Reader m_rdrForParser = null;
	private Connection m_connection = null;
    private ISQLCommand m_cmd = null;
    private ScriptRunner m_runner = null;
    private boolean m_interrupted = false;
    private int m_currentCommand = -1;
    private ScriptRunnerContext m_scriptRunnerContext = null;
    private BufferedOutputStream m_out = null;
    private URL m_baseURL = null;
    private Iterator<ISQLCommand> m_parser = null;
    public boolean m_finished = false;
    /* directory root value for @@ */
    private String m_directory = null;

    IRaptorTaskRunMode m_runMode = null;

    Logger m_logger = Logger.getLogger(getClass().getName());
    
    /**
     * Instantiates a new database query task. Has option to set mode, pauseable and cancellable.
     *
     * @param name the name
     * @param mode the mode
     * @param isPausable the is pausable
     * @param isCancellable the is cancellable
     */
    public ScriptExecutorTask(final String name, IRaptorTaskRunMode mode, boolean isPausable, boolean isCancellable) {
        this(name, mode, isPausable, isCancellable, true);
    }

    /**
     * Instantiates a new database query task. Has option to set mode, pausable , cancellable and determinate.
     *
     * @param name the name
     * @param mode the mode
     * @param isPausable the is pausable
     * @param isCancellable the is cancellable
     * @param isIndeterminate the is indeterminate
     */
/*    public ScriptExecutorTask(final String name, IRaptorTaskRunMode mode, boolean isPausable, boolean isCancellable, boolean isIndeterminate) {
      super(name, mode, isPausable, isCancellable, isIndeterminate);
     }*/
  
    public ScriptExecutorTask(final String name, IRaptorTaskRunMode mode, boolean isPausable, boolean isCancellable, boolean isIndeterminate) {
        super(name, isIndeterminate, mode);
        //setMessage(MessageFormat.format(Messages.getString("IRaptorTaskStatus.7"), new Object [] {name})); //$NON-NLS-1$ //$NON-NLS-2$
        setMessage(name);
        setPausable(isPausable);
        setCancellable(isCancellable);
      }    
      
    
    public void setConnection(Connection conn) {
        m_connection = conn;
        if (m_scriptRunnerContext != null && m_scriptRunnerContext.getCurrentConnection() == null) {  // if user did not set it in the context
        	m_scriptRunnerContext.setCurrentConnection(conn);
        }
        if (m_scriptRunnerContext != null && m_scriptRunnerContext.getBaseConnection() == null) {  // if user did not set it in the context
        	m_scriptRunnerContext.setBaseConnection(conn);
        }

    }

    public Connection getConnection() {
        return m_connection;
    }

    public String getQuery() {
        return "";
    }

    public void setStmt(String s) {
        m_rdrForParser = new StringReader(s);
    }

    public void setStmt(InputStream in) {
        setStmt(new InputStreamReader(in));
    }
    
    public void setStmt(InputStream in, String encoding) throws UnsupportedEncodingException{
    	setStmt(new InputStreamReader(in, encoding));
    }

    public void setStmt(Reader rdr) {
        m_rdrForParser = rdr;
    }
    
    //public void interrupt() {
    //    m_interrupted = true;
    //    if (m_sqlCommandRunner != null) {
    //        m_sqlCommandRunner.interrupt();
    //    }
    //     super.m_interrupt();
    //}
    //allow subclasses to access scriptRunner for cancel
    public ScriptRunner getScriptRunner() {
        return m_runner;
    }

    public ScriptRunnerContext getScriptRunnerContext() {
        return m_scriptRunnerContext;
    }

    public void setScriptRunnerContext(ScriptRunnerContext context) {
        m_scriptRunnerContext = context;

    }

    public BufferedOutputStream getOut() {
        return m_out;
    }
    
    public void setOut(BufferedOutputStream out) {
        m_out = out;
    }
    
    public void flushOut(boolean close){
    	try {
    		if (close){
    			m_out.flush();
    			m_out.close();
    		} else {
    			m_out.flush();
    		}	
    	} catch (IOException e){
    		
    	}
    }

    @Override
    protected Void doWork() throws TaskException {
        try {
            startup();
            try {
                checkCanProceed();
                loopThroughAllStatements();
            } finally {
                cleanup();
                setMessage(Messages.getString("IRaptorTaskStatus.4")); //$NON-NLS-1$
            }
        } catch (ExecutionException ee) {
            Throwable cause = ee.getCause();
            if (cause instanceof TaskException) {
                throw (TaskException) cause;
            } else {
                throw new TaskException(cause);
            }
        }
        return null;
    }
    
    protected void loopThroughAllStatements() throws ExecutionException {
        // loop through all of the statements/commands
        beforeLoopProcessing();

        // Main loop for processing each command
        // ISQLCommand command = null;
        m_currentCommand = -1;
        try {
            while (m_parser.hasNext()) {
                m_cmd=m_parser.next();
                m_currentCommand++;
                incrementCMDCount();
                this.checkCanProceed(); // frequent calls make sure the task is responsive to cancel/pausing
                if(!isInterupted()){ //
                    //check to see if it was canceled after a pause
                   	this.checkCanProceed();//this throws an exception is cancel was pressed
                	beforeRunStatementProcessing();
                    
                   	run(m_cmd);

                   	afterRunStatementProcessing();
                    // check for any dbms_output if oracle connection
                    // if (conn instanceof OracleConnection) {
                     if (m_scriptRunnerContext.getExited() == true) {
                        break;
                    }
                }
            }
            
        } catch (Exception e) {
        	Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
        	try {
        		m_out.close();
        	} catch (Exception ex) {
        	}
        } finally {
            if (this.m_rdrForParser!=null) {
                try {
                    this.m_rdrForParser.close();
                } catch (IOException io) {
                }
            }
        }
        
        afterLoopProcessing();
    }
 
    protected void beforeRunStatementProcessing()throws ExecutionException {
        m_scriptRunnerContext.putProperty(ScriptRunnerContext.ERR_ENCOUNTERED, Boolean.FALSE);
        m_scriptRunnerContext.putProperty(ScriptRunnerContext.ERR_MESSAGE, null); // clear the err message
        m_scriptRunnerContext.setOutputStreamWrapper(m_out);
        m_scriptRunnerContext.putProperty(ScriptRunnerContext.CURRENT_CMD_COUNT, m_currentCommand);
    }
    
    protected void afterRunStatementProcessing() throws ExecutionException{
       	// Commenting out this code which was extracted from Script Executor
    	// Because the PrintWriter doesn't support encoding
    	// Instead methods to test for errors, get stmt and get error msg are provided
    	/*if (m_scriptRunnerContext != null && ((Boolean) m_scriptRunnerContext.getProperty(ScriptRunnerContext.ERR_ENCOUNTERED)).booleanValue()) {
            PrintWriter pw = m_scriptRunnerContext.getErrWriter();
            if (pw != null) {
                pw.println(m_cmd);
                pw.println();
                String err = (String) m_scriptRunnerContext.getProperty(ScriptRunnerContext.ERR_MESSAGE);
                err = err.replaceAll("\n", "\nREM "); //$NON-NLS-1$ //$NON-NLS-2$
                pw.println(err);
                pw.println('\n');
            }
        }
        */
    }
    // The following two methods implemented for convenience of
    // afterRunStatementProcessing() implementors
    
    // Convenience method to get the current statement
    protected ISQLCommand getCurrentStatement(){
    	if (m_cmd != null && m_currentCommand >= 0){
    	  return m_cmd;
    	}
    	return null;
    }	
    // Convenience method to check if an error was encountered when a statement was run
    public boolean wasErrorEncountered(){
    	if (m_scriptRunnerContext != null && ((Boolean) m_scriptRunnerContext.getProperty(ScriptRunnerContext.ERR_ENCOUNTERED)).booleanValue()){
    		return true;
    	}
    	return false;
    }
    // Convenience method to get potential error message
    public String getStatementErrorMessage(){
    	if (getCurrentStatement() != null && wasErrorEncountered()){
    		return (String) m_scriptRunnerContext.getProperty(ScriptRunnerContext.ERR_MESSAGE);	
    	}
    	return "";
    }

    protected void startup() throws ExecutionException {
        if (m_scriptRunnerContext == null) m_scriptRunnerContext = new ScriptRunnerContext();
        if (m_scriptRunnerContext.getTopLevel() == true) {
            m_scriptRunnerContext.setBaseConnection(getConnection());
            m_scriptRunnerContext.setCurrentConnection(getConnection());
        }
        if (m_baseURL != null) {// Bug No: 5585711
            m_scriptRunnerContext.setLastUrl(m_baseURL);
        }
        m_scriptRunnerContext.putProperty(ScriptRunnerContext.BASE_URL, m_baseURL);
        if (m_out == null) {
            if (m_scriptRunnerContext.getOutputStream() != null)
                m_out = m_scriptRunnerContext.getOutputStream().getMainStream();
            else
                m_out = new BufferedOutputStream(System.out);
            m_scriptRunnerContext.putProperty(ScriptRunnerContext.SYSTEM_OUT, Boolean.TRUE);
        }
        m_parser = SqlParserProvider.getScriptParserIterator(getScriptRunnerContext(), m_rdrForParser);
     }
    
     protected void cleanup() throws ExecutionException {        
        if ((m_scriptRunnerContext.getTopLevel() == true) && (m_scriptRunnerContext.getCloseConnection() == true)) {
            //should this only fire on graceful exited (m_interrupted is set to false above.)
            m_scriptRunnerContext.writeDisconnectWarning();
        }

        if ((m_scriptRunnerContext.getTopLevel() == true) && (m_scriptRunnerContext.getCloseConnection() == true)) {
            // there has been a successful connection via sqlplus connect
            // so the connection has to be closed.

            try {
                m_connection.commit();
            } catch (SQLException e) {
                Logger.getLogger(ScriptExecutorTask.class.getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            }
            try {
                m_connection.close();
            } catch (SQLException e) {
                Logger.getLogger(ScriptExecutorTask.class.getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            }
        }

        try {
            m_out.flush();
        } catch (IOException e) {
            if (!m_interrupted) {
                Logger.getLogger(ScriptExecutorTask.class.getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            }
        }
        
        if ((m_scriptRunnerContext.getTopLevel() == true)) {
            if (m_scriptRunnerContext.getExited() == true) {
                m_scriptRunnerContext.reInitOnExit();
            } else {
                m_scriptRunnerContext.reInitNoExit();
                // holds whenever and substitution variables.
            }
        }
    }
    
    protected void run(ISQLCommand command) throws IOException {
        if (m_runner == null) {
            m_runner = new ScriptRunner(getConnection(), m_out, m_scriptRunnerContext);
            m_runner.setDirectory(getDirectory()); 
        }
        m_runner.run(command);

    }

    public String getConnectionName(){
    	return "";
    	//m_connection.
    	//return Connections.getInstance().getConnectionName(getConnection());
     }
    
    public void setDirectory(String dir) {
        m_directory = dir;
    }
    
    public String getDirectory() {
        return m_directory;
    }
    boolean isInterupted() {
        return Thread.currentThread().isInterrupted();
    }

    protected void checkCanProceed() throws ExecutionException {
        super.checkCanProceed();
    }
    
    private void incrementCMDCount() {
        int currentCmdCount = getNotNullCMDCount(); 
         m_scriptRunnerContext.putProperty(ScriptRunnerContext.CURRENT_CMD_COUNT, currentCmdCount+1); 
    }
    private int getNotNullCMDCount() {
        Integer currentCmdCount = (Integer)m_scriptRunnerContext.getProperty(ScriptRunnerContext.CURRENT_CMD_COUNT);
        if(currentCmdCount == null){
            currentCmdCount = 0;
        }
        return currentCmdCount;
    }
    
    public void beforeLoopProcessing() throws ExecutionException {
    	  m_scriptRunnerContext.putProperty(ScriptRunnerContext.RESULTS, null);
        CommandRegistry.fireBeginScriptListeners(getConnection(), getScriptRunnerContext());
    }
    
    public void afterLoopProcessing() throws ExecutionException {
        CommandRegistry.fireEndScriptListeners(getConnection(), getScriptRunnerContext());
    }
    
}
