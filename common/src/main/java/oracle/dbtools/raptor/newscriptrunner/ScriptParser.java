/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtResultType.G_R_NONE;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType.G_S_SET_UNKNOWN;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_SQLPLUS;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * Script parser will take a script and break it into logical statements which can be consumed by the SQL engine.
 *
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ScriptParser.java">Barry McGillin</a>
 */
public class ScriptParser extends CoreScriptParser {
    private static final Logger s_logger = Logger.getLogger(ScriptParser.class.getName());

    public  final static String TERMINATOR="terminator";
    final static String FORWARDSLASH="/";
    // this also has to pick and use the next character and handle Q'
    private List<ISQLCommand> m_sqlArray = new ArrayList<ISQLCommand>();
    private String m_rawSQL;
    private BufferedReaderStack  m_reader = null;
    private int m_readerLineCnt = -1; //BASE0 : note that the ScriptParser startline is base 0, just like the BasicEditor.
    //When reporting errors we add 1 on so it matches the gutter line number
    private int m_readerLineStartCnt = 0;

	  private int _numOfEmptyLineAbove = 0;
	  public static final String NUMEMPTYLINESABOVE = "NUMEMPTYLIENSABOVE";
    private ScriptRunnerContext _scriptRunnerContext = null;
    private boolean m_isFirstPartOfCompoundSetStmt = false;
    private int m_setSubCommandCount = 0;

    /**
     *
     * ScriptParser with an inputstream
     * @param stream {@link InputStream}.
     * 				This is the stream to parse
     */
    public ScriptParser(InputStream stream) {
        this(stream, null);
    }

    public ScriptParser(Reader rdr) {
        this(rdr, null);
    }

    public ScriptParser(InputStream in, Connection conn) {
        this(new InputStreamReader(in), conn);
    }

    public ScriptParser(Reader rdr, Connection conn) {
        scriptParserInit(rdr, conn);
    }

    private void scriptParserInit(Reader rdr, Connection c) {
        m_reader = new BufferedReaderStack(new BufferedReader(rdr));
    }

    public ScriptParser(String s) {
        this(s, null);
    }

    public ScriptParser(String s, Connection conn) {
        this(new StringReader(s), conn);
        this.m_rawSQL = s;
    }

    public static void main(String[] args) throws Exception {
        File f = new File(args[ 0 ]);
        FileInputStream in = new FileInputStream(f);
        ScriptParser s = new ScriptParser(in);
        // parse the file
        s.parse();
        ISQLCommand[] stmts = s.getSqlStatements();
        // loop the results
        for(int i=0;i<stmts.length;i++){
            System.out.print("Command "+i + ":" + stmts[i].getStmtType() + ":"+stmts[i].getStmtSubType() +"\n"+stmts[i].getSql()+"\n");
            if (stmts[i].isSqlPlusSetCmd()) {
                System.out.println(" SQLPLUS");
            } else if (stmts[i].isCreateCmd()) {
                System.out.println(" Create");
            } else if (stmts[i].isCreatePLSQLCmd()) {
                System.out.println(" PLSQL");
            } else if (stmts[i].isCreateSQLCmd()) {
                System.out.println(" SQL");
            }
        }
    }

    public void parse() {
        // process each line
        setLeftOver(null);
        setSetLeftOver(null);
        m_readerLineCnt = -1;//BASE0
        try {
            ISQLCommand cmd = next();
            while (cmd != null) {
                m_sqlArray.add(cmd);
                cmd = next();
                // add to the list
            }// end main loop
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
        }
    }

    public void parseinit() {
        // process each line
        setLeftOver(null);
        setSetLeftOver(null);
        m_readerLineCnt = -1;//BASE0
    }
    public ISQLCommand next() throws IOException {
       	m_reader.setContext(_scriptRunnerContext);
       	//if the last statement in a subscript is incompete. It can be completed in the parent script
       	if(_scriptRunnerContext!=null && isInFile()){ //only want to use leftover if going into another file. If on the command line then let sqlcli handle it
       	  SQLCommand incompleteCmd = (SQLCommand) _scriptRunnerContext.getProperty(ScriptRunnerContext.INCOMPLETE) ;
       	  if(incompleteCmd!=null && incompleteCmd.getOrigSQL()!=null){
       	  	if(getLeftOver() == null){
       	  		setLeftOver(incompleteCmd.getOrigSQL());
       	  	} else {
       	  		setLeftOver( getLeftOver() + " " + incompleteCmd.getOrigSQL());
       	  	}
       	  	_scriptRunnerContext.removeProperty(ScriptRunnerContext.INCOMPLETE) ;
       	  }
        }

       	String line = getTop(getLeftOver()); // TODO: refactor so null doesnt have to passed in
        setLeftOver(null);//this may be populated in the identifyEndOfStatement later on
        if (line == null) {
            //if there are trailing empty lines in a file. return a comment" statement with empty line above
        		if(_numOfEmptyLineAbove >0 && _scriptRunnerContext != null && _scriptRunnerContext.isCommandLine()){
        			return makeCommentWithEmptyLines(_numOfEmptyLineAbove);
        		}
            return null;
            // break;
        }
        //first see if we can match it with a ScriptParser Hint /*SQLDEV:QUERY*/ or /*SQLDEV:STMT*/
        SQLCommand stmtProperties = getPropertiesForParserHint(line);
        if(stmtProperties == null){
            // see if one of we can match the start of the input with a token that doesnt require a space after it.
            // this would match things like comments which require no space afterwards EX: /*this is a comment*/
            stmtProperties = getPropertiesWhenNoSpaceRequired(line);
        }
        // if not found check next level of commands
        // these commands are identified by tokens seperated by space EX: "UPDATE TABLE", or "SELECT *"
        if (stmtProperties == null) {
            stmtProperties = getPropertiesWhenTokensKnown(line);
            //minor hack - want to pretend it is ; terminated if indexOf = !=-1
            if ((stmtProperties!=null)&&(stmtProperties.getStmtSubType().equals(StmtSubType.G_S_ALIAS))) {
                if (line.indexOf('=')!=-1) {
                    stmtProperties.setStmtType(StmtType.G_C_SQL);
                    //SQL covers most - if it starts with begin or declare - go for PLSQL terminator
                    String[] equalsSplit=line.split("=");
                    if ((equalsSplit!=null)&&(equalsSplit.length>1)&&(equalsSplit[1]!=null)) {
                        String[] whiteSplit=equalsSplit[1].trim().split("\\s");
                        if ((whiteSplit!=null)&&(whiteSplit.length>0)&&(whiteSplit[0]!=null)) {
                            if(whiteSplit[0].equalsIgnoreCase("begin")||
                                (whiteSplit[0].equalsIgnoreCase("declare"))||(equalsSplit[1].startsWith("q'")||equalsSplit[1].startsWith("Q'"))) {//Q necessary ? nice to have validate next char
                            	stmtProperties.setStmtType(StmtType.G_C_PLSQL);
                            }
                        }
                    }

                }
            }
            if ((stmtProperties!=null)&&(stmtProperties.getStmtSubType().equals(StmtSubType.G_S_NET))) {
                if (line.indexOf('=')!=-1) {//allow multiline assignments ending in ;
                    stmtProperties.setStmtType(StmtType.G_C_SQL);
                }
            }
        }
        // if not found check next level of commands
        // these command require multiple tokens to be recognized
        // the line is stripped of comments and junk and then white space. we try and match "setspoolon" for input like "set spool on". =
        // G_S_SET_SPOOL_ON
        // used for SET commands mostly and CREATETABLE, CREATEFUNCTION . Just to give more detail about the CREATE
        if (stmtProperties == null) {
            stmtProperties = getPropertiesWhenMulitpleTokensKnown(line);
            if ((stmtProperties!=null)&&(stmtProperties.getStmtSubType().equals(StmtSubType.G_S_CREATE_UNKNOWN)
                    ||((stmtProperties.getStmtSubType().equals(StmtSubType.G_S_WITH)
                            &&(stmtProperties.getStmtType().equals(StmtType.G_C_SQL)))))) {
                //with might be a with /* */ procedure which might be a plsql
                stmtProperties.setProperty(SQLCommand.CREATEREFINE, Boolean.TRUE);
            }
        }
        //last hope before we mark it as unmatched.
        //Currently we dont have a parser for SQL Server, Sybase, MySQL , ... scripts
        //So only very similar statements are being recognizes and run correctly.
        //A new parser should be written for each third party script language, but in the mean time we can do  a bit of a hack.
        //Even for Oracle there may be other CREATE and SET commands (multiTokens) which we have not explicitly recognized.
        //Firstly third party databases may have other types or other names for objects. Example CREATE DATATYPE. So recognize CREATE statements
        if(stmtProperties == null){
            stmtProperties = getHalfRecognizedMultiToken(line);
            if ((stmtProperties!=null)&&(stmtProperties.getStmtSubType().equals(StmtSubType.G_S_CREATE_UNKNOWN)
                    ||((stmtProperties.getStmtSubType().equals(StmtSubType.G_S_WITH)
                            &&(stmtProperties.getStmtType().equals(StmtType.G_C_SQL)))))) {
                //with might be a with /* */ procedure which might be a plsql
                stmtProperties.setProperty(SQLCommand.CREATEREFINE, Boolean.TRUE);
            }
        }

        // statement unmatched, mark as unknown, and unrunnable if Oracle. But third party connections will execute it
        if (stmtProperties == null) {
            stmtProperties = createUnknownStatementProperties();
        }
        stmtProperties = refineScriptRecognition(stmtProperties,line);
        // We should use more than the first token to identify a statement correctly.
        // For Example the statement was recognized as SET DEFINE , but is it ON or OFF?
        stmtProperties = refineStmtRecognition(stmtProperties, line);
        //remove the comments from the text so we can "see" better
        String processLine = line.replaceAll("--.*|/\\*((.|\\n)(?!=*/))+\\*/", "");
        //remove the string literals from the text so we can "see" better.
        processLine = processLine.replaceAll("'(.)*'","");
        //remove white space and uppercase everything so we can "see" better
        processLine = processLine.replaceAll("\\s+","").toUpperCase();
        String withComments=line.replaceAll("\\s+","").toUpperCase();
        if(withComments.indexOf("/*+WITH_PLSQL")!=-1 ){
        	stmtProperties.setProperty(TERMINATOR, FORWARDSLASH);
        }
        if(processLine.indexOf("WITHFUNCTION")!=-1 || processLine.indexOf("WITHPROCEDURE")!=-1){
        	stmtProperties.setProperty(TERMINATOR, FORWARDSLASH);
        }
        // read all lines that are part of this statement
        // add the completed text to the stmtProperties and the start and end line numbers
        stmtProperties = identifyEndOfStatement(line, m_reader, stmtProperties);

        if(stmtProperties.getStmtResultType() != null && stmtProperties.getStmtSubType()== SQLCommand.StmtSubType.G_S_WITH){
        	stmtProperties.setStmtType(StmtType.G_C_SQL);
        }
        stmtProperties = refineConnectOnFirstLine(stmtProperties,line);
        stmtProperties.setProperty(NUMEMPTYLINESABOVE, _numOfEmptyLineAbove);
        return stmtProperties;
    }

	private ISQLCommand makeCommentWithEmptyLines(int _numOfEmptyLineAbove2) {
		ISQLCommand stmtProperties = createUnknownStatementProperties();
		stmtProperties.setStmtId(StmtSubType.G_S_COMMENT_SQL);
		StringBuffer sb = new StringBuffer();
		for(int i=1;i<=_numOfEmptyLineAbove;i++){
			sb.append("\n");
		}
		stmtProperties.setSQLOrig(sb.toString());
		stmtProperties.setSql(sb.toString());
		stmtProperties.setModifiedSQL(sb.toString());
		stmtProperties.setProperty(NUMEMPTYLINESABOVE, _numOfEmptyLineAbove);
		_numOfEmptyLineAbove = 0;
		return stmtProperties;
		}

	private SQLCommand refineConnectOnFirstLine(SQLCommand stmtProperties, String line) {
		if (stmtProperties.getStmtSubType().equals(StmtSubType.G_S_UNKNOWN) && m_readerLineCnt <1) {
			String patterns = "(\\w+\\/\\w+@\\w+(\\s+as\\s+(sysdba)|(sysoper))?)|\\w+\\/\\w+@\\w+\\s+as\\s+sysdba|\\w+\\/\\w+@\\w+\\s+as\\s+sysoper|\\w+\\/\\w+\\s+as\\s+sysdba|\\w+\\/\\w+\\s+as\\s+sysoper|\\w+\\/\\w+";
			Pattern pattern=Pattern.compile(patterns, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(line.trim());
					if (matcher.matches()) {
						//Do not run the conenct/connect line at the top
						stmtProperties.setRunnable(false);
					}
		}
		return stmtProperties;
	}

	private SQLCommand refineScriptRecognition(SQLCommand stmtProperties, String line) {
		//If single line then make this sqlplus, otherwise make it plsql ending in /
    	if (stmtProperties.getStmtType().equals(SQLCommand.StmtType.G_C_SCRIPT)) {
    		if (line.split("\\s+").length>1) {
    			stmtProperties.setStmtType(StmtType.G_C_SQLPLUS);
    		}
    	}
		return stmtProperties;
	}

	private SQLCommand getPropertiesForParserHint(String line) {
        SQLCommand prop = null;
        if(line.startsWith("/*")){
           line = line.toLowerCase();
           for(String key:SQLStatementTypes.hintTokens.keySet()) {
                if(line.startsWith(key)){
                    prop = SQLStatementTypes.hintTokens.get(key);
                }
            }
        }
        return prop;
    }

    private SQLCommand getHalfRecognizedMultiToken(String line) {
        line=line.trim();
    	Set<String> keys = SQLStatementTypes.halfRecognizedMultiToken.keySet();
        for (String key: keys) {
            if (line.toLowerCase().startsWith(key)) {
                return SQLStatementTypes.halfRecognizedMultiToken.get(key);
            }
        }
        return null;
    }
    /**
     * set usage error to be picked up on execute - better than uncaught arrayoutofbounds
     */
    private void setError(SQLCommand stmtProperties) {
       stmtProperties.setProperty(SQLCommand.PROP_ERROR_STRING, ScriptRunnerDbArb.getString(ScriptRunnerDbArb.SQLPLUS_COMMAND_USAGE));
    }

    private void resolveCompoundSetCommands(SQLCommand stmtProperties, String line) {

        if ( m_isFirstPartOfCompoundSetStmt) {
            m_isFirstPartOfCompoundSetStmt = false;
            stmtProperties.setIsSetCompoundCmd(true);
            stmtProperties.setCompoundSetcmd(line);
            if (m_setSubCommandCount > 0) {
                --m_setSubCommandCount;
            }
        } else if (m_setSubCommandCount > 0) {
            stmtProperties.setIsSetCompoundCmd(true);
            stmtProperties.setCompoundSetcmd(null);
            --m_setSubCommandCount;
        } else if (m_setSubCommandCount == 0) {
            stmtProperties.setIsSetCompoundCmd(false);
            stmtProperties.setCompoundSetcmd(null);
        }
    }

    /**
     * set the other options like object name or "ON" "OFF"
     */
    private SQLCommand refineStmtRecognition(SQLCommand stmtProperties, String line) {
        String property = null;
        String propertyNotLowered = null;

         if (stmtProperties.getStmtSubType() == SQLCommand.StmtSubType.G_S_SET) {
             // is this a compound set statement or an SQL CONSTRAINT(S), ROLE or TRANSACTION statement

             String[] tokens = getTokens(line);
             if (tokens.length >= 2) {
                 if (tokens[1].equalsIgnoreCase("constraint")  ||  //$NON-NLS-1$
                      tokens[1].equalsIgnoreCase("constraints") ||  //$NON-NLS-1$
                      tokens[1].equalsIgnoreCase("role") ||  //$NON-NLS-1$
                      tokens[1].equalsIgnoreCase("transaction")) //$NON-NLS-1$
                  {
                       SQLCommand subCmd = getSetCommand(tokens[1]);
                       subCmd.setOrigSQL(line);
                       subCmd.setModifiedSQL(line);
                       subCmd.setStartLine(stmtProperties.getStartLine());
                       subCmd.setEndLine(stmtProperties.getEndLine());
                       subCmd.setLines(stmtProperties.getLines());
                       subCmd.setOrigEndLine(stmtProperties.getOrigEndLine());
                       subCmd.setOrigStartLine(stmtProperties.getOrigStartLine());
                       subCmd.setStmtType(StmtType.G_C_SQL);
                       //((SQLCompoundCommand)stmtProperties).addCommand(subCmd);
                       stmtProperties = subCmd;
                  } else if (tokens[1].equalsIgnoreCase("worksheetname")) { //$NON-NLS-1$
                      //needs to be user defined ie to end of line rather than plus continuation charaacter,
                      SQLCommand subCmd = getSetCommand(tokens[1]);
                      subCmd.setOrigSQL(line);
                      subCmd.setModifiedSQL(line);
                      subCmd.setStartLine(stmtProperties.getStartLine());
                      subCmd.setEndLine(stmtProperties.getEndLine());
                      subCmd.setLines(stmtProperties.getLines());
                      subCmd.setOrigEndLine(stmtProperties.getOrigEndLine());
                      subCmd.setOrigStartLine(stmtProperties.getOrigStartLine());
                      subCmd.setStmtType(StmtType.G_C_USERDEFINED);
                      //((SQLCompoundCommand)stmtProperties).addCommand(subCmd);
                      stmtProperties = subCmd;
                      if (tokens.length>2) {
                          propertyNotLowered = tokens[ 2 ];
                          property = tokens[ 2 ].toLowerCase();
                      } else {
                          this.setError(stmtProperties);
                          return stmtProperties;
                      }
                      //the following lines for worksheetname are a duplication of further down where getStmtSubType != SET
                      //setup on/off differently, everything else can be saved as is. EX: SET SPOOL filename, save the filename, or SET DEFINE '!'.
                      if (property.equals("on")) { //$NON-NLS-1$
                          stmtProperties.setProperty(SQLCommand.PROP_STATUS_BOOLEAN, new Boolean(true));
                      } else if (property.equals("off")) { //$NON-NLS-1$
                          stmtProperties.setProperty(SQLCommand.PROP_STATUS_BOOLEAN, new Boolean(false));
                      }
                      //do this anyway, the more info the merrier, although if the above set a boolean value , its only for historical sake we keep the "on" and "off". But look at SetServeroutOutput! it still references PROP_STRING for "on" and "off".
                      if ((propertyNotLowered != null) && (!(propertyNotLowered.equals("")))) {
                         stmtProperties.setProperty(SQLCommand.PROP_STRING, propertyNotLowered);
                      } else {
                         stmtProperties.setProperty(SQLCommand.PROP_STRING, property);
                      }
                      if(stmtProperties.getProperty(SQLCommand.PROP_STATUS_BOOLEAN)==null){//for those which are not on or off, set it too on, like spool c:\filename.txt
                          stmtProperties.setProperty(SQLCommand.PROP_STATUS_BOOLEAN, new Boolean(true));
                      }
                  } else if (tokens[1].equalsIgnoreCase("history")) {
                	  //needs to be user defined ie to end of line rather than plus continuation charaacter,
                      SQLCommand subCmd = getSetCommand(tokens[1]);
                      subCmd.setOrigSQL(line);
                      subCmd.setModifiedSQL(line);
                      subCmd.setStartLine(stmtProperties.getStartLine());
                      subCmd.setEndLine(stmtProperties.getEndLine());
                      subCmd.setLines(stmtProperties.getLines());
                      subCmd.setOrigEndLine(stmtProperties.getOrigEndLine());
                      subCmd.setOrigStartLine(stmtProperties.getOrigStartLine());
                      subCmd.setStmtType(StmtType.G_C_USERDEFINED);
                      stmtProperties = subCmd;
                  } else {
                       //tokens = getTokens(SQLPLUS.removeDashNewline(line)); //multiple tokens may use line continuation '-'
                       stmtProperties.setProperty(oracle.dbtools.raptor.newscriptrunner.commands.Set.SET_TOKENS, tokens);
                       stmtProperties.setStmtClass(StmtType.G_C_SQLPLUS);
                       //to be late evaluated when SQLCompoundCommand has no subcommands to handle continuation initCompoundSetCommand((SQLCompoundCommand)stmtProperties);
                  }
               } else {
                   stmtProperties.setStmtSubType(StmtSubType.G_S_SET_ECHO);//some valid inocuous set command
                   setError(stmtProperties);//complain about arguments
                   stmtProperties.setStmtClass(StmtType.G_C_SQLPLUS);//no tokens eat the line and error
               }
               return stmtProperties;
         } else if(stmtProperties.isSqlPlusSetCmd() ||
               stmtProperties.isCreateCmd() ||
               stmtProperties.getStmtSubType() == SQLCommand.StmtSubType.G_S_SET_WORKSHEETNAME){ //SET SPOOL ON, SET SPOON filename, CREATE TABLE tablename
               String[] tokens = getTokens(line);
               if (tokens.length>2) {
                   propertyNotLowered = tokens[ 2 ];
                   property = tokens[ 2 ].toLowerCase();
               } else {
            	   propertyNotLowered = tokens[ 2 ];
                   property = tokens[ 2 ].toLowerCase();
                   this.setError(stmtProperties);
                   return stmtProperties;
               }
            //if CREATE OR     REPLACE   PROCEDURE <procname> , then property = "REPLACE"
               //CREATE GLOBAL TEMPORARY TABLE <tablename>
               if(property.equals("replace") || property.equals("temporary")){ //$NON-NLS-1$
                   if (tokens.length>4) {
                       propertyNotLowered = tokens[ 4 ];
                       property = tokens[4];
                   } else {
                       this.setError(stmtProperties);
                       return stmtProperties;
                   }
               }
        } else if ( stmtProperties.getStmtSubType() == SQLCommand.StmtSubType.G_S_SPOOL ||
                    stmtProperties.getStmtSubType() == SQLCommand.StmtSubType.G_S_DEFINE){
            String[] tokens = getTokens(line);
            if (tokens.length>1) {
                propertyNotLowered = tokens[ 1 ];
                property = tokens[ 1 ].toLowerCase();
            } else {
                return stmtProperties;
            }
        } else {
            //nothing to do here
            return stmtProperties;
        }

        //setup on/off differently, everything else can be saved as is. EX: SET SPOOL filename, save the filename, or SET DEFINE '!'.
        if (property.equals("on")) { //$NON-NLS-1$
            stmtProperties.setProperty(SQLCommand.PROP_STATUS_BOOLEAN, new Boolean(true));
        } else if (property.equals("off")) { //$NON-NLS-1$
            stmtProperties.setProperty(SQLCommand.PROP_STATUS_BOOLEAN, new Boolean(false));
        }
        //do this anyway, the more info the merrier, although if the above set a boolean value , its only for historical sake we keep the "on" and "off". But look at SetServeroutOutput! it still references PROP_STRING for "on" and "off".
        if ((propertyNotLowered != null) && (!(propertyNotLowered.equals("")))) {
           stmtProperties.setProperty(SQLCommand.PROP_STRING, propertyNotLowered);
        } else {
           stmtProperties.setProperty(SQLCommand.PROP_STRING, property);
        }
        if(stmtProperties.getProperty(SQLCommand.PROP_STATUS_BOOLEAN)==null){//for those which are not on or off, set it too on, like spool c:\filename.txt
        	stmtProperties.setProperty(SQLCommand.PROP_STATUS_BOOLEAN, new Boolean(true));
        }
        return stmtProperties;
    }

    private void lateEvaluateInitCompoundSetCommand(SQLCompoundCommand cmd, boolean newlineRemoved) {
        SQLCompoundCommand compCmd = cmd;
        Boolean parsed = (Boolean) compCmd.getProperty(oracle.dbtools.raptor.newscriptrunner.commands.Set.SET_PARSED);
        if (parsed == null || !parsed) {

            String[] tokens = null;
            if (!(newlineRemoved)) {//this is the first set statement of a compound set.
                tokens = getTokens(SQLPLUS.removeDashNewline(cmd, cmd.getOrigSQL()));
            } else {
                tokens = getTokens(cmd.getOrigSQL());
            }
                //(String[]) compCmd.getProperty(oracle.dbtools.raptor.newscriptrunner.commands.Set.SET_TOKENS);
            cmd.setProperty(oracle.dbtools.raptor.newscriptrunner.commands.Set.SET_PARSED, Boolean.TRUE);
            if (tokens != null) {
                int lastCommandEnd = 0;
                for (int i = 1; i < tokens.length; i ++) {
                    SQLCommand subCmd = null;
                    boolean allTokens = false;;
                    if (i!=lastCommandEnd+1) {
                        String tokenLower=tokens[ i ].toLowerCase(Locale.US);
                        subCmd = getSetCommand(tokenLower);
                        SQLCommand prevTokenCommand=null;
                        if (i==lastCommandEnd+2) {
                        	prevTokenCommand=getSetCommand(tokens[ i-1 ].toLowerCase(Locale.US));
                        }
                        if ((prevTokenCommand!=null)&&(prevTokenCommand.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SET_DDL))) {
                            //always eat next word after ddl - it may match lots of other sets that may otherwise cause a multiset.
                            subCmd=null;
                            continue;
                        }
                        if ((prevTokenCommand!=null)&&(prevTokenCommand.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SET_ERROR_LOGGING))) {

                           break;

                        }
                        if ((prevTokenCommand!=null)&&(prevTokenCommand.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SET_SERVEROUTPUT))) {
                            //better way of doing this synchronised hash set - only on set serveroutput and format so will not slow other commands
                            //This was originally for format only actually out of position anything is eated - double check on size and format (will be noop second time)- minor performance hit.
                        	String[] toeat=new String[] {
                                "size", //$NON-NLS-1$
                                "format", //$NON-NLS-1$
                                "forma", //$NON-NLS-1$
                                "form", //$NON-NLS-1$
                                "for", //$NON-NLS-1$
                                "wrapped", //$NON-NLS-1$
                                "wrappe", //$NON-NLS-1$
                                "wrapp", //$NON-NLS-1$
                                "wrap", //$NON-NLS-1$
                                "wra",  //$NON-NLS-1$
                                "word_wrapped",  //$NON-NLS-1$
                                "word_wrappe",  //$NON-NLS-1$
                                "word_wrapp",  //$NON-NLS-1$
                                "word_wrap",  //$NON-NLS-1$
                                "word_wra",  //$NON-NLS-1$
                                "word_wr",  //$NON-NLS-1$
                                "word_w",  //$NON-NLS-1$
                                "word_",  //$NON-NLS-1$
                                "word",  //$NON-NLS-1$
                                "wor",  //$NON-NLS-1$
                                "truncated",  //$NON-NLS-1$
                                "truncate",  //$NON-NLS-1$
                                "truncat",  //$NON-NLS-1$
                                "trunca",  //$NON-NLS-1$
                                "trunc",  //$NON-NLS-1$
                                "trun",  //$NON-NLS-1$
                                "tru",  //$NON-NLS-1$
                                "unlimited",  //$NON-NLS-1$
                                "unlimite",  //$NON-NLS-1$
                                "unlimit",  //$NON-NLS-1$
                                "unlimi",  //$NON-NLS-1$
                                "unlim",  //$NON-NLS-1$
                                "unli",  //$NON-NLS-1$
                                "unl"  //$NON-NLS-1$
                            };
                            //serveroutput [any]
                            // [SIZE {n | UNL[IMITED]}]
                            //[ FOR[MAT] { WRA[PPED] | WOR[D_WRAPPED] | TRU[NCATED] } ]
                            //format and size in any order
                            int internalCount=i;//on any (already on any)
                            boolean outerContinue=false;
                            while (true) {
                            	internalCount++;//past end, on format or size or known mis type or start of next set
                                //at end break
                                if (internalCount>=tokens.length) {
                                   break;//end of input.
                                }
                                //in format any
                                if (tokens[ internalCount ].toLowerCase(Locale.US).matches("^fo(?:r|rm|rma|rmat)$")) {  //$NON-NLS-1$
                                    //actually checks for format options internalcount ++ if  not at end and WRA[PPED] | WOR[D_WRAPPED] | TRU[NCATED
                                    internalCount++;//on option or gone off the end
                                    if ((internalCount)>=(tokens.length)) {
                                        break;
                                    }
                                    //eat option if OK or size
                                    String optionToEat=tokens[ internalCount ].toLowerCase(Locale.US);
                                    boolean matchFound=false;
                                    for (String check:toeat) {
                                        if (optionToEat.equals(check)) {
                                           matchFound=true;
                                           break;
                                        }
                                    }
                                    if (!matchFound) {
                                        internalCount--;
                                    }
                                    continue;
                                }
                                //in size any
                                if ((tokens[ internalCount ].toLowerCase(Locale.US).equals("size"))) {  //$NON-NLS-1$
                                    internalCount++;//on any
                                    continue;
                                }
                                //not in format or set could be out of place string - note set serveroutput can not be compound set followed by wra on
                                //eat option if OK or size
                                String optionToEat=tokens[ internalCount ].toLowerCase(Locale.US);
                                boolean matchFound=false;
                                for (String check:toeat) {
                                    if (optionToEat.equals(check)) {
                                       matchFound=true;
                                       break;
                                    }
                                }
                                if (matchFound) {
                                    //match found move on one
                                    continue;
                                }
                                //not in that lot -> new set
                                outerContinue=true;
                                i=internalCount-1;
                                fillSubCmd(cmd,tokens,lastCommandEnd,internalCount-1);
                                lastCommandEnd = internalCount-1;
                                break;
                            }
                            if (outerContinue) {
                            	continue;
                            }
                        }
                        if ((prevTokenCommand!=null)&&(prevTokenCommand.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SET_AUTOTRACE))) {
                            //Usage: SET AUTOT[RACE] {OFF | ON | TRACE[ONLY]} [EXP[LAIN]] [STAT[ISTICS]]

                        	//better way of doing this synchronised hash set - only on set serveroutput and format so will not slow other commands
                            //This was originally for format only actually out of position anything is eated - double check on size and format (will be noop second time)- minor performance hit.
                        	String[] toeat=new String[] {
                                "exp",  //$NON-NLS-1$
                                "expl", //$NON-NLS-1$
                                "expla", //$NON-NLS-1$
                                "explai", //$NON-NLS-1$
                                "explain", //$NON-NLS-1$
                                "stat", //$NON-NLS-1$
                                "stati", //$NON-NLS-1$
                                "statis", //$NON-NLS-1$
                                "statist", //$NON-NLS-1$
                                "statisti", //$NON-NLS-1$
                                "statistic", //$NON-NLS-1$
                                "statistics" //$NON-NLS-1$
                            };
                            //serveroutput [any]
                            // [SIZE {n | UNL[IMITED]}]
                            //[ FOR[MAT] { WRA[PPED] | WOR[D_WRAPPED] | TRU[NCATED] } ]
                            //format and size in any order
                            int internalCount=i;//on any (already on any)
                            boolean outerContinue=false;
                            while (true) {
                            	internalCount++;//past end, on format or size or known mis type or start of next set
                                //at end break
                                if (internalCount>=tokens.length) {
                                   break;//end of input.
                                }
                                //in format any
                                if (tokens[ internalCount ].toLowerCase(Locale.US).matches("^fo(?:r|rm|rma|rmat)$")) {  //$NON-NLS-1$
                                    //actually checks for format options internalcount ++ if  not at end and WRA[PPED] | WOR[D_WRAPPED] | TRU[NCATED
                                    internalCount++;//on option or gone off the end
                                    if ((internalCount)>=(tokens.length)) {
                                        break;
                                    }
                                    //eat option if OK or size
                                    String optionToEat=tokens[ internalCount ].toLowerCase(Locale.US);
                                    boolean matchFound=false;
                                    for (String check:toeat) {
                                        if (optionToEat.equals(check)) {
                                           matchFound=true;
                                           break;
                                        }
                                    }
                                    if (!matchFound) {
                                        internalCount--;
                                    }
                                    continue;
                                }
                                //in size any
                                if ((tokens[ internalCount ].toLowerCase(Locale.US).equals("size"))) {  //$NON-NLS-1$
                                    internalCount++;//on any
                                    continue;
                                }
                                //not in format or set could be out of place string - note set serveroutput can not be compound set followed by wra on
                                //eat option if OK or size
                                String optionToEat=tokens[ internalCount ].toLowerCase(Locale.US);
                                boolean matchFound=false;
                                for (String check:toeat) {
                                    if (optionToEat.equals(check)) {
                                       matchFound=true;
                                       break;
                                    }
                                }
                                if (matchFound) {
                                    //match found move on one
                                    continue;
                                }
                                //not in that lot -> new set
                                outerContinue=true;
                                i=internalCount-1;
                                fillSubCmd(cmd,tokens,lastCommandEnd,internalCount-1);
                                lastCommandEnd = internalCount-1;
                                break;
                            }
                            if (outerContinue) {
                            	continue;
                            }
                        }
                        if ((subCmd!=null)&&(((tokenLower.startsWith("wra")||(tokenLower.startsWith("tru"))))/*&&(i>2)&&(tokens[i-1].toLowerCase(Locale.US).startsWith("for")) want tru and wra to be part of serveroutput even if in wrong order*/)) { //$NON-NLS-1$ //$NON-NLS-2$  //$NON-NLS-3$
                        	//possible format wrapped for set serveroutput on size unlimited format wrapped - want to peek back for serverout
                        	for (int peekBack=lastCommandEnd+1;peekBack<i;peekBack++) {
                        		if (tokens[peekBack].toLowerCase(Locale.US).startsWith("serverout")) { //$NON-NLS-1$
                        			subCmd=null;//its part of the set serverout not a new command.
                        			break;
                        		}
                        	}
                        }

                        if ((subCmd!=null)&&(tokenLower.startsWith("linenum"))) { //$NON-NLS-1$ //$NON-NLS-2$  //$NON-NLS-3$
                              subCmd=null;//its part of the set serverout not a new command.
                        	  break;
                        }

                        if ((subCmd!=null)&&(tokenLower.equals("table") //$NON-NLS-1$
                                ||(tokenLower.equals("worksheetname")) //$NON-NLS-1$
                                ||(tokenLower.equals("spool")))) { //$NON-NLS-1$
                            //table is used in subclauses tab is the match
                            //set spool does not exist it is an error.
                            //worksheetname isnt a keyword it is a standalone thing.
                            subCmd = null;
                        } else
                            if (subCmd == null) {
                                allTokens = checkAllTokens(tokenLower);
                                if ((allTokens==true)&&((tokenLower.startsWith("wra")||(tokenLower.startsWith("tru")))/*&&(i>2)&&(tokens[i-1].toLowerCase(Locale.US).startsWith("for")) want wrapped/truncated to be part of serveroutput even if out of order*/)) { //$NON-NLS-1$ //$NON-NLS-2$  //$NON-NLS-3$
                                	//possible format wrapped or format truncated for set serveroutput on size unlimited format wrapped - want to peek back for serverout
                                	for (int peekBack=lastCommandEnd+1;peekBack<i;peekBack++) {
                                		if (tokens[peekBack].toLowerCase(Locale.US).startsWith("serverout")) { //$NON-NLS-1$
                                			allTokens=false;//its part of the set serverout not a new command.
                                			break;
                                		}
                                	}
                                }
                                if ((allTokens==true) && (tokenLower.equals("context") || tokenLower.equals("default"))) { //$NON-NLS-1$
                                    allTokens=false; /* the match con is for concat */
                                }
                                if ((allTokens==true) &&(tokenLower.equals("table") //$NON-NLS-1$
                                        ||(tokenLower.equals("worksheetname")) //$NON-NLS-1$
                                        ||(tokenLower.equals("spool")))) { //$NON-NLS-1$
                                    //table is used in subclauses tab is the match
                                    //set spool does not exist it is an error.
                                    //worksheetname isnt a keyword it is a standalone thing.
                                    allTokens=false;
                                }
                            }
                        //head is ok in markup command but gets confused with set hea[ding] on off
                        //so if token is head and next token is not on or off set subcommand null and all tokens false
                        if (tokenLower.equals("head")&&(tokens[lastCommandEnd+1].toLowerCase(Locale.US).startsWith("mark"))) { //$NON-NLS-1$ //$NON-NLS-2$
                            //next exists and is not on or off
                            if ((i+1)!=(tokens.length-1) && (!((tokens[ i +1 ].toLowerCase(Locale.US).equals("on"))||  //$NON-NLS-1$
                                    (tokens[ i +1 ].toLowerCase(Locale.US).equals("off"))))) {  //$NON-NLS-1$
                                subCmd = null;
                                allTokens=false;
                            }
                        }
                    }
                    String lastLower = "";  //$NON-NLS-1$
                    String secondLastLower = "";  //$NON-NLS-1$
                    if (((subCmd != null) || (allTokens == true))) {// set subcmd == null if this could be text rather than keyword
                        lastLower = tokens[ i - 1 ].toLowerCase(Locale.US);
                        if ((i > (lastCommandEnd + 1))) {
                            secondLastLower = tokens[ i - 2].toLowerCase(Locale.US);
                        }
                        // commands that can be followed by tokens in a larger command
                        if ((i > (lastCommandEnd + 1))
                                && ((tokens[lastCommandEnd+1].toLowerCase(Locale.US).startsWith("errorl") && ((lastLower.equals("table")|| lastLower.equals("identifier")) && ( //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
                                        /*this table or identified is not a text ie is not proceeded by table or identifier*/
                                        !(secondLastLower.equals("table")|| secondLastLower.equals("identifier"))))) || //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
                                        (tokens[lastCommandEnd+1].toLowerCase(Locale.US).startsWith("mark") //$NON-NLS-1$
                                                && (lastLower.equals("table") || lastLower.equals("body")||lastLower.equals("head")) //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
                                                /* and this table head or body is not a text is is not proceeded by table head or body */
                                                &&(!(secondLastLower.equals("table") || secondLastLower.equals("body")||secondLastLower.equals("head")))))){ //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
                            subCmd = null;
                        } else
                            /* second commands that are followed by text  */
                            if ((i == (lastCommandEnd + 2))
                                    && ((lastLower.startsWith("appi") || lastLower.startsWith("editf") || lastLower.startsWith("colsep") || lastLower.equals("null") || //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
                                            lastLower.startsWith("pau") || lastLower.startsWith("worksheetname") || lastLower.startsWith("numf") || lastLower.startsWith("sqlco")  || lastLower.startsWith("sqlp") //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$ //$NON-NLS-5$
                                            || lastLower.equals("instance") || lastLower.equals("logsource")|| lastLower.startsWith("suf")))) { //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
                                subCmd = null;
                            } else
                                /** next user text that has a couple of keywords before it */
                                if (((i - lastCommandEnd) % 2 == 1) && (tokens[ lastCommandEnd + 1 ].toLowerCase().equals("xquery") && (lastLower.equals("context") || lastLower.equals("baseuri")))) { //$NON-NLS-1$ //$NON-NLS-2$  //$NON-NLS-3$
                                    subCmd = null;
                                }
                    }
                    if ((subCmd != null)||(allTokens==true)) {
                          SQLCommand lastAdded=fillSubCmd(cmd,tokens,lastCommandEnd,i-1);
                          lastCommandEnd = i-1;
                          //if last added was unknown - we can stop parsing
                          if ((lastAdded!=null) && (lastAdded.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SET_UNKNOWN))) {
                             break;
                          }
                    }
                }
                if (lastCommandEnd!=(tokens.length-1)) {//hmm which it wont...
                    fillSubCmd(cmd,tokens, lastCommandEnd, tokens.length-1);
                }
            }
        }
    }

    private void checkIfCompoundSetCommand(SQLCommand stmtProperties, String line) {

    	Boolean parsed = (Boolean) stmtProperties.getProperty(oracle.dbtools.raptor.newscriptrunner.commands.Set.SET_PARSED);
    	if (parsed == null || !parsed ) {
    		if ( line != null) {
    			String[] tokens = null;
    			String theSet="set"; //$NON-NLS-1$
    			if ((line.trim().substring(0,3).equalsIgnoreCase(theSet))) {
    				tokens = getTokens(line);
    				// may need to add more conditions for various SET commands if there are more than one parameter/value.
    				if (tokens.length > 3) {
    					m_isFirstPartOfCompoundSetStmt = true;
    				}
    			}
    		}
    	}
    }

    private SQLCommand fillSubCmd(SQLCompoundCommand compCmd,String[] tokens, int lastEnd, int thisEnd) {

        SQLCommand subCmd = getSetCommand(tokens[ lastEnd + 1 ]);
        if (subCmd==null){//could be as yet unsupported subcommand we still need to parse out and error just this one
            subCmd=(new SQLCommand(G_C_SQLPLUS, G_S_SET_UNKNOWN, G_R_NONE, false,Restricted.Level.UNKNOWN));
            // bug 19659881. Set this for any unsupported command.
            // compCmd.setStmtSubType(StmtSubType.G_S_SET_UNKNOWN);
            // return subCmd; should execute up to here in the compound set and error out on executing this one.
        }
        if ((subCmd != null)||tokens[lastEnd + 1].equalsIgnoreCase("worksheetname")) {  //$NON-NLS-1$
            compCmd.addCommand(subCmd);
            // cut ob set keyword while not prefexed by appixxx colsep editfxxx table identifier null pauxxx context
            String theSet="set"; //$NON-NLS-1$
            if ((compCmd.getOrigSQL().trim().length()>3)&&(compCmd.getOrigSQL().trim().substring(0,3).equalsIgnoreCase(theSet))) {
                theSet=compCmd.getOrigSQL().trim().substring(0,3);
            }
            StringBuffer setStringBuffer = new StringBuffer(theSet); //$NON-NLS-1$
            int j = lastEnd + 1;
            while (true) {
                if ((j > thisEnd) || (j >= tokens.length)) {
                    break;
                }
                if (tokens[ j ] != null) {
                    setStringBuffer.append(" " + tokens[ j ]); //$NON-NLS-1$
                }
                j++;
            }
            //copy start end lines from parent.
            subCmd.setOrigStartLine(compCmd.getOrigStartLine());
            subCmd.setOrigEndLine(compCmd.getOrigEndLine());
            subCmd.setStartLine(compCmd.getStartLine());
            subCmd.setEndLine(compCmd.getEndLine());
            subCmd.setOrigSQL(setStringBuffer.toString());
            subCmd.setModifiedSQL(subCmd.getOrigSQL());
         	if (m_setSubCommandCount > 0 && !m_isFirstPartOfCompoundSetStmt) {
         		subCmd.setIsSetCompoundCmd(true);
         		subCmd.setCompoundSetcmd(null);
        	}
            int i = lastEnd + 1;
            if (thisEnd >=(i + 1)) {
                String propertyNotLowered = tokens[ i + 1 ];
                String property = propertyNotLowered.toLowerCase();
                if (property.equals("on")) //$NON-NLS-1$
                {
                    subCmd.setProperty(SQLCommand.PROP_STATUS_BOOLEAN, Boolean.TRUE);
                } else
                    if (property.equals("off")) //$NON-NLS-1$
                    {
                        subCmd.setProperty(SQLCommand.PROP_STATUS_BOOLEAN, Boolean.FALSE);
                    }
                // do this anyway, the more info the merrier, although if the above set a boolean value , its only for historical sake we keep the "on" and "off". But
                // look at SetServeroutOutput! it still references PROP_STRING for "on" and "off".
                if ((propertyNotLowered != null) && (!(propertyNotLowered.equals("")))) //$NON-NLS-1$
                {
                    subCmd.setProperty(SQLCommand.PROP_STRING, propertyNotLowered);
                } else {
                    subCmd.setProperty(SQLCommand.PROP_STRING, property);
                }
                if (subCmd.getProperty(SQLCommand.PROP_STATUS_BOOLEAN) == null) { // for those which are not on or off, set it too on, like spool c:\filename.txt
                    subCmd.setProperty(SQLCommand.PROP_STATUS_BOOLEAN, Boolean.TRUE);
                }
                if(subCmd.getProperty(SQLCommand.PROP_STATUS_BOOLEAN)==null){//for those which are not on or off, set it too on, like spool c:\filename.txt
                    subCmd.setProperty(SQLCommand.PROP_STATUS_BOOLEAN, new Boolean(true));
                }
            } else {
                if (!(subCmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SET_UNKNOWN))) {
                    //if unknown preferr unknown error - do the rest so error line numbers will be good
                    this.setError(subCmd);
                }
            }
        }
        else {
            compCmd.setStmtSubType(StmtSubType.G_S_SET_UNKNOWN);
        }

        return subCmd;
    }
    /**
     * all tokens not obsolete which may need splitting up for better error checkin
     */
    private boolean checkAllTokens (String token) {
        String[] cutOn =  {"appi","array","auto","blo","cmds","colsep","con","copyc","copytypecheck",   //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$   //$NON-NLS-5$  //$NON-NLS-6$  //$NON-NLS-7$  //$NON-NLS-8$ //$NON-NLS-9$  //$NON-NLS-10$
                "def","describe","echo","editf","emb","errorl","esc","exitc","feed","flagger",   //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$   //$NON-NLS-5$  //$NON-NLS-6$  //$NON-NLS-7$  //$NON-NLS-8$ //$NON-NLS-9$  //$NON-NLS-10$
                "flu","hea","instrance", "lin","lobof","logsource","long","mark","newp","null",   //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$   //$NON-NLS-5$  //$NON-NLS-6$  //$NON-NLS-7$  //$NON-NLS-8$ //$NON-NLS-9$  //$NON-NLS-10$
                "num","pages","prompt","pau","recsep","secureliteral","serverout","shift","show","sqlbl","sqlc","sqln",   //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$   //$NON-NLS-5$  //$NON-NLS-6$  //$NON-NLS-7$  //$NON-NLS-8$ //$NON-NLS-9$  //$NON-NLS-10$  //$NON-NLS-11$
                "sqlpluscompat","sqlpre","sqlp","sqlt","suf","tab","term","ti",   //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$   //$NON-NLS-5$  //$NON-NLS-6$  //$NON-NLS-7$  //$NON-NLS-8$
                "trim","und","ver","wra","xmlopt","xquery"   //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$   //$NON-NLS-5$  //$NON-NLS-6$
        };
        for (String key:cutOn) {
            if (token.startsWith(key)) {
                return true;
            }
        }
        return false;
    }

    private SQLCommand getSetCommand(String token) {
        String tokenLower=token.toLowerCase(Locale.US);
        java.util.Set<String> keys = SQLStatementTypes.setCommandsExact.keySet();
        for (String key: keys) {
            if (tokenLower.equals(key)) {
                return SQLStatementTypes.setCommandsExact.get(key);
            }
        }
        keys = SQLStatementTypes.setCommands.keySet();
        for (String key: keys) {
            if (tokenLower.startsWith(key)) {
                return SQLStatementTypes.setCommands.get(key);
            }
        }
        return null;
    }
//    private boolean isOptionParsingRequired(SQLCommand stmtProperties) {
//        StmtSubType subType = stmtProperties.getStmtSubType();
//        if(stmtProperties.isSqlPlusSetCmd() ||
//           subType == SQLCommand.StmtSubType.G_S_DEFINE ||
//           subType == SQLCommand.StmtSubType.G_S_SPOOL ||
//           subType == SQLCommand.StmtSubType.G_S_SET_VERIFY){
//            return true;
//        }
//        return false;
//    }
//
    enum State {NORM, INQUOTE, INDOUBLEQUOTE};
    public static String[] getTokens(String line) {
        line = line.replaceAll("\r\n|[\r\n\u2028\u2029\u0085]|\t", " "); //$NON-NLS-1$ //$NON-NLS-2$
        line = line.replace("  ", " "); //$NON-NLS-1$ //$NON-NLS-2$
        line = line.trim();
        if(line.endsWith(";")){ //remove the  ";"
        	line = line.substring(0,line.length()-1);
        }
        //basically go down the line
        //getcharacter
        //events are ' '
        //'"' when in " and not ""
        //''' when in ' and not ''
        //states are normal looking for ' and looking for " and end of string
        State currentState = State.NORM;
        ArrayList<String> myTokens = new ArrayList<String>();
        int lastFlush = 0;
        for(int i=0;i<line.length();i++) {
            switch (currentState) {
                case INQUOTE:
                    if (line.charAt(i)=='\'') {
                        if ((i==line.length()-1) || (!(line.charAt(i+1)=='\''))) {
                            currentState=State.NORM;
                            myTokens.add(line.substring(lastFlush,i+1));
                            while ((i+1)!=line.length()&&(line.charAt(i+1)==' ')) {
                                i++;
                            }
                            lastFlush=i+1;
                        } else if ((i!=line.length()-1) && ((line.charAt(i+1)=='\''))) {
                            i++; // do not want the second quote to be caught as a single quote next i
                        }
                    }
                    break;
                case INDOUBLEQUOTE:
                    if (line.charAt(i)=='"') {
                        if ((i==line.length()-1) || (!(line.charAt(i+1)=='"'))) {
                            currentState=State.NORM;
                            myTokens.add(line.substring(lastFlush,i+1));
                            while ((i+1)!=line.length()&&(line.charAt(i+1)==' ')) {
                                i++;
                            }
                            lastFlush=i+1;
                        } else if ((i!=line.length()-1) && ((line.charAt(i+1)=='"'))) {
                            i++; // do not want the second quote to be caught as a single quote next i
                        }
                    }
                    break;
                case NORM:
                    if (line.charAt(i)=='\'') {
                        if ((i==0)||(line.charAt(i-1)==' ')) {
                            currentState=State.INQUOTE;
                        }
                    } else if (line.charAt(i)=='"') {
                        if ((i==0)||(line.charAt(i-1)==' ')) {
                            currentState=State.INDOUBLEQUOTE;
                        }
                    } else if (line.charAt(i)==' ') {
                        myTokens.add(line.substring(lastFlush,i));
                        while ((i+1)!=line.length()&&(line.charAt((i+1))==' ')) {
                            i++;
                        }
                        lastFlush=i+1;
                    }
                    break;
            }

        }
        if (lastFlush != line.length()) {
            myTokens.add(line.substring(lastFlush));
        }
        //String[] localTokens = line.split(" ");
        return myTokens.toArray(new String[myTokens.size()]); //$NON-NLS-1$
    }



    private SQLCommand createUnknownStatementProperties() {
        return new SQLCommand(SQLCommand.StmtType.G_C_SQLPLUS, SQLCommand.StmtSubType.G_S_UNKNOWN, SQLCommand.StmtResultType.G_R_UNKNOWN, true, Restricted.Level.UNKNOWN);
    }

    private SQLCommand getPropertiesWhenNoSpaceRequired(String line) {
        SQLCommand prop = null;
        if (line == null || line.trim().length() == 0) {
            return null;
        }
        String input = null;
        // the noSpaceRequiredTokens have either 1 or 2 characters only.
        // check for the longest match first (2 characters) then the shortest match (1 character).
        line = line.trim();
        // first check if the trimmed line has 2 characters
        if (line.length() >= 2) {
            input= line.trim().substring(0, 2);
            if (SQLStatementTypes.noSpaceRequiredTokens.containsKey(input)) {
                prop = SQLStatementTypes.noSpaceRequiredTokens.get(input);
            } else {

            input = line.trim().substring(0, 1); // check to see if the first character is recognized
            if (SQLStatementTypes.noSpaceRequiredTokens.containsKey(input)) {
                prop = SQLStatementTypes.noSpaceRequiredTokens.get(input);
            }
            }
        } else { // we know its greater than 0 but less than 2 chars in length, so it must be 1 character in length
            input = line;
            if (SQLStatementTypes.noSpaceRequiredTokens.containsKey(input)) {
                prop = SQLStatementTypes.noSpaceRequiredTokens.get(input);
            }
        }
        return prop;
        // note the old code would check if there where 5 items in the nested array.
        // if so it assigned the resultType to the 4th value, if not, no value was assigned to the resultType.
        // I dont know why this was. Maybe the resultType in the SQLStatementTypes.stmts was not good enough and has to be determined else where.
    }

    private SQLCommand getPropertiesWhenTokensKnown(String line) {
        String[] tokens = line.trim().split("[ \n\r\t*=\\-]");
        // remove ; if added on
        if (tokens[ 0 ] != null) {
        	if( tokens[ 0 ].length() > 0 &&
        		tokens[ 0 ].charAt(tokens[ 0 ].length() - 1) == ';') {
            tokens[ 0 ] = tokens[ 0 ].replaceAll(";", ""); // this removes ";" from the first token //$NON-NLS-1$ //$NON-NLS-2$
        	}
        	return SQLStatementTypes.spaceRequiredTokens.get(tokens[ 0 ].toLowerCase(java.util.Locale.ENGLISH));
        } else {
        	//Tokens are null, so lets give nothing back and have it fall through.
        	return null;
        }
    }

    private boolean isWrapped(String currentStatement, SQLCommand stmtProperties) {
        Boolean declaredWrapped=(Boolean)stmtProperties.getProperty(ISQLCommand.PROP_IS_WRAPPED);
        if ((declaredWrapped!=null)&&(declaredWrapped.equals(Boolean.TRUE))) {
            return declaredWrapped;
        }
        String lowercaseStmt = currentStatement.toLowerCase();
        if (lowercaseStmt.indexOf("wrapped") == -1) { //$NON-NLS-1$
            return false; // dont bother looking into this, it doesnt even have the wrapped word
        }
        boolean result = false;
        String commentLessLowerCase = checkInLineComments(lowercaseStmt);
        int wrappedStart = commentLessLowerCase.indexOf("wrapped"); //$NON-NLS-1$
        // if it exists, see if it is its own keyword token and not another word like "wrapped_col"
        // the char before and the char after should be a whitespace or new line
        if (wrappedStart > 2 && commentLessLowerCase.length() > wrappedStart + 8) {
            // check the char before "wrapped"
            char charBefore = commentLessLowerCase.charAt(wrappedStart - 1);
            char charAfter = commentLessLowerCase.charAt(wrappedStart + 7);
            if (Character.isWhitespace(charBefore) && Character.isWhitespace(charAfter)) {
                result = true; // "wrapped" is a keyword so cannot be used as an identifier
                stmtProperties.setProperty(ISQLCommand.PROP_IS_WRAPPED, true);
                //never have to calculate it again for this command;
            }
        }
        return result;
    }



    private StringBuffer findEndOfSQLAndPLSQL(String line, BufferedReaderStack reader, SQLCommand stmtProperties,boolean semiColonTerminatesIn) throws IOException {
        createRefine=stmtProperties;
        createRefineSemiColonTerminates=semiColonTerminatesIn;
        StringBuffer currentStatement = new StringBuffer();
        boolean searching = true;
        int quotes;
        boolean enquote = false;
        int embeddedSemicolon = 0;
        String stmtTerminator = null;
        while (searching) {
            int initialSemicolon = -1;
            // append the line to the statement
            if (line == null) {
                quotes = 0;
            } else {
                // get the number of "'" quotes in the line
            	boolean returnOnSemiColon = true;
            	if(stmtProperties.getStmtType()==StmtType.G_C_PLSQL || stmtProperties.getProperty(TERMINATOR)!=null && stmtProperties.getProperty(TERMINATOR)==FORWARDSLASH){
            		returnOnSemiColon = false;
            	}
            	this.createRefineReturnOnSemicolon=returnOnSemiColon;
                if ((createRefine.getProperty(SQLCommand.CREATEREFINE)!=null&&
                        createRefine.getProperty(SQLCommand.CREATEREFINE).equals(Boolean.TRUE))) {
                    createRefineLineSoFar=new StringBuffer(currentStatement);
            	    if (line!=null) {
            	        createRefineLineSoFar.append("\n").append(line);
            	    }
                }
                int[] retval = numberOfQuotes(line/*, returnOnSemiColon*/, true, true);
                quotes = retval[ 0 ];
                embeddedSemicolon = retval[ 1 ];
                initialSemicolon = retval[ 2 ];
            }
            // if the count is odd the line is quoting the \n\r
            // if enquote and odd number of quotes close the enquote
            if (quotes % 2 != 0) {
                enquote = !enquote;
            }
            //doneill: there are some tricky conditions here which would require a proper complete parser for ex:
            //insert into tafibf01_table3 values (bfilename('tafibf01_dir2, 'tafibf01_file2.txt'));
            //the above statement is missing an enclosing single quote. We could take this to mean  the second string begins  as ')); with it finishing on the next line.
            //SQLplus doesn't interpret it that way. It sees the semi colon at the very end of the line and thinks. Ah somethings wrong. This semi colon is not inside a string.
            //New new thing (or rather be like sqlplus in classic, be like old sqlcl in not classic, eat this if not classic and not in quotes
            //&&(classic=sqlplus(; end of sql) || (not classic(old/new sqlcl)(; end of sql only when not embeded) && (initia; =-1 or < line last index of ;line.lastIndexOf(";") == initialsemicolon)
            Boolean classic=((this._scriptRunnerContext!=null &&
                    this._scriptRunnerContext.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null &&
                    Boolean.parseBoolean(this._scriptRunnerContext.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())));
            if(createRefineReturnOnSemicolon && line.trim().endsWith(";") &&
                    (classic || ((!classic)
                            &&(!enquote)&&(/* this is a real not in quote etc semicolon */initialSemicolon!=-1&&line.lastIndexOf(";")==initialSemicolon)))){
            	enquote = false;
            	embeddedSemicolon = 0;
            }

            //if we are on a "\n/" assume we will have would have hit wrapped keyword,
            //check if wrapped and if wrapped ignore enquote
            if ((line!=null)&&(createRefineSemiColonTerminates==false)&&(enquote==true)&&
                    ((line.trim().endsWith("\n/") || line.trim().equals("/")))){ //$NON-NLS-1$ //$NON-NLS-2$
                //might be wrapped with odd quotes iswrapped is fairly expensive
                if(isWrapped(currentStatement.toString(), stmtProperties)) {
                    quotes=0;
                    enquote=false;
                }
            }
            if (line != null && !enquote) {
                if (initialSemicolon != -1 && createRefineSemiColonTerminates) {
                    if (line.length() != initialSemicolon + 1) {
                        setLeftOver( line.substring(initialSemicolon + 1));
                        if(getLeftOver().trim().equals("")){ //if there is really nothing left over then set it too null
                        	setLeftOver(null);
                        }
                    } else {
                        setLeftOver(null);
                    }
                    line = line.substring(0, initialSemicolon);
                    stmtTerminator = ";";
                    searching = false;
                } else if ((line.trim().endsWith(";")) && (embeddedSemicolon == 0) && createRefineSemiColonTerminates && (classic || ((!classic)  //$NON-NLS-1$
                        &&(!enquote)&&(/* this is a real not in quote etc semicolon */initialSemicolon!=-1&&line.lastIndexOf(";")==initialSemicolon)))) { //$NON-NLS-1$
                    searching = false;
                    line = line.substring(0, line.lastIndexOf(";")); //$NON-NLS-1$
                    stmtTerminator = ";";
                } else if (hasForwardSlashOnLastLine(line)) { //$NON-NLS-1$ //$NON-NLS-2$
                    searching = false;
                    line = line.substring(0, line.lastIndexOf("/")); //$NON-NLS-1$
                    stmtTerminator = "/";
                    if(stmtProperties.getProperty(TERMINATOR)!=null&&stmtProperties.getProperty(TERMINATOR)==FORWARDSLASH){
                    	//remove the last semi colon if one it there, as this is a mixture of sql/plsql
                    	if(currentStatement.toString().trim().endsWith(";")){
                    		int i = currentStatement.length()-1;
                    	   while(currentStatement.charAt(i) != ';'){
                    	  	 currentStatement.deleteCharAt(i);//remove any rhs white space
                    	  	 i--;
                    		 }
                    		 currentStatement.deleteCharAt(i);//remove the ;
                    		stmtTerminator = ";\n/";
                    	}
                    }

                } else if (line.trim().equals(".") && (!inMultilineComment(currentStatement) || classic)) { //ignore dot inside a comment when not classic mode
                    searching = false;
                    line = line.substring(0, line.lastIndexOf(".")); //$NON-NLS-1$
                    stmtTerminator=".";
                  	stmtProperties.setExecutable(false);
                  	stmtProperties.setRunnable(false);
                }
                if(!line.trim().equals("")){
                	currentStatement.append(line); //$NON-NLS-1$
                }
                if (searching) {
                    line = reader.readline(true, false, null, true, stateInitial());
                    if(line != null){ //this is the next line so append a line seperator
                    	currentStatement.append("\n");
                    }
                    m_readerLineCnt++;
                }
            } else {
            	  if(!line.trim().equals("")){
                  currentStatement.append(line); //$NON-NLS-1$
            	  }
                line = reader.readLine();
                if(line != null){ //this is the next line so append a line seperator
                	currentStatement.append("\n");
                }
                m_readerLineCnt++;
            }

            //doneill: This identifies statements which are incomplete, but maybe completed either on the command line or a parent script.
            if (searching &&  (( line == null           && (stmtProperties.getStmtType()!=StmtType.G_C_SQL || stmtProperties.getStmtType()==StmtType.G_C_SQL && !isSQLBlankLines()))
            		                 ||
            		                 (line!=null && line.trim().equals("")) &&  stmtProperties.getStmtType()==StmtType.G_C_SQL && !isSQLBlankLines())){
              searching = false;

              if (isInFile()&&(!isEndOnEOF())) {
              	//this is not a complete statement. SQLcl can handle this... so set the flag
              	stmtProperties.setIsComplete(false);
              	stmtProperties.setExecutable(false);
              	stmtProperties.setRunnable(false);
              }
            } else if (searching && line ==null){
            	 searching = false;
            }
        } // end while loop
        stmtProperties.setOrigStartLine(m_readerLineStartCnt);
        stmtProperties.setOrigEndLine(m_readerLineCnt );
        stmtProperties.setStatementTerminator(stmtTerminator);
        return currentStatement;
    }

     /**
       * Returns true if the currentStatement ends within a multiline comment
       * ex: SELECT1,2
       *  /* this is a comment
       *
       *
       * The above text would return true. Note that the mulitline comment is not complete.
       * This method is not perfect. calculating the string literals with escape characters is beyond the scope
       * Also is a multiline comment is within a single line comment then it is still recognized as a multiline comment
       * @param currentStatement The text to check if it ends in the middle of a multiline comment
       * @return true or false
       */
  	private boolean inMultilineComment(StringBuffer currentStatement) {
  		int startOfMultilineComment = currentStatement.lastIndexOf("/*");
  		int endOfMultilineComment = currentStatement.lastIndexOf("*/");
  		// check that a multiline comment has "opened" but not yet "closed"
  		if (startOfMultilineComment > endOfMultilineComment) {// quick check to see if its worth doing more investigation
  			// we maybe inside a multiline comment, lets remove the single quoted literals and start again
  			String processLine = currentStatement.toString().replaceAll("'(.)*'", "");// remove most (not escaped) single quoted literals
  			startOfMultilineComment = processLine.lastIndexOf("/*");
  			endOfMultilineComment = processLine.lastIndexOf("*/");
  			//ex:                                          origSQL => processLine
  			// ex: SELECT 'A /*this is not in a multiline comment' => SELECT
  			// ex: SELECT 'A' /*this is in a multiline comment 'A' => SELECT /*this is in a multilinecomment
  			// ex: SELECT 'A' /*this is in a multiline comment => SELECT /*this is in a multilinecomment
  			// ex: SELECT 'A' /*this is in a multiline comment 'A => SELECT /*this is in a multilinecomment
  			if (startOfMultilineComment > endOfMultilineComment) {
  					return true;
  			}
  		}
  		return false;
  	}

	private boolean hasForwardSlashOnLastLine(String line) {
		try {
			if (line == null) {
				return false;
			}
			//if the last line ends with a forward slash on a new line (no spaces before the new line)
			if (line.trim().endsWith(System.getProperty("line.separator")+"/") || line.trim().equals("/")) {
				return true;
			}
			//get the actual last line and trim and check if it has only a forward slash. this finds forward slashes with white space before them
			int lastLineIndex = line.lastIndexOf("\n");
			if (lastLineIndex != -1) {
				String lastLine = line.substring(lastLineIndex + 1);
				if (lastLine.trim().equals("/")) {
					return true;
				}
			}
		} catch (Throwable t) {
			//do not throw an error
			return false;
		}
		return false;
	}

		private boolean isBareConnect(SQLCommand stmtProperties, String soFar) {
    	//connect command where trim does not contain whitespace  - comments be damned.
    	if (stmtProperties.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_CONNECT)) {
    		if ((soFar.trim().indexOf(" ")==-1)&&(soFar.indexOf("\n")==-1)) {//TODO might be tab unhandled  //$NON-NLS-1$ //$NON-NLS-2$
    			//if is command line and is not top level need to prompt or at least echo with the right string
    			//1/do it right?? vs 2/fake a prompt either in parse/sqlcl/runconnect vs 3/ignore prompt for now will work without (prompt for interactive would be nice though)- work in file priority
    			return true;
    		}
    	}
    	return false;
    }

    private StringBuffer findEndOfSQLPLUS(String line, BufferedReaderStack reader, SQLCommand stmtProperties) throws IOException {
        StringBuffer currentStatement = new StringBuffer();
        boolean searching = true;
        //
        // find sqlplus statement
        //
        while (searching) {
            // append the line to the statement
            // check for ending ;
            boolean isBareConnect=isBareConnect(stmtProperties,currentStatement+line);
        	if (isBareConnect||((line.trim().endsWith("-")&&(!(line.trim().startsWith("--")))))) { //$NON-NLS-1$ //$NON-NLS-2$
                // continuation
                searching = true;
                currentStatement.append(line + "\n"); //$NON-NLS-1$
                line = reader.readline(true, false, null, true, stateInitial());
                m_readerLineCnt++;
                if (line == null) {
                    searching = false;
                }
            } else {
                // found the end
                searching = false;
                currentStatement.append(line + "\n"); //$NON-NLS-1$
            }
        } // end while
        stmtProperties.setOrigStartLine(m_readerLineStartCnt);
        stmtProperties.setOrigEndLine(m_readerLineCnt );
        //remove last semicolon (accepted but not required by sqlplus
        if (currentStatement.toString().trim().endsWith(";")) {
            if (currentStatement.length()>1) {
                String s = currentStatement.substring(0, currentStatement.lastIndexOf(";")); //$NON-NLS-1$
                currentStatement = new StringBuffer(s);
                stmtProperties.setStatementTerminator(";");
            }
        }
        return currentStatement;
    }

    private StringBuffer findEndOfMultiLineComment(String line, BufferedReaderStack reader, SQLCommand stmtProperties) throws IOException {
        StringBuffer currentStatement = new StringBuffer();
        boolean searching = true;
        //
        // find multiline comment
        //
        while (searching) {
            // append the line to the statement
            // check for ending ;
            int nearEndOfComment = line.indexOf("*/"); //$NON-NLS-1$
            if (nearEndOfComment >= 0) {
                // continuation
                searching = false;
                if (nearEndOfComment + 2 == line.length()) {
                    setLeftOver (null); //$NON-NLS-1$ // setting this to "" interfers with emptyline counter and when prompts are displayed. See testcase :/common/sqlcl/test/regression/testcases/gold_comparison/gold/sqlplus/feedback/feedback003.sql
                } else {
                    setLeftOver ( line.substring(nearEndOfComment + 2));
                    if(getLeftOver().trim().equals("")){
                    	setLeftOver( null); // trailing spaces on a multiline comment are not treated as extra information
                    }
                    line = line.substring(0, nearEndOfComment + 2);
                }
                currentStatement.append(line + "\n"); //$NON-NLS-1$
            } else {
                currentStatement.append(line + "\n"); //$NON-NLS-1$
                line = reader.readLine(false);
                m_readerLineCnt++;
                if (line == null) {
                    searching = false;
                    currentStatement.append("*/"); //$NON-NLS-1$
                }
            }
        } // end while
        stmtProperties.setOrigStartLine(m_readerLineStartCnt);
        stmtProperties.setOrigEndLine(m_readerLineCnt);
        return currentStatement;
    }

    private StringBuffer findEndOfOldComment(String line, BufferedReaderStack reader, SQLCommand stmtProperties) throws IOException {
        StringBuffer currentStatement = new StringBuffer();
        boolean searching = true;
        //
        // find multiline comment
        //
        while (searching) {
            // append the line to the statement
            // check for ending ;
            if(!line.trim().equals("#")){
                currentStatement.append(line + "\n"); //$NON-NLS-1$
                // Bug 22760779 we don't parse each line of the comment
                // so setting it to false.
                line = reader.readLine(false);
                m_readerLineCnt++;
                if (line == null) {
                    searching = false;
                    currentStatement.append("\n#\n"); //$NON-NLS-1$
                }
            } else {
            	currentStatement.append(line + "\n"); //$NON-NLS-1$
            	searching = false;
            }
        } // end while
        stmtProperties.setOrigStartLine(m_readerLineStartCnt);
        stmtProperties.setOrigEndLine(m_readerLineCnt);
        return currentStatement;
    }

    private SQLCommand identifyEndOfStatement(String line, BufferedReaderStack reader, SQLCommand stmtProperties) {
        StringBuffer currentStatement = new StringBuffer();
        try {
            switch (stmtProperties.getStmtType()) {
                case G_C_SQL:
                	  //12c introduces a new /*+WITH_PLSQ*/ "hint". which can be used in SQL DML, but the DML now terminates with a forward slash.
                		boolean semiColonTerminator = true;
                		if(stmtProperties.getProperty(TERMINATOR)!=null && stmtProperties.getProperty(TERMINATOR) == FORWARDSLASH){
                			semiColonTerminator = false;
                		}
                    currentStatement = findEndOfSQLAndPLSQL(line, reader, stmtProperties,semiColonTerminator);
                    break;
                case G_C_SCRIPT:
                case G_C_PLSQL:
                    currentStatement = findEndOfSQLAndPLSQL(line, reader, stmtProperties,false);
                    break;
                case G_C_SQLPLUS:
                    currentStatement = findEndOfSQLPLUS(line, reader, stmtProperties);
                    break;
                case G_C_USERDEFINED:
                case G_C_COMMENT:
                    currentStatement = findEndOfLine(line,stmtProperties);

                    break;
                case G_C_UNKNOWN:
                    s_logger.severe(ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNSUPPORTED_COMMAND_ARG, line));
                case G_C_MULTILINECOMMENT:
                    currentStatement = findEndOfMultiLineComment(line, reader, stmtProperties);
                    break;
                case G_C_JDBC:
                    currentStatement = findAllLines(line, reader, stmtProperties);
                    break;
                case G_C_OLDCOMMENT:
                	currentStatement = findEndOfOldComment(line, reader, stmtProperties);
                    break;
                case G_C_EMPTYLINE:
                default:
                    break;
            }
        } catch (IOException e) {
            s_logger.severe(e.getLocalizedMessage());
        }
        //totierne a wrapped statement was causing an alias state to be carried over
        //between statements - may have to eat a wrapped statement differently to a normal
        //statement as it may have a loose ' or ". It may also contains escape characters..
        initialiseState();
        // add statement to the array
        if (currentStatement.length() > 0) {
            // make sure to remove ";" and "/" from the end of statments.
					//trim right stmt text ? why?
           //while (currentStatement.charAt(currentStatement.length() - 1) == '\n' || currentStatement.charAt(currentStatement.length() - 1) == '\r') {
           //     currentStatement.deleteCharAt(currentStatement.length() - 1);
           //}
            String currstmt = currentStatement.toString();
            // remove single line comments (replace them with \n full statement printed to work out line numbers.)
            // infrequently used concatenation characters cause minor line number issues.
            // do this to queries and DML, leave in place for DDL
            if (stmtProperties.getStmtResultType() != null && (stmtProperties.getStmtResultType() == SQLCommand.StmtResultType.G_R_QUERY || stmtProperties.getStmtResultType() == SQLCommand.StmtResultType.G_R_DML) && !isWrapped(currentStatement.toString(), stmtProperties)) {
                currstmt = checkInLineComments(currentStatement.toString());// remove single line comments
                stmtProperties.setProperty(stmtProperties.STRIPPED_CONTINUATION, Boolean.TRUE);
            }
            if(stmtProperties.getStmtResultType() != null && stmtProperties.getStmtSubType()== SQLCommand.StmtSubType.G_S_WITH && currstmt.trim().endsWith(";")){
            	currstmt = currstmt.trim().substring(0,currstmt.trim().length()-1);//trim semicolon - and any space
            }
            stmtProperties.setOrigSQL(currentStatement.toString());
            stmtProperties.setModifiedSQL(currstmt);
        } else {
            //the rest of the code really does not like null in cmd.getSqll
            stmtProperties.setOrigSQL(currentStatement.toString());
            stmtProperties.setModifiedSQL(currentStatement.toString());
        }
        if (stmtProperties.getStmtSubType().equals(StmtSubType.G_S_SET)&&(stmtProperties instanceof SQLCompoundCommand)) {
            SQLCompoundCommand localCompound = (SQLCompoundCommand) stmtProperties;

            String stmt_terminator = null;
            if (stmtProperties.getStatementTerminator() != null ) {
            	stmt_terminator = stmtProperties.getStatementTerminator();
            }

            if ( m_setSubCommandCount == 0)
				checkIfCompoundSetCommand(localCompound, line);

            lateEvaluateInitCompoundSetCommand(localCompound, getSetLeftOver()!=null);
            //command is
            //if (stmtProperties.getStmtSubType().equals(StmtSubType.G_S_SET) && (localCompound.getCommands().size()==1)
            //        && localCompound.getCommands().get(0).getStmtClass().equals(StmtType.G_C_SQL)){
            //    stmtProperties=localCompound.getCommands().get(1);
            //} else
            if (!(stmtProperties.getStmtSubType().equals(StmtSubType.G_S_SET_UNKNOWN))){
                //eat first set regurgitate the rest.
                if (getSetLeftOver()==null) {
                    if (getLeftOver()==null) {
                        setSetLeftOver("");//on the first set, set the ;leftover //$NON-NLS-1$
                    } else {
                        setSetLeftOver(getLeftOver());
                    }
                }
                List<SQLCommand> theCommands=localCompound.getCommands();

                if (m_isFirstPartOfCompoundSetStmt) {
            		m_setSubCommandCount = theCommands.size() ;
            	}
                String unusedTokens = "set "; //$NON-NLS-1$
                boolean setMade = false;
                for (SQLCommand aCommand : theCommands) {
                    if (!(setMade)) {
                        stmtProperties=aCommand;
                        if (stmt_terminator != null ) {
                        	stmtProperties.setStatementTerminator(stmt_terminator);
                        }
                        setMade=true;
                    } else {
                        unusedTokens+= aCommand.getSql().trim().substring(3).trim()+" "; //$NON-NLS-1$
                    }
                }
                if (theCommands.size()==1) {
                	if(!getSetLeftOver().trim().equals("")){
                		setLeftOver( getSetLeftOver());//on the last set, use the leftover // setting this to "" interfers with emptyline counter and when prompts are displayed. See testcase :/common/sqlcl/test/regression/testcases/gold_comparison/gold/sqlplus/feedback/feedback003.sql
                	}
                   setSetLeftOver(null);
                } else {
                   setLeftOver  (unusedTokens);
                }
            }

            resolveCompoundSetCommands(stmtProperties, line) ;

        } else {//could be a rogue set ending in set role or set worksheetname if in multiset clear the remainder
            if(getSetLeftOver()!=null) {//we are in a multi set
                setLeftOver(getSetLeftOver());//on the last set or rogue non standard set, use the leftover
                setSetLeftOver(null);
            }
        }

        return stmtProperties;
    }

    /**
     * This can be used to identify JDBC style statements which take up all the text
     */
    private StringBuffer findAllLines(String line, BufferedReaderStack reader, SQLCommand stmtProperties) throws IOException {
    	 StringBuffer currentStatement = new StringBuffer();
         while (line!=null) {
        	 currentStatement.append(line+"\n");
             line = reader.readLine(false);
             m_readerLineCnt++;
         }
         stmtProperties.setOrigStartLine(m_readerLineStartCnt);
         stmtProperties.setOrigEndLine(m_readerLineCnt);
         return currentStatement;
	}

	private StringBuffer findEndOfLine(String line, SQLCommand stmtProperties) {
        StringBuffer currentStatement = new StringBuffer();
        currentStatement.append(line + "\n"); //$NON-NLS-1$
        stmtProperties.setOrigStartLine(m_readerLineStartCnt);
        stmtProperties.setOrigEndLine(m_readerLineCnt );

        return currentStatement;
    }

    /**
     * @param currstmt
     * @return
     */
    private String checkInLineComments(String currstmt) {
        // put continuation strings together
        currstmt = ScriptUtils.checkforContinuationChars(currstmt);
        //do not strip single line characters
        /* if (currstmt.contains("--")) { //$NON-NLS-1$
            int position = 0;
            int nextPosition = currstmt.indexOf("--", position); //$NON-NLS-1$
            while (true) {
                if (nextPosition == -1) {
                    break;
                }
                if (amIInitial(currstmt.substring(0, nextPosition), true)) {
                    int nextPos = currstmt.indexOf("\n", nextPosition); //$NON-NLS-1$
                    if ((nextPos == -1) || (nextPos == currstmt.length())) {
                        currstmt = currstmt.substring(0, nextPosition);
                        nextPosition = -1;
                    } else {
                        currstmt = currstmt.substring(0, nextPosition) + System.getProperty("line.separator") + currstmt.substring(nextPos + 1); // or //$NON-NLS-1$
                        // nothing
                        nextPosition = currstmt.indexOf("--", nextPosition); //$NON-NLS-1$
                    }
                } else {
                    nextPosition = nextPosition + 1;
                    nextPosition = currstmt.indexOf("--", nextPosition); //$NON-NLS-1$
                    // since -- is matched and not in initial -<here>- will nor be at end and not in a state transition.
                }
            }
        } */
        return currstmt;
    }





    /**
     * returns the top of the statement. the top of the statement is defined as till ";" or "/" or "begin"
     *
     * @return
     */
    private String getTop(String inString) {
        String top = null;
        _numOfEmptyLineAbove = 0;
        try {
            if (inString == null) {
                top = m_reader.readTopLine();
                  m_readerLineCnt++;
            } else {
                top = inString;
            }
            if (top == null) {
                return null;
            }
            // eat up the top if its just white space
            while (isWhitespaceLTrim(top).equals("")) { //$NON-NLS-1$
            	 _numOfEmptyLineAbove = _numOfEmptyLineAbove + 1;
                top = m_reader.readTopLine();
                m_readerLineCnt++;
                if (top == null) {
                    return null;
                }
            }
            if (top != null) {
                top = isWhitespaceLTrim(top);
            }
            m_readerLineStartCnt = m_readerLineCnt;
            // whitespace has been eaten
            // only look for more line for create statements
            String topTrimLowerCase=top.trim().toLowerCase();
            if (topTrimLowerCase.startsWith("create") || topTrimLowerCase.startsWith("drop") || topTrimLowerCase.startsWith("alter") || topTrimLowerCase.startsWith("with")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$  //$NON-NLS-4$
                String currLine = top; //$NON-NLS-1$
                String currLineLowerCase = null;
                while (currLine != null && currLine.indexOf(";") == -1 && currLine.indexOf("/") == -1 && (currLineLowerCase=currLine.toLowerCase()).indexOf("begin") == -1 && currLineLowerCase.indexOf("wrapped")==-1 ) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$  //$NON-NLS-4$
                    currLine=m_reader.readline(true, true, top, false, false);
                    if (currLine != null) {
                        top = top + "\n" + currLine; //$NON-NLS-1$
                    }
                    m_readerLineCnt++;
                }
            }
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
        }
        return top;
    }

    private String isWhitespaceLTrim(String top) {
        if ((top==null)||(top.equals(""))) { //$NON-NLS-1$
            return top;
        }
        int firstNonWhitespace=-1;
        int lastNewLine=-1;
        for(int i=0; i<top.length(); i++) {
            if (!(Character.isWhitespace(top.charAt(i)))) {
                firstNonWhitespace=i;
                break;
            } else if (top.charAt(i)=='\n') { //We want to remove blank lines above, but not leading spaces on a line
            	lastNewLine=i;
            }
        }
        if (firstNonWhitespace==-1) {
            top=""; //$NON-NLS-1$
        } else if (firstNonWhitespace!=0) {
        	if( 0<(lastNewLine+1)&& (lastNewLine+1)<=firstNonWhitespace)
        	  top = top.substring(lastNewLine+1);
        	else
        		if (lastNewLine==-1) {
              //top = top.substring(firstNonWhitespace);
        		}
        }
        return top;
    }
    public String getRawSQL() {
        return m_rawSQL;
    }

    public void setRawSQL(String rawSQL) {
        this.m_rawSQL = rawSQL;
    }

    public ISQLCommand[] getSqlStatements() {
        return m_sqlArray.toArray(new SQLCommand[ m_sqlArray.size() ]);
    }






    boolean isQuoteStringValid(String quoteString) {
        boolean retVal = true;
        if (quoteString.equals("\t") || quoteString.equals("\r") || quoteString.equals("\n") || quoteString.equals(" ")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            retVal = false;
        }
        return retVal;
    }



    private boolean isEndOnEOF() {
        Boolean endOnEOF=Boolean.FALSE;
        if (_scriptRunnerContext!=null) {
           endOnEOF=(Boolean)_scriptRunnerContext.getProperty(ScriptRunnerContext.ALIAS_END_ON_EOF);
        }
        if (endOnEOF==null) {
           endOnEOF=Boolean.FALSE;
        }
        return endOnEOF;
    }
    private boolean isInFile(){
    	if(_scriptRunnerContext == null){
    		return false;
    	} else {
    		 if(_scriptRunnerContext.getProperty(ScriptRunnerContext.APPINFOARRAYLIST)!=null &&
        	  ((ArrayList<String>)_scriptRunnerContext.getProperty(ScriptRunnerContext.APPINFOARRAYLIST)).size() >0 ){
        		return true;
        	} else {
        		return false;
        	}
    	}
    }

    private boolean isSQLBlankLines(){
    	if(_scriptRunnerContext == null){
    		return true; // the SQL Worksheet will allow blank lines
    	} else {
				String sqlbl = (String) _scriptRunnerContext.getProperty("SQLBLANKLINES"); //$NON-NLS-1$
				//Ok, What is SQLbl set to.
				boolean on = false;
				if (sqlbl==null) {
					on = false;
				} else if (sqlbl.equals("on")) { //$NON-NLS-1$
					on = true;
				} else {
					on = false;
				}
				return on;
    	}
    }

    public void setScriptRunnerContext(ScriptRunnerContext runCtx){
      _scriptRunnerContext = runCtx;
    }
}
