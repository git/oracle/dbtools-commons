/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.app.Literals2Binds;
import oracle.dbtools.logging.Timer;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.plsql.ParsedSql;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.Substitution.SubstitutionException;
import oracle.dbtools.raptor.newscriptrunner.commands.SetEcho;
import oracle.dbtools.raptor.query.Bind;
import oracle.jdbc.OracleConnection;

public class ScriptRunner {
    private static final Logger LOGGER = Logger.getLogger(ScriptRunner.class.getName());
    //private final String ENCODING = "UTF-8"; //$NON-NLS-1$
    private Connection m_conn;
    private ScriptRunnerContext m_scriptRunnerContext = null;
    private BufferedOutputStream m_out = null;
    private boolean m_printComments = false;
    private ScriptStatusDisplay m_status;
    /* directory root value for @@ */
    private String m_directory = null;
    public static final String FORCE_PRINT = "force_print"; // without the new lines //$NON-NLS-1$
    public static final byte[] FORCE_PRINT_BYTES = ScriptRunner.getFORCE_PRINT_BYTES();
    SQLCommandRunner m_commandRunner = null;
    
    private static byte[] getFORCE_PRINT_BYTES() {
        try {
            return " force_print\n".getBytes("UTF-8");//rely on GBK win1252 shift jis superset of ascii //with the new lines //$NON-NLS-1$  //$NON-NLS-2$
        } catch (UnsupportedEncodingException e) {
            LOGGER.info(e.getMessage());
        }
        //will not happen
        return null;
    }
    public static String[] nextToChar(String in, char sep) {
        int position = in.indexOf(sep);
        if (position == -1) {
            usage();
        }
        String theRest;
        if (position == in.length() - 1) {
            theRest = ""; //$NON-NLS-1$
        } else {
            theRest = in.substring(position + 1);
        }
        String[] stringArray = { in.substring(0, position), theRest };
        return stringArray;
    }
    
    public static void usage() {
        LOGGER.info("Usage: java ooracle.dbtools.raptor.scriptrunner.ScriptRunner username/password@host:port:sid @file1 @file2 @file3 ..."); //$NON-NLS-1$
        System.exit(1);
    }
    
    public ScriptRunner(Connection conn, BufferedOutputStream out, ScriptRunnerContext context) {
        setConn(conn);
        m_out = out;
        m_scriptRunnerContext = context;
    }
    
    public Connection getConn() {
        return m_conn;
    }
    
    public BufferedOutputStream getOut() {
        return m_out;
    }
    
    public ScriptRunnerContext getScriptRunnerContext() {
        return m_scriptRunnerContext;
    }
    
    public ScriptStatusDisplay getStatusDisplay() {
        return m_status;
    }
    
    public String getStatusLine() {
        return m_status != null ? m_status.getText() : null;
    }
    
    public boolean isPrintComments() {
        return m_printComments;
    }
    
    protected void report(String s) {
        if (m_out != null) {
            try {
                m_out.write(s.getBytes(m_scriptRunnerContext.getEncoding()));
                m_out.write('\n');
            } catch (IOException e) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            }
        }
    }
    
    public void run(ISQLCommand cmd) throws IOException {	
    	ScriptRunnerContext context = getScriptRunnerContext();
    	//Check the restricted level to see if this command can be run
    	if(context.getRestrictedLevel().isRestricted(cmd.getRestrictedLevel())){
    	  if(!cmd.getRestrictedLevel().ifRestrictedContinue()){
    	    context.setExited(true);//exit the script if this command specifies that the script should not continue if restricted
    	  } 
    	  report(createReportMsg(context,cmd));
    	  return;
    	} else {
    	boolean isOracleConnection = ((getConn()!=null)&&(getConn() instanceof OracleConnection));
	
    	if (isOracleConnection) {
		ScriptRunnerContext.set_storedContext(null);
		ScriptRunnerContext.set_storedContext(getScriptRunnerContext());
	}
	
    	boolean isTimesTenConnection = false;
    	try {
    		isTimesTenConnection = (getConn()!=null)&&(getConn().getMetaData().getDatabaseProductName().indexOf("TimesTen")!=-1); 
    	} catch (Exception e) {
    	}
    	boolean isHttp=(getConn()!=null&&
    			getScriptRunnerContext()!=null&&
    			ScriptUtils.isHttpCon(getConn(), getScriptRunnerContext()));
    	cmd.setExecutable(true);//unless set false inside the next if - default to executable;
    	  //if the connection is not oracle or timesten, then dont recognize it as SQLPLUS.. recognize it as unknown
        if((getConn()!=null)&&((!isOracleConnection&&!isTimesTenConnection&&!isHttp)  && cmd.getStmtType()==SQLCommand.StmtType.G_C_SQLPLUS && 
        		!SQLCommand.isNonOracleExecutable(cmd)))
        {
        ///*sqldev:stmt*/", new SQLCommand(G_C_SQL, G_S_CREATE_UNKNOWN, G_R_NONE, true));
           cmd.setStmtClass(SQLCommand.StmtType.G_C_SQL);
           cmd.setStmtId(SQLCommand.StmtSubType.G_S_CREATE_UNKNOWN);
           cmd.setResultsType(SQLCommand.StmtResultType.G_R_NONE);
           cmd.setExecutable(true);
        } 
        if(((getScriptRunnerContext().getProperty(ScriptRunnerContext.NOLOG)!=null)
                &&((Boolean)getScriptRunnerContext().getProperty(ScriptRunnerContext.NOLOG).equals(Boolean.TRUE)
                &&!SQLCommand.isNoLogExecutable(cmd)))) {
        // now have nolog as well
            ///*sqldev:stmt*/", new SQLCommand(G_C_SQL, G_S_CREATE_UNKNOWN, G_R_NONE, true));
            if (!(cmd.getStmtClass().equals(SQLCommand.StmtType.G_C_UNKNOWN)||
                    (cmd.getStmtClass().equals(SQLCommand.StmtType.G_C_SQLPLUS)&&
                            (cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_UNKNOWN)||
                                  cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SET_UNKNOWN))))) {
            	//TODO - Setting this connection
                //cmd.setStmtClass(SQLCommand.StmtType.G_C_NOT_CONNECTED);
                //cmd.setStmtId(SQLCommand.StmtSubType.G_S_UNKNOWN);
            }
            //cmd.setStmtId(SQLCommand.StmtSubType.G_S_CREATE_UNKNOWN);
            //cmd.setResultsType(SQLCommand.StmtResultType.G_R_NONE);
            cmd.setExecutable(false);
        }
        //this is the single point of truth m_out is used over ctx
        //enforce the wrap if it has not already been made.
        if (!(m_out instanceof WrapListenBufferOutputStream)) {
            getScriptRunnerContext().setOutputStreamWrapper(m_out);
            //need to make sure m_out is a wrapper if it is already a wrapper 
            //the switch has been made if necesary - spin a few cycles putting a wrapper around m_out,
            m_out = getScriptRunnerContext().getOutputStream();
        }
        
        //do echo before substitution (and even if exited which is pre static of set echo behavior)
        if ((getScriptRunnerContext().getProperty(ScriptRunnerContext.SILENT) == null)) {
        	SetEcho.coreBeginWatcher(m_conn, getScriptRunnerContext(), cmd);//i.e. the output switch on off is elsewhere
        }
        //Nolog checked to see if this is a command we can run while not connected.
        if (context.getExited()
            	//&& SQLCommand.checkNologCommands(cmd.getStmtSubType(),context)
        		) {
            //do not even want to do the substitution if in exited state.
           // fire begin listeners
        	//fire echo ?
            CommandRegistry.fireBeginListeners(m_conn, getScriptRunnerContext(), cmd);
            return;
        }
        // reolve substitution
        Substitution sub = new Substitution(getScriptRunnerContext());
        try {
            try{
                //do substitution before even firing begin listeners.
                sub.replaceSubstitution(cmd);
            } finally {
            //	if (SQLCommand.checkNologCommands(cmd.getStmtSubType(),context)) {
                CommandRegistry.fireBeginListeners(m_conn, getScriptRunnerContext(), cmd);
            	//}
            }
        } catch (SubstitutionException e) {
            report(e.getMessage());
            return;
        }

        /*
         * // populate the binds list on the command. List al = DBUtil.getBinds(cmd.getSql(), true); cmd.setBinds((ArrayList)al);
         */
        if (getScriptRunnerContext().getExited()) {
            // do nothing
            // the begin listener could have canceled the script
            // in the example of set pause if the user clicked the no to
            // continue
            return;
        }
          
        if((getConn()!=null)&&!(isOracleConnection||isHttp) && (cmd.getStmtId() == SQLCommand.StmtSubType.G_S_UNKNOWN)){
            //We should recognize all Oracle commands, if we dont then report a warning and dont run it
            //we can be a little more aggresive with unknown commands if its not an ORacle database.
            //basically we know that we dont recognize all non Oracle commands, so we shouldnt ignore them.
            //We should recognize all Oracle commands, if we dont then report a warning and dont run it
            //this changes the type of the cmd to the same as if it had the HINT /*sqldev:stmt*/
            SQLCommand genericStmtCmd = SQLStatementTypes.hintTokens.get("/*sqldev:stmt*/");
            cmd.setStmtClass(genericStmtCmd.getStmtClass());
            cmd.setStmtId(genericStmtCmd.getStmtId());
            cmd.setResultsType(genericStmtCmd.getResultsType());
            cmd.setExecutable(genericStmtCmd.getExecutable());
        }
            
            if (!CommandRegistry.fireListeners(m_conn, getScriptRunnerContext(), cmd)) { // will the listeners handle this
                // if not handle the statement here
            	
            	switch (cmd.getStmtType()) {
                    case G_C_SQL:
                    	if (cmd.getExecutable()) {
                    		runSQL(cmd);
                    		if(getScriptRunnerContext().getProperty(ScriptRunnerContext.LAST_ERR_TYPE)!=null) cmd.setFail();
                    	} else { 
                            report(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOT_CONNECTED));cmd.setFail();
                    	}
                        break;// theres a little mising logic here. it only wants to do Oracle Desc, not Desc for any other connection type
                    case G_C_PLSQL:
                    	if (cmd.getExecutable()) {
                    		if (cmd.getRunnable()) {
                    		  cmd.setProperty("INSECURE_LITERALS","true");
	                    		runPLSQL(cmd);
	                    		if(getScriptRunnerContext().getProperty(ScriptRunnerContext.LAST_ERR_TYPE)!=null) {
	                    			cmd.setFail();
	                    		}
                    		} else {
                    			if (getScriptRunnerContext().getProperty(ScriptRunnerContext.JLINE_SETTING)!=null) {
                        			getScriptRunnerContext().getSQLPlusBuffer().resetBuffer(cmd.getSQLOrig());
                    	        	getScriptRunnerContext().getSQLPlusBuffer().getBufferSafe().resetBuffer(Arrays.asList(cmd.getSQLOrigWithTerminator().split("\n")));
                    	        }
                    		}
                    	} else {
                            report(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOT_CONNECTED));cmd.setFail();
                    	}
                        break;
                    case G_C_SQLPLUS:
                    case G_C_OLDCOMMENT:
                    	if (cmd.getExecutable()||!cmd.getStmtSubType().equals(StmtSubType.G_S_EXECUTE)) {
                    		runSQLPLUS(cmd);
                    	} else {
                            report(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOT_CONNECTED));
                            report(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOT_CONNECTED_FOR_EXECUTE));
                            cmd.setFail();
                    	}
                        break;
                    case G_C_COMMENT:
                    case G_C_MULTILINECOMMENT:
                    case G_C_EMPTYLINE:
                        //do nothing
                        break;
                    case G_C_UNKNOWN:
                            unknown(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.UNKNOWN_COMMAND), cmd);
                            report("\n"); //$NON-NLS-1$
                        break;
                    case G_C_NOT_CONNECTED:
                        report(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOT_CONNECTED));cmd.setFail();
                        break;
                    default:
                        unknown("class", cmd); //$NON-NLS-1$
                        break;
                }
            } else {//startup change the connection
                if ((cmd.getStmtSubType().equals(StmtSubType.G_S_STARTUP)||
                        (cmd.getStmtSubType().equals(StmtSubType.G_S_SHUTDOWN)))
                        &&(getScriptRunnerContext().getCurrentConnection()!=null)&&
                        (!(m_conn!=null&&m_conn.equals(getScriptRunnerContext().getCurrentConnection())))) {
                    m_conn=getScriptRunnerContext().getCurrentConnection();
                }
            }
     
        // fire end listeners, only if the statement was run. If the statement is not runnable then it is handled by the data grid which happens latter
        // the fireEndListeners should happen after the data grid is run
        if(cmd.getRunnable() 
        		//&& SQLCommand.checkNologCommands(cmd.getStmtSubType(), context)
        		){
        	CommandRegistry.fireEndListeners(m_conn, getScriptRunnerContext(), cmd);
        }

        // mostly for testing but if system.out is being used not need print the
        // force_print
        if (m_scriptRunnerContext.getProperty(ScriptRunnerContext.SYSTEM_OUT) == null || !(Boolean) m_scriptRunnerContext.getProperty(ScriptRunnerContext.SYSTEM_OUT)) {
            m_out.write(FORCE_PRINT_BYTES);
        }
        m_out.flush();
    	}
    }
    
    public String createReportMsg(ScriptRunnerContext context, ISQLCommand cmd) {
      if (context.isSQLPlusClassic()) {
        String s = cmd.getSQLOrig().split(" ", 2)[0];
        return ScriptRunnerDbArb.format(ScriptRunnerDbArb.RESTRICTED_COMMAND_CLASSIC, s);
      } else {
        return ScriptRunnerDbArb.format(ScriptRunnerDbArb.RESTRICTED_COMMAND, cmd.getSQLOrig());
      }
    }
    
    private void runPLSQL(ISQLCommand cmd) {
        String sql = cmd.getSql();
        
        PLSQL plsql = new PLSQL(cmd, m_out);
        boolean replaceBinds = false;  // only for anonymous blocks
        if((getScriptRunnerContext().getProperty(ScriptRunnerContext.SECURELITERALS)==null && (cmd.getEndLine()-cmd.getStartLine()<20) )||
                ((getScriptRunnerContext().getProperty(ScriptRunnerContext.SECURELITERALS)!=null 
                && (getScriptRunnerContext().getProperty(ScriptRunnerContext.SECURELITERALS).equals("ON")))))   //$NON-NLS-1$
        	try {
        		String prefix = sql.substring(0, 100); // performance shortcut introduced during apex investigation
        		List<LexerToken> src = LexerToken.parse(prefix);
        		for( LexerToken t : src ) {
        			if( "begin".equalsIgnoreCase(t.content) ) {
        				replaceBinds = true;
        				break;
        			}
        			if( "declare".equalsIgnoreCase(t.content) ) {
        				replaceBinds = true;
        				break;
        			}
        			break;
        		}
                if( replaceBinds ) { // more detailed check
                	replaceBinds = false;
                	ParsedSql parsed = new ParsedSql(sql);
                	ParseNode root = parsed.parse();
                	if( root.contains("block_stmt") )
                		replaceBinds = true;
                	else 
                		for( ParseNode child : root.children() ) {
                			if( child.contains("block_stmt") )
                				replaceBinds = true;
                			break;
                		}
        		}
        	} catch( Exception e ) {}
        Map<String,String> auxBinds = new HashMap<String,String>();

        if( replaceBinds ) {
            Literals2Binds bp = null;
            //if (!ScriptUtils.isHttpCon(m_conn, getScriptRunnerContext())) {
                bp = Literals2Binds.bindAid(sql);
            //}else {
            //    bp = Literals2Binds.Literals2BindsNull(sql);
            //}
            cmd.setSql(bp.getSql());
            plsql = new PLSQL(cmd, m_out);
            auxBinds = bp.getBinds();
            for( String bindName : auxBinds.keySet() ) {
                Bind b = new Bind();
                b.setName(bindName);
                b.setType("VARCHAR2"); //$NON-NLS-1$
                b.setValue(auxBinds.get(bindName));
                getScriptRunnerContext().getVarMap().put(bindName, b);              
            }
        } 

        try {
        	m_commandRunner = plsql;
        	plsql.setScriptRunnerContext(getScriptRunnerContext());
        	plsql.setConn(m_conn);
        	// plsql.setStatusLine(statusLine);
        	plsql.run();
        	setScriptRunnerContext(plsql.getScriptRunnerContext());
        	if (getScriptRunnerContext().getProperty(ScriptRunnerContext.JLINE_SETTING)!=null) {
        		getScriptRunnerContext().getSQLPlusBuffer().resetBuffer(cmd.getSQLOrigWithTerminator());
        		getScriptRunnerContext().getSQLPlusBuffer().getBufferSafe().resetBuffer(Arrays.asList(cmd.getSQLOrigWithTerminator().split("\n")));
        	}
        } finally {
        	for( String bindName : auxBinds.keySet() )
        		getScriptRunnerContext().getVarMap().remove(bindName);
            plsql = null;
        }
         
    }
    
    private void runSQL(ISQLCommand cmd) {
        SQL sql = new SQL(cmd, m_out);
        m_commandRunner=sql;
        sql.setScriptRunnerContext(getScriptRunnerContext());
        sql.setConn(m_conn);
        int originalFeedback = sql.getScriptRunnerContext().getFeedback();
        // sql.setStatusLine(statusLine);
        try {
            if (cmd.getStmtId().equals(SQLCommand.StmtSubType.G_S_DESCRIBE)) {
                sql.getScriptRunnerContext().setFeedback(ScriptRunnerContext.FEEDBACK_OFF);
            }
        } catch (Exception e) {
        }
        sql.run();
        setScriptRunnerContext(sql.getScriptRunnerContext());
        if (getScriptRunnerContext().getProperty(ScriptRunnerContext.JLINE_SETTING)!=null) {
        	getScriptRunnerContext().getSQLPlusBuffer().getBufferSafe().resetBuffer(cmd.getSQLOrigWithTerminator());//keep the terminator bug 22569924
        }
        sql.getScriptRunnerContext().setFeedback(originalFeedback);//only turn off feedback for the DESC command, dont affect other commands to be run
        // sql.start();
        sql = null;
    }
    
    private void runSQLPLUS(ISQLCommand cmd) {
        SQLPLUS sqlplus = new SQLPLUS(cmd, m_out);
        m_commandRunner = sqlplus;
        sqlplus.setScriptRunnerContext(getScriptRunnerContext());
        sqlplus.setConn(m_conn);
        sqlplus.setDirectory(m_directory);
        sqlplus.run();
        m_conn = sqlplus.getConn();// Connection may have changed due to a connect.
        setScriptRunnerContext(sqlplus.getScriptRunnerContext());
        sqlplus = null;
        getScriptRunnerContext().setCurrentConnection(m_conn); //this has to be passed around as well
    }
    
    public void setConn(Connection conn) {
        this.m_conn = conn;
    }
    
    public void setOut(BufferedOutputStream out) {
        this.m_out = out;
    }
    
    public void setPrintComments(boolean printComments) {
        m_printComments = printComments;
    }
    
    public void setScriptRunnerContext(ScriptRunnerContext scriptRunnerContext) {
        this.m_scriptRunnerContext = scriptRunnerContext;
    }
    
    public void setStatusDisplay(ScriptStatusDisplay status) {
        this.m_status = status;
    }
    
    public void setStatusLine(String statusLine) {
        if (m_status != null) {
            m_status.setText(statusLine);
        }
    }
    
    private void unknown(String text, ISQLCommand cmd) {
        try {
            setStatusLine(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.UNKNOWN_COMMAND));
            String localString = ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(), ScriptRunnerDbArb.format(ScriptRunnerDbArb.UNKNOWN_COMMAND_ARG, text), this.getScriptRunnerContext());
            m_out.write(localString.getBytes(m_scriptRunnerContext.getEncoding()));
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
        }
    }
    
    public void setDirectory(String dir) {
        m_directory = dir;
    }
    
    public void cancel(){
        if(m_commandRunner!=null){
            m_commandRunner.interrupt();
        }
    }
}
