/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOError;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import oracle.dbtools.common.utils.FileUtils;
import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.ConnectionResolver;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.plusplus.IBuffer;
import oracle.dbtools.raptor.backgroundTask.IRaptorTaskProgressUpdater;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.newscriptrunner.commands.BottomTitle;
import oracle.dbtools.raptor.newscriptrunner.commands.IStoreCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.SetSpool;
import oracle.dbtools.raptor.newscriptrunner.commands.StoreRegistry;
import oracle.dbtools.raptor.newscriptrunner.commands.TopTitle;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowRegistry;
import oracle.dbtools.raptor.newscriptrunner.console.IConsoleReader;
import oracle.dbtools.raptor.newscriptrunner.handlers.StandAloneHandlePauseProvider;
import oracle.dbtools.raptor.newscriptrunner.handlers.StandAloneHandleSetPauseProvider;
import oracle.dbtools.raptor.newscriptrunner.handlers.StandAlonePromptedAcceptProvider;
import oracle.dbtools.raptor.newscriptrunner.handlers.StandAlonePromptedConnectFieldsProvider;
import oracle.dbtools.raptor.newscriptrunner.handlers.StandAlonePromptedPasswordFieldsProvider;
import oracle.dbtools.raptor.newscriptrunner.handlers.StandAlonePromptedSubstitutionProvider;
import oracle.dbtools.raptor.newscriptrunner.restricted.RunnerRestrictedLevel;
import oracle.dbtools.raptor.proformatter.ICodingStyleSQLOptions;
import oracle.dbtools.raptor.proformatter.ICoreFormatter;
import oracle.dbtools.raptor.query.Bind;
import oracle.dbtools.util.Encodings;
import oracle.dbtools.util.IExceptionHandler;
/**
 * Script Runner Context for the Script Runner
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ScriptRunnerContext">Barry McGillin</a> 
 *
 */
public class ScriptRunnerContext implements IScriptRunnerContext, Cloneable {
	public static final String ALWAYSUTF8="UTF-8";  //$NON-NLS-1$
    public static final String CLI_CONN_URL   = "cli.conn.url"; //$NON-NLS-1$
    public static final String CLI_CONN_PROPS = "cli.conn.props"; //$NON-NLS-1$
    public static final String SQL_FORMAT = "sql.format"; //$NON-NLS-1$
    public static final String SQL_FORMAT_FULL = "sql.format.full"; //$NON-NLS-1$ 
    public static final String PRE_COMMAND = "pre.command"; //$NON-NLS-1$
    public static final String POST_COMMAND = "post.command"; //$NON-NLS-1$
    public static final String TOTAL_CMD_COUNT = "total_cmd_count"; //$NON-NLS-1$
    public static final String CURRENT_CMD_COUNT = "current_cmd_count"; //$NON-NLS-1$
    public static final String OUT_STREAM_WRAPPER = "output_stream_wrapper"; //$NON-NLS-1$
    public static final int FEEDBACK_ON = -1;
    public static final int FEEDBACK_OFF = -2;
    public static final String LAST_ERR_TYPE = "sqldev.last.err.type"; //$NON-NLS-1$
    public static final String LAST_ERR_NAME = "sqldev.last.err.name"; //$NON-NLS-1$
    public static final String ERR_ANY_CMDS_IN_SCRIPT = "sqldev.error.any.in"; //$NON-NLS-1$
    public static final String ERR_ENCOUNTERED = "sqldev.error"; //$NON-NLS-1$
    public static final String ERR_MESSAGE = "sqldev.last.err.message"; //$NON-NLS-1$
    public static final String ERR_MESSAGE_SQLCODE = "sqldev.last.err.message.forsqlcode"; //$NON-NLS-1$
    public static final String TOP_BASE_URL = "script.runner.topbase.url"; //$NON-NLS-1$
    public static final String BASE_URL = "script.runner.base.url"; //$NON-NLS-1$
    public static final String NODE_URL = "script.runner.node.url"; //$NON-NLS-1$
    public static final String SYSTEM_OUT = "script.runner.using.systemout"; //$NON-NLS-1$
    public static final String DBCONFIG_GLOGIN = "DBConfig.GLOGIN"; //$NON-NLS-1$
    public static final String DBCONFIG_GLOGIN_FILE = "DBConfig.GLOGIN_FILE"; //$NON-NLS-1$
    public static final String DBCONFIG_DEFAULT_PATH = "DBConfig.DEFAULT_PATH"; //$NON-NLS-1$
    public static final String DBCONFIG_USE_THICK_DRIVER = "DBConfig.USE_THICK_DRIVER"; //$NON-NLS-1$
    public static final String SPOOLOUTBUFFER = "Spool.out.buffer"; //$NON-NLS-1$
    public static final String SPOOLOUTFILENAME = "Spool.out.filename"; //$NON-NLS-1$
    public static final String EXITCOMMIT = "script.runner.exitcommit"; //$NON-NLS-1$
    public static final String POPUPBINDS = "f9.popup.binds"; //$NON-NLS-1$
    public static final String APPINFO = "script.runner.appinfo"; //$NON-NLS-1$
    public static final String APPINFOSTRING = "script.runner.appinfostring"; //$NON-NLS-1$
    public static final String APPINFOARRAYLIST = "script.runner.appinfo.arraylist"; //$NON-NLS-1$
    public static final String ARRAYSIZE = "script.runner.arraysize"; //$NON-NLS-1$
    public static final String AUTOCOMMITCOUNTER = "script.runner.autocommit.counter"; //$NON-NLS-1$
    public static final String AUTOCOMMITSETTING = "script.runner.autocommit.setting"; //$NON-NLS-1$
    public static final String ERR_FOR_AUTOCOMMIT = "script.runner.autocommit.errorFlag"; //$NON-NLS-1$
    public static final String COPYCOMMIT = "script.runner.copycommit"; //$NON-NLS-1$
    public static final String CHECKBOXAUTOCOMMIT = "script.runner.autocommit.checkbox"; //$NON-NLS-1$
    public static final String LASTSETSERVEROUTPUT = "script.runner.lastsetserveroutput"; //$NON-NLS-1$
    public static final String REINIT_ON_EXIT = "script.runner.reinitOnExit"; //$NON-NLS-1$
    public static final String DBMSPUTPUTPANE = "script.runner.dbmsouputpane"; //$NON-NLS-1$
    public static final String RESULTS = "script.runner.results"; //$NON-NLS-1$
    public static final Long AUTOCOMMITON = new Long(-1L);
    public static final Long AUTOCOMMITOFF = new Long(-2L);
    public static final int VERIFY_OFF = -3;
    public static final int VERIFY_ON = -4;
    public static final int AUTOPRINT_OFF = -5;
    public static final int AUTOPRINT_ON = -6;
    public static final String DBPROPS = "sqldev.script.runner.connection.properties"; //$NON-NLS-1$
    public final static String AUTOTRACE_CTXTRACESTATE = "AUTOTRACE_STATE"; //$NON-NLS-1$
    public final static String AUTOTRACE_CTXTRACETYPE = "AUTOTRACE_TYPE"; //$NON-NLS-1$
    public final static String AUTOTRACE_AUTO_EXPLAIN = "EXPLAIN"; //$NON-NLS-1$
    public final static String AUTOTRACE_AUTO_STAT = "STATISTICS"; //$NON-NLS-1$
    public final static String AUTOTRACE_AUTO_ALL = "ALL"; //$NON-NLS-1$
    public final static String AUTOTRACE_AUTO_NONE = "NONE"; //$NON-NLS-1$
    public final static String SHOWSERVEROUTPUT="script.runner.context.serveroutput"; //$NON-NLS-1$
    public final static String SHOWTIMING="script.runner.context.timing"; //$NON-NLS-1$
    public final static String SHOWAUTOPRINT="script.runner.context.autoprint"; //$NON-NLS-1$
    public final static String SETNULL="script.runner.setnull"; //$NON-NLS-1$
    public final static String SETNUMWIDTH = "script.runner.setnumwidth"; //$NON-NLS-1$
    public final static String SETNUMFORMAT = "script.runner.setnumformat"; //$NON-NLS-1$
    public final static String SETPAGESIZE = "script.runner.setpagesize"; //$NON-NLS-1$
    public final static String SETLINESIZE = "script.runner.setlinesize"; //$NON-NLS-1$
    public final static String SETLONG = "script.runner.setlong"; /** set CLOBSIZE n is now = set LONG n */ //$NON-NLS-1$
    public final static String SETLONGCHUNKSIZE = "script.runner.setlongchunksize"; //$NON-NLS-1$
    public final static String SETCOLSEP = "script.runner.setcolsep"; //$NON-NLS-1$
    public final static String SETHEADING = "script.runner.setheading"; //$NON-NLS-1$
    public final static String SETHEADSEP = "script.runner.setheadsep"; //$NON-NLS-1$
    public final static String SETHEADSEPCHAR = "script.runner.setheadsepchar"; //$NON-NLS-1$
    public final static String SETWRAP = "script.runner.setwrap"; //$NON-NLS-1$ 
    public final static String SETPRELIM = "script.runner.setprelim"; //$NON-NLS-1$ 
    public final static String SETSHOWMODE = "script.runner.setshowmode"; //$NON-NLS-1$ 
    public final static String SETDESCRIBE = "script.runner.setdescribe"; //$NON-NLS-1$ 
    public final static String SETSOURCEREF = "script.runner.sourceref"; //$NON-NLS-1$
    public final static String SETSCAN = "sql.scan"; //$NON-NLS-1$
    public final static String TTITLE_ON = "ON"; //$NON-NLS-1$
    public final static String TTITLE_OFF = "OFF"; //$NON-NLS-1$
    public final static String BTITLE_ON = "ON"; //$NON-NLS-1$
    public final static String BTITLE_OFF = "OFF"; //$NON-NLS-1$
    public final static String JLINE_SETTING = "script.runner.jline"; //$NON-NLS-1$
    public final static String NOTSYSDBA = "NOTSYSDBA"; //$NON-NLS-1$
    public final static String OPTLFLAG = "SqlCli.optlflag"; //$NON-NLS-1$
    public final static String SETTRIMSPOOL = "script.runner.settrimspool"; //$NON-NLS-1$
    public final static String SETTRIMOUT = "script.runner.settrimout"; //$NON-NLS-1$
	public final static String NOLOG = "script.runner.sqlplus.nolog";//$NON-NLS-1$
	public final static String SILENT = "script.runner.sqlplus.silent";//$NON-NLS-1$
    //persistent exit value for command line, null=no exit, 0-exit ok, 1-exit on whenever 
    public final static String EXIT_INT = "script.runner.exit_int"; //$NON-NLS-1$
    public final static String EXIT_INT_WHENEVER = "script.runner.exit_int_whenever"; //$NON-NLS-1$
    public final static String EXIT_INT_WHENEVER_WAS_SQLCODE = "script.runner.exit_int_whenever.wassqlcode"; //$NON-NLS-1$    
	public static final String SQLPLUS_CMDLINE_LOGIN = "script.runner.cmdline.login"; //$NON-NLS-1$;
	public static final String BASECONNECTIONID = "script.runner.connection.id"; //$NON-NLS-1$
	public static final String SUFFIX = "script.runner.file.suffix";//$NON-NLS-1$
	public static final String EDITFILE = "script.runner.file.editfile";//$NON-NLS-1$
	public static final String SETCOLOR = "script.runner.color.coding"; //$NON-NLS-1$
	public static final String CDPATH = "script.runner.cd_command"; //$NON-NLS-1$
	public static final String SCRIPT_DEPTH = "script.runner.script_depth";  //$NON-NLS-1$
	public static final String LDAPCON = "script.runner.ldapcon";  //$NON-NLS-1$
    public static final String SETNET = "script.runner.setnet";  //$NON-NLS-1$
    public static final String SETNETOVERWRITE = "script.runner.setnetoverwrite";  //$NON-NLS-1$
    public static final String SQLPROMPTTIME = "script.runner.setsqlplustime";  //$NON-NLS-1$
    public static final String SERVEROUTPUTPENDING = "script.runner.serveroutputpending";  //$NON-NLS-1$
    public static final String SET_PAUSE = "script.runner.set_pause"; //$NON-NLS-1$
    public static final String SET_SECUREDCOL = "script.runner.set_securedcol"; //$NON-NLS-1$
    public static final String SET_SECUREDCOL_UNAUTH = "script.runner.set_securedcol_unauth"; //$NON-NLS-1$
    public static final String SET_SECUREDCOL_UNKNOWN = "script.runner.set_securedcol_unknown"; //$NON-NLS-1$
    public static final String SET_PAUSE_VALUE = "script.runner.set_pause_value"; //$NON-NLS-1$
    public static final String SET_SQLCODE = "script.runner.sqlcode"; //$NON-NLS-1$
    public static final String SET_OSCODE = "script.runner.oscode"; //$NON-NLS-1$
    public static final String SET_XMLFORMAT = "script.runner.set_xmlformat"; //$NON-NLS-1$
    public static final String SET_CLOSECURSOR = "script.runner.set_xmlformat"; //$NON-NLS-1$
    public static final String SET_SQLPLUSCOMPATIBILITY = "script.runner.set_sqlpluscompat"; //$NON-NLS-1$
    public static final String LNO = "script.runner.lno"; //$NON-NLS-1$
    public static final String PNO = "script.runner.pno"; //$NON-NLS-1$
    public static final String PRELIMAUTH = "script.runner.PRELIM_AUTH"; //$NON-NLS-1$
    public static final String PRELIMAUTHPREERROR = "script.runner.PRELIM_AUTHPREERROR"; //$NON-NLS-1$
	public static final String HTTP_PROXY = "script.runner.hhtp.proxy"; //$NON-NLS-1$
	public static final String HTTP_PROXY_PORT = "script.runner.hhtp.proxy.port"; //$NON-NLS-1$
	public static final String HTTP_PROXY_HOST = "script.runner.hhtp.proxy.host"; //$NON-NLS-1$
    public final static String OPTIONAL_SHOW_HEADER = "script.runner.optional_show_header";  //$NON-NLS-1$
    public static final String INCOMPLETE = "incomplete"; //$NON-NLS-1$
    public static final String SPECIAL = "script.runner.special"; //$NON-NLS-1$
    public static final String LOGON = "script.runner.sqlplus.logon"; //$NON-NLS-1$
    public static final String THREETIMES = /* actually !logon for first sqlplus logon*/ "script.runner.threetimes"; //$NON-NLS-1$
    public static final String PROMPTFORDATABASE = "SQLCLDBPROMPT"; /*centralizedconst for eash change/no typos*/ //$NON-NLS-1$
    public static final String CLEARSCREEN = "sqlcl.clear.screen"; //$NON-NLS-1$
	public static final String NEWPAGE = "script.runner.newpage"; //$NON-NLS-1$
	public static final String HIDDENOPTIONX = "script.runner.sqlcl.optionx"; //$NON-NLS-1$
	public static final String HIDDENOPTIONC = "script.runner.sqlcl.optionc"; //$NON-NLS-1$
	public static final String SQLPLUS_CLASSIC_MODE = "sqlplus.classic.mode"; //$NON-NLS-1$
	public static final String SQLPLUS_EMBEDDED_MODE = "sqlplus.embedded.mode"; //$NON-NLS-1$
	public static final String LOGIN_FILE = "sqlplus.login.file"; //$NON-NLS-1$
	public static final String EXPLICITSETECHO = "script.setecho"; //$NON-NLS-1$
	public static final String ERR_SQLCODE = "sqldev.last.err.sqlcode"; //$NON-NLS-1$
	public static final String SQLCLI_STDIN = "script.runner.sqlcl.stdin.input"; //$NON-NLS-1$
	public static final String SQLCLI_STDIN_WAS_ON = "script.runner.sqlcl.stdin.input.was.on"; //$NON-NLS-1$
	public static final String SQLPLUS_EXECUTE_FILE ="script.runner.sqlcl.execute.file"; //$NON-NLS-1$
	public static final String SQLTERMINATOR ="script.runner.sqlcl.sql.terminator"; //$NON-NLS-1$
	public static final String IGNORECOLUMNSETTINGS = "script.runner.sqlcl.ignore.settings"; //$NON-NLS-1$
	public static final String ORACLE_HOME_MAIN_THREAD = "script.runner.oraclehomemainthread"; //$NON-NLS-1$
	public static final String SQLPATH_PROVIDER_MAIN_THREAD = "script.runner.sqlpathprovidermainthread"; //$NON-NLS-1$
	public static final String LASTLOGINTIME = "sqlcl.last.login.time"; //$NON-NLS-1$
	public static final String ISSLASHSTATEMENT = "script.runner.isslashstatement"; //$NON-NLS-1$
	public static final String SERVEROUTFORMAT = "script.runner.serveroutformat"; //NON-NLS-1 //$NON-NLS-1$
	public static final String SERVEROUTPUTUNLIMITED = "script.runner.serveroutputunlimited"; //NON-NLS-1 //$NON-NLS-1$
	public static final String PRINTBLLINES = "script.runner.printblanks.aftersubs"; //NON-NLS-1 //$NON-NLS-1$
    public static final String COMMANDLINECONNECT = "script.runner.commandlineconnect";//NON-NLS-1 //$NON-NLS-1$
	public static final String SERVEROUTNOLOGLATER = "script.runner.serveroutputnologpending"; //NON-NLS-1 //$NON-NLS-1$
	public static final String CONNECT_CALLED = "script.runner.connectcalled"; //NON-NLS-1 //$NON-NLS-1$
	public static final String NOMULTILINEACCEPT = "script.runner.nomultilineaccept"; //NON-NLS-1 //$NON-NLS-1$
	public static final String SQLCLI_GLOGIN_SQL = "sqlcli.glogin.sql"; //NON-NLS-1 //$NON-NLS-1$
	public static final String SQLCLI_LOGIN_SQL = "sqlcli.login.sql"; //NON-NLS-1 //$NON-NLS-1$
	public static final String ECHOSCRIPTDEPTH = "sqlcli.runner.echo.scriptdepth"; //NON-NLS-1 //$NON-NLS-1$
	public static final String NOHISTORY = "sqlcl.batch.nohistory";  //NON-NLS-1 //$NON-NLS-1$
	public static final String NOHISTORYLIST = "sqlcl.batch.nohistory.command.list";  //NON-NLS-1 //$NON-NLS-1$
	public static final String ISHTTPCON = "sqlcl.connection.httpcon";  //NON-NLS-1 //$NON-NLS-1$
	public static final String SETCOLINVISIBLE = "sqlcl.set.colinvisible"; //NON-NLS-1 //$NON-NLS-1$
	public static final String SPOOLTHREAD = "sqlcl.spool.thread"; //NON-NLS-1 //$NON-NLS-1$
	public static final String ALIAS_END_ON_EOF = "sqcl.alias.end_statement_on_eof"; //NON-NLS-1 //$NON-NLS-1$
	public static final String MAXROWS_SPOOLONLY = "sqlcl.spool.spoolonly"; //NON-NLS-1 
	public static final String MAXROWS_NEXTTRUE = "sqlcl.spool.nexttrue"; //NON-NLS-1
	public static final String ERROR_LOGGING = "sqlplus.error.logging";//$NON-NLS-1$
	public static final String ERROR_LOGGING_TABLE = "sqlplus.error.logging.table";//$NON-NLS-1$
	public static final String ERROR_LOGGING_SCHEMA = "sqlplus.error.logging.schema";//$NON-NLS-1$
	public static final String ERROR_LOGGING_DISPLAY_SCHEMATABLE = "sqlplus.error.logging.displayschematable";//$NON-NLS-1$
	public static final String ERROR_LOGGING_IDENTIFIER = "sqlplus.error.logging.identifier";//$NON-NLS-1$
	public static final String SECURELITERALS = "sqlcl.script.secureliterals";//$NON-NLS-1$
	public static final String IN_INITIAL_SQLCLCONNECT = "sqlcl.script.ininitialconnect"; //NON-NLS-1 
	public static final String SERVEROUTPUT_OPTIMIZED = "sqlcl.serveroutput.optimized"; //NON-NLS-1
	private final Logger logger = Logger.getLogger(this.getClass().getName());
	private Connection baseConnection = null;
    private boolean closeConnection = false;
    private boolean topLevel = false;
    private Map<String, String> map = null;
    // Keeping track of Data type for the Substitution variable.
    private Map<String, String> subvartypeMap = null;
    private Map<String,String> columnMap = null;
    private Map<String,String> ovcolumnMap = null;
    private boolean exited = false;
    /** allow exited to be passed back to the caller */
    private boolean returnExited = false;
    private int osError = 0;
    private int sqlError = 0;
    private boolean escape = false;
    private char escapeChar = '\\';
    private char substitutionChar = '&';
    private boolean substitutionOn = true;
    private boolean scanOn = true;
    private char terminateChar = ';';
    private Character substitutionTerminateChar = '.';
    private URL _lastUrl = null;
    private Map<String, Object> _props = new HashMap<String, Object>();
    private boolean supressOutput = false;
    // -1 feedback is on
    // -2 feedback is off
    // positive int is the feedback number
    private int feedback = -1;
    private Object _outputComp;
    // -3 verify off
    private boolean _verify = true;
    private boolean _echo = false;
    private int _autoprint = AUTOPRINT_OFF;
    private String _sqlPrompt = "SQL> "; //$NON-NLS-1$ 
	private String m_basePrompt=""; //$NON-NLS-1$
    private Map<String, Bind> varMap = null;
    private Map<String, List<DataValue>> varBatchMap = null;
    private PrintWriter _errWriter; // vasriniv: introducing for logging failed SQL.
    private IRaptorTaskProgressUpdater m_taskProgressUpdater = null;
    private boolean m_isRunScript = true;
    private Connection m_currentConnection = null;
    private String m_encoding = System.getProperty("file.encoding");  //$NON-NLS-1$
    private static String s_outputEncoding = "UTF-8"; //$NON-NLS-1$    
    private int[] dmlResult ;
    private RunnerRestrictedLevel.Level restrictedLevel = RunnerRestrictedLevel.Level.NONE;
    
    /*
     * In some places a scriptrunnercontext is needed by other closed classes.  it can be set here and returned staticlly where required
     * for specific changes.  For example, when DatatypeUtil formats a refcursor it uses a sqlpluscmdformatter which sets a base context.
     * This will allow it to pick up the latest settings like pagesize, linesize etc.
     */
	private static ScriptRunnerContext _storedContext;
	
    /**
	 * @param _storedContext the _storedContext to set
	 */
	public static void set_storedContext(ScriptRunnerContext ctx) {
		ScriptRunnerContext._storedContext = ctx;
	}

	private boolean dbmsOutputEnable = false;
	private StringBuffer m_dbmsOutput = null;
	private Map<String,String> m_nlsMap = null;
	private boolean m_isConsumerRunning = false;
	private HashMap<String, Savepoint> savepoints = null;
	private HashMap<String, ArrayList<String>> m_formatCmdList = new HashMap<String, ArrayList<String>>();
	private LinkedHashMap<String, ArrayList<String>> m_breakCmdList = new LinkedHashMap<String, ArrayList<String>>();
	// <---- TreeMap  ------><-TreeMap-><-HashMap->
	//                                 <-TreeMap-><-HashMap->
    //                                 <-TreeMap-><-HashMap->
	//                                 ...
    //                                 ...
	private TreeMap<String, ArrayList<TreeMap<String, HashMap<String, String>>>> m_computeCmdList = new TreeMap<String, ArrayList<TreeMap<String, HashMap<String, String>>>>(String.CASE_INSENSITIVE_ORDER);
	private TopTitle m_tTitle = null;
	private BottomTitle m_bTitle = null;
	private String m_tTitleCmd = ""; //$NON-NLS-1$
	private String m_bTitleCmd = ""; //$NON-NLS-1$
	private boolean m_tTitleFlag = false;
	private boolean m_bTitleFlag = false;
	protected String m_EditorText = ""; //$NON-NLS-1$
	private IGetSpoolFilterProvider m_getSpoolFilterProvider = null;
	private IGetPromptedFieldProvider m_getPromptedFieldProvider = new StandAlonePromptedAcceptProvider();
	private IGetPromptedFieldProvider m_getSubstitutionFieldProvider = new StandAlonePromptedSubstitutionProvider();
	private IGetPromptedPasswordFieldsProvider m_getPasswordFieldsProvider = new StandAlonePromptedPasswordFieldsProvider();
	private IGetPromptedConnectFieldsProvider m_getConnectFieldsProvider = new StandAlonePromptedConnectFieldsProvider();
	private IGetHandlePauseProvider m_getHandlePauseProvider = new StandAloneHandlePauseProvider();
	private IGetHandlePauseProvider m_getHandleSetPauseProvider = new StandAloneHandleSetPauseProvider();
	private boolean _jsonOutput = false;
	private Version cmdlineVersion = new Version("4.1.0"); //$NON-NLS-1$
	private boolean _jsonQuery = false;
	
	// This is for LRG/SRG tests only and cannot be used for production.
	// Stores Column formatting for Views.
	private HashMap<String, ArrayList<String>> m_121ViewColCmdList = new HashMap<String, ArrayList<String>>();
	
	ArrayList<ISQLCommand> m_executedCommandList = new ArrayList<ISQLCommand>();
    public ArrayList<ISQLCommand> getExecutedCommandList() {
        return m_executedCommandList;
    }


    public void setScriptText(int startIndex, int endIndex) {
        m_scriptStartIndex = startIndex;
        m_scriptEndIndex = endIndex;
    }

    /** Keep a copy of the script running here, as the parser breaks this into commands , but little bits like semi colon can disapear.
     *  Easier to replicate the script for the history 
     */
    public int m_scriptStartIndex = 0;
    public int m_scriptEndIndex = 0;
    //this is replacing what vadim had in for plsql displays. Its horrible and needs removed.
    //I've put a listener mechanism in to register these in plsql and sql
	public Object node =null;
	private IConsoleReader _sqlplusConsoleReader=null;
	private IBuffer _sqlplusBuffer;
	private ICodingStyleSQLOptions _codingStyleSQLOptions;
	private ICoreFormatter _coreFormatter;
	
    public boolean isRunScript () {
        return m_isRunScript;
    }
    
    public void setIsRunScript(boolean isRunScript){
        m_isRunScript = isRunScript;
    }
    public ScriptRunnerContext() {
        reInitClear();
    }
    
    private void clearCurrent() {
        if (this.getProperty(JLINE_SETTING)==null) {//might want nolog to continue??? on rerun seems an edge case. Not handled.
            if (this.getBaseConnection()!=null) {//could check if its closed
                this.putProperty(ScriptRunnerContext.NOLOG, false);
            }
            this.putProperty(ScriptRunnerContext.PRELIMAUTH, false);
            this.alwaysCloseCurrentConnection();//if base != current //run script twice - second time connection = base connection
            this.setCurrentConnection(this.getBaseConnection());
        }
    }
    //on exit either do a clear or a reinitnoexit i.e.a lesser clear.
    public void reInitOnExit() {
        clearCurrent();
        //depending on setting reinit on exit=clear or reinit on no exit.
        Boolean choice=(Boolean)this.getProperty(this.REINIT_ON_EXIT);
        if ((choice!=null)&&(choice.equals(Boolean.FALSE))) {
            reInitNoExit();
        } else {//Boolean.TRUE or null (and not DBCommandRunner connections.)
            reInitClear();
        }
    }
    public void reInitClear() {//note sqlcli calls this for real first time as JLINE_SETTING is null on first init.
    	if (this.getProperty(JLINE_SETTING)==null) {
    		deferredReInitClear();
    	}
    	try {
	    	getMap().put(SqlplusVariable._PWD.toString(),Paths.get(".").toRealPath().toString());
		} catch (IOException e) {		}
    }
    //this clears the context
    public void deferredReInitClear() {
    
        closeConnection = false;
        topLevel = false;
        map = Collections.synchronizedMap(new HashMap<String, String>());
        subvartypeMap = Collections.synchronizedMap(new HashMap<String, String>());
        varMap = Collections.synchronizedMap(new LinkedHashMap<String, Bind>());
        varBatchMap = null;
        columnMap = Collections.synchronizedMap(new HashMap <String, String>());
        ovcolumnMap = Collections.synchronizedMap(new HashMap <String, String>());
        returnExited = exited;
        exited = false;
        osError = 0;
        sqlError = 0;
        escape = false;
        substitutionChar = '&';
        substitutionOn = true;
        scanOn = true;
        _errWriter = null;
        m_isConsumerRunning=true;

        if ((this.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER)) != null) {
            stopSpool();
        }
        setFeedback(6);
        putProperty(NOHISTORY, Boolean.FALSE);
        putProperty(SQLTERMINATOR, ";"); //$NON-NLS-1$
        putProperty(POPUPBINDS, new HashMap<String, Object>());
        putProperty(APPINFO, new Boolean(false));
        putProperty(APPINFOARRAYLIST, new ArrayList<String> () ); 
        putProperty(AUTOCOMMITSETTING,AUTOCOMMITOFF);
        putProperty(AUTOCOMMITCOUNTER,new Long(0L));
        putProperty(COPYCOMMIT,"0"); //$NON-NLS-1$
        putProperty(IGNORECOLUMNSETTINGS, Boolean.FALSE);
        putProperty(SETPAGESIZE, new Integer(14));
        putProperty(NEWPAGE, new Integer(1));
        putProperty(SETLINESIZE, new Integer(80));
        putProperty(SETLONG, new Integer(80));
        putProperty(SETLONGCHUNKSIZE, new Integer(80));
        putProperty(SETHEADING, "ON"); //$NON-NLS-1$
        putProperty(EXITCOMMIT, "ON"); //$NON-NLS-1$
        putProperty(SQLPLUS_CLASSIC_MODE, Boolean.FALSE);
        putProperty(SQLPLUS_EMBEDDED_MODE,"OFF"); //$NON-NLS-1$
        putProperty(SETHEADSEP, "ON"); //$NON-NLS-1$
        putProperty(SETTRIMOUT,  new Boolean(true));
        putProperty(SETHEADSEPCHAR, "|"); //$NON-NLS-1$
        putProperty(SETWRAP, "ON"); //$NON-NLS-1$
        putProperty(SETPRELIM, "OFF"); //$NON-NLS-1$
        putProperty(SETSHOWMODE, "OFF"); //$NON-NLS-1$
        putProperty(SETDESCRIBE, new HashMap<String, Object>()); // DEPTH = 1, linenum, indent
        putProperty(SETCOLSEP," "); //$NON-NLS-1$
        putProperty(SETSCAN, "ON"); //$NON-NLS-1$
        putProperty(SET_PAUSE, Boolean.FALSE);
        putProperty(SET_SECUREDCOL, Boolean.FALSE);
        putProperty(SET_SECUREDCOL_UNAUTH,"******************************"); //$NON-NLS-1$
        putProperty(SET_SECUREDCOL_UNKNOWN, "??????????????????????????????"); //$NON-NLS-1$
        putProperty(SET_XMLFORMAT, Boolean.FALSE);
        putProperty(SET_CLOSECURSOR, Boolean.FALSE);
        putProperty(SET_PAUSE_VALUE, ""); //$NON-NLS-1$
        putProperty(PRINTBLLINES,Boolean.TRUE);
        putProperty(SETCOLINVISIBLE, Boolean.FALSE);
        putProperty(SETNUMWIDTH, new Integer(10)); 
        putProperty(EDITFILE, "afiedt.buf"); //$NON-NLS-1$
        putProperty(SUFFIX, "sql");//$NON-NLS-1$
        putProperty(SETNUMFORMAT, ""); //$NON-NLS-1$
        putProperty(SETSOURCEREF,""); //$NON-NLS-1$
        putProperty(SETCOLOR, "OFF"); //$NON-NLS-1$
        putProperty(SQLPROMPTTIME, "OFF"); //$NON-NLS-1$
        putProperty(LNO, new Integer(0));
        putProperty(PNO, new Integer(0));
        putProperty(SET_SQLCODE, new Integer(0));
        putProperty(SET_OSCODE, new Integer(0));
        putProperty(SQLCLI_GLOGIN_SQL, Boolean.FALSE);
        putProperty(SQLCLI_LOGIN_SQL, Boolean.FALSE);
        putProperty(ERROR_LOGGING, Boolean.FALSE);
        putProperty(ERROR_LOGGING_TABLE, "SPERRORLOG");
        removeProperty(SERVEROUTPUT_OPTIMIZED);
        
        String defaultEditor="vi"; /* actually ed in real slplus */ //$NON-NLS-1$
    	if (System.getenv("SQLPLUS_CLASSIC")!=null) { //$NON-NLS-1$
			putProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE, Boolean.TRUE);
			defaultEditor="ed"; //$NON-NLS-1$
		}
        String envEditor=System.getenv("EDITOR"); //$NON-NLS-1$
        if ((envEditor!=null)&&(!envEditor.equals(""))) { //$NON-NLS-1$
            defaultEditor=envEditor;
        } else {
            if ((System.getProperty("os.name").toLowerCase().indexOf("win")>-1)) { //$NON-NLS-1$  //$NON-NLS-2$
                defaultEditor="notepad"; //$NON-NLS-1$
            }
        }
        getMap().put(SqlplusVariable._EDITOR.toString(), defaultEditor);
        setSubstitutionTerminateChar('.');
        resetDDLOptions();
    }
    
    public void reInitNoExit() {
    	if (this.getProperty(JLINE_SETTING)==null) {
    		deferredReInitNoExit();
    	}
    }
    public void deferredReInitNoExit() {
    	this.clearCurrent();
        closeConnection = false;
        topLevel = false;
        _errWriter = null;
        exited = false;
        this.putProperty(APPINFOARRAYLIST, new ArrayList<String> ());
        if ((this.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER)) != null) {
            stopSpool();
        }
    }
    
    public void stopSpool(){
    	//stop spool now echos if there is no spool running so only call is there is a spool
        new SetSpool().stopSpool(this);//see setSpool for setting up spool
    }
    public PrintWriter getErrWriter() {
        return _errWriter;
    }
    
    public void setErrWriter(PrintWriter errW) {
        _errWriter = errW;
    }

    public static String getOutputEncoding()
    {
        return s_outputEncoding;
    }
  
    public void setNLSMap(Map<String,String> nlsMap){
        m_nlsMap = nlsMap;
    }
    
    public Map<String,String> getNLSMap(){
        return m_nlsMap;
    }
    public static void setOutputEncoding(String in)
    {
        s_outputEncoding= in;
    }
    public byte[] stringToByteArrayForScriptRunnerNonStatic(String in) {
        try {
           return in.getBytes(getEncoding());
        } catch (UnsupportedEncodingException e) {
           return in.getBytes();
        }
    } 

    public Connection getBaseConnection() {
        return baseConnection;
    }
    
    public void setBaseConnection(Connection baseConnection) {
        this.baseConnection = baseConnection;
    }
    
    public boolean getExited() {
        return exited;
    }
    
    public void setExited(boolean exited) {
        this.exited = exited;
        if (exited) {
            Integer exitVal=(Integer)getProperty(ScriptRunnerContext.EXIT_INT);
            if (exitVal==null) {
                putProperty(ScriptRunnerContext.EXIT_INT,0);
            }
        }
    }
    
    public boolean getReturnExited() {
        return returnExited;
    }
    
    public void setReturnExited(boolean returnExited) {
        this.returnExited = returnExited;
    }
    
    public String getPreCommand(){
      return (String) _props.get(PRE_COMMAND);
    }
    public void setPreCommand(String cmd){
      _props.put(PRE_COMMAND, cmd);
    }
    
    public String getPostCommand(){
      return (String) _props.get(POST_COMMAND);
    }
    public void setPostCommand(String cmd){
      _props.put(POST_COMMAND, cmd);
    }
    
    public boolean getEscape() {
        return escape;
    }
    
    public void setEscape(boolean escape) {
        this.escape = escape;
    }
    
    public boolean getCloseConnection() {
        return closeConnection;
    }
    
    public void setCloseConnection(boolean closeConnection) {
        this.closeConnection = closeConnection;
    }
    
    public boolean getTopLevel() {
        return topLevel;
    }
    
    public void setTopLevel(boolean topLevel) {
        this.topLevel = topLevel;
    }
    
    public int getOsError() {
        return this.osError;
    }
    
    public void setOsError(int osError) {
        this.osError = osError;
    }
    
    public int getSqlError() {
        return this.sqlError;
    }
    
    public void setSqlError(int sqlError) {
        this.sqlError = sqlError;
    }
    
    public Map<String, String> getMap() {
        return this.map;
    }
    
    public Map<String, String> getSubVarTypeMap() {
        return subvartypeMap;
    }
    
    public void setMap(Map<String, String> map) {
        this.map = map;
    }
    public Map <String,String> getColumnMap() {
        return this.columnMap;
    }

    public void setColumnMap(Map <String,String> columnMap) {
        this.columnMap = columnMap;
    }
    
    public Map <String,String> getOVColumnMap() {
        return this.ovcolumnMap;
    }

    public void setOVColumnMap(Map <String,String> columnMap) {
        this.ovcolumnMap = columnMap;
    }

    public char getSubstitutionChar() {
        return this.substitutionChar;
    }
    
    public void setSubstitutionChar(char substitutionChar) {
        this.substitutionChar = substitutionChar;
    }
    
    public boolean getSubstitutionOn() {
        return substitutionOn;
    }
    
    public void setSubstitutionOn(boolean substitutionOn) {
        this.substitutionOn = substitutionOn;
    }
    
    public boolean getScanOn() {
        return scanOn;
    }
    
    public void setScanOn(boolean scanOn) {
        this.scanOn = scanOn;
    }
    
    public char getEscapeChar() {
        return this.escapeChar;
    }
    
    public void setEscapeChar(char escapeChar) {
        this.escapeChar = escapeChar;
    }
    
    public char getTerminateChar() {
        return this.terminateChar;
    }
    
    public void setTerminateChar(char terminateChar) {
        this.terminateChar = terminateChar;
    }
    
    public Character getSubstitutionTerminateChar() {
        return this.substitutionTerminateChar;
    }
    
    public void setSubstitutionTerminateChar(Character substitutionTerminateChar) {
        this.substitutionTerminateChar = substitutionTerminateChar;
    }
 
    public String getLastDirName() {
        try {
            if (_lastUrl == null) {
                return null;
            }
            return FileUtils.getDirFromFileURL(_lastUrl);
        } catch (IOException ioe) {
            /* on error skip the node check */
            return null;
        }
    }
    public String getLastNodeDirName() {
        try {
            URL nodeURL = (URL)this.getProperty(ScriptRunnerContext.NODE_URL);
            if ((nodeURL == null)) { //$NON-NLS-1$
                return null;
            }
            return FileUtils.getDirFromFileURL(nodeURL);
        } catch (IOException ioe) {
            /* on error skip the node check */
            return null;
        }
    }

    public URL getLastNodeForDirNameURL(){
        URL nodeURL = (URL)this.getProperty(ScriptRunnerContext.NODE_URL);
        if (nodeURL == null) { //$NON-NLS-1$
            return null;
        }
        return nodeURL;
    }
    public URL getLastUrl() {
        return this._lastUrl;
    }
    
    public void setLastUrl(URL lastUrl) {
        this._lastUrl = lastUrl;
    }
    
    public void putProperty(String name, Object value) {
        _props.put(name, value);
    }
    
    public Object removeProperty(String name) {
        return _props.remove(name);
    }
    
    public Object getProperty(String name) {
        return _props.get(name);
    }
    
    public Map<String,Object> getProperties() {
    	return _props;
    }
    public void setProperties(Map<String, Object> properties) {
    	_props = properties;
    }
    public void putProperties(Map<? extends String,? extends Object> props) {
        _props.putAll(props);
    }
    public WrapListenBufferOutputStream getOutputStream() {
        return (WrapListenBufferOutputStream) _props.get(OUT_STREAM_WRAPPER);
    }
    
    public void setOutputStreamWrapper(BufferedOutputStream out) {
        if (getOutputStream()==null) {
            if (out instanceof WrapListenBufferOutputStream) {
                _props.put(OUT_STREAM_WRAPPER, out);
            } else if(_jsonOutput){
           	 _props.put(OUT_STREAM_WRAPPER, new JSONWrapBufferedOutputStream(out, this));
           } else {
               _props.put(OUT_STREAM_WRAPPER, new WrapListenBufferOutputStream(out, this));
            }
        } else {
            try {
                //get the 'bottom' main Stream (want to avoid nested (as opposed to listed) buffered output streams)
                //Spool bufferedOutputStream added by getOutputStream.addtolist(OutputStream)
                BufferedOutputStream bos = out;
                while (true) {
                    if (bos instanceof WrapListenBufferOutputStream) {
                        bos = ((WrapListenBufferOutputStream) bos).getMainStream();
                    } else {
                        break;
                    }
                }
                getOutputStream().replaceMainStream(bos);
            } catch (IOException e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getStackTrace()[ 0 ].toString(), e);
            }
        }
    }
    
    public boolean isOutputSupressed() {
        return supressOutput;
    }
    
    public void setSupressOutput(boolean supress) {
        supressOutput = supress;
    }
    
    public void setFeedback(int feed) {
        feedback = feed;
    }
    
    public int getFeedback() {
        return feedback;
    }
    
    public void setOutputComponenet(Object outputComp) {
        _outputComp = outputComp;
    }
    
    public Object getOutputComponent() {
        return _outputComp;
    }
    
  
    public void write(String s) {
      if(isConsumerRunning()){
        try {
            this.getOutputStream().write(this.stringToByteArrayForScriptRunnerNonStatic(s));
        } catch (IOException e) {
            logger.log(Level.SEVERE, Messages.getString("ScriptRunnerContext.15"), e); //$NON-NLS-1$
        }
      } else {
          Logger.getLogger(getClass().getName()).log(Level.INFO,"The following was not written to the Script Runner Result Window as the outputStream was closed:"+s); //$NON-NLS-1$
      }
    }
    
    public void setVerify(boolean id) {
        _verify = id;
    }
    
    public boolean isCommandLine(){
      return _props.get(JLINE_SETTING) != null;
    }
    public boolean isVerifyOn() {
        return _verify;
    }
    public void setEcho(boolean echo) {
        _echo = echo;
    }
    
    public boolean isEchoOn() {
        return _echo;
    }
    
    public Map<String, Bind> getVarMap() {
        return varMap;
    }
    
    public void setVarMap(Map<String, Bind> map) {
    	varMap = map;
    }
    
    public Map<String, List<DataValue>> getBatchVarMap() {
      return varBatchMap;
    }

    public void setBatchVarMap(Map<String, List<DataValue>> map) {
      varBatchMap = map;
    }
    
    /**
     * @return the _autoprint
     */
     public int get_autoprint() {
        return _autoprint;
    }
    
    /**
     * @param _autoprint the _autoprint to set
     */
    public void set_autoprint(int _autoprint) {
        this._autoprint = _autoprint;
    }
    
    public IRaptorTaskProgressUpdater getTaskProgressUpdater() {
        return m_taskProgressUpdater;
    }
    
    public void setTaskProgressUpdater(IRaptorTaskProgressUpdater taskProgressUpdater) {
        m_taskProgressUpdater = taskProgressUpdater;
    }
    /*
     * public void setVarMap(HashMap varMap) { this.varMap = varMap; }
     */
    
    public void setCurrentConnection(Connection conn) {
//release the last currentConnection if any
//  if(getCurrentConnection() != null){
//      LockManager.unlock(getCurrentConnection());
//}
        if (((m_currentConnection!=null)&&(!(conn!=null&&conn.equals(m_currentConnection))))||(conn==null)) {
            //Connection identified from login .sql gets lost, want to null all but connection identifier
        	//Assumption - connection changed theirfor gone through connect which nulls and sets connection identifier 
        	this.doAllNullSqlplusVar();
        }
        m_currentConnection = conn;
       
//lock the connection
//  LockManager.lock(conn);
    }

    public Connection getCurrentConnection() {
        return m_currentConnection;
    }
	/**
	 * For any worksheet work, we need to make sure that the context we pass in
	 * gets IdeUtil.getIdeEncoding();
	 * getEncoding
	 * @return ScriptRunnerContext
	 */
    public String getEncoding() {
        return m_encoding;
    }

    public void setEncoding(String encoding) {
    	String correctEncodingName = correctEncodingName(encoding);
    	if( correctEncodingName!=null){
    		m_encoding =correctEncodingName;
    	} else {
    		m_encoding=encoding; // some issues with upgrading preferences and UTF8 being used not UTF-8. Throw new IllegalArgumentException("This is not a valid encoding:"+encoding); //$NON-NLS-1$
    	}
    }

	private String correctEncodingName(String encoding) {
       	String[] encodings = Encodings.getEncodingsWithoutAlias();
       	for(String validEncoding:encodings){
       		if(validEncoding.equalsIgnoreCase(encoding)){
       			return validEncoding;
       		}
       	}
       	return null;
	}

	/**
     * given column map - if our variable comes up, stuff it in a substitution
     * variable. Example from Apex:
     * 
     * column foo3 new_val LOG1
     * 
     * select 'install'||to_char(sysdate,'YYYY-MM-DD_HH24-MI-SS')||'.log' foo3
     * from dual;
     * 
     * [can also be can be column foo3 new_val LOG1 NOPRINT]
     * 
     * Current functionality to do much the same thing is:
     * 
     * VARIABLE LOG1 varchar2(1000)
     * 
     * begin select 'install'||to_char(sysdate,'YYYY-MM-DD_HH24-MI-SS')||'.log'
     * into :LOG1 from dual; end; /
     * 
     * Bug 25030438: Making sure to get the valid data type for the column.
     */
    public void updateColumn(String[] columns, String[] values, String[] types) {
        Map <String,String> cMap = this.getColumnMap();
        boolean valuesIsNull = (values == null);
        if ((cMap.size() == 0)) {
            return;
        }
        for (int i=1; i<columns.length; i++) {
            String columnName = columns[i];
            String s = null;
            if (!valuesIsNull) {
                s=values[i];
            }
            if (s==null) {
                s=""; //$NON-NLS-1$
            }
            
            String dataType = null;
            if(types != null) {
            	dataType = types[i];
            }

            /**
             *  get what the column name matches if anything
             */
            String theSubVariable = cMap.get(columnName.toUpperCase());
            if (theSubVariable!=null) {
                if (!((valuesIsNull) && (this.getMap().keySet().contains(theSubVariable.toUpperCase())))){
                    this.getMap().put(theSubVariable.toUpperCase(),s);
                    if(dataType != null)
                       subvartypeMap.put(theSubVariable.toUpperCase(), dataType);
                }
                if (valuesIsNull) {//no rows if not set default
                    if (!(this.getMap().keySet().contains(theSubVariable.toUpperCase()))) {
                        this.getMap().put(theSubVariable.toUpperCase(),""); //$NON-NLS-1$
                    }
                }
            }
        }
    }
    
    /**
     * This is for col cmd old_val and Works similar to col command new_val
     * except when used in tti and bti commands
     * 
     * Bug 25030438: Making sure to get the valid data type for the column.
     *  
     * @param columns
     * @param values
     */
    public void updateOVColumn(String[] columns, String[] values, String[] types) {
        Map <String,String> cMap = this.getOVColumnMap();
        boolean valuesIsNull = (values == null);
        if ((cMap.size() == 0)) {
            return;
        }
        for (int i=1; i<columns.length; i++) {
            String columnName = columns[i];
            String s = null;
            if (!valuesIsNull) {
                s=values[i];
            }
            if (s==null) {
                s=""; //$NON-NLS-1$
            }
            
            String dataType = null;
            if(types != null) {
            	dataType = types[i];
            }

            /**
             *  get what the column name matches if anything
             */
            String theSubVariable = cMap.get(columnName.toUpperCase());
            if (theSubVariable!=null) {
                if (!((valuesIsNull) && (this.getMap().keySet().contains(theSubVariable.toUpperCase())))){
                    this.getMap().put(theSubVariable.toUpperCase(),s);
                    if(dataType != null)
                        subvartypeMap.put(theSubVariable.toUpperCase(), dataType);
                }
                if (valuesIsNull) {//no rows if not set default
                    if (!(this.getMap().keySet().contains(theSubVariable.toUpperCase()))) {
                        this.getMap().put(theSubVariable.toUpperCase(),""); //$NON-NLS-1$
                    }
                }
            }
        }
    }

	public void enableDbmsOutput() {
		dbmsOutputEnable = true;
		
	}
  public void setdbmsEnable(boolean enabled){
	  dbmsOutputEnable = enabled;
  }
	public boolean isDbmsEnabled() {
		return dbmsOutputEnable;
	}

	public void clearDbmsFlags() {
		dbmsOutputEnable = false;
	}

	public void pushDbmsOutput(String s) {
		if(m_dbmsOutput == null){
			m_dbmsOutput = new StringBuffer();
		}
		m_dbmsOutput.append(s);
	}

	public String popDbmsOutput() {
		if(m_dbmsOutput ==null){
			return null;
		}
		String sTemp = m_dbmsOutput.toString();
		m_dbmsOutput = null;
		return sTemp;
	}

    public void writeDisconnectWarning() {
    	if (this.getProperty(ScriptRunnerContext.JLINE_SETTING)==null) {
    		//if you have a sub connection reset _connect_identifier to last set from connect
			this.getMap().put(ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER.toString(), 
			      (String)this.getProperty(ScriptRunnerContext.BASECONNECTIONID));
			this.putProperty(ScriptRunnerContext.BASECONNECTIONID, null);
			this.getMap().put(ScriptRunnerContext.SqlplusVariable._PRIVILEGE.toString(),null);//the rest will be nulled on setconnection
    		this.write(Messages.getString("WRITEDISCONNECTWARNING")+"\n"); //$NON-NLS-1$ //$NON-NLS-2$
    	}
    	try {
    		this.getOutputStream().flush();
    	} catch (IOException e) {
    		Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getStackTrace()[ 0 ].toString(), e);
    	}
    }
    
    public void closeCurrentConnection() {
    	if (this.getProperty(ScriptRunnerContext.JLINE_SETTING)==null) {
    		this.alwaysCloseCurrentConnection();
    	}
    }
    public void alwaysCloseCurrentConnection() {
        try{
        if(getBaseConnection()==null || getCurrentConnection() != getBaseConnection()){ //dont want to close the base connection by accident
            if (getCurrentConnection()!=null) {
                if(!getCurrentConnection().isClosed()){
                    getCurrentConnection().commit();
                    getCurrentConnection().close();
                }
            }
        }   
        } catch(SQLException e){
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
        }
    }

    public void consumerRuning(boolean isConsumerRunning) {
        m_isConsumerRunning = isConsumerRunning;
    }
    
    public boolean isConsumerRunning(){
    	//if the consumer is not running , give it a fraction of a second to startup. only do this once
    	//sometimes we need this little wait, because we want to write something onto the pipe before it has had a chance to setup
    	//for example @c:\test.sql 10 --pass in a paramter
    	//we want to write the contents of the file out to the result window before executing. 
    	//normally the time it takes to execute a command is enough to allow the pipe to be setup, but in this case we want to write before we execute
    	if(!m_isConsumerRunning) {
    		try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
			}
    	}
        return m_isConsumerRunning;
    }
    
    public void addSavepoint(Savepoint sp) {
    	if (savepoints == null) {
    		savepoints = new HashMap<String, Savepoint>(); 
    	}
    	try {
			savepoints.put(sp.getSavepointName(), sp);
		} catch (SQLException e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
		}
    }
    public HashMap<String, Savepoint> getSavePoints() {
    	return savepoints;
    }
    public void setSavepoints(HashMap<String, Savepoint> sps) {
    	savepoints = sps;
    }
    public Savepoint getSavepoint(String name) {
    	if (savepoints != null) {
    		return savepoints.get(name);
    	}
    	return null;
    }
    
    public HashMap<String, ArrayList<String>> getStoredFormatCmds() {
    	return m_formatCmdList;
    }
    
    public void setStoredFormatCmds(HashMap<String, ArrayList<String>> cmds) {
    	m_formatCmdList = cmds;
    }
    
    public LinkedHashMap<String, ArrayList<String>> getStoredBreakCmds() {	
    	return m_breakCmdList;
    }
    
    public void setStoredBreakCmds(LinkedHashMap<String, ArrayList<String>> cmds) {
    	m_breakCmdList = cmds;
    }
    
    public TreeMap<String, ArrayList<TreeMap<String, HashMap<String, String>>>> getStoredComputeCmds() {	
        return m_computeCmdList;
    }
    
    public void setStoredComputeCmds(TreeMap<String, ArrayList<TreeMap<String, HashMap<String, String>>>> cmds) {
        m_computeCmdList = cmds;
    }
    
    public String getTTitleCmd() {
    	return m_tTitleCmd;
    }
    
    public void setTTitleCmd(String topTitle) {
    	m_tTitleCmd = topTitle;
    }
    
    public String getBTitleCmd() {
    	return m_bTitleCmd;
    }
    
    public void setBTitleCmd(String bottomTitle) {
    	m_bTitleCmd = bottomTitle;
    }
    
    public boolean getBTitleFlag() {
    	return m_bTitleFlag;
    }
    
    public void setBTitleFlag(boolean flag) {
    	m_bTitleFlag = flag;
    }
    
    public void setTTitle(TopTitle titleObj) {
    	m_tTitle = titleObj;
    }
    
    public TopTitle getTTitle() {
    	return m_tTitle;
    }
    
    public void setBTitle(BottomTitle titleObj) {
    	m_bTitle = titleObj;
    }
    
    public BottomTitle getBTitle() {
    	return m_bTitle;
    }
    
    public void setTTitleFlag(boolean flag) {
    	m_tTitleFlag = flag;
    }
    
    public boolean getTTitleFlag() {
    	return m_tTitleFlag;
    }
    
    public void setPromptedFieldProvider(IGetPromptedFieldProvider provider) {
        m_getPromptedFieldProvider = provider;
    }
    
    public IGetPromptedFieldProvider getPromptedFieldProvider() {
        return  m_getPromptedFieldProvider;
    }
    public void setSpoolFilterProvider(IGetSpoolFilterProvider provider) {
        m_getSpoolFilterProvider = provider;
    }
    
    public IGetSpoolFilterProvider getSpoolFilterProvider() {
        return  m_getSpoolFilterProvider;
    }
    public void setSubstitutionFieldProvider(IGetPromptedFieldProvider provider) {
        m_getSubstitutionFieldProvider = provider;
    }
    
    public IGetPromptedFieldProvider getSubstitutionFieldProvider() {
        return  m_getSubstitutionFieldProvider;
    }
    public void setPasswordFieldsProvider(IGetPromptedPasswordFieldsProvider provider) {
        m_getPasswordFieldsProvider = provider;
    }
    
    public IGetPromptedPasswordFieldsProvider getPasswordFieldsProvider() {
        return  m_getPasswordFieldsProvider;
    }
    public void setConnectFieldsProvider(IGetPromptedConnectFieldsProvider provider) {
        m_getConnectFieldsProvider = provider;
    }
    public IGetPromptedConnectFieldsProvider getConnectFieldsProvider() {
        return  m_getConnectFieldsProvider;
    }
    public void setHandlePauseProvider(IGetHandlePauseProvider provider) {
        m_getHandlePauseProvider = provider;
    }
    public IGetHandlePauseProvider getHandlePauseProvider() {
        return  m_getHandlePauseProvider;
    }
    public void setHandleSetPauseProvider(IGetHandlePauseProvider provider) {
        m_getHandleSetPauseProvider = provider;
    }
    public IGetHandlePauseProvider getHandleSetPauseProvider() {
        return  m_getHandleSetPauseProvider;
    }
    public void setSourceRef(String base) {
        if ((base==null) || (base.equals(""))) {  //$NON-NLS-1$
            this.putProperty(this.SETSOURCEREF,"");  //$NON-NLS-1$
            return;
        }
        int frontSlash = base.indexOf("/");  //$NON-NLS-1$
        int backSlash = base.indexOf("\\");  //$NON-NLS-1$
        if (((frontSlash==-1)&&(backSlash==-1))||(base.endsWith("/"))||(base.endsWith("\\"))) {  //$NON-NLS-1$  //$NON-NLS-2$
            this.putProperty(ScriptRunnerContext.SETSOURCEREF,"");  //$NON-NLS-1$
            return;
        }
        String lower=base.toLowerCase(Locale.US);
        if (FileUtils.startsWithHttpOrFtp(lower)){ //include ...file:
            this.putProperty(ScriptRunnerContext.SETSOURCEREF,base);
        } else {
            //file description
            if (File.separator.equals("/")) {  //$NON-NLS-1$
                base=base.replace("\\","/");  //$NON-NLS-1$  //$NON-NLS-2$
            } else {
                if (!(base.startsWith("//"))){//c:...  //$NON-NLS-1$
                    base=base.replace("/", "\\");  //$NON-NLS-1$ //$NON-NLS-2$
                }
            }
            this.putProperty(ScriptRunnerContext.SETSOURCEREF,base);
        }
    }
    /**
     * Choose between with or without file reference
     * LINE_COMMAND_ERR = \nError starting at line {0} in command:\n{1}\nError report:\n{2}
     *
     * LINE_COMMAND_FILE_ERR = \nError starting at line {0} File:{1} in command:\n{2}\nError report:\n{3}
     */
    public static String lineErr(Object startLine, Object origSQL, Object errorMsg, ScriptRunnerContext src) {
      String retVal=""; //$NON-NLS-1$
      String sourceRef=""; //$NON-NLS-1$
      if (src!=null) {
    	  /* sourceRef could be file:/the%20space */
          sourceRef=(String) src.getProperty(SETSOURCEREF);
          sourceRef=FileUtils.decodeIfFile(sourceRef);
      }
      //add special adhocsql json to the output
      if(src.getOutputStream() instanceof JSONWrapBufferedOutputStream){
      	Integer sqlcode = (Integer)src.getProperty(ScriptRunnerContext.ERR_SQLCODE);
      	String jdbcErrorMessage = (String) src.getProperty(ERR_MESSAGE_SQLCODE);
      	if(jdbcErrorMessage != null) {
      		jdbcErrorMessage =  JSONWrapBufferedOutputStream.quote(jdbcErrorMessage.trim(), true);
      	}
      	jdbcErrorMessage = jdbcErrorMessage ==null?"\"unknown\"":jdbcErrorMessage; //$NON-NLS-1$
      	if(sqlcode== null){
      		sqlcode = 0;
      	}
      	String errorInfo = MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.JSON_COMMAND_ERR), new Object[] {0+sqlcode,startLine,0,jdbcErrorMessage});
      	((JSONWrapBufferedOutputStream)src.getOutputStream()).addStmtInfo(errorInfo);
      } 
      if (src.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null &&Boolean.parseBoolean(src.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
         //if in classic we do not want file/line information. additional classic test below redundant
         retVal=errorMsg.toString();
      }
      else {
          if ((sourceRef==null)||(sourceRef.equals(""))) { //$NON-NLS-1$
              if (!(src.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null &&Boolean.parseBoolean(src.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString()))) {
                  retVal=MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.LINE_COMMAND_ERR), new Object[] {startLine, origSQL, errorMsg});  
              } else {
                  retVal =  MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.LINE_FILE_COMMAND_ERR_CLASSIC),
                      new Object[] {startLine,errorMsg});
              }
          } else {
              retVal=MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.LINE_FILE_COMMAND_ERR), new Object[] {startLine, sourceRef, origSQL, errorMsg});
          }
      }
      return retVal;
  }
    /**
     * Choose between with or without file reference.
     */
    public static String lineColErr(Object startLine, Object origSQL, Object endLine, Object startCol, Object errorMsg, ScriptRunnerContext src) {
      String retVal=""; //$NON-NLS-1$
      String sourceRef=""; //$NON-NLS-1$
      if (src!=null) {
          sourceRef=(String) src.getProperty(SETSOURCEREF);
      }
      
      //add special adhocsql json to the output
      if(src.getOutputStream() instanceof JSONWrapBufferedOutputStream){
        String jdbcErrorMessage = (String) src.getProperty(ERR_MESSAGE_SQLCODE);
        jdbcErrorMessage = jdbcErrorMessage ==null?"\"unknown\"":"\""+jdbcErrorMessage.trim()+"\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        jdbcErrorMessage=JSONWrapBufferedOutputStream.quote(jdbcErrorMessage,true); 
        System.out.println("jdbcErrorMessage:"+jdbcErrorMessage); //$NON-NLS-1$
      	Integer sqlcode =(Integer)src.getProperty(ScriptRunnerContext.ERR_SQLCODE);
      	String errorInfo = MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.JSON_COMMAND_ERR), new Object[] {0+sqlcode,startLine,0,jdbcErrorMessage});
      	((JSONWrapBufferedOutputStream)src.getOutputStream()).addStmtInfo(errorInfo);
      } 
      
      if ((sourceRef==null)||(sourceRef.equals(""))) { //$NON-NLS-1$
          retVal=MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.LINE_COL_COMMAND_ERR), new Object[] {startLine, origSQL, endLine, startCol, errorMsg});
      } else {
          retVal=MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.LINE_COL_FILE_COMMAND_ERR), new Object[] {startLine,origSQL,endLine,startCol,sourceRef,errorMsg});
      }
      return retVal;
  }
    public String getSourceRef() {
        return (String) this.getProperty(ScriptRunnerContext.SETSOURCEREF);
    }
	public ScriptRunnerContext clone() {
			ScriptRunnerContext clone = new ScriptRunnerContext();
			clone.putProperty(IGNORECOLUMNSETTINGS, this.getProperty(IGNORECOLUMNSETTINGS));
			clone.set_autoprint(get_autoprint());
			clone.setTTitleFlag(getTTitleFlag());
			clone.setBTitleFlag(getBTitleFlag());
			clone.setBTitle(getBTitle());
			clone.setTTitle(getTTitle());
			clone.setBTitleCmd(getBTitleCmd());
			clone.setTTitleCmd(getTTitleCmd());
			clone.setStoredFormatCmds(getStoredFormatCmds());
			clone.setSavepoints(getSavePoints());
			clone.consumerRuning(false); //This is a clone, so nothing will be.
			clone.setdbmsEnable(isDbmsEnabled());
			clone.setEncoding(getEncoding());
			clone.setCurrentConnection(getCurrentConnection());
			clone.set_autoprint(get_autoprint());
			clone.setVarMap(getVarMap());
			clone.setBatchVarMap(getBatchVarMap());
			clone.setVerify(isVerifyOn());
			clone.setEcho(isEchoOn());
			clone.setFeedback(getFeedback());
			clone.setSupressOutput(isOutputSupressed());
			clone.setProperties(getProperties());
			clone.setLastUrl(getLastUrl());
			clone.setTerminateChar(getTerminateChar());
			clone.setSubstitutionTerminateChar(getSubstitutionTerminateChar());
			clone.setEscapeChar(getEscapeChar());
			clone.setScanOn(getScanOn());
			clone.setSubstitutionChar(getSubstitutionChar());
			clone.setSubstitutionOn(getSubstitutionOn());
			clone.setMap(getMap());
			clone.setColumnMap(getColumnMap());
			clone.setOVColumnMap(getOVColumnMap());
			clone.setSqlError(getSqlError());
			clone.setOsError(getOsError());
			clone.setCloseConnection(getCloseConnection());
			clone.setEscape(getEscape());
			clone.setReturnExited(getReturnExited());
			clone.setBaseConnection(getBaseConnection());
			clone.setNLSMap(getNLSMap());
			clone.setSourceRef(getSourceRef());
			clone.setPromptedFieldProvider(getPromptedFieldProvider());
			clone.setSubstitutionFieldProvider(getSubstitutionFieldProvider());
			clone.setPasswordFieldsProvider(getPasswordFieldsProvider());
			clone.setHandlePauseProvider(getHandlePauseProvider());
			clone.setHandleSetPauseProvider(getHandleSetPauseProvider());
			clone.setSQLPlusFormatter(getSQLPlusFormatter());
			clone.setSQLPlusCodingStyleOptions(getSQLPlusCodingStyleOptions());
			return clone;
	}

	/**
	 * getSettingsDir
	 * @return ScriptRunnerContext
	 */
    public String getSettingsDir() {
       String base = System.getProperty("ide.pref.dir"); //$NON-NLS-1$
       if (base==null) {
           base = System.getProperty("ide.base.dir"); //$NON-NLS-1$
       }
       if (base==null){
           base = System.getProperty("user.home")+ File.separator + ".sqldeveloper"; //$NON-NLS-1$ //$NON-NLS-2$
       }
       try {
          if (!new File(base).exists()) {
              new File(base).mkdir();
          }
       } catch (Exception e) {
         Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
       }
       return base;
        
    }

	/**
	 * getExceptionHandler
	 * @return ScriptRunnerContext
	 */
    public IExceptionHandler getExceptionHandler() {
	    // TODO Need interface here to provide exception handler. This will push out to 
    	// specific worksheet or cmd line.
	    return null;
    }

	/**
	 * getStringTerminator
	 * @return ScriptRunnerContext
	 */
    public String getStringTerminator() {
       //running the ScriptRunner standalone the Ide is not initialized, so take the default
       return System.getProperty("line.separator"); //$NON-NLS-1$

    }


	/**
	 * hasNode
	 * @return ScriptRunnerContext
	 */
    public boolean hasNode() {
	    if (node!=null) 
	    	return true;
	    else 
	    	return false;
    }
    public Object getNode() {
    	return node;
    }


    public enum SqlplusVariable {
        _DATE,
        _CONNECT_IDENTIFIER,
        _USER,
        _PRIVILEGE,
        _SQLPLUS_RELEASE,
        _EDITOR,
        _O_VERSION,
        _O_RELEASE,
        _PWD
    }
	/**
	 * setPrompt
	 * @param string ScriptRunnerContext
	 */
    public void setPrompt(String string) {
	    _sqlPrompt = string;
    }


	/**
	 * getPrompt
	 * @return ScriptRunnerContext
	 */
    public String getPrompt() {
        //For prompts we dont need quotes so nuke em
        //notice " might really mean quote me do not expand me - ignore for now "_DATE="_DATE> -> _DATE=16/07/2014> or something like that
        final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss "); //$NON-NLS-1$
        Calendar now = Calendar.getInstance();
    	boolean addTime = getProperty(SQLPROMPTTIME).equals("ON"); //$NON-NLS-1$
    	String prompt = doAllReplaceSqlplusVar(_sqlPrompt).replaceAll("'", "").replaceAll("\"", "");  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$ 
	    return addTime?dateFormat.format(now.getTime()) + prompt : prompt;
    }

    /**
     * getDate: Gets the current date.
     * @param source
     * @return
     */
    public String getDate(String source) {
    	return doPromptReplaceSqlplusVar(source,ScriptRunnerContext.SqlplusVariable._DATE);
    }

    public String getPrivilege(String source) {
        return doPromptReplaceSqlplusVar(source,ScriptRunnerContext.SqlplusVariable._PRIVILEGE);
    }

    public String getConnectionID(String source) {
        return doPromptReplaceSqlplusVar(source,ScriptRunnerContext.SqlplusVariable._CONNECT_IDENTIFIER);
    }
    
    public String getRawPrompt() {
        return _sqlPrompt;
    }

    String doAllReplaceSqlplusVar(String source) {
        String interrim=source;
        for (SqlplusVariable current: SqlplusVariable.values()) {
            interrim=doPromptReplaceSqlplusVar(interrim,current); 
        }
        //All set sqlplus ones are now done, do the rest too.
       Set<String> vars = getMap().keySet();
       for (String s :vars) {
    	   if (trimQuotes(interrim).toLowerCase().trim().equals(s.toLowerCase().trim())) {
    		   try {
    		   interrim = Pattern.compile(s, Pattern.CASE_INSENSITIVE|Pattern.LITERAL).matcher(interrim).replaceAll(getMap().get(s));
    		   } catch (NullPointerException e) {
    			   //eat me.
    		   }
    	   }
       }
       
        return interrim;
    }
    
    private String trimQuotes(String interrim) {
		if(interrim ==null){
			return null;
		}else if (interrim.trim().startsWith("\"") && interrim.trim().endsWith("\"")){ //$NON-NLS-1$ //$NON-NLS-2$
			return interrim.trim().substring(1,interrim.trim().length()-1);
		} else {
			return interrim;
		}
	}
    
    void doAllNullSqlplusVar() {
        for (SqlplusVariable current: SqlplusVariable.values()) {
            if ((!current.equals(SqlplusVariable._PRIVILEGE))
            		&&(!current.equals(SqlplusVariable._CONNECT_IDENTIFIER))
            		&&(!current.equals(SqlplusVariable._EDITOR))
            		&&(!current.equals(SqlplusVariable._PWD))) {
                this.getMap().put(current.toString(),null);
            }
        }
    }
    
    public String doPromptReplaceSqlplusVar(String source, SqlplusVariable current) {
    	return doPromptReplaceSqlplusVar(this.getCurrentConnection(), source, current);
    }
    //need to set before we set official connection e.g. before login,.sql
    String doPromptReplaceSqlplusVar(Connection preLoginConn, String source, SqlplusVariable current) {
        if ((source.toUpperCase().indexOf(current.toString())!=-1)) {
            if (!((preLoginConn!=null)||(SqlplusVariable._SQLPLUS_RELEASE.equals(current)
            		||SqlplusVariable._EDITOR.equals(current)))) {
            	String value="";
                //sqlplus release is the only non database dependant one - _Date should be client side (if it is worth changing)
            	switch (current) {
            	case _DATE:
            		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy"); //$NON-NLS-1$
					value = sdf.format(new Date()).toUpperCase();
	            	pushValtoMap(SqlplusVariable._DATE,value);

					break;
            	case _PWD:
                   
                    try {
                    	String x = getMap().get(current.toString());
                        value = Paths.get("").toAbsolutePath().toString();
                    	pushValtoMap(SqlplusVariable._PWD,x!=null?x:value);
                    } catch (Exception e) {
                    	Logger.getLogger(getClass().getName()).log(Level.WARNING,e.getStackTrace()[0].toString(),e);
                    } 
                   
                    break;
					default:
		            	value = "";
            	}
           		value = caseInsensitiveReplace(source,current.toString(),value);

                return value;
            }
            if (((preLoginConn==null)&&(SqlplusVariable._SQLPLUS_RELEASE.equals(current)))||
                     (ConnectionResolver.isOracle(preLoginConn))) {
                String value=this.getMap().get(current.toString());
                //calculate - might want to do _DATE separately as it is (more) dynamic
                switch (current) {
                    case _DATE:
                        //should really do this client side
                        if (preLoginConn==null) {
                            value=SqlplusVariable._DATE.toString();
                        } else {
    						try {
								if (preLoginConn!=null && !preLoginConn.isClosed() ) {
								value=DBUtil.getInstance(preLoginConn)
								    .executeOracleReturnOneCol("select TO_CHAR(cast(current_timestamp as  date)) from dual", new HashMap<String, Object>()); //$NON-NLS-1$
								} else {
									
									SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy"); //$NON-NLS-1$
									value = sdf.format(new Date()).toUpperCase();
								}
							} catch (SQLException e) {
								SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy"); //$NON-NLS-1$
								value = sdf.format(new Date()).toUpperCase();
							}  //$NON-NLS-1$
                        }
                        break;
                    case _USER:
                    	
                        if (value!=null){
                            break;
                        }
                        
					try {
						if (preLoginConn!=null && !preLoginConn.isClosed() )
                        value=DBUtil.getInstance(preLoginConn)
                                .executeOracleReturnOneCol("select USER from dual", new HashMap<String, Object>()); //$NON-NLS-1$
						else
							value=""; //$NON-NLS-1$
					} catch (SQLException e2) {
					}  //$NON-NLS-1$
                        
                        break;
                    case _EDITOR:
                        if (value!=null){
                            break;
                        }
                        break;
                    case _PRIVILEGE:
                        //null = "UNKNOWN" "" = "UNKNOWN" "AS SYSDBA" = "AS SYSDBA" "NOTSYSDBA" = ""
                        if ((value==null)
                                ||(value.equals(""))  //$NON-NLS-1$
                                ||(this.getProperty(ScriptRunnerContext.JLINE_SETTING)==null)){
                            //would be nice if this could be used outside sqlplus.sh
                            value="UNKNOWN";  //$NON-NLS-1$
                            break;
                        }
                        if (value.equals(ScriptRunnerContext.NOTSYSDBA)) {
                            value="";  //$NON-NLS-1$
                            break;
                        } 
                        break;
                    case _CONNECT_IDENTIFIER:
                        if ((value!=null)&&(value!="")){ //$NON-NLS-1$
                            break;
                        }
                        if (LockManager.lock(preLoginConn)) {
                            try {
                                //in sqlplus.runconnection we use connection string if less than 80 chars.
                                //added check for service name - if sys$ (i.e. probably beq/oracle_sid) go to dbname/pdbname
                                //wants the connection name (tnsentry/sqldev handle) closest I would say is con_name and if not set dbname
                                //note bad outcome if you are dealing with lots of container /root databases.
                                Statement stmt=null;
                                ResultSet rs=null;
                                value = ""; //$NON-NLS-1$
                                boolean gotValue=false;
                                try {
                                    stmt=preLoginConn.createStatement();
                                    rs=stmt.executeQuery("select sys_context('userenv','service_name') from dual"); //$NON-NLS-1$
                                    if (rs.next()) {
                                        value=rs.getString(1);
                                        //sqlplus does not report the 'global' yourdomain bit
                                        if ((value!=null)&&(value.indexOf(".")!=-1)) { //$NON-NLS-1$
                                            value=value.substring(0,value.indexOf(".")); //$NON-NLS-1$
                                        }
                                        //SYS$USERS - common default service if sid is set rather than service name
                                        if ((value!=null)&&(!value.equals("")&&(!value.toLowerCase().startsWith("sys$")))) { //$NON-NLS-1$  //$NON-NLS-2$
                                            gotValue=true;
                                        }
                                    }
                                } catch (SQLException e) {
                                	//If the database is down, we're not going to have anything to set.
                                	if (!e.getMessage().contains("ORA-01034")) { //$NON-NLS-1$
                                     Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                                	}
                                } finally {
                                    if (rs!=null) {
                                        try {
                                            rs.close();
                                        } catch (SQLException e) {
                                            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                                        }
                                    }
                                    if (stmt!=null) {
                                        try {
                                            stmt.close();
                                        } catch (SQLException e) {
                                            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                                        }
                                    }
                                }
                                if (!gotValue) {
                                    try {
                                        stmt=preLoginConn.createStatement();
                                        rs=stmt.executeQuery("select sys_context('userenv','con_name') from dual"); //$NON-NLS-1$
                                        if (rs.next()) {
                                            value=rs.getString(1);
                                            if ((value!=null)&&(!value.equals(""))) { //$NON-NLS-1$
                                                gotValue=true;
                                            }
                                        }
                                    } catch (SQLException e) {
                                        if(e.getErrorCode()!=2003){//not 12c 
                                            //do not print error if its not a 12c.
                                            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                                        }
                                    } finally {
                                        if (rs!=null) {
                                            try {
                                                rs.close();
                                            } catch (SQLException e) {
                                                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                                            }
                                        }
                                        if (stmt!=null) {
                                            try {
                                                stmt.close();
                                            } catch (SQLException e) {
                                                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                                            }
                                        }
                                    }
                                }
                                if (!gotValue) {
                                    try {
                                        stmt=preLoginConn.createStatement();
                                        rs=stmt.executeQuery("select sys_context('userenv','db_name') from dual"); //$NON-NLS-1$
                                        if (rs.next()) {
                                            value=rs.getString(1);
                                            if ((value!=null)&&(!value.equals(""))) { //$NON-NLS-1$
                                                gotValue=true;
                                            }
                                        }
                                    } finally {
                                        if (rs!=null) {
                                            try {
                                                rs.close();
                                            } catch (SQLException e) {
                                                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                                            }
                                        }
                                        if (stmt!=null) {
                                            try {
                                                stmt.close();
                                            } catch (Exception e) {
                                                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                                            }
                                        }
                                    }
                                }
                            } catch (SQLException e1) {
                                Logger.getLogger(getClass().getName()).log(Level.WARNING, e1.getStackTrace()[ 0 ].toString(), e1);
                            } finally {
                                LockManager.unlock(preLoginConn);
                            }
                        }
                        if (value==null) {
                            value=""; //$NON-NLS-1$
                        }
                        break;
                    case _O_RELEASE:
                        if (value!=null){
                            break;
                        }
                        if (value==null) {
                            value=""; //$NON-NLS-1$
                        }
                        if (LockManager.lock(preLoginConn)) {
                            try {
                                int major=preLoginConn.getMetaData().getDatabaseMajorVersion();
                                int minor=preLoginConn.getMetaData().getDatabaseMinorVersion();
                                String majmin=""; //$NON-NLS-1$
                                if (major>9) {
                                    majmin=""+major; //$NON-NLS-1$
                                } else {
                                    majmin="0"+major; //$NON-NLS-1$
                                }
                                if (minor>9) {
                                    majmin=majmin+minor;
                                } else {
                                    majmin=majmin+"0"+minor; //$NON-NLS-1$
                                }
                                value=majmin+"000000"; //$NON-NLS-1$
                            } catch (SQLException e1) {
                                Logger.getLogger(getClass().getName()).log(Level.WARNING, e1.getStackTrace()[ 0 ].toString(), e1);
                            } finally {
                                LockManager.unlock(preLoginConn);
                            }
                        }
                        break;
                    case _SQLPLUS_RELEASE:
                        if (value!=null){
                            break;
                        }
                        int[] numbs=getCmdlineVersion().toIntArray();
                        int vals=1;
                        String majmin=""; //$NON-NLS-1$
                        for (Integer i:numbs) {
                            if (vals>6) {
                                break;
                            }
                            if (i==null) {
                                break;
                            }
                            if (i>9) {
                                majmin=majmin+""+i; //$NON-NLS-1$
                            } else {
                                majmin=majmin+"0"+i; //$NON-NLS-1$
                            }
                            vals++;
                        }
                        for (int zeros=vals;zeros<6;zeros++) {
                            majmin+="00"; //$NON-NLS-1$
                        }
                        value=majmin;
                        break;
                    case _O_VERSION:
                        if (value!=null){
                            break;
                        }
                    try {
                        Boolean prelim=(Boolean)(this.getProperty(ScriptRunnerContext.PRELIMAUTH));
                        if ((prelim!=null)&&(prelim.equals(Boolean.TRUE))) {
                            if (preLoginConn!=null && !preLoginConn.isClosed() )
                                value=DBUtil.getInstance(preLoginConn)
                                .executeOracleReturnOneCol("select * from v$version where upper(banner) like upper('Oracle Database%')", new HashMap<String, Object>()); //$NON-NLS-1$
                            else 
                                value=""; //$NON-NLS-1$
                        } else {
                            if (preLoginConn!=null && !preLoginConn.isClosed() ) {
                                if (LockManager.lock(preLoginConn)) {
                                    try {
                                        DatabaseMetaData dBMD=preLoginConn.getMetaData();
                                        if (dBMD!=null) {
                                            value=dBMD.getDatabaseProductVersion();
                                            if (value==null) {
                                                value=""; //$NON-NLS-1$
                                            }
                                        }
                                        
                                    } finally {
                                        LockManager.unlock(preLoginConn);
                                    }
                                }
                            }else 
                                value=""; //$NON-NLS-1$
                        }
                    } catch (SQLException e) {
                    }  //$NON-NLS-1$
                        break;
                    case _PWD:
                        if (value !=null) {
                            break;
                        }
                        value = "UNKNOWN"; //$NON-NLS-1$
                        Throwable alle=null;
                        try {
                        	String x = getMap().get(SqlplusVariable._PWD.toString());
                        	String path = ""; //$NON-NLS-1$
                            Path currentRelativePath = Paths.get(path); //$NON-NLS-1$
                            value = currentRelativePath.toAbsolutePath().toString();
                        } catch (IOError e) {
                            alle=e;
                        } catch (InvalidPathException e) {
                            alle=e;
                        }
                        if (alle!=null) {
                            Logger.getLogger(getClass().getName()).log(Level.WARNING,alle.getStackTrace()[0].toString(),alle);
                        }
                        break;
                }   
                if ((value!=null)&&(!current.equals(SqlplusVariable._DATE))
                        &&(!current.equals(SqlplusVariable._PRIVILEGE))){
                    this.getMap().put(current.toString(),value);
                }

                //value should not be null 
                if (value != null) {
                    source=caseInsensitiveReplace(source,current.toString(),value);//source.replaceAll("(?i)"+current.toString(),value);  //$NON-NLS-1$
                }
            }
        }
        return source;
    }
    private void pushValtoMap(SqlplusVariable variable, String value) {
		if (getMap().containsKey(variable.toString())) {
			getMap().put(variable.toString(), value);
		}
	}


	private String caseInsensitiveReplace(String source, String toReplace, String value) {
        int charInt = 0;
        int last =0;
        if ((source==null)||(source.equals(""))) { //$NON-NLS-1$
            return ""; //$NON-NLS-1$
        }
        if ((toReplace==null)||(toReplace.equals(""))) { //$NON-NLS-1$
            return source;
        }
        if (value==null) {
            value=""; //$NON-NLS-1$
        }
        String toReplaceUpper=toReplace.toUpperCase();
        String sourceUpper=source.toUpperCase();
        while ((charInt=sourceUpper.indexOf(toReplaceUpper,last))!=-1) {
            source=source.substring(0, charInt)+value+source.substring(charInt+toReplaceUpper.length());
            sourceUpper=source.toUpperCase();
            last=charInt+value.length();
        }
        return source;
    }
    
    public URL getLastDirNameURL() {
        return  _lastUrl;
    }

    public Version getCmdlineVersion() {
        return cmdlineVersion;
    }
    
    public void setVersion(Version version) {
        cmdlineVersion = version;
    }

/**
 * This will only be ever called from SQLcl  This means that the connection we make will 
 * feedback something with details of the connection.  This will be suppressed when -S 
 * is given
 */
	public void setSQLPlusCmdLineLogin() {
		getProperties().put(SQLPLUS_CMDLINE_LOGIN,"true"); //$NON-NLS-1$
	}
	public void setSQLPlusConsoleReader(IConsoleReader reader) {
		_sqlplusConsoleReader = reader;
	}
	public IConsoleReader getSQLPlusConsoleReader() {
		return _sqlplusConsoleReader;
	}
	public void setSQLPlusCodingStyleOptions(ICodingStyleSQLOptions sqlOptions) {
		_codingStyleSQLOptions = sqlOptions;
	}
	public ICodingStyleSQLOptions getSQLPlusCodingStyleOptions() {
		return _codingStyleSQLOptions;
	}
	
	public void setSQLPlusFormatter(ICoreFormatter coreFormatter) {
		_coreFormatter = coreFormatter;
	}
	public ICoreFormatter getSQLPlusFormatter() {
		return _coreFormatter;
	}
	
		public void setSQLPlusBuffer(IBuffer buffer) {
			_sqlplusBuffer = buffer;
		}
		public IBuffer getSQLPlusBuffer() {
			return _sqlplusBuffer;
		}


		public Connection cloneCLIConnection() throws SQLException{
		   if ( getProperty(CLI_CONN_URL) == null || getProperty(CLI_CONN_PROPS) == null ){
		     throw new RuntimeException("No CLI connection information"); //$NON-NLS-1$
		   }
		   return DriverManager.getConnection((String)getProperty(CLI_CONN_URL), (Properties)getProperty(CLI_CONN_PROPS));
		 }
		/**
		 * Assumption CD was valid Directory when set, and contains \ on windows. 
		 * Assumption fileFragment is not http or ftp or URL.
		 * Assumpution if file fragment is full path or cd not set - filefragment is returned.
		 * CD might be ;: separated use the first one. file fragment my start with / or c:
		 * @param fileFragment fragment of file cd is to be prepended to
		 * @return cd+filefragment
		 */
	public String prependCD(String fileFragment) {
		String retVal=fileFragment;
		String inputFile=fileFragment;
        boolean prependDirectory = false;
        if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
            if (!((inputFile.length() > 3) && (((inputFile.charAt(1) == ':') && ((inputFile.charAt(2) == '/') || (inputFile.charAt(2) == '\\'))) || ((inputFile.charAt(0) == '/')
                && (inputFile.charAt(2) == ':') && (inputFile.charAt(3) == '/'))))) { //$NON-NLS-1$
                    prependDirectory = true;
            }
        } else {
            if (!((inputFile.startsWith("/")))) { //$NON-NLS-1$
               prependDirectory = true;
            }
        }
		String changepwd=(String) this.getProperty(ScriptRunnerContext.CDPATH);
		if ((prependDirectory)&&(changepwd!=null)) {
			String[] stArray=changepwd.split(File.pathSeparator);
			if ((stArray!=null)&&(stArray.length>0)
					&&(stArray[0]!=null)&&(new File(stArray[0]).isDirectory())) {
				changepwd=stArray[0];
				if (changepwd.endsWith(File.separator)) {
					retVal=changepwd+retVal;
				} else {
					retVal=changepwd+File.separator+retVal;
				}
			}
		}
		return retVal;
	}

	public String save(String filename, int mode) {
		ArrayList<String> list = new ArrayList<String>();
		String message = ""; //$NON-NLS-1$
		int hasExt = filename.indexOf("."); //$NON-NLS-1$
		String newFilename = filename;
		BufferedWriter out = null;
		FileWriter fstream = null;
		if (hasExt == -1) {
			if (!getProperty(SUFFIX).equals("")) //$NON-NLS-1$
				newFilename = filename + "." + getProperty(SUFFIX); //$NON-NLS-1$
		}
		File file = new File(newFilename) ;
		try {
			switch (mode) {
			case 0: //UNKNOWN
				break ;
			case 1: //CREATE
				if (file.exists()) {
					message = MessageFormat.format(Messages.getString("ScriptRunnerContext.97"), new Object[] {filename}); //$NON-NLS-1$
					message += Messages.getString("ScriptRunnerContext.98"); //$NON-NLS-1$
					return message;
				} 
				fstream = new FileWriter(newFilename, false);
				break;
			case 2: //APPEND
				fstream = new FileWriter(newFilename, true);
				break;
			case 3://REPLACE
				if (file.exists()) file.delete();
				fstream = new FileWriter(newFilename, false);
				break;
			}
			if (mode!=0) {
				out = new BufferedWriter(fstream);
				Iterator<IStoreCommand> it = StoreRegistry.iterator();

				while (it.hasNext()) {
					IStoreCommand cmd = it.next();
					String s = cmd.getStoreCommand(this);
					if (s!=null) {
					list.add(s);
					} else { 
						System.out.println(cmd.getClass().getName()); 
					}
				}
				Collections.sort(list);
				Iterator<String> it2 = list.iterator();
				while (it2.hasNext()) 
				out.write(it2.next().trim()+"\n"); //$NON-NLS-1$
				out.flush();
			}
		} catch (Exception e) { e.printStackTrace();
				System.err.println("Error: " + e.getMessage()); //$NON-NLS-1$
		} finally {
			try {
				if (out != null) { 
					out.flush();
					out.close();
				}
				if (fstream !=null) fstream.close();
			} catch (IOException e) {
				System.err.println("Error: " + e.getMessage()); //$NON-NLS-1$
			}
			if (message.length()==0) {
				switch(mode) {
					case 0:
						return Messages.getString("ScriptRunnerContext.102") +  //$NON-NLS-1$
								Messages.getString("ScriptRunnerContext.103");  //$NON-NLS-1$
					case 1:
						return MessageFormat.format(Messages.getString("ScriptRunnerContext.104"), new Object[] {filename}); //$NON-NLS-1$
					case 2:
						return MessageFormat.format(Messages.getString("ScriptRunnerContext.105"), new Object[] {filename}); //$NON-NLS-1$
					case 3:
						return MessageFormat.format(Messages.getString("ScriptRunnerContext.106"), new Object[] {filename}); //$NON-NLS-1$
				}
			}
		}
	return message;	
	}
	 public boolean getTiming() {
	    	if (getProperty(SHOWTIMING)!=null) {
	    		return (boolean) getProperty(SHOWTIMING); 
	    	} else {
	    		return false;
	    	}
	    }
	 
//TODO Make this complete.
	private ArrayList<String> getContextCommands() {
		ArrayList<String> list = new ArrayList<String>();
		String[]  s = ShowRegistry.showAllStrings();
		int i = SQLStatementTypes.setCommands.size();
		list.add("set feedback OFF"); //$NON-NLS-1$
		list.add("set heading ON"); //$NON-NLS-1$
		return list;
	}


	public void setBasePrompt(String string) {
		m_basePrompt = string;
		
	}
	public String getBasePrompt() {
		return m_basePrompt;
	}

  boolean _interrupted = false;
	public boolean isInterrupted() {
		return _interrupted;
	}
	
	public void setInterrupted(boolean interrupted){
		 _interrupted = interrupted;
	}

	private  HashMap<String, String> _ddlOptions = new HashMap<String, String>();

	private void resetDDLOptions() {
		_ddlOptions.clear();
		 String[] TransformOptions = { "PRETTY", "SQLTERMINATOR", "CONSTRAINTS", "REF_CONSTRAINTS", "CONSTRAINTS_AS_ALTER",  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			   "OID",  "SIZE_BYTE_KEYWORD", "PARTITIONING", "SEGMENT_ATTRIBUTES", "STORAGE", "TABLESPACE", "SPECIFICATION",  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
			   "BODY", "FORCE", "INSERT", "DEFAULT", "INHERIT" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		 for (String s: TransformOptions) {
			 _ddlOptions.put(s, "ON"); //$NON-NLS-1$
		 }
		 _ddlOptions.put("DEFAULT", "OFF"); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public Map<String,String> getDDLOptions() {
		if  (_ddlOptions != null) {
			return _ddlOptions;
		} else
		return null;
	}

	public void setDDLOptions(String string, String onOff) {
		if (string.toUpperCase().equals("RESET")) { //$NON-NLS-1$
			if (onOff.toUpperCase().equals("ON")) //$NON-NLS-1$
			resetDDLOptions();
			return;
		} else if (_ddlOptions.keySet().contains(string)) {
			if(_ddlOptions.containsKey(string)) _ddlOptions.remove(string);
				_ddlOptions.put(string, onOff);
				boolean dFault = true;
				for (String option : _ddlOptions.keySet()) {
					if (!option.toUpperCase().equals("RESET")) { //$NON-NLS-1$
						if (_ddlOptions.get(option.toUpperCase()).equals("OFF")) { //$NON-NLS-1$
							dFault=false;
						}
					}
				}
				if(_ddlOptions.containsKey("DEFAULT")) _ddlOptions.remove("DEFAULT"); //$NON-NLS-1$ //$NON-NLS-2$
				_ddlOptions.put("DEFAULT", dFault?"ON":"OFF"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		} 
	}
	
	public void setJSONOutput(boolean jsonOutput) {
		_jsonOutput = jsonOutput;
	}
	
	public boolean isJSONOutput(){
		return _jsonOutput;
	}

	public void setJSONQueryRun(boolean jsonQuery) {
		_jsonQuery = jsonQuery;
	}
	
	public boolean isJsonQuery(){
		return _jsonQuery;
	}
	
	private HashMap<String, HashMap<String, HashMap<String, String>>> m_objectTypeAttributesMap = new HashMap<String, HashMap<String, HashMap<String, String>>>();
	private HashMap<String, String> m_AliasTypeAttributesMap = new HashMap<String, String>();
	private int unknownStatements=0;
	
	public static final String OBJECT_TYPE_ATTRIBUTE_ALIAS = "ALIAS"; //$NON-NLS-1$
	public static final String OBJECT_TYPE_ATTRIBUTE_FORMAT = "FORMAT"; //$NON-NLS-1$
	public static final String OBJECT_TYPE_ATTRIBUTE_LIKE = "LIKE"; //$NON-NLS-1$
	public static final String OBJECT_TYPE_ATTRIBUTE_ON_OFF = "ON_OFF"; //$NON-NLS-1$
	public static final String LOGIN_FILE_ON_CWD = "LOGIN_FILE_ON_CWD";//$NON-NLS-1$



	public HashMap<String, HashMap<String, HashMap<String, String>>> getObjectTypeAttributes() {
		return m_objectTypeAttributesMap;
	}
	
	public void setObjectTypeAttributes(HashMap<String, HashMap<String, HashMap<String, String>>> m_objectTypeAttributesMap) {
		this.m_objectTypeAttributesMap = m_objectTypeAttributesMap;
	}
	
	public HashMap<String, String> getAliasTypeAttributesMap() {
		return m_AliasTypeAttributesMap;
	}

	public void setAliasTypeAttributesMap(HashMap<String, String> m_AliasTypeAttributesMap) {
		this.m_AliasTypeAttributesMap = m_AliasTypeAttributesMap;
	}
	
	public boolean isSQLPlusClassic() {
		if (getProperty(SQLPLUS_CLASSIC_MODE)!=null && Boolean.parseBoolean(getProperty(SQLPLUS_CLASSIC_MODE).toString())) {
		  putProperty(SQLPLUS_CLASSIC_MODE,Boolean.TRUE);
			return true;
		} 
		return false;
	}
	public boolean isStdin() {
		if (getProperty(SQLCLI_STDIN)!=null && Boolean.parseBoolean(getProperty(SQLCLI_STDIN).toString())) {
      putProperty(SQLCLI_STDIN,Boolean.TRUE);

			return true;
		} 
		return false;
	}
	
	/*
	 * LRG/SRG tests only The following function cannot be used for production.
	 */
    public HashMap<String, ArrayList<String>> get121IdentViewColCmds() {
    	return m_121ViewColCmdList;
    }
    
	/*
	 * LRG/SRG tests only The following function cannot be used for production.
	 */
    public boolean is121LongIdentUsed() {
    	boolean is121longident = false;
    	Iterator<String> iter = m_121ViewColCmdList.keySet().iterator();
    	while (iter.hasNext()) {
    		String col = iter.next();
    		is121longident = m_121ViewColCmdList.get(col).contains("_12.1_longident"); //$NON-NLS-1$
    		if (is121longident)
    			break;
    	}
    	return is121longident;
    }


	public boolean isSQLPlusSilent() {
		if (getProperty(SILENT)!=null && Boolean.parseBoolean(getProperty(SILENT).toString())) {
			return true;
		}
		return false;
	}


	public boolean isLoginSQL() {
		if (Boolean.parseBoolean(getProperty(SQLCLI_GLOGIN_SQL).toString())||Boolean.parseBoolean(getProperty(SQLCLI_LOGIN_SQL).toString()))
			return true;
		return false;
	}

	/**
	 * When there are severalUnknown statements, a help command will be printed.
	 * This will increment the counter.
	 */
	public void addUnknown() {
		unknownStatements++;
	}

	/**
	 * Check if the max unknown statements is 4 or more.
	 * @return
	 */
	public boolean isMaxUnknown() {
		if (unknownStatements>=4) 
			return true;
		return false;
	}

	/**
	 * reset unknownstatrment counter after processing.
	 */
	public void resetUnknown() {
		unknownStatements=0;
	}

/**
 * In some places a scriptrunnercontext is needed by other closed classes.  it can be set here and returned staticlly where required
 * for specific changes.  For example, when DatatypeUtil formats a refcursor it uses a sqlpluscmdformatter which sets a base context.
 * This will allow it to pick up the latest settings like pagesize, linesize etc.
 * @return
 */
	public static ScriptRunnerContext getCurrentContext() {
		
		return _storedContext;
	}

public void errorLog(String script, String message, String sql) {
	boolean logging = (boolean) getProperty(ERROR_LOGGING); 	
		if (logging) {
			String unformattedInsertSQL="insert into {0} (USERNAME , TIMESTAMP ,SCRIPT , IDENTIFIER ,MESSAGE , STATEMENT) values (:SCHEMA, CAST(SYSDATE AS TIMESTAMP), :SCRIPT,:IDENTIFIER, :MESSAGE,:STATEMENT)";
			String errorlog = (String) getProperty(ERROR_LOGGING_TABLE);
			if (getProperty(ERROR_LOGGING_SCHEMA)!=null) {
				//The log is somewhere else, hence add the schema
				errorlog = MessageFormat.format("{0}.{1}", getProperty(ERROR_LOGGING_SCHEMA).toString().toUpperCase(), errorlog);
			}
			String insertSQL = MessageFormat.format(unformattedInsertSQL,errorlog);
			HashMap<String,String> binds = new HashMap<String,String>();
			try {
			String schema=getBaseConnection().getSchema();
				binds.put("SCHEMA", schema.toUpperCase());	
			} catch (SQLException e) {	}
			if (script!=null && script.length()>0) {
				Path p = Paths.get(script);
				script = p.getFileName().toString();
			}
			binds.put("SCRIPT", script); //Fixme
			String identifier = "";
			if (getProperty(ERROR_LOGGING_IDENTIFIER)!=null) {
				identifier=getProperty(ERROR_LOGGING_IDENTIFIER).toString();
			}
			binds.put("IDENTIFIER", identifier); //Fixme
			binds.put("MESSAGE", message);
			binds.put("STATEMENT", sql);
			DBUtil.getInstance(getCurrentConnection()).execute(insertSQL, binds); //
		}
	}


  public void setDMLResult(int[] nums) {
    this.dmlResult = nums;
  }

  public int[] getDMLResult() {
    return dmlResult;
  }
  
  public void setRestrictedLevel(final RunnerRestrictedLevel.Level restrictedLevel){
      this.restrictedLevel = restrictedLevel;
  }
  
  public RunnerRestrictedLevel.Level getRestrictedLevel(){
    return restrictedLevel;
  }
}
