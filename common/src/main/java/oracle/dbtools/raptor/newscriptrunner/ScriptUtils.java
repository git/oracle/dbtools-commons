/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowSqlcode;
import oracle.dbtools.raptor.query.Parser;
import oracle.dbtools.util.Service;
import oracle.jdbc.OracleResultSet;
import oracle.sql.Datum;

public class ScriptUtils {
	private static final String m_lineSeparator = System
			.getProperty("line.separator"); //$NON-NLS-1$
    protected static final Logger LOGGER = Logger.getLogger(ScriptUtils.class.getName());
    public static final int EXIT = 4;
    public static final int CONTINUE = 0;
    public static final int EXITMASK = 4;
    public static final int ACTIONMASK = 3;
    public static final int ACTIONNONE = 0;
    public static final int ACTIONCOMMIT = 1;
    public static final int ACTIONROLLBACK = 2;
    public static final String EXECUTEFAILED = "EXECUTEFAILED";
    /**
     * get arguments taking into account ' " and whitespace. examples used in testing:
     * 
     * testarg(" '' \"\" ",new String[] {"",""}); testarg(" ' ' \" \" ",new String[] {" "," "}); testarg("''\"\"",new String[] {"",""}); testarg("' '\" \"",new String[] {" "," "});
     * testarg(" one two ",new String[] {"one","two"}); testarg(" 'one' \"two\" ",new String[] {"one","two"}); testarg(" 'o ne' \"t wo\" ",new String[] {"o ne","t wo"});
     * testarg(" 'one'two ",new String[] {"one","two"}); testarg(" \"one\"two ",new String[] {"one","two"}); testarg(" one two",new String[] {"one","two"});
     * testarg(" one 'two'",new String[] {"one","two"}); testarg(" one \"two\"",new String[] {"one","two"}); testarg("one two",new String[] {"one","two"}); testarg("",null);
     * testarg(" ",null); testarg(null,null); //exception error out on //testarg("'one two",new String("one","two")); //testarg("\"one two",new String("one","two")); I have not
     * handled escapes for now. also handles "" is " in a "string", '' is ' in a 'string'
     * does not handle escapes and xx'xx = xx'xx i.e. not starting ending on quote - no quoted quotes or other special treatment.
     * returns null for no args.
     */
    public static String[] executeArgs(String a) throws ArgumentQuoteException {
        // whitspace separated arguments which may be enclosed in ' or " (in
        // which case they can contain whitespce.
        if (a == null) {
            return null;
        }
        if (a.replaceAll("^\\s+$", "").equals("")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            return null;
        }
        ArrayList<String> theArguments = new ArrayList<String>();
        a = " " + a; //$NON-NLS-1$
        Pattern nextBlankP = Pattern.compile("\\s"); //$NON-NLS-1$
        while (true) {
            Matcher m = nextBlankP.matcher(a);
            boolean retFind = m.find();
            if (!(retFind)) {
                // no active argument
                break;
            }
            a = a.substring(m.start()).trim();
            if (a.startsWith("'")) { //$NON-NLS-1$
                int nextq = 1;
                while (true) {
                    nextq = a.indexOf("'", nextq); //$NON-NLS-1$
                    if (nextq == -1) {
                        break;
                    }
                    if (a.substring(nextq).startsWith("''")) { //$NON-NLS-1$
                        nextq = nextq + 2;
                    } else {
                        break;
                    }
                }
                if (nextq == -1) {
                    throw new ArgumentQuoteException("'"); //$NON-NLS-1$
                }
                theArguments.add(a.substring(1, nextq).replaceAll("''", "'")); //$NON-NLS-1$ //$NON-NLS-2$
                if (a.length() > nextq + 1) {
                    a = " " + a.substring(nextq + 1); //$NON-NLS-1$
                } else {
                    break;
                }
            } else
                if (a.startsWith("\"")) { //$NON-NLS-1$
                    int nextq = 1;
                    while (true) {
                        nextq = a.indexOf("\"", nextq); //$NON-NLS-1$
                        if (nextq == -1) {
                            break;
                        }
                        if (a.substring(nextq).startsWith("\"\"")) { //$NON-NLS-1$
                            nextq = nextq + 2;
                        } else {
                            break;
                        }
                    }
                    if (nextq == -1) {
                        throw new ArgumentQuoteException("\""); //$NON-NLS-1$
                    }

                    theArguments.add(a.substring(1, nextq).replaceAll("\"\"", "\"")); //$NON-NLS-1$ //$NON-NLS-2$
                    if (a.length() > nextq + 1) {
                        a = " " + a.substring(nextq + 1); //$NON-NLS-1$
                    } else {
                        break;
                    }
                } else {// need to find whitspace not just space
                    m = nextBlankP.matcher(a);
                    retFind = m.find();
                    if (!(retFind)) {
                        // if the trimmed a equals nothing ignore.
                        // you get nothing through '' and "" nowhere else...
                        if (!(a.equals(""))) { //$NON-NLS-1$
                            theArguments.add(a);
                        }
                        // no active argument
                        break;// there were only non spaces left
                    }
                    int nexts = m.start();// is not going to start with ' '
                    theArguments.add(a.substring(0, nexts));
                    a = a.substring(nexts);
                }
            // handle state
        }
        if (theArguments.size() == 0) {
            return null;
        }
        return theArguments.toArray(new String[ theArguments.size() ]);
    }
    protected static void report(ScriptRunnerContext ctx, String s, BufferedOutputStream out) {
        if (out != null) {
            try {
                String modstr = s+"\n";
                if ((ctx!=null)&&(ctx.getFeedback()!=ScriptRunnerContext.FEEDBACK_OFF))
                	out.write((modstr).getBytes(ctx.getEncoding())); //$NON-NLS-1$
            	// Bug 19497212
            	// This is required to get the correct line number when the report is 
            	// printed using SQLPlus commands and is important especially for 
            	// SET EMBED ON command.
                if(ctx != null) {
                	Integer lnoval = (Integer) ctx.getProperty(ScriptRunnerContext.LNO);
                	if(lnoval.intValue() > 0) {
	                	int nlt = 0;
	                	StringTokenizer st = new StringTokenizer(modstr, "\n", true);
	                    while (st.hasMoreTokens()) {
	                        if(st.nextToken().equals("\n")) {
	                        	nlt++;
	                        }
	                    }
	                	int  lno = lnoval.intValue() + nlt;
	                	ctx.putProperty(ScriptRunnerContext.LNO, new Integer(lno));
                	}
                }
                out.flush();
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            }
        }
    }

    public static void doWhenever(ScriptRunnerContext src, ISQLCommand cmd,
	        Connection conn, boolean SqlError) {// rollback commit and or set
        cmd.setProperty(EXECUTEFAILED, EXECUTEFAILED);
        BufferedOutputStream out = src.getOutputStream();
        ;
        // exited
        int flags = 0;
        if (SqlError == true) {
            flags = src.getSqlError();
        } else {
            flags = src.getOsError();
        }
        int threeOptions = flags & ACTIONMASK;
        if (threeOptions == ACTIONROLLBACK) {
            if (conn != null) {
                try {
                    conn.rollback();
                    if (!src.isSQLPlusClassic())
                    	report(src, "Rollback", out); //$NON-NLS-1$
                } catch (SQLException e) {
                    report(src, ScriptRunnerDbArb.format(ScriptRunnerDbArb.ERROR_ON,
                            "Rollback"), out); //$NON-NLS-1$
                }
            }
        } else if (threeOptions == ACTIONCOMMIT) {
            if (conn != null) {
                try {
                    conn.commit();
                } catch (SQLException e) {
                    report(src,ScriptRunnerDbArb.format(ScriptRunnerDbArb.ERROR_ON,
                            "Commit"), out); //$NON-NLS-1$
                }
            }
        }
        // there is also an ACTIONNONE which is do nothing on continue
        if ((flags & EXITMASK) == EXIT) {
            src.setExited(true);
            if (src.getProperty(ScriptRunnerContext.EXIT_INT_WHENEVER)!=null) {
            	if ((int)src.getProperty(ScriptRunnerContext.EXIT_INT_WHENEVER)!=-1) {
            		Boolean wasSQLCODE=(Boolean) src.getProperty(ScriptRunnerContext.EXIT_INT_WHENEVER_WAS_SQLCODE);
            		if ((wasSQLCODE!=null)&&(wasSQLCODE.equals(Boolean.TRUE))) {
            			int parsedNum=ShowSqlcode.getCodeNum(src);
            			//0 = nothing to parse , -1 = parse error - passing these straight out
            			src.putProperty(ScriptRunnerContext.EXIT_INT, parsedNum);
            		} else {
        		        src.putProperty(ScriptRunnerContext.EXIT_INT, src.getProperty(ScriptRunnerContext.EXIT_INT_WHENEVER));
            		}
            	} else {
                	src.putProperty(ScriptRunnerContext.EXIT_INT, 1);
                	report(src, ScriptRunnerDbArb.get(ScriptRunnerDbArb.EXIT_JUNK2), out);
            	}
            } else { 
              src.putProperty(ScriptRunnerContext.EXIT_INT, 1);
            }
        }
    }

    public static String[] parseNameAndTypeUtil( String sql) {
        String owner=null;
        boolean wasAlter=false;
        boolean foundBody=false;
        String name=null;
        String type=null;
        String txt = sql.length()>1000 ? sql.substring(0,1000) : sql; // need only 1000 chars to figure out name and type
        List<LexerToken> src = LexerToken.parse(txt);
        if (src.size()>1){
            if ("alter".equalsIgnoreCase(src.get(0).content)) {
                wasAlter=true;
                boolean foundCompile=false;
                for (int i = 0; i < 20; i++) {
                    if (i+1>src.size()) {
                        break;
                    }
                    String thisToken=src.get(i).content;
                    if ("compile".equalsIgnoreCase(thisToken)) {  //$NON-NLS-1$
                        foundCompile=true;
                    }
                    if ("resolve".equalsIgnoreCase(thisToken)) {  //$NON-NLS-1$
                        foundCompile=true;
                    }
                    if ("body".equalsIgnoreCase(thisToken)) {  //$NON-NLS-1$
                        foundBody=true; //body comes at the end in alter package
                    }
                }
                if (!foundCompile){
                    return null;
                }
            }
        }
        for (int i = 0; i < 15; i++) {
            if (i>=src.size()){//src.size may be less than 15 could/should be part of the for declaration
                break;
            }
            LexerToken t = src.get(i);
            if (type == null && "DECLARE".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                break;//anonymous block
            else if (type == null && "BEGIN".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                break;//anonymous block
            else if( type == null && "PACKAGE".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                type = t.content.toUpperCase();
            else if( type == null && "PROCEDURE".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                type = t.content.toUpperCase();
            else if( type == null && "FUNCTION".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                type = t.content.toUpperCase();
            else if( type == null && "VIEW".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                type = t.content.toUpperCase();
            else if( type == null && "TRIGGER".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                type = t.content.toUpperCase();
            else if( type == null && "JAVA".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                type = t.content.toUpperCase();
            else if( type == null && "LIBRARY".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                type = t.content.toUpperCase();
            else if( type == null && "TYPE".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                type = t.content.toUpperCase();
            else if( type != null && "BODY".equalsIgnoreCase(t.content)) {//$NON-NLS-1$
                type += " " + t.content.toUpperCase(); //$NON-NLS-1$
                foundBody=false;//do not handle twice
            }
            else if( type != null && "SOURCE".equalsIgnoreCase(t.content)) //$NON-NLS-1$
                type += " " + t.content.toUpperCase(); //$NON-NLS-1$
            //"Java Class" not bugged asked for yet - note not clear how to get the class name.
            else if(( type != null && ((wasAlter) || !(type.startsWith("JAVA"))) && !"BODY".equalsIgnoreCase(t.content)  //$NON-NLS-1$  //$NON-NLS-2$
                    && name == null )||(type != null &&(!wasAlter)&& (type.startsWith("JAVA"))  //$NON-NLS-1$
                        && ((i>1)&&(src.get(i-1)!=null)&&(src.get(i-1).content!=null)
                                &&(src.get(i-1).content.equalsIgnoreCase("NAMED")))  //$NON-NLS-1$
                                && name == null)){ //$NON-NLS-1$
                if (type.equals("PACKAGE")&&(foundBody)) {   //$NON-NLS-1$
                    type ="PACKAGE BODY";   //$NON-NLS-1$
                }
                if (type.equals("TYPE")&&(foundBody)) {   //$NON-NLS-1$
                    type ="TYPE BODY";   //$NON-NLS-1$
                }
                name = Service.handleMixedCase(t.content);
                //so this can solve this bug but I should be able to fix it more 
                //generically basically any [].[].[] combination
                //should I assume user is dbo... basically fix this bug others can fly.
                if (name.equals("[")) { //$NON-NLS-1$
                    //this mean s we have a name like [database].[owner].[name]
                    //wobblies [my database]..[name] [own er].name [database].[owner].[na me]
                    //take here, or token following last dot to end or first space (not in [])
                    String localtxt = txt + " ";//put in an end stop
                    boolean inbracket = true;
                    int possibleStartName=t.begin+1;
                    int end = localtxt.length() - 1;
                    for (int token = t.begin + 1 ; token<1000 ; token++) {
                        if (token>=localtxt.length()-1) {//src.size may be less than 10 could/should be part of the for declaration
                            break;
                        }
                        String tlocal =localtxt.substring(token,token+1);
                        if (tlocal.matches("[ \t\r\n]") && (inbracket == false)) { //$NON-NLS-1$
                            end = token;
                            break;
                        }
                        if (tlocal.equals(".") && (inbracket == false)) { //$NON-NLS-1$
                            possibleStartName = token + 1;
                        }
                        if (tlocal.equals("[")) { //$NON-NLS-1$
                            inbracket=true;
                        } else if (tlocal.equals("]")) { //$NON-NLS-1$
                            inbracket=false;
                        }
                    }
                    //sanity checks size of last name whould be <100 characters and >0
                    //I am just churning out the name here less the [ ] in case it causes 
                    //a sqlexception elsewhere
                    if (((end - possibleStartName)<100) && ((end - possibleStartName)>0)) {
                        name=localtxt.substring(possibleStartName,end).replaceAll("\\[","").replaceAll("\\]",""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                    }
                }
                else if(src.size() > i+2){//then check the next 2 tokens to see if its the owner name
                    //Ex:  hr.tablename
                    LexerToken possibleDOTToken = src.get(i+1);
                    if( ".".equalsIgnoreCase(possibleDOTToken.content) ) { //$NON-NLS-1$
                        owner = name;
                        LexerToken objectNameToken = src.get(i+2);
                        if (!((owner.startsWith("\"") && owner.endsWith("\"")))) {
                            owner=owner.toUpperCase(Locale.US);
                        }
                        name = objectNameToken.content;
                        if (!((name.startsWith("\"") && name.endsWith("\"")))) {
                            name=name.toUpperCase(Locale.US);
                        }
                    }   
                }
                break;// stop looping looking for the object name as we have it
            }
        }
        if((name!=null) && (name.startsWith("\"") && name.endsWith("\"")) )//name may be set to null //$NON-NLS-1$ //$NON-NLS-2$
            name = name.substring(1, name.length()-1);
        //Warning to however uses this owner and type are stripped of " and or uppercased as required.
        return new String[]{owner,name,type};
    }
	public static String eatOneWord(String in) {
		// this is show - there are no spaces in the word or double quotes
		return (in.trim() + " ").replaceAll("^[^\\s]*\\s+", "").trim(); //$NON-NLS-1$ //$NON-NLS-2$  //$NON-NLS-3$
	}
	
	public static String stripFirstN(String in,int n, boolean continuation, boolean sqlplus) {
	    return stripFirstN(in, n, continuation, false, sqlplus);
	}
    public static String stripFirstN(String in,int n, boolean continuation, boolean nullOnUnmatched, boolean sqlplus) {
        boolean showUsage=false; 
        //Strip comments functionality is required for create or replace. in create or replace could simply stop at first ' or " to allow parsing up to possible double quoted name.
        String removeComments=in;
        if (in.length()>n) {
            removeComments=in.substring(0,n);
        }
        //need to coalesee continuation character
        if (continuation) {
            if (sqlplus) {
                removeComments=SQLPLUS.removeDashNewline(removeComments);
            } else {
                removeComments=checkforContinuationChars(removeComments);
            }
        } 
        //remove -- and /* comments
        int next=0;
        int nextDash=0;
        int nextMulti=0;
        while (next!=-1) {
            //not covered:
            //does not handle escape characters. [done in substitution before execution??] kind a funny - escape is only for substituion
            //stop on '' or "" strings asssume up to first ' or " is all that is required in several cases
            //   actually will fail on whenever oserror exit "something containing -- or /* " which does not make much sense but sqlplus takes in without error
            //Performance issue - stringbuffer vs string - assumption small command few if any comments 2index ofs =-1 -> no significant additional string operations.
            //   could do index of less.
            nextMulti=removeComments.indexOf("/*",next); //$NON-NLS-1$
            nextDash=removeComments.indexOf("--",next); //$NON-NLS-1$
            //redundant int nextQQuote=removeComments.indexOf("q'",next); //$NON-NLS-1$
            int nextQQuote=999999;
            int nextQuote=removeComments.indexOf("'",next); //$NON-NLS-1$
            int nextDQuote=removeComments.indexOf("\"",next); //$NON-NLS-1$
            if ((nextMulti==-1)&&(nextDash==-1)) {
                break;
            }
            int least=99999;
            if (nextMulti!=-1) {
                least=nextMulti;
            }
            if ((nextDash!=-1)&&(nextDash<least)) {
                least=nextDash;
            }
            if ((nextQQuote!=-1)&&(nextQQuote<least)) {
                least=nextQQuote;
            }
            if ((nextQuote!=-1)&&(nextQuote<least)) {
                least=nextQuote;
            }
            if ((nextDQuote!=-1)&&(nextDQuote<least)) {
                least=nextDQuote;
            }
            if (nextMulti!=-1&&(nextMulti==least)) {
                String before=removeComments;
                removeComments=removeComments.replaceFirst("(?s)/\\*.*?\\*/"," "); //$NON-NLS-1$ //$NON-NLS-2$
                if (removeComments.equals(before)||removeComments.indexOf("/*",next)==nextMulti) {
                    //unmatched/* stop.
                    if (nullOnUnmatched) {
                        return (null);
                    }
                    break;
                }
                next=nextMulti;
            } 
            if (nextDash!=-1&&(nextDash==least)) {
                String before=removeComments;
                removeComments=removeComments.replaceFirst("(?s)--[^\n]*"," "); //$NON-NLS-1$ //$NON-NLS-2$
                if (removeComments.equals(before)||removeComments.indexOf("--",next)==nextDash) {
                    //unmatched-- stop
                    if (nullOnUnmatched) {
                        return (null);
                    }
                    break;
                }
                next=nextDash;
            }
            if (least==nextQQuote||least==nextDQuote||least==nextQuote) {
                break;//assume interesting stuff is up to first ' or " or q'
            }
        }
        return removeComments.trim();//
	}
    /**
     * Adding support for continuation characters in sql statement. This should help old plus scripts run easily.
     * 
     * @param string
     * @return new string.
     */
    public static String checkforContinuationChars(final String currstmt) {
        boolean firstLine = true;
        //if (!(stmtProperties.getStmtType().equals(StmtType.G_C_SQLPLUS))) {
        //    return (currstmt);
        //}
        if (currstmt.contains("-")) { //$NON-NLS-1$
            String[] lines = currstmt.split("\n"); //$NON-NLS-1$
            StringBuffer newStatement = new StringBuffer(""); //$NON-NLS-1$
            String newStatementSt = null;
            if (lines.length!=0) {
                int lineNo=0;
                for (String line : lines) {
                    lineNo++;
                    String token = line;
                    StringBuffer newToken = new StringBuffer(""); //$NON-NLS-1$
                    // if token - whitespace ends with '-', then nuke it cos its a continuation char
                    if ((firstLine) && (token.indexOf("-")!=-1) && (token.replaceAll("\\s+$", "").endsWith("-")) && (!(token.replaceAll("^\\s+", "").startsWith("--")))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
                        CharacterIterator it = new StringCharacterIterator(token);
                        boolean found = false;
                        for (char ch = it.last(); ch != CharacterIterator.DONE; ch = it.previous()) {
                            if (ch == '-' && !found) {
                                newToken.insert(0,' ');
                                found = true;
                            } else {
                                newToken.insert(0,ch);
                            }
                        }
                        newStatement.append(newToken);
                    } else {
                        if (lineNo==1) {
                            //there were no continuation characters
                            return currstmt;
                        }
                        firstLine = false;
                        newToken = new StringBuffer(token);
                        newStatement.append(newToken).append(" \n"); //$NON-NLS-1$
                    }
                }
                newStatementSt = newStatement.toString();
            } else {
                newStatementSt = currstmt;
            }
            // remove possible \n newline
            if (newStatementSt.length() > 2) {
                if (newStatementSt.endsWith("\r\n")) { //$NON-NLS-1$
                    newStatementSt = newStatementSt.substring(0, newStatementSt.lastIndexOf('\r'));
                } else if (newStatementSt.endsWith("\n")) { //$NON-NLS-1$
                    newStatementSt = newStatementSt.substring(0, newStatementSt.lastIndexOf('\n'));
                }
            }
            // newStatement = newStatement.replaceAll("\r?\n$","");
            return newStatementSt;
        } else {
            return currstmt;
        }
    }
    /* QA imposed additional checks for non happy day scenarios */

	public static boolean newConnectionDetailsError(ConnectionDetails inDetails) {
        if (inDetails==null) {
            return true;
        }
        if ((inDetails.getAt()!=-1) && (inDetails.getConnectDB()==null || inDetails.getConnectDB().trim().equals(""))) {
            return true;//@ and blank connect string
        }
        Boolean[] boolArr= new Boolean[] {new Boolean(true),new Boolean(inDetails.getSlash()!=-1)};
        String[] argArr= new String[] {inDetails.getConnectName(), inDetails.getConnectPassword()};

        for (int count=0;count<2;count++){
        	if (boolArr[count].equals(new Boolean(true))) {
        		if ((argArr[count]!=null)&&
        				(argArr[count].trim().contains(" ")&& //$NON-NLS-1$
        						(!(argArr[count].length()>1&&argArr[count].startsWith("\"")&& //$NON-NLS-1$
        								argArr[count].endsWith("\""))))){ //$NON-NLS-1$
        			return true;// space and not double quoted
        		}
        	}
        }
        if (inDetails.getRoleBad()) {
        	return true;
        }
    	return false;
    }
	public static boolean isHttpCon(Connection conn, ScriptRunnerContext ctx) {
		//cheapst - ctx, expensice instanceof on conn (or conn.getutl - assume context is available for now
		Boolean ctxHttpCon=(Boolean) ctx.getProperty(ScriptRunnerContext.ISHTTPCON);
		return ((ctxHttpCon!=null)&&ctxHttpCon.equals(Boolean.TRUE));
	}
	public static void setHttpCon(Connection conn, ScriptRunnerContext ctx, boolean current) {
		//cheapst - ctx, expensice instanceof on conn (or conn.getutl - assume context is available for now
		if (ctx!=null) {
			ctx.putProperty(ScriptRunnerContext.ISHTTPCON, current);
		}
		//hopefully do not need conn (looks like i do a get url on connecting => acceptiple for now (more reads than writes)
	}

	public static int getLastEdition(String localSql) {
		//at least do it in one place:
		//issues edition might occur in usernaame password @ blah or @(Description=.. or even edition name
		//we are looking for (Z= multiple spaces or tabs) editionz=z['"]*xxZxx['"]z$
		//I guess cut from last index of lowercase trim ends with edition.
		//assume edition name is not going to contain = mostly.
		//assume edition name is not going to have add number of double quotes.
		//count double quotes to make sure we are not in something strange and double quoted. (from the last edition on)
		String lower=localSql.toLowerCase();
		int lastEquals=lower.lastIndexOf("="); //$NON-NLS-1$
		if ((lastEquals!=-1)&&(lower.substring(0,lastEquals).replaceAll("\\s+", " ").trim().endsWith(" edition"))) {  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
			int potential=lower.lastIndexOf("edition");  //$NON-NLS-1$
			//number of double quotes is even.
			boolean even=true;
			for (int i=potential;i<lower.length();i++) {
				if (lower.charAt(i)=='"') {
					even=!even;
				}
			}
			if (even) {
				return potential;
			}
		}
		return -1; //$NON-NLS-1$
	}
    public static ConnectionDetails getConnectionDetails(String localSql) {
        ConnectionDetails cd = null;
        String fullString = null;
        boolean found = false;
        boolean callDialog = false;
        String connectName = ""; //$NON-NLS-1$
        String connectPassword = ""; //$NON-NLS-1$
        String connectDB = ""; //$NON-NLS-1$
        String role = ""; //$NON-NLS-1$
        String edition = ""; //$NON-NLS-1$
        boolean roleBad=false;
        int i = 0;
        // /@ search
        int slash=-1;
        int at=-1;
        for (i = 0; i < localSql.length(); i++) {
            if (Character.isWhitespace(localSql.charAt(i))) {
                found = true;
                break;
            }
        }
        if (!(found == false)) {// we have whitespace
            fullString = localSql.substring(i).trim();//assumption we want full String with edition if possible
        }
        if (getLastEdition(localSql)!=-1) {
        	//According to the syntax, there should be nothing else on a connect after an edition statement.
        	try {
        		if (localSql.lastIndexOf('=')!=-1) {
        			edition=localSql.substring(localSql.lastIndexOf('=')+1);
        			//edition left around annoys connect this is going to break if edition is used elsewhere in the connect string - username password od description
        			localSql=localSql.substring(0,getLastEdition(localSql));
        		}
        	} catch (Exception e) { /* do nothing.  If this doesnt work the edition is not correct */ }
        }
        if (!(found == false)) {// we have whitespace
            // get first argument if the argument has space
            // in the name or is quoted this will fail //ok first argument is connect
            localSql = localSql.substring(i).trim();
            // get name
            String remainder = ""; //$NON-NLS-1$
            //note if as sysdba ends connect for some other reason - end of password -> bust
            if(localSql!=null&&!localSql.equals("")) { //$NON-NLS-1$
                String[] both=separateRole(localSql);
                localSql=both[0];
                role=both[1];
                roleBad=(both[2]!=null)&&(both[2].equals((Boolean.TRUE).toString()));
            }
            if ((localSql.startsWith("[") && (localSql.contains("]jdbc:")))||(localSql.startsWith("jdbc:"))) {  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
                //third party database with no upfront username/password - 
                //[ could mean user and password as up front properties.
                //issues - if user =jdbc:something or [something and ]jdbc: used in the string i.e. unlikely
                //thats possible - will check localSQL is a correct url in bridgespeak i.e. optional [ and full jdbc:x
                //note two connectionDetails so need to specify this one explicitly
                oracle.dbtools.db.ConnectionDetails cd3=null;
                try{
                    cd3=new oracle.dbtools.db.ConnectionDetails(localSql);
                } catch (Exception e) {
                    //do not want to fail badly if 3rd party connection parsing fails
                    LOGGER.log(Level.SEVERE,e.getMessage());
                }
                if (cd3!=null&&cd3.getDriver()!=null) {
                    return new ConnectionDetails(fullString, callDialog, null, null, localSql, role,roleBad, 1/*i.e. do not prompt*/,1 /*i.e. do not prompt*/,null);
                }
            }
            //ezconnect may well contain /
            // "/" and "@" not in double quotes.
            // actually / before @ (or no@)
            // or no / (or no @)
            // leave after @ includeing as role unchanged for now, except for badRole flag
            localSql=localSql.trim();
            int doubleQuote=0;
            for (int atChar=0;(atChar<localSql.length())&&(at==-1);atChar++) {
                switch (localSql.charAt(atChar)) {
                    case '"':
                        doubleQuote++;
                        break;
                    case '@':
                        if (doubleQuote%2==0) {
                            at=atChar;
                        }
                        break;
                    case '/'://issue not addressed last slash before @ noted (untrapped syntax error)
                        if (doubleQuote%2==0) {
                            slash=atChar;
                        }
                        break;
                    default:
                        //non active char
                        break;
                }
            }
            if (slash != -1 &&  //$NON-NLS-1$
                    ((at==-1)|| //$NON-NLS-1$
                            (at>slash))){ //$NON-NLS-1$  //$NON-NLS-2$
                connectName = localSql.substring(0,slash);
                remainder = localSql.substring(slash+1);
                if (connectName.equals("")) { //$NON-NLS-1$
                    callDialog = true;
                }
                if (at != -1) { //$NON-NLS-1$
                    connectPassword = localSql.substring(slash+1,at);
                    connectDB = localSql.substring(at+1).trim().replace("\"", ""); //$NON-NLS-1$ //$NON-NLS-2$
                } else {// no @ so could be missing database
                	if(remainder.toLowerCase().indexOf("edition")!=-1) {
                	 connectPassword=remainder.substring(0, remainder.toLowerCase().indexOf("edition"));
                	 if (remainder.lastIndexOf('=')!=-1) {
                		 //Just check that there is an equals with the edition
                		 remainder=remainder.substring(remainder.lastIndexOf('=')+1);
                	 }
                	} else {
                     connectPassword = remainder.trim();
                	}
                } 
                if (getLastEdition(remainder)!=-1) {
                	if (remainder.toLowerCase().indexOf("=")!=-1) {
                	   edition=remainder.substring(remainder.toLowerCase().lastIndexOf('=')+1);	
                	}
                }
                if (connectName.trim().equals("") && connectPassword.trim().equals(""))  //$NON-NLS-1$ //$NON-NLS-2$
                {
                    //Hopefully ops$ login - dont ask anything
                    callDialog=false; 
                } 
            } else {// no / so could be missing password
                callDialog = true;
                if (at != -1) { //$NON-NLS-1$
                    connectName = localSql.substring(0,at);
                    connectDB = localSql.substring(at+1).trim().replace("\"", ""); //$NON-NLS-1$ //$NON-NLS-2$;
                } else {// no @ so could be missing database
                    connectName = localSql.trim();
                }
            }
        }// ignore proxy and as sysdba and as sysoper
        else {
            // call dialog for username and password and fo through environmental cariables for problem is dialog allows all options and may just
            // be a restart of the process. sqlplus allows connect user:totierne password:totierne@DEV10G or user:totierne/totierne@DEV10G who is
            // this done in sqlplus itself..
            callDialog = true;
        }
        ConnectionDetails cdNew= new ConnectionDetails(fullString, callDialog, connectName, connectPassword, connectDB, role, roleBad, at, slash, null,edition);
        if (cdNew!=null) {
            cdNew.setCallUsage(ScriptUtils.newConnectionDetailsError(cdNew));
        }
        return cdNew;
    }

    /**
     * next string to character, returning the string and the rest
     */
    private static String[] nextToChar(String in, char sep) {
        int position = in.indexOf(sep);
        String theRest, retVal;
        if (position == -1) {
            retVal = in;
            theRest = ""; //$NON-NLS-1$
        } else {
            if (position == in.length() - 1) {
                theRest = ""; //$NON-NLS-1$
            } else {
                theRest = in.substring(position + 1);
            }
            retVal = in.substring(0, position);
        }
        String[] stringArray = { retVal, theRest };
        return stringArray;
    }

    private static String getArgument(String arg,String[] parts) {
        for (int i=0;i<parts.length;i++) {
            if (parts[i].equals(arg)) {
                if (i<parts.length-1) {
                    return parts[i+1];
                }
            }
        }
        return ""; //$NON-NLS-1$
    }


    private static String[] separateRole(String both) {
        Boolean roleBad=Boolean.FALSE;
        String role=""; //$NON-NLS-1$
        String connectDB=both;
        if (connectDB!=null&&connectDB.length()>0){
            String connectDBlower=connectDB.toLowerCase().replace("\"", " ");
            String[] connectDbArray=connectDBlower.split("\\s+");  //$NON-NLS-1$
            if (connectDbArray!=null&&connectDbArray.length>1)
                /* sysoper is not handled higher up but at least recognise it for now */
                for (String check:new String[]{"sysoper","sysdba","sysbackup","sysdg","syskm","sysasm"}) {  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$  //$NON-NLS-5$  //$NON-NLS-6$
                    if (connectDbArray[connectDbArray.length-1].equals(check)&&
                            connectDbArray[connectDbArray.length-2].equals("as")) {  //$NON-NLS-1$
                        role = "as "+check;  //$NON-NLS-1$
                        break;
                    }
                }
            if (role.trim().equals("")&&connectDbArray[connectDbArray.length-1].toLowerCase().equals("as")) { //$NON-NLS-1$ //$NON-NLS-2$
                //Role only half defined.  Raise an error.
                roleBad=Boolean.TRUE;
            }
            if (!role.equals("")) {  //$NON-NLS-1$
                //note sysoper and sysdba do not contain 'as' but sysasm does
                String truncate=connectDB.trim();
                truncate=connectDB.substring(0,connectDB.length()-5);
                //a/b@fred as sysdba
                String stub=connectDB.substring(connectDBlower.substring(0,connectDBlower.lastIndexOf(role.substring(role.indexOf(" ")))).lastIndexOf("as"));  //$NON-NLS-1$  //$NON-NLS-2$
                int qCount=0;
                for (int count=0;count < stub.length();count++) {
                	if (stub.charAt(count)=='"') {
                		qCount++;
                	}
                }
                int findAs=truncate.toLowerCase().lastIndexOf("as"); //$NON-NLS-1$
                connectDB=truncate.substring(0,findAs).trim(); 
                if ((qCount%2) != 0) {
                	//need to nibble last "
                	String theTrim=connectDB.trim();
                	if (theTrim.endsWith("\"")) {
                		connectDB=theTrim.substring(0,theTrim.length()-1);
                	}
                	//issue here do not flag it for now - outstanding " to close.
                }
            }
        }
        return new String[] {connectDB,role, roleBad.toString()};
    }
    
	public static String replaceEnvVars(final String string) {
		String text=string;
		String os = System.getProperty("os.name");
		
		String pattern = "\\$([A-Za-z0-9_\\-]+)";
		if (os.startsWith("Windows")) {
			pattern="\\%([A-Za-z0-9_\\-]+)\\%";
		}
		Pattern expr = Pattern.compile(pattern,Pattern.DOTALL);
		Matcher matcher = expr.matcher(text);
		while (matcher.find()) {
		    String envValue = System.getenv().get(matcher.group(1).toUpperCase());
		    if (envValue == null) {
		        envValue = "";
		    } else {
		        envValue = envValue.replace("\\", "\\\\");
		    }
		    Pattern subexpr = Pattern.compile(Pattern.quote(matcher.group(0)));
		    text = subexpr.matcher(text).replaceAll(envValue);
		}
		return text;
	}
    public static char[] bytesToHex(byte[] b) {
        int length = b.length ;
        char[] hex = new char[length*2];
        for (int i = 0; i < length; i++) {
            int val=b[i]& 0xFF;
            hex[2*i]=Character.forDigit(val/16,16);
            hex[(2*i)+1]=Character.forDigit((val-((val/16)*16)),16);
        }
        return hex;
    }
	public static String expandVariables(String filename) {
	    if ((System.getProperty("os.name").toLowerCase().indexOf("win")!=-1)) {  //$NON-NLS-1$   //$NON-NLS-2$ 
	        if (filename.indexOf("%")==-1) {  //$NON-NLS-1$   
	            return filename;
	        }
	        String[] keySet= System.getenv().keySet().toArray(new String[System.getenv().keySet().size()]);
	        for (String key:keySet) {
	            filename=filename.replaceAll("(?i)"+Pattern.quote("%"+key+"%"),Matcher.quoteReplacement(System.getenv(key)));  //$NON-NLS-1$   //$NON-NLS-2$   //$NON-NLS-3$
	        }
	        return filename;
        } else {
            if (filename.indexOf("$")==-1) {  //$NON-NLS-1$ 
                return filename;
            }
            String[] keySet= System.getenv().keySet().toArray(new String[System.getenv().keySet().size()]);
            for (String key:keySet) {
                filename=filename.replaceAll("\\$\\{"+Pattern.quote(key)+"\\}",Matcher.quoteReplacement(System.getenv(key)));  //$NON-NLS-1$   //$NON-NLS-2$ 
                filename=filename.replaceAll("\\$"+Pattern.quote(key)+"$",Matcher.quoteReplacement(System.getenv(key)));  //$NON-NLS-1$   //$NON-NLS-2$ 
                filename=filename.replaceAll("\\$"+Pattern.quote(key)+"([^\\p{Alnum}_])",Matcher.quoteReplacement(System.getenv(key))+"$1"); //$NON-NLS-1$   //$NON-NLS-2$ //$NON-NLS-3$ 
            }
            return filename;
        }
	    
	}
	
	public static String getErrorStringAtLine(ISQLCommand cmd, SQLException se) {
		String command = cmd.getSQLOrig();
		String[] lines = command.split("\n");
		int line = getErrorLineFromException(cmd,se);
		return lines[line-1];
	}
	
	public static int getErrorLineFromException(ISQLCommand cmd, SQLException se) {
		int offset=1;
		int newoffset=getOffSetFromOjdbc8(se);
		if (newoffset>1) {
			offset = newoffset;
		}
		String[] lines = cmd.getSQLOrigWithTerminator().split("\n");
		int total = 0;
		for (int i = 0; i < lines.length; i++) {
			int linelength= lines[i].length();
			if ((total+linelength)>=offset) {
				//This is the line we want.
				int chars=offset-total;
				return i+1;
			}
			total += lines[i].length();
			if (i+1 < lines.length) {
				total += 1;
			}
		}
		return 1;
	}
	public static int getOffSetFromOjdbc8(Exception se) {
		int offset = 1;
		try {
			//Does the class exist? 
			Class ode = Class.forName("oracle.jdbc.OracleDatabaseException", false, ScriptUtils.class.getClassLoader());
			Throwable exceptionClass =	se.getCause();
			if (exceptionClass!=null && exceptionClass.getClass().isAssignableFrom(ode)) {
			Method methodToFind = ode.getMethod("getErrorPosition", (Class<?>[]) null);
			if (methodToFind != null) {
				   // Method found. You can invoke the method like
				  offset = (int) methodToFind.invoke(se.getCause(), (Object[]) null);
				}
			}
		} catch (ClassNotFoundException e) {
		} catch (NoSuchMethodException e) {
		} catch (SecurityException e) {
		} catch (IllegalAccessException e) {
		} catch (IllegalArgumentException e) {
		} catch (InvocationTargetException e) {
		}
		return offset;
	}
	public static Object getErrorAsterixFromException(ISQLCommand cmd, SQLException se) {
		String sqlOrig = cmd.getSQLOrig();
		int offset = 1;
		int newoffset=getOffSetFromOjdbc8(se);
		if (newoffset>1) {
			offset = newoffset;
		}
//		if ((Exception)se instanceof oracle.jdbc.OracleDatabaseException) {
//			offset = ((oracle.jdbc.OracleDatabaseException)se.getCause()).getErrorPosition();	
//		}
		String[] lines = sqlOrig.split("\n");
		int total = 0;
		for (int i = 0; i < lines.length; i++) {
			int linelength= lines[i].length();
			if ((total+linelength)>=offset) {
				//This is the line we want.
				int chars=offset-total;
				if (chars==0) {
					return "*";
				} else {
					return (String.format("%1$"+chars+ "s", " ")+"*");					
				}
			}
			total += lines[i].length();
			if (i+1 < lines.length) {
				total += 1;
			}
		}
		return "*";
	}
	
	/**
	 * TODO move to a utilities file?
	 * @param dataType
	 * @param colsize
	 * @param val
	 * @return
	 */
	public static StringBuffer wordwrapNotXml(int dataType, int colsize, StringBuffer val) {
		StringBuffer buf = val;
		int q = 0;
		int space = -1;
		int line = 0;
		if /* not xml */ (!(dataType == 2009)){//shouldnt be XML here anyway there is a forked function for that.
			for (;((buf!=null)&&(q<buf.length()));) {
				if (buf.charAt(q)==' ') {
					buf.delete(q,q+1);
				} else {
					break;
				}
			}
		}
		if ((dataType == 2004 /* OracleTypes.BLOB */|| dataType == -13 /*
		 * OracleTypes.
		 * BFILE
		 */)
		 && colsize < val.length()) {
			return wrap(dataType, colsize, val);
		} else if (colsize < val.length()) {
			while (q < buf.length()) {
				/* do end word check before start word check */
				if (q>1 && buf.charAt(q) == ' ' && q - 1 < buf.length()
						&& buf.charAt(q - 1) != ' ') // we will only deal with
					// one space here
					space = -1;
				// Bug 20461131 additional check to not exceed the buf length.
				if (buf.charAt(q) == ' ' && q + 1 < buf.length()
						&& buf.charAt(q + 1) != ' ') // we will only deal with
					// one space here
					space = q;
				if((buf.charAt(q) == '\n')||(buf.charAt(q)=='\r')) {
				/*if (m_lineSeparator.length() > 0
						&& buf.charAt(q) == m_lineSeparator.toCharArray()[0]) {*/
					space = -1;
					line = q + 1;
				}
				if (q > line + colsize - 1) {
					if (space != -1) {
						// buf.setCharAt(space,'\n');
						buf.delete(space, space + 1);
						buf.insert(space, m_lineSeparator.toCharArray());
						line = space + 1;
						space = -1;
					} else {
						buf.insert(q, m_lineSeparator.toCharArray());
						q=q+m_lineSeparator.length();
						//eat spaces
						for (;((buf!=null)&&(q<buf.length()));) {
							if (buf.charAt(q)==' ') {
								buf.delete(q,q+1);
							} else {
								break;
							}
						}
						q=q-1;
						
						line = q + 1;
					}
				}
				q++;
			}
		}
		return buf;
	}
	public static StringBuffer wordwrap(int dataType, int colsize, StringBuffer val, int longIn) {
		StringBuffer buf = val;
		int q = 0;
		int space = -1;
		int line = 0;
		if /* not xml */ (!(dataType == 2009)){
			for (;((buf!=null)&&(q<buf.length()));) {
				if (buf.charAt(q)==' ') {
					buf.delete(q,q+1);
				} else {
					break;
				}
			}
		}
		if ((dataType == 2004 /* OracleTypes.BLOB */|| dataType == -13 /*
		 * OracleTypes.
		 * BFILE
		 */)
		 && colsize < val.length()) {
			return wrap(dataType, colsize, val);
		} else if (dataType == 2009 && colsize < val.length()) {
			return xmlWordWrap(colsize, val, longIn);
		} else if (colsize < val.length()) {
			while (q < buf.length()) {
				/* do end word check before start word check */
				if (q>1 && buf.charAt(q) == ' ' && q - 1 < buf.length()
						&& buf.charAt(q - 1) != ' ') // we will only deal with
					// one space here
					space = -1;
				// Bug 20461131 additional check to not exceed the buf length.
				if (buf.charAt(q) == ' ' && q + 1 < buf.length()
						&& buf.charAt(q + 1) != ' ') // we will only deal with
					// one space here
					space = q;
				// if(buf.charAt(q) == '\n') {
				/* careful now data may have \n but no \r\n */
				/*25316690 SET SERVEROUTPUT FORMAT WORD_WRAP NOT PRINTING CORRECTLY (on windows)
				 if (m_lineSeparator.length() > 0
						&& buf.charAt(q) == m_lineSeparator.toCharArray()[0]) {*/
				if ((buf.charAt(q) == '\n')||(buf.charAt(q)== '\r')) {//assume \r is start of \r\n
					space = -1;
					line = q + 1;
				}
				if (q > line + colsize - 1) {
					if (space != -1) {
						// buf.setCharAt(space,'\n');
						buf.delete(space, space + 1);
						buf.insert(space, m_lineSeparator.toCharArray());
						line = space + 1; //not altering to length as hitting \n at q+1 is a noop
						space = -1;
					} else {
						buf.insert(q, m_lineSeparator.toCharArray());
						q=q+m_lineSeparator.length();//if not .length hit \n in \r\n and no space strip
						//eat spaces
						for (;q<buf.length();) {
							if (buf.charAt(q)==' ') {
								buf.delete(q,q+1);
							} else {
								break;
							}
						}
						q=q-1;
						line = q + 1;
					}
				}
				q++;
			}
		}
		return buf;
	}

	public static StringBuffer truncate(int dataType, int colsize, StringBuffer val) {
		StringBuffer buf = new StringBuffer("");
		if (colsize < val.length()) {
			String colval = val.toString();
			int count = val.toString().replaceAll(m_lineSeparator, "").length();
			// Bug 19498508 we don't count line terminators anymore when 
			// evaluating colval size, this avoids unnecessary chopping of strings
			// during truncation.
			// We will not emulate SQLPlus behavior because lots of line terminators in XMLTYPE and SQLXML types.
			if(count <= colsize) 
				buf.append(colval);
			else 
				buf.append(colval.substring(0, colsize));
			return buf;
		}
		buf = val;
		return buf;
	}

	public static StringBuffer wrap(int dataType, int colsize, StringBuffer val) {
		StringBuffer buf = new StringBuffer("");
		int q = 0;
		int line = 0;
		String remainStr = "";
		StringBuffer tval = new StringBuffer("");
		if (val.indexOf(m_lineSeparator) == -1) {
			tval.append(val.toString().replaceAll("\\n", m_lineSeparator));
		} else {
			tval.append(val.toString());
		}
		int lterms = tval.toString().split(m_lineSeparator).length;
		while (q < tval.length()) {
			if (q > line + colsize - 1) {
				buf.append(tval.substring(line, line + colsize));
				if (lterms >= 1 && dataType != -10) // dont wrap, oracle cursor
					// data type = -10.
					// http://docs.oracle.com/cd/E11882_01/appdev.112/e13995/constant-values.html#oracle_jdbc_OracleTypes_CURSOR
					buf.append(m_lineSeparator);
				line = q;
				remainStr = "";
				remainStr += tval.charAt(q);
			} else{
				// Bug 21145787 if there are line terminators then
				// quietly append to the buffer and continue.
				if(tval.length()>1 && (tval.charAt(q) == '\r' && tval.charAt(q+1) == '\n')) {
		     	buf.append(tval.substring(line, q));
					buf.append(tval.charAt(q));
					q++;
					buf.append(tval.charAt(q));
					q++;
					line = q;
					remainStr = "";
				}
				else if(tval.charAt(q) == '\n'){
					buf.append(tval.substring(line, q));
					buf.append(tval.charAt(q));
					q++;
					line = q;
					remainStr = "";
				}

				if(q < tval.length())
					remainStr += tval.charAt(q);
			}
			q++;
		}

		if (remainStr.length() > 0) {
			buf.append(remainStr);
		}

		if (buf.length() == 0) {
			buf = val;
			return buf;
		}
		return buf;
	}

	public static StringBuffer xmlWordWrap(int colsize, StringBuffer val, int longIn) {
		StringBuffer wrappedXML = null;
		String xml = val.toString().trim();
        // Bug 21285374 implemented a separate word wrap for xml.
        if (xml.length() > colsize && xml.contains(" ")) {
            final String line = xml.substring(0, colsize);
            final int lineBreakIndex = line.indexOf(m_lineSeparator);
            final int lineLastSpaceIndex = line.lastIndexOf(" ");
            final int breakIndex = lineBreakIndex > -1 ? lineBreakIndex : (lineLastSpaceIndex > -1 ? lineLastSpaceIndex : colsize);
            
            if(breakIndex == colsize) 
            	xml = xml.substring(0, breakIndex) + m_lineSeparator + xmlWordWrap(colsize, new StringBuffer(xml.substring(breakIndex)), longIn);
            else
                xml = xml.substring(0, breakIndex) + m_lineSeparator + xmlWordWrap(colsize, new StringBuffer(xml.substring(breakIndex + 1)), longIn);
        } 

		try {
			if (xml == null || xml.trim().length() == 0)
				return val;
			StringBuilder sb = new StringBuilder();
			String[] rows = xml.trim().split(m_lineSeparator);

			for (int i = 0; i < rows.length; i++) {
				if (rows[i] == null || rows[i].trim().length() == 0)
					continue;
				String row = rows[i].replaceAll("^\\s+", "");
				sb.append(row + m_lineSeparator);
			}
			if (longIn < sb.length()) {
				wrappedXML = new StringBuffer(sb.toString()
						.substring(0, longIn));
			} else {
				wrappedXML = new StringBuffer(sb.toString());
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return wrappedXML;
	}
	/**
	 * If running against orest - might give not implemented exception (on clob or blob) - prefer sqlexception so sqlcl can try getObject.
	 * @param rset
	 * @param i
	 * @return
	 * @throws SQLException
	 */
	public static Datum getOracleObjectWrap(OracleResultSet rset, int i) throws SQLException {
		try {
			return rset.getOracleObject(i);
		} catch (Throwable e) {
			if (!(e instanceof SQLException)) {
				throw new SQLException(e);
			}
			throw (SQLException) e;
		}
	}
	/**
	 * if running against orest might hit unimplemented exception (for clobs and blobs) prefer sqlexception so sqlcl canm try .getObject
	 * @param rset
	 * @param name
	 * @return
	 * @throws SQLException
	 */
	public static Datum getOracleObjectWrap(OracleResultSet rset, String name) throws SQLException{
		try {
			return rset.getOracleObject(name);
		} catch (Throwable e) {
			if (!(e instanceof SQLException)) {
				throw new SQLException(e);
			}
			throw (SQLException) e;
		}
	}
}
