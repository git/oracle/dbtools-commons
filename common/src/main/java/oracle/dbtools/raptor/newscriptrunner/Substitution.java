/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;

public class Substitution {
    /**
     * Not in substituting mode.
     */
    static final int DEFAULT = 0;
    /**
     * In substituting mode.
     */
    static final int ACCEPTINGSUBSTITUTION = 1;
    private ScriptRunnerContext scriptRunnerContext = null;
    boolean subStitutionMade = false;
    private String endCharacters = "?@\"'. =*-;,/~\n\r|%^()\t!<>+:&[]{}\\#$\u00A3"; //$NON-NLS-1$
    private Map<String, String> localMap = new HashMap<String,String>();
    
    /**
     * initialise with a context.
     */
    public Substitution(ScriptRunnerContext context) {
        scriptRunnerContext = context;
    }
    
    /**
     * do a substitution with by default &FOO prompting for FOO.
     */
    public void replaceSubstitution(ISQLCommand cmd) throws SubstitutionException {

        if ((((scriptRunnerContext.getSubstitutionOn() == true) && (scriptRunnerContext.getScanOn() == true))
                || (scriptRunnerContext.getEscape() && cmd.getSql().indexOf(scriptRunnerContext.getEscapeChar()) != -1))
                && (!((cmd.getStmtId() == SQLCommand.StmtSubType.G_S_COMMENT_PLUS)))
                && !cmd.getStmtId().equals(StmtSubType.G_S_DOC_PLUS)) {
            String newLine = "\n\r"; //$NON-NLS-1$
            String in = cmd.getSql();
            subStitutionMade = false;
            boolean escapeNextChar = false;
            StringBuffer outStringBuffer = new StringBuffer();
            char inChar;
            StringBuffer matchStringBuffer = new StringBuffer();
            String substitutionString = new Character(scriptRunnerContext.getSubstitutionChar()).toString();
            int state = DEFAULT;
            // if there are no substitution string we do not need to go any
            // further.
            // Bug 17539739 need to also check for escape command
            if (in.indexOf(substitutionString) == -1 && !scriptRunnerContext.getEscape()) {
                return;
            }
            if ((cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_DOC_PLUS)||
                    (cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_COMMENT_PLUS)))) {
                return;
            }
            	
            if (!scriptRunnerContext.getSubstitutionOn())
                return;
            boolean concat=true;
            /**
             * Concatenation char sometimes already stripped.
             */
            if (cmd.getProperty(SQLCommand.STRIPPED_CONTINUATION) != null) {
                concat=false;
            }
            boolean eatspaces = false;
            int inLength = in.length();
            String intrim = in.trim();
            // if we have a multiline comment, then just return nothing.
            if (intrim.startsWith("/*") && intrim.endsWith("*/")) { //$NON-NLS-1$ //$NON-NLS-2$
                return;
            }
            if (intrim.startsWith("--")) { //$NON-NLS-1$ // single line comment
                                            // /
                // no point in substituting in single line comments not you
                // cannot concat '-$' a single line comment statement
                return;
            }
            // Eats blanklines and newlines String[] lines=intrim.split("\n");
            // if command line only want to push to spool if toplevel is
            // true
            // ArrayList<String>bitsAL=new ArrayList<String>();
            CoreScriptParser core=null;
            String splitIntoLines = intrim;
            // just in case:
            if (splitIntoLines == null) {
                splitIntoLines = ""; //$NON-NLS-1$
            }
            int splitIndex = -1;
            int nextVal = -1;
            int maxLength = splitIntoLines.length();
            boolean exit = false;
            int j = -1;
            StringBuffer finalString = new StringBuffer();
            int lastsub = 0;
            int lastesc = 0;
            int prevIndex = 0;
            String current = null;
            while (exit == false) {

                // if state = default the rest does not contain substitution or
                // escape do not go char by char
                boolean shortCut = false;
                /*
                 * check for off by one splitindex_+1 start of line nextval+1
                 * start of nect lane (past the \n
                 */
                if ((exit == false) && (state == DEFAULT) && (escapeNextChar == false)
                        && (scriptRunnerContext.getSubstitutionOn() == false || // last
                                                                                // sub
                                                                                // is
                                                                                // -1
                                                                                // only
                                                                                // look
                                                                                // up
                                                                                // if
                                                                                // last
                                                                                // sub
                                                                                // !=-1
                                                                                // &&<
                                                                                // splitIndex+1
                                (lastsub == -1)
                                || (((prevIndex == 0 || (lastsub < splitIndex + 1)) && (lastsub = splitIntoLines
                                        .indexOf(scriptRunnerContext.getSubstitutionChar(), splitIndex + 1)) == -1)))
                        && (scriptRunnerContext.getEscape() == false || (scriptRunnerContext.getEscapeChar() == '\0') || // last
                                                                                                                            // esc
                                                                                                                            // ==-1
                                                                                                                            // on;y
                                                                                                                            // look
                                                                                                                            // up
                                                                                                                            // if
                                                                                                                            // last
                                                                                                                            // esc
                                                                                                                            // !-
                                                                                                                            // -1
                                                                                                                            // and
                                                                                                                            // <
                                                                                                                            // split
                                                                                                                            // Index+1
                                (lastesc == -1)
                                || (((prevIndex == 0 || lastesc < splitIndex + 1) && (lastesc = splitIntoLines
                                        .indexOf(scriptRunnerContext.getEscapeChar(), splitIndex + 1)) == -1)))) {
                    // nothing happening from here to end
                    finalString.append(splitIntoLines.substring(splitIndex + 1));
                    exit = true;
                } else {
                	if (core==null) {//only create core if & exists - extra null check per line  -important?
                		core = new CoreScriptParser(); 
                	}
                    nextVal = splitIntoLines.indexOf("\n", splitIndex + 1); //$NON-NLS-1$
                    if (nextVal != -1) {
                        current = splitIntoLines.substring(splitIndex + 1, nextVal + 1);
                        prevIndex = splitIndex + 1;
                        splitIndex = nextVal;
                        j++;
                    } else {
                        if (splitIndex != (maxLength - 1)) {// no left over
                                                            // bytes
                            current = splitIntoLines.substring(splitIndex + 1);
                            prevIndex = splitIndex + 1;
                            j++;
                        }
                        exit = true;
                    }

                    // NOTHING HAPPENING THIS LINE:
                    if ((exit == false) && (state == DEFAULT) && (escapeNextChar == false) && (scriptRunnerContext
                            .getSubstitutionOn() == false || (lastsub == -1)
                            || (lastsub >= nextVal + 1)
                            || ((((lastsub < prevIndex)) && (((lastsub = splitIntoLines
                                    .indexOf(scriptRunnerContext.getSubstitutionChar(), prevIndex)) == -1)
                                    || (lastsub >= nextVal + 1)))))
                            && (scriptRunnerContext.getEscape() == false
                                    || (scriptRunnerContext.getEscapeChar() == '\0') || (lastesc == -1)
                                    || (lastesc >= nextVal + 1)
                                    || (((lastesc < prevIndex)
                                            && (((lastesc = splitIntoLines.indexOf(scriptRunnerContext.getEscapeChar(),
                                                    prevIndex)) == -1) || (lastesc >= nextVal + 1)))))) {
                        // nothing happening in this line
                    	if (current.endsWith("\n")) {
                    		core.incrementalOkForSubst(current.substring(0,current.length()-1), concat, true);//keep track of state per 'discarded' line
                    	} else {
                    		core.incrementalOkForSubst(current, concat, true);//keep track of state per 'discarded' line
                    	}
                    	finalString.append(current);
                        continue;
                    }
                    inLength = current.length();
                    outStringBuffer.setLength(0);
                    int stateUpUntil=0;
                    int stateUpUntili=0;
                    for (int i = 0; i < inLength; i++) {
                        inChar = current.charAt(i);

                        switch (state) {
                        case DEFAULT: {

                            if ((inChar == scriptRunnerContext.getSubstitutionChar()) && (escapeNextChar == false)) {
                            	boolean reallyAmp=false;
                            	if (stateUpUntil==outStringBuffer.length()){/* SUBST COULD HAVE BEEN "" FOLLOWED BY & */
                            		reallyAmp=core.incrementalOkForSubst("", concat, false);  //$NON-NLS-1$
                            	} else {
                            		reallyAmp=core.incrementalOkForSubst(outStringBuffer.substring(stateUpUntil), concat, false);
                            	}
                            	stateUpUntil=outStringBuffer.length();
                            	stateUpUntili=i;
                            	if (reallyAmp) {
                            		state = ACCEPTINGSUBSTITUTION;
                            		eatspaces = true;
                            		matchStringBuffer = new StringBuffer();
                            		matchStringBuffer.append(inChar);
                                break;
                            	}
                            }
                            if ((scriptRunnerContext.getEscape() == true)
                                    && (inChar == scriptRunnerContext.getEscapeChar()) && (escapeNextChar == false)) {
                                escapeNextChar = true;
                                break;
                            }
                            if (escapeNextChar) {
                                // if we have got here we have already skipped
                                // ACCEPTINGSUBSTITUTION if that was necessary
                                // bug 6494839
                                escapeNextChar = false;
                            }
                            outStringBuffer.append(inChar);
                            break;
                        }
                        case ACCEPTINGSUBSTITUTION: {
                            if (eatspaces == true) {
                                if (!(Character.isWhitespace(inChar))) {
                                    eatspaces = false;
                                }
                            }
                            if ((scriptRunnerContext.getSubstitutionTerminateChar() != null)
                                    && (inChar == scriptRunnerContext.getSubstitutionTerminateChar())) {
                                if (matchStringBuffer.toString().trim().equals("")) { //$NON-NLS-1$
                                    throw new SubstitutionException(ScriptRunnerContext.lineErr(
                                            new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                                            ScriptRunnerDbArb.getString(ScriptRunnerDbArb.INVALID_SUBSTITUTION),
                                            scriptRunnerContext));
                                } else {
                                    String match = getSubString(matchStringBuffer.toString());
                                    if (match == null) {// ctrld -> exit
                                        scriptRunnerContext.setExited(true);
                                        throw new SubstitutionException("\n" + getCancelled()); //$NON-NLS-1$
                                    }
                                    outStringBuffer.append(match);
                                }
                                state = DEFAULT;
                            } else {
                                if ((((endCharacters.indexOf(inChar) != -1)
                                        && (!((eatspaces == true) && (Character.isWhitespace(inChar)))))
                                        && (!((inChar == scriptRunnerContext.getSubstitutionChar())
                                                && (matchStringBuffer.toString().equals(substitutionString)))))) {
                                    if (!((Character.isWhitespace(inChar)
                                            && (matchStringBuffer.toString().endsWith(substitutionString))))) {
                                        String match = getSubString(matchStringBuffer.toString());
                                        if (match == null) {// ctrld -> exit
                                            if (scriptRunnerContext.isSQLPlusClassic()) { // sqlplus
                                                                                            // will
                                                                                            // quit
                                                                                            // if
                                                                                            // ctrl+c
                                                                                            // is
                                                                                            // entered.
                                                                                            // but
                                                                                            // sqlcl
                                                                                            // was
                                                                                            // asked
                                                                                            // to
                                                                                            // be
                                                                                            // enhanced
                                                scriptRunnerContext.setExited(true);
                                            }
                                            throw new SubstitutionException("\n" + getCancelled()); //$NON-NLS-1$
                                        }
                                        outStringBuffer.append(match);
                                        i--;
                                        state = DEFAULT;
                                        break;
                                    }
                                } else {
                                    matchStringBuffer.append(inChar);
                                }
                            }
                        }
                        }
                    }

                    // Bug 24369611 - IN CLASSIC LAST SUBSTITUTION IS DONE AFTER
                    // OLD/NEW PRINTED.
                    /*
                     * If last line ends with substitution - handle it before
                     * verify output - not last line OK as they are \n
                     * terminated
                     */
                    if ((exit == true) && (state == ACCEPTINGSUBSTITUTION)) {
                        String match = "";
                        if (matchStringBuffer.toString().trim().equals("")) { //$NON-NLS-1$
                            throw new SubstitutionException(
                                    ScriptRunnerContext.lineErr(new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
                                            ScriptRunnerDbArb.getString(ScriptRunnerDbArb.INVALID_SUBSTITUTION),
                                            scriptRunnerContext));
                        } else {
                            match = getSubString(matchStringBuffer.toString());
                            if (match == null) {// ctrld -> exit
                                scriptRunnerContext.setExited(true);
                                throw new SubstitutionException("\n" + getCancelled()); //$NON-NLS-1$
                            }
                            outStringBuffer.append(match);
                        }
                    }
                    //update state for rest of buffer performance issue - note \n could bork continuation character
                    if (outStringBuffer.length()<=stateUpUntil) {
                        core.incrementalOkForSubst("", concat, true);
                    } else {
                    	//will only be no newline on the last one -`> dont care no additional &
                    	int olength=outStringBuffer.length();
                    	if ((olength>0)&&(outStringBuffer.charAt(olength-1)=='\n')) {
                    		core.incrementalOkForSubst(outStringBuffer.substring(stateUpUntil,olength-1), concat, true);//keep track of state per 'discarded' line
                    	} else {
                    		core.incrementalOkForSubst(outStringBuffer.substring(stateUpUntil), concat, true);//keep track of state per 'discarded' line
                    	}
                    	////still need to process endswith newline
                    	////warning trim  troves any trailing \n could be a performance issue.
                        //(new ScriptUtils()).new MiniScriptParser().incrementalOkForSubst(core, outStringBuffer.substring(stateUpUntil).trim()+" ", true, true);
                    }
                    finalString.append(outStringBuffer);
                    ;

                    if (scriptRunnerContext.isSQLPlusClassic() && scriptRunnerContext.isVerifyOn()
                            && (!cmd.getStmtType().equals(SQLCommand.StmtType.G_C_SQLPLUS))) {
                        String anewLine = outStringBuffer.toString();
                        if (!((current.trim().equals(anewLine.trim())
                                || (scriptRunnerContext.getEscape() && (stripEsc(scriptRunnerContext, current.trim())
                                        .equals(stripEsc(scriptRunnerContext, anewLine.trim()))))))) {
                            // Only do this if there are substitutions done
                            scriptRunnerContext.write(MessageFormat.format("old  {0}: {1}\n", //$NON-NLS-1$
                                    new Object[] { j + 1, current.replaceAll("\\s+$", "") }));
                            scriptRunnerContext.write(MessageFormat.format("new  {0}: {1}\n", //$NON-NLS-1$
                                    new Object[] { j + 1, anewLine.replaceAll("\\s+$", "") }));
                        }
                        try {
                            scriptRunnerContext.getOutputStream().flush();
                        } catch (IOException e) {
                        }
                    }
                }
            }

            if ((subStitutionMade) && (scriptRunnerContext.isVerifyOn())) {
                if (!cmd.getStmtType().equals(SQLCommand.StmtType.G_C_SQLPLUS)) { // Never
                                                                                    // show
                                                                                    // subs
                                                                                    // when
                                                                                    // its
                                                                                    // a
                                                                                    // connect.
                    if (!scriptRunnerContext.isSQLPlusClassic()) {
                        cmd.setProperty(SQLCommand.PROP_PRINTED_STRING,
                                ScriptRunnerDbArb.getString(ScriptRunnerDbArb.SUBSTITUTIONOLD) + cmd.getSql() + "\n" + //$NON-NLS-1$
                                        ScriptRunnerDbArb.getString(ScriptRunnerDbArb.SUBSTITUTIONNEW) + finalString
                                        + "\n"); //$NON-NLS-1$
                    }
                }
            }
            if (subStitutionMade) {
                resetBooleanProperty(cmd);
            }
            cmd.setSql(finalString.toString());
            return;
        } else {
            return;
        }
    }
    private String stripEsc(ScriptRunnerContext ctx, String in) {
        StringBuffer inBuffer=new StringBuffer(in);
        StringBuffer outBuffer=new StringBuffer();
        char escape=ctx.getEscapeChar();
        for (int i=0;i<inBuffer.length();i++) {
            char current=inBuffer.charAt(i);
            if (current!=escape) {
                outBuffer.append(current);
            }
        }
        return outBuffer.toString().trim();
    }
    private String getCancelled() {
        ScriptRunnerContext ctx=this.scriptRunnerContext;
        String retVal=null;
        if (ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null && Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())
                ) {
            retVal=ScriptRunnerDbArb.get(ScriptRunnerDbArb.SUBSTITUTION_CANCELLED_CLASSIC);
        } else {
            retVal=ScriptRunnerDbArb.get(ScriptRunnerDbArb.SUBSTITUTION_CANCELLED);
        }
        return retVal;
    }
    private String getSubString(String subVal) {
        // look up upper cased substitution variable or prompt for it
        String toOrig = subVal.trim();
        boolean atAt = false;
        String contextSubString = Character.toString(scriptRunnerContext.getSubstitutionChar());
        String doubleSub=contextSubString + contextSubString;
        if ((toOrig.equals(doubleSub) || toOrig.equals(contextSubString))) {
            return subVal;
        }
        subStitutionMade = true;
        String toUpper = null;
        if (toOrig.startsWith(doubleSub)) {
            atAt = true;
            toOrig = toOrig.substring(2).trim();
        } else if (toOrig.startsWith(contextSubString)) {
            toOrig = toOrig.substring(1).trim();
        } 
        toUpper = toOrig.toUpperCase();
        Map<String, String> map = scriptRunnerContext.getMap();
        String match = null;
        try {//look up(calculate if not null) 7 special sqlplus vars
            ScriptRunnerContext.SqlplusVariable v=ScriptRunnerContext.SqlplusVariable.valueOf(toUpper);
            match=scriptRunnerContext.doPromptReplaceSqlplusVar(v.toString(), v);
        } catch (IllegalArgumentException iae) {
            match = map.get(toUpper);
        }
        if (match == null) {
            match = scriptRunnerContext.getSubstitutionFieldProvider().getPromptedField
                    (scriptRunnerContext, ScriptRunnerDbArb.format(ScriptRunnerDbArb.SUBST_PROMPT,toOrig),false);
            if ((atAt == true)&&(match !=null)) {
                map.put(toUpper, match);
            }
        }
        //keep a local hashtable so the value can be remembered if a property has &VAL to be looked up (and not reprompted)
        if (match !=null) {
            localMap.put(toUpper, match);
        }
        return match;
    }
    
    
    public class SubstitutionException extends Exception {
        SubstitutionException(String msg){
            super(msg);
        }
    }
    private void resetBooleanProperty(ISQLCommand cmd){
        String prop = (String) cmd.getProperty(ISQLCommand.PROP_STRING);
        if ((prop!=null)&&(prop.length()>1)&&
                ((prop.charAt(0)==scriptRunnerContext.getSubstitutionChar())
                        ||(Character.toString(prop.charAt(0)).toLowerCase().equals
                                (Character.toString(scriptRunnerContext.getSubstitutionChar())))
                                ||(Character.toString(prop.charAt(0)).toUpperCase().equals
                                        (Character.toString(scriptRunnerContext.getSubstitutionChar()))))) {
            String lookup = prop.substring(1).trim().toUpperCase();
            int i =0;
            for (i=0;i<lookup.length();i++) {
                if (this.endCharacters.contains(Character.toString(lookup.charAt(i)))) {
                    break;
                }
            }
            if (i!=0) {
                lookup = localMap.get(lookup.substring(0,i));
            }
            if ((lookup!=null)&&(i!=0)) {
                String lookupLower=lookup.toLowerCase();
                if (lookupLower.equals("off")) { //$NON-NLS-1$
                    cmd.setProperty(ISQLCommand.PROP_STATUS_BOOLEAN,new Boolean(false));
                } else {
                    if (lookupLower.equals("on")){ //$NON-NLS-1$
                        cmd.setProperty(ISQLCommand.PROP_STATUS_BOOLEAN,new Boolean(true));
                    }
                }
                //assumption the not (yes|no) match match has already been set in initial code.
                //set the property value to the lookup
                cmd.setProperty(ISQLCommand.PROP_STRING, lookup);
            }
        }
    }
}
