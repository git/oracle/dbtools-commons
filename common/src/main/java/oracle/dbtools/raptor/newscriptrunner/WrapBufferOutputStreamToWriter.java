/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * class to spool out to spool. write out to pipe to be read on a different
 * thread, so inputstream reader can be used. the output stream is trivially
 * (api wise) converted to the configured encoding.
 * 
 * @author turloch
 * 
 */
public class WrapBufferOutputStreamToWriter extends BufferedOutputStream {
    private PipedOutputStream m_out = null;
    OutputRedirectorForSpool outputRedirectorForSpool = null;
	private String m_terminator =System.getProperty("line.separator");

    public WrapBufferOutputStreamToWriter(OutputStream out) {
        super(new BufferedOutputStream(new ByteArrayOutputStream()));
    }

    public WrapBufferOutputStreamToWriter(OutputStream out, int size) {
        super(new BufferedOutputStream(new ByteArrayOutputStream()), size);
    }

    public void setOut(ScriptRunnerContext ctx, BufferedWriter newB) throws IOException {
        this.flush();
        this.close();
        PipedOutputStream outs;
        PipedInputStream in = new PipedInputStream();
        outs = new PipedOutputStream(in);
        m_out = outs;
        outputRedirectorForSpool = new OutputRedirectorForSpool(ctx, in, newB);
        outputRedirectorForSpool.start();

    }

    public Thread getThread() {
    	return outputRedirectorForSpool;
    }
    // basically for all BufferedOutout do wrapped functions: for
    // (BufferedOutputStream buf: m_alout) { doitwithbuf }
    @Override
    public void flush() throws IOException {
        if (m_out != null) {
            m_out.flush();
        }
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        if (m_out != null) {
            m_out.write(b, off, len);
        }
    }

    @Override
    public void write(int b) throws IOException {
        if (m_out != null) {
            m_out.write(b);
        }
    }

    // from FilterOutputStream
    public void close() throws IOException {
        if (m_out != null) {
            flush();
            m_out.close();
            m_out = null;
        }
    }

    @Override
    public void write(byte[] b) throws IOException {
        if (m_out != null) {
            m_out.write(b);
        }
    }
    
    public void setTerminator(String terminator) {
    	m_terminator=terminator;
    }
    public String getTerminator() {
    	return m_terminator;
    }

    /**
     * redirector class to suck off the PipeInputStream and write into a writer.
     * @author turloch
     * Initiall thought internal stream UTF8 and spool out whatever you want 
     * - looks like we are moving to configurable encoding (as internal output gets condused with stdout) 
     */
    class OutputRedirectorForSpool extends Thread {
        private BufferedWriter m_bw = null;
        private PipedInputStream m_in = null;
        private BufferedReader m_reader;
        private String m_term = null;
        //this is the default terminator.  Push one in if it is differnt.
		private String m_stringTerminator =System.getProperty("line.separator");

        public OutputRedirectorForSpool(ScriptRunnerContext ctx,PipedInputStream in, BufferedWriter bw) {
            m_bw = bw;
            m_in = in;
            m_term = getLineTerminator();
            try {
                m_reader = new BufferedReader(new InputStreamReader(m_in, ctx.getEncoding())); //$NON-NLS-1$
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            }
        }

        public void run() {
            try {
                while (true) {
                    String _line = m_reader.readLine();
                    if (_line == null) {
                        break;// end of stream
                    }
                    m_bw.write(_line + m_term);
                }
            } catch (IOException e) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            } finally {
                try {
                    if (m_reader != null) {
                        m_reader.close();
                    }
                } catch (IOException e) {
                }
                try {
                    if (m_bw != null) {
                        m_bw.flush();
                        m_bw.close();
                    }
                } catch (IOException e) {
                }
            }

        }

        public void setLineTerminator(String terminator) {
        	m_stringTerminator = terminator;
        }
        String getLineTerminator() {
        	return m_stringTerminator;
        }
    }
}