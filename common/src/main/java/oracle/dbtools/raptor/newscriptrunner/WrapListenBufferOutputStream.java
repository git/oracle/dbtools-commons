/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * act as a bufferouputstream passing any write on to added bufferoutputstreams
 * required for spool. Assuming forprint is writen in one go, , and optionally
 * removes forceprint.
 * 
 * @author turloch
 * 
 */
public class WrapListenBufferOutputStream extends BufferedOutputStream {

	private BufferedOutputStream m_out = null;
	private ArrayList<BufferedOutputStream> m_alout = new ArrayList<BufferedOutputStream>();
	private File m_fileOutput = null;
	private boolean m_removeForcePrint = false;
	private ScriptRunnerContext ctx = null;
	private ByteArrayOutputStream blankStore = null;
	private BufferedOutputStream blankBuff= null;
  private boolean bufferEmpty=true;
  private static final String ENCODING = "UTF-8";  //$NON-NLS-1$ 
	/**
	 * 
	 * WrapListenBufferOutputStream
	 * 
	 * @param out
	 */
	public WrapListenBufferOutputStream(OutputStream out) {
		super(new BufferedOutputStream(new ByteArrayOutputStream()));
		assert (out instanceof BufferedOutputStream);
		m_out = (BufferedOutputStream) out;
		addToList((BufferedOutputStream) out);
	}

	/**
	 * 
	 * WrapListenBufferOutputStream
	 * 
	 * @param out
	 * @param size
	 */
	public WrapListenBufferOutputStream(OutputStream out, int size) {
		super(new BufferedOutputStream(new ByteArrayOutputStream()), size);
		assert (out instanceof BufferedOutputStream);
		m_out = (BufferedOutputStream) out;
		addToList((BufferedOutputStream) out);
	}

	/**
	 * 
	 * WrapListenBufferOutputStream
	 * 
	 * @param out
	 * @param ctxin
	 */
	public WrapListenBufferOutputStream(OutputStream out, ScriptRunnerContext ctxin) {
		super(new BufferedOutputStream(new ByteArrayOutputStream()));
		assert (out instanceof BufferedOutputStream);
		m_out = (BufferedOutputStream) out;
		addToList((BufferedOutputStream) out);
		this.ctx = ctxin;
	}

	/**
	 * 
	 * getMainStream
	 * 
	 * @return WrapListenBufferOutputStream
	 */
	public BufferedOutputStream getMainStream() {
		return m_out;
	}

	public void writeToNotSpool(byte[] bytes) throws IOException {
		IOException io = null;
		if (this.ctx != null) {
			BufferedOutputStream spool=(BufferedOutputStream)ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
			this.flush();
			for (BufferedOutputStream buf : m_alout) {
				try {
					if (doIWrite(buf)) {
						//ie not term out
						//if I am not spool -> write
						if (spool==null) {
							buf.write(bytes);
							buf.flush();
						} else if (!(spool.equals(buf))) {
							buf.write(bytes);
							buf.flush();
						}
					}
				} catch (IOException ioe) {
					io = ioe;
				}
			}
			//any IO exceptions throw the flush one or the last one.
			if (io != null) {
				throw io;
			}
		}
	}
	private boolean doIWrite(BufferedOutputStream amIMain) {
		//if I am on max rows do not write to main
		if (this.ctx != null) {
			Boolean onMaxRows=(Boolean) ctx.getProperty(ScriptRunnerContext.MAXROWS_SPOOLONLY);
			if (onMaxRows!=null) {//all writes go through here so the quicker I skip extra code the better.
				BufferedOutputStream buff=(BufferedOutputStream) ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
				if (onMaxRows&&(buff!=null&&this.equals(buff))) {
					//spool writes its own buffers.
					return true;
				}
				if ((onMaxRows)&&((buff==null/*always false if on max rows*/||(!(buff.equals(amIMain)))))) {
					//if on max rows and not spool ; //this.equals(buff)? extra if lower down
					return false;
				}
				if (onMaxRows) {
					//duplication of if further down - want to keep set term off isOutputSuppressed.
					//amimain - buffer you want to print to = spool
					//if ((this.ctx != null) && (!(ctx.getTopLevel()))
					//        && ctx.isOutputSupressed()) {
						return true;
					//}
				}
			}
		}
		if ((this.ctx != null) && (!(ctx.getTopLevel()))
		        && ctx.isOutputSupressed()) {
			BufferedOutputStream buff=(BufferedOutputStream) ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
			if ((buff!=null)&&((this.equals(buff))||(amIMain.equals(buff)))) {
				//this.equals(buff) - spool wraplistenbuffer writes out its buffers.
				//amIMain.equals(buff) - pass into spool wraplistenbuffer
				//instead of testing for blocking 'amIMain', do not block 'amISpool'
				return true;
			}
			return false;
		}
		return true;
	}

	public void replaceMainStream(BufferedOutputStream newMain) throws IOException {
		if (!m_out.equals(newMain)) {
			flush();
			removeFromList(m_out);
			addToList(newMain);
			m_out = newMain;
		}
	}

	public void addToList(BufferedOutputStream bo) {
		m_alout.add(bo);
	}

	public boolean removeFromList(BufferedOutputStream bo) {
		return m_alout.remove(bo);
	}

	// basically for all BufferedOutout do wrapped functions: for
	// (BufferedOutputStream buf: m_alout) { doitwithbuf }
	@Override
	public void flush() throws IOException {
		IOException io = null;
		for (BufferedOutputStream buf : m_alout) {
			try {
				buf.flush();
			} catch (IOException ioe) {
				io = ioe;
			}
		}
		//any IO exceptions throw the last one.
		if (io != null) {
			throw io;
		}
	}

	/**
	*assumption - byte writes are full characters - might break on buffered
	*bytes are utf8 - have encoding at the top of this class
	*act if I am the spool out not if my subordinates are
	*special characters initially \t space \r \n or other - sounds like a state machine - i.e. no 'special' whitespace
	*set trimspool off - switch off this behaviour in sync (on next \r\n (or current empty buffer)) 
	*set through set trimspool off in src
	*	case next byte
	*' '
	*'\t'
	*store 
	*\r
	*\n
	*eat or write out depending on flag. reset to skip this check if flag off
	*ie continue on current flag setting (if buffer is not empty) until newline
	*on set trimspool off - keep trim spool on until a newline - if there is something in the buffer.
	*commands end with a new line - buffer empty, commands start with a new line - buffer emptied.
	*close - (as newline - i.e. buffer emptied)
	*Outside that normal buffer empty or about to be flushed usage: 
	*- the code will trim up to next newline/close when the trimspool is reset oddly.
	*trimspool off - normal behaviour.
	*not spool WrapListenBufferOutputStream - normal behaviour.
	*(There is a set trimout for non spool output not requested by QA at the moment)
	*Slight rearrangement so all code requiring filtering goes through a single code block.
	*/
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		checkForExit();//other formatters like ansiconsole pass through this method.
		if (len!=0) {
			if ((ctx!=null)&&((Boolean)ctx.getProperty(ScriptRunnerContext.SETTRIMSPOOL)!=null)
					&&((Boolean)ctx.getProperty(ScriptRunnerContext.SETTRIMSPOOL).equals(Boolean.TRUE))
					&&(ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER)!=null)
					&&((ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER).equals(this)))||(!bufferEmpty)) {
				if (blankStore==null) {
					blankStore = new ByteArrayOutputStream();
				}
				if (blankBuff==null) {
					blankBuff=new BufferedOutputStream(blankStore);
				}
				StringBuffer stringStore=new StringBuffer("");   //$NON-NLS-1$
				byte[] toByte=new byte[len];
				System.arraycopy(b,off,toByte,0,len);
				String encoding=ENCODING;
				//want ctx encoding if we have it
				if (ctx!=null) {
					encoding=ctx.getEncoding();
				}
				String current=new String(toByte,encoding);
				for (int i=0;i<current.length();i++) {
					String thisOne=current.substring(i,i+1);
					switch (thisOne) {
					case "\r":  //$NON-NLS-1$
					case "\n":  //$NON-NLS-1$
						blankStore = new ByteArrayOutputStream();
						blankBuff=new BufferedOutputStream(blankStore);
						stringStore.append(thisOne);
						bufferEmpty=true;
						continue;
					case "\t":  //$NON-NLS-1$
					case " ":  //$NON-NLS-1$
						blankStore.write(thisOne.getBytes(Charset.forName(encoding)));
						bufferEmpty=false;
						continue;
					default:
						blankBuff.flush();
						byte[] bA = blankStore.toByteArray();
						blankStore = new ByteArrayOutputStream();
						blankBuff=new BufferedOutputStream(blankStore);
						bufferEmpty=true;
						if (bA.length>0) {
							stringStore.append(new String(bA,ENCODING));
						}
						stringStore.append(thisOne);
					}
				}
				if (stringStore.length()>0){
					byte[] myBytes=stringStore.toString().getBytes(Charset.forName(ENCODING));
					reallywrite(myBytes,0,myBytes.length);
				}
			} else {
				reallywrite(b,off,len);
			}
		}
	}

	public void reallywrite(byte[] b, int off, int len) throws IOException {
		for (BufferedOutputStream buf : m_alout) {
			IOException io = null;
			try {
				if (doIWrite(buf)) {
					buf.write(b, off, len);
				}
			} catch (IOException ioe) {
				io = ioe;
			}
			if (io != null) {
				throw io;
			}
		}
	}

	@Override
	public void write(int b) throws IOException {
		checkForExit(); //other formatters like ansiconsole may pass through this method.
		byte[] b1 = new byte[] {(byte)b};
		write(b1, 0, 1);
	}

	// from FilterOutputStream
	public void close() throws IOException {
		for (BufferedOutputStream buf : m_alout) {
			IOException io = null;
			try {
				buf.close();
			} catch (IOException ioe) {
				io = ioe;
			}
			if (io != null) {
				throw io;
			}
		}
	}

	@Override
	public void write(byte[] b) throws IOException {
		checkForExit();
		if ((ctx!=null)&&((Boolean)ctx.getProperty(ScriptRunnerContext.SETTRIMSPOOL)!=null)
				&&((Boolean)ctx.getProperty(ScriptRunnerContext.SETTRIMSPOOL).equals(Boolean.TRUE))
				&&(ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER)!=null)
				&&((ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER).equals(this)))||(!bufferEmpty)) {
			if (!(this.m_removeForcePrint && ((Arrays.equals(ScriptRunner.FORCE_PRINT.getBytes(ctx.getEncoding()), b)) || (Arrays
					.equals(ScriptRunner.FORCE_PRINT_BYTES, b))))) {
				//centralize the trimspool change - we are trimming whitespace.
				write(b,0,b.length);
			}
		} else {
		// this is where we do all our force print writes
			for (BufferedOutputStream buf : m_alout) {
				IOException io = null;
				try{
					if (!(this.m_removeForcePrint && ((Arrays.equals(ScriptRunner.FORCE_PRINT.getBytes("UTF8"), b)) || (Arrays
							.equals(ScriptRunner.FORCE_PRINT_BYTES, b))))) {
						if (doIWrite(buf)) {
							buf.write(b);
						}
					}
				} catch (IOException ioe) {
					io = ioe;
				}
				if (io != null) {
					throw io;
				}
			}
		}
	}

	/**
	 * In the worksheet, the writing is interrupted/stopped as its thread is interupted.
	 * In SQLCli, the writing thread is not interrupted, so this performs an explicit check to continue writing
	 */
	protected void checkForExit() throws IOException{
		if( ctx!=null && ctx.isCommandLine() && ctx.isInterrupted()){
			throw new InterruptedIOException();
		}
	}

	// clone got from object so why should I override...

	public File getFileOutput() {
		return m_fileOutput;
	}

	public void setFileOutput(File fileOutput) {
		this.m_fileOutput = fileOutput;
	}

	public boolean isRemoveForcePrint() {
		return m_removeForcePrint;
	}

	public void setRemoveForcePrint(boolean forcePrint) {
		m_removeForcePrint = forcePrint;
	}
	
	public ScriptRunnerContext getScriptRunnerContext(){
		return ctx;
	}
	
}