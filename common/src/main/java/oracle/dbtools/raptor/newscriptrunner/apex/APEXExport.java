/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.apex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

//
//
//  * Application Express application export program
//
//  * @author    jkallman
//  * @modified  01-FEB-2005
//  * @modified  17-JUL-2006
//  * @modified  21-JAN-2009    jstraub, added support for exporting saved interactive reports
//  * @modified  13-JAN-2010    jstraub, added support for exporting workspaces
//  * @modified  03-DEC-2010    jstraub, modified gStmt to include predicate for workspace_id (bug 10324795)
//  * @modified  09-MAR-2011    jstraub, modified gStmt, gStmtInstance and ExportFile to support exporting Websheets (bug 10055338)
//  * @modified  16-JUN-2011    jstraub, modified exportFile logic that determines if it is Websheet, changed view to apex_ws_applications
//  * @modified  03-NOV-2011    jstraub, modified exportFile logic that determines if it is Websheet, changed view to apex_ws_applications
//  * @modified  23-NOV-2011    jstraub, modified ExportWorkspace and ExportFeedback to start file at set define (bug 13403278)
//  * @modified  28-NOV-2011    jstraub, added ExportStaticFiles
//  * @modified  21-MAR-2013    jstraub, added expMinimal
//  * @modified  16-APR-2013    cneumuel, added expOriginalIds (feature #985)
//  * @modified  25-NOV-2013    jstraub, added expLocked
//  * @modified  16-APR-2014    vuvarov, added p_export_pkg_app_mapping parameter in export_application_to_clob call (feature #1399)
//
//  * Note:  Assumes export from database character set AL32UTF8
//
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;

public class APEXExport {
  private final String gStmt = "select application_id, application_name from apex_applications where workspace_id = ? and build_status <> 'Run and Hidden' union "
      + "select application_id, application_name from apex_ws_applications where workspace_id = ? order by application_id";

  private final String gStmt2 = "select application_id, application_name from apex_applications where workspace_id = ? union "
      + "select application_id, application_name from apex_ws_applications where workspace_id = ? order by application_id";

  private final String gStmtInstance = "select application_id, application_name from apex_applications where workspace_id <> 10 and build_status <> 'Run and Hidden' union "
      + "select application_id, application_name from apex_ws_applications where workspace_id <> 10 order by application_id";

  private final String gStmtWorkspaces = "select workspace_id, workspace from apex_workspaces where workspace_id > 11 order by workspace_id";

  private final String gStmtWorkspacesFeedback = "select distinct workspace_id, workspace_name from apex_team_feedback where workspace_id > 11 order by workspace_id";

  private final String gStmtWorkspace = "select workspace from apex_workspaces where workspace_id = ?";

  private final String gStmtSetSGID = "begin wwv_flow_api.set_security_group_id(p_security_group_id=>?); end;";

  private final String gStmtGetSGID = "select v('FLOW_SECURITY_GROUP_ID') from sys.dual";

  private final String gStmtIsWS = "select count(*) from apex_ws_applications where application_id = ?";

  public boolean debug = false;

  public boolean skipDate = false;

  public boolean instance = false;

  public boolean pubReports = false;

  public boolean savedReports = false;

  public boolean IRNotifications = false;

  public boolean expWorkspace = false;

  public boolean expMinimal = false;

  public boolean expTeamdevdata = false;

  public boolean expFeedback = false;

  public boolean expTranslations = false;

  public boolean expFiles = false;

  public boolean expOriginalIds = false;

  public boolean expLocked = false;

  private Connection gConn;

  private BigDecimal workspaceID;

  private String deploymentSystem;

  private Date expFeedbackSince;

  private java.util.Date expFeedbackSinceU;

  private String url;

  private String userName;

  private String password;

  private BigDecimal appID;

  private boolean _valid;

  private OutputStream _out;

  private APEXExportSplitter splitter = null;

  private boolean _splitFlat = false;

  private boolean _split = false;

  private boolean _splitUpdate;

  private boolean _splitNoCheckSum;

  private String _cwd;

  //
  // Creates a new instance of APEXExport
  //
  public APEXExport() {
    _valid = false;

  }

  private APEXExportSplitter getSplitter() {
    if (splitter == null) {
      splitter = new APEXExportSplitter();
    }
    splitter.setOptions(_cwd, _splitFlat, _splitNoCheckSum, debug, _splitUpdate);
    return splitter;
  }

  public void setCWD(String cwd) {
    _cwd = cwd;
  }

  // ********************************************
  // ExportFiles
  //
  // ********************************************
  //
  private void exportFiles(BigDecimal appIDToExport, BigDecimal workspaceID, String userName) throws Exception {
    java.util.Date now;

    if (instance) {
      BigDecimal appID;
      String appName = null;
      PreparedStatement stmt = gConn.prepareStatement(gStmtInstance);

      ResultSet result = stmt.executeQuery();

      while (result.next()) {
        appID = result.getBigDecimal(1);
        appName = result.getString(2);

        write("Exporting Application " + appID + ":'" + appName + "' \n");

        if (debug) {
          now = new java.util.Date();
          write(" Start " + now + "\n");
        }

        exportFile(appID, true);
        now = new java.util.Date();
        write("  Completed at " + now + "\n");
      }

      result.close();
      stmt.close();
    }

    else if (workspaceID != null && workspaceID.longValue() != 0) {
      BigDecimal appID;
      BigDecimal securityGroupID = new BigDecimal(0);
      String appName = null;
      PreparedStatement stmt = gConn.prepareStatement(gStmtSetSGID);
      stmt.setBigDecimal(1, workspaceID);

      // Set the Security Group ID
      stmt.executeUpdate();
      stmt.close();

      stmt = gConn.prepareStatement(gStmtGetSGID);
      ResultSet result = stmt.executeQuery();
      while (result.next()) {
        securityGroupID = result.getBigDecimal(1);
      }
      result.close();
      stmt.close();

      if (!securityGroupID.equals(workspaceID)) {
        write("Invalid Workspace ID '" + workspaceID + "' for User '" + userName + "'\n");
        System.exit(1);
      }

      if (!expLocked) {
        stmt = gConn.prepareStatement(gStmt);
      } else {
        stmt = gConn.prepareStatement(gStmt2);
      }

      stmt.setBigDecimal(1, workspaceID);
      stmt.setBigDecimal(2, workspaceID);

      result = stmt.executeQuery();

      while (result.next()) {
        appID = result.getBigDecimal(1);
        appName = result.getString(2);

        write("Exporting Application " + appID + ":'" + appName + "' \n");

        if (debug) {
          now = new java.util.Date();
          write(" Start " + now + "\n");
        }

        exportFile(appID, true);
        now = new java.util.Date();
        write("  Completed at " + now + "\n");
      }

      result.close();
      stmt.close();
    } else {
      // Exporting only one specific application
      write("Exporting application " + appIDToExport);

      if (debug) {
        now = new java.util.Date();
        write(" Start " + now + "\n");
      }

      exportFile(appIDToExport, false);
      now = new java.util.Date();
      write("  Completed at " + now + "\n");
    }
  }

  // ********************************************
  // ExportWorkspaces
  //
  // ********************************************
  //
  private void exportWorkspaces(BigDecimal workspaceID, boolean teamdevdata, boolean minimal) throws SQLException, java.io.IOException {
    java.util.Date now;

    if (workspaceID != null && workspaceID.longValue() != 0) {
      // Exporting only one specific workspace
      String wkspName = null;
      PreparedStatement stmt = gConn.prepareStatement(gStmtWorkspace);
      stmt.setBigDecimal(1, workspaceID);

      ResultSet result = stmt.executeQuery();

      while (result.next()) {
        wkspName = result.getString(1);
        write("Exporting Workspace " + workspaceID + ":'" + wkspName + "' " + "\n");

        if (debug) {
          now = new java.util.Date();
          write(" Start " + now + "\n");
        }

        exportWorkspace(workspaceID, teamdevdata, minimal);
        now = new java.util.Date();
        write("  Completed at " + now + "\n");
      }

      result.close();
      stmt.close();

    } else {
      // Export all workspaces
      BigDecimal wkspID;
      String wkspName = null;
      PreparedStatement stmt = gConn.prepareStatement(gStmtWorkspaces);

      ResultSet result = stmt.executeQuery();

      while (result.next()) {
        wkspID = result.getBigDecimal(1);
        wkspName = result.getString(2);

        write("Exporting Workspace " + wkspID + ":'" + wkspName + "' " + "\n");

        if (debug) {
          now = new java.util.Date();
          write(" Start " + now + "\n");
        }

        exportWorkspace(wkspID, teamdevdata, minimal);
        now = new java.util.Date();
        write("  Completed at " + now + "\n");
      }

      result.close();
      stmt.close();
    }
  }

  // ********************************************
  // ExpFeed
  //
  // ********************************************
  //
  private void ExpFeed(BigDecimal workspaceID, String deploymentSystem, java.sql.Date expFeedbackSince) throws SQLException, java.io.IOException {
    java.util.Date now;

    if (workspaceID != null && workspaceID.longValue() != 0) {
      // Exporting only one specific workspace feedback
      String wkspName = null;
      PreparedStatement stmt = gConn.prepareStatement(gStmtWorkspace);
      stmt.setBigDecimal(1, workspaceID);

      ResultSet result = stmt.executeQuery();

      while (result.next()) {
        wkspName = result.getString(1);
        write("Exporting Feedback for Workspace " + workspaceID + ":'" + wkspName + "' " + "\n");

        if (debug) {
          now = new java.util.Date();
          write(" Start " + now + "\n");
        }

        exportFeedback(workspaceID, deploymentSystem, expFeedbackSince);
        now = new java.util.Date();
        write("  Completed at " + now + "\n");
      }

      result.close();
      stmt.close();

    } else {
      // Export feedback for all workspaces
      BigDecimal wkspID;
      String wkspName = null;
      PreparedStatement stmt = gConn.prepareStatement(gStmtWorkspacesFeedback);

      ResultSet result = stmt.executeQuery();

      while (result.next()) {
        wkspID = result.getBigDecimal(1);
        wkspName = result.getString(2);

        write("Exporting Feedback for Workspace " + wkspID + ":'" + wkspName + "' " + "\n");

        if (debug) {
          now = new java.util.Date();
          write(" Start " + now + "\n");
        }

        exportFeedback(wkspID, deploymentSystem, expFeedbackSince);
        now = new java.util.Date();
        write("  Completed at " + now + "\n");
      }

      result.close();
      stmt.close();
    }
  }

  // ********************************************
  // ExportFile
  //
  // For the specified Application ID, call the procedure which will export the
  // SQL Application export as a CLOB. Then, in the current directory, write
  // the contents of the CLOB out to a file.
  // ********************************************
  //
  public void exportFile(BigDecimal appID, boolean expPkgAppMapping) throws Exception {
    boolean firstTime = true;

    //
    // Call the stored procedure export_to_clob, which will return the
    // application export in a CLOB
    OracleCallableStatement cstmt = (OracleCallableStatement) gConn
        .prepareCall("begin\n" + "    ? := wwv_flow_utilities.export_application_to_clob (\n" + "             p_application_id            => ?,\n" + "             p_export_ir_public_reports  => ?,\n"
            + "             p_export_ir_private_reports => ?,\n" + "             p_export_ir_notifications   => ?,\n" + "             p_export_translations       => ?,\n"
            + "             p_export_pkg_app_mapping    => ?,\n" + "             p_with_original_ids         => case when ?='Y' then true else false end );\n" + "end;");

    cstmt.registerOutParameter(1, OracleTypes.CLOB);
    cstmt.setBigDecimal(2, appID);
    cstmt.setString(3, pubReports ? "Y" : "N");
    cstmt.setString(4, savedReports ? "Y" : "N");
    cstmt.setString(5, IRNotifications ? "Y" : "N");
    cstmt.setString(6, expTranslations ? "Y" : "N");
    cstmt.setString(7, expPkgAppMapping ? "Y" : "N");
    cstmt.setString(8, expOriginalIds ? "Y" : "N");

    cstmt.execute();

    oracle.sql.CLOB clob = cstmt.getCLOB(1);
    Reader instream = clob.getCharacterStream();

    int size = clob.getBufferSize();
    char[] buffer = new char[size];
    int length = -1;

    //
    // Create a new file in the local directory
    //
    BigDecimal isWS = null;
    String isWSString = "1";
    PreparedStatement stmt = gConn.prepareStatement(gStmtIsWS);

    stmt.setBigDecimal(1, appID);
    ResultSet result = stmt.executeQuery();
    while (result.next()) {
      isWS = result.getBigDecimal(1);
      isWSString = isWS.toString();
    }
    result.close();
    stmt.close();
    String theFileName;

    if (isWSString.equals("0")) {
      theFileName = "f" + appID + ".sql";
    } else {
      theFileName = "ws" + appID + ".sql";
    }

    File theFile = new File(_cwd + File.separator + theFileName);
    theFile.delete();
    theFile.createNewFile();

    FileOutputStream foutstream = new FileOutputStream(theFile);

    // Cast to OutputStreamWrite to handle character array conversion to byte
    // array
    // Note that we MUST open this as a UTF-8 file
    OutputStreamWriter owstream = new OutputStreamWriter(foutstream, "UTF-8");

    // read line by line
    BufferedReader in = new BufferedReader(instream);
    String line = null;

    while ((line = in.readLine()) != null) {
      // put \n back
      line = line + "\n";
      // loose headers
      if (firstTime) {
        if (line.indexOf("set define") == 0) {
          firstTime = false;
          owstream.write(line, 0, line.length());
        }
      } else {
        //
        // Remove the "exported on" line from the exported file.
        // This is necessary if you want to checkin to source code control
        // and the only difference between the previous version is the "Exported
        // On" date
        // contained within the application export.
        //
        if (!(skipDate && line.indexOf("--   Date and Time:") == 0)) {
          owstream.write(line, 0, line.length());
        }
      }
    }

    owstream.flush();

    if (debug) {
      write(" Wrote " + theFile.length() + " bytes to " + theFile.getAbsolutePath() + "\n");
    }

    if (_split) {
      getSplitter().processFile(theFile.getAbsolutePath().toString());

    }
    //
    // Close all streams and statements
    //
    instream.close();
    owstream.close();
    foutstream.close();
    cstmt.close();
  }

  // ********************************************
  // ExportWorkspace
  //
  // For the specified Workspace ID, call the procedure which will export the
  // workspace export as a CLOB. Then, in the current directory, write
  // the contents of the CLOB out to a file.
  // ********************************************
  //
  public void exportWorkspace(BigDecimal workspaceID, boolean teamdevdata, boolean minimal) throws SQLException, java.io.IOException {
    boolean firstTime = true;

    //
    // Call the stored procedure export_to_clob, which will return the
    // application export in a CLOB

    String lstmt = null;
    if (teamdevdata) {
      lstmt = "begin ? := wwv_flow_utilities.export_workspace_to_clob(?, true";
    } else {
      lstmt = "begin ? := wwv_flow_utilities.export_workspace_to_clob(?, false";
    }

    if (minimal) {
      lstmt = lstmt + ",true); end;";
    } else {
      lstmt = lstmt + "); end;";
    }

    OracleCallableStatement cstmt = (OracleCallableStatement) gConn.prepareCall(lstmt);

    cstmt.setBigDecimal(2, workspaceID);
    cstmt.registerOutParameter(1, OracleTypes.CLOB);
    cstmt.execute();

    oracle.sql.CLOB clob = cstmt.getCLOB(1);
    Reader instream = clob.getCharacterStream();

    int size = clob.getBufferSize();
    char[] buffer = new char[size];
    int length = -1;

    //
    // Create a new file in the local directory
    //
    File theFile = new File(_cwd + File.separator + "w" + workspaceID + ".sql");
    theFile.delete();
    theFile.createNewFile();

    FileOutputStream foutstream = new FileOutputStream(theFile);

    // Cast to OutputStreamWrite to handle character array conversion to byte
    // array
    // Note that we MUST open this as a UTF-8 file
    OutputStreamWriter owstream = new OutputStreamWriter(foutstream, "UTF-8");

    // read line by line
    BufferedReader in = new BufferedReader(instream);
    String line = null;

    while ((line = in.readLine()) != null) {
      // put \n back
      line = line + "\n";
      // loose headers
      if (firstTime) {
        if (line.indexOf("set define") == 0) {
          firstTime = false;
          owstream.write(line, 0, line.length());
        }
      } else {
        //
        // Remove the "exported on" line from the exported file.
        // This is necessary if you want to checkin to source code control
        // and the only difference between the previous version is the "Exported
        // On" date
        // contained within the application export.
        //
        if (!(skipDate && line.indexOf("--   Date and Time:") == 0)) {
          owstream.write(line, 0, line.length());
        }
      }
    }

    owstream.flush();

    if (debug) {
      write(" Wrote " + theFile.length() + " bytes to " + theFile.getAbsolutePath());
    }

    //
    // Close all streams and statements
    //
    instream.close();
    owstream.close();
    foutstream.close();
    cstmt.close();
  }

  // ********************************************
  // ExportStaticFiles
  //
  // For the specified Workspace ID, call the procedure which will export the
  // workspace files as a CLOB. Then, in the current directory, write
  // the contents of the CLOB out to a file.
  // ********************************************
  //
  public void exportStaticFiles(BigDecimal workspaceID) throws SQLException, java.io.IOException {
    boolean firstTime = true;
    //
    // Call the stored procedure export_files_to_clob, which will return the
    // files export in a CLOB

    String lstmt = null;

    lstmt = "begin ? := wwv_flow_utilities.export_files_to_clob(?); end;";

    OracleCallableStatement cstmt = (OracleCallableStatement) gConn.prepareCall(lstmt);

    cstmt.setBigDecimal(2, workspaceID);
    cstmt.registerOutParameter(1, OracleTypes.CLOB);
    cstmt.execute();

    oracle.sql.CLOB clob = cstmt.getCLOB(1);
    Reader instream = clob.getCharacterStream();

    int size = clob.getBufferSize();
    char[] buffer = new char[size];
    int length = -1;

    //
    // Create a new file in the local directory
    //
    File theFile = new File(_cwd + File.separator + "files_" + workspaceID + ".sql");
    theFile.delete();
    theFile.createNewFile();

    FileOutputStream foutstream = new FileOutputStream(theFile);

    // Cast to OutputStreamWrite to handle character array conversion to byte
    // array
    // Note that we MUST open this as a UTF-8 file
    OutputStreamWriter owstream = new OutputStreamWriter(foutstream, "UTF-8");

    // read line by line
    BufferedReader in = new BufferedReader(instream);
    String line = null;

    while ((line = in.readLine()) != null) {
      // put \n back
      line = line + "\n";
      // loose headers
      if (firstTime) {
        if (line.indexOf("set define") == 0) {
          firstTime = false;
          owstream.write(line, 0, line.length());
        }
      } else {
        //
        // Remove the "exported on" line from the exported file.
        // This is necessary if you want to checkin to source code control
        // and the only difference between the previous version is the "Exported
        // On" date
        // contained within the application export.
        //
        if (!(skipDate && line.indexOf("--   Date and Time:") == 0)) {
          owstream.write(line, 0, line.length());
        }
      }
    }

    owstream.flush();

    if (debug) {
      write(" Wrote " + theFile.length() + " bytes to " + theFile.getAbsolutePath() + "\n");
    }

    //
    // Close all streams and statements
    //
    instream.close();
    owstream.close();
    foutstream.close();
    cstmt.close();
  }

  // ********************************************
  // ExportFeedback
  //
  // For the specified Workspace ID, call the procedure which will export the
  // team development feedback as a CLOB. Then, in the current directory, write
  // the contents of the CLOB out to a file.
  // ********************************************
  //
  public void exportFeedback(BigDecimal workspaceID, String deploymentSystem, java.sql.Date expFeedbackSince) throws SQLException, java.io.IOException {
    boolean firstTime = true;

    //
    // Call the stored procedure export_feedback_to_development or
    // export_feedback_to_deployment, which will return the
    // application export in a CLOB
    String lstmt = null;
    if (deploymentSystem == null) {
      lstmt = "begin ? := wwv_flow_utilities.export_feedback_to_development(?,?); end;";
    } else {
      lstmt = "begin ? := wwv_flow_utilities.export_feedback_to_deployment(?,?,?); end;";
    }

    OracleCallableStatement cstmt = (OracleCallableStatement) gConn.prepareCall(lstmt);

    if (deploymentSystem == null) {
      cstmt.setDate(3, expFeedbackSince);
      cstmt.setBigDecimal(2, workspaceID);
      cstmt.registerOutParameter(1, OracleTypes.CLOB);
    } else {
      cstmt.setDate(4, expFeedbackSince);
      cstmt.setString(3, deploymentSystem);
      cstmt.setBigDecimal(2, workspaceID);
      cstmt.registerOutParameter(1, OracleTypes.CLOB);
    }

    cstmt.execute();
    oracle.sql.CLOB clob = cstmt.getCLOB(1);
    Reader instream = clob.getCharacterStream();

    int size = clob.getBufferSize();
    char[] buffer = new char[size];
    int length = -1;

    //
    // Create a new file in the local directory
    //
    File theFile = new File(_cwd + File.separator + "fb" + workspaceID + ".sql");
    theFile.delete();
    theFile.createNewFile();

    FileOutputStream foutstream = new FileOutputStream(theFile);

    // Cast to OutputStreamWrite to handle character array conversion to byte
    // array
    // Note that we MUST open this as a UTF-8 file
    OutputStreamWriter owstream = new OutputStreamWriter(foutstream, "UTF-8");

    // read line by line
    BufferedReader in = new BufferedReader(instream);
    String line = null;

    while ((line = in.readLine()) != null) {
      // put \n back
      line = line + "\n";
      // loose headers
      if (firstTime) {
        if (line.indexOf("set define") == 0) {
          firstTime = false;
          owstream.write(line, 0, line.length());
        }
      } else {
        //
        // Remove the "exported on" line from the exported file.
        // This is necessary if you want to checkin to source code control
        // and the only difference between the previous version is the "Exported
        // On" date
        // contained within the application export.
        //
        if (!(skipDate && line.indexOf("--   Date and Time:") == 0)) {
          owstream.write(line, 0, line.length());
        }
      }
    }

    owstream.flush();

    if (debug) {
      write(" Wrote " + theFile.length() + " bytes to " + theFile.getAbsolutePath() + "\n");
    }

    //
    // Close all streams and statements
    //
    instream.close();
    owstream.close();
    foutstream.close();
    cstmt.close();
  }

  public void export() throws Exception {
    //
    // Export the applications or workspaces
    //
    if (expWorkspace) {
      // export workspaces
      exportWorkspaces(workspaceID, expTeamdevdata, expMinimal);
    } else if (expFeedback) {
      ExpFeed(workspaceID, deploymentSystem, expFeedbackSince);
    } else if (expFiles) {
      exportStaticFiles(workspaceID);
    } else {
      // export the applications
      exportFiles(appID, workspaceID, userName);
    }
    //
    // When we're done, close the connection
    //
  }

  public String usage() throws Exception {
    StringBuilder sb = new StringBuilder();
    // write("Usage APEXExport -db -user -password -applicationid -workspaceid
    // -instance -expWorkspace -expMinimal -expFiles -skipExportDate
    // -expPubReports -expSavedReports -expIRNotif -expTranslations
    // -expTeamdevdata -expFeedback -deploymentSystem -expFeedbackSince
    // -expOriginalIds -debug ");
    // write(" -db: Database connect url in JDBC format ");
    // write(" -user: Database username");
    // write(" -password: Database password");
    sb.append("    -applicationid:    ID for application to be exported\n");
    sb.append("    -workspaceid:      Workspace ID for which all applications to be exported or the workspace to be exported\n");
    sb.append("    -instance:         Export all applications\n");
    sb.append("    -expWorkspace:     Export workspace identified by -workspaceid or all workspaces if -workspaceid not specified\n");
    sb.append("    -expMinimal:       Only export workspace definition, users, and groups\n");
    sb.append("    -expFiles:         Export all workspace files identified by -workspaceid\n");
    sb.append("    -skipExportDate:   Exclude export date from application export files\n");
    sb.append("    -expPubReports:    Export all user saved public interactive reports\n");
    sb.append("    -expSavedReports:  Export all user saved interactive reports\n");
    sb.append("    -expIRNotif:       Export all interactive report notifications\n");
    sb.append("    -expTranslations:  Export the translation mappings and all text from the translation repository\n");
    sb.append("    -expFeedback:      Export team development feedback for all workspaces or identified by -workspaceid to development or deployment\n");
    sb.append("    -expTeamdevdata:   Export team development data for all workspaces or identified by -workspaceid\n");
    sb.append("    -deploymentSystem: Deployment system for exported feedback\n");
    sb.append("    -expFeedbackSince: Export team development feedback since date in the format YYYYMMDD\n");
    sb.append("    -expOriginalIds:   If specified, the application export will emit ids as they were when the application was imported\n");
    sb.append("    -split:            Split the exported file\n");
    sb.append("    -splitFlat:        Split with no directory strucure\n");
    sb.append("    -splitUpdate:      Generate update.sql file while splitting\n");
    sb.append("    -splitNoCheckSum:  Overwrite all files\n");    
    sb.append("    \n");
    sb.append(" Application Example:            apex export -applicationid 31500 \n");
    sb.append(" Workspace Example:              apex export -workspaceid 9999\n ");
    sb.append(" Instance Example:               apex export -instance \n");
    sb.append(" Export All Workspaces Example:  apex export -expWorkspace \n");
    sb.append(" Export Feedback to development environment:\n");
    sb.append("         apex export -workspaceid 9999 -expFeedback \n");
    sb.append(" Export Feedback to deployment environment EA2 since 20100308:\n");
    sb.append("         apex export -workspaceid 9999 -expFeedback -deploymentSystem EA2 -expFeedbackSince 20100308 \n");
    // System.exit(1);
    return sb.toString();
  }



  public  void processArgs(String[] args) throws Exception {
    //
    // If there aren't three arguments, display usage and terminate the program
    //
    // String url = null;
    // String userName = null;
    // String password = null;
    // String deploymentSystem = null;
    // BigDecimal appID = null;
    // java.util.Date expFeedbackSinceU = null;
    // java.sql.Date expFeedbackSince = null;
    validRequest(true);
    for (int i = 0; args != null && i < args.length && isValid(); i++) {
      if (debug)
        write("Parameter:" + args[i]);

      if (args[i].equalsIgnoreCase("-db")) {
        url = args[++i];
      } else if (args[i].equalsIgnoreCase("-user")) {
        userName = args[++i];
      } else if (args[i].equalsIgnoreCase("-password")) {
        password = args[++i];
      } else if (args[i].equalsIgnoreCase("-workspaceid")) {
        workspaceID = new BigDecimal(args[++i]);
      } else if (args[i].equalsIgnoreCase("-applicationid")) {
        appID = new BigDecimal(args[++i]);
      } else if (args[i].equalsIgnoreCase("-debug")) {
        debug = true;
      } else if (args[i].equalsIgnoreCase("-skipExportDate")) {
        skipDate = true;
      } else if (args[i].equalsIgnoreCase("-expPubReports")) {
        pubReports = true;
      } else if (args[i].equalsIgnoreCase("-expSavedReports")) {
        savedReports = true;
      } else if (args[i].equalsIgnoreCase("-expIRNotif")) {
        IRNotifications = true;
      } else if (args[i].equalsIgnoreCase("-expTranslations")) {
        expTranslations = true;
      } else if (args[i].equalsIgnoreCase("-instance")) {
        instance = true;
      } else if (args[i].equalsIgnoreCase("-expWorkspace")) {
        expWorkspace = true;
      } else if (args[i].equalsIgnoreCase("-expMinimal")) {
        expMinimal = true;
      } else if (args[i].equalsIgnoreCase("-expFiles")) {
        expFiles = true;
      } else if (args[i].equalsIgnoreCase("-expFeedback")) {
        expFeedback = true;
      } else if (args[i].equalsIgnoreCase("-expTeamdevdata")) {
        expTeamdevdata = true;
      } else if (args[i].equalsIgnoreCase("-deploymentSystem")) {
        deploymentSystem = args[++i];
      } else if (args[i].equalsIgnoreCase("-splitNoCheckSum")) {
        _splitNoCheckSum = true;
      } else if (args[i].equalsIgnoreCase("-splitUpdate")) {
        _splitUpdate = true;
      } else if (args[i].equalsIgnoreCase("-splitFlat")) {
        _splitFlat = true;
      } else if (args[i].equalsIgnoreCase("-split")) {
        _split = true;
      } else if (args[i].equalsIgnoreCase("-expFeedbackSince")) {
        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
        try {
          expFeedbackSinceU = dtFmt.parse(args[++i]);
          expFeedbackSince = new java.sql.Date(expFeedbackSinceU.getTime());
        } catch (ParseException e) {
          write("Invalid date format: " + args[++i]);
        }
      } else if (args[i].equalsIgnoreCase("-expOriginalIds")) {
        expOriginalIds = true;
      } else if (args[i].equalsIgnoreCase("-expLocked")) {
        expLocked = true;
      } else {
        write(usage());
        validRequest(false);
      }

    }

    if (debug) {
      write(url);
      write(userName);
      write(password);
      write(workspaceID);
      write(appID);
      write(skipDate);
      write(pubReports);
      write(savedReports);
      write(IRNotifications);
      write(expTranslations);
      write(instance);
      write(expWorkspace);
      write(expMinimal);
      write(expFiles);
      write(expFeedback);
      write(expTeamdevdata);
      write(deploymentSystem);
      write(expFeedbackSinceU);
    }

    if ((appID == null || appID.longValue() == 0) && (workspaceID == null || workspaceID.longValue() == 0)) {
      validRequest(false);
    } else {
      validRequest(true);
    }

  }

  private void write(Object o) throws UnsupportedEncodingException, IOException {
    _out.write(o.toString().getBytes("UTF8"));
  }

  public void setOutStream(OutputStream out) {
    _out = out;
  }

  private void validRequest(boolean b) {
    _valid = b;
  }

  public boolean isValid() {
    return _valid;
  }

  public void setConnection(Connection conn) {
    gConn = conn;
  }

  public static void main(String[] args) throws Exception {
    APEXExport t = new APEXExport();
    t.processArgs(args);

    t.setOutStream(System.out);
    //
    // Load the Oracle JDBC driver
    //
    DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
    t.gConn = DriverManager.getConnection("jdbc:oracle:thin:@" + t.url, t.userName, t.password);

    //
    // Turn on autocommit.
    //
    t.gConn.setAutoCommit(true);

    t.export();

    //
    // When we're done, close the connection
    //
    t.gConn.close();
    System.exit(0);
  }

}
