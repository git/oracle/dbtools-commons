/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.apex;

/**
 * <code>APEXExportSplitter</code>
 * <p>
 * .....
 * <p>
 * @author Kris Rice
 * @since 2.2
 * @version $Revision: 1.1 $
 *
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashSet;
import java.util.zip.Adler32;
import java.util.zip.CheckedInputStream;


public class APEXExportSplitter {

    private  boolean flatten = false;

    private  boolean checksum = true;

    private  boolean debug = false;

    private  boolean update = false;

    private  String baseFolder;

    private  FileOutputStream updateFile;

    public void setOptions(String baseFolder , boolean flat, boolean checksum,boolean debug,boolean update){
      this.flatten = flat;
      this.checksum = checksum;
      this.debug = debug;
      this.update = update;
      this.baseFolder = baseFolder;
    }
    //
    private  void printUpdate(String s) throws Exception {
        if (update) {
            if (updateFile == null) {
                // create the update file
                // this is building a date to use in the filename
                StringBuffer sb = new StringBuffer();
                sb.append(Calendar.getInstance().get(Calendar.MONTH));
                sb.append("-");
                sb.append(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                sb.append("-");
                sb.append(Calendar.getInstance().get(Calendar.YEAR));
                sb.append("-");
                sb.append(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
                sb.append("-");
                sb.append(Calendar.getInstance().get(Calendar.MINUTE));
                sb.append("-");
                sb.append(Calendar.getInstance().get(Calendar.SECOND));

                // create update filename
                String f = baseFolder + File.separator + "update-" + sb.toString() + ".sql";

                // check for flatten
                if (flatten) {
                    f = f.replaceAll("/", "_");
                }

                // create the file
                updateFile = new FileOutputStream(new File(f));
                updateFile.write("@init.sql \r\n".getBytes());
                updateFile.write("@env.sql \r\n".getBytes());
            }
            // write the changed file
            updateFile.write(("@" + s + " \r\n").getBytes());
        }
    }

    // log
    private  void log(String s) {
        // common procedure to log if in debug mode
        if (debug)
            System.out.print(s);
    }

    // Copies src file to dst file.
    // If the dst file does not exist, it is created
    private static void copy(File src, File dst) {
        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private  void checkFileForChanges(File file, String filename) throws Exception {
        // the file is the buffer which was just written
        // the filename is where the file will end up
        File dest = new File(filename);
        // set the move to default true
        boolean move = true;
        // if the dest does exist and nochecksum was not set on the cmd line
        if (dest.exists() && checksum) {
            // check sum the 2 files and compare.
            // if they are the same a copy is not needed so the move is set to false
            if (getCheckSum(file) == getCheckSum(dest))
                move = false;
        }
        // if move is still true and nochecksum was not set on the cmd line
        // move the file
        if (move || !checksum) {
            log("---Renaming... to " + filename + "\n");
            copy(file, dest);
            // create a update file
            printUpdate(filename);
        }
        // remove temp file
        file.delete();
    }

    private static long getCheckSum(File file) {
        long checksum = 0;
        try {
            // Compute Adler-32 checksum
            CheckedInputStream cis = new CheckedInputStream(new FileInputStream(file), new Adler32());
            byte[] tempBuf = new byte[128];
            while (cis.read(tempBuf) >= 0) {}
            checksum = cis.getChecksum().getValue();
        }
        catch (IOException e) {}
        return checksum;
    }

    public  void processFile(String fileName) throws IOException, Exception{
        // make a reader for the file to be processed
        BufferedReader in = new BufferedReader(new FileReader(fileName));
        String line = null;
        File tmpFile = null;

        // grab the base file name
        baseFolder = fileName.substring(0, fileName.indexOf("."));

        // the init.sql file declared
        // this file will be in the begining of the file
        // for things like setting security group id
        String initFileName = "application/" + "init.sql";

        // if the -flat was passed change all "/" to "_"
        // so that the files all end up in one dir but with the same basic structure
        if (flatten) {
            initFileName = initFileName.replaceAll("/", "_");
        }

        // this buffer will be the main run script
        // all file generated will be referenced
        StringBuffer runScript = new StringBuffer("@" + initFileName + "\n");

        // create the name of the initfile
        initFileName = baseFolder + "/" + initFileName;

        // var for the current file name being written
        String currentFileName = null;

        // var for the current file writter being written
        FileWriter currentWriter = null;

        // var for the init file being written
        StringBuffer init = new StringBuffer();

        // vars for detecting duplicate file names
        String currentDir = "";
        HashSet<String> currentDirFiles = new HashSet<String>(100);

        while ((line = in.readLine()) != null) {
            // if --application is found. It's time to switch the writter to a new file
            if (line.indexOf("prompt --application") == 0 ||
                line.indexOf("--application") == 0)
            {
                // the first time the writter will be null since the content is going to the init.
                // if it's not null flush it and  check for changes via the checksum
                if (currentWriter != null) {
                    currentWriter.flush();
                    currentWriter.close();
                    // check to see if anything changed
                    checkFileForChanges(tmpFile, currentFileName);
                }
                //
                //1) trim the -- from the --application string
                //2) replace all "." with nothing ""
                //3) replace spaces " " with "_" . This is for nicer filenames
                currentFileName = line.substring(line.indexOf("--") + 2).trim().replaceAll("\\.", "").replaceAll(" ", "_");

                // Check the list of already processed files in the current directory for duplicates. If we
                // find a duplicate, let's use a new file name by appending a sequence number.
                if (!currentDir.equals(currentFileName.substring(0, currentFileName.lastIndexOf("/"))) ) {
                    currentDir = currentFileName.substring(0, currentFileName.lastIndexOf("/") );
                    currentDirFiles.clear();
                }
                String baseFileName = currentFileName;
                int i = 1;
                while (currentDirFiles.contains(currentFileName) ) {
                    currentFileName = baseFileName + "_" + ++i;
                }
                currentDirFiles.add(currentFileName);

                // add a .sql
                currentFileName = currentFileName + ".sql";


                // if the -flat was passed change all "/" to "_"
                // so that the files all end up in one dir but with the same basic structure
                if (flatten) {
                    currentFileName = currentFileName.replaceAll("/", "_");
                }

                // create the full filename for the current file
                currentFileName = baseFolder + "/" + currentFileName;

                // make sure dir is created
                File dir = new File(currentFileName.substring(0, currentFileName.lastIndexOf("/")));

                // recursively make the dirs needed
                dir.mkdirs();

                // add the currently processing file to the run script
                runScript.append("@" + currentFileName.substring(currentFileName.indexOf("/") + 1) + "\n");

                // create a temp file which will later be moved to it's final destination
                tmpFile = File.createTempFile("apex", ".sql");

                // log info
                log("--Processing:" + currentFileName + "\n");

                // create the file writter for the current file
                currentWriter = new FileWriter(tmpFile);
            }
            // if the writer is null we're processing the init so add it to the init buffer
            if (currentWriter != null) {
                currentWriter.write(line + "\n");
            } else {
                init.append(line + "\n");
            }
        }
        currentWriter.flush();
        currentWriter.close();
        // jstraub - 02/05/2007 for bug 5862484
        checkFileForChanges(tmpFile, currentFileName);

        // write the runner script
        FileWriter file = new FileWriter(initFileName);
        file.write(init.toString());
        file.flush();
        file.close();

        // write the runner script
        String installFileName = "install.sql";

        //if the -flat was passed change all "/" to "_"
        // so that the files all end up in one dir but with the same basic structure
        if (flatten) {
            installFileName = installFileName.replaceAll("/", "_");
        }

        // write the main run script
        file = new FileWriter(baseFolder + "/" + installFileName);
        file.write(runScript.toString());
        file.flush();
        file.close();

        // if creating and update<date>.sql file close it.
        if (updateFile != null)
            updateFile.close();
    }

    public static void usage() {
        System.out.println("-help       : < print this syntax >");
        System.out.println("-flat       : < flat file structure >");
        System.out.println("-debug      : < print debug info >");
        System.out.println("-update     : < create update file >");
        System.out.println("-nochecksum : < don't check for changes >");
        System.exit(0);
    }

    public static void main(String[] args) throws Exception {
        // no arg passed nothing to do.
        // print usage
        if (args.length == 0) {
            usage();
        }
        APEXExportSplitter s = new APEXExportSplitter();
        // loop args which were passed in and set flag accordingly
        String arg;
        for (int i = 0;i<args.length;i++) {
            arg = args[i];
            if (arg.equals("-flat")) {
                s.flatten = true;
                System.out.println("Using flat structure");
            } else if (arg.equals("-nochecksum")) {
              s.checksum = false;
            } else if (arg.equals("-debug")) {
              s.debug = true;
            } else if (arg.equals("-update")) {
              s.update = true;
            } else if (arg.equals("-help")) {
                usage();
            }
        }

        for (int i = 0;i<args.length;i++) {
            arg = args[i];
            // loop the args to process the files
            // if the arg start with "-" skip it since it's probably a flag and not a file
            if (arg.indexOf("-") == -1) {
                try {
                    System.out.println("Processing:" + arg);
                    // process the file
                    s.processFile(arg);
                }
                catch (Exception e) {
                    System.err.println("Could not process:" + arg);
                    System.err.println(e.getMessage());
                    if (s.debug) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }
}
