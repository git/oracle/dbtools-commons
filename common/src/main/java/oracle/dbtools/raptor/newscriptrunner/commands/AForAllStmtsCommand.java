/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.StringTokenizer;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=AForAllStmtsCommand.java">Barry McGillin</a> 
 *
 */
public abstract class AForAllStmtsCommand extends CommandListener  implements IShowMode {
    private SQLCommand.StmtSubType m_cmdStmtSubType;
    protected boolean m_isCmdOn = false;

    public AForAllStmtsCommand(SQLCommand.StmtSubType cmd) {
        m_cmdStmtSubType = cmd;
    }
    
    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if (cmd.getStmtSubType() == getStmtSubType()) {
            boolean isHandled = doHandleCmd(conn, ctx, cmd);
            if (isHandled && cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN) != null) {
                m_isCmdOn = (Boolean) cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN);
            }
            return isHandled;
        } else if (isListenerOn(conn, ctx, cmd)) {
            return doHandleWatcher(conn, ctx, cmd);
        } else {
            return false;
        }
    }
    
    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if (cmd.getStmtSubType() == getStmtSubType()) {
            doBeginCmd(conn, ctx, cmd);
        } else if (isListenerOn(conn, ctx, cmd)) {
            doBeginWatcher(conn, ctx, cmd);
        }
    }
    
    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if (cmd.getStmtSubType() == getStmtSubType()) {
            doEndCmd(conn, ctx, cmd);
        } else if (isListenerOn(conn, ctx, cmd)) {
            doEndWatcher(conn, ctx, cmd);
        }
    }
    
    protected boolean isListenerOn(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        return m_isCmdOn;
    }
    
    protected boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        return true;
    }
    
    protected boolean doHandleWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        return false;
    }
    
    protected void doBeginCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    protected void doBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    protected void doEndCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    protected void doEndWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    private StmtSubType getStmtSubType() {
        return m_cmdStmtSubType;
    }

    /**
     * getters and setter added by totierne so that the command could be programatically
     * switched off on exit.
     * @return
     */
    public boolean isCmdOn() {
        return m_isCmdOn;
    }

    public void setCmdOn(boolean cmdOn) {
        m_isCmdOn = cmdOn;
    }
    
    @Override
   	public void setProperties(ISQLCommand cmd, ScriptRunnerContext ctx, String oldvalue, String printableString, String contextProperty) {
   		cmd.setProperty(IShowMode.OLDVALUE, oldvalue);
   		cmd.setProperty(IShowMode.PRINTVALUE, printableString);
   		cmd.setProperty(IShowMode.CTX_SET_STRING, contextProperty);
   	}
    
    public void writeShowMode(ISQLCommand cmd, ScriptRunnerContext ctx, String newVal) {
		String showmode = (String) ctx.getProperty(ScriptRunnerContext.SETSHOWMODE);
		 if ( showmode.matches("(?i:on|both)") && cmd.isSqlPlusSetCmd()) {
			ctx.write("old: "+ cmd.getProperty(IShowMode.PRINTVALUE) +" "+ cmd.getProperty(IShowMode.OLDVALUE)+"\n");  //$NON-NLS-1$  //$NON-NLS-2$
     		ctx.write("new: "+ cmd.getProperty(IShowMode.PRINTVALUE) +" "+ newVal +"\n"); //$NON-NLS-1$  //$NON-NLS-2$
		 }
    }

}
