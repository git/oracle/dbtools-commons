/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLPLUS;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;

/**
 * ACCEPT
 * <p>
 * ------
 * <p>
 * <p>
 * Reads a line of input and stores it in a given substitution variable. In
 * iSQL*Plus, displays the Input Required screen for you to enter a value for
 * the substitution variable.
 * <p>
 * <p>
 * 
 * ACC[EPT] variable [NUM[BER] | CHAR | DATE | BINARY_FLOAT | BINARY_DOUBLE]
 * <p>
 * [FOR[MAT] format] [DEF[AULT] default] [PROMPT text | NOPR[OMPT]]
 * <p>
 * [HIDE]
 * <p>
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Accept.java"
 *         >Barry McGillin</a>
 * 
 */
public class Accept extends CommandListener {

	private ScriptRunnerContext m_ctx = null;
	private Connection m_conn = null;

	public static String[][] driver= new String[][] {
		{null,"NUM","NUMB","NUMBE","NUMBER","CHAR","DATE","BINARY_FLOAT","BINARY_DOUBLE"},  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$  //$NON-NLS-5$  //$NON-NLS-6$  //$NON-NLS-7$  //$NON-NLS-8$
		{"true","FOR","FORM","FORMA","FORMAT"},  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$  //$NON-NLS-5$  
		{"true","DEF","DEFA","DEFAU","DEFAUL","DEFAULT"},  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$  //$NON-NLS-5$  
		{"true","PROMPT"},  //$NON-NLS-1$ 
		{null,"NOPROMPT"},  //$NON-NLS-1$ 
		{null,"HIDE"}};  //$NON-NLS-1$ 
	private static int TYPE=0;
	private static int FORMAT=1;
	private static int DEFAULT=2;
	private static int PROMPT=3;
	private static int NOPROMPT=4;
	private static int HIDE=5;
	
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		m_ctx = ctx;
		m_conn = conn;
		try{
		    runAccept(ctx, cmd);
		} catch (NoConnectionException e) {
			ctx.write(Messages.getString("ACCEPTNOCONNECTION"));
			//call it a sqlerror if anyone cares this is a new error sqlplus validates without a connection
			ScriptUtils.doWhenever(ctx, cmd, conn, true);
		}
		return true;
	}

	/**
	 * Null implementation. This is not needed to create the accept
	 * functionality.
	 * 
	 * @param conn
	 *            {@link Connection}
	 * @param ctx
	 *            {@link ScriptRunnerContext}
	 * @param cmd
	 *            {@link ISQLCommand}
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/**
	 * Null implementation. This is not needed to create the accept
	 * functionality.
	 * 
	 * @param conn
	 *            {@link Connection}
	 * @param ctx
	 *            {@link ScriptRunnerContext}
	 * @param cmd
	 *            {@link ISQLCommand}
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	private String stripIfNumber(String type, String val) {
		if ((type!=null)&&(val!=null) &&
				((type.equals("NUMBER")|| //$NON-NLS-1$
						(type.equals("BINARY_FLOAT")|| //$NON-NLS-1$
								(type.equals("BINARY_DOUBLE")))))) { //$NON-NLS-1$
			val=val.trim();
		}
		return val;
	}
	
	private String zeroIfNumber(String type, String val) { //$NON-NLS-1$
		if ((type!=null)&&((val==null)||(val.equals(""))) &&
				((type.equals("NUMBER")|| //$NON-NLS-1$
						(type.equals("BINARY_FLOAT")|| //$NON-NLS-1$
								(type.equals("BINARY_DOUBLE")))))) { //$NON-NLS-1$
			val="0";
		}
		return val;
	}
	private void runAccept(ScriptRunnerContext ctx, ISQLCommand cmd) throws NoConnectionException {
		//uses connection lots to validate types
		//could work for no explicit or implicit mask/cast no type conversion"
     
		// accept variable possibilities prompt text or noprompt
		// text can be quoted
		boolean hide = false;
		String args[] = cmd.getSQLOrig().trim().split("\\s+"); //$NON-NLS-1$
		if (args.length == 1 && args[0].toLowerCase().equals("accept")) { //$NON-NLS-1$
			write(Messages.getString("Accept.2"), m_ctx); //$NON-NLS-1$
			ctx.errorLog(ctx.getSourceRef(), Messages.getString("Accept.2"), cmd.getSql());
			return;
		}
		cmd.setSql(SQLPLUS.removeDashNewline(cmd,cmd.getSql()));
		String localVarName=null;
		List<LexerToken> src = LexerToken.parse(cmd.getSql(),false);//happy days handles comments
		if ((src.size()>1)&&!(src.get(1).content.trim().equals(""))) { //$NON-NLS-1$
			localVarName = src.get(1).content.trim();
			String [][] firstPass=parse(ctx,cmd,src,cmd.getSql());
			if (firstPass==null) {
				//error written in parse;//need to fix that so it is not just system out
				return;
			}
			// need to get prompt if there is one
			if (firstPass[HIDE][0]!=null) { //$NON-NLS-1$
				hide = true;
			}
			//NOPROMPT-ignore
			
			//int index = restUpper.indexOf("PROMPT"); //$NON-NLS-1$
			String promptText = ""; //$NON-NLS-1$
			boolean errorOut = false;
			if (firstPass[PROMPT][0]!=null) {
				//looks like prompt string error is not standard need to customise prepare generic error
				//error template						write(ScriptRunnerContext.lineErr( new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.PROMPT_STRING_NOT_FOUND), m_ctx), m_ctx);
				promptText=firstPass[PROMPT][1];
			}
			String defaultText = ""; //$NON-NLS-1$
			if (errorOut == false) {
				if (firstPass[DEFAULT][0]!=null) {
					defaultText = firstPass[DEFAULT][1];
				}
			}
			String formatString = null;
			if (errorOut == false) {
				if (firstPass[FORMAT][0]!= null) {
					formatString = firstPass[FORMAT][1];
				}
			}
			String requiredType = null;
			if (errorOut == false) {
				if (firstPass[TYPE][0]!=null) {
					String possibleType = firstPass[TYPE][0];
					if (possibleType.equals("NUM") || //$NON-NLS-1$
							possibleType.equals("NUMB") || //$NON-NLS-1$
							possibleType.equals("NUMBE") || //$NON-NLS-1$
							possibleType.equals("NUMBER")) { //$NON-NLS-1$
						requiredType = "NUMBER"; //$NON-NLS-1$
					} else if (possibleType.equals("DATE") || //$NON-NLS-1$
							possibleType.equals("BINARY_FLOAT") || //$NON-NLS-1$
							possibleType.equals("BINARY_DOUBLE")) { //$NON-NLS-1$
						requiredType = possibleType;
					}
				}
			}
			//the ACCEPT command has been parsed.
			if (errorOut == false) {
				//if date ser format to default forma
				if (requiredType!=null&&requiredType.equals("DATE") && (formatString==null||formatString.equals(""))) {  //$NON-NLS-1$  //$NON-NLS-2$
					   if (((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE))))) {
						   formatString="DD-MON-RR";
					   } else {
					formatString=DBUtil.getInstance(ctx.getCurrentConnection())
							.executeReturnOneCol("SELECT value FROM   nls_session_parameters WHERE  parameter = 'NLS_DATE_FORMAT'");  //$NON-NLS-1$
					   }
					//possible issues - should catch nolog somewhere?
				}
				String retVal = null;
				boolean validated = false;
				String lastError = null;
				defaultText=stripIfNumber(requiredType,defaultText);
				defaultText=zeroIfNumber(requiredType,defaultText);
				boolean skipDefaultCheck=false;
				//verify that the ACCEPT command has a valid default and format value
				if ( requiredType!=null&&requiredType.toUpperCase().startsWith("DATE") 
						   && (!((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE))))) 
						) {
					//verify format
					DBUtil dbUtil=DBUtil.getInstance(ctx.getCurrentConnection());
					ArrayList<String> bindL=new ArrayList<String>();
					bindL.add(formatString);
					dbUtil.getLastException();
					
					   if (!((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE))))) {
						   String notUsed=dbUtil
									.executeReturnOneCol("SELECT to_char(sysdate,:1) from Dual", //$NON-NLS-1$ 
											bindL);  
							SQLException e=dbUtil.getLastException();
							if (e!=null) {
								//SP2-0597: "ddzzyy" is not a valid DATE format
								errorWrap(ctx,cmd,ScriptRunnerDbArb.format(ScriptRunnerDbArb.ACCEPT_BAD_DATE_MASK, formatString)+"\n");  //$NON-NLS-1$	
								return;
							}
				        }
					
					if ((defaultText==null)||defaultText.equals("")) {
						skipDefaultCheck=true;
					}
				}
				if ((!skipDefaultCheck)&&(!validateDefaultAndFormat(defaultText, formatString, requiredType,ctx,cmd))) {
					return; //dont proceed to prompt for value. The ACCEPT command needs to be valid
				}

				while (!validated) {
					String errorAndPrompt = null;
					//					if (lastError != null) {
					//						errorAndPrompt = lastError + "\n" + promptText; //$NON-NLS-1$
					//					} else {
					errorAndPrompt = promptText;
					//					}

					retVal = m_ctx.getPromptedFieldProvider().getPromptedField(m_ctx, errorAndPrompt, hide);
					if ((retVal==null)&&(hide)) {
						retVal=""; //$NON-NLS-1$
					}
					if (retVal == null) {
						String wasCalled="Commit"; //$NON-NLS-1$
						/* pressed cancel */
						if (m_conn!=null) {
							if (LockManager.lock(m_conn)) {
								try {
							    	if (m_ctx.getProperty(ScriptRunnerContext.EXITCOMMIT)!=null &&m_ctx.getProperty(ScriptRunnerContext.EXITCOMMIT).equals("OFF")) { //$NON-NLS-1$
										wasCalled="Rollback"; //$NON-NLS-1$
										m_conn.rollback();
									} else {
										m_conn.commit();
									}
								} catch (SQLException e) {
								    //report(ScriptRunnerDbArb.format(ScriptRunnerDbArb.ERROR_ON, "Commit")); //$NON-NLS-1$
								    write(ScriptRunnerDbArb.format(ScriptRunnerDbArb.ERROR_ON, wasCalled), m_ctx); //$NON-NLS-1$
							    } finally {
									LockManager.unlock(m_conn);
								}
							}
						}
						m_ctx.setExited(true);
						return;
					} else if (retVal.equals("")) { //$NON-NLS-1$
						retVal = defaultText;
					}
					if ((retVal == null /* || retVal.equals("") "" matchs a20*/) && (formatString!=null && !formatString.equals(""))) { //$NON-NLS-1$ //$NON-NLS-2$
						write(MessageFormat.format("SP2-0598:\"\" does not match input format \"{0}\"\n", formatString), m_ctx); //$NON-NLS-1$
						ctx.errorLog(ctx.getSourceRef(), MessageFormat.format("SP2-0598:\"\" does not match input format \"{0}\"\n", formatString), cmd.getSql());
					} else {
						int errorNumber = 0;
						retVal=stripIfNumber(requiredType,retVal);
						errorNumber = acceptValidate(retVal, requiredType, formatString);
						validated = errorNumber == 0 ? true : false;
						lastError = null;
						String maybeMaskedInput = retVal;
						if (hide) {
							maybeMaskedInput = "*********"; //$NON-NLS-1$
						}
						if ((!validated)&&(requiredType!=null)&&(requiredType.equals("DATE"))) { //$NON-NLS-1$
							//SP2-0685: The date "22" is invalid or format mismatched "DD-MON-RR"
							errorWrap(ctx,cmd,ScriptRunnerDbArb.format(ScriptRunnerDbArb.ACCEPT_DATE_ERROR, maybeMaskedInput, formatString)+"\n");  //$NON-NLS-1$	
						} else if(errorNumber == 1722 && (formatString!=null && !formatString.equals(""))) { //$NON-NLS-1$
				        	  if ((ctx!=null && 
				            			ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null && 
				            			Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString()))){
				        		  writeAndFlush(MessageFormat.format("SP2-0598: \"{0}\" does not match input format \"{1}\"\n",retVal,formatString), m_ctx); //$NON-NLS-1$
				        			ctx.errorLog(ctx.getSourceRef(), MessageFormat.format("SP2-0598:\"\" does not match input format \"{0}\"\n", formatString), cmd.getSql());
				        	  } else {
				        		  writeAndFlush(MessageFormat.format("SP2-0598: \"{0}\" does not match input format \"{1}\"\n",maybeMaskedInput,formatString), m_ctx); //$NON-NLS-1$
				        			ctx.errorLog(ctx.getSourceRef(), MessageFormat.format("SP2-0598:\"\" does not match input format \"{0}\"\n", formatString), cmd.getSql());
				        	  }
						} else  if (errorNumber == -1 || errorNumber > 0) {
							// report(ScriptRunnerDbArb.format(ScriptRunnerDbArb.LINE_COMMAND_ERR,
							// new Integer(cmd.getStartLine() + 1),
							// cmd.getSQLOrig(),
							// ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ACCEPT_STATEMENT_MUST_SPECIFY)));
							 if(errorNumber ==1722){
								 errorWrap(ctx,cmd,MessageFormat.format(Messages.getString("Accept.19_gen"),retVal, requiredType.toUpperCase(Locale.US))); //$NON-NLS-1$
								 ctx.errorLog(ctx.getSourceRef(), MessageFormat.format(Messages.getString("Accept.19_gen"),retVal, requiredType.toUpperCase(Locale.US)), cmd.getSql());
							 } else {
								 errorWrap(ctx,cmd,ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ACCEPT_STATEMENT_MUST_SPECIFY)+"\n");  //$NON-NLS-1$
								 ctx.errorLog(ctx.getSourceRef(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ACCEPT_STATEMENT_MUST_SPECIFY)+"\n", cmd.getSql());

							 }
						}
						if (validated) {
							Map<String, String> m = m_ctx.getMap();
							m.put(localVarName.toUpperCase(Locale.US), retVal);
						}
					}
				}
			}
		} else {
			// report(ScriptRunnerDbArb.format(ScriptRunnerDbArb.LINE_COMMAND_ERR,
			// new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(),
			// ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ACCEPT_STATEMENT_MUST_SPECIFY)));
			write(ScriptRunnerContext.lineErr( new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(), ScriptRunnerDbArb.getString(ScriptRunnerDbArb.ACCEPT_STATEMENT_MUST_SPECIFY)+"\n", m_ctx), m_ctx);  //$NON-NLS-1$	
			writeAndFlush("\n", m_ctx);//$NON-NLS-1$
		}
		// SQL> help accept
		//
		// ACCEPT
		// ------
		//
		// Reads a line of input and stores it in a given substitution variable.
		// In iSQL*Plus, displays the Input Required screen for you to enter a
		// value for the substitution variable.
		//
		// ACC[EPT] variable [NUM[BER] | CHAR | DATE | BINARY_FLOAT |
		// BINARY_DOUBLE]
		// [FOR[MAT] format] [DEF[AULT] default] [PROMPT text | NOPR[OMPT]]
		// [HIDE]
		//
	}

	private static void writeAndFlush(String string, ScriptRunnerContext ctx) {
		write(string,ctx);
		try {
			ctx.getOutputStream().flush();
		} catch (IOException e) {
			Logger.getLogger(new Accept().getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}
	}

	private boolean validateDefaultAndFormat(String defaultText, String formatString, String dataType, ScriptRunnerContext ctx, ISQLCommand cmd) throws NoConnectionException {
		if( (defaultText == null || defaultText.equals("")) && //$NON-NLS-1$
				(formatString == null || formatString.equals(""))) { //$NON-NLS-1$
			return true; //no validation test required as there is no formatString or default value
		}
		int errorNumber = 0;
		String value = defaultText;
		if(value == null || value.equals("")){ //$NON-NLS-1$
		  if ( dataType!=null&&dataType.toUpperCase().startsWith("DATE") ) { //$NON-NLS-1$
		  	value = "";//-> sql NULL in to_DATE(:x,'ddmonyyyy') "sysdate"; //$NON-NLS-1$
		  } else {
		  	value = "0"; //$NON-NLS-1$
		  }
		}
		errorNumber = acceptValidate(value,dataType,formatString);
		 if(errorNumber > 0){
			 if(errorNumber ==1722){
				 if(formatString == null || formatString.equals("")){ //$NON-NLS-1$
					 writeAndFlush(MessageFormat.format(Messages.getString("Accept.19"),value),m_ctx); //$NON-NLS-1$
						ctx.errorLog(ctx.getSourceRef(), MessageFormat.format(Messages.getString("Accept.19"),value), cmd.getSql());

				 } else {
					 writeAndFlush(MessageFormat.format(Messages.getString("Accept.20"),value, formatString), m_ctx); //$NON-NLS-1$
					 ctx.errorLog(ctx.getSourceRef(), MessageFormat.format(Messages.getString("Accept.20"),value), cmd.getSql());
				 }
			 } else { // (errorNumber  == ){
				 writeAndFlush(MessageFormat.format(Messages.getString("Accept.21"), formatString,dataType),m_ctx); //$NON-NLS-1$
				 ctx.errorLog(ctx.getSourceRef(), MessageFormat.format(Messages.getString("Accept.21"),value), cmd.getSql());
			 }
		 } else if ((errorNumber==-1) && (formatString!=null) && (!formatString.equals(""))) { //$NON-NLS-1$
			 writeAndFlush(MessageFormat.format(Messages.getString("Accept.ValueFormat"), value, formatString),m_ctx); //$NON-NLS-1$
			 ctx.errorLog(ctx.getSourceRef(), MessageFormat.format(Messages.getString("Accept.ValueFormat"),value), cmd.getSql());
		 }
		return errorNumber == 0?true:false;
	}

	private int acceptValidate(String promptResult, String requiredType, String formatString) throws NoConnectionException{
		int ACCEPTED  = 0;
		int INVALID = -1;
		int BAD = -2;
		// char needs to be handled separately as to_char(char,format does not
		// make sense assume format is annn so easy to process everything else
		// is select TO_REQUIRED_TYPE(:1 promptResult as a bind string, format
		// as a bind string and it will error out on format first if that is an
		// error and format not matching prompt with a different oerr I really
		// do not want the result select 1 from dual where exist (select
		// to_number('$9,123','$9,999') from
		try {
			if ((requiredType == null)) { // default char
				if ((formatString != null) && ((formatString.matches("^[aA][0-9]*$")))) { //$NON-NLS-1$
					String formatNumber = formatString.replaceAll("^[aA]([0-9]*)$", "$1"); //$NON-NLS-1$ //$NON-NLS-2$
					Integer formatInt = null;
					try {
						formatInt = Integer.parseInt(formatNumber);
					} catch (NumberFormatException nfe) {
						return  INVALID;
					}
					if (promptResult.matches(".{0," + formatInt + "}")) { //$NON-NLS-1$ //$NON-NLS-2$
						return ACCEPTED;
					} else {
						return 1722;/*1722 does not match input format*/
					}
				} else {
					if ((formatString != null) && (!(formatString.matches("^[aA][0-9]*$")))) { //$NON-NLS-1$
						return INVALID;
					}
				}
				return 0;
			} else {
				if (requiredType!=null&&requiredType.equals("DATE")&&((promptResult==null)||promptResult.equals(""))) {  //$NON-NLS-1$ //$NON-NLS-2$
					return 1722;
				}
				// 2/select TO_CHAR(TO_<type>(value,format)) from dual
				// to catch and report on value (ask for value again) and
				// format(error out) errors.
				// Use the input value to continue.
				// There are edge cases I may miss / handle differently.

				// select TO_CHAR(TO_<type>(value,format)) from dual
				// select TO_CHAR(TO_<type>(:v,:f)) or if no f
				// select TO_CHAR(TO_<type>(:v))
				// new rule - null output from non null input is not valid.
				if (m_ctx.getCurrentConnection()==null||
						(m_ctx.getProperty(ScriptRunnerContext.NOLOG)!=null
						&&m_ctx.getProperty(ScriptRunnerContext.NOLOG).equals(Boolean.TRUE))) {
					throw new NoConnectionException();
				}
				//DBUtil dbUtil = DBUtil.getInstance(m_ctx.getCurrentConnection());
				//HashMap<String, Object> myBinds = new HashMap<String, Object>();
				ResultSet rs = null;
				PreparedStatement s = null;
				boolean amILocked = false;
				String succeeded = null;
				try {
					amILocked = LockManager.lock(m_ctx.getCurrentConnection());
					if (!amILocked) {
						return INVALID;
					}
					if ((formatString != null) && (!formatString.equals(""))) { //$NON-NLS-1$
						s = m_ctx.getCurrentConnection().prepareStatement(
						        "select TO_CHAR(TO_" + requiredType + "(:v,:f)) from dual"); //$NON-NLS-1$ //$NON-NLS-2$
						s.setString(1, promptResult);
						s.setString(2, formatString);
					} else {
						s = m_ctx.getCurrentConnection().prepareStatement(
						        "select TO_CHAR(TO_" + requiredType + "(:v)) from dual"); //$NON-NLS-1$ //$NON-NLS-2$
						s.setString(1, promptResult);
					}
					rs = s.executeQuery();
					if (rs.next()) {
						succeeded = rs.getString(1);
						if ((succeeded == null ||succeeded.equals(null) || succeeded.equals("")) && //$NON-NLS-1$
						        ((promptResult != null) && !(promptResult.equals("")))) { //$NON-NLS-1$
							// bad to get null output from not null input
							return BAD;
						}
					}
				} catch (SQLException e) {
					// TODO: handle exception
					// need to handle exceptions - value exceptions and format
					// mask exceptions
					// for different possible values:
					// [NUM[BER]
					// value error: ORA-01722: invalid number format mask error:
					// ORA-01481: invalid number format model
					// DATE
					// value error: lots of different errors format mask error:
					// ORA-01821: date format not recognized ORA-01820: format
					// code cannot appear in date input format
					// BINARY_FLOAT
					// value error: format mask error:ORA-01481: invalid number
					// format model
					// BINARY_DOUBLE
					// value error: format mask error: ORA-01481: invalid number
					// format model
					// need to check error codes around ORA-01481 and ORA-01821
					// as there are many similar errors.
					// if not format mask error - assume it is a value error
					String cause = e.getMessage();
					if (cause == null) {
						return INVALID;
					}
					int hasReturn = cause.indexOf('\n');
					if (hasReturn != -1) {
						cause = cause.substring(0, hasReturn);
					}
					if (!cause.matches("^[oO][rR][aA]-[0-9]*.*")) { //$NON-NLS-1$
						return INVALID;
					}
					Integer causeNumber = null;
					try {
						causeNumber = Integer.parseInt(cause.replaceAll("^[oO][rR][aA]-([0-9]*).*", "$1")); //$NON-NLS-1$ //$NON-NLS-2$
					} catch (NumberFormatException e2) {
						return INVALID;
					}
					if ((causeNumber == 1481) || ((causeNumber > 1809) && (causeNumber < 1830))) {
						// its a format error error out
						return causeNumber;
					} else if(causeNumber!= null && causeNumber ==1722){
						return causeNumber;
					} else { // assume it is a value error retry
						return INVALID;
					}
				} finally {
					if (rs != null) {
						try {
							rs.close();
						} catch (Exception e) {
						}
					}
					if (s != null) {
						try {
							s.close();
						} catch (Exception e) {
						}
					}
					// amilocked is true if we get here at all.[put the check in
					// anyway]
					if (amILocked) {
						try {
							LockManager.unlock(m_ctx.getCurrentConnection());
						} catch (Exception e) {
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof NoConnectionException) {
				throw e;
			}
			return INVALID;
		}
		return ACCEPTED;
	}

	/**
	 * next string to character, returning the string and the rest
	 */
	private String[] nextWordAndRest(String in) {
		in = in.trim();
		if (in.length() == 0) {
			return null;
		}
		int i = 0;
		boolean inQuote = false;
		boolean found = false;
		String rest = ""; //$NON-NLS-1$
		String firstWord = ""; //$NON-NLS-1$
		for (i = 0; i < in.length(); i++) {
			if (in.charAt(i) == '"') {
				if (inQuote) {
					inQuote = false;
				} else {
					inQuote = true;
				}
			}
			if ((Character.isWhitespace(in.charAt(i)) && (inQuote == false))) {
				found = true;
				break;
			}
		}
		if (!(found == false)) {// we have whitespace
			rest = in.substring(i).trim();
			firstWord = in.substring(0, i);
		} else {
			firstWord = in;
		}
		String[] stringArray = { firstWord, rest };
		return stringArray;
	}

	/**
	 * 
	 * write the output to the configured output.
	 * 
	 * @param str
	 *            {@link String} to write
	 * @param ctx
	 *            {@link ScriptRunnerContext}
	 */
	public static void write(String str, ScriptRunnerContext ctx) {
		BufferedOutputStream out = ctx.getOutputStream();
		try {
			out.write(ctx.stringToByteArrayForScriptRunnerNonStatic(str)); out.flush();
		} catch (IOException e) {
			Logger.getLogger(new Accept().getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}
	}

	public static String[][] parse(ScriptRunnerContext ctx, ISQLCommand cmd, List<LexerToken> src, String in){
		String[][] retVal=new String[][]{{null},{null},{null},{null},{null},{null}};

        for (int i=2; i<src.size();i++) {
            LexerToken t=src.get(i);
            boolean matchFound=false;
            boolean skipTwo=false;
            String tUpper=t.content.toUpperCase(Locale.US);
            for (int di=0;di<driver.length;di++){
            	String[] checkThese=driver[di];
            	//assumption there is at least one in string to look for in each driver entry
            	for(int ii=1;ii<checkThese.length;ii++) {
            		if (tUpper.equals(checkThese[ii])) {
            			if ((checkThese[0]!=null)) {
            				if ((i+1)<(src.size())) {
            					skipTwo=true;
            					matchFound=true;
            					String[] subParse=new String[]{tUpper,fixMe(src.get(i+1).content)};
            					retVal[di]=subParse;
            					break;
            				} else {
            					errorWrap(ctx, cmd, Messages.getString("ACCEPTEOL"));  //$NON-NLS-1$
            					return null;
            				}
            			} else {
            			//no second param for this match
            				String[] subParse=new String[] {tUpper,null};
            				retVal[di]=subParse;
            				matchFound=true;
            				break;
            			}
            		}
            	}
            	if (matchFound) {
            		break;
            	}
            }
            if (matchFound) {
            	if (skipTwo) {
            		i++;
            	}
            	skipTwo=false;
            	matchFound=false;
            } else {
            	report(ctx, cmd, i,t,src,in);
            	return null;
            }
        }
		return retVal;
	}
    public static void report(ScriptRunnerContext ctx, ISQLCommand cmd, int i, LexerToken t, List<LexerToken> src, String in) {
    	errorWrap(ctx,cmd,MessageFormat.format(Messages.getString("ACCEPTNOMATCH"),in.substring(t.begin))); //$NON-NLS-1$
    }

	/*public static void dump(String[][] d){
		if (d == null) {
			System.out.println ("null\n");
		} else {
			for (String[] sa:d) {
				for (String sa2:sa) {
					System.out.print(sa2+" ");
				}
				System.out.println("\n");
			}
		}
	}*/
	public static String fixMe(String in) {
		if (in.startsWith("'") && in.endsWith("'")&&in.length()>1) {  //$NON-NLS-1$  //$NON-NLS-2$
			return in.substring(1,in.length()-1).replaceAll("''","'");  //$NON-NLS-1$  //$NON-NLS-2$
		}
		if (in.startsWith("\"") && in.endsWith("\"")&&in.length()>1) {  //$NON-NLS-1$  //$NON-NLS-2$ 
			return in.substring(1,in.length()-1).replaceAll("\"\"","\"");  //$NON-NLS-1$  //$NON-NLS-2$
		}
		return in;
	}
	public static void errorWrap(ScriptRunnerContext ctx, ISQLCommand cmd, String theValue) {
		if (ctx != null && ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) != null
				&& Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
		    writeAndFlush(theValue, ctx);
		} else {
		    writeAndFlush(ScriptRunnerContext.lineErr( new Integer(cmd.getStartLine() + 1), cmd.getSQLOrig(), theValue, ctx), ctx);
		}
	}
	public static class NoConnectionException extends Exception {
		
	}
}
