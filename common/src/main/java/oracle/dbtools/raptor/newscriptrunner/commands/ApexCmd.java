/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.FileUtils;
import oracle.dbtools.common.utils.MetaResource;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.apex.APEXExport;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryXMLSupport;
import oracle.dbtools.raptor.utils.IListPrinter;
import oracle.dbtools.raptor.utils.ListPrinter;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level = Restricted.Level.R4)
public class ApexCmd extends CommandListener implements IHelp {

  private static final String PADDING = "                                                                                                                    "; //$NON-NLS-1$

  static QueryXMLSupport s_xml = null;

  static private IListPrinter _listPrinter = new ListPrinter();

  protected static synchronized QueryXMLSupport getXMLQueries() {
    if (s_xml == null) {
      // All sql stored in this file
      s_xml = QueryXMLSupport.getQueryXMLSupport(new MetaResource(ApexCmd.class.getClassLoader(), "oracle/dbtools/raptor/newscriptrunner/commands/describe.xml")); //$NON-NLS-1$
    }
    return s_xml;
  }

  /*
   * HELP DATA
   */
  public String getCommand() {
    return "APEX"; //$NON-NLS-1$
  }

  public String getHelp() {
    return CommandsHelp.getString(getCommand());
  }

  @Override
  public boolean isSqlPlus() {
    return false;
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    // nothing to do
  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

  }

  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    if (cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("apex")) { //$NON-NLS-1$

      // apex
      // apex list
      // apex export 102
      String[] parts = cmd.getSql().trim().split(" "); //$NON-NLS-1$
      String base = parts[0];
      String subCmd = parts.length > 1 && parts[1] != null ? parts[1] : "list"; //$NON-NLS-1$
      String filter = parts.length > 2 && parts[2] != null ? parts[2] : null;
      if (subCmd.trim().equalsIgnoreCase("")) { //$NON-NLS-1$
        subCmd = "list"; //$NON-NLS-1$
      }
      if (conn == null) {
        ctx.write(Messages.getString("ApexCmd.7")); //$NON-NLS-1$
        ctx.errorLog(ctx.getSourceRef(), Messages.getString("ApexCmd.7"), cmd.getSql());
        return true;
      }

      String report = null;
      if (subCmd.equalsIgnoreCase("list")) { //$NON-NLS-1$
        report = "apex.list"; //$NON-NLS-1$
        printList(conn, ctx, report);
      } else if (subCmd.equalsIgnoreCase("log")) { //$NON-NLS-1$
        report = "apex.log"; //$NON-NLS-1$
        printList(conn, ctx, report);
      } else if (subCmd.equalsIgnoreCase("export") ) { //$NON-NLS-1$
        try {
          String[] args =null;
          APEXExport apexExport = new APEXExport();
          apexExport.setCWD(FileUtils.getCWD(ctx));
          apexExport.setConnection(conn);
          apexExport.setOutStream(ctx.getOutputStream());

          if ( parts.length== 2 ){
            ctx.write(apexExport.usage());
          } else if ( parts.length== 3 ){
           args = new String[]{"-applicationid",filter};           
         } else {
           args = new String[parts.length-2];
           System.arraycopy(parts, 2, args, 0, parts.length-2);//5 is the length to copy

         }
          apexExport.processArgs(args);
          
          if ( apexExport.isValid() ){ 
            apexExport.export(); 
          } else {
            apexExport.usage();
          }
          
          
          //ctx.write(getDDL(conn, filter));
        } catch (NumberFormatException e) {
          ctx.write(Messages.getString("ApexCmd.0")); //$NON-NLS-1$
        } catch (Exception e) {
          ctx.write(Messages.getString("ApexCmd.1")); //$NON-NLS-1$
          ctx.write(e.getLocalizedMessage());
        }
      }

      return true;
    } else {
      return false;// nothing to do here.
    }
  }

  /**
   * @param conn
   * @param ctx
   * @param report
   */
  private void printList(Connection conn, ScriptRunnerContext ctx, String report) {
    Properties props = ((OracleConnection) conn).getProperties();
    Query colSQL = getXMLQueries().getQuery(report, conn);
    DBUtil dbUtil = DBUtil.getInstance(conn);
    HashMap<String, String> binds = new HashMap<String, String>();
    List<List<?>> rows = dbUtil.executeReturnListofList(colSQL.getSql(), binds);
    _listPrinter.printListofList(ctx, rows);
  }

  /**
   * Get apex's "ddl"
   * 
   * @param conn
   * @param filter
   * @return
   */
  public String getDDL(Connection conn, String filter) {
    DBUtil dbUtil = DBUtil.getInstance(conn);
    HashMap<String, String> binds = new HashMap<String, String>();
    binds.put("APPLICATION_ID", filter); //$NON-NLS-1$
    String worspaceId = dbUtil.executeReturnOneCol("select workspace_id from apex_applications where application_id = :APPLICATION_ID", binds); //$NON-NLS-1$
    binds.put("WORKSPACE_ID", worspaceId); //$NON-NLS-1$

    String OWA_INIT = "declare " + " nm     owa.vc_arr;   " + " vl     owa.vc_arr;   " + " begin  " + " nm(1) := 'WEB_AUTHENT_PREFIX'; " //$NON-NLS-4$ //$NON-NLS-5$
                                                                                                                                         // //$NON-NLS-3$
                                                                                                                                         // //$NON-NLS-4$
                                                                                                                                         // //$NON-NLS-5$
        + " vl(1) := 'WEB$'; " + " owa.init_cgi_env( 1, nm, vl ); " + " sys.htp.htbuf_len := 84; " + " end;"; //$NON-NLS-4$ //$NON-NLS-2$
                                                                                                              // //$NON-NLS-3$
                                                                                                              // //$NON-NLS-4$

    dbUtil.execute(OWA_INIT);
    dbUtil.execute("begin apex_util.export_application(p_application_id=>:APPLICATION_ID,p_workspace_id=>:WORKSPACE_ID); end; ", binds); //$NON-NLS-1$
    String ret = getOWA(conn);
    if (ret != null && ret.indexOf("\n\n") > -1) //$NON-NLS-1$
      ret = ret.substring(ret.indexOf("\n\n")); //$NON-NLS-1$
    SQLException e = dbUtil.getInstance(conn).getLastException();
    if (e != null)
      return "\n" + e.getMessage(); //$NON-NLS-1$
    return ret;
  }

  /**
   * Helper proc to get owa output
   * 
   * @param conn
   * @return
   */
  public String getOWA(Connection conn) {
    CallableStatement cs = null;
    String parcialContent = null;
    try {
      String GET_PAGE = "declare " + //$NON-NLS-1$
          "   l_buf  varchar2(32767); " + //$NON-NLS-1$
          "   l_clob CLOB; " + //$NON-NLS-1$
          "   l_lines htp.htbuf_arr; " + //$NON-NLS-1$
          "   l_num   number := 999999; " + //$NON-NLS-1$
          " begin " + //$NON-NLS-1$
          "   dbms_lob.createtemporary(l_clob, TRUE); " + //$NON-NLS-1$
          "   OWA.GET_PAGE(l_lines, l_num); " + //$NON-NLS-1$
          "   for i in 1..l_num loop " + //$NON-NLS-1$
          "     dbms_lob.append(l_clob,l_lines(i)); " + //$NON-NLS-1$
          "   end loop; " + //$NON-NLS-1$
          "   ? := l_clob;" + //$NON-NLS-1$
          " end;"; //$NON-NLS-1$
      cs = conn.prepareCall(GET_PAGE);
      cs.registerOutParameter(1, Types.CLOB);
      cs.execute();
      Clob clob = cs.getClob(1);
      parcialContent = clob.getSubString(1, (int) clob.length());
    } catch (SQLException e) {
      Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    } finally {
      if (cs != null) {
        try {
          cs.close();
        } catch (Exception e) {
        }
      }
    }
    return parcialContent;
  }

  static public void setListPrinter(IListPrinter listprinter) {
    _listPrinter = listprinter;
  }

}
