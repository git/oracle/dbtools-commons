/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Properties;

import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.jdbc.OracleConnection;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ArchiveLogList.java">Barry McGillin</a> 
 *
 */
public class ArchiveLogList extends CommandListener {
    // Insert strings into messages called like:Messages.getString("SetPause.1")
    // archive log list etc
    private static final String CMD = "archive log list"; //$NON-NLS-1$

    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }

    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }

    @Override
    public boolean handleEvent(Connection myconn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && 
                (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
            return false;
        }
        if (cmd.getSql().trim().toLowerCase().replaceAll("\\s+", " ").startsWith(CMD)) { //$NON-NLS-1$ //$NON-NLS-2$
            Connection conn = ctx.getCurrentConnection();
            boolean amILocked = false;
            Connection lockedConn = null;
            String writeOrError = ""; //$NON-NLS-1$
            try {
                lockedConn = ctx.getCurrentConnection();
                if ((lockedConn)!=null&&(amILocked = LockManager.lock(lockedConn))) {
                    // ARCHIVELOG NOARCHIVELOG or MANUAL (archiving mode)
                    String logMode = DBUtil.getInstance(conn).
                        executeReturnOneCol("select LOG_MODE from v$database"); //$NON-NLS-1$



                    // archive destination
                    String role = null;
                    if (conn instanceof OracleConnection) {
                        Properties p =  ((OracleConnection) conn).getProperties();
                        if (p!=null) {
                            role = p.getProperty("internal_logon"); //$NON-NLS-1$
                        }
                    }
                    if ((role != null) && ((role.equalsIgnoreCase("sysdba") //$NON-NLS-1$
                                            || (role.equalsIgnoreCase("sysoper"))))) { //$NON-NLS-1$
                        if (logMode != null) {
                            if (logMode.equals("NOARCHIVELOG")) { //$NON-NLS-1$
                                writeOrError += (cutTo40(Messages.getString("DATABASE_LOG_MODE"))  //$NON-NLS-1$
                                                         + Messages.getString("NO_ARCHIVE_MODE")+"\n"); //$NON-NLS-1$ //$NON-NLS-2$
                                writeOrError += (cutTo40(Messages.getString("AUTOMATIC_ARCHIVAL")) //$NON-NLS-1$
                                        + Messages.getString("DISABLED") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                            } else
                                if (logMode.equals("ARCHIVELOG")) { //$NON-NLS-1$
                                    writeOrError += (cutTo40(Messages.getString("DATABASE_LOG_MODE")) //$NON-NLS-1$  
                                                     + Messages.getString("ARCHIVE_LOG_MODE") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                                    writeOrError += (cutTo40(Messages.getString("AUTOMATIC_ARCHIVAL"))  //$NON-NLS-1$ 
                                                     + Messages.getString("ENABLED") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                                } else
                                    if (logMode.equals("MANUAL")) { //$NON-NLS-1$
                                        writeOrError += (cutTo40(Messages.getString("DATABASE_LOG_MODE")) //$NON-NLS-1$ 
                                                         + Messages.getString("ARCHIVE_LOG_MODE") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                                        writeOrError += (cutTo40(Messages.getString("AUTOMATIC_ARCHIVAL")) //$NON-NLS-1$
                                                         + Messages.getString("DISABLED") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                                    }
                        }
                        // method 1 LOG_ARCHIVE_DEST_n
                        Statement s = null;
                        ResultSet rs = null;
                        HashMap<Integer, String> locations = new HashMap<Integer, String>();
                        HashMap<Integer, String> locationsFlag = new HashMap<Integer, String>();
                        String logFileDest = null;
                        try {
                            int maxLocation = 0;
                            s = conn.createStatement();
                            rs = s.executeQuery("select value, to_number(substr(name,18)) num from " +  //$NON-NLS-1$
                                                "v$parameter where name like 'log_archive_dest_%' and " +  //$NON-NLS-1$
                                                "name not like 'log_archive_dest_state%' order by num"); //$NON-NLS-1$
                            while (rs.next()) {
                                int location = rs.getInt(2);
                                if (location > maxLocation) {
                                    maxLocation = location;
                                }
                                locations.put(location, rs.getString(1));
                            }
                            rs.close();
                            s.close();
                            s = conn.createStatement();
                            rs = s.executeQuery("select value, to_number(substr(name,24)) num "+  //$NON-NLS-1$
                                                "from v$parameter where name like "+  //$NON-NLS-1$
                                                "'log_archive_dest_state_%' order by num"); //$NON-NLS-1$
                            while (rs.next()) {
                                locationsFlag.put(rs.getInt(2), rs.getString(1));
                            }
                            for (int i = 1; i < (maxLocation + 1); i++) {
                                String locationString = locations.get(i);
                                String locationFlag = locationsFlag.get(i);
                                if ((locationString != null) && (!(locationString.trim().equals("")))  //$NON-NLS-1$
                                    && ((locationFlag != null) && (locationFlag.equalsIgnoreCase("enabled")))) { //$NON-NLS-1$
                                    // location=
                                    // service=
                                    // parse it out by regexp later
                                    logFileDest = locationString;
                                    break;
                                }
                            }
                            if (logFileDest == null) {
                                // try method 2: log_archive_dest
                                String logArchiveDest = DBUtil.getInstance(conn).executeReturnOneCol
                                    ("select value from v$parameter where name = 'log_archive_dest' and value != null"); //$NON-NLS-1$ 
                                if ((logArchiveDest != null) && (!(logArchiveDest.equals("")))) { //$NON-NLS-1$
                                    logFileDest = logArchiveDest;
                                } else {
                                    logFileDest = "USE_DB_RECOVERY_FILE_DEST"; //$NON-NLS-1$
                                }
                            }
                            if ((logFileDest != null)&&(!(logFileDest.equals("USE_DB_RECOVERY_FILE_DEST")))) { //$NON-NLS-1$
                                logFileDest = logFileDest.trim();
                                String[] rip = { "[lL][oO][cC][aA][tT][iI][oO][nN]\\s*=\\s*'([^']*)'.*",  //$NON-NLS-1$
                                                 "[lL][oO][cC][aA][tT][iI][oO][nN]\\s*=\\s*\"([^']*)\".*", //$NON-NLS-1$
                                                 "[lL][oO][cC][aA][tT][iI][oO][nN]\\s*=\\s*([^\\s]*).*",  //$NON-NLS-1$
                                                 "[sS][eE][rR][vV][iI][cC][eE]\\s*=\\s*'([^']*)'.*", //$NON-NLS-1$
                                                 "[sS][eE][rR][vV][iI][cC][eE]\\s*=\\s*\"([^']*)\".*",  //$NON-NLS-1$
                                                 "[sS][eE][rR][vV][iI][cC][eE]\\s*=\\s*([^\\s]*).*",  //$NON-NLS-1$
                                                 "'([^']*)'.*", "\"([^']*)\".*", //$NON-NLS-1$ //$NON-NLS-2$
                                                 "([^\\s]*).*" }; //$NON-NLS-1$
                                for (String regexp: rip) {
                                    if (logFileDest.matches(regexp)) {
                                        
                                        logFileDest = logFileDest.replaceAll(regexp, "$1"); //$NON-NLS-1$
                                        break;
                                    }
                                }
                            }
                            writeOrError += (cutTo40(Messages.getString("ARCHIVE_DESTINATION")) + logFileDest + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                            String oldestOnline = DBUtil.getInstance(conn).executeReturnOneCol
                                ("select min(sequence#) from v$log"); //$NON-NLS-1$
                            if (oldestOnline != null) {
                                writeOrError += (cutTo40(Messages.getString("OLDEST_ONLINE_LOG_SEQUENCE")) + oldestOnline + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                            }
                            if ((logMode != null) && (!(logMode.equals("NOARCHIVELOG")))) { //$NON-NLS-1$
                                String nextLog = DBUtil.getInstance(conn).executeReturnOneCol
                                    ("select max(sequence#) from v$log where ARCHIVED='NO'"); //$NON-NLS-1$
                                if (nextLog != null) {
                                    writeOrError += (cutTo40(Messages.getString("NEXT_LOG_SEQUENCE_TO_ARCHIVE")) + nextLog + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                                }
                            }
                            String current = DBUtil.getInstance(conn).executeReturnOneCol
                                ("select max(sequence#) from v$log where status='CURRENT'"); //$NON-NLS-1$
                            if (current != null) {
                                writeOrError += (cutTo40(Messages.getString("CURRENT_LOG_SEQUENCE")) + current + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                            }
                            ctx.write(writeOrError);// do not write out on exception
                        } finally {
                            if (rs != null) {
                                try {
                                    rs.close();
                                } catch (Exception e) {
                                }
                            }
                            if (s != null) {
                                try {
                                    s.close();
                                } catch (Exception e) {
                                }
                            }
                        }
                    } else {
                        ctx.write(Messages.getString("INSUFFICIENT_PRIVILEGES") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                    }
                }
            } catch (Exception e) {
                ctx.write(e.getLocalizedMessage() + "\n"); //$NON-NLS-1$
            } finally {
                if (amILocked) {
                    LockManager.unlock(lockedConn);
                }
            }
            return true;
        }
        ctx.write(Messages.getString("ONLY_ARCHIVE_LOG_LIST_HANDLED")+"\n"); //$NON-NLS-1$ //$NON-NLS-2$
        return false;
    }

    String cutTo40(String in) {
        String longSpaces = "                                                                       "; //$NON-NLS-1$
        if (in == null) {
            in = ""; //$NON-NLS-1$
        }
        in = in + longSpaces;
        return in.substring(0, 39) + " "; //$NON-NLS-1$
    }

    public void beginScript(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }

    public void endScript(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
}
