/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Set;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * 
 * @author Shounak Roychowdhury
 * @author <a href="mailto:shounak.roychowdhury@oracle.com?subject=Attribute.java">Shounak Roychowdhury</a>
 *
 */
public class Attribute extends CommandListener {

	private static final String ATTRIBUTE = "ATTRIBUTE";
	private static final String m_lineSeparator = System.getProperty("line.separator"); //$NON-NLS-1$

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// ignore
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// ignore
	}

	
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		
		// ATTR[IBUTE] [type_name.attribute_name [option ...]]
		
		String sqlpcmd = cmd.getSql().toLowerCase().trim().replaceAll("\\s+", " "); //$NON-NLS-1$ //$NON-NLS-2$
		String[] cmds = sqlpcmd.split(" "); //$NON-NLS-1$
		
		String attributeKeyword = cmds[0].trim();
		
		HashMap<String, HashMap<String, HashMap<String, String>>> typeAttributesMap = ctx.getObjectTypeAttributes();
		
		// case 1) only attribute word, return true. This is a sanitary check. 
		if (!testAttributeKeyword( attributeKeyword ) ) {
			return false;
		}
		
		// case 2) when there are not type attributes.
		if ( cmds.length == 1 && (typeAttributesMap == null ||  typeAttributesMap.isEmpty()) ) {
			return false;
		}
		
		if ( cmds.length == 1 && (typeAttributesMap != null && !typeAttributesMap.isEmpty()) ) {
			List<String> typeAttributeKeys = getTypeAttributeKeys(typeAttributesMap);
			for (int k = 0; k < typeAttributeKeys.size()-1; k++) {
				processAttributes( ctx, new String[]{ATTRIBUTE, typeAttributeKeys.get(k)}, typeAttributesMap, true ); //append new string is true.
			}
			processAttributes( ctx, new String[]{ATTRIBUTE, typeAttributeKeys.get(typeAttributeKeys.size()-1)}, typeAttributesMap, false ); //append new string is false for the last one.
			return true;
		}
		
		if ( cmds.length > 1 ) {	
			return processAttributes( ctx, cmds, typeAttributesMap, false );
		}
		return true;
	}
	
	private List<String> getTypeAttributeKeys( 
			HashMap<String, HashMap<String, HashMap<String, String>>> typeAttributesMap ) {
		List<String> tempList = new ArrayList();
		Set<String> typeKeySet = typeAttributesMap.keySet();
		Iterator<String> typeKeySetIterator = typeKeySet.iterator();
		while (typeKeySetIterator.hasNext()) {
			String typeKeyName = typeKeySetIterator.next();
			HashMap<String, HashMap<String, String>> attributeMap = typeAttributesMap.get(typeKeyName);
			Set<String> attributeKeySet = attributeMap.keySet();
			Iterator<String> attributeKeySetIterator = attributeKeySet.iterator();
			while (attributeKeySetIterator.hasNext()) {
				String attributeName = attributeKeySetIterator.next();
				tempList.add(typeKeyName + "." + attributeName);
			}
		}
		return tempList;
	}

	private boolean processAttributes(ScriptRunnerContext ctx, String[] cmds, 
			HashMap<String, HashMap<String, HashMap<String, String>>> typeAttributesMap, boolean appendLineSeparator ) {
		
		
		// deal with type and names
		// ========================
		String typeAndAttributeNames = cmds[1].trim().toUpperCase();
		// check if it is alias.
		String attributeAlias = getTypeAttributeForAlias(ctx, typeAndAttributeNames);
		if (attributeAlias != null) {
			typeAndAttributeNames = attributeAlias;
		}
		
		if (!validateTypeAttribute ( typeAndAttributeNames, ctx )) {
			return false;
		}

		String[] typeAttributes = typeAndAttributeNames.split("\\."); //$NON-NLS-1$
		if ( typeAttributes == null || typeAttributes.length != 2 ) {
			return false;
		}

		String typeName = typeAttributes[0].trim().toUpperCase(); //$NON-NLS-1$
		if ( typeName == null || typeName.length() == 0 ) {
			return false;
		}
		String attributeName = typeAttributes[1].trim(); //$NON-NLS-1$
		if ( attributeName == null || attributeName.length() == 0 ) {
			return false;
		}

		HashMap<String, HashMap<String, String>> attributeMap = typeAttributesMap.get(typeName);
		if (attributeMap == null) {
			attributeMap = new HashMap<String, HashMap<String, String>>();
			typeAttributesMap.put(typeName,attributeMap);
		}
		HashMap<String, String> attributeValueMap = attributeMap.get(attributeName);
		if (attributeValueMap == null) {
			attributeValueMap = new HashMap<String, String>();
			attributeMap.put(attributeName, attributeValueMap);
		}

		// now deal with options
		//======================
		// ALI[AS] alias
		// CLE[AR]
		// FOR[MAT] format
		// LIKE {type_name.attribute_name | alias}
		// ON | OFF

		if (cmds.length == 2) {
			// check attributeMap contents to verify it is already defined.
			if ( checkForDefinedAttributes (ctx, attributeValueMap, typeAndAttributeNames, appendLineSeparator )) {
				// Need to return false after writing values.
				return false;
			}
			else {
				if (!appendLineSeparator) { 
					ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_NOT_DEFINED"), new Object[] {typeAndAttributeNames.toLowerCase()}) + m_lineSeparator    );
				}
				return false;
			}
		}

		String optionName = cmds[2];

		if (optionName == null || optionName.length() == 0) {
			return false;
		}

		if (!(isAlias(optionName) || isClear(optionName) || isFormat(optionName) || isLike(optionName) || isOnOff(optionName)) ) {
			ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_UNKNOWN_OPTION"), new Object[] {optionName}) + m_lineSeparator    ); 
			return false;
		}
		// ALIAS
		if (isAlias(optionName)) {
			// expect keyword ALIAS
			if (cmds.length == 4) {
				// next value is alias
				String aliasValue = cmds[3].trim();
				if ( aliasValue.length() > 0 ) {
					attributeValueMap.put( ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_ALIAS, aliasValue );
					addAlias(ctx,aliasValue,typeName+"."+attributeName );
					attributeValueMap.put(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_ON_OFF,"ON");
					return true;
				}
				else {
					return false;
				}
			}
			else {
				// need to throw an error.
				ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_STRING_VALUE_MISSING"), new Object[] {}) + m_lineSeparator    ); 
				return false;
			}
		}
		// CLEAR 
		// clearing everything related to an attribute.
		if (isClear(optionName)) {
			attributeValueMap = attributeMap.get(attributeName);
			if (attributeValueMap == null) {
				attributeValueMap = new HashMap<String, String>();
				attributeMap.put(attributeName, attributeValueMap);
			}
			else {
				java.util.Set<String> keys = attributeValueMap.keySet();
				if (keys.size() > 0) {
					for (String key: keys) {
						attributeValueMap.put(key, "");
					}	 
				}
				else {
					ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_NOT_DEFINED"), new Object[] {typeAndAttributeNames.toLowerCase()}) + m_lineSeparator    );
					return false;
				}
			}
			return true;
		}
		// FORMAT
		if (isFormat(optionName)) {
			// expect format token
			if (cmds.length == 4) {
				// next token is alias
				String formatValue = cmds[3].trim();
				attributeValueMap.put(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_FORMAT, formatValue);
				attributeValueMap.put(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_ON_OFF,"ON");
				return true;
			}
			else {
				ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_STRING_VALUE_MISSING"), new Object[] {}) + m_lineSeparator    ); 
				return false;
			}
		}
		// LIKE
		if (isLike(optionName)) {
			// expect format token
			if (cmds.length == 4) {
				// next token is either an alias or type.attribute
				String likeValue = cmds[3].trim();
				attributeValueMap.put(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_LIKE, likeValue);
				attributeValueMap.put(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_ON_OFF,"ON");
				return true;
			}
			else {
				ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_STRING_VALUE_MISSING"), new Object[] {}) + m_lineSeparator    );
				return false;
			}
		}
		// ON | OFF
		if (isOnOff(optionName)) {
			// it is either ON or OFF.
			attributeValueMap.put(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_ON_OFF, optionName);
			return true;
		}
		return true;

	}
	
	
	private boolean checkForDefinedAttributes(
			ScriptRunnerContext ctx, HashMap<String, String> attributeValueMap, String typeAndAttributeNames, boolean appendLineSeparator) {
		
		boolean onOffFlag = true;
		boolean aliasFlag = true;
		boolean formatFlag = true;
		boolean likeFlag = true;
		
		if ( attributeValueMap != null) {
			String formatData = attributeValueMap.get(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_FORMAT);
			String likeData = attributeValueMap.get(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_LIKE);
			String onOff = attributeValueMap.get(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_ON_OFF);
			String aliasData = attributeValueMap.get(ScriptRunnerContext.OBJECT_TYPE_ATTRIBUTE_ALIAS);
			if ( onOff != null && onOff.length() > 0) {
				if (onOff.equalsIgnoreCase("ON")) {
					ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_ON_VALUE"), new Object[] {typeAndAttributeNames.toLowerCase()}) + m_lineSeparator    );
				}
				else {
					ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_OFF_VALUE"), new Object[] {typeAndAttributeNames.toLowerCase()}) + m_lineSeparator    );	
				}
			}
			else {
				onOffFlag = false;
			}
			if ( aliasData != null && aliasData.length() > 0) {
				ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_ALIAS_VALUE"), new Object[] {aliasData}) + m_lineSeparator    ); 
			}
			else {
				aliasFlag = false;
			}
			if ( formatData != null && formatData.length() > 0) {
				ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_FORMAT_VALUE"), new Object[] {formatData}) + m_lineSeparator    ); 
			}
			else {
				formatFlag = false;
			}
				
			if ( likeData != null && likeData.length() > 0) {
				ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_LIKE_VALUE"), new Object[] {likeData}) + m_lineSeparator    ); 
			}
			else {
				likeFlag = false;
			}
			
			if (appendLineSeparator) {
				ctx.write( m_lineSeparator );
			}
			if ( !onOffFlag && !aliasFlag && !formatFlag && !likeFlag ) {
				return false;
			}
			return true;
		}
		return false;
	}

	
	private void addAlias(ScriptRunnerContext ctx, String aliasValue,
			String typeAttributeName) {
		ctx.getAliasTypeAttributesMap().put(aliasValue.toUpperCase(), typeAttributeName.toUpperCase());
	}

	private String getTypeAttributeForAlias(ScriptRunnerContext ctx, String aliasValue) {
		return ctx.getAliasTypeAttributesMap().get(aliasValue.toUpperCase());
	}

	//private boolean isTypeAttributeAnAlias(ScriptRunnerContext ctx, String aliasValue) {
		//return ctx.getAliasTypeAttributesMap().get(aliasValue.toUpperCase());
	//}
	
	
	private boolean isOnOff(String keyword) {
		if ( keyword.equalsIgnoreCase("ON") || keyword.equalsIgnoreCase("OFF") ) {
			return true;
		}
		return false;
	}

	private boolean isLike(String keyword) {
		if ( keyword.equalsIgnoreCase("LIKE") ) {
			return true;
		}
		return false;
	}

	
	private boolean isFormat(String keyword) {
		if ( keyword.equalsIgnoreCase("FOR") || keyword.equalsIgnoreCase("FORM") || 
				keyword.equalsIgnoreCase("FORMA") || keyword.equalsIgnoreCase("FORMAT") ) {
			return true;
		}
		return false;
	}
	
	private boolean isClear(String keyword) {
		if ( keyword.equalsIgnoreCase("CLE") || keyword.equalsIgnoreCase("CLEA") || keyword.equalsIgnoreCase("CLEAR") ) {
			return true;
		}
		return false;
	}

	
	private boolean isAlias(String keyword) {
		if ( keyword.equalsIgnoreCase("ALI") || keyword.equalsIgnoreCase("ALIA") || keyword.equalsIgnoreCase("ALIAS") ) {
			return true;
		}
		return false;
	}

	private boolean testAttributeKeyword(String keyword) {
		if ( keyword.equalsIgnoreCase("ATTR") || keyword.equalsIgnoreCase("ATTRI") ||
			 keyword.equalsIgnoreCase("ATTRIB") || keyword.equalsIgnoreCase("ATTRIBU") ||
			 keyword.equalsIgnoreCase("ATTRIBUT") || keyword.equalsIgnoreCase("ATTRIBUTE")
			)
		{
			return true;
		}
		return false;
	}
	
	
	private boolean validateTypeAttribute ( String typeAndAttributeNames, ScriptRunnerContext ctx )  {
		if (typeAndAttributeNames == null) {
			return false;
		}
		if (!typeAndAttributeNames.contains(".")) {
			ctx.write( MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_NOT_DEFINED"), new Object[] {typeAndAttributeNames.toLowerCase()}) + m_lineSeparator    );
			return false;
		}
	    return true;
	}

}