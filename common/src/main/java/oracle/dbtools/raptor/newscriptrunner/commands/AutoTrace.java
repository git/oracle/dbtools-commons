/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.db.SQLPLUSCmdFormatter;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQL;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class AutoTrace extends AForAllStmtsCommand {
    private final static String ctxTraceState = ScriptRunnerContext.AUTOTRACE_CTXTRACESTATE;
    private final static String ctxTraceType = ScriptRunnerContext.AUTOTRACE_CTXTRACETYPE;
    private final static String SPACE = "                                                                                "; //$NON-NLS-1$
    private String AUTO_EXPLAIN = ScriptRunnerContext.AUTOTRACE_AUTO_EXPLAIN;
    private String AUTO_STAT = ScriptRunnerContext.AUTOTRACE_AUTO_STAT;
    private String AUTO_ALL = ScriptRunnerContext.AUTOTRACE_AUTO_ALL;
    private String AUTO_NONE = ScriptRunnerContext.AUTOTRACE_AUTO_NONE;
    // select to get all stats
    private String statSql1 = "select ms.Statistic# stat,ms.value,sn.name from v$mystat ms, v$statname  sn where sn.statistic# =  ms.statistic# "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
    private String statSql2 = "\n and sn.name in ('recursive calls','db block gets','consistent gets','physical reads','redo size'," + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
    		"'bytes sent via SQL*Net to client','bytes received via SQL*Net from client','SQL*Net roundtrips to/from client','sorts (memory)','sorts (disk)')"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
    private String statSql3 = "\n order by 1"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
    private String statSql = statSql1+statSql3;
    private ResultSet _rset = null;
    private HashMap<Integer, Stat> bStats;
    private ArrayList<Stat> aStats;
    private final static SQLCommand.StmtSubType s_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_AUTOTRACE;
    
    public AutoTrace() {
        super(s_cmdStmtSubType);
    }
    
    @Override
    public void doBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
            return;
        }
        
        if( ctx!=null 
         && ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null 
         && "ON".equals(ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE))
         || "true".equals(HiddenParameters.parameters.get("classicMode")) 
        ) 
            statSql = statSql1+statSql2+statSql3;
        else
            statSql = statSql1+statSql3;
            


      // stash the on/off into the context
        Boolean state = (Boolean) ctx.getProperty(ctxTraceState);
        if (state == null) {
            ctx.putProperty(ctxTraceState, false);
            ctx.putProperty(ctxTraceType, false);
            state = false;
        }
        // Grab the current session stats before the user work is done
        if (state == true && (!cmd.getStmtClass().equals(SQLCommand.StmtType.G_C_SQLPLUS))) {
            bStats = new HashMap<Integer, Stat>();
            try {
                PreparedStatement ps = conn.prepareStatement(statSql);
                _rset = ps.executeQuery();
                while (_rset.next()) {
                  // stash all the stats for comparison later
                    Stat _stat = new Stat(_rset.getInt(2), _rset.getString(3));
                    bStats.put(_rset.getInt(1), _stat);
                }
            } catch (SQLException e) {
                // e.printStackTrace();
            }
        }
    }
    
    @Override
    public void doEndWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
            return;
        }
        if( cmd.isFail() )
        	return;
        if( (Boolean) ctx.getProperty(ctxTraceState) 
                //redundant: && (cmd.getStmtClass/orType?().equals(SQLCommand.StmtType.G_C_SQL))
                && (cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SELECT)
                  || cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_INSERT)
                  || cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_UPDATE)
                  || cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_DELETE)
                  || cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_MERGE)
                  || cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_WITH)
                        )
        ) {
            // collect end stats b4 explain
            aStats = new ArrayList<Stat>();
            try {
                PreparedStatement ps = conn.prepareStatement(statSql);
                _rset = ps.executeQuery();
                while (_rset.next()) {
                    String name = _rset.getString(3);
                    if( name.startsWith("workarea") ) //$NON-NLS-1$
                        continue;
                    if( name.endsWith("memory") || name.endsWith("memory max")) //$NON-NLS-1$ //$NON-NLS-2$
                        continue;
                    // compare new stat with before stat.  if NON-zero add to the list to be printed
                    boolean notZeroValue = _rset.getInt(2) - bStats.get(_rset.getInt(1)).getValue() != 0;
                    if( ctx!=null 
                     && ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null 
                     && "ON".equals(ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE))
                     || "true".equals(HiddenParameters.parameters.get("classicMode")) 
                         )
                        notZeroValue = true;
                    if ( notZeroValue ){
                        Stat _stat = new Stat(_rset.getInt(2) - bStats.get(_rset.getInt(1)).getValue(), name);                      
                        aStats.add( _stat);
                    }
                }
            } catch (SQLException e) {
                // if unable to get stats we will know because aStats size is 0
                // so don't do anything here
            }
            // explain stmt
            if (ctx.getProperty(ctxTraceType).equals(AUTO_ALL) || ctx.getProperty(ctxTraceType).equals(AUTO_EXPLAIN)) {
                try {
                    PreparedStatement ps = conn.prepareStatement("explain plan for " + cmd.getSql()); //$NON-NLS-1$
                    
                    // cmd.getBinds() doesn't have any binds. 
                    // Leverage SQLCommandRunner.setBinds() instead.
                    SQL tmp = new SQL(cmd, null);
                    tmp.setScriptRunnerContext(ctx);
                    tmp.setConn(conn);
                    tmp.setBinds(ps);
                    
                    _rset = ps.executeQuery();
                    // use dbms_xplan so as not to rebuild a plan table in text
                    ps = conn.prepareStatement("SELECT * FROM table(DBMS_XPLAN.DISPLAY)"); //$NON-NLS-1$
                    _rset = ps.executeQuery();
                    SQLPLUSCmdFormatter cmdf = new SQLPLUSCmdFormatter(ctx);
                    
                    StringBuffer buf = new StringBuffer("Explain Plan\n-----------------------------------------------------------\n"); //
                    write(ScriptUtils.wordwrap(1, (int)ctx.getProperty(ScriptRunnerContext.SETLINESIZE), buf, 1).toString(),ctx);
                    try {
						cmdf.formatResults(ctx.getOutputStream(), _rset, cmd);
					} catch (IOException e) {
	                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
					}
                    write("\n", ctx); //$NON-NLS-1$
                } catch (SQLException e) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                }
            }
            // staistics for stmt
            if (ctx.getProperty(ctxTraceType).equals(AUTO_ALL) || ctx.getProperty(ctxTraceType).equals(AUTO_STAT)) {
                if (aStats.size() > 0) {
                	StringBuffer buf = new StringBuffer("   Statistics\n-----------------------------------------------------------\n");
                    write("", ctx); //$NON-NLS-1$
                    // Sort based on the name to be consistant run to run.
                    Collections.sort(aStats, new Comparator(){

                      @Override
                      public int compare(Object o1, Object o2) {
                        return ((Stat)o1).getName().compareTo(((Stat)o2).getName());
                      }
                      
                    });
                    
                    
                    for(Stat stat:aStats) {
                        buf.append((lpad(16, String.valueOf(stat.getValue())) + "  " + stat.getName() + "\n")); //$NON-NLS-1$ //$NON-NLS-2$
                    }
                    write(ScriptUtils.wordwrap(1, (int)ctx.getProperty(ScriptRunnerContext.SETLINESIZE), buf, 1).toString(),ctx);
                } else {
                  // error messages when permissions are missing
                    write(Messages.getString("AutoTrace.19"), ctx); //$NON-NLS-1$
                    write(Messages.getString("AutoTrace.20"), ctx); //$NON-NLS-1$
                }
            }
        }
    }
    
    @Override
    public boolean doHandleCmd( Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd ) {
        PreparedStatement stmt = null; //$NON-NLS-1$
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("select 1 from dual"); //$NON-NLS-1$
            rs = stmt.executeQuery();
        } catch( SQLException e ) {
            String msg = e.getLocalizedMessage();
            System.out.println(msg);
            return true;
        } finally {
            if( rs != null ) try {
                rs.close();
            } catch (SQLException e) {}
            if( stmt != null ) try {
                stmt.close();
            } catch (SQLException e) {}
        }
        String txt = cmd.getLoweredTrimmedSQL();
        List<LexerToken> src = LexerToken.parse(txt);
        // check tokens on the list with increasing order
        if( src.size() < 2 ) {
            write("Impossible -- incomplete comand", ctx); //$NON-NLS-1$
            return true;
        }
        if( !"set".equals(src.get(0).content) ) {//$NON-NLS-1$
            write("Impossible -- not a set command", ctx); //$NON-NLS-1$
            return true;
        }
        if( src.get(1).content.length() < 5 || !"autotrace".startsWith(src.get(1).content)  ) {    //$NON-NLS-1$        
            write(MessageFormat.format(
                    Messages.getString("AutoTrace.SyntaxError"), new Object[] { src.get(1).content }) //$NON-NLS-1$
                    , ctx
              ); 
            return true;
        }
        if( src.size() == 2 ) {
            write(Messages.getString("AutoTrace.UnknownAction"), ctx); //$NON-NLS-1$
            return true;
        }
        if( "on".equals(src.get(2).content) ) { //$NON-NLS-1$
            if( 4 == src.size() && 3 <= src.get(3).content.length() && "explain".startsWith(src.get(3).content) ) { //$NON-NLS-1$
                ctx.putProperty(ctxTraceState, true);
                ctx.putProperty(ctxTraceType, AUTO_EXPLAIN);
                write(Messages.getString("AutoTrace.22"), ctx); //$NON-NLS-1$
            } else if( 4 == src.size() && 4 <= src.get(3).content.length() && "statistics".startsWith(src.get(3).content)  ) { //$NON-NLS-1$
                ctx.putProperty(ctxTraceState, true);
                ctx.putProperty(ctxTraceType, AUTO_STAT);
                write(Messages.getString("AutoTrace.24"), ctx); //$NON-NLS-1$
            } else if( 5 == src.size() && // both in either order
                          (4 <= src.get(3).content.length() && "statistics".startsWith(src.get(3).content) 
                        && 3 <= src.get(4).content.length() && "explain".startsWith(src.get(4).content)
                      || 4 <= src.get(4).content.length() && "statistics".startsWith(src.get(4).content) 
                        && 3 <= src.get(3).content.length() && "explain".startsWith(src.get(3).content) )                                                               
                    ) { //$NON-NLS-1$
                ctx.putProperty(ctxTraceState, true);
                ctx.putProperty(ctxTraceType, AUTO_ALL );
                write(Messages.getString("AutoTrace.Enabled"), ctx); //$NON-NLS-1$
            } else if( 3 < src.size() ) { //$NON-NLS-1$
                write(MessageFormat.format(
                      Messages.getString("AutoTrace.ExtraOption"), new Object[] { src.get(src.size()-1).content }) //$NON-NLS-1$
                      , ctx
                ); 
            } else {
                ctx.putProperty(ctxTraceState, true);
                ctx.putProperty(ctxTraceType, AUTO_ALL);
                write(Messages.getString("AutoTrace.Enabled"), ctx); //$NON-NLS-1$
            }
            return true;
        }
        if( "off".equals(src.get(2).content) ) {
            if( 3 < src.size() ) { //$NON-NLS-1$
                write(MessageFormat.format(
                      Messages.getString("AutoTrace.ExtraOption"), new Object[] { src.get(src.size()-1).content }) //$NON-NLS-1$
                      , ctx
                ); 
            } else {
                ctx.putProperty(ctxTraceState, false);
                ctx.putProperty(ctxTraceType, AUTO_NONE);
                write(Messages.getString("AutoTrace.Disabled"), ctx); //$NON-NLS-1$
            }
            return true;
        }
        if( 5 <= src.get(2).content.length() && "traceonly".startsWith(src.get(2).content) ) {
            write(Messages.getString("AutoTrace.26"), ctx); //$NON-NLS-1$
            return true;
        }
        write(Messages.getString("AutoTrace.UnknownAction"), ctx); //$NON-NLS-1$
        return true;
    }
    
    /**
     * @param spaces - Number of spaces to pad with
     * @param str - String to pad
     * @return
     */
    private static String lpad(int spaces, String str) {
        StringBuffer result = new StringBuffer(SPACE.substring(0, spaces - str.length()));
        result.append(str);
        return result.toString();
    }
 
    /**
     * @param str - String to print
     * @param ctx - Scripting Context
     */
    public void write(String str, ScriptRunnerContext ctx) {
        BufferedOutputStream out = ctx.getOutputStream();
        try {
            out.write(ctx.stringToByteArrayForScriptRunnerNonStatic(str));
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
        }
    }
    
    @Override
    protected boolean isListenerOn(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        Boolean state = (Boolean) ctx.getProperty(ctxTraceState);
        if (state == null || state == false) {
            return false;
        }
        return true;
    }
    
    // Stats class to hold the values
    //
    private  class Stat {
        int _value;
        String _name;
        
        Stat(int value, String name) {
            _value = value;
            _name = name;
        }
        
        String getName() {
            return _name;
        }
        
        Integer getValue() {
            return _value;
        }
    }
    
}
