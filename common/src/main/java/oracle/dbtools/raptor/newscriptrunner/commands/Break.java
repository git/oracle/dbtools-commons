/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * @author ramprasad.thummala@oracle.com
 *
 */
public class Break extends CommandListener {
	
	private static final String m_lineSeparator = System.getProperty("line.separator"); //$NON-NLS-1$
	private static final String BREAK = "\\b(?i:br(?:e\\b|ea\\b|eak\\b))"; //$NON-NLS-1$
	private static final String ON = "\\b(?i:(?:on|by|across))\\b"; //$NON-NLS-1$
	//private static final String REPORT_ELEMENT = "(?:\\b(?i:row)\\b|\\b(?i:report)\\b|\\b(.*)\\b)"; //$NON-NLS-1$
	private static final String REPORT_ELEMENT = "(?:\\b(?i:report)\\b|\\b(?i:row)\\b|((?!(?i:report)|(?i:row))([\\p{Graph}\\u00b7])+))"; //$NON-NLS-1$
	
	private static final String SKIP1 = "(?i:\\bski(?:|p)\\b)(\\s+\\d+)?";
	private static final String SKIP2  = "((?i:\\bski(?:|p)\\b)\\s+)?(?i:\\bpage\\b)";
	private static final String SKIP = "(?:" + SKIP1 + "|" + SKIP2 + ")";
	//private static final String SKIP = "(?i:\\bski(?:|p)\\b)\\s+(?:\\d+|(?i:\\bpage\\b))"; //$NON-NLS-1$
	
	private static final String DUP = "(?i:dup(?:|licates))";
	private static final String NODUP = "(?i:no)" + DUP;
	private static final String DUP_OR_NODUP = "(?:" + DUP + "|" + NODUP + ")";
	//private static final String DUP_OR_NODUP = "(?i:no)?(?i:dup(?:|licates))"; //$NON-NLS-1$
	
	//private static final String ACTION = "(?:(?i:ski(?:|p))\\s+(?:\\d+|(?i:page))|(?i:no)?(?i:dup(?:|licates)))"; //$NON-NLS-1$
	private static final String ACTION = "(?:" + SKIP + "|" + DUP_OR_NODUP + ")"; //$NON-NLS-1$
	
	private static final String CMD = BREAK + "(\\s+" + ON +  "\\s+" + REPORT_ELEMENT + "(\\s+" + ACTION + ")*" + ")+";  //$NON-NLS-1$
	private static final String CLEAR = "\\b(?i:cl(?:e\\b|ea\\b|ear\\b))\\s+" + BREAK; //$NON-NLS-1$

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
		String brkcmd = cmd.getSql().trim();
		LinkedHashMap<String, ArrayList<String>> storedBreakcmds = ctx.getStoredBreakCmds();
		Pattern ptrn = Pattern.compile(CMD);
		Matcher m = ptrn.matcher(brkcmd);
		boolean matches = m.matches();
		if(matches) {
			brkcmd = brkcmd.trim();
			storedBreakcmds.clear();
		    brkcmd = brkcmd.replaceFirst(BREAK + "\\s+", "");	
		    String[] rptElesActions = brkcmd.split("(\\s*" + ON +  "\\s*)");
		    for(String rptEleActions: rptElesActions) {
		    	  if(rptEleActions.length() > 0) {
		    		  ArrayList<String> actions = new ArrayList<String>();
		    		  String rptElement =  rptEleActions.split("\\s+")[0];
		    		  rptElement = rptElement.replaceAll("(^(\\\"|\\')|(\\\"|\\')$)", "");
		    		  String reActions = rptEleActions.replaceFirst(Pattern.quote(rptElement), "").replaceFirst("\\s+", "");
		    		  Pattern aptrn = Pattern.compile(ACTION);
		    		  Matcher amtchr = aptrn.matcher(reActions);
		    		  while(amtchr.find()) {
		    			  if(amtchr.group().matches(SKIP2)) {
		    				  actions.add("page");
		    			  }
		    			  else if(amtchr.group().matches(SKIP1)) {
		    				  if(amtchr.group(1) != null && !amtchr.group(1).trim().equals("0")) {
		    					  actions.add(amtchr.group()); 
		    				  }
		    			  }
		    			  else {
			    		      actions.add(amtchr.group());
		    			  }
		    		  }
		    		  if(actions.size() == 0) {
		    			  actions.add("nodup"); //default action
		    		  }
		    		  else {
		    			  Iterator iter = actions.iterator();
		    			  boolean hasDuporNoDup = false; 
		    			  while(iter.hasNext()) {
		    				  String action = (String)iter.next();
		    				  if(action.matches(DUP_OR_NODUP)) {
		    					  hasDuporNoDup = true;
		    					  break;
		    				  }
		    			  }
		    			  if(!hasDuporNoDup) {
		    				  actions.add("nodup"); //default action
		    			  }
		    		  }
		    		  // Bug 20307104 order of the break columns is important.
		    		  String key = getKeyForStr(ctx.getStoredBreakCmds(), rptElement);
		    		  if(key != null && (rptElement.equalsIgnoreCase("report") || rptElement.equalsIgnoreCase("row"))) {
		    			  ctx.getStoredBreakCmds().put(key, actions);
		    		  }
		    		  else {
		    			  LinkedHashMap<String, ArrayList<String>> stoBrkcmds = new  LinkedHashMap<String, ArrayList<String>>();
	    				  if(rptElement.equalsIgnoreCase("report")) {
		    				  // REPORT should be first item i.e., the outermost break column
		    				  stoBrkcmds.put(rptElement, actions);
		    				  stoBrkcmds.putAll(ctx.getStoredBreakCmds());
		    				  ctx.setStoredBreakCmds(stoBrkcmds);
		    			  }
		    			  else if(rptElement.equalsIgnoreCase("row")) {
		    				  // ROW should be last item i.e., the innermost break column
		    				  stoBrkcmds.putAll(ctx.getStoredBreakCmds());
		    				  stoBrkcmds.put(rptElement, actions);
		    				  ctx.setStoredBreakCmds(stoBrkcmds);
		    			  }
		    			  else {
		    				  String rowKey =  getKeyForStr(ctx.getStoredBreakCmds(), "row");
		    				  if(rowKey != null) {
		    					  ArrayList<String> rowVal = new ArrayList<String>();
		    					  ArrayList<String> orowVal = ctx.getStoredBreakCmds().get(rowKey);
		    					  rowVal.addAll(orowVal);
			    				  stoBrkcmds.putAll(ctx.getStoredBreakCmds());
			    				  stoBrkcmds.remove(rowKey);
			    				  stoBrkcmds.put(rptElement, actions);
			    				  stoBrkcmds.put(rowKey, rowVal);
			    				  ctx.setStoredBreakCmds(stoBrkcmds);
		    				  }
		    				  else {
		    					  ctx.getStoredBreakCmds().put(rptElement, actions);
		    				  }
		    			  }
		    		  }
		    	  }
		      }
		}
		else if(brkcmd.matches(BREAK)) {
			String breakcmd = "";
			if(storedBreakcmds.size() > 0) {
				breakcmd = "break ";
				String spaces = String.format("%1$" + breakcmd.length() + "s", " ");
				Iterator<String> iter = storedBreakcmds.keySet().iterator();
				
				int k = -1;
				while(iter.hasNext()) {
					String rptElement = iter.next();
					ArrayList<String> actions = storedBreakcmds.get(rptElement);
					k++;
					if(k > 0) {
						 breakcmd += spaces;
					}
					for(int i = 0; i < actions.size(); i++) {
						if(i == 0)
						   breakcmd += "on " + rptElement + " ";
						String action = actions.get(i);
						breakcmd +=  action + " ";
					}
					breakcmd += m_lineSeparator;
				}
				ctx.write(breakcmd + m_lineSeparator);
			}
			else {
				ctx.write(Messages.getString("NOBREAKS") + m_lineSeparator); 
			}
		}
		else {
			String bcmd = brkcmd;
			String[] patterns = new String[] {BREAK, ON, REPORT_ELEMENT, ACTION};
			for(int i = 0; i < patterns.length; i++) {
				Pattern p = Pattern.compile(patterns[i]);
				Matcher a = p.matcher(bcmd);
				if(a.find()) {
					if(patterns[i].equals(REPORT_ELEMENT)) {
						bcmd = bcmd.replaceFirst(patterns[i], "").trim();
					}
					else {
					    bcmd = bcmd.replaceAll(patterns[i], "").trim();
					}
				}
				else {
					if(patterns[i].equals(ON)) {
						ctx.write(Messages.getString("BRK_SPEC") + m_lineSeparator);
						break;
					}
					else if(patterns[i].equals(ACTION)) {
						ctx.write(MessageFormat.format(Messages.getString("BRK_ACTN"), new Object[] { bcmd.split("\\s+")[0] }) + m_lineSeparator);
						break;
					}
				}
			}
		}
		return false;
	}
	
	private String getKeyForStr(LinkedHashMap<String, ArrayList<String>> storedBreakcmds, String forStr) {
		String key = null;
		Iterator<String> iter = storedBreakcmds.keySet().iterator();
		while(iter.hasNext()) {
			String s = iter.next();
			if(forStr.equalsIgnoreCase(s)) {
				key = s;
				return key;
			}
		}
		return key;
	}
	
	public static boolean hasRowElement(LinkedHashMap<String, ArrayList<String>> storedBreakcmds) {
		boolean flag = false;
		Iterator<String> iter = storedBreakcmds.keySet().iterator();
		while(iter.hasNext()) {
			String s = iter.next();
			if(s.matches("(?i:row)")) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
	}

}
