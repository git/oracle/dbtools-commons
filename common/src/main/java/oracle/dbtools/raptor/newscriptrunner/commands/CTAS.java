/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.omg.CORBA.CTX_RESTRICT_SCOPE;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;

public class CTAS extends CommandListener implements IHelp {

  @Override
  public String getCommand() {
    return "CTAS";
  }

  @Override
  public String getHelp() {
    return CommandsHelp.getString(getCommand());
  }

  @Override
  public boolean isSqlPlus() {
    return false;
  }
/**
 * handle the CTAS event
 * @param conn {@link Connection}
 * @param ctx  {@link ScriptRunnerContext}
 * @param cmd  {@link ISQLCommand}
 * @return {@link Boolean}
 */
  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	    if (   matches("ctas",cmd.getSql())){
	        String[] parts = cmd.getSql().split("\\W");
	        HashMap<String, String> binds = new HashMap<String,String>();
	        if ( parts.length < 3 ){
	         ctx.write(getHelp());
	         return true;
	        }
	        try {
	        	if (conn == null) {
	        		ctx.write(Messages.getString("CTAS_CONNECTION_FAILED")); //$NON-NLS-1$
	        		return false;
	        	}
	        	if(conn.isClosed()){
	        		ctx.write(Messages.getString("CTAS_CONNECTION_FAILED")); //$NON-NLS-1$
	        		return false;
	        	}
	        } catch(Exception e){
	        	Logger.getLogger("ScriptExecutor").log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);		
	        }
	        
	        binds.put("NAME", parts[1].toUpperCase());
	        binds.put("NEW_NAME", parts[2].toUpperCase());
	        DBUtil dbUtil = DBUtil.getInstance(conn);
	        // remove FKs
	        dbUtil.execute("begin DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM, 'REF_CONSTRAINTS',false); end;");
	        // setup the rename
	        dbUtil.execute("begin DBMS_METADATA.SET_REMAP_PARAM(dbms_metadata.session_transform,'REMAP_NAME',:NAME,:NEW_NAME); end;", binds);
	        // get the ddl
	        String orig = dbUtil.executeReturnOneCol("select dbms_metadata.get_ddl('TABLE',:NAME) from dual", binds );
	        if (orig == null) {
	        	ctx.write(MessageFormat.format(Messages.getString("CTAS_NO_SOURCE_TABLE"),new Object[] {parts[1].toUpperCase()})); //$NON-NLS-1$
	        	return false;
	        }
	        else if ( orig!=null && orig.length() < 1){
	          // didn't get anything
	          ctx.write(getHelp());
	          return true;
	        }
	        orig = orig.replace("\"" + parts[1].toUpperCase() + "\"", "\"" + parts[2].toUpperCase() + "\"");
	        String str=orig.trim();
	        if (str.length() > 0 && str.charAt(str.length()-1)==';') {
	            str = str.substring(0, str.length()-1);
	            orig=str;
	          }
	        BufferedReader sr = new BufferedReader(new StringReader(orig));
	        String line = null;
	        String newSQL = "";
	        try {
	        	boolean last=false;
	          // this loop splices off the datatypes since the db gets it from the select
	        ArrayList<String> ddl = new ArrayList<String>();
	        while ( ( line = sr.readLine() ) != null ) {
	        	ddl.add(line);
	        }
	        line="";
	        int i=0;
	        Iterator<String> it = ddl.iterator();
	          while( it.hasNext() ) {
	        	  line=it.next(); i++;
	            if ( line.indexOf("\t\"") > -1) {
	              int q1 = line.indexOf("\"");
	              int q2 = line.indexOf("\"", q1+1);
	              String newLine = line.substring(0,q2+1);
	              if ( line.indexOf(" ",q2) > 0 ){
	                // must have a not null or something after the datatype
	               // newLine += line.substring(line.indexOf(" ",q2+1));
	              } else{
	                newLine +=",";
	              }
	             if ( it.hasNext() && ((ddl.get(i).indexOf("SEGMENT CREATION") > -1)||(ddl.get(i).trim().equals(")")))){//Hack Hack Hack!
	                newSQL += newLine + "\n";
	              } else  if ( ! ( newLine.trim().endsWith(",") ) ) {
		                newSQL += newLine + ",\n";
		          } 
	            } else {
	              newSQL += line + "\n";
	            }
	          }
	        } catch (IOException e) {
	            newSQL = orig;
	        }
	        
	        
	        newSQL += " as \n" + "select * from " + parts[1].toUpperCase();
	        ctx.write(newSQL+"\n");
	        // change the command to normal sql
	        cmd.setStmtClass(SQLCommand.StmtType.G_C_SQL);
	        cmd.setStmtId(SQLCommand.StmtSubType.G_S_CREATE_TABLE);
	        cmd.setSql(newSQL);
	        //Now populate buffersafe
	        String[] sqlArr = newSQL.trim().split("\n");
	        if (ctx.getSQLPlusBuffer()!=null) {
	        	ctx.getSQLPlusBuffer().getBufferSafe().resetBuffer(Arrays.asList(sqlArr));
	        }
	      }
	      
    return true;
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {    
  }

}
