/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptParser;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext.SqlplusVariable;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.query.Bind;


/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=AliasCommand.java"
 *         >Barry McGillin</a>
 *
 */
public class CdCommand extends CommandListener  implements IHelp, IShowCommand{

  /*
   * HELP DATA
   *
   */
  public String getCommand(){
    return "CD"; //$NON-NLS-1$
  }
  
  public String getHelp(){
    return CommandsHelp.getString(getCommand());
  }

  @Override
  public boolean isSqlPlus() {
    return false;
  }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java
	 * .sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String[] tokens = cmd.getSql().trim().split("[ \t\n]+"); //$NON-NLS-1$
		if (tokens.length > 0) {
			String query=null;
			if (tokens[0].toLowerCase().equals(getCommandName().toLowerCase())) {
				if (tokens.length == 1) {
					ctx.getProperties().remove(ScriptRunnerContext.CDPATH);
					try {
						ctx.getMap().put(ScriptRunnerContext.SqlplusVariable._PWD.toString(), Paths.get(".").toRealPath().toString());
					} catch (Exception e) {		} 
					return handleReturn(true);
				} else if (tokens.length > 1) {
					String cdPath=cmd.getSql().trim().substring(2).trim();
					if (((cdPath.startsWith("'")&&cdPath.endsWith("'"))||  //$NON-NLS-1$  //$NON-NLS-2$
						(cdPath.startsWith("\"")&&cdPath.endsWith("\"")))  //$NON-NLS-1$  //$NON-NLS-2$
						&&cdPath.length()>2){
					    cdPath=cdPath.substring(1,cdPath.length()-1);
					}

					if (((System.getProperty("os.name").startsWith("Windows"))   //$NON-NLS-1$  //$NON-NLS-2$
							&&(cdPath.startsWith("/")&&(cdPath.length()>3)  //$NON-NLS-1$  //$NON-NLS-2$
							  &&(cdPath.substring(2,3).equals(":"))))) {   //$NON-NLS-1$ 
						  cdPath=cdPath.substring(1);
					}
					if (((System.getProperty("os.name").startsWith("Windows")))) {   //$NON-NLS-1$  //$NON-NLS-2$
						  cdPath=cdPath.replace("/","\\");  //$NON-NLS-1$  //$NON-NLS-2$
					}
					if (!(System.getProperty("os.name").startsWith("Windows"))) {
						if (cdPath.startsWith("~"))
							cdPath = cdPath.replaceFirst("~", System.getProperty("user.home"));
					}
						Path currentDir = Paths.get(ctx.getMap().get(ScriptRunnerContext.SqlplusVariable._PWD.toString()));
						Path newPath=null;
						try {
						 newPath= currentDir.resolve(cdPath);
						} catch (Exception e) {
					        

						  ctx.write(MessageFormat.format(Messages.getString("CDNOTFOUND"), cdPath)+"\n"); //$NON-NLS-1$  //$NON-NLS-2$
						}
						try {
							newPath =newPath.toRealPath();
						} catch (IOException e1) {		}
						File f = newPath.toFile();
					if ((cdPath!=null)&&f.isDirectory()) {
						String cPath = null;
						try {
							cPath = (new File(newPath.toString())).getCanonicalPath();
						} catch (IOException e) {}
							ctx.putProperty(ScriptRunnerContext.CDPATH,cPath!=null? cPath : cdPath);
						ctx.getMap().put(ScriptRunnerContext.SqlplusVariable._PWD.toString(), cPath!=null?cPath:cdPath); //$NON-NLS-1$ 
					} else {
						ctx.write(MessageFormat.format(Messages.getString("CDNOTFOUND"), cdPath)+"\n"); //$NON-NLS-1$  //$NON-NLS-2$
					}
				}
				return handleReturn(true);
			}
		}
		return false;// not alias handleReturn(false);
	}

    public String getCommandName(){
    	return "CD";  //$NON-NLS-1$
    }
	boolean handleReturn(boolean b) {
		if (b == false) {
			// doWhenever(true);
		}
		// may want to do whenever - do not want to fall back to default as it
		// thinks it is a sql statement as it ends with ;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java
	 * .sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql
	 * .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	 /* (non-Javadoc)
	* @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShow#handle(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	*/
	@Override
		public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String format = null;
		if (ctx.getProperties().containsKey(ScriptRunnerContext.CDPATH)&&(ctx.getProperties().get(ScriptRunnerContext.CDPATH)!=null)) {
			format=(String)ctx.getProperties().get(ScriptRunnerContext.CDPATH);
		} else {
			format="Default";
		}
		ctx.write("");
		ctx.write(MessageFormat.format(Messages.getString("CD"),new Object[]{format}) + "\n");
		return true;
	}
	 
	/* (non-Javadoc)
	* @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShow#getShowCommand()
	*/
	@Override
	public String[] getShowAliases() {
		return new String[]{"cd"};
	}

    @Override
    public boolean needsDatabase() {
        return false;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }

}
