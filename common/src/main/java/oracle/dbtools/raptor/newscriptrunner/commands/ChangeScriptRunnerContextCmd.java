/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;


/**
 * This Command Listener looks at comments in the following format
 * */
/*SQLDEV:SRC:<key>=<value,<key>=<value> ..... */
/*
--SQLDEV:SRC:<key>=<value,<key>=<value> ..... 
* It allows us to change the Script Runner Context from within the script itself.
* The only thing implemented at this time is ScriptRunnerContext.setSQLError , which has to to with what to do if an error occurs.
* USE CASE: I have a script where I want the script runner to stop if an error occurs creating my tables.
* But I dont want to stop when the script is creating indexes, as these are only for performance, and I would rather my script carry on
* Well If you look at CREATE_REPOSITORY.sql you can see I change the script runner context for setSQLError when I create some indexes.
* Aside note, the creation indexes failed sometimes due to BLOCK size of tablespaces being too small ... Go figure. 
* But I didnt want the migration repository creation to fail just because an index failed. 
* 
* Other developers may want to add in other settings to change in the ScriptRunnerContext
 * @author dermot
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ChangeScriptRunnerContextCmd extends AForAllStmtsCommand {
    private final static SQLCommand.StmtSubType s_cmdStmtSubType = SQLCommand.StmtSubType.G_S_COMMENT_SQL;
    public ChangeScriptRunnerContextCmd() {
        super(s_cmdStmtSubType) ;
    }

    public static final String SQLDEVSRC = "SQLDEV:SRC:";
    public static final String SQLERROR = "sqlError";

    @Override
    public void doBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if (isScriptCmd(conn, ctx, cmd)) {
            String command = null;
            if (cmd.getStmtClass() == SQLCommand.StmtType.G_C_MULTILINECOMMENT) {
                command = cmd.getSql().trim().substring(2, cmd.getSql().trim().length() - 2);
            } else if (cmd.getStmtClass() == SQLCommand.StmtType.G_C_COMMENT) {
                command = cmd.getSql().trim().substring(2);
            }
            command = command.substring(SQLDEVSRC.length()).trim();
            String[] commands = command.split(",");
            for (String com: commands) {
                String[] keyValue = com.split("=");
                if (keyValue[ 0 ].equalsIgnoreCase(SQLERROR)) {
                    int value = Integer.parseInt(keyValue[ 1 ]);
                    ctx.setSqlError(value);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean isScriptCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if (cmd.getStmtClass() == SQLCommand.StmtType.G_C_MULTILINECOMMENT) {
            String sql = cmd.getSql().trim().substring(2, cmd.getSql().trim().length() - 2);
            return isSQLDevSRC(sql);
        } else if (cmd.getStmtClass() == SQLCommand.StmtType.G_C_COMMENT) {
            String sql = cmd.getSql().trim().substring(2);
            return isSQLDevSRC(sql);
        } else {
            return false;
        }
    }

    private boolean isSQLDevSRC(String sql) {
        if (sql.startsWith(SQLDEVSRC)) {
            return true;
        } else {
            return false;
        }
    }
}
