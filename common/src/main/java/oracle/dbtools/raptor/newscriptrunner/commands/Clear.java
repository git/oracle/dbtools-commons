/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.SwingUtilities;

import oracle.dbtools.raptor.newscriptrunner.Clearable;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.console.IConsoleClearScreen;

/**
 * 
 * @author Ramprasad Thummala
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=Clear.java" >
 *         Barry McGillin</a>
 *
 */
public class Clear extends CommandListener implements IHelp {

	private static final String ALLCLR_PTRN = "(?:\\bcol(?:u|um|umn|umns)?\\b|\\bscr(?:e|ee|een)?\\b|\\bcomp(?:|u|ut|ute|utes)\\b|\\bbre(?:ak|aks)?\\b|\\bbuf(?:f|fe|fer)?\\b|\\bsq(?:l)?\\b|\\bcontext\\b)"; //$NON-NLS-1$
	private static final String CLEAR = "CLEAR"; //$NON-NLS-1$

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// ignore
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// ignore
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String sqlpcmd = cmd.getSql().toLowerCase().trim().replaceAll("\\s+", " "); //$NON-NLS-1$ //$NON-NLS-2$
		String[] cmds = sqlpcmd.split(" ",2); //$NON-NLS-1$
		String clroption = ""; //$NON-NLS-1$
		boolean top = ctx.getTopLevel();
		boolean bval = false;
		if (cmds.length > 1) {
			Pattern pattern = null;
			pattern = Pattern.compile(ALLCLR_PTRN, Pattern.COMMENTS);
			if (pattern != null) {
				Matcher matcher = pattern.matcher(cmds[1]);
				if (!matcher.find()) {
					ctx.write(MessageFormat.format("SP2-0158: unknown CLEAR option \"{0}\"\n",cmds[1]));
					ctx.errorLog(ctx.getSourceRef(), MessageFormat.format("SP2-0158: unknown CLEAR option \"{0}\"\n",cmds[1]), cmd.getSql());
					return true;
				} else {
					matcher=pattern.matcher(cmds[1]);
				}
				while (matcher.find()) {
					String match = matcher.group();
					if (match.equals("col")||match.equals("colu")||match.equals("colum")||match.equals("column")||match.equals("columns")) { //$NON-NLS-1$
						ctx.getStoredFormatCmds().clear();
						bval = true;
						if (top)
						ctx.write(Messages.getString("Clear.6")); //$NON-NLS-1$
					} else if (match.equals("buff")||match.equals("buffe")||match.equals("buffer")) { //$NON-NLS-1$
							if (ctx.getSQLPlusBuffer() != null) {
								ctx.getSQLPlusBuffer().clear();
								ctx.getSQLPlusBuffer().getBufferSafe().clear();
								if (top)
								ctx.write(Messages.getString("Clear.2")); //$NON-NLS-1$
							} 
					} else if (match.equals("sql")) { //$NON-NLS-1$
						if (ctx.getSQLPlusBuffer() != null) {
							ctx.getSQLPlusBuffer().clear();
							ctx.getSQLPlusBuffer().getBufferSafe().clear();
							if (top)
							ctx.write(Messages.getString("Clear.4")); //$NON-NLS-1$
						}
					} else if (match.equals("comp")||match.equals("compu")||match.equals("comput")||match.equals("compute")||match.equals("computes")) { //$NON-NLS-1$
						ctx.getStoredComputeCmds().clear();
						if (top)
						ctx.write(Messages.getString("Clear.5")); //$NON-NLS-1$
					} else if (match.startsWith("bre")) { //$NON-NLS-1$
						ctx.getStoredBreakCmds().clear();
						if (top)
						ctx.write(Messages.getString("Clear.7")); //$NON-NLS-1$
					} else if (match.equals("context")) { //$NON-NLS-1$
						ctx.deferredReInitClear();
						if (top)
						ctx.write(Messages.getString("Clear.8")); //$NON-NLS-1$
					} else if (match.equals("scr")||match.equals("scre")||match.equals("scree")||match.equals("screen")) { //$NON-NLS-1$

						// grab component which the output is going to
						if (ctx.getSQLPlusConsoleReader() != null) {
							try {
								String prompt = ctx.getPrompt();

								IConsoleClearScreen runner = (IConsoleClearScreen) ctx.getSQLPlusConsoleReader();
								runner.setPrompt(""); //$NON-NLS-1$
								runner.doClearScreen((String) ctx.getProperty(ScriptRunnerContext.CLEARSCREEN));
								runner.setPrompt(prompt);
								bval = true;
							} catch (IOException e) {
								// Ignore this if we cant get it.
							}
						}
						if (ctx.getOutputComponent() instanceof Clearable) {
							final Clearable clearme = (Clearable) ctx.getOutputComponent();
							// use utils to ensure no deadlock
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									try {
										// clear the buffer
										clearme.clear();
									} catch (Exception e) {
										Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
									}
								}
							});
							bval = true;
						}
					} else {
						ctx.write(MessageFormat.format("SP2-0158: unknown CLEAR option \"{0}\"\n",match));
						ctx.errorLog(ctx.getSourceRef(), MessageFormat.format("SP2-0158: unknown CLEAR option \"{0}\"\n",cmds[1]), cmd.getSql());
						return true;
					}

				}
			}
		} else {
			if (!ctx.isSQLPlusClassic()) {
			ctx.write(getHelp());
			}
		}
		return bval;
	}

	@Override
	public String getCommand() {
		return CLEAR;
	}

	@Override
	public String getHelp() {
		return HelpMessages.getString(CLEAR);
	}

	@Override
	public boolean isSqlPlus() {
		return true;
	}
}