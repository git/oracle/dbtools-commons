/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import oracle.dbtools.raptor.newscriptrunner.Clearable;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.console.IConsoleClearScreen;

public class ClearScreen extends AForAllStmtsCommand {
	private static final StmtSubType type = SQLCommand.StmtSubType.G_S_CLEAR_SCREEN;
	public ClearScreen() {
		super(type);
	}

	
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
		if (ctx.getSQLPlusConsoleReader() != null) {
			try {
				((IConsoleClearScreen) ctx.getSQLPlusConsoleReader())
						.doClearScreen((String) ctx.getProperty(ScriptRunnerContext.CLEARSCREEN));
				return true;
			} catch (IOException e) {
				// If it doesnt clear it here, then ignore it for now.
			}
		}
		// grab component which the output is going to
		if (ctx.getOutputComponent() instanceof Clearable) {
			final Clearable clearme = (Clearable) ctx.getOutputComponent();
			// use utils to ensure no deadlock
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					try {
						// clear the buffer
						clearme.clear();
					} catch (Exception e) {
						Logger.getLogger(getClass().getName()).log(
								Level.WARNING, e.getStackTrace()[0].toString(),
								e);
					}
				}
			});
			return true;
		}
		return false;
	}
}
