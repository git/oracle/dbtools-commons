/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtResultType.G_R_NONE;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType.G_S_COLUMN;
import static oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType.G_C_SQLPLUS;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.datatypes.xml.XMLUtil;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowNoRows;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowPrefixNameNewline;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;
import oracle.sql.NUMBER;

/**
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Column.java"
 *         >Barry McGillin</a>
 * 
 */
public class Column extends CommandListener implements IShowCommand, IShowPrefixNameNewline, IShowNoRows  {
	private static final String m_lineSeparator = System.getProperty("line.separator"); //$NON-NLS-1$
	// REGEX patterns to recognize abbreviated & case-insensitive column
	// commands
	private static final String CMD = "\\b(?i:co(?:l\\b|lu\\b|lum\\b|lumn\\b))"; //$NON-NLS-1$
	private static final String CLEAR = "\\b(?i:cl(?:e\\b|ea\\b|ear\\b))"; //$NON-NLS-1$
	private static final String ONOROFF = "\\b(?i:on)|(?i:off)\\b"; //$NON-NLS-1$
	private static final String ON = "\\b(?i:on)\\b"; //$NON-NLS-1$
	private static final String OFF = "\\b(?i:off)\\b"; //$NON-NLS-1$
	private static final String WRAPPED = "(?i:wr(?:a|ap|app|appe|apped))"; //$NON-NLS-1$
	private static final String WORD_WRAPPED = "(?i:wo(?:r|rd|rd_|rd_w|rd_wr|rd_wra|rd_wrap|rd_wrapp|rd_wrappe|rd_wrapped))"; //$NON-NLS-1$
	private static final String TRUNCATED = "(?i:tr(?:u|un|unc|unca|uncat|uncate|uncated))"; //$NON-NLS-1$ 
	private static final String WRAP = "\\b(?:" + WRAPPED + "|" + WORD_WRAPPED + "|" + TRUNCATED + ")\\b"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	private static final String PRINT = "(?i:pr(?:i|in|int))"; //$NON-NLS-1$
	private static final String NPRINT = "(?i:nopr(?:i|in|int))"; //$NON-NLS-1$
	private static final String PRNT = "\\b(?:" + PRINT + "|" + NPRINT + ")\\b"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	private static final String FORMAT = "\\b(?i:fo(?:r|rm|rma|rmat))\\b"; //$NON-NLS-1$
	private static final String HEADING = "\\b(?i:he(?:a|ad|adi|adin|ading))\\b"; //$NON-NLS-1$
	private static final String LEFT = "(?i:l(?:|e|ef|eft))"; //$NON-NLS-1$
	private static final String CENTER = "(?i:c(?:|e|en|ent|ente|entr|enter|entre))"; //$NON-NLS-1$
	private static final String RIGHT = "(?i:r(?:|i|ig|igh|ight))"; //$NON-NLS-1$
//	private static final String JUSTIFY = "\\b(?i:ju(?:s|st|sti|stif|stify))\\s+(?:" + LEFT + "|" + CENTER + "|" + RIGHT + ")\\b"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	private static final String JUSTIFY = "\\b(?i:ju(?:s|st|sti|stif|stify))\\b"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	private static final String NULL = "\\b(?i:nu(?:l|ll))\\b"; //$NON-NLS-1$
	private static final String NEWVAL = "\\b(?i:new_(?:v|va|val|value))\\b"; //$NON-NLS-1$
	private static final String OLDVAL = "\\b(?i:old_(?:v|va|val|value))\\b"; //$NON-NLS-1$
	private static final String HEADVAL_PTRN = HEADING + "\\s+" + "('|\").+('|\")"; //$NON-NLS-1$ //$NON-NLS-2$
	private static final String FOLD_BEFORE = "\\b(?i:fold_b(?:|e|ef|efo|efor|efore))\\b"; //$NON-NLS-1$
	private static final String FOLD_AFTER = "\\b(?i:fold_a(?:|f|ft|fte|fter))\\b"; //$NON-NLS-1$
	private static final String ALIAS = "\\b(?i:ali(?:|a|as))\\b"; //$NON-NLS-1$
	private static final String NUMFORMAT = "\\$?\\b(([9,]+(\\.[9Ee]+)?)|((?i:b|c|d|g|l|s|u|v)?([09]+((\\.|\\,|\\$)[09]*)?))|" //$NON-NLS-1$
			+ "([09]+(((\\.|\\,|\\$)[09]*)|(?i:b|c|d|g|l|s|u|v)|" //$NON-NLS-1$
			+ "(?i:pr|mi))?)|" //$NON-NLS-1$
			+ "(?i:b|c|d|g|l|s|u|v)?([09]+((?i:b|c|d|g|l|s|u|v|[09])*)?)|" //$NON-NLS-1$
			+ "((\\.|\\$)[09]+)|(?i:rn)|(?i:tm(9|e)?)|" //$NON-NLS-1$
			+ "([09]+((\\.)[09]*)?(?i:e){4})|" //$NON-NLS-1$
			+ "([09]*(?i:x)+)|" //$NON-NLS-1$
			+ "((\\$)?[09\\,]+(\\.[09]+)?))\\b"; //$NON-NLS-1$
	
	private static final String FUNCTION = "([\\p{Graph}]+\\((\\'?[^\\(\\)]+\\'?,?)+\\))"; //$NON-NLS-1$
	private static final String EXPR = NUMFORMAT + "|(-?\\w+(?:\\.\\w+(?:E\\w+)?)?(\\s*[-+/\\*]\\s*-?\\w+(?:\\.\\w+(?:E\\w+)?)?)*)|(\\s+)";
	private static final String FUNC_OR_EXPR = FUNCTION + "|" + EXPR;

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String sqlpcmd = cmd.getSql().trim();
		List<String> matchList = new ArrayList<String>();
		String sqlpcmd1 = sqlpcmd.replaceFirst(CMD, "");
		matchList.add("col"); // for column
		Pattern ptrn = Pattern.compile(FUNC_OR_EXPR);
		Matcher matcher = ptrn.matcher(sqlpcmd1);
		while (matcher.find()) {
			if(matcher.group().matches("\\s+|" + m_lineSeparator + "|-")) 
				continue;
		    matchList.add(matcher.group());
		}
		String[] cmds = new String[matchList.size()];
		matchList.toArray(cmds);
		
		if (cmds[0].matches(CMD)) {
			HashMap<String, ArrayList<String>> storedColumns = ctx.getStoredFormatCmds();
			if(ctx.is121LongIdentUsed())
				storedColumns = ctx.get121IdentViewColCmds();
			if (cmds.length <= 2) {
				if (cmds.length == 1)
					printAllColumns(ctx, storedColumns);
				else {
					// Bug 25054105
					String str = sqlpcmd1.replaceAll(FUNC_OR_EXPR, "");
					if(str.length() > 0) {
						if(str.equals("-") && cmds[1] != null) 
							ctx.write(MessageFormat.format(Messages.getString("COLUMN_NOTDEFINED"), new Object[] { cmds[1] }) + m_lineSeparator);
						else
						    ctx.write(MessageFormat.format(Messages.getString("UNKNOWN_COLOPTION"), new Object[] { str }) + m_lineSeparator);
						return true;
					}
					printColumn(ctx, storedColumns, cmds[1].trim());
				}
			} else {
				addOrRemoveColumnCommand(ctx, sqlpcmd);
			}
			return true;
		}
		return false;
	}

	private boolean checkIgnoreSetting(ScriptRunnerContext ctx) {
		Object o = ctx.getProperty(ScriptRunnerContext.IGNORECOLUMNSETTINGS);
		if (o!=null & Boolean.parseBoolean(o.toString())) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * addOrRemoveColumnCommand
	 * 
	 * @param ctx
	 * @param sqlpcmd
	 *            Column
	 */
	private void addOrRemoveColumnCommand(ScriptRunnerContext ctx, String sqlpcmd) {
		HashMap<String, ArrayList<String>> storedColumns = null;
		// Bug 21111863 Code modified with the Regexp to handle
		// quoted strings.
		List<String> matchList = new ArrayList<String>();
		String sqlpcmd1 = sqlpcmd.replaceFirst(CMD, "");
		matchList.add("col"); // for column
		Pattern pattern = Pattern.compile(FUNC_OR_EXPR + "|[^\\s\\\"']+|\\\"[^\\\"]*\\\"|'[^']*'|'[^'\\\"]*\\\"|\\\"[^'\\\"]*'"); //$NON-NLS-1$
		Matcher matcher = pattern.matcher(sqlpcmd1);
		while (matcher.find()) {
			String grp = matcher.group();
			// Bug 25081889 Workaround for this one.
			if(grp.matches("\\w+\\s*-\\s*\\n\\w+")) {
				grp = grp.replaceAll("\\n|-", "");
				String[] grps = grp.split("\\s+");
				for(String x : grps) {
					matchList.add(x);
				}
				continue;
			}
			else if (grp.matches("\\s+")) {
				continue;
			}
		    matchList.add(grp);
		}
		
		String[] cmds = new String[matchList.size()];
		matchList.toArray(cmds);
 
		if (cmds.length >= 3) {
			String col = cmds[1].toLowerCase();
			// This is for LRG/SRG Regression tests
			// checking for _12.1_longident is ON  
			if (checkIgnoreSetting(ctx)) {
				storedColumns = ctx.get121IdentViewColCmds();
			} else {
				storedColumns = ctx.getStoredFormatCmds();
			}
			
			String colcmd = sqlpcmd.replaceFirst(CMD, "").trim(); //$NON-NLS-1$
			// Bug 21477290
			if(col.equals("''") || col.equals("\"\"")) { //$NON-NLS-1$ //$NON-NLS-2$
				ctx.write(MessageFormat.format(Messages.getString("INVALID_COLNAME"), new Object[] { col }) + m_lineSeparator); //$NON-NLS-1$
				return;
			}
			else if(col.matches("'[^']+")) { //$NON-NLS-1$
				String subcmd = colcmd.replaceFirst("'\\s*", "");  //$NON-NLS-1$ //$NON-NLS-2$
				ctx.write(MessageFormat.format(Messages.getString("NOT_TERMINATED"), new Object[] { "'", subcmd, "'" }) + m_lineSeparator); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				return;
			}
            else if(col.matches("\"[^\"]+")) { //$NON-NLS-1$
            	String subcmd = colcmd.replaceFirst("\"\\s*", "");  //$NON-NLS-1$ //$NON-NLS-2$
				ctx.write(MessageFormat.format(Messages.getString("NOT_TERMINATED"), new Object[] { "\"", subcmd, "\"" }) + m_lineSeparator); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				return;
			}
			String clmn = getColumnForAlias(storedColumns, cmds[1]);
			col = clmn.isEmpty() ? col : clmn;
			ArrayList<String> optslist = null;
			optslist = storedColumns.get(col) != null ? storedColumns.get(col) : new ArrayList<String>();
			for (int i = 2; i < cmds.length; i++) {
				String opts = ""; //$NON-NLS-1$
				if (cmds[i].matches(CLEAR)) {
					if (storedColumns.containsKey(col)) {
						storedColumns.remove(col);
						optslist = null;
						continue;
					}
					else {
						ctx.write(MessageFormat.format(Messages.getString("COLUMN_NOTDEFINED"), col) + m_lineSeparator);
					}
				} else if (cmds[i].matches(NEWVAL)) {
					Map<String, String> columnMap = ctx.getColumnMap();
					columnMap.put(col.toUpperCase(), cmds[++i]);
				} else if (cmds[i].matches(OLDVAL)) {
					Map<String, String> columnMap = ctx.getOVColumnMap();
					columnMap.put(col.toUpperCase(), cmds[++i]);
				} else if (cmds[i].matches(ONOROFF)) {
					if (cmds[i].matches(ON)) {
						removeItem(optslist, "off"); //$NON-NLS-1$
					} else if (cmds[i].matches(OFF)) {
						if (!optslist.get(0).equalsIgnoreCase(cmds[i])) {
							optslist.add(0, cmds[i]); // let's have this on top
														// of the list.
						}
					}
				} else if (cmds[i].matches(WRAP)) {
					if (optslist.contains("wrapped")) { //$NON-NLS-1$
						removeItem(optslist, "wrapped"); //$NON-NLS-1$  
					} else if (optslist.contains("word_wrapped")) { //$NON-NLS-1$
						removeItem(optslist, "word_wrapped"); //$NON-NLS-1$
					} else if (optslist.contains("truncated")) { //$NON-NLS-1$
						removeItem(optslist, "truncated"); //$NON-NLS-1$
					}

					if (cmds[i].matches(WRAPPED)) {
						opts = "wrapped"; //$NON-NLS-1$
					} else if (cmds[i].matches(WORD_WRAPPED)) {
						opts = "word_wrapped"; //$NON-NLS-1$
					} else if (cmds[i].matches(TRUNCATED)) {
						opts = "truncated"; //$NON-NLS-1$
					}
				} else if (cmds[i].matches(PRNT)) {
					if (optslist.contains("noprint")) { //$NON-NLS-1$
						removeItem(optslist, "noprint"); //$NON-NLS-1$
					} else {
						removeItem(optslist, "print"); //$NON-NLS-1$
					}

					if (cmds[i].matches(PRINT)) { //$NON-NLS-1$
						opts = "print"; //$NON-NLS-1$
					} else if (cmds[i].matches(NPRINT)) {
						opts = "noprint"; //$NON-NLS-1$
					}
				} else if (cmds[i].matches(FORMAT) || cmds[i].matches(HEADING) || cmds[i].matches(JUSTIFY) /*i + 1 < cmds.length && (cmds[i] + " " + cmds[i + 1]).matches(JUSTIFY)*/ //$NON-NLS-1$
						|| cmds[i].matches(NULL) || cmds[i].matches(ALIAS)) {
					if (i == cmds.length - 1) {
						String val = "" ;
						if(cmds[i].matches(JUSTIFY)) {
							ctx.write(MessageFormat.format(Messages.getString("UNKNOWN_COLJUSTIFY"), new Object[] {val}) + m_lineSeparator);
						}
						else {
							ctx.write(MessageFormat.format(Messages.getString("SQLPLUS.ATTRIBUTE_STRING_VALUE_MISSING"), new Object[] {val}) + m_lineSeparator);
						}
					} else {
						removeItem(optslist, cmds[i]);
						if (cmds[i].matches(FORMAT)) {
							String fmtstr = cmds[i+1];
							if(fmtstr.matches("(?i:a)[1-9]\\d*") || isValidColFormat(fmtstr)) { //$NON-NLS-1$
							    opts = "format "; //$NON-NLS-1$
							}
							else {
								ctx.write(MessageFormat.format(Messages.getString("ILLEGAL_COLFORMAT"), new Object[] { fmtstr }) + m_lineSeparator); //$NON-NLS-1$
								return;
							}
						} else if (cmds[i].matches(HEADING)) {
							opts = "heading "; //$NON-NLS-1$
						} else if (cmds[i].matches(JUSTIFY) /*(cmds[i] + " " + cmds[i + 1]).matches(JUSTIFY)*/) { //$NON-NLS-1$
							opts = "justify "; //$NON-NLS-1$ 
						} else if (cmds[i].matches(NULL)) {
							opts = "null "; //$NON-NLS-1$
						} else if (cmds[i].matches(ALIAS)) {
							opts = "alias "; //$NON-NLS-1$
						}
						String str = cmds[++i];
						if (opts.startsWith("justify")) { //$NON-NLS-1$
							if (str.matches(LEFT)) {
								str = "left"; //$NON-NLS-1$
							}
							else if (str.matches(CENTER)) {
								str = "center"; //$NON-NLS-1$
							}
							else if (str.matches(RIGHT)) {
								str = "right"; //$NON-NLS-1$
							}
							else {
								ctx.write(MessageFormat.format(Messages.getString("UNKNOWN_COLOPTION1"), new Object[] { "JUSTIFY", cmds[i] }) + m_lineSeparator); //$NON-NLS-1$ //$NON-NLS-2$
								return;
							}
						}
						str = str.replaceAll("[\'\"]", ""); //$NON-NLS-1$ //$NON-NLS-2$
						opts += str;
					}
				}
				else if (cmds[i].matches(FOLD_AFTER) || cmds[i].matches(FOLD_BEFORE)) {
					ctx.write(Messages.getString("FOLD_BEFORE_AFTER_NOTSUPPORTED") + m_lineSeparator); //$NON-NLS-1$
				}
				else {
					ctx.write(MessageFormat.format(Messages.getString("UNKNOWN_COLOPTION"), new Object[] { cmds[i] }) + m_lineSeparator); //$NON-NLS-1$
					return;
				}
				
				if (!opts.equals("")) { //$NON-NLS-1$
					if (optslist == null) {
						optslist = new ArrayList<String>();
					}
					optslist.add(opts);
				}
			}
			if (optslist != null) {
				// This is for LRG/SRG Regression tests
				// checking for _12.1_longident is ON  
				if (checkIgnoreSetting(ctx)) {
					if (!optslist.contains("_12.1_longident"))
						optslist.add("_12.1_longident");
				}
				else {
					// that means the column command has been overwritten.
					if (optslist.contains("_12.1_longident")) { //$NON-NLS-1$
						removeItem(optslist, "_12.1_longident");
					}
				}
				storedColumns.put(col, optslist);
			}
		}

	}
	
	/**
	 * 
	 * removeItem
	 * 
	 * @param optslist
	 * @param opt
	 *            Column
	 */
	private void removeItem(ArrayList<String> optslist, String opt) {
		if (optslist != null) {
			for (int k = 0; k < optslist.size(); k++) {
				if (optslist.get(k).startsWith(opt.toLowerCase()) || optslist.get(k).equalsIgnoreCase(opt)) {
					optslist.remove(k);
					break;
				}
			}
		}
	}

	/**
	 * 
	 * printAllColumns
	 * 
	 * @param ctx
	 * @param storedColumns
	 *            Column
	 */
	private void printAllColumns(ScriptRunnerContext ctx, HashMap<String, ArrayList<String>> storedColumns) {
		if (storedColumns != null) {
			for (Map.Entry<String, ArrayList<String>> entry : storedColumns.entrySet()) {
				print(ctx, entry);
			}
		}
	}

	/**
	 * 
	 * printColumn
	 * 
	 * @param ctx
	 * @param storedColumns
	 * @param column
	 *            Column
	 */
	private void printColumn(ScriptRunnerContext ctx, HashMap<String, ArrayList<String>> storedColumns, String column) {
		if (column.length() > 0) {
			for (Map.Entry<String, ArrayList<String>> entry : storedColumns.entrySet()) {
				String clmn = getColumnForAlias(storedColumns, column);
				String col = clmn.isEmpty() ? column : clmn;
				if (entry.getKey().equalsIgnoreCase(col)) {
					print(ctx, entry);
					return;
				}
			}
			if (validColumnName(column)) {
				ctx.write(MessageFormat.format(Messages.getString("COLUMN_NOTDEFINED"), column) + m_lineSeparator); //$NON-NLS-1$
			} else {
				ctx.write(MessageFormat.format(Messages.getString("Column.0"), new Object[] { column }) + m_lineSeparator);  //$NON-NLS-1$
			}
		}
	}

	private boolean validColumnName(String column) {
		if (column.matches("[A-Za-z0-9]+") || column.matches("[A-Za-z_$#0-9]+")) { //$NON-NLS-1$ //$NON-NLS-2$
			return true;
		}
		return false;
	}

	/**
	 * 
	 * print
	 * 
	 * @param ctx
	 * @param entry
	 *            Column
	 */
	private void print(ScriptRunnerContext ctx, Map.Entry<String, ArrayList<String>> entry) {
		if (ctx != null && entry != null) {
			String pstr = ""; //$NON-NLS-1$
			String sstr = ""; //$NON-NLS-1$
			String col = entry.getKey();
//			String clmn = getColumnForAlias(ctx.getStoredFormatCmds(), col);
//			col = clmn.isEmpty() ? col : clmn;
			ArrayList<String> cmdlist = entry.getValue();
			boolean bNewValFound = ctx.getColumnMap().containsKey(col.toUpperCase()) || ctx.getOVColumnMap().containsKey(col.toUpperCase());
			if (cmdlist.size() > 0 || bNewValFound) {
				String str =""; //$NON-NLS-1$
				if(cmdlist.size() > 0) {
					str = cmdlist.get(0);
				}
				else if(bNewValFound){
					str = "ON"; //$NON-NLS-1$
				}
				
				// Bug 22763107 getting rid of quotes for now.
				if (str.equalsIgnoreCase("off")) { //$NON-NLS-1$
					pstr = "COLUMN  " + col + " OFF" + m_lineSeparator; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ $NON-NLS-2$
				} else {
					pstr = "COLUMN  " + col + " ON" + m_lineSeparator; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ $NON-NLS-2$
				}
			}

			// Commenting out for now as SQLPlus is not sorting it anymore.
           //	Collections.sort(cmdlist);
			for (int i = 0; i < cmdlist.size(); i++) {
				String str = cmdlist.get(i);
				if (!str.equalsIgnoreCase("off")) { //$NON-NLS-1$
					String[] cmds = str.split("\\s+"); //$NON-NLS-1$
					if (cmds.length > 1) {
						if (cmds[0].equalsIgnoreCase("heading")) { //$NON-NLS-1$
							pstr += cmds[0].toUpperCase().trim() + "  " + "'" + str.replaceFirst(HEADING + "\\s{1}", "") + "'"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
							// commented out for Bug 19658703
//							pstr += " headsep " + "'" + (String) ctx.getProperty(ctx.SETHEADSEPCHAR) + "'"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							pstr += m_lineSeparator;
						} else if (cmds[0].equalsIgnoreCase("justify")) { //$NON-NLS-1$
							sstr += cmds[0].toUpperCase().trim() + " " + cmds[1] + " "; //$NON-NLS-1$ //$NON-NLS-2$
						} else
							pstr += cmds[0].toUpperCase().trim() + "  " + cmds[1] + m_lineSeparator; //$NON-NLS-1$

					} else {
						if (cmds[0].equalsIgnoreCase("word_wrapped")) { //$NON-NLS-1$
							sstr += "word_wrap "; //$NON-NLS-1$
						} else if (cmds[0].equalsIgnoreCase("wrapped")) { //$NON-NLS-1$
							sstr += "wrap "; //$NON-NLS-1$
						} else if (cmds[0].equalsIgnoreCase("truncated")) { //$NON-NLS-1$
							sstr += "truncate "; //$NON-NLS-1$
						} else if (cmds[0].equalsIgnoreCase("heading")) { //$NON-NLS-1$ checking for empty strings
							sstr += cmds[0].toUpperCase().trim() + "  ''"; //$NON-NLS-1$
						} else {
							sstr += cmds[0].toUpperCase().trim() + " "; //$NON-NLS-1$
						}
					}
				}
			}
			
			// for NEW_VALUE
			if(ctx.getColumnMap().containsKey(col.toUpperCase())) {
				 pstr += "NEW_VALUE" + "  " + ctx.getColumnMap().get(col.toUpperCase()) + m_lineSeparator; //$NON-NLS-1$ //$NON-NLS-2$
			}
			
			// for OLD_VALUE
			if(ctx.getOVColumnMap().containsKey(col.toUpperCase())) {
				 pstr += "OLD_VALUE" + "  " + ctx.getOVColumnMap().get(col.toUpperCase()) + m_lineSeparator; //$NON-NLS-1$ //$NON-NLS-2$
			}
				
			//Bug 19784439 just have a line terminator at the end.
			if(!pstr.endsWith(m_lineSeparator)) {
				pstr += m_lineSeparator;
			}
			
			//Bug 21662069
			if(sstr.length() > 0) {
				if(!sstr.endsWith(m_lineSeparator)) {
					sstr += m_lineSeparator;
				}
				pstr += sstr;
			}
			
			ctx.write(pstr);
		}
	}
	
	boolean isValidColFormat(String fmtstr) {
		boolean bValid = true;
		String fmtdNum = ""; //$NON-NLS-1$
		String numfmtRegExpr = NUMFORMAT;
		// Bug 20668304
		// Bug 20759581 as per
		// http://yourmachine.yourdomain/12/121/server.121/e18404/ch_twelve013.htm#BABBDHHE
		if(fmtstr.matches(numfmtRegExpr)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private String getColumnForAlias(HashMap<String, ArrayList<String>> storedColumns, String alias) {
		String column = ""; //$NON-NLS-1$
		if (alias.length() > 0) {
			for (Map.Entry<String, ArrayList<String>> entry : storedColumns.entrySet()) {
				String col = entry.getKey();
				ArrayList<String> ls = storedColumns.get(col);
				for (int k = 0; k < ls.size(); k++) {
					String colcmd = ls.get(k);
					String[] cmds = colcmd.split("\\s"); //$NON-NLS-1$
					if (cmds[0].equalsIgnoreCase("alias")) { //$NON-NLS-1$
						for (int i = 1; i < cmds.length; i++) {
							if (i == cmds.length - 1) {
								if(cmds[i].equals(alias)) {
									column = col;
									return column;
								}
							} 
						}
					}
				}
			}
			
		}
		return column;
	}

	/**
	 * 
	 * beginScript
	 * 
	 * @param conn
	 * @param ctx
	 * @param cmd
	 *            Column
	 */
	public void beginScript(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/**
	 * 
	 * endScript
	 * 
	 * @param conn
	 * @param ctx
	 * @param cmd
	 *            Column
	 */
	public void endScript(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	private static final String[] SHOWCOLUMN = {"column"}; //$NON-NLS-1$ 

	@Override
	public String[] getShowAliases() {
	    return SHOWCOLUMN;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	    if ((ctx.getCurrentConnection()==null)||(ctx.getCurrentConnection() instanceof OracleConnection)) {
	        if ((ctx.getStoredFormatCmds()!=null) && (ctx.getStoredFormatCmds().size()!=0)) {
                String optionalHeader=(String) ctx.getProperty(ScriptRunnerContext.OPTIONAL_SHOW_HEADER);
                if (optionalHeader!=null&&(!optionalHeader.equals(""))) { //$NON-NLS-1$
                    ctx.write(optionalHeader);
                }
    	        SQLCommand cmdTmp=new SQLCommand(G_C_SQLPLUS, G_S_COLUMN, G_R_NONE, false,Restricted.Level.R4);   
    	        cmdTmp.setOrigSQL("column"); //$NON-NLS-1$ 
    	        cmdTmp.setSql("column"); //$NON-NLS-1$ 
    	        //column command minus before/after listeners eg no echo wanted.
    	        //assumption - nothing else needs to be set in cmdTmp
    	        return this.handleEvent(conn, ctx, cmdTmp);
	        }
	    }
	    return false;
	}

	@Override
	public boolean needsDatabase() {
	    return false;
	}

	@Override
	public boolean inShowAll() {
	    return false;
	}
}
