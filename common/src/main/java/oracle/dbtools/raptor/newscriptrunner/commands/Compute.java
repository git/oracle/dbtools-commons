/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * @author ramprasad.thummala@oracle.com
 *
 */
public class Compute extends CommandListener {
	
	private static final String m_lineSeparator = System.getProperty("line.separator"); //$NON-NLS-1$
	private static final List<String> m_cmptFuncSequence = Arrays.asList("AVG", "COUNT", "MINIMUM", "MAXIMUM", "NUMBER", "SUM", "STD", "VARIANCE");  //$NON-NLS-1$ //$NON-NLS-2$//$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$
	private static final String COMPUTE = "\\b(?i:comp(?:|u|ut|ute))\\b"; //$NON-NLS-1$
	private static final String FUNCTION = "\\b(?i:avg|cou(?:|nt)|min(?:|i|im|imu|imum)|max(?:|i|im|imu|imum)|num(?:|b|be|ber)|sum|std|var(?:|i|ia|ian|ianc|iance))\\b"; //$NON-NLS-1$
	private static final String LABEL = "\\s*((?i:lab(|e|el))\\s+(([^(\\p{Punct})(\\p{Space})]{1,500})|(\\\"[^\\\"]{1,498}\\\")|(\\'[^\\']{1,498}\\')))?(?:(\\r\\n|\\n|\\r|\\-|\\s)*)"; //$NON-NLS-1$
	private static final String OF = "(?i:of)"; //$NON-NLS-1$
	private static final String EXCEPT_ONOF = "(((\"((?!(?i:(on|of))).)*\")|('((?!(?i:(on|of))).)*')|([^\\s]+\\b(?<!\\b(?i:(on|of)))\\b))(\\s+)?)+"; //$NON-NLS-1$
	private static final String CMPT_ELEMENT = EXCEPT_ONOF;
	private static final String FUNC_EXPR = "(" + FUNCTION + LABEL + ")+"; //$NON-NLS-1$ //$NON-NLS-2$
	private static final String CMPT_EXPR = FUNC_EXPR + OF + "\\s+" + CMPT_ELEMENT; //$NON-NLS-1$ 
	private static final String ON = "(?i:(?:on|by|across))"; //$NON-NLS-1$
	private static final String REPORT_ELEMENT = "((\\s+)?(?:(?i:report)|(?i:row)|(?!(?i:report)|(?i:row)|(?i:on))" + EXCEPT_ONOF + "))"; //$NON-NLS-1$ //$NON-NLS-2$
	private static final String REPORT_EXPR = "(" + ON  + "\\s+" + REPORT_ELEMENT + "+" + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	private static final String CMD = 	COMPUTE + "\\s+"  +  CMPT_EXPR  + REPORT_EXPR ; //$NON-NLS-1$
	TreeMap<String, ArrayList<TreeMap<String, HashMap<String, String>>>> m_storedComputecmds = null;
	private static final HashMap<String, String> m_orafunc_to_compfunc = new HashMap<String, String>();
	

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
		String cmptcmd = cmd.getSql().trim();
		ArrayList<String> cmptdElements = new ArrayList<String>();
		TreeMap<String, ArrayList<TreeMap<String, HashMap<String, String>>>> storedComputecmds = ctx.getStoredComputeCmds();
		m_storedComputecmds = storedComputecmds;
		ArrayList<String> funcregExpList = new ArrayList<String>();
		funcregExpList.add("(?i:avg)");
		funcregExpList.add("(?i:cou(?:|nt))");
		funcregExpList.add("(?i:min(?:|i|im|imu|imum))");
		funcregExpList.add("(?i:max(?:|i|im|imu|imum))");
		funcregExpList.add("(?i:num(?:|b|be|ber))");
		funcregExpList.add("(?i:sum)");
		funcregExpList.add("(?i:std)");
		funcregExpList.add("(?i:var(?:|i|ia|ian|ianc|iance))");
		
		
		if(cmptcmd.matches(COMPUTE)) {
			if(storedComputecmds.size() > 0) {
				Iterator<String> ckeys = storedComputecmds.keySet().iterator();
				while(ckeys.hasNext()) {
				    String repCol = ckeys.next();
				    String compute = "COMPUTE "; //$NON-NLS-1$
				    String on = " ON " + repCol; //$NON-NLS-1$
				    String function = ""; //$NON-NLS-1$
					ArrayList<TreeMap<String, HashMap<String, String>>> cmptColKeyList = storedComputecmds.get(repCol);
				    Iterator<TreeMap<String, HashMap<String, String>>> keys = cmptColKeyList.iterator();
				    while(keys.hasNext()) {
				        TreeMap<String, HashMap<String, String>> cmptColKey = keys.next();
				    	Iterator<String> cckeysIter = cmptColKey.keySet().iterator();
						while(cckeysIter.hasNext()) {
							function = ""; //$NON-NLS-1$
							String cmptCol = cckeysIter.next();
							String of = " OF " + cmptCol; //$NON-NLS-1$
							HashMap<String, String> func_to_label = cmptColKey.get(cmptCol);
							Iterator<String> funcIter = func_to_label.keySet().iterator();
							while(funcIter.hasNext()) {
								String func = funcIter.next();
								String labelTxt = func_to_label.get(func);
								function += m_orafunc_to_compfunc.get(func.toUpperCase()).toLowerCase() + " LABEL " + labelTxt + (funcIter.hasNext() ? " " : ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
								if(!funcIter.hasNext())
								   function += of;
							}
						}
						String computeCmd = compute + function + on + m_lineSeparator;
						ctx.write(computeCmd);
				    }
				}
			}
			else {
				ctx.write(Messages.getString("NOCOMPUTES") + m_lineSeparator); //$NON-NLS-1$
			}
		}
		else {
			Pattern ptrn = Pattern.compile(CMD, Pattern.MULTILINE);
			Matcher m = ptrn.matcher(cmptcmd);
			m_orafunc_to_compfunc.put("AVG", "AVG"); //$NON-NLS-1$ //$NON-NLS-2$
			m_orafunc_to_compfunc.put("COUNT", "COUNT"); //$NON-NLS-1$ //$NON-NLS-2$
			m_orafunc_to_compfunc.put("MIN", "MINIMUM"); //$NON-NLS-1$ //$NON-NLS-2$
			m_orafunc_to_compfunc.put("MAX", "MAXIMUM"); //$NON-NLS-1$ //$NON-NLS-2$
			m_orafunc_to_compfunc.put("NUMBER", "NUMBER"); //$NON-NLS-1$ //$NON-NLS-2$
			m_orafunc_to_compfunc.put("SUM", "SUM"); //$NON-NLS-1$ //$NON-NLS-2$
			m_orafunc_to_compfunc.put("STDDEV", "STD"); //$NON-NLS-1$ //$NON-NLS-2$
			m_orafunc_to_compfunc.put("VARIANCE", "VARIANCE"); //$NON-NLS-1$ //$NON-NLS-2$
			
			
			boolean matches = m.matches();
			if(matches) {
				cmptcmd = cmptcmd.replaceFirst(COMPUTE + "\\s+", ""); //$NON-NLS-1$ //$NON-NLS-2$
				String repElements = cmptcmd.replaceFirst(CMPT_EXPR, "").trim().replaceFirst(ON, "").trim(); //$NON-NLS-1$ //$NON-NLS-2$
				Pattern rep = Pattern.compile(REPORT_ELEMENT);
				Matcher rem = rep.matcher(repElements);
				Pattern cep = Pattern.compile(CMPT_ELEMENT);
//				String cmptElements = cmptcmd.replaceAll(FUNC_EXPR, "").trim().replaceFirst(REPORT_EXPR, "").trim().replaceFirst(OF, "").trim(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				String cmptElements = cmptcmd.replaceFirst(FUNC_EXPR, "").trim().replaceFirst(REPORT_EXPR, "").trim().replaceFirst(OF, "").trim(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				// Bug 21432952 fixes in RegExp
				String funcExps = cmptcmd.replaceFirst(OF + "\\s+" + CMPT_ELEMENT + "+", "").trim().replaceFirst(REPORT_EXPR, "").trim(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	            // Bug 26629594
				String warnmsg = "";
				// Eliminate LABELs as this might end up being counted. 
				String funcOnly = funcExps.replaceAll(LABEL, "");
				for (String fn : funcregExpList) {
					Pattern p = Pattern.compile(fn);
					Matcher fm = p.matcher(funcOnly);
					int cnt = 0;
					String fname = "";
					while (fm.find()) {
						cnt += 1;
						fname = fm.group();
					}
					if(cnt > 1) {
						warnmsg += MessageFormat.format(Messages.getString("CMPT_FNMULTIOCCUR"), fname, String.valueOf(cnt)) + System.getProperty("line.separator");
					}
				}
				if(warnmsg.length() > 0) {
					ctx.write(warnmsg);
				}
				
	
	//         Problem with Expressions so commented out			
	//			while(rem.find()) {
	//				String repElement = rem.group().trim();
	//				Matcher cm = cep.matcher(cmptElements);
	//				TreeMap<String, HashMap<String, String>> cmptEle_to_func = null;
	//				while(cm.find()) {
	//					String cmptElement = cm.group().trim();
	//					cmptEle_to_func = processFunction(funcExps, cmptElement);
	//					storeCompute(repElement, cmptElement, cmptEle_to_func);
	//				}
	//			}
				
				String[] repEleArr = repElements.split("\\s+"); //$NON-NLS-1$
//				String[] cmptEleArr = cmptElements.split("\\s+"); //$NON-NLS-1$
				String[] cmptEleArr = null ;
				
				List<String> matchList = new ArrayList<String>();
				Pattern regex = Pattern.compile("[^\\s\"']+|\"[^\"]*\"|'[^']*'");
				Matcher regexMatcher = regex.matcher(cmptElements);
				while (regexMatcher.find()) {
				    matchList.add(regexMatcher.group());
				}
				cmptEleArr = new String[matchList.size()];
				matchList.toArray(cmptEleArr);
				
				for(int i = 0; i < repEleArr.length; i++) {
					String repElement = repEleArr[i].replaceAll("^(\\\"|')|(\\\"|')$", ""); //$NON-NLS-1$ //$NON-NLS-2$
					TreeMap<String, HashMap<String, String>> cmptEle_to_func = null;
					for(int j = 0; j < cmptEleArr.length; j++) {
						String cmptElement = cmptEleArr[j];
						cmptEle_to_func = processFunction(funcExps, cmptElement);
						storeCompute(repElement, cmptElement, cmptEle_to_func);
					}
				}
			}
			else {
				String ccmd = cmptcmd;
				String[] patterns = new String[] {COMPUTE + "\\s+", CMPT_EXPR, REPORT_EXPR}; //$NON-NLS-1$
				boolean innerBreak = false;
				for(int i = 0; i < patterns.length; i++) {
					String pattern = "^" + patterns[i]; //$NON-NLS-1$
					Pattern p = Pattern.compile(pattern);
					Matcher ma = p.matcher(ccmd);
					if(ma.find()) {
						if(i < patterns.length - 2 ) {
						   ccmd = ccmd.replaceAll(patterns[i], "").trim(); //$NON-NLS-1$
					    }
						else {
						   ccmd = ccmd.replaceFirst(patterns[i], "").trim(); //$NON-NLS-1$
						}
					}
					else {
						if(patterns[i].equals(CMPT_EXPR)) {
							Pattern pof = Pattern.compile(OF);
						    Matcher mof = pof.matcher(ccmd);
						    int ofonfind = -1;
						    String funclabel = ""; //$NON-NLS-1$
						    while(mof.find()) {
						    	ofonfind = mof.start();
						    }
						    
						    if(ofonfind == -1) {
						    	Pattern pon = Pattern.compile(ON);
							    Matcher mon = pof.matcher(ccmd);
							    while(mon.find()) {
							    	ofonfind = mon.start();
							    }
						    }
							
						    if(ofonfind > 0) {
							    funclabel = ccmd.substring(0, ofonfind);
							    if(funclabel.length() > 0) {
							    	ccmd = ccmd.replaceFirst(funclabel, ""); //$NON-NLS-1$
							    }
						    }
						    else {
						    	funclabel = ccmd;
						    	if(funclabel.length() > 0) {
							    	ccmd = ccmd.replaceFirst(funclabel, ""); //$NON-NLS-1$
							    }
						    }
								
							while(funclabel.length() > 0) {
								if(funclabel.matches(FUNC_EXPR)) {
									funclabel = funclabel.replaceAll(funclabel, ""); //$NON-NLS-1$
								}
								else {
									String labelRp = "^\\b(?i:lab(|e|el))\\b"; //$NON-NLS-1$
									Pattern lblp = Pattern.compile(labelRp);
									Matcher lm = lblp.matcher(funclabel);
									if(lm.find()) {
										ctx.write(Messages.getString("CMPT_LBL") + m_lineSeparator); //$NON-NLS-1$
										innerBreak = true;
										break;
									}
									else if(funclabel.matches("\\s+")) { //$NON-NLS-1$
										ctx.write(Messages.getString("CMPT_LBLBLANK") + m_lineSeparator); //$NON-NLS-1$
										innerBreak = true;
										break;
									}
									else {
										String grpstr = ""; //$NON-NLS-1$
										Pattern funexpr = Pattern.compile("(" + FUNCTION + LABEL + "?)"); //$NON-NLS-1$ //$NON-NLS-2$
										Matcher fm = funexpr.matcher(funclabel);
										if(fm.matches()) {
											grpstr = fm.group();
											funclabel = funclabel.replaceFirst(grpstr, "").replaceAll("^\\s+",""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
										}
										else {
											String splitExp = "[^\\s\"']+|\"([^\"]*)\"|'([^']*)'|\"([^\"]*)|'([^']*)|\\s+$"; //$NON-NLS-1$
											Pattern splPat = Pattern.compile(splitExp);
											Matcher splm = splPat.matcher(funclabel);
											int x = 0;
											String ftoken = ""; //$NON-NLS-1$
											while(splm.find()) {
												x++;
												ftoken = splm.group();
												if(x == 1 && !ftoken.matches(FUNCTION)) {
													if(ftoken.matches("\\b(?i:lab(|e|el))\\b")) {  //$NON-NLS-1$
												       ctx.write(Messages.getString("CMPT_LBL") + m_lineSeparator); //$NON-NLS-1$
													}
												    else {
												       ctx.write(MessageFormat.format(Messages.getString("CMPT_UNK1"), new Object[] { ftoken.substring(0, 10) }) + m_lineSeparator); //$NON-NLS-1$
												    }
													innerBreak = true;
													break;
												}
												else if(x == 2) {
													if(!ftoken.matches("\\b(?i:lab(|e|el))\\b")) {   //$NON-NLS-1$
														if(ftoken.length() > 0 && !ftoken.matches(FUNCTION)) {
															ctx.write(MessageFormat.format(Messages.getString("CMPT_UNK"), new Object[] { ftoken }) + m_lineSeparator); //$NON-NLS-1$
															innerBreak = true;
															x = 0;
															break;
														}
														else 
														  x = 0;
													}
													else if(ftoken.matches("\\s+")) { //$NON-NLS-1$
														ctx.write(Messages.getString("CMPT_LBLBLANK") + m_lineSeparator); //$NON-NLS-1$
														innerBreak = true;
														break;
													}
													
												}
												else if(x == 3) {
												  	x = 0;
												  	if(ftoken.matches("^\"[^\"]*")) { //$NON-NLS-1$
												  		if(ftoken.length() > 1) {
												  			ctx.write(MessageFormat.format(Messages.getString("CMPT_MISSINGTERM1"), new Object[] { ftoken.substring(0, 10), "\"" }) + m_lineSeparator); //$NON-NLS-1$ //$NON-NLS-2$
												  		}
												  		else {
												  		   ctx.write(MessageFormat.format(Messages.getString("CMPT_MISSINGTERM"), new Object[] { "\"", "\"" }) + m_lineSeparator); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
												  		}
												  		innerBreak = true;
														break;
												  	}
												  	else if(ftoken.matches("^'[^']*")) { //$NON-NLS-1$
												  		if(ftoken.length() > 1) {
												  			ctx.write(MessageFormat.format(Messages.getString("CMPT_MISSINGTERM1"), new Object[] { ftoken.substring(0, 10), "'" }) + m_lineSeparator); //$NON-NLS-1$ //$NON-NLS-2$
												  		}
												  		else {
												  		    ctx.write(MessageFormat.format(Messages.getString("CMPT_MISSINGTERM"), new Object[] { "'", "'" }) + m_lineSeparator); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
												  		}
												  		innerBreak = true;
														break;
												  	}
												  	else if(ftoken.matches("\\s+$")) { //$NON-NLS-1$
												  		ctx.write(Messages.getString("CMPT_LBLBLANK") + m_lineSeparator); //$NON-NLS-1$
														innerBreak = true;
														break;
												  	}
												}
												funclabel = funclabel.replaceFirst(ftoken, "").replaceAll("^\\s+",""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
											}
											
											if(x == 2) {
												ctx.write(Messages.getString("CMPT_LBLTXT") + m_lineSeparator); //$NON-NLS-1$
												innerBreak = true;
												break;
											}
										}
									}
								}
								if(innerBreak) break;
							}	
							if(innerBreak) break;
							
							if(ccmd.matches("^" + OF + "(.*)?")) { //$NON-NLS-1$ //$NON-NLS-2$
								ccmd = ccmd.replaceFirst(OF, "").replaceAll("^\\s+",""); //$NON-NLS-1$ //$NON-NLS-2$
								if(ccmd.matches("^" + CMPT_ELEMENT)) { //$NON-NLS-1$
									ccmd = ccmd.replaceFirst(CMPT_ELEMENT, "").replaceAll("^\\s+","");  //$NON-NLS-1$//$NON-NLS-2$
								}
								else {
									ctx.write(Messages.getString("CMPT_COLS") + m_lineSeparator); //$NON-NLS-1$
									innerBreak = true;
									break;
								}
							}
							else {
								ctx.write(Messages.getString("CMPT_OF") + m_lineSeparator); //$NON-NLS-1$
								innerBreak = true;
								break;
							}							
						}
						else if(patterns[i].equals(REPORT_EXPR)) {
							if(ccmd.length() == 0) {
								ctx.write(Messages.getString("CMPT_ON") + m_lineSeparator); //$NON-NLS-1$
								innerBreak = true;
								break;
							}
							else {
								if(ccmd.matches(ON + "$")) { //$NON-NLS-1$
									ctx.write(Messages.getString("REPT_COLS") + m_lineSeparator); //$NON-NLS-1$
									innerBreak = true;
									break;
								}
							}
							
							
							String on = ON + "\\s+";  //$NON-NLS-1$
							Pattern onp = Pattern.compile(on);
							Matcher onm = onp.matcher(ccmd);
							if(onm.matches()) {
							   int idx = m.start();
							   if(idx == 0) {
								   ccmd = ccmd.replaceAll(on, "").trim(); //$NON-NLS-1$
							   }
							}
							
							String report = REPORT_ELEMENT + "+"; //$NON-NLS-1$
							Pattern rptp = Pattern.compile(report);
							Matcher rptm = rptp.matcher(ccmd);
							if(rptm.matches()) {
							   int idx = m.start();
							   if(idx == 0) {
								   ccmd = ccmd.replaceAll(report, "").trim(); //$NON-NLS-1$
							   }
							}
						}
					}
				}
				
				if(!innerBreak && ccmd.length() > 0) {
					if(ccmd.matches("^" + ON + ".*")) { //$NON-NLS-1$ //$NON-NLS-2$
						ctx.write(Messages.getString("CMPT_ONALREADY") + m_lineSeparator); //$NON-NLS-1$
					}
					else if(ccmd.matches("^" + OF + ".*")) { //$NON-NLS-1$ //$NON-NLS-2$
						ctx.write(Messages.getString("CMPT_OFALREADY") + m_lineSeparator); //$NON-NLS-1$
					}
				}
			}
		}
		return false;
	}
	
	private void storeCompute(String repElement, String cmptElement, TreeMap<String, HashMap<String, String>> cmptEle_to_func) {
		ArrayList<TreeMap<String, HashMap<String, String>>> maps = m_storedComputecmds.get(repElement);
		if(maps == null) {
			maps = new  ArrayList<TreeMap<String, HashMap<String, String>>>();
			maps.add(cmptEle_to_func);
			m_storedComputecmds.put(repElement, maps);
		}
		else {
			for(int i = 0; i < maps.size(); i++) {
				TreeMap<String, HashMap<String, String>> cmpcol_to_func = maps.get(i);
				if(cmpcol_to_func.containsKey(cmptElement)) {
					maps.remove(cmpcol_to_func);
					break;
				}
			}
			maps.add(cmptEle_to_func);
		}
	}
	
	
	private TreeMap<String, HashMap<String, String>> processFunction(String funcExps, String cmpElement) {
		TreeMap<String, HashMap<String, String>> retval = new TreeMap<String, HashMap<String, String>>(String.CASE_INSENSITIVE_ORDER);
		HashMap<String, String> func_to_label = null;
//		Pattern fp = Pattern.compile("(" + FUNCTION + LABEL + "?)"); //$NON-NLS-1$ //$NON-NLS-2$
		Pattern fp = Pattern.compile("(" + FUNCTION + LABEL + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		Matcher fm = fp.matcher(funcExps);
		while(fm.find()) {
			String funcExp = fm.group().trim();
//			String func = funcExp.replaceFirst(LABEL + "(\\s+(\\-(\\s+)?)?)?", "").trim(); //$NON-NLS-1$ //$NON-NLS-2$
			String func = funcExp.replaceAll(LABEL, "").trim(); //$NON-NLS-1$ //$NON-NLS-2$
			String lblandtxt = funcExp.replaceFirst(FUNCTION, "") ; //$NON-NLS-1$
			String preval = ""; //$NON-NLS-1$
			if(func.matches(FUNCTION)) {
				if(func.matches("\\b(?i:avg)\\b")) { //$NON-NLS-1$
					preval = func;
					func = "avg"; //$NON-NLS-1$
				}
				if(func.matches("\\b(?i:cou(?:|nt))\\b")) { //$NON-NLS-1$
					preval = func;
					func = "count"; //$NON-NLS-1$
				}
				else if(func.matches("\\b(?i:min(?:|i|im|imu|imum))\\b")) { //$NON-NLS-1$
					preval = func;
					func = "min"; //$NON-NLS-1$
				}
                else if(func.matches("\\b(?i:max(?:|i|im|imu|imum))\\b")) { //$NON-NLS-1$
                	preval = func;
					func = "max"; //$NON-NLS-1$
				}
                else if(func.matches("\\b(?i:num(?:|b|be|ber))\\b")) { //$NON-NLS-1$
                	preval = "number"; //$NON-NLS-1$
					func = "number"; //$NON-NLS-1$
				}
                else if(func.matches("\\b(?i:sum)\\b")) { //$NON-NLS-1$
                	preval = func;
					func = "sum"; //$NON-NLS-1$
				}
                else if(func.matches("\\b(?i:std)\\b")) { //$NON-NLS-1$
                	preval = func;
					func = "stddev"; //$NON-NLS-1$
                }
                else if(func.matches("\\b(?i:var(?:|i|ia|ian|ianc|iance))\\b")) { //$NON-NLS-1$
                	preval = func;
					func = "variance"; //$NON-NLS-1$
				}
                
			}
			
			if(lblandtxt.isEmpty()) {
				if(func.equals("max")) { //$NON-NLS-1$
					lblandtxt = "'" + "maximum" + "'"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
				else if(func.equals("min")) { //$NON-NLS-1$
					lblandtxt = "'" + "minimum" + "'"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
				else if(func.equals("stddev")) { //$NON-NLS-1$
					lblandtxt = "'" + "std" + "'"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
				else if(func.equals("count")) { //$NON-NLS-1$
				   lblandtxt =  "'" + "count" + "'"; //$NON-NLS-1$ //$NON-NLS-2$
				}
				else {
					lblandtxt = "'" + func.toLowerCase() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
			else {
				lblandtxt = funcExp.replaceFirst(FUNCTION, "").trim().replaceFirst("\\b(?i:lab(?:|e|el))\\b\\s+", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				int idx = lblandtxt.lastIndexOf("-"); //$NON-NLS-1$
				if(idx > -1) {
					lblandtxt = lblandtxt.substring(0, idx-1).trim();
				}
				
				// Bug 21430771 look if the single quote is at the first occurrence i.e., at index 0.
				// also replace 2 single/double quotes to 1 single/double quote.
				lblandtxt = lblandtxt.replaceAll("''", "'"); //$NON-NLS-1$ //$NON-NLS-2$
				lblandtxt = lblandtxt.replaceAll("\"\"", "\""); //$NON-NLS-1$ //$NON-NLS-2$
				lblandtxt = lblandtxt.replaceAll("\"", "'");
//				lblandtxt = lblandtxt.replaceAll("^(\\\"|')|(\\\"|')$", ""); //$NON-NLS-1$ //$NON-NLS-2$
				idx = lblandtxt.indexOf("'"); //$NON-NLS-1$
				if(idx == -1) {
					lblandtxt = "'" + lblandtxt + "'"; //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
			if(func_to_label == null) 
			     func_to_label = new LinkedHashMap<String, String>();
			func_to_label.put(func, lblandtxt);
		}
		
		// Bug 21432952
		String cmpElm = cmpElement.startsWith("\"") || cmpElement.startsWith("'") ? cmpElement.substring(1, cmpElement.length()) : cmpElement; //$NON-NLS-1$ //$NON-NLS-2$
		cmpElm = cmpElm.endsWith("\"") || cmpElm.endsWith("'") ? cmpElm.substring(0, cmpElm.length()-1) : cmpElm; //$NON-NLS-1$ //$NON-NLS-2$
		retval.put(cmpElm, func_to_label);
		
		return retval;
	}
	
	/**
	 * Sorts the function maps based on the function order as defined by
	 * m_cmptFuncSequence.
	 * 
	 * @param list
	 */
	public static void sort(ArrayList<TreeMap<String, HashMap<String, String>>> list) {
		Collections.sort(list, new Comparator<TreeMap<String, HashMap<String, String>>>() {
			@Override
			public int compare(TreeMap<String, HashMap<String, String>> o1, TreeMap<String, HashMap<String, String>> o2) {
				
				HashMap<String, String> func1_to_lbl1 = o1.get(o1.keySet().toArray()[0]);
				String func1 = m_orafunc_to_compfunc.get(((String)func1_to_lbl1.keySet().toArray()[0]).toUpperCase());
//				String func1 = ((String)func1_to_lbl1.values().toArray()[0]).toUpperCase().replaceAll("'", "");
				
				HashMap<String, String> func2_to_lbl2 = o2.get(o2.keySet().toArray()[0]);
				String func2 = m_orafunc_to_compfunc.get(((String)func2_to_lbl2.keySet().toArray()[0]).toUpperCase());
//				String func2 = ((String)func2_to_lbl2.values().toArray()[0]).toUpperCase().replaceAll("'", "");
				
				return Integer.valueOf(m_cmptFuncSequence.indexOf(func1))
				         .compareTo(Integer.valueOf(	m_cmptFuncSequence.indexOf(func2)));
			}
		});
	}
	
	public static void sortFunctions(ArrayList<TreeMap<String, HashMap<String, String>>> list) {
		for(int i = 0; i < list.size(); i++) {
			TreeMap<String, HashMap<String, String>> colMap = list.get(i);
			Iterator<String> colKeys = colMap.keySet().iterator();
			while(colKeys.hasNext()) {
				String colKey = colKeys.next();
				HashMap<String, String> func_to_lbl = colMap.get(colKey);
				HashMap<String, String> srtdfunc_to_lbl = sort(func_to_lbl);
				colMap.put(colKey, srtdfunc_to_lbl);
			}
		}
		
	}
	
	public static HashMap<String, String> sort(HashMap<String, String> map) {
		 List<Map.Entry<String, String>> list = 
					new LinkedList<Map.Entry<String, String>>(map.entrySet());
		 
		 Collections.sort(list, new Comparator<Object>() {
			  @Override
		      public int compare(Object o1, Object o2) {
				  String func1 =((Map.Entry) (o1)).getValue().toString().toUpperCase().replaceAll("\'", "");
				  func1 = func1 == null ? ((Map.Entry) (o1)).getKey().toString().toUpperCase() : func1;
				  func1 = m_orafunc_to_compfunc.get(func1);
				  String func2 =((Map.Entry) (o2)).getValue().toString().toUpperCase().replaceAll("\'", "");
				  func2 = func2 == null ? ((Map.Entry) (o2)).getKey().toString().toUpperCase() : func2;
				  func2 = m_orafunc_to_compfunc.get(func2);
				  int val1 = m_cmptFuncSequence.indexOf(func1);
				  int val2 = m_cmptFuncSequence.indexOf(func2);
				  return Integer.valueOf(val1).compareTo(val2);
		      }
		 });
		 
		HashMap<String, String> sortedMap = new LinkedHashMap<String, String>();
		for (Iterator<Map.Entry<String, String>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, String> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
        return sortedMap;
	}
	
	public static int getIndexofFunction(String func) {
		return m_cmptFuncSequence.indexOf(func.toUpperCase());
	}
	
	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
		// TODO Auto-generated method stub

	}

}
