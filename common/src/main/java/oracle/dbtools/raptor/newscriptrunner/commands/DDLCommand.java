/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.ddl.DbmsMetadataDDLGenerator;

/**
 * Generate DDL for an object name
 *
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=DDLCommand.java"
 *         >Barry McGillin</a>
 *
 */
public class DDLCommand extends CommandListener implements IHelp, Connected {






  protected static final String CMD = "ddl"; //$NON-NLS-1$

  protected final static SQLCommand.StmtSubType s_cmdStmtSubType = SQLCommand.StmtSubType.G_S_DDL;

  // Keywords
  protected static final String SAVE_KWD = "SAVE"; //$NON-NLS-1$

 

  private Connection _conn = null;

  private ScriptRunnerContext _ctx = null;

  /**
   *
   * @return
   */
  protected String help() {
    return CommandsHelp.getString(getCommand());
  }

  /*
   * (non-Javadoc)
   * 
   * @see oracle.dbtools.raptor.newscriptrunner.commands.AForAllStmtsCommand#
   * handleEvent(java.sql.Connection,
   * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
   * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
   */
  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	  DbmsMetadataDDLGenerator ddlGen = new DbmsMetadataDDLGenerator(conn,ctx);
    try {
      if (conn == null || !conn.isValid(10)) {
        ctx.write(Messages.getString("DDLCommand.71")); //$NON-NLS-1$
        return true;
      }
      setContext(ctx);
    } catch (SQLException e) {
      ctx.write(Messages.getString("DDLCommand.72")); //$NON-NLS-1$
      return true;
    }

    String sql = cmd.getSql();

    ArrayList<String> cmds = new ArrayList<String>();
    splitArgs(sql, cmds);

    int length = cmds.size();

    // Show Help
    if (length == 1) {
      ctx.write(help());
      return true;
    }

    if (conn != null) {
      setConn(conn);

      // Extract SAVE [<filename>]
      String filename=null;
      if (length >= 4 &&  
           (SAVE_KWD.equalsIgnoreCase(cmds.get(2))|| SAVE_KWD.equalsIgnoreCase(cmds.get(3))) ) {
        filename = cmd.getSql().trim().substring(cmd.getSql().toLowerCase().trim().indexOf(" save ") + 6);
        if (filename!=null) {
        	filename=filename.trim().replaceAll("\"", "").replaceAll("'", "");
        }
        if (filename.lastIndexOf(".") == -1) { //$NON-NLS-1$
          String suffix = (String) ctx.getProperty(ScriptRunnerContext.SUFFIX);
          if (suffix.length() > 0)
            filename = filename + "." + suffix; //$NON-NLS-1$
        }
      } else {
        filename = null;
        if (cmds.size()>=3) {
        	  if (SAVE_KWD.equalsIgnoreCase(cmds.get(2))|| (cmds.size()>3 && SAVE_KWD.equalsIgnoreCase(cmds.get(3)))) {
        		  // This is a save without a filename
        		  ctx.write("Invalid option\n");
        		  ctx.write(help());
        		  return true;
        	  }
        }
      }
      //remove quotes from filename
      if (filename!=null) {
    	  filename=filename.trim().replaceFirst("^\"", "").replaceFirst("\"$","");
      }
      // Extract type
      String resultType;
      if (length >= 3 && !cmds.get(2).equalsIgnoreCase(SAVE_KWD)) {
        StringBuilder sb = new StringBuilder();
        sb.append(cmds.get(2));

        for (int i = 3; i < length; i++) {
          sb.append(" "); //$NON-NLS-1$
          sb.append(cmds.get(i));
        }

        length = 2;
        resultType = sb.toString();
      } else {
        resultType = null;
      }

      // Extract Object
      String object;
      if (length >= 2) {
        object = cmds.get(1);
      } else {
        object = null;
      }

      if (object != null) {
        ddlGen.processDDL(object, resultType, filename, false);
        return true;
      }
    }

    writeToCtx(MessageFormat.format(Messages.getString("DDL_PARSE_ERROR"), new Object[] {})); //$NON-NLS-1$
    ctx.write(help());

    return true;
  }

  /**
   *
   * @param ctx
   */
  protected void setContext(ScriptRunnerContext ctx) {
    _ctx = ctx;
  }

  /**
   *
   * @param conn
   */
  protected void setConn(Connection conn) {
    _conn = conn;
  }

  protected boolean splitArgs(String commandline, ArrayList<String> args) {
    final int lineLength = commandline.length();
    final int terminalIndex = lineLength - 1;
    final char nullChar = '\0';

    StringBuilder arg = new StringBuilder();
    StringBuilder quoted = new StringBuilder();
    char quoteChar = nullChar;

    int i = 0;
    while (i < lineLength) {
      char ch = commandline.charAt(i);

      if (quoteChar != nullChar) {
        quoted.append(ch);

        if (ch == quoteChar) {
          arg.append(quoted);
          quoted = new StringBuilder();
          quoteChar = nullChar;
        }
      } else if (ch == '"' || ch == '\'') {
        quoted.append(ch);
        quoteChar = ch;
      } else if (Character.isWhitespace(ch)) {
        if (arg.length() > 0) {
          args.add(arg.toString());
          arg = new StringBuilder();
        }
      } else {
        arg.append(ch);
      }

      if (i == terminalIndex && quoteChar != nullChar) {
        arg.append(quoteChar);
        i = i - (quoted.length() - 1);
        quoted = new StringBuilder();
        quoteChar = nullChar;
      }

      i++;
    }

    if (arg.length() > 0) {
      args.add(arg.toString());
    }

    return true;
  }

  protected void writeToCtx(String message, BufferedWriter bw) throws IOException {
    if (message != null && bw == null) {
      _ctx.write(message);
      _ctx.write("\n"); //$NON-NLS-1$
    }

    if (message != null && bw != null) {
      bw.write(message);
      bw.newLine();
    }
  }

  protected void writeToCtx(String message) {
    try {
      writeToCtx(message, null);
    } catch (IOException e) {
      ; // consume - can never happen as null passed
    }
  }

  /**
     *
     */
  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
  }

  /**
     *
     */
  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
  }

  /**
     *
     */
  public String getCommand() {
    return "DDL"; //$NON-NLS-1$
  }

  /**
     *
     */
  public String getHelp() {
    return help();
  }

  /**
     *
     */
  public boolean isSqlPlus() {
    return false;
  }

  @Override
  public boolean needsConnection() {
    return true;
  }

}
