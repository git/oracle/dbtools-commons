/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.SQLPLUSCmdFormatter;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.JSONWrapBufferedOutputStream;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryXMLSupport;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.dbtools.raptor.utils.ExceptionHandler;
import oracle.dbtools.raptor.utils.ResolvedDBObject;
import oracle.dbtools.util.Closeables;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleResultSet;
/*
  Syntax : DESC[RIBE] {[schema.]object[@db_link]}
  Lists the column definitions for the specified table, view or synonym, or the specifications for the specified function or procedure.
  schema : Represents the schema where the object resides. If you omit schema, SQL*Plus assumes you own object.
  object : Represents the table, view, type, procedure, function, package or synonym you wish to describe.
  @db_link : Consists of the database link name corresponding to the database where object exists.

*/
public class DescribePrep extends CommandListener {
    private Connection _conn;
    private ScriptRunnerContext m_ctx;
    private static final String DESCRIBE = "(?i:desc(?:|r|ri|rib|ribe))"; //$NON-NLS-1$ 
    private static final String JAVAREGEX = "[^#|\"][\\w|\\$#]+|[\\w|\\$]+|[\\w]+[.][\\w|\\$]+|\"[\\w]+\"[.][\\w|\\$]+|\"[\\w]+\"[.]\"[\\w|\\$]+\"|[\\w]+[.]\"[\\w|\\$]+\"|[\\w]+[.]\"[\\w\\s]+\"|\"[\\w\\s]+\"[.]\"[\\w\\s]+\"|\"[\\w]+\"[.]\"[\\w|\\$]+\"@\\w+|[\\w]+\"[\\w\\s]+\"|\"[\\w\\s]+\"|\"[\\w]+[.][\\w\\s]+\"||[\\w]+[.][\\w]+@\\w+|\"[\\w\\s]+\"@\\w+|[\\w]+@\\w+|\"[\\w\\s]+[.][\\w\\s]+\"@\\w+|\"[\\w\\s]+\"[.]\\w+@\\w+";
   // private static final String JAVAREGEX = "((\\\")?\\w+(\\\")?\\@(\\\")?\\w+(\\\")?)|((\\\")?\\w+(\\\")?\\.(\\\")?\\w+(\\\")?\\@(\\\")?\\w+(\\\")?)|((\\\")?\\w+(\\\")?\\.(\\\")?\\w+(\\\")?)|((\\\")?\\w+(\\\")?)";
    private static final String OBJECTNAME = DESCRIBE + "\\s+"+JAVAREGEX;  //$NON-NLS-1$
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private QueryXMLSupport m_xml = QueryXMLSupport.getQueryXMLSupport("oracle/dbtools/raptor/newscriptrunner/ScriptParserQueries.xml"); //$NON-NLS-1$
    
    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        
    }

    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    	if ( m_isTypeMethods)
    		describeTypeMethods(conn, ctx, cmd);
    	clearColumnFormats(ctx);
    	if ( m_isSetNullChanged ) {
    		ctx.putProperty(ScriptRunnerContext.SETNULL, m_setNull);
    		m_isSetNullChanged = false;
    		m_setNull = null;
    	}
    }
    
    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    	boolean usage = false;
    	if (cmd.getStmtClass().equals(SQLCommand.StmtType.G_C_SQLPLUS) && cmd.getStmtId().equals(SQLCommand.StmtSubType.G_S_DESCRIBE) && (conn == null || conn instanceof OracleConnection)) {
            _conn = conn;
            setScriptRunnerContext(ctx);
            parseDescribe(ctx, cmd, conn);
        }
    	usage = ctx.getProperties().get("descr.usage")!=null?(Boolean)ctx.getProperties().get("descr.usage"):Boolean.FALSE;
        if ( matches("describe",cmd.getSql())  && usage ) {
              ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESCR_USAGE) + LINE_SEPARATOR);
              return true;
        }        
        return false;
    }
    
    public void parseDescribe(ScriptRunnerContext ctx, ISQLCommand cmd, Connection conn) {
	
	    String[] res = new String[] {"","unknown_table"};
	    boolean ownerNotSpecified = false;
	    
	    String describePtrn = "(?i:desc(?:|r|ri|rib|ribe)\\b\\s*)";
	    String rawName = cmd.getSql().replaceFirst(describePtrn, "").trim();
	    
	    // Bug 25108328 - ENTER DESC ONLY SHOULD GET THE USAGE OF DESC 
	    if ( "".equals(rawName) ) {
	    	ctx.putProperty("descr.usage", Boolean.TRUE);
	   	    return;
	    }
	    
	    //Bug 25108475 - 4.2: NLS: SQLCL: DESCRIBE COMMAND NOT WORK FOR MULTIBYTE TABLE NAME 
	    // Enables the Unicode version of Predefined character classes and POSIX character classes.
	    if ( ! Pattern.compile(JAVAREGEX, Pattern.UNICODE_CHARACTER_CLASS).matcher(rawName).matches()) {
	    	if (!validateRawObjectName(ctx, rawName) ) {
	    		ctx.putProperty("descr.usage", Boolean.FALSE);
	    		return;
	    	}
	    	ctx.putProperty("descr.usage", Boolean.TRUE);
	    	return;
	    }
	    /*
	    if ( !rawName.matches(JAVAREGEX) ) {
	    	if (!validateRawObjectName(ctx, rawName) ) {
	    		ctx.putProperty("descr.usage", Boolean.FALSE);
	    		return;
	    	}
	        ctx.putProperty("descr.usage", Boolean.TRUE);
	   	    // ctx.write("\n"+ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESCR_USAGE) + "\n");
	   	    return;
	    } */
	    
	    if ( !isValidDBLinkConnection (ctx, rawName)) {
	    	return;
	    }
	    //This could a bit bold.  Why not save it and replace it when you are done.
	    m_setNull = (String) ctx.getProperty(ScriptRunnerContext.SETNULL);
	    ctx.removeProperty(ScriptRunnerContext.SETNULL);
	    ctx.putProperty(ScriptRunnerContext.SETNULL, "");
	    m_isSetNullChanged = true;
	    
	    String filter = null;
	    SetServerOutput.coreEndWatcher(conn, ctx,cmd);
	    // Bug 22783070 - DESCRIBE OBJECT WITH DBLINK IS NOT WORKING 
	    ResolvedDBObject dbObj = ResolvedDBObject.resolveDBObject(conn, rawName, filter);

	    // Bug 21047542 - DESC A PROCEDURE WHOSE NAME HAS A SPACE IS NOT WORKING FINE 
	    // Filtered out unwanted syntax
	    if ( dbObj == null || (dbObj != null && dbObj.isBad()) ) {
	    	if (conn == null) {
	    		ctx.write(LINE_SEPARATOR+ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOT_CONNECTED)+LINE_SEPARATOR);
	    		return;
	    	}
	    	if ( !checkObjectExists(rawName)) {
	    		ctx.write(LINE_SEPARATOR+ScriptRunnerDbArb.format(ScriptRunnerDbArb.DESCR_NO_OBJECT,rawName)+LINE_SEPARATOR);
	    		ctx.putProperty("descr.usage", Boolean.FALSE); //$NON-NLS-1$
	    		
	        //add special adhocsql json to the output
	        if(ctx.getOutputStream() instanceof JSONWrapBufferedOutputStream){
	        	Integer sqlcode = 4043;
	        	String jdbcErrorMessage =  JSONWrapBufferedOutputStream.quote(ScriptRunnerDbArb.format(ScriptRunnerDbArb.DESCR_NO_OBJECT,rawName),true);
	        	String errorInfo = MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.JSON_COMMAND_ERR), new Object[] {0+sqlcode,cmd.getStartLine()+1,0,jdbcErrorMessage});
	        	((JSONWrapBufferedOutputStream)ctx.getOutputStream()).addStmtInfo(errorInfo);
	        }
	    		return;
	    	}
	    }
			if (cmd.getSql().contains(".")) {
				StringTokenizer st = new StringTokenizer(cmd.getSql(), "\\.");
				StringTokenizer st2 = new StringTokenizer(st.nextToken(), "\\ ");
				st2.nextToken();
				res[0] = st2.nextToken();// assuming no spaces in name!
				res[1] = st.nextToken();
			} else {
				String regex = "\"([^\"]*)\"|(\\S+)";
				//	    "([^"]*)"|(\S+)
				//	    \_____/  \___/
				//	       1       2
				//	   There are 2 alternates:
				//
				//	   The first alternate matches the opening double quote, a sequence of anything but double quote (captured in group 1), then the closing double quote
				//	   The second alternate matches any sequence of non-whitespace characters, captured in group 2
				    Matcher m = Pattern.compile(regex).matcher(cmd.getSql());
				m.find();// jump over desc
				while (m.find()) {
					res[1] = m.group();
				}
				ownerNotSpecified=true;
			}
	        res[ 1 ] = res[ 1 ].replaceAll("\n", ""); //$NON-NLS-1$ //$NON-NLS-2$
	        res[ 1 ] = res[ 1 ].replaceAll("\r", ""); //$NON-NLS-1$ //$NON-NLS-2$
	        
	        String orgName = res[1];
	        String objType = getObjType(res[ 1 ], res[ 0 ]);
	        String viewType = "all";
	        if (objType == null || objType.length() == 0 && res[1] != null && res[1].length() > 0)
	        {
	            objType = getObjTypeDBA(res[1], res[0]);
	            if (objType != null && objType.length() > 0)
	            {
	                viewType = "dba";
	            }
	        }
	        // quick dirty fix for bug 5982551. A table & an index could share the same
	        // name.
	        // executeQuery called from getObjType would in such case return
	        // "TABLE/nINDEX"
	        // return the first one, which seems to be the table always
	        if (objType.indexOf("\n") > -1) { //$NON-NLS-1$
	        	// Bug 25047425 - DESC GET ERROR EVEN FOR A EXISTED OBJECT 
	        	// Bug 24760904 - GET SP2-0749 WHEN DESC A PROCEDURE USING SCHEMA.PROCEDURE_NAME 
	        	if ( objType.contains("SYNONYM") && objType.contains("TABLE"))
	        		objType = "SYNONYM" ;
	        	else 
	        		objType = objType.split("\n")[ 0 ]; //$NON-NLS-1$
	        }
	        if (objType.equals("SYNONYM")) { //$NON-NLS-1$
	            // find base object
	        	String details[] = getBaseSynonymObjectRecursive( res[ 1 ], res[ 0 ]);
	        	if (details == null )
	        		return;
	            objType = details[ 2 ];
	            // Bug 25087151 - DESC SYNONYM GET SP2-0749 FOR NON-EXISTENT TABLE 
	            if ( UNDEFINED_OBJECT_TYPE.equals(objType)) {
	            	String object = "";
	            	if ( details[0] != null && !("".equals(details[0])) ) {
	            		object = "\""+details[0]+"\"."; //$NON-NLS-1$
	            	}
	            	if ( details[1] != null && !("".equals(details[1])) ) {
	            		object += "\""+details[1].replaceAll("\"|\'", "")+"\""; //$NON-NLS-1$
	            	}            		
	            	ctx.write(LINE_SEPARATOR+ScriptRunnerDbArb.format(ScriptRunnerDbArb.DESCR_NO_OBJECT,object)+LINE_SEPARATOR);
		    		ctx.putProperty("descr.usage", Boolean.FALSE); //$NON-NLS-1$
		    		return;
	            }
	            
	            if (objType.indexOf("\n") > -1) { //$NON-NLS-1$
		            objType = objType.split("\n")[ 0 ]; //$NON-NLS-1$
		        }
	            
	            res[ 0 ] = details[ 0 ];
	            // Bug 22095455 - FAIL TO DESC SYNONYM FOR LOWER CASE TABLE 
	            res[ 1 ] = "'"+details[ 1 ]+"'"; // add single quotes 
	        }
	        String displayName = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DISPLAY_NAME);
	        String displayNull = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DISPLAY_NULL);
	        String displayType = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DISPLAY_TYPE);
	        String displayArgumentName = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DISPLAY_ARGUMENT_NAME);
	        String displayInOut = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DISPLAY_IN_OUT);
	        String displayDefault = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DISPLAY_DEFAULT);
	        String displayDimName = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DISPLAY_DIM_NAME);
	        String displayHierName = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DISPLAY_HIER_NAME);
	        String displayRole = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DISPLAY_ROLE);
	        // remove the double quotes
	        if (res[ 0 ].indexOf("\"") > -1||res[ 0 ].indexOf("\'") > -1) { //$NON-NLS-1$
	            res[ 0 ] = res[ 0 ].replaceAll("\"|\'", ""); //$NON-NLS-1$ //$NON-NLS-2$
	        } else {
	            res[ 0 ] = upperCaseLocaleUS(ctx,res[ 0 ].replaceAll("\"|\'", "")); //pass on uppercase//$NON-NLS-1$
	        }
	        if (res[ 1 ].indexOf("\"") > -1||res[ 1 ].indexOf("\'") > -1) { //$NON-NLS-1$
	            res[ 1 ] = res[ 1 ].replaceAll("\"|\'", ""); //$NON-NLS-1$ //$NON-NLS-2$
	        } else {
	            res[ 1 ] = upperCaseLocaleUS(ctx,res[ 1 ].replaceAll("\"|\'", "")); //pass on uppercase//$NON-NLS-1$
	        }
	        if (objType == null)
	        	objType = getObjType(res[ 1 ], res[ 0 ]);
	        if (objType == null || objType.length() == 0 && res[1] != null && res[1].length() > 0)
	        {
	            objType = getObjTypeDBA(res[1], res[0]);
	        }
	
	        // for desc [owner].object[@dbLink] case
	        String dbLinkName = ""; //$NON-NLS-1$
	        String dbLink[] = DBUtil.resolveDBLink(res[1]);
	        if (dbLink != null) {
	            dbLinkName = "@" + dbLink[ 1 ]; //$NON-NLS-1$
	            if (res[0].equals("")) {
	            	res[0] = getDBLinkOwner(res[1], "");
	            }
	            res[ 1 ] = dbLink[ 0 ];
	        }
	        Query q = null;
	        String sql = null;
	        String owner = null;
	        boolean knownObject = true;
	        Object[] formatArgs = null;
	        String sqlQuery = null;
	
	        if (objType.equals("TABLE") || objType.equals("VIEW") || objType.endsWith("MATERIALIZED VIEW")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	        	    if ( objType.equals("TABLE")  && isTableXMLType(res[ 1 ], res[ 0 ])) {
	        		     sqlQuery = "DESC_XMLTYPE_TABLE"; //$NON-NLS-1$
	        	    } else 
	             	sqlQuery = "DESC_TABLE_VIEW"; //$NON-NLS-1$
	        	if ((!(dbLinkName.equals(""))) && (ownerNotSpecified)) {  //$NON-NLS-1$
	        		sqlQuery += "_NO_OWNER";  //$NON-NLS-1$
	        	}
	        	String hidden="'YES'";
	        	if (Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SETCOLINVISIBLE).toString())) {
	        		hidden="'SHOW_ALL'";
	        	}
	        	formatArgs = new Object[] { dbLinkName, displayName, displayNull, displayType, viewType, hidden, owner };
	        	if ( objType.equals("VIEW") ) {
	        		String objectName = res[1] ;
	        		if (!ownerNotSpecified ) {
	        			objectName = res[0]+"."+res[1];
	        		}
	        		compileView(objectName);
	        	}
	        	addColumnFormatForTableView(conn, ctx);
	        } else if (objType.startsWith("PACKAGE")) { //$NON-NLS-1$
	            if (isDefaultDetailsAvailable(dbLinkName)) {
	                sqlQuery = "DESC_PACKAGE_WITH_ARGS_DEFAULTS"; //$NON-NLS-1$
	            } else {
	                sqlQuery = "DESC_PACKAGE_WITHOUT_ARGS_DEFAULTS"; //$NON-NLS-1$
	            }
	            if ((!(dbLinkName.equals(""))) && (ownerNotSpecified)) {  //$NON-NLS-1$
	                sqlQuery += "_NO_OWNER";  //$NON-NLS-1$
	            }
	            formatArgs = new Object[] { dbLinkName, displayArgumentName, displayType, displayInOut, displayDefault, owner };
	            try {    	
	            	q = m_xml.getQuery(sqlQuery, conn);
		            sql = q.getSql().replaceAll("'", "''"); //$NON-NLS-1$ //$NON-NLS-2$
		            if (!(res[ 0 ].equals(""))) { //$NON-NLS-1$
		                  owner = ":1"; //$NON-NLS-1$
		            } else {//now using _NO_OWNER version over database links. [could have used username from dblink metadata instead]
		                owner = "sys_context('USERENV', 'CURRENT_USER')"; //$NON-NLS-1$
		            }
		           formatArgs[ formatArgs.length - 1 ] = owner;
		            sql = MessageFormat.format(sql, formatArgs);
		            cmd.setSql(sql);
		            if ((!(res[ 0 ].equals("")))&&(!(sqlQuery.endsWith("_NO_OWNER")))) { //$NON-NLS-1$  //$NON-NLS-2$
		                cmd.addBind(res[ 0 ]);
		                cmd.addBind(res[ 1 ]);
		            } else {
		                cmd.addBind(res[ 1 ]);
		            }
	               describePackage(conn, ctx, formatArgs, res, cmd.getModifiedSQL());
	            } catch (Exception ex) {
	            	ExceptionHandler.handleException(ex);
	            	//ctx.write(ex.toString() +System.getProperty("line.seperator"));
	            }
	            return;
	        } else if (objType.equals("FUNCTION") || objType.equals("PROCEDURE")) { //$NON-NLS-1$ //$NON-NLS-2$
	        	if ( !isObjectStatusValid(conn, res, objType) ) {
	        		ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESCRIBE_INVALID_OBJECT)+LINE_SEPARATOR); //$NON-NLS-1$
	        		return;
	        	}
	            if (isDefaultDetailsAvailable(dbLinkName)) {
	                sqlQuery = "DESC_PROC_FUNC_WITH_ARGS_DEFAULTS"; //$NON-NLS-1$
	            } else {
	                sqlQuery = "DESC_PROC_FUNC_WITHOUT_ARGS_DEFAULTS"; //$NON-NLS-1$
	            }
	            if ((!(dbLinkName.equals(""))) && (ownerNotSpecified)) {  //$NON-NLS-1$
	                sqlQuery += "_NO_OWNER";  //$NON-NLS-1$
	            }
	            if (objType.equals("PROCEDURE") )
	                writeOutputHeader (rawName, objType, ctx, null);
	            if (objType.equals("FUNCTION") )
	                writeOutputHeader (rawName, objType, ctx, getFunctionReturnType(dbLinkName, orgName));
	            formatArgs = new Object[] { dbLinkName, displayArgumentName, displayType, displayDefault, owner };
	            addColumnFormatForStoreProcedures(conn, ctx) ;
	        } else if (objType.startsWith("TYPE")) { //$NON-NLS-1$
	        	Object[] typeFormatArgs = null;
	        	if (!(res[ 0 ].equals(""))) { //$NON-NLS-1$
	                formatArgs = new Object[] { dbLinkName, displayName, displayNull, displayType, " UPPER(owner)= UPPER(:1) and " }; //$NON-NLS-1$
	                typeFormatArgs = new Object[] { dbLinkName, displayArgumentName, displayType, displayInOut, displayDefault, " UPPER(owner)= UPPER(:1) and " }; //$NON-NLS-1$
	            } else {
	                formatArgs = new Object[] { dbLinkName, displayName, displayNull, displayType, "" }; //$NON-NLS-1$
	                typeFormatArgs = new Object[] { dbLinkName, displayArgumentName, displayType, displayInOut, displayDefault, "sys_context('USERENV', 'CURRENT_USER')" }; //$NON-NLS-1$
	            }

	        	if (!isUserDefinedTypeCollection(dbLinkName, orgName) ) {
	        		sqlQuery = "DESC_TYPE"; //$NON-NLS-1$	
	        		try {
	        			m_isTypeMethods = isTypeMethodsExists(conn, ctx, res, typeFormatArgs);
	        		} catch (Exception ex) {
	        			ExceptionHandler.handleException(ex);
	        			//ctx.write(ex.toString() +System.getProperty("line.seperator"));
	        		}
	        	} else {
	        		sqlQuery = "DESC_TYPE_COLLECTION"; //$NON-NLS-1$
	        		String CurrUser = getCurrentUser();
	        		String[] refType = getReferencedType(dbLinkName, orgName);
	        		if (refType!=null) {
		        		if (  refType[0] == null || refType[0].equalsIgnoreCase(CurrUser)) {
		        			writeTypeOutputHeader(rawName, objType, ctx, dbLinkName, null);
		        		} else {
		        			res[0] = refType[0];
		        			writeTypeOutputHeader(rawName, objType, ctx, dbLinkName, refType[0] );
		        		}
	        		}
	        		while (!refType[1].equals("")) {
	        			String refType2[] = getReferencedType(dbLinkName, refType[1]);
	        			if ( !refType2[1].equals("")) {
	        				if (refType2[0]!= null)
	        					res[0] = refType2[0];
	        			} 
	        			writeTypeOutputHeader(refType[1], objType, ctx, dbLinkName, refType[0]);
	        			sqlQuery = "DESC_TYPE"; //$NON-NLS-1$
	        			res[1] = refType[1];
	        			refType[1] = refType2[1];
	        		} 

	        		if (sqlQuery.equals("DESC_TYPE")) 
	        			ctx.write(LINE_SEPARATOR); //$NON-NLS-1$ 
	        	}
	        	
	            if ((!(dbLinkName.equals(""))) && (ownerNotSpecified)) {  //$NON-NLS-1$
	                sqlQuery += "_NO_OWNER";  //$NON-NLS-1$
	            }
	            
	            addColumnFormatForTableView(conn, ctx);
	        } else if (objType.equals("HIERARCHY")){
	            sqlQuery = "DESC_HIERARCHY";
	            formatArgs = new Object[] { dbLinkName, displayName, displayRole, displayType, viewType, owner };
	        } else if (objType.equals("ANALYTIC VIEW")){
	            sqlQuery = "DESC_ANALYTIC_VIEW";
	            formatArgs = new Object[] { dbLinkName, displayName, displayDimName, displayHierName, displayRole, displayType, viewType, owner };      
	        }
	        // default try to describe it as a table
	        else {
	            // doneill: removed line number in error msg as this does not reflect how
	            // sql*plus does it
	            // see bug
	        	if (objType.equalsIgnoreCase("INDEXTYPE")) {
	        		ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESCRIBE_INVALID_OBJECTTYPE)+LINE_SEPARATOR); //$NON-NLS-1$
	        		return;
	        	} else if (objType.equals("")) {
	        		ctx.write(LINE_SEPARATOR+ScriptRunnerDbArb.format(ScriptRunnerDbArb.DESCR_NO_OBJECT,rawName)+LINE_SEPARATOR);
	        		return;
	        	} else {
	        		cmd.setSql("select '" + ScriptRunnerDbArb.format(ScriptRunnerDbArb.CANNOT_DESCRIBE_ERROR, res[ 1 ]) + "' \"ERROR:\"  from dual"); //$NON-NLS-1$ //$NON-NLS-2$
	        	}
	        	knownObject = false;
	        }
	        if (knownObject) {
	            q = m_xml.getQuery(sqlQuery, conn);
	            sql = q.getSql().replaceAll("'", "''"); //$NON-NLS-1$ //$NON-NLS-2$
	            if (!(res[ 0 ].equals(""))) { //$NON-NLS-1$
	                  owner = ":1"; //$NON-NLS-1$
	            } else {//now using _NO_OWNER version over database links. [could have used username from dblink metadata instead]
	                owner = "sys_context('USERENV', 'CURRENT_USER')"; //$NON-NLS-1$
	            }
	           formatArgs[ formatArgs.length - 1 ] = owner;
	            sql = MessageFormat.format(sql, formatArgs);
	            cmd.setSql(sql);
	            if ((!(res[ 0 ].equals("")))&&(!(sqlQuery.endsWith("_NO_OWNER")))) { //$NON-NLS-1$  //$NON-NLS-2$
	                cmd.addBind(res[ 0 ]);
	                cmd.addBind(res[ 1 ]);
	            } else {
	                cmd.addBind(res[ 1 ]);
	            }
	        }
	        cmd.setStmtClass(SQLCommand.StmtType.G_C_SQL);
	        // cmd.setStmtId(SQLCommand.StmtSubType.G_S_SELECT);
	        cmd.setResultsType(SQLCommand.StmtResultType.G_R_QUERY);
	    }
    
    // Bug 25059902 - DESC SHOULD GET SP2-0312 WITH MISSING TERMINATING QUOTE 
    private boolean validateRawObjectName(ScriptRunnerContext ctx, String rawName) {
    	
    	 if ( !isValidDBLinkConnection (ctx, rawName)) {
 	    	return false;
 	    }
    	if (rawName.contains(".")) {
    		StringTokenizer st = new StringTokenizer(rawName, "\\.");
    		String schemaName = st.nextToken();
    		String objectName = st.nextToken();

    		if ( ((schemaName.startsWith("\"")) && (!schemaName.endsWith("\""))) || ((objectName.startsWith("\"")) && (!objectName.endsWith("\""))) ) {
    			ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESC_SP2_0312) + LINE_SEPARATOR);
    			return false;
    		}
    		if ( ((schemaName.endsWith("\"")) && (!schemaName.startsWith("\""))) || ((objectName.endsWith("\"")) && (!objectName.startsWith("\"")))) {
    			ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESC_SP2_0565)+ LINE_SEPARATOR);
    			return false;
    		}
    		if ( (schemaName.contains("-")) || (objectName.contains("-")) ) {
    			ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESC_SP2_0565)+ LINE_SEPARATOR);
    			return false;
    		}
    	} else {
    		if ((rawName.startsWith("\"")) && (!rawName.endsWith("\""))) {
    			ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESC_SP2_0312) + LINE_SEPARATOR);
    			return false;
    		}
    		if ((rawName.endsWith("\"")) && (!rawName.startsWith("\""))) {
    			ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESC_SP2_0565)+ LINE_SEPARATOR);
    			return false;
    		}
    		if (rawName.contains("-")) {
    			ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESC_SP2_0565)+ LINE_SEPARATOR);
    			return false;
    		}
    	}
    	return true;
    }
    
    private void clearColumnFormats(ScriptRunnerContext ctx) {
    	
    	clearColumnFormatForStoreProcedures(ctx);
    	clearColumnFormatForTableViews(ctx);
    }
    
    private void addColumnFormatForStoreProcedures(Connection conn, ScriptRunnerContext ctx) {	   	
    	HashMap<String, ArrayList<String>> storedColumns = ctx.getStoredFormatCmds();
    	/* Calculations for SQLPlus format of column width
    	 * LineSize | Arg Name  | Type | In/Out | Default 	| 
    	 * 20       | 30     	| 23   | 6   	|	8		| 
    	 * 40       | 30     	| 23   | 6   	|	8		|
    	 * 60       | 30     	| 23   | 6   	|	8		| 
    	 * 80       | 30     	| 23   | 6   	|	8		| 
    	 * 100      | 30     	| 23   | 6   	|	8		| 
    	 * 150      | 30     	| 23   | 6   	|	8		| 
    	 *
    	 */
    	ArrayList<String> cmdlist = new ArrayList<String>();
    	cmdlist.add("format a30");
    	storedColumns.put("argument name", cmdlist);
    	cmdlist = new ArrayList<String>();
    	cmdlist.add("format a23");
    	storedColumns.put("type", cmdlist);
    	cmdlist = new ArrayList<String>();
    	cmdlist.add("format a6");
    	storedColumns.put("in/out", cmdlist);
    	cmdlist = new ArrayList<String>();
    	cmdlist.add("format a8");
    	storedColumns.put("default?", cmdlist); 
    }

    private void addColumnFormatForTableView(Connection conn, ScriptRunnerContext ctx) {	   	
    	HashMap<String, ArrayList<String>> storedColumns = ctx.getStoredFormatCmds();
    	
    	
    	/* Calculations for SQLPlus format of column width
    	 * LineSize |  Name  | Null | Type | Ratio Name/Type
    	 * 20       | 17     | 8    | 12   | 1.4
    	 * 40       | 17     | 8    | 12   | 1.4
    	 * 60       | 29     | 8    | 20   | 1.45
    	 * 80       | 41     | 8    | 28   | 1.46
    	 * 100      | 53     | 8    | 36   | 1.47
    	 * 150      | 83     | 8    | 56   | 1.48
    	 *
    	 */
    	
    	int linesize = ((Integer) ctx.getProperty(ScriptRunnerContext.SETLINESIZE)).intValue();
    	// default values for linesize = 80
    	int nameWidth = 41;
    	int nullWidth = 8;
    	int typeWidth = 28;
    	double ratio = 1;
    	if (linesize <= 40 ) {
    		nameWidth = 17;
        	typeWidth = 12;
    	} else if (linesize > 80 ) {
    		ratio = 1.46 + (linesize - 80.0)/2000.0;
    		typeWidth = (int) Math.round((linesize -10.0)/(1.0 + ratio)) ;
    		nameWidth = linesize - typeWidth -10 ; // 8 (null) + 2 (blankspaces) 
    	}
    	
    	ArrayList<String> cmdlist = new ArrayList<String>();
    	cmdlist.add("format a"+nameWidth);
    	storedColumns.put("name", cmdlist);
    	cmdlist = new ArrayList<String>();
    	cmdlist.add("format a8");
    	storedColumns.put("null?", cmdlist);
    	cmdlist = new ArrayList<String>();
    	cmdlist.add("format a"+typeWidth);
    	storedColumns.put("type", cmdlist);
    }
    
    private void clearColumnFormatForStoreProcedures(ScriptRunnerContext ctx) {
    	// remove col commands when done.
    	HashMap<String, ArrayList<String>> storedColumns = ctx.getStoredFormatCmds();
    	storedColumns.remove("argument name");
    	storedColumns.remove("type");
    	storedColumns.remove("in/out");
    	storedColumns.remove("default?"); 
    }
    
    private void clearColumnFormatForTableViews(ScriptRunnerContext ctx) {
    	// remove col commands when done.
    	HashMap<String, ArrayList<String>> storedColumns = ctx.getStoredFormatCmds();
    	storedColumns.remove("name");
    	storedColumns.remove("null?");
    	storedColumns.remove("type"); 
    }

    private void describePackage(Connection conn, ScriptRunnerContext ctx, Object[] formatArgs, String[] objectName, String sqlQuery) throws SQLException {
    	
    	String outScript = "SELECT subprogram_id, subprogram_type, procedure_name,  data_type FROM \n" + 
    			"(\n" + 
    			"  SELECT p.object_name, p.procedure_name,\n" + 
    			"         CASE WHEN a.object_id IS NULL THEN 'PROCEDURE' ELSE 'FUNCTION' END AS subprogram_type, p.subprogram_id, a.data_type\n" + 
    			"  FROM all_procedures p\n" + 
    			"  LEFT JOIN all_arguments a ON ( a.object_id = p.object_id\n" + 
    			"                             AND a.subprogram_id = p.subprogram_id AND a.position = 0 )\n" + 
    			"  WHERE p.owner = UPPER(:0)\n" + 
    			")\n" + 
    			"where object_name = :1\n" + 
    			"and procedure_name is not NULL\n" + 
    			"order by procedure_name, subprogram_id";

    	if (!(objectName[ 0 ].equals(""))) { //$NON-NLS-1$
    		outScript = outScript.replace(":0", "'"+objectName[0]+"'"); //$NON-NLS-1$
        } else {//now using _NO_OWNER version over database links. [could have used username from dblink metadata instead]
          outScript = outScript.replace(":0", "sys_context('USERENV', 'CURRENT_USER')"); //$NON-NLS-1$
        }
    	outScript = outScript.replace(":1", "'"+objectName[1]+"'"); //$NON-NLS-1$
    	
    	DBUtil dbUtil = DBUtil.getInstance(conn);
    	ResultSet rsOuter = null;	
    	rsOuter = dbUtil.executeOracleQuery(outScript, null);

    	addColumnFormatForStoreProcedures(conn, ctx) ;

    	ResultSetMetaData rmeta = rsOuter.getMetaData();
    	int count = rmeta.getColumnCount();
    	int rowCount = 0;
    	while (rsOuter.next()) {
    		Object[] obj = new Object[count + 1];
    		String headerString = "";
    		String colValue = "";
    		String subproId = "";
    		for (int i = 1; i < count + 1; i++) {
    			String columnName = "";
    			if (rsOuter instanceof OracleResultSet) {
    				try {
    					columnName = rmeta.getColumnName(i);
    					Object rObj = ((OracleResultSet) rsOuter).getOracleObject(i);
    					colValue = DataTypesUtil.stringValue(rObj, conn);
    				}
    				catch (Exception ex) {
    					ExceptionHandler.handleException(ex);
    					// ctx.write(ex.toString() +System.getProperty("line.seperator"));
    				}
    			}
    			if (columnName.equalsIgnoreCase("subprogram_type"))
    				headerString += colValue ;
    			else if (columnName.equalsIgnoreCase("procedure_name"))
    				headerString += " "+colValue ;
    			else if (columnName.equalsIgnoreCase("data_type") && colValue != null)
    				headerString += " RETURNS "+colValue ;
    			else if (columnName.equalsIgnoreCase("subprogram_id"))
    				subproId = colValue ;
    		}
    		ctx.write(headerString);
    		packageArgumentsDisplay(conn, ctx, subproId, formatArgs, objectName, sqlQuery) ;
    		ctx.write(LINE_SEPARATOR);
    	}	
    	ctx.write(LINE_SEPARATOR);
    	clearColumnFormatForStoreProcedures(ctx) ;
    	rsOuter.close();
    }

    private void packageArgumentsDisplay(Connection conn, ScriptRunnerContext ctx, String subprogId, Object[] formatArgs, String[] objectName, String sqlQuery) throws SQLException {   	
    	
    	if (!(objectName[ 0 ].equals(""))) { //$NON-NLS-1$
    		sqlQuery = sqlQuery.replace(":1", "'"+objectName[0]+"'"); //$NON-NLS-1$
        } else {//now using _NO_OWNER version over database links. [could have used username from dblink metadata instead]
        	sqlQuery = sqlQuery.replace(":1", "sys_context('USERENV', 'CURRENT_USER')"); //$NON-NLS-1$
        }
    	
    	sqlQuery = sqlQuery.replaceAll(":2", "'"+objectName[1]+"'"); //$NON-NLS-1$
    	sqlQuery = sqlQuery.replace(":3", "'"+subprogId+"'");
    	
    	try {
    		DBUtil dbUtil = DBUtil.getInstance(conn);
    		ResultSet rsOuter = null;	
    		rsOuter = dbUtil.executeOracleQuery(sqlQuery, null);

    		SQLPLUSCmdFormatter rFormat = new SQLPLUSCmdFormatter(ctx); 
    		rFormat.formatResults(ctx.getOutputStream(), rsOuter, sqlQuery);
    		rsOuter.close();
    	}  catch (IOException ioe) {
    		ExceptionHandler.handleException(ioe);
    		// ctx.write(ioe.toString() +System.getProperty("line.seperator"));
    	}
    }	

    private String getDBLinkOwner(String objName, String ownerName) { 
        String dbLink[] = DBUtil.resolveDBLink(objName);
    	if (ownerName.equals("")) {
    		//For fixed links, ie ones with auth in the link, u dont need to specify the schema
    		//In these cases, we need a bit more info to check the obj, ie, we need to figure out
    		//the owner name of the user in the link
    		String dresult = null;
    		String result = null;
    		String mod_dblink = dbLink[1];
    		
    		String domain = "select value from v$parameter where name IN ('db_domain')";
    		dresult = executeQuery(domain);
    		
    		if (dresult != null && !dresult.equals("")) {
    			mod_dblink = dbLink[1]+"."+dresult;
    		}	
    		String sql="select username from all_db_links where db_link=UPPER('"+mod_dblink+"') AND username IS NOT null";
    		
    		String aa = DBUtil.getInstance(_conn).executeOracleReturnOneCol("select username from all_db_links where db_link=UPPER('mydblink.yourdomain') AND username IS NOT null", new HashMap<String, String>());
    		result = executeQuery(sql);
    		if (result != null && !result.equals("")) {
    			return result;
    		}	
    	} 
    	return ownerName;
    }
    
    private String getDBLinkOwner(String dbLink) {
    	String sql="select username from all_db_links where db_link=UPPER('"+ dbLink+"') AND username IS NOT null";
    	String result = executeQuery(sql);
    	if (result != null && !result.equals("")) {
    		return result;
    	}	
    	return null;
    }
    
    private String getObjType(String objName, String ownerName) {
        String sql = null;
        String result = null;
        String modifiedOwner = null;
        String modifiedObjectName = null;
        boolean noOwnerDatabaseLink = false;
        String dbLink[] = DBUtil.resolveDBLink(objName);
        if (dbLink != null) {
                if (ownerName.equals("")) {
                    noOwnerDatabaseLink = true;
                        ownerName=getDBLinkOwner(objName, ownerName);
                        }
            objName = dbLink[ 0 ];
            if (noOwnerDatabaseLink) {
                sql = "select distinct object_type " + " from sys.user_objects@" + dbLink[ 1 ] ; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            } else  {
                sql = "select distinct object_type " + " from sys.all_objects@" + dbLink[ 1 ] ; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            }
        } else {
            sql = "select distinct object_type " + " from sys.all_objects " ; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        }
        
        if (ownerName != null && !ownerName.equals("")) {
        	String dblnk = null;
        	if (dbLink !=null && dbLink[1].length() > 0)
        		dblnk = dbLink[1];
           if ( !ownerExists(dblnk, ownerName) ) {
        	   String name = "\""+ownerName.toUpperCase().replaceAll("\"|\'", "")+"\"."+"\""+objName.toUpperCase().replaceAll("\"|\'", "")+"\"";
        	   getScriptRunnerContext().write(LINE_SEPARATOR+ScriptRunnerDbArb.format(ScriptRunnerDbArb.DESCR_NO_OBJECT,name)+LINE_SEPARATOR); 
        	   return null;
           }
        }
        
        String strippedObjName = objName.replaceAll("\"|\'", ""); //$NON-NLS-1$ //$NON-NLS-2$
        if (objName.startsWith("\"")) { //$NON-NLS-1$
        	modifiedObjectName = "'" + strippedObjName + "'";
            sql = sql + " where object_name =" + "'" + strippedObjName + "'"; //$NON-NLS-1$
        } else {
        	modifiedObjectName = "UPPER('" + strippedObjName + "')";
            /* note will match upper('lowercase')=upper('LOWERCASE') which is not intended */
            sql = sql +  " where UPPER(object_name) =" + "UPPER('" + strippedObjName + "')"; //$NON-NLS-1$
        }
        String query = ""; //$NON-NLS-1$
        if (noOwnerDatabaseLink) {
            query = sql;
        } else if (ownerName.equals("")) { //$NON-NLS-1$
        	modifiedOwner = "UPPER(sys_context('USERENV', 'CURRENT_USER'))";
            query = sql + " and UPPER(owner)= UPPER(sys_context('USERENV', 'CURRENT_USER'))"; //$NON-NLS-1$
        } else {
            String strippedOwnerName = ownerName.replaceAll("\"|\'", ""); //$NON-NLS-1$ //$NON-NLS-2$
            modifiedOwner = "UPPER('" + strippedOwnerName + "')";
            query = sql + " and UPPER(owner)= UPPER('" + strippedOwnerName + "')"; //$NON-NLS-1$ //$NON-NLS-2$
        }
        query = query + " and object_type != 'INDEX' "; // 19216553: indexes are not describable
        query = query + " and object_type != 'TABLE PARTITION' "; // 16696203 & 27496723
        query = query + " and object_type != 'TABLE SUBPARTITION' ";  
        result = executeQuery(query);
        if (result == null) {
            result = ""; //$NON-NLS-1$
        }
        // check if its a synonym
        if (ownerName.equals("") && result.trim().equals("")) { //$NON-NLS-1$ //$NON-NLS-2$
            query = sql + "and OBJECT_TYPE ='SYNONYM'"; //$NON-NLS-1$
            result = executeQuery(query);
            if (result == null) {
                result = ""; //$NON-NLS-1$
            } else if (!result.trim().equals("")) { //$NON-NLS-1$
                result = "SYNONYM"; //$NON-NLS-1$
            }
        } else if (result.trim().equals("") && !ownerName.equals("")) {
        	/*String symSql = "select distinct object_type  from sys.all_objects where UPPER(owner)="+modifiedOwner+" and object_type != 'INDEX'  and rownum < 100 "+
        			        "and object_name in ( select table_name from all_synonyms" + 
        			        " where UPPER(synonym_name) = "+modifiedObjectName+ 
        			        " and UPPER(owner)= "+modifiedOwner+")";
        	result = executeQuery(symSql);
            if (result == null) {
                result = ""; //$NON-NLS-1$
            }*/
        	String dblink = null ;
        	if (dbLink != null) {
        		dblink = dbLink[ 1 ] ;
        	}
        	if ( !checkObjectExists(strippedObjName, modifiedOwner, dblink) && !checkObjectExists(strippedObjName, "\'PUBLIC\'", dblink)) { //$NON-NLS-1$
        		result = UNDEFINED_OBJECT_TYPE; //$NON-NLS-1$
        	} else 
        		result = "SYNONYM"; //$NON-NLS-1$
        }
        if (result.indexOf("TABLE PARTITION") != -1 //$NON-NLS-1$  Taking table partitions
                        ||result.indexOf("TABLE\nINDEX")!=-1) { //$NON-NLS-1$ Taking synonyms which have same name indexes too.
            result = "TABLE"; //$NON-NLS-1$
        }
        return result;
    }
    
    // select table_name from user_xml_tables where table_name = 'XWAREHOUSES';
    private boolean isTableXMLType(String objName, String ownerName) {
        String sql = null;
        String result = null;
        String modifiedOwner = null;
        String modifiedObjectName = null;
        boolean noOwnerDatabaseLink = false;
        String dbLink[] = DBUtil.resolveDBLink(objName);
        if (dbLink != null) {
                if (ownerName.equals("")) {
                    noOwnerDatabaseLink = true;
                        ownerName=getDBLinkOwner(objName, ownerName);
                        }
            objName = dbLink[ 0 ];
            if (noOwnerDatabaseLink) {
                sql = "select distinct table_name " + " from sys.user_xml_tables@" + dbLink[ 1 ] ; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            } else  {
                sql = "select distinct table_name " + " from sys.all_xml_tables@" + dbLink[ 1 ] ; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            }
            setDblinkName(dbLink[ 1 ]);
        } else {
            sql = "select distinct table_name " + " from sys.all_xml_tables " ; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            setDblinkName(null);
        }
        
        if (ownerName != null && !ownerName.equals("")) {
        	String dblnk = null;
        	if (dbLink !=null && dbLink[1].length() > 0)
        		dblnk = dbLink[1];
           if ( !ownerExists(dblnk, ownerName) ) {
        	   String name = "\""+ownerName.toUpperCase().replaceAll("\"|\'", "")+"\"."+"\""+objName.toUpperCase().replaceAll("\"|\'", "")+"\"";
        	   getScriptRunnerContext().write(LINE_SEPARATOR+ScriptRunnerDbArb.format(ScriptRunnerDbArb.DESCR_NO_OBJECT,name)+LINE_SEPARATOR); 
        	   return false;
           }
        }
        
        String strippedObjName = objName.replaceAll("\"|\'", ""); //$NON-NLS-1$ //$NON-NLS-2$
        if (objName.startsWith("\"")) { //$NON-NLS-1$
        	modifiedObjectName = "'" + strippedObjName + "'";
            sql = sql + " where table_name =" + "'" + strippedObjName + "'"; //$NON-NLS-1$
        } else {
        	modifiedObjectName = "UPPER('" + strippedObjName + "')";
            /* note will match upper('lowercase')=upper('LOWERCASE') which is not intended */
            sql = sql +  " where UPPER(table_name) =" + "UPPER('" + strippedObjName + "')"; //$NON-NLS-1$
        }
        
        setObjectName(modifiedObjectName);
        String query = ""; //$NON-NLS-1$
        if (noOwnerDatabaseLink) {
            query = sql;
        } else if (ownerName.equals("")) { //$NON-NLS-1$
        	setOwnerName(ownerName);
        	modifiedOwner = "UPPER(sys_context('USERENV', 'CURRENT_USER'))";
            query = sql + " and UPPER(owner)= UPPER(sys_context('USERENV', 'CURRENT_USER'))"; //$NON-NLS-1$
        } else {
            String strippedOwnerName = ownerName.replaceAll("\"|\'", ""); //$NON-NLS-1$ //$NON-NLS-2$
            setOwnerName(strippedOwnerName);
            modifiedOwner = "UPPER('" + strippedOwnerName + "')";
            query = sql + " and UPPER(owner)= UPPER('" + strippedOwnerName + "')"; //$NON-NLS-1$ //$NON-NLS-2$
        }
       
        query = query + " and rownum < 100"; // 16696203: there is never more than 100 object types
        result = executeQuery(query);
        if (result != null && result.length() > 0) {
        	   return true;
        }
        return false;
    }

    /**
     * @param objName name of object (not null or empty)
     * @param ownerName name of owner (not null or empty)
     * @return
     */
    private String getObjTypeDBA(String objName, String ownerName) {
        String sql = null;
        String dbLink[] = DBUtil.resolveDBLink(objName);
        boolean noOwnerDatabaseLink = false;
        if (dbLink != null) {
        	if (ownerName.equals("")) {
                noOwnerDatabaseLink = true;
                    ownerName=getDBLinkOwner(objName, ownerName);
            }
        	objName = dbLink[ 0 ];
        	
        	if (noOwnerDatabaseLink) {
                sql = "select distinct object_type " + " from sys.user_objects@" + dbLink[ 1 ] ; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            } else  {
                sql = "select distinct object_type " + " from sys.dba_objects@" + dbLink[ 1 ] ; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            }
        } else {
            sql = "select distinct object_type " + " from sys.dba_objects " ; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        }
        	
        String strippedObjName = objName.replaceAll("\"|\'", ""); //$NON-NLS-1$ //$NON-NLS-2$
        String strippedOwnerName = ownerName.replaceAll("\"|\'", ""); //$NON-NLS-1$ //$NON-NLS-2$
        if (objName.startsWith("\"")) { //$NON-NLS-1$
            sql = sql + " where object_name = '" + strippedObjName + "'"; //$NON-NLS-1$
        } else {
            sql = sql +  " where UPPER(object_name) = UPPER('" + strippedObjName + "')"; //$NON-NLS-1$
        }
        if (ownerName.startsWith("\"")) { //$NON-NLS-1$
            sql = sql + " and owner = '" + strippedOwnerName + "'"; //$NON-NLS-1$
        } else {
            sql = sql +  " and UPPER(owner) = UPPER('" + strippedOwnerName + "')"; //$NON-NLS-1$
        }
        String query = sql; //$NON-NLS-1$
        query = query + " and object_type != 'INDEX' "; // 19216553: indexes are not describable
        query = query + " and rownum < 100"; // 16696203: there is never more than 100 object types
        String result = executeQuery(query);
        if (result == null) {
            result = ""; //$NON-NLS-1$
        }
        if (result.indexOf("TABLE PARTITION") != -1 //$NON-NLS-1$  Taking table partitions
                        ||result.indexOf("TABLE\nINDEX")!=-1) { //$NON-NLS-1$ Taking synonyms which have same name indexes too.
            result = "TABLE"; //$NON-NLS-1$
        }
        return result;
    }
    
    /**
     * because of bug 183707 in the ORacle database. The default is not know in all_arguments. This information can only be retrieved from
     * sys.argument$ This table is not accessable to everyone This procedure checks if it is available. 
     * @param dblink Needs dblink = "" for none or "@dblink"
     * 
     * @return
     */

    private boolean isDefaultDetailsAvailable(String dblink) {
        String sql = "select 'available' from sys.argument$"+dblink+" where rownum <=1"; //$NON-NLS-1$  //$NON-NLS-2$
        String result = executeQuery(sql);
        if (result.equals("available")) { //$NON-NLS-1$
            return true;
        }
        return false;
    }
    
    private boolean isUserDefinedTypeCollection(String dblink, String objName) {
    	
    	String sql ;
    	if ( !(dblink.equals("")) ) 
    	     sql = "select typecode from sys.all_types@"+dblink+" where type_name="; //$NON-NLS-1$  //$NON-NLS-2$
    	else 
   	         sql = "select typecode from sys.all_types where type_name="; //$NON-NLS-1$  //$NON-NLS-2$
    	
    	if (objName.startsWith("\"")) { //$NON-NLS-1$
            sql = sql + "'" + objName.replaceAll("\"|\'", "") + "'"; //$NON-NLS-1$
        } else {
            sql = sql +  "UPPER('" + objName + "')"; //$NON-NLS-1$
        }
    	
    	sql = sql + " and UPPER(owner)= UPPER(sys_context('USERENV', 'CURRENT_USER'))";
    	 
        String result = executeQuery(sql);
        if (result.equals("COLLECTION")) { //$NON-NLS-1$
            return true;
        }
        return false;
    }
    
    private String executeQuery(String sql) {
        String result = ""; //$NON-NLS-1$
        Statement stmt = null;
        ResultSet rs = null;
        if (_conn == null) {
            return null;
        }
        try {
            // Retrieve the data
            stmt = _conn.createStatement();
            rs = stmt.executeQuery(sql);
            // dont loop through, just take the first one
            // the first loop
            rs.next();
            ResultSetMetaData rsmd = rs.getMetaData();
            int numCols = rsmd.getColumnCount();
            if (numCols == 1) {
                result = result + rs.getString(1);
            } else {
                for (int i = 1; i <= numCols; i++) {
                    result = result + rs.getString(i) + ":"; //$NON-NLS-1$
                }
            }
            // loop through rows
            while (rs.next()) {
                result = result + "\n"; //$NON-NLS-1$
                if (numCols == 1) {
                    result = result + rs.getString(1);
                } else {
                    for (int i = 1; i <= numCols; i++) {
                        result = result + rs.getString(i) + ":"; //$NON-NLS-1$
                    }
                }
            }

        } catch (SQLException ex) {
            // ex.printStackTrace();
        } finally {
          Closeables.close(stmt,rs);
        }
        return result;
    }
    
    private String[] getBaseSynonymObj( String synonymName, String synonymOwner) {
        String sqlPrivate = null;
        String sqlPublic = null;
        String result = null;
        String sql = "select table_owner ,table_name, db_link from all_synonyms where  synonym_name = "; //$NON-NLS-1$
        String strippedSynonymName = synonymName.replaceAll("\"", ""); //$NON-NLS-1$ //$NON-NLS-2$
        if (synonymName.startsWith("\"")) { //$NON-NLS-1$
            sql = sql + "'" +strippedSynonymName + "'"; //$NON-NLS-1$
        } else {
            sql = sql + "UPPER('"+strippedSynonymName + "')"; //$NON-NLS-1$
        }
        String synonymOwnerTrim = synonymOwner == null ? "" : synonymOwner.trim();
        if (synonymOwnerTrim.length() > 2 && synonymOwnerTrim.startsWith("\"") && synonymOwnerTrim.endsWith("\""))
        {
            synonymOwnerTrim = synonymOwnerTrim.substring(1, synonymOwnerTrim.length() - 1);
        }
        if (!synonymOwnerTrim.equals("") && !synonymOwnerTrim.equalsIgnoreCase("SYS")) { //$NON-NLS-1$
            sqlPrivate = sql + " and owner =UPPER('" + synonymOwnerTrim + "')"; //$NON-NLS-1$ //$NON-NLS-2$
        } else {
            sqlPrivate = sql + " and (UPPER(owner)= UPPER(sys_context('USERENV', 'CURRENT_USER')) )"; //$NON-NLS-1$  
        }
        result = executeQuery(sqlPrivate);
        if (result == null || result.trim().equals("")) { //$NON-NLS-1$
        	sqlPublic = sql + " and ( owner = 'PUBLIC')"; //$NON-NLS-1$
            result = executeQuery(sqlPublic);
        }
        
        String table_owner = ""; //$NON-NLS-1$
        String table_name = ""; //$NON-NLS-1$
        String db_link = ""; //$NON-NLS-1$
        String obj_type = ""; //$NON-NLS-1$
        if (result == null || result.trim().equals("")) { //$NON-NLS-1$
            result = ""; //$NON-NLS-1$
        } else {
        	List<String> items = Arrays.asList(result.split("\\s*:\\s*"));      	
        	table_owner = items.get(0);
            table_name = items.get(1);
            db_link = items.get(2);
            
            if (table_owner.equals("null") && !db_link.equals("null") ) {
            	table_owner = getDBLinkOwner(db_link);
            }
            /* table_owner and table_name will be in the right case */
            //probably should be done but most reporting assumes no " in schema table_owner='"'+table_owner+'"';
            table_name='"'+table_name+'"';
            if ( db_link != null ) {
            	if (! db_link.equalsIgnoreCase("null")) {
            		table_name = table_name+"@"+db_link;
            		obj_type = getObjType(table_name, table_owner);
            	}
            	else 
            		obj_type = getObjType(table_name, table_owner);
            }
            else 
        		obj_type = getObjType(table_name, table_owner);
        }
        String[] res = { table_owner, table_name, obj_type };
        return res;
    }
    
    private String[] getBaseSynonymObjectRecursive( String synonymName, String synonymOwner) {
    	// 0:table_owner, 1:table_name, 2:obj_type
    	ArrayList<String> synonymList = new ArrayList<String>();
    	
    	String dbLink[] = DBUtil.resolveDBLink(synonymName);
    	
    	if (dbLink != null) {
            synonymName= getObjectName(dbLink[0]).replaceAll("\"|\'", ""); //$NON-NLS-1$
        } 
    	
    	String details[] = getBaseSynonymObj(synonymName, synonymOwner);
    	if (details[2] == null)
    		return null;
    	if ( details[2] != null && details[2].equals("") ) {
    		// Bug 22841863 - DIFF:DESC SYNONYM FAIL WITH SP2-0749 
    		getScriptRunnerContext().write(LINE_SEPARATOR+ScriptRunnerDbArb.format(ScriptRunnerDbArb.DESCR_NO_BASE_OBJECT,"\""+synonymName+"\"")+LINE_SEPARATOR);
    		return null;
    	}
    	// Bug 22095122 - DESC NOT WORK FOR SAME SYNONYMS IN DIFFERENT SCHEMAS 
    	if (details[ 2 ].equals("SYNONYM")) { //$NON-NLS-1$
    		if ( "".equals(details[0])) {
    			synonymList.add(details[1]) ;
    		} else {
    			synonymList.add(details[0]+"."+details[1]) ;
    		}
    		boolean exists = false;
    		do {
    			details = getBaseSynonymObj(details[ 1 ], details[ 0 ]);
    			if (details[2] == null)
    				return null;
    			if ( "".equals(details[0])) {
    				exists = synonymList.contains(details[0]) ;
    			} else {
    				exists = synonymList.contains(details[0]+"."+details[1]) ;
    			}
    			if ( exists) {
    				// Bug 22841863 - DIFF:DESC SYNONYM FAIL WITH SP2-0749 
    				getScriptRunnerContext().write(LINE_SEPARATOR+ScriptRunnerDbArb.format(ScriptRunnerDbArb.DESCR_NO_BASE_OBJECT,"\""+synonymName+"\"")+LINE_SEPARATOR);
    				return null;
    			} else {
    				if ( "".equals(details[0])) {
    					synonymList.add(details[1]) ;
    				} else {
    					synonymList.add(details[0]+"."+details[1]) ;
    				}
    			}
    		} while (details[ 2 ].equals("SYNONYM")) ; //$NON-NLS-1$
    	}
    	
    	String[] objectDetails = { details[0], "'"+details[ 1 ]+"'", details[2] };
        return objectDetails;
    }

    public String upperCaseLocaleUS(ScriptRunnerContext ctx, String lower) {
        String retVal = null;
        if ((lower==null)||(lower.equals(""))) { //$NON-NLS-1$ 
            retVal = lower;
        } else {
            //direction from Sergiusz Wolicki - for SQL at least to uppercase local american should be used
            retVal = lower.toUpperCase(Locale.US);
        }
        return retVal;
    }
      
    private void writeOutputHeader (String objectName, String objectType, ScriptRunnerContext ctx, String datatype ) {
    	if (objectType.equals("PROCEDURE")) //$NON-NLS-1$ //$NON-NLS-2$
    		ctx.write(LINE_SEPARATOR+objectType+" "+objectName+LINE_SEPARATOR); //$NON-NLS-1$
    	else if ( objectType.equals("FUNCTION"))
    		ctx.write(LINE_SEPARATOR+objectType+" "+objectName+ " RETURNS "+datatype+LINE_SEPARATOR); //$NON-NLS-1$ 
    }
    
    private void writeTypeOutputHeader (String objName, String objectType, ScriptRunnerContext ctx, String dblink, String owner ) {
    	
    	String sql = null;
    	if ( owner == null) {
    		sql = "select case when t.upper_bound is null then t.COLL_TYPE\n" + 
    				"    	else decode (t.COLL_TYPE, 'VARYING ARRAY', 'VARRAY') ||'('||t.upper_bound||')'     \n" + 
    				"   end || ' OF ' || d.referenced_name from sys.all_coll_types t, sys.all_dependencies d \n" + 
    				"   where d.referenced_name != 'STANDARD' and t.owner=d.owner and t.type_name = d.name\n" + 
    				"   and t.type_name = " ;
    	} else {
    		sql = "select case when t.upper_bound is null then t.COLL_TYPE\n" + 
    				"    	else decode (t.COLL_TYPE, 'VARYING ARRAY', 'VARRAY') ||'('||t.upper_bound||')'     \n" + 
    				"   end || ' OF ' || d.referenced_owner || '.' ||d.referenced_name from sys.all_coll_types t, sys.all_dependencies d \n" + 
    				"   where d.referenced_name != 'STANDARD' and t.owner=d.owner and t.type_name = d.name\n" + 
    				"   and t.type_name = " ;
    	}
    	if (objName.startsWith("\"")) { //$NON-NLS-1$
    		sql = sql + "'" + objName.replaceAll("\"|\'", "") + "'"; //$NON-NLS-1$
    	} else {
    		sql = sql +  "UPPER('" + objName + "')"; //$NON-NLS-1$
    	}

    	sql = sql + " and UPPER(t.owner)= UPPER(sys_context('USERENV', 'CURRENT_USER'))";

    	String result = executeQuery(sql);
    	
    	if (result != null && ! result.equals("") ) {
    		// ctx.write(LINE_SEPARATOR+result+LINE_SEPARATOR); //$NON-NLS-1$ 
    		ctx.write(LINE_SEPARATOR+objName+" "+result); //$NON-NLS-1$ 
    	}
    
    }
    
    private String getFunctionReturnType(String dblink, String objName) {
    	String sql ;
    	if ( !(dblink.equals("")) ) 
    		sql = "select data_type from sys.all_arguments@"+dblink+" where object_name="; //$NON-NLS-1$  //$NON-NLS-2$
    	else 
    		sql = "select data_type from sys.all_arguments where object_name="; //$NON-NLS-1$  //$NON-NLS-2$

    	if (objName.startsWith("\"")) { //$NON-NLS-1$
    		sql = sql + "'" + objName.replaceAll("\"|\'", "") + "'"; //$NON-NLS-1$
    	} else {
    		sql = sql +  "UPPER('" + objName + "')"; //$NON-NLS-1$
    	}

    	sql = sql + " and UPPER(owner)= UPPER(sys_context('USERENV', 'CURRENT_USER')) and argument_name is NULL";

    	String result = executeQuery(sql);
    	return result;
    }

    
    private String[] getReferencedType(String dblink, String objName) {
    	String sql ;
    	String[] res = new String[3];
    	if ( !(dblink.equals("")) ) 
    		sql = "select data_type from sys.all_arguments@"+dblink+" where object_name="; //$NON-NLS-1$  //$NON-NLS-2$
    	else 
    		sql =  "select referenced_owner, referenced_name, referenced_type FROM sys.all_dependencies where referenced_name != 'STANDARD' and name="; //$NON-NLS-1$ 

    	if (objName.startsWith("\"")) { //$NON-NLS-1$
    		sql = sql + "'" + objName.replaceAll("\"|\'", "") + "'"; //$NON-NLS-1$
    	} else {
    		sql = sql +  "UPPER('" + objName + "')"; //$NON-NLS-1$
    	}

    	sql = sql + " and UPPER(owner)= UPPER(sys_context('USERENV', 'CURRENT_USER')) ";

    	String result = executeQuery(sql);
    	
        if (result == null || result.trim().equals("")) { //$NON-NLS-1$
            res[0] = null ;
            res[1] = "";
            res[2] = null;
        } else {
        	// ref_owner:ref_name:ref_type
        	List<String> items = Arrays.asList(result.split("\\s*:\\s*"));  
            res[0] = items.get(0);
            res[1] = items.get(1);
            res[2] = items.get(2);
        }
        return res;
    }
    
    private boolean ownerExists(String dblink, String ownerName) {
    	String sql ;
		ownerName = ownerName.replaceAll("\"|\'", ""); //$NON-NLS-1$
		if ( ownerName.equalsIgnoreCase("public") || ownerName.equalsIgnoreCase("sys"))
			return true;
    	if ( dblink != null && !(dblink.equals("")) ) 
    		sql = "select username from sys.all_users@"+dblink+" where username="; //$NON-NLS-1$  //$NON-NLS-2$
    	else 
    		sql = "select username from sys.all_users where username="; //$NON-NLS-1$  //$NON-NLS-2$

    	sql = sql +  "UPPER('" + ownerName + "')"; //$NON-NLS-1$

    	String result = executeQuery(sql);
    	if (result != null && !result.equals("")) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    private boolean checkObjectExists(String rawName) {
    	if (_conn==null) return false;
        OracleCallableStatement call = null;
        String sqlScript = ""; //$NON-NLS-1$
        String objName=""; //$NON-NLS-1$
        String ownerName= ""; //$NON-NLS-1$
        String dbLink[] = DBUtil.resolveDBLink(rawName);
        
        if (dbLink != null) {
            objName = dbLink[0];
            sqlScript = "select count(*) from sys.all_objects@" + dbLink[1] ; //$NON-NLS-1$ 
            objName= getObjectName(objName).replaceAll("\"|\'", ""); //$NON-NLS-1$
        } else {
            sqlScript = "select count(*) from sys.all_objects " ; //$NON-NLS-1$
            objName= getObjectName(rawName).replaceAll("\"|\'", ""); //$NON-NLS-1$
        }
                
        sqlScript += " where object_name = "+"UPPER('"+objName+"')";
        ownerName = getObjectOwner(rawName);
        
        String objectType;
        if ( ownerName == null)
        	objectType = getObjType(objName, "") ;
        else
        	objectType = getObjType(objName, ownerName) ;
        
        if (objectType == null || objectType.equals(UNDEFINED_OBJECT_TYPE)) 
        	return false;
        try {
        	if ( objectType != null && (!objectType.equalsIgnoreCase("SYNONYM")) ) {
        		if (ownerName != null ) {
        			ownerName = "UPPER('" + ownerName.replaceAll("\"|\'", "") + "')";
        			sqlScript += " and owner = "+ownerName ;
        		}  
        	}
          PreparedStatement checkStmt = _conn.prepareStatement(sqlScript);
          ResultSet rset = checkStmt.executeQuery();
          rset.next();
          if (rset.getInt(1) == 0) {
              return false;
          }
          return true;
        } catch (SQLException e) {

        } finally{
          if (call != null) {
            try {
              call.close();
            } catch (SQLException e) {
            }
          }
        }
        return true;
    }
    
    
    private boolean checkObjectExists(String objName, String ownerName, String dblink) {
    	if (_conn==null) return false;
    	OracleCallableStatement call = null;
    	String sqlScript = ""; //$NON-NLS-1$
    	if (dblink != null) {
    		sqlScript = "select count(*) from sys.all_objects@" + dblink ; //$NON-NLS-1$ 
    	} else {
    		sqlScript = "select count(*) from sys.all_objects " ; //$NON-NLS-1$
    	} 
    	objName= "UPPER('"+getObjectName(objName).replaceAll("\"|\'", "")+"')"; //$NON-NLS-1$
    	sqlScript += " where object_name = "+objName ; //$NON-NLS-1$
    	if ( ownerName != null || !ownerName.equals("") ) { //$NON-NLS-1$
    		sqlScript += " and owner = "+ownerName ; //$NON-NLS-1$
    	}
    	try {
    		PreparedStatement checkStmt = _conn.prepareStatement(sqlScript);
    		ResultSet rset = checkStmt.executeQuery();
    		rset.next();
    		if (rset.getInt(1) == 0) {
    			return false;
    		}
    		return true;
    	} catch (SQLException e) {

    	} finally{
    		if (call != null) {
    			try {
    				call.close();
    			} catch (SQLException e) {
    			}
    		}
    	}
    	return true;
    }
    
    //Bug 24732093 - DESC SHOWS INCORRECT COL DEF OF A VIEW AFTER MODIFYING THE ORIGINAL TABLE 
    private void compileView(String viewName) {
    	if ("".equals(viewName) )
    		return;
    	String compileSQL = "alter view "+viewName+ " compile" ;
    	String result = executeQuery(compileSQL);
    	if (result != null && !result.equals("")) {
    		return;
    	} 
    }
    
    //Bug 25108498 - DESC AN INVALID FUNCTION SHOULD GET ORA-24372 
    private boolean isObjectStatusValid(Connection conn, String[] res, String objectType) {
    	if (conn==null) 
    		return false;
    	if ("".equals(res[1]) )
    		return false;
        OracleCallableStatement call = null;
        String sqlScript = "";
        String objName="";
        String ownerName= "";
        boolean isValidObject = false;
        String valString = "";
        
        String dbLink[] = DBUtil.resolveDBLink(res[1]);
        if (dbLink != null) {
            objName = dbLink[0];
            sqlScript = "select status from sys.all_objects@" + dbLink[1] ; //$NON-NLS-1$ 
            objName= "UPPER('"+getObjectName(objName).replaceAll("\"|\'", "")+"')";
        } else {
            sqlScript = "select status from sys.all_objects " ; //$NON-NLS-1$
            objName= "UPPER('"+getObjectName(res[1]).replaceAll("\"|\'", "")+"')";
        }
                
        sqlScript += " where object_name = "+objName ;
        ownerName = res[0];
        
        try {
          if (ownerName != null && !ownerName.equals("") ) {
        	  ownerName = "UPPER('" + ownerName.replaceAll("\"|\'", "") + "')";
              sqlScript += " and owner = "+ownerName ;
              valString = res[0] +".";
          } 
          valString += res[1];
          PreparedStatement checkStmt = conn.prepareStatement(sqlScript);
          ResultSet rset = checkStmt.executeQuery();
          rset.next();
          if (rset.getString(1).equalsIgnoreCase("INVALID")) {
        	  isValidObject = false;
          } else 
        	  isValidObject = true;
          // Bug 25906829 - DESC A PROCEDURE DOES NOT WORK AFTER TABLE DEFINITION ALTERED 
          if ( !isValidObject ) {
        	  
        	  String sqlstmt = "ALTER "+ objectType +" "+valString+ " COMPILE";;
        	  Statement stmt = conn.createStatement();
        	  stmt.executeQuery(sqlstmt);
   
        	  checkStmt = conn.prepareStatement(sqlScript);
              rset = checkStmt.executeQuery();
              rset.next();
              if (rset.getString(1).equalsIgnoreCase("INVALID")) {
            	  return false;
              } else 
            	  return true;
          }           
        } catch (SQLException e) {

        } finally{
          if (call != null) {
            try {
              call.close();
            } catch (SQLException e) {
            }
          }
        }
        return true;
    }
    
    
    //Bug 25108744 - DESCRIBE A@ZZZ.ZZZ SHOULD GET ORA-02019 
    private boolean isValidDBLinkConnection(ScriptRunnerContext ctx, String rawName) {	
    	if (!rawName.contains("@")) {
    		return true;
    	}
    	String sqlScript = "";
    	String dbLink[] = rawName.split("@"); //$NON-NLS-1$
    	 	
    	if (dbLink != null) {
    		if (dbLink.length == 2 )
    			sqlScript = "select * from dual@" + dbLink[1] ; //$NON-NLS-1$
    		else if (dbLink.length > 2 ) {
    			String newDBlink = rawName.substring(rawName.indexOf("@"), rawName.length());
    			sqlScript = "select * from dual" + newDBlink ; //$NON-NLS-1$
    		}
    	} else {
    		return true;
    	}
    	String result = executeQuery(sqlScript);
    	if (result != null && result.equalsIgnoreCase("X"))
    		return true;
    	else {
    		ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.DESCRIBE_INVALID_DBLINK) + LINE_SEPARATOR);
    		ctx.putProperty("descr.usage", Boolean.FALSE);
    		return false;
    	}
    }
    
    private String getObjectName(String object) {
		String[] names = object.split("\\.");	//$NON-NLS-1$
		if (names.length == 1) {
			return names[0];
		} else if (names.length == 2) {
			return names[1];
		} else {
			return object;
		}
	}
    
    private String getObjectOwner(String object) {
		String[] names = object.split("\\.");	//$NON-NLS-1$
		if (names.length == 1) {
			return null;
		} else if (names.length == 2) {
			return names[0];
		} else {
			return null;
		}
	}
    
    private ScriptRunnerContext getScriptRunnerContext() {
    	return m_ctx;
    }
    
    private void setScriptRunnerContext(ScriptRunnerContext ctx) {
    	m_ctx = ctx;
    }
    
    private String getCurrentUser() {
    	return DBUtil.getInstance(_conn).executeOracleReturnOneCol("select USER from dual", new HashMap<String, String>());
    }
    
    ////
    
    private void describeTypeMethods(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    	
        cmd.setStmtClass(SQLCommand.StmtType.G_C_SQL);
        cmd.setResultsType(SQLCommand.StmtResultType.G_R_QUERY);
    	addColumnFormatForStoreProcedures(conn, ctx) ;
    	for ( int i = 1 ; i <= m_numberOfMethods ; ++i ) {
    		ctx.write(LINE_SEPARATOR);
    		ctx.write("METHOD"+LINE_SEPARATOR); //$NON-NLS-1$
    		ctx.write("------"+LINE_SEPARATOR); //$NON-NLS-1$
    		getTypeMethodsSignature(conn, ctx, i, m_typeMethodNames.get(i-1));
    		writeTypeMethodArguments(conn, ctx, i);
    	}
    	clearColumnFormatForStoreProcedures(ctx) ;
    	ctx.write(LINE_SEPARATOR);
    	reclaimTypeMethodStorage();
    }
    
    private boolean getTypeMethodsSignature(Connection conn, ScriptRunnerContext ctx, int method_no, String method_name ) {
    	
        Query q = null;
        String sqlQuery = null;
    	q = m_xml.getQuery("DESC_TYPE_METHOD_SIGNATURE", conn); //$NON-NLS-1$
    	sqlQuery = q.getSql();
    	
    	sqlQuery = sqlQuery.replace(":1", m_typeName[0]); //$NON-NLS-1$
    	sqlQuery = sqlQuery.replace(":2", "'"+m_typeName[1]+"'"); //$NON-NLS-1$
    	sqlQuery = sqlQuery.replace(":3", "'"+method_name+"'"); //$NON-NLS-1$
    	sqlQuery = sqlQuery.replace(":4", Integer.toString(method_no)); //$NON-NLS-1$
    	
    	String dresult = executeQuery(sqlQuery);
    	if (dresult != null && !dresult.equals("")) { //$NON-NLS-1$
    		if ( m_typeSelfMethods.contains(method_no) ){
    			dresult = dresult.replace("STATIC", "MEMBER"); //$NON-NLS-1$
    		}
    		ctx.write(dresult);  			
    	}
    	return true; 	
    }
    
    private boolean writeTypeMethodArguments(Connection conn, ScriptRunnerContext ctx, int method_no) {
    	Query q = null;
    	String sqlQuery = null;
    	q = m_xml.getQuery("DESC_TYPE_METHOD_ARGUMENTS", conn); //$NON-NLS-1$
    	sqlQuery = q.getSql().replaceAll("'", "''"); //$NON-NLS-1$ //$NON-NLS-2$
    	sqlQuery = MessageFormat.format(sqlQuery, m_formatArgs);
    	try {
    		packageArgumentsDisplay(conn, ctx, Integer.toString(method_no), m_formatArgs, m_typeName, sqlQuery) ;
    	} catch (Exception ex) {
    		ExceptionHandler.handleException(ex);
    		//ctx.write(ex.toString() +System.getProperty("line.seperator"));
    	}
    	return true;
    }
    
    private boolean isTypeMethodsExists(Connection conn, ScriptRunnerContext ctx, String[] objectName, Object[] formatArgs ) throws SQLException {
    	String objName;
    	String ownerName;
    	initializeTypeMethodStorage();
    	if (!(objectName[ 0 ].equals(""))) { //$NON-NLS-1$
    		ownerName = "UPPER('"+objectName[0]+"')";
    	} else {//now using _NO_OWNER version over database links. [could have used username from dblink metadata instead]
    		ownerName = "UPPER(sys_context('USERENV', 'CURRENT_USER'))"; //$NON-NLS-1$
    	}
    	formatArgs[ formatArgs.length - 1 ] = ownerName;
    	objName = objectName[1];

    	String sqlScript = "select count(*) from all_type_methods where type_name = UPPER('"+objName+"') and UPPER(owner) = "+ownerName ; //$NON-NLS-1$
    	 
    	String listMethods = "select method_name from all_type_methods where type_name = UPPER('"+objName+"') and UPPER(owner) = "+ownerName+" order by method_no"; //$NON-NLS-1$
    	
    	String selfMethods = "select distinct tm.method_no from all_arguments aa, all_type_methods tm where \n" + 
    						 "aa.owner = tm.owner and aa.package_name = tm.type_name and aa.object_name = tm.method_name and aa.argument_name = 'SELF'\n" + 
    						 "and tm.final != 'YES' and tm.method_type = 'PUBLIC' and UPPER(tm.owner) = "+ownerName+" and tm.type_name = UPPER('"+objName+"')\n" + 
    						 "order by tm.method_no" ;

    	String dresult = executeQuery(sqlScript);
    	if (dresult != null && !dresult.equals("")) {
    		m_numberOfMethods = Integer.parseInt(dresult) ;
    		if ( m_numberOfMethods > 0) {
    			String rList = executeQuery(listMethods);
    			if (rList != null && !rList.equals("")) 
    				m_typeMethodNames = new ArrayList<String>(Arrays.asList(rList.split("\\n")));
    			
    			String rSelf = executeQuery(selfMethods);
    			if (rSelf != null && !rSelf.equals("")) 
    				m_typeSelfMethods = getIntegerArrayFromStringArray(new ArrayList<String>(Arrays.asList(rSelf.split("\\n"))));
    			
    			m_typeName[0] = ownerName;
    			m_typeName[1] = objName;
    			m_formatArgs = formatArgs;
    			return true;
    		}   			
    	} else {
    		return false;
    	}
    	return false;
    }
    
    private ArrayList<Integer> getIntegerArrayFromStringArray(ArrayList<String> stringArray) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(String stringValue : stringArray) {
            try {  
                result.add(Integer.parseInt(stringValue));
            } catch(NumberFormatException nfe) {
            	ExceptionHandler.handleException(nfe);
            } 
        }       
        return result;
    }
    
    private void initializeTypeMethodStorage()  {
    	m_isTypeMethods = false;
        m_numberOfMethods = 0;
        m_typeMethodNames =  new ArrayList<String>();
        m_typeSelfMethods =  new ArrayList<Integer>();
        m_typeName = new String[2];
        m_formatArgs = new Object[6];
    }
    
    private void reclaimTypeMethodStorage()  {
    	m_isTypeMethods = false;
        m_numberOfMethods = 0;
        m_typeMethodNames =  null;
        m_typeSelfMethods =  null;
        m_typeName = null;
        m_formatArgs = null;
    }
    
    public String getObjectName() {
		return m_objectName;
	}

	public void setObjectName(String objectName) {
		this.m_objectName = objectName;
	}

	public String getOwnerName() {
		return m_ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.m_ownerName = ownerName;
	}

	public String getDblinkName() {
		return m_dblinkName;
	}

	public void setDblinkName(String dblinkName) {
		this.m_dblinkName = dblinkName;
	}
    
    // Type Methods Variables
    private boolean m_isTypeMethods = false;
    private int m_numberOfMethods = 0;
    private ArrayList<String> m_typeMethodNames =  new ArrayList<String>();
    private ArrayList<Integer> m_typeSelfMethods =  new ArrayList<Integer>();
    private String[] m_typeName = new String[2];
    private Object[] m_formatArgs = new Object[6];
    private final String UNDEFINED_OBJECT_TYPE = "UNDEFINED_OBJECT_TYPE" ;
    private String m_setNull ;
    private boolean m_isSetNullChanged = false ;
    private String m_objectName = null; 
	private String m_ownerName = null;
    private String m_dblinkName = null;
    
}
    
