/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ErrorOut.java">Barry McGillin</a> 
 *
 */
public class ErrorOut extends AForAllStmtsCommand {
    private final static SQLCommand.StmtSubType s_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_ECHO;
    //I am not set echo - but I have to be something I am overriding the type check anyhow.
    
    // private long start;
    public ErrorOut() {
        super(s_cmdStmtSubType);
    }
    
    @Override
    public void doBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        String errorOut=(String) cmd.getProperty(SQLCommand.PROP_ERROR_STRING);
        if (cmd.isSqlPlusSetCmd() && (errorOut!=null)) {
        	if (cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SET_AUTOCOMMIT)||cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SET_AUTOPRINT)||cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SET)) {
    			//Let the actions take the issue.
    			return false;
    		}
        	if (cmd.getSql().trim().equalsIgnoreCase("set")) { //$NON-NLS-1$
        		
        		String prompt = ctx.getPrompt();
				if (prompt.toString().indexOf("@|") >= 0 && prompt.toString().indexOf("|@") > 0 ) { //$NON-NLS-1$ //$NON-NLS-2$
					prompt = prompt.replaceAll("@\\|\\w+\\s?", ""); //$NON-NLS-1$ //$NON-NLS-2$
					prompt = prompt.replaceAll("\\|@", ""); //$NON-NLS-1$ //$NON-NLS-2$
				};
        		if (ctx.isEchoOn()) {
        			ctx.write(prompt + " set\n"); //$NON-NLS-1$
        		}
        		ctx.write(Messages.getString("ErrorOut.8"));  //$NON-NLS-1$

        	} else {
        		ctx.write(errorOut+"\n"); //$NON-NLS-1$
        	}
            return true;
        }
        return false;
    }
}
