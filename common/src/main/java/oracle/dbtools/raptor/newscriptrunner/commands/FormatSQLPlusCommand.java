/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.PatternSyntaxException;

import oracle.dbtools.app.Format;
import oracle.dbtools.app.Persist2XML;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.dbtools.raptor.newscriptrunner.ArgumentQuoteException;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.util.Logger;

public class FormatSQLPlusCommand extends CommandListener implements IHelp{
    

  /*
   * HELP DATA
   *
   */
  public String getCommand(){
    return "FORMAT";
  }
  
  public String getHelp(){
    return CommandsHelp.getString(getCommand());
  }

  @Override
  public boolean isSqlPlus() {
    return false;
  }
  
  
    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx,
            ISQLCommand cmd) {
        //for now assume - = default = Oracle:SQL
        String[] parts  = cmd.getSQLOrig().split("\\s+");
        String toFormat=null;

        try {
        	if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING)!=null) {
        		if (parts.length==2 && parts[1].toUpperCase().equals("BUFFER")) {
        			toFormat = ctx.getSQLPlusBuffer().getBufferSafe().getBuffer();
        			String formatted = null;
        			try {
        				formatted = new Format().format(toFormat);
        			} catch( IOException e ) {
        				ctx.write(MessageFormat.format("Error: {0}\n",e.getMessage()));
        			}
        			if (formatted!=null) {
        				String[] formattedArray = formatted.split("\n");
        				ctx.getSQLPlusBuffer().getBufferSafe().resetBuffer(Arrays.asList(formattedArray));
        				ctx.write(ctx.getSQLPlusBuffer().getBufferSafe().list(false));
        				return true;
        			}
        			ctx.write("Buffer not formatted\n");
        			return true;
        		} else if (parts.length==3 && parts[1].toUpperCase().equals("RULES")){
        			String rulesFile = ctx.prependCD(parts[2]);
        			File f = new File (rulesFile);
        			if( f.exists() ) {
        				URL url = f.toURI().toURL();
        				try {
        					Map<String,Object> options = Persist2XML.read(url);
        					Format.setOptions(options);
        					ctx.write("Formatter rules loaded\n");
        				} catch( Exception e ) {
        					ctx.write(MessageFormat.format("Failed to read {0}. Error: {1}\n",rulesFile,e.getMessage()));
        				}
        			} else {
        				ctx.write(MessageFormat.format("File {0} cannot be opened.\n",rulesFile));
        			}
        			return true;
        		}
        	}
            if (cmd.getSql().split("\n")[0].matches("^(?i)format\\s*-\\s+.*$")) {  //$NON-NLS-1$  //$NON-NLS-2$
                toFormat= cmd.getSql().substring(cmd.getSql().indexOf("-")+1).trim();  //$NON-NLS-1$
            } else {
                //remove for now format file format buffer should be enough could put format select 1 from dual in at some stage, not multiline -> does not make sense
                //if (cmd.getSql().split("\n")[0].matches("^(?i)format\\s*q'..*$")) {  //$NON-NLS-1$  //$NON-NLS-2$
                //    toFormat= cmd.getSql().substring(cmd.getSql().indexOf("q'")).trim();  //$NON-NLS-1$
                //} else {
                    String cmdNoNewline=cmd.getSql().replace("\r\n", " ").replace("\n", " ");//assumption -no linebreaks in file names  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
                    if (cmdNoNewline.matches("^(?i)format\\s*file\\s+.*$")) {  //$NON-NLS-1$
                        toFormat= cmdNoNewline.substring(cmd.getSql().toUpperCase().indexOf("F",2)).trim();  //$NON-NLS-1$
                    } 
                //}
            }
            if (toFormat!=null) {
                /* if ((toFormat.startsWith("q'"))&&(toFormat.length()>5) //&& char at3 matches car at length-2  //$NON-NLS-1$  //$NON-NLS-2$
                        &&toFormat.endsWith(ScriptParser.getEndQuoteString(toFormat.substring(2,3))+"'")) {//&& char at3 matches car at length-2  //$NON-NLS-1$  //$NON-NLS-2$
                    toFormat=toFormat.substring(3,toFormat.length()-2);
                
                    String formatted=ctx.getSQLPlusFormatter().format((ICodingStyleSQLOptions) null,
                            /* will go via getenv in sqlcl and 
                             * default profile in sqldev could put/get a ICodingStyleSQLOpions from ctxO/
                            toFormat);
                    if (formatted!=null) {
                        //check if to format contains whitespace at the ends 
                        //as core formatter strips this
                        if (toFormat.endsWith("\n")){ //$NON-NLS-1$
                            if (!formatted.endsWith("\n")) {//do not want to add whitespace twice.  //$NON-NLS-1$
                                formatted = formatted + "\n";  //$NON-NLS-1$
                            }
                         }
                        if (toFormat.endsWith(" ")) {  //$NON-NLS-1$
                            if (!formatted.endsWith(" ")) {  //$NON-NLS-1$
                                formatted = formatted + " ";  //$NON-NLS-1$
                            }
                        }
                        //want newline at the end so prompt does not come out wrong
                        if (!formatted.endsWith("\n")) {//really want a newline if at all possible. //$NON-NLS-1$
                            formatted = formatted + "\n";  //$NON-NLS-1$
                        }
                        ctx.write(formatted);
                        try {
                            ctx.getOutputStream().flush();
                        } catch (IOException e) {
                            Logger.severe(this.getClass(), e);
                        }
                    } 
                }else*/ if (toFormat.toUpperCase().startsWith("FILE")) {  //$NON-NLS-1$ 
                    String[] args=ScriptUtils.executeArgs(toFormat);
                    //arg0 = -f arg1 = input test if file exists with CD added if necessary arg2 output file or directory m
                    if (args.length==3) {
                        doWork(ctx, args[1], args[2]);
                    } else {
                        ctx.write(getHelp());  //$NON-NLS-1$  
                    }
                } else {
                    ctx.write(getHelp());  //$NON-NLS-1$  
                }
                return true;
            }
        } catch (PatternSyntaxException e) {
            Logger.severe(this.getClass(),e.getLocalizedMessage(),e);
            return true;
        } catch (ArgumentQuoteException e) {
            Logger.severe(this.getClass(),e.getLocalizedMessage(),e);
            ctx.write(getHelp());  //$NON-NLS-1$  
            return true;
        } catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        ctx.write(getHelp()); //$NON-NLS-1$ 
        //must return true if at all possible as it thinks it is a SQL i.e. ending in;
        return true;
    }

    public static int CHUNKSIZE = 1000;

    protected Void doWork(ScriptRunnerContext ctx, String inputFile, String outputFile) {
        boolean errorMade;
        if (inputFile != null && outputFile != null) {
            inputFile = putInDir(ctx,inputFile);
            outputFile = putInDir(ctx,outputFile);
        }

        // So we need to check if the inputfile and the output file is ok. If its not, then we need to fail out and
        // prompt the usage.

        if ((inputFile == null) || (outputFile == null) ) {
            usage(ctx);
            return null;
        } 
        
        if( inputFile.contains("input=") )                        //$NON-NLS-1$  
        	inputFile = inputFile.substring("input=".length());   //$NON-NLS-1$  
        if( outputFile.contains("output=") )                       //$NON-NLS-1$  
        	outputFile = outputFile.substring("output=".length());  //$NON-NLS-1$  
        
        if( !new File(inputFile).exists() && !new File(inputFile).isDirectory() ) {
        	ctx.write(inputFile + " not exists\n");
            return null;
        }  

        // OK. if both input/output are files, then do files,
        // If both are directories, then do directories
        if (new File(inputFile).isDirectory() && (new File(outputFile).isDirectory() || !(new File(outputFile).exists()))) {
            errorMade=doDirectory(ctx, inputFile, outputFile);
        } else {
            errorMade=inputFileToOutputFile(ctx, inputFile, outputFile);
        }

        if (errorMade) {
            usage(ctx);
        }
        return null;
    }

    private void usage(ScriptRunnerContext ctx) {
    	ctx.write(getHelp());
    }

    /**
     * This will format a directory of source and stuff it into an output directory
     *     
     * @param sourceDir
     *            The Source directory to format.
     * @param targetDir
     *            The target directory to put the formatted files.
     */
    private boolean doDirectory(ScriptRunnerContext ctx, String sourceDir, String targetDir) { 
        boolean errorMade=false;;
        // Check that this is a directory cos if its not, this is all rubbish below!
        if (!(new File(targetDir).isDirectory())) {
            new File(targetDir).delete();
            if (!new File(targetDir).mkdir()) {
                errorOn(ctx, sourceDir, targetDir);
                return true;
            }
        }

            // List all the files. We dont care about types here so the user needs to be careful!
        File[] files = new File(sourceDir).listFiles();
        if (files != null) {
            for (File f : files) {
                int slash = f.getPath().lastIndexOf(File.separator);
                int cslash = f.getPath().lastIndexOf(":"); //$NON-NLS-1$
                
                if ((System.getProperty("os.name").toLowerCase().indexOf("win")!=-1)) {  //$NON-NLS-1$   //$NON-NLS-2$ 
                    if ((cslash > slash) && (cslash > 0) && (cslash < 4)) {
                        slash = cslash;
                    }
                }

                if ((slash == -1) || (f.getPath().endsWith(File.separator))) {
                    errorOn(ctx, sourceDir, targetDir);
                    errorMade=true;
                    continue;
                }

                String end = f.getPath();
                if (slash != -1) {
                    end = f.getPath().substring(slash + 1);
                }

                if (f.isDirectory()) {
                    // Drill into all directories recursively
                	if( f.getPath().toUpperCase().equals(targetDir.toUpperCase()) )
                		continue;
                    if(doDirectory(ctx, f.getPath(), targetDir + File.separator + end)) {
                        errorMade=true;
                    }
                    continue;
                }
                
                // Now we have just a file, format it and stuff it in its target directory.
                if(inputFileToOutputFile(ctx, f.getPath(), targetDir + File.separator + end)) {
                    errorMade=true;
                }
            }
        }
        return errorMade;
    }

    /**
     * If we find an error in the way the user has used the simple syntax, flag it and tell them. This will halt the
     * format process.
     * 
     * @param input
     *            The input string which is supposed to either a file, or a direcotry
     * @param output
     *            The output string which is supposed to either a file, or a direcotry
     */
    private void errorOn(ScriptRunnerContext ctx, String input, String output) {
        ctx.write(ScriptRunnerDbArb.format(ScriptRunnerDbArb.ERRORON, input, output)); // (authorized)
    }


    /**
     * Format a simple file and output it to another file. Ultimately, this is what the formatter will do. All other
     * code leads to this method.
     * 
     * @param inputFile
     *            The file to format.
     * @param outputFile
     *            The formatted file target.
     */
    private boolean inputFileToOutputFile(ScriptRunnerContext ctx,String inputFile, String outputFile) {
        BufferedWriter bw = null;
        BufferedReader br = null;
        try {
            if ((inputFile == null) || (outputFile == null) || (!(new File(inputFile).exists()))) {
                errorOn(ctx, inputFile, outputFile);
                return true;
            }
            // Read the file with appropraite encoding as this can lead to issues recognising the charactters and
            // then the format can fail
            String encoding = ctx.getEncoding();//never set??//IdeUtil.getIdeEncoding();
            if ((encoding == null) || (encoding.equals(""))) { //$NON-NLS-1$
                br = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
            } else {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), encoding));
            }
            StringBuilder inputBuffer = new StringBuilder(""); //$NON-NLS-1$
            char chunk[] = new char[CHUNKSIZE];
            int i = 0;
            while ((i = br.read(chunk, 0, CHUNKSIZE)) != -1) {
                inputBuffer.append(chunk, 0, i);
            }
            String inputBufferSt=inputBuffer.toString();
            String output = null;
            final Format format = new Format();
            format.rethrowSyntaxError = true;
			try {
                output = format.format(inputBufferSt);
            } catch( SyntaxError e ) {
            	output = null;
            }
            if (output!=null) {
                //check if to format contains whitespace at the ends 
                //as core formatter strips this
                if (output.endsWith("\n")){ //$NON-NLS-1$
                    if (!output.endsWith("\n")) {//do not want to add whitespace twice.  //$NON-NLS-1$
                        output = output + "\n";  //$NON-NLS-1$
                    }
                 }
                if (inputBufferSt.endsWith(" ")) {  //$NON-NLS-1$
                    if (!output.endsWith(" ")) {  //$NON-NLS-1$
                        output = output + " ";  //$NON-NLS-1$
                    }
                }
                //want newline at the end so prompt does not come out wrong
                if (!output.endsWith("\n")) {//really want a newline if at all possible. //$NON-NLS-1$
                    output = output + "\n";  //$NON-NLS-1$
                }
                String newLine="\n";
                if ((System.getProperty("os.name").toLowerCase().indexOf("win")!=-1)) {  //$NON-NLS-1$   //$NON-NLS-2$ 
                    newLine="\r\n";  //$NON-NLS-1$ 
                }
                output = output.replaceAll("\r{0,1}\n", newLine);  //$NON-NLS-1$ 
            
                new File(outputFile).delete();
                if ((encoding == null) || (encoding.equals(""))) { //$NON-NLS-1$
                    bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));
                } else {
                    bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), encoding));
                }
                bw.write(output);
            } else {
                ctx.write("Skipped formatting "+outputFile+" -- parse error\n");
                return true;
            }
        } catch (Exception ex) {
            Logger.severe(this.getClass(), ex.getStackTrace()[0].toString(), ex);
            errorOn(ctx, inputFile, outputFile);
            return true;
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    // ignore
                }
            }
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
        return false;
    }

    /**
     * Elongate the any filename without its full path
     * 
     * @param input
     *            file or sub directory
     * @return the formatted String
     */
    public String putInDir(ScriptRunnerContext ctx,final String input) {
        if (input == null) {
            return null;
        }  

        int cslash = input.indexOf(":"); //$NON-NLS-1$
        if ((System.getProperty("os.name").toLowerCase().indexOf("win")!=-1)) { //$NON-NLS-1$ 
            if ((cslash > 0) && (cslash < 4)) {
                return input;
            }
        } else {
            if ((input.startsWith("/"))) { //$NON-NLS-1$
                return input;
            }
        }
        String path = (String) ctx.getProperty(ScriptRunnerContext.CDPATH); 
        if (path==null) {
            // If path is not set then get the path from where we started the tool which may not be user.dir
            // not sure what to default to if CD is not set is it the same for sqlcl and sqldev???
            //path=System.getProperty("ide.startingcwd");
            //path=path.replaceAll("\"", "");
            return input;
        }
        if (path.endsWith(File.separator)) {
            return path + input;
        } else {
            return path + File.separator + input;
        }
    }
    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        
    }

    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    public static void main( String[] args ) {
    	for ( int i = 50; 0 < i ; i-- ) {
    		String f = "C:\\temp";
    		for( int j = 0; j < i; j++ ) 
    			f = f + "\\format";
    		final File dir = new File(f);
			System.out.println(i+"="+(dir.exists()));
			if( dir.exists() )
				for( File file: dir.listFiles() ) 
					if (!file.isDirectory()) {
						boolean delete = file.delete();
						if( !delete )
							System.out.print('.');
					}
    		boolean delete = dir.delete();
			if( !delete )
				System.out.println('!');
    	}
	}
}
