/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.CommandRegistry;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;


/**
 * HELP
 * <p>
 * ----
 * <p>
 * 
 * Accesses this command line help system. Enter HELP INDEX or ? INDEX for a
 * list of topics.
 * <p>
 * You can view SQL*Plus resources at
 * <p>
 * 
 * <pre>
 * {@linkplain http://www.oracle.com/technology/tech/sql_plus/}
 * </pre>
 * 
 * and the Oracle Database Library at
 * <p>
 * 
 * <pre>
 * {@linkplain http://www.oracle.com/technology/documentation/}
 * </pre>
 * 
 * HELP|? [topic]
 * <p>
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Help.java"
 *         >Barry McGillin</a>
 * 
 */
public class Help extends CommandListener {

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

		String command = (cmd.getLoweredTrimmedSQL() +" ").substring((cmd.getLoweredTrimmedSQL()+ " ").indexOf(" ")).trim(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		try {
		String[] sets = command.split("\\s+"); //$NON-NLS-1$
		if (sets.length==2 && sets[0].toLowerCase().equals("set")) { //$NON-NLS-1$
			command=sets[0]+sets[1];
		}
		String[] lines= HelpOnOrIndex(command, ctx).split("\n");
		for (String line:lines) {
			ctx.write(line + "\n");
		}
		} catch (Exception e) {
			ctx.write(e.getLocalizedMessage());
			Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		} finally {
		}
		return true;
	}

	private String HelpOnOrIndex(String in, ScriptRunnerContext ctx) throws Exception {
		if ((in == null) || in.equals("") || in.equalsIgnoreCase("help") //$NON-NLS-1$  //$NON-NLS-2$
				|| in.equalsIgnoreCase("index") || in.equalsIgnoreCase("all") || in.equalsIgnoreCase("?")) { //$NON-NLS-1$  //$NON-NLS-2$ //$NON-NLS-3$  
			return getHelpProperties("",ctx); //$NON-NLS-1$ 
		}
		Enumeration<String> A = HelpMessages.getKeys();
		String help = "";//$NON-NLS-1$
		help += getHelpProperties(in,ctx);
		return help;  
	}

	private String getHelpProperties(String in, ScriptRunnerContext ctx) throws Exception {
		String retVal = null;
		Enumeration<String> tagsEnumeration = HelpMessages.getKeys();
		if (tagsEnumeration == null) {
			throw new Exception(Messages.getString("NO_HELP_KEYS")); //$NON-NLS-1$ 
		}
		ArrayList<String> tags = Collections.list(tagsEnumeration);
		removeSetDetails(tags);
		for (IHelp h : CommandRegistry.getCommandsWithHelp()) {
			// Add * so it gets highlighted in the menu
			if (!h.isSqlPlus()) {
				tags.add(h.getCommand() + "*"); //$NON-NLS-1$
			} else {
				tags.add(h.getCommand());
			}
		}
		List<String> depdupeCommands =
			    new ArrayList<>(new LinkedHashSet<>(tags));
		Collections.sort(depdupeCommands); //$NON-NLS-1$ 
		if (in.equals("")) { //$NON-NLS-1$
			retVal = getIndexList(ctx, depdupeCommands);
		} else if (in.length() > 0 && !(tags.contains(in.toUpperCase()) || in.startsWith("set" ) ) ) { //$NON-NLS-1$
			retVal = Messages.getString("LIST_OF_HELP_TOPICS"); //$NON-NLS-1$ 
			StringBuilder returnMeBuff = new StringBuilder();
			for (String mytag : tags) {
				String name = mytag.replace("_", " "); //$NON-NLS-1$ //$NON-NLS-2$
				if (name.toUpperCase().equals(in.trim().toUpperCase())||name.toUpperCase().equals(in.trim().toUpperCase()+"*")) { //$NON-NLS-1$
					retVal = HelpMessages.getString(in.replace(" ", "_").toUpperCase()); //$NON-NLS-1$  //$NON-NLS-2$ 
					if (wasError(retVal)) {
						// try for a new command
						retVal = HelpMessages.getString(in.replace(" ", "_").toUpperCase() + "*"); //$NON-NLS-1$  //$NON-NLS-2$ //$NON-NLS-3$
					}
					if (wasError(retVal)) {
						IHelp localHelp = CommandRegistry.getCommandWithHelp(in);
						if (localHelp != null) {
							retVal = localHelp.getHelp();
						}
					}
					return retVal;
				}
				if (name.toUpperCase().startsWith(in.toUpperCase())) {
					returnMeBuff.append("    ").append(name).append("\n"); //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
				}
			}
			if (returnMeBuff.length() > 0) {
			   retVal = retVal + returnMeBuff.toString();
			} else {
				retVal = retVal + MessageFormat.format(Messages.getString("Help.ObjectNotFound"),in); //$NON-NLS-1$
				retVal = retVal + getIndexList(ctx, depdupeCommands);
			}
		} else {
			retVal = HelpMessages.getString(in.replace(" ", "_").toUpperCase()); //$NON-NLS-1$  //$NON-NLS-2$ 
			if (wasError(retVal)) {
				// try for a new command
				retVal = HelpMessages.getString(in.replace(" ", "_").toUpperCase() + "*"); //$NON-NLS-1$  //$NON-NLS-2$ //$NON-NLS-3$
			}

			// check command help
			if (wasError(retVal)) {
				IHelp localHelp = CommandRegistry.getCommandWithHelp(in);
				if (localHelp != null) {
					retVal = localHelp.getHelp();
				}
			}
			if (wasError(retVal)) {
				if (in!=null && in.toUpperCase().startsWith("set")) { //$NON-NLS-1$
					
					//show help set
					retVal = HelpMessages.getString(in.replace(" ", "_").toUpperCase()); //$NON-NLS-1$  //$NON-NLS-2$ 
					if (wasError(retVal)) {
						// try for a new command
						retVal = HelpMessages.getString(in.replace(" ", "_").toUpperCase() + "*"); //$NON-NLS-1$  //$NON-NLS-2$ //$NON-NLS-3$
					}
					retVal = Messages.getString("Help.1") + retVal; //$NON-NLS-1$
				} else {
					//
					retVal=getIndexList(ctx, depdupeCommands);
				}
			}

		}
		return retVal;
	}
        private boolean wasError(String retval) {//prefer not to have to know key - will go wrong if lookup really was !something!, open to better ideas
            return ((retval==null)||((retval.startsWith("!")&&retval.endsWith("!")))); //$NON-NLS-1$  //$NON-NLS-2$ 
        }


	private String getIndexList(ScriptRunnerContext ctx, List<String> depdupeCommands) throws IOException {
		String retVal = ""; //$NON-NLS-1$
			retVal = Messages.getString("LIST_OF_HELP_TOPICS"); //$NON-NLS-1$ 
			StringBuilder returnMeBuff = new StringBuilder();
			if (ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null && Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
				int cols = 120;
				int maxlen = 0;
				for (String item : depdupeCommands) {
					if (item.length() > maxlen)
						maxlen = item.length();
				}
				int noscols = cols % (maxlen + 1);
				String output = ""; //$NON-NLS-1$
				String line = ""; //$NON-NLS-1$
				int col = 0;
				for (String item : depdupeCommands) {
					if (col >= noscols) {
						output += line + "\n"; //$NON-NLS-1$
						line = ""; //$NON-NLS-1$
						col = 0;
					}
					String pad = new String(new char[maxlen - item.length()]).replace('\0', ' ');
					line += item + pad;
					col++;
				}
				if (line.length() > 0)
					output += line + "\n"; //$NON-NLS-1$
				retVal = retVal + output;
			} else {
			for (String mytag : depdupeCommands) {
				String name = mytag.replace("_", " "); //$NON-NLS-1$ //$NON-NLS-2$
				if (name.endsWith("*")) { //$NON-NLS-1$
					name = highlightBold(name.replace("*", "")); //$NON-NLS-1$ //$NON-NLS-2$
				}
				returnMeBuff.append("    ").append(name).append("\n"); //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
			}
			retVal += printColumns(depdupeCommands, ctx);
			if (ctx.isCommandLine())
			retVal += ""; //$NON-NLS-1$


			}
		return retVal;
	}

	public String highlightBold(String text) {
			return text;//no highlights by defauly. SQLcl may highlight it
	}

	/**
	 * @param tags
	 */
	private void removeSetDetails(ArrayList<String> tags) {
		Iterator<String> it = tags.iterator();
		ArrayList<String> tags2 = new ArrayList<String>();
		while (it.hasNext()) {
			String x = it.next();
			if (!x.toLowerCase().startsWith("set")) { //$NON-NLS-1$
				tags2.add(x);
			}
		}
		tags2.add("SET"); //$NON-NLS-1$
		tags.clear();
		for (String tag:tags2) tags.add(tag);
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}
	
	public String printColumns(final Collection<? extends CharSequence> items, ScriptRunnerContext ctx) throws IOException {
		StringBuilder s = new StringBuilder();
		if (items == null || items.isEmpty()) {
			return ""; //$NON-NLS-1$
		}
		
		int width = 82;
		if (ctx.isCommandLine()) {
			if (ctx.getSQLPlusConsoleReader().getWidth()>width)
				width=ctx.getSQLPlusConsoleReader().getWidth();
		}
		int maxWidth = 0;
		for (CharSequence item : items) {
			maxWidth = Math.max(maxWidth, item.length());
		}
		maxWidth = maxWidth + 3;
		
		StringBuilder buff = new StringBuilder();
		int cols = width/maxWidth;
		int col=0;
		for (CharSequence item : items) {
			String name = item.toString();			
			if (name.endsWith("*")) { //$NON-NLS-1$
			 name = highlightBold(name.replace("*", " ")); //$NON-NLS-1$ //$NON-NLS-2$
			}
			if (col<cols){
				buff.append(name);
				for (int i = 0; i < (maxWidth - item.length()); i++) {
					buff.append(' ');
				}
				col++;
			} else {
				s.append(buff.toString()+"\n"); //$NON-NLS-1$
				buff.setLength(0);
				col=0;
			}
		}
		if (buff.length() > 0) {
			s.append(buff.toString());
		}	
		return s.toString();
	}	
}
