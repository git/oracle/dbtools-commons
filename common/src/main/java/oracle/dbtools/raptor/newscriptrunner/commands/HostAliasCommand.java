/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;

import oracle.dbtools.common.utils.FileUtils;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.Substitution;
import oracle.dbtools.raptor.newscriptrunner.Substitution.SubstitutionException;


/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=HostAliasCommand.java"
 *         >Barry McGillin</a>
 *
 */
public class HostAliasCommand extends CommandListener {
	private static final String CMD = "host"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java
	 * .sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		int hostReturn=0;
		Substitution sub =new Substitution(ctx);
		try {
			sub.replaceSubstitution(cmd);
		} catch (SubstitutionException e1) {}
		String command = cmd.getSql();//getSQLOrig has not been through substitution
		ArrayList<String> cmdTokens = new ArrayList<String>();
		if (command.trim().startsWith("!") || command.trim().startsWith("$")) { //$NON-NLS-1$ //$NON-NLS-2$
			command = command.replaceFirst("\\$|\\!|host|ho|hos", "");// lose the alias //$NON-NLS-1$ //$NON-NLS-2$
																// off the front
		}
		StringTokenizer st = new StringTokenizer(command);
		if (command.trim().toLowerCase().startsWith("host")||command.trim().toLowerCase().startsWith("hos")||command.trim().toLowerCase().startsWith("ho")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			String host = st.nextToken(); // move past host
			command=command.replaceFirst(host, ""); //$NON-NLS-1$
		}
		String addPath="";  //$NON-NLS-1$ 
		if (!((System.getProperty("os.name").toLowerCase().indexOf("win") > -1))) { //$NON-NLS-1$ //$NON-NLS-2$
			try{
				String pwd=Paths.get(".").toAbsolutePath().normalize().toString();
				addPath=" export PATH=\""+pwd+":$PATH\" && "; //$NON-NLS-1$ //$NON-NLS-2$
			} catch (Exception e) {
				ctx.write(e.getMessage()+"\n");  //$NON-NLS-1$
			}
		}

		command=command.trim(); //newline at the end confuses, 
		//whitespace at the front do not care. This is post history and beyond v$sql
		
		// LRGSRG fix: Line continuation characters are causing few SRG's to fail.
		// The issue is in SRG's that use SQLLDR commands with Line continuation character 
		// and that was causing SQLLDR to report LRM-00118: syntax error at '-' at the end of input.
		command = ScriptUtils.checkforContinuationChars(command);
		
		// If we are windows, we'll have to do this in a cmd window
		if (command.trim().equals("")) { //$NON-NLS-1$
			if ((System.getProperty("os.name").toLowerCase().indexOf("win") > -1)) { //$NON-NLS-1$ //$NON-NLS-2$
				cmdTokens.add("cmd.exe"); //$NON-NLS-1$
			}else {
				cmdTokens.add("bash"); //$NON-NLS-1$
		        cmdTokens.add("-c"); //$NON-NLS-1$
			    cmdTokens.add("SQLCLTERM=`stty -g 2>/dev/null` && stty sane 2>/dev/null && "+addPath+"bash -l ; SQLCLEXIT=$? && if test \"M$SQLCLTERM\" != \"M\" ; then stty \"$SQLCLTERM\" 2>/dev/null; fi; exit $SQLCLEXIT"); //$NON-NLS-1$  //$NON-NLS-$
			}
		} else {
			if ((System.getProperty("os.name").toLowerCase().indexOf("win") > -1)) { //$NON-NLS-1$ //$NON-NLS-2$
				cmdTokens.add("cmd.exe"); //$NON-NLS-1$
				cmdTokens.add("/C"); //$NON-NLS-1$
				cmdTokens.add(command);
			} else {
				cmdTokens.add("bash"); //$NON-NLS-1$
		        cmdTokens.add("-c"); //$NON-NLS-1$
			    cmdTokens.add("SQLCLTERM=`stty -g 2>/dev/null` ; stty sane 2>/dev/null; "+addPath+command + "; SQLCLEXIT=$? ; if test \"M$SQLCLTERM\" != \"M\" ; then stty \"$SQLCLTERM\" 2>/dev/null ; fi; exit $SQLCLEXIT"); //$NON-NLS-1$ //$NON-NLS-2$
			}

		}

		ProcessBuilder builder = new ProcessBuilder(cmdTokens);
		builder.directory(new File(FileUtils.getSQLPath(ctx)[0]));
		String changepwd=(String) ctx.getProperty(ScriptRunnerContext.CDPATH);
		if (changepwd!=null) {
			String[] stArray=changepwd.split(File.pathSeparator);
			if ((stArray!=null)&&(stArray.length>0)
					&&(stArray[0]!=null)&&(new File(stArray[0]).isDirectory())) {
				changepwd=stArray[0];
				builder.directory(new File(stArray[0]));
			}
		}

		Process process;
		BufferedInputStream bis=null;
		BufferedOutputStream bos=null;
		ByteArrayOutputStream bufferStore = null;
		try {
			builder.redirectInput(ProcessBuilder.Redirect.INHERIT);
			builder.redirectErrorStream(true);
			if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING)!=null) {
				builder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
				builder.inheritIO();
				//sqlplus host does not put output to spool 
			    process = builder.start();
			    // Might need to populate error status in ctx with this.
			    int x = process.waitFor();
			    hostReturn=x;
			    ctx.getMap().put("_RC", String.valueOf(x)); //$NON-NLS-1$
			} else {
				File tmpInput=null;
				try {
					tmpInput=File.createTempFile("sqldhost", null);  //$NON-NLS-1$
					tmpInput.deleteOnExit();
				//sqldeveloper non interactive: stuff ctx.write 
					//Side affect - output goes to spool (as well as result pane).
					builder.redirectOutput(ProcessBuilder.Redirect.PIPE);
					builder.redirectInput(ProcessBuilder.Redirect.from(tmpInput));
					process = builder.start();
		            try {
		            	int bufSize = 4096;
		            	bis = new BufferedInputStream(process.getInputStream(), bufSize);
		            	bufferStore = new ByteArrayOutputStream();
		            	bos = new BufferedOutputStream(bufferStore);
		            	int len;
		            	byte buffer[] = new byte[ bufSize ];
		            	boolean written=false;
		            	String afterMessage=null;
		            	while (true) {
		            		len=0;
		            		boolean readAlready=false;
		            		try {
		            			process.exitValue();
		            		} catch (IllegalThreadStateException e) {
		            			//thread not finished
		            			readAlready=true;
		            			if ((len = bis.read(buffer, 0, Math.min(bufSize,bis.available())))==-1) {
		            				break;
		            			}
		            		}
		            		if ((!readAlready)&&((len = bis.read(buffer, 0, bufSize))==-1)) {
		            			break;
		            		}
	
		            		bos.write(buffer,0,len);
		            		written=true;
		            		Thread.sleep(200);//do not want a complete busy wait
		            		if (ctx.getTaskProgressUpdater()!=null){
		            			try{
		            				ctx.getTaskProgressUpdater().checkCanProceed();
								} catch (ExecutionException e) {
									process.destroy();
									if (e.getCause()!=null) {
										afterMessage=e.getCause().getMessage()+"\n"; //$NON-NLS-1$
									} else {
										afterMessage=e.getMessage()+"\n"; //$NON-NLS-1$
									}
								}	
		            		}
		            	}
		            	if (written) {
		            		bos.flush();
		            		ctx.write(bufferStore.toString());//best bet default encoding.
			            	ctx.getOutputStream().flush();
		            	}
		            	if (afterMessage!=null) {
		            		ctx.write(afterMessage);
		            	}
		            } finally {
		            	try {
		            		if (tmpInput!=null) {
		            			tmpInput.delete();
		            		}
		            	} catch (Exception e) {
		            		ctx.write(e.getLocalizedMessage().trim()+"\n");  //$NON-NLS-1$
		            	}
		            }
	            } finally {
	            	if (bis!=null) {
	            		try {
	            			bis.close();
	            		} catch (IOException ioe) {
	            			ioe.printStackTrace();
	            		}
	            	}
	            	if (bos!=null) {
	            		try {
	            			bos.close();
	            		} catch (IOException ioe) {
	            			ioe.printStackTrace();
	            		}
	            	}
	            	if (bufferStore!=null) {
	            		try {
	            			bufferStore.close();
	            		} catch (IOException ioe) {
	            			ioe.printStackTrace();
	            		}
	            	}
	            }
	            int x = process.waitFor();
	            hostReturn=x;
	            ctx.getMap().put("_RC", String.valueOf(x)); //$NON-NLS-1$
			}
		} catch (IOException e) {
			ctx.write(e.getMessage()+"\n"); //$NON-NLS-1$
		} catch (InterruptedException e) {
			ctx.write(e.getMessage()+"\n"); //$NON-NLS-1$
		}
		ctx.write("\n"); //$NON-NLS-1$
		if (hostReturn!=0) {
			ScriptUtils.doWhenever(ctx, cmd, conn, false);
		}
		return true;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java.sql.Connection,
	 *      oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 *      oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if(ctx.isCommandLine()){
			ctx.getSQLPlusConsoleReader().pauseReader(true);
		}
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql.Connection,
	 *      oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 *      oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if(ctx.isCommandLine()){
			ctx.getSQLPlusConsoleReader().pauseReader(false);
		}
	}

}
