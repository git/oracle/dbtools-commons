/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.MetaResource;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryXMLSupport;
import oracle.dbtools.raptor.utils.IListPrinter;
import oracle.dbtools.raptor.utils.ListPrinter;
import oracle.dbtools.raptor.utils.ResolvedDBObject;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class Info extends CommandListener implements IHelp {

  private static final String PADDING = "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "; //$NON-NLS-1$

  private static final String PADLINE = "â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€"; //$NON-NLS-1$

  private static final String PADMID = "â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�"; //$NON-NLS-1$

  private static final String THE_RETURN_VALUE_OF_A_FUNCTION = "this is the return value of a function call"; //$NON-NLS-1$

  static QueryXMLSupport s_xml = null;
  
  private static final Logger LOGGER = Logger.getLogger(Info.class.getName());
  
  private long _lastTime;

  private boolean _debug = false;
  
  private static IListPrinter _listPrinter = new ListPrinter();

  protected static synchronized QueryXMLSupport getXMLQueries() {
    if (s_xml == null) {
      // All sql stored in this file
      s_xml = QueryXMLSupport.getQueryXMLSupport(new MetaResource(Info.class.getClassLoader(), "oracle/dbtools/raptor/newscriptrunner/commands/describe.xml")); //$NON-NLS-1$
    }
    return s_xml;
  }

  private void startTime(){
    if ( _debug ){
      _lastTime = System.currentTimeMillis();
    }
  }
  private void time(String s){
    if ( _debug  ) {
      System.out.println("->" + s + ":" + ( System.currentTimeMillis() - _lastTime )); //$NON-NLS-1$ //$NON-NLS-2$
      _lastTime = System.currentTimeMillis();
    }

  }
  private boolean withStats;

  /*
   * HELP DATA
   */
  public String getCommand() {
    return "INFORMATION"; //$NON-NLS-1$
  }

  public String getHelp() {
    return CommandsHelp.getString(getCommand());
  }

  @Override
  public boolean isSqlPlus() {
    return false;
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

    // nothing to do
  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

  }

  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    if (matches("information", cmd.getSql()) || cmd.getLoweredTrimmedSQL().startsWith("info+")) { //$NON-NLS-1$ //$NON-NLS-2$
      String rawName = cmd.getSql().indexOf(" ") > 0 ? cmd.getSql().substring(cmd.getSql().indexOf(" ")).trim() : null; //$NON-NLS-1$ //$NON-NLS-2$
      
      if (rawName == null ) {
        ctx.write(getHelp());
        return true;
      } else if (rawName.toLowerCase().startsWith("loadxmlquerydefinitions")  ) { //$NON-NLS-1$
      
          String newXml = rawName.substring(rawName.indexOf(" ")).trim(); //$NON-NLS-1$
          s_xml = null;
          s_xml = QueryXMLSupport.getQueryXMLSupport(new MetaResource(this.getClass().getClassLoader(), "file://" + newXml)); //$NON-NLS-1$
          if ( s_xml == null ){
            ctx.write("Could not load : " + newXml); //$NON-NLS-1$
          }
          _debug = false;
        return true;
      }
      if (rawName.toLowerCase().startsWith("debugxmlquerydefinitions")  ) { //$NON-NLS-1$
        _debug=true;
      }
      if (rawName.toLowerCase().startsWith("nodebugxmlquerydefinitions")  ) { //$NON-NLS-1$
        _debug=false;
      }
      
      if (rawName == null || rawName.length() == 0) {
        ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.INFO_USAGE) + "\n"); //$NON-NLS-1$
        return true;
      }

      if (cmd.getSql().split(" ")[0].endsWith("+")) { //$NON-NLS-1$ //$NON-NLS-2$
        withStats = true;
      } else {
        withStats = false;
      }
      String filter = null;
      // 0 is a table

      
      ResolvedDBObject dbObj = ResolvedDBObject.resolveDBObject(conn, rawName, filter);
      if (dbObj != null && dbObj.isBad()) {
    	  ctx.write(MessageFormat.format(Messages.getString("Info.19"),rawName)); //$NON-NLS-1$
        return true;
      }
      if (dbObj != null) {
        /*
         * 2 - table / mviews 4 - view 5 - synonym 7 - procedure (top level) 8 -
         * function (top level) 9 - package
         */
        switch (dbObj.getType()) {
        case 1: // index
            describeIndex(conn, ctx, dbObj);
            break;
        case 2: // table
        case 4: // view
            describeTable(conn, ctx, dbObj);
            break;
        case 6: // sequence
            describeSequence(conn, ctx, dbObj);
            break;
        case 7: // procedure
        case 8: // function
          describeProc(conn, ctx, dbObj);
          break;
        case 9: // package
          describePackage(conn, ctx, dbObj);
          break;
        case 13: // type
            describeType(conn, ctx, dbObj);
            break; 
        case 151: // attribute dimension
            describeDimension(conn, ctx, dbObj);
            break;
        case 150: // hierarchy
            describeHierarchy(conn, ctx, dbObj);
            break;
        case 152: // analytic view
            describeAnalyticView(conn, ctx, dbObj);
            break;
        default:
            write(ctx, MessageFormat.format(Messages.getString("Info.20"), rawName )); //$NON-NLS-1$
            break;
       }
      } else {
          write(ctx, MessageFormat.format(Messages.getString("Info.21"), rawName )); //$NON-NLS-1$
      }
      write(ctx, "" + ansireset()); //$NON-NLS-1$
      return true;
    } else {
      return false;// nothing to do here.
    }
  }

	private void describePackage(Connection conn, ScriptRunnerContext ctx, ResolvedDBObject dbObj) {
    write(ctx, underline("Package") + "\n\n"); //$NON-NLS-1$ //$NON-NLS-2$
    Query colSQL = getXMLQueries().getQuery("desc.package", conn); //$NON-NLS-1$
    
    DBUtil dbUtil = DBUtil.getInstance(conn);
    write(ctx, white_black("/* Package " + dbObj.getOwner() + "." + dbObj.getName() + " */")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

    {
      List<Map<String, ?>> procs = dbUtil.executeReturnList(colSQL.getSql(), dbObj.getBinds());
      for (Map<String, ?> proc : procs) {
        String type = "PROCEDURE"; //$NON-NLS-1$
        String owner = dbObj.getOwner();
        String name = proc.get("PROCEDURE_NAME").toString(); //$NON-NLS-1$
        ArrayList<String> argNames = new ArrayList<String>();
        ArrayList<String> argInOut = new ArrayList<String>();
        ArrayList<String> datatypes = new ArrayList<String>();
        HashMap binds = dbObj.getBinds();
        binds.put("OVERLOAD", proc.get("OVERLOAD") == null ? null : proc.get("OVERLOAD").toString()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        binds.put("PROC_NAME", proc.get("PROCEDURE_NAME").toString()); //$NON-NLS-1$ //$NON-NLS-2$

        Query argsSQL = getXMLQueries().getQuery("desc.package.args", conn); //$NON-NLS-1$
        List<Map<String, ?>> args = dbUtil.executeReturnList(argsSQL.getSql(), binds);
        if (args.size() > 0 && args.get(0) != null && args.get(0).get("POSITION").toString().equals("0")) { //$NON-NLS-1$ //$NON-NLS-2$
          type = "FUNCTION"; //$NON-NLS-1$
        }
        for (Map<String, ?> arg : args) {
          argNames.add(arg.get("ARGUMENT_NAME") == null ? THE_RETURN_VALUE_OF_A_FUNCTION : arg.get("ARGUMENT_NAME").toString()); //$NON-NLS-1$ //$NON-NLS-2$
          argInOut.add(arg.get("IN_OUT")==null? "":arg.get("IN_OUT").toString()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          datatypes.add(arg.get("DATA_TYPE")==null? "":arg.get("DATA_TYPE").toString()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

        }

        write(ctx, "\n"); //$NON-NLS-1$

        printProc(ctx, owner, dbObj.getName() + "." + name, type, argNames, argInOut, datatypes); //$NON-NLS-1$

      }
      write(ctx, "\n\n"); //$NON-NLS-1$
    }

  }

	private void printProc(ScriptRunnerContext ctx, String owner, String name, String type, ArrayList<String> argNames, ArrayList<String> argInOut, ArrayList<String> datatypes) {
    write(ctx, "/*  " + white(type + "  " + owner + "." + name)+ "  */\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

    String line = null;
    if (type.equals("FUNCTION")) { //$NON-NLS-1$
      write(ctx, "    /*   RETURN " + black_white(datatypes.get(0) + "   */\n")); //$NON-NLS-1$ //$NON-NLS-2$
      line = "     v_ret := " + owner + "." + name + "("; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    } else {
      line = "    " + owner + "." + name + "("; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }
    write(ctx, line);
    int paramStart = line.length() + 1;

    if (argNames.size() == 0) {
      write(ctx, ");\n"); //$NON-NLS-1$
    } else {
      write(ctx, " "); //$NON-NLS-1$

      // take out the function return
      if (type.equalsIgnoreCase("function")) { //$NON-NLS-1$
        argNames.remove(0);
        argInOut.remove(0);
        datatypes.remove(0);
      }
      int width = 0;
      for (String arg : argNames) {
        if (width < arg.length()) {
          width = arg.length() + 1;
        }

      }

      for (int i = 0; i < argNames.size(); i++) {
        if (argNames.get(i) != null) {
          // TODO for quoting
          if (i != 0) {
            write(ctx, ",\n"); //$NON-NLS-1$
            write(ctx, PADDING.substring(0, paramStart));
          }
        //Handle null params.   
        if (argNames.get(i)!=null&&argNames.get(i).length()>0&&!argNames.get(i).equals(THE_RETURN_VALUE_OF_A_FUNCTION)) {
          line = "  " + argNames.get(i); //$NON-NLS-1$

          line = line + "  " + PADDING.substring(0, (width - argNames.get(i).length())); //$NON-NLS-1$
          line = line + "=>  " + "p_" + argInOut.get(i).replace("/", "_") + "_param" + i + "  /*   " + format(datatypes.get(i))+ "   */"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$

          write(ctx, line);
         }
        }
      }
      write(ctx, ");\n"); //$NON-NLS-1$
    }
  }

	private void describeProc(Connection conn, ScriptRunnerContext ctx, ResolvedDBObject dbObj) {

    Query colSQL = getXMLQueries().getQuery("desc.plsql", conn); //$NON-NLS-1$
    DBUtil dbUtil = DBUtil.getInstance(conn);

    String type = null;
    String owner = null;
    String name = null;
    ArrayList<String> argNames = new ArrayList<String>();
    ArrayList<String> argInOut = new ArrayList<String>();
    ArrayList<String> datatypes = new ArrayList<String>();

    {
      List<Map<String, ?>> procs = dbUtil.executeReturnList(colSQL.getSql(), dbObj.getBinds());
      owner = dbObj.getOwner();
      name = dbObj.getName();
      type = procs.get(0).get("OBJECT_TYPE").toString(); //$NON-NLS-1$

      Query argsSQL = getXMLQueries().getQuery("desc.proc.args", conn); //$NON-NLS-1$
      List<Map<String, ?>> args = dbUtil.executeReturnList(argsSQL.getSql(), dbObj.getBinds());
      for (Map<String, ?> arg : args) {
        argNames.add(arg.get("ARGUMENT_NAME") == null ? THE_RETURN_VALUE_OF_A_FUNCTION : arg.get("ARGUMENT_NAME").toString()); //$NON-NLS-1$ //$NON-NLS-2$
        argInOut.add(arg.get("IN_OUT").toString()); //$NON-NLS-1$
        datatypes.add(arg.get("DATA_TYPE").toString()); //$NON-NLS-1$
      }

      printProc(ctx, owner, name, type, argNames, argInOut, datatypes);
    }

  }

  private void describeTable(Connection conn, ScriptRunnerContext ctx, ResolvedDBObject dbObj) {

    // {
    // Ansi ansi = Ansi.ansi();
    //
    // for (Ansi.Attribute c : Ansi.Attribute.values()){
    // System.out.println( ansi.a(c).a("") + "This is a test of:" + ansi.reset()
    // + " " + c);
    // }
    // }

    /*
     * Columns
     */

    // â”Œâ”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”�
    // â”‚ EMP â”‚
    // â•žâ•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•�â•¡
    // â”‚ â”‚
    // â”‚ â”‚
    // â””â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”€â”˜

    // int len = dbObj.getName().length()+2;
    //
    // write(ctx,"â”Œ"+ PADLINE.substring(0,len) +"â”�\n");
    // write(ctx,"â”‚ " + dbObj.getName() + " â”‚\n");
    // write(ctx,"â•ž"+ PADMID.substring(0,len)+"â•¡\n");
    // write(ctx,"â”‚" + PADDING.substring(0,len)+"â”‚\n");
    // write(ctx,"â””"+ PADLINE.substring(0,len) +"â”˜\n");
    // num_rows,sample_size,last_analyzed,inmemory,dml_timestamp ,comments


    startTime();
    Query tableSQL = getXMLQueries().getQuery("desc.table", conn); //$NON-NLS-1$
    DBUtil dbUtil = DBUtil.getInstance(conn);
    time("Got table sql"); //$NON-NLS-1$
    {
      List<Map<String, ?>> cols = dbUtil.executeReturnList(tableSQL.getSql(), dbObj.getBinds());
      time("Exec table sql"); //$NON-NLS-1$
      // doesn't exit for views
      if (cols != null && cols.size() == 1 && cols.get(0) != null) {
        Map<String, ?> m = cols.get(0);
        write(ctx, format_reset("TABLE: " + dbObj.getName()) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$

        // write(ctx, Ansi.ansi().format("\t LAST DML:" +
        // cols.get(0).get("DML_TIMESTAMP")).reset() + " \n");
        write(ctx, format_reset("\t LAST ANALYZED:" + getValue(m, "LAST_ANALYZED"))+ " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        write(ctx, format_reset("\t ROWS         :" + getValue(m, "NUM_ROWS")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        write(ctx, format_reset("\t SAMPLE SIZE  :" + getValue(m, "SAMPLE_SIZE")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        write(ctx, format_reset("\t INMEMORY     :" + getValue(m, "INMEMORY")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        write(ctx, format_reset("\t COMMENTS     :" + getValue(m, "COMMENTS").replace("\n", "\n                       "))+ " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
        write(ctx, "\n"); //$NON-NLS-1$
        time("Print table sql"); //$NON-NLS-1$

      }
    }

    write(ctx, format_reset("Columns") + " \n"); //$NON-NLS-1$ //$NON-NLS-2$

    Query colSQL = getXMLQueries().getQuery("desc.cols", conn); //$NON-NLS-1$
    time("Got col sql"); //$NON-NLS-1$
    {
      List<Map<String, ?>> cols = dbUtil.executeReturnList(colSQL.getSql(), dbObj.getBinds());
      time("Exec col sql"); //$NON-NLS-1$
      int widestCol = getWidest(cols, "COLUMN_NAME") + 2; //$NON-NLS-1$
      int widestData = getWidest(cols, "DATA_TYPE") + 2; //$NON-NLS-1$
      int widestLow = getWidest(cols, "LOW_VALUE") + 2; //$NON-NLS-1$
      int widestDefault = getWidest(cols, "DATA_DEFAULT") + 2; //$NON-NLS-1$

      int widestHigh = getWidest(cols, "HIGH_VALUE") + 2; //$NON-NLS-1$
      int widestDist = getWidest(cols, "NUM_DISTINCT") + 2; //$NON-NLS-1$
      int widestHist = getWidest(cols, "HISTOGRAM") + 2; //$NON-NLS-1$
      if (withStats) {
        String fmt = "%-" + widestCol + "s" + "%-" + widestData + "s %-5s %-10s %-" + widestLow + "s %-" + widestHigh + "s %-" + widestDist + "s %-" + widestHist + "s\n"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$

        write(ctx, underline_format(fmt,"NAME", "DATA TYPE", "NULL", "DEFAULT", "LOW_VALUE", "HIGH_VALUE", "NUM_DISTINCT", "HISTOGRAM" )); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$

        for (Map<String, ?> m : cols) {
          if (getValue(m, "COLUMN_NAME").startsWith("*")) { //$NON-NLS-1$ //$NON-NLS-2$
            write(ctx, format("%-" + widestCol + "s", getValue(m, "COLUMN_NAME").trim())); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          } else {
            write(ctx, String.format("%-" + widestCol + "s", getValue(m, "COLUMN_NAME"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          }

          write(ctx, String.format("%-" + widestData + "s", getValue(m, "DATA_TYPE"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

          write(
              ctx,
              String.format(" %-5s %-" + widestDefault + "s %-" + widestLow + "s %-" + widestHigh + "s %-" + widestDist + "s %-" + widestHist + "s\n", getValue(m, "NULLABLE"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
                  getValue(m, "DATA_DEFAULT"), getValue(m, "LOW_VALUE"), getValue(m, "HIGH_VALUE"), getValue(m, "NUM_DISTINCT"), getValue(m, "HISTOGRAM"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

        }
      } else {
        write(ctx, underline_format("%-" + widestCol + "s" + "%-" + widestData + "s %-5s %-10s %s\n", "NAME", "DATA TYPE", "NULL", "DEFAULT", "COMMENTS")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$

        for (Map<String, ?> m : cols) {
          if (getValue(m, "COLUMN_NAME").startsWith("*")) { //$NON-NLS-1$ //$NON-NLS-2$
            write(ctx, format("%-" + widestCol + "s", getValue(m, "COLUMN_NAME"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          } else {
            write(ctx, String.format("%-" + widestCol + "s", getValue(m, "COLUMN_NAME"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          }

          //int cmtPad = widestCol + widestData + 5 + 10 + widestDefault + 3;
          //write(ctx, String.format("%-" + widestData + "s", getValue(m, "DATA_TYPE")));
          //write(ctx,
          //    String.format(" %-5s %-" + widestDefault + "s %s\n", getValue(m, "NULLABLE"), getValue(m, "DATA_DEFAULT"), getValue(m, "COMMENTS").replace("\n", "\n" + PADDING.substring(0, cmtPad))));

          int cmtPad = widestCol + widestData + 5 + 10 + 3;
          write(ctx, String.format("%-" + widestData + "s", getValue(m, "DATA_TYPE"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          write(ctx, String.format(" %-5s", getValue(m, "NULLABLE"))); //$NON-NLS-1$ //$NON-NLS-2$
          
          String default_value = getValue(m, "DATA_DEFAULT"); //$NON-NLS-1$
          if ( default_value != null && default_value.isEmpty() )
        	  write(ctx, String.format(" %-" + (widestDefault-4) + "s",getValue(m, "DATA_DEFAULT"))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          else 
        	  write(ctx, String.format(" %-" + (widestDefault) + "s",getValue(m, "DATA_DEFAULT"))); // ToDO : what id default values in CLOB/long ? //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          
          String comments = getValue(m, "COMMENTS"); //$NON-NLS-1$
          comments = comments.replaceAll("\\n", ""); //m_lineSeparator //$NON-NLS-1$ //$NON-NLS-2$
          StringBuffer commentsBuff;
          if (comments.length() > 50 ) // Assumed this comment column width = 50
        	  commentsBuff = ScriptUtils.wordwrap(12, 50 , new StringBuffer(comments), -1); // JDBC Varchar type is 12 
          else 
        	  commentsBuff = new StringBuffer(comments);
          write(ctx, String.format(" %s\n", commentsBuff.toString().replace(m_lineSeparator, m_lineSeparator + PADDING.substring(0, cmtPad))));  //$NON-NLS-1$
        }
      }
      time("printed col sql"); //$NON-NLS-1$

    }

    /*
       * 
       */
    write(ctx, ansireset() + "\n"); //$NON-NLS-1$

    /*
     * Indexes
     */
    {
      Query idxSQL = getXMLQueries().getQuery("desc.idx", conn); //$NON-NLS-1$
      time("Got idx sql"); //$NON-NLS-1$
      List<List<?>> rows = dbUtil.executeReturnListofList(idxSQL.getSql(), dbObj.getBinds());
      time("Exec idx sql"); //$NON-NLS-1$
      if (rows.size() > 1) {
        _listPrinter.printListofList(ctx, "Indexes", rows); //$NON-NLS-1$
      }
      time("printed idx sql"); //$NON-NLS-1$

    }

    /*
     * 
     */
    write(ctx, "\n"); //$NON-NLS-1$
    /*
     * Child Tables
     */
    Query refSQL = getXMLQueries().getQuery("desc.child", conn); //$NON-NLS-1$
    time("Got FK sql"); //$NON-NLS-1$

    List<List<?>> rows = dbUtil.executeReturnListofList(refSQL.getSql(), dbObj.getBinds());
    time("Exec FK sql"); //$NON-NLS-1$

    if (rows.size() > 1) {
     _listPrinter.printListofList(ctx, "References", rows); //$NON-NLS-1$
    }
    time("printed FK sql"); //$NON-NLS-1$


  }

  private void describeIndex(Connection conn, ScriptRunnerContext ctx, ResolvedDBObject dbObj) {
      writeObjectMetadata(conn, ctx, "INDEX", dbObj.getOwner(), dbObj.getName()); //$NON-NLS-1$
  }
  
    private void describeSequence(Connection conn, ScriptRunnerContext ctx, ResolvedDBObject dbObj) {
        //writeObjectMetadata(conn, ctx, "SEQUENCE", dbObj.getOwner(), dbObj.getName()); //$NON-NLS-1$

        Query sequenceSQL = getXMLQueries().getQuery("desc.sequence", conn); //$NON-NLS-1$
        DBUtil dbUtil = DBUtil.getInstance(conn);
        Map<String, Object> binds = new HashMap<>();
        binds.put("OBJECT_OWNER", dbObj.getOwner()); //$NON-NLS-1$
        binds.put("OBJECT_NAME", dbObj.getName()); //$NON-NLS-1$
       
        List<Map<String, ?>> cols = dbUtil.executeReturnList(sequenceSQL.getSql(), binds);
        time("Exec Sequence sql"); //$NON-NLS-1$
        if (cols != null && cols.size() == 1 && cols.get(0) != null) {
            Map<String, ?> m = cols.get(0);
            write(ctx, format_reset("SEQUENCE: " + dbObj.getName()) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$
            write(ctx, format_reset("\t SEQUENCE_OWNER :" + getValue(m, "SEQUENCE_OWNER")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            write(ctx, format_reset("\t SEQUENCE_NAME  :" + getValue(m, "SEQUENCE_NAME")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            write(ctx, format_reset("\t MIN_VALUE      :" + getValue(m, "MIN_VALUE")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            write(ctx, format_reset("\t MAX_VALUE      :" + getValue(m, "MAX_VALUE")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            write(ctx, format_reset("\t INCREMENT_BY   :" + getValue(m, "INCREMENT_BY")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            write(ctx, format_reset("\t CYCLE_FLAG     :" + getValue(m, "CYCLE_FLAG")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            write(ctx, format_reset("\t ORDER_FLAG     :" + getValue(m, "ORDER_FLAG")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            write(ctx, format_reset("\t CACHE_SIZE     :" + getValue(m, "CACHE_SIZE")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            write(ctx, format_reset("\t LAST_NUMBER    :" + getValue(m, "LAST_NUMBER")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            String part  = getValue(m, "PARTITION_COUNT");
            if ( "".equals(part) || part == null ) 
                part = "(null)" ;
            write(ctx, format_reset("\t PARTITION_COUNT:" + part) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            write(ctx, format_reset("\t SESSION_FLAG   :" + getValue(m, "SESSION_FLAG")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            write(ctx, format_reset("\t KEEP_VALUE     :" + getValue(m, "KEEP_VALUE")) + " \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            
            write(ctx, "\n"); //$NON-NLS-1$
            time("Print Sequence sql"); //$NON-NLS-1$
        }
    }
  
  private void describeType(Connection conn, ScriptRunnerContext ctx, ResolvedDBObject dbObj) {
      writeObjectMetadata(conn, ctx, "TYPE", dbObj.getOwner(), dbObj.getName()); //$NON-NLS-1$
  }

    private void describeHierarchy(Connection conn, ScriptRunnerContext ctx, ResolvedDBObject dbObj) {
        DBUtil dbUtil = DBUtil.getInstance(conn);
        write(ctx, format("HIERARCHY: " + dbObj.getName())+ " \n\n"); //$NON-NLS-1$ //$NON-NLS-2$

        write(ctx, format("Levels") + " \n"); //$NON-NLS-1$ //$NON-NLS-2$
        Query levelsSQL = getXMLQueries().getQuery("desc.hierarchy.levels", conn); //$NON-NLS-1$
        Map<String, Object> binds = new HashMap<>();
        binds.put("OWNER", dbObj.getOwner()); //$NON-NLS-1$
        binds.put("OBJECT_NAME", dbObj.getName()); //$NON-NLS-1$
        List<Map<String, ?>> cols = dbUtil.executeReturnList(levelsSQL.getSql(), binds);    
        _listPrinter.print(ctx, cols);
        
        write(ctx, format("References") + " \n"); //$NON-NLS-1$ //$NON-NLS-2$
        Query referencesSQL = getXMLQueries().getQuery("desc.object.references", conn); //$NON-NLS-1$
        cols = dbUtil.executeReturnList(referencesSQL.getSql(), binds);
        _listPrinter.print(ctx, cols);
    }
    
    private void describeAnalyticView(Connection conn, ScriptRunnerContext ctx, ResolvedDBObject dbObj) {
        DBUtil dbUtil = DBUtil.getInstance(conn);
        write(ctx, format("ANALYTIC VIEW: " + dbObj.getName()) + " \n\n"); //$NON-NLS-1$ //$NON-NLS-2$

        Query measuresSQL = getXMLQueries().getQuery("desc.analytic.view.measures", conn); //$NON-NLS-1$
        Map<String, Object> binds = new HashMap<>();
        binds.put("OWNER", dbObj.getOwner()); //$NON-NLS-1$
        binds.put("OBJECT_NAME", dbObj.getName()); //$NON-NLS-1$
        List<Map<String, ?>> cols = dbUtil.executeReturnList(measuresSQL.getSql(), binds);
        if(!cols.isEmpty()){
            write(ctx, format("Measures") + " \n"); //$NON-NLS-1$ //$NON-NLS-2$
            _listPrinter.print(ctx, cols);
        }
        
        Query calculatedSQL = getXMLQueries().getQuery("desc.analytic.view.calc.measures", conn); //$NON-NLS-1$
        cols = dbUtil.executeReturnList(calculatedSQL.getSql(), binds);
        if(!cols.isEmpty()){
            write(ctx, format("Calculated Measures")+ " \n"); //$NON-NLS-1$ //$NON-NLS-2$
            _listPrinter.print(ctx, cols);
        }
        
        write(ctx, format("Dimensionality") + " \n"); //$NON-NLS-1$ //$NON-NLS-2$
        Query dimensionalitySQL = getXMLQueries().getQuery("desc.analytic.view.dimensionality", conn); //$NON-NLS-1$
        cols = dbUtil.executeReturnList(dimensionalitySQL.getSql(), binds);
        _listPrinter.print(ctx, cols);
        
        write(ctx, format("References") + " \n"); //$NON-NLS-1$ //$NON-NLS-2$
        Query referencesSQL = getXMLQueries().getQuery("desc.object.references", conn); //$NON-NLS-1$
        cols = dbUtil.executeReturnList(referencesSQL.getSql(), binds);
        _listPrinter.print(ctx, cols);
    }
    
    private void describeDimension(Connection conn, ScriptRunnerContext ctx, ResolvedDBObject dbObj) {
        DBUtil dbUtil = DBUtil.getInstance(conn);
        write(ctx,format("ATTRIBUTE DIMENSION: " + dbObj.getName()) + " \n\n"); //$NON-NLS-1$ //$NON-NLS-2$

        write(ctx, format("Sources") + " \n"); //$NON-NLS-1$ //$NON-NLS-2$
        Query measuresSQL = getXMLQueries().getQuery("desc.dimension.sources", conn); //$NON-NLS-1$
        Map<String, Object> binds = new HashMap<>();
        binds.put("OWNER", dbObj.getOwner()); //$NON-NLS-1$
        binds.put("OBJECT_NAME", dbObj.getName()); //$NON-NLS-1$
        List<Map<String, ?>> cols = dbUtil.executeReturnList(measuresSQL.getSql(), binds);    
        _listPrinter.print(ctx, cols);
        
        write(ctx, format("Attributes ")+ " \n"); //$NON-NLS-1$ //$NON-NLS-2$
        Query calculatedSQL = getXMLQueries().getQuery("desc.dimension.attributes", conn); //$NON-NLS-1$
        cols = dbUtil.executeReturnList(calculatedSQL.getSql(), binds);
        _listPrinter.print(ctx, cols);
        
        write(ctx, format("References") + " \n"); //$NON-NLS-1$ //$NON-NLS-2$
        Query referencesSQL = getXMLQueries().getQuery("desc.object.references", conn); //$NON-NLS-1$
        cols = dbUtil.executeReturnList(referencesSQL.getSql(), binds);
        _listPrinter.print(ctx, cols);
    }
  
  private void writeObjectMetadata(Connection conn, ScriptRunnerContext ctx, String objectType, String owner, String objectName) {
      DBUtil dbUtil = DBUtil.getInstance(conn);
      Query argsSQL = getXMLQueries().getQuery("desc.metadata", conn); //$NON-NLS-1$
      Map<String, Object> binds = new HashMap<>();
      binds.put("OBJECT_TYPE", objectType); //$NON-NLS-1$
      binds.put("OBJECT_NAME", objectName); //$NON-NLS-1$
      binds.put("OWNER", owner); //$NON-NLS-1$
      String metadata = dbUtil.executeReturnOneCol(argsSQL.getSql(), binds);
      write(ctx, metadata + "\n\n");   //$NON-NLS-1$
  }
  
  public void write(ScriptRunnerContext ctx, String s) {
    // ctx.write(color);
    ctx.write(s);
    // ctx.write(ANSI_RESET);
    try {
      ctx.getOutputStream().flush();
    } catch (IOException e) {    }
  }

  public String getValue(Map<String, ?> m, String key) {
    if (m.get(key) == null) {
      return ""; //$NON-NLS-1$
    } else {
      return m.get(key).toString();
    }
  }

  public int getWidest(List<Map<String, ?>> cols, String string) {
    int ret = string.length();
    for (Map<String, ?> m : cols) {
      if (m.get(string) != null) {
        int l = m.get(string).toString().length();
        ret = l > ret ? ret = l : ret;
      }
    }
    return ret;
  }

  
	public String format(String string) {
	 return string;
	}
	
	public String format(String fmt,Object... args) {
		return String.format(fmt,args);
	}
	
	public String underline_format(String fmt,Object... args) {
		return String.format(fmt,args);
	}
	
  public String ansireset() {
  	return ""; //$NON-NLS-1$
	}
  
	public String white(String string) {
  	return string;
	}
	
  public String white_black(String string) {
  	return string;
	}
  
  public String black_white(String string) {
  	return string;
  }

	public String underline(String string) {
  	return string;
	}
	
  public String format_reset(String string) {
	 return string;
	}
  
  static public void setListPrinter(IListPrinter listprinter){
  	_listPrinter = listprinter;
  }
  
  private static final String m_lineSeparator = System.getProperty("line.separator"); //$NON-NLS-1$
}
