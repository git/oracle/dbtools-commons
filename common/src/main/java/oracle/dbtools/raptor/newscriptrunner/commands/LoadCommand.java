/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.data.loadservice.ExitCode;
import oracle.dbtools.data.loadservice.LoadAPI;
import oracle.dbtools.data.loadservice.LoadParmsAPI;
import oracle.dbtools.data.loadservice.LoadParmsAPI.SERVICE_METHOD;
import oracle.dbtools.data.readservice.ReadParmsDelimitedAPI;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;

import oracle.dbtools.util.LogHandler;


/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=LoadCSVCommand.java">Barry McGillin</a> 
 *
 */
public class LoadCommand extends CommandListener implements IHelp, IShowCommand, Connected {
	
	private String _filename;
	private String _responseName;
	private Connection _conn;
	private ScriptRunnerContext _ctx;
	
	private String _schema;
	private String _table;
	
	private File _loadFile;
	private File _responseFile;
	private FileInputStream _input;
	private OutputStream _output;
	
	private boolean _doCommit;
	
	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#handle(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		return false;
	}

	
	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.IHelp#getCommand()
	 */
	@Override
	public String getCommand() {
		return "LOAD";	//$NON-NLS-1$
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.IHelp#getHelp()
	 */
	@Override
	public String getHelp() {
		return CommandsHelp.getString(getCommand());
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.IHelp#isSqlPlus()
	 */
	@Override
	public boolean isSqlPlus() {
		return false;
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		boolean isValid=true;
		_ctx = ctx;
		String sql = cmd.getSql().trim();
		String[] cmds = sql.split(" ");	//$NON-NLS-1$
		try {
			if (conn != null) {
				_conn=conn;
				int length = cmds.length;
				if (length > 3) {
					return false;
				} else if (length == 3) {
					
					String object = cmds[1];
					_filename = ctx.prependCD(cmds[2]);
					if (!validateTable(object)){
						isValid=false;
						ctx.write(MessageFormat.format(Messages.getString("LOAD_TABLE_ERR"),_schema+"."+_table)+"\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					}
					if (!validateFile(_filename)){
						isValid=false;
						ctx.write(MessageFormat.format(Messages.getString("LOAD_FILE_ERR"),_filename)+"\n"); //$NON-NLS-1$ //$NON-NLS-2$
					}
					if (isValid){
						Long commitSetting = (Long) ctx.getProperty(ScriptRunnerContext.AUTOCOMMITSETTING);
			            _doCommit = commitSetting.equals(ScriptRunnerContext.AUTOCOMMITON);
						// don't need to capture and handle the exitcode because we are writing the
						// response file to the console.
						doLoad(null,null);
					} 
				} else if (length == 2) {
					//ctx.write(Messages.getString("LOAD_INCOMPLETE_CMD") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
					ctx.write(getHelp());
				} else if (length == 1) {
					ctx.write(getHelp());
				}
				return true;
			}
			return false;
		} catch (Exception e) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getLocalizedMessage());
			return true;
		}

	}
	
	private boolean validateFile(String fileName){
		_loadFile = new File(fileName);
		if (_loadFile.exists() && !_loadFile.isDirectory()){
			return true;
		}
		return false;
	}
	
	private boolean validateTable(String table){
		_schema=getObjectOwner(table);
		if (_schema!=null && !_schema.startsWith("\"") && !_schema.endsWith("\"")){ //$NON-NLS-1$ //$NON-NLS-2$
			_schema = _schema.toUpperCase();
		}
		_table=getObjectName(table);
		if (_table!=null && !_table.startsWith("\"") && !_table.endsWith("\"")){ //$NON-NLS-1$ //$NON-NLS-2$
			_table = _table.toUpperCase();
		}
		return true;
	}
	
	/**
	 * 
	 * @param object
	 * @return
	 */
	private String getObjectName(String object) {
		String[] names = object.split("\\.");	//$NON-NLS-1$
		if (names.length == 1) {
			return names[0];
		} else if (names.length == 2) {
			return names[1];
		} else {
			return object;
		}
	}

	/**
	 * 
	 * @param object
	 * @return
	 */
	private String getObjectOwner(String object) {
		String[] names = object.split("\\.");	//$NON-NLS-1$
		if (names.length == 1) {
			return null;
		} else if (names.length == 2) {
			return names[0];
		} else {
			return null;
		}
	}
	
	private ExitCode doLoad(ReadParmsDelimitedAPI readParms, LoadParmsAPI loadParms){

		LogHandler handler = null;
		try{

			// Create Temporary File
            _responseFile = File.createTempFile("sqlcl_load", ".tmp"); //$NON-NLS-1$ //$NON-NLS-2$
            _responseFile.deleteOnExit();
            
			_input = new FileInputStream(new File(_filename));
			_output = new FileOutputStream(_responseFile);
			//_output = (OutputStream)_ctx.getOutputStream();
			
			String msg = MessageFormat.format(Messages.getString("LOAD_START"),_schema+"."+_table,_filename); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			Logger.getLogger(getClass().getName()).log(Level.INFO,msg);

			LoadParmsAPI loadParms2 = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.responseFormat(LoadParmsAPI.RESPONSE_FORMAT.RESPONSE_SQL)
			.doCommit(_doCommit)
			.build();	
			
			
			LoadAPI loader = new LoadAPI.Builder(_conn,_schema,_table,_input,_output)
			.loadParms(loadParms2)
			.readParms(readParms)
			.logger(Logger.getLogger(getClass().getName()))
			.build();


			return loader.load();
		} catch (IllegalStateException ise){
			// standard error from constructing/starting load service 
			//System.out.println(ise.getMessage());
			return ExitCode.EXIT_SEVERE;
		} catch (Exception e){
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getStackTrace()[ 0 ].toString(), e);
			return ExitCode.EXIT_SEVERE;
		} finally {
			BufferedReader tmp=null;
			try {
				if (_input!=null){
					_input.close();
				}	
			} catch (Exception e1) {  
				Logger.getLogger(getClass().getName()).log(Level.WARNING, e1.getLocalizedMessage());
			}
			try {
				if (_output!=null){
					_output.close();
					tmp = new BufferedReader(new FileReader(_responseFile));
					String line;
					while ((line = tmp.readLine())!=null){
						_ctx.write(line+"\n");	//$NON-NLS-1$
					}
				}	
				
			} catch (Exception e) {  
				Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage());
			}

			try {	
				String msg = MessageFormat.format(Messages.getString("LOAD_END"),_schema+"."+_table,_filename); //$NON-NLS-1$ //$NON-NLS-2$ 
				Logger.getLogger(getClass().getName()).log(Level.INFO,msg);
				if (handler!=null){
					handler.close();
				}	

			} catch (Exception e) {  
				Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage());			
			} finally {
				try {
					if (tmp!=null){
						tmp.close();
					}
				} catch (Exception e){
					Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage());
				}
			}

		}
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public String[] getShowAliases() {
		return new String[] {getCommand()};
	}

	@Override
    public boolean needsDatabase() {
        return false;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }


	@Override
	public boolean needsConnection() {
		return true;
	}
	
}
