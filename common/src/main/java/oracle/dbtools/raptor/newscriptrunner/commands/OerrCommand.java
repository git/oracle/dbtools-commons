/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.util.StringTokenizer;

import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.utils.oerr.Oerr;
import oracle.dbtools.raptor.utils.oerr.OerrException;

/**
 * Command to expose OERR in the script runner.
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ShowTables.java"
 *         >Barry McGillin</a>
 *
 */
public class OerrCommand extends AForAllStmtsCommand implements IHelp{

	private static final String CMD = "oerr"; //$NON-NLS-1$
	private final static SQLCommand.StmtSubType s_cmdStmtSubType = SQLCommand.StmtSubType.G_S_OERR;

	public OerrCommand() {
		super(s_cmdStmtSubType);
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
		String loweredTrimmedSQL=cmd.getLoweredTrimmedSQL();
		if (loweredTrimmedSQL.startsWith(CMD)) {
			ctx.write("\n"); //$NON-NLS-1$
			try {
				Oerr oerr = new Oerr();
				String options = loweredTrimmedSQL.substring(CMD.length());
				if (options.length() > 0) {
					StringTokenizer st = new StringTokenizer(options);
					if (st.countTokens() > 2) {
						throw new OerrException(Messages.getString("OerrCommand.1")); //$NON-NLS-1$
					} else {
						if (st.countTokens() == 1) {
							ctx.write(oerr.oerr(options));
						} else {
							ctx.write(oerr.oerr(st.nextToken(), st.nextToken()));
						}
					}
				} else {
					ctx.write(getHelp()); //$NON-NLS-1$
				}
			} catch (OerrException oe) {
				ctx.write(oe.getLocalizedMessage()); //$NON-NLS-1$
			}
			ctx.write("\n"); //$NON-NLS-1$
			return true;
		}
		return false;
	}

	@Override
	public String getCommand() {
		return CMD.toUpperCase();
	}

	@Override
	public String getHelp() {
		return CommandsHelp.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return false;
	}
}
