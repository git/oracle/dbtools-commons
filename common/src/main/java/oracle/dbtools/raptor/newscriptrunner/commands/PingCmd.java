/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Logger;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.utils.TCPTNSEntry;
import oracle.dbtools.raptor.utils.TNSHelper;
import oracle.net.ns.NSProtocol;
import oracle.net.ns.SQLnetDef;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class PingCmd extends CommandListener implements IHelp {
  public static long UNKNOWN = -2;

  public static long DOWN = -1;

  private static Logger LOGGER = Logger.getLogger(PingCmd.class.getName());

  /*
   * HELP DATA
   */
  public String getCommand() {
    return "TNSPING";
  }

  public String getHelp() {
    return CommandsHelp.getString(getCommand());
  }

  @Override
  public boolean isSqlPlus() {
    return false;
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    // nothing to do
  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

  }

  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    if (matches("tnsping", cmd.getSql())) {
      String rawName = cmd.getSql().indexOf(" ") > 0 ? cmd.getSql().substring(cmd.getSql().indexOf(" ")).trim() : null;
      if (rawName != null){
        ctx.write(" ping:"+ ping(ctx,rawName)+ "ms\n");
      } else {
        ctx.write(getHelp());
      }
      return true;
    } else {
      return false;// nothing to do here.
    }
  }

  public static long ping(ScriptRunnerContext ctx,String tns) {
    long ret = UNKNOWN;
    String retMsg = null;
    long start = System.currentTimeMillis();
    Properties props = new Properties();
    props.put(SQLnetDef.TCP_CONNTIMEOUT_STR, "2000"); //$NON-NLS-1$
    // props.put(SQLnetDef.TCP_READTIMEOUT_STR,5000);
    //12.2 expects some props set
    props.put("oracle.net.networkCompression","off"); //$NON-NLS-1$  //$NON-NLS-2$
    props.put("oracle.net.networkCompressionThreshold","1024"); //$NON-NLS-1$  //$NON-NLS-2$
    props.put("oracle.net.networkCompressionLevels","(high)"); //$NON-NLS-1$  //$NON-NLS-2$
    props.put("oracle.net.DOWN_HOSTS_TIMEOUT","600"); //$NON-NLS-1$  //$NON-NLS-2$
    
    String translated = null;
    String urlString = tns;
    
    if (urlString.indexOf("jdbc") == 0) {//$NON-NLS-1$
    	String jdbc="";
        translated = urlString.substring(urlString.indexOf("@"));
    } else {
        translated =  tns;
        Iterator<TCPTNSEntry> it = TNSHelper.getTNSEntries().iterator();
    	while (it.hasNext()) {
    		TCPTNSEntry entry = it.next();
    		if (entry.getName().equalsIgnoreCase(tns)) {
    			 tns = entry.getHostname()+":"+entry.getPortno();
    			if (entry.getSid()!=null) {
    				tns = tns + ":" + entry.getSid();
    			} else {
    				tns = tns + "/" +entry.getServicename();
    			}
    			if (tns.length()>0) translated = tns;
    			break;
    		}
    	}
    }

    NSProtocol nsp = createProtocolInstance();
    if (nsp != null) {
      try {
        // Connect
        try {
          nsp.connect(translated, props);
        } catch (Exception e) {
          String msg = e.getLocalizedMessage();
          final int idx = msg.indexOf("ORA-"); //$NON-NLS-1$
          if (idx > 0) {
            msg = msg.substring(idx);
            retMsg = msg;
            ctx.write(msg + "\n");
          }
          ret = DOWN;
          throw e;
        }

        // Calculate Latency and disconnect
        try {
          ret = System.currentTimeMillis() - start;
          
        } finally {
          // Fix to prevent 11.1 XP database crash
        }
      } catch (Exception e) {
        LOGGER.finest(e.getMessage());
      } finally {
        if (nsp != null) {
          // Disconnect
          try {
            nsp.disconnect();
          } catch (Exception e) {
          }
        }
      }
    }
    return ret;
  }

  private static Class<? extends NSProtocol> s_protCls;

  private static NSProtocol createProtocolInstance() {
    if (s_protCls == null) {
      // In 12c, NSProtocol is abstract; we need to load a subclass.
      try {
        s_protCls = (Class<? extends NSProtocol>) Class.forName("oracle.net.ns.NSProtocolStream"); //$NON-NLS-1$
      } catch (ClassNotFoundException e) {
        // If we didn't find the subclass, then just use NSProtocol
        s_protCls = NSProtocol.class;
      }
    }

    NSProtocol prot = null;
    if (s_protCls != null) {
      try {
        prot = s_protCls.newInstance();
      } catch (Exception e) {
        LOGGER.severe(e.getMessage());
      }
    }

    return prot;
  }

}
