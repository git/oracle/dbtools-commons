/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.util.ArrayList;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptParser;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * Listens for Prompt Commands 
 * Ex:
 * PROMPT This is the start of the script
 * DOC This is an old style prompt syntax
 * 
 * 
 * The PROMPT command is handled by IPromptListeners which can be registered.
 * A default IPromptListener write the PROMPT message to the output, but other listeners can work with the prompt command as well.
 */
public class Prompt extends CommandListener {
	
	/**
	 * The listener interface for receiving PROMPT events.
	 * The class that is interested in processing a PROMPT
	 * event implements this interface, and the object created
	 * with that class is registered.
	 *
	 * @see IPromptEvent
	 */
	public interface IPromptListener {
		
		/**
		 * Consume. 
		 * Everytime a PROMPT command is executed, if this IPromptListener is executed, then the consume method is called with the correct parameters
		 *
		 * @param conn the connection
		 * @param ctx the ScriptRunnerContext
		 * @param cmd the ISQLCommand (PROMPT)
		 */
		void consume(Connection conn,ScriptRunnerContext ctx, ISQLCommand cmd);
	}

	/**
	 * Add a listener for PROMPT commands
	 *
	 * @param listener the IPromptListener
	 */
	public static void addListener(IPromptListener listener) {
		if(!listeners.contains(listener)){
			listeners.add(listener);
		}
	}

	/** The listeners. */
	static ArrayList<IPromptListener> listeners = new ArrayList<IPromptListener>();
	
	//Add the default IPromptListener which writes to the output
	{
		addListener(DefaultPromptListener.getInstance());
	}
	
	
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		return true;
	}

	/*
	 * Loops through the IPromptListeners and allows them to perform some action based on the PROMPT
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/* Not used, the IPromptListeners are invoked during the beginEvent, no need for anything to happen after the PROMPT command has run
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		for (IPromptListener listener : listeners) {
			listener.consume(conn, ctx, cmd);
		}
	}

	/**
	 * A default implementation of IPromptListener. 
	 * This writes the PROMPT message to the output
	 * @see DefaultPromptEvent
	 */
	protected static class DefaultPromptListener implements IPromptListener {
		
		/** The _instance.
		 * Use an instance as only unique listeners objects can be registered.
		 * Using an instance stops more than 1 DefaultPromptListener object to be registered*/
		static DefaultPromptListener _instance = null;
		
		/**
		 * Gets the single instance of DefaultPromptListener.
		 *
		 * @return single instance of DefaultPromptListener
		 */
		public static IPromptListener getInstance() {
			if(_instance == null){
				_instance = new DefaultPromptListener();
			}
			return _instance;
		}
		
		@Override
		public void consume(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
			String promptMessage = getPromptMessage(cmd,ctx);
			ctx.write(promptMessage);
		}

		/**
		 * Gets the prompt message. note that the command maybe PROMPT or PRO, PROM, DOC,DOCUMENT,... so best to identify the first space and take the message after
		 *
		 * @param cmd the cmd
		 * @param ctx 
		 * @return the prompt message
		 */
		protected String getPromptMessage(ISQLCommand cmd, ScriptRunnerContext ctx) {
			
			String cmdTrim=cmd.getSql().trim();
			String[] s = cmdTrim.split("\\s+");
			//remove first word
			String localSql = cmdTrim.substring(s[0].length(), cmdTrim.length()).trim();
			//Remove quotes if there and dont trim any more
			if (localSql.startsWith("\"")&&localSql.endsWith("\"")) {
				localSql=localSql.substring(1,localSql.length()-1);
			}
			if (cmd.getStmtType().equals(StmtType.G_C_OLDCOMMENT)) {
				String[] lines = cmd.getSql().split("\n");
				String document="";
				if(ctx.isEchoOn()) {
	                //Print blanks if they are in the script.
					if (cmd.getProperty(ScriptParser.NUMEMPTYLINESABOVE) != null
							&& Integer.parseInt(cmd.getProperty(ScriptParser.NUMEMPTYLINESABOVE).toString()) > 0) {
						int noslines = Integer.parseInt(cmd.getProperty(ScriptParser.NUMEMPTYLINESABOVE).toString());
						if (noslines > 0) {
							for (int i = 0; i < noslines; i++)
								document += ctx.getPrompt() + "\n";
						}
					}
					//First line is SQL
					if (lines.length>0) {
						document+=ctx.getPrompt()+lines[0] +"\n";
					for (int i=1; i< lines.length; i++) {					
						document +="DOC>"+lines[i] +"\n";
					}
					}
				} else {
					document += "DOC>#\n";
				}
				return document;
			} else {
				return localSql+"\n";	
			}					
		}
	}
}
