/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.util.Arrays;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptParser;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.utils.NLSUtils;

/**
 * @author srkrkris
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class RePopulateNLSMap extends CommandListener implements IShowCommand {

  private static final String SET_NLS_CMD = "setnls"; //$NON-NLS-1$

  private static final String SET_TIMEZONE_CMD = "altersessionsettime_zone"; //$NON-NLS-1$

  private static final String NLS_AUTO = "NLS_REPOPULATE";

  private static final String NLS_AUTO_PRE = "PRE";

  private static final String NLS_AUTO_POST = "POST";

  private static final String NLS_AUTO_DEFAULT = "DEFAULT";

  private static final String NLS_AUTO_NONE = "OFF";

  private static final String[] OPTIONS = { NLS_AUTO_PRE, NLS_AUTO_POST, NLS_AUTO_DEFAULT, NLS_AUTO_NONE };

  private static final String[] aliases = { "AUTONLS" };

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    // moved before the sql
    if (ctx.getProperty(NLS_AUTO) != null && ctx.getProperty(NLS_AUTO).equals(NLS_AUTO_PRE) && conn != null) {
      NLSUtils.populateNLS(ctx.getCurrentConnection());
    }

  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    if ((ctx != null) && (ctx.getProperty(ScriptRunnerContext.NOLOG) != null) 
        && (((Boolean) (ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))
        || ((ctx.getProperty(NLS_AUTO) != null && ctx.getProperty(NLS_AUTO).equals(NLS_AUTO_NONE)))) {
      return;
    }
    // assume strip comments is expensive so only call it when individual words
    // found.
    // looks like this class is not referenced anywhere so I cannot test it.
    String cmdlower = cmd.getLoweredTrimmedSQL();

    if (((ctx.getProperty(NLS_AUTO) != null && ctx.getProperty(NLS_AUTO).equals(NLS_AUTO_DEFAULT))) && (cmdlower.contains("alter") && cmdlower.contains("time_zone")) || cmdlower.contains("nls")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      cmdlower = ScriptUtils.stripFirstN(cmdlower, 9000, cmd.getProperty(SQLCommand.STRIPPED_CONTINUATION) == null, false);
      ScriptParser parser = new ScriptParser(cmdlower);
      String sql = parser.stripLine(cmdlower);
      if ((sql.indexOf("setnls") > 0) || (sql.startsWith(SET_TIMEZONE_CMD))) //$NON-NLS-1$
      {
        NLSUtils.populateNLS(conn);
      }
    } else if ( ctx.getProperty(NLS_AUTO) != null && ctx.getProperty(NLS_AUTO).equals(NLS_AUTO_POST)) {
      NLSUtils.populateNLS(conn);
    }
  }

  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    if (cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("setautonls")) {
      String[] parts = cmd.getSQLOrig().split(" ");
      if (parts.length != 3) {
        ctx.write(String.format(Messages.getString("NLS_AUTO"), "") + "\n");
        ctx.write(String.format(Messages.getString("NLS_AUTO_OPTIONS"), "") + "\n");
      } else {
        // PRE/POST/DEFAULT/NONE
        String option = parts[2].trim().toUpperCase();
        if (Arrays.asList(OPTIONS).contains(option)) {
          ctx.putProperty(NLS_AUTO, option);
        } else {
          ctx.write(String.format(Messages.getString("NLS_AUTO_OPTIONS"), "") + "\n");
        }
        return true;
      }
    }
    return false;
  }

  @Override
  public String[] getShowAliases() {
    return aliases;
  }

  @Override
  public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    ctx.write("Auto Reload NLS:" + ctx.getProperty(NLS_AUTO) + "\n");
    return true;
  }

  @Override
  public boolean needsDatabase() {
    return false;
  }

  @Override
  public boolean inShowAll() {
    return false;
  }

}
