/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunner;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * Demonstrate how to add some functionality Before and After an entire Script is run In this example. The start and end time are added to the script
 * output
 * 
 * @author dermot
 */
public class ScriptTimer extends CommandListener {
    Date m_startDate = null;
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$
    // all logging in this class can go through this
    private Logger m_logger = Logger.getLogger(getClass().getName());
    
    /**
     * Gets call at the start of every run script. This write out the Script Start time to the default output.
     * 
     * @param conn the database connection that will be used to run cmd against
     * @param ctx the ScriptRunnerContext, holds things like the BufferedOutput and worksheet editors
     * @param cmd the command to be run against the database, holds a stmtId and sql string to be run
     */
    public void beginScript(Connection conn, ScriptRunnerContext ctx) {
        try {
            m_startDate = new java.util.Date();
            if (ctx != null && ctx.getOutputStream() != null) {
                ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic((Messages.getString("ScriptTimer.1") + dateFormat.format(m_startDate) + "\n"))); //$NON-NLS-1$ //$NON-NLS-2$
                // force_print tells makes sure that the lines on the outputstream get written to the output window
                ctx.getOutputStream().write(ScriptRunner.FORCE_PRINT_BYTES);
            }
        } catch (IOException e) {
            // always at the very least log an exception, never ignore
            m_logger.log(Level.SEVERE, e.getStackTrace()[ 0 ].toString(), e);
        }
    }
    
    /**
     * Gets call at the end of every run script. This write out the Script End time to the default output.
     * 
     * @param conn the database connection that will be used to run cmd against
     * @param ctx the ScriptRunnerContext, holds things like the BufferedOutput and worksheet editors
     * @param cmd the command to be run against the database, holds a stmtId and sql string to be run
     */
    public void endScript(Connection conn, ScriptRunnerContext ctx) {
        try {
            Date endDate = new java.util.Date();
            if (ctx != null && ctx.getOutputStream() != null) {
                ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(
                   (Messages.getString("ScriptTimer.3") + dateFormat.format(m_startDate)))); //$NON-NLS-1$
                ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(
		   (Messages.getString("ScriptTimer.4") + dateFormat.format(endDate)))); //$NON-NLS-1$
                ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(
		   (Messages.getString("ScriptTimer.5") + (endDate.getTime() - m_startDate.getTime())+""))); //$NON-NLS-1$ //$NON-NLS-2$
            }
        } catch (IOException e) {
            // always at the very least log an exception, never ignore
            m_logger.log(Level.SEVERE, e.getStackTrace()[ 0 ].toString(), e);
        }
    }
    
    /**
     * Default Constructor
     */
    public ScriptTimer() {
    }
    
    /**
     * Has to be overriden as it is abstract in CommandListener. No processing required
     */
    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    /**
     * Has to be overriden as it is abstract in CommandListener. No processing required
     */
    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    /**
     * Returns false as the CommandListener does not handle any events
     */
    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        return false;
    }
}
