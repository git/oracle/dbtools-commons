/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLPLUS;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.jdbc.OracleConnection;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetAppinfo.java">Barry McGillin</a> 
 *
 */
public class SetAppinfo extends AForAllStmtsCommand {

    private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_APPINFO;
	public static String APPINFO=SQLPLUS.PRODUCT_NAME;

    public SetAppinfo() {
        super(m_cmdStmtSubType);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
            return false;
        }
        // on off and string check spool for other string example
        String input = (String)cmd.getProperty(ISQLCommand.PROP_STRING);
        if ((input!=null)&&(!(input.toLowerCase().equals("on")||(input.toLowerCase().equals("off"))))) { //$NON-NLS-1$  //$NON-NLS-2$
            //use the string - dont it is truncated if it has a space, need to remove \\s*[Ss][Ee][Tt]\\s++ from cmd.getSql
            input = cmd.getSql().trim().replaceFirst("^[Ss][Ee][Tt]\\s+[^\\s]+","").trim(); //$NON-NLS-1$
            ArrayList<Object> al = new ArrayList<Object>();
            if (input.startsWith("\"") && input.endsWith("\"") && (input.length()>1)) { //$NON-NLS-1$  //$NON-NLS-2$
                input=input.substring(1,input.length()-1);
            }
            if (input.startsWith("'") && input.endsWith("'") && (input.length()>1)) { //$NON-NLS-1$  //$NON-NLS-2$
                input=input.substring(1,input.length()-1);
            }
            if ((input==null)||(input.equals(""))) { //$NON-NLS-1$
                ctx.write(Messages.getString("APPINFONULL")); //$NON-NLS-1$
            } else if (input.length()>48 ){
            	ctx.write(MessageFormat.format(Messages.getString("APPINFO_MAX_STRING_ERROR"), input.substring(0,10) + "...")  + "\n"); //$NON-NLS-1$
            }
            else {
                al.add(input);
                DBUtil.getInstance(conn).execute("BEGIN DBMS_APPLICATION_INFO.SET_MODULE(:1,null); END;", al);  //$NON-NLS-1$
                ctx.putProperty(ScriptRunnerContext.APPINFOSTRING, input);
                APPINFO=input;
            }
        } else {
            if (cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN)!=null) {
                ctx.putProperty(ScriptRunnerContext.APPINFO,(Boolean)cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN));
                if ((Boolean)cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN).equals(new Boolean(false))) {
                    //set the appiinfo value to SQL Developer
                    ArrayList<Object> al = new ArrayList<Object>();
                    al.add(SQLPLUS.PRODUCT_NAME);
                    DBUtil.getInstance(conn).execute("BEGIN DBMS_APPLICATION_INFO.SET_MODULE(:1,null); END;", al);  //$NON-NLS-1$
                } else {
                    if ((Boolean)cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN).equals(new Boolean(true))) {
                        ArrayList<String> scrArrayList = (ArrayList<String>)ctx.getProperty(ScriptRunnerContext.APPINFOARRAYLIST);
                        if (scrArrayList.size()!=0) {
                            setAppinfo(ctx,ctx.getCurrentConnection(),
                                       scrArrayList.get(scrArrayList.size()-1),
                                       scrArrayList.size());
                        } else {
                            setAppinfo(ctx,ctx.getCurrentConnection(),
                                       APPINFO, //$NON-NLS-1$p
                                       0);
                        }
                    }
                }
            } else {
                return false;
            }
        }
        return true;
    }

    public static void setAppinfo(ScriptRunnerContext src, Connection conn, String scriptname, Integer level) {
        if ((src!=null) && (src.getProperty(ScriptRunnerContext.NOLOG)!=null) && (((Boolean)(src.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
            return;
        }
        if ((src.getCurrentConnection() instanceof OracleConnection)&&
                (src.getProperty(ScriptRunnerContext.APPINFO)!=null)&&
                (Boolean)(src.getProperty(ScriptRunnerContext.APPINFO)).equals(new Boolean(true))){
        	if (!(DBUtil.isOracleConnectionAlive(src.getCurrentConnection()))) {
        		return;
        	}
            DBUtil myDbUtil = DBUtil.getInstance(src.getCurrentConnection());
            Integer myI = level;
            ArrayList<Object> al = new ArrayList<Object>();
            al.add(new Integer(myI));
            al.add(scriptname);
            //call DBMS_APPLICATION_INFO.SET_MODULE cutting to 48 bytes if necessary
            if (level.equals(0)) {
                al = new ArrayList<Object>();
                al.add(APPINFO); 
                myDbUtil.execute("BEGIN DBMS_APPLICATION_INFO.SET_MODULE(:1,null);END;",al); //$NON-NLS-1$
            } else {
            myDbUtil.execute("DECLARE\n"+ //$NON-NLS-1$
                             "  inN numeric := :1;\n"+ //$NON-NLS-1$
                             "  --inN NUMERIC := 1;\n"+ //$NON-NLS-1$
                             "  inC varchar2(1000) := :2;\n"+ //$NON-NLS-1$
                             "  --inC  VARCHAR2(1000) := '1234567891123456789212345678931234567894123456789';\n"+ //$NON-NLS-1$
                             "  inNtoC NUMERIC        := inN;\n"+ //$NON-NLS-1$
                             "  inNC   VARCHAR2(1000) := inN;\n"+ //$NON-NLS-1$
                             "  toUse  VARCHAR2(1000);\n"+ //$NON-NLS-1$
                             "BEGIN\n"+ //$NON-NLS-1$
                             "  IF (inN<10) THEN\n"+ //$NON-NLS-1$
                             "    inNC:='0'||TO_CHAR(inN);\n"+ //$NON-NLS-1$
                             "  ELSE\n"+ //$NON-NLS-1$
                             "    inNC:=TO_CHAR(inN);\n"+ //$NON-NLS-1$
                             "  END IF;\n"+ //$NON-NLS-1$
                             "  --size is 48 bytes\n"+ //$NON-NLS-1$
                             "  toUse             :=(inNC||' '||inC);\n"+ //$NON-NLS-1$
                             "  \n"+ //$NON-NLS-1$
                             "  IF (lengthB(toUse))>48 THEN\n"+ //$NON-NLS-1$
                             "    DECLARE\n"+ //$NON-NLS-1$
                             "      p_work VARCHAR2(1000);\n"+ //$NON-NLS-1$
                             "      p_bsize NUMBER      := (48-lengthb((inNC||'<')));\n"+ //$NON-NLS-1$
                             "      v_work  VARCHAR2(1000);\n"+ //$NON-NLS-1$
                             "      v_bsize NUMBER(10);\n"+ //$NON-NLS-1$
                             "    BEGIN\n"+ //$NON-NLS-1$
                             "      p_work := inC;\n"+ //$NON-NLS-1$
                             "      IF LENGTHB(p_work) <= p_bsize THEN\n"+ //$NON-NLS-1$
                             "        inC              := p_work;\n"+ //$NON-NLS-1$
                             "      ELSE\n"+ //$NON-NLS-1$
                             "        v_work                              := p_work;\n"+ //$NON-NLS-1$
                             "        v_work                              := LTRIM(SUBSTRB(v_work, - p_bsize));\n"+ //$NON-NLS-1$
                             //p_bsize will always be > 1, cut off partial characters are replaced with
                             //blanks which are removed by the ltrim. inNC||'<'||null = 01< which is reasonable.
                             "        inC := v_work;\n"+ //$NON-NLS-1$
                             "      END IF;\n"+ //$NON-NLS-1$
                             "    END;\n"+ //$NON-NLS-1$
                             "    toUse:=(inNC||'<')||inC;\n"+ //$NON-NLS-1$
                             "  END IF;\n"+ //$NON-NLS-1$
                             "  DBMS_APPLICATION_INFO.SET_MODULE(toUse,null);\n"+ //$NON-NLS-1$
                             "END;", al); //$NON-NLS-1$
            }
        }
    }
}
