/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetAutoCommit.java">Barry McGillin</a> 
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4_ABORT_SCRIPT)
public class SetAutoCommit extends AForAllStmtsCommand {
    private final static SQLCommand.StmtSubType s_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_AUTOCOMMIT;
    
    //Usage: SET AUTO[COMMIT] {OFF|ON|IMM[EDIATE]|n}
    private static final String SET = "(?i:set)"; //$NON-NLS-1$
    private static final String AUTOCOMMIT = "(?i:auto(?:|c|co|com|comm|commi|commit))"; //$NON-NLS-1$
//    private static final String OBJECTNAME = SET +"\\s+" + AUTOCOMMIT + "\\s+"+"[^\\s]+\\s*[^\\s]+";  //$NON-NLS-1$
    private static final String OBJECTNAME = "(?i:set)\\s+(?i:auto(?:|c|co|com|comm|commi|commit))\\s+(on|off|imm|immediate|\\d+(\\s+|$))"; //$NON-NLS-1$
//    private static final String OBJECTNAME = SET +"\\s+" + AUTOCOMMIT + "\\s+"+"[on|off|imm|immediate|\\d]";  //$NON-NLS-1$
    private static final String LINE_SEPARATOR = System.getProperty(Messages.getString("SetAutoCommit.1")); //$NON-NLS-1$
    
    // private long start;
    public SetAutoCommit() {
        super(s_cmdStmtSubType);
    }
    @Override
    public void doBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        ctx.putProperty(ScriptRunnerContext.ERR_FOR_AUTOCOMMIT,Boolean.FALSE);
    }
    @Override
    public void doEndWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    	if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) &&
    			(((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
    		return;
    	}

    	try {
    			Long commitSetting = (Long) ctx.getProperty(ScriptRunnerContext.AUTOCOMMITSETTING);
    			if (commitSetting.equals(ScriptRunnerContext.AUTOCOMMITOFF) || commitSetting.equals(new Long(0L))) {
    				return;
    			}
    			/* Bug 21104192 - SET AUTOCOMMIT ON COMMITS ON EVERYTHING
            		if (commitSetting.equals(ScriptRunnerContext.AUTOCOMMITON)) {
                		doCommit(ctx);
                	return;
            		}
    			*/
    			switch (cmd.getStmtSubType()) {
    			case G_S_INSERT:
    			case G_S_UPDATE:
    			case G_S_DELETE:
    			case G_S_EXECUTE:
    			case G_S_BEGIN:
    				//main error flag does not get reset each statement.
    				if ((ctx.getProperty(ScriptRunnerContext.ERR_FOR_AUTOCOMMIT)!=null) && 
    						((Boolean) ctx.getProperty(ScriptRunnerContext.ERR_FOR_AUTOCOMMIT)).booleanValue()) {
    					break;
    				}

    				if (commitSetting.equals(ScriptRunnerContext.AUTOCOMMITON)) {
    					doCommit(ctx);
    					return;
    				} else {
    					Long commitValue = (Long) ctx.getProperty(ScriptRunnerContext.AUTOCOMMITCOUNTER);
    					commitValue++;
    					if (commitSetting.equals(commitValue)) {
    						ctx.putProperty(ScriptRunnerContext.AUTOCOMMITCOUNTER, new Long(0L));
    						doCommit(ctx);
    					} else {
    						ctx.putProperty(ScriptRunnerContext.AUTOCOMMITCOUNTER, commitValue);
    					}
    					return;
    				}
    			case G_S_SET_AUTOCOMMIT:
    			case G_S_COMMIT:
    			case G_S_ROLLBACK_PLUS:
    			case G_S_ROLLBACK_SQL:
    				//set autocommit does not fire this so repeating this 
    				//in the main autocommit code.
    				ctx.putProperty(ScriptRunnerContext.AUTOCOMMITCOUNTER,new Long(0L));
    				break;
    			}
 
    	} catch (Exception e) {
    		Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
    	}
    }
    
    private void doCommit(ScriptRunnerContext ctx) {
        Connection lockedConn = ctx.getCurrentConnection();
        if (lockedConn==null) {
            ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOT_CONNECTED)+LINE_SEPARATOR); //$NON-NLS-1$
            return;
        }
        boolean amILocked = false;
        try {
            if (amILocked=LockManager.lock(lockedConn)) {
                ctx.getCurrentConnection().commit();
                if (ctx.getFeedback() != ScriptRunnerContext.FEEDBACK_OFF) {
                    ctx.write(Messages.getString("COMMIT_COMPLETE")+LINE_SEPARATOR); //$NON-NLS-1$  //$NON-NLS-2$
                }
            }
        } catch (SQLException e) {
                ctx.write(Messages.getString("COMMIT_FAILED")+LINE_SEPARATOR);  //$NON-NLS-1$  //$NON-NLS-2$
                ctx.write(e.getLocalizedMessage()+LINE_SEPARATOR);  //$NON-NLS-1$
        } finally {
            if (amILocked) {
                try {
                    LockManager.unlock(lockedConn);
                } catch (Exception e) {
                }
            }
        }
        return;
    }
    @Override
    public boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    	if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && 
    			(((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
    		return false;
    	}
    	// Bug 21477124 - BAD USAGE FOR SET COMMAND GET NO ERRORS 
    	String[] extra = cmd.getSql().trim().split("\\s+"); //$NON-NLS-1$ //$NON-NLS-2$
    	if ( cmd.getSql().toLowerCase().matches(OBJECTNAME) ) {
    		
    		return doAutoCommitCommand(ctx, cmd, extra);
    	}
    	else {
    		if (extra.length==3) {
    			String x = extra[2];
    			if (Character.isDigit(x.charAt(0))) {
    				ctx.write(Messages.getString("SetAutoCommit.2")); //$NON-NLS-1$
    			} else {
    				try {
        				int a = Integer.parseInt(extra[2]);
    					if (a < 0 || a> 2000000000) {
    						ctx.write(String.valueOf(Messages.getString("SetAutoCommit.3"))); //$NON-NLS-1$
    						return true;
    					}
    				} catch (NumberFormatException e) {
        				ctx.write(MessageFormat.format(Messages.getString("SetAutoCommit.4"),"autocommit",extra[2])); //$NON-NLS-1$ //$NON-NLS-2$
            			ctx.write(Messages.getString("SETAUTOUSAGE")+LINE_SEPARATOR); //$NON-NLS-1$
            			return true;
    				}
    			}
    		} else if (extra.length>3) {
    			try {
    			int a = Integer.parseInt(extra[2]);
    			String[] sArr = cmd.getSql().split(" "); //$NON-NLS-1$
    		    String firstStrs = ""; //$NON-NLS-1$
    		    for(int i = 0; i < 3; i++)
    		        firstStrs += sArr[i] + " "; //$NON-NLS-1$
    		    cmd.setSql(firstStrs.trim());
    		    cmd.setModifiedSQL(firstStrs.trim());
    			doAutoCommitCommand(ctx, cmd, extra);
    			//ctx.write(MessageFormat.format("SP2-0158: unknown SET option \"{0}\"\n",extra[3]));
    			} catch (NumberFormatException e) {
        			ctx.write(MessageFormat.format(Messages.getString("SetAutoCommit.10"),"autocommit",extra[2])); //$NON-NLS-1$ //$NON-NLS-2$
        			ctx.write(Messages.getString("SETAUTOUSAGE")+LINE_SEPARATOR);    				 //$NON-NLS-1$
    			}
    		} else if (extra.length<3) {
    			ctx.write(Messages.getString("SetAutoCommit.13")); //$NON-NLS-1$
    			ctx.write(Messages.getString("SetAutoCommit.14")); //$NON-NLS-1$
    		}
    	}  
    	return true;
    }
	/**
	 * @param ctx
	 * @param cmd
	 * @param extra
	 * @param displayMsg
	 */
	private boolean doAutoCommitCommand(ScriptRunnerContext ctx, ISQLCommand cmd, String[] extra) {
    	boolean displayMsg = false;

		String propertyString = (String) cmd.getProperty(ISQLCommand.PROP_STRING);
		if (propertyString !=null ) {
			try {
				if (ctx.getCurrentConnection().getAutoCommit() == true) {
					ctx.write(Messages.getString("SCRIPT_AUTOCOMMIT_FORCED_OFF_CONNECTION_AUTOCOMMIT_ON")); //$NON-NLS-1$
					ctx.putProperty(ScriptRunnerContext.AUTOCOMMITSETTING,ScriptRunnerContext.AUTOCOMMITOFF);
					return true;
				}
			} catch (SQLException e) {
				Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
			}
			propertyString = propertyString.toLowerCase().trim();
			
			setProperties(cmd, ctx, ((Long) (ctx.getProperty(ScriptRunnerContext.AUTOCOMMITSETTING)) == -1 ) ? "IMMEDIATE" : "OFF", "autocommit", ScriptRunnerContext.AUTOCOMMITSETTING); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			if ((propertyString.toLowerCase().equals("imm")||propertyString.toLowerCase().equals("immediate")||(propertyString.startsWith("on")))) {//immediate  //$NON-NLS-1$  //$NON-NLS-2$ //$NON-NLS-3$
				writeShowMode(cmd, ctx, "IMMEDIATE"); //$NON-NLS-1$
				ctx.putProperty(ScriptRunnerContext.AUTOCOMMITSETTING,ScriptRunnerContext.AUTOCOMMITON);
			} else if (propertyString.toLowerCase().equals("off")) {  //$NON-NLS-1$
				writeShowMode(cmd, ctx, "OFF"); //$NON-NLS-1$
				ctx.putProperty(ScriptRunnerContext.AUTOCOMMITSETTING,ScriptRunnerContext.AUTOCOMMITOFF);
			} else {
				Long l;
				try {
					l = Long.parseLong(propertyString);
					if ((l >-1L)&&(l<2000000001L)) {
						ctx.putProperty(ScriptRunnerContext.AUTOCOMMITSETTING,l);
						ctx.putProperty(ScriptRunnerContext.AUTOCOMMITCOUNTER,0L);
					}else {
						ctx.write(MessageFormat.format(Messages.getString("AUTO_COMMIT_RANGE"), new Object[] {String.valueOf(l)})+LINE_SEPARATOR);  //$NON-NLS-1$  //$NON-NLS-2$
					}
					
					if ( extra.length > 3) { 
						if (extra[3].length()>10) {
		            		String fail=extra[3].substring(0, 10)+"..."; //$NON-NLS-1$
		                ctx.write(MessageFormat.format(Messages.getString("SETUNKNOWNOPTION_BEGINNING"), fail)); //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
		            	} else {
							 ctx.write(MessageFormat.format(Messages.getString("SETUNKNOWNOPTION"), new Object[] {extra[3]})); //$NON-NLS-1$
		            	}
					 displayMsg = true;
				    }
				} catch (NumberFormatException e){
					if ( Pattern.compile("^[A-Za-z]").matcher(propertyString).find()) { //$NON-NLS-1$
						if (!displayMsg )
							ctx.write(MessageFormat.format(Messages.getString(Messages.getString("SetAutoCommit.21"))+LINE_SEPARATOR, new Object[]{propertyString})); //$NON-NLS-1$
						ctx.write(Messages.getString("SETAUTOUSAGE")+LINE_SEPARATOR); //$NON-NLS-1$
					} else 
						ctx.write(Messages.getString("AUTO_COMMIT_NUMBER_FORMAT_EXCEPTION")+LINE_SEPARATOR);  //$NON-NLS-1$  //$NON-NLS-2$
				return false;
				}
			}
		}
		return true;
	}
}
