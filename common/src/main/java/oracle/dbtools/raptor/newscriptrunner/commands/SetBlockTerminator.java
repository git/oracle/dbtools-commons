/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.StringTokenizer;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetBlockTerminator.java">Barry McGillin</a> 
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class SetBlockTerminator extends AForAllStmtsCommand {
	private String replaceMe = "^\"|^\'|\"$|\'$";
	private String withblank = "";
    private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_BLO;

    public SetBlockTerminator() {
    	super(m_cmdStmtSubType);
    }
    

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.AForAllStmtsCommand#doHandleCmd(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	protected boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		  if (cmd.getProperty(ISQLCommand.PROP_STRING) != null) {
	            String str = (String) cmd.getSql();
	            String[] tokens = str.split("\\s+"); //$NON-NLS-1$
	            if (tokens.length!=3) {
	            	ctx.write(Messages.getString("SetBlockTerminator.1")); //$NON-NLS-1$
	            } else {
	            	String token = tokens[2];
	            	if (token.toLowerCase().equals("on")||token.toLowerCase().equals("off")) { //$NON-NLS-1$ //$NON-NLS-2$
	            		showModeForBlockTerminator(ctx, cmd, token);
	            		ctx.putProperty("BLOCKTERMINATOR", token); //$NON-NLS-1$
	            	}
	            	else if (isAlphaChar(token)) {
	            		ctx.write(Messages.getString("SetBlockTerminator.5")); //$NON-NLS-1$
	            	} else {
	            		if (token.replaceAll(replaceMe, withblank).length()>1) {
	        				ctx.write(MessageFormat.format(Messages.getString("SetBlockTerminator.6"),token)); //$NON-NLS-1$
	            		} else {
	            			showModeForBlockTerminator(ctx, cmd, token);
	            			ctx.putProperty("BLOCKTERMINATOR", token.replaceAll(replaceMe, withblank)); //$NON-NLS-1$
	            		}
	            	}
	            }

		  }
		  return true;
	}
	private boolean isAlphaChar(String seq) {
		String seq1= seq.replaceAll(replaceMe, withblank);
	    int len = seq1.length();
	    for(int i=0;i<len;i++) {
	        char c = seq1.charAt(i);
	        // Test for all positive cases
	        if('0'<=c && c<='9') continue;
	        if('a'<=c && c<='z') continue;
	        if('A'<=c && c<='Z') continue;
	        if(c==' ') continue;
	        // ... insert more positive character tests here
	        // If we get here, we had an invalid char, fail right away
	        return false;
	    }
	    // All seen chars were valid, succeed
	    return true;
	}
	
	private void showModeForBlockTerminator(ScriptRunnerContext ctx, ISQLCommand cmd, String termin) {
		String  btermin = null;
		if ( ctx.getProperty("BLOCKTERMINATOR") != null ) { //$NON-NLS-1$
			btermin = ctx.getProperty("BLOCKTERMINATOR").toString();
			if (btermin.equalsIgnoreCase("on")) { //$NON-NLS-1$
				btermin = "\"" +"."+ "\"" + " (hex "+String.format("%02x", (int) '.')+")"; //$NON-NLS-1$
			} else if (btermin.equalsIgnoreCase("off")) { //$NON-NLS-1$
				btermin = "OFF"; //$NON-NLS-1$
			} else {
				char [] charary = btermin.toCharArray();
				btermin = "\"" +btermin+ "\"" + " (hex "+String.format("%02x", (int) charary[0])+")"; //$NON-NLS-1$
			}
		} else {
			btermin = "\"" +"."+ "\"" + " (hex "+String.format("%02x", (int) '.')+")"; //$NON-NLS-1$
		}	
		
		setProperties(cmd, ctx, btermin, "blockterminator", "BLOCKTERMINATOR"); //$NON-NLS-1$
		
		if (termin.equalsIgnoreCase("on")) { //$NON-NLS-1$
			termin = "\"" +"."+ "\"" + " (hex "+String.format("%02x", (int) '.')+")"; //$NON-NLS-1$
		} else if (termin.equalsIgnoreCase("off")) { //$NON-NLS-1$
			termin = "OFF"; //$NON-NLS-1$
		} else {
			char [] charary = termin.toCharArray();
			termin = "\"" +termin+ "\"" + " (hex "+String.format("%02x", (int) charary[0])+")"; //$NON-NLS-1$
		}
		writeShowMode(cmd, ctx, termin);
	}
}
