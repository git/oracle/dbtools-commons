/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetCloseCursor.java">
 *         Barry McGillin</a>
 *
 */
public class SetCloseCursor extends CommandListener implements IShowCommand {
	String[] aliases = { "closecursor", "closecurso","closecurs","closecur" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	@Override
	public String[] getShowAliases() {
		return aliases;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (!ctx.isSQLPlusClassic()) {
			ctx.write(MessageFormat.format(Messages.getString("SetSecuredCol.0"), cmd.getSql())); //$NON-NLS-1$
			return true;
		}
		if (ctx.getProperty(ScriptRunnerContext.SET_CLOSECURSOR) != null) {
			String on = "OFF"; //$NON-NLS-1$
			boolean x = (boolean) ctx.getProperty(ScriptRunnerContext.SET_CLOSECURSOR);
			if (x)
				on = "ON"; //$NON-NLS-1$
			else
				on = "OFF"; //$NON-NLS-1$
			ctx.write(MessageFormat.format(Messages.getString("SetCloseCursor.7"), new Object[] { on })); //$NON-NLS-1$
		}
		return true;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return false;
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (!ctx.isSQLPlusClassic()) {
			ctx.write(MessageFormat.format(Messages.getString("SetSecuredCol.0"), cmd.getSql())); //$NON-NLS-1$
			return true;
		}
		String sql = cmd.getSql();
		String[] tokens = sql.split("\\s+"); //$NON-NLS-1$
		if (tokens.length != 3) {
			ctx.write(Messages.getString("SetCloseCursor.9"));  //$NON-NLS-1$
		} else {
			String token = tokens[2];
			if (token.toLowerCase().equals("on") || token.toLowerCase().equals("off")) { //$NON-NLS-1$ //$NON-NLS-2$
				boolean x = token.toLowerCase().equals("on") ? Boolean.TRUE : Boolean.FALSE; //$NON-NLS-1$
				ctx.putProperty(ScriptRunnerContext.SET_CLOSECURSOR, x); // $NON-NLS-1$
			} else {
				ctx.write(Messages.getString("SetCloseCursor.0")); //$NON-NLS-1$
			}
		}
		return true;
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

}