/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;

/**
 * @author Turloch O'Tierney 16609 23-02-2015
 * see set escape
 */
public class SetConcat extends AForAllStmtsCommand {
    private static final String CONCAT = "set con"; //$NON-NLS-1$
    private static final String CONCAT_ON = "on"; //$NON-NLS-1$
    private static final String CONCAT_OFF = "off"; //$NON-NLS-1$
    private static final String[] aliases= {"concat", "conca", "conc", "con"};  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
    
    private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_CONCAT;

    public SetConcat() {
    	super(m_cmdStmtSubType);
    }
    
    @Override
    public boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        String sql = cmd.getSql().trim().toLowerCase();
        if (sql.endsWith(";")) { //$NON-NLS-1$
            sql = sql.substring(0, sql.length() - 1);
        }
        if ((sql.startsWith("set con ")||sql.startsWith("set conc ")||sql.startsWith("set conca ")||sql.startsWith("set concat ")) && !sql.substring(sql.lastIndexOf(" ")).trim().equals(CONCAT_OFF) ) { //$NON-NLS-1$
            String toggle = sql.substring(sql.lastIndexOf(" ")).trim(); //$NON-NLS-1$
            Character escapeChar;
            if (sql.replaceFirst(CONCAT+"[^\\s]*","").trim().equals("")) { //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
                ctx.write(Messages.getString("NO_ESCAPE_VAL")); /* not escape specific */ //$NON-NLS-1$
                return true;//warning printed.
            } else {
                if (!toggle.equals(CONCAT_ON)) {
                    String checkValue = validString(toggle, ctx);
                    if (checkValue == null) {
                        return true;//warning printed in validString
                    }
                    escapeChar = checkValue.charAt(0);;
                } else {
                    escapeChar='.'; 
                }
            }
            showModeForConcat(ctx, cmd, "on", escapeChar);
            ctx.setSubstitutionTerminateChar(escapeChar);
            return true;
        } else if (sql.startsWith(CONCAT)) {
            //off
        	showModeForConcat(ctx, cmd, "off", null);
            ctx.setSubstitutionTerminateChar(null);
            return true;
        } else {
            return false;
        }
    }
    /**
     * check for quoted and unquoted valid set escape parameter
     * @param toTestIn the string eg '#' "#" ! # but not a-z or A-Z or 0-9 or whitespace
     * @return the core string or null if not valid
     */
    public String validString(final String toTestIn, final ScriptRunnerContext ctx) {
        String toTest = toTestIn;
        if ((toTest == null) || (toTest.equals(""))) {//$NON-NLS-1$
            ctx.write(Messages.getString("NO_ESCAPE_VAL")); /* not escape specific */ //$NON-NLS-1$
            return null;
        }
        if ((toTest.equals("\"\"\"\""))||(toTest.equals("'\"'"))) {//$NON-NLS-1$ //$NON-NLS-2$
            return "\"";//$NON-NLS-1$
        }
        if ((toTest.equals("''''"))||(toTest.equals("\"'\""))) {//$NON-NLS-1$ //$NON-NLS-2$
            return "'";//$NON-NLS-1$
        }
        toTest=removeQuotesString(toTest);
        if (toTest.length()>1) {
            ctx.write(MessageFormat.format(Messages.getString("ONE_CHARACTER"), toTest)); //$NON-NLS-1$ //
            return null;
        }
        if (toTest.equals("") || toTest.equals("\"") || toTest.equals("'")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            ctx.write(Messages.getString("INVALID_ESCAPE")); //$NON-NLS-1$ //
            return null;
        }
        if (toTest.matches("^\\s") ||  (toTest.matches("^\\w") //$NON-NLS-1$ //$NON-NLS-2$
                    &&(!toTest.startsWith("_"))) || toTest.matches("^\\d") //$NON-NLS-1$ //$NON-NLS-2$ 
            ) {
           ctx.write(Messages.getString("NOT_ALHA_OR_WHITE")); //$NON-NLS-1$
           return null;
        }
        return toTest.substring(0,1);
    }
    /**
     * strip[ 'f' to f and "f" to f
     * @param toBeStripped
     * @return
     */
    public String removeQuotesString(String toBeStripped) {
        if (toBeStripped.startsWith("'") && toBeStripped.endsWith("'") && toBeStripped.length()>=2) { //$NON-NLS-1$ //$NON-NLS-2$ 
            return toBeStripped.substring(1, toBeStripped.length() - 1);
        }
        if (toBeStripped.startsWith("\"") && toBeStripped.endsWith("\"") && toBeStripped.length()>=2) { //$NON-NLS-1$ //$NON-NLS-2$ 
            return toBeStripped.substring(1, toBeStripped.length() - 1);
        }
        return toBeStripped;
    }

    private void showModeForConcat(ScriptRunnerContext ctx, ISQLCommand cmd, String onoff, Character newConcatchar) {
    	String  btermin = null;
    	String  termin = null;
    	
    	if ( ctx.getSubstitutionTerminateChar() != null ) { //$NON-NLS-1$
			 btermin = "\"" +ctx.getSubstitutionTerminateChar()+ "\"" + " (hex "+String.format("%02x", (int) ctx.getSubstitutionTerminateChar())+")"; //$NON-NLS-1$
		} else {
				btermin = "OFF"; //$NON-NLS-1$
		}
		
		setProperties(cmd, ctx, btermin, "concat", "concat"); //$NON-NLS-1$
		
		if ( newConcatchar != null ) { //$NON-NLS-1$		
			if (onoff.equalsIgnoreCase("on")) { //$NON-NLS-1$
				termin = "\"" +newConcatchar+ "\"" + " (hex "+String.format("%02x", (int) newConcatchar)+")"; //$NON-NLS-1$
			} else if (onoff.equalsIgnoreCase("off")) { //$NON-NLS-1$
				termin = "OFF"; //$NON-NLS-1$
			}
		} else {
			if (onoff.equalsIgnoreCase("on")) { //$NON-NLS-1$
				termin = "\"" +"."+ "\"" + " (hex "+String.format("%02x", (int)  '.')+")"; //$NON-NLS-1$
			} else if (onoff.equalsIgnoreCase("off")) { //$NON-NLS-1$
				termin = "OFF"; //$NON-NLS-1$
			}
		}
		writeShowMode(cmd, ctx, termin);
	}
}
