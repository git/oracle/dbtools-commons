/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowPrefixNameNewline;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetDDLSettings.java"
 *         >Barry McGillin</a>
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class SetDDLSettings extends CommandListener implements IHelp, IShowCommand, IShowPrefixNameNewline {
  private static final Object DDL = "ddl";

  private static final Object ON = "on";

  private static final Object OFF = "off";

 

  /*
   * (non-Javadoc)
   * 
   * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#
   * getShowCommand()
   */
  @Override
  public String[] getShowAliases() {
    return new String[] { "DDL" };
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#handle
   * (java.sql.Connection,
   * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
   * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
   */
  @Override
  public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	   if (ctx.getDDLOptions() !=null) { 
			 Set<String> options =  ctx.getDDLOptions().keySet();
			 if (ctx.getDDLOptions().get("DEFAULT").equals("ON")) {
				 ctx.write("DEFAULT : ON\n");
			 } else {
				 for (String option: options) {
					 if (!option.equals("DEFAULT")) {
						 ctx.write(MessageFormat.format("{0} : {1}\n", option, ctx.getDDLOptions().get(option)));
					 }
				 }
			 }
			   
		   }
		return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see oracle.dbtools.raptor.newscriptrunner.IHelp#getCommand()
   */
  @Override
  public String getCommand() {
    return "DDL";
  }

  /*
   * (non-Javadoc)
   * 
   * @see oracle.dbtools.raptor.newscriptrunner.IHelp#getHelp()
   */
  @Override
  public String getHelp() {
    return CommandsHelp.getString(getCommand());
  }

  /*
   * (non-Javadoc)
   * 
   * @see oracle.dbtools.raptor.newscriptrunner.IHelp#isSqlPlus()
   */
  @Override
  public boolean isSqlPlus() {
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java
   * .sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
   * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
   */
  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("setddl")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    String[] cmds = cmd.getSQLOrig().toLowerCase().split("\\s+");
    int length = cmds.length;
    if (length == 3 && cmds[1].toLowerCase().equals(DDL) && cmds[2].toLowerCase().equals(OFF)) {
      String defaultsql = "begin dbms_metadata.set_transform_param(dbms_metadata.session_transform,'DEFAULT',true); end;";
      DBUtil dbUtil = DBUtil.getInstance(conn);
      dbUtil.execute(defaultsql);
      ctx.setDDLOptions("DEFAULT", "ON");
      ctx.write("DDL default generation reset\n");
      return true;
    }
    if (length == 4) {
      if (cmds[1].toLowerCase().equals(DDL) && (cmds[3].toLowerCase().equals(ON) || cmds[3].toLowerCase().equals(OFF))) {
        String on = cmds[3].toLowerCase().equals(ON) ? "true" : "false";
        String option = cmds[2].toUpperCase();
        // if (Arrays.asList(TransformOptions).contains(option)) {
        String optionSQL = "";
        if (on.equals("true"))
          optionSQL = "begin dbms_metadata.set_transform_param(dbms_metadata.session_transform,:OPTION,true); end;";
        else
          optionSQL = "begin dbms_metadata.set_transform_param(dbms_metadata.session_transform,:OPTION,false); end;";
        Map<String, String> binds = new HashMap<String, String>();
        binds.put("OPTION", option);
        try {
          DBUtil dbUtil = DBUtil.getInstance(conn);
          
          dbUtil.execute(optionSQL, binds);
          ctx.write(MessageFormat.format("DDL Option {0} {1}\n", option, cmds[3].toLowerCase()));
          ctx.setDDLOptions(option, cmds[3].toUpperCase());
          if (cmds[3].toUpperCase().equals("OFF")) {
        	  ctx.setDDLOptions("DEFAULT", "OFF");
          }
        } catch (Exception e) {
          ctx.write("Not a valid transform option\n");

        }
      }
      return true;
    }
		}
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java
   * .sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
   * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
   */
  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql
   * .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
   * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
   */
  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
  }

  @Override
  public boolean needsDatabase() {
    return false;
  }

  @Override
  public boolean inShowAll() {
    return false;
  }
}
