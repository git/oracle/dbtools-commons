/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptParser;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;

/**
 * 
 * @author <a href=
 *         "mailto:ramesh.uppala@oracle.com@oracle.com?subject=SetDescribe.java">
 *         Ramesh Uppala</a>
 *  
 */

// Syntax : DESCRIBE [DEPTH {1|n|ALL}][LINENUM {ON|OFF}][INDENT {ON|OFF}]
// example : SET DESCRIBE DEPTH 2 LINENUM ON INDENT ON

public class SetDescribe extends AForAllStmtsCommand {
	private final static SQLCommand.StmtSubType s_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_DESCRIBE;
	private static final String SET = "set"; //$NON-NLS-1$ 
	private static final String DESCRIBE = "(?i:desc(?:|r|ri|rib|ribe))"; //$NON-NLS-1$ 
	private static final String JAVAREGEX ="[\\w|\\$]+\\s+[\\w|$]+|[\\w|\\$]+\\s+[\\w|$]+\\s+[\\w|\\$]+\\s+[\\w|$]+|[\\w|\\$]+\\s+[\\w|$]+\\s+[\\w|\\$]+\\s+[\\w|$]+\\s+[\\w|\\$]+\\s+[\\w|$]+";

	private static final String COMMAND_NAME = SET +"\\s+"+ DESCRIBE;  //$NON-NLS-1$
	private static final String COMMAND_DEPTH = "DEPTH";
	private static final String COMMAND_LINENUM = "LINENUM";
	private static final String COMMAND_INDENT = "INDENT";
	
	private int m_depthNum = 1; // default
	private boolean m_indent = false, m_linenum = false;
	
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");
	
	// private long start;
	public SetDescribe() {
		super(s_cmdStmtSubType);
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		super.beginEvent(conn,ctx,cmd); 
	}

	@Override
	public boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		
		String setPtrn = "(?i:set\\b\\s*)";
		String describePtrn = "(?i:desc(?:|r|ri|rib|ribe)\\b\\s*)";
		String descStmt = cmd.getCompoundSetcmd();
		if ( descStmt == null ) {
			ctx.write(Messages.getString("SETDESCRIBE_USAGE")+"\n"); //$NON-NLS-1$
			return false;
		}
		String rawName = ((descStmt.replaceFirst(setPtrn, "").trim()).replaceFirst(describePtrn, "").trim());

		if ( !rawName.matches(JAVAREGEX) ) {
			ctx.putProperty("descr.usage", Boolean.TRUE);
			ctx.write(Messages.getString("SETDESCRIBE_USAGE")+"\n"); //$NON-NLS-1$
			return false;
		} 
		
		List<String> commandList = new ArrayList<String>();
		StringTokenizer strToken = new StringTokenizer(rawName);
		while (strToken.hasMoreElements()) {
			commandList.add((String)strToken.nextElement());
		}

		// Iterate through the command list
		if (commandList.isEmpty() || commandList.size() < 2 ) {
			// ToDO : throw error 
		} else {
			String depthValue, linenum_value, indent_value;
			for (int i = 0; i < commandList.size(); i++) {
				String command = commandList.get(i);
				if ( command.equalsIgnoreCase(COMMAND_DEPTH) ) {
					depthValue = commandList.get(++i);
					if ( depthValue.equalsIgnoreCase("ALL") ) {
						setDepthNum(50);
					} else {
						int value = Integer.parseInt(depthValue);
						if ( value > 0 && value <= 50)
							setDepthNum(Integer.parseInt(depthValue));
						else {
							ctx.write(Messages.getString("SETDESCRIBE_DEPTH_INVALID")+"\n"); //$NON-NLS-1$
						}
					}	
					
				} else if  ( command.equalsIgnoreCase(COMMAND_LINENUM) ) {
					linenum_value = commandList.get(++i);
					if ( linenum_value.equalsIgnoreCase("ON") ) {
						setLinenum(true);
					} else if ( linenum_value.equalsIgnoreCase("OFF") ){
						setLinenum(false);
					} else {
						ctx.write(Messages.getString("SETDESCRIBE_USAGE")+"\n"); //$NON-NLS-1$
					}
					
				} else if ( command.equalsIgnoreCase(COMMAND_INDENT) ) {
					indent_value = commandList.get(++i);
					if ( indent_value.equalsIgnoreCase("ON") ) {
						setIndent(true);
					} else if ( indent_value.equalsIgnoreCase("OFF") ){
						setIndent(false);
					} else {
						ctx.write(Messages.getString("SETDESCRIBE_USAGE")+"\n"); //$NON-NLS-1$
					}
				} else {
					// ToDO : throw error
				}
		    }
			setDescribeValuesInProperty(ctx);
		}
		return true;
	}
	
	
	private void setDescribeValuesInProperty(ScriptRunnerContext ctx ) {

		HashMap<String, Object> properties = new HashMap();
		properties.put(COMMAND_DEPTH, new Integer(getDepthNum()));
		properties.put(COMMAND_LINENUM, new Boolean(isLinenum()));
		properties.put(COMMAND_INDENT, new Boolean(isIndent()));
		
		ctx.putProperty(ScriptRunnerContext.SETDESCRIBE, properties);
	}
	
	public int getDepthNum() {
		return m_depthNum;
	}

	public void setDepthNum(int m_depthNum) {
		this.m_depthNum = m_depthNum;
	}

	public boolean isIndent() {
		return m_indent;
	}

	public void setIndent(boolean m_indent) {
		this.m_indent = m_indent;
	}

	public boolean isLinenum() {
		return m_linenum;
	}

	public void setLinenum(boolean m_linenum) {
		this.m_linenum = m_linenum;
	}

}



