/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.datatypes.oracle.plsql.BOOLEAN;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowRegistry;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.newscriptrunner.SQLPLUS;
import oracle.dbtools.raptor.newscriptrunner.SQLStatementTypes;
import oracle.dbtools.raptor.newscriptrunner.ScriptParser;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetEcho.java">
 *         Barry McGillin</a>
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class SetEcho extends AForAllStmtsCommand implements IStoreCommand{
	private final static SQLCommand.StmtSubType s_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_ECHO;
	private static int padding = 2;

	// private long start;
	public SetEcho() {
		super(s_cmdStmtSubType);
	}

	@Override
	protected boolean isListenerOn(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// need thie sqitch even when begin watcher would not otherwise be
		// called.
		if (this.isCmdOn() != ctx.isEchoOn()) {
			// we are out of which follow context - we do this a lot -
			// performance?
			this.setCmdOn(ctx.isEchoOn());
		}
		return m_isCmdOn;
	}

	public static void coreBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String theDebug=System.getenv("SQLCL_ISCLOSED");
		try {
			if ((conn!=null)&&(theDebug!=null)&&(!theDebug.equals(""))) {
				Boolean noLog=(Boolean)ctx.getProperty(ScriptRunnerContext.NOLOG);
				if (noLog==null||(noLog.equals(Boolean.FALSE))) {
		 		ctx.putProperty("SQLCL_ISCLOSED",conn.isClosed());
				}
			}
		} catch (Exception e) {
			//delibrerately skipped
		}
		if (ctx.isEchoOn()) {
			//first set the script depth up. This is handy when calculating prompts later
			Object depthObj = ctx.getProperty(ScriptRunnerContext.SCRIPT_DEPTH);
			Integer lastScriptDepth= (Integer) ctx.getProperty(ScriptRunnerContext.ECHOSCRIPTDEPTH);
			if (lastScriptDepth==null) {
				lastScriptDepth=0;
			}
			int scriptDepthChange = 0;
			if(depthObj !=null){
				int depth = (Integer)depthObj;
				if(depth < lastScriptDepth){
					scriptDepthChange = lastScriptDepth - depth;
				}
				lastScriptDepth = depth;
			}
			//see if this is a call to a sub script. required because the called script may not have any contents, but we want to take the call into account
			if(cmd.getStmtSubType()==StmtSubType.G_S_ATNESTED || cmd.getStmtSubType()==StmtSubType.G_S_AT){
				lastScriptDepth = lastScriptDepth + 1;
			}
			ctx.putProperty(ScriptRunnerContext.ECHOSCRIPTDEPTH, lastScriptDepth);
	
			Boolean isSlashStatement=(Boolean) ctx.getProperty(ScriptRunnerContext.ISSLASHSTATEMENT);		
		
			if (!ctx.isSQLPlusSilent() && ctx.isEchoOn()&&((isSlashStatement==null||isSlashStatement.equals(Boolean.FALSE)))&&   
					!cmd.getStmtType().equals(StmtType.G_C_OLDCOMMENT)) {
				String prompt = "> ";
				if (ctx.getPrompt() != null && ctx.getPrompt().length() > 0) {
					prompt = ctx.getPrompt();
					if (prompt.toString().indexOf("@|") >= 0 && prompt.toString().indexOf("|@") > 0 ) {
						prompt = prompt.replaceAll("@\\|\\w+\\s?", "");
						prompt = prompt.replaceAll("\\|@", "");
					};
				}
				try {
					String toprint = "";
					if (cmd.getProperty(ScriptParser.NUMEMPTYLINESABOVE) != null
							&& Integer.parseInt(cmd.getProperty(ScriptParser.NUMEMPTYLINESABOVE).toString()) > 0) {
						int lines = Integer.parseInt(cmd.getProperty(ScriptParser.NUMEMPTYLINESABOVE).toString());
						if (lines > 0) {
							for (int i = 0; i < lines; i++)
								toprint += prompt + "\n";
						}
					}
					// if command line only want to push to spool if toplevel is
					// true
		
					String[] bits = cmd.getSQLOrigWithTerminator().split("\n");
					if (cmd.getStmtType().equals(StmtType.G_C_SQL) || cmd.getStmtType().equals(StmtType.G_C_PLSQL)) {
						toprint = getEchoStatement(toprint, bits, cmd, ctx, prompt);
					} else if (cmd.getStmtType().equals(StmtType.G_C_SQLPLUS)){
						toprint = getEchoStatement(toprint, bits, cmd, ctx, prompt, true);
						/* always greaterThan > for second prompt in sqlplus. TODO use greaterthan prompt for all continuation '-' instead of number */
					} else if (cmd.getStmtType().equals(StmtType.G_C_MULTILINECOMMENT)){
						toprint = getEchoComments(toprint,bits,cmd,ctx,prompt);
					} else {
						toprint = getEchoOtherStatements(toprint, bits, cmd, ctx, prompt);
					}
					 
					if(ctx.isOutputSupressed() && scriptDepthChange != 0){
						StringBuffer sb = new StringBuffer();
						for(int i = 1;i<=scriptDepthChange;i++){
							sb.append(prompt);
						}
						toprint = sb.toString() + toprint;
					}
					if (ctx.isCommandLine()) {
		
						if (ctx.getTopLevel()) {
							if (isStdIn(ctx) && isSQLPlus(ctx)) {
								if (cmd.isSqlPlusSetCmd() && cmd.isSetCompoundCmd() ) {
									echoCompoundSetCommands(ctx, cmd, prompt);
								} else {
									ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(toprint)); // $NON-NLS-1$
									ctx.getOutputStream().flush();
								}
							}
							ctx.getOutputStream().flush();// skip any trimspool
															// weirdness.
							BufferedOutputStream spool = (BufferedOutputStream) ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
							if (spool != null) {
								if (cmd.isSqlPlusSetCmd() && cmd.isSetCompoundCmd() ) {
									echoCompoundSetCommands(ctx, cmd, prompt);
								} else {
									spool.write(ctx.stringToByteArrayForScriptRunnerNonStatic(toprint)); // $NON-NLS-1$
								}
							}
						} else {
							// push to both if toplevel is false
							if (cmd.isSqlPlusSetCmd() && cmd.isSetCompoundCmd() ) {
								echoCompoundSetCommands(ctx, cmd, prompt);
							} else {
								ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(toprint)); // $NON-NLS-1$
								ctx.getOutputStream().flush();
							}
						}
					} else {// SetSpool handles this when SetEcho is not on and its
							// the Top Level. This handles it when Set Echo is on at
							// any level
						if (cmd.isSqlPlusSetCmd() && cmd.isSetCompoundCmd() ) {
							echoCompoundSetCommands(ctx, cmd, prompt);
						} else {
							ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(toprint)); // $NON-NLS-1$
							ctx.getOutputStream().flush();
						}
					}
				} catch (IOException e) {
					Logger.getLogger(SetEcho.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
				}
			}
		}
	}

	@Override
	public void doBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		//Made static and done before substitution coreBeginWatcher(conn,ctx,cmd);
	}

	private static String getEchoComments(String toprint, String[] bits, ISQLCommand cmd, ScriptRunnerContext ctx, String prompt) {
		String printme = toprint;
		//First line is SQL
		if (bits.length>0) {
			toprint+=ctx.getPrompt()+bits[0] +"\n";
			String docPrompt="SQL>";
			if (ctx.getProperty(ScriptRunnerContext.SET_SQLPLUSCOMPATIBILITY)!=null&&ctx.getProperty(ScriptRunnerContext.SET_SQLPLUSCOMPATIBILITY).toString().equalsIgnoreCase("9.2.0")) {
				docPrompt = "DOC>";
			}
			for (int i=1; i< bits.length; i++) {					
				toprint +=docPrompt+bits[i] +"\n";
			}
		}
		return toprint;
	}

	/**
	 * Another method which tries to highlight the changes we make on SQL*Plus
	 * emulations This one is specifically for At files
	 * 
	 * @param toprint
	 * @param bits
	 * @param cmd
	 * @param ctx
	 * @param prompt
	 * @return
	 */
	private static String getEchoOtherStatements(String toprint, String[] bits, ISQLCommand cmd, ScriptRunnerContext ctx, String prompt) {
		String echoString = toprint;
		if ((cmd.getStmtSubType().equals(StmtSubType.G_S_AT)
				||cmd.getStmtSubType().equals(StmtSubType.G_S_EXIT)) 
		   && isSQLPlus(ctx) && isStdIn(ctx)) {
			echoString += prompt + prompt + cmd.getSQLOrigWithTerminator() + "\n";
		} else {
			for (String bit : bits) {
				echoString += prompt + bit + "\n";
			}
		}
		return echoString;
	}

	/**
	 * getEchoStatement is a private method which will print the statement
	 * differently if we are from stdin vs normal mode. This is solely for
	 * sqlplus testing compliance.
	 * 
	 * @param toprint
	 * @param bits
	 * @param cmd
	 * @param ctx
	 * @param prompt
	 * @return
	 */
	private static String getEchoStatement(String toprint, String[] bits, ISQLCommand cmd, ScriptRunnerContext ctx, String prompt) {
		return getEchoStatement(toprint, bits, cmd, ctx, prompt, false);
	}
	/**
	 * 
	 * @param toprint
	 * @param bits
	 * @param cmd
	 * @param ctx
	 * @param prompt
	 * @param greaterThan = true for continuation character - used for sqlplus commands (should be all continuation lines)
	 * @return
	 */
	private static String getEchoStatement(String toprint, String[] bits, ISQLCommand cmd, ScriptRunnerContext ctx, String prompt, boolean greaterThan) {
		String echoString = toprint;
		if (isStdIn(ctx) && isSQLPlus(ctx)) {
			// SQLPLus in std in always adds a second prompt. Only want this one
			// in classic mode.
			echoString += prompt;
			int x = 1;
			for (String bit : bits) {
				if (x == 1)
					echoString += prompt + bit;
				else 
					echoString += getIndex(x, greaterThan);
				x++;
			}

		} else {
			int x = 1;
			for (String bit : bits) {
				if (x == 1)
					echoString += prompt + bit;
				else 
					echoString += getIndex(x, greaterThan) + bit;
				if (x < bits.length)
					echoString += "\n";
				x++;
			}
		}
		if ((!(isStdIn(ctx) && isSQLPlus(ctx)))&&((echoString!=null)&&(!(echoString.endsWith("\n"))))) {
			//want next write to be on a new line (unless we are not printing the bits..)
			echoString+="\n";
		}
		//An incomplete command, needs to echo out the empty line that terminated it.
		if(!cmd.isComplete() && (cmd.getEndLine()-cmd.getStartLine()>=bits.length)){
			if (greaterThan) {
				echoString = echoString + "  "+">\n";
			} else {
				echoString = echoString + "  "+(bits.length +1)+"\n";
			}
		}
		return echoString;
	}

	/**
	 * Check if we are running from stdin. (if we finished and continue on the
	 * cmd line, the flag wont be null but will be set to false to signify it
	 * was used but is now off
	 * 
	 * @param ctx
	 *            {@link ScriptRunnerContext}
	 * @return {@link Boolean}
	 */
	private static boolean isStdIn(ScriptRunnerContext ctx) {
		if (ctx != null && ctx.getProperty(ScriptRunnerContext.SQLCLI_STDIN) != null
				&& Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLCLI_STDIN).toString()) 
				//if we are in execute file then we are in normal territory, bug out....
				&& !(ctx.getProperty(ScriptRunnerContext.SQLPLUS_EXECUTE_FILE)!=null 
				&& Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_EXECUTE_FILE).toString()))) {
			return true;
		}
		return false;
	}

	private static boolean isSQLPlus(ScriptRunnerContext ctx) {
		if (ctx != null && ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) != null
				&& Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
			return true;
		}
		return false;
	}

	private static String getPad() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < padding; i++) {
			sb.append(" ");
		}
		return sb.toString();
	}

	public static String getIndex(int lineNumber, boolean greaterThan) {
		if (greaterThan) {
			return "> ";
		}
		return getFiller(lineNumber) + lineNumber + getPad();
	}

	private static String getFiller(int lineNumber) {
		String padding = "";
		int numChars = (lineNumber + padding).length();
		if (numChars < 3) {
			if (numChars == 1) {
				return "  ";
			} else {
				return " ";
			}
		}
		return "";
	}

	@Override
	public boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// have to watch myself
		setProperties(cmd, ctx, (ctx.isEchoOn() ? "ON" :"OFF"), "echo", "echo");
		if (ctx.isEchoOn()) {
			doBeginWatcher(conn, ctx, cmd);
		} 
		writeShowMode(cmd, ctx, ((Boolean) cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN) ? "ON" :"OFF") );
		ctx.putProperty(ScriptRunnerContext.EXPLICITSETECHO, Boolean.TRUE);
		ctx.setEcho((Boolean) cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN));
		return true;
	}
	
	private static void echoCompoundSetCommands(ScriptRunnerContext ctx, ISQLCommand cmd, String prompt) {
		try {
		if (cmd.getCompoundSetcmd()!= null) {
			ctx.write(prompt); // $NON-NLS-1$
			ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(cmd.getCompoundSetcmd())); // $NON-NLS-1$
			if (cmd.getStatementTerminator() != null && !cmd.getCompoundSetcmd().endsWith(cmd.getStatementTerminator()) ) {
				ctx.write(cmd.getStatementTerminator());
            }
			ctx.write("\n"); // $NON-NLS-1$
			ctx.getOutputStream().flush();
		} 
		} catch (IOException e) {
			Logger.getLogger(SetEcho.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.AForAllStmtsCommand#doEndWatcher(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	protected void doEndWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String theDebug=System.getenv("SQLCL_ISCLOSED");
try {
	if ((conn!=null)&&(theDebug!=null)&&(!theDebug.equals(""))) {
		Boolean noLog=(Boolean)ctx.getProperty(ScriptRunnerContext.NOLOG);
		if (noLog==null||(noLog.equals(Boolean.FALSE))) {
			boolean newSetting=conn.isClosed();
			Boolean oldSetting=(Boolean)ctx.getProperty("SQLCL_ISCLOSED");
			if (oldSetting!=null&&oldSetting.equals(Boolean.FALSE)&&newSetting==true) {
				ctx.write("SQLCL_ISCLOSED ZZ:"+ctx.getSourceRef()+":"+cmd.getStartLine()+":"+cmd.getSql());
			}
 		ctx.putProperty("SQLCL_ISCLOSED",newSetting);
		}
	}
} catch (Exception e) {
	//delibrerately skipped
}
		if (ctx.isSQLPlusClassic() && wasStdinOn(ctx,cmd) && cmd.getStmtSubType().equals(StmtSubType.G_S_AT)) {
			ctx.write(ctx.getPrompt()+ctx.getPrompt());
		}
	}

	private boolean wasStdinOn(ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (ctx.getProperty(ScriptRunnerContext.SQLCLI_STDIN_WAS_ON)!=null && Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLCLI_STDIN_WAS_ON).toString())&& ctx.getProperty(ScriptRunnerContext.SCRIPT_DEPTH)!=null && (Integer) ctx.getProperty(ScriptRunnerContext.SCRIPT_DEPTH)==1) {
			return true;
		} 
		return false;
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		if (ctx.isEchoOn()) {
			return StoreRegistry.getCommand("echo","on");
		} 
		return StoreRegistry.getCommand("echo","off");
	}

	
	
}
