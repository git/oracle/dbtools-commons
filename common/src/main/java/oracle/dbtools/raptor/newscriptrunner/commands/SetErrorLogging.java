/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Stack;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.util.Logger;

/**
 * SET ERRORL[OGGING] {ON | OFF} [TABLE [schema.]tablename] [TRUNCATE]
 * [IDENTIFIER identifier] Turns SQL*Plus error logging ON or OFF. Error logging
 * records SQL, PL/SQL and SQL*Plus errors and associated parameters in an error
 * log table. You can then query the log table to review errors resulting from a
 * query. When error logging is ON, errors are recorded whether the query is run
 * interactively or from a script. This is particularly useful for capturing
 * errors generated from long running queries and avoids capturing all output
 * using the SPOOL command, or having to remain present during the run.
 * 
 * By default, errors are written to a the table SPERRORLOG in your schema. If
 * this table does not exist, it is created automatically. You can also use the
 * TABLE schema.tablename option to specify other tables to use. When using a
 * table other than SPERRORLOG, it must already exist, and you must have access
 * to it. See Creating a User Defined Error Log Table.
 * 
 * If an internal error occurs, to avoid recursion errors caused by the errorlog
 * calling itself, errorlogging is automatically set OFF.
 * 
 * Error logging is set OFF by default.
 * 
 * ON
 * 
 * Writes ORA, PLS and SP2 errors to the default table, SPERRORLOG.
 * 
 * OFF
 * 
 * Disables error .
 * 
 * TABLE [schema.]tablename
 * 
 * Specifies a user defined table to use instead of the default, SPERRORLOG. If
 * you omit schema. the table is created in the current schema. The table you
 * specify must exist, and you must have access permissions.
 * 
 * If the table specified does not exist, or you do not have access, an error
 * message is displayed and the default table, SPERRORLOG, is used.
 * 
 * TRUNCATE
 * 
 * Clears all existing rows in the error log table and begins recording errors
 * from the current session.
 * 
 * IDENTIFIER identifier
 * 
 * A user defined string to identify errors. You can use it to identify errors
 * from a particular session or from a particular version of a query.
 * 
 * @author bamcgill
 *
 */
public class SetErrorLogging extends AForAllStmtsCommand implements IHelp, Connected {

	private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_ERROR_LOGGING;
	private static final String TRUNCATE = "truncate";
	private static final String TABLE = "table";
	private static final String IDENTIFIER = "identifier";
	private static final String SP2_1507 = "SP2-1507: Errorlogging table, role or privilege is missing or not accessible if its not\n";
	private static final String SP2_1509 = "SP2-1509: Invalid option for SET ERRORLOGGING ON\n";
	private static final String SP2_1508 = "SP2-1508: Invalid option for SET ERRORLOGGING OFF\n";
	private static final String SP2_0640 = "SP2-0640: Not connected\n";
	private static String defaultErrorTableName = "SPERRORLOG";

	private static String spErrorLogDDl = "CREATE TABLE SPERRORLOG ( \"USERNAME\" VARCHAR2(256),\"TIMESTAMP\" TIMESTAMP (6),"
			+ "	\"SCRIPT\" CLOB,  \"IDENTIFIER\" VARCHAR2(256)," + "	\"MESSAGE\" CLOB, \"STATEMENT\" CLOB" + "   ) ";
	private static String TABLEEXISTSSQL = "SELECT TABLE_NAME FROM ALL_TABLES WHERE UPPER(OWNER)=UPPER(:OWNER) AND UPPER(TABLE_NAME)=UPPER(:TABLENAME)";
	private static String TABLETRUNCATE = "TRUNCATE TABLE {0}.{1}";
	private boolean cmdState;
	private String errorTableName = "";
	private boolean errorTableTruncate = false;
	private String errorIdentifier = "";
	private int nosParams = 0;

	public SetErrorLogging() {
		super(m_cmdStmtSubType);
	}

	@Override
	protected boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		/*
		 * So we check for the sperrorlog table if no table is defined. If it is
		 * found, we use it. If not, we create it if we can. (manage errors) If
		 * tablename is used, then the table needs to exist. throw SP2-1507:
		 * Errorlogging table, role or privilege is missing or not accessible if
		 * its not. if truncate is used, truncate the table for me if identifier
		 * is used, then preface the session with insert identifier.
		 */
		if (cmd.getSql().toLowerCase().contains("errorl")) {
			boolean on = (boolean) ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING);
			try {
				if (conn != null && !conn.isClosed()) {
					boolean parseCommand = parseCommand(ctx, cmd.getSql());
					if (parseCommand) {
						if (!cmdState && nosParams > 3) {
							// we dont expect any parameters for errorlogging  off
							ctx.write(SP2_1508);
							return true;
						}
						if (!cmdState) {
							//Switch it off
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING, Boolean.FALSE);
							ctx.removeProperty(ScriptRunnerContext.ERROR_LOGGING_IDENTIFIER);
							ctx.removeProperty(ScriptRunnerContext.ERROR_LOGGING_SCHEMA);
							ctx.removeProperty(ScriptRunnerContext.ERROR_LOGGING_TABLE);
							ctx.removeProperty(ScriptRunnerContext.ERROR_LOGGING_DISPLAY_SCHEMATABLE);
							return true;
						}
						//Ok, errorl is being switched on, now lets process the parts.
						if (errorIdentifier.length() > 0) {
							// If this is set, add it.
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING_IDENTIFIER, errorIdentifier);
						} else if (on) {
							// if its on already, leave it as it is.
						}else {
							//reset it
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING_IDENTIFIER, "");
						}
						
						String schema = conn.getSchema();
						String displaySchemaTable=errorTableName;
						if (errorTableName.contains(".")){
							//If there is ., there the errorlog is in another schema.
							schema=errorTableName.substring(0,errorTableName.indexOf('.'));
							errorTableName=errorTableName.substring(errorTableName.indexOf('.')+1);
						}
						if (!on) { 
							if (errorTableName.equals("")) {
								errorTableName=defaultErrorTableName;
							}
						}
						HashMap<String, String> binds = new HashMap<String, String>();
						binds.put("OWNER", schema);
						binds.put("TABLENAME", errorTableName);
						String tablename = DBUtil.getInstance(conn).executeReturnOneCol(TABLEEXISTSSQL, binds);
						if (tablename == null && errorTableName.equalsIgnoreCase(defaultErrorTableName)) {
							// If its the default table SPERRORLOG, we can
							// create it.
							DBUtil.getInstance(conn).execute(spErrorLogDDl);
							// If there was a problem with these, then punt and
							// return.
							if (DBUtil.getInstance(conn).getLastException() != null) {
								ctx.write(SP2_1507);
								return true;
							}
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING, Boolean.TRUE);
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING_TABLE, defaultErrorTableName);
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING_SCHEMA, conn.getSchema());
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING_DISPLAY_SCHEMATABLE, displaySchemaTable);
						} else if (tablename != null && tablename.equalsIgnoreCase(errorTableName)) {
							// Errorlog table exists. Move along. Noting to do.
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING, Boolean.TRUE);
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING_TABLE, tablename);
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING_SCHEMA, schema.toUpperCase());
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING_DISPLAY_SCHEMATABLE, displaySchemaTable);
						} else if (on) {
							// move on we're keeping the table name
						}else {
							// ok, some other name has been suggested and, it
							// doesnt exist or we cant see it.
							ctx.write(SP2_1507);
							// Now, if its on, switch it off as we just
							// encountered a bad table.
							ctx.putProperty(ScriptRunnerContext.ERROR_LOGGING, Boolean.FALSE);
							return true;
						}
						
						if (errorTableTruncate) {
							String tschema = conn.getSchema();
							if (ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_SCHEMA)!=null) {
								tschema=ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_SCHEMA).toString();
							}
							String sql = MessageFormat.format(TABLETRUNCATE, tschema, ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_TABLE));
							DBUtil.getInstance(conn).execute(sql);
						}
					} else {
						// Ok, bad options. punt and return
						if (cmdState) {
							ctx.write(SP2_1509);
							ctx.write(getHelp());
						} else {
							ctx.write(SP2_1508);
						}
					}
				} else {
					ctx.write(SP2_0640);
				}
			} catch (SQLException e) {
				Logger.fine(this.getClass(), e);
			}
			resetParams();
			return true;
		} else {
			resetParams();
			return false;
		}
	}

	private boolean parseCommand(ScriptRunnerContext ctx, String sql) {
		// SET ERRORL[OGGING] {ON | OFF} [TABLE [schema.]tablename] [TRUNCATE] [IDENTIFIER identifier]
		boolean on = (boolean) ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING);
		String[] tokens = sql.split("\\s+");
		Stack<String> st = new Stack<String>();
		for (int i = tokens.length - 1; i >= 0; i--) {
			st.add(tokens[i]);
		}
		for (int i = 0; i < tokens.length; i++) {
			String token = null;
			switch (i) {
			case 0:
			case 1:
				st.pop();
				break;

			case 2:
				token = st.pop();
				if (tokens[2].equalsIgnoreCase("on")) {
					cmdState = true;
				} else if (tokens[2].equalsIgnoreCase("off")) {
					cmdState = false;
				} else {
					return false;
				}
				break;
			default:
				token = st.pop();
				if (token.equalsIgnoreCase(TABLE) && !st.isEmpty()) {			
					errorTableName = st.pop();
					i++;
				} else if (token.equalsIgnoreCase(TRUNCATE)) {
					errorTableTruncate = true;
				} else if (token.equalsIgnoreCase(IDENTIFIER) && !st.isEmpty()) {
					errorIdentifier = st.pop();
					i++;
				} else {
					return false;
				}
				break;
			}
		}

		return true;
	}

	@Override
	public boolean needsConnection() {
		return true;
	}

	@Override
	public String getCommand() {
		return "SETERRORL";
	}

	@Override
	public String getHelp() {
		String help = "SET ERRORL[OGGING] {ON | OFF} [TABLE [schema.]tablename] [TRUNCATE] [IDENTIFIER identifier]\n";
		return help;
	}

	@Override
	public boolean isSqlPlus() {
		return true;
	}

	private void resetParams() {
		errorTableName = "";
		errorTableTruncate = false;
		errorIdentifier = "";
	}
}
