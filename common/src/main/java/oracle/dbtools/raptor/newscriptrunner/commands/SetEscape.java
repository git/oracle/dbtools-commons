/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetEscape">Barry McGillin</a>
 */
public class SetEscape extends CommandListener {
	private static final String ESCAPE = "escape"; //$NON-NLS-1$
	private static final String ESCAPE_ON = "on"; //$NON-NLS-1$
	private static final String ESCAPE_OFF = "off"; //$NON-NLS-1$

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String sql = cmd.getSql().trim().toLowerCase();
		String[] tokens = sql.split("\\s+");
		if (tokens.length < 3) {
			ctx.write(Messages.getString("NO_ESCAPE_VAL")); //$NON-NLS-1$
			return true;// warning printed.
		} else if (tokens.length != 3) {
			ctx.write(Messages.getString("NO_ESCAPE_VAL")); //$NON-NLS-1$
			return true;// warning printed.
		} else {
			if (tokens[1].equalsIgnoreCase(ESCAPE) && tokens[2].equalsIgnoreCase(ESCAPE_OFF)) {
				ctx.setEscapeChar('\0');
				ctx.setEscape(false);
			} else if (tokens[1].equalsIgnoreCase(ESCAPE) && tokens[2].equalsIgnoreCase(ESCAPE_ON)) {
				ctx.setEscapeChar('\\');
				ctx.setEscape(true);
			} else {
				String checkValue = validString(tokens[2], ctx);
				if (checkValue == null) {
					return true;// warning printed in validString
				}
				ctx.setEscapeChar(checkValue.charAt(0));
				ctx.setEscape(true);
			}
			return true;
		}

	}

	/**
	 * check for quoted and unquoted valid set escape parameter
	 * 
	 * @param toTestIn
	 *            the string eg '#' "#" ! # but not a-z or A-Z or 0-9 or whitespace
	 * @return the core string or null if not valid
	 */
	public String validString(final String toTestIn, final ScriptRunnerContext ctx) {
		String toTest = toTestIn;
		if ((toTest == null) || (toTest.equals(""))) {//$NON-NLS-1$
			ctx.write(Messages.getString("NO_ESCAPE_VAL")); //$NON-NLS-1$
			return null;
		}
		if ((toTest.equals("\"\"\"\"")) || (toTest.equals("'\"'"))) {//$NON-NLS-1$ //$NON-NLS-2$
			return "\"";//$NON-NLS-1$
		}
		if ((toTest.equals("''''")) || (toTest.equals("\"'\""))) {//$NON-NLS-1$ //$NON-NLS-2$
			return "'";//$NON-NLS-1$
		}
		toTest = removeQuotesString(toTest);
		if (toTest.length() > 1) {
			ctx.write(MessageFormat.format(Messages.getString("ONE_CHARACTER"), toTest)); //$NON-NLS-1$ //
			return null;
		}
		if (toTest.equals("") || toTest.equals("\"") || toTest.equals("'")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			ctx.write(Messages.getString("INVALID_ESCAPE")); //$NON-NLS-1$ //
			return null;
		}
		if (toTest.matches("^\\s") || (toTest.matches("^\\w") //$NON-NLS-1$ //$NON-NLS-2$
				&& (!toTest.startsWith("_"))) || toTest.matches("^\\d") //$NON-NLS-1$ //$NON-NLS-2$
		) {
			ctx.write(Messages.getString("NOT_ALHA_OR_WHITE")); //$NON-NLS-1$
			return null;
		}
		return toTest.substring(0, 1);
	}

	/**
	 * strip[ 'f' to f and "f" to f
	 * 
	 * @param toBeStripped
	 * @return
	 */
	public String removeQuotesString(String toBeStripped) {
		if (toBeStripped.startsWith("'") && toBeStripped.endsWith("'") && toBeStripped.length() >= 2) { //$NON-NLS-1$ //$NON-NLS-2$
			return toBeStripped.substring(1, toBeStripped.length() - 1);
		}
		if (toBeStripped.startsWith("\"") && toBeStripped.endsWith("\"") && toBeStripped.length() >= 2) { //$NON-NLS-1$ //$NON-NLS-2$
			return toBeStripped.substring(1, toBeStripped.length() - 1);
		}
		return toBeStripped;
	}
}
