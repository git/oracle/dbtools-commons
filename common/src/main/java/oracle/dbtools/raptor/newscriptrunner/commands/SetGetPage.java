/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * @author klrice
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class SetGetPage extends AForAllStmtsCommand {
    public static final String GET_PAGE = "declare nlns number;\n" + " buf_t varchar2(32767);\n" + " lines htp.htbuf_arr;\n" + "begin\n" + "  nlns := ?;\n" + "  OWA.GET_PAGE(lines, nlns);\n" + "  if (nlns < 1) then\n" + "   buf_t := null;\n" + "  else \n" + "   for i in 1..nlns loop\n" + "     buf_t:=buf_t||lines(i);\n" + "   end loop;\n" + "  end if;\n" + "  ? := buf_t; ? := nlns;\n" + "end;"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$
    public static final String RESET = "begin " + " DBMS_SESSION.reset_package; " + " end;"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_GETPAGE;
    
    /**
     * @param cmd
     */
    public SetGetPage() {
        super(m_cmdStmtSubType);
    }
    
    @Override
    public void doEndWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && 
                (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
            return;
        }
        if (cmd.getStmtClass() == SQLCommand.StmtType.G_C_PLSQL || cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQL) {
            try {
                ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(getDataBlock(conn)));
                if (cmd.getStmtClass() == SQLCommand.StmtType.G_C_PLSQL || cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQL) {
                    CallableStatement stmt = null;
                    try {
                        stmt = conn.prepareCall(RESET);
                        stmt.execute();
                    } catch (SQLException e) {
                        try {
                            ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(("Error calling DBMS_SESSION.reset_package;:" + e.getMessage()))); //$NON-NLS-1$
                        } catch (IOException e1) {
                        }
                    } finally {
                        if (stmt != null) {
                            try {
                                stmt.close();
                            } catch (SQLException se) {
                            }
                        }
                    }
                }
            } catch (IOException e) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            } catch (SQLException e) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            }
        }
    }
    
    /**
     * Fetch a block of data from the OWA and return it as a StringBuffer; size of each piece of generated page is 128x256 bytes change this value
     * according to max size of generated page and HTBUF_LEN HTBUF_LEN = 256 (in htp public spec)
     */
    private String getDataBlock(Connection conn) throws SQLException {
        CallableStatement cs = null;
        String parcialContent;
        try {
            int maxLines = 128;
            int nlines = maxLines;
            cs = conn.prepareCall(GET_PAGE);
            cs.setInt(1, nlines);
            cs.registerOutParameter(2, Types.VARCHAR);
            cs.registerOutParameter(3, Types.BIGINT);
            cs.execute();
            nlines = cs.getInt(3);
            // Signal that fetch is initiated
            if (nlines < maxLines) {
                // Signal end-of-fetch
                if (nlines < 1) {
                    return (""); //$NON-NLS-1$
                }
            }
            parcialContent = cs.getString(2);
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (Exception e) {
                }
            }
        }
        return (parcialContent);
    }
}
