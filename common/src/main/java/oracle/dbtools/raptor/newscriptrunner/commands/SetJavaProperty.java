/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.logging.Logger;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * @author klrice
 * This added support in the worksheet for : set property XYZ  ABC
 * It will set the System property and print the new and old values
 *
 */
public class SetJavaProperty extends CommandListener {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    
    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        String[] parts = cmd.getSQLOrig().split("\\s+"); //$NON-NLS-1$
        if ( parts.length == 4 && parts[1].equalsIgnoreCase("property") ){
                String old = System.getProperty(parts[2]);
                if (old!=null) old=" (" + old + ")\n"; else old="\n";
                String message = MessageFormat.format(Messages.getString("ChangeLoggerLevel.1"), parts[ 2 ] ,parts[ 3 ] + old); //$NON-NLS-1$ ChangeLoggerLevel.1=Setting {0} to {1}
                logger.info(message);
                ctx.write(message);
                System.setProperty(parts[2], parts[3]);
                return true;
        }
        return false;
    }
}
