/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.net.NetCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;



/**
 * set null string. 
 * @author turloch *
 */
public class SetNetOverwrite extends AForAllStmtsCommand implements IShowCommand{
    private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_NETOVERWRITE;
    
    public SetNetOverwrite() {
        super(m_cmdStmtSubType);
    }
    @Override
    protected boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if (cmd.getProperty(ISQLCommand.PROP_STRING) != null) {
            String in = (String) cmd.getSql();
            in=in.trim().replaceAll("^[sS][eE][tT]\\s+[nN][oO][vV][eE][rR][wW][rR][iI][tT][eE]\\s+","").trim();  //$NON-NLS-1$  //$NON-NLS-2$
            if ((in.startsWith("\""))&&(in.endsWith("\""))&&(in.length()>1)) {  //$NON-NLS-1$  //$NON-NLS-2$
                in=in.substring(1,in.length()-1);
            } else 
                if ((in.startsWith("'"))&&(in.endsWith("'"))&&(in.length()>1)) {  //$NON-NLS-1$  //$NON-NLS-2$
                    in=in.substring(1,in.length()-1);
                }
            if (in.equalsIgnoreCase(NetCommand.ON)) {
                in=NetCommand.ON;
            } else if (in.equalsIgnoreCase(NetCommand.OFF)) {
                in=NetCommand.OFF;  
            } else if (in.equalsIgnoreCase(NetCommand.WARN)) {
                in=NetCommand.WARN;
            } else {
                ctx.write("ERROR:set noverwrite on|off|warn\n"); //$NON-NLS-1$ 
                return true;
            }
            ctx.putProperty(ScriptRunnerContext.SETNETOVERWRITE, in);
        }
        return true;
    }
    @Override
    public String[] getShowAliases() {
      return new String[]{"noverwrite"}; //$NON-NLS-1$ 
    }
    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        String val=(String) ctx.getProperty(ScriptRunnerContext.SETNETOVERWRITE);
        if (val==null) {
            val=NetCommand.WARN;
        }
        ctx.write("noverwrite: "+val+"\n"); //$NON-NLS-1$  //$NON-NLS-2$ 
        return true;
    }
    @Override
    public boolean needsDatabase() {
        // TODO Auto-generated method stub
        return false;
    }
    @Override
    public boolean inShowAll() {
        // TODO Auto-generated method stub
        return false;
    }
}
