/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.sql.NUMBER;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;

public class SetNumberformat extends AForAllStmtsCommand {
	private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_NUMBERFORMAT;
	private static final String m_lineSeparator = System.getProperty("line.separator"); //$NON-NLS-1$
	// Regular Expression based on http://yourmachine.yourdomain/12/121/server.121/e18404/ch_twelve013.htm#BABBDHHE for
	// Number formats.
	private static final String NUMFMTREGEXP = "^(((?i:b|c|d|g|l|s|u|v)?([09]+((\\.|\\,|\\$)[09]*)?))|" //$NON-NLS-1$
											+ "([09]+(((\\.|\\,|\\$)[09]*)|(?i:b|c|d|g|l|s|u|v)|" //$NON-NLS-1$
											+ "(?i:pr|mi))?)|([09]+((?i:b|c|d|g|l|s|u|v)?[09]*)?)|" //$NON-NLS-1$
											+ "([09]+(((\\.)[09]+)?(?i:pr|mi)))|" //$NON-NLS-1$
											+ "((\\.|\\$)[09]+)|(?i:rn)|(?i:tm(9|e)?)|" //$NON-NLS-1$
											+ "([09]+((\\.)[09]*)?(?i:e){4})|" //$NON-NLS-1$
											+ "([09]*(?i:x)+)|" //$NON-NLS-1$
											+ "((\\$)?[09\\,]+(\\.[09]+)?)|)$"; //$NON-NLS-1$

	public SetNumberformat() {
		super(m_cmdStmtSubType);
	}

	@Override
	protected boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (cmd.getProperty(ISQLCommand.PROP_STRING) != null) {
			String str = (String) cmd.getSql();
			str = str.trim().replaceAll("^(?i:set\\s+num)(?i:f|fo|for|form|forma|format)\\s+", "").trim(); //$NON-NLS-1$  //$NON-NLS-2$
			str = str.replaceAll("[\'\"]", ""); //$NON-NLS-1$ //$NON-NLS-2$
			
			// Bug 21543451
			if(str.length() >= 0 && str.matches(NUMFMTREGEXP)) {
				ctx.putProperty(ScriptRunnerContext.SETNUMFORMAT, str);
			}
			else {
				String msg = MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NUMFORMAT_ILLEGAL), new Object[] {"\"" + str + "\""}) + m_lineSeparator; //$NON-NLS-1$ //$NON-NLS-2$
				try {
					ctx.getOutputStream().write(msg.getBytes());
				} catch (IOException e) {
					Logger.getLogger(this.getClass().toString()).log(Level.WARNING, "Failed to set the Number width"); //$NON-NLS-1$
				}
			}
		}
		return true;
	}

}
