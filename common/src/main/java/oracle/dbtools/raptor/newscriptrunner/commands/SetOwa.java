/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * @author klrice
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class SetOwa extends AForAllStmtsCommand {
    public static final String OWA_INIT = "declare " + //$NON-NLS-1$
            " nm     owa.vc_arr;   " + //$NON-NLS-1$
            " vl     owa.vc_arr;   " + //$NON-NLS-1$
            " begin  " + //$NON-NLS-1$
            " nm(1) := 'WEB_AUTHENT_PREFIX'; " + //$NON-NLS-1$
            " vl(1) := 'WEB$'; " + //$NON-NLS-1$
            " owa.init_cgi_env( 1, nm, vl ); " + //$NON-NLS-1$
            " end;"; //$NON-NLS-1$
    private final static SQLCommand.StmtSubType s_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_OWA;
    
    /**
     * @param cmd
     */
    public SetOwa() {
        super(s_cmdStmtSubType);
    }
    
    @Override
    public void doBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && 
                (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
            return;
        }
        // init owa...
        if (cmd.getStmtClass() == SQLCommand.StmtType.G_C_PLSQL || cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQL) {
            CallableStatement stmt = null;
            try {
                stmt = conn.prepareCall(OWA_INIT);
                stmt.execute();
            } catch (SQLException e) {
                try {
                    ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic((Messages.getString("SetOwa.0") + e.getMessage()))); //$NON-NLS-1$
                } catch (IOException e1) {
                }
            } finally {
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException se) {
                    }
                }
            }
        }
    }
}
