/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class SetPause extends AForAllStmtsCommand implements IStoreCommand {
	private final static SQLCommand.StmtSubType s_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_PAUSE;
	private final static Boolean ON = Boolean.TRUE;
	private final static Boolean OFF = Boolean.FALSE;
	private final static String startUnique = "\nNewPagemarker";
	private final static String endUnique = "Z\n";
	private int pageNo = 0;
	private Integer pauseUnique = null;

	public SetPause() {
		super(s_cmdStmtSubType);
	}

	/**
	 * potentiually gui bit passed off to a handler. note there is a setpause and a pause - probably a dupe.
	 */

	// begin watcher removed - we only get called from buffered output on SQLPLUSCmdFormatter.
	// f8 run command gui pause does not make sense - get separate tabs for each sql anyway

	@Override
	public boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

		String setPtrn = "(?i:set\\b\\s*)"; //$NON-NLS-1$
		String pausePtrn = "(?i:pau(?:|s|se)\\b\\s*)"; //$NON-NLS-1$
		String regex = "[\'|\"][\\s\\S]*[\'|\"]|\\w+"; //$NON-NLS-1$

		String pauseString = cmd.getSql().replaceFirst(setPtrn, "").trim();
		pauseString = pauseString.replaceFirst(pausePtrn, "").trim();
		if ( "".equals(pauseString) ) {
			ctx.write(MessageFormat.format(Messages.getString("SETUNKNOWNOPTION"), cmd.getSQLOrig().split("\\s+")[1]));
			return true;
		}

		if ( !pauseString.matches(regex) ) {
			ctx.write(MessageFormat.format(Messages.getString("SETUNKNOWNOPTION") + "\n", cmd.getSQLOrig().split("\\s+")[3]));
		} 

		String theString = (String) cmd.getProperty(ISQLCommand.PROP_STRING);
		if ((theString == null) || (theString.equals(""))) { //$NON-NLS-1$
			ctx.write(Messages.getString("SETPAUSENOARG")); // actually this is caught higher up the stack //$NON-NLS-1$
		} else if (theString.equalsIgnoreCase("on")) { //$NON-NLS-1$
			ctx.putProperty(ScriptRunnerContext.SET_PAUSE, ON);
		} else if (theString.equalsIgnoreCase("off")) { //$NON-NLS-1$
			ctx.putProperty(ScriptRunnerContext.SET_PAUSE, OFF);
		} else {
			if (theString.length() > 1) {
				if ((theString.startsWith("'") && theString.endsWith("'")) || //$NON-NLS-1$ //$NON-NLS-2$
						((theString.startsWith("\"") && theString.endsWith("\"")))) { //$NON-NLS-1$ //$NON-NLS-2$
					theString = theString.substring(1, theString.length() - 1);
				} else {
					theString = theString.split(" ",2)[0] ;
				}
			}
			ctx.putProperty(ScriptRunnerContext.SET_PAUSE_VALUE, theString);
		}
		return true;
	}

	
	@Override
	protected void doEndCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		super.doEndCmd(conn, ctx, cmd);
		Boolean onOrOff = (Boolean) ctx.getProperty(ScriptRunnerContext.SET_PAUSE);
		if (onOrOff != null) {
			if (onOrOff.equals(ON)) {
				// on/off is unreliable "garbage" = on
				setCmdOn(true);
			} else if (onOrOff.equals(OFF)) {
				setCmdOn(false);
			}
		}
	}
	

	private int matchesInString(ScriptRunnerContext ctx, StringBuffer data, int uniqueNo) {
		String unique = SetPause.startUnique + uniqueNo + SetPause.endUnique;
		int offset = 0;
		int matches = 0;
		while ((offset = data.indexOf(unique, offset)) != -1) {
			offset++;
			matches++;
		}
		return matches;
	}

	private int rewrite(ScriptRunnerContext ctx, StringBuffer data, int uniqueNo, int proposedNo) {
		String unique = SetPause.startUnique + uniqueNo + SetPause.endUnique;
		String proposed = SetPause.startUnique + proposedNo + SetPause.endUnique;
		int diff = proposed.length() - unique.length();
		int offset = 0;
		int matches = 0;
		while ((offset = data.indexOf(unique, offset)) != -1) {
			StringBuffer dataNew = new StringBuffer(data);
			data.setLength(offset);
			data.append(proposed);
			if ((offset + unique.length()) < dataNew.length()) {
				data.append(dataNew.substring(offset + unique.length()));
			}
			offset++;
			offset = offset + diff;
			matches++;
			if (matches >= (pageNo - 1)) {
				break;
			}
		}
		return matches;
	}

	public void mark(ScriptRunnerContext ctx, Object out, StringBuffer databuf) {
		pageNo++;
		if (!(out instanceof BufferedOutputStream)) {
			return;
		}
		if ((ctx.getProperty(ScriptRunnerContext.SET_PAUSE) != null) && (((Boolean) (ctx.getProperty(ScriptRunnerContext.SET_PAUSE))).equals(Boolean.TRUE))) {
			Integer unique = pauseUnique;
			Integer proposed = unique;
			if (unique == null) {
				proposed = 1;
			}
			while (true) {
				int matchesInString = (this.matchesInString(ctx, databuf, proposed));
				// happy days
				if (matchesInString == (pageNo - 1)) {
					if (unique == null) {
						pauseUnique = proposed;
					}
					if (proposed == unique) {
						break;
					}
					break;
				}
				boolean exitOuter = false;
				// inner loop keep going up until page matches = 0;proposed++;
				while (true) {
					proposed++;
					matchesInString = (this.matchesInString(ctx, databuf, proposed));
					if (matchesInString == 0) {
						if (unique != null) {
							rewrite(ctx, databuf, unique, proposed);
						}
						pauseUnique = proposed;
						exitOuter = true;
						break;
					}
				}
				if (exitOuter) {
					break;
				}
			}
			databuf.append(SetPause.startUnique + proposed + SetPause.endUnique);
		} else {
			return;
		}
	}

	public boolean writeOut(ScriptRunnerContext ctx, Object out, StringBuffer dataBuf) throws IOException {
		// TODO Auto-generated method stub
		if (!(out instanceof BufferedOutputStream)) {
			return false;
		}
		if ((ctx.getProperty(ScriptRunnerContext.SET_PAUSE) != null) && (((Boolean) (ctx.getProperty(ScriptRunnerContext.SET_PAUSE))).equals(Boolean.TRUE))
				&& (ctx.getProperty(ScriptRunnerContext.SET_PAUSE_VALUE) != null) && (pageNo != 0)) {
			String unique = SetPause.startUnique + pauseUnique + SetPause.endUnique;
			int offset = 0;
			int prevOffset = 0;
			int matches = 0;
			boolean printRest = false;
			while ((offset = dataBuf.indexOf(unique, prevOffset)) != -1) {
				write(ctx, out, dataBuf.substring(prevOffset, offset));
				((BufferedOutputStream) out).flush();
				ctx.getHandleSetPauseProvider().handlePause(ctx, null);// should not need cmd
				if (offset + unique.length() < dataBuf.length()) {
					offset = offset + unique.length();
				} else {
					break;
				}
				prevOffset = offset;
				matches++;
				if (matches >= pageNo) {// incase we used unique match in last page
					printRest = true;
					break;
				}
			}
			if ((offset == -1) || (printRest)) {
				write(ctx, out, dataBuf.substring(prevOffset));
			}
			pageNo = 0;
		} else {
			write(ctx, out, dataBuf.toString());
		}
		return true;
	}

	public static void directWriteOut(ScriptRunnerContext ctx, OutputStreamWriter out) {
		// need we filter it in case it is used in strange ways i.e.
		// part of top/info etc - Barry - they want it -> they got it...
		if ((ctx != null) && (ctx.getProperty(ScriptRunnerContext.SET_PAUSE) != null)
				&& (((Boolean) (ctx.getProperty(ScriptRunnerContext.SET_PAUSE))).equals(Boolean.TRUE))
				&& (ctx.getProperty(ScriptRunnerContext.SET_PAUSE_VALUE) != null)) {

			try {
				out.flush();
			} catch (IOException e) {
				// bad but not terrible if flush fails.
				Logger.getLogger("SetPause").log(Level.WARNING, e.getStackTrace()[0].toString(), e); //$NON-NLS-1$
			}

			ctx.getHandleSetPauseProvider().handlePause(ctx, null);// should not need cmd
		}
	}

	private void write(ScriptRunnerContext ctx, Object out, String s) throws IOException {
		try {
			((BufferedOutputStream) out).write(s.getBytes(ctx.getEncoding())); //$NON-NLS-1$
		} catch (UnsupportedEncodingException e) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getStackTrace()[0].toString(), e);
		}
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		String onoff = "";
		String message = null;

		if ((ctx.getProperty(ScriptRunnerContext.SET_PAUSE) != null)) {
			if ((Boolean) ctx.getProperty(ScriptRunnerContext.SET_PAUSE).equals(Boolean.TRUE)) {
				onoff = "ON";
			} else {
				onoff = "OFF"; //$NON-NLS-1$
			}
		}
		if (ctx.getProperty(ScriptRunnerContext.SET_PAUSE_VALUE) != null)
			message = (String) ctx.getProperty(ScriptRunnerContext.SET_PAUSE_VALUE); // $NON-NLS-1$
		String toreturn = "";
		if (onoff.length() > 0) {
			toreturn += StoreRegistry.getCommand("pause", onoff);
		}
		if (message != null && message.length()>0)
			toreturn += StoreRegistry.getCommand("pause", MessageFormat.format("\"{0}\"", message));
		return toreturn;
	}
}
