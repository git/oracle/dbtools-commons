/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;

import oracle.dbtools.raptor.format.FormatRegistry;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * A simple sets the format to use when printing sql
 * 
 * 
 * @author klrice
 * 
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class SetSQLFormat extends CommandListener implements IShowCommand {

  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

    // check for our command
    if (cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("setsqlformat")) {
      String[] parts = cmd.getSQLOrig().split(" ");
      if (parts.length < 3 || ( parts.length==3 && parts[2].equalsIgnoreCase("default"))) {
        ctx.write(Messages.getString("SQLFORMATCLEARED"));
        ctx.putProperty(ScriptRunnerContext.SQL_FORMAT,null);
        return true;
      }
      String format = parts[2].trim();

      if (format == null || !isValid(format) || FormatRegistry.getFormatter(format) == null) {
        ctx.write(Messages.getString("SQLFORMATBAD"));
        return true;
      }

      ctx.putProperty(ScriptRunnerContext.SQL_FORMAT, format);
      ctx.putProperty(ScriptRunnerContext.SQL_FORMAT_FULL, parts);

      return true;
    }
    return false;
  }
  
  private boolean isValid(String format) {
	  boolean valid = false;
	  ArrayList<String> list = (ArrayList<String>) FormatRegistry.getAllTypes();
	  for(int i = 0; i < list.size(); i++) {
		  if(list.get(i).equalsIgnoreCase(format)) {
			  return true;
		  }
	  }
	  return valid;
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

  }

  /* (non-Javadoc)
   * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShow#handle(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
   */
  @Override
  public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    String format=null;
    if (ctx.getProperties().containsKey(ScriptRunnerContext.SQL_FORMAT)&&(ctx.getProperties().get(ScriptRunnerContext.SQL_FORMAT)!=null)) {
      format=(String)ctx.getProperties().get(ScriptRunnerContext.SQL_FORMAT);
    } else {
      format="Default";
    }
    ctx.write("");
    ctx.write(MessageFormat.format(Messages.getString("SQLFORMAT"),new Object[]{format}) + "\n");
    return true;
  }

  /* (non-Javadoc)
   * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShow#getShowCommand()
   */
  @Override
  public String[] getShowAliases() {
    return new String[]{"sqlformat"};
  }

@Override
public boolean needsDatabase() {
    // TODO Auto-generated method stub
    return false;
}

@Override
public boolean inShowAll() {
    // TODO Auto-generated method stub
    return false;
}

}
