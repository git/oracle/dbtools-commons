/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;

/**
 * @author <a href= "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetSecuredCol.java"> Barry McGillin</a>
 *
 */
public class SetSecuredCol extends CommandListener implements IShowCommand {
	String[] aliases = { "securedcol" }; //$NON-NLS-1$
	final String auth = "******************************"; //$NON-NLS-1$
	final String unknown = "??????????????????????????????"; //$NON-NLS-1$
	String unsupported = Messages.getString("SetSecuredCol.0"); //$NON-NLS-1$

	@Override
	public String[] getShowAliases() {
		return aliases;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (!ctx.isSQLPlusClassic()) {
			ctx.write(MessageFormat.format(unsupported, cmd.getSql()));
		}
		if (ctx.getProperty(ScriptRunnerContext.SET_SECUREDCOL) != null) {
			String on = "OFF"; //$NON-NLS-1$
			boolean x = (boolean) ctx.getProperty(ScriptRunnerContext.SET_SECUREDCOL);
			if (x) {
				on = "ON"; //$NON-NLS-1$
				ctx.write(MessageFormat.format(Messages.getString("SetSecuredCol.1"), new Object[] { on, //$NON-NLS-1$
						ctx.getProperty(ScriptRunnerContext.SET_SECUREDCOL_UNAUTH), ctx.getProperty(ScriptRunnerContext.SET_SECUREDCOL_UNKNOWN) }));
			} else {
				on = "OFF"; //$NON-NLS-1$
				ctx.write(MessageFormat.format(Messages.getString("SetSecuredCol.2"), new Object[] { on })); //$NON-NLS-1$
			}
		}
		return true;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return false;
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

		if (!ctx.isSQLPlusClassic()) {
			ctx.write(MessageFormat.format(unsupported, cmd.getSql()));
		}
		String lauth = auth;
		String lunknown = unknown;
		String msg = Messages.getString("SetSecuredCol.3"); //$NON-NLS-1$
		String sql = cmd.getSql();

		// Modify now for quoted Strings
		String regex = "\"([^\"]*)\"|(\\S+)"; //$NON-NLS-1$
		Matcher m = Pattern.compile(regex).matcher(sql);
		ArrayList<String> ltokens = new ArrayList<String>();
		while (m.find()) {
			if (m.group(1) != null) {
				ltokens.add(m.group(1));
			} else {
				ltokens.add(m.group(2));
			}
		}
		String[] tokens = (String[]) ltokens.toArray(new String[ltokens.size()]);
		if (tokens.length < 3) {
			ctx.write(""); //$NON-NLS-1$
		} else {
			boolean onoff = false;
			String token = tokens[2];
			if (token.toLowerCase().equals("on") || token.toLowerCase().equals("off")) { //$NON-NLS-1$ //$NON-NLS-2$
				onoff = token.toLowerCase().equals("on") ? Boolean.TRUE : Boolean.FALSE; //$NON-NLS-1$
			} else {
				ctx.write(""); //$NON-NLS-1$
			}
			if (onoff) {
				msg = MessageFormat.format(msg, onoff ? "ON" : "OFF"); //$NON-NLS-1$ //$NON-NLS-2$
				if (tokens.length > 3) {
					if (tokens.length == 5 || tokens.length == 7) {
						if (tokens.length == 5) { // Must be 5 then
							if (tokens[3].equalsIgnoreCase("unauth") || tokens[3].equalsIgnoreCase("unauthorized")) { //$NON-NLS-1$ //$NON-NLS-2$
								lauth = tokens[4];
							} else if (tokens[3].equalsIgnoreCase("unk") || tokens[3].equalsIgnoreCase("unknown")) { //$NON-NLS-1$ //$NON-NLS-2$
								lunknown = tokens[4];
							} else {
								ctx.write(msg);
								return true;
							}
						}
						if (tokens.length == 7) {
							if (tokens[3].equalsIgnoreCase("unauth") || tokens[3].equalsIgnoreCase("unauthorized")) { //$NON-NLS-1$ //$NON-NLS-2$
								lauth = tokens[4];
							} else if (tokens[3].equalsIgnoreCase("unk") || tokens[3].equalsIgnoreCase("unknown")) { //$NON-NLS-1$ //$NON-NLS-2$
								lunknown = tokens[4];
							} else {
								ctx.write(msg);
								return true;
							}
							if (tokens[5].equalsIgnoreCase("unauth") || tokens[5].equalsIgnoreCase("unauthorized")) { //$NON-NLS-1$ //$NON-NLS-2$
								lauth = tokens[6];
							} else if (tokens[5].equalsIgnoreCase("unk") || tokens[5].equalsIgnoreCase("unknown")) { //$NON-NLS-1$ //$NON-NLS-2$
								lunknown = tokens[6];
							} else {
								ctx.write(msg);
								return true;
							}
						}
					} else {
						ctx.write(msg);
						return true;
					}
				}
				ctx.putProperty(ScriptRunnerContext.SET_SECUREDCOL, onoff); // $NON-NLS-1$
				ctx.putProperty(ScriptRunnerContext.SET_SECUREDCOL_UNAUTH, lauth); // $NON-NLS-1$
				ctx.putProperty(ScriptRunnerContext.SET_SECUREDCOL_UNKNOWN, lunknown); // $NON-NLS-1$
			} else {
				if (tokens.length > 3) {
					msg = MessageFormat.format(msg, onoff ? "ON" : "OFF"); //$NON-NLS-1$ //$NON-NLS-2$
					ctx.write(msg);
					return true;
				} else {
					ctx.putProperty(ScriptRunnerContext.SET_SECUREDCOL, onoff); // $NON-NLS-1$
					ctx.putProperty(ScriptRunnerContext.SET_SECUREDCOL_UNAUTH, auth); // $NON-NLS-1$
					ctx.putProperty(ScriptRunnerContext.SET_SECUREDCOL_UNKNOWN, unknown); // $NON-NLS-1$

				}
			}
		}
		return true;
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

}