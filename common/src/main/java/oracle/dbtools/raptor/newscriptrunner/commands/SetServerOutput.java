/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.SQLPLUSCmdFormatter;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.JSONWrapBufferedOutputStream;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.utils.MessageLogging;
import oracle.jdbc.OracleConnection;

/**
 * @author klrice
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class SetServerOutput extends AForAllStmtsCommand implements IOnConnection {
  private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_SERVEROUTPUT;

  private static Hashtable<Connection, Boolean> m_enableHashFlag = new Hashtable<Connection, Boolean>();//doneill:Is this a leak ??

  public static enum FormatStyle {
    WRAPPED, WORD_WRAPPED, TRUNCATED
  }

  private boolean isDbmsOn = false;

  /**
   * @param cmd
   */
  public SetServerOutput() {
    super(m_cmdStmtSubType);
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    boolean noConn = false;
    super.beginEvent(conn, ctx, cmd);
    if ((ctx != null) && (ctx.getProperty(ScriptRunnerContext.NOLOG) != null) && (((Boolean) (ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
      // isenabled is passed connection. set serveroutput on works in nolog in
      // sqlplus. shutting it off for now.
      // could remember the actions and use them on 'connect command'
      noConn = true;
    }
    // if the command is a show push serveroutput state to ctx
    if (cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SHOW)) {
      // show maybe be show serveroutput or show all so needs to know the
      // current state.
      Boolean amIOn = Boolean.FALSE;
      boolean fromCall = (ctx.getProperty(ScriptRunnerContext.SERVEROUTNOLOGLATER) != null && (Boolean) (ctx.getProperty(ScriptRunnerContext.SERVEROUTNOLOGLATER)).equals(Boolean.TRUE));
      if (!noConn) {// test this
        fromCall = isEnabled(ctx.getCurrentConnection());
      }
      if (fromCall == true) {
        amIOn = Boolean.TRUE;
      }
      ctx.putProperty(ScriptRunnerContext.SHOWSERVEROUTPUT, amIOn);
    }
    if (this instanceof IOnConnection && cmd.getStmtSubType().equals(StmtSubType.G_S_DISCONNECT)) {
      runOnDisconnect(conn, ctx, cmd);
    }
  }

  // need to call this dbmsoutput 'end watcher' early straight after plsql non
  // create and execute
  public static void coreEndWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    if ((ctx != null) && (ctx.getProperty(ScriptRunnerContext.NOLOG) != null) && (((Boolean) (ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
      return;
    }
    cmd.getStmtSubType();
    if (cmd != null) {
      if (cmd.getProperty(ISQLCommand.PROP_DBMS_OUTPUT_CALLED) != null) {
        return;
      } else {
        cmd.setProperty(ISQLCommand.PROP_DBMS_OUTPUT_CALLED,
            Boolean.TRUE/* anything so get !=null) */);
      }
    }
    if ((ctx!=null)&&(ctx.getProperty(ScriptRunnerContext.SERVEROUTPUT_OPTIMIZED)!=null)) {
        if (cmd != null && ! cmd.isFail() && (!(cmd.getStmtSubType()==StmtSubType.G_S_CALL))
                &&(!(cmd.getStmtSubType() == StmtSubType.G_S_EXECUTE)) &&
                /* punt non-sql , non-plsql */
                (!(cmd.getStmtType() == StmtType.G_C_SQL || (cmd.getStmtType() == StmtType.G_C_PLSQL))
                        ||  (cmd.getStmtType() == StmtType.G_C_SQL && ! ( cmd.getStmtSubType() == StmtSubType.G_S_SELECT || cmd.getStmtSubType() == cmd.getStmtSubType().G_S_WITH))
                        || (cmd.getStmtSubType() == StmtSubType.G_S_ALTER)
                        /* punt create statements */
                        || cmd.isCreateCmd()
                        /* punt non-executable */
                        || !cmd.getExecutable()
                        /* punt a noop apex install has 500+ of these */
                        || cmd.getLoweredTrimmedSQL().equals("begin\nnull;\nend;"))) {
            return ;
        }
    }


    if (conn != null && conn instanceof OracleConnection) {
      String s = "\n"; //$NON-NLS-1$
      Boolean paneRegistered = (Boolean) ctx.getProperty(ScriptRunnerContext.DBMSPUTPUTPANE);
      if (isEnabled(conn) || ((paneRegistered != null) && (paneRegistered.equals(Boolean.TRUE)/* set in DBCommandRunnerTask */))) {
        if (!ScriptUtils.isHttpCon(conn, ctx)) {
          s = format(ctx, DBUtil.getInstance(conn).getDBMSOUTPUT());
        }
      }
      if (!s.equals("\n")) { //$NON-NLS-1$
        try {
          if (isEnabled(conn)) {
            if (ctx.getOutputStream() instanceof JSONWrapBufferedOutputStream) {
              ((JSONWrapBufferedOutputStream) ctx.getOutputStream()).addStmtInfo(getDBMSOUTPUTJSON(s));
            }
            ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(s));
          }
          ctx.pushDbmsOutput(s);
        } catch (IOException e) {
          Logger.getLogger(new SetServerOutput().getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
        }
      }
    }
  }

  private static String getDBMSOUTPUTJSON(String s) {
    return "\"dbmsOutput\":" + JSONWrapBufferedOutputStream.quote(s, true);
  }

  @Override
  protected void doEndWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    coreEndWatcher(conn, ctx, cmd);
  }

  private static boolean isEnabled(Connection conn) {
    if (conn != null) {
      if (m_enableHashFlag.get(conn) == null)
        return false;
      return m_enableHashFlag.get(conn);
    }
    return false;
  }

  @Override
  protected boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    boolean noConn = false;
    if ((ctx != null) && (ctx.getProperty(ScriptRunnerContext.NOLOG) != null) && (((Boolean) (ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
      noConn = true;
    }
    int flag = getSetServerOnOrOff(cmd);
    boolean retValue = false;
    switch (flag) {
    case 3:
            //setunoptimized flag
      ctx.removeProperty(ScriptRunnerContext.SERVEROUTPUT_OPTIMIZED);
      break;
    case 2:
      //setoptimized flag
      ctx.putProperty(ScriptRunnerContext.SERVEROUTPUT_OPTIMIZED,
                        Boolean.TRUE/* anything so get !=null) */);
      break;
    case 1:
      retValue = setServerOutputOn(conn, ctx, cmd, noConn);
      isDbmsOn = true;
      break;
    case 0:
      retValue = setServerOutputOnOrOff(conn, ctx, cmd, noConn, true);// off
                                                                      // needs
                                                                      // to
                                                                      // parse
                                                                      // size
                                                                      // and
                                                                      // format
                                                                      // as well
      if (!noConn) {
        ctx.enableDbmsOutput(); // Enable the output for the Dbms tab if it's
                                // there because we want the output on the tab
      }
      isDbmsOn = true;// false surely otherwise listeners will fire when off
      break;
    default:
      if (ctx.isSQLPlusClassic()) {
        ctx.write(Messages.getString("SETSERVEROUTPUTONOROFF")); //$NON-NLS-1$
      } else {
        ctx.write(Messages.getString("SETSERVEROUTPUTONOROFFOPT"));  //$NON-NLS-1$
      }
      return false;// so I am not switched on

    }
    return retValue;
  }

  private boolean setServerOutputOff(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    DBUtil.getInstance(conn).execute("BEGIN DBMS_OUTPUT.DISABLE(); END;", (List) null); //$NON-NLS-1$
    m_enableHashFlag.put(conn, false);
    ctx.enableDbmsOutput(); // Enable the output for the Dbms tab if it's there
                            // because we want the output on the tab
    return true;
  }

  private boolean setServerOutputOn(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd, boolean noConn) {
    return setServerOutputOnOrOff(conn, ctx, cmd, noConn, false);
  }

  private boolean setServerOutputOnOrOff(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd, boolean noConn, boolean isOff) {
    FormatStyle toSet = null;// FormatStyle.WORD_WRAPPED;
    String theSql = cmd.getSql();
    if (isOff) {
      theSql = (theSql + " ").replaceFirst("(?i) off ", " on ").trim();// for //$NON-NLS-1$ //$NON-NLS-2$
                                                                       // easier
                                                                       // lexing
                                                                       // later
    } 
    // FOR[MAT] {WRA[PPED] | WOR[D_WRAPPED] | TRU[NCATED]
    String theSqlLower = theSql.toLowerCase(Locale.US);

    if (theSqlLower.indexOf("for") != -1) { //$NON-NLS-1$
      String[] split = theSqlLower.trim().split("\\s+"); //$NON-NLS-1$
      if ((split != null) && (split.length > 1)) {
        int forAt = -1;
        for (int word = split.length - 2; word > 1; word--) {
          if (split[word].matches("^fo(?:r|rm|rma|rmat)$")) { //$NON-NLS-1$
            forAt = word;
            break;
          }
        }
        if (forAt != -1) { // $NON-NLS-1$
          if (split[forAt + 1].matches("^wr(?:a|ap|app|appe|apped)$")) { //$NON-NLS-1$
            toSet = FormatStyle.WRAPPED;
          } else if (split[forAt + 1].matches("^wo(?:r|rd|rd_|rd_w|rd_wr|rd_wra|rd_wrap|rd_wrap|rd_wrapp|rd_wrappe|rd_wrapped)$")) { //$NON-NLS-1$
            toSet = FormatStyle.WORD_WRAPPED;
          } else if (split[forAt + 1].matches("^tr(?:u|un|unc|unca|uncat|uncate|uncated)$")) { //$NON-NLS-1$
            toSet = FormatStyle.TRUNCATED;
          } else {
            writeUsage(ctx);
            return false;// print usage
          }
        }
      }
    }
    if (toSet == null) {// need to preserve prev setting if possible.
      toSet = (FormatStyle) ctx.getProperty(ScriptRunnerContext.SERVEROUTFORMAT);
      if (toSet == null) {
        toSet = FormatStyle.WORD_WRAPPED;
      }
    } else {
      theSql = theSql.replaceFirst("(?i)for[^\\s]*\\s\\s*[^\\s][^\\s]*", ""); //$NON-NLS-1$ //$NON-NLS-2$
    }
    // use output of set serveroutput size n n is 2000 to 1000000 unlimited is
    // the default on 11g call that 1000000
    String numberOrUnlimited = theSql.toLowerCase().replace("\n", " ").replace("-", "").replace("\\", " ") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
        .replaceAll("\\s+", " ").trim().replace("set serveroutput on size", "") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        .replace("set serveroutpu on size", "") //$NON-NLS-1$ //$NON-NLS-2$
        .replace("set serveroutp on size", "") //$NON-NLS-1$ //$NON-NLS-2$
        .replace("set serverout on size", "").trim(); //$NON-NLS-1$ //$NON-NLS-2$
                                                      // //$NON-NLS-3$
                                                      // //$NON-NLS-4$
    DBUtil dbinst = null;
    if (!noConn) {
      dbinst = DBUtil.getInstance(conn);
      hackExceptionHandler(dbinst);
    }

    if (numberOrUnlimited.equals("unlimited") || numberOrUnlimited.equals("unl") || //$NON-NLS-1$ //$NON-NLS-2$
        numberOrUnlimited.equals("unli") || numberOrUnlimited.equals("unlim") || //$NON-NLS-1$ //$NON-NLS-2$
        numberOrUnlimited.equals("unlimi") || numberOrUnlimited.equals("unlimit") || numberOrUnlimited.equals("unlimite") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

    ) { // $NON-NLS-1$
      if (!noConn) {
        if (!isOff) {
          dbinst.execute("begin dbms_output.enable(1000000); end;", (List) null); //$NON-NLS-1$
        } else {
          dbinst.execute("begin dbms_output.disable(); end;", (List) null); //$NON-NLS-1$
        }
      }
      SQLException ex = null;
      if (!noConn) {
        ex = dbinst.getLastException();
      }
      if (ex != null) {
        System.out.println(ex.getLocalizedMessage());
        return false;
      }
      ctx.putProperty(ScriptRunnerContext.LASTSETSERVEROUTPUT, new Integer(1000000));
      ctx.putProperty(ScriptRunnerContext.SERVEROUTPUTUNLIMITED, Boolean.TRUE);
      if (!noConn) {
        if (isOff) {
          m_enableHashFlag.put(conn, false);
        } else {
          flushOnSwitchOn(conn, ctx);
          m_enableHashFlag.put(conn, true);
        }
      }
    } else {
      try {
        int i = new Integer(numberOrUnlimited).intValue();
        if (i > 1999 && i < 1000001) {
          if (!noConn) {
            if (!isOff) {
              dbinst.execute("begin dbms_output.enable(" + i + "); end;", (List) null); //$NON-NLS-1$ //$NON-NLS-2$
            } else {
              dbinst.execute("begin dbms_output.disable(); end;", (List) null); //$NON-NLS-1$
            }
          }
          SQLException ex = null;
          if (!noConn) {
            ex = dbinst.getLastException();
          }
          if (ex != null) {
            System.out.println(ex.getLocalizedMessage());
            return false;
          }
          if (!noConn) {
            if (isOff) {
              m_enableHashFlag.put(conn, false);
            } else {
              flushOnSwitchOn(conn, ctx);
              m_enableHashFlag.put(conn, true);
            }
          }
          ctx.putProperty(ScriptRunnerContext.LASTSETSERVEROUTPUT, new Integer(i));
          ctx.putProperty(ScriptRunnerContext.SERVEROUTPUTUNLIMITED, Boolean.FALSE);
        } else {
          ctx.write(MessageFormat.format(Messages.getString("SERVEROUTPUT_ERROR"), //$NON-NLS-1$
              new Object[] { numberOrUnlimited }) + "\n"); //$NON-NLS-1$
          return false;// so I am not switched on
        }
      } catch (NumberFormatException nfe) {
        // default before number and unlimited check.
        Integer prevNum = (Integer) ctx.getProperty(ScriptRunnerContext.LASTSETSERVEROUTPUT);
        Boolean wasUnlimited = (Boolean) ctx.getProperty(ScriptRunnerContext.SERVEROUTPUTUNLIMITED);
        SQLException ex = null;
        if (!noConn) {
          hackExceptionHandler(dbinst);
          if (!isOff) {
            if (prevNum != null) {
              dbinst.execute("begin dbms_output.enable(" + prevNum + "); end;", (List) null); //$NON-NLS-1$ //$NON-NLS-2$
            } else {
              dbinst.execute("begin dbms_output.enable(1000000); end;", (List) null); //$NON-NLS-1$
            }
          } else {
            dbinst.execute("begin dbms_output.disable(); end;", (List) null); //$NON-NLS-1$
          }
          ex = dbinst.getLastException();
        }
        if (ex != null) {
          System.out.println(ex.getLocalizedMessage());
          return false;
        }
        if (prevNum == null) {
          ctx.putProperty(ScriptRunnerContext.LASTSETSERVEROUTPUT, new Integer(1000000));
        }
        if (wasUnlimited == null) {
          ctx.putProperty(ScriptRunnerContext.SERVEROUTPUTUNLIMITED, Boolean.TRUE);// default
        }
        // number format exception could be lack of size in that case do not
        // echo usage
        if ((numberOrUnlimited != null) && (!((numberOrUnlimited.equals("set serveroutput on") || //$NON-NLS-1$
            numberOrUnlimited.equals("set serveroutput on") || //$NON-NLS-1$
            numberOrUnlimited.equals("set serveroutpu on") || //$NON-NLS-1$
            numberOrUnlimited.equals("set serveroutp on")) || //$NON-NLS-1$
            numberOrUnlimited.equals("set serverout on")))) { //$NON-NLS-1$
          writeUsage(ctx);
        }
        if (!(noConn)) {
          if (isOff) {
            m_enableHashFlag.put(conn, false);
          } else {
            flushOnSwitchOn(conn, ctx);
            m_enableHashFlag.put(conn, true);
          }
        }
      }
    }
    /*
     * Assume success so use toset (looks like number format exception is an
     * issue.)
     */
    ctx.putProperty(ScriptRunnerContext.SERVEROUTFORMAT, toSet);
    if (noConn) {
      ctx.putProperty(ScriptRunnerContext.SERVEROUTNOLOGLATER, (isOff == false));
    }
    return true;
  }

  private static void writeUsage(ScriptRunnerContext ctx) {
      if (ctx.isSQLPlusClassic()) {
          ctx.write("Usage: SET SERVEROUTPUT { ON | OFF } [SIZE {n | UNL[IMITED]}] \n" //$NON-NLS-1$
                              + "      [ FOR[MAT] { WRA[PPED] | WOR[D_WRAPPED] | TRU[NCATED] } ]\n"); //$NON-NLS-1$);
      } else {
          ctx.write("Usage: SET SERVEROUTPUT { OPTIMIZED | UNOPTIMIZED | { ON | OFF } [SIZE {n | UNL[IMITED]}] \n" //$NON-NLS-1$
                    + "      [ FOR[MAT] { WRA[PPED] | WOR[D_WRAPPED] | TRU[NCATED] } ]}\n"); //$NON-NLS-1$
      }
  }
  /**
   * Hack to have DBUtil to report exception without unwanted sugarcoating (bug
   * 21521503)
   * 
   * @param dbinst
   */
  public static void hackExceptionHandler(DBUtil dbinst) {
    SQLException old = dbinst.getLastException();
    if (old != null)
      System.out.println(old.getLocalizedMessage());
    try { // dbinst.clearLastException();
      Method clearLastException = DBUtil.class.getDeclaredMethod("clearLastException");
      clearLastException.setAccessible(true);
      clearLastException.invoke(dbinst);
    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      // System.err.println(e.toString());
    }
    MessageLogging.setMessageLogger(new MessageLogging() {
      @Override
      public void log(String txt) {
        System.out.println(txt);
      }
    });
  }

  private int getSetServerOnOrOff(ISQLCommand cmd) {
    String[] splitSql = cmd.getSql().toLowerCase().replaceFirst("set", " ").replaceFirst("serverout[a-z]*", " ").trim().split(" "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
    if (splitSql[0].equalsIgnoreCase("on")) //$NON-NLS-1$
      return 1;
    else if (splitSql[0].equalsIgnoreCase("off")) //$NON-NLS-1$
      return 0;
    else if (splitSql[0].equalsIgnoreCase("optimized")&&(splitSql.length==1))
      return 2;
    else if (splitSql[0].equalsIgnoreCase("unoptimized")&&(splitSql.length==1))
      return 3;
    else
      return -1;
  }

  protected boolean isListenerOn(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    return isDbmsOn;
  }

  @Override
  public void setCmdOn(boolean cmdOn) {
    super.setCmdOn(cmdOn);
    isDbmsOn = cmdOn;
  }

  protected void flushOnSwitchOn(Connection conn, ScriptRunnerContext ctx) {
    if ((ctx != null) && (ctx.getProperty(ScriptRunnerContext.NOLOG) != null) && (((Boolean) (ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
      return;
    }
    if (conn instanceof OracleConnection) {
      if (!(isEnabled(conn))) { // if previously off and we are now switching on
                                // - clear the output.
        String s = "\n"; //$NON-NLS-1$
        DBUtil dbinst = DBUtil.getInstance(conn);
        // assumption honor wrapped/word wrapped/truncated for consistency
        if (!ScriptUtils.isHttpCon(conn, ctx)) {
          s = format(ctx, dbinst.getDBMSOUTPUT());
        }
        if (!s.equals("\n")) { //$NON-NLS-1$
          ctx.pushDbmsOutput(s);
        }
      }
    }
  }

  private static String format(ScriptRunnerContext ctx, String rawInString) {
    if (rawInString == null) {
      return ""; //$NON-NLS-1$
    }

    FormatStyle formatStyle = (FormatStyle) ctx.getProperty(ScriptRunnerContext.SERVEROUTFORMAT);
    StringBuffer retVal = null;
    StringBuffer rawIn = null;

    if (rawInString != null) {
      rawIn = new StringBuffer(rawInString);
    }
    if (formatStyle == null) {
      formatStyle = FormatStyle.WORD_WRAPPED;
    }
    Integer linesize = ((Integer) ctx.getProperty(ScriptRunnerContext.SETLINESIZE));
    if ((linesize == null) || (linesize == -1)) {
      // some sensible default - will use Integer,MAX_VALUE for now
      linesize = Integer.MAX_VALUE;
    }
    switch (formatStyle) {
    case WRAPPED:
      retVal = ScriptUtils
          .wrap(/* type that should not constrain output OracleTypes.CLOB */ 2005, linesize, rawIn);
      break;
    case WORD_WRAPPED:
      retVal = ScriptUtils.wordwrap(2005, linesize, rawIn,
          Integer.MAX_VALUE /*
                             * maybe long? but do not want it limited to 80 by
                             * default
                             */);
      break;
    case TRUNCATED:
      retVal = truncLines(rawIn, linesize);
      // new trunc preserves ending CRLF
      break;
    }
    String retValString = null;
    if (retVal != null) {
      retValString = retVal.toString();
    }
    return retValString;

  }

  public static StringBuffer truncLines(StringBuffer buf, int linesize) {
    StringBuffer output = new StringBuffer();
    if (buf == null) {
      return output;// or null?
    }
    int intMax = buf.length();
    int curPos = -1;
    int nextPos = 0;
    while ((nextPos = buf.indexOf("\n", curPos + 1)) != -1) { //$NON-NLS-1$
      int thislinesize = nextPos - (curPos + 1);
      if (thislinesize < linesize) {
        output.append(buf.substring(curPos + 1, nextPos + 1));
      } else {
        boolean wasr = false;
        if ((nextPos > 0) && (thislinesize != linesize)) {
          wasr = (buf.charAt(nextPos - 1) == '\n');
        }
        output.append(buf.substring(curPos + 1, curPos + 1 + linesize));
        if (wasr) {
          output.append("\r\n"); //$NON-NLS-1$
        } else {
          output.append("\n"); //$NON-NLS-1$
        }
      }
      curPos = nextPos;
    }
    if ((curPos + 1 < intMax)) {
      if (intMax - (curPos) < linesize) {
        output.append(buf.substring(curPos + 1));
      } else {
        output.append(buf.substring(curPos + 1, curPos + 1 + linesize));
      }
    }
    return output;
  }

  public static String serverOutputFormatToString(ScriptRunnerContext ctx) {
    FormatStyle formatStyle = (FormatStyle) ctx.getProperty(ScriptRunnerContext.SERVEROUTFORMAT);
    if (formatStyle == null) {
      formatStyle = FormatStyle.WORD_WRAPPED;
    }
    return "FORMAT " + formatStyle.toString(); //$NON-NLS-1$
  }

  @Override
  public boolean runOnConnect(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    conn = ctx.getCurrentConnection();
    Boolean wasFirstConnect = (!((ctx.getProperty(ScriptRunnerContext.CONNECT_CALLED) != null) && ((Boolean) (ctx.getProperty(ScriptRunnerContext.CONNECT_CALLED).equals(Boolean.TRUE)))));
    if /* connect succeeded ie not nolog */
    (!((ctx.getProperty(ScriptRunnerContext.NOLOG) != null) && (((Boolean) (ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE))))) {
      ctx.putProperty(ScriptRunnerContext.CONNECT_CALLED, Boolean.TRUE);// just
                                                                        // not
                                                                        // null
    }
    // is hack exception handler required?
    boolean closedOrError = false;
    try {
      closedOrError = (conn == null || conn.isClosed() || ctx.getProperty(ScriptRunnerContext.SERVEROUTNOLOGLATER) == null
          || (Boolean) (ctx.getProperty(ScriptRunnerContext.SERVEROUTNOLOGLATER)).equals(Boolean.FALSE));
    } catch (SQLException e) {
      closedOrError = true;
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getLocalizedMessage());

    }
    if ((wasFirstConnect) && (conn != null) && (conn instanceof OracleConnection)
        && ((ctx != null) && (!((ctx.getProperty(ScriptRunnerContext.NOLOG) != null) && (((Boolean) (ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))))) && (!(closedOrError))) {
      // dbms output enable..
      DBUtil dbinst = DBUtil.getInstance(conn);
      // is hack exception handler required - at least log/ctx.write
      SQLException e = dbinst.getLastException();
      // not mine but report it.
      if (e != null) {
        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getLocalizedMessage());
      }
      Integer numb = (Integer) ctx.getProperty(ScriptRunnerContext.LASTSETSERVEROUTPUT);
      if (numb == null) {
        numb = 100000;
      }
      ArrayList<Integer> forBinds = new ArrayList<Integer>();
      forBinds.add(numb);
      dbinst.execute("begin dbms_output.enable(:size); end;", forBinds); //$NON-NLS-1$
      SQLException ex = dbinst.getLastException();
      if (ex != null) {
        ctx.write("SET SETSERVEROUT ON:" + ex.getLocalizedMessage()); //$NON-NLS-1$
        return false;
      }
      ctx.putProperty(ScriptRunnerContext.LASTSETSERVEROUTPUT, new Integer(1000000));
      m_enableHashFlag.put(conn, true);
      ctx.putProperty(ScriptRunnerContext.SERVEROUTNOLOGLATER, Boolean.FALSE);
      // no need to flush empty open as it is a new connection
      isDbmsOn = true;// class instance is created for new connection so is
                      // initialised = false;
    }
    if /* connect succeeded ie not nolog */
    (!((ctx.getProperty(ScriptRunnerContext.NOLOG) != null) && (((Boolean) (ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE))))) {
      // real connect not nolog - need to reset the pending serveroutput flag
      ctx.putProperty(ScriptRunnerContext.SERVEROUTNOLOGLATER, Boolean.FALSE);
    }
    return true;
  }

  @Override
  public boolean runOnDisconnect(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    // TODO Turloch do your thing here
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * oracle.dbtools.raptor.newscriptrunner.commands.AForAllStmtsCommand#endEvent
   * (java.sql.Connection,
   * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
   * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
   */
  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    super.endEvent(conn, ctx, cmd);
    if (this instanceof IOnConnection && cmd.getStmtSubType().equals(StmtSubType.G_S_CONNECT)) {
      runOnConnect(conn, ctx, cmd);
    }
  }
}
