/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.newscriptrunner.ArgumentQuoteException;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.WrapBufferOutputStreamToWriter;
import oracle.dbtools.raptor.newscriptrunner.WrapListenBufferOutputStream;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;


/**
 * spool handle "spool on" "spool off" "spool" "spool file_name" and spool out
 * in local encoding and line terminator.
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetSpool.java"
 *         >Barry McGillin</a>
 * @author turloch
 * 
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R2)
public class SetSpool extends AForAllStmtsCommand {
	// set spool does not exist , the command is Spool
	private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SPOOL;
	public static String fileExists = "FILE_EXISTS"; //$NON-NLS-1$

	public SetSpool() {
		super(m_cmdStmtSubType);
	}

	@Override
	protected void doBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		try {
			if (!ctx.isEchoOn()) {
				if (ctx.getTopLevel() && ctx.getProperty(ScriptRunnerContext.SILENT) != null
						&& !(boolean) ctx.getProperty(ScriptRunnerContext.SILENT)) {
					writeToExisting(ctx.getPrompt() + cmd.getSQLOrig(), ctx);
					if (cmd.getStmtType().equals(SQLCommand.StmtType.G_C_SQL)) {
						writeToExisting(";", ctx); //$NON-NLS-1$

					} else if (cmd.getStmtType().equals(SQLCommand.StmtType.G_C_PLSQL)) {
						writeToExisting("\n/", ctx); //$NON-NLS-1$
					}
					writeToExisting("\n", ctx); //$NON-NLS-1$
				} else if (ctx.isStdin() && cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_AT)) {
					writeToExisting(ctx.getPrompt() + cmd.getSQLOrig() + "\n", ctx); //$NON-NLS-1$
				}
			}
			if (cmd.getStmtSubType() == SQLCommand.StmtSubType.G_S_SPOOL) {
				WrapListenBufferOutputStream existing = (WrapListenBufferOutputStream) ctx
						.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
				if ((existing != null)) {
					// if I am spool write out current spool file path
					writeToMain(MessageFormat.format(Messages.getString("SetSpool.filedisplay"), //$NON-NLS-1$
							existing.getFileOutput().getAbsolutePath()), ctx);
					writeToExisting("\n\n", ctx); //$NON-NLS-1$
				}
			}
		} finally {

		}
	}

	@Override
	protected boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

		if (!ctx.isEchoOn()) {
			if (ctx.getTopLevel() && (!ctx.isSQLPlusClassic() && !cmd.getSQLOrig().toLowerCase().endsWith("off"))) { //$NON-NLS-1$
				writeToExisting(ctx.getPrompt() + cmd.getSQLOrig(), ctx);
				writeToExisting("\n", ctx); //$NON-NLS-1$
			}
		}
		if ((Boolean) cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN) != null
				&& (Boolean) cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN)) { // SET
																					// SPOOL
																					// ON
			if (cmd.getProperty(ISQLCommand.PROP_STRING) != null) {
				// real file after & substitution and environmental variable
				// substitution
				String realFile = ScriptUtils
						.expandVariables(cmd.getSql().trim().replaceAll("^.*?[sS][pP][oO][^\\s]*\\s+", "").trim()); //$NON-NLS-1$ //$NON-NLS-2$
				if (realFile.indexOf("$") != -1) { //$NON-NLS-1$
					// There may be more than just $ to consider for this.
					this.writeToMain(Messages.getString("SetSpool.5"), ctx); //$NON-NLS-1$
					return true;
				}
				String[] args = null;
				boolean writeFail = true;
				try {
					boolean firstArgBad = false;
					String[] bySplit = realFile.split("\\s+"); //$NON-NLS-1$
					if (bySplit != null && bySplit.length > 0) {
						String first = bySplit[0];
						if (first.equalsIgnoreCase("CRE") || //$NON-NLS-1$
								first.equalsIgnoreCase("CREA") || //$NON-NLS-1$
								first.equalsIgnoreCase("CREAT") || //$NON-NLS-1$
								first.equalsIgnoreCase("CREATE") || //$NON-NLS-1$
								first.equalsIgnoreCase("REP") || //$NON-NLS-1$
								first.equalsIgnoreCase("REPL") || //$NON-NLS-1$
								first.equalsIgnoreCase("REPLA") || //$NON-NLS-1$
								first.equalsIgnoreCase("REPLACE") || //$NON-NLS-1$
								first.equalsIgnoreCase("APP") || //$NON-NLS-1$
								first.equalsIgnoreCase("APPE") || //$NON-NLS-1$
								first.equalsIgnoreCase("APPEN") || //$NON-NLS-1$
								first.equalsIgnoreCase("APPEND")) { //$NON-NLS-1$
							firstArgBad = true;
							this.writeToMain(Messages.getString("SetSpool.filenamebad") + "\n", ctx); //$NON-NLS-1$ //$NON-NLS-2$
							usage(ctx, false, true);
							return true;
						}
					}
					args = ScriptUtils.executeArgs(realFile);
					if (args != null) {
						String processedRealFile = args[0];
						writeFail = false;
						if ((args.length > 2) || ((args.length > 1) && (!((args[1].equalsIgnoreCase("CRE") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("CREA") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("CREAT") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("CREATE") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("REP") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("REPL") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("REPLA") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("REPLACE") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("APP") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("APPE") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("APPEN") || //$NON-NLS-1$
								args[1].equalsIgnoreCase("APPEND")))))) { //$NON-NLS-1$
							usage(ctx, true, false);
						} else {
							String createReplaceAppend = "R"; //$NON-NLS-1$
							if (args.length > 1) {
								if ((args[1].equalsIgnoreCase("CRE") || //$NON-NLS-1$
										args[1].equalsIgnoreCase("CREA") || //$NON-NLS-1$
										args[1].equalsIgnoreCase("CREAT") || //$NON-NLS-1$
										args[1].equalsIgnoreCase("CREATE"))) { //$NON-NLS-1$
									createReplaceAppend = "C"; //$NON-NLS-1$
								} else if ((args[1].equalsIgnoreCase("APP") || //$NON-NLS-1$
										args[1].equalsIgnoreCase("APPE") || //$NON-NLS-1$
										args[1].equalsIgnoreCase("APPEN") || //$NON-NLS-1$
										args[1].equalsIgnoreCase("APPEND"))) { //$NON-NLS-1$
									createReplaceAppend = "A"; //$NON-NLS-1$
								}
							}
							if (processedRealFile.equalsIgnoreCase("out")) { //$NON-NLS-1$
								stopSpool(ctx);
								writeToMain(Messages.getString("SetSpool.printingnotsupported") + "\n", ctx); //$NON-NLS-1$ //$NON-NLS-2$
							} else {
								initFile(processedRealFile, ctx, cmd, createReplaceAppend);
							}
						}
					}
				} catch (ArgumentQuoteException aqe) {
					String theQuote = "'"; //$NON-NLS-1$
					if (realFile.length() > 1) {
						if (realFile.substring(0, 1).equals("\"")) { //$NON-NLS-1$
							theQuote = "\""; //$NON-NLS-1$
						}
					}
					this.writeToMain(
							MessageFormat.format(Messages.getString("SetSpool.missingquote"), realFile, theQuote) //$NON-NLS-1$
									+ "\n", //$NON-NLS-1$
							ctx);
					this.writeToMain(Messages.getString("SetSpool.cannotcreate") + "\n", ctx); //$NON-NLS-1$ //$NON-NLS-2$
					writeFail = false;// already written the cannot create.
				}
				if (writeFail) {
					this.writeToMain(Messages.getString("SetSpool.invalidfilename") + "\n", ctx); //$NON-NLS-1$ //$NON-NLS-2$
				}

			} else if ((cmd.getStmtSubType() == SQLCommand.StmtSubType.G_S_SPOOL)
					&& (cmd.getSql().trim().toLowerCase().equals("spool"))) { //$NON-NLS-1$
				spoolKeyword(ctx); // for SPOOL, and nothing else.
			} else {
				initFile("spool", ctx, cmd, "R"); // when //$NON-NLS-1$ //$NON-NLS-2$
													// SET SPOOL ON, but no file
													// defined
			}
		} else if ((cmd.getStmtSubType() == SQLCommand.StmtSubType.G_S_SPOOL)
				&& (cmd.getSql().trim().toLowerCase().equals("spool") || cmd.getSql().trim().toLowerCase().equals("spo") //$NON-NLS-1$ //$NON-NLS-2$
						|| cmd.getSql().trim().toLowerCase().equals("spoo"))) { //$NON-NLS-1$
			spoolKeyword(ctx); // for SPOOL, and nothing else.
		} else { // SET SPOOL OFF
			if (cmd.getSql().trim().split(" ").length > 2) { //$NON-NLS-1$
				usage(ctx, true, false);
			} else if (cmd.getSql().trim().split(" ").length == 2) { //$NON-NLS-1$
				stopSpool(ctx);
			}
		}
		return true;
	}

	private void usage(ScriptRunnerContext ctx, boolean illegal, boolean usage) {
		if (illegal) {
			ctx.write(Messages.getString("SetSpool.ILLEGALSPOOLANDUSAGE")); //$NON-NLS-1$
		} else if (usage) {
			ctx.write(Messages.getString("SetSpool.SPOOLUSAGE")); //$NON-NLS-1$
		}
	}

	private void spoolKeyword(ScriptRunnerContext ctx) {
		try {
			WrapListenBufferOutputStream existing = (WrapListenBufferOutputStream) ctx
					.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
			if (existing != null) {
				writeToMain(MessageFormat.format(Messages.getString("SetSpool.filedisplay"), //$NON-NLS-1$
						existing.getFileOutput().getName()), ctx);
			} else {
				writeToMain(Messages.getString("SetSpool.2"), ctx); //$NON-NLS-1$
			}
			writeToMain("\n\n", ctx); //$NON-NLS-1$
		} finally {
		}
	}

	public void writeToMain(String op, ScriptRunnerContext ctx) {
		WrapListenBufferOutputStream existing = ctx.getOutputStream();
		if (existing != null) {
			try {
				existing.write(op.getBytes("UTF8")); //$NON-NLS-1$
			} catch (UnsupportedEncodingException e) {
				Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
			} catch (IOException e) {
				Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
			}
		}
	}

	public static void writeToExisting(String op, ScriptRunnerContext ctx) {
		WrapListenBufferOutputStream existing = (WrapListenBufferOutputStream) ctx
				.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
		if (existing != null) {
			try {
				existing.write(op.getBytes("UTF8")); //$NON-NLS-1$
			} catch (UnsupportedEncodingException e) {
				Logger.getLogger("SetSpool").log(Level.WARNING, e.getStackTrace()[0].toString(), e); //$NON-NLS-1$
			} catch (IOException e) {
				Logger.getLogger("SetSpool").log(Level.WARNING, e.getStackTrace()[0].toString(), e); //$NON-NLS-1$
			}
		}
	}

	public void initFile(String filename, ScriptRunnerContext ctx, ISQLCommand cmd, String createReplaceAppend) {
		boolean written = false;
		String fullpath = ""; //$NON-NLS-1$
		filename = ctx.prependCD(filename);// if people have set cd they want to
											// use it.
		boolean fileExistsError = false;
		try {
			/* setUri(cmds[2]); */
			if ((filename.startsWith("/")) || (filename.startsWith("\\")) //$NON-NLS-1$ //$NON-NLS-2$
					|| ((filename.length() > 2) && (filename.charAt(1) == ':'))) {
				// assume I am a full path is /tmp/fred.sql or c:\fred.sql
				fullpath = filename;
			} else {
				// is the ctx url is not strange or tmporary use it
				URL myurl = ((URL) ctx.getProperty(ScriptRunnerContext.NODE_URL));
				if ((myurl != null) && (myurl.getProtocol().equalsIgnoreCase("file"))) { //$NON-NLS-1$
					FileOutputStream outStream = null;

					try {
						String localFullPath = null;
						;
						File f;
						try {
							f = new File(myurl.toURI());
						} catch (URISyntaxException e) {
							f = new File(myurl.getPath());
						}
						if (!(f.getParent().equals(System.getProperty("java.io.tmpdir")))) { //$NON-NLS-1$
							localFullPath = f.getParent() + File.separator + filename;
							localFullPath = localFullPath.replace('/', '\\');
							if (!this.doesParentExist(localFullPath)) {
								localFullPath = localFullPath.replace('\\', '/');
							}
							if (this.addDotLst(localFullPath)) {
								if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
									localFullPath = localFullPath + ".LST"; //$NON-NLS-1$
								} else {
									localFullPath = localFullPath + ".lst"; //$NON-NLS-1$
								}
							}

							if (createReplaceAppend.equals("C")) { //$NON-NLS-1$
								if (new File(localFullPath).exists()) {
									fullpath = localFullPath;
									fileExistsError = true;// probably really
															// want to report
															// this rather than
															// defaulting spool
															// to somewhere
															// temporary
									throw new IOException(fileExists);
								}
							}
							new File(localFullPath).createNewFile();
							File outputFile = new File(localFullPath);
							if (!outputFile.canWrite()) {
								throw new IOException();
							}
							fullpath = localFullPath;
							written = true;
						}
					} catch (Exception e) {
						// ignore errors outcome is written = false
					} finally {
						if (outStream != null) {
							try {
								outStream.close();
							} catch (Exception e1) {
								// ignore.
							}
						}
					}
				}
				if (written == false) {
					if (fileExistsError) {// 'normal' place of finding a file
											// has found a file in create mode.
						throw new IOException(fileExists);
					}
					String settingsDir = ctx.getSettingsDir();
					fullpath = settingsDir + File.separator + filename;
				}
			}

			// check if fullpath parent exists \ or /
			fullpath = fullpath.replace('/', '\\');
			if (!this.doesParentExist(fullpath)) {
				fullpath = fullpath.replace('\\', '/');
			}
			if (this.addDotLst(fullpath)) {
				if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
					fullpath = fullpath + ".LST"; //$NON-NLS-1$
				} else {
					fullpath = fullpath + ".lst"; //$NON-NLS-1$
				}
			}
			createNewSpoolOut(filename, fullpath, ctx, written, createReplaceAppend);

		} catch (IOException e) {
			//if there is a spool stop
			WrapListenBufferOutputStream existing = (WrapListenBufferOutputStream) ctx
					.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
			if (existing != null) {//do not want not spooling currently message hence check.
				stopSpool(ctx);
			}			
			if ((e.getMessage() != null) && (e.getMessage().equals(fileExists))) {
				this.writeToMain(MessageFormat.format(Messages.getString("SetSpool.fileExists") + "\n", fullpath), ctx); //$NON-NLS-1$ //$NON-NLS-2$
			} else {
				this.writeToMain(Messages.getString("SetSpool.invalidfilename") + "\n", ctx); //$NON-NLS-1$ //$NON-NLS-2$
			}
			ScriptUtils.doWhenever(ctx, cmd, ctx.getCurrentConnection(), false);
		}
	}

	public void stopSpool(ScriptRunnerContext ctx) {
		WrapListenBufferOutputStream existing = (WrapListenBufferOutputStream) ctx
				.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
		if (existing != null) {// existing one
			try {
				existing.flush();
			} catch (IOException e) {
				Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
			}
			ctx.getOutputStream().removeFromList(existing);
			try {
				existing.flush();
				existing.close();
			} catch (IOException e) {
				Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
			}
			ctx.putProperty(ScriptRunnerContext.SPOOLOUTBUFFER, null);
			ctx.putProperty(ScriptRunnerContext.SPOOLOUTFILENAME, null);
			Thread child = (Thread) (ctx.getProperty(ScriptRunnerContext.SPOOLTHREAD));
			try {
				if (child != null) {
					// edge case - child has not started
					for (int wait = 0; wait < 5; wait++) {
						if (!(child.getState().equals(Thread.State.NEW))) {
							break;
						}
						try {
							Thread.sleep(100);
						} catch (InterruptedException ie) {
							// ignore - loop around and getstate early
						} catch (IllegalArgumentException ie) {
							// never going to happen log it anyway
							Logger.getLogger(getClass().getName()).log(Level.WARNING, ie.getStackTrace()[0].toString(),
									ie);
						}
					}
					// put some sort of timeout in to avoid hangs
					child.join(2000);
				}
			} catch (InterruptedException ie) {
				Logger.getLogger(getClass().getName()).log(Level.WARNING, ie.getStackTrace()[0].toString(), ie);
			} finally {
				// worst case it runs a bit in the background
				ctx.putProperty(ScriptRunnerContext.SPOOLTHREAD, null);
			}
		} else {
			ctx.write("not spooling currently\n"); //$NON-NLS-1$
		}
	}

	private void createNewSpoolOut(String origFileName, String fullpath, ScriptRunnerContext ctx, boolean written,
			String createReplaceAppend) throws IOException {

		if (written == false) {// need to do a check
			if (createReplaceAppend.equals("C")) { //$NON-NLS-1$
				if (new File(fullpath).exists()) {
					throw new IOException(fileExists);
				}
			}
		}
		boolean isAppend = false;
		if (createReplaceAppend.equals("A")) { //$NON-NLS-1$
			isAppend = true;
		}
		// create a new file or NOOP.
		new File(fullpath).createNewFile();
		// crate a new WrapListenBufferOutputStream that can be passed around
		// and written to as a BufferedOutputStream
		String encodingSetting = ctx.getEncoding();
		WrapListenBufferOutputStream myout = null;
		// if we are \n and utf8 encoding we could short circuit this...
		WrapBufferOutputStreamToWriter wb = null;
		wb = new WrapBufferOutputStreamToWriter((BufferedOutputStream) null);
		wb.setTerminator(ctx.getStringTerminator());
		wb.setOut(ctx,
				new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fullpath, isAppend), encodingSetting)));
		// Putting in ctx: need ctx to read set trimspool, previously set to
		// null
		// if we have a filter set (currently on sqlcl only for ansi stripping
		// add it
		OutputStream filterIfAvailable = null;
		if (ctx.getSpoolFilterProvider() != null) {
			// get a warning here that wb is not closed Assuming wb is closed
			// whenever myout is closed
			filterIfAvailable = ctx.getSpoolFilterProvider().getFilterOutputStream(ctx, wb);
		} else {
			filterIfAvailable = wb;
		}
		myout = new WrapListenBufferOutputStream(new BufferedOutputStream(filterIfAvailable), ctx);

		myout.setFileOutput(new File(fullpath));
		myout.setRemoveForcePrint(true);
		WrapListenBufferOutputStream existing = (WrapListenBufferOutputStream) ctx
				.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
		if (existing != null) {// existing one
			// stop and restart restart wipes old spool file
			// only want to do this when we have a new buffer to put in.
			this.stopSpool(ctx);
			ctx.putProperty(ScriptRunnerContext.SPOOLOUTBUFFER, null);
			ctx.putProperty(ScriptRunnerContext.SPOOLOUTFILENAME, null);
		}
		ctx.putProperty(ScriptRunnerContext.SPOOLOUTBUFFER, myout);
		ctx.putProperty(ScriptRunnerContext.SPOOLOUTFILENAME, origFileName);
		ctx.putProperty(ScriptRunnerContext.SPOOLTHREAD, wb.getThread());
		ctx.getOutputStream().addToList(myout);
	}

	private boolean doesParentExist(String in) {
		int lastslash = in.lastIndexOf("/"); //$NON-NLS-1$
		int lastbslash = in.lastIndexOf("\\"); //$NON-NLS-1$
		int lastdir = lastbslash;
		if (lastslash > lastdir) {
			lastdir = lastslash;
		}
		if (lastdir == -1) {
			return false;
		}
		if (!((in.endsWith("/") || (in.endsWith("\\"))))) { //$NON-NLS-1$ //$NON-NLS-2$
			lastdir++;// include the last / or \ so .\fred handled correctly, as
						// . will always exist whether \ or / is right
		}
		return new File(in.substring(0, lastdir)).exists();
	}

	private boolean addDotLst(String in) {
		// special case - must be a better way.
		if (in.equals("/dev/null") || in.equals("\\dev\\null") || //$NON-NLS-1$ //$NON-NLS-2$
				in.equals("/dev/stderr") || in.equals("\\dev\\stderr") || //$NON-NLS-1$ //$NON-NLS-2$
				in.equals("/dev/stdout") || in.equals("\\dev\\stdout") || //$NON-NLS-1$ //$NON-NLS-2$
				in.equals("/dev/error") || in.equals("\\dev\\error")) { //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}
		int lastslash = in.lastIndexOf("/"); //$NON-NLS-1$
		int lastbslash = in.lastIndexOf("\\"); //$NON-NLS-1$
		int lastdot = in.lastIndexOf("."); //$NON-NLS-1$
		int lastdir = lastbslash;
		if (lastslash > lastdir) {
			lastdir = lastslash;
		}
		if (lastdot > lastdir) {
			return false;
		}
		return true;
	}

	@Override
	public void endScript(Connection conn, ScriptRunnerContext ctx) {
		super.endScript(conn, ctx);
		if (ctx.getTopLevel() == true) {
			this.stopSpool(ctx);
			this.setCmdOn(false);
		}
	}

}
