/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.sql.SQLException;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class SetStatementCache extends CommandListener {

  private static final String CMD="setstatementcache";
  
  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
      if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && 
              (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
          return false;
      }
   // check for our command
    if (cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith(CMD)  &&
          conn instanceof OracleConnection ) {
      String parts[] = cmd.getSql().toLowerCase().trim().split(" ");
      try {
       // if just on set it to 100
        if ( parts[2].equals("on") ) {
            ((OracleConnection)conn).setImplicitCachingEnabled(true);
            ((OracleConnection)conn).setStatementCacheSize(100);
            ctx.write("Statement Caching set to 100");
        } else  if ( parts[2].equals("off") ) {
            // off means off
            ((OracleConnection)conn).setImplicitCachingEnabled(true);
            ctx.write("Statement Caching disabled");
        } else {
          // other should be an int so parse it and check
          // // but if it fails make it 100 also
          Integer size = 100;        
          try {
            Integer.parseInt(parts[2]);
          } catch ( NumberFormatException nfe){
            
          }
          ((OracleConnection)conn).setImplicitCachingEnabled(true);
          ((OracleConnection)conn).setStatementCacheSize(size);
          // print to the output that it works
          ctx.write("Statement Caching set to " + size);

        }
      } catch (SQLException e) {
        ctx.write("Failed to enable Statement Caching" + e.getLocalizedMessage());
      }
      return true;
    }
    return false;
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    // TODO Auto-generated method stub
    
  }

  
  
}
