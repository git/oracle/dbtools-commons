/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.utils.TCPTNSEntry;
import oracle.dbtools.raptor.utils.TNSHelper;
import oracle.jdbc.pool.OracleDataSource;

/**
 * A simple sets the format to use when printing sql
 * 
 * 
 * @author klrice
 * 
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class SetTNSAdmin extends CommandListener implements IShowCommand {
	private boolean pingDB = false;

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

		// check for our command
		if (cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("settns")) {
			String[] parts = cmd.getSQLOrig().split(" ");
			if (parts.length < 3) {
				ctx.write(Messages.getString("TNS_USAGE"));
				return true;
			}
			if (parts[2].equalsIgnoreCase("off")) {
				System.setProperty("oracle.net.tns_admin", "");
			} else {
				// put the path back togeher in case there's spaces in the path
				StringBuilder sb = new StringBuilder();
				for (int i = 2; i < parts.length; i++) {
					sb.append(parts[i]).append(" ");
				}

				File f = new File(sb.toString().trim());
				if (f.exists() && f.isDirectory()) {
					ctx.write(MessageFormat.format(Messages.getString("TNS_LOCATION"), new Object[] { sb.toString().trim() }) + "\n");
					System.setProperty("oracle.net.tns_admin", sb.toString());
				} else {
					ctx.write(MessageFormat.format(Messages.getString("TNS_INVALID"), new Object[] { f.getAbsolutePath() }) + "\n");
				}
			}

			return true;
		}
		return false;
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.commands.show.IShow#handle(java.sql
	 * .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		ArrayList<TCPTNSEntry> entries = null;
		String message = null;
		/*
		 * This is the property which sets tns_admin directly. We need to do it
		 * first in show and then look elsewhere for the show command.
		 */
		if (System.getProperty("oracle.net.tns_admin") != null && System.getProperty("oracle.net.tns_admin").toString().length() > 0) {
			message = MessageFormat.format(Messages.getString("TNS_LOCATION"), new Object[] { System.getProperty("oracle.net.tns_admin") });
			ctx.write("");
			ctx.write(message + "\n");
			entries = TNSHelper.getTNSEntries();
			writeEntries(entries, ctx);
		} else {
			String[] bits = cmd.getSql().trim().split("\\s");
			if (bits.length == 2 && bits[1].equalsIgnoreCase("tnsping")) {
				pingDB = true;
			}
			entries = TNSHelper.getTNSEntries();

			Iterator<String> it = TNSHelper.getTNSLookupLocations().keySet().iterator();
			ctx.write("TNS Lookup locations\n--------------------\n");
			int i = 1;
			while (it.hasNext()) {
				String key = it.next();
				if (it.hasNext()) {
					ctx.write(MessageFormat.format("{0}.  {1}\n    {2}\n", new Object[] { i, key, TNSHelper.getTNSLookupLocations().get(key) }));
					i++;
				} else {
					ctx.write(MessageFormat.format("\nLocation used:\n-------------\n\t{0}\n", new Object[] { TNSHelper.getTNSLookupLocations().get(key) }));
				}

			}
			writeEntries(entries, ctx);

			return true;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.commands.show.IShow#getShowCommand(
	 * )
	 */
	@Override
	public String[] getShowAliases() {
		return new String[] { "tns", "tnsadmin", "tns_admin", "tnsping" };
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return true;
	}

	public static String test(String entry_name) {
		StringBuilder sb = new StringBuilder();
		Connection pconnection = null;
		long ping = 0;
		long start = 0;
		try {
			String l_url = TNSHelper.getEntry(entry_name).getJDBCUrl();
			// System.out.println( "Connection string = " + l_url );
			start = System.currentTimeMillis();

			OracleDataSource ods = new OracleDataSource();
			ods.setURL(l_url);
			pconnection = ods.getConnection();
			ping = System.currentTimeMillis() - start;

		} catch (SQLException e) {
			int errorCode = e.getErrorCode();
			// sb.append("Error Code: " + errorCode);
			if (errorCode == 12514) {
				sb.append("\tListener is UP but database is DOWN");
			}
			if (errorCode == 17002) {
				sb.append("\tListener is DOWN");
			}
			if (errorCode == 1017) {
				sb.append("\tListener is UP and database is UP\n");
				sb.append(MessageFormat.format("\tping {0}ms", new Object[] { ping }));
			}
		} finally {
			try {
				if (pconnection != null)
					pconnection.close();
			} catch (Exception e) {
			}
		}
		return sb.toString();
	}

	private void writeEntries(ArrayList<TCPTNSEntry> entries, ScriptRunnerContext ctx) {
		ctx.write("\nAvailable TNS Entries\n---------------------\n");
		ArrayList<String> names = new ArrayList<String>();
		Iterator<TCPTNSEntry> it2 = entries.iterator();

		while (it2.hasNext()) {
			TCPTNSEntry entry = it2.next();
			names.add(entry.getName());
		}
		Iterator<String> it3 = names.iterator();
		while (it3.hasNext()) {
			String name = TNSHelper.getEntry(it3.next()).getName();
			ctx.write(name + "\n");

			if (pingDB) {
				ctx.write(test(name) + "\n");
			}
		}
	}
}
