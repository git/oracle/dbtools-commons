/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.backgroundTask.IRaptorTaskProgressUpdater;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetTiming.java"
 *         >Barry McGillin</a>
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class SetTiming extends AForAllStmtsCommand {
	private long start;
	private long _runningTotal = 0;
	private IRaptorTaskProgressUpdater _currentTPU = null;
	private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_TIMING;

	public SetTiming() {
		super(m_cmdStmtSubType);
	}

	protected boolean isListenerOn(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		return true;
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		super.beginEvent(conn, ctx, cmd);
		if ((cmd.getStmtSubType() != null) && (cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_SHOW))) {
			// show maybe be show timing or show all so needs to know the
			// current state.
			Boolean amIOn = Boolean.FALSE;
			if ((isCmdOn() == true)) {
				amIOn = Boolean.TRUE;
			}
			ctx.putProperty(ScriptRunnerContext.SHOWTIMING, amIOn);
		}
	}

	@Override
	public void doBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (cmd.getStmtClass() == SQLCommand.StmtType.G_C_PLSQL || cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQL
				|| (cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQLPLUS && cmd.getStmtSubType() == SQLCommand.StmtSubType.G_S_EXECUTE)
				|| (cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQLPLUS && cmd.getStmtSubType() == SQLCommand.StmtSubType.G_S_AT)
				|| (cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQLPLUS && cmd.getStmtSubType() == SQLCommand.StmtSubType.G_S_START)) {
			if (!cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_DESCRIBE)) {
			start = System.currentTimeMillis();
			}
		}
	}

	@Override
	public void doEndWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (cmd.getStmtClass() == SQLCommand.StmtType.G_C_PLSQL || cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQL
				|| (cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQLPLUS && cmd.getStmtSubType() == SQLCommand.StmtSubType.G_S_EXECUTE)
				|| (cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQLPLUS && cmd.getStmtSubType() == SQLCommand.StmtSubType.G_S_AT)
				|| (cmd.getStmtClass() == SQLCommand.StmtType.G_C_SQLPLUS && cmd.getStmtSubType() == SQLCommand.StmtSubType.G_S_START)) {
			if (!cmd.getStmtSubType().equals(SQLCommand.StmtSubType.G_S_DESCRIBE)) {
				long stop = System.currentTimeMillis();
				long interval = stop - start;
				handleScriptTiming(ctx, interval);
				Object[] obj = { (interval) };
				long totalMilliseconds = stop - start;
				long remainderMilliseconds = totalMilliseconds % 1000;
				long totalSeconds = totalMilliseconds / 1000;
				long remainderSeconds = totalSeconds % 60;
				long totalMinutes = totalSeconds / 60;
				long remainderMinutes = totalMinutes % 60;
				long totalHoures = totalMinutes / 60;
				String formattedTime = leadingZeroIfLessThanTen(totalHoures) + ":" + leadingZeroIfLessThanTen(remainderMinutes) + ":" //$NON-NLS-1$  //$NON-NLS-2$ 
						+ leadingZeroIfLessThanTen(remainderSeconds) + "." + leadingTwoZerosIfLessThanTen(remainderMilliseconds); //$NON-NLS-1$

				String msg = MessageFormat.format(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.TIMINGELAPSED), formattedTime); //$NON-NLS-1$
				try {
					if (m_isCmdOn) {
						ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic(msg));
					}
				} catch (IOException e) {
					Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
				}
			}
		}
	}

	private void handleScriptTiming(ScriptRunnerContext ctx, long interval) {
		if (ctx != null && ctx.getTaskProgressUpdater() != null && ctx.getTaskProgressUpdater().getDescriptor() != null) {
			if (_currentTPU != ctx.getTaskProgressUpdater()) {// we are in a new
																// script runner
				_currentTPU = ctx.getTaskProgressUpdater();
				_runningTotal = 0;
			}
			_runningTotal = _runningTotal + interval;
			// update this for each command as we dont know when the last one
			// will be
			//ctx.getTaskProgressUpdater().getDescriptor().overrideElapseTime(_runningTotal);
		}
	}

	String leadingZeroIfLessThanTen(long in) {
		String prepend = ""; //$NON-NLS-1$
		if (in < 10) {
			prepend = "0"; //$NON-NLS-1$
		}
		return prepend + in;
	}

	String leadingTwoZerosIfLessThanTen(long in) {
		String prepend = ""; //$NON-NLS-1$
		if (in < 100) {
			prepend = "0"; //$NON-NLS-1$
		}
		if (in < 10) {
			prepend = "00"; //$NON-NLS-1$
		}
		return prepend + in;
	}


}
