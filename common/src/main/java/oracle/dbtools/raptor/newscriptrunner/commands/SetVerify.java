/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * @author Barry McGillin
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class SetVerify extends AForAllStmtsCommand implements IStoreCommand{
	private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_VERIFY;

	public SetVerify() {
		super(m_cmdStmtSubType);
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String toPrint = (String) cmd.getProperty(ISQLCommand.PROP_PRINTED_STRING);
		// print if echo off, on top level and verify has not printed me
		if ((ctx.isVerifyOn() && (toPrint != null) && (!toPrint.equals(""))) && !cmd.getStmtType().equals(StmtType.G_C_SQLPLUS)) { //$NON-NLS-1$
			ctx.write(toPrint);
			cmd.setProperty(ISQLCommand.PROP_PRINTED_STRING, ""); //$NON-NLS-1$ //so it is not printed twice
		}
		if (cmd.getStmtSubType() == m_cmdStmtSubType) {
			boolean isHandled = doHandleCmd(conn, ctx, cmd);
			if (isHandled && cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN) != null) {
				this.setCmdOn((Boolean) cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN));
			}
			return isHandled;
		} else if (isListenerOn(conn, ctx, cmd)) {
			return doHandleWatcher(conn, ctx, cmd);
		} else {
			return false;
		}
	}

	@Override
	public void doBeginWatcher(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String toPrint = (String) cmd.getProperty(cmd.PROP_PRINTED_STRING);
		// print if echo off, on top level and verify has not printed me
		if (ctx.isVerifyOn() && toPrint != null && (!toPrint.equals(""))) { //$NON-NLS-1$ //(not already printed)
			ctx.write(toPrint);
			cmd.setProperty(ISQLCommand.PROP_PRINTED_STRING, ""); //$NON-NLS-1$ //so it is not printed twice
		}
	}

	@Override
	public boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		ctx.setVerify((Boolean) cmd.getProperty(ISQLCommand.PROP_STATUS_BOOLEAN));
		return true;
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		return StoreRegistry.getCommand("verify", ctx.isVerifyOn()?"ON":"OFF");

	}
}
