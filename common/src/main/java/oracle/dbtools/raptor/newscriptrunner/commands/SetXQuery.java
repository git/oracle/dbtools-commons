/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;

import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptParser;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetXQuery.java">
 *         Barry McGillin</a>
 *
 */

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class SetXQuery extends CommandListener {
    private enum Handling {
        OK,
        NOOPTIONS,
        BADOPTION,
        BADVALUE;
    }
    
	public static final String XQUERY_BASEURI = "xquery.baseuri"; //$NON-NLS-1$
	public static final String XQUERY_ORDERING = "xquery.ordering"; //$NON-NLS-1$
	public static final String XQUERY_NODE = "xquery.node"; //$NON-NLS-1$
	public static final String XQUERY_CONTEXT = "xquery.context"; //$NON-NLS-1$

	public static final String help = "Usage: SET XQUERY {BASEURI text | CONTEXT text |\n" + " NODE {BYVALUE | BYREFERENCE | DEFAULT} |\n" //$NON-NLS-1$
			+ " ORDERING {UNORDERED | ORDERED | DEFAULT}}\n"; //$NON-NLS-1$

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// nothing to do here
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    if (!cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("setxquery")) {
      return false;
    }
		String[] parts = ScriptParser.getTokens(cmd.getSql().trim());
		if (parts.length < 2)
			return false;

		if (parts.length >= 2) {
			if (!(parts[0] + parts[1]).trim().toLowerCase().equals("setxquery")) { //$NON-NLS-1$
				return false;
			}
		}
        
	    Handling lastHandling = Handling.NOOPTIONS;
	    String   lastProperty = "";

		//Check that the options divid by 2 so we have an option and a value.
        handleparts:
		if (parts.length > 2) {
		    int i=2;
		    lastHandling = Handling.BADOPTION;
            
			for (; i+1<parts.length;i++) {
                lastProperty = parts[i];
                String value = parts[++i]; 
                
			    lastHandling = handleProperty(ctx, lastProperty, value);
                if (lastHandling != Handling.OK) {
                    break handleparts;
                }
			}
            
            if ((i!=parts.length)) {
                // handle last property
                lastProperty = parts[i];
                
                lastHandling = handleProperty(ctx, lastProperty, null);
            }
		}
        
        switch (lastHandling) {
            case BADOPTION:
                ctx.write(MessageFormat.format(Messages.getString("SETUNKNOWNOPTION"), new Object[] {lastProperty})); //$NON-NLS-1$
                break;
            case NOOPTIONS:
                ctx.write(Messages.getString("SetXQuery.1")); //$NON-NLS-1$
                // fall-thru
            case BADVALUE:
                ctx.write(help);
                break;
        }

		return true;
	}
    
    private Handling handleProperty(ScriptRunnerContext ctx, String property, String value) {
        Handling result = Handling.BADVALUE;
        String lowerValue = (value != null) ? value.toLowerCase() : "";
        property = property.toLowerCase();
        
        if ("baseuri".equals(property)) { //$NON-NLS-1$
            ctx.putProperty(XQUERY_BASEURI, unQuote(value));
            result = Handling.OK;
        } else if ("context".equals(property)) { //$NON-NLS-1$
            ctx.putProperty(XQUERY_CONTEXT, unQuote(value));
            result = Handling.OK;
        } else if ("node".equals(property)) { //$NON-NLS-1$
            if ("byvalue".equals(lowerValue) || "byreference".equals(lowerValue) || "default".equals(lowerValue)) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                ctx.putProperty(XQUERY_NODE, value.toUpperCase());
                result = Handling.OK;
            }
        } else if ("ordering".equals(property)) { //$NON-NLS-1$
            if ("unordered".equals(lowerValue) || "ordered".equals(lowerValue) || "default".equals(lowerValue)) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                ctx.putProperty(XQUERY_ORDERING, value.toUpperCase());
                result = Handling.OK;
            }
        } else {
            result = Handling.BADOPTION;
        }
        
        return (result == Handling.OK && value == null) ? Handling.BADVALUE : result;
    }
    
    private String unQuote(String str) {
        StringBuilder sb;
        
        if (str == null || str.length() == 0) {
            sb = new StringBuilder();
        } else {
            sb = new StringBuilder(str);

            final char firstChar = sb.charAt(0);
            
            if (firstChar == '\'' || firstChar == '"') {
                sb.deleteCharAt(0);
                
                for (int i = 0; i < sb.length(); i++) {
                    if (sb.charAt(i) == firstChar) {
                        if (i == sb.length()-1 || sb.charAt(i+1) == firstChar) {
                            sb.deleteCharAt(i);
                        }
                    }
                }
            }
        }
    
        return sb.toString();
    }
}
