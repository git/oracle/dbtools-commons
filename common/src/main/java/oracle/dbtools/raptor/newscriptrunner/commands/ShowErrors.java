/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE) 
public class ShowErrors extends CommandListener implements IShowCommand{
    private static final String CMD = "show err"; //$NON-NLS-1$
    private static final String SHOCMD = "sho err"; //$NON-NLS-1$
    private static final String VERBOSE_CMD = "set errorswithlines"; //$NON-NLS-1$
    private static final String VERBOSE_CMD_NUM_LINES = "set errorswithlines num lines"; //$NON-NLS-1$
    private ISQLCommand _lastCmd;
    
    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
      
        if (!(cmd.getLoweredTrimmedSQL().startsWith(CMD)||
        		cmd.getSql().trim().toLowerCase().startsWith(SHOCMD))) {
            _lastCmd = cmd;
        }
    }
    
    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    	int linesize = ((Integer) ctx.getProperty(ScriptRunnerContext.SETLINESIZE)).intValue();

        if (cmd.getLoweredTrimmedSQL().startsWith(VERBOSE_CMD)) {
            String[] parts = cmd.getSQLOrig().split(" "); //$NON-NLS-1$
            if (parts.length >= 3 && parts[ 2 ].toLowerCase().trim().equals("on")) { //$NON-NLS-1$
                ctx.putProperty(VERBOSE_CMD, Boolean.TRUE);
            } else {
                ctx.putProperty(VERBOSE_CMD, Boolean.FALSE);
            }
            if (parts.length == 5) {
                ctx.putProperty(VERBOSE_CMD_NUM_LINES, Integer.parseInt(parts[ 3 ]));
            }
        }
        if (cmd.getLoweredTrimmedSQL().startsWith(CMD)||
        		cmd.getLoweredTrimmedSQL().startsWith(SHOCMD)) {
            if ((ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && 
            (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
                ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOT_CONNECTED)+"\n"); //$NON-NLS-1$);
                return true;
            }
            
            String ret = DBUtil.getInstance(conn).executeReturnOneCol("select count(1) cnt from all_errors where owner = SYS_CONTEXT ('USERENV', 'CURRENT_SCHEMA')");
            if ( ret.equals("0")){
              return true;
            }
            
            String[] parts = cmd.getSQLOrig().split(" "); //$NON-NLS-1$
            if (parts!=null && parts.length==2 && !parts[1].toLowerCase().equals("err")&&!parts[1].toLowerCase().equals("error")&&!parts[1].toLowerCase().equals("errors")) return false;
            boolean lastCommand = false;
            boolean linesGot = false;
            //show errors might have name and type remove show err[^\\s]*\\s* then look for each type
            String moreArgs = cmd.getSql().trim().replaceAll("^[sS][hH][oO][wW]?\\s+[eE][rR][rR][^\\s]*", "").trim();  //$NON-NLS-1$ //$NON-NLS-2$
            String[] splitMoreArgs=moreArgs.toUpperCase().split("\\s+");  //$NON-NLS-1$
            String name = null;
            String dname = null;
            String type = null;
            if (splitMoreArgs.length>1) {
                String[] targetDouble={"PACKAGE BODY", "TYPE BODY", "JAVA CLASS", "JAVA SOURCE"};  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
                String[] target={"FUNCTION","PROCEDURE", "PACKAGE", "TRIGGER",  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
                        "VIEW", "TYPE", "DIMENSION"}; //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$ 
                String[][] bothTargets={targetDouble,target};
                boolean matchOrEnd=false;
                boolean withBody = false;
                for (String[] targets:bothTargets) {
                    for (String aTarget: targets) {
                        String oneOrTwo = splitMoreArgs[0];
                        withBody = false;
                        if (aTarget.contains(" ")){  //$NON-NLS-1$
                            oneOrTwo=(splitMoreArgs[0]+" "+splitMoreArgs[1]);  //$NON-NLS-1$
                            withBody=true;
                        }
                        if (oneOrTwo.equals(aTarget)) {
                            matchOrEnd=true;
                            type = aTarget;
                            break;
                        }
                    }
                    if (matchOrEnd) {
                        break;
                    }
                }
                if (type!=null) {
                    if (withBody) {
                        moreArgs=moreArgs.replaceAll("^[^\\s]*","").trim();  //$NON-NLS-1$  //$NON-NLS-2$
                        //remove one words
                    } //remove second word
                    moreArgs=moreArgs.replaceAll("^[^\\s]*","").trim(); //$NON-NLS-1$  //$NON-NLS-2$
                    name = moreArgs;
                } 
            }
            if (((type==null)&&(moreArgs.length()>0))|
                    (!(type==null) && ((name == null)||(name.equals(""))))) { //$NON-NLS-1$
                //if more args trimed has size and could not find type syntax error 
                ctx.write("Usage: SHOW ERRORS [{ FUNCTION | PROCEDURE | PACKAGE |\n"+ //$NON-NLS-1$
                        "    PACKAGE BODY | TRIGGER | VIEW\n"+ //$NON-NLS-1$
                        "    | TYPE | TYPE BODY | DIMENSION\n"+ //$NON-NLS-1$
                        "    | JAVA SOURCE | JAVA CLASS } [schema.]name]\n"); //$NON-NLS-1$
                return true;
            }
            if (name == null || type == null) {
                // CONFIRM_YES, CONFIRM_NO or CONFIRM_CANCEL.
                // grab the name/type from the previous sql cmd
                name = (String) ctx.getProperty(ScriptRunnerContext.LAST_ERR_NAME);
                if (ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null && Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())) {
                	if (name != null && name.length()>0 && name.lastIndexOf(".")!=-1) {
                		name=name.substring(name.lastIndexOf(".")+1, name.length());
                	}
                } 
                //hack - do not double quote if all uppercase.
                dname="";  //$NON-NLS-1$
                if (name!=null) {
                     String[] vals=name.split("\\.");
                
                    for (String val:vals) {
                        if (!(val.toUpperCase(Locale.US).equals(val))) {
                            val="\""+val+"\"";  //$NON-NLS-1$  //$NON-NLS-2$
                        } 
                    dname+=val+"."; //$NON-NLS-1$ 
                    }
                    if (dname.contains(".")) {  //$NON-NLS-1$ 
                        dname=dname.substring(0,dname.lastIndexOf("."));  //$NON-NLS-1$ 
                    }
                }
                type = (String) ctx.getProperty(ScriptRunnerContext.LAST_ERR_TYPE);
                if (name == null || type == null) {
                    ctx.write("No errors.\n"); //$NON-NLS-1$
                    return true;
                }
                lastCommand = true;
            }
            //Bug 21080716 report on strange combination - stops with .
            if (name.endsWith(".")) { //$NON-NLS-1$
                ctx.write(MessageFormat.format(Messages.getString("RESOLVE_FAILED"),name)+"\n"); //$NON-NLS-1$ //$NON-NLS-2$
                return true;
            }
            // get the resolved name true for get package body if packag does not exist.
            String useDname=dname;
            if (dname==null) {
                useDname=name;
            }
            Map<String, String> resolved = DBUtil.getInstance(conn).resolveName(useDname,true,false);
            // get the name/owner/type from resolution
            boolean badName=false;
            if ((resolved == null&&name!=null)) {
            	//sqlplus gives different error if an object by that name is not possible
            	//This is a first attempt at dealing with negative testing.
            	//sqlplus seems to have its own rules on when is no errors and when is object is invalid.
            	// Bug 25111092 - DESC A PACKAGE DOES NOT DISPLAY THE CORRECTLY DATA TYPE FOR PROCEDURE PARAMETERS 
            	DBUtil dbutil=DBUtil.getInstance(conn);
            	SQLException e=DBUtil.getInstance(conn).getLastException();
            	//do I report other peoples issues? NO
            	ArrayList<String> binds=new ArrayList<String>();
            	boolean moreThanTwo=false;
            	if (useDname.contains(".")) { //$NON-NLS-1$
            		String[] two=(useDname.split("\\."));
            		if (two.length!=2) {
            			moreThanTwo=true;
            		}else {
            			binds.add(two[0]);
            			binds.add(two[1]);
            			dbutil.execute("declare x varchar2(1000); s varchar2(1000);  begin s:=DBMS_ASSERT.SCHEMA_NAME(:a);x:=DBMS_ASSERT.SIMPLE_SQL_NAME(:x); end;",binds);  //$NON-NLS-1$
            			e=dbutil.getLastException();
            		}
            	} else {
            		binds.add(useDname);
            		dbutil.execute("declare x varchar2(1000);  begin x:=DBMS_ASSERT.SIMPLE_SQL_NAME(:x); end;",binds);  //$NON-NLS-1$
            		e=dbutil.getLastException();
            	}

            	if (e!=null||moreThanTwo) {//should really check error code - note simple_sql_name does not check length
            		badName=true;
            	} else {
            		badName=false;//the default
            	}
            }
            if (resolved==null) {
                if (badName){
                    //SP2-0564: Object "'test12345'" is INVALID, it may not be described.
                    //note double quoted will be double quoted assumption - bad double quoted names infrequent
                    ctx.write(MessageFormat.format(Messages.getString("BADOBJECTNAME"),useDname)); //$NON-NLS-1$ 
                    return true;
                } else {
                    ctx.write("No errors.\n"); /* odd but sqlplus -> no errors for cannot find */ //$NON-NLS-1$ 
                    return true;
                }
            }
            if ((type.toUpperCase().indexOf("BODY") > 0)&&(resolved.get("OBJECT_TYPE").toUpperCase().indexOf("BODY")==-1)) {//i.e. do not have 2 bodys //$NON-NLS-1$  //$NON-NLS-2$ //$NON-NLS-3$
                resolved.put("OBJECT_TYPE", resolved.get("OBJECT_TYPE") + " BODY"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            } else {
            	resolved.put("OBJECT_TYPE", resolved.get("OBJECT_TYPE").toUpperCase()); //$NON-NLS-1$ //$NON-NLS-2$
            }
            
            String val = DBUtil.getInstance(conn).executeReturnOneCol("SELECT value FROM v$parameter WHERE name = 'plsql_warnings'");
            String sql = "select substr(line||'/'||position,0,10) linenum ,text from all_errors " + " where  type = :TYPE " + " and    name = :OBJECT_NAME " + " and    owner = :OWNER" + " and    attribute != 'WARNING' order by line asc, position, sequence desc"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

            if (val!=null && val.equalsIgnoreCase("ENABLE:ALL")) sql = "select substr(line||'/'||position,0,10) linenum ,text from all_errors " + " where  type = :TYPE " + " and    name = :OBJECT_NAME " + " and    owner = :OWNER" + " order by line asc, position, sequence desc"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
            List<String> binds = new ArrayList<String>();
            binds.add(resolved.get("OBJECT_TYPE")); //$NON-NLS-1$
            binds.add(resolved.get("OBJECT_NAME")); //$NON-NLS-1$
            binds.add(resolved.get("OWNER")); //$NON-NLS-1$
            ResultSet rs = null;
            List<String> line = new ArrayList<String>();
            List<String> txt = new ArrayList<String>();
            try {
                DBUtil dbUtil = DBUtil.getInstance(conn);  
                if (LockManager.lock(conn)) {
                    rs = dbUtil.executeQuery(sql, binds);
                    while (rs.next()) {
                        line.add(rs.getString(1));
                        txt.add(rs.getString(2));
                    }
                }
            }  catch (SQLException e) {
                ctx.write(e.getMessage());
            } finally {
            	DBUtil.closeResultSet(rs);
            	LockManager.unlock(conn);
            }
            String[] lines = null;
            if (line.size() > 0) {
            //translate
                line.add(0,Messages.getString("LINE_COL")); //$NON-NLS-1$
                txt.add(0,Messages.getString("ERROR")); //$NON-NLS-1$
                int lSize = 5;//low to ensure it is checking LINE_COL size
                int tSize = 10;
                int ttSize = 10;//length of longest line after split \n
                for (String l: line) {
                    if (l.length() > lSize) {
                        lSize = l.length();
                    }
                }
                for (String t: txt) {
                    if (t.length() > tSize) {
                        tSize = t.length();
                    }
                    for (String tt: t.split(("\\r?\\n"))) { //$NON-NLS-1$
                        if ((tt!=null)&&(tt.length()>ttSize)) {
                            ttSize=tt.length();
                        }
                    }
                }
                lSize += 1; // add 1 spaces to space the message
                String pad = "                                       "; //$NON-NLS-1$
                //note name includes schema - minor issue (improvement?) will not fix until bugged.
                ctx.write("\n"+MessageFormat.format(Messages.getString("ERRORS_FOR_TYPE_NAME")/*"Errors for {0} {1}:\n"*/+"\n\n",type,useDname)); flush(ctx);
                
                StringBuffer minuses=new StringBuffer(""); //$NON-NLS-1$
                for (int i=1;i<lSize+5;i++){
                    minuses.append("-"); //$NON-NLS-1$
                }
                //minor issue ttSize can be greater than LINESIZE.
                line.add(1,minuses.substring(0,lSize-1)+" "); //$NON-NLS-1$
                minuses=new StringBuffer(""); //$NON-NLS-1$
                int errminues = ttSize+10;
   //             if (linesize==80) errminues=65; //Regressions.   
                for (int i=1;i<errminues+5;i++){
                    minuses.append("-");
                }
                // Bug 23104059
                // Make sure the results of show error conform to the Linesize(show linesize command).
                // If linesize < 11 the wordwrapping for the ERROR text won't work
                // and it becomes impossible to read. The results will reflect the min. linesize set to 11.
                linesize = linesize < 11 ? 11 : linesize;
                // Rest of the linesize after the first column is done.
                int adjls = linesize - lSize;
                //if adjls > max text ttsize and ttsize>Messages.getString("ERROR").length adjls=ttsize
                int adjlsMin = adjls;
                if (adjlsMin>65) {
                    //actually enforced in sqlplus by a column default setting
                    adjlsMin=65;
                }
                if (adjlsMin<minuses.length()) {
                    txt.add(1,minuses.substring(0, adjlsMin));
                } else {
                    txt.add(1,minuses.toString());
                }
                for (int i = 0; i < line.size(); i++) {
                    ctx.write(line.get(i) + pad.substring(0, lSize - line.get(i).length()));flush(ctx);
                    boolean firstOne=true;
                    String lterm = System.getProperty("line.separator");
                    //should line up the nth line
                    for (String onetxt:txt.get(i).split("\\r?\\n",-1)) { //$NON-NLS-1$
                       	if (onetxt!=null && onetxt.length() > 0) {
                           onetxt=onetxt.trim();
                           onetxt = ScriptUtils.wordwrap(12, adjls, new StringBuffer(onetxt), -1).toString();
                           // // Bug 23104059
                           // Pad with spaces to the left of the string for the second column.
                           onetxt = onetxt.replaceAll(lterm, lterm + String.format("%1$-" + lSize + "s", " "));
                           if (!firstOne) {
                               ctx.write(pad.substring(0,lSize));flush(ctx);
                           } else {
                               firstOne=false;
                           }
                           ctx.write(onetxt + "\n");flush(ctx); //$NON-NLS-1$
                       } else {
                           if (firstOne) {
                               ctx.write("\n");flush(ctx); //$NON-NLS-1$
                               firstOne=false;
                           }
                       }
                    }
                    
                    // this will print the before/after lines
                    // of where the error happened
                    Boolean verbose_cmd = (Boolean)ctx.getProperty(VERBOSE_CMD);
                    if (verbose_cmd != null && verbose_cmd ) {
                        if (lastCommand==true) {
                            lines =  _lastCmd.getSQLOrig().split("\n"); //$NON-NLS-1$
                        }
                        else {
                            if (linesGot == false) {
                                linesGot = true;
                                List<String> bindsLines = new ArrayList<String>();
                                bindsLines.add(resolved.get("OBJECT_TYPE")); //$NON-NLS-1$
                                bindsLines.add(resolved.get("OBJECT_NAME")); //$NON-NLS-1$
                                bindsLines.add(resolved.get("OWNER")); //$NON-NLS-1$
                                String sqlLines = "select text from all_source " + " where  type = :TYPE " + " and    name = :OBJECT_NAME " + " and    owner = :OWNER order by line"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                                List<String> lineList = new ArrayList<String>();
                                try {
                                    DBUtil dbUtil = DBUtil.getInstance(conn);
                                    if (LockManager.lock(conn)) {
                                        rs = dbUtil.executeQuery(sqlLines, bindsLines);

                                        while (rs.next()) {
                                            lineList.add(rs.getString(1));
                                        }
                                        lines = (String[]) lineList.toArray(new String[ lineList.size() ]);
                                        if (lineList.size() == 0) {
                                            lines = null;
                                        }
                                    }
                                } catch (SQLException e) {
                                } finally {
                                    if (rs != null) {
                                    	DBUtil.closeResultSet(rs);
                                    }
                                    LockManager.unlock(conn);
                                }
                                if (lines == null) {
                                    ctx.write(MessageFormat.format
                                              (Messages.getString("TYPE_NAME_SOURCE_NOT_FOUND"), //$NON-NLS-1$
                                               new Object[] {resolved.get("OBJECT_TYPE"),resolved.get("OWNER")+"."  //$NON-NLS-1$  //$NON-NLS-2$
                                                  +resolved.get("OBJECT_NAME")}));flush(ctx);  //$NON-NLS-1$
                                }
                            }
                        }
                    }
                    if ((verbose_cmd != null && verbose_cmd && lines!=null)) {
                        Integer showLines = 1;
                        if (ctx.getProperty(VERBOSE_CMD_NUM_LINES) != null) {
                            showLines = (Integer) ctx.getProperty(VERBOSE_CMD_NUM_LINES);
                            showLines = showLines - 1; // adjust for zero vs 1 array counting
                        }
                        int linenum = Integer.parseInt(line.get(i).substring(0, line.get(i).indexOf("/"))); //$NON-NLS-1$
                        // plsql tracks starting at 1 the array will be starting at zero
                        linenum = linenum - 1;
                        int colnum = Integer.parseInt(line.get(i).substring(line.get(i).indexOf("/") + 1)); //$NON-NLS-1$
                        // print the before lines.
                        for (int ii = showLines; ii >= 0; ii--) {
                            if (linenum - 1 - ii >= 0) {
                                ctx.write((linenum - ii) + "  " + lines[ linenum - 1 - ii ] + "\n"); flush(ctx);//$NON-NLS-1$ //$NON-NLS-2$
                            }
                        }
                        // print and * the error line
                        // then in the line below mark the col with ^
                        if (linenum > 0 && linenum < lines.length) {
                            ctx.write((linenum + 1) + "* " + lines[ linenum ] + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
                            String x100 = "                                                                                                    "; //$NON-NLS-1$
                            int hundreds = (int) Math.round(Math.floor(colnum / 100));
                            // padd for the length of the line num
                            String l = new String();
                            l = "" + linenum + "  "; //$NON-NLS-1$ //$NON-NLS-2$
                            ctx.write(x100.substring(0, l.length()));flush(ctx);
                            for (int ii = 0; ii < hundreds; ii++) {
                                ctx.write(x100);flush(ctx);
                            }
                            ctx.write(x100.substring(0, (colnum % 100)) + "^\n"); flush(ctx);//$NON-NLS-1$
                        }
                        // print the after lines
                        for (int ii = 1; ii < showLines + 2; ii++) {
                            if (linenum + ii < lines.length) {
                                ctx.write((linenum + 1 + ii) + "  " + lines[ linenum + ii ] + "\n"); flush(ctx);//$NON-NLS-1$ //$NON-NLS-2$
                            }
                        }
                        ctx.write("\n\n");flush(ctx); //$NON-NLS-1$
                    }
                }
            } else {
                ctx.write(Messages.getString("ShowErrors.34")); flush(ctx);//$NON-NLS-1$
            }
            return true;
        }
        return false;
    }
    
    private void flush(ScriptRunnerContext ctx) {
        try {
			ctx.getOutputStream().flush();
		} catch (IOException e1) {
		}
	}

	public void beginScript(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }
    
    public void endScript(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }

	@Override
	public String[] getShowAliases() {
		return new String[] {"err","erro","error","errors"};
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		return handleEvent(conn, ctx, cmd);
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return false;
	}
 
}
