/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowAppicmd;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowArraycmd;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowAutocommit;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowAutoprint;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowAutorecovery;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowAutotrace;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowBlockTerminator;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowColsep;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowConcat;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowCopyc;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowDefine;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowDescribe;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowEcho;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowEscape;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowFeedback;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowHeadsep;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowLinesize;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowLong;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowNewPage;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowNull;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowNumformat;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowNumwidth;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowPagesize;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowPause;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowSQLBlankLines;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowSqlprompt;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowSuffix;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowTermout;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowTimingout;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowTrimout;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowTrimspool;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowVerify;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowWrap;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowLongChunkSize;
/**
 * @author bamcgill
 *
 *  This is a simple registry to handle all the store ABC commands.
 *  
 */
public class StoreRegistry {

  private static ConcurrentHashMap<String, IStoreCommand> s_registry = new ConcurrentHashMap<String,IStoreCommand>();

  private static void put(Class<? extends IStoreCommand>  istore) {
    try {
    	IStoreCommand store = istore.newInstance();
      register(store);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public static Iterator<IStoreCommand> iterator() {
	  return s_registry.values().iterator();
  }

  public static void register(IStoreCommand istore) {
    s_registry.put(istore.getClass().getName().toUpperCase(), istore);
  }
  
  static {
      put(SetColInvisble.class);
      put(ShowAppicmd.class);   
      put(ShowArraycmd.class);  
      put(ShowAutocommit.class);    
      put(ShowAutoprint.class); 
      put(ShowAutotrace.class);
      put(ShowBlockTerminator.class);
      put(BottomTitle.class);
      put(TopTitle.class);
      put(ShowColsep.class);    
      put(ShowCopyc.class); 
      put(ShowEcho.class);
      put(ShowEscape.class);    
      put(ShowFeedback.class);  
      put(ShowDefine.class);   
      put(ShowHeadsep.class);  
      put(ShowLinesize.class);   
      put(ShowLong.class);  
      put(ShowLongChunkSize.class); 
      put(ShowNewPage.class);
      put(ShowNull.class); 
      put(ShowConcat.class); 
      put(ShowNumformat.class); 
      put(ShowNumwidth.class);  
      put(ShowPagesize.class);  
//      put(ShowParameter.class); 
//      put(ShowPrelim.class); 
//      put(ShowShowMode.class);  
//      put(ShowRecSep.class);
//      put(ShowRecSepChar.class);
//      put(ShowRepheader.class);
//      put(ShowRepfooter.class);
//      put(ShowRecyclebin.class);    
//      put(ShowRelease.class);   
      put(ShowSQLBlankLines.class);
//      put(ShowSQLNumber.class); 
//      put(ShowScan.class);  
//      put(ShowSecuredCol.class);
//      put(ShowServeroutput.class);  
//      put(ShowShowMode.class);
//      put(ShowSpace.class); 
//      put(ShowSpool.class); 
//      put(ShowSpparameter.class);   
//      put(ShowSqlcode.class);   
      put(ShowSqlprompt.class); 
      put(ShowSuffix.class);
      put(ShowTermout.class);   
      put(ShowTimingout.class); 
      put(ShowTrimout.class);   
      put(ShowTrimspool.class); 
      put(ShowVerify.class);    
      put(ShowWrap.class);  
      put(ShowDescribe.class);
  }

public static String getCommand(String string, String string2) {

	return MessageFormat.format("set {0} {1}\n",string,string2);
}
}
