/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.utils.DataTypesUtil;
import oracle.sql.NUMBER;


/**
 * 
 * @author Ramprasad Thummala
 *
 */
public class Title extends CommandListener implements IStoreCommand{

	protected static final String m_lineSeparator = System.getProperty("line.separator");
	private static final String LEFTPTRN = "(?i:\\bl(?:e|ef|eft)\\b)"; //"(?i:left\\s+)";
	private static final String LEFTCMDPTRN = "(?i:" + LEFTPTRN +"\\s+(?:\'[^\']+\'\\s+(?i:sql.pno)|\'[^\']+\'|(?i:sql.pno)))";
	private static final String RIGHTPTRN = "(?i:\\br(?:|i|ig|igh|ight)\\b)"; //(?i:right\\s+)";
	private static final String RIGHTCMDPTRN = "(?i:" + RIGHTPTRN + "\\s+(?:\\\'[^\\\']+\\\'\\s+(?i:sql.pno)|\\\'[^\\\']+\\\'|(?i:sql.pno)))";
	private static final String CENTERPTRN = "(?i:\\bc(?:e|en|ent|ente|enter)\\b)";  //"(?i:center\\s+)";
	private static final String CENTERCMDPTRN = "(?i:" + CENTERPTRN + "\\s+(?:\\\'[^\\\']+\\\'\\s+(?i:sql.pno)|\\\'[^\\\']+\\\'|(?i:sql.pno)))";
	private static final String SKIPPTRN =  "(?i:\\bs(?:|k|ki|kip)\\b)"; //"(?i:skip(?:\\s+|\\z))"; 
	private static final String SKIPPTRN_HAS_NUM =  "(?i:\\bs(?:|k|ki|kip)\\d{1,2}\\b)"; //"(?i:skip(?:\\s+|\\z))"; 
	private static final String SKIPCMDPTRN =  "(?i:" + SKIPPTRN + "(?:|\\s+)(?:\\d+|\\z))"; //"(?:(?i:skip(?:\\s+|\\z)(?:\\d+|\\D+))|(?i:skip))";
	private static final String FORMATPTRN = "(?i:\\bformat\\b)";
	private static final String COLPTRN = "(?i:\\bcol\\b)";
	private static final String COLCMDPTRN = "(?:" + COLPTRN + "\\s+(?:|" + FORMATPTRN + "\\s+)" + "(?:\'[^\']+\'|\'[^\']+\'\\s+(?i:sql.pno)|.+))";
	private static final String FONTPTRN = "(?i:\\bfont\\b)";
	private static final String TABPTRN = "(?i:\\btab\\b)";

	
	private static final String JUSTIFY = "justify"; // LEFT|CENTER|RIGHT
	private static final String COL = "col"; // COL
	private static final String TAB = "tab"; // TAB
	private static final String FORMAT = "format"; // FORMAT
	private static final String SKIP   = "skip"; // SKIP
	private static final String SQLPNO = "SQL.PNO"; // VARIABLE
	private static final String SQLSCAN = "SQL.SCAN"; // VARIABLE
	private static final String BOLD = "BOLD"; // VARIABLE
	private static final int SKIP_COLUMN_HEADER_LINES = 3; // maximum number of skip lines.

	private static final String SQLPAGESIZE = "SQL.PAGESIZE"; // VARIABLE
	private static final String SQLUSER = "SQL.USER"; // VARIABLE
	private static final String SQLCASE = "SQL.CASE"; // VARIABLE
	private static final String SQLSQLCASE = "SQL.SQLCASE"; // VARIABLE
	private static final String SQLDEFINE = "SQL.DEFINE"; // VARIABLE
	private static final String SQLCMDSEP = "SQL.CMDSEP"; // VARIABLE
	private static final String SQLSQLPREFIX = "SQL.SQLPREFIX"; // VARIABLE
	private static final String SQLLINESIZE = "SQL.LINESIZE"; // VARIABLE
	private static final String SQLSQLCODE = "SQL.SQLCODE"; // VARIABLE
	private static final String SQLOSCODE = "SQL.OSCODE"; // VARIABLE
	private static final String SQLSPACE = "SQL.SPACE"; // VARIABLE
	private static final String SQLNUMFORMAT = "SQL.NUMFORMAT"; // VARIABLE
	private static final String SQLNUMWIDTH = "SQL.NUMWIDTH"; // VARIABLE
	private static final String SQLSUFFIX = "SQL.SUFFIX"; // VARIABLE
	private static final String SQLPREFIX = "SQL.PREFIX"; // VARIABLE
	private static final String SQLLNO = "SQL.LNO"; // VARIABLE
	private static final String SQLLOGSOURCE = "SQL.LOGSOURCE"; // VARIABLE
	private static final String SQLSPOOL = "SQL.SPOOL"; // VARIABLE
	private static final String SQLHEADING = "SQL.HEADING"; // VARIABLE
	private static final String SQLVERIFY = "SQL.VERIFY"; // VARIABLE
	private static final String SQLLONG = "SQL.LONG"; // VARIABLE
	private static final String SQLRELEASE = "SQL.RELEASE"; // VARIABLE
	private static final String SQLSQLPROMPT = "SQL.SQLPROMPT"; // VARIABLE
	private static final String SQLNULL = "SQL.NULL"; // VARIABLE
	private static final String SQLFEEDBACK = "SQL.FEEDBACK"; // VARIABLE
	private static final String SQLTERMOUT = "SQL.TERMOUT"; // VARIABLE
	private static final String SQLTIMING = "SQL.TIMING"; // VARIABLE
	private static final String SQLTRIMOUT = "SQL.TRIMOUT"; // VARIABLE
	private static final String SQLAUTORECOVERY = "SQL.AUTORECOVERY"; // VARIABLE
	private static final String SQLECHO = "SQL.ECHO"; // VARIABLE
	private static final String SQLHEADSEP = "SQL.HEADSEP"; // VARIABLE
	private static final String SQLWRAP = "SQL.WRAP"; // VARIABLE
	private static final String SQLESCAPE = "SQL.ESCAPE"; // VARIABLE
	private static final String SQLAUTOCOMMIT = "SQL.AUTOCOMMIT"; // VARIABLE
	private static final String SQLPAUSE = "SQL.PAUSE"; // VARIABLE

	private static final String EMPTYSTRING = ""; // VARIABLE
	private static final String ORA_SUBSTITUTION_MARKUP = "[ORA_SUB_MARKUP]";

	private static final String JUSTIFYPTRN = "(?:" + LEFTPTRN + "|" + CENTERPTRN + "|" + RIGHTPTRN + ")";
	private static final String SCAN_ON = "ON";
	private static final String SCAN_OFF = "OFF";

	private static final String CURRPAGENO = "(?i:sql.pno)";
	private static final int LINESIZE = 80; // default line size.
	private static final CharSequence SQL_DOT_SQL = "SQL.SQL";
	private static final CharSequence SQL_DOT_COMMAND = "SQL.COMMAND";
	protected String titleType = "GENERIC_TITLE";

	protected int m_linesize = -1;
	protected String m_titleCmd = "";
	protected String CMD = ""; //$NON-NLS-1$
	protected ArrayList<String> m_parsedList = null;
	//protected String m_origCmd = "";
	protected Connection m_connection = null;
	protected String m_headsepchar = "";
	protected String m_scan = "";
	private int m_pagesize = -1;
	private String m_colsep = "";
	private String m_headsep = "";
	private String m_heading = "";
	private String m_wrap = "";
	private int m_numwidth = -1;
	private String m_numformat = "";
	private int m_long = -1;
	private String m_null = "";
	private int m_lno = -1;
	private String m_suffix = "";
	private Long m_autoCommit = -1L;
	private Boolean m_pause = Boolean.FALSE;
	private int m_sqlcode = 0;
	private int m_oscode = 0;


	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// ignore
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// ignore
	}


	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		return false;
	}

	public StringBuffer getTitle(ScriptRunnerContext ctx, Integer pageNo, Map<String,String> subNewValueMap, Map<String,String> subOldValueMap,  
			ArrayList<StringBuffer> row, String[] columns, String currentSql) {
		
		StringBuffer titleStr = new StringBuffer("");
		LinkedHashMap<String, String> cmd_to_val = new LinkedHashMap<String, String>();
		m_parsedList = new ArrayList<String>();
		m_headsepchar = ((String)ctx.getProperty(ctx.SETHEADSEPCHAR));
		m_linesize = ((Integer)ctx.getProperty(ctx.SETLINESIZE)).intValue() + m_lineSeparator.length();
		m_scan = (ctx.getScanOn())? SCAN_ON : SCAN_OFF ;

		m_lno = ((Integer) ctx
				.getProperty(ScriptRunnerContext.LNO)).intValue();
		m_pagesize = ((Integer) ctx
				.getProperty(ScriptRunnerContext.SETPAGESIZE)).intValue();
		m_linesize = ((Integer) ctx
				.getProperty(ScriptRunnerContext.SETLINESIZE)).intValue();
		m_scan = ((String) ctx
				.getProperty(ScriptRunnerContext.SETSCAN));
		m_colsep = ((String) ctx
				.getProperty(ScriptRunnerContext.SETCOLSEP));
		m_headsep = ((String) ctx
				.getProperty(ScriptRunnerContext.SETHEADSEP));
		m_headsepchar = ((String) ctx
				.getProperty(ScriptRunnerContext.SETHEADSEPCHAR));
		m_heading = ((String) ctx
				.getProperty(ScriptRunnerContext.SETHEADING));
		m_wrap = ((String) ctx
				.getProperty(ScriptRunnerContext.SETWRAP));
		m_long = ((Integer) ctx
				.getProperty(ScriptRunnerContext.SETLONG)).intValue();
		m_numwidth = ((Integer) ctx
				.getProperty(ScriptRunnerContext.SETNUMWIDTH)).intValue();
		m_numformat = ((String) ctx
				.getProperty(ScriptRunnerContext.SETNUMFORMAT));
		m_null = ((String) ctx
				.getProperty(ScriptRunnerContext.SETNULL));
		m_autoCommit = ((Long) ctx
				.getProperty(ScriptRunnerContext.AUTOCOMMITSETTING));
		m_pause = ((Boolean) ctx
				.getProperty(ScriptRunnerContext.SET_PAUSE));
		m_sqlcode = ((Integer) ctx
				.getProperty(ScriptRunnerContext.SET_SQLCODE));
		m_oscode = ((Integer) ctx
				.getProperty(ScriptRunnerContext.SET_OSCODE));


		if(m_linesize < 10) {
			m_linesize = LINESIZE + m_lineSeparator.length(); // set it to default size.
		}
		if(m_linesize > 1) {
			titleStr.append(addLine());

			String titlCmd = m_titleCmd;
			if(titlCmd.length() > 0 && subNewValueMap != null && !subNewValueMap.isEmpty() && 
					row != null && row.size() > 0 && columns != null) {
				// Bug 24938716: A SECOND OLD_VALUE DOES NOT OVERWRITE THE FIRST ONE
				// More than 1 column can be bounded to a variable as described in the bug.
				// The last column get bounded instead of the first one. 
				for(int i = columns.length-1; i > 0; i--) {
					String val = subNewValueMap.get(columns[i]);
					// Bug 23047177	TTITLE PRINT THE VARIABLE SPECIFIED BY NEW_VALUE EVEN IT IS QUOTED 
					// We added new code here to handle substitution of variable values.
					titlCmd = substituteColumnValues(val, row.get(i).toString(), titlCmd);			
				}
			}
			if(titlCmd.length() > 0 && subOldValueMap != null && !subOldValueMap.isEmpty() && 
					row != null && row.size() > 0 && columns != null) {
				for(int i = columns.length-1; i > 0; i--) {
					String val = subOldValueMap.get(columns[i]);
					// Bug 23047177	TTITLE PRINT THE VARIABLE SPECIFIED BY NEW_VALUE EVEN IT IS QUOTED 
					// We added new code here to handle substitution of variable values.
					titlCmd = substituteColumnValues(val, row.get(i).toString(), titlCmd);					
				}
			}
			//else {
				// Bug 21461356 TTITLE ON IS NOT WORKING. 
				// This a special case where ttitle is ON but there is no title string. 
				// The current SQL statement becomes the title. 
				if ( (titlCmd.length() == 0) && (ctx.getTTitleFlag()) )  {
					titlCmd = "ttitle left _date right page sql.pno skip 1 center '" + currentSql + "'";
				}
			//}

			while(titlCmd.split("\\s+").length > 0 && titlCmd.length() > 0) {

				String[] strs = splitStr(titlCmd);
				String str = strs[0];

				if(str.matches(JUSTIFYPTRN)) {
					cmd_to_val.put(JUSTIFY, str);
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.matches(SKIPPTRN) || str.matches(SKIPPTRN_HAS_NUM) ) {

					String ptrn = null;
					String val = null;

					if (str.matches(SKIPPTRN_HAS_NUM)) {
						ptrn = SKIPPTRN_HAS_NUM;
						val = extractFirst(titlCmd, ptrn);
						if(!val.isEmpty()) {
							String[] skipWord =  val.split("\\d+");
							String skipNum =  val.substring(skipWord[0].length());
							String n = skipNum.trim();
							cmd_to_val.put(SKIP, n);
						}
					}
					else {
						ptrn = SKIPPTRN + "\\s*(-?\\d)*";
						val = extractFirst(titlCmd, ptrn);
						if(!val.isEmpty()) {
							String[] skip =  val.split("\\s+");
							if(skip.length == 2) {
								String n = skip[1];
								cmd_to_val.put(SKIP, n);
							}
							else {
								cmd_to_val.put(SKIP, "0");
							}
						}
					}

					applyPrintSpec(titleStr, cmd_to_val, "");
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else {
						if(val.isEmpty())
							titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
						else
							titlCmd = titlCmd.substring(val.length(), titlCmd.length()).trim();	
					}      
				}
				else if(str.matches(COLPTRN)) {
					String ptrn = COLPTRN + "\\s+\\d+";
					String val = extractFirst(titlCmd, ptrn);
					if(!val.isEmpty()) {
						String[] cols =  val.split("\\s+");
						if(cols.length == 2) {
							String n = cols[1];
							cmd_to_val.put(COL, n);
						}
					}
					if(str.equals(titlCmd))
						titlCmd = "";
					else {
						if(val.isEmpty()) 
							titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();	
						else
							titlCmd = titlCmd.substring(val.length(), titlCmd.length()).trim();
					}
				}
				else if(str.matches(FORMATPTRN)) {
					// Bugs 13357515 & 13368978
					String ptrn1 = FORMATPTRN + "\\s+[\\w.,$\\d]+\\s+(?i:sql.pno)";
					String ptrn2 = FORMATPTRN + "\\s+[\\w.,$\\d]+\\s+.+\\s+(?i:sql.pno)";
					String ptrn3 = ".+\\s+" + FORMATPTRN + "\\s+[\\w.,$\\d]+\\s+(?i:sql.pno)";
					String val = "";
					String[] fmts = null;
					String fmtstr = "";
					String text = "";
					if(titlCmd.matches(ptrn1)) {
						val = extractFirst(titlCmd, ptrn1);
						if(!val.isEmpty()) {
							fmts = val.split("\\s+");
							fmtstr = fmts[1];
							text = getFormattedStr(pageNo.toString(), fmtstr);
						} 
					}
					else if(titlCmd.matches(ptrn2)) {
						val = extractFirst(titlCmd, ptrn2);
						if(!val.isEmpty()) {
							text = titlCmd.replaceAll(FORMATPTRN + "\\s+[\\w.,$\\d]+\\s+", "").trim();
							text = text.replaceAll("\\s+(?i:sql.pno)", "").trim();
							text = text.replaceAll("[\'\"]", "").trim();
							fmts = val.split("\\s+");
							fmtstr = fmts[1];
							//Bug 19778335 May revisit this one. Removing spaces for now.
							//    						   text = text + "     " + getFormattedStr(pageNo.toString(), fmtstr);
							text = text + getFormattedStr(pageNo.toString(), fmtstr);
						}
					}
					else if(titlCmd.matches(ptrn3)) {
						val = extractFirst(titlCmd, ptrn3);
						if(!val.isEmpty()) {
							text = titlCmd.replaceAll("\\s+" + FORMATPTRN + "\\s+[\\w.,$\\d]+\\s+(?i:sql.pno)", "").trim();
							text = text.replaceAll("[\'\"]", "").trim();
							fmts = val.split("\\s+");
							fmtstr = fmts[2];
							//Bug 19778335 May revisit this one. Removing spaces for now.
							//    						   text = text + "          " + getFormattedStr(pageNo.toString(), fmtstr);
							text = text + getFormattedStr(pageNo.toString(), fmtstr);
						}
					}

					if(cmd_to_val.size() == 0) {
						// justify to left
						cmd_to_val.put(JUSTIFY, text);
					}
					applyPrintSpec(titleStr, cmd_to_val, text);
					cmd_to_val.clear();

					if(str.equals(titlCmd))
						titlCmd = "";
					else {
						if(val.isEmpty())
							titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
						else
							titlCmd = titlCmd.substring(val.length(), titlCmd.length()).trim();
					}
				}
				else if(CheckForUnsuppottedCmds(str)) {
					applyPrintSpec(titleStr, cmd_to_val, EMPTYSTRING);
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(BOLD)) {
					applyPrintSpec(titleStr, cmd_to_val,  String.valueOf(""));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLLNO)) {
					applyPrintSpec(titleStr, cmd_to_val,  String.valueOf(m_lno));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLPNO)) {
					applyPrintSpec(titleStr, cmd_to_val, pageNo.toString());
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLSCAN)) {
					applyPrintSpec(titleStr, cmd_to_val, m_scan);
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLPAGESIZE)) {
					applyPrintSpec(titleStr, cmd_to_val, new Integer(m_pagesize).toString());
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLNUMFORMAT)) {
					applyPrintSpec(titleStr, cmd_to_val, m_numformat);
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLNUMWIDTH)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_numwidth));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLLINESIZE)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_linesize));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLLONG)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_long));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLNULL)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_null));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLSUFFIX)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_suffix));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLHEADING)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_heading));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLHEADSEP)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_headsep));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLWRAP)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_wrap));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLAUTOCOMMIT)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_autoCommit));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLPAUSE)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_pause));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLSQLCODE)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_sqlcode));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.equalsIgnoreCase(SQLOSCODE)) {
					applyPrintSpec(titleStr, cmd_to_val, String.valueOf(m_oscode));
					cmd_to_val.clear();
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.matches(FONTPTRN)) {
					// not implemented.
					if(str.equals(titlCmd))
						titlCmd = "";
					else
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
				}
				else if(str.matches(TABPTRN)) {
					// not implemented.
					String ptrn = TABPTRN + "\\s+\\d+";
					String val = extractFirst(titlCmd, ptrn);
					if(!val.isEmpty()) {
						String[] cols =  val.split("\\s+");
						if(cols.length == 2) {
							String n = cols[1];
							cmd_to_val.put(TAB, n);
						}
					}
					if(str.equals(titlCmd))
						titlCmd = "";
					else {
						if(val.isEmpty()) 
							titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();	
						else
							titlCmd = titlCmd.substring(val.length(), titlCmd.length()).trim();
					}
				}
				else {
					String text = "";

					if (str.equalsIgnoreCase("_date")) {
						text = ctx.getDate(str);
					}
					// Bug 19778398
					else if ((str.startsWith("'") && str.endsWith("'")) || (str.startsWith("\"") && str.endsWith("\"")) ) {
						// fix for 2 single quotes
						if (str.contains("\"\"")) {
							String[] modifiedStrings = appendLookahead(titlCmd, ctx);

							text = modifiedStrings[0];
							text = text.replaceAll("\"\"", "\"");
							titlCmd = modifiedStrings[1];
							str = ""; // Reset "str";

						} 
						else if (str.contains("''")) {
							String[] modifiedStrings = appendLookahead(titlCmd, ctx);

							text = modifiedStrings[0];
							text = text.replaceAll("\'\'", "\'");
							titlCmd = modifiedStrings[1];
							str = ""; // Reset "str";

						} 
						else {
							String[] modifiedStrings = appendLookahead(titlCmd, ctx);
							text = modifiedStrings[0];
							// test to see if the has any two quotes.
							if (text.contains("\'\'")) {
								text = text.replaceAll("\'\'", "\'");
							}
							else {
								text = text.replaceAll("\'", "");	
							}
							
							if (text.contains("\"")) {
								text = text.replaceAll("\"\"", "\"");
							}
							else {
								text = text.replaceAll("\"", "");	
							}
							titlCmd = modifiedStrings[1];
							str = ""; // Reset "str";
						}
					}
					else {
						// look for "define"
						Map<String, String> map = ctx.getMap();
						text = map.get(str);
						if(text == null || text.isEmpty()) {
							//At this place we know that the variable "str" is an unquoted string and is not a keyword.
							// This str should be appended to the previous text and remove all white spaces.   
							String[] modifiedStrings = appendLookahead(titlCmd, ctx);

							text = modifiedStrings[0];
							titlCmd = modifiedStrings[1];
							str = ""; // Reset "str";

						}
					}

					String[] tcmds = splitStr(titlCmd);
					if ((tcmds != null) && (tcmds.length > 0)) {
						String currentCommand = tcmds[0];
						if(CheckForUnsuppottedCmds(currentCommand)) { 
							text += "";
							if(currentCommand.equals(titlCmd)) 
								titlCmd = "";
							else {
								str = ""; // reset str because we are consuming SQL.PNO.
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(BOLD)) {
							String regexp = str + "\\s+" + "(?i:bold)";
							text +=  "          ";
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; // reset str because we are consuming BOLD.
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLLNO)) {
							String regexp = str + "\\s+" + "(?i:sql.lno)";
							text += "          " + pageNo.toString();
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; // reset str because we are consuming SQL.PNO.
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLPNO)) {
							String regexp = str + "\\s+" + "(?i:sql.pno)";
							text += "          " + pageNo.toString();
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; // reset str because we are consuming SQL.PNO.
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLSCAN)) {
							String regexp = str + "\\s+" + "(?i:sql.scan)";
							text += "  " + m_scan;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; // reset str because we are consuming SQL.SCAN.
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLPAGESIZE)) {
							String regexp = str + "\\s+" + "(?i:sql.pagesize)";
							text += "  " + m_pagesize;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLNUMFORMAT)) {
							String regexp = str + "\\s+" + "(?i:sql.numformat)";
							text += "  " + m_numformat;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLNUMWIDTH)) {
							String regexp = str + "\\s+" + "(?i:sql.numwidth)";
							text += "  " + m_numwidth;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLLINESIZE)) {
							String regexp = str + "\\s+" + "(?i:sql.linesize)";
							text += "  " + m_linesize;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLLONG)) {
							String regexp = str + "\\s+" + "(?i:sql.long)";
							text += "  " + m_long;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLNULL)) {
							String regexp = str + "\\s+" + "(?i:sql.null)";
							text += "  " + m_null;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLSUFFIX)) {
							String regexp = str + "\\s+" + "(?i:sql.suffix)";
							text += "  " + m_suffix;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLHEADING)) {
							String regexp = str + "\\s+" + "(?i:sql.heading)";
							text += "  " + m_heading;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLHEADSEP)) {
							String regexp = str + "\\s+" + "(?i:sql.headsep)";
							text += "  " + m_headsep;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLWRAP)) {
							String regexp = str + "\\s+" + "(?i:sql.wrap)";
							text += "  " + m_wrap;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLAUTOCOMMIT)) {
							String regexp = str + "\\s+" + "(?i:sql.autocommit)";
							text += "  " + m_autoCommit;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLPAUSE)) {
							String regexp = str + "\\s+" + "(?i:sql.pause)";
							text += "  " + m_pause;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLSQLCODE)) {
							String regexp = str + "\\s+" + "(?i:sql.sqlcode)";
							text += "  " + m_sqlcode;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(SQLOSCODE)) {
							String regexp = str + "\\s+" + "(?i:sql.oscode)";
							text += "  " + m_oscode;
							if(currentCommand.equals(titlCmd) || titlCmd.matches(regexp)) 
								titlCmd = "";
							else {
								str = ""; 
								titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim();
							}
						}
						else if(currentCommand.equalsIgnoreCase(FORMAT)) {
							String regexp = FORMATPTRN + "\\s+[\\w.,$]+\\s+(?i:sql.pno)";
							String val = extractFirst(titlCmd, regexp);
							if(!val.isEmpty()) {
								String[] fmts =  val.split("\\s+");
								if(fmts.length == 3) {
									String fmtstr = fmts[1];
									String pageno = pageNo.toString();
									String txt = getFormattedStr(pageno, fmtstr);
									text += txt;
									if(val.equals(titlCmd))
										titlCmd = "";
									else {
										if(val.isEmpty())
											titlCmd = titlCmd.substring(currentCommand.length(), titlCmd.length()).trim(); 	
										else	
											titlCmd = titlCmd.substring(val.length(), titlCmd.length()).trim();
									}
								}
							}
						}
					}
					// Bug 12929290
					if(cmd_to_val.size() == 0) {
						//if(cmd_to_val.size() >= 0) {
						// justify left by default.
						cmd_to_val.put(JUSTIFY, text);
					}

					if(str.equals(titlCmd)) {
						titlCmd = "";
					}
					else if(titlCmd.length() > 0) {
						titlCmd = titlCmd.substring(str.length(), titlCmd.length()).trim();
					}
					if (cmd_to_val.isEmpty() && text.length() > 0) { // Bug 19778335
						titleStr.append(text);
					}
					else {
						applyPrintSpec(titleStr, cmd_to_val, text);
						cmd_to_val.clear();
					}
				}
			}
		}

		if(!titleStr.toString().endsWith(m_lineSeparator)) {
			titleStr.append(m_lineSeparator);
		}
		return titleStr;
	}


	private String[] appendLookahead(String titleCommand, ScriptRunnerContext ctx) {

		String truncatedTitleCommand = titleCommand; 
		String[] modifiedStrings = new String[2];
		StringBuffer sb = new StringBuffer();
		String[] lookAheadTokens = splitStr(titleCommand);
		boolean tokenMarkedUp = false;
		String markedUpToken = null;

		for (String lookheadToken: lookAheadTokens) {
			// the fist one should be the same. Still match to validate.
			if (!testCoreKeyword(lookheadToken)) {

				if ( lookheadToken.startsWith("\'") && lookheadToken.endsWith("\'") ) {
					if (lookheadToken.length() > 1) {
						if (hasSubstituitionOraMarkup(lookheadToken)) { // this is a markup token
							tokenMarkedUp = true;
							markedUpToken = lookheadToken;
							lookheadToken = " " + removeSubstituitionOraMarkup(lookheadToken);
						}
						else {
							tokenMarkedUp = false;
							markedUpToken = null;
						}
						sb.append(lookheadToken.subSequence(1, lookheadToken.length()-1));
					}
				}
				else  if ( lookheadToken.startsWith("\'") && !lookheadToken.endsWith("\'") ) {
					if (hasSubstituitionOraMarkup(lookheadToken)) { // this is a markup token
						tokenMarkedUp = true;
						markedUpToken = lookheadToken;
						lookheadToken = " " + removeSubstituitionOraMarkup(lookheadToken);
					}
					else {
						tokenMarkedUp = false;
						markedUpToken = null;
					}
					sb.append(lookheadToken.subSequence(1, lookheadToken.length()));
				}
				else if ( !lookheadToken.startsWith("\'") && lookheadToken.endsWith("\'") ) {
					if (hasSubstituitionOraMarkup(lookheadToken)) { // this is a markup token
						tokenMarkedUp = true;
						markedUpToken = lookheadToken;
						lookheadToken = " " + removeSubstituitionOraMarkup(lookheadToken);
					}
					else {
						tokenMarkedUp = false;
						markedUpToken = null;
					}
					sb.append(lookheadToken.subSequence(0, lookheadToken.length()-1));
				}
				else if ( lookheadToken.startsWith("\"") && lookheadToken.endsWith("\"") ) {
					if (lookheadToken.length() > 1) {
						if (hasSubstituitionOraMarkup(lookheadToken)) { // this is a markup token
							tokenMarkedUp = true;
							markedUpToken = lookheadToken;
							lookheadToken = " " + removeSubstituitionOraMarkup(lookheadToken);
						}
						else {
							tokenMarkedUp = false;
							markedUpToken = null;
						}
						sb.append(lookheadToken.subSequence(1, lookheadToken.length()-1));
					}
				}
				else  if ( lookheadToken.startsWith("\"") && !lookheadToken.endsWith("\"") ) {
					if (hasSubstituitionOraMarkup(lookheadToken)) { // this is a markup token
						tokenMarkedUp = true;
						markedUpToken = lookheadToken;
						lookheadToken = " " + removeSubstituitionOraMarkup(lookheadToken);
					}
					else {
						tokenMarkedUp = false;
						markedUpToken = null;
					}
					sb.append(lookheadToken.subSequence(1, lookheadToken.length()));
				}
				else if ( !lookheadToken.startsWith("\"") && lookheadToken.endsWith("\"") ) {
					if (hasSubstituitionOraMarkup(lookheadToken)) { // this is a markup token
						tokenMarkedUp = true;
						markedUpToken = lookheadToken;
						lookheadToken = " " + removeSubstituitionOraMarkup(lookheadToken);
					}
					else {
						tokenMarkedUp = false;
						markedUpToken = null;
					}
					sb.append(lookheadToken.subSequence(0, lookheadToken.length()-1));
				}
				else {
					if (hasSubstituitionOraMarkup(lookheadToken)) { // this is a markup token
						tokenMarkedUp = true;
						markedUpToken = lookheadToken;
						lookheadToken = " " + removeSubstituitionOraMarkup(lookheadToken);
					} 
					else {
						tokenMarkedUp = false;
						markedUpToken = null;
					}
					sb.append(lookheadToken);
				}
				if (tokenMarkedUp && markedUpToken != null) {
					truncatedTitleCommand = truncatedTitleCommand.substring(markedUpToken.length(), truncatedTitleCommand.length()).trim();	
				}
				else {
					truncatedTitleCommand = truncatedTitleCommand.substring(lookheadToken.length(), truncatedTitleCommand.length()).trim();
				}
			}
			else {
				break;
			}
		}
		modifiedStrings[0]= sb.toString();
		modifiedStrings[1]= truncatedTitleCommand;
		return modifiedStrings;
	}

	private void applyPrintSpec(StringBuffer titleStr, LinkedHashMap<String, String> printspec, String text) {

		int lines = titleStr.toString().split(m_lineSeparator).length;

		if(printspec != null) {

			for (Map.Entry<String, String> entry : printspec.entrySet()) {
				String ps_key = entry.getKey();
				String ps_val = entry.getValue();
				String hs_text = applyHeadSep(text.trim());

				if(ps_key.equals(JUSTIFY)) {
					String val = "";

					if(ps_val.matches(LEFTPTRN)) { 
						val = justify_left(hs_text, m_linesize-m_lineSeparator.length());
					}
					else if(ps_val.matches(CENTERPTRN)) {
						val = justify_center(hs_text, m_linesize-m_lineSeparator.length());
					}
					else if(ps_val.matches(RIGHTPTRN)) {
						val = justify_right(hs_text, m_linesize-m_lineSeparator.length());
					}
					else {
						val = justify_left(hs_text, m_linesize); //default	
					}
					int idx = val.indexOf(hs_text); 
					if(idx >= 0 && lines > 0) {
						int startValIndex = idx;
						int endValIndex = startValIndex + hs_text.length();
						// Title in many lines.
						if ( lines > 1 ) {

							if (hs_text.contains(m_lineSeparator)){
								String[] sub_hs_texts = hs_text.split(m_lineSeparator);

								for (int i = 0; i < sub_hs_texts.length; i++) {
									if (i > 0 ) {
										titleStr.append(addLine());
									}
									String sub_hs_text = sub_hs_texts[i].trim();
									int titleStrLen = titleStr.capacity();
									int rowId = ((lines + i) - 1) * m_linesize;
									int startValueIndex4 = rowId + startValIndex;
									int endValueIndex4 =  startValueIndex4 + sub_hs_text.length();
									titleStr.replace(startValueIndex4, endValueIndex4, sub_hs_texts[i].trim());
								}
							}
							else {
								int startValueIndex2 = startValIndex + ((lines-1) * m_linesize);
								int endValueIndex2 = endValIndex + ((lines-1) * m_linesize);
								titleStr.replace(startValueIndex2, endValueIndex2, hs_text);
							}
						}
						else {
							// Title in 1 line.
							if (hs_text.contains(m_lineSeparator)){

								String[] sub_hs_texts = hs_text.split(m_lineSeparator);

								for (int i = 0; i < sub_hs_texts.length; i++) {
									if (i > 0 ) {
										titleStr.append(addLine());
									}
									String sub_hs_text = sub_hs_texts[i].trim();
									int rowId = i * m_linesize;
									int startValueIndex4 = rowId + startValIndex;
									int endValueIndex4 =  startValueIndex4 + sub_hs_text.length();
									titleStr.replace(startValueIndex4, endValueIndex4, sub_hs_texts[i].trim());
								}
							}
							else {
								titleStr.replace(startValIndex, endValIndex, hs_text);
							}
						}
					}

				}       
				else if(ps_key.equals(COL) || ps_key.equals(TAB)) {
					int idx = new Integer(ps_val).intValue();
					//int lines = titleStr.toString().split(m_lineSeparator).length;
					int minIdx = m_linesize * (lines-1);
					int maxIdx = minIdx + m_linesize  - (m_lineSeparator.length() + 1);
					int findIdx = titleStr.indexOf(hs_text);
					if(findIdx > -1 && (findIdx >= minIdx && findIdx <= maxIdx)&&(hs_text!=null)&&(!(hs_text.equals("")))){
						String blankStr = String.format("%1$-" + hs_text.length() + "s", "");
						titleStr.replace(findIdx, findIdx + hs_text.length(), blankStr);
					}
					else {
						// Bug 19777755: TTITLE TAB N OPTION CAN NOT WORK WELL
						if (ps_key.equals(TAB)) {
							//titleStr.replace(minIdx + (idx-1), minIdx + (idx-1) + hs_text.length(), hs_text);
							int startIndex = minIdx + (idx-1);
							int endIndex =  minIdx + (idx-1) + hs_text.length();
							int titleStrCapacity = titleStr.capacity();
							if (endIndex < titleStrCapacity) {
								int allowableLength = titleStrCapacity - endIndex;
								hs_text = hs_text.substring(0,allowableLength);
								titleStr.replace(startIndex,endIndex, hs_text);	
							}
						} 
						else {
							// Bug 19778381
							// titleStr.replace(minIdx + (idx-1), minIdx + (idx - 1) + hs_text.length(), hs_text);
							// In above commented code there is no check for the size of the title buffer.
							int startIndex = minIdx + (idx-1);
							int endIndex = 	minIdx + (idx - 1) + hs_text.length();	
							// Bug 21498132	:GET EXCEPTION FROM SELECT WITH A SPECIFIC VALUE FOR COL TTITLE.
							int titleStrSize = titleStr.capacity();
							if (idx >= titleStrSize) {
								int numLines = idx%titleStrSize;
								for (int i = 0; i < numLines; i++) {
									titleStr.append(addLine());
								}
							}
							titleStr.replace(startIndex, endIndex, hs_text);
						}
					}
					if(idx >= minIdx && idx <= maxIdx) {
						if(idx + hs_text.length() <= maxIdx) {
							if (ps_key.equals(TAB)) {
								titleStr.replace(idx-1, idx-1 + hs_text.length(), hs_text);
							} 
							else {
								titleStr.replace(idx-1, idx-1 + hs_text.length(), hs_text);
							}
						}
						else {
							int diff = maxIdx - idx;
							String trnc_txt = hs_text.substring(0, diff);
							titleStr.replace(idx-1, idx-1 + trnc_txt.length(), trnc_txt);
						}
					}
				}
				else if(ps_key.equals(SKIP)) {
					int val = new Integer(ps_val);
					if(val < 0) {
						titleStr.append(addLine());
						String negativeSkipValue = Integer.toString(val);
						titleStr.replace(0, negativeSkipValue.length(), negativeSkipValue);
					}
					else if ((val == 0) || (val == 1)) {
						titleStr.append(addLine());	
					}
					else {
						// bug 21498336: GET OOM SETTING A LARGE N FOR SKIP OF BTITLE COMMAND
		                // bug 21498526: GET INCORRECT VALUE FROM SELECT WHEN BTITLE SKIP SOME VALUE
						int effectiveSkipLines = m_pagesize - SKIP_COLUMN_HEADER_LINES;
						if (val >= effectiveSkipLines) {
							val = effectiveSkipLines;
						}
						for(int i = 0; i < val; i++) {
							titleStr.append(addLine());
						}
					}
				}
			}
		}
	}

	protected String extractFirst(String fromStr, String regexp) {
		String retval = "";
		Pattern ptrn = Pattern.compile(regexp);
		if(ptrn != null) {
			Matcher m = ptrn.matcher(fromStr);
			if(m != null && m.find()) {
				retval = m.group();
			}
		}
		return retval;
	}

	
	private String[] splitStr(String titleCmd) {

		String regex = null;
		String[] results = null;

		String regexWord = "\\\\|/|\\{|\\}|\\[|\\]|<|>|,|\\+|\\^|\\)|\\(|@|&|\\$|!|%|\\?|#|;|:|\\*|\\.|=|-|_|\\w|\\d|(-?\\d+)|\\"+m_headsepchar;
		String regexWordSpace = "\\s|" + regexWord;
		// bug 21475570: QUOTES ARE NOT PARSED CORRECTLY FOR BTITLE
		// Regular Expression 	=> '[\w\s]*('{2})*[\w\s]*'|\w+	
		// Regular Expression in Java  =>	"'[\\w\\s]*('{2})*[\\w\\s]*'|\\w+"
		String regexDoubleQuote = "\"["+regexWordSpace+"]*(\"{2})*["+regexWordSpace+"]*\"|["+regexWord+"]+";
		String regexSingleQuote = "'["+regexWordSpace+"]*('{2})*["+regexWordSpace+"]*'|["+regexWord+"]+";

		if (titleCmd.contains("\"")) {
			regex = regexDoubleQuote;
		}
		else {
			regex = regexSingleQuote;
		}

		ArrayList<String> matches = new ArrayList<String>(); 
		Pattern ptrn = Pattern.compile(regex);

		if(ptrn != null) {
			Matcher m = ptrn.matcher(titleCmd);
			if(m != null) {
				while(m.find()) {
					String group = m.group();
					matches.add(group);
				}
			}
			if(matches.size() > 0)
				results = matches.toArray(new String[matches.size()]);
		}

		return results;
	}


	private boolean testCoreKeyword(String result) {
		String resultTemp = result.trim();
		if (
				(resultTemp.equalsIgnoreCase("LEFT")) || (resultTemp.equalsIgnoreCase("LE")) ||
				(resultTemp.equalsIgnoreCase("RIGHT")) || (resultTemp.equalsIgnoreCase("R")) ||
				(resultTemp.equalsIgnoreCase("CENTER")) || (resultTemp.equalsIgnoreCase("CE")) ||
				(resultTemp.equalsIgnoreCase("SKIP")) || (resultTemp.equalsIgnoreCase("S")) || validateSkip(resultTemp) ||
				(resultTemp.equalsIgnoreCase("TAB")) || 
				(resultTemp.equalsIgnoreCase("COL")) ||
				(resultTemp.equalsIgnoreCase("FORMAT")) ||
				(resultTemp.equalsIgnoreCase("BOLD")) ||
				(resultTemp.equalsIgnoreCase("ON")) || (resultTemp.equalsIgnoreCase("OFF")) ||
				(resultTemp.equalsIgnoreCase("SQL.PNO")) ||
				(resultTemp.equalsIgnoreCase("SQL.SCAN")) ||
				(resultTemp.equalsIgnoreCase("SQL.LONG")) ||
				(resultTemp.equalsIgnoreCase("SQL.LINESIZE")) ||
				(resultTemp.equalsIgnoreCase("SQL.SQLCODE")) ||
				(resultTemp.equalsIgnoreCase("SQL.OSCODE")) ||
				(resultTemp.equalsIgnoreCase("SQL.NUMWIDTH")) ||
				(resultTemp.equalsIgnoreCase("SQL.NUMFORMAT")) ||
				(resultTemp.equalsIgnoreCase("SQL.LNO")) ||
				(resultTemp.equalsIgnoreCase("SQL.SQLPROMPT")) ||
				(resultTemp.equalsIgnoreCase("SQL.NULL")) ||
				(resultTemp.equalsIgnoreCase("SQL.SUFFIX")) ||
				(resultTemp.equalsIgnoreCase("SQL.PREFIX")) ||
				(resultTemp.equalsIgnoreCase("SQL.SQLPREFIX")) ||
				(resultTemp.equalsIgnoreCase("SQL.LOGSOURCE")) ||
				(resultTemp.equalsIgnoreCase("SQL.FEEDBACK")) ||
				(resultTemp.equalsIgnoreCase("SQL.HEADING")) ||
				(resultTemp.equalsIgnoreCase("SQL.SPACE")) ||
				(resultTemp.equalsIgnoreCase("SQL.VERIFY")) ||
				(resultTemp.equalsIgnoreCase("SQL.TERMOUT")) ||
				(resultTemp.equalsIgnoreCase("SQL.TRIMOUT")) ||
				(resultTemp.equalsIgnoreCase("SQL.TIMING")) ||
				(resultTemp.equalsIgnoreCase("SQL.WRAP")) ||
				(resultTemp.equalsIgnoreCase("SQL.USER")) ||
				(resultTemp.equalsIgnoreCase("SQL.SQLCASE")) ||
				(resultTemp.equalsIgnoreCase("SQL.PAGESIZE")) ||
				(resultTemp.equalsIgnoreCase("SQL.AUTORECOVERY")) ||
				(resultTemp.equalsIgnoreCase("SQL.DEFINE")) ||
				(resultTemp.equalsIgnoreCase("SQL.ESCAPE")) ||
				(resultTemp.equalsIgnoreCase("SQL.ECHO")) ||
				(resultTemp.equalsIgnoreCase("SQL.CMDSEP")) ||
				(resultTemp.equalsIgnoreCase("SQL.PAUSE")) ||
				(resultTemp.equalsIgnoreCase("SQL.ESCAPE")) ||
				(resultTemp.equalsIgnoreCase("SQL.HEADSEP")) ||
				(resultTemp.equalsIgnoreCase("SQL.SQLPREFIX")) ||
				(resultTemp.equalsIgnoreCase("SQL.AUTOCOMMIT")) ||
				(resultTemp.equalsIgnoreCase("SQL.CMDSEP"))
				) {
			return true;
		}
		return false; 
	}


	private boolean CheckForUnsuppottedCmds(String param) {
		if (
				param.equalsIgnoreCase (SQLPREFIX) ||
				param.equalsIgnoreCase (SQLSQLPREFIX) ||
				param.equalsIgnoreCase (SQLCMDSEP) ||
				param.equalsIgnoreCase (SQLSPACE) ||
				param.equalsIgnoreCase (SQLDEFINE) ||
				param.equalsIgnoreCase (SQLCASE) ||
				param.equalsIgnoreCase (SQLSQLCASE) ||
				param.equalsIgnoreCase (SQLUSER) ||
				param.equalsIgnoreCase (SQLSQLCASE) ||
				param.equalsIgnoreCase (SQLTRIMOUT) ||
				param.equalsIgnoreCase (SQLTIMING) ||
				param.equalsIgnoreCase (SQLTERMOUT) ||
				param.equalsIgnoreCase (SQLVERIFY) ||
				param.equalsIgnoreCase (SQLSPOOL) ||
				param.equalsIgnoreCase (SQLLOGSOURCE) ||
				param.equalsIgnoreCase (SQLFEEDBACK) ||
				param.equalsIgnoreCase (SQLECHO) ||
				param.equalsIgnoreCase (SQLSQLPROMPT) ||
				param.equalsIgnoreCase (SQLESCAPE) ||
				param.equalsIgnoreCase (SQLRELEASE) ||
				param.equalsIgnoreCase (SQLAUTORECOVERY)
				) {
			return true;
		}
		else {
			return false;
		}
	}


	private boolean validateSkip(String resultTemp) {

		if (resultTemp.startsWith("skip") || resultTemp.startsWith("SKIP")) {
			resultTemp = resultTemp.substring(4, resultTemp.length()).trim();
			if (isInteger(resultTemp)) {
				return true;
			}
		}
		return false;
	}

	private boolean isInteger(String s) {
		if(s.isEmpty()) return false;
		for(int i = 0; i < s.length(); i++) {
			if(i == 0 && s.charAt(i) == '-') {
				if(s.length() == 1) return false;
				else continue;
			}
			if(Character.digit(s.charAt(i),10) < 0) return false;
		}
		return true;
	}

	private String applyHeadSep(String text) {
		String str = "";

		StringTokenizer stnzr = new StringTokenizer(text, m_headsepchar);

		while (stnzr.hasMoreTokens()) {
			String token = stnzr.nextToken().trim();

			if(token.length() > m_linesize)
				str += token.substring(0, m_linesize);
			else
				str += token;
			if (stnzr.hasMoreTokens()) {
				str += m_lineSeparator;
			}
		}

		if(str.length() == 0) {
			str = text;
		}
		else {
			// remove quotes if any.
			if ((str.startsWith("'")) || (str.startsWith("\""))) {
				str = str.substring(1);
			}
			if ((str.endsWith("'")) || (str.endsWith("\""))) {
				str = str.substring(0,str.length()-1);
			}
		}
		return str;
	}

	private void skip(StringBuffer titleStr, String cmdPtrn, String stripPtrn) {
		//    	Pattern ptrn = Pattern.compile(cmdPtrn);
		String match = getMatch(cmdPtrn);
		if(match != null && !match.isEmpty()) {
			//if(!match.equals("") || match.length() > 0) {  
			match = match.trim().replaceAll(stripPtrn, "").trim();
			// strip single and/or double quotes.
			match = match.replaceAll("[\'\"]", "");
			boolean isDigit = false;
			isDigit = match.equals("") ? false : Pattern.matches("^\\d*$", match);
			if(isDigit) {
				int val = new Integer(match);
				if(val <= 1) {
					titleStr.append(addLine());
				}
				else {
					for(int i = 0; i < val; i++) {
						titleStr.append(addLine());
					}
				}
			}
			else {
				titleStr.append(addLine());	
			}
			//}
		}
	}

	private void justify(StringBuffer titleStr, String cmdPtrn, String stripPtrn, Integer pageNo) {
		String val = "";
		String match = getMatch(cmdPtrn);
		if(match != null && !match.isEmpty()) {
			match = match.trim().replaceAll(stripPtrn, "").trim();
			// strip single and/or double quotes.
			match = match.replaceAll("[\'\"]", "");
			int sqlpidx = match.toUpperCase().indexOf("SQL.PNO"); //$NON-NLS
			if(sqlpidx > 0) {
				match = match.replaceAll(CURRPAGENO, "     " + pageNo.toString());
			}

			if(cmdPtrn.equals(LEFTCMDPTRN)) 
				val = justify_left(match, m_linesize-m_lineSeparator.length());
			else if(cmdPtrn.equals(RIGHTCMDPTRN))
				val = justify_right(match, m_linesize-m_lineSeparator.length());
			else if(cmdPtrn.equals(CENTERCMDPTRN))
				val = justify_center(match, m_linesize-m_lineSeparator.length());
			else
				val = justify_left(match, m_linesize); //default
			int idx = val.indexOf(match); 
			int lines = titleStr.toString().split(m_lineSeparator).length;
			if(idx >= 0 && lines > 0){
				int start = idx + m_linesize * (lines-1);
				int end = start + match.length();
				titleStr.replace(start, end, match);
			}
		}
	}

	private void col(StringBuffer titleStr, String cmdPtrn, String stripPtrn, Integer pageNo) {
		int lines = titleStr.toString().split(m_lineSeparator).length;
		int minIdx = m_linesize * (lines-1);
		int maxIdx = minIdx + m_linesize  - (m_lineSeparator.length() + 1);

		String match = getMatch(cmdPtrn);

		if(match != null && !match.isEmpty()) {
			String[] colcmds = match.split("(?:\\s+|[\'\"].+[\'\"])");
			if(colcmds.length >= 3) {
				boolean isDigit = false;
				int idx = 0;
				String ttext = "", fmtstr = "", append_pageno = "";
				for(int i = 1; i < colcmds.length; i++) {
					isDigit = colcmds[i].matches("^\\d+$");
					if(isDigit) {
						idx = new Integer(colcmds[i]).intValue() - 1;
						continue;
					}
					if(colcmds[i].equalsIgnoreCase("format")) {
						fmtstr = colcmds[++i];
					}
					else if(colcmds[i].equalsIgnoreCase("sql.pno")) {
						String temp = getFormattedStr(pageNo.toString(), fmtstr);
						append_pageno = "     ";
						ttext += append_pageno + temp;
					}
					else {
						ttext += colcmds[i];
					}
				}

				Matcher matcher = Pattern.compile("[\'\"].+[\'\"]").matcher(match);
				if(matcher.find()) {
					String qtd_txt = matcher.group();
					ttext = qtd_txt + ttext;
				}

				ttext = ttext.replaceAll("[\'\"]", "");
				// apply column format rule here.
				ttext = getFormattedStr(ttext, fmtstr);
				ttext = append_pageno + ttext;
				if(idx >= minIdx && idx <= maxIdx) {
					if(idx + ttext.length() <= maxIdx) {
						titleStr.replace(idx, idx + ttext.length(), ttext);
					}
					else {
						int diff = maxIdx - idx;
						String trnc_txt = ttext.substring(0, diff);
						titleStr.replace(idx, idx + trnc_txt.length(), trnc_txt);
					}
				}
			}
		}
		else {
			return;
		}
	}

	private String getMatch(String cmdPtrn) {
		String match = "";
		Pattern ptrn = Pattern.compile(cmdPtrn);
		if(ptrn != null) {
			Matcher matcher = ptrn.matcher(m_titleCmd);
			while(matcher.find()) {
				match = matcher.group();
				if(m_parsedList.size() == 0) {
					m_parsedList.add(match);
					break;
				}
				else {
					if(m_parsedList.contains(match)) {
						continue;
					}
					else {
						m_parsedList.add(match);
						break;
					}
				}
			}
		}
		return match;
	}


	private String addLine() {
		String retval = "";
		retval = justify_left(" ", m_linesize-m_lineSeparator.length());
		retval += m_lineSeparator;
		return retval;
	}

	private String justify_left(String str, int width) {
		return String.format("%1$-" + width + "s", str);
	}


	private String justify_right(String str, int width) {
		return String.format("%1$" + width + "s", str);
	}

	private String justify_center(String str, int width) {
		String val = str.trim();
		if(width < val.length()) return str;
		int lenR = (width - val.length())/2 + val.length();
		// Justify Right
		String strR = String.format("%1$" + lenR + "s", val);
		// Justify Left
		String ret = String.format("%1$-" + width + "s", strR);
		return ret;
	}

	private String getFormattedStr(String str, String fmtstr) {
		String ret = str;
		if(fmtstr.startsWith("a") || fmtstr.startsWith("A")) {
			String strlen = fmtstr.replaceAll("^\\D*", "").trim();
			ret = justify_left(ret, new Integer(strlen).intValue());
		}
		else {
			//if(ret.matches("^\\d+$")) {
			NUMBER num = new NUMBER(new Integer(ret).intValue());
			//ret = getFormattedNumber(m_connection, num, fmtstr);
			try {
				ret = num.toFormattedText(fmtstr, null);
			}
			catch(Exception ex) {
				ret = num.stringValue();
			}
			//}
		}
		return ret;
	}

	private String getFormattedNumber(Connection conn, NUMBER number, String fmtstr) {
		String retval = "";
		ResultSet rset = null; 
		String sql = "SELECT TO_CHAR(";
		sql += number.stringValue() + ",";
		sql += "'" + fmtstr + "'" + ") from dual";

		if(conn != null && !sql.equals("")) {
			try {
				DBUtil dbutil = DBUtil.getInstance(conn);
				rset = dbutil.executeOracleQuery(sql, null);
				while (rset.next()) {
					retval = rset.getString(1);
				}
			}
			catch(Exception ex) {
				// just return the original value without formatting.
				retval = DataTypesUtil.stringValue(number, conn);
			}
			finally {
				if (rset != null) {
					DBUtil.closeResultSet(rset);
				}
			}
		}
		return retval;
	}

	public String getCommand() {
		return m_titleCmd;
	}

	public String buildShowTitle(Title title, ScriptRunnerContext ctx) {
		String str = "";
		String tCmd = "";
		boolean flag = false;
		if(title instanceof TopTitle) {
			str = "ttitle";
			flag = ctx.getTTitleFlag();
			tCmd = ctx.getTTitleCmd();
		}
		else if(title instanceof BottomTitle){
			str = "btitle";
			flag = ctx.getBTitleFlag();
			tCmd = ctx.getBTitleCmd();
		}
		else {
			return str;
		}

		if(flag) {
			if(tCmd.equals("")) {
				str += " " + Messages.getString("Title.1") + m_lineSeparator;	
			}
			else {
				str += " " + MessageFormat.format(Messages.getString("Title.2"), new Object[] {tCmd.length()}) + m_lineSeparator;
				str += tCmd + m_lineSeparator;
			}
		}
		else {
			if(tCmd.equals("")) {
				str += " " + Messages.getString("Title.3") + m_lineSeparator;	
			}
			else {
				str += " " + MessageFormat.format(Messages.getString("Title.4"), new Object[] {tCmd.length()}) + m_lineSeparator;
				str += tCmd + m_lineSeparator;
			}
		}
		return str;
	}

	protected String[] validateTitle(String titleString) {
		String[] tokens = titleString.toUpperCase().split("\\s+");

		ArrayList<String> tokenList = new ArrayList<String>();
		for (String token: tokens) {
			token = token.trim();
			if (token.contains(SQL_DOT_SQL)) {
				if (token.matches("SQL.SQL\\d*")) {
					 tokenList.add(token);
				}
			}
			if (token.contains(SQL_DOT_COMMAND)) {
				if (token.matches("SQL.COMMAND\\d*")) {
					tokenList.add(token);
				}
			}
		}
		if (tokenList.size() > 0) {
			String[] forbiddenTokens = new String[tokenList.size()];
			tokenList.toArray(forbiddenTokens);
			return forbiddenTokens;
		}
		return null;
	}
	
	private String substituteColumnValues(String oldValue, String newValue, String titleCmdString) {
				
		// Recall val is always UPPERCASE.
		if(oldValue != null) {
			String temporaryTitleCmd = new String(titleCmdString);
			
			if (temporaryTitleCmd.contains("'") || temporaryTitleCmd.contains("\"")) {
				
				String[] titlePieces = splitStr(temporaryTitleCmd);
				
				for (String titlePiece: titlePieces) {
					if ( titlePiece.contains(oldValue) ) {					
						if (!(titlePiece.endsWith("'") || titlePiece.endsWith("\"")) && 
								!(titlePiece.startsWith("'") || titlePiece.startsWith("\"")) ) {
							
							int IndexOfTemporaryTitleCmd = temporaryTitleCmd.indexOf(titlePiece);
							int titlePieceLength = titlePiece.length();
							int lenTitleCmdString = titleCmdString.length();
							String F = titleCmdString.substring(0, IndexOfTemporaryTitleCmd);
							//int lenF = F.length();
							int nextIndex = (IndexOfTemporaryTitleCmd + titlePieceLength);
							String L = titleCmdString.substring(nextIndex, lenTitleCmdString);
							//int lenL = L.length();
							titleCmdString = F + newValue + ORA_SUBSTITUTION_MARKUP + L;
							//int lenTitleCmdString2 = titleCmdString.length();
							temporaryTitleCmd = temporaryTitleCmd.replaceFirst( titlePiece, newValue+ ORA_SUBSTITUTION_MARKUP );
							//int lenTemporaryTitleCmdAfter = temporaryTitleCmd.length();
						}
						else {
							// consume it by removing the titlePiece from temporaryTitleCmd because this is a quoted literal.
							temporaryTitleCmd = temporaryTitleCmd.replaceFirst(titlePiece, emptyStringOfSize(titlePiece));
						}
					}
					else {
						// consume it by removing the titlePiece from temporaryTitleCmd.
						temporaryTitleCmd = temporaryTitleCmd.replaceFirst(titlePiece, emptyStringOfSize(titlePiece));
					}
				} // end of for (String titlePiece: titlePieces) ...
			}
			else {
				titleCmdString = titleCmdString.replaceAll("(?i:" + oldValue + ")", newValue);
			}
		}
		return titleCmdString;
	}
	
	private String emptyStringOfSize ( String anyString ) {
		StringBuffer sb = new StringBuffer(anyString.length());
		for ( int i = 0; i < anyString.length(); i++) {
			sb.insert(i, " ");
		}
		return sb.toString();
	}

	private boolean hasSubstituitionOraMarkup(String token) {
		if (token.endsWith(ORA_SUBSTITUTION_MARKUP)) {
			return true;
		}
		return false;
	}
	
	private String removeSubstituitionOraMarkup(String token) {
		if (token.endsWith(ORA_SUBSTITUTION_MARKUP)) {
			token = token.substring(0, token.length()-(ORA_SUBSTITUTION_MARKUP).length());
		}
		return token;
	}
	
	public String getType() {
		return titleType;
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		String command = "";
		if (this instanceof BottomTitle) {
			command += "btitle ";
			if (ctx.getBTitleFlag()) {
				command += "on ";
			} else {
				command += "off ";
			}
			command += "\n";
			if (ctx.getBTitleCmd()!=null && ctx.getBTitleCmd().length()>0 ) {
				command += "btitle " + ctx.getBTitleCmd() + "\n";
			}
		} else if (this instanceof TopTitle) {
			command += "ttitle ";
			if (ctx.getTTitleFlag()) {
				command += "on ";
			} else {
				command += "off ";
			}
			command += "\n";
			if (ctx.getTTitleCmd()!=null && ctx.getTTitleCmd().length()>0 ) {
				command += "ttitle " + ctx.getTTitleCmd() + "\n";
			}
			
		}
		return command;
	}
}