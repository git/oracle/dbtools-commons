/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLPLUS;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * 
 * @author Ramprasad Thummala
 *
 */
public class TopTitle extends Title {
	final static String TITLE_TYPE_TOP = "TITLE_TYPE_TOP";
	
	protected String CMD = "tti";
	final String titleType = TITLE_TYPE_TOP;
				
	@Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		
		
    	if (cmd.getSql().trim().toLowerCase().startsWith(CMD)) {
            cmd.setSql(SQLPLUS.removeDashNewline(cmd,cmd.getSql()));
    		m_connection = conn;
    		String titlePtrn = "(?i:\\btt(?:i|it|itl|itle)\\b\\s*)";
    		String tCmd = cmd.getSql().trim();
    		if(tCmd.matches(titlePtrn)) {
    			tCmd = tCmd.replaceAll(titlePtrn, "").trim();
    			String str = buildShowTitle(this, ctx);
    			ctx.write(str);
    			return true;
    		}
    		
    		/*
    		String TCMDPTRN = "(?i:on|off)";
    		if(tCmd.matches(TCMDPTRN)) {
    			if(!tCmd.equalsIgnoreCase("on") || !tCmd.equalsIgnoreCase("off")) {
    				//tCmd.replaceAll("(?i:on|off)", "");
    				String[] tCmds = tCmd.split(TCMDPTRN);
    				for(String tcmd : tCmds) {
    					m_origCmd = tcmd + " ";
    				}
    				m_origCmd = m_origCmd.trim();
    			}
    		}
    		*/
    		
    		tCmd = tCmd.replaceFirst(titlePtrn, "").trim();
    		
    		if(tCmd.equalsIgnoreCase(ctx.TTITLE_OFF) || tCmd.toUpperCase().indexOf(ctx.TTITLE_OFF) > 0) {
        	    ctx.setTTitleFlag(false);
        	    return true;
    	    }
    	    else if(tCmd.equalsIgnoreCase(ctx.TTITLE_ON)) {
    	    	ctx.setTTitleFlag(true);
    	    }
    		/*
    	    else if(extractFirst("(?i:" + ctx.TTITLE_ON + ")", ctx.TTITLE_ON).length() > 0) {
    	    	ctx.setTTitleFlag(true);
    	    	tCmd = tCmd.replaceAll("\\s+(?i:on)\\s*", "").trim();
    	    	ctx.setTTitle(this);
    	    	if(tCmd.length() > 0) {
    	    	    m_titleCmd = tCmd;
    	    		ctx.setTTitleCmd(m_titleCmd);
    	    	}
    	    }
    	    */
    	    else if (
    	    		// The syntax of ttitle is the following. ON comes later after all other parameters and variables.
    	    		// TTI[TLE] [printspec [text | variable] ...] [ON | OFF].
    	    		// Pattern.compile("(((\\w|\\d|\\s)*\\s*['|\"](\\w|\\d|\\s)*['|\"])+\\s*[Oo][Nn]\\s*)").matcher(tCmd).find()
    	    		// Pattern.compile("(((\\w|\\d|\\s)*\\s+['|\"](\\w|\\d|\\s)*['|\"])+\\s+(?i:on)\\s*)").matcher(tCmd).find()
    	    		//Pattern.compile("(((.)*\\s+['|\"](.)*['|\"])+\\s+(?i:on)\\s*)").matcher(tCmd).find()
    	    		tCmd.toUpperCase().endsWith(" " + ctx.TTITLE_ON)
    	    	) {
    	    	ctx.setTTitleFlag(true);
    	    	int lastIndexOnOn = tCmd.toUpperCase().trim().lastIndexOf(ctx.TTITLE_ON);
    	    	tCmd = tCmd.trim().substring(0,lastIndexOnOn).trim();
    	    	ctx.setTTitle(this);
    	    	if(tCmd.length() > 0) {
    	    	    
    	    	    String[] errorStrings = validateTitle(tCmd);
    	    	    if (errorStrings == null) {
    	    	    	m_titleCmd = tCmd;
    	    	    	ctx.setTTitleCmd(m_titleCmd);
    	    	    }
    	    	    else {
    	    	    	for (String errorString : errorStrings ) {
    	    	    		ctx.write(MessageFormat.format(Messages.getString("TITLE_ERROR"), new Object[] { errorString }) + m_lineSeparator);
    	    	    	}
    	    	    	return false;
    	    	    }
    	    	}
    	    }
    	    //else if(Pattern.compile("\\b\\s+(?i:on)\\s+\\b").matcher(tCmd).find()) {
    	    //	ctx.setTTitleFlag(true);
    	    //	m_titleCmd = tCmd.replaceAll("\\s+(?i:on)\\s+", "");
    	    //	ctx.setTTitleCmd(m_titleCmd);
    	    //}
    	    else {
    	    	ctx.setTTitleFlag(true);
    	    	String[] errorStrings = validateTitle(tCmd);
    	    	if (errorStrings == null) {
    	    		m_titleCmd = tCmd;
	    	    	ctx.setTTitleCmd(m_titleCmd);
	    	    }
	    	    else {
	    	    	for (String errorString : errorStrings ) {
	    	    		ctx.write(MessageFormat.format(Messages.getString("TITLE_ERROR"), new Object[] { errorString }) + m_lineSeparator);
	    	    	}
	    	    	return false;
	    	    }
    	    }
    	    m_linesize = ((Integer)ctx.getProperty(ctx.SETLINESIZE)).intValue() + m_lineSeparator.length();
    	    ctx.setTTitle(this);
    	    m_parsedList = new ArrayList<String>();
    	    return true;
    	}
    	return false;
    }

}