/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import oracle.dbtools.db.ConnectionResolver;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.util.Service;

/**
 * Poor-man's db-links
 * https://vadimtropashko.wordpress.com/2014/04/07/automatic-database-links-in-sqldeveloper/
 * 
 * Typical usage:

set autodblink on;

(select * from employees@gbr30060_hr -- assuming there is connection "gbr30060_hr"
minus
select * from employees)
union
(select * from employees
minus
select * from employees@gbr30060_hr);

 * @author Dim
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class TransparentGateway extends CommandListener {
    //private static final String AUTO_DB_LINKS = "Auto.DB.Links";
    private Set<String> links; // temporary db links populated from connections
    
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    
    private static HashMap<Connection, Boolean> autoDbLinksAtConnection = new HashMap<Connection, Boolean>();//doneill: could this be a leak. RESTRICT as R4 anyway
    
    public void beginEvent( Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd ) {
        cleanLinks(conn, ctx);
        
        if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && 
                (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
            return;
        }
        //if( !Boolean.TRUE.equals(ctx.getProperty(AUTO_DB_LINKS)) )
        if( !Boolean.TRUE.equals(autoDbLinksAtConnection.get(conn)) )
            return;
        if( cmd.getStmtType() != SQLCommand.StmtType.G_C_SQL
         && cmd.getStmtType() != SQLCommand.StmtType.G_C_PLSQL )
            return;

        String sql = cmd.getSql();
        if( sql.indexOf('@') < 0 )
            return;
        
        List<LexerToken> src = LexerToken.parse(sql);
        boolean linkIsNext = false;
        int adjustPos = 0;
        for( LexerToken t : src ) {
            String token = t.content;
            if( "@".equals(token) ) {   //$NON-NLS-1$
                linkIsNext = true;
                continue;
            }
            if( linkIsNext ) { 
                String scrambled = token.toUpperCase();
                if( token.charAt(0)=='"' ) {
                    token = token.substring(1, token.length()-1);
                    scrambled = Service.into2chars(token);
                    sql = cmd.getSql(); // may be more than one dblink to substitute
                    // I'm struggling to have the same behaviour in script and grid.
                    // setSql (==setModifiedSql) affects script only
                    cmd.setSql(sql.substring(0, t.begin+adjustPos)+scrambled+sql.substring(t.end+adjustPos));
                    adjustPos = scrambled.length()-(t.end-t.begin);
                }
                for( String candidateConn : ConnectionResolver.getConnectionNames() ) {
                    Properties connectionInfo = ConnectionResolver.getConnectionInfo(candidateConn);
                    String connName = (String)connectionInfo.get("ConnName");
                    if( connName == null )
                        connName = candidateConn.substring(candidateConn.indexOf("%23")+3);
                    if( token.equalsIgnoreCase(connName) )
                        try {
                            Statement stmt = conn.createStatement();
                            String key = "sid"; //$NON-NLS-1$
                            Object sidValue = connectionInfo.get(key);
                            if( sidValue == null ) {
                                key = "serviceName"; //$NON-NLS-1$
                                sidValue = connectionInfo.get(key);
                                key = "service_name"; //$NON-NLS-1$
                                if( sidValue == null )
                                    return;
                            }
                            Object pwd = connectionInfo.get("password");
                            if( pwd == null ) {
                                JPasswordField field = new JPasswordField(10);
                                int action = JOptionPane.showConfirmDialog(null, field,"Enter Password for "+token,JOptionPane.OK_CANCEL_OPTION);
                                if( action < 0 )
                                    return;
                                else //JOptionPane.showMessageDialog(null,"Your password is "+new String(pwd.getPassword()));
                                    pwd = new String(field.getPassword());
                              
                            }
                            boolean linkExists = false;                            
                            String user = Service.handleMixedCase((String)connectionInfo.get("user"));
                            Object hostname = connectionInfo.get("hostname");
                            Object port = connectionInfo.get("port");
                            
                            String searchLink = "select 'USER='||username||host from user_db_links where db_link like '"+scrambled+".%' or db_link = '"+scrambled+"'";
                            String candidate = DBUtil.getInstance(conn).executeReturnOneCol(searchLink);
                            if( candidate != null ) {
                                linkExists = true;
                                // decide to report duplicate link or not
                                List<LexerToken> src1 = LexerToken.parse(candidate);
                                String u = null;
                                String h = null;
                                String p = null;
                                String s = null;
                                for( LexerToken t1 : src1 ) {
                                    if( "=".equals(t1.content) )
                                        continue;
                                    if( "".equals(u) ) {
                                        u = Service.handleMixedCase(t1.content);
                                        continue;
                                    }
                                    if( "".equals(h) ) {
                                        h = t1.content;
                                        continue;
                                    }
                                    if( "".equals(p) ) {
                                        p = t1.content;
                                        continue;
                                    }
                                    if( "".equals(s) ) {
                                        s = t1.content;
                                        continue;
                                    }                                        
                                    if( "USER".equals(t1.content) ) {
                                        u = "";
                                        continue;
                                    }
                                    if( "HOST".equals(t1.content) ) {
                                        h = "";
                                        continue;
                                    }
                                    if( "PORT".equals(t1.content) ) {
                                        p = "";
                                        continue;
                                    }
                                    if( key.toUpperCase().equals(t1.content) ) {
                                        s = "";
                                        continue;
                                    }
                                }
                                System.out.println(u+" "+h+" "+p+" "+s+" ");
                                if( !user.equals(u) || !hostname.equals(h) || !port.equals(p) || !sidValue.equals(s) )
                                    log_error(ctx, "Name conflict: Database link "+scrambled+" already exists");
                            }
                            
                            if( !linkExists ) {
                                String cs = "CREATE DATABASE LINK "+scrambled+" \n"  //$NON-NLS-1$
                                        + "CONNECT TO "+user+" IDENTIFIED BY "+pwd+" \n" //$NON-NLS-1$
                                        + "USING '(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = "+hostname+")(PORT = "+port+")))(CONNECT_DATA = ("+key.toUpperCase()+" = "+sidValue+"))) '"; //$NON-NLS-1$
                                stmt.execute(cs);
                                links.add(scrambled);
                                log_info(ctx, "Temporary Database Link " + scrambled + " created\n"); //$NON-NLS-1$
                            }
                            //System.out.println(cs); // (authorized)
                        } catch( Exception e ) {
                            log_error(ctx, "CREATE DATABASE LINK failed: "+e.getMessage()); // (authorized)
                        }        
                }
            }
            linkIsNext = false;
        }

    }

    public void endEvent( Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd ) {
        if ((ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && 
                (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
            return;
        }
        try { // 20918122: for some reason this is not executed by default for query against remote db
            if( Boolean.TRUE.equals(autoDbLinksAtConnection.get(conn)) && conn.getAutoCommit() ) 
                conn.commit();           
        } catch (SQLException e) {
        }
        cleanLinks(conn, ctx);        
    }

    private synchronized void cleanLinks( Connection conn, ScriptRunnerContext ctx ) {
        if( links != null )
    		for( String link : links ) 
    			try {
    				Statement stmt = conn.createStatement();
    				String sql = "DROP DATABASE LINK "+link; //$NON-NLS-1$
    				stmt.execute(sql);
    				//System.out.println(sql); // (authorized)            
                    log_info(ctx, "Temporary Database Link " + link + " dropped\n"); //$NON-NLS-1$
                } catch( SQLException e ) {
                	try {
                		if( e.getErrorCode() == 2018 )
                			log_error(ctx, e.getMessage()+": most likely you have an open query result set (grid)"); //$NON-NLS-1$
                		else
                			log_error(ctx, e.getMessage()); // (authorized)
                	} catch( Exception e1 ) {
                		System.out.println("TransparentGateway.cleanLinks(): "+e1.getMessage());
                	}
    			} catch( Exception e ) {
    			    log_error(ctx, e.getMessage()); // (authorized)
    			}
        links = new HashSet<String>();
    }

    public boolean handleEvent( Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd ) {
        String sql = cmd.getSql();
        if( cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("setautodblink") ) {
            List<LexerToken> src = LexerToken.parse(sql);
            if( 2 < src.size() && "set".equals(src.get(0).content.toLowerCase()) //$NON-NLS-1$
             && src.get(1).content.toLowerCase().startsWith("autodblink") //$NON-NLS-1$
            ) {
                String state = src.get(2).content.toLowerCase();
                log_info(ctx, "autodblink ="+state);
                if( "on".equals(state) )                    
                    //ctx.putProperty(AUTO_DB_LINKS,Boolean.TRUE);
                    autoDbLinksAtConnection.put(conn, Boolean.TRUE);
                else if( "off".equals(state) )
                    //ctx.putProperty(AUTO_DB_LINKS,Boolean.FALSE);
                    autoDbLinksAtConnection.put(conn, Boolean.FALSE);
                return true;
            }
        }
        
        return false; 
    }
    
    private void log_error( ScriptRunnerContext ctx, String msg ) {
    	try {
        	ctx.write(msg);
    	} catch( Throwable t ) {
    		logger.log(Level.SEVERE, msg);
    	}
    }
    private void log_info( ScriptRunnerContext ctx, String msg ) {
    	try {
        	ctx.write(msg);
    	} catch( Throwable t ) {
    		logger.log(Level.INFO, msg);
    	}
    }
    
}
