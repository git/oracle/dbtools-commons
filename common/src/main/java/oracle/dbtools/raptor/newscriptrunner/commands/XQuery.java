/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

public class XQuery extends CommandListener {
    // private static final String XQUERY = "xquery";
    private static final String SELECT_STUB = "select column_value from {0}"; //$NON-NLS-1$
    //private static final String SELECT_STUB = "select {0} as column_value from dual"; //$NON-NLS-1$
    private static final String XTABLE_STUB = "xmltable({0})"; //$NON-NLS-1$
    private static final String XQUERY_STUB = "xmlquery({0} returning content)"; //$NON-NLS-1$
    private static final String PASSING_STUB = "{0} passing {1}"; //$NON-NLS-1$
    
    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        // grab the context values
        String baseuri = (String) ctx.getProperty(SetXQuery.XQUERY_BASEURI);
        String order = (String) ctx.getProperty(SetXQuery.XQUERY_ORDERING);
        String node = (String) ctx.getProperty(SetXQuery.XQUERY_NODE);
        String context = (String) ctx.getProperty(SetXQuery.XQUERY_CONTEXT);
        
        // build up the pre string
        StringBuilder sb = new StringBuilder();
        if (baseuri != null && baseuri.length() > 0) {
            sb.append("declare base-uri " + enclose(baseuri,'"') + "; "); //$NON-NLS-1$ //$NON-NLS-2$
        }
        if (node != null && (! "default".equals(node.toLowerCase()))) {
            sb.append("declare node " + node.toLowerCase() + "; "); //$NON-NLS-1$ //$NON-NLS-2$
        }
        if (order != null && (! "default".equals(order.toLowerCase()))) {
            sb.append("declare ordering " + order.toLowerCase() + "; "); //$NON-NLS-1$ //$NON-NLS-2$
        }
        
        // change the class to select from unknown
        cmd.setStmtClass(SQLCommand.StmtType.G_C_SQL);
        // replace the sql with the scrubbed sql
        String cmdSQL = cmd.getSql();
        Pattern p = Pattern.compile("(?i)\\s*xquery\\s*;?\\s*"); //$NON-NLS-1$ // insert your pattern here
        Matcher m = p.matcher(cmdSQL);
        int position = (m.find()) ?  m.end() : 0;
        String selectSQL = cmdSQL.substring(position).trim();
        
        String queryString = enclose(selectSQL, '\'');

        String passingContext = (context != null && context.length() > 0)
            ? MessageFormat.format(XQUERY_STUB, enclose(context, '\''))
            : null;

        String xmlQuery = (passingContext != null) 
            ? MessageFormat.format(PASSING_STUB, queryString, passingContext)
            : queryString;
        
        String newSQL = MessageFormat.format(SELECT_STUB, MessageFormat.format(XTABLE_STUB, xmlQuery));
        
          cmd.setResultsType(SQLCommand.StmtResultType.G_R_QUERY);
        cmd.setSql(newSQL);
    }
    
    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        // nothing to do here
    }
    
    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        return false;
    }
    
    private String enclose(String str, char c) {
        return escape(str, c, true);
    }
    
    private String escape(String str, char c) {
        return escape(str, c, false);
    }
    
    private String escape(String str, char c, boolean enclose) {
        StringBuilder sb = new StringBuilder();
        
        if (enclose) {
            sb.append(c);
        }
        
        if (str != null) {
            String pattern = "" + c;
            String replacement = pattern + c;
            sb.append(str.replaceAll(Pattern.quote(pattern),replacement));
        }
        
        if (enclose) {
            sb.append(c);
        }
        
        return sb.toString();
    }
}
