/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.alias;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptParser;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;
import oracle.dbtools.raptor.query.Bind;


/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=AliasCommand.java"
 *         >Barry McGillin</a>
 *
 */
public class AliasCommand extends CommandListener implements IHelp {
	private static String LIST = "list"; //$NON-NLS-1$
	private static String DROP = "drop"; //$NON-NLS-1$
	private static String ALIAS = "alias"; //$NON-NLS-1$
	private static String SAVE = "save"; //$NON-NLS-1$
	private static String LOAD = "load"; //$NON-NLS-1$
	private static String DESC = "desc"; //$NON-NLS-1$

	/*
	 * HELP DATA
	 *
	 */
	public String getCommand() {
		return "ALIAS"; //$NON-NLS-1$
	}

	public String getHelp() {
		return CommandsHelp.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java
	 * .sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String[] tokens = cmd.getSql().trim().split("[ \t\n]+"); //$NON-NLS-1$
		if (tokens.length > 0) {
			String query = null;
			if (tokens[0].toLowerCase().equals(getCommandName())) {
				if (tokens.length == 1) {
					ArrayList<String> aliases = Aliases.getInstance().getAliases();
					Iterator<String> it = aliases.iterator();
					while (it.hasNext()) {
						ctx.write(it.next() + "\n"); //$NON-NLS-1$
					}
					return handleReturn(true);
				} else if (tokens.length > 1) {
					String key = ""; //$NON-NLS-1$

					if (tokens[1].toLowerCase().equals(DROP)) {
						if (tokens.length == 3) {
							ctx.write(Aliases.getInstance().drop(tokens[2]) + "\n"); //$NON-NLS-1$
							return handleReturn(true);
						}
						ctx.write(Messages.getString("AliasCommand.21")); //$NON-NLS-1$
						return true;
					} else if (tokens[1].toLowerCase().equals(DESC)) {
						String alias = ""; //$NON-NLS-1$
						if (tokens.length > 2)
							alias = tokens[2];
						if (tokens.length >= 4) {
							ArrayList<String> aliases = Aliases.getInstance().getAliases();
							if (aliases.contains(alias)) {
								Alias a = Aliases.getInstance().get(alias);
								String newValue = ""; //$NON-NLS-1$
								for (int i = 0; i < 3; i++)
									newValue = cmd.getSql().substring(cmd.getSql().indexOf(" ") + 1); //$NON-NLS-1$
								a.setDesc(newValue);
								return true;
							}
						} else {
							ctx.write(MessageFormat.format("ALIAS-22: {0} Not enough arguments", new Object[] { alias })); //$NON-NLS-1$
						}
					} else if (tokens[1].toLowerCase().equals(LIST)) {
						// placeholder same as no args
						String alias = ""; //$NON-NLS-1$
						ArrayList<String> aliases = Aliases.getInstance().getAliases();
						if (tokens.length > 2 && tokens[2].length() > 0) {
							alias = tokens[2];
							if (aliases.contains(alias)) {
								String aliasdesc = alias ;
								if (Aliases.getInstance().get(alias).getDesc().length()>0) {
									aliasdesc = alias + " - " + Aliases.getInstance().get(alias).getDesc(); //$NON-NLS-1$
								}
								
								String line = new String(new char[aliasdesc.length()]).replace("\0", "-"); //$NON-NLS-1$ //$NON-NLS-2$
								ctx.write(MessageFormat.format("{0}\n{1}\n\n", new Object[] { aliasdesc, line })); //$NON-NLS-1$
								ctx.write(Aliases.getInstance().get(alias).getQuery() + "\n\n"); //$NON-NLS-1$
							} else {
								ctx.write(MessageFormat.format(Messages.getString("AliasCommand.27"), new Object[] { alias })); //$NON-NLS-1$
							}
						} else {
							Iterator<String> it = aliases.iterator();
							int i=0;
							while (it.hasNext()) {
								String a = it.next().trim();
								if (a.length()>i) i=a.length();
							}
							 it = aliases.iterator();
							while (it.hasNext()) {
								String x = it.next();
								Alias a = Aliases.getInstance().get(x);
								if (i<30)
									x=String.format("%0$-"+i+"s", x); //$NON-NLS-1$ //$NON-NLS-2$
								else 
									x=String.format("%0$-30s", x); //$NON-NLS-1$
								if (a.getDesc().length()>0)
 								ctx.write(x + " - " + a.getDesc() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
								else 
									ctx.write(x + "\n"); //$NON-NLS-1$
							}
						}
						return handleReturn(true);
					} else if (tokens[1].toLowerCase().equals(SAVE)) {
						if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING) != null) {
							if (tokens.length > 2 && tokens[2] != null) {
								ctx.write(Aliases.getInstance().save(tokens[2]));
							} else {
								ctx.write(Aliases.getInstance().save());
							}
						} else {
							ctx.write(Messages.getString("AliasCommand.29")); //$NON-NLS-1$
						}
						return handleReturn(true);
					} else if (tokens[1].toLowerCase().equals(LOAD)) {
						if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING) != null) {
							if (tokens.length > 2 && tokens[2] != null) {
								ctx.write(Aliases.getInstance().load(tokens[2]));
							} else {
								ctx.write(Aliases.getInstance().load());
							}
						} else {
							ctx.write(Messages.getString("AliasCommand.30")); //$NON-NLS-1$
						}
						return handleReturn(true);
					} else if (cmd.getSql().contains("=") && (!tokens[1].startsWith("="))) { //$NON-NLS-1$ //$NON-NLS-2$
						String[] preEqSplit = cmd.getSql().trim().split("=")[0].split("\\s+"); //$NON-NLS-1$ //$NON-NLS-2$
						key = preEqSplit[1];
						if (preEqSplit.length > 2) {
							key = key + preEqSplit[2];
						}
						if (key.equalsIgnoreCase("alias")) { //$NON-NLS-1$
							ctx.write(Messages.getString("AliasCommand.10")); //$NON-NLS-1$
							return true;
						}
						String payload = cmd.getSql().substring(cmd.getSql().toLowerCase().indexOf("=") + 1).trim(); //$NON-NLS-1$
						if (payload.startsWith(":")) { //$NON-NLS-1$
							if (payload.length() > 1) {
								payload = payload.substring(1);
								Bind b = ctx.getVarMap().get(payload.toUpperCase());
								if (b != null) {
									query = b.getValue();
								}
							}
							if ((query == null) || (query.equals(""))) { //$NON-NLS-1$
								ctx.write(Messages.getString("AliasCommand.16")); //$NON-NLS-1$
								return handleReturn(true);
							}
						} else {
							String payloadUpper = payload.toUpperCase();
							if ((payload.length() > 5) && payloadUpper.startsWith("Q'") //$NON-NLS-1$
									&& payload.endsWith(ScriptParser.getEndQuoteString(payload.substring(2, 3)) + "'")) { //$NON-NLS-1$
								payload = payload.substring(3, payload.length() - 2);
							} else {
								if ((payload == null || payload.equals(""))) { //$NON-NLS-1$
									ctx.write(Messages.getString("AliasCommand.20")); //$NON-NLS-1$
									return handleReturn(true);
								}
							}
							query = payload;
						}
						// with Binds introduced, then shortly after set by
						// default.
						Aliases.getInstance().add(key, query);
						return handleReturn(true);
					} else {
						ctx.write(Messages.getString("AliasCommand.31")); //$NON-NLS-1$
						return handleReturn(true);
					}
				} else {
					ctx.write(Messages.getString("AliasCommand.32")); //$NON-NLS-1$
					ctx.write(getHelp());
					return handleReturn(true);
				}
			}
		}
		return false;// not alias handleReturn(false);
	}

	public String getCommandName() {
		return ALIAS;
	}

	boolean handleReturn(boolean b) {
		if (b == false) {
			// doWhenever(true);
		}
		// may want to do whenever - do not want to fall back to default as it
		// thinks it is a sql statement as it ends with ;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java
	 * .sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql
	 * .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

}
