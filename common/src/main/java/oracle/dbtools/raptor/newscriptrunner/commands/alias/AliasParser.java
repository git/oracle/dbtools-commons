/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.alias;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import oracle.dbtools.raptor.utils.XMLHelper;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=AliasParser.java"
 *         >Barry McGillin</a>
 *
 */
public class AliasParser {
	private static final String ALIAS = "alias"; //$NON-NLS-1$
	private static final String ALIASES = "aliases"; //$NON-NLS-1$
	private static final String DESC = "description"; //$NON-NLS-1$
	private static final String QUERIES = "queries"; //$NON-NLS-1$
	private static final String QUERY = "query"; //$NON-NLS-1$
	private static final String SQL = "sql"; //$NON-NLS-1$
	private static final String NAME = "name"; //$NON-NLS-1$
	protected static Logger LOGGER = Logger.getLogger(AliasParser.class.getName());
	private XMLDocument _document;
	private DOMParser parser;
	ArrayList<Alias> aliases = new ArrayList<Alias>();

	public AliasParser() {
	}

	public AliasParser(URL u) {
		try {
			parser.parse(u.openStream());
			_document = parser.getDocument();
			// XMLHelper.
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * 	<alias name="tables">
		<description><![CDATA[tables <schema> - show tables from schema]]></description>
		<queries> 
			<query>
				<sql><![CDATA[ select table_name "TABLES"from user_tables]]>
                    </sql>
                 </query>
             </queries>		
		</alias>
	 */

	public void putXML(File f, ArrayList<Alias> aliasAL) throws Exception {
		f.createNewFile();
		Document dom;
		Element e = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		dom = db.newDocument();
		Element rootEle = dom.createElement(ALIASES);
		// create data elements and place them under root
		for (Alias anAlias: aliasAL) {
			e = dom.createElement(ALIAS);
			e.setAttribute(NAME,anAlias.getName());
			Element desc=dom.createElement(DESC);
			desc.appendChild(dom.createCDATASection(anAlias.getDesc()));
			e.appendChild(desc);
			Element queries=dom.createElement(QUERIES);
			Element query=dom.createElement(QUERY);
			Element sql=dom.createElement(SQL);
			sql.appendChild(dom.createCDATASection(anAlias.getQuery()));
			query.appendChild(sql);
			queries.appendChild(query);
			e.appendChild(queries);
			rootEle.appendChild(e);
		}

		dom.appendChild(rootEle);
		FileOutputStream fos = null;
		try {
			Transformer tr = TransformerFactory.newInstance().newTransformer();
			tr.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$
			tr.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
			tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8"); //$NON-NLS-1$
			tr.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS, SQL+" "+DESC); //$NON-NLS-1$
			tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4"); //$NON-NLS-1$ //$NON-NLS-2$
	
			fos=new FileOutputStream(f.getAbsolutePath());
			tr.transform(new DOMSource(dom), 
					new StreamResult(fos));
		} finally {
			if (fos!=null) {
				try {
					fos.close();
				} catch (IOException ioe) {
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,ioe.getLocalizedMessage());
				}
			}
		}

	}
	
	public ArrayList<Alias> processXML(InputStream is) throws Exception {
		try {
		if (parser == null) {
			parser = new DOMParser();
		} else {
			parser.reset();
		}
		parser.setPreserveWhitespace(false);
		parser.parse(is);
		_document = parser.getDocument();
		NodeList rootNodes = _document.getChildNodes();
		for (int i = 0; i < rootNodes.getLength(); i++) {
			processNodes(rootNodes.item(i));
		}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					// ignore.
				}
			}
		}
		return aliases;
	}
	
	public ArrayList<Alias> processXML(URL u) throws Exception {
		return processXML(u.openStream());
	}

	private void processNodes(Node item) {
		NodeList children = item.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			String childName = child.getNodeName();
			if (childName.equalsIgnoreCase(ALIAS)) { //$NON-NLS-1$
				newItem(child);
			}
		}
	}

	private void newItem(Node child) {
		String aliasname = XMLHelper.getAttributeNode(child, NAME);
		String desc = ""; //$NON-NLS-1$
		String query = ""; //$NON-NLS-1$
		NodeList items = child.getChildNodes();
		for (int i = 0; i < items.getLength(); i++) {
			Node x = items.item(i);
			if (x.getNodeName().equalsIgnoreCase(DESC)) {
				Node n1 = x.getFirstChild();
				if (n1 instanceof Text) {
					desc = ((Text) n1).getTextContent();
				}
			}
			if (x.getNodeName().equalsIgnoreCase(QUERIES)) {
				NodeList ql = x.getChildNodes();
				for (int j = 0; j < ql.getLength(); j++) {
					Node y = ql.item(j);
					if (y.getLocalName().equalsIgnoreCase(QUERY)) {
						NodeList sqls = y.getChildNodes();
						for (int k = 0; k < sqls.getLength(); k++) {
							Node z = sqls.item(k);
							Node n1 = z.getFirstChild();
							if (n1 instanceof Text) {
								query = ((Text) n1).getTextContent();
							}

						}

					}
				}

			}

		}
		aliases.add(new Alias(aliasname, query, desc));
	}

	public static void main(String[] args) {

		AliasParser ap = new AliasParser();
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL u = cl.getResource("oracle/dbtools/raptor/newscriptrunner/commands/alias/aliases.xml"); //$NON-NLS-1$
		try {
			ap.processXML(u);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
