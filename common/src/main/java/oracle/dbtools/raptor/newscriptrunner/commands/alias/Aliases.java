/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.alias;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.PlatformUtils;

/**
 * 
 */
public class Aliases {
	public static final String CONFIGFILE = File.separator + "aliases.xml"; //$NON-NLS-1$
	public static final String CONFIGBACK = File.separator + "aliases.xml~"; //$NON-NLS-1$

	public static String getBase() {
		String storage = System.getProperty("user.home") + File.separator + ".sqlcl"; //$NON-NLS-1$ //$NON-NLS-2$
		if (PlatformUtils.isWindows()) {
			String appData = System.getenv("APPDATA"); //$NON-NLS-1$
			if (appData != null) {
				storage = appData + File.separator + "sqlcl"; //$NON-NLS-1$
			} else {
				storage = null;
			}
		}
		return storage;
	}

	private HashMap<String, Alias> aliases = new HashMap<String, Alias>();
	// assumption we only need to know if it is a bind for now
	private static Aliases _INSTANCE;
	private Set<String> defaultAliases = new HashSet<String>();

	public static Aliases getInstance() {
		if (_INSTANCE == null) {
			_INSTANCE = new Aliases();
			_INSTANCE.load();
		}
		return _INSTANCE;
	}

	/**
	 * Thus was called initially from AliasCommand, is Bind is true by default -
	 * if neither with subs or with binds is used.
	 */

	public Aliases() {

		URL u = this.getClass().getClassLoader().getResource("oracle/dbtools/raptor/newscriptrunner/commands/alias/aliases.xml"); //$NON-NLS-1$
		AliasParser parser = new AliasParser();
		try {
			ArrayList<Alias> filealiases = parser.processXML(u);
			Iterator<Alias> it = filealiases.iterator();
			while (it.hasNext()) {
				Alias a = it.next();
				defaultAliases.add(a.getName().toUpperCase());
				aliases.put(a.getName(), a);
			}
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getLocalizedMessage());
		}
	}

	public void add() {

	}

	public void add(String name, String query, String desc) {
		if (aliases.containsKey(name)) {
			aliases.remove(name);
		}
		Alias alias = new Alias(name, query, desc);
		aliases.put(name, alias);

	}

	public void add(String name, String query) {
		add(name, query, ""); //$NON-NLS-1$
	}

	/**
	 * @return
	 * 
	 */

	public String drop(String name) {
		if (aliases.containsKey(name)) {
			aliases.remove(name);
			return MessageFormat.format(Messages.getString("Aliases.0"), new Object[] { name }); //$NON-NLS-1$
		}
		return MessageFormat.format(Messages.getString("Aliases.1"), new Object[] { name }); //$NON-NLS-1$
	}

	/**
	 * 
	 */

	public ArrayList<String> getAliases() {
		ArrayList<String> list = new ArrayList<String>();
		for (String alias : aliases.keySet()) {
			list.add(alias);
		}
		Collections.sort(list);
		return list;
	}

	/**
	 * 
	 */

	public boolean contains(String key) {
		if (aliases.containsKey(key)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean addDescription(String key, String description) {
		if (aliases.containsKey(key)) {
			(aliases.get(key)).setDesc(description);
			return true;
		} else {
			return false;
		}
	}

	public Alias get(String key) {
		return aliases.get(key);
	}

	public String save() {
		return save(null);
	}

	public String save(String filename) {
		String base = getBase();
		new File(base).mkdirs();
		File file = null;
		if (filename != null) {
			if (filename.lastIndexOf('/') > -1) {
				if (!filename.substring(filename.lastIndexOf('/')).contains(".")) //$NON-NLS-1$
					// Add .xml if there is no ext.
					filename = filename + ".xml"; //$NON-NLS-1$
			} else {
				if (!filename.contains(".")) { //$NON-NLS-1$
					// Add .xml if there is no ext.
					filename = filename + ".xml"; //$NON-NLS-1$
				}
			}
			file = new File(filename);
		} else {
			filename = base + CONFIGFILE;
			file = new File(filename);
		}
		ArrayList<Alias> aliasesAL = new ArrayList<Alias>();
		AliasParser parser = new AliasParser();
		try {

			Iterator<Entry<String, Alias>> it = aliases.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, Alias> a = it.next();
				Alias al = a.getValue();
				if (!(defaultAliases.contains(al.getName().toUpperCase()))) {
					aliasesAL.add(al);
				}
			}
			if (base == null) {
				return Messages.getString("Aliases.8"); //$NON-NLS-1$
			}
			if (file.exists()) {
				try {
					int a = 1;
					boolean found = false;
					String filenamebak = filename + ".bak_"; //$NON-NLS-1$
					while (!found) {
						File findme = new File(filenamebak + a);
						if (findme.exists() && a < 3) {
							a++;
						} else {
							Files.deleteIfExists(Paths.get(filenamebak + a));
							found = true;
						}
					}
					if (file.exists())
						Files.move(Paths.get(filename), Paths.get(filenamebak + a), REPLACE_EXISTING); // $NON-NLS-1$
				} catch (Exception e) {
					return MessageFormat.format(Messages.getString("Aliases.11"), new Object[] { filename, filename + ".bak", e.getMessage() }); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
			if (base != null) {
				parser.putXML(new File(filename), aliasesAL);
			}

		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getLocalizedMessage());
		}
		return MessageFormat.format(Messages.getString("Aliases.13"), filename); //$NON-NLS-1$
	}

	public String load() {
		String base = getBase();
		if (base == null) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, Messages.getString("Aliases.14")); //$NON-NLS-1$
		}
		String filename = base + CONFIGFILE;
		return load(filename);
	}

	public String load(String filename) {
		File file = null;
		URLConnection uc = null;
		try {

			if (filename != null) {
				// Check that we have the right named files
				if (filename.lastIndexOf('/') > -1) {
					if (!filename.substring(filename.lastIndexOf('/')).contains(".")) //$NON-NLS-1$
						// Add .xml if there is no ext.
						filename = filename + ".xml"; //$NON-NLS-1$
				} else {
					if (!filename.contains(".")) { //$NON-NLS-1$
						// Add .xml if there is no ext.
						filename = filename + ".xml"; //$NON-NLS-1$
					}
				}
				// Check if we are http or ftp and if so, open up a URL
				// Connection
				if (startsWithHttpOrFtp(filename) && (haveIBytes(filename))) {
					uc = new URL(filename.replaceAll("\\\\", "/")).openConnection(); //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					file = new File(filename);
					if (file.exists()) {
						uc = file.toURI().toURL().openConnection();
					}
				}

			} else {
				// No file set, lets load the basic one we have stored.
				String base = getBase();
				if (base == null) {
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, Messages.getString("Aliases.14")); //$NON-NLS-1$
				}
				filename = base + CONFIGFILE;
				file = new File(filename);
				if (file.exists()) {
					uc = file.toURI().toURL().openConnection();
				}
			}
		} catch (MalformedURLException e) {// eatme
		} catch (IOException e) {// and me
		}

		if (uc != null) {
			try {
				AliasParser parser = new AliasParser();
				if (uc.getInputStream() != null) {
					ArrayList<Alias> filealiases = parser.processXML(uc.getInputStream());
					Iterator<Alias> it = filealiases.iterator();
					while (it.hasNext()) {
						Alias a = it.next();
						if (!(defaultAliases.contains(a.getName().toUpperCase()))) {
							aliases.put(a.getName(), a);
						}
					}
					return Messages.getString("Aliases.15"); //$NON-NLS-1$
				}
			} catch (Exception e) {
				Logger.getLogger(this.getClass().getName()).log(Level.WARNING, e.getLocalizedMessage());
			}

		} else {
			return MessageFormat.format(Messages.getString("Aliases.16"), filename); //$NON-NLS-1$
		}

		return ""; //$NON-NLS-1$
	}

	static boolean containsFile(String inFile) {
		// can sometimes be .*file:
		boolean retVal = false;
		if (inFile.indexOf(":") > 1) { //$NON-NLS-1$
			String stub = inFile.substring(0, inFile.indexOf(":")); //$NON-NLS-1$
			if (stub.endsWith("file") && (stub.indexOf("/") == -1) && (stub.indexOf("\\") == -1)) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				retVal = true;
			}
		}
		return retVal;
	}

	/**
	 * try \ in case its a microsoft file:\\
	 * 
	 * @param base
	 * @return
	 */
	boolean haveIBytesRaw(String base) {
		if (startsWithHttpOrFtp(base) == false) {
			return false;
		}
		InputStream is = null;
		URLConnection c = null;
		try {
			String lower = base.toLowerCase();
			if (containsFile(lower)) { // $NON-NLS-1$
				base = base.substring(lower.indexOf("file:")); //$NON-NLS-1$
			}
			if (base.indexOf("\\") != -1) { //$NON-NLS-1$
				base.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
				c = new URL(base).openConnection(); // $NON-NLS-1$ //$NON-NLS-2$

				is = c.getInputStream();
				if (is.read() != -1) {
					return true;
				}
			}
		} catch (MalformedURLException e1) {
			return false;
		} catch (IOException e1) {
			return false;
		} catch (Exception e2) {// was getting illegal argument exception on
								// !"XYZ$% file input
			return false;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					// ignore.
				}
			}
		}
		return false;
	}

	static boolean haveIBytes(String base) {
		if (startsWithHttpOrFtp(base) == false) {
			return false;
		}
		InputStream is = null;
		URLConnection c = null;
		try {
			String lower = base.toLowerCase();
			if (containsFile(lower)) { // $NON-NLS-1$
				base = base.substring(lower.indexOf("file:")); //$NON-NLS-1$
			}
			c = new URL(base.replaceAll("\\\\", "/")).openConnection(); //$NON-NLS-1$ //$NON-NLS-2$
			is = c.getInputStream();
			if (is.read() != -1) {
				return true;
			}
		} catch (MalformedURLException e1) {
			return false;
		} catch (IOException e1) {
			return false;
		} catch (Exception e2) {// was getting illegal argument exception on
								// !"XYZ$% file input
			return false;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					// ignore.
				}
			}
		}
		return false;
	}

	static boolean startsWithHttpOrFtp(String base) {
		if (base == null) {
			return false;
		}
		String lower = base.toLowerCase();
		if (!((lower.startsWith("http://") //$NON-NLS-1$
				|| lower.startsWith("http:\\\\") //$NON-NLS-1$
				|| lower.startsWith("https://") //$NON-NLS-1$
				|| lower.startsWith("https:\\\\") //$NON-NLS-1$
				|| containsFile(lower) || lower.startsWith("ftp:\\\\") //$NON-NLS-1$
				|| lower.startsWith("ftp://")))) { //$NON-NLS-1$
			return false;
		}
		return true;
	}
}
