/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.bridge;

import java.sql.Connection;
import java.util.ArrayList;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptResult;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;


/**
 * The Class BridgeCmd.
 */
public class BridgeCmd extends CommandListener implements IHelp {

	/** The _bridge parser. */
	BridgeParser _bridgeParser = null;

	/** The status ok. */
	boolean _statusOK = true;

	/*
	 * HELP DATA
	 *
	 */
	public String getCommand() {
		return "BRIDGE";
	}

	public String getHelp() {
		return CommandsHelp.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return false;
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		//create the tables and populate them with data
		try {
			_statusOK = true;//should be reset
			String str = cmd.getModifiedSQL();
			_bridgeParser = null;//rest the _bridgeParser each time as the constructor BridgeParser can fail leaving a stale BridgeParser to carry on ..
			_bridgeParser = new BridgeParser(str);
			//loop through all the bridge tables
			for (BridgeTableDef tableDef : _bridgeParser.getTableDefs()) {
				tableDef.setDefaultConn(conn);
				tableDef.setScriptRunnerContext(ctx);
				saveInfo(tableDef.createTable(), ctx);
			}
		} catch(BridgeParserException e) {
			handleExceptionPrintHelp(Messages.getString("BRIDGE_COMMAND_INVALID_SYNTAX"),ctx);
			_statusOK = false;
	  } catch (Exception e) {
			handleException(e.getLocalizedMessage(), ctx);
			_statusOK = false;
		}
	}

	private void saveInfo(BridgeTableCreationInfo info, ScriptRunnerContext ctx) {
		ctx.write("\n"+info.getReport()+"\n");
		ArrayList<ScriptResult> scriptResults = (ArrayList<ScriptResult>) ctx.getProperty(ScriptRunnerContext.RESULTS);
		if (scriptResults == null) {
			scriptResults = new ArrayList<ScriptResult>();
			ctx.putProperty(ScriptRunnerContext.RESULTS, scriptResults);
		}
		scriptResults.add(info);
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		//setup the query to run against the new tables or do nothing
		if (_statusOK) {
			if (_bridgeParser.getQuery() != null) {
				cmd.setSql(_bridgeParser.getQuery());
				return false;
			} else {
				return true;
			}
		}
		return true;//do nothing
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		//drop any unwanted tables.
		if (_bridgeParser != null && _bridgeParser.getQuery() != null) {
			for (BridgeTableDef tableDef : _bridgeParser.getTableDefs()) {
				if (!tableDef._isAppend) {
					tableDef.drop();
				}
			}
		}
	}

	private void handleExceptionPrintHelp(String message, ScriptRunnerContext ctx) {
		ctx.write("\n"+message + "\n");
		ctx.write(getHelp());
		ctx.putProperty(ScriptRunnerContext.ERR_ENCOUNTERED, Boolean.TRUE);
		ctx.putProperty(ScriptRunnerContext.ERR_ANY_CMDS_IN_SCRIPT, Boolean.TRUE);
		ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE, message);
	}
	private void handleException(String message, ScriptRunnerContext ctx) {
		ctx.write("\n"+message + "\n");
		ctx.putProperty(ScriptRunnerContext.ERR_ENCOUNTERED, Boolean.TRUE);
		ctx.putProperty(ScriptRunnerContext.ERR_ANY_CMDS_IN_SCRIPT, Boolean.TRUE);
		ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE, message);
	}
}
