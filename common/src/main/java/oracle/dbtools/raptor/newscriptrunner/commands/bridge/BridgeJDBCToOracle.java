/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.bridge;

import java.sql.Types;

public class BridgeJDBCToOracle {

	public static String map(boolean isOracle, int precision, int scale, int jdbcType, boolean is12c) {
		String retVal = "OTHER"; //$NON-NLS-1$
		switch (jdbcType) {
		case Types.BIT:
			retVal = "NUMBER(10)"; //$NON-NLS-1$
			break;
		case Types.TINYINT:
			retVal = "NUMBER(10)"; //$NON-NLS-1$
			break;
		case Types.SMALLINT:
			retVal = "NUMBER(10)"; //$NON-NLS-1$
			break;
		case Types.INTEGER:
			retVal = "NUMBER(10)"; //$NON-NLS-1$
			break;
		case Types.BIGINT:
			retVal = "NUMBER(38)"; //$NON-NLS-1$
			break;
		case Types.FLOAT:
			retVal = "NUMBER"; //$NON-NLS-1$
			break;
		case Types.REAL:
			retVal = "NUMBER"; //$NON-NLS-1$
			break;
		case Types.DOUBLE:
			retVal = "NUMBER"; //$NON-NLS-1$
			break;
		case Types.NUMERIC:
			if (isOracle) {
				//(0,-127) = NUMBER with no precision or scale
				if (precision == 0 && scale == -127) {
					retVal = "NUMBER";
				} else if (precision == 126 && scale == -127) { //NUMBER(126,-127)  = FLOAT with no precision or scale
					retVal = "FLOAT";
				} else if (precision > 38) { //precision max 38
					retVal = "NUMBER(" + precision + ")";
				} else {
					retVal = "NUMBER(" + precision + "," + scale + ")";
				}
			} else {
				retVal = "NUMBER(38,2)"; //$NON-NLS-1$
			}
			break;
		case Types.DECIMAL:
			retVal = "NUMBER(38,2)"; //$NON-NLS-1$
			break;
		case Types.NCHAR:
		case Types.CHAR:
			if (is12c) {
				if (isOracle) {// treat the precision as if it where BYTES
					if (precision == 0) {// jdbc has not returned the size
						retVal = "CHAR(32767 BYTE)"; //$NON-NLS-1$
					} else if (precision <= 32767) { // max size of bytes in Oracle VARCHAR2
						retVal = "CHAR(" + precision + " BYTE)"; //$NON-NLS-1$  //$NON-NLS-2$
					} else { // too large for Oracle VARCHAR2, use CLOB
						retVal = "CLOB"; //$NON-NLS-1$
					}
				} else {// 3rd party database, treat the precision as if it where CHAR to make certain that the data fits during insert
					if (precision == 0) {// jdbc has not returned the size
						retVal = "CHAR(200 CHAR)";//smaller than the max, but we wanted to use a reasonable size (not too large) //$NON-NLS-1$
					} else if (precision <= 8191) { // max size of bytes in Oracle VARCHAR2
						retVal = "CHAR(" + precision + " CHAR)"; //$NON-NLS-1$  //$NON-NLS-2$
					} else { // too large for Oracle VARCHAR2, use CLOB
						retVal = "CLOB"; //$NON-NLS-1$
					}
				}
			} else {
				if (isOracle) {// treat the precision as if it where BYTES
					if (precision == 0) {// jdbc has not returned the size
						retVal = "CHAR(2000 BYTE)"; //$NON-NLS-1$
					} else if (precision <= 2000) { // max size of bytes in Oracle VARCHAR2
						retVal = "CHAR(" + precision + " BYTE)"; //$NON-NLS-1$  //$NON-NLS-2$
					} else { // too large for Oracle VARCHAR2, use CLOB
						retVal = "CLOB"; //$NON-NLS-1$
					}
				} else {// 3rd party database, treat the precision as if it where CHAR to make certain that the data fits during insert
					if (precision == 0) {// jdbc has not returned the size
						retVal = "CHAR(200 CHAR)";//smaller than the max, but we wanted to use a reasonable size (not too large)  //$NON-NLS-1$
					} else if (precision <= 500) { // max size of bytes in Oracle VARCHAR2
						retVal = "CHAR(" + precision + " CHAR)"; //$NON-NLS-1$  //$NON-NLS-2$
					} else { // too large for Oracle VARCHAR2, use CLOB
						retVal = "CLOB"; //$NON-NLS-1$
					}
				}
			}
			break;
		case Types.NVARCHAR:
		case Types.VARCHAR:
			if (isOracle) {// treat the precision as if it where BYTES
				if (precision == 0) {// jdbc has not returned the size
					if (is12c) {
						retVal = "VARCHAR2(32767 BYTE)"; //$NON-NLS-1$
					} else {
						retVal = "VARCHAR2(4000 BYTE)"; //$NON-NLS-1$
					}
				} else if ((precision <= 32767) && is12c) {
					retVal = "VARCHAR2(" + precision + " BYTE)"; //$NON-NLS-1$  //$NON-NLS-2$
				} else if (precision <= 4000) { // max size of bytes in Oracle VARCHAR2
					retVal = "VARCHAR2(" + precision + " BYTE)"; //$NON-NLS-1$  //$NON-NLS-2$
				} else { // too large for Oracle VARCHAR2, use CLOB
					retVal = "CLOB"; //$NON-NLS-1$
				}
			} else {// 3rd party database, treat the precision as if it where CHAR to make certain that the data fits during insert
				if (!is12c) {
					if (precision == 0) {// jdbc has not returned the size
						retVal = "VARCHAR2(4000 BYTE)"; //$NON-NLS-1$
					} else if (precision <= 1000) { // max size of bytes in Oracle VARCHAR2
						retVal = "VARCHAR2(" + precision + "CHAR)"; //$NON-NLS-1$  //$NON-NLS-2$
					} else { // too large for Oracle VARCHAR2, use CLOB
						retVal = "CLOB"; //$NON-NLS-1$
					}
				} else {
					if (precision == 0) {// jdbc has not returned the size
						retVal = "VARCHAR2(32767 BYTE)"; //$NON-NLS-1$
					} else if (precision <= 8191) { // max size of bytes in Oracle VARCHAR2
						retVal = "VARCHAR2(" + precision + "CHAR)"; //$NON-NLS-1$  //$NON-NLS-2$
					} else { // too large for Oracle VARCHAR2, use CLOB
						retVal = "CLOB"; //$NON-NLS-1$
					}
				}
			}
			break;
		case Types.LONGVARCHAR:// swapped from varchar2(4000 byte)
			retVal = "CLOB"; //$NON-NLS-1$
			break;
		case Types.DATE:
			retVal = "DATE"; //$NON-NLS-1$
			break;
		case Types.TIME:
			retVal = "DATE"; //$NON-NLS-1$
			break;
		case Types.TIMESTAMP:
			retVal = "DATE"; //$NON-NLS-1$
			break;
		case Types.BINARY:
			if (is12c) {
				if (precision == 0) { // jdbc has not returned the size
					retVal = "RAW(32767)"; //$NON-NLS-1$
				} else if (precision <= 32767) {// its reasonable
					retVal = "RAW(" + precision + ")"; //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					if (precision > 32767) {
						retVal = "BLOB"; //$NON-NLS-1$
					} else {
						retVal = "RAW(32767)"; //$NON-NLS-1$
					}
				}
			} else {
				if (precision == 0) { // jdbc has not returned the size
					retVal = "RAW(2000)"; //$NON-NLS-1$
				} else if (precision <= 2000) {// its reasonable
					retVal = "RAW(" + precision + ")"; //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					if (precision > 2000) {
						retVal = "BLOB"; //$NON-NLS-1$
					} else {
						retVal = "RAW(2000)"; //$NON-NLS-1$
					}
				}
			}
			break;
		case Types.VARBINARY:
			if (is12c) {
				if (precision == 0) { // jdbc has not returned the size
					retVal = "RAW(32767)"; //$NON-NLS-1$
				} else if (precision <= 32767) {// its reasonable
					retVal = "RAW(" + precision + ")"; //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					if (precision > 32767) {
						retVal = "BLOB"; //$NON-NLS-1$
					} else {
						retVal = "RAW(32767)"; //$NON-NLS-1$
					}
				}
			} else {
				if (precision == 0) { // jdbc has not returned the size
					retVal = "RAW(2000)"; //$NON-NLS-1$
				} else if (precision <= 2000) {// its reasonable
					retVal = "RAW(" + precision + ")"; //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					if (precision > 2000) {
						retVal = "BLOB"; //$NON-NLS-1$
					} else {
						retVal = "RAW(2000)"; //$NON-NLS-1$
					}
				}
			}
			break;
		case Types.BLOB:
			retVal = "BLOB"; //$NON-NLS-1$
			break;
		case Types.CLOB:
			retVal = "CLOB"; //$NON-NLS-1$
			break;
		case Types.BOOLEAN:
			retVal = "NUMBER(10)"; //$NON-NLS-1$
			break;
		case Types.LONGVARBINARY: // swapped from long raw which works but is old fashioned
			retVal = "BLOB"; //$NON-NLS-1$
			break;
		case Types.NCLOB: // flattening to clob
			retVal = "CLOB"; //$NON-NLS-1$
			break;
		case Types.OTHER:
			if (is12c) {
				retVal = "VARCHAR2(32767)"; //$NON-NLS-1$
			} else {
				retVal = "VARCHAR2(4000)"; //$NON-NLS-1$
			}
			break;
		default:
			if (is12c) {
				retVal = "VARCHAR2(32767)"; //$NON-NLS-1$
			} else {
				retVal = "VARCHAR2(4000)"; //$NON-NLS-1$
			}
			break;
		}
		return retVal;
	}
}
