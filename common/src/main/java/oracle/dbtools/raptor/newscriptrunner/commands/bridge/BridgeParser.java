/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.bridge;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;

import oracle.dbtools.db.ConnectionResolver;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;

public class BridgeParser {
	/**
	 * BRIDGE  target(repousers$repo) AS (select distinct schemaname from mgv_all_schema_details where capturedorconverted='CONVERTED'),
	           all_objects$targetdb   AS target(select status,owner,object_name from all_objects where owner in (select distinct owner from repousers$repo) )
	   SELECT * FROM DUAL;
	  
	     or like the following better
	     
	     BRIDGE all_objects$targetdb   AS target(select status,owner,object_name from all_objects where owner in ({select schemaname from mgv_all_schema_details where capturedorconverted='CONVERTED'}) )
	     SELECT * FROM DUAL;
	*/
	public static final String CMD = "BRIDGE"; //$NON-NLS-1$
	private ArrayList<BridgeTableDef> _tableDefs = new ArrayList<BridgeTableDef>();
	private String _query = null;

	BridgeParser(String str) throws BridgeParserException {
		try{
			String stmt = str.trim();
			int start = 0;
			if (stmt.toUpperCase().startsWith(SQLCommand.SCRIPTTABLE)) {
				stmt = stmt.substring(SQLCommand.SCRIPTTABLE.length()).trim();
				start = SQLCommand.SCRIPTTABLE.length();
			}
			if (stmt.toUpperCase().startsWith(CMD)) {
				processCMD(str.substring(start));
			}
		} catch (Exception e){
			throw new BridgeParserException(e);
		}
	}

	private void processCMD(String stmt) throws Exception {
		String stmtLower = stmt.toUpperCase();
		int start = stmtLower.indexOf(CMD);
		int end = start + CMD.length();
		start = end;
		processRemainingStmt(stmt, start);
	}

	private void processRemainingStmt(String stmt, int start) throws Exception {
		String stmtLower = stmt.toLowerCase();
		//was indexof("as" but tablenames can have "as" in them
		int end = stmtLower.indexOf(" as", start); //$NON-NLS-1$
		if (end != -1) {
			end = end + 1;
		}
		int end2 = stmtLower.indexOf(")as", start); //$NON-NLS-1$
		if ((end2 != -1) && (end2 < end)) {
			end = end2 + 1;
		}
		end2 = stmtLower.indexOf("\nas", start); //$NON-NLS-1$
		if ((end2 != -1) && (end2 < end)) {
			end = end2 + 1;
		}
		end2 = stmtLower.indexOf("\tas", start); //$NON-NLS-1$
		if ((end2 != -1) && (end2 < end)) {
			end = end2 + 1;
		}
		String tableNameDetails = stmt.substring(start, end).trim();
		String tableConn = null;
		String tableName = tableNameDetails;
		int tableConnIndex = tableNameDetails.indexOf("("); //$NON-NLS-1$
		if (tableConnIndex != -1) {
			tableConn = tableNameDetails.substring(0, tableConnIndex);
			tableName = tableNameDetails.substring(tableConnIndex + 1, tableNameDetails.indexOf(")"));
		}
		start = end + 2;
		end = findMatchingBracket(stmtLower, start, -1);
		String tableDefDetails = stmt.substring(start, end).trim();
		String tableDefConn = null;
		int tableDefIndex = tableDefDetails.indexOf("("); //$NON-NLS-1$
		if (tableDefIndex == 0) {
			tableDefDetails = tableDefDetails.substring(1, tableDefDetails.length());
		} else {
			tableDefConn = tableDefDetails.substring(0, tableDefIndex);
			tableDefDetails = tableDefDetails.substring(tableDefIndex + 1, tableDefDetails.length());
		}
		start = end;
		String queryStmt = null;
		String remainingStmt = stmtLower.substring(start).trim();
		boolean isAppend = false;
		boolean isReplace = false;
		boolean isTruncate  =false;
		boolean isSkip = false;
		if(remainingStmt.startsWith(")append")){
      isAppend = true;
      start= start + 7;
      remainingStmt = stmtLower.substring(start).trim();
  } else if(remainingStmt.startsWith(")replace")){
      isReplace = true;
      start = start + 8;
      remainingStmt = stmtLower.substring(start).trim();
  } else if(remainingStmt.startsWith(")truncate")){
  	  isTruncate = true;
  	  start = start + 9;
      remainingStmt = stmtLower.substring(start).trim();
  } else if(remainingStmt.startsWith(")skip")){
	  isSkip = true;
	  start = start + 5;
    remainingStmt = stmtLower.substring(start).trim();
  } else if(remainingStmt.startsWith(")")){
      start = start + 1;
      remainingStmt = stmtLower.substring(start).trim();
  }

	  _tableDefs.add(new BridgeTableDef(tableName, getConnection(tableConn),tableDefDetails, getConnection(tableDefConn),isAppend,isReplace,isTruncate,isSkip));
    
    if(remainingStmt.startsWith(",")){  //$NON-NLS-1$
        start = stmt.indexOf(",",start) +1;//to make up for trim
        String substring = stmt.substring(start);
        String substringLower  = stmtLower.substring(start);
        processRemainingStmt(stmt, start);
    } else {
        queryStmt = stmt.substring(start).trim();
        if(queryStmt.length() == 0 || queryStmt.equals(";") || queryStmt.equals("/")){  //$NON-NLS-1$  //$NON-NLS-2$
            queryStmt =null;
        }
        _query = queryStmt;
        
    }
	}

	private Connection getConnection(String connStr) throws Exception {
		Connection conn = null;
		if (connStr == null) {
			return null;
		}
		try {
			if (connStr.startsWith("\"")) {
				conn = ConnectionResolver.getConnection(connStr);
			} else {
				boolean cloneConnection = false;
				//if the connection starts with $, use a cloned connection
				if(connStr.startsWith("$")){
					cloneConnection = true;
					connStr = connStr.substring(1);
				}
				String qualifiedName = ConnectionResolver.getQualifiedConnectionName(connStr);
				if(cloneConnection){
				  conn = ConnectionResolver.getUniqueConnection(qualifiedName);
				} else {
				  conn = ConnectionResolver.getConnection(qualifiedName);
				}
			}
		} catch (Throwable t) {
			throw new BridgeException(MessageFormat.format(Messages.getString("BRIDGE_TABLE_FAIL_CREATE_CONNECTION"),connStr), t); // This needs to trickle back up to the top BridgeCmd
		}
		if (conn == null) {
			throw new BridgeException(MessageFormat.format(Messages.getString("BRIDGE_TABLE_FAIL_CREATE_CONNECTION"),connStr));// This needs to trickle back up to the top BridgeCmd
		}
		return conn;
	}

	private int findMatchingBracket(String stmtLower, int start, int nestLevel) {
		if (start == -1) {
			throw new IllegalArgumentException();
		}
		int rBracket = stmtLower.indexOf(")", start + 1); //$NON-NLS-1$
		int lBracket = stmtLower.indexOf("(", start + 1); //$NON-NLS-1$
		if (lBracket != -1 && lBracket < rBracket) {// found a nested bracket
			nestLevel++;
			return findMatchingBracket(stmtLower, lBracket, nestLevel);
		} else if (nestLevel == 0) { //found an end bracket at the top level
			return rBracket;
		} else { //found an end bracket for a nested pair
			nestLevel--;
			return findMatchingBracket(stmtLower, rBracket, nestLevel);
		}
	}

	public ArrayList<BridgeTableDef> getTableDefs() {
		return _tableDefs;
	}

	public String getQuery() {
		return _query;
	}
}
