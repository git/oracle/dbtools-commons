/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.bridge;

import java.sql.SQLException;
import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.ScriptResult;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;

public class BridgeTableCreationInfo implements ScriptResult{
	String _tableName = null;
	int _numRows = 0;
	boolean _commited = false;
	boolean _isAppended = false;
	Exception _constraintException= null;
	boolean tableAlreadyExists = false;

	public BridgeTableCreationInfo(String tableName) {
		_tableName = tableName;
	}

	public void setNumRows(int numRows) {
		_numRows = numRows;
	}

	public void setCommit(boolean commited) {
		_commited = commited;
	}

	public String getReport() {
	 if(isAppended()){
		 return MessageFormat.format(Messages.getString("BRIDGE_SUCCESS_APPEND") ,getNumRows(),getTableName());
	 } else if(!tableAlreadyExists) {
		 return MessageFormat.format(Messages.getString("BRIDGE_SUCCESS_CREATED") ,getTableName(),getNumRows());
	 } else {
	   return MessageFormat.format(Messages.getString("BRIDGE_SUCCESS_ALREADY_EXISTS") ,getTableName(),getNumRows());
	 }
	}

	private int getNumRows() {
		return _numRows;
	}

	private String getTableName() {
		return _tableName;
	}

	private boolean isAppended() {
		return _isAppended;
	}

	public void setAppended(boolean isAppended) {
	 _isAppended = isAppended;
	}

	public void setConstraintException(SQLException constraintException) {
		_constraintException = constraintException;
	}

	public void tableAlreadyExist(boolean tableAlreadyExists){
	  this.tableAlreadyExists = tableAlreadyExists;
	}
}
