/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.bridge;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.BLOB;
import oracle.sql.CLOB;

/**
 * The Class BridgeTableDef.
 */
public class BridgeTableDef {
	/** The _table name. */
	String _tableName = null;
	/** The _target table name. */
	String _targetTableName = null;
	/** The _target conn name. */
	String _targetConnName = null;
	/** The _query. */
	String _query = null;
	/** The _queries. */
	ArrayList<String> _queries = new ArrayList<String>();
	/** The _source conn name. */
	String _sourceConnName = null;
	/** The _source conn. */
	Connection _targetConn = null, _sourceConn = null;
	/** The _default conn. */
	Connection _defaultConn = null;
	/** The _is append. */
	boolean _isAppend = false;
	/** The _is replace. */
	boolean _isReplace = false;
	boolean _isTruncate = false;
	boolean _isSkip = false;
	/** The _ zero length string to space. */
	boolean _ZeroLengthStringToSpace = true;
	/** The m_ctx. */
	ScriptRunnerContext m_ctx = null;
	/** The _db12c features. */
	boolean _db12cFeatures = false;
	final int batchSize = 1000;

	/** The Constant BRIDGEREPLACEKEY. */
	public static final String BRIDGEREPLACEKEY = "BRIDGEREPLACEKEY";

	/**
	 * Instantiates a new bridge table def.
	 * 
	 * @param tableName
	 *            the table name
	 * @param tableConn
	 *            the table conn
	 * @param query
	 *            the query
	 * @param queryConn
	 *            the query conn
	 * @param isAppend
	 *            the is append
	 * @param isReplace
	 *            the is replace
	 * @param isTruncate 
	 */
	public BridgeTableDef(String tableName, Connection targetConn, String query, Connection sourceConn, boolean isAppend, boolean isReplace, boolean isTruncate, boolean isSkip) {
		_tableName = tableName;
		_query = query;
		_isAppend = isAppend;
		_isReplace = isReplace;
		_isTruncate = isTruncate;
		_isSkip = isSkip;
//		if(_isTruncate){
//			_isAppend = true;
//		}
		_targetConn = targetConn;
		_sourceConn = sourceConn;
	}

	/**
	 * Execute.
	 * 
	 * @return true, if successful
	 * @throws BridgeException 
	 */
	public BridgeTableCreationInfo createTable() throws BridgeException {
		BridgeTableCreationInfo result = new BridgeTableCreationInfo(getTableName());
		initializeConnections();
		ResultSet rs = null;
		DBUtil sourceDBUtil = null;
		CopyToOracleHint hint = null;
		boolean moveData = true;
		try {
			lockConnections(getSourceConnection(), getTargetConnection());
			initializeConnection(getTargetConnection());
			sourceDBUtil = DBUtil.getInstance(getSourceConnection());
			processDynamicSQLToInlineList();
			boolean isLoop = processLoopSQLToMultipleQueries();
			boolean isOracle = isOracle(getSourceConnection());
		  //truncate if required
			if(_isTruncate){
				truncate(getTargetTableName());
			}
			for (String query : getQueries()) {
				try {
					if (isInterupted()) {
						throw new InterruptedException();
					}
					hint = new CopyToOracleHint(query);
					query = hint.queryWithoutHint();
					//execute the query and get the result set
					rs = getResultSet(sourceDBUtil, query);
					//create the table from the resulset metadata
					BridgeTableDetails tableDetails = createTableDetails(rs.getMetaData(), getSourceConnection().getMetaData(), isOracle, hint);
					boolean tableCreated=false;
					ArrayList<String> autoColumns = new ArrayList<String>();
					if (isInterupted()) {
						throw new InterruptedException();
					}
					try{
							createTableAction(tableDetails);
							tableCreated=true;
					} catch(Exception e){
						if(_isSkip){
							moveData = false;
						} else if (_isAppend||_isTruncate) { // otherwise ignore creation issues and just insert
							moveData = true;//save problem with creation issue
							result.tableAlreadyExist(true);
						} else {
							throw e;
						}
					}
					if (tableCreated) {
						autoColumns = autoColumnsCalculate(tableDetails, rs);
					}
					if(moveData){
						//insert the data
						int numRows = insertData(tableDetails, rs);
						result.setNumRows(numRows);
						commitData(_targetConn);
						result.setCommit(true);
					}
					if(tableDetails.hasConstraints()){
						try{
							createConstraints(tableDetails);
						}catch(SQLException e){
							result.setConstraintException(e);
						}
					}
					if (tableCreated) {
						addAutonumber(autoColumns);
					}
				} finally {
					DBUtil.closeResultSet(rs);
				}
			}
		} catch (BridgeException|InterruptedException|SQLException|IOException e) {
			try {
				Connection targetConn = getTargetConnection();
				if(targetConn == null){
					throw new BridgeException(MessageFormat.format(Messages.getString("BRIDGE_TABLE_CREATION_FAILED_NO_CONNECTION"),getTableFullName(hint))+" \n"+e.getLocalizedMessage());
				}
				rollbackChanges(targetConn);
			} catch (SQLException rbException) {
				throw new BridgeException(MessageFormat.format(Messages.getString("BRIDGE_TABLE_CREATION_FAILED_AND_ROLLBACK_FAILED"),getTableFullName(hint))+" \n"+e.getLocalizedMessage(), e);
			}
			throw new BridgeException(MessageFormat.format(Messages.getString("BRIDGE_TABLE_CREATION_FAILED_AND_ROLLBACK_OK"),getTableFullName(hint))+" \n"+e.getLocalizedMessage(), e);
		} finally {
			try {
				DBUtil.closeResultSet(rs);
			} catch (Throwable t) {
				throw new BridgeException(Messages.getString("BRIDGE_TABLE_CREATION_FAILED_TO_CLOSE_RS"), t);
			}
			//unlock the source and target connection
			unlockConnections(getSourceConnection(), getTargetConnection());
		}
		return result;
	}
	/**
	 * Auto columns calculate.
	 * 
	 * @param oracleColumns
	 *            the oracle columns
	 * @param rs
	 *            the rs
	 * @return the array list
	 */
	private ArrayList<String> autoColumnsCalculate(BridgeTableDetails tableDetails, ResultSet rs) {
		ArrayList<BridgeColumnDetails> oracleColumns=tableDetails.getColumnDetails();
		ArrayList<String> autoColumns = new ArrayList<String>();
		if (true/* locked above DBUtil.getConnectionResolver().lock(_sourceConn)*/) {
			try {
				ResultSetMetaData rsmd = rs.getMetaData();
				for (int i = 0; i < (oracleColumns.size()); i++) {//the last col had been explicitly skipped in pre release - debug shows no dummy last column
					if (oracleColumns.get(i).getTargetDataTypeStr().toUpperCase(Locale.US).startsWith("NUMBER") && (rsmd.isAutoIncrement(i + 1))) { //$NON-NLS-1$
						autoColumns.add(createValidIdentifier(rsmd.getColumnName(i + 1), "part")); //$NON-NLS-1$
					}
				}
			} catch (SQLException se) {
				Logger.getLogger(getClass().getName()).log(Level.SEVERE,Messages.getString("BRIDGE_ERROR_FOR_LOG"), se);
			} finally {
				//locked above DBUtil.getConnectionResolver().unlock(_sourceConn);
			}
		}
		return autoColumns;
	}
	
	/**
	 * Adds the autonumber.
	 * 
	 * @param oracleColumns
	 *            the oracle columns
	 * @param autoColumns
	 *            the auto columns
	 * @throws SQLException
	 *             the sQL exception
	 */
	private void addAutonumber(ArrayList<String> autoColumns) throws SQLException {
		String autoTableName = this.getTargetTableName();
		if (true){//locked above DBUtil.getConnectionResolver().lock(_targetConn)) {
			CallableStatement stmt = null;
			try {
				if (!(isOracle(_targetConn))) {
					return;
				}
				for (String thisColumn : autoColumns) {
					try {
						stmt = _targetConn.prepareCall("Declare \n" + //$NON-NLS-1$
								"  pragma autonomous_transaction; \n" + //$NON-NLS-1$
								"  e_already_exists EXCEPTION; \n" + //$NON-NLS-1$
								"  PRAGMA EXCEPTION_INIT (e_already_exists, -955); \n" + //$NON-NLS-1$
								"  e_trig_already_exists EXCEPTION; \n" + //$NON-NLS-1$
								"  PRAGMA EXCEPTION_INIT (e_trig_already_exists, -4081); \n" + //$NON-NLS-1$
								"  tablel varchar2(100):= :b1; \n" + //$NON-NLS-1$
								"  columnl varchar2(100) := :b2; \n" + //$NON-NLS-1$
								"  eimmediate varchar2(4000 byte) := NULL; \n" + //$NON-NLS-1$
								"  postfix varchar2(100) := NULL; \n" + //$NON-NLS-1$
								"  seqname varchar2(200) := NULL; \n" + //$NON-NLS-1$
								"  trigname varchar2(200) := NULL; \n" + //$NON-NLS-1$
								"  seqcreated boolean := false; \n" + //$NON-NLS-1$
								"  trigcreated boolean := false; \n" + //$NON-NLS-1$
								"  outputl varchar2(200) := 'OK'; \n" + //$NON-NLS-1$
								"  FUNCTION truncateStringByteSize(p_work VARCHAR2, p_bsize NUMBER) RETURN VARCHAR2 \n" + //$NON-NLS-1$
								"  IS \n" + //$NON-NLS-1$
								"    v_work VARCHAR2(4000); \n" + //$NON-NLS-1$
								"    v_bsize NUMBER(10); \n" + //$NON-NLS-1$
								"  BEGIN \n" + //$NON-NLS-1$
								"    IF LENGTHB(p_work) <= p_bsize THEN \n" + //$NON-NLS-1$
								"      return p_work; \n" + //$NON-NLS-1$
								"    END IF; \n" + //$NON-NLS-1$
								"    v_work := p_work; \n" + //$NON-NLS-1$
								"    v_work := SUBSTRB(v_work, 1, p_bsize); \n" + //$NON-NLS-1$
								"    WHILE INSTRC(p_work, v_work , 1, 1) <> 1 LOOP -- a character has been cut in half or in 2/3 or 3/4 by substrb (multibyte can have up to 4 bytes)  \n" + //$NON-NLS-1$
								"    --note each left over corrupt byte can be a single character \n" + //$NON-NLS-1$
								"      BEGIN \n" + //$NON-NLS-1$
								"        v_bsize := LENGTHB(v_work); \n" + //$NON-NLS-1$
								"        v_work := SUBSTRB(v_work, 1, v_bsize-1); \n" + //$NON-NLS-1$
								"      END; \n" + //$NON-NLS-1$
								"    END LOOP;  \n" + //$NON-NLS-1$
								"    return v_work; \n" + //$NON-NLS-1$
								"  END; \n" + //$NON-NLS-1$
								"  FUNCTION get_name(p_work VARCHAR2, p_suffix VARCHAR2) RETURN VARCHAR2 \n" + //$NON-NLS-1$
								"  IS \n" + //$NON-NLS-1$
								"    p_maxlen NUMBER(38,0) := 30; \n" + //$NON-NLS-1$
								"    v_suflen NUMBER := LENGTHB(p_suffix); \n" + //$NON-NLS-1$
								"    v_truncamount NUMBER; \n" + //$NON-NLS-1$
								"  BEGIN \n" + //$NON-NLS-1$
								"    IF LENGTHB(p_work) < p_maxlen - v_suflen THEN \n" + //$NON-NLS-1$
								"      RETURN p_work || p_suffix; \n" + //$NON-NLS-1$
								"    END IF; \n" + //$NON-NLS-1$
								"    v_truncamount := LENGTHB(p_work) + v_suflen - p_maxlen; \n" + //$NON-NLS-1$
								"    RETURN truncateStringByteSize(p_work, LENGTHB(p_work)-v_truncamount) || p_suffix; \n" + //$NON-NLS-1$
								"  END get_name; \n" + //$NON-NLS-1$
								"begin \n" + //$NON-NLS-1$
								"  for n in 0 .. 100 \n" + //$NON-NLS-1$
								"  loop \n" + //$NON-NLS-1$
								"    begin \n" + //$NON-NLS-1$
								"      if (n=0)  \n" + //$NON-NLS-1$
								"      then \n" + //$NON-NLS-1$
								"        postfix:='_'||'SEQ'; \n" + //$NON-NLS-1$
								"      else \n" + //$NON-NLS-1$
								"        postfix:='_'||n||'SEQ'; \n" + //$NON-NLS-1$
								"      end if; \n" + //$NON-NLS-1$
								"      seqname:=get_name(tablel || '_' || columnl ,postfix); \n" + //$NON-NLS-1$
								"      eimmediate := 'CREATE SEQUENCE  ' || seqname || chr(10) ||  \n" + //$NON-NLS-1$
								"      '  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE'; \n" + //$NON-NLS-1$
								"      -- thesequencewithseqnamein; \n" + //$NON-NLS-1$
								"      execute immediate eimmediate; \n" + //$NON-NLS-1$
								"      seqcreated := true; \n" + //$NON-NLS-1$
								"      --just skip already exists 3exception  \n" + //$NON-NLS-1$
								"    Exception  \n" + //$NON-NLS-1$
								"      when  e_already_exists then \n" + //$NON-NLS-1$
								"      null; \n" + //$NON-NLS-1$
								"    end; \n" + //$NON-NLS-1$
								"    if (seqcreated = true)  \n" + //$NON-NLS-1$
								"    then \n" + //$NON-NLS-1$
								"      for t in 0 .. 100 \n" + //$NON-NLS-1$
								"        loop \n" + //$NON-NLS-1$
								"        begin \n" + //$NON-NLS-1$
								"        if (t=0)  \n" + //$NON-NLS-1$
								"        then \n" + //$NON-NLS-1$
								"          postfix:='_'||'TRIG'; \n" + //$NON-NLS-1$
								"        else \n" + //$NON-NLS-1$
								"          postfix:='_'||t||'TRIG'; \n" + //$NON-NLS-1$
								"        end if; \n" + //$NON-NLS-1$
								"        trigname:=get_name(tablel||'_'||columnl,postfix); \n" + //$NON-NLS-1$
								"        --execute immediate  \n" + //$NON-NLS-1$
								"        eimmediate := \n" + //$NON-NLS-1$
								"'CREATE TRIGGER '||trigname||' BEFORE INSERT OR UPDATE ON ' || tablel || chr(10) || \n" + //$NON-NLS-1$
								"'FOR EACH ROW' || chr(10) || \n" + //$NON-NLS-1$
								"'DECLARE ' || chr(10) || \n" + //$NON-NLS-1$
								"'v_newVal NUMBER(12) := 0;' || chr(10) || \n" + //$NON-NLS-1$
								"'v_incval NUMBER(12) := 0;' || chr(10) || \n" + //$NON-NLS-1$
								"'BEGIN' || chr(10) || \n" + //$NON-NLS-1$
								"'  IF INSERTING AND :new.'|| columnl ||' IS NULL THEN' || chr(10) || \n" + //$NON-NLS-1$
								"'    SELECT  '||seqname||'.NEXTVAL INTO v_newVal FROM DUAL;' || chr(10) || \n" + //$NON-NLS-1$
								"'    -- If this is the first time this table have been inserted into (sequence == 1)' || chr(10) || \n" + //$NON-NLS-1$
								"'    IF v_newVal = 1 THEN ' || chr(10) || \n" + //$NON-NLS-1$
								"'      --get the max indentity value from the table' || chr(10) || \n" + //$NON-NLS-1$
								"'      SELECT NVL(max('||columnl||'),0) INTO v_newVal FROM '||tablel||';' || chr(10) || \n" + //$NON-NLS-1$
								"'      v_newVal := v_newVal + 1;' || chr(10) || \n" + //$NON-NLS-1$
								"'      --set the sequence to that value' || chr(10) || \n" + //$NON-NLS-1$
								"'      LOOP' || chr(10) || \n" + //$NON-NLS-1$
								"'           EXIT WHEN v_incval>=v_newVal;' || chr(10) || \n" + //$NON-NLS-1$
								"'           SELECT '||seqname||'.nextval INTO v_incval FROM dual;' || chr(10) || \n" + //$NON-NLS-1$
								"'      END LOOP;' || chr(10) || \n" + //$NON-NLS-1$
								"'    END IF;' || chr(10) || \n" + //$NON-NLS-1$
								"'   -- assign the value from the sequence to emulate the identity column' || chr(10) || \n" + //$NON-NLS-1$
								"'   :new.'||columnl||' := v_newVal;' || chr(10) || \n" + //$NON-NLS-1$
								"'  END IF;' || chr(10) || \n" + //$NON-NLS-1$
								"'END;'; \n" + //$NON-NLS-1$
								"        execute immediate eimmediate; \n" + //$NON-NLS-1$
								"        trigcreated := true; \n" + //$NON-NLS-1$
								"        exit; \n" + //$NON-NLS-1$
								"        --just skip already exists 3exception  \n" + //$NON-NLS-1$
								"        Exception  \n" + //$NON-NLS-1$
								"          when  e_already_exists then \n" + //$NON-NLS-1$
								"          null; \n" + //$NON-NLS-1$
								"          when e_trig_already_exists then \n" + //$NON-NLS-1$
								"            null; \n" + //$NON-NLS-1$
								"        end; \n" + //$NON-NLS-1$
								"      END LOOP;   \n" + //$NON-NLS-1$
								"      exit; \n" + //$NON-NLS-1$
								"    end if; \n" + //$NON-NLS-1$
								"  end loop; \n" + //$NON-NLS-1$
								"  if (seqcreated = false) \n" + //$NON-NLS-1$
								"  then  \n" + //$NON-NLS-1$
								"    outputl:= 'create sequence '||seqname||' failed:'; \n" + //$NON-NLS-1$
								"  end if; \n" + //$NON-NLS-1$
								"  if (trigcreated = false) \n" + //$NON-NLS-1$
								"  then \n" + //$NON-NLS-1$
								"    if (outputl = 'OK') \n" + //$NON-NLS-1$
								"    then \n" + //$NON-NLS-1$
								"      outputl := null; \n" + //$NON-NLS-1$
								"    end if; \n" + //$NON-NLS-1$
								"    outputl:= outputl || 'create trigger '||trigname||' failed'; \n" + //$NON-NLS-1$
								"  end if; \n" + //$NON-NLS-1$
								"  :op := outputl; \n" + //$NON-NLS-1$
								"end; " //$NON-NLS-1$
						);
						stmt.setString(1, autoTableName);
						stmt.setString(2, thisColumn);
						stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
						stmt.executeUpdate();
						String amIOK = stmt.getString(3);
						if ((amIOK == null) || (!(amIOK.equals("OK")))) { //$NON-NLS-1$
							Logger.getLogger(getClass().getName()).log(Level.SEVERE, amIOK == null ? "null" : amIOK); //$NON-NLS-1$
						}
					} catch (SQLException se) {
						Logger.getLogger(getClass().getName()).log(Level.SEVERE,Messages.getString("BRIDGE_ERROR_FOR_LOG"), se);
					} finally {
						if (stmt != null) {
							try {
								stmt.close();
							} catch (SQLException e) {
							}
						}
					}
				}
			} finally {
				//locked above DBUtil.getConnectionResolver().unlock(_targetConn);
			}
		}
	}
	private void truncate(String tableName) {
		DBUtil.getInstance(_targetConn).execute("TRUNCATE TABLE " + tableName); //$NON-NLS-1$	
	}

	private Object getTableFullName(CopyToOracleHint hint) {
		String sourceName = null;
		if(hint!=null){
			sourceName = hint.getFullName();
		}
		if(sourceName == null){
			sourceName = getTableName();
		}
		return sourceName;
	}

	private void createConstraints(BridgeTableDetails tableDetails) throws SQLException{
		DBUtil dbUtil = DBUtil.getInstance(_targetConn);
		String constraintSQL = tableDetails.getConstraintDDL();
		
		dbUtil.execute(constraintSQL);
		SQLException sqlex = dbUtil.getLastException();
		if (sqlex != null) {
			throw sqlex;
		}
	}

	private ResultSet getResultSet(DBUtil sourceDBUtil, String query) throws SQLException {
		return querySource(sourceDBUtil, query);
	}

	private void rollbackChanges(Connection conn) throws SQLException {
		// rollback on error
		conn.rollback();
	}

	private void lockConnections(Connection sourceConnection, Connection targetConnection) throws BridgeException {
		//acquire the lock for the source and target connection
		if (!LockManager.lock(targetConnection)) {
			throw new BridgeException(Messages.getString("BRIDGE_TARGET_CONNECTION_LOCK_FAILED"));
		}
		if (!LockManager.lock(sourceConnection)) {
			throw new BridgeException(Messages.getString("BRIDGE_SOURCE_CONNECTION_LOCK_FAILED"));
		}
	}

	private void unlockConnections(Connection sourceConnection, Connection targetConnection) {
		if (sourceConnection != null) {
			LockManager.unlock(sourceConnection);
		}
		if (targetConnection != null) {
			LockManager.unlock(targetConnection);
		}
	}

	private void initializeConnection(Connection conn) throws SQLException {
		// a table will most likely be created so commit is expected and gives somewhere to rollback to.
		if (!conn.getAutoCommit()) { // this commit will also clear temp tables
			conn.commit();
		}
	}

	private void initializeConnections() {
		if (_targetConn == null) {
			_targetConn = getDefaultConn();
		}
		if (_sourceConn == null) {
			_sourceConn = getDefaultConn();
		}
	}

	private void commitData(Connection targetConn) throws SQLException {
		// dont commit temp tables yet
		if (!isTempTable()) {
			targetConn.commit();
		}
	}

	private String createValidIdentifier(String columnName, String string) {
		return BridgeUtil.createValidIdentifier(columnName, string);
	}

	private boolean isOracle(Connection conn) {
		try {
			if (conn != null && conn.getMetaData().getDatabaseProductName().toLowerCase().indexOf("oracle") != -1) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			return true;//go with it as Oracle
		}
	}

	/**
	 * Process loop sql.
	 * Used in Data Quality Report. Run a query multiple times  (select count(*) from  table). 
	 * The list of table names is defined in select count(*) from {select * from all_tables}
	 * 
	 * @return true, if successful
	 * @throws SQLException the SQL exception
	 */
	private boolean processLoopSQLToMultipleQueries() throws SQLException {
		boolean isLoop = false;
		String query = getQuerySQL();
		int start = getQuerySQL().toLowerCase().indexOf("{"); //$NON-NLS-1$
		int end = getQuerySQL().indexOf("}"); //$NON-NLS-1$
		if (start != -1) {
			isLoop = true;
			String dynamicSQL = getQuerySQL().substring(start + 1, end);
			ResultSet rs = null;
			DBUtil dbutil = null;
			try {
				dbutil = DBUtil.getInstance(getTargetConnection());
				rs = dbutil.executeOracleQuery(dynamicSQL, null);
				int type = rs.getMetaData().getColumnType(1);
				StringBuffer sb = new StringBuffer();
				while (rs.next()) {
					if (type == java.sql.Types.VARCHAR) {
						String find = "{" + dynamicSQL + "}"; //$NON-NLS-1$  //$NON-NLS-2$
						int replaceIndexStart = getQuerySQL().indexOf(find);
						int replaceIndexEnd = replaceIndexStart + find.length();
						StringBuffer replacesb = new StringBuffer(getQuerySQL());
						replacesb.delete(replaceIndexStart, replaceIndexEnd);
						replacesb.insert(replaceIndexStart, rs.getString(1));
						String newQuery = replacesb.toString();
						newQuery = processBinds(rs, newQuery);
						_queries.add(newQuery);
					}
				}
			} finally {
				DBUtil.closeResultSet(rs);
			}
		} else {
			_queries.add(query);
		}
		return isLoop;
	}

	/**
	 * Process binds.
	 * 
	 * @param rs
	 *            the rs
	 * @param newQuery
	 *            the new query
	 * @return the string
	 * @throws SQLException 
	 */
	private String processBinds(ResultSet rs, String newQuery) throws SQLException {
		int columNum = rs.getMetaData().getColumnCount();
		for (int i = 1; i <= columNum; i++) {
			String aliasName = rs.getMetaData().getColumnLabel(i);
			if (aliasName.toLowerCase().startsWith("sqldev")) {
				newQuery = replace(newQuery, ":" + aliasName, rs.getString(i));
			}
		}
		return newQuery;
	}

	/**
	 * Replace.
	 * 
	 * @param aInput
	 *            the a input
	 * @param aOldPattern
	 *            the a old pattern
	 * @param aNewPattern
	 *            the a new pattern
	 * @return the string
	 */
	public static String replace(final String aInput, final String aOldPattern, final String aNewPattern) {
		if (aOldPattern.equals("")) {
			throw new IllegalArgumentException(Messages.getString("BRIDGE_TABLE_ILLEGAL_ARG"));
		}
		final StringBuffer result = new StringBuffer();
		// startIdx and idxOld delimit various chunks of aInput; these
		// chunks always end where aOldPattern begins
		int startIdx = 0;
		int idxOld = 0;
		while ((idxOld = aInput.indexOf(aOldPattern, startIdx)) >= 0) {
			// grab a part of aInput which does not include aOldPattern
			result.append(aInput.substring(startIdx, idxOld));
			// add aNewPattern to take place of aOldPattern
			result.append(aNewPattern);
			// reset the startIdx to just after the current match, to see
			// if there are any further matches
			startIdx = idxOld + aOldPattern.length();
		}
		// the final chunk will go to the end of aInput
		result.append(aInput.substring(startIdx));
		return result.toString();
	}

	/**
	 * Insert data.
	 * 
	 * @param tableDetails
	 *            the oracle columns
	 * @param rs
	 *            the rs
	 * @throws SQLException
	 *             the sQL exception
	 * @throws IOException 
	 * @throws BridgeException 
	 * @throws InterruptedException 
	 */
	private int insertData(BridgeTableDetails tableDetails, ResultSet rs) throws SQLException, IOException, BridgeException, InterruptedException {
		int count = 0;
		boolean targetConnAutoCommit = false;
		ResultSetMetaData md = rs.getMetaData();
		String insertSQL = getInsertSQL(md);
		PreparedStatement ps = null;
		try {
			ps = _targetConn.prepareCall(insertSQL);
			targetConnAutoCommit = turnOffAutoCommit(ps);//turn off autocommit so batch insert will work
			while (rs.next()) {
				if (isInterupted()) {
					throw new InterruptedException();
				}
				ArrayList<Blob> blobs = new ArrayList<Blob>();
				ArrayList<Clob> clobs = new ArrayList<Clob>();
				ArrayList<Object> binds = getNextBinds(rs, tableDetails, blobs, clobs);
				DBUtil.bind(ps, binds);
				ps.addBatch();
				//TODO: figure out how to batch insert BLOBS and CLOBS. At the moment. If they exist. Execute Batch for every row
				if ((++count % batchSize == 0) || blobs.size() > 0 || clobs.size() > 0) {
					ps.executeBatch();
				}
				// have to handle blobs/clobs seperately. May have to do something special for bulk insert
				for (Blob b : blobs) {
					try {
						((BLOB) b).freeTemporary();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				for (Clob c : clobs) {
					try {
						((CLOB) c).freeTemporary();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} finally {
			ps.executeBatch(); // insert remaining records
			resetAutoCommit(ps, targetConnAutoCommit);
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					saveError(Level.WARNING,"Failed to close PreparedStatement after inserting data", e);
				}
			}
		}
		return count;
	}

	private void resetAutoCommit(PreparedStatement ps, boolean targetConnAutoCommit) {
		if (ps instanceof OraclePreparedStatement) {
			try {
				if (!isTempTable()) {
					_targetConn.commit();// requires an explicit commit
				}
			} catch (SQLException e) {
				saveError(Level.SEVERE,"failed to reset the commit preference. could not commit", e);
			}
			try {
				_targetConn.setAutoCommit(targetConnAutoCommit);// set back to the way it was before the BRIDGE command
			} catch (SQLException e) {
				saveError(Level.SEVERE,"failed to reset the commit preference", e);
			}
		}
	}

	private boolean turnOffAutoCommit(PreparedStatement ps) throws SQLException {
		boolean currentAutoCommit = false;
		if (ps instanceof OraclePreparedStatement) {
			currentAutoCommit = _targetConn.getAutoCommit();
			_targetConn.setAutoCommit(false);
		}
		return currentAutoCommit;
	}

	private String getInsertSQL(ResultSetMetaData md) throws SQLException {
		int count = md.getColumnCount();
		int[] columnType = new int[count];
		String[] columnTypeName = new String[count + 1];
		StringBuffer sbInsert = new StringBuffer("INSERT INTO " + getTargetTableName() + " VALUES ("); //$NON-NLS-1$  //$NON-NLS-2$
		for (int i = 1; i <= count; i++) {
			sbInsert.append(":bindname" + i + ","); //$NON-NLS-1$  //$NON-NLS-2$
			columnType[i - 1] = md.getColumnType(i);
			columnTypeName[i - 1] = md.getColumnTypeName(i);
		}
		sbInsert.deleteCharAt(sbInsert.length() - 1); // remove the last comma
		sbInsert.append(")");
		return sbInsert.toString();
	}

	private ArrayList<Object> getNextBinds(ResultSet rs, BridgeTableDetails tableDetails, ArrayList<Blob> blobs, ArrayList<Clob> clobs) throws SQLException, IOException {
		ResultSetMetaData md = rs.getMetaData();
		int count = md.getColumnCount();
		ArrayList<Object> binds = new ArrayList<Object>();
		for (int i = 1; i <= md.getColumnCount(); i++) {
			Object o = rs.getObject(i);
			if (o == null) {
				binds.add(o);
				continue;
			}
			if (isOracle(_targetConn)) {
				if (o instanceof BigInteger) {
					oracle.sql.NUMBER oNUM = new oracle.sql.NUMBER((BigInteger) o);
					binds.add(oNUM);
				} else if (((tableDetails.getColumnDetails().get(i - 1).getDataTypeJDBC() == Types.LONGVARBINARY) || (tableDetails.getColumnDetails().get(i - 1).getTargetDataTypeStr().startsWith("BLOB"))) && (o instanceof byte[])) {
					binds.add(bindByte(o, blobs, clobs));
				} else if (o instanceof Blob) {
					binds.add(binbBlob(o, blobs, clobs));
				} else if (((tableDetails.getColumnDetails().get(i - 1).getDataTypeJDBC() == Types.LONGVARCHAR) || (tableDetails.getColumnDetails().get(i - 1).getTargetDataTypeStr().startsWith("CLOB"))) && (o instanceof String)) {
					binds.add(bindStringToClob(o, blobs, clobs));
				} else if (o instanceof Clob) {
					binds.add(bindClobToClob(o, blobs, clobs));
				} else if (tableDetails.getColumnDetails().get(i - 1).getDataTypeJDBC() == java.sql.Types.NVARCHAR) {
					binds.add(maybeSpace(o.toString()));// totierne added toString to flatten nxx to xx
				} else if (tableDetails.getColumnDetails().get(i - 1).getDataTypeJDBC() == java.sql.Types.NCHAR) {
					binds.add(maybeSpace(rtrim(o.toString())));// totierne added toString to flatten nxx to xx trim as our char defaults to 200
				} else if (tableDetails.getColumnDetails().get(i - 1).getDataTypeJDBC() == java.sql.Types.CHAR) {
					binds.add(maybeSpace(rtrim(o.toString())));// totierne added toString to flatten nxx to xx trim as our char defaults to 200
				} else if (tableDetails.getColumnDetails().get(i - 1).getDataTypeJDBC() == java.sql.Types.VARCHAR) {
					binds.add(maybeSpace(o.toString()));
				} else {
					binds.add(o);// totierne removed toString
				}
			} else {// try blob clobs 'naked' etc if not handled by Oracle specific code above - will probably fail
				binds.add(o);
			}
		}
		return binds;
	}

	private Clob bindClobToClob(Object o, ArrayList<Blob> blobs, ArrayList<Clob> clobs) throws SQLException, IOException {
		Clob c = null;
		BufferedReader binp = null;
		BufferedWriter bwr = null;
		Reader inp = null;
		Writer wr = null;
		int BUFFERSIZE = 2048;
		try {
			c = CLOB.createTemporary(_targetConn, false, CLOB.DURATION_SESSION);
			clobs.add(c);
			((CLOB) c).open(CLOB.MODE_READWRITE);
			inp = ((Clob) o).getCharacterStream();
			binp = new BufferedReader(inp);
			char[] myc = new char[BUFFERSIZE];
			wr = c.setCharacterStream(1L);
			int lastRead = 0;
			bwr = new BufferedWriter(wr);
			while ((lastRead = binp.read(myc, 0, myc.length)) != -1) {
				bwr.write(myc, 0, lastRead);
			}
			bwr.flush();
		} finally {
			close(inp);
			close(wr);
			close(binp);
			close(bwr);
		}
		return c;
	}

	private Clob bindStringToClob(Object o, ArrayList<Blob> blobs, ArrayList<Clob> clobs) throws SQLException, IOException {
		Clob c = null;
		BufferedWriter bwr = null;
		Writer wr = null;
		try {
			c = CLOB.createTemporary(_targetConn, false, CLOB.DURATION_SESSION);
			clobs.add(c);
			((CLOB) c).open(CLOB.MODE_READWRITE);
			wr = c.setCharacterStream(1L);
			int lastRead = 0;
			bwr = new BufferedWriter(wr);
			bwr.write((String) o);
			bwr.flush();
		} finally {
			close(wr);
			close(bwr);
		}
		return c;
	}

	private Blob binbBlob(Object o, ArrayList<Blob> blobs, ArrayList<Clob> clobs) throws SQLException, IOException {
		Blob b = null;
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		InputStream inp = null;
		OutputStream os = null;
		int BUFFERSIZE = 2048;
		try {
			b = BLOB.createTemporary(_targetConn, false, BLOB.DURATION_SESSION);
			blobs.add(b);
			((BLOB) b).open(BLOB.MODE_READWRITE);
			inp = ((Blob) o).getBinaryStream();
			bis = new BufferedInputStream(inp);
			byte[] buffer = new byte[BUFFERSIZE];
			os = b.setBinaryStream(1L);
			int lastRead = 0;
			bos = new BufferedOutputStream(os);
			while ((lastRead = bis.read(buffer, 0, buffer.length)) != -1) {
				bos.write(buffer, 0, lastRead);
			}
			bos.flush();
		} finally {
			close(inp);
			close(os);
			close(bis);
			close(bos);
		}
		return b;
	}

	public void close(Closeable c) {
		if (c == null)
			return;
		try {
			c.close();
		} catch (IOException e) {
			saveError(Level.WARNING,"PROBLEM CLOSING", e);
		}
	}

	private Blob bindByte(Object o, ArrayList<Blob> blobs, ArrayList<Clob> clobs) throws SQLException, IOException {
		Blob b = null;
		BufferedOutputStream bos = null;
		OutputStream os = null;
		try {
			b = BLOB.createTemporary(_targetConn, false, BLOB.DURATION_SESSION);
			blobs.add(b);
			((BLOB) b).open(BLOB.MODE_READWRITE);
			byte[] buffer = (byte[]) o;
			os = b.setBinaryStream(1L);
			bos = new BufferedOutputStream(os);
			bos.write(buffer, 0, buffer.length);
			bos.flush();
		} finally {
			close(os);
			close(bos);
		}
		return b;
	}

	// if space is "" then it should be " "
	/**
	 * Maybe space.
	 * 
	 * @param in
	 *            the in
	 * @return the string
	 */
	private String maybeSpace(String in) {
		if (_ZeroLengthStringToSpace == false) {
			return in;
		}
		if (in == null) {// just in case should be sorted higher up.
			return in;
		}
		if (in.equals("")) { //$NON-NLS-1$
			return " "; //$NON-NLS-1$
		}
		return in;
	}

	// do not cut back to zero length string but otherwise remove trailing spaces
	// an rtrim of sorts
	/**
	 * Rtrim.
	 * 
	 * @param myline
	 *            the myline
	 * @return the string
	 */
	private String rtrim(String myline) {
		String retVal = null;
		if ((myline == null) || (myline.equals(""))) { //$NON-NLS-1$
			retVal = myline;
		} else {
			boolean truncate = false;
			int i = 0;
			for (i = myline.length() - 1; i > 0; i--) {
				if (!(myline.charAt(i) == (' '))) {
					break;
				}
			}
			retVal = myline.substring(0, i + 1);
		}
		return retVal;
	}

	/*
	 * return create tables datatypes with a full create tables string at the end create table brroken up to allow create table data types to be returned even on exception from create table sql
	 */
	/**
	 * Creates the table string.
	 * 
	 * @param md
	 *            the md
	 * @param databaseMetaData 
	 * @param isOracle
	 *            the is oracle
	 * @return the array list
	 * @throws SQLException
	 *             the sQL exception
	 */
	private BridgeTableDetails createTableDetails(ResultSetMetaData md, DatabaseMetaData databaseMetaData, boolean isOracle, CopyToOracleHint hint) throws SQLException {
		BridgeTableDetails tableDetails = new BridgeTableDetails();
		tableDetails.isOracle(isOracle);
		String tableHeader, tableType;
		if (isTempTable()) {
			tableHeader = "CREATE GLOBAL TEMPORARY TABLE ";
			tableType = " on commit delete rows ";
		} else {
			tableHeader = "CREATE TABLE ";
			tableType = " ";
		}
		StringBuffer sb = new StringBuffer(tableHeader + getTargetTableName() + "("); //$NON-NLS-1$  
		for (int i = 1; i <= md.getColumnCount(); i++) {
			String colName = md.getColumnName(i);
			colName = createValidIdentifier(colName, "part"); //$NON-NLS-1$
			int dataTypeJDBC = md.getColumnType(i);
			int precision = md.getPrecision(i);
			int scale = md.getScale(i);
			if (!isOracle) {
				int displaysize = md.getColumnDisplaySize(i);
				if (displaysize > precision) {
					precision = displaysize;
				}
			}
			String dataType = BridgeJDBCToOracle.map(isOracle, precision, scale, dataTypeJDBC, getDb12cFeatures());
			tableDetails.addColumn(new BridgeColumnDetails(dataType.toUpperCase(), precision, scale, dataTypeJDBC));
			String amINull = " NULL "; //unknown == null == "" sqlserver assumes opposite so be explicit  //$NON-NLS-1$
			if (md.isNullable(i) == ResultSetMetaData.columnNoNulls) {
				amINull = " NOT NULL "; //$NON-NLS-1$
			}
			sb.append(colName + " " + dataType + amINull + ","); //$NON-NLS-1$  //$NON-NLS-2$
		}
		sb.deleteCharAt(sb.length() - 1); // remove the last comma
		sb.append(")"); //$NON-NLS-1$
		sb.append(tableType);
		String createTableSQL = sb.toString();
		if (hint.isCopyToOracle()) {
			String primaryKeyDDL = createPrimaryKeyDDL(_sourceConn.getMetaData(), hint.getCatalog(), hint.getSchema(), hint.getTable());
			if (primaryKeyDDL != null) {
				tableDetails.setPKeyDDL(primaryKeyDDL);
			}
		}
		tableDetails.setDDL(createTableSQL);
		return tableDetails;
	}

	private String createPrimaryKeyDDL(DatabaseMetaData dbMetadata, String catalog, String schema, String tableName) {
		try {
			ResultSet rsKey = dbMetadata.getPrimaryKeys(catalog, schema, tableName);
			StringBuffer sb = new StringBuffer();
			boolean first = true;
			sb.append("(");
			while (rsKey.next()) {
				String primaryKey = rsKey.getString("COLUMN_NAME");
				sb.append(primaryKey);
				if (first) {
					first = false;
				} else {
					sb.append(",");
				}
			}
			sb.append(")");
			if (first) {//no primary key
				return null;
			} else {
				return "ALTER TABLE " + tableName + " ADD PRIMARY KEY" + sb.toString();
			}
		} catch (SQLException e) {
			saveError(Level.WARNING, "Failed to generate Primary Key DDL", e);
		}
		return null;
	}

	/**
	 * Creates the table action.
	 * 
	 * @param tableDetails
	 *            the oracle columns
	 * @throws SQLException
	 *             the sQL exception
	 */
	private void createTableAction(BridgeTableDetails tableDetails) throws SQLException {
		DBUtil dbUtil = DBUtil.getInstance(_targetConn);
		String createTableSQL = tableDetails.getDDL();
		if (_isReplace) {
			String dropTableSQL = "DROP TABLE " + getTargetTableName();
			try {
				dbUtil.execute(dropTableSQL);
			} catch (Exception e) {
				// ignore all errors here
			}
		}
		if(_isTruncate){
			String truncateTableSQL = "TRUNCATE TABLE " + getTargetTableName();
			try {
				dbUtil.execute(truncateTableSQL);
			} catch (Exception e) {
				// ignore all errors here
			}
		}
		// if append, should we create the table if it does not exist ?, I think so.
		SQLException sqlex = dbUtil.getLastException();
		dbUtil.execute(createTableSQL);
		sqlex = dbUtil.getLastException();
		if (sqlex != null) {
			throw sqlex;
		}
	}

	/**
	 * Gets the target table name.
	 * 
	 * @return the target table name
	 */
	private String getTargetTableName() {
		if (_targetTableName == null) {
			_targetTableName = createValidIdentifier(getTableName(), "object"); //$NON-NLS-1$
		}
		if (_targetTableName != null && _targetTableName.startsWith("#")) {
			return _targetTableName.substring(1);
		} else {
			return _targetTableName;
		}
	}

	/**
	 * Checks if is temp table.
	 * 
	 * @return true, if is temp table
	 */
	private boolean isTempTable() {
		getTargetTableName();
		if (_targetTableName != null && _targetTableName.startsWith("#")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Query source.
	 * 
	 * @param sourceDBUtil
	 *            the source db util
	 * @param query
	 *            the query
	 * @return the result set wrapper
	 * @throws SQLException 
	 */
	private ResultSet querySource(DBUtil sourceDBUtil, String query) throws SQLException {
		ResultSet rs = null;
		rs = sourceDBUtil.executeQuery(query, new HashMap());
		if (rs == null) {
			SQLException e = sourceDBUtil.getLastException();
			if (e != null) {
				throw e;
			}
		}
		return rs;
	}

	/**
	 * Gets the query sql.
	 * 
	 * @return the query sql
	 */
	private String getQuerySQL() {
		return _query;
	}

	/**
	 * Sets the query sql.
	 * 
	 * @param query
	 *            the new query sql
	 */
	private void setQuerySQL(String query) {
		_query = query;
	}

	/**
	 * Gets the default conn.
	 * 
	 * @return the default conn
	 */
	private Connection getDefaultConn() {
		return _defaultConn;
	}

	/**
	 * Sets the default conn.
	 * 
	 * @param conn
	 *            the new default conn
	 */
	public void setDefaultConn(Connection conn) {
		_defaultConn = conn;
	}

	/**
	 * Drop.
	 */
	public void drop() {
		DBUtil.getInstance(_targetConn).execute("DROP TABLE " + getTargetTableName()); //$NON-NLS-1$
	}

	/**
	 * Sets the db12c features.
	 * 
	 * @param db12cFeaturesIn
	 *            the new db12c features
	 */
	public void setDb12cFeatures(boolean db12cFeaturesIn) {
		_db12cFeatures = db12cFeaturesIn;
	}

	/**
	 * Gets the db12c features.
	 * 
	 * @return the db12c features
	 */
	public boolean getDb12cFeatures() {
		return _db12cFeatures;
	}

	/**
	 * Sets the script runner context.
	 * 
	 * @param ctx
	 *            the new script runner context
	 */
	public void setScriptRunnerContext(ScriptRunnerContext ctx) {
		m_ctx = ctx;
	}

	/**
	 * Checks if is interupted.
	 * 
	 * @return true, if is interupted
	 */
	private boolean isInterupted() {
		if (m_ctx != null) {
			return m_ctx.getExited();
		}
		return false;
	}

	/**
	 * Gets the target connection.
	 * 
	 * @return the target connection
	 */
	public Connection getTargetConnection() {
		return _targetConn;
	}

	/**
	 * Gets the source connection.
	 * 
	 * @return the source connection
	 */
	public Connection getSourceConnection() {
		return _sourceConn;
	}

	/**
	 * Gets the queries.
	 * 
	 * @return the queries
	 */
	private ArrayList<String> getQueries() {
		return _queries;
	}

	/**
	 * Gets the table name.
	 * 
	 * @return the table name
	 */
	private String getTableName() {
		return _tableName;
	}

	/**
	 * Process dynamic sql.
	 * 
	 * @throws SQLException
	 *             the sQL exception+
	 */
	private void processDynamicSQLToInlineList() throws SQLException {
		int start = getQuerySQL().indexOf("({"); //$NON-NLS-1$
		int end = getQuerySQL().indexOf("}"); //$NON-NLS-1$
		if (start != -1) {
			String dynamicSQL = getQuerySQL().substring(start + 2, end);
			ResultSet rs = null;
			DBUtil dbutil = null;
			try {
				dbutil = DBUtil.getInstance(getTargetConnection());
				rs = dbutil.executeOracleQuery(dynamicSQL, null);
				int type = rs.getMetaData().getColumnType(1);
				StringBuffer sb = new StringBuffer();
				while (rs.next()) {
					if (type == java.sql.Types.VARCHAR || type == java.sql.Types.CHAR) {
						sb.append("'" + rs.getString(1) + "',"); //$NON-NLS-1$  //$NON-NLS-2$
					} else {
						sb.append(rs.getString(1) + ","); //$NON-NLS-1$  
					}
				}
				if (sb.length() > 0) {
					sb.deleteCharAt(sb.length() - 1);// get rid of last ","
					String find = "{" + dynamicSQL + "}"; //$NON-NLS-1$  //$NON-NLS-2$
					int replaceIndexStart = getQuerySQL().indexOf(find);
					int replaceIndexEnd = replaceIndexStart + find.length();
					StringBuffer replacesb = new StringBuffer(getQuerySQL());
					replacesb.delete(replaceIndexStart, replaceIndexEnd);
					replacesb.insert(replaceIndexStart, sb.toString());
					setQuerySQL(replacesb.toString());
				} else {
					saveError(Level.SEVERE, "Dynamic SQL returned no rows", null);
				}
			} finally {
				DBUtil.closeResultSet(rs);
			}
		}
	}

	private void saveError(Level level, String customMsg, Throwable t) {
		Logger.getLogger(getClass().getName()).log(level, customMsg, t.getLocalizedMessage());
	}
}
