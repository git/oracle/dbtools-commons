/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.bridge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import oracle.dbtools.common.utils.OracleSQLConstants;
/**
 * 
 * 
 * @author dbtools_ie team
 */
public class BridgeUtil {
  private final static String NOTALLOWEDCHARS = " ' .@`!\"%^&*()-+=[]{};:,.<>?/~''\u00A3"; //$NON-NLS-1$
  private final static String DIGITS = "0123456789"; //$NON-NLS-1$
  private final static String EXTRAALLOWED = "_$"; //$NON-NLS-1$
	public static String createValidIdentifier(String sourceName, String type) {
		try {
			if (sourceName == null || sourceName.trim().equals("")) { //$NON-NLS-1$
				return sourceName; // dont bother processing
			}
			String targetName = sourceName;
			//looks like literals are being mistaken for identifiers if they are in double quotes.
			//how to know if a double quoted string is an identifier or literal?
			//the tree walker should be recognizing this distinction, but sometimes it does the wrong thing
			//anyway one edge case is the empty string represented by double quotes
			if (targetName.trim().equals("\"\"")) { //$NON-NLS-1$
				//empty strings are always literals, obviously not identifiers
				return targetName;
			}

			//single quoted strings are always literals arent they?
			//there should be no confusion return them immediately
			if (targetName.trim().startsWith("'") && targetName.trim().endsWith("'")) { //$NON-NLS-1$ //$NON-NLS-2$
				return targetName;
			}
			// return the best guess for a target name
			// note pivot columns (type.equals("part")) can have a numeric starting character.

			// There are 10 rules defined for identifier naming:
			// See http://yourmachine.yourdomain/10/102/server.102/b14200/sql_elements008.htm#i27570

			// remove quotes
			targetName = removeQuoteCharacters(targetName);

			//this is the pivot issue with numbers in square brackets (see test qa_sqlsc_pivot)
			//identifiers are never just numbers, so if an "identifier" is just a number Ex: [7] 
			//it must really be a literal and the grammar got it wrong again. return the literal value
			if (type.equals("object")) {//only unknown identifiers can fall into this trap, all other identifiers (ex: type = part) should be managed
				int intValue = -999999996;//magic random number to check against
				try {
					intValue = Integer.parseInt(targetName);
				} catch (Exception e) {/*do nothing its an identifier*/
				}
				if (intValue != -999999996) {
					return "A_" + targetName; //its a real number, return it staight away as it is not an identifier
				}
			}
			String backupTargetName = targetName;
			// left trim non alphanumeric characters like '\u00A3' '/' ... ,  \u00A3 = sterling pound char
			// dont want to replace these with underscores as they are at the start.
			targetName = leftTrimNonAlphaNumeric(targetName);

			if (targetName.length() == 0) { //so the targetName was just a NonAlphaNumeric like '%' . Ex: SELECT col1 AS '%' FROM table1 
				//In this edge case (It has happened with a customer...) I think just use double qouted identifier as there is nothing neat to change the alias to. SELECT col1 AS "%" FROM table1. is better than SELECT col1 AS A FROM table1
				return "\"" + backupTargetName + "\"";
			}
			// first char check (must be an alpha character) if not add 'A' to the front
			targetName = firstCharCheck(targetName, type);

			//temporary object check
			targetName = temporaryObjectCheck(targetName);
			// Alphanumeric characters from your database charset plus '_', '$', '#' only, replace with underscore
			// only 1 underscore in a row, mulitple underscores in a row are redundant
			//we leave  # or ## at the start of a name as temporary objects are handled later
			targetName = removeInvalidCharacters(targetName);

			//Do not trim targetTable name. This is left to client
			//targetName = trimToSize(targetName, 30);

			// Reserve Words get appended with _
			targetName = modifyReservedNames(targetName);

			return targetName;
		} catch (Exception e) {
			return sourceName;
		}
	}
  
  public static String createValidObjectNameForScratchEditor(String sourceName)
  {
      String targetName = sourceName;
      targetName = temporaryObjectCheck(targetName);
      targetName = removeInvalidCharacters(targetName);
      targetName = trimToSize(targetName, 30);
      return targetName;
  }

  private static String temporaryObjectCheck(String targetName) {
    if(targetName.trim().startsWith("#")){ //$NON-NLS-1$
      //Possible temporary table, this is handled in the PLSQLFromTSQLGenerationAction, altough I think it should be refactored to here.
    }
    return targetName;
  }

  private static String removeInvalidCharacters(String targetName) {
    char[] targetNameCharArray = targetName.toCharArray();
    int i = 0;
    for (char c : targetNameCharArray) {
      if (NOTALLOWEDCHARS.indexOf(c) != -1) { // its not a valid character, replace with underscore
        targetNameCharArray[i] = '_';
      }
      i++;
    }
    // remove duplicate underscores for neatness
    i = 0; // reset index
    StringBuffer newTargetName = new StringBuffer();
    for (char c : targetNameCharArray) {
      if (c == '_') { // add only if the next character is not an underscore
        if(targetNameCharArray.length == i +1){//if this is the last character leave it
          newTargetName.append(c);
        } else if (targetNameCharArray.length >i+1 && targetNameCharArray[i + 1] != '_') {
          newTargetName.append(c);
        }
      } else {
        newTargetName.append(c);
      }
      i++;
    }
    return newTargetName.toString();
  }

  private static String modifyReservedNames(String targetName) {
    // append with underscore if a keyword
    if (OracleSQLConstants.isSignificantWord(targetName) || OracleSQLConstants.isRservedWord(targetName)) {
      targetName = targetName + "_"; //$NON-NLS-1$
    }
    return targetName;
  }

  private static String trimToSize(String targetName, int size) {
    if (targetName != null && targetName.trim().length() > size) {
      targetName = targetName.trim().substring(0, size);
    }
    return targetName;
  }

  private static String firstCharCheck(String targetName, String type) {
    // first char can only be a alpha character, if not prepend A
    char firstChar = targetName.charAt(0);
    if ((NOTALLOWEDCHARS + EXTRAALLOWED + DIGITS).indexOf(firstChar) != -1) {
      targetName = "A" + targetName; //$NON-NLS-1$
    }
    return targetName;
  }

  private static String leftTrimNonAlphaNumeric(String targetName) {
    int startIndex = 0;
    for (char c : targetName.toCharArray()) {
      if ((NOTALLOWEDCHARS + EXTRAALLOWED).indexOf(c) == -1) { // it is a valid char
        break; // leave the for loop, this char is fine
      }
      startIndex = startIndex + 1;
    }
    return targetName.substring(startIndex);
  }

  protected static String removeQuoteCharacters(String name) {
    //looks like literals are being mistaken for identifiers if they are in double quotes.
    //how to know if a double quoted string is an identifier or literal?
    //the tree walker should be recognizing whats a literal and whats an identifier
    //treating all as identifiers here
    if ((name != null) && (name.trim().startsWith("[") && name.trim().endsWith("]") || name.trim().startsWith("\"") && name.trim().endsWith("\""))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
      name = name.trim().substring(1, name.length() - 1);
    }
    return name;
  }

  public ArrayList<String> getEmulationFuncsForSchema(Object schemaId) {
    return null;
  }

  public ArrayList<String> getEmulationFuncsForConnection(Object connectionId) {
    return null;
  }

  public void addInfoKeyValue(String key, String value) {

  }

  public String getInfoKeyValue(String key) {

    return null;
  }

  public void clearInfoKey() {

  }








public LinkedList<Long> getViewGenOrderList() {
	return null;
}

public void setViewGenOrderList(final LinkedList<Long> viewList) {
}


public boolean getUseViewList() {
	return false;
}

public void setUseViewList(boolean flag) {
}

}

