/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.bridge;

public class CopyToOracleHint {

	private String _query = null;
	private String _queryWithoutHint = null;
	private boolean _hasCopyToOracleHint = false;
	private String _catalog = null;
	private String _schema = null;
	private String _table = null;
	public static final String COPYTOORACLEHINT = "/*CTO:";

	public CopyToOracleHint(String query) {
		_query = query;
		if (query != null) {
			if (_query.indexOf(COPYTOORACLEHINT) != -1) {
				_hasCopyToOracleHint = true;
			}
			if (_hasCopyToOracleHint) {
				_queryWithoutHint = removeCopyToOracleHint(_query);
				processHint(_query);
			} else {
				_queryWithoutHint = _query;
			}
		}
	}

	private void processHint(String query) {
		if (query != null) {
			int startHint = query.indexOf(COPYTOORACLEHINT);
			int endHint = query.indexOf("*/", startHint);
			if (startHint != -1 && endHint != -1) {
				String hint = query.substring(startHint, endHint);
				String[] hints = hint.split(":");
				_catalog = hints[1];
				_schema = hints[2];
				_table = hints[3];
			}
		}
	}

	public boolean isCopyToOracle() {
		return _hasCopyToOracleHint;
	}

	public String getCatalog() {
		return _catalog;
	}

	public String getSchema() {
		return _schema;
	}

	public String getTable() {
		return _table;
	}

	private String removeCopyToOracleHint(String query) {
		if (query != null) {
			int startHint = query.indexOf(COPYTOORACLEHINT);
			int endHint = query.indexOf("*/", startHint);
			if (startHint != -1 && endHint != -1) {
				query = query.substring(endHint+2);
			}
		}
		return query;
	}

	public static String createCopyToOracleHint(String dbName, String schema, String name) {
		return COPYTOORACLEHINT + dbName + ":" + schema + ":" + name + "*/";
	}

	public String queryWithoutHint() {
		return _queryWithoutHint;
	}

	public String getFullName() {
	  if(getCatalog()!=null && getSchema()!=null){
			return getCatalog()+"."+getSchema()+"."+getTable();
		} else if (getCatalog()!=null && getSchema()==null){
			return getCatalog()+".."+getTable();
		} else if(getCatalog()==null && getSchema()!=null) {
			return getSchema()+"."+getTable();
		} else {
			return getTable();
		}
	}
}
