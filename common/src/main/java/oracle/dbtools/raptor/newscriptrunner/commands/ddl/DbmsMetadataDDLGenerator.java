/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.ddl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import oracle.dbtools.common.utils.MetaResource;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.commands.ddl.DbmsMetadataDDLGenerator.DDLObjective;
import oracle.dbtools.raptor.newscriptrunner.commands.ddl.DbmsMetadataDDLGenerator.ObjectNamespace;
import oracle.dbtools.raptor.newscriptrunner.commands.ddl.DbmsMetadataDDLGenerator.ResolvedDBName;
import oracle.dbtools.raptor.newscriptrunner.commands.ddl.DbmsMetadataDDLGenerator.TokenizedDBName;
import oracle.dbtools.raptor.newscriptrunner.commands.ddl.DbmsMetadataDDLGenerator.TopLevelType;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryXMLSupport;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=DbmsMetadataDDLGenerator.java">Barry McGillin</a>
 *
 */
public class DbmsMetadataDDLGenerator {
	
	
	private Connection _conn;

	private ScriptRunnerContext _ctx;

	public DbmsMetadataDDLGenerator(Connection conn, ScriptRunnerContext ctx) {
		_conn = conn;
		_ctx=ctx;
	}

	// Components
	public static final String BODY = "BODY"; //$NON-NLS-1$

	public static final String CONSUMER = "CONSUMER"; //$NON-NLS-1$

	public static final String GROUP = "GROUP"; //$NON-NLS-1$

	public static final String LOG = "LOG"; //$NON-NLS-1$

	public static final String MATERIALIZED = "MATERIALIZED"; //$NON-NLS-1$

	public static final String PARTITION = "PARTITION"; //$NON-NLS-1$

	public static final String REDO = "REDO"; //$NON-NLS-1$

	public static final String REF = "REF"; //$NON-NLS-1$

	public static final String PL_SQL = "PL/SQL"; //$NON-NLS-1$

	public static final String PLSQL = "PLSQL"; //$NON-NLS-1$

	// Relational
	public static final String TABLE = "TABLE"; //$NON-NLS-1$

	public static final String VIEW = "VIEW"; //$NON-NLS-1$

	// Types
	public static final String CONSTRAINT = "CONSTRAINT"; //$NON-NLS-1$

	public static final String CONSUMERGROUP = "CONSUMER GROUP"; //$NON-NLS-1$

	public static final String CONTAINERRMPLAN = "CONTAINERRMPLAN"; //$NON-NLS-1$

	public static final String DATAFILE = "DATAFILE"; //$NON-NLS-1$

	public static final String DATABASELINK = "DATABASE LINK"; //$NON-NLS-1$

	public static final String DB_LINK = "DB_LINK"; //$NON-NLS-1$

	public static final String DBLINK = "DBLINK"; //$NON-NLS-1$

	public static final String FUNCTION = "FUNCTION"; //$NON-NLS-1$

	public static final String INDEX = "INDEX"; //$NON-NLS-1$

	public static final String MATVIEW = "MATERIALIZED VIEW"; //$NON-NLS-1$

	public static final String MATVIEWLOG = "MATERIALIZED VIEW LOG"; //$NON-NLS-1$

	public static final String OBJECT = "OBJECT"; //$NON-NLS-1$

	public static final String PACKAGE = "PACKAGE"; //$NON-NLS-1$

	public static final String PACKAGEBODY = "PACKAGE BODY"; //$NON-NLS-1$

	public static final String PROCEDURE = "PROCEDURE"; //$NON-NLS-1$

	public static final String PROFILE = "PROFILE"; //$NON-NLS-1$

	public static final String QUEUE = "QUEUE"; //$NON-NLS-1$

	public static final String QUEUETABLE = "QUEUE TABLE"; //$NON-NLS-1$

	public static final String REDOLOGGROUP = "REDO LOG GROUP"; //$NON-NLS-1$

	public static final String REFCONSTRAINT = "REF CONSTRAINT"; //$NON-NLS-1$

	public static final String ROLE = "ROLE"; //$NON-NLS-1$

	public static final String SEQUENCE = "SEQUENCE"; //$NON-NLS-1$

	public static final String SYNONYM = "SYNONYM"; //$NON-NLS-1$

	public static final String TABLEPARTITION = "TABLE PARTITION"; //$NON-NLS-1$

	public static final String TABLESPACE = "TABLESPACE"; //$NON-NLS-1$

	public static final String TRIGGER = "TRIGGER"; //$NON-NLS-1$

	public static final String TYPE = "TYPE"; //$NON-NLS-1$

	public static final String TYPEBODY = "TYPE BODY"; //$NON-NLS-1$

	public static final String USER = "USER"; //$NON-NLS-1$

	public static String[][] types = { { CONSTRAINT }, { CONSUMER, GROUP }, { CONTAINERRMPLAN }, { DATAFILE }, { DATABASELINK }, { DB_LINK }, { DBLINK },
			{ FUNCTION }, { INDEX }, { MATERIALIZED, VIEW }, { MATERIALIZED, VIEW, LOG }, { OBJECT }, { PACKAGE }, { PACKAGE, BODY }, { PROCEDURE },
			{ PROFILE }, { QUEUE }, { QUEUE, TABLE }, { REDO, LOG, GROUP }, { REF, CONSTRAINT }, { ROLE }, { SEQUENCE }, { SYNONYM }, { TABLE },
			{ TABLE, PARTITION }, { TABLESPACE }, { TRIGGER }, { TYPE }, { TYPE, BODY }, { USER }, { VIEW } };

	public static final String COMMENT = "COMMENT"; //$NON-NLS-1$

	public static final String PACKAGEWITHBODY = "PACKAGE SPEC+BODY"; //$NON-NLS-1$

	// ids
	public static final String TABLE_DDL = "table_ddl"; //$NON-NLS-1$

	public static final String VIEW_DDL = "view_ddl"; //$NON-NLS-1$

	public static final String INDEX_DDL = "index_ddl"; //$NON-NLS-1$

	public static final String CONSTRAINT_DDL = "constraint_ddl"; //$NON-NLS-1$

	public static final String FUNCTION_DDL = "function_ddl"; //$NON-NLS-1$

	public static final String CONSUMERGROUP_DDL = "consumergroup_ddl"; //$NON-NLS-1$

	public static final String CONTAINERRMPLAN_DDL = "containerrmplan_ddl"; //$NON-NLS-1$

	public static final String DATAFILE_DDL = "datafile_ddl"; //$NON-NLS-1$

	public static final String DBLINK_DDL = "dblink_ddl"; //$NON-NLS-1$

	public static final String MATVIEW_DDL = "matview_ddl"; //$NON-NLS-1$

	public static final String MATVIEWLOG_DDL = "matviewlog_ddl"; //$NON-NLS-1$

	public static final String OBJECT_DDL = "script_ddl"; //$NON-NLS-1$

	public static final String PROFILE_DDL = "profile_ddl"; //$NON-NLS-1$

	public static final String QUEUE_DDL = "queue_ddl"; //$NON-NLS-1$

	public static final String QUEUETABLE_DDL = "queueTable_ddl"; //$NON-NLS-1$

	public static final String REDOLOGGROUP_DDL = "redologgroup_ddl"; //$NON-NLS-1$

	public static final String PROCEDURE_DDL = "procedure_ddl"; //$NON-NLS-1$

	public static final String PACKAGESPEC_DDL = "packagespec_ddl"; //$NON-NLS-1$

	public static final String PACKAGEBODY_DDL = "packagebody_ddl"; //$NON-NLS-1$

	public static final String PACKAGE_DDL = "package_ddl"; //$NON-NLS-1$

	public static final String SEQUENCE_DDL = "seq_ddl"; //$NON-NLS-1$

	public static final String SYNONYM_DDL = "syn_ddl"; //$NON-NLS-1$

	public static final String TABLESPACE_DDL = "tablespace_ddl"; //$NON-NLS-1$

	public static final String TYPE_DDL = "type_ddl"; //$NON-NLS-1$

	public static final String TYPE_BODY_DDL = "type_body_ddl"; //$NON-NLS-1$

	public static final String USER_DDL = "user_ddl"; //$NON-NLS-1$

	public static final String ROLE_DDL = "role_ddl"; //$NON-NLS-1$

	public static final String REFCONSTRAINT_DDL = "refconstraint_ddl"; //$NON-NLS-1$

	public static final String TRIGGER_DDL = "trigger_ddl"; //$NON-NLS-1$

	public static enum TopLevelType {
		TABLE(2), VIEW(4), SYNONYM(5), PROCEDURE(7), FUNCTION(8), PACKAGE(9);

		private int index;

		TopLevelType(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public static TopLevelType lookup(int index) {
			switch (index) {
			case 2:
				return TABLE;
			case 4:
				return VIEW;
			case 5:
				return SYNONYM;
			case 7:
				return PROCEDURE;
			case 8:
				return FUNCTION;
			case 9:
				return PACKAGE;
			default:
				return null;
			}
		}

		public String getObjectType() {
			return toString();
		}
	}

	  public static enum ObjectNamespace {
		    TABLE(0), PLSQL(1), SEQUENCE(2), TRIGGER(3),
		    // JAVE_SOURCE(4),
		    // JAVA_RESOURCE(5),
		    // JAVA_CLASS(6),
		    TYPE(7),
		    // JAVA_SHARED_DATA(8),
		    INDEX(9);

		    private int index;

		    ObjectNamespace(int index) {
		      this.index = index;
		    }

		    public int getIndex() {
		      return index;
		    }

		    public static ObjectNamespace lookup(int index) {
		      switch (index) {
		      case 0:
		        return TABLE;
		      case 1:
		        return PLSQL;
		      case 2:
		        return SEQUENCE;
		      case 3:
		        return TRIGGER;
		        /*
		         * case 4: return JAVE_SOURCE; case 5: return JAVA_RESOURCE; case 6:
		         * return JAVA_CLASS; case 8: return JAVA_SHARED_DATA;
		         */
		      case 7:
		        return TYPE;
		      case 9:
		        return INDEX;
		      default:
		        return null;
		      }
		    }

		    public static ObjectNamespace lookup(String type) {
		      if (type != null) {
		        switch (type) {
		        case DbmsMetadataDDLGenerator.TABLE:
		        case DbmsMetadataDDLGenerator.VIEW:
		          return TABLE;
		        case DbmsMetadataDDLGenerator.PL_SQL:
		        case DbmsMetadataDDLGenerator.PACKAGE:
		        case DbmsMetadataDDLGenerator.FUNCTION:
		        case DbmsMetadataDDLGenerator.PROCEDURE:
		          return PLSQL;
		        default:
		          try {
		            return ObjectNamespace.valueOf(type);
		          } catch (Exception e) {
		            return null;
		          }
		        }
		      } else {
		        return null;
		      }
		    }

		    public String getObjectType() {
		      switch (this) {
		      case PLSQL:
		      case TABLE:
		        return null; // ignore generic types
		      default:
		        return toString();
		      }
		    }
		  }
	  
	  public static class DDLObjective {
		    private LinkedList<ResolvedDBName> resolvedDBNames;

		    private final String origName;

		    private final String origType;

		    private final boolean outputComponentsOnly;

		    private final ArrayList<String> componentTypes;

		    public DDLObjective(String origName, String origType, List<String> componentTypes, boolean outputComponentsOnly) {
		      this.origName = origName;
		      this.origType = origType;
		      this.outputComponentsOnly = outputComponentsOnly;
		      this.componentTypes = (componentTypes != null) ? new ArrayList<String>(componentTypes) : new ArrayList<String>();

		      this.resolvedDBNames = new LinkedList<ResolvedDBName>();
		    }

		    public DDLObjective(String origName, String origType) {
		      this(origName, origType, null, false);
		    }

		    public String getOrigName() {
		      return this.origName;
		    }

		    public String getOrigType() {
		      return this.origType;
		    }

		    public boolean outputComponentsOnly() {
		      return this.outputComponentsOnly;
		    }

		    public List<String> getComponentTypes() {
		      return this.componentTypes;
		    }

		    public List<ResolvedDBName> getResolvedDBNames() {
		      return this.resolvedDBNames;
		    }

		    public void setResolvedDBName(List<ResolvedDBName> resolvedDBNames) {
		      this.resolvedDBNames = (resolvedDBNames != null) ? new LinkedList<ResolvedDBName>(resolvedDBNames) : new LinkedList<ResolvedDBName>();
		    }
		  }  
	  
	  

	  public static class TokenizedDBName {
	    private String partA;

	    private String partB;

	    private String partC;

	    private String dbLink;

	    public TokenizedDBName(String partA, String partB, String partC, String dbLink) {
	      this.partA = partA;
	      this.partB = partB;
	      this.partC = partC;
	      this.dbLink = dbLink;
	    }

	    public TokenizedDBName(String name) {
	      this(name, null, null, null);
	    }

	    public final String getPartA() {
	      return partA;
	    }

	    public final String getPartB() {
	      return partB;
	    }

	    public final String getPartC() {
	      return partC;
	    }

	    public final String getDbLink() {
	      return dbLink;
	    }

	    public ResolvedDBName pseudoResolve() {
	      if (partB != null) {
	        return new ResolvedDBName(partA.toUpperCase(), partB, partC, dbLink);
	      } else {
	        return new ResolvedDBName(null, partA, null, dbLink);
	      }
	    }
	  }

	  public static class ResolvedDBName {
	    private final ObjectNamespace namespace;

	    private final TokenizedDBName dbName;

	    private final TopLevelType part1Type;

	    private Integer objectId;

	    private String objectType;

	    public ResolvedDBName(ObjectNamespace namespace, String schema, String part1, String part2, String dbLink, TopLevelType part1Type, Integer objectId) {
	      this.namespace = namespace;
	      this.dbName = new TokenizedDBName(schema, part1, part2, dbLink);
	      this.part1Type = part1Type;
	      this.objectId = objectId;
	      this.objectType = null;
	    }

	    public ResolvedDBName(String schema, String part1, String part2, String dbLink) {
	      this(null, schema, part1, part2, dbLink, null, null);
	    }

	    public final ObjectNamespace getNamespace() {
	      return this.namespace;
	    }

	    public final String getSchema() {
	      return dbName.getPartA();
	    }

	    public final String getPart1() {
	      return dbName.getPartB();
	    }

	    public final String getPart2() {
		      return dbName.getPartC();
		    }

	    public final String getDbLink() {
	      return dbName.getDbLink();
	    }

	    public final TopLevelType getPart1Type() {
	      return part1Type;
	    }

	    public final Integer getObjectId() {
	      return objectId;
	    }

	    public final void setObjectId(Integer objectId) {
	      this.objectId = objectId;
	    }

	    public final String getObjectType() {
	      return objectType;
	    }

	    public final void setObjectType(String objectType) {
	      this.objectType = objectType;
	    }

	    public final String getTopLevelName() {
	      if (part1Type != null) {
	        switch (part1Type) {
	        case FUNCTION:
	        case PROCEDURE:
	          return getPart2();
	        }
	      }

	      return getPart1();
	    }

	    public final String getTopLevelType() {
	      if (objectType != null) {
	        return objectType;
	      } else if (part1Type != null) {
	        return part1Type.getObjectType();
	      } else if (namespace != null) {
	        return namespace.getObjectType();
	      } else {
	        return null;
	      }
	    }

	    @Override
		public ResolvedDBName clone() {
	      ResolvedDBName another = new ResolvedDBName(this.namespace, getSchema(), getPart1(), getPart2(), getDbLink(), this.part1Type, this.objectId);
	      another.setObjectType(this.objectType);

	      return another;
	    }
	  }

	  
	  protected List<DDLObjective> getObjectives(String object, String resultType) {
		    List<DDLObjective> ddlObjectives = new LinkedList<DDLObjective>();
		    LinkedList<String> componentTypes = new LinkedList<String>();

		    // Determine Lookup Type
		    String objectType;
		    boolean outputComponentTypeOnly;
		    if (resultType != null) {
		    	if (resultType.equalsIgnoreCase(DbmsMetadataDDLGenerator.TABLEPARTITION)) {
		      	  resultType = DbmsMetadataDDLGenerator.TABLE;
		        }
		    	resultType = resultType.toUpperCase();
		      switch (resultType) {
		      case DbmsMetadataDDLGenerator.DB_LINK:
		      case DbmsMetadataDDLGenerator.DBLINK:
		        objectType = DbmsMetadataDDLGenerator.DATABASELINK;
		        outputComponentTypeOnly = false;
		        break;
		      case DbmsMetadataDDLGenerator.MATVIEWLOG:
		        objectType = DbmsMetadataDDLGenerator.TABLE;
		        componentTypes.add(DbmsMetadataDDLGenerator.MATVIEWLOG);
		        outputComponentTypeOnly = true;
		        break;
		      default:
		        objectType = resultType;
		        outputComponentTypeOnly = false;
		      }
		    } else {
		      objectType = resultType;
		      outputComponentTypeOnly = false;
		    }

		    // Resolve Names
		    LinkedList<ResolvedDBName> resolvedNames = getTypes(object, objectType);

		    boolean objectFound = (resolvedNames.size() == 1 && resolvedNames.getFirst().getObjectId() != null);
		    boolean tooManyObjects = (resolvedNames.size() > 1);

		    if (resolvedNames.size() == 2) {
		      // Get First and Last
		      ResolvedDBName firstObject = resolvedNames.getFirst();
		      ResolvedDBName lastObject = resolvedNames.getLast();

		      // Fix Package and Body order
		      if (DbmsMetadataDDLGenerator.PACKAGEBODY.equalsIgnoreCase(firstObject.getTopLevelType()) && DbmsMetadataDDLGenerator.PACKAGE.equalsIgnoreCase(lastObject.getTopLevelType())) {
		        // Reverse over so Package is before Package body
		        resolvedNames.add(resolvedNames.pollFirst());

		        firstObject = resolvedNames.getFirst();
		        lastObject = resolvedNames.getLast();
		      }

		      if (DbmsMetadataDDLGenerator.PACKAGE.equalsIgnoreCase(firstObject.getTopLevelType()) && DbmsMetadataDDLGenerator.PACKAGEBODY.equalsIgnoreCase(lastObject.getTopLevelType())) {

		        objectFound = (firstObject.getObjectId() != null);
		        tooManyObjects = !objectFound;
		      }
		    } else if (objectFound && resolvedNames.size() == 1) {
		      ResolvedDBName firstObject = resolvedNames.getFirst();

		      ObjectNamespace ns = firstObject.getNamespace();
		      if (ns != null) {
		        String topLevelType = firstObject.getTopLevelType();

		        switch (ns) {
		        case PLSQL:
		          if (DbmsMetadataDDLGenerator.PL_SQL.equalsIgnoreCase(objectType) || DbmsMetadataDDLGenerator.PLSQL.equalsIgnoreCase(objectType)) {
		            if (DbmsMetadataDDLGenerator.PACKAGE.equalsIgnoreCase(topLevelType)) {
		              componentTypes.push(DbmsMetadataDDLGenerator.PACKAGEWITHBODY);
		              outputComponentTypeOnly = true;
		            }
		          } else if (!objectType.equalsIgnoreCase(topLevelType)) {
		            objectFound = false;
		          }
		          break;
		        case TABLE:
		          if (objectType != null && (!objectType.equalsIgnoreCase(topLevelType))) {
		            objectFound = false;
		          }
		          break;
		        }
		      }
		    }

		    // Create Objective
		    DDLObjective ddlObjective = new DDLObjective(object, objectType, componentTypes, outputComponentTypeOnly);
		    
		    // Check we can process
		    if (tooManyObjects) {
		      String origName = ddlObjective.getOrigName();

		      // Get Name List
		      StringBuffer buf = new StringBuffer();
		      for (ResolvedDBName resolvedName : resolvedNames) {
		        if (buf.length() != 0) {
		          buf.append(","); //$NON-NLS-1$
		        }
		        buf.append(resolvedName.getTopLevelType());
		      }

		      writeToCtx(MessageFormat.format(Messages.getString("DDL_OBJECT_NOT_UNIQUE"), new Object[] { origName, buf.toString() })); //$NON-NLS-1$
		      return null;
		    } else if (!objectFound) {
		      String origName = ddlObjective.getOrigName();
		      String origType = ddlObjective.getOrigType();
		      if (origType != null) {
		        writeToCtx(MessageFormat.format(Messages.getString("DDL_TYPED_OBJECT_NOT_FOUND"), new Object[] { origName, origType })); //$NON-NLS-1$
		      } else {
		        writeToCtx(MessageFormat.format(Messages.getString("DDL_OBJECT_NOT_FOUND"), new Object[] { origName })); //$NON-NLS-1$
		      }
		      return null;
		    }
		    
		    
		    

		    // Fill out resolved names
		    ddlObjective.setResolvedDBName(resolvedNames);

		    // Add objective
		    ddlObjectives.add(ddlObjective);

		    return ddlObjectives;
	  }
	  /**
	   *
	   * @param objectOwner
	   * @param objectName
	   * @return
	   */
	  protected LinkedList<ResolvedDBName> getTypes(String name, String typeHint) {
	    ResolvedDBName resolvedDBName = null;
	    LinkedList<ResolvedDBName> resolvedDBNames = new LinkedList<ResolvedDBName>();

	    try {
	      // Try to use typeHint
	      if (typeHint != null) {
	        ObjectNamespace namespace = ObjectNamespace.lookup(typeHint);

	        if (namespace != null) {
	          resolvedDBName = resolveDBName(name, namespace);
	        }
	      }

	      if (resolvedDBName == null) {
	        resolvedDBName = pseudoResolveDBName(name);
	      }

	      if (resolvedDBName.getTopLevelType() == null) {
	        String query;
	        Map<String, String> binds = new HashMap<String, String>();

	        if (resolvedDBName.getObjectId() != null) {
	          query = "select object_id,object_type from all_objects where object_id=:OBJECT_ID"; //$NON-NLS-1$
	          binds.put("OBJECT_ID", resolvedDBName.getObjectId().toString()); //$NON-NLS-1$
	        } else if (typeHint != null) {
	          query = "select object_id,object_type from all_objects where object_name=:OBJECT_NAME and owner=:OWNER and object_type = :OBJECT_TYPE"; //$NON-NLS-1$
	          try {
	            binds.put("OWNER", (resolvedDBName.getSchema() != null) //$NON-NLS-1$
	            ? resolvedDBName.getSchema()
	                : _conn.getMetaData().getUserName());
	            binds.put("OBJECT_NAME", resolvedDBName.getTopLevelName()); //$NON-NLS-1$
	            binds.put("OBJECT_TYPE", typeHint); //$NON-NLS-1$
	          } catch (SQLException e) {
	            resolvedDBName.setObjectType(typeHint);
	            binds = null;
	          }
	        } else {
	        	// Bug 21636020 - DDL DOES NOT RECOGNIZE TABLE PARTITION AS OBJECT TYPE IF MULTIPLE OBJS W/NAME 
	        	// Table with Table Partition returns multiple rows of same object name
	          query = "select object_id,object_type from all_objects where object_name=:OBJECT_NAME and owner=:OWNER and object_type != 'TABLE PARTITION' order by object_type asc"; //$NON-NLS-1$
	          try {
	            binds.put("OWNER", (resolvedDBName.getSchema() != null) //$NON-NLS-1$
	            ? resolvedDBName.getSchema()
	                : _conn.getMetaData().getUserName());
	            binds.put("OBJECT_NAME", resolvedDBName.getTopLevelName()); //$NON-NLS-1$
	          } catch (SQLException e) {
	            resolvedDBNames.add(resolvedDBName);
	            binds = null;
	          }
	        }

	        if (binds != null) {
	          ResultSet rs = DBUtil.getInstance(_conn).executeQuery(query, binds);

	          if (rs != null) {
	            try {
	              while (rs.next()) {
	                Integer id = (rs.getObject(1) != null) ? rs.getInt(1) : null;
	                String type = rs.getString(2);

	                ResolvedDBName template = resolvedDBName.clone();

	                // Apply instance data
	                template.setObjectId(id);
	                template.setObjectType(type);

	                // Add to list
	                resolvedDBNames.add(template);
	              }
	            } catch (SQLException e) {
	              ; // consume
	            } finally {
	              try {
	            	  if (rs!=null) {
	            		 Statement s = rs.getStatement();
	            		 if (s!=null) {
	            			 s.close();
	            		 }
	                rs.close();
	            	  }
	              } catch (SQLException e) {
	                ; // consume
	              }
	            }
	          }
	        }
	      }
	    } finally {
	      if (resolvedDBNames.size() == 0) {
	        resolvedDBNames.add(resolvedDBName);
	      }
	    }

	    return resolvedDBNames;
	  }

	  protected TokenizedDBName callTokenizeDBName(String name) throws SQLException {
	    final String nameResolve = "begin SYS.DBMS_UTILITY.NAME_TOKENIZE(?,?,?,?,?,?); end;"; //$NON-NLS-1$

	    if (LockManager.lock(_conn)) {
	      CallableStatement stmt = null;
	      try {
	        try {
	          stmt = _conn.prepareCall(nameResolve);

	          stmt.setString(1, name);
	          stmt.registerOutParameter(2, Types.VARCHAR);
	          stmt.registerOutParameter(3, Types.VARCHAR);
	          stmt.registerOutParameter(4, Types.VARCHAR);
	          stmt.registerOutParameter(5, Types.VARCHAR);
	          stmt.registerOutParameter(6, Types.INTEGER);
	          stmt.execute();

	          String partA = stmt.getString(2);
	          String partB = stmt.getString(3);
	          String partC = stmt.getString(4);
	          String dbLink = stmt.getString(5);

	          return new TokenizedDBName(partA, partB, partC, dbLink);
	        } finally {
	          if (stmt != null) {
	            try {
	              stmt.close();
	            } catch (SQLException e) {
	              ; // consume
	            }
	          }
	        }
	      } finally {
	        LockManager.unlock(_conn);
	      }
	    }

	    return null;
	  }

	  protected ResolvedDBName callResolveDBName(String name, ObjectNamespace namespace) throws SQLException {
	    final String nameResolve = "begin SYS.DBMS_UTILITY.NAME_RESOLVE(?,?,?,?,?,?,?,?); end;"; //$NON-NLS-1$

	    if (LockManager.lock(_conn)) {
	      CallableStatement stmt = null;
	      try {
	        try {
	          stmt = _conn.prepareCall(nameResolve);

	          stmt.setString(1, name);
	          stmt.setInt(2, namespace.getIndex());
	          stmt.registerOutParameter(3, Types.VARCHAR);
	          stmt.registerOutParameter(4, Types.VARCHAR);
	          stmt.registerOutParameter(5, Types.VARCHAR);
	          stmt.registerOutParameter(6, Types.VARCHAR);
	          stmt.registerOutParameter(7, Types.INTEGER);
	          stmt.registerOutParameter(8, Types.INTEGER);
	          stmt.execute();

	          String schema = stmt.getString(3);
	          String part1 = stmt.getString(4);
	          String part2 = stmt.getString(5);
	          String dbLink = stmt.getString(6);
	          Integer part1Type = (stmt.getObject(7) != null) ? stmt.getInt(7) : null;
	          Integer objectId = (stmt.getObject(8) != null) ? stmt.getInt(8) : null;

	          TopLevelType topLevelType = TopLevelType.lookup(part1Type);

	          return new ResolvedDBName(namespace, schema, part1, part2, dbLink, topLevelType, objectId);
	        } finally {
	          if (stmt != null) {
	            try {
	              stmt.close();
	            } catch (SQLException e) {
	              ; // consume
	            }
	          }
	        }
	      } finally {
	        LockManager.unlock(_conn);
	      }
	    }

	    return null;
	  }

	  protected TokenizedDBName tokenizeDBName(String name) {
	    TokenizedDBName tokenizedName;
	    try {
	      tokenizedName = callTokenizeDBName(name);
	    } catch (SQLException e) {
	      tokenizedName = null;
	    }

	    return (tokenizedName != null) ? tokenizedName : new TokenizedDBName(name);
	  }

	  protected ResolvedDBName pseudoResolveDBName(String name) {
	    return tokenizeDBName(name).pseudoResolve();
	  }

	  protected ResolvedDBName resolveDBName(String name, ObjectNamespace context) {
	    try {
	      return callResolveDBName(name, context);
	    } catch (SQLException e) {
	      return pseudoResolveDBName(name);
	    }
	  }
	  
	  protected void writeToCtx(String message, BufferedWriter bw) throws IOException {
	    if (message != null && bw == null) {
	      _ctx.write(message);
	      _ctx.write("\n"); //$NON-NLS-1$
	    }

	    if (message != null && bw != null) {
	      bw.write(message);
	      bw.newLine();
	    }
	  }

	  protected void writeToCtx(String message) {
	    try {
	      writeToCtx(message, null);
	    } catch (IOException e) {
	      ; // consume - can never happen as null passed
	    }
	  }
	  public boolean processDDL(BufferedWriter bw, String object, String resultType) {
			 boolean ok = false;
			      // Create list of DDL Objective
			      List<DDLObjective> ddlObjectives = getObjectives(object, resultType);

			      if (ddlObjectives != null) {
			    	  try {
						ok = getDDL(bw, ddlObjectives);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			      }

		  return ok;
	  }
	  /**
	   * 
	   * @param object
	   * @param resultType
	   * @param filename
	   * @return
	   */
	  public boolean processDDL(String object, String resultType, String filename) {
		  return processDDL(object, resultType, filename, true);
	  }
	  /**
	   *
	   * @param object
	   * @param filename
	   * @return
	   * @throws Exception
	   */
	  public boolean processDDL(String object, String resultType, String filename, boolean append) {
	 boolean ok = false;

	    BufferedWriter bw = null;
	    
	    try {
	      // Create list of DDL Objective
	      List<DDLObjective> ddlObjectives = getObjectives(object, resultType);

	      if (ddlObjectives != null) {
	        // Create File
	        File f = null;
	        if (filename != null) {
	          f = new File(filename);
	          try {
		          if (!f.exists()) {		            
		              f.createNewFile();
		          } else {
		        	  if (!append) {
			        	  f.delete();
			              f.createNewFile();
		        	  }
		          }
	            } catch (IOException e) {
		              // Cannot access file
		              writeToCtx(MessageFormat.format(Messages.getString("DDL_FILE_NOT_CREATED"), new Object[] { filename, e.getLocalizedMessage() })); //$NON-NLS-1$
		              ok = false;
		              return ok;
		            }

	          bw = new BufferedWriter(new FileWriter(f, true));
	        }

	        // Clear Buffer
	        if (_ctx.getSQLPlusBuffer()!=null)
	        _ctx.getSQLPlusBuffer().clear();

	        // Get DDL
	        ok = getDDL(bw, ddlObjectives);
	      }
	    } catch (IOException e) {
	      // Cannot access file
	      writeToCtx(MessageFormat.format(Messages.getString("DDL_FILE_NOT_WRITABLE"), new Object[] { filename, e.getLocalizedMessage() })); //$NON-NLS-1$
	      ok = false;
	    } finally {
	      // Close Resources
	      if (bw != null) {
	        try {
	          bw.flush();
	        } catch (IOException fce) {
	          ; // ignore
	        }
	        try {
	          bw.close();
	        } catch (IOException fce) {
	          ; // ignore
	        }
	      }
	    }

	    return ok;
	  }
	  protected boolean getDDL(BufferedWriter bw, List<DDLObjective> ddlObjectives) throws IOException {
		    boolean ok = (ddlObjectives.size() > 0);

		    // Get DLL for Objectives
		    Iterator<DDLObjective> iterator = ddlObjectives.iterator();

		    while (ok && iterator.hasNext()) {
		      DDLObjective ddlObjective = iterator.next();
		      ok = getDDL(bw, ddlObjective);
		    }

		    return ok;
		  }

		  protected boolean getDDL(BufferedWriter bw, DDLObjective ddlObjective) throws IOException {
		    boolean ok = (ddlObjective.getResolvedDBNames().size() > 0);

		    // Get DLL for Components
		    Iterator<ResolvedDBName> objectIter = ddlObjective.getResolvedDBNames().iterator();

		    while (ok && objectIter.hasNext()) {
		      ResolvedDBName resolvedDBName = objectIter.next();

		      // Get DDL for Top Level
		      if (!ddlObjective.outputComponentsOnly()) {
		        ok = getDDL(bw, ddlObjective, resolvedDBName.getTopLevelType(), resolvedDBName);
		      }

		      // Get DLL for Components
		      Iterator<String> componentIter = ddlObjective.getComponentTypes().iterator();

		      while (ok && componentIter.hasNext()) {
		        String type = componentIter.next();
		        ok = getDDL(bw, ddlObjective, type, resolvedDBName);
		      }
		    }

		    return ok;
		  }

		  /**
		   *
		   * @param conn
		   * @param id
		   * @return
		   */
		  protected final String getQuery(Connection conn, String id) {
		    Query q = getQueryFromFile(conn, id, getQueryFile());
		    if (q == null) {
		      // Fall back to the default query file if we couldn't find the query
		      // in the
		      // type-specific file
		      q = getQueryFromFile(conn, id, getDefaultQueryFile());
		    }
		    return q.getSql();
		  }

		  /**
		   * @return
		   */
		  protected String getQueryFile() {
		    return getDefaultQueryFile();
		  }

		  /**
		   *
		   * @param conn
		   * @param id
		   * @param file
		   * @return
		   */
		  protected Query getQueryFromFile(Connection conn, String id, String file) {
		    URL url = this.getClass().getResource(file);
		    MetaResource res = new MetaResource(getClass().getClassLoader(), file);
		    if (res.toURL() == null)
		      res.setUrl(url);

		    QueryXMLSupport xml = QueryXMLSupport.getQueryXMLSupport(res);
		    Query q = xml.getQuery(id, conn);
		    return q;
		  }

		  /**
		   * @return
		   */
		  protected String getDefaultQueryFile() {
		    return "META-INF/ddl/oracle/ObjectSql.xml"; //$NON-NLS-1$
		  }

		  protected boolean getDDL(BufferedWriter bw, DDLObjective ddlObjective, String type, ResolvedDBName resolvedDBName) throws IOException {
		    boolean ok = false;
 	        ResultSet rset = null;
		    SQLException se = null;

		    try {
			  String sqlfilename = "META-INF/ddl/oracle/"; //$NON-NLS-1$
			  String dbafilename = "META-INF/ddl/oracledba/"; //$NON-NLS-1$
		      Query query = null;
		      switch (type) {
		      case DbmsMetadataDDLGenerator.TABLEPARTITION: // Adding partitions in here
		      case DbmsMetadataDDLGenerator.TABLE:
		        sqlfilename += "TableSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.TABLE_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.VIEW:
		        sqlfilename += "ViewSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.VIEW_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.INDEX:
		        sqlfilename += "IndexSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.INDEX_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.CONSTRAINT:
		        sqlfilename += "ConstraintSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.CONSTRAINT_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.FUNCTION:
		        sqlfilename += "FunctionSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.FUNCTION_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.CONSUMERGROUP:
		        dbafilename += "ConsumerGroupSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.CONSUMERGROUP_DDL, dbafilename);
		        break;
		      case DbmsMetadataDDLGenerator.CONTAINERRMPLAN:
		        dbafilename += "ContainerRMPlanSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.CONTAINERRMPLAN_DDL, dbafilename);
		        break;
		      case DbmsMetadataDDLGenerator.DATAFILE:
		        dbafilename += "DataFileSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.DATAFILE_DDL, dbafilename);
		        break;
		      case DbmsMetadataDDLGenerator.DBLINK:
		        sqlfilename += "DbLinkSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.DBLINK_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.MATVIEW:
		        sqlfilename += "MatViewSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.MATVIEW_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.MATVIEWLOG:
		        sqlfilename += "MatViewLogSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.MATVIEWLOG_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.OBJECT:
		        sqlfilename += "ObjectSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.OBJECT_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.PROFILE:
		        dbafilename += "ProfileSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.PROFILE_DDL, dbafilename);
		        break;
		      case DbmsMetadataDDLGenerator.QUEUE:
		        sqlfilename += "QueueSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.QUEUE_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.QUEUETABLE:
		        sqlfilename += "QueueTableSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.QUEUETABLE_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.REDOLOGGROUP:
		        dbafilename += "RedoLogGroupSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.REDOLOGGROUP_DDL, dbafilename);
		        break;
		      case DbmsMetadataDDLGenerator.PROCEDURE:
		        sqlfilename += "ProcedureSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.PROCEDURE_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.PACKAGE:
		        sqlfilename += "PackageSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.PACKAGESPEC_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.PACKAGEBODY:
		        sqlfilename += "PackageSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.PACKAGEBODY_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.PACKAGEWITHBODY:
		        sqlfilename += "PackageSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.PACKAGE_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.SEQUENCE:
		        sqlfilename += "SequenceSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.SEQUENCE_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.SYNONYM:
		        sqlfilename += "SynonymSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.SYNONYM_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.TABLESPACE:
		        dbafilename += "TableSpaceSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.TABLESPACE_DDL, dbafilename);
		        break;
		      case DbmsMetadataDDLGenerator.TYPE:
		    	  sqlfilename += "TypeSql.xml"; //$NON-NLS-1$
		          query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.TYPE_DDL, sqlfilename);
		          break;
		      case DbmsMetadataDDLGenerator.TYPEBODY:
		        sqlfilename += "TypeBodySql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.TYPE_BODY_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.USER:
		        dbafilename += "UserSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.USER_DDL, dbafilename);
		        break;
		      case DbmsMetadataDDLGenerator.ROLE:
		        dbafilename += "RoleSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.ROLE_DDL, dbafilename);
		        break;
		      case DbmsMetadataDDLGenerator.REFCONSTRAINT:
		        sqlfilename += "RefConstraintSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.REFCONSTRAINT_DDL, sqlfilename);
		        break;
		      case DbmsMetadataDDLGenerator.TRIGGER:
		        sqlfilename += "TriggerSql.xml"; //$NON-NLS-1$
		        query = getQueryFromFile(_conn, DbmsMetadataDDLGenerator.TRIGGER_DDL, sqlfilename);
		        break;
		      default:
		        break;
		      }

		      Map<String, String> binds = new HashMap<String, String>();

		      binds.put("OWNER", (resolvedDBName.getSchema() != null) //$NON-NLS-1$
		      ? resolvedDBName.getSchema()
		          : _conn.getMetaData().getUserName());
		      binds.put("NAME", resolvedDBName.getTopLevelName()); //$NON-NLS-1$

		      DBUtil dbUtil = DBUtil.getInstance(_conn);
		      dbUtil.execute("begin dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SQLTERMINATOR',true); end;"); //$NON-NLS-1$
		      rset = dbUtil.executeQuery(query.getSql(), binds);
		      if (rset != null) {
		        try {
		          while (rset.next()) {
		            String block = rset.getString(1);
		            String[] lines = block.split("\n"); //$NON-NLS-1$

		            for (int i = 0; i < lines.length; i++) {
		                // Load SQLPlus Buffer if it exists
		            	  if (_ctx!=null && _ctx.getSQLPlusBuffer()!=null)
		            		  _ctx.getSQLPlusBuffer().add(lines[i]);
		                // Write to Prompt nd File
		                writeToCtx(lines[i], bw);
		            }
		          }
            	  	if (_ctx!=null && _ctx.getSQLPlusBuffer()!=null)
            		  _ctx.getSQLPlusBuffer().setBufferSafe(_ctx.getSQLPlusBuffer().getBufferList());
		          ok = true;
		        } finally {
	        	  DBUtil.closeResultSet(rset);
		        }
		      } else {
		        se = dbUtil.getLastException();
		      }
		    } catch (SQLException e) {
		      se = e;
		    } finally {
		    	DBUtil.closeResultSet(rset);
			}

		    if (se != null) {
		      writeToCtx(MessageFormat.format(Messages.getString("DDL_OBJECT_TYPE_NOT_GENERATED"), new Object[] { ddlObjective.getOrigName(), type, se.getLocalizedMessage() })); //$NON-NLS-1$
		      ok = false;
		    }

		    return ok;
		  }

}
