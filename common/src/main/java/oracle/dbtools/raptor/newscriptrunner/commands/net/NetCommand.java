/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.net;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptParser;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;
import oracle.dbtools.raptor.query.Bind;


/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=NetCommand.java"
 *         >Barry McGillin</a>
 *
 */
public class NetCommand extends CommandListener  implements IHelp {
	private static String LIST = "list";
	private static String DROP = "drop";
	private static String NET = "net";
	private static String SAVE = "save";
	private static String LOAD = "load";
	private static String SHOW = "show";
	private static String HELP = "help";
	public static final String ON = "ON";
	public static final String OFF = "OFF";
	public static final String WARN = "WARN";
	public static final String READONLY = "READONLY";

  /*
   * HELP DATA
   *
   */
  public String getCommand(){
    return "NET";
  }
  
  public String getHelp(){
    return CommandsHelp.getString(getCommand());
  }

  @Override
  public boolean isSqlPlus() {
    return false;
  }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java
	 * .sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String[] tokens = cmd.getSql().trim().split("[ \t\n]+"); //$NON-NLS-1$ 
		if (tokens.length > 0) {
			String query=null;
			if (tokens[0].toLowerCase().equals(getCommandName())) {
				if (tokens.length == 1) {
					ArrayList<String> netEntries = NetEntries.getInstance().getNetEntries();
					Iterator<String> it = netEntries.iterator();
					while (it.hasNext()) {
						ctx.write(it.next() + "\n"); //$NON-NLS-1$ 
					}
					return handleReturn(true);
				} else if (tokens.length > 1) {
					String key=""; //$NON-NLS-1$ 
					if (cmd.getSql().contains("=")&&(!tokens[1].startsWith("="))) { //$NON-NLS-1$  //$NON-NLS-2$ 
						String[] preEqSplit=cmd.getSql().trim().split("=")[0].split("\\s+"); //$NON-NLS-1$  //$NON-NLS-2$ 
						key = preEqSplit[1];
						if (preEqSplit.length>2) {
							key=key+preEqSplit[2];
						}
						String payload = cmd.getSql().substring(cmd.getSql().toLowerCase().indexOf("=") + 1).trim(); //$NON-NLS-1$ 
						if (payload.startsWith(":")) { //$NON-NLS-1$ 
							if (payload.length() > 1) {
								payload = payload.substring(1);
								Bind b = ctx.getVarMap().get(payload.toUpperCase());
								if (b != null) {
									query = b.getValue();
								}
							}
							if ((query == null) || (query.equals(""))) { //$NON-NLS-1$ 
								ctx.write("NET-006: No valid bind value\n"); //$NON-NLS-1$ 
								return handleReturn(true);
							}
						} else {
							String payloadUpper = payload.toUpperCase();
							if ((payload.length() > 5) && payloadUpper.startsWith("Q'") //$NON-NLS-1$ 
									&& payload.endsWith(ScriptParser.getEndQuoteString(payload.substring(2, 3)) + "'")) { //$NON-NLS-1$ 
								payload = payload.substring(3, payload.length() - 2);
							} else {
								if ((payload==null||payload.equals(""))) { //$NON-NLS-1$ 
									ctx.write("NET-005: String not valid\n"); //$NON-NLS-1$ 
									return handleReturn(true);
								}
							}
							query = payload;
						}
						add(ctx,(String)ctx.getProperty(ScriptRunnerContext.SETNETOVERWRITE),key,query,true);
						return handleReturn(true);
					//} else if (asToken < 3) {
						//ctx.write("NET-003: Net add does not have = token in right place\n");
						//return handleReturn(true);
					} else if (tokens[1].toLowerCase().equals(DROP)) {
						if (tokens.length == 3) {
							NetEntries.getInstance().drop(tokens[2]);
							return handleReturn(true);
						}
					} else if (tokens[1].toLowerCase().equals(LIST)) {
						//placeholder same as no args
						String net=""; //$NON-NLS-1$ 
						if (tokens.length>2 && tokens[2].length()>0) {
							net=tokens[2];
							String matchingEntry=NetEntries.getInstance().get(net);
							if (matchingEntry!=null) {
								String line = new String(new char[net.length()]).replace("\0", "-"); //$NON-NLS-1$  //$NON-NLS-2$ 
								ctx.write(MessageFormat.format("{0}\n{1}\n", new Object[] {net, line})); //$NON-NLS-1$  //$NON-NLS-2$ 
								ctx.write(matchingEntry+"\n"); //$NON-NLS-1$ 
							} else {
								ctx.write(MessageFormat.format("NET-010 - net {0} not found \n", new Object[] {net})); //$NON-NLS-1$ 
							}
						} else {
							Iterator<String> it = NetEntries.getInstance().getNetEntries().iterator();
							
							while (it.hasNext()) {
								ctx.write(it.next() + "\n"); //$NON-NLS-1$ 
							}
						}
					    return handleReturn(true);
					} else if (tokens[1].toLowerCase().equals(SAVE)) {
						if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING)!=null) {
	                         if (tokens.length>2 && tokens[2]!=null) {
	                                ctx.write(NetEntries.getInstance().save(tokens[2]));
	                            } else {
	                                ctx.write(NetEntries.getInstance().save());
	                            }
						} else {
							ctx.write("NET-008 - save only enabled in command line mode\n"); //$NON-NLS-1$ 
						}
						return handleReturn(true);
					} else if (tokens[1].toLowerCase().equals(LOAD)) {
                        if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING)!=null) {
                            if (tokens.length>2 && tokens[2]!=null) {
                                ctx.write(NetEntries.getInstance().load(tokens[2]));
                            } else {
                                ctx.write(NetEntries.getInstance().load());
                            }
                        } else {
                            ctx.write("NET-009 - load only enabled in command line mode\n");
                        }
                        return handleReturn(true);
					}else if (tokens[1].toLowerCase().equals(LOAD)) {
						if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING)!=null) {
							NetEntries.getInstance().load();
						} else {
							ctx.write("NET-009 - load only enabled in command line mode\n"); //$NON-NLS-1$ 
						}
						return handleReturn(true);
					} else if (tokens[1].toLowerCase().equals(HELP)) {
						ctx.write(getHelp());  
					
					}else {
						ctx.write("NET-001: Net command not found\n"); //$NON-NLS-1$ 
						return handleReturn(true);
					}
				} else {
					ctx.write("NET-002: Net add not enough arguments\n"); //$NON-NLS-1$ 
					return handleReturn(true);
				}
			}
		}
		return false;// not net handleReturn(false);
	}
	public static void addOrWarn(ScriptRunnerContext src, String overWrite, String key, String query, boolean fromCommand) {
	    if (overWrite==null) {
	        overWrite=WARN;
	    }
	    String existing=NetEntries.getInstance().get(key);
	    switch (overWrite) {
	        case WARN:
	            if (existing!=null) {
	                //warn if already exists and does not match current entry
	                if (!(existing!=null&&query.trim().equalsIgnoreCase(existing.trim()))) {
	                    src.write(ScriptRunnerDbArb.format(ScriptRunnerDbArb.KEY_EXISTS,
	                        "net "+key.toUpperCase()+"="+query+";")); //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$ 
	                }
	                return;
	            }
	            break;
	        case OFF:
	            if (existing!=null) {
	                return;
	            }
	            break;
	    }
	    if (!(existing!=null&&query.trim().equalsIgnoreCase(existing.trim()))) {
	        //if there is a change write it
	        NetEntries.getInstance().add(key, query);
	    }
	    return;
	}
	public static void add(ScriptRunnerContext src, String overWrite, String key, String query, boolean fromCommand) {
        //with Binds introduced, then shortly after set by default.
	    //auto add service_name removed.
        if ((key.length()<40&&(key.indexOf("=")==-1))) {//No point in clogging it up with long (DESCRIPTION= as key //$NON-NLS-1$ 
            if (fromCommand) {
                NetEntries.getInstance().add(key, query);
            } else {
                addOrWarn(src, overWrite, key, query, fromCommand);
            }
        }
        NetEntries.getInstance().save();
	}
    public String getCommandName(){
    	return NET;
    }
	boolean handleReturn(boolean b) {
		if (b == false) {
			// doWhenever(true);
		}
		// may want to do whenever - do not want to fall back to default as it
		// thinks it is a sql statement as it ends with ;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java
	 * .sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql
	 * .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

}
