/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.net;
import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Set;

import oracle.dbtools.common.utils.PlatformUtils;
import oracle.dbtools.raptor.utils.TCPTNSEntry;
import oracle.dbtools.raptor.utils.TNSHelper;

/**
 * 
 */
public class NetEntries {
	public static final String CONFIGFILE=File.separator+"netEntries.xml";
	public static final String CONFIGBACK=File.separator+"netEntries.xml~";
	public static Object LOCK=new Object();
	public static String getBase() {
		String storage=System.getProperty("user.home")+File.separator+".sqlcl";
		if (PlatformUtils.isWindows()) {
			String appData=System.getenv("APPDATA");
			if (appData!=null) {
				storage=appData+File.separator+"sdsql";
			} else {
				storage=null;
			}
		}
		return storage;
	}
	private HashMap<String, Net> netEntries = new HashMap<String, Net>();
	//assumption we only need to know if it is a bind for now
	private static NetEntries _INSTANCE;
    private Set<String> netEntriesUpper = new HashSet<String>();
	public static NetEntries getInstance() {
		synchronized(LOCK) {
		    if (_INSTANCE == null) {
		        _INSTANCE = new NetEntries();
		        _INSTANCE.load();
		    }
		}
		return _INSTANCE;
	}
	/**
	 * Thus was called initially from NetCommand, 
	 * is Bind is true by default - if neither with subs or with binds is used.
	 */

	private NetEntries() {
		
		URL u = this.getClass().getClassLoader().getResource("oracle/dbtools/raptor/newscriptrunner/commands/net/netEntries.xml");
		NetParser parser=new NetParser();
		try {
			ArrayList<Net> filenetEntries = parser.processXML(u);
			Iterator<Net> it = filenetEntries.iterator();
			while (it.hasNext()) {
				Net a = it.next();
				netEntries.put(a.getName(), a);
				netEntriesUpper.add(a.getName().toUpperCase());
			}
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,e.getLocalizedMessage());
		}
	}
	
	public static ArrayList<String> getMatching(String stub) {
	    return getMatching(stub,true);//probably need to handle the boolean at the net rather than the tab end probably by making true the default and a global
	}
	
	public static ArrayList<String> getMatching(String stub, boolean withNetXml) {
	    ArrayList<String> gold=new ArrayList<String>();
	    boolean all=false;
	    if ((stub==null) ||(stub.equals(""))) {
	        all=true;
	    } else {
	        stub=stub.toUpperCase();
	    }
	    synchronized(LOCK) {
    	    NetEntries ne=NetEntries.getInstance();
    	    HashSet<String> upper=new HashSet<String>();
    	    //need to add the tnsentries
    	    ArrayList<TCPTNSEntry> myTCPEntry = TNSHelper.getTNSEntries();
    	    if (myTCPEntry != null) {
    	        for (int i = 0; i < myTCPEntry.size(); i++) {
    	            String local=myTCPEntry.get(i).getName();
    	            String localUpper=local.toUpperCase();
    	            // a problem assume it is up to the first . if there is a dot
    	            // note yourmachine.yourdomain will be picked up in connect by blah or yourmachine.yourdomain 
    	            //so no need to duplicate entries here (for now)
    	            if (all||(localUpper.startsWith(stub))) {
    	                if (!(upper.contains(localUpper))) {
    	                    upper.add(localUpper);
    	                    gold.add(local);
    	                }
    	            }
    	        }
    	    }
    	    if (withNetXml) {
        	    //double loop for entries getnetentries goes through a loop as well as a loop here.
        	    ArrayList<String> allNe=ne.getNetEntries(); 
        	    if (allNe!=null) {
        	        for (String local:allNe) {
        	            String localUpper=local.toUpperCase();
        	            if (all||(localUpper.startsWith(stub))) {
        	                if (!(upper.contains(localUpper))) {
        	                    upper.add(localUpper);
        	                    gold.add(local);
        	                }
        	            }
        	        }
        	    }
    	    }
    	    Collections.sort(gold, String.CASE_INSENSITIVE_ORDER);
	    }
	    return gold;
	}
	public void add() {
		
	}
	public void add(String name, String query, String desc) {
		synchronized(LOCK) {
		    if (netEntriesUpper.contains(name.toUpperCase())) {
		        removeIgnoreCase(name);
    			netEntriesUpper.remove(name.toUpperCase());
    		}
    		Net net = new Net(name, query, desc);
    		netEntries.put(name, net);
    		netEntriesUpper.add(name.toUpperCase());
    		this.save();
		}
	}
	
	public void removeIgnoreCase(String key) {
	    ArrayList<String> al=new ArrayList<String>();
	    for (String entry:netEntries.keySet()) {
	        if (entry.equalsIgnoreCase(key)) {
	            al.add(entry);
	        }
	    }
	    for (String remove:al) {
            netEntries.remove(remove);
	    }
	}
	public String getKeyIgnoreCase(String key) {
	    for (String entry:netEntries.keySet()) {
	       if (entry.equalsIgnoreCase(key)) {
	           return(entry);
	       }
	    }
	    return null;
	}
	public void add(String name, String query) {
		add(name, query, "");
	}
	/**
	 * 
	 */

	public void drop(String name) {
	    synchronized(LOCK) {
    		if (netEntriesUpper.contains(name.toUpperCase())) {
    			this.removeIgnoreCase(name);
    			netEntriesUpper.remove(name.toUpperCase());
    			save();
    		}
	    }
	}
	/**
	 * 
	 */

	public ArrayList<String> getNetEntries() {
		ArrayList<String> list = new ArrayList<String>();
		synchronized(LOCK) {
    		for (String net : netEntries.keySet()) {
    			list.add(net);
    		}
		}
		Collections.sort(list);
		return list;
	}
	/**
	 * 
	 */
	
	public boolean contains(String key) {
	    synchronized(LOCK) {
    		if (this.netEntriesUpper.contains(key.toUpperCase())) {
    			return true;
    		} else {
    			return false;
    		}
	    }
	}
	
	public String get(String key) {
	    synchronized(LOCK) {
	        Net net=netEntries.get(key);
            if (net!=null) {
                return net.getQuery();
            }
	        Set<String> sset=netEntries.keySet();
	        for (String s:sset) {
	            if (s.equalsIgnoreCase(key)) {
	                Net n2=netEntries.get(s);
	                if (n2!=null) {
	                    return n2.getQuery();
	                }
	            }
	        }
	        return null;
	        /*Net net=netEntries.get(key);
	        if (net==null) {
	            return null;
	        }
	        return net.getQuery(); maybe want case insensitive get*/
	    }
	}

	public String save() {
	    return save(null);
	}
	public String save(String filename) {
	    ArrayList<Net> netEntriesAL=new ArrayList<Net>();
	    String exceptionRet=null;
	    synchronized(LOCK) {
	        NetParser parser=new NetParser();
	            
	        try {
	            String base=getBase();
	            if (base==null) {
	                throw new Exception("NET-013 APPDATA is null");
	            }
	            if (base!=null&&!(new File(base).exists())) {
	                if (!(new File(base).mkdirs())) {
	                    throw new Exception ("NET-009 "+base+" Directory did not create");
	                }
	            }
	            File file = null;
	            if (filename!=null) {
	                file=new File(filename);
	            } else {
	                filename =base+CONFIGFILE;
	                file = new File(filename);
	                if (base!=null&& new File(base+CONFIGFILE).exists()) {
	                    if  (new File(base+CONFIGBACK).exists()) {
	                        if (!new File(base+CONFIGBACK).delete()) {
	                            throw new Exception ("NET-011 "+base+CONFIGBACK+" could not be deleted ");
	                        }
	                    }
	                    try {
	                        Files.move(Paths.get(base+CONFIGFILE),Paths.get(base+CONFIGBACK),REPLACE_EXISTING);
	                    } catch (Exception e) {
	                        throw new Exception ("NET-010 "+base+CONFIGFILE+" could not be renamed to "+base+CONFIGBACK+" "+e.getMessage());
	                    }
	                }
	            }
	            Iterator<Entry<String, Net>> it = netEntries.entrySet().iterator();
	            while (it.hasNext()) {
	                Entry<String, Net> a = it.next();
	                Net al = a.getValue();
	                netEntriesAL.add(al);
	            }
	            if (base!=null) {
	                parser.putXML(file, netEntriesAL);
	            }
   
	        } catch (Exception e) {
	            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,e.getLocalizedMessage());
	            exceptionRet=e.getLocalizedMessage()+"\n";
	        }
	    }
	    if (exceptionRet==null) {
	        return MessageFormat.format("Net-007 - Net Entries saved to {0}\n", filename);
	    } else {
	        return exceptionRet;
	    }
	}
	public String load() {
	       return load(null);
	}
	public String load(String filename) {
	    synchronized(LOCK) {
	        File file = null;
	        if (filename!=null) {
	            file=new File(filename);
	        } else {
	            String base=getBase();
	            if (base==null) {
	                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,"NET-013 APPDATA is null\n");
	            }
	            filename=base+CONFIGFILE;
	            file=new File(filename);
	        }
    		NetParser parser=new NetParser();
    		try {
    			if (file!=null&&file.exists()) {
    				URL u = file.toURI().toURL();
    				if (file.canRead()) {
    					ArrayList<Net> filenetes = parser.processXML(u);
    					Iterator<Net> it = filenetes.iterator();
    					while (it.hasNext()) {
    						Net a = it.next();
    						this.removeIgnoreCase(a.getName());
                            netEntries.put(a.getName(), a);
                            netEntriesUpper.add(a.getName().toUpperCase());
    					}
    				} else {
    	                   return MessageFormat.format("NET-012 {0} could not be read.\n",filename);
    				}
    			} else {
    			    return MessageFormat.format("NET-012 {0} could not be read.\n",filename);
    			}
    		} catch (Exception e) {
    			Logger.getLogger(this.getClass().getName()).log(Level.WARNING,e.getLocalizedMessage());
    		}
	    }
		return ""; 
	}
	
}
