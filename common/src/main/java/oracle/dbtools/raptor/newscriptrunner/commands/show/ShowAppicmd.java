/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.MessageFormat;

import oracle.dbtools.db.LockManager;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLPLUS;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.IStoreCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.commands.SetAppinfo;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowAppicmd implements IShowCommand, IStoreCommand {
	private static final String[] SHOWAPPICMD = { "appi", //$NON-NLS-1$
			"appin", //$NON-NLS-1$
			"appinf", //$NON-NLS-1$
			"appinfo" }; //$NON-NLS-1$

	@Override
	public String[] getShowAliases() {
		return SHOWAPPICMD;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if ((ctx.getCurrentConnection() == null) || (ctx.getCurrentConnection() instanceof OracleConnection)) {
			return doShowAppicmd(conn, ctx, cmd);
		}
		return false;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return true;
	}

	private boolean doShowAppicmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		try {
			String appinfoValue = SetAppinfo.APPINFO; //$NON-NLS-1$
			boolean appi = (boolean) ctx.getProperty(ScriptRunnerContext.APPINFO);
			String appiString = ctx.getProperty(ScriptRunnerContext.APPINFOSTRING)!=null? String.valueOf(ctx.getProperty(ScriptRunnerContext.APPINFOSTRING)):null;
			if (appiString==null) appiString = SQLPLUS.PRODUCT_NAME;
			String onOrOff = "OFF"; //$NON-NLS-1$
			if (!appi) {
				appinfoValue = appiString;
			} else {
				appinfoValue = newValue(ctx, appinfoValue);
			}

			if ((ctx.getProperty(ScriptRunnerContext.APPINFO) != null) && ((Boolean) ctx.getProperty(ScriptRunnerContext.APPINFO).equals(new Boolean(true)))) {
				onOrOff = "ON"; //$NON-NLS-1$
			}
			ctx.write(MessageFormat.format(Messages.getString("APPINFO"), onOrOff, appinfoValue) + "\n"); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (Exception e) {
			ctx.write(e.getLocalizedMessage());
		}
		return true;

	}

	/**
	 * @param ctx
	 * @param appinfoValue
	 * @return
	 * @throws SQLException
	 */
	private String newValue(ScriptRunnerContext ctx, String appinfoValue) throws SQLException {
		if (ctx.getCurrentConnection() instanceof OracleConnection && !ctx.getCurrentConnection().isClosed()) {
			CallableStatement myCallable = null;
			if (LockManager.lock(ctx.getCurrentConnection())) {
				try {
					myCallable = ctx.getCurrentConnection().prepareCall("BEGIN DBMS_APPLICATION_INFO.READ_MODULE(:MOD, :ACT); END;"); //$NON-NLS-1$
					myCallable.registerOutParameter(1, Types.VARCHAR);
					myCallable.registerOutParameter(2, Types.VARCHAR);
					myCallable.execute();
					appinfoValue = myCallable.getString(1);
				} finally {
					if (myCallable != null) {
						try {
							myCallable.close();
						} catch (Exception e) {
						}
					}
					try {
						LockManager.unlock(ctx.getCurrentConnection());
					} catch (Exception e) {
					}
				}
			}
		}
		return appinfoValue;
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		String store = "";
		try {
			String appinfoValue = ""; //$NON-NLS-1$
			boolean appi = (boolean) ctx.getProperty(ScriptRunnerContext.APPINFO);
			String onOrOff = "OFF"; //$NON-NLS-1$
			if (!appi) {
				appinfoValue = SQLPLUS.PRODUCT_NAME;
			} else {
				appinfoValue = newValue(ctx, appinfoValue);
			}

			if ((ctx.getProperty(ScriptRunnerContext.APPINFO) != null) && ((Boolean) ctx.getProperty(ScriptRunnerContext.APPINFO).equals(new Boolean(true)))) {
				onOrOff = "ON"; //$NON-NLS-1$
			}
			store = MessageFormat.format("set appinfo {0}\n", onOrOff);
			store = store + MessageFormat.format("set appinfo \"{0}\"\n", appinfoValue);
		} catch (Exception e) {
			//
		}
		return store;
	}

}