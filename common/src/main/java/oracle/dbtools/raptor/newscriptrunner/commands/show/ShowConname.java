/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;


import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.db.VersionTracker;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.util.Logger;
import oracle.jdbc.OracleConnection;


//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowConname implements IShowCommand {
	private static final String SHOWCONNAME = "con_name"; //$NON-NLS-1$ 
	private static final String[] BOTH = {SHOWCONNAME,"con_id"}; //$NON-NLS-1$ 
    private static final String preAmble = new StringBuffer("DECLARE \n").append( //$NON-NLS-1$
            "CHECKONE VARCHAR2(1000):=NULL; \n").append( //$NON-NLS-1$
           "BEGIN \n").append( //$NON-NLS-1$
           " BEGIN \n").append( //$NON-NLS-1$
           "   CHECKONE:=").toString();
    private static final String postAmble = new StringBuffer(";\n EXCEPTION \n").append( //$NON-NLS-1$
           " WHEN OTHERS THEN \n").append( //$NON-NLS-1$
           "  CHECKONE :=NULL; \n").append( //$NON-NLS-1$
           " END; \n").append( //$NON-NLS-1$
           " :CHECKFORONE:=CHECKONE; \n").append(  //$NON-NLS-1$
           "END;").toString();
    @Override
    public String[] getShowAliases() {
        return BOTH;
    }

    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx.getCurrentConnection()==null)||(ctx.getCurrentConnection() instanceof OracleConnection)) { 
			if ((ScriptUtils.eatOneWord(cmd.getSql()).trim().toUpperCase()).startsWith(SHOWCONNAME.toUpperCase())) {
				return doConNameOrId(conn, ctx, cmd,false);
			} else {
				return doConNameOrId(conn, ctx, cmd,true);
			}
        }
        return false;
    }

    @Override
    public boolean needsDatabase() {
        return true;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }

	/**
	 * support 12c feature show con_name or con_idwhich will error out on: <12.1
	 * or not consolidated database (displays 0 for con_id) otherwise return
	 * plugin name or CDB$ROOT i.e. the result of SELECT SYS_CONTEXT ('USERENV',
	 * 'CON_NAME') CON_NAME FROM DUAL
	 * 
	 * @param shoAndShow
	 *            show con_name
	 * @param conn
	 *            connection - we should use be sure to use CurrentConnection
	 *            for getting con_name
	 * @param ctx
	 *            context - for getting current connection
	 * @param cmd
	 *            - not used
	 * @param amIConId
	 *            use this code for con_id
	 * @return true
	 */
	private boolean doConNameOrId(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd, boolean amIConId) {
		String header="CON_NAME "; //$NON-NLS-1$ 
		if (amIConId) {
		    header="CON_ID "; //$NON-NLS-1$ 
		}
		try {
			String querySql = null;
			ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, null);
			String errorString = null;
			Connection currConn = ctx.getCurrentConnection();
			DBUtil dbUtil = DBUtil.getInstance(currConn);
			if (new Version(this.fetchDbVersionImpl(conn)).compareTo(new Version("12.1")) >= 0) { //$NON-NLS-1$ 
				if (!amIConId) {
					String cdbName=this.noDualUsed(conn,"NVL(SYS_CONTEXT('USERENV','CDB_NAME'),'1')");  //$NON-NLS-1$
					if (cdbName==null||cdbName.equals("1")) { //$NON-NLS-1$  
						// Non Consolidated
						errorString = Messages.getString("SHOWCONNONCONSOLIDATED"); //$NON-NLS-1$1;
						ctx.write(header+errorString); // 1;
					}
					// above or = 12.1
					querySql = "substr(SYS_CONTEXT ('USERENV', 'CON_NAME'),1,30)"; //$NON-NLS-1$ 
				} else {
					// above or = 12.1
					querySql = "substr(SYS_CONTEXT ('USERENV', 'CON_ID'),1,30)"; //$NON-NLS-1$ 
					}
			} else {
				// "SP2-0614: Server version too low for this feature";
				errorString = Messages.getString("SHOWCONVERSIONTOOLOW"); //$NON-NLS-1$ 
				ctx.write(header+errorString);
				// putting english version into SQLCODE incase translation
				// scrambles "SP2-0614:"
				ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, "SP2-0614: Server version too low for this feature"); // NON-NLS-1$
				ctx.errorLog(ctx.getSourceRef(), "SP2-0614: Server version too low for this feature", cmd.getSql());
			}
			if (errorString == null) {
				String result = this.noDualUsed(conn, querySql);
				if (result != null) {
				    ctx.write(header + "\n" + "------------------------------\n" + result+"\n"); //$NON-NLS-1$
				} else {
					ctx.write(header+Messages.getString("SHOWPARAMETERSNORESULTSET")); //$NON-NLS-1$
				}
			}
		} catch (Exception e){ 
            Logger.warn(this.getClass(), e);
		}
		return true;
	}

	//this should be in a utility class somewhere
    public String noDualUsed(Connection conn, String oneOutBind) {
        String retVal=null;
        CallableStatement stmt = null;
        if (conn!=null&&(conn instanceof OracleConnection)) {
            boolean amILocked = LockManager.lock(conn);
            
            try {
                if (amILocked) {
                    stmt = conn.prepareCall(preAmble+oneOutBind+postAmble); //$NON-NLS-1$
                            stmt.registerOutParameter(1, java.sql.Types.VARCHAR); //$NON-NLS-1$
                            stmt.executeUpdate(); //$NON-NLS-1$
                            retVal=stmt.getString(1); //$NON-NLS-1$
                            if (stmt.wasNull()==true) { 
                                retVal=null; 
                            }
                }
            } catch (Exception e) {
                retVal=null;
            } finally {
                if (stmt!=null) {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        Logger.ignore(this.getClass(),e);
                    }
                }
                if (amILocked) {
                    LockManager.unlock(conn);
                }
            }
            
        }
        return retVal;  //$NON-NLS-1$
    }
    protected String fetchDbVersionImpl(Connection conn) throws SQLException, ThreadDeath {
        String result="1.0.0";//$NON-NLS-1$
        DatabaseMetaData dmd = null;
        if ((conn!=null)&&(conn instanceof OracleConnection)) {
        	if (LockManager.lock(conn)) {
        		try {
			        dmd = conn.getMetaData();
			        try {
			            result = dmd.getDatabaseMajorVersion() + "." //$NON-NLS-1$
			                    + dmd.getDatabaseMinorVersion() + ".0"; //$NON-NLS-1$
			        } catch (Throwable t) {
			            if (t instanceof ThreadDeath) {
			                throw (ThreadDeath) t;
			            }
			            // If third parties dont support these
			            // above, then
			            // we need to manage the expectation of a
			            // version
			            result = "1.0.0"; //$NON-NLS-1
			        }
        		} finally {
        			LockManager.unlock(conn);
        		}
        	}
        }
        return result;
    }
}