/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.HashMap;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.IStoreCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.commands.Show;
import oracle.dbtools.raptor.newscriptrunner.commands.StoreRegistry;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

/**
 * 
 * @author <a href=
 *         "mailto:ramesh.uppala@oracle.com@oracle.com?subject=ShowDescribe.java"
 *         > Ramesh Uppala</a>
 * 
 */

// Syntax : DESCRIBE [DEPTH {1|n|ALL}][LINENUM {ON|OFF}][INDENT {ON|OFF}]
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowDescribe implements IShowCommand, IStoreCommand {

	private static final String[] SHOWDESCRIBE = { "desc", //$NON-NLS-1$
			"descr", //$NON-NLS-1$
			"descri", //$NON-NLS-1$
			"describ", //$NON-NLS-1$
			"describe" }; //$NON-NLS-1$

	@Override
	public String[] getShowAliases() {
		return SHOWDESCRIBE;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if ((ctx.getCurrentConnection() == null) || (ctx.getCurrentConnection() instanceof OracleConnection)) {
			return doShowDescribe(conn, ctx, cmd);
		}
		return false;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return true;
	}

	/**
	 * 
	 * @param conn
	 * @param ctx
	 * @param cmd
	 * @return
	 */
	// describe DEPTH 2 LINENUM OFF INDENT ON
	private boolean doShowDescribe(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

		HashMap<String, Object> properties = (HashMap<String, Object>) ctx.getProperty(ScriptRunnerContext.SETDESCRIBE);

		int depth = ((Integer) properties.get(COMMAND_DEPTH)).intValue();
		boolean linenum = ((Boolean) properties.get(COMMAND_LINENUM));
		boolean indent = ((Boolean) properties.get(COMMAND_INDENT));

		String str = MessageFormat.format(Messages.getString("SHOW_DESCRIBE"), 
				                          new Object[] { String.valueOf(depth),
			                                            linenum ? "ON" : "OFF",
			                                            indent ? "ON" : "OFF"}) + Show.m_lineSeparator;
		ctx.write(str);
		return true;
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		return StoreRegistry.getCommand("describe", String.valueOf(ctx.getProperty(ScriptRunnerContext.SETDESCRIBE)));
	}

	private static final String COMMAND_DEPTH = "DEPTH";
	private static final String COMMAND_LINENUM = "LINENUM";
	private static final String COMMAND_INDENT = "INDENT";
}
