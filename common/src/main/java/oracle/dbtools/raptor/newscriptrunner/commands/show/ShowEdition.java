/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.db.ResultSetFormatter;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

/**
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ShowEdition.java"
 *         >Barry McGillin</a>
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowEdition implements IShowCommand {
	private static final String[] SHOWEDITION = { "edition" }; //$NON-NLS-1$ 

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#
	 * getShowAliases()
	 */
	@Override
	public String[] getShowAliases() {
		return SHOWEDITION;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#handleShow
	 * (java.sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if ((ctx.getCurrentConnection() == null) || (ctx.getCurrentConnection() instanceof OracleConnection)) {
			return doShowEdition(conn, ctx, cmd);
		}
		return false;
	}

	private boolean doShowEdition(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		ResultSet rset = null;
		if ((conn != null) && (LockManager.lock(conn))) {
			try {
				String sql = "SELECT sys_context('USERENV', 'CURRENT_EDITION_NAME') edition FROM dual";
				ArrayList<String> localBind = new ArrayList<String>();
				rset = DBUtil.getInstance(ctx.getCurrentConnection()).executeQuery(sql, localBind);
				if (rset != null) {
					ResultSetFormatter rFormat = new ResultSetFormatter(ctx);
					rFormat.setProgressUpdater(ctx.getTaskProgressUpdater());
					rFormat.rset2sqlplusShrinkToSize(rset, ctx.getCurrentConnection(), ctx.getOutputStream());
				}
			} catch (IOException e) {
				ctx.write(e.getLocalizedMessage());
			} catch (SQLException e) {
				ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, e.getMessage());
				ctx.write(e.getLocalizedMessage());
			} finally {
				if (rset != null) {
					DBUtil.closeResultSet(rset);
				}
				LockManager.unlock(conn);
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#
	 * needsDatabase()
	 */
	@Override
	public boolean needsDatabase() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#inShowAll
	 * ()
	 */
	@Override
	public boolean inShowAll() {
		return false;
	}

}
