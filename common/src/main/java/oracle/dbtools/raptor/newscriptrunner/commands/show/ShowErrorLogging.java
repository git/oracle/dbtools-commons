/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.IStoreCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.StoreRegistry;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.util.Logger;

/**
 * @author <a href=
 *         "mailto:shounak.roychowdhury@oracle.com?subject=ShowSQLNumber.java">
 *         Shounak Roychowdhury</a>
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowErrorLogging implements IShowCommand, IStoreCommand {
	private static final String[] SHOWNEWPAGE = { "errorl", "errorlo", "errorlog", "errorlogg", "errorloggi", //$NON-NLS-1$ //$NON-NLS-2$
			"errorloggin", "errorlogging" };

	@Override
	public String[] getShowAliases() {

		return SHOWNEWPAGE;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		try {
			if (ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING) != null) {
				boolean value = (boolean) ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING);
				if (value) {
					String onoff = "ON";
					String table = "";
					String identifier = "";
					String schema = null;
					if (ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_TABLE) != null) {
						if (ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_SCHEMA) != null) {
							schema = (String) ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_SCHEMA);
							//Unless the schema is different to the current one, dont show it.
							if (schema.equalsIgnoreCase(conn.getSchema())) {
								schema = null;
							}

						}
						String displaySchemaTable=(String) ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_DISPLAY_SCHEMATABLE);
						if ((displaySchemaTable!=null)&&(!(displaySchemaTable.equals("")))) { //$NON-NLS-1$
							table=displaySchemaTable;
						} else {
							table = (String) ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_TABLE);
							if (schema != null) {
								table = MessageFormat.format("{0}.{1}", schema, table);
							}
						}
						table = MessageFormat.format("TABLE {0}", table);
					}
					if (ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_IDENTIFIER) != null
							&& ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_IDENTIFIER).toString().length() > 0) {
						identifier = "IDENTIFIER " + ctx.getProperty(ScriptRunnerContext.ERROR_LOGGING_IDENTIFIER);
					}
					ctx.write(MessageFormat.format("errorlogging is {0} {1} {2}\n", onoff, table, identifier).trim()
							+ "\n");
				} else {
					ctx.write("errorlogging is OFF\n");
				}
			}
		} catch (SQLException e) {
			Logger.fine(getClass(), e);
		}
		return true;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return true;
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		return StoreRegistry.getCommand("newpage", String.valueOf(ctx.getProperty(ScriptRunnerContext.NEWPAGE))); //$NON-NLS-1$
	}
}
