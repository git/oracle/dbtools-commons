/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;
import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.commands.IStoreCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.commands.StoreRegistry;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowFeedback implements IShowCommand, IStoreCommand {
	private static final String[] SHOWFEEDBACK = { "feedback", "feedbac", "feedba", "feedb", "feed" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	@Override
	public String[] getShowAliases() {
		return SHOWFEEDBACK;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if ((ctx.getCurrentConnection() == null) || (ctx.getCurrentConnection() instanceof OracleConnection)) {
			return doFeedback(conn, ctx, cmd);
		}
		return false;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return true;
	}

	public boolean doFeedback(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		int feedback = ctx.getFeedback();
		if (feedback == ScriptRunnerContext.FEEDBACK_ON) {
			ctx.write(MessageFormat.format(Messages.getString("FEEDBACKONFORN"), new Object[] { "FEEDBACK ON", 1 })); //$NON-NLS-1$ //$NON-NLS-2$
		} else if (feedback == ScriptRunnerContext.FEEDBACK_OFF) {
			ctx.write("feedback OFF\n"); //$NON-NLS-1$
		} else if (feedback == 0) {
			ctx.setFeedback(ScriptRunnerContext.FEEDBACK_OFF);
			ctx.write("feedback OFF\n"); //$NON-NLS-1$ f
		} else {
			ctx.write(MessageFormat.format(Messages.getString("FEEDBACKONFORN"), new Object[] { "FEEDBACK ON", feedback })); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return true;
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		int feedback = ctx.getFeedback();
		if (feedback == ScriptRunnerContext.FEEDBACK_ON) {
			return StoreRegistry.getCommand("feedback", "1");
		} else if (feedback == ScriptRunnerContext.FEEDBACK_OFF) {
			return StoreRegistry.getCommand("feedback", "OFF");
		} else if (feedback == 0) {
			return StoreRegistry.getCommand("feedback", "OFF");
		} else {
			return StoreRegistry.getCommand("feedback", String.valueOf(feedback));
		}
	}
}