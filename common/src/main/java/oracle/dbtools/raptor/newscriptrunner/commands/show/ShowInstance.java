/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.SQLPLUSCmdFormatter;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.Show;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=ShowInstance.java"
 *         >Barry McGillin</a>
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowInstance extends CommandListener implements IShowCommand, IShowPrefixNameNewline {
	
	String sql = "select "+
"    s.username username, "+
"    i.instance_name instance_name, "+
"    i.host_name host_name, "+
"    to_char(s.sid) sid, "+
"    (select substr(banner, instr(banner, 'Release ')+8,10) from v$version where rownum = 1) version,"+
"    to_char(startup_time, 'YYYYMMDD') startup_day"+
" from "+
"    v$session s,"+
"    v$instance i,"+ 
"    v$process p"+
" where "+
"    s.paddr = p.addr"+
" and "+
"    sid = (select sid from v$mystat where rownum = 1)";

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#
	 * getShowCommand()
	 */
	public String[] getShowAliases() {

		return new String[]{"instance"};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#handle
	 * (java.sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		
		try {
			//Bug 20719344 - SHOW INSTANCE IS NOT WORKING AS EXPECTED
			String username = conn.getMetaData().getUserName();
			if ( "SYS".equalsIgnoreCase(username) || "SYSTEM".equalsIgnoreCase(username)) {
				ctx.write("Instance \"local\""+Show.m_lineSeparator);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBUtil dbUtil = DBUtil.getInstance(conn);
		List<List<?>> results = dbUtil.executeReturnListofList(sql , null);
		try {
			if(conn instanceof OracleConnection) {
			    //list 0: column names list 1 values
			    if (results!=null&&results.size()==2) {
			        for (int i=0;i<results.get(0).size();i++) {
			            ctx.write(results.get(0).get(i).toString()+" "+ results.get(1).get(i).toString()+"\n");
			        } 
			    } else {
					ctx.write("Instance \"local\""+Show.m_lineSeparator);
				}
				/*SQLPLUSCmdFormatter rFormat = new SQLPLUSCmdFormatter(ctx); 
				rFormat.formatResults(ctx.getOutputStream(), rs, sql);*/
			} else {
				ctx.write("Instance \"local\""+Show.m_lineSeparator);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java
	 * .sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// Nothing to do here in the command
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java
	 * .sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql
	 * .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

    @Override
    public boolean needsDatabase() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean inShowAll() {
        // TODO Auto-generated method stub
        return false;
    }

}
