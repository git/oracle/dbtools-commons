/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.io.File;
import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ShowJava.java">Barry McGillin</a> 
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowJava  implements IShowCommand{


	@Override
	public String[] getShowAliases() {
		return new String[] { "java" };
	}
private void print(ScriptRunnerContext ctx,String val) {
	ctx.write(ScriptUtils.wordwrapNotXml(1, (int) ctx.getProperty(ScriptRunnerContext.SETLINESIZE), new StringBuffer(val)).toString());
}
	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String pad = new String(new char[11]).replace('\0', '-');
		ctx.write("Java Detail\n"+pad+"\n");
		print(ctx,"java.home= " + System.getProperty("java.home") +"\n"); //	Installation directory for Java Runtime Environment (JRE)
		print(ctx,"java.vendor= " + System.getProperty("java.vendor") +"\n");  //	JRE vendor name
		print(ctx,"java.vendor.url= " + System.getProperty("java.vendor.url") +"\n");  //	JRE vendor URL
		print(ctx,"java.version= " + System.getProperty("java.version") +"\n");  //	JRE version number
		int ls = (int) ctx.getProperty(ScriptRunnerContext.SETLINESIZE);
		pad = new String(new char[ls]).replace('\0', '-');
		ctx.write(pad+"\n");
		print(ctx,"os.arch= "+System.getProperty("os.arch") +"\n");  //	Operating system architecture
		print(ctx,"os.name= "+System.getProperty("os.name") +"\n");  //	Operating system name
		print(ctx,"os.version= "+System.getProperty("os.version") +"\n");  //	Operating system version
		print(ctx,"path.separator= "+System.getProperty("path.separator") +"\n");  //	Path separator character used in java.class.path
		print(ctx,"file.separator= " + System.getProperty("file.separator") +"\n"); //	Character that separates components of a file path. This is "/" on UNIX and "\" on Windows.
		print(ctx,"line.separator= "+System.getProperty("line.separator") +"\n");  //	Sequence used by operating system to separate lines in text files
		print(ctx,"user.dir= "+System.getProperty("user.dir") +"\n");  //	User working directory
		print(ctx,"user.home= "+System.getProperty("user.home") +"\n");  //	User home directory
		print(ctx,"user.name= "+System.getProperty("user.name") +"\n");  //
		ctx.write(pad+"\n");
		String sqlhome=System.getenv("SQL_HOME");
        print(ctx,"SQL_HOME="+sqlhome+"\n");
        ctx.write(pad+"\n");

		StringBuilder sb = new StringBuilder();
		sb.append("Classpath\n");
		String cp = System.getProperty("java.class.path");
		String[] arr = cp.split(File.pathSeparator);
		for (String s : arr) {
			if (!s.contains(System.getProperty("java.version"))) {
				if (s.contains(sqlhome))
					s="$SQL_HOME" + s.substring(sqlhome.length());
			sb.append(s +"\n");
			}
		}
		
		print(ctx,sb.toString() +"\n"); //	Path used to find directories and JAR archives containing class files. Elements of the class path are separated by a platform-specific character specified in the path.separator property.
		ctx.write(pad+"\n");
ctx.write(System.getenv("SQL_HOME")+"\n");
		return true;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return false;
	}

}
