/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.util.Date;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;

import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleDriver;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowJdbc implements IShowCommand, IShowPrefixNameNewline{
private static final String[] SHOWJDBC = {"jdbc"}; //$NON-NLS-1$ 

    @Override
    public String[] getShowAliases() {
        return SHOWJDBC;
    }

    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if (ctx.getCurrentConnection()!=null) { 
            return doShowJDBC(conn, ctx, cmd);
        }
        return false;
    }

    @Override
    public boolean needsDatabase() {
        return true;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }
    
    private String resourceLookup(String lookupResourceName) {



        try {

            if (lookupResourceName == null || lookupResourceName.length()==0) {
                return "";
            }
            // "/java/lang/String.class"

            // Check if entered data was in java class name format
            if (lookupResourceName.indexOf("/")==-1) {
                lookupResourceName = lookupResourceName.replaceAll("[.]", "/");
                lookupResourceName =  "/" + lookupResourceName + ".class";
            }

            URL url = this.getClass().getResource(lookupResourceName);
            if (url == null) {
                return("Unable to locate resource "+ lookupResourceName);

            }

            String resourceUrl = url.toExternalForm();

            Pattern pattern =
                Pattern.compile("(zip:|jar:file:/)(.*)!/(.*)", Pattern.CASE_INSENSITIVE);

            String jarFilename = null;
            String resourceFilename = null;
            Matcher m = pattern.matcher(resourceUrl);
            if (m.find()) {
                jarFilename = m.group(2);
                resourceFilename = m.group(3);
            } else {
                return "Unable to parse URL: "+ resourceUrl;

            }

            if (!jarFilename.startsWith("C:") ){
              jarFilename = "/"+jarFilename;  // make absolute path on Linux
            }

            File file = new File(jarFilename);
            Long jarSize=null;
            Date jarDate=null;
            Long resourceSize=null;
            Date resourceDate=null;
            if (file.exists() && file.isFile()) {

                jarSize = file.length();
                jarDate = new Date(file.lastModified());

                try {
                    JarFile jarFile = new JarFile(file, false);
                    ZipEntry entry = jarFile.getEntry(resourceFilename);
                    resourceSize = entry.getSize();
                    resourceDate = new Date(entry.getTime());
                } catch (Throwable e) {
                    return ("Unable to open JAR" + jarFilename + "   "+resourceUrl +"\n"+e.getMessage());

                }

               return "\nresource: "+resourceFilename+"\njar: "+jarFilename + "  \nJarSize: " +jarSize+"  \nJarDate: " +jarDate.toString()+"  \nresourceSize: " +resourceSize+"  \nresourceDate: " +resourceDate.toString()+"\n";


            } else {
                return("Unable to load jar:" + jarFilename+ "  \nUrl: " +resourceUrl);

            }
        } catch (Exception e){
            return e.getMessage();
        }


    }
	/**
	 * 
	 * @param conn
	 * @param ctx
	 * @param cmd
	 * @return
	 */
	public boolean doShowJDBC(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		StringBuffer sb = new StringBuffer();
		try {
			if (LockManager.lock(conn)) {
				try {
					DatabaseMetaData dbmd = conn.getMetaData();
					sb.append("-- Database Info --\n");
					sb.append("Database Product Name: " + dbmd.getDatabaseProductName() + "\n");
					sb.append("Database Product Version: " + dbmd.getDatabaseProductVersion() + "\n");
					sb.append("Database Major Version: " + dbmd.getDatabaseMajorVersion() + "\n");
					sb.append("Database Minor Version: " + dbmd.getDatabaseMinorVersion() + "\n");
					sb.append("-- Driver Info --\n");
					sb.append("Driver Name: " + dbmd.getDriverName() + "\n");
					sb.append("Driver Version: " + dbmd.getDriverVersion() + "\n");
					sb.append("Driver Major Version: " + dbmd.getDriverMajorVersion() + "\n");
					sb.append("Driver Minor Version: " + dbmd.getDriverMinorVersion() + "\n");
					sb.append("Driver URL: " + dbmd.getURL() + "\n");
					sb.append("Driver Location: " + resourceLookup("oracle.jdbc.OracleDriver") + "\n");
				} finally {
					LockManager.unlock(conn);
				}
			}

		} catch (Exception e) {
			sb.append("\nUnable to gather information\n");
		}
		ctx.write(sb.toString());
		return true;
	}
}