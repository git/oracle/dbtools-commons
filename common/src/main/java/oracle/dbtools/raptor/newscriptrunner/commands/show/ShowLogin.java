/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;

import oracle.dbtools.common.utils.FileUtils;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLPLUS;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.utils.TCPTNSEntry;
import oracle.dbtools.raptor.utils.TNSHelper;
import oracle.dbtools.util.Logger;
import oracle.jdbc.pool.OracleDataSource;

/**
 * @author <a
 *         href="mailto:turloch.otierney@oracle.com@oracle.com?subject=ShowLogin.java"
 *         >Turloch O'Tierney</a>
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowLogin extends CommandListener implements IShowCommand, IShowPrefixNameNewline {
	private static String[] prefixes={"http", "https", "ftp", "sftp"};   //$NON-NLS-1$  //$NON-NLS-2$   //$NON-NLS-3$ 
	private static final String[] SHOWLOGIN = { "login", "glogin" };  //$NON-NLS-1$  //$NON-NLS-2$

	@Override
	public String[] getShowAliases() {
		return SHOWLOGIN;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		return handleShowLogin(conn, ctx, cmd);
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return false;
	}

	private boolean handleShowLogin(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		
		String overRide= getGUIOverRide(ctx);
		if (overRide != null) {
			ctx.write(Messages.getString("OVERRIDE"));   //$NON-NLS-1$
			ctx.write(overRide+"\n\n");  //$NON-NLS-1$
		}
		
		if (isCannotReadFileSystem(ctx)) {
			ctx.write(Messages.getString("CANNOTREADFILESYSTEM"));   //$NON-NLS-1$
			ctx.write("\n\n");  //$NON-NLS-1$
		}
		ArrayList<String> entries = getLocations(ctx, null,false);
		
		ctx.write(Messages.getString("GLOGINDIR"));  //$NON-NLS-1$
		for (String entry: entries) {
			if (entry != null) {
				ctx.write(entry+"\n");  //$NON-NLS-1$
			}
		}
		entries = getLocations(ctx, "glogin.sql", false);  //$NON-NLS-1$
		if (entries.size()!=0) {
			ctx.write("\nglogin.sql \n---------------------\n"+entries.get(0)+"\n");  //$NON-NLS-1$  //$NON-NLS-2$
		}
		
		entries = getLocations(ctx, null,true);
		ctx.write(Messages.getString("LOGINDIR"));  //$NON-NLS-1$
		for (String entry: entries) {
			if (entry != null) {
				ctx.write(entry+"\n");  //$NON-NLS-1$
			}
		}

		
		entries = getLocations(ctx, "login.sql", true);  //$NON-NLS-1$
		if (entries.size()!=0) {
			ctx.write("\nlogin.sql \n---------------------\n"+entries.get(0)+"\n"); //$NON-NLS-1$  //$NON-NLS-2$
		}
		
		return true;
	}
	public static String getGUIOverRide(ScriptRunnerContext ctx) {
		Boolean isGUIOverRide=(Boolean) ctx.getProperty(ScriptRunnerContext.DBCONFIG_GLOGIN);
		String gUIOverRide=(String) ctx.getProperty(ScriptRunnerContext.DBCONFIG_GLOGIN_FILE);
		if (((isGUIOverRide==null)||
				(isGUIOverRide.equals(Boolean.TRUE)))&&
						(gUIOverRide!=null&&
						(!(gUIOverRide.equals(""))))) {  //$NON-NLS-1$
			return gUIOverRide;
		}
		return null;
	}
	static boolean isCannotReadFileSystem(ScriptRunnerContext ctx) {
	  return ctx.getRestrictedLevel().getLevel() >=2;
	}
	public static String firstOrNull(ArrayList <String> aL) {
		String retVal=null;
		if (aL.size()>0) {
			retVal=aL.get(0);
		}
		return retVal;
	}
	/**
	 * 
	 * @param ctx
	 * @param match
	 * @param login true for login.sql false for glogin.sql
	 * @return
	 */
	public static ArrayList<String> getLocations(ScriptRunnerContext ctx,String match, boolean login) {
		ArrayList<String> retVal=new ArrayList<String>();
		
		//assume check SQL_HOME for glogin or login
		if (match!=null&&isCannotReadFileSystem(ctx)) {
			return retVal;
		}
		String overRide=getGUIOverRide(ctx);
		if ((match!=null)&&(match.equals("glogin.sql"))&&(overRide!=null)){  //$NON-NLS-1$
			retVal.add(overRide);
			return retVal;
		}
		if ((match!=null)&&(login)&&(overRide!=null)){  //$NON-NLS-1$
			return retVal;
		}
		String SQLH=System.getenv("SQL_HOME"); //$NON-NLS-1$
		if (SQLH!=null) {
			retVal.add(addDirEnding(SQLH));
		}
		String OH=/*System.getenv("ORACLE_HOME")*/ (String) ctx.getProperty(ScriptRunnerContext.ORACLE_HOME_MAIN_THREAD);
		if ((OH!=null)&&(!(OH.equals("")))&&(login==false)) {  //$NON-NLS-1$
			if (!(OH.endsWith(File.separator))) {
				OH+=File.separator;
			} 
			OH+="sqlplus"+File.separator+"admin";  //$NON-NLS-1$  //$NON-NLS-2$
			retVal.add(addDirEnding(OH));
		}
		ArrayList<String> al= null;
		if (login==true) {
			al=ShowLogin.getSQLPath(ctx); 
		}
		if (al!=null) {
			for (String sp:al) {
				retVal.add(addDirEnding(sp));
			}
		}

		if (match!=null) {
			for (String dir:retVal) {
				if (dir!=null&&new File(dir+match).canRead()) {
					ArrayList<String>retOne=new ArrayList<String>();
					retOne.add(dir+match);
					return retOne;
				}
			}
			return new ArrayList<String>();
		}
		return retVal;
	}
	public static ArrayList<String> getSQLPath(ScriptRunnerContext ctx) {

		ArrayList<String>retVal=new ArrayList<String>(); 
		String [] sources = new String[] {
			(String) ctx.getProperty(ScriptRunnerContext.CDPATH),
			(String) ctx.getProperty(ScriptRunnerContext.DBCONFIG_DEFAULT_PATH),
		    (String) ctx.getProperty(ScriptRunnerContext.SQLPATH_PROVIDER_MAIN_THREAD)
		};
	// , "." , iff in sqldev otherwise in sasql it is in provider.
		for (String sA: sources) {
			String lastPrefix=null;
			if (sA!=null) {
				for (String s: sA.split(File.pathSeparator)) {
					if (File.pathSeparator.equals(":")) {  //$NON-NLS-1$
						if (lastPrefix==null) {
							for (String prefix: prefixes) {
								if (s.equals(prefix)) {  //$NON-NLS-1$
									lastPrefix=prefix;
									break;
								}
							}
							if (lastPrefix!=null) {
								continue;
							}
						}
					}
					if (lastPrefix!=null) {
						s=lastPrefix+":"+s; //$NON-NLS-1$
						lastPrefix=null;
					}
					retVal.add(s);
				}
			}
		}
		return retVal;
	}
	
	public static String addDirEnding(String in) {
		String retVal=in;
		String add=File.separator;
		if (retVal !=null) {
			if (retVal.equals("")) {   //$NON-NLS-1$
				//do not want to end up searching /login.sql /glogin.sql
				return null;
			}
			if ((retVal.startsWith("http:")||retVal.startsWith("https:")||retVal.startsWith("ftp:"))) {  //$NON-NLS-1$   //$NON-NLS-2$  //$NON-NLS-3$
				return null;
				//could handle this - then have to sort out at run login/glogin level.
				/*int lastSlash=retVal.lastIndexOf("/");
				int lastbSlash=retVal.lastIndexOf("\\");
				if (lastbSlash>lastSlash) {
					add="\\";
				}*/
			}
			if (!(retVal.endsWith(add))) {
				retVal +=add;
			}
		} 
		return retVal;
	}
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		return false;
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

}
