/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.StringTokenizer;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowObsolete implements IShowCommand, IShowAllNeither{
	private static final String[] SHOWOBSOLETE = {
				"sqlc", //$NON-NLS-1$
				"sqlco", //$NON-NLS-1$
				"sqlcon", //$NON-NLS-1$
				"sqlcont", //$NON-NLS-1$
				"sqlconti", //$NON-NLS-1$
				"sqlcontin", //$NON-NLS-1$
				"sqlcontinu", //$NON-NLS-1$
				"sqlcontinue", //$NON-NLS-1$
				"flu", //$NON-NLS-1$
				"flus", //$NON-NLS-1$
				"flush", //$NON-NLS-1$
				"doc", //$NON-NLS-1$
				"docu", //$NON-NLS-1$
				"docum", //$NON-NLS-1$
				"docume", //$NON-NLS-1$
				"documen", //$NON-NLS-1$
				"document", //$NON-NLS-1$
				"maxd", //$NON-NLS-1$
				"maxda", //$NON-NLS-1$
				"maxdat", //$NON-NLS-1$
				"maxdata", //$NON-NLS-1$
				"sqlpre", //$NON-NLS-1$
				"sqlpref", //$NON-NLS-1$
				"sqlprefi", //$NON-NLS-1$
				"sqlprefix", //$NON-NLS-1$
				"label", //$NON-NLS-1$
				"_ociclientversion", //$NON-NLS-1$
				"cmds", //$NON-NLS-1$
				"cmdse", //$NON-NLS-1$
				"cmdsep", //$NON-NLS-1$
				"tab", //$NON-NLS-1$
				"escchar", //$NON-NLS-1$
				"desc", //$NON-NLS-1$
				"descr", //$NON-NLS-1$
				"describe", //$NON-NLS-1$
				};
	//(shoAndShow.startsWith(SHOWSQLPLUSC)||shoAndShow.startsWith(SHOWSQLC)||shoAndShow.startsWith(SHOWFLUSH)||shoAndShow.startsWith(SHOWDOCUMENT)||shoAndShow.startsWith(SHOWMAXDATA)||shoAndShow.startsWith(SHOWSQLPREFIX)||shoAndShow.startsWith(SHOWLABEL)||shoAndShow.startsWith(SHOWCLOSECURSOR)||shoAndShow.startsWith(SHOWCOMPATABILITY)||shoAndShow.startsWith(UND_OCI_CLI_VER)||shoAndShow.startsWith(SHOWCMDS))

    @Override
    public String[] getShowAliases() {
        return SHOWOBSOLETE;
    }

    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx.getCurrentConnection()==null)||(ctx.getCurrentConnection() instanceof OracleConnection)) { 
            return doObsolete(conn, ctx, cmd);
        }
        return false;
    }

    @Override
    public boolean needsDatabase() {
        return false;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }
	
	public static boolean doObsolete(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String shoAndShow = cmd.getSql().trim().toLowerCase().replaceAll("^show?\\s+", "");
		StringTokenizer st = new StringTokenizer(shoAndShow);
		String token = "";
		if (st.hasMoreTokens()) {
			token = st.nextToken();
			ctx.write(MessageFormat.format("show {0} is obsolete.\n", new Object[] { token }));
		}
		return true;
	}

}