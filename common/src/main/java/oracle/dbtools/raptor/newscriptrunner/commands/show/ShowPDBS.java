/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.ConnectionIdentifier;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.db.SQLPLUSCmdFormatter;
import oracle.dbtools.db.VersionTracker;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ShowPDBS.java">Barry McGillin</a> 
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowPDBS extends CommandListener implements IShowCommand, IShowPrefixNameNewline, IShowNoRows {

	String sql = "SELECT CON_ID, NAME as \"CON_NAME\", OPEN_MODE as \"OPEN MODE\", RESTRICTED FROM V$PDBS";

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#getShowCommand()
	 */
	public String[] getShowAliases() {
		return new String[]{"PDBS"};
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#handle(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		ConnectionIdentifier id = DefaultConnectionIdentifier.createIdentifier(null, conn);
		 DBUtil dbUtil = DBUtil.getInstance(id);
		 ResultSet rs = null;	
		 boolean amILocked=false;
	     try {
	         amILocked = LockManager.lock(ctx.getBaseConnection());
	         if (amILocked) {
    		     if (VersionTracker.checkVersion(id, new Version("22"), new Version("12.0"))) {  //$NON-NLS-1$  //$NON-NLS-2$
                     rs = dbUtil.executeOracleQuery(sql, null);
                 }
    			 if(rs!=null&&conn instanceof OracleConnection) {
    			     if (rs.isBeforeFirst()) {
    			         String optionalHeader=(String) ctx.getProperty(ScriptRunnerContext.OPTIONAL_SHOW_HEADER);
    			         if (optionalHeader!=null&&(!optionalHeader.equals(""))) { //$NON-NLS-1$
    			             ctx.write(optionalHeader);
    			         }
        		    	 addColumnFormatForShowPDBs(conn, ctx);
    			         SQLPLUSCmdFormatter rFormat = new SQLPLUSCmdFormatter(ctx);
    			         rFormat.formatResults(ctx.getOutputStream(), rs, sql);
    			 		 clearColumnFormatForShowPDBs(ctx);
    			     }
    			 }
    			 else {
    				 ctx.write(Messages.getString("NO_SHOW_PDBS_COMMAND")); //$NON-NLS-1$
    				 ctx.errorLog(ctx.getSourceRef(), Messages.getString("NO_SHOW_PDBS_COMMAND"), cmd.getSql());
    			 }
	         }
		 } catch (Exception e) {
             Logger.getLogger(this.getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		 } finally {
		     try {
		         if (rs!=null) {
		             DBUtil.closeResultSet(rs);
		         }
		     } catch (Exception e) {
		         Logger.getLogger(this.getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		     }
		     if (amILocked) {
	                try {
	                	LockManager.unlock(ctx.getBaseConnection());
	                } catch (Exception e) {
	                }
		     }
		 }
		 return false;
	}
	

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// Done need to do anything here
		return false;
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

    @Override
    public boolean needsDatabase() {
        return true;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }
    
    private void addColumnFormatForShowPDBs(Connection conn, ScriptRunnerContext ctx) {	   	
    	HashMap<String, ArrayList<String>> storedColumns = ctx.getStoredFormatCmds();
    	/* Calculations for SQLPlus format of column width
    	 * LineSize | CON_ID  	| CON_NAME 	| OPEN MODE | RESTRICTED| 
    	 * 20       | 10     	| 30   		| 10   		|	10		| 
    	 * 40       | 10     	| 30   		| 10   		|	10		|
    	 * 80       | 10    	| 30   		| 10   		|	10		| 
    	 */
    	ArrayList<String> cmdlist = new ArrayList<String>();
    	cmdlist.add("format a10");
    	storedColumns.put("con_id", cmdlist);
    	cmdlist = new ArrayList<String>();
    	cmdlist.add("format a30");
    	storedColumns.put("con_name", cmdlist);
    	cmdlist = new ArrayList<String>();
    	cmdlist.add("format a10");
    	storedColumns.put("open mode", cmdlist);
    	cmdlist = new ArrayList<String>();
    	cmdlist.add("format a10");
    	storedColumns.put("restricted", cmdlist); 
    }
    
    private void clearColumnFormatForShowPDBs(ScriptRunnerContext ctx) {
    	// remove col commands when done.
    	HashMap<String, ArrayList<String>> storedColumns = ctx.getStoredFormatCmds();
    	storedColumns.remove("con_id");
    	storedColumns.remove("con_name");
    	storedColumns.remove("open mode"); 
    	storedColumns.remove("restricted"); 
    }
}
