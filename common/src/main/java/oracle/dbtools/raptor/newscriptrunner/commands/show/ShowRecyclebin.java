/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.HashMap;

import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.db.ResultSetFormatter;
import oracle.dbtools.db.SQLPLUSCmdFormatter;
import oracle.dbtools.db.VersionTracker;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowRecyclebin implements IShowCommand, IParsedShow{
	private static final String[] SHOWRECYCLE = {
		"recyc", //$NON-NLS-1$
		"recycl", //$NON-NLS-1$
		"recycle", //$NON-NLS-1$
		"recycleb", //$NON-NLS-1$
		"recyclebi", //$NON-NLS-1$
		"recyclebin"} ;//$NON-NLS-1$
		
    @Override
    public String[] getShowAliases() {
        return SHOWRECYCLE;
    }



    @Override
    public String handleShowRemainder(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx.getCurrentConnection()==null)||(ctx.getCurrentConnection() instanceof OracleConnection)) { 
            return doShowRecyclebin(conn, ctx, cmd);
        }
        return null;
    }

    @Override
    public boolean needsDatabase() {
        return true;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }

	private String doShowRecyclebin(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		boolean quoted=false;
		// eat show recyclebin word
		String shoAndShow = ScriptUtils.eatOneWord(ScriptUtils.eatOneWord(cmd.getSql()).trim()).trim();
		// eat shoAnsShow up to first not " encased whitespace or end
		boolean noexit = true;
		int charAt = -1;
		int end = shoAndShow.length();
		boolean indquote = false;
		boolean insquote = false;
		while (noexit) {
			charAt++;
			if (charAt == end) {
				noexit = false;
				break;
			}
			String next = shoAndShow.substring(charAt, charAt + 1);
			if (next.equals("\"")) { //$NON-NLS-1$
				if (insquote == false) {
					if (indquote == false) {
						indquote = true;
					} else {
						indquote = false;
					}
					;
				}
				continue;
			}
			if (next.equals("'")) { //$NON-NLS-1$
				if (indquote == false) {
					if (insquote == false) {
						insquote = true;
					} else {
						insquote = false;
					}
				}
				continue;
			}
			if (next.matches("\\s") && (indquote == false) && (insquote == false)) { //$NON-NLS-1$
				noexit = false;
				break;
			}
		}
		String arg = shoAndShow.substring(0, charAt).trim();
		String arg_orig=arg;
		if ((arg.startsWith("\"") && (arg.endsWith("\"")) && (arg.length() > 1))) { //$NON-NLS-1$  //$NON-NLS-2$
			quoted=true;
			arg = arg.substring(1, arg.length() - 1);
		} else if ((arg.startsWith("'") && (arg.endsWith("'")) && (arg.length() > 1))) { //$NON-NLS-1$  //$NON-NLS-2$
			quoted=true;
		}
		if (charAt == end) {
			shoAndShow = ""; //$NON-NLS-1$
		} else {
			shoAndShow = shoAndShow.substring(charAt).trim();
		}
		if ((VersionTracker.getDbVersion(DefaultConnectionIdentifier.createIdentifier(conn)).compareTo(new Version("10.0")) < 0)) { //$NON-NLS-1$
			ctx.write(Messages.getString("SERVER_VERSION_TOO_LOW")); //$NON-NLS-1$
			return shoAndShow;
		}
		// getlock
		boolean amILocked = false;
		PreparedStatement s = null;
		ResultSet rs = null;
		try {
			amILocked = LockManager.lock(ctx.getBaseConnection());
			if (amILocked) {
				boolean tryNonDba = false;
				String querySql = null;
				String extraWhere = " original_name = :INPUT"; //$NON-NLS-1$ 
				String dbaQuery = "select original_name \"ORIGINAL NAME\", object_name \"RECYCLEBIN NAME\", type \"OBJECT TYPE\", droptime  \"DROP TIME\"  " //$NON-NLS-1$ 
						+ " from dba_recyclebin r where owner = USER"; //$NON-NLS-1$ 
				String nonDbaQuery = "select original_name \"ORIGINAL NAME\", object_name \"RECYCLEBIN NAME\", type \"OBJECT TYPE\", droptime  \"DROP TIME\" " //$NON-NLS-1$ 
						+ " from recyclebin r"; //$NON-NLS-1$
				
				try {
					if (!(arg.equals(""))) { //$NON-NLS-1$ 
						querySql = dbaQuery + " and " + extraWhere; //$NON-NLS-1$
						s = conn.prepareStatement(querySql);
						HashMap<String, String> binds = new HashMap<String, String>();
						if ( quoted )
							binds.put("INPUT", arg); //$NON-NLS-1$
						else 
							binds.put("INPUT", arg.toUpperCase()); //$NON-NLS-1$
						DBUtil.bind(s, binds);
					} else {
						querySql = dbaQuery;
						s = conn.prepareStatement(querySql);
					}
					rs = s.executeQuery();
				} catch (SQLException e) {
					tryNonDba = true;
					// ResultSet will always be null here, but we should clean up the Statement
					if (s != null) {
						try {
							s.close();
						} catch (SQLException e2) {
						}
					}
				}
				if (tryNonDba) {
					if (!(arg.equals(""))) { //$NON-NLS-1$ 
						querySql = nonDbaQuery + " where " + extraWhere; //$NON-NLS-1$
						s = conn.prepareStatement(querySql);
						s.setString(1, arg);
					} else {
						querySql = nonDbaQuery;
						s = conn.prepareStatement(querySql);
					}
	                if (rs != null) {
	                    try {
	                        rs.close();
	                    } catch (SQLException e2) {
	                    }
	                }
					rs = s.executeQuery();
				}

				if (rs.next()) { // we have a row
					if (rs != null) {
						try {
							rs.close();
						} catch (SQLException e2) {
						}
					}
					if (s != null) {
						try {
							s.close();
						} catch (SQLException e2) {
						}
					}
					if (!(arg.equals(""))) { //$NON-NLS-1$ 
						s = conn.prepareStatement(querySql);
						if ( quoted )
							s.setString(1, arg);
						else 
						    s.setString(1, arg.toUpperCase());
					} else {
						s = conn.prepareStatement(querySql);
					}
					rs = s.executeQuery();

					ResultSetFormatter rFormat = new ResultSetFormatter(ctx);
					rFormat.setProgressUpdater(ctx.getTaskProgressUpdater());
					int cnt = rFormat.rset2sqlplusShrinkToSize(rs, conn, ctx.getOutputStream());

				} else {
					if (!ctx.isSQLPlusClassic())
					ctx.write(MessageFormat.format(Messages.getString("ShowRecyclebin.RECYCLEBIN_INVALID_KEY"),arg_orig)); //$NON-NLS-1$
				}
			}
		} catch (IOException e) {
			ctx.write(e.getLocalizedMessage());
		} catch (SQLException e) {
			ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, e.getMessage());
			ctx.write(e.getLocalizedMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
			}
			if (s != null) {
				try {
					s.close();
				} catch (SQLException e) {
				}
			}
			if (amILocked) {
				LockManager.unlock(ctx.getBaseConnection());
			}
		}
		return shoAndShow;
	}





    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        // TODO Auto-generated method stub
        return false;
    }

}