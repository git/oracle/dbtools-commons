/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.io.IOException;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.commands.LoadCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.commands.SetColInvisble;
import oracle.dbtools.raptor.newscriptrunner.commands.ShowErrors;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.CopyNumberToFloat;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowAutoRecovery;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowCmdSep;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowCopyTypeCheck;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowEndBufT;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowEndBufToken;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowErrorLogging;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowFlagger;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowLobOffSet;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowLongChunkSize;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowNewPage;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowRecSep;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowRecSepChar;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowRepfooter;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowRepheader;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowSQLNumber;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowSecuredCol;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowUnderLine;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.ShowXMLOptimization;
import oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported.Show_ShowRowShip;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.newscriptrunner.restricted.RunnerRestrictedLevel;
/**
 * @author klrice
 *
 *  This is a simple registry to handle all the show ABC commands.
 *  
 */
public class ShowRegistry {

  private static ConcurrentHashMap<String, IShowCommand> s_registry = new ConcurrentHashMap<String,IShowCommand>();  //doneill: is this thead safe
  private static boolean _init = true;
  private static HashSet<String> showAll= new HashSet<String>();
  private static HashSet<String> showAllExtended= new HashSet<String>();
  private static HashSet<String> showAllExtendedMultiline= new HashSet<String>();
  private static ConcurrentHashMap<String, String> toFirst= new ConcurrentHashMap<String, String>();
  private static boolean showAllDirty=true;
  private static String showAllPrevious="";  //$NON-NLS-1$
  private static String showAllExtendedPrevious="";  //$NON-NLS-1$
  private static String showAllExtendedMultilinePrevious="";  //$NON-NLS-1$
  private static String first40="================================================================================".substring(0,40);  //$NON-NLS-1$
          //"------------------------------------------------------------------------".substring(0,40);  //$NON-NLS-1$

  //need to add con_id as it is shoved in with con_name
  /**
   * @param conn
   * @param ctx
   * @param cmd
   * @return
   */
  public static boolean processShowCommand(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd){
    init();
    String[] parts=null;
    boolean onceTrue=false;
    String all=cmd.getSql();
    all=ScriptUtils.eatOneWord(all);//show
    if (all!=null){
        parts=all.trim().split("\\s+"); //$NON-NLS-1$);
      }
    if (parts[0].toLowerCase().equals("err")||parts[0].toLowerCase().equals("erro")||parts[0].toLowerCase().equals("error")||parts[0].toLowerCase().equals("errors")) { //$NON-NLS-1$);
        return false;
        //handled as show command - note bug show errors does not work in compound 
        //show currently (even before breakup of monolithic show.java)
    }
    boolean stopIt=false;
    boolean showAllExtends=false;
    if (all.trim().endsWith("DISPLAYNAME")) {  //$NON-NLS-1$
        showAllExtends=true;
        all=all.substring(0, all.lastIndexOf("DISPLAYNAME"));  //$NON-NLS-1$
    }
    while (!stopIt) {
      boolean alreadyEaten=false;
      if (all!=null){
          parts=all.trim().split("\\s+"); //$NON-NLS-1$);
        } 
      if ((parts!=null)&&( parts.length > 0)&&(!parts[0].equals(""))){ //$NON-NLS-1$ //$NON-NLS-2$
        IShowCommand show = null;
        if ((parts[0].toUpperCase().equals("ALL")||(parts[0].toUpperCase().equals("ALL+")))) { //$NON-NLS-1$ //$NON-NLS-2$
            String additional=""; //$NON-NLS-1$
            synchronized(showAll) {
                if (showAllDirty) {
                    StringBuffer showAll=new StringBuffer(""); //$NON-NLS-1$);
                    for (String inAll:showAllStrings()) {
                        showAll.append(inAll);
                        showAll.append(" "); //$NON-NLS-1$);
                    }
                    showAllPrevious=showAll.toString();
                    StringBuffer showAllExtended=new StringBuffer(""); //$NON-NLS-1$);
                    for (String inAll:showAllExtendedStrings()) {
                        showAllExtended.append(inAll);
                        showAllExtended.append(" "); //$NON-NLS-1$);
                    }
                    showAllExtendedPrevious=showAllExtended.toString();
                    StringBuffer showAllExtendedMultiline=new StringBuffer(""); //$NON-NLS-1$
                    for (String inAll:showAllExtendedMultilineStrings()) {
                        showAllExtendedMultiline.append(inAll);
                        showAllExtendedMultiline.append(" "); //$NON-NLS-1$
                    }
                    showAllExtendedMultilinePrevious=showAllExtendedMultiline.toString();
                }
                showAllDirty=false;
            }
            additional=ScriptUtils.eatOneWord(all);
            if (parts[0].toUpperCase().equals("ALL+")) {  //$NON-NLS-1$
                ctx.write("Show All:\n"+first40+"\n");  //$NON-NLS-1$  //$NON-NLS-2$
            }
            cmd.setSql("show "+showAllPrevious);  //$NON-NLS-1$
            if(processShowCommand(conn, ctx, cmd)) {
                onceTrue=true;
            }
            if (parts[0].toUpperCase().equals("ALL+")) {  //$NON-NLS-1$
                ctx.write("\nShow All+ "+Messages.getString("SINGLE_LINE")+":\n"+first40+"\n"); //$NON-NLS-1$  //$NON-NLS-2$ //$NON-NLS-3$  //$NON-NLS-4$
                cmd.setSql("show "+showAllExtendedPrevious+" DISPLAYNAME"); //$NON-NLS-1$  //$NON-NLS-2$
                if(processShowCommand(conn, ctx, cmd)) {
                    onceTrue=true;
                }
                ctx.write("\nShow All+ "+Messages.getString("MULTI_LINE")+":\n"+first40+"\n"); //$NON-NLS-1$  //$NON-NLS-2$ //$NON-NLS-3$  //$NON-NLS-4$
                cmd.setSql("show "+showAllExtendedMultilinePrevious+" DISPLAYNAME"); //$NON-NLS-1$  //$NON-NLS-2$
                if(processShowCommand(conn, ctx, cmd)) {
                    onceTrue=true;
                }
                ctx.write("\n"); //$NON-NLS-1$
            }
            all=additional;
            continue;
        }
        String first=toFirst.get(parts[0].toUpperCase());
        if (first!=null) {
          try{
            show= getShowCommand(first.toUpperCase(),ctx.getRestrictedLevel());
          } catch(RestrictedCommandException rce){
            //if a show all, then just skip. If its show command, the return restricted message#
            String[] origParts = cmd.getSQLOrig().split("\\s+");
            if(!(origParts.length>=2 && origParts[1].equalsIgnoreCase("ALL"))){
              ctx.write(ScriptRunnerDbArb.format(ScriptRunnerDbArb.RESTRICTED_COMMAND,"SHOW "+first.toUpperCase()));//$NON-NLS-1$
            }
            all=ScriptUtils.eatOneWord(all);
            continue;
          }
        }
        cmd.setSql("show "+all); //$NON-NLS-1$
        if ( show != null ){
          if ((showAllExtends)&&((show instanceof ShowUFI)||(show instanceof ShowSqldev)||(show instanceof ShowErrors)||
                  (show instanceof ShowSqldev2)||(show instanceof ShowTables /*Showtables is blank... */)||(show instanceof LoadCommand))) {
              all=ScriptUtils.eatOneWord(all);
              continue;//showufi etc skip in show all.
          }
          if (show instanceof IEndOfShowCommand) {
              
             if ((showAllExtends)) {
                 all=ScriptUtils.eatOneWord(all);
                 continue;//spparameters parameters etc skip in show all.
             }
             stopIt=true;
          }
          if (showAllExtends) {
              //ctx.write("DEBUG ************"+show.getShowAliases()[0]+"*******************\n");
              //some require to say what you are showing as you are in a show all rather than a specific show
              if (show instanceof IShowPrefixName) {
                  ctx.write(parts[0]+" ");//assumes the show command prints a new line later.  //$NON-NLS-1$
              } else {
                  if (show instanceof IShowPrefixNameNewline) {
                      if (show instanceof ShowSga) {
                          //do sga prints whitespace remove the \n
                          ctx.write("\n"+parts[0]+":\n"+first40);//assumes the show command prints a new line later.  //$NON-NLS-1$ //$NON-NLS-2$
                      } else {
                          String header=/*todo ansify*/ "\n"+parts[0]+":\n"+first40+"\n";//assumes the show command prints a new line later.  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                          if (show instanceof IShowNoRows) {
                              ctx.putProperty(ScriptRunnerContext.OPTIONAL_SHOW_HEADER,header);
                          } else {
                              ctx.write(header);//assumes the show command prints a new line later.  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                          }
                      }
                      
                  }
              }
          }

          if (show.needsDatabase()&&(ctx!=null) && (ctx.getProperty(ScriptRunnerContext.NOLOG)!=null) && 
              (((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG)).equals(Boolean.TRUE)))) {
            ctx.write(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NOT_CONNECTED)+"\n"); //$NON-NLS-1$);
            onceTrue=true;
            alreadyEaten=false;
          } else {
            //some show require name, some show require name:\n
              
            if (show instanceof IParsedShow) {
                String rest=((IParsedShow)show).handleShowRemainder(conn,ctx,cmd);
                if (rest!=null){
                    all=rest;
                    alreadyEaten=true;
                    onceTrue=true;
                }
            } else
            if (show.handleShow(conn,ctx,cmd)) {
              alreadyEaten=false;
              onceTrue=true;
            }
          }
        } else {
            //sqlco is universally obsolete
            if (all.toLowerCase().startsWith("sqlco")) { //$NON-NLS-1$
                if(ShowObsolete.doObsolete(conn,ctx,cmd)) {
                    onceTrue=true;
                }
            } else { 
            	String fail = parts[0];
            	if (fail.length()>10) {
            		fail=fail.substring(0, 10)+"...";
                ctx.write(MessageFormat.format(Messages.getString("SHOWUNKNOWNOPTION_BEGINNING"), fail)); //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
            	} else {
            		ctx.write(MessageFormat.format(Messages.getString("SHOWUNKNOWNOPTION"), fail)); //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
            	}
                cmd.setFail();
                try {
                    ctx.getOutputStream().flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        /* do not want optional show header to be remembered */
        ctx.putProperty(ScriptRunnerContext.OPTIONAL_SHOW_HEADER,null);
        if (!alreadyEaten) {
          all=ScriptUtils.eatOneWord(all);
        }
      } else {
        break;
      }
    }
    return onceTrue;
  }
  
  /**
   * 
   * @param showCommandName
   * @param runnerRestrictedLevel
   * @return
   */
  private static IShowCommand getShowCommand(final String showCommandName,final RunnerRestrictedLevel.Level runnerRestrictedLevel) throws RestrictedCommandException {
   IShowCommand showCommand= s_registry.get(showCommandName);
   if(runnerRestrictedLevel == RunnerRestrictedLevel.Level.NONE){ //do not filter out any listeners
     return showCommand;
   } 
   Restricted.Level level = Restricted.Level.R4; //BY DEFAULT THE RESTRICT LEVEL IS R4
   if(showCommand!=null && showCommand.getClass().getAnnotation(Restricted.class) !=null){
     level = showCommand.getClass().getAnnotation(Restricted.class).level();
   }
   if(runnerRestrictedLevel.isRestricted(level)){
     throw new ShowRegistry.RestrictedCommandException();
   } 
   return showCommand;
  }


  private static void init() {
    if ( _init  ){
      put(ShowUFI.class);
      _init = false;
    }
    
  }


  private static void put(Class<? extends IShowCommand>  clazz) {
    try {
      IShowCommand show = clazz.newInstance();
      register(show);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public static boolean get(String command) {
	 String[] allStrs =  showAllStrings();
	 List<String> list = new ArrayList<String>();
	 list = Arrays.asList(allStrs);
	 return list.contains(command.toUpperCase()); 
  }


  public static void register(IShowCommand ishow) {
    for (String show:ishow.getShowAliases()) {
        toFirst.put(show.toUpperCase(),ishow.getShowAliases()[0].toUpperCase());
    }
    synchronized(showAll) {
        if (ishow.inShowAll()) {
            showAllDirty=true;
            showAll.add(ishow.getShowAliases()[0].toUpperCase());
        }
        else {
            if ((!(ishow instanceof IShowAllNeither))) {
                showAllDirty=true;
                if (!(ishow instanceof IShowPrefixNameNewline)) {
                    showAllExtended.add(ishow.getShowAliases()[0].toUpperCase());
                } else {
                    showAllExtendedMultiline.add(ishow.getShowAliases()[0].toUpperCase());
                }
            }
        }
    }
    s_registry.put(ishow.getShowAliases()[0].toUpperCase(),ishow);
  }
  
  public static  String[] showAllStrings() {
      String[] retVal=null;
      synchronized(showAll) {
          List<String> list=new ArrayList<String>(showAll);
          Collections.sort(list,String.CASE_INSENSITIVE_ORDER);
          retVal=list.toArray(new String[list.size()]);
      }
      return retVal;
  }
  private static  String[] showAllExtendedStrings() {
      String[] retVal=null;
      synchronized(showAll) {
          List<String> list=new ArrayList<String>(showAllExtended);
          list.add("CON_ID");//con_name and con_id are done by the same show //$NON-NLS-1$
          Collections.sort(list,String.CASE_INSENSITIVE_ORDER);
          retVal=list.toArray(new String[list.size()]);
      }
      return retVal;
  }
  private static  String[] showAllExtendedMultilineStrings() {
      String[] retVal=null;
      synchronized(showAll) {
          List<String> list=new ArrayList<String>(showAllExtendedMultiline);
          Collections.sort(list,String.CASE_INSENSITIVE_ORDER);
          retVal=list.toArray(new String[list.size()]);
      }
      return retVal;
  }
  static {
      put(SetColInvisble.class);
      put(CopyNumberToFloat.class);
      put(ShowAppicmd.class);   
      put(ShowArraycmd.class);  
      put(ShowAutoRecovery.class);//there are two showautorecovery ???? 
      put(ShowAutocommit.class);    
      put(ShowAutoprint.class); 
      put(ShowAutorecovery.class);  //there are two showautorecovery ???? 
      put(ShowAutotrace.class);
      put(ShowBinds.class);
      put(ShowBlockTerminator.class);
      put(ShowJava.class);
      put(ShowBtitle.class);    
      put(ShowBuffer.class);    
      put(ShowCmdSep.class);
      put(ShowColsep.class);    
      put(ShowConnection.class);
      put(ShowConname.class);   
      put(ShowCopyTypeCheck.class);
      put(ShowCopyc.class); 
      put(ShowCommandLine.class); 
      put(ShowDefine.class);
      put(ShowDefines.class);
      put(ShowDescribe.class);
      put(ShowEcho.class);
      put(ShowEdition.class);
      put(ShowEncoding.class);
      put(ShowEncodings.class);
      put(ShowEndBufT.class);
      put(ShowEndBufToken.class);
      put(ShowErrorLogging.class);
      put(ShowEscape.class);    
      put(ShowFeedback.class);  
      put(ShowFlagger.class);
      put(ShowHeading.class);   
      put(ShowHeadsep.class);  
      put(ShowInternalErrors.class);  
      put(ShowInstance.class);  
      put(ShowJdbc.class);  
      put(ShowLinesize.class);   
      put(ShowLobOffSet.class);
      put(ShowLong.class);  
      put(ShowLongChunkSize.class); 
      put(ShowNewPage.class);
      put(ShowNls.class);  
      put(ShowNull.class); 
      put(ShowConcat.class); 
      put(ShowNumformat.class); 
      put(ShowNumwidth.class);  
      put(ShowObsolete.class);  
      put(ShowPDBS.class);  
      put(ShowPagesize.class);  
      put(ShowParameter.class); 
      put(ShowPause.class);
      put(ShowPrelim.class); 
      put(ShowShowMode.class);  
      put(ShowRecSep.class);
      put(ShowRecSepChar.class);
      put(ShowRepheader.class);
      put(ShowRepfooter.class);
      put(ShowRecyclebin.class);    
      put(ShowRelease.class);   
      put(ShowSQLBlankLines.class);
      put(ShowSQLNumber.class); 
      put(ShowScan.class);  
      put(ShowSecuredCol.class);
      put(ShowServeroutput.class);  
      put(ShowSga.class);   
      put(ShowShowMode.class);//twice registered?
      put(ShowSpace.class); 
      put(ShowSpool.class); 
      put(ShowSpparameter.class);   
      put(ShowSqlcode.class);   
      put(ShowSqldev.class);    
      put(ShowSqldev2.class);   
      put(ShowSqlprompt.class); 
      put(ShowSuffix.class);
      put(ShowSystemOut.class);  
      put(ShowTables.class);    
      put(ShowTermout.class);   
      put(ShowTimingout.class); 
      put(ShowTrimout.class);   
      put(ShowTrimspool.class); 
      put(ShowTtitle.class);    
      put(ShowURLs.class);  
      put(ShowUnderLine.class);
      put(ShowUsercmd.class);   
      put(ShowVerify.class);    
      put(ShowWrap.class);  
      put(ShowXMLOptimization.class);
      put(ShowXMLOptimizationCheck.class);  
      put(ShowXquery.class);    
      put(Show_Restrict.class); 
      put(Show_ShowRowShip.class);
      put(ShowLNo.class);
      put(ShowPNo.class);
      put(ShowBinds.class);//listed twice
      put(ShowTopLevel.class);

      /*
	  put(ShowXMLOptimization.class);
	  put(ShowXMLOptimizationCheck.class);
	  put(ShowErrorLogging.class);
	  put(ShowLongChunkSize.class);
	  put(ShowCmdSep.class);
	  put(ShowBuffer.class);
	  put(ShowEndBufT.class);
	  put(ShowEndBufToken.class);
	  put(ShowBlockTerminator.class);
	  put(ShowCopyTypeCheck.class);
	  put(ShowRecSep.class);
	  put(ShowShowMode.class);
	  put(Show_ShowRowShip.class);
	  put(ShowEditFile.class);
	  put(ShowPause.class);
	  put(ShowSQLBlankLines.class);
	  put(ShowSecuredCol.class);
	  put(ShowRecSepChar.class);
	  put(ShowLobOffSet.class);
	  put(ShowAutoRecovery.class);
	  put(ShowUnderLine.class);
	  put(CopyNumberToFloat.class);
	  put(ColInvisible.class);
	  put(ShowFlagger.class);
	  put(ShowNewPage.class);
	  put(ShowSQLNumber.class);	
	  put(ShowPrelim.class);*/	
  }
  
  private static  class RestrictedCommandException extends Exception{
  }
  
}
