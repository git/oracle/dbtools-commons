/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowRelease implements IShowCommand {
	private static final String[] SHOWRELEASE = {
	    "rel", //$NON-NLS-1$
		"rele", //$NON-NLS-1$
		"relea", //$NON-NLS-1$
		"releas", //$NON-NLS-1$
		"release"}; //$NON-NLS-1$


    @Override
    public String[] getShowAliases() {
        return SHOWRELEASE;
    }

    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx.getCurrentConnection()==null)||(ctx.getCurrentConnection() instanceof OracleConnection)) { 
            return doShowRelease(conn, ctx, cmd);
        }
        return false;
    }

    @Override
    public boolean needsDatabase() {
        return true;
    }

    @Override
    public boolean inShowAll() {
        return true;
    }

	public boolean doShowRelease(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String banner = DBUtil.getInstance(ctx.getCurrentConnection()).executeOracleReturnOneCol("select * from v$version where banner like '%Oracle%'",
				(HashMap<String, ?>) null);
		if ((!(banner == null))) {
			String[] first = banner.split(" "); //$NON-NLS-1$
			// look for the string that is N.N.
			String regex = "[0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*"; //$NON-NLS-1$
			String result = null;
			try {
				for (String s : first) {
					if (Pattern.matches(regex, s)) {
						result = s;
					}
				}
			} catch (PatternSyntaxException e) {
				ctx.write(e.getLocalizedMessage() + "\n"); //$NON-NLS-1$
			}
			if (result != null) {
				String[] many = result.split("\\."); //$NON-NLS-1$
				try {
					int release = new Integer(many[0]) * 100000000;
					release = release + new Integer(many[1]) * 1000000;
					release = release + new Integer(many[2]) * 10000;
					release = release + new Integer(many[3]) * 100;
					release = release + new Integer(many[4]);
					ctx.write(Messages.getString("SHOWRELEASE") + " " + release + "\n"); //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
				} catch (NumberFormatException e) {
					ctx.write(e.getLocalizedMessage() + "\n"); //$NON-NLS-1$
				}
			}
		}
		return true;
	}

}