/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.SetServerOutput;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowServeroutput implements IShowCommand {
private static final String[] SHOWSERVEROUTPUT = {"serverout", //$NON-NLS-1$
    "serveroutp", //$NON-NLS-1$
    "serveroutpu", //$NON-NLS-1$
    "serveroutput"}; //$NON-NLS-1$


    @Override
    public String[] getShowAliases() {
        return SHOWSERVEROUTPUT;
    }

    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx.getCurrentConnection()==null)||(ctx.getCurrentConnection() instanceof OracleConnection)) { 
            return doShowServeroutput(conn, ctx, cmd);
        }
        return false;
    }

    @Override
    public boolean needsDatabase() {
        return false;
    }

    @Override
    public boolean inShowAll() {
        return true;
    }
    
    public boolean doShowServeroutput(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        Boolean isServerOutputOn = (Boolean) ctx.getProperty(ScriptRunnerContext.SHOWSERVEROUTPUT);
        Integer size = (Integer) ctx.getProperty(ScriptRunnerContext.LASTSETSERVEROUTPUT);
        String addOptimized="";  //$NON-NLS-1$
        if (ctx.getProperty(ScriptRunnerContext.SERVEROUTPUT_OPTIMIZED)!=null) {
            addOptimized=" OPTIMIZED";
        }
        if ((isServerOutputOn != null) && (isServerOutputOn.equals(Boolean.TRUE))) {            
            Boolean wasUnlimited=(Boolean)(ctx.getProperty(ScriptRunnerContext.SERVEROUTPUTUNLIMITED));
        	if ((wasUnlimited!=null)&&(wasUnlimited.equals(Boolean.TRUE))) {
        		ctx.write("serveroutput ON SIZE UNLIMITED "+SetServerOutput.serverOutputFormatToString(ctx)+addOptimized+"\n"); //$NON-NLS-1$ //$NON-NLS-2$            
        	} else {
           		ctx.write("serveroutput ON SIZE " +size.intValue()+ " "+SetServerOutput.serverOutputFormatToString(ctx)+addOptimized+"\n"); //$NON-NLS-1$   //$NON-NLS-2$     //$NON-NLS-3$                     		
        	}
        } else {
            ctx.write("serveroutput OFF"+addOptimized+"\n"); //$NON-NLS-1$  //$NON-NLS-2$
        }
        // sqlplus goes further - giving the buffer size - but this is not
        // recorded by sqldeveloper and I did not immediately see the way to
        // retreive it through (pl/)sql
        return true;
    }

}