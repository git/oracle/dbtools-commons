/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowSga implements IShowCommand, IShowPrefixNameNewline{
private static final String[] SHOWSGA = {"sga"}; //$NON-NLS-1$ 

    @Override
    public String[] getShowAliases() {
        return SHOWSGA;
    }

    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx.getCurrentConnection()==null)||(ctx.getCurrentConnection() instanceof OracleConnection)) { 
            return doShowSga(conn, ctx, cmd);
        }
        return false;
    }

    @Override
    public boolean needsDatabase() {
        return true;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }
	public boolean doShowSga(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		boolean amILocked = false;
		ResultSet rs = null;
		Statement s = null;
		try {
			amILocked = LockManager.lock(ctx.getBaseConnection());
			if (amILocked) {
				ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, null);
				String querySql = "SELECT DECODE(null,'','Total System Global Area','') NAME_COL_PLUS_SHOW_SGA, " //$NON-NLS-1$
						+ " to_char(SUM(VALUE)) VALUE, DECODE (null,'', 'bytes','') units_col_plus_show_sga FROM V$SGA " + " UNION ALL " //$NON-NLS-1$  //$NON-NLS-2$ 
						+ " SELECT NAME NAME_COL_PLUS_SHOW_SGA , to_char(value) VALUE, " //$NON-NLS-1$
						+ " DECODE (null,'', 'bytes','') units_col_plus_show_sga FROM V$SGA"; //$NON-NLS-1$
				s = ctx.getCurrentConnection().createStatement();
				rs = s.executeQuery(querySql);
				String overSizedSpace = "                                                                                                                                "; //$NON-NLS-1$
				int maxsize = 0;
				// Need to figure out size and pad appropriately
				if (rs != null) {
					while (rs.next()) {
						int size = rs.getString(2).length();
						if (size > maxsize) {
							maxsize = size;
						}
					}
					if (rs != null) {
						try {
							rs.close();
						} catch (Exception e) {
						}
					}
				}
				if (rs != null) {
					rs = s.executeQuery(querySql);
					int[] columnSizes = new int[] { 25, maxsize + 1, 10 };
					ctx.write("\n"); //$NON-NLS-1$
					while (rs.next()) {
						int i = 0;
						for (int thisColumn : columnSizes) {
							i++;
							String columnString = rs.getString(i);
							if (columnString == null) {
								columnString = ""; //$NON-NLS-1$
							}
							if (i == 2) {
								String padString = overSizedSpace + columnString;
								columnString = padString.substring(padString.length() - thisColumn);
							} else {
								columnString = (columnString + overSizedSpace).substring(0, thisColumn);
							}
							ctx.write(columnString);
							ctx.write(" "); //$NON-NLS-1$
						}
						ctx.write("\n"); //$NON-NLS-1$
					}
				}
			}
		} catch (SQLException e) {
			ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, e.getMessage());
			ctx.write(e.getLocalizedMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
			}
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
				}
			}
			if (amILocked) {
				try {
					LockManager.unlock(ctx.getBaseConnection());
				} catch (Exception e) {
				}
			}
		}
		return true;
	}
}