/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import oracle.dbtools.db.ConnectionResolver;
import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.db.ResultSetFormatter;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowSpparameter implements IShowCommand, IEndOfShowCommand{
	private static final String[] SHOWSPPARAMETER = {"spparameter","spparameters"}; //$NON-NLS-1$  //$NON-NLS-2$

    @Override
    public String[] getShowAliases() {
        return SHOWSPPARAMETER;
    }

    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx.getCurrentConnection()==null)||(ctx.getCurrentConnection() instanceof OracleConnection)) { 
            return doShowSpparameters(conn, ctx, cmd);
        }
        return false;
    }

    @Override
    public boolean needsDatabase() {
        return true;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }

	private boolean doShowSpparameters(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String toLower = ScriptUtils.eatOneWord(cmd.getSql()).trim().toLowerCase();
		String userArgument = null;
		if (toLower.startsWith(SHOWSPPARAMETER[1])) {
			userArgument = toLower.replaceAll("^" + SHOWSPPARAMETER[1], "").trim().split("\\s+")[0]; //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
		} else {
			userArgument = toLower.replaceAll("^" + SHOWSPPARAMETER[0], "").trim().split("\\s+")[0]; //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
		}
		String argument = "%"; //$NON-NLS-1$ 
		if (!(userArgument.equals(""))) { //$NON-NLS-1$ 
			argument = "%" + userArgument + "%"; //$NON-NLS-1$  //$NON-NLS-2$ 
		}
		// getlock
		boolean amILocked = false;
		boolean amIDoubleLocked = false;
		ResultSet rset = null;
		Connection curConn = ctx.getCurrentConnection();
		try {
			if (curConn != null) {
				amILocked = LockManager.lock(curConn);
			}
			if (amILocked) {

				ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, null);
				String querySql = "SELECT " + " substr(SID,0,20) SID, " + " substr(NAME,0,50) NAME, " + " TYPE, " //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$  //$NON-NLS-4$
						+ " DISPLAY_VALUE VALUE FROM V$SPPARAMETER " + " WHERE UPPER(NAME) LIKE UPPER(:NMBIND_SHOW_OBJ) " + " ORDER BY NAME,VALUE"; //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$

				ArrayList<String> localBind = new ArrayList<String>();
				localBind.add(argument);
				rset = DBUtil.getInstance(ctx.getCurrentConnection()).executeQuery(querySql, localBind);
				if (rset != null) {
					ResultSetFormatter rFormat = new ResultSetFormatter(ctx);
					rFormat.setProgressUpdater(ctx.getTaskProgressUpdater());
					Integer fs = null;
					try {// this is copied from sql java might be better to
							// leave it out - get parameters will be hundreds
							// not thousands.
							// doneill:see bug 8533854. the props are for the
							// base connection, not the current connection
							// as the base connection is always a SQL Developer
							// connection it will always have properties.
						if (ctx.getProperty(ScriptRunnerContext.ARRAYSIZE) != null) {
							fs = Integer.valueOf((String) ctx.getProperty(ScriptRunnerContext.ARRAYSIZE));
						} else {
							boolean doubleLocked = false;
							Connection baseConn = ctx.getBaseConnection();
							if (baseConn != null) {
								doubleLocked = LockManager.lock(baseConn);
							}
							if (doubleLocked) {
								try {
									Properties props = ConnectionResolver.getConnectionInfo(ConnectionResolver.getConnectionName(baseConn));

									fs = Integer.valueOf(props.getProperty(DBUtil.PREFERRED_FETCH_SIZE));
								} finally {
									LockManager.unlock(baseConn);
								}
							}
						}
					} catch (NumberFormatException e) {
						// Ignore, fs is null
					}
					if (fs != null) {
						try {
							rset.setFetchSize(fs);
						} catch (SQLException e) {
						}
					}
					   //  Bug 20519337 - FORMAT: OUTPUT OF SHOW PARAMETER IS UNFRIENDLY
	                //  This will override the existing column format. Not sure if it is ok? 
	                rFormat.rset2sqlplusShrinkToSize(rset, ctx.getCurrentConnection(), ctx.getOutputStream());
					//rFormat.formatResults(ctx.getOutputStream(), rset, querySql);
					//rFormat.rset2sqlplus(rset, ctx.getCurrentConnection(), ctx.getOutputStream());
				} else {
					ctx.write(Messages.getString("SHOWSPPARAMETERSNORESULTSET")); //$NON-NLS-1$
				}
			}
		} catch (IOException e) {
			ctx.write(e.getLocalizedMessage());
		} catch (SQLException e) {
			ctx.putProperty(ScriptRunnerContext.ERR_MESSAGE_SQLCODE, e.getMessage());
			ctx.write(e.getLocalizedMessage());
		} finally {
			if (rset != null) {
				DBUtil.closeResultSet(rset);
			}
			if (amILocked) {
				try {
					LockManager.unlock(curConn);
				} catch (Exception e) {
				}
			}
		}
		return true;
	}

}