/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.console.IConsoleClearScreen;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowSqldev implements IShowCommand {
	private static final String[] SHOWSQLDEV = {"sqldev"};//$NON-NLS-1$ 

    @Override
    public String[] getShowAliases() {
        return SHOWSQLDEV;
    }

    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx.getCurrentConnection()==null)||(ctx.getCurrentConnection() instanceof OracleConnection)) { 
            return doShowSqldev(conn, ctx, cmd);
        }
        return false;
    }

    @Override
    public boolean needsDatabase() {
        return false;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }

	private boolean doShowSqldev(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		ctx.write(".  .............$$$+====+++++++++$$$........................\n" + ".  .... .?Z===========+++++++++++++??????$?.  ..............\n"
				+ ".....,?~~===========++++++++++++++???????????7=.............\n" + "...$~~~~===========+++++++++++++?????????????III$...........\n"
				+ ".=~~~~============+++++++++++++?????????????IIIIII$.........\n" + ",~~~~===========+++++++++++++??????????????IIIIIIII+........\n"
				+ "$~~============++++++++++++??????????????IIIIIIIIII$, ......\n" + "$============+++++++++++++??????????????IIIIIIIIIII$,.. ... \n"
				+ "$===========++++++++++++??????????????IIIIIIIIIIII$$,.......\n" + "$~~$=====++++++++++++++??????????????IIIIIIIIIII$II$,.......\n"
				+ "$~:::,$+=++++++++++++??????????????IIIIIIIIII$IIIII$,.......\n" + "$~:::,,,.,$++++++++??????????????IIIIIIII$IIIIIIIII$,.......\n"
				+ "$~:::,,,... ..,,=$$7+???????????7$$7??IIIIIIIIIIIII$,.......\n" + "$~:::,,,... ..,,:::~~~~====+++++??????IIIIIIIIIIIII$,.. ....\n"
				+ "$~:::,,,... ..,,:::~~~~====+++++??????IIIIIIIIIIIII$,.......\n" + "$~:::,,,... ..,,:::~~~~====+88?+??????IIIIIIIIIIIII$,.......\n"
				+ "$7:::,,,... ..,,:::~~~~====+OII8I?????IIIIIIIIIIIII$,.......\n" + "$~Z::,,,... ..,,:::~~~~====+O?++?8Z???IIIIIIIIIII$I$,.......\n"
				+ "$~::?.,,... ..,,:::~~~~====+8+=~~=+78IIIIIIIIII$III$,.......\n" + "$~:::,,.$.. ..,,:::~~~~===++8+~:,::~+I8$III$7IIIIII$,.......\n"
				+ "$~:::,,,... =Z=,:::~~~~====+8+=:,,,:~=+I8O7IIIIIIII$,.......\n" + "$~:::,,,... ..,,:::~~~=~====8+=~::,::~=+?IO87IIIIII$,.......\n"
				+ "$~:::,,,... ..,,:::~~~~====+8?=~~:::::~~=+?I$8$IIII$,.......\n" + "$~:::,,,... ..,,:::~~~~===++8?+=~:::~~~~==+?I7$8O7I$:.......\n"
				+ "$=:::,,,... ..,,:::~~~~===++8I+=~~~~~~~===++??I7$OO$:.......\n" + "$~$::,,,... ..,,:::~~~~===++8I+==~~~=====++++??I77$Z8~......\n"
				+ "$~::$,,,... ..,,:::~~~~===++8I?+=======++++????II77$$Z8I....\n" + "$~:::,,$I.....,,:::~~~~===++87?++===++++++???III777$$ZZOO8. \n"
				+ "$~:::,,,....$$:,:::~~~~====+87?++++++++?????II777$$$$ZZO8I,.\n" + "$~:::,,,... ..,,::~~~~+I$$$$87??+++++????IIII777$$$ZZ8Z+. ..\n"
				+ "$~:::,,,......,,:::~~~~====+87I???????IIII777$$$ZZZ8Z~......\n" + "$~:::,,,......,,:::~~~~====+87I?????IIII777$$$ZZO8O$: ......\n"
				+ ".~:::,,,... ..,,:::~~~~====+87IIIIIII7777$$$ZZ8OZ7I$, ......\n" + ".::::,,,......,,:::~~~~====+8$7IIII777$$$ZZZ8O$III$:........\n"
				+ "..,$:,,,... ..,,:::~~~~====+8$77777$$$$ZZZ8O7III$?,.........\n" + "....,=I.... ..,,:::~~~~====+8$77$$$$ZZZ88Z7III7=,...........\n"
				+ "........~IZ...,,:::~~~~====+8$$$$ZZZO88$7$7+~,. ............\n" + "............,:~+$Z$~~~~===++8$ZZZOO8Z?=:,...................\n"
				+ "...............   ....,,,,,,8ZZO8O+,  ......................\n" + "............................8O8$~...........................\n"
				+ "............................II,.............................\n" + "................     .            . ..     .     . ..     . \n");
		try {
			Thread.currentThread().sleep(1500);
			if (ctx.getSQLPlusConsoleReader() != null) {
				IConsoleClearScreen runner = (IConsoleClearScreen) ctx.getSQLPlusConsoleReader();
				runner.doClearScreen(IConsoleClearScreen.CLEARTOP);

			}
		} catch (Exception e) {
		}
		return true;
	}

}