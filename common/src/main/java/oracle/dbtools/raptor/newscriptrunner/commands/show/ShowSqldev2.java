/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.console.IConsoleClearScreen;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowSqldev2 implements IShowCommand {
	private static final String[] SHOWSQLDEV2 = {"sqldev2"};//$NON-NLS-1$ 

    @Override
    public String[] getShowAliases() {
        return SHOWSQLDEV2;
    }

    @Override
    public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        if ((ctx.getCurrentConnection()==null)||(ctx.getCurrentConnection() instanceof OracleConnection)) { 
            return doShowSqldev2(conn, ctx, cmd);
        }
        return false;
    }

    @Override
    public boolean needsDatabase() {
        return false;
    }

    @Override
    public boolean inShowAll() {
        return false;
    }
	private boolean doShowSqldev2(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

		String[] computer1 = { "   .----.", "   |C>_.|", " __|____|__", "|  ______--|", "`-/.::::.\\-'", " `--------'" };
		String[] computer2 = { "   .----.   . - .", "   |C>_.|.:' .:.  .", " __|____|__ `:. ", "|  ______--|   ", "`-/.::::.\\-'   ", " `--------'" };
		String[] computer3 = { "   .----.   . - .    ", "   |C>_.|.:' .:.  .  ", " __|____|__ `:.  O_/ ", "|  ______--|   \\/M   ", "`-/.::::.\\-'   _/ \\_ ",
				" `--------'          " };

		String[] barry = { "  __________ ", "  \\______   \\_____ ______________ ___.__.", "  |    |  _/\\__  \\\\_  __ \\_  __ <   |  |",
				"  |    |   \\ / __ \\|  | \\/|  | \\/\\___  |", "  |______  /(____  /__|   |__|   / ____|", "         \\/      \\/              \\/    " };
		String[] kris = { " ____  __.      .__ ", "|    |/ _|______|__| ______  ", "|      < \\_  __ \\  |/  ___/ ", "|    |  \\ |  | \\/  |\\___ \\ ",
				"|____|__ \\|__|  |__/____  >", "        \\/              \\/" };
		String[] ram = { " _____ ", "|  __ \\   ", "| |__) |__ _ _ __ ___  ", "|  _  // _` | '_ ` _ \\  ", "| | \\ \\ (_| | | | | | |",
				"|_|  \\_\\__,_|_| |_| |_|  " };
		String[] dermot = { " _____", "|  __ \\                          | |", "| |  | | ___ _ __ _ __ ___   ___ | |_ ",
				"| |  | |/ _ \\ '__| '_ ` _ \\ / _ \\| __|", "| |__| |  __/ |  | | | | | | (_) | |_ ", "|_____/ \\___|_|  |_| |_| |_|\\___/ \\__|" };
		String[] turloch = { " _______         _            _ ", "|__   __|       | |          | |", "   | |_   _ _ __| | ___   ___| |__",
				"   | | | | | '__| |/ _ \\ / __| '_ \\", "   | | |_| | |  | | (_) | (__| | | |", "   |_|\\__,_|_|  |_|\\___/ \\___|_| |_|" };
		String[][] names = { barry, kris, ram, turloch, dermot };
		if (ctx.getSQLPlusConsoleReader() != null) {
			try {
				String prompt = ctx.getPrompt();

				IConsoleClearScreen runner = (IConsoleClearScreen) ctx.getSQLPlusConsoleReader();
				runner.setPrompt("");
				runner.doClearScreen(IConsoleClearScreen.CLEARTOP);
				for (int i = 0; i < computer1.length; i++) {
					System.err.println(computer1[i]);
				}
				Thread.currentThread().sleep(500);
				runner.doClearScreen(IConsoleClearScreen.CLEARTOP);
				for (int i = 0; i < computer2.length; i++) {
					System.err.println(computer2[i]);
				}
				Thread.currentThread().sleep(500);
				runner.doClearScreen(IConsoleClearScreen.CLEARTOP);
				for (int i = 0; i < computer3.length; i++) {
					System.err.println(computer3[i]);
				}
				Thread.currentThread().sleep(500);

				runner.doClearScreen(IConsoleClearScreen.CLEARTOP);
				for (String[] a : names) {
					runner.doClearScreen(IConsoleClearScreen.CLEARTOP);

					for (int i = 0; i < computer3.length; i++) {
						System.err.println(computer3[i] + a[i]);

					}
					Thread.currentThread().sleep(500);
					runner.doClearScreen(IConsoleClearScreen.CLEARTOP);
				}

			} catch (Exception e) {

			}
		}
		return true;
	}
}