/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.Connection;
import java.util.Calendar;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * @author klrice
 * 
 * Just a simple silly screen that is the pre cursor to sqlplus
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowUFI implements IShowCommand{
final static int year = Calendar.getInstance().get(Calendar.YEAR);
  final static String  ufi = "\n"+
      "          ██████████    ██████████         ██         █████████  █              ███████████\n" + 
      "         █          █   █         █       █  █       █           █             █\n" + 
      "        █            █  █          █     █    █     █            █            █\n" + 
      "        █            █  █         █     █      █    █            █            █\n" + 
      "        █            █  █   ██████      █      █    █            █            ███████████\n" + 
      "        █            █  █      █       ██████   █   █            █            █\n" + 
      "        █            █  █       █     █         █   █            █            █\n" + 
      "         █          █   █        █   █           █   █           █             █\n" + 
      "          ██████████    █         █  █           █    █████████   ██████████    ███████████\n" + 
      " \n" + 
      " \n" + 
      "                               ORACLE Database Management System\n" + 
      " \n" + 
      "                           (C) Copyright Oracle Corporation, "+ year +"\n" + 
      " \n" +  
      "                                      All Rights Reserved.\n" + 
      " \n" + 
      "                     This software has been provided under a license agreement\n" + 
      "                       containting certain restrictions on use and disclosure.\n" + 
      "                          Reverse engineering of object code is prohibited.\n" + 
      " \n" + 
      "                                    Press Any Key To Continue...\n";
  
  
  @Override
  public String[] getShowAliases() {
    return new String[]{"ufi"};
  }

  @Override
  public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    ctx.write(ufi.replaceAll("█", ""+(char)0x2588));
    
    
    return false;
  }

@Override
public boolean needsDatabase() {
    // TODO Auto-generated method stub
    return false;
}

@Override
public boolean inShowAll() {
    // TODO Auto-generated method stub
    return false;
}

}
