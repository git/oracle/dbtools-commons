/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.HashMap;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.util.Logger;
import oracle.jdbc.OracleConnection;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class ShowUsercmd implements IShowCommand {
	private static final String[] SHOWUSERCMD = { "user" }; //$NON-NLS-1$ 

	@Override
	public String[] getShowAliases() {
		return SHOWUSERCMD;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if ((ctx.getCurrentConnection() == null) || (ctx.getCurrentConnection() instanceof OracleConnection)) {
			return doShowUsercmd(conn, ctx, cmd);
		}
		return false;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return true;
	}

	private boolean doShowUsercmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		try {
			//if preliminary connection pretend sys
			if ((ctx!=null)&&(ctx.getProperty(ScriptRunnerContext.PRELIMAUTH)!=null)
					&&(((Boolean)(ctx.getProperty(ScriptRunnerContext.PRELIMAUTH).equals(Boolean.TRUE))))) {
						ctx.getOutputStream()
						.write(ctx.stringToByteArrayForScriptRunnerNonStatic(MessageFormat.format(Messages.getString("SHOWUSER"), new Object[] { "USER",
							"SYS" })));
            } else {
				if (conn == null) {
					ctx.getOutputStream()
							.write(ctx.stringToByteArrayForScriptRunnerNonStatic(MessageFormat.format(Messages.getString("SHOWUSER"), new Object[] { "USER",
									"" })));
				} else {
					ctx.getOutputStream().write(
							ctx.stringToByteArrayForScriptRunnerNonStatic(MessageFormat.format(Messages.getString("SHOWUSER"), new Object[] { "USER", //$NON-NLS-1$ //$NON-NLS-2$
									this.userNoDualUsed(conn)}))); //$NON-NLS-1$
				}
            }
			ctx.getOutputStream().write(ctx.stringToByteArrayForScriptRunnerNonStatic("\n")); //$NON-NLS-1$
		} catch (Exception e) {
			ctx.write(e.getLocalizedMessage());
		}
		return true;

	}
    public String userNoDualUsed(Connection conn) {
        String retVal=null;
        CallableStatement stmt = null;
        if (conn!=null&&(conn instanceof OracleConnection)) {
            boolean amILocked = LockManager.lock(conn);
            
            try {
                if (amILocked) {
                    stmt = conn.prepareCall(new StringBuffer("DECLARE \n").append( //$NON-NLS-1$
                             "CHECKONE VARCHAR2(1000):=NULL; \n").append( //$NON-NLS-1$
                            "BEGIN \n").append( //$NON-NLS-1$
                            " BEGIN \n").append( //$NON-NLS-1$
                            "   CHECKONE:=USER; \n").append( //$NON-NLS-1$
                            " EXCEPTION \n").append( //$NON-NLS-1$
                            " WHEN OTHERS THEN \n").append( //$NON-NLS-1$
                            "  CHECKONE :=NULL; \n").append( //$NON-NLS-1$
                            " END; \n").append( //$NON-NLS-1$
                            " :CHECKFORONE:=CHECKONE; \n").append(  //$NON-NLS-1$
                            "END;").toString()); //$NON-NLS-1$
                            stmt.registerOutParameter(1, java.sql.Types.VARCHAR); //$NON-NLS-1$
                            stmt.executeUpdate(); //$NON-NLS-1$
                            retVal=stmt.getString(1); //$NON-NLS-1$
                            if (stmt.wasNull()==true) { 
                                retVal=null; 
                            }
                }
            } catch (Exception e) {
                retVal=null;
            } finally {
                if (stmt!=null) {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        Logger.ignore(this.getClass(),e);
                    }
                }
                if (amILocked) {
                    LockManager.unlock(conn);
                }
            }
            
        }
        if (retVal==null) {
        	retVal="NULL";
        }
        return (retVal);  //$NON-NLS-1$
    }
}