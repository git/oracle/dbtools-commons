/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.commands.show.unsupported;

import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.IStoreCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.StoreRegistry;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;


/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ShowLongChunkSize.java">Barry McGillin</a> 
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class ShowLongChunkSize  implements IShowCommand, IStoreCommand {

    String[] ALIASES={ "longchunksize", //$NON-NLS-1$
            "longch", //$NON-NLS-1$
            "longchu", //$NON-NLS-1$
            "longchun", //$NON-NLS-1$
            "longchunk", //$NON-NLS-1$
            "longchunks", //$NON-NLS-1$
            "longchunksi", //$NON-NLS-1$
            "longchunksiz", //$NON-NLS-1$
            "longc"}; //$NON-NLS-1$

	@Override
	public String[] getShowAliases() {
		return ALIASES;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		ctx.write(getShowAliases()[0]+" " +ctx.getProperty(ScriptRunnerContext.SETLONGCHUNKSIZE)+"\n");
		return true;
	}

    @Override
    public boolean needsDatabase() {
        return false;
    }

    @Override
    public boolean inShowAll() {
        return true;
    }

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		return StoreRegistry.getCommand("setlongchunksize", String.valueOf(ctx.getProperty(ScriptRunnerContext.SETLONGCHUNKSIZE)));
	}
}
