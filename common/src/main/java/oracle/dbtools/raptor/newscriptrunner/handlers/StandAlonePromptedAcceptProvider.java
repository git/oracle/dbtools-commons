/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.newscriptrunner.handlers;

import oracle.dbtools.raptor.newscriptrunner.IGetPromptedFieldProvider;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;


/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=StandAlonePromptedAcceptProvider.java">Barry McGillin</a> 
 *
 */
public class StandAlonePromptedAcceptProvider implements IGetPromptedFieldProvider {

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.IGetPromptedFieldProvider#getPromptedField(oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, java.lang.String, boolean)
	 */
	@Override
	public String getPromptedField(ScriptRunnerContext ctx, String prompt, boolean hide) {
		/*
		 * Forcing this to return null and show its face as something you need
		 * to change. SQLCli uses JLine to provide console reader. You'll need
		 * to do something similar here
		 */
		return null;
	}

}
