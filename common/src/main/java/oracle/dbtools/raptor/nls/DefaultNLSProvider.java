/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.nls;

import java.sql.Connection;

import java.text.Format;

import java.util.Map;
import java.util.TimeZone;
import oracle.sql.Datum;

/**
 * @author klrice
 *
 */
public class DefaultNLSProvider {
    
    public String PROVIDER = "PROVIDER"; //$NON-NLS-1$
    private Connection _conn;
    private String _nullDisplay;

    public DefaultNLSProvider(Connection conn) {
	_conn = conn;
        _nullDisplay = null;
    }

    public Connection getConnection(){
	return _conn;
    }
    
    public String getNullDisplay() {
        return _nullDisplay;
    }
    
    public void setNullDisplay(String nullDisplay) {
        _nullDisplay = nullDisplay;
    }
    
    public String yieldNullDisplay(Object obj) {
        String nullDisplay = _nullDisplay;
        return (isNullOrEquiv(obj) && nullDisplay != null) ? nullDisplay : null;
    }
    
    public boolean isNullOrEquiv(Object obj) {
        return (obj == null);
    }
    
    public boolean isNullDisplay(Object obj) {
        return (_nullDisplay != null && obj != null && _nullDisplay.equals(obj));
    }
    
    // this is meant to return the text format of the value i.e. 01-NOV-2007:21:31
    public  Object getValue(Object obj) {
        return getValue(obj, null);
    }

    // this is meant to return a sql usable version of the data i.e. to_date('01-NOV-2007:21:31','DD-MON-RRRR:HH24:MI')
    @Deprecated
    public  String getSQL(Object obj) {
        return obj.toString();
    }
    
    // this is meant to return the text format of the value i.e. 01-11-2007:21:31, allowing access to value
    // in generic form, for example.  (known overider, OracleNLSProvider for timestamp support)
    
    public  Object getValue(Object obj, FormatType formatType) {
        try {
            String nullDisplay = yieldNullDisplay(obj);
            
            if (nullDisplay != null) {
                return nullDisplay;
            } else if (obj == null) {
                return ""; //$NON-NLS-1$
            } else if (obj instanceof Datum) {
                String str = ((Datum)obj).stringValue();
                
                if (str != null) {
                    return str;
                }
            }
        } catch (Exception e) {
            ; // Fall thru to toString();
        }
        
        return obj.toString();
    }
    
    /**
     * Get Formatter to sql type
     * @param sqlType
     * @return
     */
    public Format getFormat(int sqlType) {
        Format format = getFormat(sqlType, FormatType.PREFERRED);
        if (format == null) {
            format = getFormat(sqlType, FormatType.DEFAULT);
        }
        
        return format;
    }

    /**
     * Get Formatter to sql type
     * @param sqlType
     * @return
     */
    public Format getFormat(int sqlType, FormatType formatType) {
        return null;
    }

    // return various format strings.
    
    public  char getGroupSeparator() {
        return ',';
    }
    
    public  char getDecimalSeparator() {
      return '.';
    }
    
    public String getDateFormat() {
	  String format = getDateFormat(FormatType.PREFERRED);
      if (format == null) {
          format = getDateFormat(FormatType.DEFAULT);
      }
      
      return format;
    }

    public String getTimeStampWithTimeZoneFormat() {
        String format = getTimeStampWithTimeZoneFormat(FormatType.PREFERRED);
        if (format == null) {
            format = getTimeStampWithTimeZoneFormat(FormatType.DEFAULT);
        }
        
        return format;
    }
    
    /**
     * @return
     */
    public String getTimeStampWithLocalTimeZoneFormat() {
        String format = getTimeStampFormat(FormatType.PREFERRED);
        if (format == null) {
            format = getTimeStampFormat(FormatType.DEFAULT);
        }
        
        return format;
    }

    /**
     * @return
     */
    public String getTimeStampFormat() {
        String format = getTimeStampFormat(FormatType.PREFERRED);
        if (format == null) {
            format = getTimeStampFormat(FormatType.DEFAULT);
        }
        
        return format;
    }

    public String getDateFormat(FormatType formatType) {
        return null;
    }

    public String getTimeStampWithTimeZoneFormat(FormatType formatType) {
        return null;
    }
    
    public String getTimeStampWithLocalTimeZoneFormat(FormatType formatType) {
        return getTimeStampFormat(formatType);
    }

    /**
     * @return
     */
    public String getTimeStampFormat(FormatType formatType) {
        return null;
    }

    // load and reload settings
    public void setNlsFromPrefs() {
    }

    public void populateNLS() {
    }

    public void updateDefaults(Map<String, String> map) {
    }
    /**
     * @param m_conn
     */
    public void initConnection() {
    }

    public String format(Object obj) {
        try {
            String nullDisplay = yieldNullDisplay(obj);
            
            if (nullDisplay != null) {
                return nullDisplay;
            } else if (obj == null) {
                return null;
            } else if (obj instanceof Datum) {
                String str = ((Datum)obj).stringValue();
                
                if (str != null) {
                    return str;
                }
            }
        } catch (Exception e) {
            ; // Fall thru to toString();
        }
        
        return obj.toString();
    }

    public Object parse(String str) {
        throw new UnsupportedOperationException();
    }

    public Object parse(String str, Object base) {
        throw new UnsupportedOperationException();
    }

    public String getDBCharset() {
	return null;
    }
    
    public TimeZone getDatabaseTimeZone() {
        return null;
    }
    
    public TimeZone getSessionTimeZone() {
        return null;
    }
}
