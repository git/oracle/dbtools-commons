/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.nls;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.newscriptrunner.ScriptExecutor;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.util.Logger;
import oracle.jdbc.OracleConnection;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=NLSLang.java">Barry McGillin</a>
 *
 */
public class NLSLang {
	private static final String NLS_LANG = "NLS_LANG";
	private static HashMap<String, String> language = new HashMap<String, String>();
	private static ArrayList<String> territory = new ArrayList<String>();

	public static void main(String[] args) {
		ProcessBuilder pb = new ProcessBuilder("/bin/bash"); // or any other program you want to run

		System.out.println(NLSLang.getLanguageFromNLS_LANG());
		System.out.println(NLSLang.getTerritoryFromNLS_LANG());
		System.out.println(NLSLang.getCharSetFromNLS_LANG());

	}

	public static String setLangFromNLS_LANG() {
		if (getLanguageFromNLS_LANG() == null)
			return null;
		String nlslanguage = getLanguageFromNLS_LANG();
		if (!NLSLang.language.containsKey(nlslanguage.toUpperCase())) {
			Logger.log(NLSLang.class, Level.FINE, "Invalid NLS Language:" + nlslanguage.toUpperCase(), null);
			return null;
		}
		return "alter session set NLS_LANGUAGE='" + getLanguageFromNLS_LANG()+ "'";
	}

	public static String setTerritoryFromNLS_LANG() {
		if (getTerritoryFromNLS_LANG() == null) {
			Logger.log(NLSLang.class, Level.FINE, "Invalid NLS Territory on NLS_LANG", null);
			return null;
		}
		return "alter session set NLS_TERRITORY='" + getTerritoryFromNLS_LANG() + "'";
	}

	public static String setEncodingFromNLS_LANG() {
		if (getCharSetFromNLS_LANG() == null)
			return null;
		System.out.println(MessageFormat.format("Set local console encoding:{0}", getCharSetFromNLS_LANG()));
		return null;
	}

	public static String getLanguageFromNLS_LANG() {
		String val = System.getenv("NLS_LANG");
		if (val != null && val.length() > 0) {
			if (val.contains("_")) {
				String[] vals = val.split("_");
				if (vals.length > 1 && vals[0].length() > 0) {
					return vals[0];
				}
				return null;
			} else if (val.contains(".")) {
				String[] vals = val.split(".");
				if (vals.length > 1) {
					return vals[0];
				}
				return null;
			} else {
				return val;
			}
		}
		return null;
	}

	public static String getTerritoryFromNLS_LANG() {
		String val = System.getenv("NLS_LANG");
		if (val != null && val.length() > 0) {
			if (!val.contains("_"))
				return null;
			if (val.contains(".")) {
				val = val.substring(0, val.indexOf('.'));
			}
			return val.substring(val.indexOf("_") + 1, val.length());
		}
		return null;
	}

	public static String getCharSetFromNLS_LANG() {
		String val = System.getenv("NLS_LANG");
		if (val != null && val.length() > 0) {
			if (!val.contains("."))
				return null;
			return val.substring(val.indexOf(".") + 1, val.length());
		}
		return null;
	}

	public static String getLanguage(String abbreviation) {
		Iterator<String> it = language.keySet().iterator();
		while (it.hasNext()) {
			String langSelection = it.next();
			String abrev = language.get(langSelection);
			if (abrev.equalsIgnoreCase(abbreviation)) {
				return langSelection;
			}
		}
		return null;
	}

	public static String getLanguageAbbreviation(String languageSelection) {
		Iterator<String> it = language.keySet().iterator();
		while (it.hasNext()) {
			String langSelection = it.next();
			if (langSelection.equalsIgnoreCase(languageSelection)) {
				return language.get(langSelection);
			}
		}
		return null;
	}

	static {
		language.put("AMERICAN", "us");
		language.put("ARABIC", "ar");
		language.put("BENGALI", "bn");
		language.put("BRAZILIAN PORTUGUESE", "ptb");
		language.put("BULGARIAN", "bg");
		language.put("CANADIAN FRENCH", "frc");
		language.put("CATALAN", "ca");
		language.put("CROATIAN", "hr");
		language.put("CZECH", "cs");
		language.put("DANISH", "dk");
		language.put("DUTCH", "nl");
		language.put("EGYPTIAN", "eg");
		language.put("ENGLISH", "gb");
		language.put("ESTONIAN", "et");
		language.put("FINNISH", "sf");
		language.put("FRENCH", "f");
		language.put("GERMAN DIN", "din");
		language.put("GERMAN", "d");
		language.put("GREEK", "el");
		language.put("HEBREW", "iw");
		language.put("HINDI", "hi");
		language.put("HUNGARIAN", "hu");
		language.put("ICELANDIC", "is");
		language.put("INDONESIAN", "in");
		language.put("ITALIAN", "i");
		language.put("JAPANESE", "ja");
		language.put("KOREAN", "ko");
		language.put("LATIN AMERICAN SPANISH", "esa");
		language.put("LATVIAN", "lv");
		language.put("LITHUANIAN", "lt");
		language.put("MALAY", "ms");
		language.put("MEXICAN SPANISH", "esm");
		language.put("NORWEGIAN", "n");
		language.put("POLISH", "pl");
		language.put("PORTUGUESE", "pt");
		language.put("ROMANIAN", "ro");
		language.put("RUSSIAN", "ru");
		language.put("SIMPLIFIED CHINESE", "zhs");
		language.put("SLOVAK", "sk");
		language.put("SLOVENIAN", "sl");
		language.put("SPANISH", "e");
		language.put("SWEDISH", "s");
		language.put("TAMIL", "ta");
		language.put("THAI", "th");
		language.put("TRADITIONAL CHINESE", "zht");
		language.put("TURKISH", "tr");
		language.put("UKRAINIAN", "uk");
		language.put("VIETNAMESE", "vn");

		territory.add("ALGERIA");
		territory.add("ICELAND");
		territory.add("QATAR");
		territory.add("AMERICA");
		territory.add("INDIA");
		territory.add("ROMANIA");
		territory.add("AUSTRALIA");
		territory.add("INDONESIA");
		territory.add("SAUDI ARABIA");
		territory.add("AUSTRIA");
		territory.add("IRAQ");
		territory.add("SINGAPORE");
		territory.add("BAHRAIN");
		territory.add("IRELAND");
		territory.add("SLOVAKIA");
		territory.add("BANGLADESH");
		territory.add("ISRAEL");
		territory.add("SLOVENIA");
		territory.add("BELGIUM");
		territory.add("ITALY");
		territory.add("SOMALIA");
		territory.add("BRAZIL");
		territory.add("JAPAN");
		territory.add("SOUTH AFRICA");
		territory.add("BULGARIA");
		territory.add("JORDAN");
		territory.add("SPAIN");
		territory.add("CANADA");
		territory.add("KAZAKHSTAN");
		territory.add("SUDAN");
		territory.add("CATALONIA");
		territory.add("KOREA");
		territory.add("SWEDEN");
		territory.add("CHINA");
		territory.add("KUWAIT");
		territory.add("SWITZERLAND");
		territory.add("CIS");
		territory.add("LATVIA");
		territory.add("SYRIA");
		territory.add("CROATIA");
		territory.add("LEBANON");
		territory.add("TAIWAN");
		territory.add("CYPRUS");
		territory.add("LIBYA");
		territory.add("THAILAND");
		territory.add("CZECH REPUBLIC");
		territory.add("LITHUANIA");
		territory.add("THE NETHERLANDS");
		territory.add("DENMARK");
		territory.add("LUXEMBOURG");
		territory.add("TUNISIA");
		territory.add("DJIBOUTI");
		territory.add("MALAYSIA");
		territory.add("TURKEY");
		territory.add("EGYPT");
		territory.add("MAURITANIA");
		territory.add("UKRAINE");
		territory.add("ESTONIA");
		territory.add("MEXICO");
		territory.add("UNITED ARAB EMIRATES");
		territory.add("FINLAND");
		territory.add("MOROCCO");
		territory.add("UNITED KINGDOM");
		territory.add("FRANCE");
		territory.add("NEW ZEALAND");
		territory.add("UZBEKISTAN");
		territory.add("GERMANY");
		territory.add("NORWAY");
		territory.add("VIETNAM");
		territory.add("GREECE");
		territory.add("OMAN");
		territory.add("YEMEN");
		territory.add("HONG KONG");
		territory.add("POLAND");
		territory.add("HUNGARY");
		territory.add("PORTUGAL");
	}

	public static boolean isNLSLANGSet() {
		if (System.getenv(NLS_LANG) != null && System.getenv(NLS_LANG).length() > 0)
			return true;
		return false;
	}

	public static void setupNLSSession(ScriptRunnerContext ctx, Connection conn) {
		if (conn != null && conn instanceof OracleConnection) {
			PreparedStatement nlsStmt = null;
			if (setLangFromNLS_LANG() != null) {
				try {
					nlsStmt = conn.prepareStatement(setLangFromNLS_LANG());
					nlsStmt.execute();
				} catch (SQLException e) {
					Logger.fine(NLS_LANG.getClass(), e);
				}
			}
			if (setTerritoryFromNLS_LANG() != null) {
				try {
					nlsStmt = conn.prepareStatement(setTerritoryFromNLS_LANG());
					nlsStmt.execute();
				} catch (SQLException e) {
					Logger.fine(NLS_LANG.getClass(), e);
				}
			}
		}

	}
}
