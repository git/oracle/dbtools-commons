/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.nls;

/**
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=OraConstants.java"
 *         >Barry McGillin</a>
 * 
 */
public interface OraConstants {
	public static final int SIZE_DATE = 7;
	public static final int SIZE_TIMESTAMP = 11;
	public static final int SIZE_TIMESTAMPTZ = 13;

	public static final int BYTE_YEAR1 = 0;
	public static final int BYTE_YEAR2 = 1;
	public final int BYTE_MONTH = 2;
	public final int BYTE_DAY = 3;
	public final int BYTE_HOUR = 4;
	public final int BYTE_MINUTE = 5;
	public final int BYTE_SECOND = 6;
	public static final int BYTE_NANOS1 = 7;
	public static final int BYTE_NANOS2 = 8;
	public static final int BYTE_NANOS3 = 9;
	public static final int BYTE_NANOS4 = 10;
	public static final int BYTE_TIMEZONE1 = 11;
	public static final int BYTE_TIMEZONE2 = 12;
}