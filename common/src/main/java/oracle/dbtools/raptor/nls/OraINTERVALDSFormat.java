/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.nls;

import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;

import oracle.dbtools.raptor.datatypes.objects.OraINTERVALDS;

import oracle.i18n.util.OraLocaleInfo;

import oracle.sql.INTERVALDS;

/**
 * The <code>OraINTERVALYMFormat</code> class is a concrete class to do
 * locale-sensitive formatting and parsing between oracle.sql.INTERVALDS and string.
 * It supports Oracle date/time formatting behavior.
 *
 * It uses its superclass <code>oracle.i18n.text.OraSimpleDateFormat</code> class to
 * do the actual formatting.
 *
 * INTERVALYMs are generally time zone unaware, but because java.util.Date is used internally,
 * this class uses GMT for any internal processing to avoid complications.
 *
 * @see <code>oracle.i18n.textOraSimpleDateFormat</code>
 */

public class OraINTERVALDSFormat extends OraIntervalDatumFormat {

    /**
     * Constructs an <code>OraINTERVALYMFormat</code> object that uses
     * the given format pattern for the given <code>OraLocaleInfo</code> object.
     *
     * Patterns containing time zone-related or fractional second format elements are illegal.
     *
     * @param pattern the given format pattern
     * @param localeInfo the given <code>OraLocaleInfo</code> object
     *
     * @exception ParseException if the format pattern is invalid
     */

    public OraINTERVALDSFormat(OraLocaleInfo localeInfo) throws ParseException {
        super(localeInfo);
    }

    /**
     * Overrides the <code>equals</code> method.
     *
     * @param object an object to be compared
     *
     * @return <code>true</code> if two objects are identical, otherwise
     *         <code>false</code>
     */

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (!(object instanceof OraINTERVALDSFormat) || !super.equals(object)) {
            return false;
        }

        return true;
    }

    /**
     * Formats an <code>oracle.sql.INTERVALDS</code> object into a date/time string.
     *
     * @param date the INTERVALDS object to be formatted.
     *
     * @return the string with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    public String format(INTERVALDS date) {
        return format(date, new StringBuffer(),
                      new FieldPosition(OraINTERVALDS.ALL_FIELDS)).toString();
    }

    /**
     * Formats an <code>oracle.sql.INTERVALDS</code> object into a date/time string.
     * Sets field position if needed.
     *
     * @param date the INTERVALDS object to be formatted.
     * @param toAppendTo the string buffer to be appended with the formatted
     *        date/time string.
     * @param pos to be used to get offsets of a given field in the returned
     *        string buffer. On input, the alignment field of which the
     *        offsets are to be returned. On output, the offsets of the alignment
     *        field in the returned string buffer.
     *
     * @return the string buffer appended with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    public StringBuffer format(INTERVALDS interval, StringBuffer toAppendTo, FieldPosition pos) {
        if (interval != null) {
            OraINTERVALDS iVal = OraINTERVALDS.getInstance(interval);
            
            int sign = iVal.get(OraINTERVALDS.SIGN);
            int days = iVal.get(OraINTERVALDS.DAYS);
            int hours = iVal.get(OraINTERVALDS.HOURS);
            int minutes = iVal.get(OraINTERVALDS.MINUTES);
            int seconds = iVal.get(OraINTERVALDS.SECONDS);
            int nanos = iVal.get(OraINTERVALDS.NANOS);
            
            String nanoPattern = "%09d";
            if (nanos % 1000 == 0) {
                nanos = nanos / 1000;
                nanoPattern = "%06d";
            }
            
            String str = ((sign == -1) ? "-" : "+") + String.format("%02d", days) + " " + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds) + "." + String.format(nanoPattern, nanos);
            
            toAppendTo.append(str);
        }
        
        return toAppendTo;
    }

    /**
     * Formats an object as an <code>oracle.sql.INTERVALDS</code> into a date/time string.
     * Sets field position if needed.
     *
     * @param obj the object to be formatted.
     * @param toAppendTo the string buffer to be appended with the formatted
     *        date/time string.
     * @param pos to be used to get offsets of a given field in the returned
     *        string buffer. On input, the alignment field of which the
     *        offsets are to be returned. On output, the offsets of the alignment
     *        field in the returned string buffer.
     *
     * @return the string buffer appended with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        if (obj == null || obj instanceof INTERVALDS) {
            return format((INTERVALDS)obj, toAppendTo, pos);
        } else {
            throw new IllegalArgumentException(Messages.getString("OraINTERVALDSFormat.90")); //$NON-NLS-1$
        }
    }

    /**
     * Parses a date/time string into a <code>INTERVALDS</code> object,
     * starting from the given parse position.
     *
     * @param text the date/time string
     * @param pos indicates where to start the parsing on input. On output,
     *        returns where the parse ends if parsing succeeds, or the start
     *        index if it fails.
     *
     * @return a <code>INTERVALDS</code> object
     *
     * @exception IllegalArgumentException if the format pattern associated
     *            with this formatter is not valid for parsing or the given
     *            date/time string  cannot be parsed into a <code>TIMESTAMP</code>
     *            object
     */
    public INTERVALDS parse(String text, ParsePosition pos) {
        if (text != null && text.length() > 0) {
            byte[] bytes = INTERVALDS.toBytes(text.substring(pos.getIndex()));
            
            return new INTERVALDS(bytes);
        } else {
            return null;
        }
    }
}
