/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.nls;

import java.text.MessageFormat;

import java.util.Locale;

import oracle.dbtools.raptor.datatypes.objects.OraDATE;
import oracle.dbtools.raptor.datatypes.objects.OraTIMESTAMP;
import oracle.dbtools.raptor.datatypes.objects.OraTIMESTAMPLTZ;
import oracle.dbtools.raptor.datatypes.objects.OraTIMESTAMPTZ;

/**
 * The <code>OraDATEFormat</code> class is a concrete class to do
 * locale-sensitive formatting and parsing between oracle.sql.DATE and string.
 * It supports Oracle date/time formatting behavior.
 *
 * It uses its superclass <code>oracle.i18n.text.OraSimpleDateFormat</code> class to
 * do the actual formatting.
 *
 * DATEs are generally time zone unaware, but because java.util.Date is used internally,
 * this class uses GMT for any internal processing to avoid complications.
 *
 * @see <code>oracle.i18n.textOraSimpleDateFormat</code>
 */

public class OraMessageFormat extends MessageFormat {
    /**
     * Constructs a MessageFormat for the default locale and the
     * specified pattern.
     * The constructor first sets the locale, then parses the pattern and
     * creates a list of subformats for the format elements contained in it.
     * Patterns and their interpretation are specified in the
     * <a href="#patterns">class description</a>.
     *
     * @param pattern the pattern for this message format
     * @exception IllegalArgumentException if the pattern is invalid
     */
    public OraMessageFormat(String pattern) {
        super(pattern);
    }

    /**
     * Constructs a MessageFormat for the specified locale and
     * pattern.
     * The constructor first sets the locale, then parses the pattern and
     * creates a list of subformats for the format elements contained in it.
     * Patterns and their interpretation are specified in the
     * <a href="#patterns">class description</a>.
     *
     * @param pattern the pattern for this message format
     * @param locale the locale for this message format
     * @exception IllegalArgumentException if the pattern is invalid
     * @since 1.4
     */
    public OraMessageFormat(String pattern, Locale locale) {
        super(pattern, locale);
    }
    
    /**
     * Creates a MessageFormat with the given pattern and uses it
     * to format the given arguments. This is equivalent to
     * <blockquote>
     *     <code>(new {@link #MessageFormat(String) MessageFormat}(pattern)).{@link #format(java.lang.Object[], java.lang.StringBuffer, java.text.FieldPosition) format}(arguments, new StringBuffer(), null).toString()</code>
     * </blockquote>
     *
     * @exception IllegalArgumentException if the pattern is invalid,
     *            or if an argument in the <code>arguments</code> array
     *            is not of the type expected by the format element(s)
     *            that use it.
     */
    public static String format(String pattern, Object ... arguments) {
        return format(pattern, null, arguments);
    }
    
    /**
     * Creates a MessageFormat with the given pattern and uses it
     * to format the given arguments. This is equivalent to
     * <blockquote>
     *     <code>(new {@link #MessageFormat(String) MessageFormat}(pattern)).{@link #format(java.lang.Object[], java.lang.StringBuffer, java.text.FieldPosition) format}(arguments, new StringBuffer(), null).toString()</code>
     * </blockquote>
     *
     * @exception IllegalArgumentException if the pattern is invalid,
     *            or if an argument in the <code>arguments</code> array
     *            is not of the type expected by the format element(s)
     *            that use it.
     */
    public static String format(String pattern, DefaultNLSProvider nlsProvider, Object ... arguments) {
        OraMessageFormat temp = new OraMessageFormat(pattern);
        
        for (int i = 0; i < arguments.length; i++) {
            try {
            if (arguments[i] instanceof oracle.sql.NUMBER) {
                arguments[i] = ((oracle.sql.NUMBER)arguments[i]).bigDecimalValue();
            } else if (arguments[i] instanceof oracle.sql.DATE) {
            	OraDATE instance = OraDATE.getInstance((oracle.sql.DATE)arguments[i]);
                if (nlsProvider != null) {
                    instance.setSessionTimeZone(nlsProvider.getSessionTimeZone());
                }
                arguments[i] = instance.getTime();
            } else if (arguments[i] instanceof oracle.sql.TIMESTAMP) {
            	OraTIMESTAMP instance = OraTIMESTAMP.getInstance((oracle.sql.TIMESTAMP)arguments[i]);
                if (nlsProvider != null) {
                	instance.setSessionTimeZone(nlsProvider.getSessionTimeZone());
                }
                arguments[i] = instance.getTime();
            } else  if (arguments[i] instanceof oracle.sql.TIMESTAMPTZ) {
                arguments[i] = OraTIMESTAMPTZ.getInstance(((oracle.sql.TIMESTAMPTZ)arguments[i])).getTime();
            } else  if (arguments[i] instanceof oracle.sql.TIMESTAMPLTZ) {
            	OraTIMESTAMPLTZ instance = OraTIMESTAMPLTZ.getInstance((oracle.sql.TIMESTAMPLTZ)arguments[i]);
                if (nlsProvider != null) {
                    instance.setDBTimeZone(nlsProvider.getDatabaseTimeZone());
                    instance.setSessionTimeZone(nlsProvider.getSessionTimeZone());
                }
                arguments[i] = instance.getTime();
            }
            } catch (Exception e) {
                ;
            }
        }
        
        return temp.format(arguments);
    }
}
