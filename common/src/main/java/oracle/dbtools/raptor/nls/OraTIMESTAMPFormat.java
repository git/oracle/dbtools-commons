/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.nls;

import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;

import java.util.Calendar;
import java.util.Locale;

import java.util.TimeZone;

import oracle.dbtools.raptor.datatypes.objects.OraTIMESTAMP;
import oracle.dbtools.raptor.datatypes.objects.OraTemporalDatum;

import oracle.i18n.text.OraDateFormat;
import oracle.i18n.util.OraLocaleInfo;

import oracle.sql.TIMESTAMP;

/**
 * The <code>OraTIMESTAMPFormat</code> class is a concrete class to do
 * locale-sensitive formatting and parsing between oracle.sql.TIMESTAMP[LTZ] and string.
 * It supports Oracle date/time formatting behavior.
 *
 * It uses its superclass <code>oracle.i18n.text.OraSimpleDateFormat</code> class to
 * do the actual formatting.
 *
 * TIMESTAMPs are generally time zone unaware, but because java.util.Date is used internally,
 * this class uses UTC for any internal processing to avoid complications.
 *
 * TIMESTAMPLTZs are in the DB time zone.  DB time zone must be retrieved from the DB
 * and used to create an OraTIMESTAMPFormat object.  The time zone is used to transform
 * the timestamp to the default time zone.
 *
 * @see <code>oracle.i18n.textOraSimpleDateFormat</code>
 */

public class OraTIMESTAMPFormat extends OraTemporalDatumFormat {

    /**
     * Constructs an <code>OraTIMESTAMPFormat</code> object that uses
     * the given format pattern for the given <code>OraLocaleInfo</code> object.
     * No DB time zone is registered.  This object cannot be used to format
     * TIMESTAMPLTZ data.
     *
     * Patterns containing time zone-related format elements are illegal.
     *
     * @param pattern the given format pattern
     * @param localeInfo the given <code>OraLocaleInfo</code> object
     *
     * @exception ParseException if the format pattern is invalid
     */

    public OraTIMESTAMPFormat(String pattern,
                              OraLocaleInfo localeInfo) throws ParseException {
        this(pattern, localeInfo, null);
    }

    /**
     * Constructs an <code>OraTIMESTAMPFormat</code> object that uses
     * the given format pattern for the given <code>OraLocaleInfo</code> object.
     * No DB time zone is registered.  This object cannot be used to format
     * TIMESTAMPLTZ data.
     *
     * Patterns containing time zone-related format elements are illegal.
     *
     * @param pattern the given format pattern
     * @param localeInfo the given <code>OraLocaleInfo</code> object
     * @param sessionTimeZone the given session <code>TimeZone</code> object
     *
     * @exception ParseException if the format pattern is invalid
     */

    public OraTIMESTAMPFormat(String pattern,
                              OraLocaleInfo localeInfo,
                              TimeZone sessionTimeZone) throws ParseException {
        super(pattern, localeInfo, sessionTimeZone);
    }

    /**
     * Applies the given format pattern of this TIMESTAMP formatter.
     *
     * Patterns containing time zone-related format elements are illegal.
     *
     * @param pattern the given format pattern
     *
     * @exception ParseException if the format pattern is invalid
     */

    @Override
    public void applyPattern(String pattern) throws ParseException {
        String uc = pattern.toUpperCase(Locale.US);
        StringBuffer pat = new StringBuffer(pattern);
        int pos = -1;
        int i;
        int cnt;
        char c;

        // Verify that the format does not contain any time zone elements
        pos = -1;
        while ((pos = uc.indexOf("TZ", pos + 1)) >= 0) { //$NON-NLS-1$
            if (pos > uc.length() - 3)
                break;
            if ((c = uc.charAt(pos + 2)) != 'R' && c != 'D' && c != 'H' && c != 'M')
                continue;
            i = -1;
            cnt = 0;
            while ((i = uc.indexOf('"', i + 1)) < pos && i != -1)
                ++cnt;
            if (cnt % 2 == 0) {
                // even number of quotes - TZx is not in quoted text
                throw new ParseException(Messages.getString("OraTIMESTAMPFormat.11"), pos); //$NON-NLS-1$
            }
        }

        // Apply and verify the pattern as OraSimpleDATEFormat pattern
        super.applyPattern(pat.toString());
    }

    /**
     * Overrides the <code>equals</code> method.
     *
     * @param object an object to be compared
     *
     * @return <code>true</code> if two objects are identical, otherwise
     *         <code>false</code>
     */

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (!(object instanceof OraTIMESTAMPFormat) || !super.equals(object)) {
            return false;
        }

        return true;
    }

    /**
     * Formats an <code>oracle.sql.TIMESTAMP</code> object into a date/time string.
     *
     * @param date the TIMESTAMP object to be formatted.
     *
     * @return the string with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    public String format(TIMESTAMP date) {
        return format(date, new StringBuffer(),
                      new FieldPosition(OraDateFormat.ALL_FIELD)).toString();
    }

    /**
     * Formats an <code>oracle.sql.TIMESTAMP</code> object into a date/time string.
     * Sets field position if needed.
     *
     * @param date the TIMESTAMP object to be formatted.
     * @param toAppendTo the string buffer to be appended with the formatted
     *        date/time string.
     * @param pos to be used to get offsets of a given field in the returned
     *        string buffer. On input, the alignment field of which the
     *        offsets are to be returned. On output, the offsets of the alignment
     *        field in the returned string buffer.
     *
     * @return the string buffer appended with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    public StringBuffer format(TIMESTAMP date, StringBuffer toAppendTo, FieldPosition pos) {
        // TIMESTAMP is time zone unaware, use UTC to remove dependency on DST
        // prepare to format in UTC (simulate no time zone)
        return formatTemporalDatum(OraTIMESTAMP.getInstance(date), toAppendTo, pos);
    }

    /**
     * Formats an object as an <code>oracle.sql.TIMESTAMP</code> into a date/time string.
     * Sets field position if needed.
     *
     * @param obj the object to be formatted.
     * @param toAppendTo the string buffer to be appended with the formatted
     *        date/time string.
     * @param pos to be used to get offsets of a given field in the returned
     *        string buffer. On input, the alignment field of which the
     *        offsets are to be returned. On output, the offsets of the alignment
     *        field in the returned string buffer.
     *
     * @return the string buffer appended with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        if (obj == null || obj instanceof TIMESTAMP) {
            return format((TIMESTAMP)obj, toAppendTo, pos);
        } else {
            throw new IllegalArgumentException(Messages.getString("OraTIMESTAMPFormat.51")); //$NON-NLS-1$
        }
    }

    /**
     * Parses a date/time string into a <code>TIMESTAMP</code> object,
     * starting from the given parse position.
     *
     * @param text the date/time string
     * @param pos indicates where to start the parsing on input. On output,
     *        returns where the parse ends if parsing succeeds, or the start
     *        index if it fails.
     *
     * @return a <code>TIMESTAMP</code> object
     *
     * @exception IllegalArgumentException if the format pattern associated
     *            with this formatter is not valid for parsing or the given
     *            date/time string  cannot be parsed into a <code>TIMESTAMP</code>
     *            object
     */
    public TIMESTAMP parse(String text, ParsePosition pos) {
        // TIMESTAMP is time zone unaware, use UTC which has on DST
        OraTIMESTAMP temporalDatum = (OraTIMESTAMP)parseTemporalDatum(text, pos, UTC);
        
        return (temporalDatum != null) ? temporalDatum.getDatum() : null;
    }
    
    protected OraTemporalDatum getTemporalDatum(Calendar calendar, int nanos) {
        OraTIMESTAMP ret = OraTIMESTAMP.getInstance(OraConversions.toZonedDateTime(calendar, nanos).toLocalDateTime());
        ret.setSessionTimeZone(getSessionTimeZone());
        return ret;
    }
}
