/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.nls;

import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;

import java.util.Calendar;
import java.util.TimeZone;

import oracle.dbtools.raptor.datatypes.objects.OraTIMESTAMPTZ;
import oracle.dbtools.raptor.datatypes.objects.OraTemporalDatum;

import oracle.i18n.text.OraDateFormat;
import oracle.i18n.util.OraLocaleInfo;

import oracle.sql.TIMESTAMPTZ;

/**
 * The <code>OraTIMESTAMPTZFormat</code> class is a concrete class to do
 * locale-sensitive formatting and parsing between oracle.sql.TIMESTAMPTZ and string.
 * It supports Oracle date/time formatting behavior.
 *
 * It uses its superclass <code>oracle.i18n.text.OraSimpleDateFormat</code> class to
 * do the actual formatting.
 *
 * TIMESTAMPTZs are time zone aware.  The time zone is used for internal calculations
 * and it is displayed if specified by format model.
 *
 * The class uses Java time zone functionality.  If an Oracle time zone name
 * is not recognized, the timestamp is formatted in GMT.
 *
 * @see <code>oracle.i18n.textOraSimpleDateFormat</code>
 */

public class OraTIMESTAMPTZFormat extends OraTemporalDatumFormat {

    /* Region id bit flag */
    private static final int REGIONIDBIT = 0x80;

    /* Excess offsets for time zone */
    private static final byte OFFSET_HOUR = 20;
    private static final byte OFFSET_MINUTE = 60;

    private static final String[] digit =
    { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$

    /**
     * Constructs an <code>OraTIMESTAMPTZFormat</code> object that uses
     * the given format pattern for the given <code>OraLocaleInfo</code> object.
     *
     * @param pattern the given format pattern
     * @param localeInfo the given <code>OraLocaleInfo</code> object
     *
     * @exception ParseException if the format pattern is invalid
     */

    public OraTIMESTAMPTZFormat(String pattern,
                                OraLocaleInfo localeInfo) throws ParseException {
        super(pattern, localeInfo, null);
    }

    /**
     * Overrides the <code>equals</code> method.
     *
     * @param object an object to be compared
     *
     * @return <code>true</code> if two objects are identical, otherwise
     *         <code>false</code>
     */

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (!(object instanceof OraTIMESTAMPTZFormat) || !super.equals(object)) {
            return false;
        }

        return true;
    }

    /**
     * Formats an <code>oracle.sql.TIMESTAMPTZ</code> object into a date/time string.
     *
     * @param date the TIMESTAMPTZ object to be formatted.
     *
     * @return the string with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    public String format(TIMESTAMPTZ date) {
        return format(date, new StringBuffer(),
                      new FieldPosition(OraDateFormat.ALL_FIELD)).toString();
    }

    /**
     * Formats an <code>oracle.sql.TIMESTAMPTZ</code> object into a date/time string.
     * Sets field position if needed.
     *
     * @param date the TIMESTAMPTZ object to be formatted.
     * @param toAppendTo the string buffer to be appended with the formatted
     *        date/time string.
     * @param pos to be used to get offsets of a given field in the returned
     *        string buffer. On input, the alignment field of which the
     *        offsets are to be returned. On output, the offsets of the alignment
     *        field in the returned string buffer.
     *
     * @return the string buffer appended with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    public StringBuffer format(TIMESTAMPTZ date, StringBuffer toAppendTo, FieldPosition pos) {
        // TIMESTAMPTZ is stored in UTC
        // prepare to format in TIMESTAMPTZ's time zone
        StringBuffer result = formatTemporalDatum(OraTIMESTAMPTZ.getInstance(date), toAppendTo, pos);
        
        /* remove GMT from GMT[+-][0-9][0-9]:[0-9][0-9] */
        int indexGMT = result.indexOf("GMT"); //$NON-NLS-1$
        if (indexGMT >= 0 && result.length() - indexGMT >= 9 &&
            (result.charAt(indexGMT + 3) == '+' || result.charAt(indexGMT + 3) == '-') &&
            result.charAt(indexGMT + 4) >= '0' && result.charAt(indexGMT + 4) <= '9' &&
            result.charAt(indexGMT + 5) >= '0' && result.charAt(indexGMT + 5) <= '9' &&
            result.charAt(indexGMT + 6) == ':' && result.charAt(indexGMT + 7) >= '0' &&
            result.charAt(indexGMT + 7) <= '9' && result.charAt(indexGMT + 8) >= '0' &&
            result.charAt(indexGMT + 8) <= '9')
            result.delete(indexGMT, indexGMT + 3);
        return result;
    }

    /**
     * Formats an object as an <code>oracle.sql.TIMESTAMP</code> into a date/time string.
     * Sets field position if needed.
     *
     * @param obj the object to be formatted.
     * @param toAppendTo the string buffer to be appended with the formatted
     *        date/time string.
     * @param pos to be used to get offsets of a given field in the returned
     *        string buffer. On input, the alignment field of which the
     *        offsets are to be returned. On output, the offsets of the alignment
     *        field in the returned string buffer.
     *
     * @return the string buffer appended with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        if (obj == null || obj instanceof TIMESTAMPTZ) {
            return format((TIMESTAMPTZ)obj, toAppendTo, pos);
        } else {
            throw new IllegalArgumentException(Messages.getString("OraTIMESTAMPTZFormat.70")); //$NON-NLS-1$
        }
    }

    /**
     * Parses a date/time string into a <code>TIMESTAMPTZ</code> object,
     * starting from the given parse position.
     *
     * Due to the restrictions of the OraSimpleDateFormat class, the returned
     * TIMESTAMPTZ is in the externally specified Java time zone and not in
     * the time zone specified by the user.
     *
     * @param text the date/time string
     * @param pos indicates where to start the parsing on input. On output,
     *        returns where the parse ends if parsing succeeds, or the start
     *        index if it fails.
     * @param tz time zone to put into the returned TIMESTAMPTZ; null=default time zone
     *
     * @return a <code>TIMESTAMPTZ</code> object in <code>tz</code> time zone
     *
     * @exception IllegalArgumentException if the format pattern associated
     *            with this formatter is not valid for parsing or the given
     *            date/time string  cannot be parsed into a <code>TIMESTAMPTZ</code>
     *            object
     */
    public TIMESTAMPTZ parse(String text, ParsePosition pos) {
        // TIMESTAMPLTZ time zone is parsed from text and stored in UTC - default to UTC
        OraTIMESTAMPTZ temporalDatum = (OraTIMESTAMPTZ)parseTemporalDatum(text, pos, UTC, true);
        
        return (temporalDatum != null) ? temporalDatum.getDatum() : null;
    }
    
    protected OraTemporalDatum getTemporalDatum(Calendar calendar, int nanos) {
        return OraTIMESTAMPTZ.getInstance(calendar, nanos);
    }
}

