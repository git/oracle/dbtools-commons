/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.nls;

import java.sql.Connection;
import java.sql.Timestamp;

import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.datatypes.objects.OraTemporalDatum;

import oracle.i18n.text.OraDateFormat;
import oracle.i18n.text.OraSimpleDateFormat;
import oracle.i18n.util.GDKMessage;
import oracle.i18n.util.OraLocaleInfo;

import oracle.sql.Datum;

public abstract class OraTemporalDatumFormat extends java.text.Format implements OraConstants {
    /* private variables */
    private OraSimpleDateFormatDelegate delegate;
    private Connection connection = null;
    
    /* private variable to hold the session time zone */
    private TimeZone sessionTimeZone;


    /* Static variables */
    final protected static TimeZone UTC = TimeZone.getTimeZone("UTC"); //$NON-NLS-1$

    public static final int ERA_FIELD = OraDateFormat.ERA_FIELD;

    public static final int YEAR_FIELD = OraDateFormat.YEAR_FIELD;

    public static final int MONTH_FIELD = OraDateFormat.MONTH_FIELD;

    public static final int DATE_FIELD = OraDateFormat.DATE_FIELD;

    public static final int HOUR_OF_DAY_FIELD = OraDateFormat.HOUR_OF_DAY_FIELD;

    public static final int MINUTE_FIELD = OraDateFormat.MINUTE_FIELD;

    public static final int SECOND_FIELD = OraDateFormat.SECOND_FIELD;

    public static final int MILLISECOND_FIELD = OraDateFormat.MILLISECOND_FIELD;

    public static final int DAY_OF_WEEK_FIELD = OraDateFormat.DAY_OF_WEEK_FIELD;

    public static final int DAY_OF_YEAR_FIELD = OraDateFormat.DAY_OF_YEAR_FIELD;

    public static final int WEEK_OF_YEAR_FIELD = OraDateFormat.WEEK_OF_YEAR_FIELD;

    public static final int WEEK_OF_MONTH_FIELD = OraDateFormat.WEEK_OF_MONTH_FIELD;

    public static final int AM_PM_FIELD = OraDateFormat.AM_PM_FIELD;

    public static final int HOUR_FIELD = OraDateFormat.HOUR_FIELD;

    public static final int TIMEZONE_FIELD = OraDateFormat.TIMEZONE_FIELD;

    public static final int ALL_FIELD = OraDateFormat.ALL_FIELD;

    public static final int CENTRY_FIELD = OraDateFormat.CENTRY_FIELD;

    public static final int JULIAN_DAY_FIELD = OraDateFormat.JULIAN_DAY_FIELD;

    public static final int QUARTER_OF_YEAR_FIELD = OraDateFormat.QUARTER_OF_YEAR_FIELD;

    public static final int SECONDS_FIELD = OraDateFormat.SECONDS_FIELD;

    public static final int TEXT_FIELD = OraDateFormat.TEXT_FIELD;

    public static final int ISO_YEAR_FIELD = OraDateFormat.ISO_YEAR_FIELD;

    public static final int ISO_WEEK_OF_YEAR_FIELD = OraDateFormat.ISO_WEEK_OF_YEAR_FIELD;

    protected static boolean areEqual(Object obj1, Object obj2) {
        return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
    }

    private static class OraDateFormatPattern {
        /* Timezone range */
        private final static int TZMAXMINUTE = 59;
        private final static int TZMAXHOUR = 14;
        private final static int TZMINMINUTE = -59;
        private final static int TZMINHOUR = -12;

        /*
         * Format code
         */
        /* Timezone hour offset */
        final static char TZH = 0;
        /* Timezone minute offset */
        final static char TZM = 1;
        /* Timezone abbreivation */
        final static char TZD = 2;
        /* Timezone region ID */
        final static char TZR = 3;

        /*
         * Format property for alphabetical field
         */
        /* Indicates a alphabetical field */
        final static char ALPHA = 128;
        /* Signals format code that needn't be justified */
        final static char FNJUS = 64;
        /* Can there be a '-' in the input */
        final static char NEG = 32;

        /* Index for error code */
        final static int FORMAT_ATTR_ERR = 0;
        /* Index for the minimum value */
        final static int FORMAT_ATTR_LOW = 1;
        /* Index for the maximum value */
        final static int FORMAT_ATTR_HIGH = 2;
        /* Index for the length/flags */
        final static int FORMAT_ATTR_LEN = 3;
        /* Index for the field position */
        final static int FORMAT_FIELD = 4;

        /* Format attribute table */
        final static int[][] FORMAT_ATTR =
        /* error, low,  high,    length,  Field Position */
        { 
            { GDKMessage.TZHOUR, TZMINHOUR, TZMAXHOUR, 3 | NEG, OraDateFormat.MILLISECOND_FIELD }, /* TZH */
            { GDKMessage.TZMINUTE, TZMINMINUTE, TZMAXMINUTE, 2, OraDateFormat.MILLISECOND_FIELD }, /* TZM */
            { GDKMessage.ETZ, 0, 12, 6 | ALPHA | FNJUS, OraDateFormat.MILLISECOND_FIELD },         /* TZD */
            { GDKMessage.ETZ, 0, 64, 32 | NEG | ALPHA | FNJUS, OraDateFormat.MILLISECOND_FIELD }   /* TZR */
        } ;
    }

    private static class OraDateFormatSymbols {
        private OraLocaleInfo localeInfo;

        public OraDateFormatSymbols(OraLocaleInfo localeInfo) {
            this.localeInfo = localeInfo;
        }

        public char getGroupingSeparator() {
            return localeInfo.getOraTerritory().getGroupSeparator().charAt(0);
        }
    }

    private class OraSimpleDateFormatDelegate extends OraSimpleDateFormat implements Cloneable {
        /* Indicats the field has not been assigned a value */
        private final static int FIELD_NULL = (1 << 31);

        /* milliseconds per hour */
        private final static int MILLISECONDS_PER_HOUR = 60 * 60 * 1000;

        /* milliseconds per minute */
        private final static int MILLISECONDS_PER_MINUTE = 60 * 1000;

        /* private variables */
        private OraLocaleInfo localeInfo;
        private String savedPattern = null;
        private OraDateFormatSymbols symbols;

        public OraSimpleDateFormatDelegate(String pattern,
                                           OraLocaleInfo localeInfo) throws ParseException {
            super(pattern, localeInfo);

            this.savedPattern = pattern;
            this.localeInfo = localeInfo;
            this.symbols = new OraDateFormatSymbols(localeInfo);
        }

        /**
         * Overrides the <code>clone</code> method.
         *
         * @return a cloned object
         */
        @Override
        public Object clone() {
            OraSimpleDateFormatDelegate osdf = (OraSimpleDateFormatDelegate)super.clone();
            osdf.localeInfo = (OraLocaleInfo)localeInfo.clone();
            osdf.savedPattern = savedPattern;

            return osdf;
        }

        public Calendar shareCalendar() {
            return super.calendar;
        }

        public void applyTimeZone(TimeZone value) {
            super.calendar.setTimeZone(value);
        }

        public void applyTimeInMillis(long millis) {
            super.calendar.setTimeInMillis(millis);
        }

        /**
         * Formats a <code>Date</code> object into a date/time string.
         * Sets field position if needed.
         *
         * @param date the date/time object to be formatted.
         * @param toAppendTo the string buffer to be appended with the formatted
         *        date/time string.
         * @param pos to be used to get offsets of a given field in the returned
         *        string buffer. On input, the alignment field of which the
         *        offsets are to be returned. On output, the offsets of the alignment
         *        field in the returned string buffer.
         *
         * @return the string buffer appended with the formatted date/time string
         *
         * @throws IllegalArgumentException if any error occurs in the format
         *         operation
         */
        public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition pos) {
            return format(savedPattern, date, toAppendTo, pos);
        }

        /**
         * Formats a <code>Date</code> object into a date/time string.
         * Sets field position if needed.
         *
         * @param date the date/time object to be formatted.
         * @param toAppendTo the string buffer to be appended with the formatted
         *        date/time string.
         * @param pos to be used to get offsets of a given field in the returned
         *        string buffer. On input, the alignment field of which the
         *        offsets are to be returned. On output, the offsets of the alignment
         *        field in the returned string buffer.
         *
         * @return the string buffer appended with the formatted date/time string
         *
         * @throws IllegalArgumentException if any error occurs in the format
         *         operation
         */
        public StringBuffer format(String pattern, Date date, StringBuffer toAppendTo, FieldPosition pos) {
            try {
                applyFormatPattern(pattern);
            } catch (ParseException pe) {
                throw new IllegalArgumentException(Messages.getString("OraSimpleDATEFormat.61"),
                                                   pe); //$NON-NLS-1$
            }
            return super.format(date, toAppendTo, pos);
        }

        /**
         * Parses a date/time string into a <code>Date</code> object,
         * starting from the given parse position.
         *
         * @param text the date/time string
         * @param pos indicates where to start the parsing on input. On output,
         *        returns where the parse ends if parsing succeeds, or the start
         *        index if it fails.
         *
         * @return a <code>Date</code> object
         *
         * @exception IllegalArgumentException if the format pattern associated
         *            with this formatter is not valid for parsing or the given
         *            date/time string  cannot be parsed into a <code>Date</code>
         *            object
         */
        public Date parse(String text, ParsePosition pos) {
            return parse(savedPattern, text, pos);
        }

        public TimeZone reparseforTimeZone(String text, ParsePosition pos) {
            return reparseforTimeZone(savedPattern, text, pos);
        }
        
        /**
         * Parses a date/time string into a <code>Date</code> object,
         * starting from the given parse position.
         *
         * @param text the date/time string
         * @param pos indicates where to start the parsing on input. On output,
         *        returns where the parse ends if parsing succeeds, or the start
         *        index if it fails.
         *
         * @return a <code>Date</code> object
         *
         * @exception IllegalArgumentException if the format pattern associated
         *            with this formatter is not valid for parsing or the given
         *            date/time string  cannot be parsed into a <code>Date</code>
         *            object
         */
        public Date parse(String pattern, String text, ParsePosition pos) {
            try {
                applyParsePattern(pattern);
            } catch (ParseException pe) {
                throw new IllegalArgumentException(Messages.getString("OraSimpleDATEFormat.62"),
                                                   pe); //$NON-NLS-1$
            }
            return super.parse(text, pos);
        }

        /**
         * Applies the given format pattern of this DATE formatter.
         *
         * Patterns containing time zone-related format elements are illegal.
         *
         * @param pattern the given format pattern
         *
         * @exception ParseException if the format pattern is invalid
         */

        protected final void gdkApplyPattern(String pattern) throws ParseException {
            String currentPattern;

            try {
                currentPattern = toPattern();
            } catch (NullPointerException npe) {
                // Possibly not initialized yet
                currentPattern = null;
            }

            if (!areEqual(pattern, currentPattern)) {
                applyPattern(pattern);
            }
        }
        
        public TimeZone reparseforTimeZone(String pattern, String text, ParsePosition pos) {
            {
                final String tzrRegEx = "((([\"][^\"]*[\"])|.)*)(TZR)";
                
                Pattern p = Pattern.compile(tzrRegEx);
                Matcher m = p.matcher(pattern);
                
                if (m.matches()) {
                    String badPattern = m.group(1) + "\"X\"TZR";
                    
                    try {
                        parse(badPattern, text, pos);
                    } catch (Exception e) {
                        int errIndex = pos.getErrorIndex();
                        ParsePosition tzrPos = new ParsePosition(errIndex);
                        
                        String tzId = (text.charAt(errIndex) == '+' || text.charAt(errIndex) == '-') 
                            ? getTZOffsetID(text, text.length(), tzrPos)
                            : getTimeZoneID(text, text.length(), tzrPos);
                        
                        if (tzId != null) {
                            return TimeZone.getTimeZone(tzId);
                        }
                    }
                }
            }
            
            {
                final String tzhmRegEx = "((([\"][^\"]*[\"])|.)*)(TZH[:]?TZM)";
                
                Pattern p = Pattern.compile(tzhmRegEx);
                Matcher m = p.matcher(pattern);
                
                if (m.matches()) {
                    String badPattern = m.group(1) + "\"X\"" + m.group(4);
                    
                    try {
                        parse(badPattern, text, pos);
                    } catch (Exception e) {
                        ParsePosition tzrPos = new ParsePosition(pos.getErrorIndex());
                        
                        String tzId = getTZOffsetID(text, text.length(), tzrPos);
                        
                        if (tzId != null) {
                            return TimeZone.getTimeZone(tzId);
                        }
                    }
                }
            }
            
            return null;
        }

        /*
         * Searches for timezone ID.
         * Returns null if no time zone ID found, timeZone ID otherwise.
         */

        private String getTimeZoneID(String text, int tlen, ParsePosition pos) {
            String[] tzIDs = TimeZone.getAvailableIDs();
            int ti = pos.getIndex();

            for (int i = 0; i < tzIDs.length; i++) {
                int tzlen = tzIDs[i].length();

                if (((ti + tzlen) <= tlen) && text.regionMatches(true, ti, tzIDs[i], 0, tzlen)) {
                    pos.setIndex(ti + tzlen);

                    return tzIDs[i];
                }
            }

            return null;
        }

        /*
         * Searches for timezone ID.
         * Returns null if no time zone ID found, timeZone ID otherwise.
         */

        private String getTZOffsetID(String text, int tlen, ParsePosition pos) {
            int ti = pos.getIndex();
            
            if (text.charAt(ti+3) == ':') {
                return "GMT" + text.substring(ti, ti+6);
            } else {
                return "GMT" + text.substring(ti, ti+3) + ":" + text.substring(ti+3, ti+5);
            }
        }

        /*
         * Searches for time zone abbreviation.
         * Returns null if no time zone abbreviation found,
         * time zone abbreviation otherwise.
         */

        private String getTimeZoneAbbr(String text, int tlen, ParsePosition pos) {
            int ti = pos.getIndex();

            if ((ti + 3) > tlen) {
                return null;
            } else {
                pos.setIndex(ti + 3);

                return text.substring(ti, ti + 3);
            }
        }

        /*
         * Set timezone for the given calendar in the given locale by using
         * the given timezone id and abbr.
         * Returns true if success, false otherwise.
         */

        private boolean setTimeZone(String id, String abbr, Calendar cal, Locale loc) {
            TimeZone tz;

            if (id != null) {
                tz = TimeZone.getTimeZone(id);
                cal.setTimeZone(tz);
            } else {
                tz = cal.getTimeZone();
            }

            /* checking conflict */
            if (abbr != null) {
                if (tz.getDisplayName(true, TimeZone.SHORT, loc).equals(abbr) &&
                    !tz.inDaylightTime(cal.getTime())) {
                    return false;
                }

                if (tz.getDisplayName(false, TimeZone.SHORT, loc).equals(abbr) &&
                    tz.inDaylightTime(cal.getTime())) {
                    return false;
                }
            }

            return true;
        }

        private int getTimeZoneOffset(String text, int tlen, ParsePosition pos, int fc, boolean fx,
                                      boolean fm) {
            int offset = FIELD_NULL;
            int ti = pos.getIndex();
            boolean tzm_only = false;
            char ch;

            if (ti >= tlen) {
                return FIELD_NULL;
            }

            ch = text.charAt(ti);

            if (ch == ':') {
                if (++ti >= tlen) {
                    return FIELD_NULL;
                }

                pos.setIndex(ti);
                ch = text.charAt(ti);
                tzm_only = true;
            }

            if (Character.isDigit(ch) || (((ti + 2) <= tlen) && ((ch == '+') || (ch == '-')))) {
                // [+|-]HH:MI
                int hr;
                int min;
                boolean isNegative;

                hr = min = offset = 0;

                if (ch == '-') {
                    isNegative = true;
                    ti++;
                } else {
                    isNegative = false;

                    if (ch == '+') {
                        ti++;
                    }
                }

                pos.setIndex(ti);

                if ((hr = toNumber(text, tlen, pos, 2, false, fx, fm)) >= 0) {
                    int[] fa;

                    if (tzm_only) {
                        min = hr;
                        fa = OraTemporalDatumFormat.OraDateFormatPattern.FORMAT_ATTR[OraTemporalDatumFormat.OraDateFormatPattern.TZM];

                        if ((isNegative && (-min < fa[OraTemporalDatumFormat.OraDateFormatPattern.FORMAT_ATTR_LOW])) ||
                            (!isNegative && (min > fa[OraTemporalDatumFormat.OraDateFormatPattern.FORMAT_ATTR_HIGH]))) {
                            return FIELD_NULL;
                        }

                        offset = min * MILLISECONDS_PER_MINUTE;
                    } else {
                        fa = OraTemporalDatumFormat.OraDateFormatPattern.FORMAT_ATTR[OraTemporalDatumFormat.OraDateFormatPattern.TZH];

                        if ((isNegative && (-hr < fa[OraTemporalDatumFormat.OraDateFormatPattern.FORMAT_ATTR_LOW])) ||
                            (!isNegative && (hr > fa[OraTemporalDatumFormat.OraDateFormatPattern.FORMAT_ATTR_HIGH]))) {
                            return FIELD_NULL;
                        }

                        offset = hr * MILLISECONDS_PER_HOUR;

                        ti = pos.getIndex();

                        if ((ti < tlen) && (text.charAt(ti) == ':')) {
                            pos.setIndex(++ti);
                        }

                        if ((ti < tlen) && (text.charAt(ti) == '+')) {
                            pos.setIndex(++ti);
                        }

                        if ((ti < tlen) &&
                            ((min = toNumber(text, tlen, pos, 2, false, fx, fm)) >= 0)) {
                            fa = OraTemporalDatumFormat.OraDateFormatPattern.FORMAT_ATTR[OraTemporalDatumFormat.OraDateFormatPattern.TZM];

                            if ((min < fa[OraTemporalDatumFormat.OraDateFormatPattern.FORMAT_ATTR_LOW]) ||
                                (min > fa[OraTemporalDatumFormat.OraDateFormatPattern.FORMAT_ATTR_HIGH])) {
                                return FIELD_NULL;
                            }

                            offset += (min * MILLISECONDS_PER_MINUTE);
                        }
                    }
                }

                if (isNegative) {
                    offset = -offset;
                }
            }

            return offset;
        }

        /*
         * Converts a (non-negative) numeric string into an integer.
         * Grouping separator may be included.
         * Returns converted numeric value if success, -(error code) otherwise.
         */

        private int toNumber(String text, int tlen, ParsePosition pos, int flen, boolean sep,
                             boolean fx, boolean fm) {
            int ti; // text index
            int fi; // text index
            int err; // error
            int num;
            char ch;

            ti = pos.getIndex();
            ch = text.charAt(ti);

            if (!Character.isDigit(ch)) {
                return (-GDKMessage.NEXP);
            }

            num = Character.digit(ch, 10);
            fi = 1;
            ti++;

            while ((fi < flen) && (ti < tlen)) {
                ch = text.charAt(ti);

                if (ch == symbols.getGroupingSeparator()) {
                    if (!sep) {
                        break;
                    }
                } else if (Character.isDigit(ch)) {
                    num = (num * 10) + Character.digit(ch, 10);
                } else {
                    break;
                }

                fi++;
                ti++;
            }

            if (fx && !fm && (fi != flen)) {
                return (-GDKMessage.BNDG);
            }

            pos.setIndex(ti);

            return num;
        }
    }

    /* Protected variables */

    public OraTemporalDatumFormat(String pattern, OraLocaleInfo localeInfo, TimeZone sessionTimeZone) throws ParseException {
        this.delegate = new OraSimpleDateFormatDelegate(pattern, localeInfo);
        this.sessionTimeZone = sessionTimeZone;

        // Reapply pattern - can be modified by subclass
        applyPattern(pattern);
    }

    public final OraLocaleInfo getOraLocaleInfo() {
        return delegate.localeInfo;
    }

    public final Locale getLocale() {
        return delegate.localeInfo.getLocale();
    }

    public final void setConnection(Connection connection) {
        this.connection = connection;
    }

    public final Connection getConnection() {
        return this.connection;
    }

    protected final String getSavedPattern() {
        return delegate.savedPattern;
    }

    protected TimeZone getSessionTimeZone() {
        return this.sessionTimeZone;
    }
    
    protected abstract OraTemporalDatum getTemporalDatum(Calendar calendar, int nanos);

    /**
     * Overrides the <code>clone</code> method.
     *
     * @return a cloned object
     */
    @Override
    public Object clone() {
        OraTemporalDatumFormat osdf = (OraTemporalDatumFormat)super.clone();
        osdf.delegate = (OraSimpleDateFormatDelegate)delegate.clone();
        osdf.connection = connection;
        return osdf;
    }

    /**
     * Applies the given format pattern of this DATE formatter.
     *
     * Patterns containing time zone-related format elements are illegal.
     *
     * @param pattern the given format pattern
     *
     * @exception ParseException if the format pattern is invalid
     */

    public void applyPattern(String pattern) throws ParseException {
        delegate.gdkApplyPattern(pattern);
        delegate.savedPattern = pattern;
    }

    /**
     * Returns the format pattern associated with this date/time formatter.
     *
     * @return a format pattern string
     */
    public String toPattern() {
        return getSavedPattern();
    }

    /**
     * Returns the format pattern associated with this date/time formatter.
     *
     * @return a format pattern string
     */
    protected String getLastPattern() {
        return delegate.toPattern();
    }

    public Calendar getCalendar() {
        return delegate.getCalendar(); // TODO: Review usage
    }

    /**
     * Applies the given format pattern of this DATE formatter.
     *
     * Patterns containing time zone-related format elements are illegal.
     *
     * @param pattern the given format pattern
     *
     * @exception ParseException if the format pattern is invalid
     */

    protected void applyParsePattern(String pattern) throws ParseException {
        String uc = pattern.toUpperCase(Locale.US);
        StringBuffer pat = new StringBuffer(pattern);
        int pos = -1;
        int i;
        int cnt;

        // Workaround for bug #5002915 - convert SP and TH to spaces
        while ((pos = uc.indexOf("SP", pos + 1)) >= 0) { //$NON-NLS-1$
            i = -1;
            cnt = 0;
            while ((i = uc.indexOf('"', i + 1)) < pos && i != -1)
                ++cnt;
            if (cnt % 2 == 0) {
                // even number of quotes - SP is not in quoted text
                throw new ParseException(Messages.getString("OraSimpleDATEFormat.63"),
                                         pos); //$NON-NLS-1$
            }
        }

        pos = -1;
        while ((pos = uc.indexOf("TH", pos + 1)) >= 0) { //$NON-NLS-1$
            if (pos > 2 && uc.substring(pos - 3, pos + 2).equals("MONTH")) //$NON-NLS-1$
                continue; // ignore MONTH
            i = -1;
            cnt = 0;
            while ((i = uc.indexOf('"', i + 1)) < pos && i != -1)
                ++cnt;
            if (cnt % 2 == 0) {
                // even number of quotes - TH is not in quoted text
                throw new ParseException(Messages.getString("OraSimpleDATEFormat.63"),
                                         pos); //$NON-NLS-1$
            }
        }

        delegate.gdkApplyPattern(pat.toString());
    }

    /**
     * Applies the given format pattern of this DATE formatter.
     *
     * Patterns containing time zone-related format elements are illegal.
     *
     * @param pattern the given format pattern
     *
     * @exception ParseException if the format pattern is invalid
     */

    protected void applyFormatPattern(String pattern) throws ParseException {
        delegate.gdkApplyPattern(pattern);
    }

    /**
     * Returns a Datum.
     */
    public abstract Datum parse(String source, ParsePosition parsePosition);

    /**
     * Parses text from the beginning of the given string to produce a number.
     * The method may not use the entire text of the given string.
     * <p>
     * See the {@link #parse(String, ParsePosition)} method for more information
     * on number parsing.
     *
     * @param source A <code>String</code> whose beginning should be parsed.
     * @return A <code>Number</code> parsed from the string.
     * @exception ParseException if the beginning of the specified string
     *            cannot be parsed.
     */
    public Datum parse(String source) throws ParseException {
        return parse(source, new ParsePosition(0));
    }

    /**
     * Parses text from a string to produce a <code>Datum</code>.
     * <p>
     * @param source A <code>String</code>, part of which should be parsed.
     * @param pos A <code>ParsePosition</code> object with index and error
     *            index information as described above.
     * @return A <code>Datum</code> parsed from the string. In case of
     *         error, returns null.
     * @exception NullPointerException if <code>pos</code> is null.
     */
    public final Datum parseObject(String source, ParsePosition pos) {
        return parse(source, pos);
    }

    /**
     * Parses a date/time string into a <code>DATE</code> object,
     * starting from the given parse position.
     *
     * @param text the date/time string
     * @param pos indicates where to start the parsing on input. On output,
     *        returns where the parse ends if parsing succeeds, or the start
     *        index if it fails.
     *
     * @return a <code>DATE</code> object
     *
     * @exception IllegalArgumentException if the format pattern associated
     *            with this formatter is not valid for parsing or the given
     *            date/time string  cannot be parsed into a <code>TIMESTAMP</code>
     *            object
     */
    protected OraTemporalDatum parseTemporalDatum(String text, ParsePosition pos,
                                                  TimeZone prepareTimeZone,
                                                  boolean parseTimezone) {
        if (text != null && text.length() > 0) {
            ParsePosition savedPos = new ParsePosition(pos.getIndex());
            
            // Initialize parser time zone
            prepareToParse(prepareTimeZone);
    
            // Parse text - Timestamp return if there are fractional seconds
            Date date = delegate.parse(text, pos);
    
            // Extract time in millis
            long time = date.getTime();
    
            // Extract nanos where available
            int nanos = (date instanceof Timestamp) ? ((Timestamp)date).getNanos() : 0;
    
            // Extract TimeZone
            TimeZone timeZone = (parseTimezone)
                ? delegate.reparseforTimeZone(text, savedPos)
                : prepareTimeZone;
            
            // Construct calendar based on parsed time at required timezone
            Calendar cal = getDatumParseCalendar(time, timeZone);
    
            // Build Temporal Datum of correct type
            return getTemporalDatum(cal, nanos);
        } else {
            return null;
        }
    }

    protected OraTemporalDatum parseTemporalDatum(String text, ParsePosition pos,
                                                  TimeZone parseTimeZone) {
        return parseTemporalDatum(text, pos, parseTimeZone, false);
    }

    private void prepareToParse(TimeZone timezone) {
        // Set time zone
        delegate.applyTimeZone(timezone);

        // Initialize the reference date to current time
        delegate.applyTimeInMillis((new Date()).getTime());
    }

    /**
     * Formats an <code>oracle.sql.DATE</code> object into a date/time string.
     * Sets field position if needed.
     *
     * @param date the DATE object to be formatted.
     * @param toAppendTo the string buffer to be appended with the formatted
     *        date/time string.
     * @param pos to be used to get offsets of a given field in the returned
     *        string buffer. On input, the alignment field of which the
     *        offsets are to be returned. On output, the offsets of the alignment
     *        field in the returned string buffer.
     *
     * @return the string buffer appended with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    protected StringBuffer formatTemporalDatum(OraTemporalDatum temporalDatum,
                                               StringBuffer toAppendTo, FieldPosition pos) {
        if (temporalDatum != null) {
            // Prepare formatter time zone
            prepareToFormat(temporalDatum.getTimeZone());
    
            // format
            return delegate.format(temporalDatum.toTimestamp(), toAppendTo, pos);
        } else {
            return toAppendTo;
        }
    }

    private void prepareToFormat(TimeZone timezone) {
        // Set time zone
        delegate.applyTimeZone(timezone);
    }

    /**
     * Formats an <code>oracle.sql.DATE</code> object into a date/time string.
     * Sets field position if needed.
     *
     * @param date the DATE object to be formatted.
     *
     * @return the string buffer appended with the formatted date/time string
     *
     * @throws IllegalArgumentException if any error occurs in the format
     *         operation
     */

    protected Calendar getDatumFormatCalendar(TimeZone timezone) {
        // use the same locale calendar
        Calendar cal = (Calendar)delegate.shareCalendar().clone();

        // clear calendar
        cal.clear();

        // Initialize to Datum time zome
        cal.setTimeZone(timezone);

        return cal;
    }

    protected Calendar getDatumParseCalendar(long time, TimeZone timezone) {
        // Create Datum parse calendar forced to Gregorian calendar
        Calendar cal = Calendar.getInstance(timezone, Locale.US);

        // Initialize to required time
        cal.setTimeInMillis(time);

        return cal;
    }
}
