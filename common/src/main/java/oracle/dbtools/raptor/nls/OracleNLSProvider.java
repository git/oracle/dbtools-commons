/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.nls;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.Format;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.MetaResource;
import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.db.VersionTracker;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.raptor.query.QueryXMLSupport;
import oracle.dbtools.raptor.utils.NLSUtils;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import oracle.sql.BINARY_DOUBLE;
import oracle.sql.BINARY_FLOAT;
import oracle.sql.DATE;
import oracle.sql.Datum;
import oracle.sql.INTERVALDS;
import oracle.sql.INTERVALYM;
import oracle.sql.NUMBER;
import oracle.sql.OPAQUE;
import oracle.sql.TIMESTAMP;
import oracle.sql.TIMESTAMPLTZ;
import oracle.sql.TIMESTAMPTZ;

public class OracleNLSProvider extends DefaultNLSProvider {

    private static final String NLS_COMP = "NLS_COMP"; //$NON-NLS-1$

    private static final String NLS_SORT = "NLS_SORT"; //$NON-NLS-1$

    private static final String NLS_LENGTH_SEMANTICS = "NLS_LENGTH_SEMANTICS"; //$NON-NLS-1$

    private static final String NLS_ISO_CURRENCY = "NLS_ISO_CURRENCY"; //$NON-NLS-1$

    private static final String NLS_CURRENCY = "NLS_CURRENCY"; //$NON-NLS-1$

    private static final String NLS_DATE_LANGUAGE = "NLS_DATE_LANGUAGE"; //$NON-NLS-1$

    // private static final String NLS_CALENDAR = "NLS_CALENDAR";

    private static final String SESSION_TIMEZONE_OFFSET = "SESSION_TIMEZONE_OFFSET"; //$NON-NLS-1$

    private static final String SESSION_TIMEZONE = "SESSION_TIMEZONE"; //$NON-NLS-1$

    private static final String NLS_DATE_FORMAT = "NLS_DATE_FORMAT"; //$NON-NLS-1$

    private static final String NLS_NUMERIC_CHARACTERS = "NLS_NUMERIC_CHARACTERS"; //$NON-NLS-1$

    private static final String NLS_CHARACTERSET = "NLS_CHARACTERSET"; //$NON-NLS-1$

    private static final String NLS_TIMESTAMP_TZ_FORMAT = "NLS_TIMESTAMP_TZ_FORMAT"; //$NON-NLS-1$

    private static final String DB_TIMEZONE = "DB_TIMEZONE"; //$NON-NLS-1$

    private static final String NLS_TERRITORY = "NLS_TERRITORY"; //$NON-NLS-1$

    private static final String NLS_LANGUAGE = "NLS_LANGUAGE"; //$NON-NLS-1$

    private static final String NLS_TIMESTAMP_FORMAT = "NLS_TIMESTAMP_FORMAT"; //$NON-NLS-1$
    
    private static final String GENERIC_TIMESTAMP_TZ_FORMAT = "YYYY-MM-DD HH24:MI:SS.FF TZR";
    private static final String GENERIC_TIMESTAMP_FORMAT = "YYYY-MM-DD HH24:MI:SS.FF";
    private static final String GENERIC_DATE_FORMAT = "YYYY-MM-DD HH24:MI:SS";

    private static final String LOADER_TIMESTAMP_TZ_FORMAT = "YYYY-MM-DD HH24:MI:SS.FF TZH:TZM";
    private static final String LOADER_TIMESTAMP_FORMAT = GENERIC_TIMESTAMP_FORMAT;
    private static final String LOADER_DATE_FORMAT = GENERIC_DATE_FORMAT;

    private static final String REST_DATE_FORMAT = "YYYY-MM-DD\"T\"HH24:MI:SS\"Z\"";
    private static final String REST_TIMESTAMP_FORMAT = "YYYY-MM-DD\"T\"HH24:MI:SS.FF\"Z\"";
    
    private static final String REST_TIMESTAMP_TZ_FORMAT = REST_TIMESTAMP_FORMAT; // "YYYY-MM-DD\"T\"HH24:MI:SS.FF\"\"TZH:TZM";
    private static final String REST_TIMESTAMP_LTZ_FORMAT = REST_TIMESTAMP_FORMAT; // "YYYY-MM-DD\"T\"HH24:MI:SS.FF";

    // Fix bug: 8249942 - SQLDEV1.5.4:NLS:PREFERENCE: NLS_TIMESTAMP_FORMAT NOT SUPPORT "TEXT"
    private static final String ALTER_SESSION = "alter session set {0}=''{1}''"; //$NON-NLS-1$

    private static final Logger LOGGER = Logger.getLogger(OracleNLSProvider.class.getName());

    private Map<String, String> nlsMappings = null;

    private Map<String, String> s_defaults = new HashMap<String, String>();

    private static final Map<String, Object> NO_BINDS = Collections.emptyMap();
    
    public OracleNLSProvider(Connection conn) {
        super(conn);
    }
    
    private final class SettingsApplicator {
        final DBUtil mUtil;
        final Connection mConn;
        
        SettingsApplicator(Connection conn) {
            this.mConn = conn;
            mUtil = DBUtil.getInstance(conn);
        }
        
        /**
         * @param setting
         * @param param
         * @return
         */
        private boolean apply(String setting, String param) {
            boolean appliedSomething = false;
            String pref = getDefault(setting);
            if (requiresSetting(setting, param)) {
                String q = getAlterSession(param, pref);
                mUtil.execute(q, NO_BINDS);
                appliedSomething = true;
            }
            return appliedSomething;
        }

        /**
         * @param timezone
         * @return
         */
        private boolean apply(TimeZone timezone) {
            try {
                String tz = timezone.getID();
                if (tz != null) {
                    // Remove GMT prefix
                    if (tz.length() > 3 && (tz.startsWith("GMT+") || tz.startsWith("GMT-"))) {
                        tz = tz.substring(3);
                    }

                    ((OracleConnection) mConn).setSessionTimeZone(tz);

                    return true;
                }
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            }

            return false;
        }

        DBUtil getUtil() {
            return mUtil;
        }
    }

    private char convertToChar(String s) {
        if ( s == null || s.length() == 0 ) {
            return 0;
        } else {
            return s.charAt(0);
        }
    }
    
    // //
    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#format(java.lang.Object)
     */
    public String format(Object obj) {
        String nullDisplay = yieldNullDisplay(obj);

        String ret;
        
        if (nullDisplay != null) {
            ret = nullDisplay;
        } else if (obj instanceof BigDecimal) {
            ret = formatBigDecimal((BigDecimal) obj);
        } else if (obj instanceof DATE) {
            ret = formatDate((DATE) obj);
        } else if (obj instanceof java.util.Date) {
            try {
                obj = new DATE(obj);
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            }
            ret = formatDate((DATE) obj);
        } else if (obj instanceof NUMBER) {
            ret = formatNUMBER((NUMBER) obj);
        } else if (obj instanceof BINARY_DOUBLE) {
            ret = formatBINARY_DOUBLE((BINARY_DOUBLE) obj);
        } else if (obj instanceof BINARY_FLOAT) {
            ret = formatBINARY_FLOAT((BINARY_FLOAT) obj);
        } else if (obj instanceof TIMESTAMP) {
            ret = formatTIMESTAMP((TIMESTAMP) obj);
        } else if (obj instanceof TIMESTAMPLTZ) {
            ret = formatTIMESTAMPLTZ((TIMESTAMPLTZ) obj);
        } else if (obj instanceof TIMESTAMPTZ) {
            ret = formatTIMESTAMPTZ((TIMESTAMPTZ) obj);
        } else if (obj instanceof INTERVALYM) {
            ret = formatINTERVALYM((INTERVALYM) obj);
        } else if (obj instanceof INTERVALDS) {
            ret = formatINTERVALDS((INTERVALDS) obj);
        } else {
            ret = super.format(obj);
        }
        
        return ret;
    }

    /**
     * @param bd
     * @return
     */
    private String formatBigDecimal(BigDecimal bd) {
        String ret = yieldNullDisplay(bd);
        
        if (ret == null) {
            // if no decimal don't format
            ret = bd.toPlainString();
            if (ret.indexOf('.') != -1) {
                // HashMap<String, String> nlsMap = getNLSMap(conn);
                // grab the separator
                char decimalSeparator = /*
                                         * new oracle.i18n.text.OraDecimalFormatSymbols(oracle.i18n.util.OraLocaleInfo .getInstance(nlsMap.get("NLS_LANGUAGE"),
                                         * nlsMap.get("NLS_TERRITORY"))).getDecimalSeparator()
                                         */
                // bug 5711442
                getDecimalSeparator();
                ret = bd.toPlainString();
                if (decimalSeparator != '.') ret = ret.replace('.', decimalSeparator);
            } // 
        }
        
        return ret;
    }

    /**
     * @param date
     * @return
     */
    public String formatDate(DATE date) {
        String ret = yieldNullDisplay(date);
        
        if (ret == null) {
            OraDATEFormat formatDate;
            try {
                formatDate = getOraDATEFormat();
            } catch (ParseException pe) {
                formatDate = null;
            }
            
            if (formatDate == null)
                ret = date.stringValue();
            else
                ret = formatDate.format(date);
        }
        
        return ret;
    }

    /**
     * @param num
     * @return
     */
    private String formatNUMBER(NUMBER num) {
        String ret = yieldNullDisplay(num);
        
        if (ret == null) {
            // if no decimal don't format
            ret = num.stringValue();
            if (ret.indexOf('.') != -1) {
                try {
                    ret = formatBigDecimal(num.bigDecimalValue());
                } catch (SQLException e) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                }
            } // 
        }
        
        return ret;
    }

    /**
     * @param num
     * @return
     */
    private String formatBINARY_DOUBLE(BINARY_DOUBLE num) {
        String ret = yieldNullDisplay(num);
        
        if (ret == null) {
            // if no decimal don't format
            ret = num.stringValue();
            if (ret.indexOf('.') != -1) {
                try {
                    ret = formatBigDecimal(getBigDecimal(num));
                } catch (SQLException e) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                }
            } //
        }
        
        return ret;
    }

    /**
     * @param num
     * @return
     */
    private String formatBINARY_FLOAT(BINARY_FLOAT num) {
        String ret = yieldNullDisplay(num);
        
        if (ret == null) {
            // if no decimal don't format
            ret = num.stringValue();
            if (ret.indexOf('.') != -1) {
                try {
                    ret = formatBigDecimal(getBigDecimal(num));
                } catch (SQLException e) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
                }
            } //
        }
        
        return ret;
    }

    /**
     * @param num
     * @return
     * @throws SQLException
     */
    private BigDecimal getBigDecimal(BINARY_FLOAT num) throws SQLException {
        return (num != null) ? new BigDecimal(num.stringValue()) : null;
    }

    /**
     * @param num
     * @return
     * @throws SQLException
     */
    private BigDecimal getBigDecimal(BINARY_DOUBLE num) throws SQLException {
        return (num != null) ? new BigDecimal(num.stringValue()) : null;
    }
/*
    private String formatDATE(DATE time) {
        String ret = null;
        OraDATEFormat formatDate;
        try {
            formatDate = getOraDATEFormat();
            ret = formatDate.format(time);
        } catch (ParseException e) {
            Map<String, String> nlsMap = getNLSMap();
            LOGGER.severe(Messages.getString("OracleNLSProvider.21") + nlsMap.get(NLS_DATE_FORMAT)); //$NON-NLS-1$
        }
        return ret;
    }
*/
    /**
     * @param time
     * @return
     */
    private String formatTIMESTAMP(TIMESTAMP time) {
        String ret = yieldNullDisplay(time);
        
        if (ret == null) {
            OraTIMESTAMPFormat formatTimestamp;
            try {
                formatTimestamp = getOraTIMESTAMPFormat();
                // Removed by RJW: Formatter no longer rewrites format
                // nlsMap.put(NLS_TIMESTAMP_FORMAT, formatTimestamp.toPattern());
                ret = formatTimestamp.format(time);
            } catch (ParseException e) {
                Map<String, String> nlsMap = getNLSMap();
                LOGGER.severe(Messages.getString("OracleNLSProvider.18") + nlsMap.get(NLS_TIMESTAMP_FORMAT)); //$NON-NLS-1$
            }
        }
        
        return ret;
    }

    /**
     * @param time
     * @return
     */
    private String formatTIMESTAMPLTZ(TIMESTAMPLTZ time) {
        String ret = null;
        OraTIMESTAMPLTZFormat formatTimestamp;
        try {
            formatTimestamp = getOraTIMESTAMPLTZFormat();
            ret = formatTimestamp.format(time);
        } catch (ParseException e) {
            Map<String, String> nlsMap = getNLSMap();
            LOGGER.severe(Messages.getString("OracleNLSProvider.19") + nlsMap.get(NLS_TIMESTAMP_FORMAT)); //$NON-NLS-1$
        }
        return ret;
    }

    /**
     * @param time
     * @return
     */
    private String formatTIMESTAMPTZ(TIMESTAMPTZ time) {
        String ret = yieldNullDisplay(time);
        
        if (ret == null) {
            OraTIMESTAMPTZFormat formatTimestampTZ;
            try {
                formatTimestampTZ = getOraTIMESTAMPTZFormat();
                 // Removed by RJW: Formatter no longer rewrites format
                 // nlsMap.put(NLS_TIMESTAMP_TZ_FORMAT, formatTimestampTZ.toPattern());
                ret = formatTimestampTZ.format(time);
            } catch (ParseException e) {
                Map<String, String> nlsMap = getNLSMap();
                LOGGER.severe(Messages.getString("OracleNLSProvider.20") + nlsMap.get(NLS_TIMESTAMP_TZ_FORMAT)); //$NON-NLS-1$
            }
        }
        
        return ret;
    }
    
    /**
     * @param loader date
     * @return
     */
    private String formatLoaderDATE(DATE time) {
        String ret = yieldNullDisplay(time);
        
        if (ret == null) {
            OraDATEFormat formatDate;
            try {
                formatDate = getOraDATEFormat(getDateFormat(FormatType.LOADER));
                ret = formatDate.format(time);
            } catch (ParseException e) {
                LOGGER.severe(Messages.getString("OracleNLSProvider.19") + LOADER_DATE_FORMAT); //$NON-NLS-1$
            }
        }
        
        return ret;
    }

    /**
     * @param loader timestamp
     * @return
     */
    private String formatLoaderTIMESTAMP(TIMESTAMP time) {
        String ret = yieldNullDisplay(time);
        
        if (ret == null) {
            OraTIMESTAMPFormat formatTimestamp;
            try {
                formatTimestamp = getOraTIMESTAMPFormat(getTimeStampFormat(FormatType.LOADER));
                ret = formatTimestamp.format(time);
            } catch (ParseException e) {
                LOGGER.severe(Messages.getString("OracleNLSProvider.19") + LOADER_TIMESTAMP_FORMAT); //$NON-NLS-1$
            }
        }
        
        return ret;
    }
    
    /**
     * @param loader timestamp with time zone
     * @return
     */
    private String formatLoaderTIMESTAMPTZ(TIMESTAMPTZ time) {
        String ret = yieldNullDisplay(time);
        
        if (ret == null) {
            OraTIMESTAMPTZFormat formatTimestamp;
            try {
                formatTimestamp = getOraTIMESTAMPTZFormat(getTimeStampWithTimeZoneFormat(FormatType.LOADER));
                ret = formatTimestamp.format(time);
            } catch (ParseException e) {
                LOGGER.severe(Messages.getString("OracleNLSProvider.19") + LOADER_TIMESTAMP_TZ_FORMAT); //$NON-NLS-1$
            }
        }
        
        return ret;
    }
    
    /**
     * @param loader timestamp with local time zone
     * Note: the mask is the same as TIMESTAMPTZ
     * @return
     */
    private String formatLoaderTIMESTAMPLTZ(TIMESTAMPLTZ time) {
        String ret = yieldNullDisplay(time);
        
        if (ret == null) {
            OraTIMESTAMPLTZFormat formatTimestamp;
            try {
                formatTimestamp = getOraTIMESTAMPLTZFormat(getTimeStampWithTimeZoneFormat(FormatType.LOADER));
                ret = formatTimestamp.format(time);
            } catch (ParseException e) {
                LOGGER.severe(Messages.getString("OracleNLSProvider.19") + LOADER_TIMESTAMP_TZ_FORMAT); //$NON-NLS-1$
            }
        }
        
        return ret;
    }
    
    /**
     * @param loader date
     * @return
     */
    private String formatRestDATE(DATE time) {
        String ret = yieldNullDisplay(time);
        
        if (ret == null) {
            OraDATEFormat formatDate;
            try {
                formatDate = getOraDATEFormat(getDateFormat(FormatType.REST));
                ret = formatDate.format(time);
            } catch (ParseException e) {
                LOGGER.severe(Messages.getString("OracleNLSProvider.19") + REST_DATE_FORMAT); //$NON-NLS-1$
            }
        }
        
        return ret;
    }

    /**
     * @param loader timestamp
     * @return
     */
    private String formatRestTIMESTAMP(TIMESTAMP time) {
        String ret = yieldNullDisplay(time);
        
        if (ret == null) {
            OraTIMESTAMPFormat formatTimestamp;
            try {
                formatTimestamp = getOraTIMESTAMPFormat(getTimeStampFormat(FormatType.REST));
                ret = formatTimestamp.format(time);
            } catch (ParseException e) {
                LOGGER.severe(Messages.getString("OracleNLSProvider.19") + REST_TIMESTAMP_FORMAT); //$NON-NLS-1$
            }
        }
        
        return ret;
    }
    
    /**
     * @param loader timestamp with time zone
     * @return
     */
    private String formatRestTIMESTAMPTZ(TIMESTAMPTZ time) {
        String ret = yieldNullDisplay(time);
        
        if (ret == null) {
            OraTIMESTAMPTZFormat formatTimestamp;
            try {
                formatTimestamp = getOraTIMESTAMPTZFormat(getTimeStampWithTimeZoneFormat(FormatType.REST));
                ret = formatTimestamp.format(time);
            } catch (ParseException e) {
                LOGGER.severe(Messages.getString("OracleNLSProvider.19") + REST_TIMESTAMP_TZ_FORMAT); //$NON-NLS-1$
            }
        }
        
        return ret;
    }
    
    /**
     * @param loader timestamp with local time zone
     * Note: the mask is the same as TIMESTAMPTZ
     * @return
     */
    private String formatRestTIMESTAMPLTZ(TIMESTAMPLTZ time) {
        String ret = yieldNullDisplay(time);
        
        if (ret == null) {
            OraTIMESTAMPLTZFormat formatTimestamp;
            try {
                formatTimestamp = getOraTIMESTAMPLTZFormat(getTimeStampWithLocalTimeZoneFormat(FormatType.REST));
                ret = formatTimestamp.format(time);
            } catch (ParseException e) {
                LOGGER.severe(Messages.getString("OracleNLSProvider.19") + REST_TIMESTAMP_TZ_FORMAT); //$NON-NLS-1$
            }
        }
        
        return ret;
    }
    
    /**
     * @param interval
     * @return
     */
    private String formatINTERVALYM(INTERVALYM interval) {
        String ret = yieldNullDisplay(interval);
        
        if (ret == null) {
            OraINTERVALYMFormat formatInterval;
            try {
                formatInterval = getOraINTERVALYMFormat();
                ret = formatInterval.format(interval);
            } catch (ParseException e) {
                Map<String, String> nlsMap = getNLSMap();
                LOGGER.severe(Messages.getString("OracleNLSProvider.22") + nlsMap.get(NLS_TIMESTAMP_TZ_FORMAT)); //$NON-NLS-1$
            }
        }
        
        return ret;
    }

    /**
     * @param interval
     * @return
     */
    private String formatINTERVALDS(INTERVALDS interval) {
        String ret = yieldNullDisplay(interval);
        
        if (ret == null) {
            OraINTERVALDSFormat formatInterval;
            try {
                formatInterval = getOraINTERVALDSFormat();
                ret = formatInterval.format(interval);
            } catch (ParseException e) {
                Map<String, String> nlsMap = getNLSMap();
                LOGGER.severe(Messages.getString("OracleNLSProvider.23") + nlsMap.get(NLS_TIMESTAMP_TZ_FORMAT)); //$NON-NLS-1$
            }
        }
        
        return ret;
    }

    /**
     * @param s
     * @return
     */
    public Number parseNumber(java.lang.String s) {
        Number ret = null;

        if (s != null) {
            char decimalSeparator = /*
                                     * new oracle.i18n.text.OraDecimalFormatSymbols(oracle.i18n.util.OraLocaleInfo .getInstance(nlsMap.get("NLS_LANGUAGE"),
                                     * nlsMap.get("NLS_TERRITORY"))).getDecimalSeparator()
                                     */
            // bug 5711442
            getDecimalSeparator();

            if (s.indexOf(decimalSeparator) != -1) {
                ret = new BigDecimal(s.replace(decimalSeparator, '.'));
            } else {
                ret = new BigInteger(s);
            }
        }

        return ret;
    }

    // //
    // get formats
    // /getNLSMap(c).get("NLS_CHARACTERSET");

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getDBCharset()
     */
    public String getDBCharset() {
        return getNLSMap().get(NLS_CHARACTERSET);
    }

    /**
     * @return
     */
    public String getDBLanguage() {
        return getNLSMap().get(NLS_LANGUAGE);
    }

    /**
     * @return
     */
    public String getDBTerritory() {
        return getNLSMap().get(NLS_TERRITORY);
    }

    /**
     * @return
     */
    public String getDBTimeZone() {
        return getNLSMap().get(DB_TIMEZONE);
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getDecimalSeparator()
     */
    public char getDecimalSeparator() {
        if ((getNLSMap()==null)||getNLSMap().get(NLS_NUMERIC_CHARACTERS)==null) {
            //limp home - could be in mount state where fixed table in nls are not available
            //Bug 25368658 - SYS AS SYSDBA: DB MOUNTED BUT NOT OPEN => FLOATING POINT MATH OUTPUT INCORRECT
            return '.';
        }
        String s = getNLSMap().get(NLS_NUMERIC_CHARACTERS);
        return s.substring(0, 1).charAt(0);
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getGroupSeparator()
     */
    public char getGroupSeparator() {
        String s = getNLSMap().get(NLS_NUMERIC_CHARACTERS);
        return s.substring(1, 2).charAt(0);
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getDateFormat(oracle.dbtools.raptor.nls.FormatType)
     */
    public String getDateFormat(FormatType formatType) {
        switch (formatType) {
            case PREFERRED:
                return getNLSMap().get(NLS_DATE_FORMAT);
            case GENERIC:
                // Generic Format - avoiding locale influences
                return GENERIC_DATE_FORMAT;
            case LOADER:
                // Loader Format - avoiding locale influences
            	return LOADER_DATE_FORMAT;
            case REST:
                // Rest Format - avoiding locale influences
                return REST_DATE_FORMAT;
            default:
                return super.getDateFormat(formatType);
        }
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getTimeStampWithTimeZoneFormat(oracle.dbtools.raptor.nls.FormatType)
     */
    public String getTimeStampWithTimeZoneFormat(FormatType formatType) {
        switch (formatType) {
            case PREFERRED:
                return getNLSMap().get(NLS_TIMESTAMP_TZ_FORMAT);
            case GENERIC:
                // Generic Format - avoiding locale influences
                return GENERIC_TIMESTAMP_TZ_FORMAT;
            case LOADER:
                // Loader Format - avoiding locale influences
                // Not, TZR b/c it returns words for zone vs the numeric representation
                // which is what we need for portability.
                return LOADER_TIMESTAMP_TZ_FORMAT;
            case REST:
                // Rest Format - avoiding locale influences
                return REST_TIMESTAMP_TZ_FORMAT;
            default:
                return super.getTimeStampWithTimeZoneFormat(formatType);
        }
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getTimeStampFormat(oracle.dbtools.raptor.nls.FormatType)
     */
    public String getTimeStampWithLocalTimeZoneFormat(FormatType formatType) {
        switch (formatType) {
            case REST:
                // REST Format - avoiding locale influences
                return REST_TIMESTAMP_LTZ_FORMAT;
            default:
                return super.getTimeStampWithLocalTimeZoneFormat(formatType);
        }
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getTimeStampFormat(oracle.dbtools.raptor.nls.FormatType)
     */
    public String getTimeStampFormat(FormatType formatType) {
        switch (formatType) {
            case PREFERRED:
                return getNLSMap().get(NLS_TIMESTAMP_FORMAT);
            case GENERIC:
                // Generic Format - avoiding locale influences
                return GENERIC_TIMESTAMP_FORMAT;
            case LOADER:
                // Loader Format - avoiding locale influences
                return LOADER_TIMESTAMP_FORMAT;
            case REST:
                // Rest Format - avoiding locale influences
                return REST_TIMESTAMP_FORMAT;
            default:
                return super.getTimeStampFormat(formatType);
        }
    }

    /**
     * @return
     */
    public String getDBDateLanguage() {
        return getNLSMap().get(NLS_DATE_LANGUAGE);
    }

    // ///

    /** 
     * Make synchronized to help make ScriptRunner thread safe
     * @return
     */
    private synchronized Map<String, String> getNLSMap() {
        if (nlsMappings == null){
        	populateNLS();
        }
        return nlsMappings;
    }

    /**
     * @param format
     * @return
     * @throws ParseException
     */
    public OraDATEFormat getOraDATEFormat(String format) throws ParseException {
        Map<String, String> nlsMap = getNLSMap();
        return new OraDATEFormat(format, oracle.i18n.util.OraLocaleInfo.getInstance(nlsMap.get(NLS_LANGUAGE), nlsMap.get(NLS_TERRITORY)));
    }

    /**
     * @param format
     * @return
     * @throws ParseException
     */
    public OraTIMESTAMPFormat getOraTIMESTAMPFormat(String format) throws ParseException {
        Map<String, String> nlsMap = getNLSMap();
        return new OraTIMESTAMPFormat(format, oracle.i18n.util.OraLocaleInfo.getInstance(nlsMap.get(NLS_LANGUAGE), nlsMap.get(NLS_TERRITORY)));
    }

    /**
     * @param format
     * @return
     * @throws ParseException
     */
    public OraTIMESTAMPLTZFormat getOraTIMESTAMPLTZFormat(String format) throws ParseException {
        Map<String, String> nlsMap = getNLSMap();
        return new OraTIMESTAMPLTZFormat(format, oracle.i18n.util.OraLocaleInfo.getInstance(nlsMap.get(NLS_LANGUAGE), nlsMap.get(NLS_TERRITORY)), getTimeZone(nlsMap.get(DB_TIMEZONE)), getSessionTimeZone());
    }

    /**
     * @param format
     * @return
     * @throws ParseException
     */
    public OraTIMESTAMPTZFormat getOraTIMESTAMPTZFormat(String format) throws ParseException {
        Map<String, String> nlsMap = getNLSMap();
        return new OraTIMESTAMPTZFormat(format, oracle.i18n.util.OraLocaleInfo.getInstance(nlsMap.get(NLS_LANGUAGE), nlsMap.get(NLS_TERRITORY)));
    }

    /**
     * @param format
     * @return
     * @throws ParseException
     */
    public OraDATEFormat getOraDATEFormat(FormatType format) throws ParseException {
        return getOraDATEFormat(getDateFormat(format));
    }

    /**
     * @param format
     * @return
     * @throws ParseException
     */
    public OraTIMESTAMPFormat getOraTIMESTAMPFormat(FormatType format) throws ParseException {
        return getOraTIMESTAMPFormat(getTimeStampFormat(format));
    }

    /**
     * @param format
     * @return
     * @throws ParseException
     */
    public OraTIMESTAMPLTZFormat getOraTIMESTAMPLTZFormat(FormatType format) throws ParseException {
        return getOraTIMESTAMPLTZFormat(getTimeStampWithLocalTimeZoneFormat(format));
    }

    /**
     * @param format
     * @return
     * @throws ParseException
     */
    public OraTIMESTAMPTZFormat getOraTIMESTAMPTZFormat(FormatType format) throws ParseException {
        return getOraTIMESTAMPTZFormat(getTimeStampWithTimeZoneFormat(format));
    }

    /**
     * @param format
     * @return
     * @throws ParseException
     */
    public OraINTERVALYMFormat getOraINTERVALYMFormat(FormatType format) throws ParseException {
        Map<String, String> nlsMap = getNLSMap();
        return new OraINTERVALYMFormat(oracle.i18n.util.OraLocaleInfo.getInstance(nlsMap.get(NLS_LANGUAGE), nlsMap.get(NLS_TERRITORY)));
    }

    /**
     * @param format
     * @return
     * @throws ParseException
     */
    public OraINTERVALDSFormat getOraINTERVALDSFormat(FormatType format) throws ParseException {
        Map<String, String> nlsMap = getNLSMap();
        return new OraINTERVALDSFormat(oracle.i18n.util.OraLocaleInfo.getInstance(nlsMap.get(NLS_LANGUAGE), nlsMap.get(NLS_TERRITORY)));
    }

    /**
     * @return
     * @throws ParseException
     */
    public OraDATEFormat getOraDATEFormat() throws ParseException {
        return getOraDATEFormat(FormatType.PREFERRED);
    }

    /**
     * @return
     * @throws ParseException
     */
    public OraTIMESTAMPFormat getOraTIMESTAMPFormat() throws ParseException {
        return getOraTIMESTAMPFormat(FormatType.PREFERRED);
    }

    /**
     * @return
     * @throws ParseException
     */
    public OraTIMESTAMPLTZFormat getOraTIMESTAMPLTZFormat() throws ParseException {
        return getOraTIMESTAMPLTZFormat(FormatType.PREFERRED);
    }

    /**
     * @return
     * @throws ParseException
     */
    public OraTIMESTAMPTZFormat getOraTIMESTAMPTZFormat() throws ParseException {
        return getOraTIMESTAMPTZFormat(FormatType.PREFERRED);
    }

    /**
     * @return
     * @throws ParseException
     */
    public OraINTERVALYMFormat getOraINTERVALYMFormat() throws ParseException {
        return getOraINTERVALYMFormat(FormatType.PREFERRED);
    }

    /**
     * @return
     * @throws ParseException
     */
    public OraINTERVALDSFormat getOraINTERVALDSFormat() throws ParseException {
        return getOraINTERVALDSFormat(FormatType.PREFERRED);
    }

    /**
     * @param tzId
     * @return
     * @throws ParseException
     */
    public TimeZone getTimeZone(String tzId) throws ParseException {
        TimeZone timeZone;
        
        if (tzId != null) {
            if (tzId.charAt(0) == '+' || tzId.charAt(0) == '-') {
                if ((timeZone = TimeZone.getTimeZone("GMT" + tzId)) == null) { //$NON-NLS-1$
                    throw new ParseException(Messages.getString("OracleNLSProvider.48") + tzId + "", 0); //$NON-NLS-1$ //$NON-NLS-2$
                }
            } else {
                if ((timeZone = TimeZone.getTimeZone(tzId)) == null) {
                    throw new ParseException(Messages.getString("OracleNLSProvider.49") + tzId + "", 0); //$NON-NLS-1$ //$NON-NLS-2$
                }
            }
        } else {
            timeZone = null;
        }
        
        return timeZone;
    }
    
    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getDatabaseTimeZone()
     */
    public TimeZone getDatabaseTimeZone() {
        try {
            return getTimeZone(getDBTimeZone());
        } catch (ParseException pe) {
            return null;
        }
    }
    
    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getSessionTimeZone()
     */
    public TimeZone getSessionTimeZone() {
        TimeZone sessionTimeZone = null;
        Map<String, String> nlsMap = getNLSMap();
        String sessionTimeZoneID = nlsMap.get(SESSION_TIMEZONE);
        if (sessionTimeZoneID != null) {
            if (sessionTimeZoneID.charAt(0) == '+' || sessionTimeZoneID.charAt(0) == '-') { 
                sessionTimeZoneID = "GMT" + sessionTimeZoneID; //$NON-NLS-1$
            }
    
            sessionTimeZone = TimeZone.getTimeZone(sessionTimeZoneID);
            if (sessionTimeZone.getID().equals("GMT") && !sessionTimeZoneID.equals("GMT")) { //$NON-NLS-1$ //$NON-NLS-2$
                /*
                 * Java fell back on GMT, i.e. Oracle time zone ID was not recognized, therefore, fall back on offset
                 */
                String sessionTimeZoneOffset = "GMT" + nlsMap.get(SESSION_TIMEZONE_OFFSET); //$NON-NLS-1$
                /*
                 * Check the last char of the offset and remove the null byte, if any. Refer: 1937516
                 */
                byte[] bytes = sessionTimeZoneOffset.getBytes();
                if (bytes[ bytes.length - 1 ] == 0) sessionTimeZoneOffset = sessionTimeZoneOffset.substring(0, sessionTimeZoneOffset.length() - 1);
                sessionTimeZone = TimeZone.getTimeZone(sessionTimeZoneOffset);
            }
        } else {
            // Use Default Timezone where not set in session
            sessionTimeZone = TimeZone.getDefault();
        }

        return sessionTimeZone;
    }

    // this is meant to return a sql usable version of the data i.e.
    // to_date('01-NOV-2007:21:31','DD-MON-RRRR:HH24:MI')
    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getSQL(java.lang.Object)
     */
    @Deprecated
    public String getSQL(Object obj) {
        return obj.toString();
    }

    /**
     * Get Formatter to sql type
     * @param sqlType
     * @return
     */
    public Format getFormat(int sqlType, FormatType formatType) {
        try {
            switch (sqlType) {
                case Types.DATE:
                    return getOraDATEFormat(formatType);
                case Types.TIMESTAMP:
                    return getOraTIMESTAMPFormat(formatType);
                case OracleTypes.TIMESTAMPTZ:
                    return getOraTIMESTAMPTZFormat(formatType);
                case OracleTypes.TIMESTAMPLTZ:
                    return getOraTIMESTAMPLTZFormat(formatType);
                case OracleTypes.INTERVALYM:
                    return getOraINTERVALYMFormat(formatType);
                case OracleTypes.INTERVALDS:
                    return getOraINTERVALDSFormat(formatType);
            }
        } catch (ParseException e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING,
                    e.getStackTrace()[0].toString(), e);
        }
        
        return null;
    }

    // this is meant to return the text format of the value i.e.
    // 01-NOV-2007:21:31
    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#getValue(java.lang.Object)
     */
    public Object getValue(Object obj, FormatType formatType) {
        String nullDisplay = yieldNullDisplay(obj);
        
        Object retVal;
        try {
            if (nullDisplay != null) {
                retVal = nullDisplay;
            } else if (obj instanceof Clob) {
                retVal = ((Clob) obj).getSubString(1, (int) ((Clob) obj).length());
            } else if (obj instanceof OPAQUE) {
                retVal = ((OPAQUE) obj).getSQLTypeName().trim();
            } else if (obj instanceof Timestamp) {
                retVal = formatTIMESTAMP(new TIMESTAMP((Timestamp) obj));
            } else if (obj instanceof TIMESTAMP) {
            	if (formatType == FormatType.LOADER){
            	    retVal = formatLoaderTIMESTAMP((TIMESTAMP) obj);
            	} else if (formatType == FormatType.REST){
            	    retVal = formatRestTIMESTAMP((TIMESTAMP) obj);
            	} else {
            	    retVal = formatTIMESTAMP((TIMESTAMP) obj);
            	}
            } else if (obj instanceof TIMESTAMPTZ) {
            	if (formatType == FormatType.LOADER){
            	    retVal = formatLoaderTIMESTAMPTZ((TIMESTAMPTZ) obj);
            	} if (formatType == FormatType.REST){
            	    retVal = formatRestTIMESTAMPTZ((TIMESTAMPTZ) obj);
            	} else {
            	    retVal = formatTIMESTAMPTZ((TIMESTAMPTZ) obj);
            	}
            } else if (obj instanceof TIMESTAMPLTZ) {
            	if (formatType == FormatType.LOADER){
            	    retVal = formatLoaderTIMESTAMPLTZ((TIMESTAMPLTZ) obj);
            	} else if (formatType == FormatType.REST){
            	    retVal = formatRestTIMESTAMPLTZ((TIMESTAMPLTZ) obj);
            	} else {
            	    retVal = formatTIMESTAMPLTZ((TIMESTAMPLTZ) obj);
            	}
            } else if ((obj instanceof DATE)) {
            	if (formatType == FormatType.LOADER){
            	    retVal = formatLoaderDATE((DATE) obj); 
            	} else if (formatType == FormatType.REST){
            	    retVal = formatRestDATE((DATE) obj); 
            	} else {
            	    retVal = formatDate((DATE) obj);
            	}
            } else if ((obj instanceof Date)) {
                retVal = formatDate(new DATE((Date) obj));
            } else if (obj instanceof NUMBER) {
                retVal = formatNUMBER((NUMBER) obj);
            } else if (obj instanceof BINARY_DOUBLE) {
                retVal = formatBINARY_DOUBLE((BINARY_DOUBLE) obj);
            } else if (obj instanceof BINARY_FLOAT) {
                retVal = formatBINARY_FLOAT((BINARY_FLOAT) obj);
            } else if (obj instanceof BigDecimal) {
                retVal = formatBigDecimal((BigDecimal) obj);
            } else if (obj instanceof INTERVALDS) {
                retVal = formatINTERVALDS((INTERVALDS) obj);
            } else if (obj instanceof INTERVALYM) {
                retVal = formatINTERVALYM((INTERVALYM) obj);
            } else if (obj instanceof Datum) {
                retVal = ((Datum) obj).stringValue();
            } else
                retVal = super.getValue(obj, formatType);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            
            retVal = "";
        }
        return retVal;
    }

    /**
     * @param key
     * @return
     */
    private String getDefault(String key) {
        return s_defaults.get(key);
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#parse(java.lang.String)
     */
    public Object parse(String str) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#parse(java.lang.String, java.lang.Object)
     */
    public Object parse(String str, Object base) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#initConnection()
     */
    public void initConnection() {
        setNlsFromPrefs();
        // grab nls settings
        populateNLS();
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#populateNLS()
     */
    public void populateNLS() {
        //
        // lookup NLS settings
        //
        PreparedStatement stmt = null;
        ResultSet rset = null;
        try {
            QueryXMLSupport xml = QueryXMLSupport.getQueryXMLSupport(new MetaResource(getClass().getClassLoader(),"oracle/dbtools/raptor/utils/loadNLS.xml")); //$NON-NLS-1$
            Query q = xml.getQuery("loadnls", getConnection()); //$NON-NLS-1$
            if (nlsMappings == null) {
                nlsMappings = new HashMap<String, String>();
            }
            stmt = getConnection().prepareStatement(q.getSql());
            rset = stmt.executeQuery();
            while (rset.next()) {
                nlsMappings.put(rset.getString(1), rset.getString(2));
                LOGGER.fine(Messages.getString("OracleNLSProvider.28") + rset.getString(1) + "=" + rset.getString(2)); //$NON-NLS-1$ //$NON-NLS-2$
            }
        }  catch (Exception e) {
            LOGGER.fine(Messages.getString("OracleNLSProvider.30") + e.getMessage()); //$NON-NLS-1$
        } finally {
            try {
                if (rset != null) rset.close();
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
            }
        }
    }

    /**
     * @param setting
     * @param prefName
     * @return
     */
    private boolean requiresSetting(String setting, String prefName) {
        // String pref = DBConfig.getInstance().getString(setting);
        String pref = getDefault(setting);
        String existing = getNLSMap().get(prefName);
        if (prefName != null && pref != null && existing != null) LOGGER.fine(Messages.getString("OracleNLSProvider.31") + prefName + ":" + pref + ":" + existing); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        return pref != null && !pref.equals("") && !pref.equals(existing); //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#setNlsFromPrefs()
     */
    public void setNlsFromPrefs() {
        String query = ""; //$NON-NLS-1$
        // String string = "\"";
        String skip = getDefault("NLS_SKIP"); //$NON-NLS-1$
        if (Boolean.parseBoolean(skip)) {
            return;
        }

        Connection conn = getConnection();
        final SettingsApplicator applicator = new SettingsApplicator(conn);
        
        Version ver = VersionTracker.getDbVersion(DefaultConnectionIdentifier.createIdentifier(conn));
        Map<String, String> map = getNLSMap();

        boolean appliedSomething = applicator.apply(NLSUtils.NLS_LANG, NLS_LANGUAGE);
        
        final boolean territoryChanged = applicator.apply(NLSUtils.NLS_TERR, NLS_TERRITORY);
        appliedSomething |= territoryChanged;
        appliedSomething |= applicator.apply(NLSUtils.NLS_SORT, NLS_SORT);

        if (requiresSetting(NLSUtils.NLS_COMP, NLS_COMP)) {
            if (ver.compareTo(new Version("9")) < 0) { //$NON-NLS-1$
            	LOGGER.log(Level.INFO,NLS_COMP + Messages.getString("OracleNLSProvider.38")); //$NON-NLS-1$
            } else if (ver.compareTo(new Version("10.2")) < 0) { //$NON-NLS-1$
                // it V9 or 10.1 DB
                query = getAlterSession(NLS_COMP, "ANSI"); //$NON-NLS-1$
                LOGGER.log(Level.INFO,NLS_COMP + Messages.getString("OracleNLSProvider.41")); //$NON-NLS-1$
            } else {
                String pref = getDefault(NLSUtils.NLS_COMP);
                query = getAlterSession(NLS_COMP, pref);
            }
            applicator.getUtil().execute(query, NO_BINDS);
            appliedSomething = true;
        }

        appliedSomething |= applicator.apply(NLSUtils.NLS_DATE_LANG, NLS_DATE_LANGUAGE);

    

        appliedSomething |= applicator.apply(NLSUtils.NLS_DATE_FORM, NLS_DATE_FORMAT);
        appliedSomething |= applicator.apply(NLSUtils.NLS_TS_FORM, NLS_TIMESTAMP_FORMAT);

        if (requiresSetting(NLSUtils.NLS_TS_TZ_FORM, NLS_TIMESTAMP_TZ_FORMAT)) {
            query = getAlterSession(NLS_TIMESTAMP_TZ_FORMAT, getDefault(NLSUtils.NLS_TS_TZ_FORM));
            if (ver.compareTo(new Version("9")) < 0) { //$NON-NLS-1$
            	LOGGER.log(Level.INFO,NLS_TIMESTAMP_TZ_FORMAT + Messages.getString("OracleNLSProvider.43")); //$NON-NLS-1$
            } else {
                applicator.getUtil().execute(query, NO_BINDS);
                appliedSomething = true;
            }
        }
        String dbPref = map.get(NLS_NUMERIC_CHARACTERS);
        char decPref = convertToChar(getDefault(NLSUtils.NLS_DEC_SEP));
        char sepPref = convertToChar(getDefault(NLSUtils.NLS_GRP_SEP));
        
        // Make sure the decimal and group separators are valid and distinct
        if ( !( decPref == 0 || sepPref == 0 || decPref == sepPref ) ) {
            String value = new StringBuilder().append(decPref).append(sepPref).toString();
            // If the territory changed, force an update to the NLS_NUMERIC_CHARACTERS in case the territory changed it from the intial
            // session state. Otherwise, update if the new value doesn't match the old value (including if the old value was null)
            if ( territoryChanged || !value.equals(dbPref) ) {
                query = getAlterSession(NLS_NUMERIC_CHARACTERS, value);
                applicator.getUtil().execute(query, NO_BINDS);
                appliedSomething = true;
            }
        }

        appliedSomething |= applicator.apply(NLSUtils.NLS_CURR, NLS_CURRENCY);
        appliedSomething |= applicator.apply(NLSUtils.NLS_ISO_CURR, NLS_ISO_CURRENCY);

        if (requiresSetting(NLSUtils.NLS_LENGTH, NLS_LENGTH_SEMANTICS)) {
            query = getAlterSession(NLS_LENGTH_SEMANTICS, getDefault(NLSUtils.NLS_LENGTH));
            if (ver.compareTo(new Version("9")) < 0) { //$NON-NLS-1$
                LOGGER.log(Level.INFO,NLS_COMP + Messages.getString("OracleNLSProvider.47")); //$NON-NLS-1$
            } else {
                applicator.getUtil().execute(query, NO_BINDS);
                appliedSomething = true;
            }
        }

        // Apply session Timezone to connection
        appliedSomething |= applicator.apply(getSessionTimeZone());

        if (appliedSomething) populateNLS();

    }

    /* (non-Javadoc)
     * @see oracle.dbtools.raptor.nls.DefaultNLSProvider#updateDefaults(java.util.Map)
     */
    public void updateDefaults(Map<String, String> map) {
        if (map != null) {
            for (String key: map.keySet()) {
                s_defaults.put(key, map.get(key));
            }
        }
    }

    // // Privates
    // Fix bug: 8249942 - SQLDEV1.5.4:NLS:PREFERENCE: NLS_TIMESTAMP_FORMAT NOT SUPPORT "TEXT"
    /**
     * @param param
     * @param value
     * @return
     */
    private String getAlterSession(String param, String value) {
        if (value != null) value = value.replaceAll("'", "''"); //$NON-NLS-1$ //$NON-NLS-2$
        return MessageFormat.format(ALTER_SESSION, param, value);
    }

}
