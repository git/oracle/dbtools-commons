/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.proformatter;


public class CodingStyleSQLOptionsBean implements ICodingStyleSQLOptions{


	/* type definition */
    private String m_name;
    private boolean m_useTab;
    private int m_uppercase;
    //this is m_sourceSql private int m_sql;
    private boolean m_forceDifference;
    private boolean m_suppressComment;
    private int m_sqlSourceEnclosed;
    private int m_sqlSourceEscape;
    private int m_sourceSql;
    private int m_smallSql;
    private boolean m_replaceComment;
    private int m_quoteChar;
    private int m_numSpaces;
    private int m_numCommas;
    private int m_bracketSpacing;
    private boolean m_bracketSpacingAndOrWhen;
    private int m_commaSpacing;
    private int m_operatorSpacing;
    private int m_lineWidth;
    private int m_lineNum;
    private boolean m_breakSelectBracket;
    private int m_breakSchema;
    private boolean m_breakKeyword;
    private boolean m_breakJoin;
    private boolean m_breakCaseWhen;
    private boolean m_breakCaseThen;
    private boolean m_breakCaseElse;
    private boolean m_breakCase;
    private boolean m_breakCaseAndOr;
    private boolean m_breakBeforeComment;
    private boolean m_breakBeforeConcat;
    private boolean m_breakBeforeComma;
    private boolean m_breakBeforeAnd;
    private boolean m_breakAfterConcat;
    private boolean m_breakAfterComma;
    private boolean m_breakAfterAnd;
    private boolean m_dblIndent;
    private boolean m_colored;
    private boolean m_indentAnd;
    private boolean m_alignOperator;
    private boolean m_alignKeyword;
    private boolean m_alignEqual;
    private boolean m_alignDecl;
    private boolean m_alignConcat;
    private boolean m_alignComment;
    private boolean m_alignComma;
    private boolean m_alignAs;
    private boolean m_sqlsourceCopied;
    private boolean m_moreNewlines;
    private int m_preserveNewlines;
    private int m_targetSql;
    
    /**
     * bean for all the getter setter options - 
     * all are curently true false, 
     * numeric - being for example number of characters in a terminal width,
     * numberic - being chosen from several options 
     * (options and translations being supplied/handled at a later date).
     * when using this bean the following options should be handled:
     * some (newer) options not set (XML from older sqldeveloper)-> new options should default to something.
     * xml contains newer options (XML from newer sqldeveloper) -> these options should be ignored.
     * Not sure how the exporting from sqlplus is going to work out - i.e. will it be imported into an old sqldeveloper.
     * name - only varchar entry 
     * key in sqldev for example 1:SQL  1=Oracle SQL=default 'always there' name.
     */
	public CodingStyleSQLOptionsBean(){
		//put defaults in here??? or load from a default XML somewhere?
		/* default constructor - please edit 
		 * name=madeup
		 * boolean all true
		 * number all zero
		 * */
		/**
		 * key is SQL type : name
		 * names contains at least SQL
		 */
        m_name="madeup";
        m_useTab=true;
        m_uppercase=0;
        /**
         * if there is a non whitespace difference flag it 
         * - unless whitespace changing option used e.g nonwhitespace changing: -- fred -> /X -- fred X/ 
         */
        m_forceDifference=true;
        m_suppressComment=true;
        /*
         * sqlsource enclosed by:
         *  notenclosed 
         * '
         * "
         */
        m_sqlSourceEnclosed=0;
        /*
         * SQLSOURCE ESCAPE BEING
         * '
         * "
         * \
         */
        m_sqlSourceEscape=0;
        /**
         * type SYBASE =1
        */
        m_sourceSql=0;
        /**
         * say 80 columns should be compact - few newlines
         */
        m_smallSql=0;
        m_replaceComment=true;
        m_quoteChar=0;
        m_numSpaces=0;
        m_numCommas=0;
        m_bracketSpacing=0;
        m_bracketSpacingAndOrWhen=true;
        m_commaSpacing=0;
        m_operatorSpacing=0;
        m_lineWidth=0;
        m_lineNum=0;
        m_breakSelectBracket=true;
        m_breakSchema=0;
        m_breakKeyword=true;
        m_breakJoin=true;
        m_breakCaseWhen=true;
        m_breakCaseThen=true;
        m_breakCaseElse=true;
        /**
         * Line break for case
         * and various case options given below.
         */
        m_breakCase=true;
        m_breakCaseAndOr=true;
        m_breakBeforeComment=true;
        m_breakBeforeConcat=true;
        m_breakBeforeComma=true;
        m_breakBeforeAnd=true;
        m_breakAfterConcat=true;
        m_breakAfterComma=true;
        m_breakAfterAnd=true;
        m_dblIndent=true;
        /**
         * whether to have coloured output - currently not used in sqldeveloper proper.
         */
        m_colored=true;
        m_indentAnd=true;
        /**
         * align on various operators
         */
        m_alignOperator=true;
        m_alignKeyword=true;
        m_alignEqual=true;
        m_alignDecl=true;
        m_alignConcat=true;
        m_alignComment=true;
        m_alignComma=true;
        m_alignAs=true;
        m_sqlsourceCopied=true;
        m_moreNewlines=true;
        m_preserveNewlines=0;
        m_targetSql=0;
	}
	
	/*getters and setters */
    public String getName() {
        return this.m_name;
    }

    public void setName(String name) {
        m_name=name;
    }
    public boolean getUseTab() {
        return this.m_useTab;
    }

    public void setUseTab(boolean useTab) {
        m_useTab=useTab;
    }
    public int getUppercase() {
        return this.m_uppercase;
    }

    public void setUppercase(int uppercase) {
        m_uppercase=uppercase;
    }

    public boolean getForceDifference() {
        return this.m_forceDifference;
    }

    public void setForceDifference(boolean forceDifference) {
        m_forceDifference=forceDifference;
    }
    public boolean getSuppressComment() {
        return this.m_suppressComment;
    }

    public void setSuppressComment(boolean suppressComment) {
        m_suppressComment=suppressComment;
    }
    public int getSqlSourceEnclosed() {
        return this.m_sqlSourceEnclosed;
    }

    public void setSqlSourceEnclosed(int sqlSourceEnclosed) {
        m_sqlSourceEnclosed=sqlSourceEnclosed;
    }
    public int getSqlSourceEscape() {
        return this.m_sqlSourceEscape;
    }

    public void setSqlSourceEscape(int sqlSourceEscape) {
        m_sqlSourceEscape=sqlSourceEscape;
    }
    public int getSourceSql() {
        return this.m_sourceSql;
    }

    public void setSourceSql(int sourceSql) {
        m_sourceSql=sourceSql;
    }
    public int getSmallSql() {
        return this.m_smallSql;
    }

    public void setSmallSql(int smallSql) {
        m_smallSql=smallSql;
    }
    public boolean getReplaceComment() {
        return this.m_replaceComment;
    }

    public void setReplaceComment(boolean replaceComment) {
        m_replaceComment=replaceComment;
    }
    public int getQuoteChar() {
        return this.m_quoteChar;
    }

    public void setQuoteChar(int quoteChar) {
        m_quoteChar=quoteChar;
    }
    public int getNumSpaces() {
        return this.m_numSpaces;
    }

    public void setNumSpaces(int numSpaces) {
        m_numSpaces=numSpaces;
    }
    public int getNumCommas() {
        return this.m_numCommas;
    }

    public void setNumCommas(int numCommas) {
        m_numCommas=numCommas;
    }
    public int getBracketSpacing() {
        return this.m_bracketSpacing;
    }

    public void setBracketSpacing(int bracketSpacing) {
        m_bracketSpacing=bracketSpacing;
    }
    public boolean getBracketSpacingAndOrWhen() {
        return this.m_bracketSpacingAndOrWhen;
    }

    public void setBracketSpacingAndOrWhen(boolean bracketSpacingAndOrWhen) {
        m_bracketSpacingAndOrWhen=bracketSpacingAndOrWhen;
    }
    public int getCommaSpacing() {
        return this.m_commaSpacing;
    }

    public void setCommaSpacing(int commaSpacing) {
        m_commaSpacing=commaSpacing;
    }
    public int getOperatorSpacing() {
        return this.m_operatorSpacing;
    }

    public void setOperatorSpacing(int operatorSpacing) {
        m_operatorSpacing=operatorSpacing;
    }
    public int getLineWidth() {
        return this.m_lineWidth;
    }

    public void setLineWidth(int lineWidth) {
        m_lineWidth=lineWidth;
    }
    public int getLineNum() {
        return this.m_lineNum;
    }

    public void setLineNum(int lineNum) {
        m_lineNum=lineNum;
    }
    public boolean getBreakSelectBracket() {
        return this.m_breakSelectBracket;
    }

    public void setBreakSelectBracket(boolean breakSelectBracket) {
        m_breakSelectBracket=breakSelectBracket;
    }
    public int getBreakSchema() {
        return this.m_breakSchema;
    }

    public void setBreakSchema(int breakSchema) {
        m_breakSchema=breakSchema;
    }
    public boolean getBreakKeyword() {
        return this.m_breakKeyword;
    }

    public void setBreakKeyword(boolean breakKeyword) {
        m_breakKeyword=breakKeyword;
    }
    public boolean getBreakJoin() {
        return this.m_breakJoin;
    }

    public void setBreakJoin(boolean breakJoin) {
        m_breakJoin=breakJoin;
    }
    public boolean getBreakCaseWhen() {
        return this.m_breakCaseWhen;
    }

    public void setBreakCaseWhen(boolean breakCaseWhen) {
        m_breakCaseWhen=breakCaseWhen;
    }
    public boolean getBreakCaseThen() {
        return this.m_breakCaseThen;
    }

    public void setBreakCaseThen(boolean breakCaseThen) {
        m_breakCaseThen=breakCaseThen;
    }
    public boolean getBreakCaseElse() {
        return this.m_breakCaseElse;
    }

    public void setBreakCaseElse(boolean breakCaseElse) {
        m_breakCaseElse=breakCaseElse;
    }
    public boolean getBreakCase() {
        return this.m_breakCase;
    }

    public void setBreakCase(boolean breakCase) {
        m_breakCase=breakCase;
    }
    public boolean getBreakCaseAndOr() {
        return this.m_breakCaseAndOr;
    }

    public void setBreakCaseAndOr(boolean breakCaseAndOr) {
        m_breakCaseAndOr=breakCaseAndOr;
    }
    public boolean getBreakBeforeComment() {
        return this.m_breakBeforeComment;
    }

    public void setBreakBeforeComment(boolean breakBeforeComment) {
        m_breakBeforeComment=breakBeforeComment;
    }
    public boolean getBreakBeforeConcat() {
        return this.m_breakBeforeConcat;
    }

    public void setBreakBeforeConcat(boolean breakBeforeConcat) {
        m_breakBeforeConcat=breakBeforeConcat;
    }
    public boolean getBreakBeforeComma() {
        return this.m_breakBeforeComma;
    }

    public void setBreakBeforeComma(boolean breakBeforeComma) {
        m_breakBeforeComma=breakBeforeComma;
    }
    public boolean getBreakBeforeAnd() {
        return this.m_breakBeforeAnd;
    }

    public void setBreakBeforeAnd(boolean breakBeforeAnd) {
        m_breakBeforeAnd=breakBeforeAnd;
    }
    public boolean getBreakAfterConcat() {
        return this.m_breakAfterConcat;
    }

    public void setBreakAfterConcat(boolean breakAfterConcat) {
        m_breakAfterConcat=breakAfterConcat;
    }
    public boolean getBreakAfterComma() {
        return this.m_breakAfterComma;
    }

    public void setBreakAfterComma(boolean breakAfterComma) {
        m_breakAfterComma=breakAfterComma;
    }
    public boolean getBreakAfterAnd() {
        return this.m_breakAfterAnd;
    }

    public void setBreakAfterAnd(boolean breakAfterAnd) {
        m_breakAfterAnd=breakAfterAnd;
    }
    public boolean getDblIndent() {
        return this.m_dblIndent;
    }

    public void setDblIndent(boolean dblIndent) {
        m_dblIndent=dblIndent;
    }
    public boolean getColored() {
        return this.m_colored;
    }

    public void setColored(boolean colored) {
        m_colored=colored;
    }
    public boolean getIndentAnd() {
        return this.m_indentAnd;
    }

    public void setIndentAnd(boolean indentAnd) {
        m_indentAnd=indentAnd;
    }
    public boolean getAlignOperator() {
        return this.m_alignOperator;
    }

    public void setAlignOperator(boolean alignOperator) {
        m_alignOperator=alignOperator;
    }
    public boolean getAlignKeyword() {
        return this.m_alignKeyword;
    }

    public void setAlignKeyword(boolean alignKeyword) {
        m_alignKeyword=alignKeyword;
    }
    public boolean getAlignEqual() {
        return this.m_alignEqual;
    }

    public void setAlignEqual(boolean alignEqual) {
        m_alignEqual=alignEqual;
    }
    public boolean getAlignDecl() {
        return this.m_alignDecl;
    }

    public void setAlignDecl(boolean alignDecl) {
        m_alignDecl=alignDecl;
    }
    public boolean getAlignConcat() {
        return this.m_alignConcat;
    }

    public void setAlignConcat(boolean alignConcat) {
        m_alignConcat=alignConcat;
    }
    public boolean getAlignComment() {
        return this.m_alignComment;
    }

    public void setAlignComment(boolean alignComment) {
        m_alignComment=alignComment;
    }
    public boolean getAlignComma() {
        return this.m_alignComma;
    }

    public void setAlignComma(boolean alignComma) {
        m_alignComma=alignComma;
    }
    public boolean getAlignAs() {
        return this.m_alignAs;
    }

    public void setAlignAs(boolean alignAs) {
        m_alignAs=alignAs;
    }
    public boolean getSqlsourceCopied() {
        return this.m_sqlsourceCopied;
    }

    public void setSqlsourceCopied(boolean sqlsourceCopied) {
        m_sqlsourceCopied=sqlsourceCopied;
    }
    public boolean getMoreNewlines() {
        return this.m_moreNewlines;
    }

    public void setMoreNewlines(boolean moreNewlines) {
        m_moreNewlines=moreNewlines;
    }
    public int getPreserveNewlines() {
        return this.m_preserveNewlines;
    }

    public void setPreserveNewlines(int preserveNewlines) {
        m_preserveNewlines=preserveNewlines;
    }
    /**
     * do the formatting deligated to CoreFormatter.
     * using the current bean settings.
     * start - the sql text,
     */
    //public String format(String start) {
    //	return SQLPlusFormatter.format((CoreCodingStyleSQLOptions) this,start);
    //}

	@Override
	public int getTargetSql() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setTargetSql(int targetSql) {
		// TODO Auto-generated method stub
		m_targetSql=targetSql;
	}
}
