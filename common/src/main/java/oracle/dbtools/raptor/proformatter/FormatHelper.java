/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.proformatter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import oracle.dbtools.util.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class FormatHelper {
    final static public int UNKNOWNINT = -1;
    final static public String ORACLE = "Oracle"; //$NON-NLS-1$
    final static public int ORACLEINT = 1;
    final static public String DB2 = "DB2"; //$NON-NLS-1$
    final static public int DB2INT = 2;
    final static public String SYBASE = "Sybase"; //$NON-NLS-1$
    final static public int SYBASEINT = 3;
    final static public String ACCESS = "Microsoft Access"; //$NON-NLS-1$
    final static public int ACCESSINT = 4;
    final static public String SQLSERVER = "Microsoft SQL Server"; //$NON-NLS-1$
    final static public int SQLSERVERINT = 5;
    final static public String MYSQL = "MySQL"; //$NON-NLS-1$
    final static public int MYSQLINT = 6;
	private static URL u;
	private static boolean fail=false;
    
    public static boolean loadFromXML(ICodingStyleSQLOptions cb, URL url) {
    	u =url;
    	return loadFromXML(cb);
    }
    
	public static boolean loadFromXML(ICodingStyleSQLOptions cb) {
		String fname = System.getenv("SQLFORMATPATH");
		if (fname == null && u !=null) {
				return populateBean(u, cb, "Item"); //$NON-NLS-1$
		} else if (!(new File(fname).exists()) && u!=null) {
				return populateBean(u, cb, "Item"); //$NON-NLS-1$
		} else return false;
		// load from xml 1:SQL
		// #structure:
		// # <?xml version = '1.0' encoding = 'UTF-8'?>
		// # <SQLprofiles
		// class="oracle.dbtools.proformatter.treePreferences.style.profile.CodingStyleSQLProfiles"
		// xmlns="http://xmlns.oracle.com/jdeveloper/110000/default-coding-sql-style-profiles">
		// # <profileMap class="java.util.HashMap">
		// # <Item>
		// # <Key>1:Old Preferences</Key>
		// # <Value
		// class="oracle.dbtools.proformatter.treePreferences.CodingStyleSQLProfile">
		// # <alignAs>true</alignAs>
		// # <alignComma>false</alignComma>
		// # ...
		// # <Key>1:SQL</Key> <--- load this one
		// # get sets number or true or false, name is the only 'free text'
		// #
		// read (ignore) until item then get key (might ask eventually which key
		// do you want to load)
		// then suck in all the vales (values are number true false or in names
		// case free text)
		// dynamically set all the variables setters in the fir
		// setthename(value) log any sets missing in XML or bean
		// i.e. get an array list of setters
		// remove them as called
		// report on missing or extra
		// until end of value.
	}
	
	public static boolean populateBean(String fname, Object cb, String item) {
		File f =new File(fname);
		try {
			return populateBean(f.toURI().toURL(), cb, item);
		} catch (MalformedURLException e) {
			Logger.fine(FormatHelper.class, e.getLocalizedMessage());
			return false;
		}
	}
	/**
	 * populate a bean
	 * 
	 * @param cb
	 *            the bean (use set methods)
	 * @param tag
	 *            used just before
	 * @param enclosedBy
	 *            values containd in this
	 */
	public static boolean populateBean(URL u, Object cb, String item/*
																 * ,String tag,
																 * String
																 * enclosedBy
																 */) {
		fail=false;
		Method[] methArray = cb.getClass().getDeclaredMethods();
		HashMap<String, Method> hm = new HashMap<String, Method>();
		int xi = 0;
		for (Method m : methArray) {
			String name = m.getName();
			if (name.startsWith("set")) { //$NON-NLS-1$
				hm.put(name, m);// store method incase we have to use it to
								// check type...
				//System.out.println(name +" "+  m.getParameters()[0]);
			}
			// System.out.println(xi++);
		}

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Throwable t = null;
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(u.openStream());
			// assume we have been applied key=1:SQL
			NodeList nl = document.getElementsByTagName(item);// may not have
																// any
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = (Node) nl.item(i);
				Node nlkeyvalue = null;
				Node nlvalue = null;
				NodeList nk = n.getChildNodes();// nlkeyvalue.getNodeName();
				for (int ij = 0; ij < nl.getLength(); ij++) {
					Node nn = (Node) nk.item(ij);
					if (nn.getNodeName().equals("Key")) { //$NON-NLS-1$
						nlkeyvalue = nn;
					}
					if (nn.getNodeName().equals("Value")) { //$NON-NLS-1$
						nlvalue = nn;
						break;
					}
				}
				if ((nlkeyvalue != null)
						&& (nlvalue != null)
						&& (nlkeyvalue.getFirstChild() != null)
						&& (nlkeyvalue.getFirstChild().getNodeValue() != null)
						&& (nlkeyvalue.getFirstChild().getNodeValue()
								.equals("1:SQL"))) {//hard coded this is the default  //$NON-NLS-1$
					Node nlc = nlvalue;
					NodeList children = nlc.getChildNodes();
					for (int ii = 0; ii < children.getLength(); ii++) {
						try {
							// method = set upper the first char theRest
							String name = children.item(ii).getNodeName();
							String methName = "set" + name.substring(0, 1).toUpperCase() + name.substring(1); //$NON-NLS-1$
							Method theM = null;
							if ((!(methName.equals("set#text")) && (!(methName.equals("setMemberOrderHashStructure"))))) { //$NON-NLS-1$  //$NON-NLS-2$
								theM = hm.get(methName);
								if (theM == null) {
									Logger.info(new FormatHelper().getClass(),"the method: " + methName + " does not exist"); //$NON-NLS-1$  //$NON-NLS-2$
								} else {
									// value = string value for name, boolean
									// true false for true false, else numver
									String value = children.item(ii)
											.getChildNodes().item(0)
											.getNodeValue();
									if (name.equals("name")) { //$NON-NLS-1$
										theM.invoke(cb, value);
										hm.remove(methName);
									} else {
										if (!((value == null || value
												.equals("")))) { //$NON-NLS-1$
											if (value.equals("true")) { //$NON-NLS-1$
												theM.invoke(cb, true);
												hm.remove(methName);
											} else if (value.equals("false")) { //$NON-NLS-1$
												theM.invoke(cb, false);
												hm.remove(methName);
											} else {
												theM.invoke(cb,
														Integer.parseInt(value));
												hm.remove(methName);
											}
										}
									}
								}
							}
						} catch (IllegalAccessException iae) {
							// illegal access exceptionr
							t = iae;
							;
						} catch (InvocationTargetException iae) {
							// illegal access exceptionr
							t = iae;
						} finally {
							if (t != null) {
								Logger.fine(new FormatHelper().getClass(), t); //$NON-NLS-1$
							   fail=true;	
							}
						}
					}
					Iterator<String> sti=hm.keySet().iterator();
					while (sti.hasNext()) {
						Logger.info(new FormatHelper().getClass(),"Method "+sti.next()+"has not been matched");
					}
				}
			}
		} catch (SAXParseException spe) {
			Exception x = spe;
			if (spe.getException() != null)
				x = spe.getException();
			// Error generated by the parser
			Logger.fine(new FormatHelper().getClass(), "\n** Parsing error" //$NON-NLS-1$
					+ ", line " + spe.getLineNumber() //$NON-NLS-1$
					+ ", uri " + spe.getSystemId() //$NON-NLS-1$
					+ "  " + spe.getMessage() + x.getMessage()); //$NON-NLS-1$
			fail=true;
		} catch (SAXException sxe) {
			// Error generated by this application
			// (or a parser-initialization error)
			Exception x = sxe;
			if (sxe.getException() != null)
				x = sxe.getException();
			t = x;
		} catch (ParserConfigurationException pce) {
			// Parser with specified options
			// cannot be built
			pce.printStackTrace();
		} catch (IOException ioe) {
			t = ioe;
		} finally {
			if (t != null) {
				Logger.fine(new FormatHelper().getClass(), t); //$NON-NLS-1$
				fail=true;
			}
			
		}
return fail;
	}

	final public static String dbToFormatter(final int db) {
		if (db == ORACLEINT) {
			return "Oracle"; //$NON-NLS-1$
		}
		if (db == DB2INT) {
			return "DB2/UDB"; //$NON-NLS-1$
		}
		if (db == SYBASEINT) {
			return "Sybase"; //$NON-NLS-1$
		}
		if (db == ACCESSINT) {
			return "MSAccess"; //$NON-NLS-1$
		}
		if (db == SQLSERVERINT) {
			return "SQL Server"; //$NON-NLS-1$
		}
		if (db == MYSQLINT) {
			return "MYSQL"; //$NON-NLS-1$
		}
		return "Any SQL"; //$NON-NLS-1$
	}
}
