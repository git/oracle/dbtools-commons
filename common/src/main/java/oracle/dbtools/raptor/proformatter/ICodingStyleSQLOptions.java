/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.proformatter;


public interface ICodingStyleSQLOptions {
	   /**
     * Indicates that the brace should appear on the same line as the
     * declaration.
     */
    public static final int BRACE_POSITION_SAME_LINE = 0;

    /**
     * Indicates that the brace should appear on next line after the
     * declaration.
     */
    public static final int BRACE_POSITION_NEXT_LINE = 1;

    /**
     * Indicates that all elements should appear on the same line.
     */
    public static final int WRAP_NEVER = 0;

    /**
     * Indicates that elements exceeding the line width should be wrapped.
     */
    public static final int WRAP_IF_LONG = 1;

    /**
     * Indicates that elements should always be wrapped.
     */
    public static final int WRAP_ALWAYS = 2;

    /**
     * sql source enclosed in "
     */
    public static final int SQLSOURCEENCLOSED_DOUBLE_QUOTE = 0;
    /**
     * sql source enclosed in '
     */
    public static final int SQLSOURCEENCLOSED_SINGLE_QUOTE = 1;

    /**
     * sql source escaped with \
     */
    public static final int SQLSOURCEESCAPE_ESCAPE = 0;
    /**
     * sql source escaped with "
     */
    public static final int SQLSOURCEESCAPE_DOUBLE_QUOTE = 1;
    /**
     * sql source escaped with '
     */
    public static final int SQLSOURCEESCAPE_SINGLE_QUOTE = 2;
    /**
     * quote character is '
     */
    public static final int QUOTECHAR_SINGLE_QUOTE = 0;
    /**
     * quote character is "
     */
    public static final int QUOTECHAR_DOUBLE_QUOTE = 1;

    /**
     * target sql is sql, vs say java strinbuffer.
     */
    public static final int TARGETSQL_SQL = 0;
    public static final int TARGETSQL_ASP_STRINGBUILD = 1; 
    public static final int TARGETSQL_C__STRINGBUILDER = 2;
    public static final int TARGETSQL_CONCATENATED_SQL = 3;
    public static final int TARGETSQL_JAVA_STRINGBUFFER = 4;
    public static final int TARGETSQL_JAVA_STRING = 5;
    public static final int TARGETSQL_JAVA_STRING_2 = 6;
    public static final int TARGETSQL_MSSQL_STRING = 7;
    public static final int TARGETSQL_PASCAL_STRING = 8;
    public static final int TARGETSQL_PHP_STRING = 9;
    public static final int TARGETSQL_VB_STRING__1_ = 10;
    public static final int TARGETSQL_VB_STRING__2_ = 11;
    public static final int TARGETSQL_VB_STRING__3_ = 12;
    public static final int TARGETSQL_VB_STRING__4_ = 13;
    public static final int TARGETSQL_VB_STRINGBUILDER = 14;
    public static final int TARGETSQL_HTML_CODE = 15;

    /**
     * breaks in schema correct for Large_sql.
     */
    public static final int BREAKSCHEMA_LARGE_SQL = 0;
    public static final int BREAKSCHEMA_SMALL_SQL = 1;
    public static final int BREAKSCHEMA_CUSTOMIZED_SQL = 2;
    public static final int BREAKSCHEMA_1_LINE_SQL = 3;

    /**
     * operator spacing keep unchanged.
     */
    public static final int OPERATORSPACING_KEEP_UNCHANGED = 0 ;
    public static final int OPERATORSPACING_NO_SPACES__WHERE_A_B_C = 1;
    public static final int OPERATORSPACING_ONE_SPACE__WHERE_A___B___C = 2;

    /**
     * comma spacing keep unchanged.
     */
    public static final int COMMASPACING_KEEP_UNCHANGED = 0;
    public static final int COMMASPACING_NO_SPACES__SELECT_A_B_C_ = 1; 
    public static final int COMMASPACING_ONE_SPACE_BEFORE__SELECT_A__B__C__ = 2;
    public static final int COMMASPACING_ONE_SPACE_AFTER__SELECT_A__B__C_ = 3;
    public static final int COMMASPACING_ONE_SPACE_AROUND__SELECT_A___B___C__ = 4;

    /**
     * bracket spacing keep unchanged.
     */
    public static final int BRACKETSPACING_KEEP_UNCHANGED = 0;
    public static final int BRACKETSPACING_NO_SPACES__MAX_C1__ = 1;
    public static final int BRACKETSPACING_ONE_SPACE_INSIDE__MAX__C1___ = 2;
    public static final int BRACKETSPACING_ONE_SPACE_OUTSIDE__MAX__C1___ = 3;
    public static final int BRACKETSPACING_ONE_SPACE_AROUND__MAX___C1____ = 4;

    /**
     * keywords to be uppercased.
     */
    public static final int UPPERCASE_KEYWORDS_UPPERCASE = 0;
    public static final int UPPERCASE_NO_CHANGE = 1;
    public static final int UPPERCASE_WHOLE_SQL_UPPERCASE = 2;
    public static final int UPPERCASE_WHOLE_SQL_LOWERCASE = 3;

    public String getName();

    public void setName(String name);

    public boolean getUseTab();

    public void setUseTab(boolean useTab);

    public int getUppercase();

    public void setUppercase(int uppercase);

    public int getTargetSql();

    public void setTargetSql(int targetSql);

    public boolean getForceDifference();

    public void setForceDifference(boolean forceDifference);

    public boolean getSuppressComment();

    public void setSuppressComment(boolean suppressComment);

    public int getSqlSourceEnclosed();

    public void setSqlSourceEnclosed(int sqlSourceEnclosed);

    public int getSqlSourceEscape();

    public void setSqlSourceEscape(int sqlSourceEnclosed);
    
    public int getSourceSql();

    public void setSourceSql(int sourceSql);

    public int getSmallSql();

    public void setSmallSql(int smallSql);

    public boolean getReplaceComment();

    public void setReplaceComment(boolean replaceComment);

    public int getQuoteChar();

    public void setQuoteChar(int quoteChar);

    public int getNumSpaces();

    public void setNumSpaces(int numSpaces);

    public int getNumCommas();

    public void setNumCommas(int numCommas);

    public int getBracketSpacing();

    public void setBracketSpacing(int bracketSpacing);

    public boolean getBracketSpacingAndOrWhen();

    public void setBracketSpacingAndOrWhen(boolean bracketSpacingAndOrWhen);

    public int getCommaSpacing();

    public void setCommaSpacing(int commaSpacing);

    public int getOperatorSpacing();

    public void setOperatorSpacing(int operatorSpacing);

    public int getLineWidth();

    public void setLineWidth(int lineWidth);

    public int getLineNum();

    public void setLineNum(int lineNum);

    public boolean getBreakSelectBracket();

    public void setBreakSelectBracket(boolean breakSelectBracket);

    public int getBreakSchema();

    public void setBreakSchema(int breakSchema);

    public boolean getBreakKeyword();

    public void setBreakKeyword(boolean breakKeyword);

    public boolean getBreakJoin();

    public void setBreakJoin(boolean breakJoin);

    public boolean getBreakCaseWhen();

    public void setBreakCaseWhen(boolean breakCaseWhen);

    public boolean getBreakCaseThen();

    public void setBreakCaseThen(boolean breakCaseThen);

    public boolean getBreakCaseElse();

    public void setBreakCaseElse(boolean breakCaseElse);

    public boolean getBreakCase();

    public void setBreakCase(boolean breakCase);

    public boolean getBreakCaseAndOr();

    public void setBreakCaseAndOr(boolean breakCaseAndOr);

    public boolean getBreakBeforeComment();

    public void setBreakBeforeComment(boolean breakBeforeComment);

    public boolean getBreakBeforeConcat();

    public void setBreakBeforeConcat(boolean breakBeforeConcat);

    public boolean getBreakBeforeComma();

    public void setBreakBeforeComma(boolean breakBeforeComma);

    public boolean getBreakBeforeAnd();

    public void setBreakBeforeAnd(boolean breakBeforeAnd);

    public boolean getBreakAfterConcat();

    public void setBreakAfterConcat(boolean breakAfterConcat);

    public boolean getBreakAfterComma();

    public void setBreakAfterComma(boolean breakAfterComma);

    public boolean getBreakAfterAnd();

    public void setBreakAfterAnd(boolean breakAfterAnd);

    public boolean getDblIndent();

    public void setDblIndent(boolean dblIndent);

    public boolean getColored();

    public void setColored(boolean colored);

    public boolean getIndentAnd();

    public void setIndentAnd(boolean indentAnd);

    public boolean getAlignOperator();

    public void setAlignOperator(boolean alignOperator);

    public boolean getAlignKeyword();

    public void setAlignKeyword(boolean alignKeyword);

    public boolean getAlignEqual();

    public void setAlignEqual(boolean alignEqual);

    public boolean getAlignDecl();

    public void setAlignDecl(boolean alignDecl);

    public boolean getAlignConcat();

    public void setAlignConcat(boolean alignConcat);

    public boolean getAlignComment();

    public void setAlignComment(boolean alignComment);

    public boolean getAlignComma();

    public void setAlignComma(boolean alignComma);

    public boolean getAlignAs();

    public void setAlignAs(boolean alignAs);

    public boolean getSqlsourceCopied();

    public void setSqlsourceCopied(boolean alignAs);

    public boolean getMoreNewlines();
    
    public void setMoreNewlines(boolean moreNewLines);

    public int getPreserveNewlines();
    
    public void setPreserveNewlines(int preserveNewLines);

    //public String format(String start);
}
