/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.proxy;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import oracle.dbtools.raptor.backgroundTask.IRaptorTaskRunMode;
import oracle.dbtools.raptor.backgroundTask.RaptorTask;
import oracle.dbtools.raptor.backgroundTask.RaptorTaskManager;
import oracle.dbtools.raptor.backgroundTask.TaskException;
import oracle.dbtools.util.Logger;

/**
 * A copy of org.netbeans.core.ProxyAutoConfig to avoid this module's dependency
 * on NetBeans core module (nb-o-n-core).
 */
class ProxyAutoConfig {

	private static final Map<String, ProxyAutoConfig> file2pac = new HashMap<String, ProxyAutoConfig>(
			2);
	private static final String NS_PROXY_AUTO_CONFIG_URL = "META-INF/nsProxyAutoConfig.js"; // NOI18N

	/**
	 * 
	 * @param pacFile
	 *            The string to be parsed into a URI
	 * @return ProxyAutoConfig for given pacFile or <code>null</code> if
	 *         constructor failed
	 */
	public static synchronized ProxyAutoConfig get(String pacFile) {
		if (file2pac.get(pacFile) == null) {
			Logger.fine(ProxyAutoConfig.class, "Init ProxyAutoConfig for "
					+ pacFile);
			ProxyAutoConfig instance = null;
			try {
				instance = new ProxyAutoConfig(pacFile);
			} catch (URISyntaxException ex) {
				Logger.warn(ProxyAutoConfig.class, "Parsing " + pacFile
						+ " to URI throws " + ex);
			} finally {
				file2pac.put(pacFile, instance);
			}
		}
		return file2pac.get(pacFile);
	}

	private Invocable inv = null;
	private RaptorTask<Invocable> initTask;
	private final URI pacURI;

	private static final class InitTask extends RaptorTask<Invocable> {
		final String pacURL;

		InitTask(String pacURL) {
			super("load PAC", true, IRaptorTaskRunMode.NO_GUI);
			setCancellable(false);
			setPausable(false);
			this.pacURL = pacURL;
		}

		@Override
		protected Invocable doWork() throws TaskException {
			ScriptEngine eng = null;

			try (InputStream pacIS = downloadPAC(pacURL)) {
				if (pacIS != null) {
					String utils = downloadUtils();
					eng = evalPAC(pacIS, utils);
				}
			} catch (FileNotFoundException ex) {
				Logger.fine(ProxyAutoConfig.class,
						"While constructing ProxyAutoConfig thrown " + ex, ex);
				return null;
			} catch (ScriptException ex) {
				Logger.fine(ProxyAutoConfig.class,
						"While constructing ProxyAutoConfig thrown " + ex, ex);
				return null;
			} catch (IOException ex) {
				Logger.fine(ProxyAutoConfig.class,
						"While constructing ProxyAutoConfig thrown " + ex, ex);
				return null;
			}

			if (eng == null) {
				Logger.warn(ProxyAutoConfig.class,
						"JavaScript engine cannot be null");
				return null;
			}
			return (Invocable) eng;
		}
	}

	private ProxyAutoConfig(final String pacURL) throws URISyntaxException {
		assert file2pac.get(pacURL) == null : "Only once object for " + pacURL
				+ " must exist.";
		final String normPAC = normalizePAC(pacURL);
		pacURI = new URI(normPAC);
		initTask = new InitTask(pacURL);

		RaptorTaskManager.getInstance().addTask(initTask);
	}

	public URI getPacURI() {
		return pacURI;
	}

	@SuppressWarnings("unchecked")
	public List<Proxy> findProxyForURL(URI u) {
		synchronized (this) {
			if (initTask != null) {
				try {
					inv = initTask.getResult();
				} catch (InterruptedException e) {
					Logger.warn(ProxyAutoConfig.class, "While loading PAC: "
							+ e);
				} catch (ExecutionException e) {
					Logger.warn(ProxyAutoConfig.class, "While loading PAC: "
							+ e);
				}
				initTask = null;
			}
		}

		if (inv == null) {
			return Collections.singletonList(Proxy.NO_PROXY);
		}
		Object proxies = null;
		try {
			proxies = inv.invokeFunction("FindProxyForURL", u.toString(),
					u.getHost()); // NOI18N
		} catch (ScriptException ex) {
			Logger.fine(ProxyAutoConfig.class,
					"While invoking FindProxyForURL(" + u + ", " + u.getHost()
							+ " thrown " + ex, ex);
		} catch (NoSuchMethodException ex) {
			Logger.fine(ProxyAutoConfig.class,
					"While invoking FindProxyForURL(" + u + ", " + u.getHost()
							+ " thrown " + ex, ex);
		}
		List<Proxy> res = analyzeResult(u, proxies);
		if (res == null) {
			Logger.info(ProxyAutoConfig.class, "findProxyForURL(" + u
					+ ") returns null.");
			res = Collections.emptyList();
		}
		Logger.fine(ProxyAutoConfig.class, "findProxyForURL(" + u
				+ ") returns " + Arrays.asList(res));
		return res;
	}

	private static InputStream downloadPAC(String pacURL) throws IOException {
		InputStream is = null;
		URL url = null;
		try {
			url = new URL(pacURL);
		} catch (MalformedURLException ex) {
			Logger.info(ProxyAutoConfig.class, "Malformed " + pacURL, ex);
			return null;
		}
		URLConnection conn = url.openConnection(Proxy.NO_PROXY);
		is = conn.getInputStream();
		return is;
	}

	private static ScriptEngine evalPAC(InputStream is, String utils)
			throws FileNotFoundException, ScriptException {
		ScriptEngineManager factory = new ScriptEngineManager();
		ScriptEngine engine = factory.getEngineByName("JavaScript");
		Reader pacReader = new InputStreamReader(is);
		engine.eval(pacReader);
		engine.eval(utils);
		return engine;
	}

	private List<Proxy> analyzeResult(URI uri, Object proxiesString) {
		if (proxiesString == null) {
			Logger.fine(ProxyAutoConfig.class, "Null result for " + uri);
			return null;
		}
		Proxy.Type proxyType;
		String protocol = uri.getScheme();
		// assert protocol != null : "Invalid scheme of uri " + uri +
		// ". Scheme cannot be null!";
		if (protocol == null) {
			return null;
		} else {
			// bug fix: Proxy.Type.HTTP should match both http and https.
			if ("http".equals(protocol) || "https".equals(protocol)) { // NOI18N
				proxyType = Proxy.Type.HTTP;
			} else {
				proxyType = Proxy.Type.SOCKS;
			}
		}
		StringTokenizer st = new StringTokenizer(proxiesString.toString(), ";"); // NOI18N
		List<Proxy> proxies = new LinkedList<Proxy>();
		while (st.hasMoreTokens()) {
			String proxy = st.nextToken();
			if (DIRECT.equals(proxy.trim())) {
				proxies.add(Proxy.NO_PROXY);
			} else {
				String host = getHost(proxy);
				Integer port = getPort(proxy);
				if (host != null && port != null) {
					proxies.add(new Proxy(proxyType, new InetSocketAddress(
							host, port)));
				}
			}
		}
		return proxies;
	}

	private static String getHost(String proxy) {
		if (proxy.startsWith("PROXY ")) {
			proxy = proxy.substring(6);
		}
		int i = proxy.lastIndexOf(":"); // NOI18N
		if (i <= 0 || i >= proxy.length() - 1) {
			Logger.info(ProxyAutoConfig.class, "No port in " + proxy);
			return null;
		}

		String host = proxy.substring(0, i);

		return normalizeProxyHost(host);
	}

	private static Integer getPort(String proxy) {
		if (proxy.startsWith("PROXY ")) {
			proxy = proxy.substring(6);
		}
		int i = proxy.lastIndexOf(":"); // NOI18N
		if (i <= 0 || i >= proxy.length() - 1) {
			Logger.info(ProxyAutoConfig.class, "No port in " + proxy);
			return null;
		}

		String port = proxy.substring(i + 1);
		if (port.indexOf('/') >= 0) {
			port = port.substring(0, port.indexOf('/'));
		}

		try {
			return Integer.parseInt(port);
		} catch (NumberFormatException ex) {
			Logger.info(ProxyAutoConfig.class, ex.getLocalizedMessage(), ex);
			return null;
		}
	}

	private static String downloadUtils() {
		StringBuilder builder = new StringBuilder();

		URL url = ProxyAutoConfig.class.getClassLoader().getResource(
				NS_PROXY_AUTO_CONFIG_URL);

		if (url != null) {
			try (BufferedReader reader = new BufferedReader(
					new InputStreamReader(url.openStream()))) {
				String line = null;
				boolean doAppend = false;
				while ((line = reader.readLine()) != null) {
					line = line.trim();
					if (line.startsWith("var pacUtils =")) { // NOI18N
						doAppend = true;
					}
					if (!doAppend)
						continue;
					if (line.endsWith("+")) { // NOI18N
						line = line.substring(0, line.length() - 1);
					}
					builder.append(line.replaceAll("\"", "")
							.replaceAll("\\\\n", "")
							.replaceAll("\\\\\\\\", "\\\\")); // NOI18N
					builder.append(System.getProperty("line.separator")); // NOI18N
				}
			} catch (IOException ex) {
				Logger.info(
						ProxyAutoConfig.class,
						"While downloading nsProxyAutoConfig.js thrown "
								+ ex.getMessage(), ex);
			}
		}
		return builder.toString();
	}

	private String normalizePAC(String pacURL) {
		int index;
		if ((index = pacURL.indexOf("\n")) != -1) { // NOI18N
			pacURL = pacURL.substring(0, index);
		}
		if ((index = pacURL.indexOf("\r")) != -1) { // NOI18N
			pacURL = pacURL.substring(0, index);
		}
		if ((index = pacURL.indexOf(" ")) != -1) { // NOI18N
			pacURL = pacURL.substring(0, index);
		}
		return pacURL.trim();
	}

	// copied from ProxySettings.java
	public static final String DIRECT = "DIRECT";

	public static String normalizeProxyHost(String proxyHost) {
		if (proxyHost.toLowerCase(Locale.US).startsWith("http://")) { // NOI18N
			return proxyHost.substring(7, proxyHost.length());
		} else {
			return proxyHost;
		}
	}
}