/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.proxy;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.common.utils.PlatformUtils;
import oracle.dbtools.util.Logger;

/**
 * Proxy configuration implementation copied from the FCP. Supports three types
 * of HTTP proxies: direct connection, manul proxy configuration, and proxy auto
 * configuration (PAC).
 * 
 * @author jmcginni
 *
 */
class ProxyConfig {
	public static final String SQLCL_SYSTEM_HTTP_PROXY = "dbtools.system_http_proxy";
	public static final String SQLCL_SYSTEM_NON_PROXY_HOSTS = "dbtools.system_http_non_proxy_hosts";

	public static final String DIRECT = "DIRECT";
	public static final String PAC_PREFIX = "PAC ";

	public static final String HTTP_PROTOCOL_PREFIX = "http://";

	public static final String DEFAULT_HTTP_PROXY_PORT = "80";
	public static final String DEFAULT_SOCKS_PROXY_PORT = "1080";

	private static final String PROXY_EXCEPTION_SEPARATOR = "|"; // NORES
	private static String DEFAULT_PROXY_EXCEPTIONS = ""; // NORES
	static final String[] LOCAL_PROXY_EXCEPTIONS = { 
		"localhost", // NOTRANS
		"localhost.localdomain", // NOTRANS
		"127.0.0.1", // NOTRANS
		"[::1]" // NOTRANS
	};

	/**
	 * Ensures that local exceptions are added to the passed in string
	 * 
	 * @param proxyExceptions
	 *            the exception string
	 * @return a string of exceptions with the local exceptions added
	 */
	public static String ensureLocalExceptionsAdded(String proxyExceptions) {
		// Normalize exceptions
		String normalizedExceptions = normalizeExceptions(proxyExceptions);

		// Break on the '|' character
		String[] currentExceptions = splitExceptionsIntoArray(normalizedExceptions);

		// Put data into a StringBuffer for better performance
		StringBuffer sb = ModelUtil.hasLength(normalizedExceptions) ? new StringBuffer(
				normalizedExceptions) : new StringBuffer();

		// Verify that each of the local proxy exceptions are added
		final List<String> localExceptions = getLocalExceptions();
		for (String exception : localExceptions) {
			if (!containsException(currentExceptions, exception)) {
				if (sb.length() > 0) {
					sb.append(PROXY_EXCEPTION_SEPARATOR);
				}

				sb.append(exception);
			}
		}

		return sb.toString();
	}

	/**
	 * Used to determine if the specified exception is contained in the array of
	 * exceptions
	 * 
	 * @param currentExceptions
	 *            the array of exceptions
	 * @param exception
	 *            the except to search for
	 * @return true if exception is found in the currentExceptions array; false
	 *         otherwise
	 */
	static boolean containsException(String[] currentExceptions,
			String exception) {
		boolean retval = false;
		if (currentExceptions != null && currentExceptions.length != 0
				&& exception.length() > 0) {
			for (String currentException : currentExceptions) {
				if (currentException.equals(exception)) {
					retval = true;
					break;
				}
			}
		}

		return retval;
	}

	/**
	 * Returns a string of "normalized" exceptions based on the passed in
	 * exceptions string. "Normalize" in this case means to put the exceptions
	 * in all lower case, change any commas to the vertical bar separator, and
	 * if the string is null sets it to the empty string
	 * 
	 * @param exceptions
	 * @return the "normalized" exceptions as described above
	 */
	static String normalizeExceptions(String exceptions) {
		String normalizedExceptions = exceptions;
		if (normalizedExceptions == null
				|| normalizedExceptions.trim().length() == 0) {
			normalizedExceptions = DEFAULT_PROXY_EXCEPTIONS;
		} else {
			normalizedExceptions = normalizedExceptions.toLowerCase();
			normalizedExceptions = normalizedExceptions.replaceAll(",",
					PROXY_EXCEPTION_SEPARATOR); // NOTRANS
			if (PlatformUtils.isWindows()) {
				normalizedExceptions = normalizedExceptions.replaceAll(";",
						PROXY_EXCEPTION_SEPARATOR);
			}
			normalizedExceptions = normalizedExceptions.replaceAll(" ", ""); // NOTRANS

			// e.g. [localhost]
			normalizedExceptions = normalizedExceptions.replaceAll("\\[", ""); // NOTRANS
			normalizedExceptions = normalizedExceptions.replaceAll("\\]", ""); // NOTRANS
		}

		return normalizedExceptions;
	}

	/**
	 * Splits the passed-in exception string into an array of strings using the
	 * vertical bar separator as the string delimiter
	 * 
	 * @param exceptions
	 * @return array of exceptions or null if none found
	 */
	public static String[] splitExceptionsIntoArray(String exceptions) {
		// Break on the '|' character
		String[] currentExceptions = null;
		if (ModelUtil.hasLength(exceptions)) {
			currentExceptions = exceptions.split("\\"
					+ PROXY_EXCEPTION_SEPARATOR); // NOTRANS
		}

		return currentExceptions;
	}

	private static List<String> getLocalExceptions() {
		// First add all the hardcoded local exceptions
		List<String> localExceptions = new ArrayList<String>(
				LOCAL_PROXY_EXCEPTIONS.length);
		for (String exception : LOCAL_PROXY_EXCEPTIONS) {
			localExceptions.add(exception);
		}

		// Now look up the local machine by name and add those as well
		// Bug#8416355
		try {
			final InetAddress address = InetAddress.getLocalHost();
			String name = address.getCanonicalHostName();
			if (name != null) {
				localExceptions.add(name);
			}

			name = address.getHostName();
			if (name != null) {
				localExceptions.add(name);
			}

			// Add the local IP address also
			// as per 14534972
			name = address.getHostAddress();
			if (name != null) {
				localExceptions.add(name);
			}
		} catch (UnknownHostException ex) {
			// Nothing useful to do here
		}

		return localExceptions;
	}

	private static class Holder {
		static final ProxyConfig INSTANCE = new ProxyConfig();
	}

	static ProxyConfig getProxyConfiguration() {
		return Holder.INSTANCE;
	}

	private SystemProxyType systemProxyType = SystemProxyType.DIRECT_CONNECTION;
	private String systemProxyHost;
	private String systemProxyPort;
	private String systemProxyExceptions;
	private String pacFile;

	private ProxyConfig() {
		String proxyHost = System.getProperty(SQLCL_SYSTEM_HTTP_PROXY);

		if (DIRECT.equals(proxyHost) || proxyHost == null
				|| proxyHost.trim().length() == 0) {
			systemProxyType = SystemProxyType.DIRECT_CONNECTION;
			systemProxyHost = null;
			systemProxyPort = null;
			systemProxyExceptions = null;
		} else if (proxyHost.startsWith(PAC_PREFIX)) // NOTRANS
		{
			systemProxyType = SystemProxyType.PAC;

			// Populate placeholder values for systemProxyHost and
			// systemProxyPort to make sure that
			// non-ProxySelector-aware HTTP client classes (e.g.
			// NetworkAccessRule) get the consistent proxy configuration.
			final String SOME_EXTERNAL_HTTP_URL = "http://www.internic.net"; // NOTRANS
			try {
				URI someExtHttpUri = new URI(SOME_EXTERNAL_HTTP_URL);

				pacFile = proxyHost.substring(PAC_PREFIX.length());
				if (pacFile != null) {
					List<Proxy> proxyList = ProxySelectorImpl
							.resolveProxyListFromPAC(someExtHttpUri, pacFile);
					if (proxyList.size() > 0) {
						Proxy p = proxyList.get(0);
						if (Proxy.Type.DIRECT == p.type()) {
							systemProxyHost = null;
							systemProxyPort = null;
						} else {
							if (p.address() instanceof InetSocketAddress) {
								systemProxyHost = ((InetSocketAddress) p
										.address()).getHostName();
								systemProxyPort = Integer
										.toString(((InetSocketAddress) p
												.address()).getPort());
							} else {
								systemProxyHost = null;
							}
						}
					}
				}
				systemProxyExceptions = ensureLocalExceptionsAdded(null); // default
																			// proxy
																			// exceptions
			} catch (Exception e) {
				Logger.fine(
						ProxySelectorImpl.class,
						"Failed to get system proxy settings from DefaultProxySelector. Default to no proxy."); // NOTRANS
			}
		} else {
			systemProxyType = SystemProxyType.MANUAL_PROXY;
			String[] hostAndPort = Strings.hostAndPort(proxyHost);
			if (hostAndPort != null) {
				systemProxyHost = hostAndPort[0];
				systemProxyPort = hostAndPort[1] == null ? DEFAULT_HTTP_PROXY_PORT
						: hostAndPort[1];
			} else {
				Logger.warn(ProxySelectorImpl.class, SQLCL_SYSTEM_HTTP_PROXY
						+ " env var is invalid: " + proxyHost);
			}
			String nonProxyHosts = System
					.getProperty(SQLCL_SYSTEM_NON_PROXY_HOSTS); // [localhost,127.0.0.0/8
			systemProxyExceptions = ensureLocalExceptionsAdded(nonProxyHosts);
		}
	}

	SystemProxyType getProxyType() {
		return systemProxyType;
	}

	String getPACFile() {
		return pacFile;
	}

	String getHttpProxyHost() {
		return systemProxyHost;
	}

	String getHttpProxyPort() {
		return systemProxyPort;
	}

	String getHttpProxyExceptions() {
		return systemProxyExceptions;
	}
}
