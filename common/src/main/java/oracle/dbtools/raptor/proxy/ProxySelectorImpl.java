/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.proxy;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import oracle.dbtools.util.Logger;

/**
 * A ProxySelector implementation that grabs the proxy configuration details
 * from the launcher. Direct, Manual, and PAC proxy settings are supported
 * 
 * @author jmcginni
 */
public final class ProxySelectorImpl extends ProxySelector {
	private final static List<Proxy> NO_PROXY_LIST = Collections.singletonList(Proxy.NO_PROXY);

	private static ProxySelector original = null;

	public ProxySelectorImpl(ProxySelector originalProxySelector) {
		original = originalProxySelector;
	}

	public List<Proxy> select(URI uri) {
		if (uri == null)
			throw new IllegalArgumentException("URI must not be null."); // NOTRANS

		List<Proxy> result = NO_PROXY_LIST;

		ProxyConfig config = ProxyConfig.getProxyConfiguration();
		// We only support HTTP proxies. Use DIRECT CONNECTION for SOCKS
		// connection.
		final String scheme = uri.getScheme();
		if ("http".equals(scheme) || "https".equals(scheme)) {
			try {
				SystemProxyType systemProxyType = config.getProxyType();
				// We only care about PAC and MANUAL_PROXY options
				if (SystemProxyType.PAC.equals(systemProxyType)) {
					result = resolveProxyListFromPAC(uri, config.getPACFile());
				} else if (SystemProxyType.MANUAL_PROXY.equals(systemProxyType)) {
					result = resolveProxyList(uri, config.getHttpProxyHost(),
							config.getHttpProxyPort(), null, null,
							config.getHttpProxyExceptions());
				}
			} catch (Exception e) {
				Logger.fine(ProxySelectorImpl.class, "Failed to resolve a proxy for " + uri + ": exception = " + e.getMessage());
			}
		}

		return result;
	}

	static List<Proxy> resolveProxyListFromPAC(URI uri, String pacFile) {
		ProxyAutoConfig pac = null;
		try {
			pac = ProxyAutoConfig.get(pacFile);
		} catch (Throwable t) {
			Logger.finest(ProxySelectorImpl.class, "Error retrieving Proxy Auto Config file " + pacFile + ": exception = " + t.getMessage());
		}
		// assert pac != null : "Instance of ProxyAutoConfig found for " +
		// pacFile;
		if (pac == null) {
			Logger.finest(ProxySelectorImpl.class, "No instance of ProxyAutoConfig(" + pacFile + ") for URI " + uri);
			return NO_PROXY_LIST;
		}
		if (pac.getPacURI().getHost() == null
				|| pac.getPacURI().getHost().equals(uri.getHost())) {
			Logger.finest(ProxySelectorImpl.class, "Malformed PAC URI " + pac.getPacURI() + " for URI " + uri);
			return NO_PROXY_LIST;
		} else {
			List<Proxy> proxies = pac.findProxyForURL(uri); // NOI18N
			return proxies;
		}
	}

	private static List<Proxy> resolveProxyList(URI uri, String httpHost,
			String httpPort, String socksHost, String socksPort,
			String proxyExceptions) throws Exception {
		// null proxyHost indicates that no proxy should be used.
		if ((httpHost == null || httpHost.trim().length() == 0)
				&& (socksHost == null || socksHost.trim().length() == 0)) {
			return NO_PROXY_LIST;
		}

		// If uri's host is found in non-proxy hosts, no proxy shoud be used.
		String[] exceptions = ProxyConfig.splitExceptionsIntoArray(proxyExceptions);
		if (exceptions != null) {
			for (String exception : exceptions) {
				if (exception.equalsIgnoreCase(uri.getHost())) {
					return NO_PROXY_LIST;
				}
			}
		}

		SocketAddress addr;
		Proxy proxy;
		List<Proxy> res = new ArrayList<Proxy>(2);

		if (httpHost != null && httpHost.trim().length() > 0) {
			int httpPortParsed = 80;
			if (httpPort != null && httpPort.trim().length() > 0)
				httpPortParsed = Integer.parseInt(httpPort);

			addr = new InetSocketAddress(httpHost, httpPortParsed);
			proxy = new Proxy(Proxy.Type.HTTP, addr);
			res.add(proxy);
		}

		if (socksHost != null && socksHost.trim().length() > 0) {
			int socksPortParsed = 80;
			if (socksPort != null && socksPort.trim().length() > 0)
				socksPortParsed = Integer.parseInt(socksPort);

			addr = new InetSocketAddress(socksHost, socksPortParsed);
			proxy = new Proxy(Proxy.Type.SOCKS, addr);
			res.add(proxy);
		}

		if (res.size() > 0)
			return res;
		else
			return NO_PROXY_LIST;
	}

	@Override
	public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
		Logger.fine(ProxySelectorImpl.class, "Connection failed: URI + " + uri + ", exception: " + ioe.getMessage());
	}

	public ProxySelector getOriginalProxySelector() {
		return original;
	}
}
