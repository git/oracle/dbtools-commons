/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.proxy;

/**
 * String utilities, stolen from FCP
 */
final class Strings
{
  private Strings() {}
  
  /**
   * Given a String URL, return the host and port.
   * 
   * @param url
   * @return null if the URL is malformed
   */
  static String[] hostAndPort( String url )
  {
    // bug 6941026
    // be lenient to host:port
    // instead of just accepting
    //  http://host
    //  http://host:port
    if( !url.startsWith("http:") && url.indexOf(":") != -1 )
    {
       url = "http:" +  url;
    }
    String[] parts = url.split( ":" );
    if ( parts.length <= 1  )
    {
      return null;
    }
    
    String host = trim( parts[1], '/' );
    if ( host == null || host.length() == 0 ) return null;
    
    String port = null;
    if ( parts.length > 2 )
    {
      port = trim( parts[2], '/' );
    }
    
    return new String[] { host, port };
  }
  
  /**
   * Trim trailing and leading instances of a specified character from a 
   * String.
   * 
   * @param s a string to trim
   * @return the character to trim.
   * @throws NullPointerException if <tt>s</tt> is null.
   */
  static String trim( String s, char c )
  {
    int start = firstNonMatchingChar( s, c );
    int end = lastNonMatchingChar( s, c );
    if ( start == -1 || end == -1 ) return null;
    if ( end - start <= 0 ) return null;
    return s.substring( start, end );
  }
  
  private static int lastNonMatchingChar( String s, char c )
  {
    for ( int i=s.length()-1; i>=0; i-- )
    {
      if ( s.charAt( i ) != c ) return i+1;
    }
    return -1;
  }
  
  private static int firstNonMatchingChar( String s, char c )
  {
    for ( int i=0; i < s.length(); i++ )
    {
      if ( s.charAt( i ) != c ) return i;
    }
    return -1;
  }
}
