/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.query;

import java.io.Serializable;

import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;

public class Bind extends QueryParameter implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DataValue _dataValue;
	private DataType  _dataType;
	private String _printValue;
	private String _value;
	private Object _valueObj;
	private Boolean _lastSetObj=false;//several other states eg uninitiated
	private String _withBracket=null;
	public enum Mode{IN,OUT,INOUT,UNKNOWN,RETURNING};
	private Mode mode; 
    /**
     * character off set for start of bind in sql.
     */
	private int _begin = -1;
    /**
     * character off set for end of bind in sql.
     */
	private int _end = -1;
    public Bind() {
    }

    /**
     * constructor with typeBracket containing 'char(10)' as well as 'char'
     */
    public Bind(String name,String type, String typeBracket, String prompt, String value, String tooltip) {
        setBracket(typeBracket);
        init(name,type,prompt,value,tooltip,Mode.UNKNOWN);
    }
    
    /**
     * original constructor containing only type not type bracket (with length).
     */
    public Bind(String name,String type, String prompt, String value, String tooltip) {
    	this(name,type,prompt,value,tooltip,Mode.UNKNOWN);
    }
    
    /**
     * original constructor containing only type not type bracket (with length).
     */
    public Bind(String name,String type, String prompt, String value, String tooltip,Mode mode) {
        setBracket(null);
        init(name,type,prompt,value,tooltip,mode);
    }
    
    
    /**
     * original constructor containing only type not type bracket (with length).
     */
    public Bind(String name,DataType type, DataValue value, Mode mode) {
        setBracket(null);
        setName(name);
        this._dataType = type;
        setDataValue(value);
        this.mode = mode;
    }
    /**
     * generic setter for most variables (except Bracket)
     * @param in 
     */
    public void init(String name,String type, String prompt, String value, String tooltip, Mode mode) {
        setName( name );
        setType( type );
        setPrompt( prompt );
        setTooltip( tooltip );
        setValue(value);
        this.mode = mode;
    }
    
    public Mode getMode(){
    	return mode;
    }
    public void setValue(String value) {
        _lastSetObj=false;
        _value = value;
    }

    public String getValue() {
        return _value;
    }
    
    public boolean isLastSetObj() {
    	return _lastSetObj;
    }
    
    public void setValueObj(Object value) {
    	_lastSetObj=true;
        _valueObj = value;
    }

    public Object getValueObj() {
        return _valueObj;
    }
    public DataValue getDataValue(){
      return _dataValue;
    }
    
    public DataType getDataType(){
      return _dataType;
    }
    
    public void setDataValue(DataValue dataValue){
        _lastSetObj=false;
        _dataValue = dataValue;
    }
    
    public void setBracket(String value) {
        _withBracket = value;
    }

    public String getBracket() {
        return _withBracket;
    }

    public void setBegin(int value) {
        _begin = value;
    }

    public int getBegin() {
        return _begin;
    }
    
    public void setEnd(int value) {
        _end = value;
    }

    public int getEnd() {
        return _end;
    }
    
    public String getPrintBind() {
    	if (_printValue==null) {
    		return getName();
    	} else 
    		return _printValue;
    }
    
    public void setPrintBind(String printName) {
    	_printValue = printName;
    }
    public String toXML(int indent){
        StringBuilder sb = new StringBuilder();
        sb.append(getIndent(indent)+"<bind id=\""+ getName()+"\"" + getBaseAttributes() + ">\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        sb.append(getBaseChildren( indent+1));
        sb.append(getIndent(indent+1)+"<value><![CDATA["+ _value+"]]></value>\n"); //$NON-NLS-1$ //$NON-NLS-2$
        sb.append(getIndent(indent+1)+"<bracket><![CDATA["+ _withBracket+"]]></bracket>\n"); //$NON-NLS-1$ //$NON-NLS-2$
        sb.append(getIndent(indent)+"</bind>\n"); //$NON-NLS-1$
        return sb.toString();
    }
}
