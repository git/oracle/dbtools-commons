/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.query;

import java.io.Serializable;

public class Column extends QueryParameter implements Serializable{
	private static final long serialVersionUID = 1L;

	private String onClickClass;
	private String format;
	private String align;
	private String vAlign;
	private String id;
	private String cellRenderer;
	private String cellEditor;
	private String cellPopup;
	private ClassLoader classLoader;
	private Boolean editable;
	private Boolean hidden;

	public Column() {
	}

	//public Column(String name, String onClick, String align, String vAlign, String format) {
	public Column(String name, String align, String vAlign, String format) {
		setName( name );
		//if (onClick != null) this.m_onClickClass = onClick;
		if (align != null) setAlign(align);
		if (vAlign != null) setVAlign(vAlign);
		if (format != null) this.format = format;
	}

	public void setOnClickClass(String onClickClass) {
		this.onClickClass = onClickClass;
	}

	public String getOnClickClass() {
		return onClickClass;
	}

	public void setAlign(String align) {
		this.align = align;
	}

	public String getAlign() {
		return align;
	}

	public void setVAlign(String vAlign) {
		this.vAlign = vAlign;
	}

	public String getVAlign() {
		return vAlign;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getFormat() {
		return format;
	}

	public void setID( String id ) 
	{
		this.id = id;
	}

	public String getID() 
	{
		return id;
	}

	public String toXML(int indent){
		StringBuilder sb = new StringBuilder();
		sb.append(getIndent(indent)+"<column"); //$NON-NLS-1$
		if ( format != null ) 
			sb.append(" colFormat=\""+format+"\""); //$NON-NLS-1$ //$NON-NLS-2$
			if (align != null)
				sb.append(" align=\""+align+"\""); //$NON-NLS-1$ //$NON-NLS-2$
			if ( vAlign != null) 
				sb.append(" valign=\""+vAlign+"\""); //$NON-NLS-1$ //$NON-NLS-2$
			if ( hidden != null) 
				sb.append(" hidden=\""+hidden.toString()+"\""); //$NON-NLS-1$ //$NON-NLS-2$
			if ( editable != null  ) 
				sb.append(" isEditable=\""+editable.toString()+"\""); //$NON-NLS-1$ //$NON-NLS-2$
			if ( cellRenderer != null ) 
				sb.append(" cellRenderer=\""+cellRenderer+"\""); //$NON-NLS-1$ //$NON-NLS-2$
			if ( cellEditor != null ) 
				sb.append(" cellEditor=\""+cellEditor+"\""); //$NON-NLS-1$ //$NON-NLS-2$
			if ( cellPopup != null ) 
				sb.append(" cellPopup=\""+cellPopup+"\""); //$NON-NLS-1$ //$NON-NLS-2$
			sb.append( getBaseAttributes() );
			sb.append(">\n"); //$NON-NLS-1$
			sb.append(getIndent(indent+1)+"<colName><![CDATA["+getName()+"]]></colName>\n"); //$NON-NLS-1$ //$NON-NLS-2$
			sb.append( getBaseChildren( indent+1 ) );
			sb.append(getIndent(indent)+"</column>\n"); //$NON-NLS-1$
			return sb.toString();
	}

	public void setCellRenderer(String cellRenderer) {
		this.cellRenderer = cellRenderer;
	}

	public String getCellRenderer() {
		return cellRenderer;
	}

	public void setCellEditor(String cellEditor) {
		this.cellEditor = cellEditor;
	}

	public String getCellEditor() {
		return cellEditor;
	}

	public void setCellPopup(String cellPopup) {
		this.cellPopup = cellPopup;
	}

	public String getCellPopup() {
		return cellPopup;
	}

	public void setEditable(boolean edit) {
		editable = new Boolean(edit);
	}

	public Boolean isEditable() {
		return editable != null && editable;
	}

	public void setHidden(boolean hide){
		hidden = hide;
	}

	public boolean isHidden(){
		if ( hidden == null){
			return false;
		}
		return hidden.booleanValue();
	}

	public void setClassLoader(ClassLoader classLoader)
	{
		this.classLoader = classLoader;
	}

	public ClassLoader getClassLoader()
	{
		return classLoader;
	}
}
