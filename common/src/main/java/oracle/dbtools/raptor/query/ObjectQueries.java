/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.query;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.ConnectionIdentifier;
import oracle.xml.parser.v2.XMLNode;

public class ObjectQueries {
	private List<Query> m_queryList;
	private Query m_default;
	private Map<VersionEntry, Query> m_queryMap = new TreeMap<VersionEntry, Query>();
	private XMLNode m_node;

	public ObjectQueries(XMLNode node) {
		m_node = node;
		initQueries(node);
	}

	public String getQueriesAttribute(String name){
	  if ( m_node.getAttributes().getNamedItem(name) != null ) {
	    m_node.getAttributes().getNamedItem(name).getTextContent();
	  } 
	  return null;
	}
	public void addQuery(Version minVer, Version maxVer, Query query) {
		m_queryMap.put(new VersionEntry(minVer, maxVer), query);
	}

	public void setDefaultQuery(Query query) {
		m_default = query;
	}

	public Query getQuery(ConnectionIdentifier id) {
		return QueryUtils.getQuery(m_queryList, id);
	}

	public Query getQuery(ConnectionIdentifier id, boolean prependDBA) {
		return QueryUtils.getQuery(m_queryList, id, prependDBA, false);
	}

	public Query getQuery(Version ver) {
		Query result = null;
		for (VersionEntry ve : m_queryMap.keySet()) {
			if (ver.compareTo(ve.m_min) >= 0) {
				// The version is greater than the entries min version
				if (ve.m_max == null || ver.compareTo(ve.m_max) <= 0) {
					result = m_queryMap.get(ve);
				}
				// continue processing the loop until we get a version that is
				// greater than ours
			} else {
				break;
			}
		}
		return result != null ? result : m_default;
	}

	public int getLoadedQueryCount() {
		return m_queryList.size();
	}

	public void initQueries(XMLNode node) {
		XMLNode loadNode = node == null ? m_node : node;
		m_queryList = new ArrayList<Query>();
		if (loadNode != null) {
			m_queryList.addAll(Query.getQueries(loadNode));
			for (Query query : m_queryList) {
				Version min = query.getMinversion();
				Version max = query.getMaxversion();

				if (min == null && max == null) {
					// This is the default query
					setDefaultQuery(query);
				} else {
					addQuery(min, max, query);
				}
			}
		}
	}

	private class VersionEntry implements Comparable<VersionEntry> {
		private Version m_min;
		private Version m_max;

		private VersionEntry(Version min, Version max) {
			assert min != null || max != null;

			m_min = min;
			m_max = max;
		}

		public int compareTo(VersionEntry other) {
			if (this == other) {
				return 0;
			} else {
				int i = m_min.compareTo(other.m_min);
				if (i == 0) {
					if (m_max != null) {
						i = (other.m_max != null ? m_max.compareTo(other.m_max)
								: 1);
					} else {
						i = (other.m_max != null ? -1 : 0);
					}
				}
				return i;
			}
		}

		public boolean equals(Object obj) {
			return this == obj
					|| ((obj instanceof VersionEntry)
							&& ModelUtil.areEqual(m_min,
									((VersionEntry) obj).m_min) && ModelUtil
								.areEqual(m_max, ((VersionEntry) obj).m_max));
		}

		public int hashCode() {
			return m_min.hashCode() + (m_max != null ? m_max.hashCode() : 0);
		}
	}
}
