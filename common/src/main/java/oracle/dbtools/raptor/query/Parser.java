/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.query;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Token;
import oracle.dbtools.raptor.query.Bind.Mode;
import oracle.dbtools.util.Service;

public class Parser {

    /**
     * Test method
     */
    public static void main( String[] args ) throws Exception {
        final String input = 
            //"select 1 from dual where x=:1 and y=:b"
            Service.readFile(Parser.class,"testBindsParser.sql") //$NON-NLS-1$
            ;
        for( String bind : getInstance().getBindNames(input) )
            System.out.println(bind);
        
    	/*
    	// inlineBinds
        String input = "select 1 from dual where x=:1 and y=:b";
        Map<String, String> binds = new HashMap<String, String>();
        binds.put("1", Integer.toString(1));
        binds.put("b", "be");        
        System.out.println(getInstance().inlineBinds(input,binds));
        // extract all the ? 
        input = "select 1 from dual where x=? and y=?";
        System.out.println(getInstance().getQuestionMarks(input));
        
    	DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
    	final Connection c = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe", "hr", "hr");
    	String sql = "with d as ( SELECT rownum true_rownum, floor(rownum/3) rn FROM dual connect by rownum <= 100 )\n"+
    			"SELECT * FROM d\n"+
    			"MATCH_RECOGNIZE (\n"+
    			"ORDER BY true_rownum\n"+
    			"MEASURES five.rn as five_rn,\n"+
    			"five.true_rownum as five_true_rownum,\n"+
    			"twelve.rn as twelve_rn,\n"+
    			"twelve.true_rownum as twelve_true_rownum\n"+
    			"ONE ROW PER MATCH\n"+
    			"AFTER MATCH SKIP TO NEXT ROW\n"+
    			"PATTERN ( five anything*? twelve )\n"+
    			"DEFINE five AS five.rn = 5,\n"+
    			"twelve AS twelve.rn = 12\n"+
    			") mr;";
    	PreparedStatement cs = c.prepareStatement(sql);
    	cs.setString(1, "?");
    	ResultSet rs = cs.executeQuery();
    	rs.next();
    	*/

    }
    
    
    
    private static Object LOCK = new Object();
    private static Parser _instance;
    

    /**
     * @return - singleton so there's a getInstance()
     */
    public static Parser getInstance() {
        synchronized (LOCK) {
            if (Parser._instance == null)
                Parser._instance = new Parser();
        }
        return Parser._instance;
    }

    /*
     * 
     * @param in - The String to parse and find Binds @return - an ArrayList<String>
     * of the bind's names
     * 
     */
    public ArrayList<String> getBindNames(String in) {
      return getBindNames(in, false);
    }
    
    public ArrayList<String> getBindNames(String in, boolean all) {
      ArrayList<Bind> binds = getBinds(in, all);
      ArrayList<String> ret = new ArrayList<String>();
      for (Bind b : binds) {
          ret.add(b.getName().intern());
      }
      return ret;
    }
    
    /**
     * @param in -
     *            The String to parse and find Binds
     * @return - return
     */
    public ArrayList<Bind> getBinds(String in) {
        return getBinds(in, false);
    }
       
    /**
     * @param in -
     *            The String to parse and find Binds
     * @param all -
     *            if set to true the return will be all binds including
     *            duplicates i.e. A,B,A,A,A if set to false the list of binds
     *            will be unique i.e. A,B
     * @return
     */    
    public ArrayList<Bind> getBinds( String input, boolean all ) {
    // After amending this function
    // please make sure to run BindsTest.main() to catch any regressions
        ArrayList<Bind> ret = new ArrayList<Bind>();
        if( input == null ) // called second time in worksheet with input==null
            return ret;
        if( input.indexOf(':') < 0 )
        	return ret;
        List<LexerToken> src = null;
        synchronized( lexCache ) {
            src = lexCache.get(input);
        }
        if( src == null ) 
        	src =  LexerToken.parse(input);
        boolean sawColon = false;
        boolean sawTrigger = false;
        boolean sawLiteral = false;
        int colonStart = 0;
        boolean foundReturning = false;
        for( LexerToken t : src ) {
            if( sawColon ) {
                if( all || !contains(ret, t.content) ) {
                    if( t.type == Token.IDENTIFIER || t.type == Token.DIGITS ) {
                    	if( sawTrigger && (t.content.equalsIgnoreCase("OLD") || t.content.equalsIgnoreCase("NEW")) ) {
                    		 sawColon = false;
                    		 continue;
                    	}
                        Bind b = new Bind(t.content, null, t.content, "NULL_VALUE", t.content,foundReturning?Mode.RETURNING:Mode.UNKNOWN);
                        b.setBegin(colonStart);
                        b.setEnd(t.end);
                        ret.add(b); //$NON-NLS-1$
                    }
                }
                sawColon = false;
            } else if( ":".equals(t.content) && !sawLiteral ) { //$NON-NLS-1$ 
                sawColon = true;
                colonStart = t.begin;
            } else if( "TRIGGER".equalsIgnoreCase(t.content) ) { //$NON-NLS-1$ 
                sawTrigger = true;
            } else if ("returning".equals(t.content.toLowerCase())){
            	foundReturning = true;
            	sawColon = false;
            }
            sawLiteral = (t.type == Token.QUOTED_STRING);
        }
        synchronized( lexCache ) {
        	cacheQueue.addLast(input); 
        	lexCache.put(input, src);
        	if( 20 < cacheQueue.size() ) {
        		String nuked = cacheQueue.removeFirst();
        		lexCache.remove(nuked);
        	}
        }
        return ret;
    }
    private LinkedList<String> cacheQueue = new LinkedList<String>();
    private Map<String, List<LexerToken>> lexCache = new TreeMap<String, List<LexerToken>>();

    /**
     * 
     * @param input
     * @return sequence of character positions
     */
    public List<Integer> getQuestionMarks( String input ) {
    	List<Integer> ret = new LinkedList<Integer>();
        if( input == null ) // called second time in worksheet with input==null
            return ret;
        List<LexerToken> src =  LexerToken.parse(input,true);
        for( LexerToken t : src ) {
            if( "?".equals(t.content) ) { //$NON-NLS-1$ 
                ret.add(t.begin);
            }
        }
        return ret;
    }

    
    public String inlineBinds( String input, Map<String,String> bindValues ) {
    	StringBuilder ret = new StringBuilder();
        List<LexerToken> src =  LexerToken.parse(input,true);
        boolean sawColon = false;
        for( LexerToken t : src ) {
            if( sawColon ) {
                if( t.type == Token.IDENTIFIER || t.type == Token.DIGITS ) {
                	Object value = bindValues.get(t.content);
                	if( value instanceof Integer )
                		ret.append(value);
                	else
                		ret.append("'"+value+"'");
                }
                sawColon = false;
            } else if( ":".equals(t.content) ) //$NON-NLS-1$
                sawColon = true;
            else
            	ret.append(t.content);
        }
        
        return ret.toString();
    }
    
    
    private static boolean contains( ArrayList<Bind> binds, String name ) {   
        for( Bind bind: binds ) {
            if (bind.getName().equals(name) )
                return true;
        }
        return false;
    }
    
}
