/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.query;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.query.db.QueryDB;
import oracle.dbtools.raptor.query.db.QueryItem;
import oracle.dbtools.raptor.utils.XLIFFHelper;
import oracle.dbtools.raptor.utils.XMLHelper;
import oracle.dbtools.util.Service;

import org.w3c.dom.Node;

final public class Query implements Serializable {
    private static final String DBA_PREFIX = "Dba_"; //$NON-NLS-1$

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String NO_ID = "THIS_QUERY_HAS_NO_ID"; //$NON-NLS-1$

	private transient Version m_minversion;
    private transient Version m_maxversion;
    
        
  //  private XLIFFHelper _xliffHelper;
  //  private Node _node;

	private long _itemID=-1;
	@SuppressWarnings("unused")
    private String _checksum;
	private String _id;
    private long _dbaID=-1;


    /**
     * @param node
     * @return
     */
    public static Query getQuery(Node node){
        return getQuery(node,null,null);
    }

    /**
     * @param node
     * @param xliffHelper
     * @param txn
     * @param cl
     * @return
     */
    public static Query getQuery(Node node, XLIFFHelper xliffHelper, ClassLoader cl){
        return QueryUtils.getQuery(node, xliffHelper,cl);
    }

    /**
     * @param node
     * @param xliffHelper
     * @param txn
     * @return
     */
    public static Query getQuery(Node node, XLIFFHelper xliffHelper){
        return getQuery(node, xliffHelper, null);
    }

    /**
     * @param node
     * @return
     */
    public static List<Query> getQueries(Node node){
        return getQueries(node,null);
    }

    /**
     * @param item
     * @param txn
     * @return
     */
    public static List<Query> getQueries(Node item, ClassLoader cl) {
       return  QueryUtils.getQueries(item,cl);
    }
    
    protected Query(Node node){
    	init();
    	buildQuery(node,null,null);
    }

    /**
     * @param node
     * @param xliffHelper
     * @param txn
     */
    protected Query(Node node, XLIFFHelper xliffHelper){
        this(node, xliffHelper, null);
    }

    /**
     * @param node
     * @param xliffHelper
     * @param txn
     * @param cl
     */
    protected Query(Node node, XLIFFHelper xliffHelper, ClassLoader cl){
    	init();
        buildQuery(node, xliffHelper,cl);
    }
    
    public Query() {
    	init();
    }

    /**
     * @param id
     */
    public Query(long id) {
    	_itemID = id;
    }

    /**
     * @param md
     */
    public Query(String md) {
    	_checksum = md;
	}

    private QueryItem getItem(){
    	 QueryItem qi =  QueryDB.getDatabase().get(_itemID);
        if (qi == null) {
            qi = new QueryItem();
            //on set this will be saved
            if (_itemID!= -1) {
                qi.setPk(_itemID);
            }
        }
        return qi;
    }
//    private QueryItem getItem(Transaction txn){
//        QueryItem ret = null;
//    	if (_itemID != -1) {
//    		ret= QueryDB.getDatabase().get(_itemID,txn);
//    	} else if (_checksum!=null) {
//    		ret= QueryDB.getDatabase().getItemByChecksum(_checksum,txn);
//                _itemID = ret.getPk();
//    	}
//
//    	return ret;
//    }
	private void init(){
    	_itemID = QueryDB.getDatabase().getNextSeq();
    }

    /**
     * @return
     */
    public long getItemPK(){
        return _itemID;
    }

    /**
     * @return
     */
    public Set<String> getRequiredObjects(){
        return getItem().getReqs();
    }

    /**
     * @return
     */
    public String[] getRequiredFeatures() {
        return getItem().getRequiredFeatures();
    }

    /**
     * @param features
     */
    public void setRequiredFeatures(String[] features) {
        getItem().setRequiredFeatures(features);
    }

    /**
     * @param minversion
     */
    public void setMinversion(Version minversion) {
    	QueryItem qi = getItem();
    	qi.setMin(minversion.toCanonicalString());
    	QueryDB.getDatabase().put(qi);
        this.m_minversion = minversion;
    }

    /**
     * @return
     */
    public Version getMinversion() {
    	if (m_minversion == null) {
    		String min = getItem().getMin();
    		m_minversion = min == null  ? null : new Version(min);
     	}

        return this.m_minversion;
    }

    /**
     * @param maxversion
     */
    public void setMaxversion(Version maxversion) {
    	QueryItem qi = getItem();
    	qi.setMax(maxversion.toCanonicalString());
    	QueryDB.getDatabase().put(qi);
        this.m_maxversion = maxversion;
    }

    /**
     * @return
     */
    public Version getMaxversion() {
    	if (m_maxversion == null) {
    		String max = getItem().getMax();
    		m_maxversion = max==null ? null : new Version(max);
    	}
        return this.m_maxversion;
    }

    /**
     * @param sql
     */
    public void setSql(String sql) {
    	QueryItem qi = getItem();
    	qi.setSql(sql);
    	QueryDB.getDatabase().put(qi);

    }

    /**
     * @return
     */
    public String getSql() {
        return getItem().getSql();
    }

    private List<Bind> getRawBinds(){
        List<Bind> binds =  getItem().getBinds();
        if(binds ==null){
            binds = new ArrayList<Bind>();
        }
        return binds;
    }

    /**
     * @param binds
     */
    public void setBinds(List<Bind> binds) {
    	QueryItem qi = getItem();
    	qi.setBinds(binds);
    	QueryDB.getDatabase().put(qi);

    }

    /**
     * @return
     */
    public Map<String,Object> getBindMap(){
        HashMap<String,Object> binds = new HashMap<String,Object>();
        for(Bind b:getRawBinds()){
          if(b.getDataValue()!=null){
            binds.put(b.getName(),b.getDataValue());
          } else {
            binds.put(b.getName(), b.getValue());
          }
        }
        return binds;
    }

    /**
     * @return
     */
    public List<Bind> getBinds() {
    	if (getItem().getSql() == null)
    		return getRawBinds();
        ArrayList<Bind> binds = Parser.getInstance().getBinds(getItem().getSql());
    	if (getRawBinds().size() >=  binds.size()) {
    		ArrayList<Bind> retBinds = new ArrayList<Bind>();
    		for (Bind rb:getRawBinds()){
    			for (Bind qb :binds ){
    				if (qb.getName().equals(rb.getName())){
    					retBinds.add(rb);
    					break;
    				}
    			}
    		}
    		return retBinds;
    	} else
    		return binds;
    }

    /**
     *
     * @return
     */
    public List<Bind> getReportBindsFromSQL() {
        return Parser.getInstance().getBinds(getItem().getSql());
    }
    
    /**
     * @return
     */
    public List<Bind> getReportBinds() {
        List<Bind> binds = getReportBindsFromSQL();
    	if (getRawBinds().size() >=  binds.size()) {
    		return getRawBinds();
    	} else
    		return binds;
    }

    /**
     * @param bind
     */
    public void addBind( Bind bind )
    {
        QueryItem qi = getItem();
        if ( qi.getBinds() == null )
        {
        	qi.setBinds(new ArrayList<Bind>());

        }
        
        String name = bind.getName();
        boolean found = false;
        if ( name != null ) {
            
        }
        for ( Bind b : qi.getBinds() )
        {
            if ( name.equals(b.getName()) )
            {
                found = true;
                break;
            }
        }
        
        if ( !found )
        {
        	qi.getBinds().add( bind );
        }
    	QueryDB.getDatabase().put(qi);

    }

    /**
     * @param binds
     */
    public void setOptionalBinds(List<Bind> binds) {
    	QueryItem qi = getItem();
    	qi.setOptBinds(binds);
    	QueryDB.getDatabase().put(qi);
    }

    /**
     * @return
     */
    public List<Bind> getOptionalBinds() {
      return getItem().getOptBinds();
    }

    /**
     * @param bind
     */
    protected void addOptionalBind( Bind bind )
    {
    	QueryItem qi = getItem();
        if ( qi.getOptBinds() == null )
        {
        	qi.setOptBinds(new ArrayList<Bind>());

        }
        
        String name = bind.getName();
        boolean found = false;
        if ( name != null ) {
            for ( Bind b : qi.getOptBinds() )
            {
                if (name.equals( b.getName() ) )
                {
                    found = true;
                    break;
                }
            }
        }
        
        if ( !found )
        {
        	qi.getOptBinds().add( bind );
        }
    	QueryDB.getDatabase().put(qi);
    }

    /**
     * @return
     */
  public HashMap<String, Object> getBindValues() {
    HashMap<String, Object> bindVals = new HashMap<String, Object>();
    List<Bind> sqlBinds = Parser.getInstance().getBinds(getItem().getSql());
    if (getRawBinds().isEmpty()) {
      getItem().setBinds(sqlBinds);
    }
    if (getItem().getBinds().isEmpty()) {
      return bindVals;
    } else {
      for (int i = 0; i < getRawBinds().size(); i++) {
        Object target = null;
        String name = getRawBinds().get(i).getName();
        String value = getRawBinds().get(i).getValue();//x
        String type = getRawBinds().get(i).getType();
        DataValue dataValue = getRawBinds().get(i).getDataValue();
        if (dataValue != null) {
          bindVals.put(name, dataValue);
        } else {
          if (value != null && value.equals("NULL_VALUE")) { //$NON-NLS-1$
            target = DBUtil.NULL_VALUE;
            bindVals.put(name, target);
          } //WIP, want to support binding/substituting all sorts of preferences 
            //this just allows a default connection name to be bound/substituted
          else if (name.startsWith("SQLDEVPREF")) { //$NON-NLS-1$
            //using the value , find the preference and use it as the target
            target = "target"; //$NON-NLS-1$
            bindVals.put(name, target);//TODO get target name from preference
          } else {
            target = value;
            bindVals.put(name, target);
          }
          if (type != null && type.equalsIgnoreCase(DBUtil.SUBSTITUTION)) {
            handleSubstitution(name, target);
          }
        }
      }
      return bindVals;
    }
  }

    private void handleSubstitution(String bindName,Object bindValue) {
        if(getItem().getSql().indexOf("&"+bindName) != -1 &&
           bindValue instanceof String){//substitution
            String sql = getItem().getSql();
            sql =sql.replaceAll("&"+bindName, (String)bindValue); //$NON-NLS-1$
            getItem().setSql(sql);
        }        
    }

    /**
     * @param cols
     */
    public void setCols(List<Column> cols) {
    	QueryItem qi = getItem();
    	qi.setCols(cols);
    	QueryDB.getDatabase().put(qi);
    }

    /**
     * @return
     */
    public List<Column> getCols() {
        List<Column> cols = getItem().getCols();
        if(cols == null){
             cols = new ArrayList<Column>();
        }
        return cols;
    }

    /**
     * @param col
     */
    protected void addColumn( Column col )
    {
    	QueryItem qi = getItem();
        if ( qi.getCols() == null )
        {
        	qi.setCols(new ArrayList<Column>());
        }
        
        String id = col.getID();
        boolean found = false;
        if ( id != null ) {
            for ( Column c : qi.getCols() )
            {
                if (id.equals( c.getID() ) )
                {
                    found = true;
                    break;
                }
            }
        }
        
        if ( !found )
        {
            qi.getCols().add( col );
        }
    	QueryDB.getDatabase().put(qi);
    }

    /**
     * @return
     */
    public HashMap<String,Object> getColValues() {
        HashMap<String,Object> colVals = new HashMap<String,Object>();
        if (this.getCols().isEmpty()){
            return colVals;
        } else {
            for (int i = 0; i < getCols().size(); i++) {
                String name = getCols().get(i).getName();
                colVals.put(name,getCols().get(i));
            }
            return colVals;
        }
    }


    /**
     * @param node
     * @param xliffHelper
     * @param txn
     */
    public void buildQuery(Node node, XLIFFHelper xliffHelper ){
        buildQuery(node, xliffHelper, null);
    }

    /**
     * @param node
     * @param xliffHelper
     * @param txn
     * @param cl
     */
    public void buildQuery(Node node, XLIFFHelper xliffHelper, ClassLoader cl){
        
    	QueryItem qi = getItem();
        if ( qi ==null){
            qi = new QueryItem(); 
            if (_itemID != -1){
                qi.setPk(_itemID);
            }
        }

        String skipCols = XMLHelper.getNodeValue(node,"nodisplay"); //$NON-NLS-1$
        if ( skipCols != null ){
	        String[] cols = skipCols.split(","); //$NON-NLS-1$
	        List<String> colArr = new ArrayList<String>();
	        for(int i=0;i<cols.length;i++){
	            colArr.add(cols[i]);
	        }
	        qi.setSkipCols(colArr);
        }
        
        List<Column> cols = QueryUtils.getNodeCols(node, cl);
        	qi.setCols(cols);
        List<Bind> optBinds = QueryUtils.getNodeBinds( node, "optionalBinds", xliffHelper ); //$NON-NLS-1$
        	qi.setOptBinds( optBinds );
        List<Bind> binds = QueryUtils.getNodeBinds(node, xliffHelper);
        	qi.setBinds(binds);
        
        qi.setId(XMLHelper.getAttributeNode(node,"id")); //$NON-NLS-1$
        _id = qi.getId();
        if (_id == null) {
        	_id=NO_ID;
        }
        
        String min =  XMLHelper.getAttributeNode(node,"minversion"); //$NON-NLS-1$
        qi.setMin(min);

        String reqs =  XMLHelper.getAttributeNode(node,"requiredAccessObjects"); //$NON-NLS-1$
        if ( reqs != null){
        	HashSet<String> r = new HashSet<String>();
            Collections.addAll(r, reqs.split(",")); //$NON-NLS-1$
        	qi.setReqs(r);
        }
        
        String reqFeatures = XMLHelper.getAttributeNode(node, "requiredFeatures"); //$NON-NLS-1$
        qi.setRequiredFeatures(reqFeatures != null ? reqFeatures.split(",") : null); //$NON-NLS-1$
        
        String max =  XMLHelper.getAttributeNode(node,"maxversion"); //$NON-NLS-1$
        qi.setMax(max);
            
        String sql = XMLHelper.getNodeValue(node,"sql"); //$NON-NLS-1$
        if (sql != null){
            qi.setSql(sql);
        }
        qi.setConstrained( Boolean.parseBoolean( XMLHelper.getAttributeNode( XMLHelper.getChildNode( node, "sql" ), "constrained" ) ) ); //$NON-NLS-1$ //$NON-NLS-2$
        final String canBeConstrained = XMLHelper.getAttributeNode( XMLHelper.getChildNode( node, "sql" ), "canbeconstrained" ); //$NON-NLS-1$ //$NON-NLS-2$ //Bug 20108003
        // Default value for canBeConstrained is true
        qi.setCanBeConstrained( ModelUtil.hasLength(canBeConstrained) ? Boolean.parseBoolean( canBeConstrained ) : true );
        //$NON-NLS-1$ //$NON-NLS-2$
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA"); //$NON-NLS-1$
            String md = new String(digest.digest(node.getTextContent().getBytes()));
            qi.setChecksum(md);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }

    	QueryDB.getDatabase().put(qi);

    }

    /**
     * @param skipCols
     */
    public void setSkipCols(String skipCols) {
       String[] cols = skipCols.split(","); //$NON-NLS-1$
       List<String> colArr = new ArrayList<String>();
       for(int i=0;i<cols.length;i++){
           colArr.add(cols[i]);
       }
       setSkipCols(colArr);
        
    }

    /**
     * @param skipCols
     */
    public void setSkipCols(List<String> skipCols) {
    	QueryItem qi = getItem();
    	qi.setSkipCols(skipCols);
    	QueryDB.getDatabase().put(qi);

    }

    /**
     * @return
     */
    public List<String> getSkipCols() {
        return getItem().getSkipCols();
    }

	public void setCheckSum(String md) {
    	QueryItem qi = getItem();
    	qi.setChecksum(md);
    	QueryDB.getDatabase().put(qi);		
	}

    /**
     * @return
     */
    public String getCheckSum() {
	      return getItem().getChecksum();
	}

    /**
     * @return
     */
    public boolean isConstrained() {
      return getItem().isConstrained();
    }
    
    /** 
     * Can the query be constrained to find an individual object? checked after adding/creating a new object like a table.
     * @return true if a condition can be applied to the query. for example if the query was SELECT * FROM USER_TABLES. 
     * Can it be constrained like. SELECT * FROM USER_TABLES WHERE NAME=:TABLE_NAME.
     * 99% of the cases you can constrain the query to return 1 item which can be added to the tree.
     * But in a HIVE connection for example, the "query" SHOW TABLES cannot be constrained. 
     * Instead SHOW TABLES should be executed again without change, and any new children/rows are added into the tree.
     * Bug 20108003 
     */
    public boolean canBeConstrained() {
      return getItem().canBeConstrained();
    }
    
    /**
     * @param b
     */
    public void setConstrained( boolean b )
    {
    	QueryItem qi = getItem();
    	qi.setConstrained(b);
    	QueryDB.getDatabase().put(qi);
    }
    
    /**
     * @deprecated use {@link #getRequiredObjects()}
     * @return
     */
    public Set<String> getReqs(){
    	return getItem().getReqs();
    }

    /**
     * @param reqs
     */
    public void setReqs(Set<String> reqs){
    	QueryItem qi = getItem();
    	qi.setReqs(reqs);
    	QueryDB.getDatabase().put(qi);
    }

    /**
     * @param idx
     * @return
     */
    public String getIndent(int idx){
    	String indent = ""; //$NON-NLS-1$
    	for (int i=0;i<idx;i++){
    		indent = indent + "\t"; //$NON-NLS-1$
    	}
    	return indent;
    }


    /**
     * @param indent
     * @return
     */
    public String toXML(int indent){
        StringBuilder sb = new StringBuilder();
        sb.append(getIndent(indent)+"<query"); //$NON-NLS-1$
        if ( m_minversion != null ) sb.append(" minversion=\""+ m_minversion +"\""); //$NON-NLS-1$ //$NON-NLS-2$
        if ( m_maxversion != null ) sb.append(" maxversion=\""+ m_maxversion +"\""); //$NON-NLS-1$ //$NON-NLS-2$
        if ( getItem().getReqs() != null ) {
            StringBuilder objs = new StringBuilder();
            for(String s:getItem().getReqs()){
                objs.append(s);
                objs.append(","); //$NON-NLS-1$
            }
            // chop the last ,
            objs.deleteCharAt(objs.length()-1);
            sb.append(" requiredAccessObjects=\""+ objs +"\""); //$NON-NLS-1$ //$NON-NLS-2$
        }
        sb.append(">\n");        //$NON-NLS-1$
        String sql = getItem().getSql();
        sb.append(getIndent(indent+1)+"<sql><![CDATA["+(sql != null ? sql : "")+"]]></sql>\n"); //$NON-NLS-1$ //$NON-NLS-2$
        if ( getRawBinds() != null && getRawBinds().size() > 0 ){
            sb.append(getIndent(indent+1)+"<binds>\n"); //$NON-NLS-1$
            for(Bind bind:getRawBinds()){
                sb.append(bind.toXML(indent+2));
            }
            sb.append(getIndent(indent+1)+"</binds>\n"); //$NON-NLS-1$
        }
        if ( getCols() != null && getCols().size() > 0 ){
            sb.append(getIndent(indent+1)+"<columns>\n"); //$NON-NLS-1$
            for(Column col:getCols()){
                sb.append(col.toXML(indent+2));
            }
            sb.append(getIndent(indent+1)+"</columns>\n"); //$NON-NLS-1$
        }        sb.append(getIndent(indent)+"</query>\n"); //$NON-NLS-1$
        return sb.toString();
    }

    /**
     * @return
     */
    public String getID() {
    	if (_id == null && _id != NO_ID) {
    		_id = getItem().getId();
    	}
        return _id;
    }

    /**
     * @param id
     */
    public void setID(String id){
    	_id = id;
    	QueryItem qi = getItem();
    	qi.setId(id);
    	QueryDB.getDatabase().put(qi);
    }

    
    /**
     * Automatically convert all_* to dba_*
     * @return
     */
    public Query dbaVersion() {
        Query ret =null;
        if ( _dbaID == -1 ) {
            QueryItem baseQI = getQueryItem();
            QueryItem qi = new QueryItem();
            qi.setReqs(new HashSet<String>());
            if( baseQI.getReqs()!=null )
                qi.getReqs().addAll(baseQI.getReqs());

            qi.setSql(baseQI.getSql());
            if( qi.getSql() == null )
                    return null;
            List<LexerToken> src = lex(qi.getSql());
            ParseNode root = parse(src);
            Map<Integer,String> tabAtPos = getAllTablePos(root,src);
            if( tabAtPos.size() == 0 )
                return null;
            for( Integer p : tabAtPos.keySet() ) {
                    qi.setSql(qi.getSql().substring(0, p)+DBA_PREFIX+qi.getSql().substring(p+DBA_PREFIX.length())); 
                    qi.getReqs().add(DBA_PREFIX+tabAtPos.get(p).substring(DBA_PREFIX.length()));  
            }

            qi.setBinds(baseQI.getBinds());
            qi.setCols(baseQI.getCols());
            qi.setConstrained(baseQI.isConstrained());
            qi.setCanBeConstrained(baseQI.canBeConstrained());
            qi.setId(baseQI.getId());
            qi.setMax(baseQI.getMax());
            qi.setMin(baseQI.getMin());
            qi.setOptBinds(baseQI.getOptBinds());
            qi.setSkipCols(baseQI.getSkipCols());
            if (!qi.getSql().equals(getSql())) {
	            ret = new Query();
	            qi.setPk(ret.getItemPK());
	            _dbaID = qi.getPk();
	            QueryDB.getDatabase().put(qi);
            }
        } else {
            ret = new Query(_dbaID);
        }
        
        return ret;
    }

    /**
     * @return
     */
    public QueryItem getQueryItem() {
    	return getItem();
	}

	static List<LexerToken> lex( String input ) {
        return LexerToken.parse(input);
    }
    
    static ParseNode parse( List<LexerToken> src ) {
        SqlEarley earley = SqlEarley.getInstance();
        Matrix matrix = new Matrix(earley);
        earley.parse(src, matrix); 
        return earley.forest(src, matrix);  
    }
    static Map<Integer,String> getAllTablePos( ParseNode root, List<LexerToken> src ) {
        int table_reference = SqlEarley.getInstance().getSymbol("table_reference"); //$NON-NLS-1$

        Map<Integer,String> ret = new HashMap<Integer,String>();
        for( ParseNode child : root.children() ) 
            if( child.contains(table_reference) 
              && child.to-child.from < 5
            ) {
                int pos = -1;
                for( LexerToken t : src ) {
                    pos++;
                    if( child.from <= pos && pos < child.to ) {
                        String tok = t.content.toLowerCase();
                        if( tok.startsWith("all_") ) { //$NON-NLS-1$
                            ret.put(t.begin,tok);
                            break;
                        }
                        if( t.content.startsWith("\"ALL_") ) { //$NON-NLS-1$
                            ret.put(t.begin+1,tok.substring(1, tok.length()-1));
                            break;
                        }
                    }
                }
            } else   
                ret.putAll(getAllTablePos(child, src));
        return ret;
    }

    /**
     * @return
     */
    public String toString() {
        return getSql();
    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main( String[] args ) throws Exception {
        Query q = new Query();
        String input = 
                "select emp from empno;"
                //Service.readFile(Query.class, "test.sql") //$NON-NLS-1$
            ;
        q.setSql(input);
        //q.setSkipCols(new ArrayList<String>());
        Query dbaVersion = q.dbaVersion();
		QueryItem item = dbaVersion.getItem();
		System.out.println(item.getSql());
    }
}
