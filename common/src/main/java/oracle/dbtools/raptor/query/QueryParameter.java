/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.query;

import java.io.Serializable;

import oracle.dbtools.common.utils.ModelUtil;

public abstract class QueryParameter  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String name;
    private String type;
    private boolean sortable;
    private boolean filterable;
    private String prompt;
    private String tooltip;
    private boolean isDefaultFilter;
    
    public QueryParameter() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }

    public boolean isSortable() {
        return sortable;
    }

    public void setFilterable(boolean filterable) {
        this.filterable = filterable;
    }

    public boolean isFilterable() {
        return filterable;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    public String getTooltip() {
        return tooltip;
    }
    
    public void setDefaultFilter(boolean def) {
        isDefaultFilter = def;
    }
    
    public boolean isDefaultFilter() {
        return isDefaultFilter;
    }
    
    public abstract String toXML(int indent);
    
    public static String getIndent(int idx){
    	String indent = ""; //$NON-NLS-1$
    	for (int i=0;i<idx;i++){
    		indent = indent + "\t"; //$NON-NLS-1$
    	}
    	return indent;
    }
    
    protected String getBaseAttributes() {
        StringBuffer sb = new StringBuffer();
        if ( filterable ) {
            sb.append( "filterable=\"true\"" ); //$NON-NLS-1$
        }
        if ( sortable ) {
            sb.append( "sortable=\"true\"" ); //$NON-NLS-1$
        }
        return sb.toString();
    }
    
    protected String getBaseChildren( int indent ) {
        StringBuffer sb = new StringBuffer();
        if ( ModelUtil.hasLength( type ) ) {
            sb.append(getIndent(indent)+"<type><![CDATA["+ type+"]]></type>\n"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        if ( ModelUtil.hasLength( prompt ) ) {
            sb.append(getIndent(indent)+"<prompt><![CDATA["+ prompt+"]]></prompt>\n"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        if ( ModelUtil.hasLength( tooltip ) ) {
            sb.append(getIndent(indent)+"<tooltip><![CDATA["+ tooltip+"]]></tooltip>\n"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        
        return sb.toString();
    }
        
}
