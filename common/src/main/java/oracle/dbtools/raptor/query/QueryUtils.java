/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.query;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.AccessCache;
import oracle.dbtools.db.ConnectionIdentifier;
import oracle.dbtools.db.ConnectionResolver;
import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.db.VersionTracker;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Token;
import oracle.dbtools.raptor.query.db.QueryDB;
import oracle.dbtools.raptor.query.db.QueryItem;
import oracle.dbtools.raptor.utils.DatabaseFeatureRegistry;
import oracle.dbtools.raptor.utils.XLIFFHelper;
import oracle.dbtools.raptor.utils.XMLHelper;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLElement;
import oracle.xml.parser.v2.XMLNode;
import oracle.xml.parser.v2.XMLParseException;
import oracle.xml.parser.v2.XSLException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author klrice
 * 
 */
public class QueryUtils {
	private static Logger LOGGER = Logger.getLogger(QueryUtils.class.getName());
	

	public static List<Bind> getNodeBinds(Node node) {
		return getNodeBinds(node, "binds", null); //$NON-NLS-1$
	}

	public static List<Bind> getNodeBinds(Node node, XLIFFHelper xliffHelper) {
		return getNodeBinds(node, "binds", xliffHelper); //$NON-NLS-1$
	}

	public static List<Bind> getNodeBinds(Node node, String path,
			XLIFFHelper xliffHelper) {
		List<Bind> binds = new ArrayList<Bind>();
		Node bindsNode = XMLHelper.getChildNode(node, path);
		if (bindsNode == null) {
			return binds;
		}
		XMLNode[] children = XMLHelper.getChildNodes(bindsNode, "bind"); //$NON-NLS-1$
		for (XMLNode child : children) {
			String name = XMLHelper.getAttributeNode(child, "id"); //$NON-NLS-1$
			String type = XMLHelper.getNodeValue(child, "type"); //$NON-NLS-1$
			String prompt = XMLHelper.getNodeValue(child, "prompt"); //$NON-NLS-1$
			String value = XMLHelper.getNodeValue(child, "value"); //$NON-NLS-1$
			String tooltip = XMLHelper.getNodeValue(child, "tooltip"); //$NON-NLS-1$
			if (xliffHelper != null) {
				prompt = xliffHelper.getTranslation(prompt);
				tooltip = xliffHelper.getTranslation(tooltip);
			}
			Bind oBind = new Bind(name, type, prompt, value, tooltip);
			oBind.setSortable(Boolean.parseBoolean(XMLHelper.getAttributeNode(
					child, "sortable"))); //$NON-NLS-1$
			oBind.setFilterable(Boolean.parseBoolean(XMLHelper
					.getAttributeNode(child, "filterable"))); //$NON-NLS-1$
			binds.add(oBind);
		}
		return binds;
	}

	public static Query getQuery(Node node) {
		return getQuery(node, null, null);
	}

	public static Query getQuery(Node node, XLIFFHelper xliffHelper) {
		return getQuery(node, xliffHelper, null);
	}

	public static Query getQuery(Node node, XLIFFHelper xliffHelper,
			ClassLoader cl) {
		String md = null;
		try {
			md = new String(MessageDigest.getInstance("SHA").digest(node.getTextContent().getBytes()));
		} catch (NoSuchAlgorithmException e) {
			// SHa is shipped with java so this should be hit
		}
		Query ret = null;
		if (!QueryDB.getDatabase().containsChecksum(md)) {
			// check for a sharedId
			String refId = XMLHelper.getAttributeNode(node, "sharedId"); //$NON-NLS-1$
			if (refId != null && !refId.equals("")) //$NON-NLS-1$
				ret = QueryCache.getQuery(refId);
			// if no id in the cache try and make a new Query
			// check for an id on the query
			String id = XMLHelper.getAttributeNode(node, "id"); //$NON-NLS-1$
			// if there's an id re=build the node
			if (id != null && !id.equals("")) { //$NON-NLS-1$
				ret = QueryCache.getQuery(id);
				if (ret != null)
					ret.buildQuery(node, xliffHelper, null);
			}

			if (ret == null) {
				// otherwise make a new one
				ret = new Query(node, xliffHelper, cl);
			}
		} else {
			// this maybe adding the same report in twice under a different md
			// but atleast it better than it was , where reports where "copied"
			// with no information cloned.
			ret = new Query(node, xliffHelper);
		}
		return ret;
	}

	public static List<Column> getNodeCols(Node node) {
		return getNodeCols(node, null);
	}

	public static List<Column> getNodeCols(Node node, ClassLoader cl) {
		List<Column> cols = new ArrayList<Column>();
		Node colsNode = XMLHelper.getChildNode(node, "columns"); //$NON-NLS-1$
		if (colsNode == null) {
			return cols;
		}
		XMLNode[] children = XMLHelper.getChildNodes(colsNode, "column"); //$NON-NLS-1$
		for (XMLNode child : children) {
			String name = XMLHelper.getNodeValue(child, "colName"); //$NON-NLS-1$
			if (name != null)
				name = name.trim();
			if (ModelUtil.hasLength(name)) {
				// String onClick =
				// XMLHelper.getAttributeNode(child,"onClickClass");
				String align = XMLHelper.getAttributeNode(child, "align"); //$NON-NLS-1$
				String vAlign = XMLHelper.getAttributeNode(child, "valign"); //$NON-NLS-1$
				String format = XMLHelper.getAttributeNode(child, "colFormat"); //$NON-NLS-1$
				// Column oCol = new Column(name,onClick,align,vAlign,format);
				Column oCol = new Column(name, align, vAlign, format);
				oCol.setType(XMLHelper.getNodeValue(child, "type")); //$NON-NLS-1$
				oCol.setID(XMLHelper.getAttributeNode(child, "id")); //$NON-NLS-1$
				oCol.setSortable(Boolean.parseBoolean(XMLHelper
						.getAttributeNode(child, "sortable"))); //$NON-NLS-1$
				oCol.setFilterable(Boolean.parseBoolean(XMLHelper
						.getAttributeNode(child, "filterable"))); //$NON-NLS-1$
				oCol.setDefaultFilter(Boolean.parseBoolean(XMLHelper
						.getAttributeNode(child, "defaultFilter"))); //$NON-NLS-1$
				oCol.setEditable(Boolean.parseBoolean(XMLHelper
						.getAttributeNode(child, "isEditable"))); //$NON-NLS-1$
				oCol.setHidden(Boolean.parseBoolean(XMLHelper.getAttributeNode(
						child, "hidden"))); //$NON-NLS-1$
				oCol.setCellEditor(XMLHelper.getAttributeNode(child,
						"cellEditor")); //$NON-NLS-1$
				oCol.setCellRenderer(XMLHelper.getAttributeNode(child,
						"cellRenderer")); //$NON-NLS-1$
				oCol.setCellPopup(XMLHelper
						.getAttributeNode(child, "cellPopup")); //$NON-NLS-1$
				oCol.setClassLoader(cl);
				cols.add(oCol);
			}
		}
		return cols;
	}

	public static List<Query> getQueries(Node node) {
		return getQueries(node, null);
	}

	public static List<Query> getQueries(Node node, ClassLoader cl) {
		List<Query> ret = null;

		if (node != null) {
			// try to look up the queries tag
			String shareId = XMLHelper.getAttributeNode(node, "sharedId"); //$NON-NLS-1$
			if (shareId != null && !shareId.equals("")) { //$NON-NLS-1$
				ret = QueryCache.getQueries(shareId);
			}

			if (ret == null) {
				ret = new ArrayList<Query>();
				XMLNode[] children = XMLHelper.getChildNodes(node, "query"); //$NON-NLS-1$
				for (XMLNode child : children) {
					Query qObject = Query.getQuery(child, null, cl);
					ret.add(qObject);
				}

				// parse the columns
				List<Bind> globalBinds = getNodeBinds(node);
				List<Column> globalCols = getNodeCols(node);
				List<Bind> globalOptionalBinds = getNodeBinds(node,
						"optionalBinds", null); //$NON-NLS-1$

				for (Query q : ret) {
					for (Bind b : globalBinds) {
						q.addBind(b);
					}
					for (Column c : globalCols) {
						q.addColumn(c);
					}
					for (Bind b : globalOptionalBinds) {
						q.addOptionalBind(b);
					}
				}

				// // if an ID is set cache it
				String id = XMLHelper.getAttributeNode(node, "id"); //$NON-NLS-1$
				if (id != null && !id.equals("")) //$NON-NLS-1$
					QueryCache.putQuery(id, ret);
			}
		}
		return ret;
	}

//	public static Query getQuery(List<Query> queries, String connName) {
//		return getQuery(queries, connName, false);
//	}
//
//	public static Query getQuery(List<Query> queries, String connName,
//			boolean prependDbaVersion) {
//		Query result = null;
//		try {
//			result = getQuery(queries, DBUtil.getConnectionResolver()
//					.getConnection(connName), prependDbaVersion, false);
//		} catch (Exception e) {
//			ConnectionSupport.getExceptionReporter().raiseException(e);
//		}
//		return result;
//	}
//
	private static class VersionComp implements Comparator<Version> {
		public int compare(Version v1, Version v2) {
			if (v1 == null) {
				return v2 == null ? 0 : -1;
			} else {
				return v2 == null ? 1 : v1.compareTo(v2);
			}
		}
	}

	public static Query getQuery(List<Query> queries, ConnectionIdentifier id) {
		return getQuery(queries, id, false, false);
	}

	/**
	 * If dba version is legitimate return it, otherwise return input
	 */
	public static Query promoteToDba( Query input, ConnectionIdentifier id ) {
	    List<Query> tmp = new LinkedList<Query>();
	    tmp.add(input);
	    prependDbaVersion(tmp);
	    return getQuery(tmp,id,true,false);
	}
	private static Map<ConnectionIdentifier,Map<String,Boolean>> connection2objectDba = new HashMap<ConnectionIdentifier,Map<String,Boolean>>();
    public static String promoteToDba( String sql, ConnectionIdentifier id ) {
        Map<String,Boolean> knownObjects = connection2objectDba.get(id);
        if( knownObjects == null ) {
            knownObjects = new HashMap<String,Boolean>();
            connection2objectDba.put(id, knownObjects);
        }
        
        boolean matchesAllUser = matchesAnyPrefix(sql,
                                                  new String[] { "all_", "user_" }); // doesn't work: sql.matches("/all_|user_/i");  //$NON-NLS-1$//$NON-NLS-2$
        if ( matchesAllUser ) {
            List<LexerToken> src = Query.lex(sql);
            ParseNode root = Query.parse(src);
            Map<Integer,String> tabAtPos = Query.getAllTablePos(root,src);
            if( tabAtPos.size() == 0 )
                return null;
            String ret = sql;
            for( Integer p : tabAtPos.keySet() ) {
                
                String dbao = DBA_PREFIX+tabAtPos.get(p).substring(DBA_PREFIX.length());
                Boolean hasAccess = knownObjects.get(dbao);
                if( hasAccess == null ) {
                	Connection conn = id.getConnection();
                    try( Statement stmt = conn.createStatement() ){
                        String tmp = "select 1 from "+ dbao + " where 1=2"; //$NON-NLS-1$ //$NON-NLS-2$
                        stmt.execute( tmp );
                        hasAccess = true;
                    } catch( SQLException e ) {
                        hasAccess = false;
                    }
                    knownObjects.put(dbao, hasAccess);
                }

                if( hasAccess )
                    ret = ret.substring(0, p)+DBA_PREFIX+ret.substring(p+DBA_PREFIX.length()); 
            }
            
            if( ret.toLowerCase().contains("dba_tab_privs") || ret.toLowerCase().contains("dba_col_privs") )
                ret = ret.replace("table_schema", "OwNeR"); // that's right DBA_tab_privs and ALL_tab_privs views have differently named column!
            return ret;
        }
        return sql;
    }
	
	@SuppressWarnings("unchecked")
	public static Query getQuery(List<Query> queries, ConnectionIdentifier id,
			boolean prependDbaVersion, boolean cacheOnlyCheck) {
		ArrayList<Query> validForVersion = new ArrayList<Query>();
		// check the version numbers
		// for all applicable
		for (Query q : queries) {
			// Before we check versions we should check for features - the list
			// is cached, so it should be fast
			if (hasRequiredFeatures(id, q.getRequiredFeatures())) {

				if (VersionTracker.checkVersion(id, q.getMaxversion(),q.getMinversion())) {
					//
					// check to see if q has a higher min than what's there
					List<Query> check = (List<Query>) validForVersion.clone();
					Comparator<Version> comp = new VersionComp();
					for (Query q1 : check) {
						// Check required features
						// a higher min
						if (comp.compare(q.getMinversion(), q1.getMinversion()) > 0) {
							// if the min for q is higher than the min for q1
							// remove q1
							validForVersion.remove(q1);
							// and add q
							if (!validForVersion.contains(q))
								validForVersion.add(q);
						} else if (comp.compare(q.getMinversion(),
								q1.getMinversion()) == 0) {
							// front load the Query will required Objects
							if (q.getRequiredObjects() != null) {
								validForVersion.add(0, q);
							} else {
								validForVersion.add(q);
							}
						}// end min check on new Query
					}// end loop
						// first one
					if (validForVersion.size() == 0)
						validForVersion.add(q);

				}// end version check
			}
		}// end main loop

		boolean isTimesTen = false;
		Connection conn = id.getConnection();
		if (!ConnectionResolver.isOracle(conn)) {
			try {
				DatabaseMetaData dbmd = conn.getMetaData();
				String database = dbmd.getDatabaseProductName();
				if (database.indexOf("TimesTen") != -1)isTimesTen = true; //$NON-NLS-1$
			} catch (Exception ex) {
			}
		}
		if (prependDbaVersion || calledfromObjectViewer())
			if (!isTimesTen)
				prependDbaVersion(validForVersion);

		// if there's only 1 return it
		if (validForVersion.size() == 1
				&& (validForVersion.get(0).getRequiredObjects() == null || validForVersion
						.get(0).getRequiredObjects().size() == 0))
			return validForVersion.get(0);

		// if nothing has
		Query ret = null;
		// For oracle check all_objects
		// if ( Connections.getInstance().isOracle(conn) ){
		// // if there's more than one check the requiredAccess attribute
		// ret = checkOracleAccess( conn, validForVersion );
		// } else {
		// for others check for existence with a dumb query

		// bug 5941049 - use the simple query for Oracle too. There was a case
		// where DBA_OBJECTS would show up in ALL_OBJECTS, yet a select on it
		// would fail with an ORA-942
		ret = checkNonOracleAccess(id, validForVersion, cacheOnlyCheck);
		// }
		return ret;
	}

	public static boolean hasRequiredFeatures(ConnectionIdentifier id,
			String[] reqFeatures) {
		if (reqFeatures != null) {
			for (String feature : reqFeatures) {
				if (!hasRequiredFeature(id, feature)) {
					return false;
				}
			}
		}
		return true;
	}

	private static boolean hasRequiredFeature(ConnectionIdentifier id, String feature) {
		// Remove any leading/trailing whitespace
		feature = feature.trim();
		// Check for a negation of a feature
		boolean invert = feature.startsWith("!"); //$NON-NLS-1$
		if (invert) {
			feature = feature.substring(1);
		}
		return feature.length() == 0
				|| (invert ^ DatabaseFeatureRegistry.isFeatureEnabled(id, feature));
	}

	private static final String DBA_PREFIX = "DBA_"; //$NON-NLS-1$

	private static boolean hasDBAQuery(Query q) {
		Set<String> reqs = q.getRequiredObjects();
		if (reqs != null) {
			for (String req : reqs) {
				int idx = req.indexOf('.');
				if (idx > 0) {
					// qualified name
					req = req.substring(idx + 1);
				}
				if (req.length() > DBA_PREFIX.length()
						&& req.substring(0, DBA_PREFIX.length())
								.equalsIgnoreCase(DBA_PREFIX)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Prepend dba_ versions of the query (Bug 13953985: need more than one)
	 * 
	 * @param validForVersion
	 */
	private static void prependDbaVersion(List<Query> validForVersion) {
		boolean hasDbaVersion = false;
		for (Query q : validForVersion) {
			if (hasDBAQuery(q)) {
				hasDbaVersion = true;
				break;
			}
		}
		if (!hasDbaVersion) {
			List<Query> addendum = new LinkedList<Query>();
			for (Query q : validForVersion) {
				String sql = q.getSql();
				if (sql != null) {
					boolean matchesAllUser = matchesAnyPrefix(sql,
							new String[] { "all_", "user_" }); // doesn't work: sql.matches("/all_|user_/i");  //$NON-NLS-1$//$NON-NLS-2$
					boolean matchesColPrivs = matchesAnyPostfix(sql,
							new String[] { "col_privs" }); // doesn't work: sql.matches("/all_|user_/i"); //$NON-NLS-1$
					if (matchesAllUser && !matchesColPrivs) {
						Query dbaVersion = q.dbaVersion();
						if (dbaVersion != null) {
							addendum.add(dbaVersion);
						}
					}
				}
			}
			validForVersion.addAll(0, addendum);
		}
	}

	static private boolean matchesAnyPrefix(String input, String[] prefixes) {
		List<LexerToken> src = LexerToken.parse(input);
		for (LexerToken t : src) {
			if( t.type != Token.IDENTIFIER && t.type != Token.DQUOTED_STRING )
				continue;
			String cmp = t.content.toUpperCase();
			for (String pref : prefixes) {
				if (cmp.startsWith(pref.toUpperCase()))
					return true;
				if( '"' == cmp.charAt(0) && t.content.substring(1).startsWith(pref.toUpperCase()) )
					return true;
			}
		}
		return false;
	}

	static private boolean matchesAnyPostfix(String input, String[] prefixes) {
		List<LexerToken> src = LexerToken.parse(input);
		for (LexerToken t : src) {
			String cmp = t.content.toUpperCase();
			for (String pref : prefixes)
				if (cmp.endsWith(pref.toUpperCase()))
					return true;
		}
		return false;
	}

	/*
	 * Hack: prependDbaVersion parameter has to be added in all the call chain
	 * at oracle.dbtools.raptor.query.QueryUtils.getQuery(QueryUtils.java:241)
	 * at
	 * oracle.dbtools.raptor.controls.display.DisplayModel.getQuery(DisplayModel
	 * .java:456) at
	 * oracle.dbtools.raptor.controls.display.DisplayModel.getQuery
	 * (DisplayModel.java:449) at
	 * oracle.dbtools.raptor.controls.display.DisplayPanel
	 * .buildUI(DisplayPanel.java:493) at
	 * oracle.dbtools.raptor.controls.display.
	 * DisplayPanel.<init>(DisplayPanel.java:186) at
	 * oracle.dbtools.raptor.oviewer
	 * .xmleditor.XMLBasedEditor.createViewerUI(XMLBasedEditor.java:277) so the
	 * question is if we want to make DisplayModel&Panel aware of it
	 */
	private static boolean calledfromObjectViewer() {
	  
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		boolean ret = false;
		for (StackTraceElement e : stackTrace) {
			String line = e.toString();
			if (line.contains("oviewer")) //$NON-NLS-1$
				ret = true;
			if (line.contains("ViewerEditorAddin.getEditorWeight")) //$NON-NLS-1$
				return false;
		}
		return ret;
	}

	private static Query checkNonOracleAccess(ConnectionIdentifier id,
			ArrayList<Query> validForVersion, boolean cacheOnlyCheck) {
		// Avoid NPE caused when e.g., checking for query where min/max version
		// does not pass
		if (validForVersion.size() == 0) {
			return null;
		}

		Query ret = null;
		// need to add a special case to facilitate the report editor
		// it will ask for the query but pass a null connection in
		// that case this will always return null
		// so I am adding this test to return the first query if the connection
		// is null
		// talked with Vadim and we felt this was the lowest impact solution -
		// skutz 1/26/2010
		Connection conn = id.getConnection();
		if (conn == null)
			return validForVersion.get(0);

		// Lock across the entire check - should either check all queries or
		// none
		boolean lock = false;
		if (!cacheOnlyCheck) {
			lock = LockManager.lock(conn);
		}
		if (cacheOnlyCheck || lock) {
			try {
				DBUtil dbUtil = DBUtil.getInstance(conn);
				for (Query q : validForVersion) {
					// if there's required object loop them
					final Set<String> reqObjs = q.getRequiredObjects();
					if (reqObjs != null) {
						// flag
						boolean ok = true;
						for (String s : reqObjs) {
						  boolean skipCache = false;
						  String objName = s;
						  if ( s.startsWith("NOCACHE:") ){
						    skipCache = true;
						    objName = s.replace("NOCACHE:", "");
						  }
							if (cacheOnlyCheck && !skipCache) {
								switch( AccessCache.hasAccessCached(id, objName) ) {
									case TRUE:
										ok = true;
										break;
									case FALSE:
										ok = false;
										break;
									default:
										return null;
								}
							} else {
								if (!AccessCache.hasAccess(id, objName)) {
									ok = false;
									break;
								}
							}
						}
						// if everything was ok set the ret
						if (ok) {
							ret = q;
							break;
						}
					} else if (ret == null) {
						// if the ret isn't set and the q has no required
						// objects set the ret to q
						ret = q;
					}
				}
			} finally {
				if (lock) {
					LockManager.unlock(conn);
				}
			}
		}
		return ret;
	}

	// private static Query checkOracleAccess( Connection conn,
	// ArrayList<Query> validForVersion )
	// {
	// Query ret = null;
	// for(Query q:validForVersion){
	// if ( q.getRequiredObjects() != null){
	// StringBuilder sql = new
	// StringBuilder("select count(1) from all_objects where ");
	// ArrayList<String> binds = new ArrayList<String>();
	// for(int i=0;i<q.getRequiredObjects().size();i++){
	// String obj = q.getRequiredObjects().get(i);
	// String[] ownerName = obj.split("\\.");
	// // schema
	// binds.add(ownerName[0]);
	// // name
	// binds.add(ownerName[1]);
	// // add in the where
	// sql.append(" ( owner = ? and object_name = ? )");
	// // if not the last add or ...
	// if ( q.getRequiredObjects().size()-1 > i)
	// sql.append(" \n or \n ");
	// }
	//
	// String retVal = DBUtil.getInstance().executeReturnOneCol(conn,
	// sql.toString(), binds);
	// if ( q.getRequiredObjects().size() == Integer.parseInt(retVal)){
	// ret = q;
	// }
	// } else if ( ret == null){
	// ret = q;
	// }
	// }
	// return ret;
	// }

	public static void loadFile(String s, BufferedOutputStream out) {
		DOMParser parser = new DOMParser();
		parser.setPreserveWhitespace(false);
		try {
			InputStream in = QueryUtils.class.getResourceAsStream(s.trim());
			if (in == null) {
				File f = new File(s.trim());
				if (f.exists())
					in = new FileInputStream(f);
			}
			parser.parse(in);
			XMLDocument document = parser.getDocument();
			NodeList nl = document.selectNodes("//query"); //$NON-NLS-1$
			for (int i = 0; i < nl.getLength(); i++) {
				XMLElement el = (XMLElement) nl.item(i);
				if (el.getAttribute("id") != null && !el.getAttribute("id").equals("")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					LOGGER.info(Messages.getString("QueryUtils.36") + el.getNodeName() + ":" + el.getAttribute("id") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					try {
						out.write((Messages.getString("QueryUtils.40") + el.getAttribute("id") + "\n").getBytes()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					} catch (IOException e) {
						LOGGER.log(Level.WARNING,
								Messages.getString("QueryUtils.43"), e); //$NON-NLS-1$
					}
					QueryUtils.getQuery(el);
				}
			}
		} catch (XMLParseException e) {
			LOGGER.log(Level.WARNING, Messages.getString("QueryUtils.44"), e); //$NON-NLS-1$
		} catch (SAXException e) {
			LOGGER.log(Level.WARNING, Messages.getString("QueryUtils.45"), e); //$NON-NLS-1$
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, Messages.getString("QueryUtils.46"), e); //$NON-NLS-1$
		} catch (XSLException e) {
			LOGGER.log(Level.WARNING, Messages.getString("QueryUtils.47"), e); //$NON-NLS-1$
		}

	}

	// copied from ActionSql need to sync them on main
	public static String expandQuery(String sql, Map<String, Object> subs) {
		String ret = sql;
		if (subs != null) {
			String val;
			Iterator<String> iter = subs.keySet().iterator();
			while (iter.hasNext()) {
				val = iter.next();
				if (ret.toUpperCase().indexOf("#" + val.toUpperCase() + "#") > 0) //$NON-NLS-1$ //$NON-NLS-2$
					ret = subHashMark(ret, val, subs.get(val));
			}
		}
		return ret;
	}

	// copied from ActionSql need to sync them on main
	private static String subHashMark(String s, String name, Object realValue) {
		Object value = realValue;
		if (realValue == null) {
			value = ""; //$NON-NLS-1$
		} else if (realValue instanceof oracle.sql.Datum) {
			try {
				value = ((oracle.sql.Datum) realValue).stringValue();
			} catch (SQLException e) {
				Logger.getLogger(QueryUtils.class.getClass().getName()).log(
						Level.WARNING, e.getStackTrace()[0].toString(), e);
			}
		}
		// direct substituitions
		// (?i) make the replace case insensative
		String ret = s;
		StringBuilder sb = new StringBuilder();
		String subVal = null;
		final String valString = value.toString();
		if (valString.indexOf('$') > 0) {
			for (int i = 0; i < valString.length(); i++) {
				if (valString.charAt(i) == '$') {
					sb.append("\\" + valString.charAt(i)); //$NON-NLS-1$
				} else {
					sb.append(valString.charAt(i));
				}
			}
			subVal = sb.toString();
		} else {
			subVal = valString;
		}
		try {
			// replace #0# with value
			ret = ret.replaceAll("((?i)#" + name + "#)", subVal); //$NON-NLS-1$ //$NON-NLS-2$
			// replace #"0"# with ,"value"
			ret = ret.replaceAll(
					"((?i)#\"" + name + "\"#)", "\"" + subVal + "\""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			// replace #,0# with ,value when not null and nothing when null
			ret = ret
					.replaceAll(
							"((?i)#," + name + "#)", "".equals(subVal) ? "" : "," + subVal); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			// replace #,"0"# with ,"value" when not null and nothing when null
			ret = ret
					.replaceAll(
							"((?i)#,\"" + name + "\"#)", "".equals(subVal) ? "" : ",\"" + subVal + "\""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
			// replace #esc(0)# with value with sql escaped chars
			ret = ret
					.replaceAll(
							"((?i)#esc(" + name + ")#)", "".equals(subVal) ? "" : subVal.replaceAll("'", "''")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
		} catch (Exception e) {
			Logger.getLogger(QueryUtils.class.getClass().getName()).log(
					Level.WARNING, e.getStackTrace()[0].toString(), e);
		}
		return ret;
	}
	
	public static void main( String[] args ) throws Exception {
     	DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
    	final Connection c = DriverManager.getConnection("jdbc:oracle:thin:@gbr30060.uk.oracle.com:1521/DB12PERF", "hr", "hr");
        System.out.println( c.toString() );
        System.out.println( promoteToDba("select * from \"ALL_OBJECTS\" where rownum < 10", DefaultConnectionIdentifier.createIdentifier(c)) );
        System.out.println( promoteToDba("select * from \"SYS\".\"ALL_OBJECTS\" where rownum < 3", DefaultConnectionIdentifier.createIdentifier(c)) );
        System.out.println( promoteToDba("select * from aLL_oBjects where rownum < 5", DefaultConnectionIdentifier.createIdentifier(c)) );
    }
}
