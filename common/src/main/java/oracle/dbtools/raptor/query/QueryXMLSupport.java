/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.query;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.MetaResource;
import oracle.dbtools.db.ConnectionIdentifier;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.raptor.utils.XMLHelper;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLNode;

import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class QueryXMLSupport {
  //use ThreadLocal to provide a unique HashMap for each thread. Helps make ScriptRunner thread safe.
	private final static ThreadLocal<HashMap<String, QueryXMLSupport>> s_sql = new ThreadLocal<HashMap<String, QueryXMLSupport>>(){
    @Override
    protected HashMap<String, QueryXMLSupport> initialValue()
    {
        return new HashMap<String, QueryXMLSupport>();
    }
  };
  //use ThreadLocal to provide a unique HashMap for each thread. Helps make ScriptRunner thread safe.
	private final static ThreadLocal<HashMap<String,Map<String,String>>> s_typeAtts = new ThreadLocal<HashMap<String,Map<String,String>>>(){
    @Override
    protected HashMap<String,Map<String,String>> initialValue()
    {
        return new HashMap<String,Map<String,String>>();
    }
  };
	
	/**
	 * @deprecated use getQueryXMLSupport(MetaResource)
	 * @param path
	 * @return
	 */
	public static QueryXMLSupport getQueryXMLSupport(String path) {
		return getQueryXMLSupport(new MetaResource(
				QueryXMLSupport.class.getClassLoader(), path));
	}

	public static QueryXMLSupport getQueryXMLSupport(MetaResource path) {
	  if ( path.getResourcePath().startsWith("file://") ){
	    // throw out the old for an override
	    getSQL().remove(path.getResourcePath());
	  }
		QueryXMLSupport support = getSQL().get(path.getResourcePath());
		if (support == null) {
			URL u = path.toURL();
			if ( u == null && path.getResourcePath().startsWith("file://")){
			  try {
			    // if a file for overrides
          u = new URL(path.getResourcePath());
        } catch (MalformedURLException e) {
        }
			}
			if (u != null) {
				// if we found a url process it
				DOMParser parser = new DOMParser();
				parser.setPreserveWhitespace(false);
				InputStream in = null;
        try {
          in = u.openStream();
          parser.parse(in);
          XMLDocument doc = parser.getDocument();
          QueryXMLSupport queryXMLSupport = getQueryXMLSupport(doc);
          if(queryXMLSupport != null){
            support = queryXMLSupport;
          }
          getSQL().put(path.getResourcePath(), support);
          
				} catch (Exception e) {
					Logger.getLogger(QueryXMLSupport.class.getClass().getName())
							.log(Level.WARNING,
									e.getStackTrace()[0].toString(), e);
				} finally {
					if (in != null)
						try {
							in.close();
						} catch (Exception e) {
						}
				}
			}
		}
		return support;
}
	/**
	 * Allows the SQLScriptGenerator in ORDS to process XML Modules of type sql/script
	 */
	public static QueryXMLSupport getQueryXMLSupport(XMLDocument doc) {
	  Node srcNode = XMLHelper.getChildNode(doc, "/source"); //$NON-NLS-1$
    if (srcNode != null) {
      HashMap<String, ObjectQueries> queryMap = new HashMap<String, ObjectQueries>();

      for (Node typeNode : XMLHelper.getChildNodes(srcNode, "queryType")) //$NON-NLS-1$
      {
        String typeName = XMLHelper.getAttributeNode(typeNode, "id"); //$NON-NLS-1$
        ObjectQueries q = new ObjectQueries((XMLNode) XMLHelper.getChildNode(typeNode, "queries")); //$NON-NLS-1$
        
        queryMap.put(typeName, q);
        
        HashMap<String,String> attrMap = new HashMap<String,String>();
        
        NamedNodeMap attrs = typeNode.getAttributes();  
        for(int i = 0 ; i<attrs.getLength() ; i++) {
          Attr attribute = (Attr)attrs.item(i);     
          attrMap.put(attribute.getName(), attribute.getValue());
        }
        
        getTypeAttr().put(typeName, attrMap);
        
      }
      return new QueryXMLSupport(queryMap);
    } else {
      return null;
    }
  }
 

 /** 
  * Use ThreadLocal to provide a unique HashMap for each thread. Helps make ScriptRunner thread safe.
  * 
  */
	private static HashMap<String,Map<String,String>> getTypeAttr() {
		 HashMap<String,Map<String,String>> typeAttr = s_typeAtts.get();
		 return typeAttr;
	}
 /** 
  * Use ThreadLocal to provide a unique HashMap for each thread. Helps make ScriptRunner thread safe.
  * 
  */
	private static HashMap<String, QueryXMLSupport> getSQL() {
		HashMap<String, QueryXMLSupport> sql = s_sql.get();
		return sql;
	}

	private Map<String, ObjectQueries> m_queryMap;

	private QueryXMLSupport(Map<String, ObjectQueries> queries) {
		m_queryMap = queries;
	}
	
	
  public String getQueryAttribute(String type, String name) {
    if ( getTypeAttr().get(type) != null && getTypeAttr().get(type).get(name)  != null ) {
      return getTypeAttr().get(type).get(name);
    }
    
    return null;
  }
  
  
	public Query getQuery(String type, ConnectionIdentifier id) {
		ObjectQueries queries = m_queryMap.get(type);
		return queries != null ? queries.getQuery(id) : null;
	}

	/**
	 * If you are calling this method, you are strongly advised to amend it with DBA promotion, e.g.
          Query q = getXMLSupport().getQuery("Runner.CompilationUnit", conn); 
          q = QueryUtils.promoteToDba(q, conn);
	 * @param type
	 * @param conn
	 * @return
	 */
	public Query getQuery(String type, Connection conn) {
		ObjectQueries queries = m_queryMap.get(type);
		return queries != null ? queries.getQuery(DefaultConnectionIdentifier.createIdentifier(conn)) : null;
	}
}
