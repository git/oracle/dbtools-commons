/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.query.db;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class QueryDB {

    private static QueryDB INSTANCE;
    synchronized public static QueryDB getDatabase() {
        if (INSTANCE == null) {
            INSTANCE = new QueryDB();
        }
        return INSTANCE;
    }

    
    // Lock object to use to synchronize iterations over the cache and modifications
    // to the cache
    private Object LOCK = new Object();
    private long SEQ = 0;

    
    private Map<String,Long> QI_CHECKSUM_CACHE = new HashMap<String,Long>();
    private Map<Long,QueryItem> QI_CACHE = new HashMap<Long,QueryItem>();
    
    private QueryDB() {
    }

    public QueryItem getNewQueryItem() {
        QueryItem ret = new QueryItem();
        put(ret);
        return ret;
    }

    public QueryItem get(long id) {
    	return QI_CACHE.get(id);
    }

    public void put(QueryItem item) {
	    synchronized (LOCK) {
	    	if ( item.getPk() == null ){
	    		item.setPk(SEQ++);
	    	}
	    	QI_CACHE.put(item.getPk(),item);
	    	if ( item.getChecksum() != null ){
	    		QI_CHECKSUM_CACHE.put(item.getChecksum(),item.getPk());
	    	}
	    	
	    }
    }

    public long truncate() {
        synchronized (LOCK) {
            int size = QI_CACHE.size();
            QI_CACHE.clear();
            return size;
        }
    }

    public long getRowCount() {
    	return QI_CACHE.size();
    }

    public QueryItem findSharedQuery(String shareId) {
        QueryItem ret = null;
        
        // If there isn't a shareId to check, don't bother looking through
        // the cache
        if ( shareId != null ) {
            // Important - we must synchronize on teh LOCK to ensure
            // that the list of queries does not get modified by another thread
            // as we are iterating over it
            synchronized (LOCK) {
                Collection<QueryItem> qis = QI_CACHE.values();
                for ( QueryItem qi : qis ) {
                    if ( shareId.equals(qi.getId())) {
                        ret = qi;
                        break;
                    }
                }
            }
         }
        return ret;
    }

	public boolean containsChecksum(String md) {
		return QI_CHECKSUM_CACHE.containsKey(md);
	}

	public long getNextSeq() {
		synchronized (LOCK) {
			return SEQ++;
		}
	}
}
