/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.query.db;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import oracle.dbtools.raptor.query.Bind;
import oracle.dbtools.raptor.query.Column;

// TODO: Auto-generated Javadoc
/**
 * The Class QueryItem.
 */
public class QueryItem implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3L;
	
	/** The pk. */
	private Long pk;
    
    /** The id. */
    String id;
    
    /** The sql. */
    private String sql;
    
    /** The cols. */
    private List<Column> cols;
    
    /** The binds. */
    private List<Bind> binds;
    
    /** The opt binds. */
    private List<Bind> optBinds;
	
	/** The skip cols. */
	private List<String> skipCols;
    
    /** The constrained. */
    private boolean constrained;
    
    /** The reqs. */
    private Set<String> reqs;
    
    /** The min. */
    private String min;
    
    /** The max. */
    private String max;
    
    /** The req features. */
    private String[] reqFeatures;
    
    /** The checksum. */
    private String checksum;
    
    /** The can be constrained. */
    private boolean canBeConstrained = true;
    
    /**
     * Gets the pk.
     *
     * @return the pk
     */
    public Long getPk() {
		return pk;
	}
	
	/**
	 * Sets the pk.
	 *
	 * @param pk the new pk
	 */
	public void setPk(Long pk) {
		this.pk = pk;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Gets the sql.
	 *
	 * @return the sql
	 */
	public String getSql() {
		return sql;
	}
	
	/**
	 * Sets the sql.
	 *
	 * @param sql the new sql
	 */
	public void setSql(String sql) {
		this.sql = sql;
	}
	
	/**
	 * Gets the cols.
	 *
	 * @return the cols
	 */
	public List<Column> getCols() {
		return cols;
	}
	
	/**
	 * Sets the cols.
	 *
	 * @param cols the new cols
	 */
	public void setCols(List<Column> cols) {
		this.cols = cols;
	}
	
	/**
	 * Gets the binds.
	 *
	 * @return the binds
	 */
	public List<Bind> getBinds() {
		return binds;
	}
	
	/**
	 * Sets the binds.
	 *
	 * @param binds the new binds
	 */
	public void setBinds(List<Bind> binds) {
		this.binds = binds;
	}
	
	/**
	 * Gets the opt binds.
	 *
	 * @return the opt binds
	 */
	public List<Bind> getOptBinds() {
		return optBinds;
	}
	
	/**
	 * Sets the opt binds.
	 *
	 * @param optBinds the new opt binds
	 */
	public void setOptBinds(List<Bind> optBinds) {
		this.optBinds = optBinds;
	}
	
	/**
	 * Gets the skip cols.
	 *
	 * @return the skip cols
	 */
	public List<String> getSkipCols() {
		return skipCols;
	}
	
	/**
	 * Sets the skip cols.
	 *
	 * @param skipCols the new skip cols
	 */
	public void setSkipCols(List<String> skipCols) {
		this.skipCols = skipCols;
	}
	
	/**
	 * Checks if is constrained.
	 *
	 * @return true, if is constrained
	 */
	public boolean isConstrained() {
		return constrained;
	}
	
	/**
	 * Sets the constrained.
	 *
	 * @param constrained the new constrained
	 */
	public void setConstrained(boolean constrained) {
		this.constrained = constrained;
	}
	
	/**
	 * Gets the reqs.
	 *
	 * @return the reqs
	 */
	public Set<String> getReqs() {
		return reqs;
	}
	
	/**
	 * Sets the reqs.
	 *
	 * @param reqs the new reqs
	 */
	public void setReqs(Set<String> reqs) {
		this.reqs = reqs;
	}
	
	/**
	 * Sets the checksum.
	 *
	 * @param checksum the new checksum
	 */
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	
	/**
	 * Gets the checksum.
	 *
	 * @return the checksum
	 */
	public String getChecksum() {
		return checksum;
	}
	
	/**
	 * Sets the min.
	 *
	 * @param min the new min
	 */
	public void setMin(String min) {
		this.min = min;
	}
	
	/**
	 * Gets the min.
	 *
	 * @return the min
	 */
	public String getMin() {
		return min;
	}
	
	/**
	 * Sets the max.
	 *
	 * @param max the new max
	 */
	public void setMax(String max) {
		this.max = max;
	}
	
	/**
	 * Gets the max.
	 *
	 * @return the max
	 */
	public String getMax() {
		return max;
	}
	
	/**
	 * Gets the required features.
	 *
	 * @return the required features
	 */
	public String[] getRequiredFeatures() {
	    return reqFeatures != null ? reqFeatures : new String[0];
	}
	
	/**
	 * Sets the required features.
	 *
	 * @param features the new required features
	 */
	public void setRequiredFeatures(String[] features) {
	    reqFeatures = features == null || features.length == 0 ? null : features;
	}
	
	/**
	 * Can be constrained.
	 *
	 * @return true, if successful
	 */
	public boolean canBeConstrained() {
		return canBeConstrained;
	}
	
	/**
	 * Sets the can be constrained.
	 *
	 * @param constrainable the new can be constrained
	 */
	public void setCanBeConstrained(boolean constrainable){
	  canBeConstrained = constrainable;
	}
}
