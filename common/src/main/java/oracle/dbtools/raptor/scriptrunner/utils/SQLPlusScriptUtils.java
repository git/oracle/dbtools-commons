/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.utils;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.db.ConnectionResolver;
import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.DefaultConnectionResolver;
import oracle.dbtools.raptor.newscriptrunner.ScriptExecutor;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * This utility class will allow you to run a script or SQL statement without
 * needing to know how to setup the output streams and input streams, but rather
 * pass in well known objects like a Connection and a script or SQL string.<p>
 * Right now, this one works with a connection resolver, but we are defaulting
 * to the Default one which does nothing. Need to resolve this before unleasing
 * on the public :)
 * 
 * @author klrice
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ScriptUtils.java"
 *         >Barry McGillin</a>
 * 
 */
public class SQLPlusScriptUtils {

	public static String runAsScript(String in, String connName) {
		return runAsScript(new StringReader(in), connName, false);
	}

	public static void runAsScript(FileInputStream in, String connName, ScriptRunnerContext ctx) {
		runAsScript(new InputStreamReader(in), connName, false, ctx);
	}

	public static String runAsScript(InputStream in, String connName) {
		return runAsScript(new InputStreamReader(in), connName, false);
	}

	public static String runAsScript(Reader rdr, String connName, Boolean useSystemOut) {
		return runAsScript(rdr, connName, useSystemOut, null);
	}

	public static String runAsScript(Reader rdr, String connName, Boolean useSystemOut, ScriptRunnerContext inCtx) {
		if (connName != null) {
			try {
				Connection conn = null;
				try {
					conn = resolveConnectionName(connName);
				} catch (Exception ex) {
					if (ex instanceof SQLException) {
						throw (SQLException) ex;
					}
				}
				if (conn == null) {
					return null;
				}
				ScriptExecutor runner = new ScriptExecutor(rdr, conn);
				ScriptRunnerContext src = null;
				if (inCtx != null) {
					src = inCtx;
				} else {
					src = new ScriptRunnerContext();
				}
				src.setCurrentConnection(conn);
				src.setBaseConnection(conn);
				src.setTopLevel(true);
				runner.setScriptRunnerContext(src);
				if (!useSystemOut) {
					final PipedInputStream runnerIn = new PipedInputStream();
					BufferedOutputStream out = new BufferedOutputStream(new PipedOutputStream(runnerIn));

					runner.setOut(out);
					runner.run();

				} else {
					runner.run();
				}
			} catch (SQLException e) {
				Logger.getLogger(SQLPlusScriptUtils.class.getClass().getName()).log(Level.WARNING,
				        e.getStackTrace()[0].toString(), e);
			} catch (IOException e) {
				Logger.getLogger(SQLPlusScriptUtils.class.getClass().getName()).log(Level.WARNING,
				        e.getStackTrace()[0].toString(), e);
			} finally {
				// Ide.getWaitCursor().hide();
			}
		}
		return connName;
	}

	// RUN SCRIPT AND LOG RESULTS TO OUTP
	public static String runSqlReturnResult(String sql, Connection conn) {
		try {
			String encoding = ScriptRunnerContext.getOutputEncoding();
			StringReader rdr = new StringReader(sql);
			ScriptExecutor runner = new ScriptExecutor(rdr, conn);
			ScriptRunnerContext ctx = new ScriptRunnerContext();
			ctx.putProperty(ScriptRunnerContext.SYSTEM_OUT, true);
			runner.setScriptRunnerContext(ctx);
			ByteArrayOutputStream BaS = new ByteArrayOutputStream();
			BufferedOutputStream BoS = new BufferedOutputStream(BaS);
			runner.setOut(BoS);
			runner.run();
			BoS.flush();
			return BaS.toString(encoding);
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	private static Connection resolveConnectionName(String connName) throws Exception {
		return ConnectionResolver.getConnection(connName);
	}

	public static void main(String[] args) throws Exception {
		// Test for apexins.sql
		//
		// Splice out user/pass@tns
		//
		if (args.length < 2) {
			System.err.println(""); // (authorized) //$NON-NLS-1$
			System.err.println(""); // (authorized) //$NON-NLS-1$
			System.err
			        .println("Usage: java oracle.dbtools.raptor.scriptrunner.utils.ScriptUtils username/password@tns @klr.sql define1 define2 define3 ..."); // (authorized) //$NON-NLS-1$
			System.err.println(""); // (authorized) //$NON-NLS-1$
			System.err.println(""); // (authorized) //$NON-NLS-1$
			System.exit(1);
		}
		String username = args[0].substring(0, args[0].indexOf("/")); //$NON-NLS-1$
		String password = args[0].substring(args[0].indexOf("/") + 1, args[0].indexOf("@")); //$NON-NLS-1$ //$NON-NLS-2$
		String tns = args[0].substring(args[0].indexOf("@") + 1); //$NON-NLS-1$
		String url = "jdbc:oracle:thin:@" + tns; //$NON-NLS-1$

		//
		// get the filename from arg[1]
		//
		String fileName = null;
		if (args[1].indexOf("@") == 0) //$NON-NLS-1$
			fileName = args[1].substring(1);
		else
			fileName = args[1];

		//
		//
		// if the passed in name doesn't exists tack on a .sql
		//
		File f = new File(fileName);
		if (!f.exists() && fileName.indexOf(".") == -1) { //$NON-NLS-1$
			fileName = fileName + ".sql"; //$NON-NLS-1$
		}

		// no .sql check ~/.sql
		if (!f.exists() && fileName.indexOf(".") == -1) { //$NON-NLS-1$
			fileName = fileName + ".sql"; //$NON-NLS-1$
		}
		ScriptRunnerContext ctx = new ScriptRunnerContext();
		// set the defined from the command line
		for (int i = 2; i < args.length; i++) {
			ctx.getMap().put("" + (i - 1), args[i]); //$NON-NLS-1$
		}

		// register driver
		DriverManager.registerDriver(new oracle.jdbc.OracleDriver());

		// get the connection
		Connection conn = DriverManager.getConnection(url, username, password);
		conn.setAutoCommit(false);

		// create the script runner
		// with the fileName
		ScriptExecutor runner = new ScriptExecutor(new FileInputStream(fileName), conn);
		runner.setScriptRunnerContext(ctx);
		runner.run();

	}

}
