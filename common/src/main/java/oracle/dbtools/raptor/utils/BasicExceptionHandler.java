/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.sql.SQLException;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=BasicExceptionHandler.java"
 *         >Barry McGillin</a>
 * 
 */
public class BasicExceptionHandler implements IExceptionHandler {

	private static Throwable hitException = null;
	private static IExceptionHandler handler;

	public static void setHandler(IExceptionHandler my_handler) {
		handler = my_handler;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.utils.IExceptionHandler#wasException()
	 */
	@Override
	public Throwable wasException() {
		return hitException;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.utils.IExceptionHandler#reset()
	 */
	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.utils.IExceptionHandler#handleException(java.lang
	 * .Throwable)
	 */
	@Override
	public void handleException(Throwable e) {
		System.out.println(e.getMessage());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.utils.IExceptionHandler#handleException(java.lang
	 * .String, java.lang.Throwable)
	 */
	@Override
	public void handleException(String connName, Throwable e) {
		if (e.getMessage()!=null)
   	      System.out.println(e.getMessage());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.utils.IExceptionHandler#handleException(java.lang
	 * .Exception, java.lang.String, int)
	 */
	@Override
	public void handleException(Exception e, String sql, int offset) {
		if (e.getMessage()!=null)
		System.out.println(e.getMessage());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.utils.IExceptionHandler#handleException(java.lang
	 * .Exception, java.lang.String, int, int, int)
	 */
	@Override
	public void handleException(Exception e, String sql, int offset, int line, int col) {
		if (e.getMessage()!=null)
		System.out.println(e.getMessage());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.utils.IExceptionHandler#handleExceptionWithAction
	 * (java.lang.Exception, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean handleExceptionWithAction(Exception e, String title, String okText, String cancelText) {
		if (e.getMessage()!=null)
		System.out.println(e.getMessage());
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.utils.IExceptionHandler#handleException(java.lang
	 * .Exception, java.lang.String, java.lang.String, int, int, int)
	 */
	@Override
	public void handleException(Exception e, String connName, String sql, int offset, int line, int col) {
		if (e.getMessage()!=null)
		System.out.println(e.getMessage());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.utils.IExceptionHandler#handleException(java.sql
	 * .SQLException, java.lang.String, java.lang.String, int)
	 */
	@Override
	public void handleException(SQLException e, String connName, String sql, int offset) {
		if (e.getMessage()!=null)
		System.out.println(e.getMessage());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.utils.IExceptionHandler#handleException(java.lang
	 * .Exception, java.lang.String)
	 */
	@Override
	public void handleException(Exception e, String connName) {
		if (e.getMessage()!=null)
		System.out.println(e.getMessage());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.utils.IExceptionHandler#handleException(java.lang
	 * .Exception, java.lang.String, boolean, boolean)
	 */
	@Override
	public void handleException(Exception e, String connName, boolean silent, boolean log) {
		if (!silent) {
			if (e.getMessage()!=null)
		System.out.println(e.getMessage());
		}
	}

	/**
	 * main
	 * 
	 * @param args
	 *            BasicExceptionHandler
	 */
	public static void main(String[] args) {
	}

}
