/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.nio.CharBuffer;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.Format;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.db.ResultSetFormatter;
import oracle.dbtools.db.SQLPLUSCmdFormatter;
import oracle.dbtools.raptor.datatypes.DataTypeException;
import oracle.dbtools.raptor.datatypes.DataValue;
import oracle.dbtools.raptor.datatypes.StringType;
import oracle.dbtools.raptor.datatypes.objects.LargeDatum;
import oracle.dbtools.raptor.datatypes.objects.PLSQLRecord;
import oracle.dbtools.raptor.extendedtype.ExtendedType;
import oracle.dbtools.raptor.nls.FormatType;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.dbtools.raptor.nls.OraTIMESTAMPTZFormat;
import oracle.dbtools.raptor.nls.OracleNLSProvider;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.BFILE;
import oracle.sql.BINARY_DOUBLE;
import oracle.sql.BINARY_FLOAT;
import oracle.sql.BLOB;
import oracle.sql.CLOB;
import oracle.sql.DATE;
import oracle.sql.Datum;
import oracle.sql.INTERVALDS;
import oracle.sql.INTERVALYM;
import oracle.sql.NUMBER;
import oracle.sql.OPAQUE;
import oracle.sql.RAW;
import oracle.sql.REF;
import oracle.sql.STRUCT;
import oracle.sql.TIMESTAMP;
import oracle.sql.TIMESTAMPLTZ;
import oracle.sql.TIMESTAMPTZ;
import oracle.xdb.XMLType;

/**
 * Class the managed Data Type conversions and metadata
 */
public class DataTypesUtil {

	/**
	 * DataType Kind
	 */
	public enum DataTypeKind {
		UNKNOWN, CHARACTER, NUMERIC, TEMPORAL, INTERVAL;
	}

	public static final Object EMPTY = new String(new char[0]);

	public static final int DISPLAY_NONE = 0x0;
	public static final int DISPLAY_STRUCT_VALUE = 0x1;
	public static final int DISPLAY_XMLTYPE_VALUE = 0x2;
	public static final int DISPLAY_ALL = DISPLAY_STRUCT_VALUE | DISPLAY_XMLTYPE_VALUE;

	public static final int BUFFER_SIZE = 2400;

	private static final Map<String, TimeZone> timeZoneMap = new HashMap<String, TimeZone>() {
		{
			for (String id : TimeZone.getAvailableIDs()) {
				TimeZone tz = TimeZone.getTimeZone(id);

				put(id.toUpperCase(), tz);
			}
		}
	};

	public static boolean areEqual(Object obj1, Object obj2) {
		return ((obj1 == obj2) || (obj1 != null && obj2 != null && obj1.equals(obj2)));
	}

	public static boolean isEqual(Object obj1, Object obj2, Connection conn, int columType) {
		if (isExtendedType(obj1, columType)) {
			return ModelUtil.areEqual(obj1, obj2);
		}

		if (obj1 != null && obj2 != null) {
			String s = stringValue(obj1, conn, Integer.MAX_VALUE);
			if (s != null) {
				return s.equals(obj2);
			}
		}
		return false;
	}

	public static String stringValueChecked(Object obj, Connection conn) throws SQLException, IOException {
		return stringValueChecked(obj, conn, 4000);
	}

	public static String stringValueChecked(Object obj, Connection conn, int maxLen) throws SQLException, IOException {
		return stringValue(obj, conn, maxLen, false);
	}

	public static String stringValue(Object obj, Connection conn) {
		return stringValue(obj, conn, 4000);
	}

	public static String stringValue(Object obj, Connection conn, int maxLen) {
		try {
			return stringValue(obj, conn, maxLen, true);
		} catch (SQLException e) {
			Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		} catch (IOException e) {
			Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}

		return null;
	}

	/**
	 * Get the string value with the specified format type. This has been
	 * implemented to support getting date, timestamp, timestamptz or
	 * timestampltz datatype in a generic format so that the results can be sent
	 * to a target using a different nls environment. Use GENERIC format for
	 * this.
	 *
	 * @param obj
	 * @param conn
	 * @param formatType
	 * @return
	 */
	public static String stringValue(Object obj, Connection conn, FormatType formatType) {
		try {
			return stringValue(obj, conn, 4000, true, DISPLAY_ALL, formatType);
		} catch (SQLException e) {
			Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		} catch (IOException e) {
			Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}

		return null;
	}

	public static String stringValue(Object obj, Connection conn, int maxLen, boolean logOnly)
			throws SQLException, IOException {
		return stringValue(obj, conn, maxLen, logOnly, DISPLAY_ALL);
	}

	@Deprecated
	public static String stringValue(Object obj, Connection conn, int maxLen, boolean logOnly, boolean displayValue)
			throws SQLException, IOException {
		return stringValue(obj, conn, maxLen, logOnly, displayValue, null);
	}

	@Deprecated
	public static String stringValue(Object obj, Connection conn, int maxLen, boolean logOnly, boolean displayValue,
			FormatType formatType) throws SQLException, IOException {
		return stringValue(obj, conn, maxLen, logOnly, (displayValue) ? DISPLAY_ALL : DISPLAY_NONE, formatType);
	}

	/*
	 * stringValue - calls stringValue with null FormatType allowing for default
	 * behavior when getting value for timestamp, timestamptz and timestampltz
	 * types
	 */
	public static String stringValue(Object obj, Connection conn, int maxLen, boolean logOnly, int displayValue)
			throws SQLException, IOException {
		return stringValue(obj, conn, maxLen, logOnly, displayValue, null);
	}

	// Clob needs special attention: clob.free() (bug 24310026)
	// After first invocation of stringValue() clob is freed
	// It causes exception during subsequent invocations of ((Clob)
	// obj).getSubString(1, width)
	private static Map<Clob, String> cachedClobs = new WeakHashMap<Clob, String>();

	// BUG 13680849 - RC1: NOT DISPLAY THE DATA OF MDSYS.SDO_GEOMETRY
	// Add preference for display of struct data[false] vs type.name in grid
	// All other uses will show data
	public static String stringValue(Object obj, Connection conn, int maxLen, boolean logOnly, int displayValue,
			FormatType formatType) throws SQLException, IOException {
		String nullDisplay = NLSUtils.yieldNullDisplay(conn, obj);

		if (obj == null || nullDisplay != null) {
			return nullDisplay;
		}
		String str = null;
		// Object origValue = null;
		if (obj instanceof StringValuableObject) {
			return ((StringValuableObject) obj).stringValue(conn, maxLen, logOnly, displayValue, formatType);
		} else if (obj instanceof StringValuable) {
			return ((StringValuable) obj).stringValue(conn, maxLen, logOnly,
					(displayValue & DISPLAY_STRUCT_VALUE) != DISPLAY_NONE, formatType);
		} else if (obj instanceof Clob) {
			String candidate = cachedClobs.get(obj);
			boolean isTemporary = false;
			if (candidate != null)
				str = candidate;
			else
				try {
					// CLOB may be wrapped
					try {
						Method m = obj.getClass().getMethod("isTemporary");
						isTemporary = (Boolean) (m.invoke(obj));
					} catch (Throwable t) {
						isTemporary = false;
					}

					int width = 0;
					// Bug 11731138: DOUBLE CLICK IN CLOB'S CELL EDITOR CAUSES
					// OUT OF MEMORY EXCEPTION
					// if maxLen == Integer.MAX_VALUE ((Clob)
					// obj).getSubString(1, maxLen) cause outOfMemoryError
					// this fix will maintain the fix done in rev 48794 to
					// prevent the cell renderer causing clob.getLength since
					// maxLen from renderer will
					// never be Integer.MAX_VALUE. But in case of cellEditor
					// clob.getLength will be done.
					// calls from cellRenderer greatly outnumber calls from
					// cellEditor.
					if (isTemporary || Integer.MAX_VALUE == maxLen)
						width = (int) ((Clob) obj).length();
					else
						width = maxLen;
					str = ((Clob) obj).getSubString(1, width); // $NON-NLS-1$
					if (str.length() == width && Integer.MAX_VALUE != maxLen) {
						// str += "..."; //$NON-NLS-1$
					}
					if (isTemporary) {
						cachedClobs.put((Clob) obj, str);
					}
				} catch (SQLException e) {
					if (logOnly)
						Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING,
								e.getStackTrace()[0].toString(), e);
					else
						throw e;
				} finally {
					// thirdparty drivers do not support free. So catch that
					// exception and do nothing
					try {
						if (isTemporary) {
							((Clob) obj).free();
						}
					} catch (Throwable t) { // jtds throws an abstractmethod
											// error

					}

					if (str != null && maxLen > -1 && maxLen < str.length()) {
						str = str.substring(0, maxLen);
					}
				}
		} else if (obj instanceof Blob) {
			// origValue = obj;
			str = "(BLOB)"; //$NON-NLS-1$
		} else if (obj instanceof RAW) {
			// origValue = obj;
			str = (String) NLSUtils.getValue(conn, obj);

		} else if (obj instanceof BFILE) {
			String dir = ((oracle.jdbc.OracleBfile) obj).getDirAlias();// origValue
																		// =
																		// obj;
			String file = ((oracle.jdbc.OracleBfile) obj).getName();// origValue
																	// = obj;
			str = MessageFormat.format("bfilename(''{0}'',''{1}'')", new Object[] { dir, file }); //$NON-NLS-1$
		} else if ((obj instanceof TIMESTAMPTZ) || (obj instanceof TIMESTAMPLTZ) || (obj instanceof TIMESTAMP)
				|| (obj instanceof INTERVALYM) || (obj instanceof INTERVALDS)) {
			str = (String) NLSUtils.getValue(conn, obj, formatType);
		} else if (obj instanceof Timestamp) {
			str = (String) NLSUtils.getValue(conn,
					/*
					 * new TIMESTAMP((Timestamp)obj)
					 */obj);
		} else if (obj instanceof DATE) {
			try {
				str = (String) NLSUtils.getValue(conn, obj, formatType);
			} catch (NullPointerException e) { // Please don't remove this
				// exception handler
				// until you make NLSUtils.getValue super duper robust
				DATE val = (DATE) obj;
				str = DATE.toString(val.toBytes());
			}
		} else if (obj instanceof Date) {
			str = NLSUtils.format(conn, obj);
		} else if (obj instanceof NUMBER || obj instanceof BINARY_FLOAT || obj instanceof BINARY_DOUBLE) {// Fix
																											// Bug:5087402
																											// -
																											// GLOBAL:
			// NLS SETTINGS ARE NOT USED
			// FOR
			// BINARY_FLOAT/BINARY_DOUBLE
			try {
				str = (String) NLSUtils.getValue(conn, obj);
			} catch (NullPointerException e) { // Please don't remove this
				// exception handler
				// until you make NLSUtils.getValue super duper robust
				NUMBER val = (NUMBER) obj;
				try {
					str = NUMBER.toBigDecimal(val.toBytes()).toString();
				} catch (SQLException ee) {
					if (logOnly) {
						Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING,
								e.getStackTrace()[0].toString(), e);
						str = "??"; //$NON-NLS-1$
					} else {
						throw ee;
					}
				}
			}
		} else if (obj instanceof BigDecimal) {
			str = (String) NLSUtils.getValue(conn, obj);
		} else if (obj instanceof XMLType) {
			if ((displayValue & DISPLAY_XMLTYPE_VALUE) == DISPLAY_NONE) { // RJW
				str = "(XMLTYPE)"; //$NON-NLS-1$
			} else {
				XMLType xmltype = (XMLType) obj;

				CLOB clobVal = null;

				try {
					clobVal = xmltype.getClobVal();

					Reader clobRdr = null;

					try {
						clobRdr = clobVal.getCharacterStream();

						str = DataTypesUtil.stringValue(clobRdr, conn, maxLen, logOnly, displayValue);
					} finally {
						try {
							if (clobRdr != null) {
								clobRdr.close();
							}
						} catch (Exception e) {
							; // ignore
						}
					}
				} catch (Exception e) {
					Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
					str = "(XMLTYPE)"; //$NON-NLS-1$
				} finally {
					if (xmltype != null) {
						try {
							xmltype.close();
						} catch (Exception e) {
							Exception ex = e; // ignore
						}
					}
					if (clobVal != null) {
						try {
							if (clobVal.isOpen()) {
								clobVal.close();
							}
						} catch (Exception e) {
							Exception ex = e; // ignore
						}
					}
					if (clobVal != null) {
						try {
							if (clobVal.isTemporary()) {
								clobVal.freeTemporary();
							}
						} catch (Exception e) {
							Exception ex = e; // ignore
						}
					}
				}
			}
		} else if (obj instanceof OPAQUE) {
			if ((displayValue & DISPLAY_XMLTYPE_VALUE) == DISPLAY_NONE) { // RJW
				str = "(OPAQUE)"; //$NON-NLS-1$
			} else {
				try {
					OPAQUE opaque = (OPAQUE) obj;

					str = opaque.getSQLTypeName().trim();

					if (str.equalsIgnoreCase("SYS.XMLTYPE")) { //$NON-NLS-1$
						try {
							XMLType xmltype = XMLType.createXML(opaque);

							str = DataTypesUtil.stringValue(xmltype, conn, maxLen, logOnly, displayValue);
						} catch (Exception e) {
							Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING,
									e.getStackTrace()[0].toString(), e);
							str = "(XMLTYPE)"; //$NON-NLS-1$
						}
					} else {
						str = "(OPAQUE)"; //$NON-NLS-1$
					}
				} catch (SQLException e) {
					if (logOnly)
						Logger.getLogger(DataTypesUtil.class.getName()).log(Level.INFO, e.getStackTrace()[0].toString(),
								e);
					else
						throw e;
				}
			}
		} else if (obj instanceof STRUCT) {
			if ((displayValue & DISPLAY_STRUCT_VALUE) != DISPLAY_NONE) {
				str = getStruct(obj, maxLen);
			} else {
				str = "[" + ((STRUCT) obj).getSQLTypeName().trim() + "]";
			}
		} else if (obj instanceof ARRAY) {
			try {
				String sqlt = ((ARRAY) obj).getSQLTypeName();

				str = sqlt + "("; // ((ARRAY) //$NON-NLS-1$
									// obj).getBaseTypeName()+"(";
				Datum[] dat = ((ARRAY) obj).getOracleArray();
				boolean first = true;
				// What a mess, fix one case... break another.
				// Bug 14825651 - QATEST REGRESSION:ARRAY DATA TYPE ADD
				// Bug 9678915 - EXPORT DATA FOR NESTED TABLE INTO INSERT, THE
				// FORMAT IS NOT PROPER
				// Bug 10007179 - REDUNDANT '' WHEN UNLOADING NESTED TABLE
				// Bug 12350442 - EXPORT INCORRECT SQL SCRIPT FOR ARRAY TYPE
				// DATA
				//

				String quote = "";
				for (Datum el : dat) {
					if (el instanceof oracle.sql.CHAR) {
						quote = "'";
					} else {
						quote = "";
					}
					str += ((first) ? "" : ", ") + quote + stringValue(el, conn, maxLen, logOnly, displayValue) + quote; //$NON-NLS-1$ //$NON-NLS-2$
					first = false;
				}
				str += ")"; //$NON-NLS-1$
			} catch (SQLException e) {
				if (logOnly)
					Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
				else
					throw e;
			}
		} else if (obj instanceof ResultSet) {
			ResultSetFormatter formatter = new ResultSetFormatter();
			if (conn instanceof OracleConnection) {
				formatter = new SQLPLUSCmdFormatter();
			}
			StringBuffer buf = new StringBuffer();
			try {
				int rowcount = formatter.rset2sqlplus((ResultSet) obj, conn, buf);
				if (rowcount > 8) { // default sqlplus
					buf.append(MessageFormat.format("\n{0} rows selected.\n", rowcount));
				}
			} catch (IOException e) {
				if (logOnly)
					Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
				else
					throw e;
			} catch (SQLException e) {
				if (logOnly)
					Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
				else
					throw e;
			}
			str = buf.toString();
		} else if (obj instanceof PLSQLRecord) {
			str = "<"; //$NON-NLS-1$
			int i = 0;
			for (Object fld : (List<?>) obj) {
				str += ((i++ > 0) ? "," : "") + stringValue(fld, conn, maxLen, logOnly, displayValue); //$NON-NLS-1$ //$NON-NLS-2$
																										// //$NON-NLS-3$

			}
			str += ">"; //$NON-NLS-1$
		} else if (obj instanceof DataValue) {
			try {
				str = ((DataValue) obj).getStringValue(maxLen).toString();
			} catch (DataTypeException e) {
				if (logOnly)
					Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
				else
					throw e;
			}
		} else if (obj instanceof LargeDatum) {
			try {
				str = ((LargeDatum) obj).getStringValue(StringType.DEFAULT, maxLen).toString();
			} catch (DataTypeException e) {
				if (logOnly)
					Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
				else
					throw e;
			}
		} else if (obj instanceof List<?>) {
			str = "{"; //$NON-NLS-1$
			List<?> l = (List<?>) obj;
			int i = 0;
			List<?> zeroThRow = (List<?>) l.get(0);
			for (Object row : l) {
				if (i > 0) {
					str += "<"; //$NON-NLS-1$
					int j = 0;
					for (Object fld : (List<?>) row) {
						str += zeroThRow.get(j) + "=" + stringValue(fld, conn, maxLen, logOnly, displayValue) + ","; //$NON-NLS-1$ //$NON-NLS-2$
						j++;
					}
					str = str.substring(0, str.length() - 1);
					str += ">,"; //$NON-NLS-1$
				}
				i++;
			}
			str += "}"; //$NON-NLS-1$
		} else if (obj instanceof REF) {
			try {
				return stringValue(((REF) obj).getValue(), conn, maxLen, logOnly, displayValue);
			} catch (SQLException e) {
				if (logOnly)
					Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
				else
					throw e;
			}
		} else if (obj instanceof Datum) {
			try {
				str = ((Datum) obj).stringValue();
				str = (str.equals("CLOB()") || str.equals("BLOB()") || str.equals("NCLOB()")) ? str.replace("()", "")
						: str;
			} catch (SQLException e) {
				if (logOnly)
					Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
				else
					throw e;
			}
		} else if (obj instanceof byte[]) {
			str = "(BINARY DATA)"; // $NON-NLS-2$
		} else if (obj instanceof Reader) {
			StringWriter writer = new StringWriter();
			copyData((Reader) obj, writer, maxLen);
			str = writer.toString();
		} else {
			str = obj.toString();
		}
		return str;
	}

	private static String getStruct(Object obj, int maxLen) {
		String str = ""; //$NON-NLS-1$
		try {
			Object cols[] = ((oracle.sql.STRUCT) obj).getAttributes();
			String type = ((oracle.sql.STRUCT) obj).getSQLTypeName();
			StringBuilder column = new StringBuilder(type + "("); //$NON-NLS-1$
			int lastChar = -1;
			// LRG/SRG Regression Fix: The comma separated Column list should
			// also
			// include a "single" space.
			for (int i = 0; i < cols.length; i++) {
				if (column.length() > maxLen)
					break;
				Object obj2 = cols[i];
				if (obj2 instanceof oracle.sql.STRUCT) {
					column.append(getStruct(obj2, maxLen) + ", "); //$NON-NLS-1$
				} else if (obj2 instanceof oracle.sql.ARRAY) {
					String aType = ((oracle.sql.ARRAY) obj2).getSQLTypeName();
					Object values[] = ((Object[]) ((oracle.sql.ARRAY) obj2).getArray());
					int aLength = ((oracle.sql.ARRAY) obj2).length();
					column.append(aType + "("); //$NON-NLS-1$
					for (int x = 0; x < aLength; x++) {
						if (values[x] instanceof oracle.sql.STRUCT) {
							column.append(getStruct(values[x], maxLen) + ", "); //$NON-NLS-1$
						} else if (values[x] instanceof oracle.sql.ARRAY) {
							column.append(getArray(values[x], maxLen) + ", "); //$NON-NLS-1$
						} else {
							try {// original code assumed BigDecimal
								BigDecimal bd = (BigDecimal) (values[x]);
								column.append(bd + ", "); //$NON-NLS-1$
							} catch (ClassCastException cce) {
								column.append(values[x] + ", "); //$NON-NLS-1$
							}
						}
					}
					lastChar = column.lastIndexOf(","); //$NON-NLS-1$
					if (lastChar > -1)
						column.deleteCharAt(lastChar);
					// Removing Unnecessary space
					lastChar = column.lastIndexOf(" "); //$NON-NLS-1$
					if (lastChar > -1)
						column.deleteCharAt(lastChar);
					column = column.append("), "); //$NON-NLS-1$
				} else if (obj2 instanceof BigDecimal) {
					column.append(cols[i] + ", "); //$NON-NLS-1$
				} else {
					if (cols[i] == null)
						column.append("NULL" + ", ");//$NON-NLS-1$
					else
						column.append("'" + cols[i] + "', "); //$NON-NLS-1$
				}
			}
			// column = column+cols[i]+",";
			lastChar = column.lastIndexOf(","); //$NON-NLS-1$
			if (lastChar > -1)
				column.deleteCharAt(lastChar);
			// Removing Unnecessary space
			lastChar = column.lastIndexOf(" "); //$NON-NLS-1$
			if (lastChar > -1)
				column.deleteCharAt(lastChar);
			column.append(")"); //$NON-NLS-1$
			str = column.toString();
		} catch (SQLException e) {
			Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}
		return str;
	}

	private static String getArray(Object obj, int maxLen) throws SQLException {
		String str = "";
		String sqlt = ((ARRAY) obj).getSQLTypeName();
		str = sqlt + "(";
		Datum[] dat = ((ARRAY) obj).getOracleArray();
		boolean first = true;
		String quote = "";
		for (Datum el : dat) {
			String val = "";
			if (el instanceof oracle.sql.STRUCT) {
				str += ((first) ? "" : ", ") + quote + getStruct(el, maxLen);
			}
			first = false;
		}
		str += ")"; //$NON-NLS-1$
		return str;
	}

	public static boolean isExtendedType(Object obj, int colType) {
		if (obj instanceof ExtendedType || obj instanceof BFILE || obj instanceof Blob || colType == OracleTypes.BFILE
				|| colType == Types.BLOB || colType == OracleTypes.OPAQUE || colType == OracleTypes.SQLXML
				|| colType == OracleTypes.ARRAY || colType == OracleTypes.CURSOR) {
			return true;
		}
		return false;
	}

	public static boolean isTemporalType(Object obj, int colType) {
		if (colType == Types.DATE || colType == OracleTypes.TIMESTAMP || colType == OracleTypes.TIMESTAMPLTZ
				|| colType == OracleTypes.TIMESTAMPTZ) {
			return true;
		}
		return false;
	}

	public static boolean isXMLType(Object obj) {
		try {
			return obj instanceof XMLType
					|| (obj instanceof OPAQUE && getXMLSQLTypeName().equals(((OPAQUE) obj).getSQLTypeName()));
		} catch (SQLException e) {
			Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}
		return false;
	}

	public static String getXMLSQLTypeName() {
		return "SYS.XMLTYPE"; //$NON-NLS-1$
	}

	public static String getEmptyExtendedType(Object obj, int colType) {
		if (obj instanceof Blob || colType == Types.BLOB) {
			return "empty_blob()"; //$NON-NLS-1$
		} else if (obj instanceof BFILE || colType == OracleTypes.BFILE) {
			return "bfilename(' ',' ')"; //$NON-NLS-1$
		}
		return "NULL"; //$NON-NLS-1$
	}

	public static String getJdbcTypeName(int jdbcType) {
		return dataTypesMap.get(new Integer(jdbcType));
	}

	public static Integer getJdbcTypeCode(String name) {
		return dataTypesMap2.get(name);
	}

	static Map<Integer, String> dataTypesMap = new HashMap<Integer, String>() {
		private static final long serialVersionUID = 1L;
		{
			Field[] fields = OracleTypes.class.getFields();
			for (int i = 0; i < fields.length; i++) {
				try {
					String name = fields[i].getName();
					Integer value = (Integer) fields[i].get(null);
					put(value, name);
				} catch (IllegalAccessException e) {
					;
				} catch (ClassCastException e) {
					// Not all fields are integers, but all of the ones we
					// care about are, so just ignore.
					;
				}
			}
		}
	};

	static Map<String, Integer> dataTypesMap2 = new HashMap<String, Integer>() {
		private static final long serialVersionUID = 1L;

		{
			Field[] fields = OracleTypes.class.getFields();
			for (int i = 0; i < fields.length; i++) {
				try {
					String name = fields[i].getName();
					Integer value = (Integer) fields[i].get(null);
					put(name, value);
				} catch (IllegalAccessException e) {
					;
				} catch (ClassCastException e) {
					// Not all fields are integers, but all of the ones we
					// care about are, so just ignore.
					;
				}
			}
		}
	};

	/**
	 * test main.
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
	}

	public static boolean canDuplicate(Object obj, int colType) {
		// Bug 13984958 - DUPLICATE ROW FOR BILE GOT ERROR MESSAGE
		// Added OracleTypes.BFILE for not duplicate type.
		if (obj instanceof STRUCT || colType == OracleTypes.STRUCT || obj instanceof RAW
				|| colType == Types.LONGVARBINARY || colType == Types.BINARY || colType == Types.VARBINARY
				|| obj instanceof Blob || colType == Types.BLOB || colType == Types.ROWID || obj instanceof ARRAY
				|| colType == Types.ARRAY || colType == OracleTypes.BFILE) {
			return false;
		}
		return true;
	}

	public static CLOB getCLOB(String data, Connection conn) throws SQLException {
		CLOB tempClob = null;
		try {
			// If the temporary CLOB has not yet been created, create new
			tempClob = getTemporaryCLOB(conn);

			// Open the temporary CLOB in readwrite mode to enable writing
			tempClob.open(CLOB.MODE_READWRITE);
			// Get the output stream to write
			Writer tempClobWriter = tempClob.setCharacterStream(0L);
			// Write the data into the temporary CLOB
			tempClobWriter.write(data);

			// Flush and close the stream
			tempClobWriter.flush();
			tempClobWriter.close();

			// Close the temporary CLOB
			tempClob.close();
		} catch (SQLException e) {
			tempClob.freeTemporary();
			Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		} catch (Exception e) {
			tempClob.freeTemporary();
			Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		}
		return tempClob;
	}

	public static CLOB getTemporaryCLOB(Connection conn) throws SQLException {
		return CLOB.createTemporary(conn, true, CLOB.DURATION_SESSION);
	}

	public static BLOB getTemporaryBLOB(Connection conn) throws SQLException {
		return BLOB.createTemporary(conn, true, BLOB.DURATION_SESSION);
	}

	public static CLOB getCLOB(Reader reader, Connection conn) throws SQLException, IOException {
		boolean throwing = true;

		// create Temporary Clob
		CLOB tempClob = getTemporaryCLOB(conn);

		try {
			// Open the temporary CLOB in readwrite mode to enable writing
			tempClob.open(CLOB.MODE_READWRITE);

			try {
				// Get the output stream to write
				Writer tempClobWriter = tempClob.setCharacterStream(0L);

				// Write the data into the temporary CLOB
				try {
					char[] buf = new char[2048];

					// Read chunk
					int len = reader.read(buf);

					// Read and Write to the end
					while (len != -1) {
						// Write Buffer
						tempClobWriter.write(buf, 0, len);

						// Read next chunk
						len = reader.read(buf);
					}

					// Flush Writer
					tempClobWriter.flush();

					// Finishing so not throwing
					throwing = false;
				} finally {
					// Close Writer
					try {
						tempClobWriter.close();
					} catch (IOException e) {
						if (!throwing) {
							throwing = true;
							throw e;
						} else {
							Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING,
									e.getStackTrace()[0].toString(), e);
						}
					}
				}
			} finally {
				// Close the temporary CLOB
				try {
					tempClob.close();
				} catch (SQLException e) {
					if (!throwing) {
						throwing = true;
						throw e;
					} else {
						Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING,
								e.getStackTrace()[0].toString(), e);
					}
				}
			}
		} finally {
			if (throwing) {
				// Free temporary CLOB
				try {
					tempClob.freeTemporary();
				} catch (SQLException e) {
					Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
				}
			}
		}

		return tempClob;
	}

	private static byte[] hexToBytes(char[] hex) {
		int length = hex.length / 2;
		byte[] raw = new byte[length];
		for (int i = 0; i < length; i++) {
			int high = Character.digit(hex[i * 2], 16);
			int low = Character.digit(hex[i * 2 + 1], 16);
			int value = (high << 4) | low;
			if (value > 127)
				value -= 256;
			raw[i] = (byte) value;
		}
		return raw;
	}

	public static BLOB getBLOB(char[] hex, Connection conn) throws SQLException, IOException {
		boolean throwing = true;

		// create Temporary Clob
		BLOB tempBlob = getTemporaryBLOB(conn);

		try {
			// Open the temporary CLOB in readwrite mode to enable writing
			tempBlob.open(BLOB.MODE_READWRITE);
			// Get the output stream to write
			OutputStream tempBlobWriter = tempBlob.setBinaryStream(0L);
			// Write the data into the temporary CLOB

			byte[] rawBlob = hexToBytes(hex);
			tempBlobWriter.write(rawBlob, 0, rawBlob.length);

			// Flush Writer
			tempBlobWriter.flush();
			// Finishing so not throwing
			throwing = false;
		} finally {
			if (throwing) {
				// Free temporary CLOB
				try {
					tempBlob.freeTemporary();
				} catch (SQLException e) {
					Logger.getLogger(DataTypesUtil.class.getName()).log(Level.WARNING, e.getStackTrace()[0].toString(),
							e);
				}
			}
		}

		return tempBlob;
	}

	public static void copyData(Reader reader, Writer writer) throws IOException {
		// Read in 256 byte blocks
		char[] buf = new char[2048];

		// Read chunk
		int len = reader.read(buf);

		// Read and Write to the end
		while (len != -1) {
			// Write Buffer
			writer.write(buf, 0, len);

			// Read next chunk
			len = reader.read(buf);
		}
	}

	public static void copyData(Reader reader, Writer writer, int maxLen) throws IOException {
		if (maxLen < 0) {
			copyData(reader, writer);
		} else {
			int c;
			int written = 0;

			while ((maxLen == -1 || written < maxLen) && (c = reader.read()) != -1) {
				writer.write((char) c);
				written++;
			}
		}
	}

	public static Object getDuplicate(Object o, Connection conn, int colType) throws SQLException {
		if (canDuplicate(o, colType)) {
			if (o instanceof Clob || colType == Types.CLOB) {
				if (o == null || o.equals("")) { //$NON-NLS-1$
					return ""; //$NON-NLS-1$
				} else {
					if (o instanceof Clob)
						return ((Clob) o).getSubString(1, (int) ((Clob) o).length());
					else
						return o;
				}
			}
			if (o instanceof oracle.sql.Datum) {
				try {
					oracle.sql.Datum datum = (oracle.sql.Datum) o;
					byte[] bytes = datum.getBytes();
					Class<?> clazz = datum.getClass();
					Constructor<?> constructor = clazz.getConstructor(bytes.getClass());
					return constructor.newInstance(bytes);
				} catch (Exception e) {
					; // Ignore - Try something else
				}
			}

			if (o instanceof Cloneable) {
				try {
					Class<?> clazz = o.getClass();
					Method cloneMethod = clazz.getMethod("clone");
					return cloneMethod.invoke(o);
				} catch (Exception e) {
					; // Ignore - Try something else
				}
			}

			return DataTypesUtil.stringValue(o, conn);
		}
		return null;
	}

	public static String stripDataTypeStringConstraints(String value) {
		String ret = value;

		if (ret != null) {
			Pattern p = Pattern.compile("[(][^()]*[)]"); //$NON-NLS-1$
			Matcher m = p.matcher(ret);
			while (m.matches()) {
				ret = m.replaceAll(" "); //$NON-NLS-1$
				m = p.matcher(ret);
			}

			ret = reformatDataTypeString(ret);
		}

		return ret;
	}

	public static String reformatDataTypeString(String value) {
		String ret = value;

		if (ret != null) {
			Pattern ob = Pattern.compile("[\\s]*[(][\\s]*"); //$NON-NLS-1$
			Matcher obm = ob.matcher(ret);
			ret = obm.replaceAll("("); //$NON-NLS-1$

			Pattern co = Pattern.compile("[\\s]*[,][\\s]*"); //$NON-NLS-1$
			Matcher com = co.matcher(ret);
			ret = com.replaceAll(","); //$NON-NLS-1$

			Pattern cb = Pattern.compile("[\\s]*[)]"); //$NON-NLS-1$
			Matcher cbm = cb.matcher(ret);
			ret = cbm.replaceAll(") "); //$NON-NLS-1$

			Pattern mws = Pattern.compile("[\\s]+"); //$NON-NLS-1$
			Matcher mwsm = mws.matcher(ret);
			ret = mwsm.replaceAll(" "); //$NON-NLS-1$

			ret = ret.trim();
		}

		return ret;
	}

	public static boolean canAutoFilter(int columnSqlType) {
		switch (columnSqlType) {
		case Types.CLOB:
		case Types.NCLOB:
		case OracleTypes.OPAQUE:
		case OracleTypes.SQLXML:
		case OracleTypes.STRUCT:
		case OracleTypes.BFILE:
		case OracleTypes.BLOB:
		case OracleTypes.ARRAY:
		case OracleTypes.CURSOR:
		case OracleTypes.JAVA_OBJECT:
		case OracleTypes.JAVA_STRUCT:
		case OracleTypes.OTHER:
		case Types.LONGVARBINARY:
		case Types.LONGNVARCHAR:
		case Types.LONGVARCHAR:
		case Types.BINARY:
		case Types.VARBINARY:
			return false;
		default:
			return true;
		}
	}

	public static boolean isNull(Object value) {
		return (value == null || ((value instanceof CharSequence || value instanceof oracle.sql.CHAR)
				&& "".equals(value.toString())));
	}

	public static boolean isEmpty(Object value) {
		return (value == EMPTY);
	}

	public static boolean isDateOrTime(int columnSqlType) {
		switch (columnSqlType) {
		case Types.DATE:
		case Types.TIME:
		case Types.TIMESTAMP:
			return true;
		default:
			return false;
		}
	}

	/**
	 * Get Kind of DataType. Typically used in sorting.
	 *
	 * @param sqlType
	 * @return
	 */
	public static DataTypeKind getDataTypeKind(int sqlType) {
		switch (sqlType) {
		case Types.VARCHAR:
		case Types.LONGVARCHAR:
		case Types.CHAR:
		case Types.NVARCHAR:
		case Types.NCHAR:
		case Types.LONGNVARCHAR:
			return DataTypeKind.CHARACTER;
		case Types.INTEGER:
		case Types.FLOAT:
		case Types.REAL:
		case Types.DOUBLE:
		case Types.NUMERIC:
		case Types.DECIMAL:
			return DataTypeKind.NUMERIC;
		case Types.DATE:
		case Types.TIME:
		case Types.TIMESTAMP:
		case OracleTypes.TIMESTAMPTZ:
		case OracleTypes.TIMESTAMPLTZ:
			return DataTypeKind.TEMPORAL;
		case OracleTypes.INTERVALDS:
		case OracleTypes.INTERVALYM:
			return DataTypeKind.INTERVAL;
		default:
			return DataTypeKind.UNKNOWN;
		}
	}

	public static Calendar getCalendar(Connection conn, TIMESTAMPTZ tsTZ) throws SQLException {
		Calendar cal = null;

		if (tsTZ != null) {
			cal = Calendar.getInstance();

			// Set TimeZone
			OraTIMESTAMPTZFormat tzFormat;
			try {
				tzFormat = ((OracleNLSProvider) NLSProvider.getProvider(conn)).getOraTIMESTAMPTZFormat();
			} catch (ParseException pe) {
				tzFormat = null;
			}

			try {
				tzFormat.applyPattern("TZR");

				String tzID = tzFormat.format(tsTZ);

				TimeZone tz = timeZoneMap.get(tzID.toUpperCase());

				if (tz != null) {
					cal.setTimeZone(tz);
				}
			} catch (ParseException pe) {
				; // Valid Oracle syntax - should never happen
			}

			// Set Date
			java.util.Date date = tsTZ.dateValue(conn);

			if (date != null) {
				cal.setTime(date);
			}
		}

		return cal;
	}

	public static InputStream getXMLAsBytes(Reader reader)
			throws XMLStreamException, IOException, UnsupportedEncodingException {
		return getXMLAsBytes(reader, "UTF-8"); //$NON-NLS-1$
	}

	public static InputStream getXMLAsBytes(Reader reader, String defaultEncoding)
			throws XMLStreamException, IOException, UnsupportedEncodingException {

		// Create BufferedReader
		BufferedReader in = new BufferedReader(reader);

		// Mark Enough for header
		in.mark(200);

		String encoding = getEncodingNameFromPI(in);

		if (encoding == null) {
			encoding = defaultEncoding;
		}

		in.reset();

		if (encoding != null) {
			return new ReaderInputStream(in, encoding);
		} else {
			return new ReaderInputStream(in);
		}
	}

	public static boolean getXMLAsCharacters(InputStream is, Writer writer)
			throws XMLStreamException, IOException, UnsupportedEncodingException {
		return getXMLAsCharacters(is, writer, -1);
	}

	public static boolean getXMLAsCharacters(InputStream is, Writer writer, int maxLen)
			throws XMLStreamException, IOException, UnsupportedEncodingException {
		return getXMLAsCharacters(is, writer, maxLen, null);
	}

	public static boolean getXMLAsCharacters(InputStream is, Writer writer, String defaultEncoding)
			throws XMLStreamException, IOException, UnsupportedEncodingException {
		return getXMLAsCharacters(is, writer, -1, defaultEncoding);
	}

	public static boolean getXMLAsCharacters(InputStream is, Writer writer, int maxLen, String defaultEncoding)
			throws XMLStreamException, IOException, UnsupportedEncodingException {
		Reader isr = getXMLAsReader(is, defaultEncoding);

		if (isr != null) {
			// Copy from Reader to Writer
			copyData(isr, writer, maxLen);

			return true;
		}

		return false;
	}

	public static Reader getXMLAsReader(InputStream is, String defaultEncoding)
			throws XMLStreamException, IOException, UnsupportedEncodingException {
		final byte[] bytes = new byte[200];
		final int[] bomSize = { 0 };

		// Create Buffer Input Stream
		BufferedInputStream inBis = new BufferedInputStream(is);
		inBis.mark(200); // mark start so that we can get back to it

		// Read stream header
		int count = inBis.read(bytes, 0, 195);

		// Create new input stream to detect encoding and possible read PI i.e.
		// "<?xml"
		ByteArrayInputStream byteArrayIS = new ByteArrayInputStream(bytes, 0, count);
		BufferedInputStream bis = new BufferedInputStream(byteArrayIS);

		bis.mark(count);

		// Detect encoding
		String encoding = detectedEncodingName(bis, bomSize);

		// Reset header input stream
		bis.reset();

		// Get from PI heeder
		String PIEncoding = getEncodingNameFromPI(bis);

		if (PIEncoding != null) {
			encoding = PIEncoding;
		}

		// Use Default Encoding if required
		if (encoding == null && defaultEncoding != null) {
			encoding = defaultEncoding;
		}

		if (encoding != null) {
			inBis.reset();
			inBis.skip(bomSize[0]);

			return new InputStreamReader(inBis, encoding);
		}

		return null;
	}

	public static String getEncodingNameFromPI(Reader reader) throws XMLStreamException, IOException {
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		XMLStreamReader streamReader = inputFactory.createXMLStreamReader(new ReaderInputStream(reader, "UTF-8"));

		return streamReader.getEncoding();
	}

	public static String getEncodingNameFromPI(InputStream is) throws XMLStreamException, IOException {
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		XMLStreamReader streamReader = inputFactory.createXMLStreamReader(is);

		return streamReader.getEncoding();
	}

	public static String detectedEncodingName(InputStream is, int[] bomSize) throws IOException {
		final byte[] b4 = new byte[4];
		String encoding = null;

		// this has the potential to throw an exception
		// it will be fixed when we ensure the stream is rewindable (see note
		// above)
		BufferedInputStream bis = new BufferedInputStream(is);
		bis.mark(4);
		int count = bis.read(b4, 0, 4);

		// Detect from Bye-order Mark
		encoding = detectEncodingNameFromBOM(b4, count, bomSize);

		// Detect from PI Marker
		if (encoding == null) {
			encoding = detectEncodingNameFromPIMarker(b4, count);
		}

		return encoding;
	}

	public static String detectEncodingNameFromBOM(byte[] b4, int count, int[] bomSize) {
		if (count >= 3) {
			int b0 = b4[0] & 0xFF;
			int b1 = b4[1] & 0xFF;
			int b2 = b4[2] & 0xFF;

			// UTF-8 with a BOM
			if (b0 == 0xEF && b1 == 0xBB && b2 == 0xBF) {
				bomSize[0] = 3;
				return "UTF-8"; //$NON-NLS-1$
			}

			if (count >= 4) {
				int b3 = b4[3] & 0xFF;

				// UCS-4, with BOM
				if (b0 == 0x00 && b1 == 0x00) {
					if ((b2 == 0xFE && b3 == 0xFF) ||
					// UCS-4, big-endian machine (1234 order)
							(b2 == 0xFF && b3 == 0xFE)) { // UCS-4, unusual
															// octet order
															// (2143)
						bomSize[0] = 4;
						return "ISO-10646-UCS-4"; //$NON-NLS-1$
					}
				}

				// UTF-8/UTF-16 with a BOM
				if (b0 == 0xFE && b1 == 0xFF) {
					bomSize[0] = 4;
					if (b2 == 0x00 && b3 == 0x00) { // UCS-4, unusual octet
													// order (3412)
						return "ISO-10646-UCS-4"; //$NON-NLS-1$
					} else { // UTF-16, big-endian
						return "UTF-16BE"; //$NON-NLS-1$
					}
				}

				if (b0 == 0xFF && b1 == 0xFE) {
					bomSize[0] = 4;
					if (b2 == 0x00 && b3 == 0x00) { // UCS-4, little-endian
													// machine (4321 order)
						return "ISO-10646-UCS-4"; //$NON-NLS-1$
					} else { // UTF-16, little-endian
						return "UTF-16LE"; //$NON-NLS-1$
					}
				}
			}
		}

		return null;
	}

	public static String detectEncodingNameFromPIMarker(byte[] b4, int count) {

		if (count >= 4) {
			int b0 = b4[0] & 0xFF;
			int b1 = b4[1] & 0xFF;
			int b2 = b4[2] & 0xFF;
			int b3 = b4[3] & 0xFF;

			// other encodings
			if ((b0 == 0x00 && b1 == 0x00 && b2 == 0x00 && b3 == 0x3C) ||
			// UCS-4, big-endian machine (1234 order)
					(b0 == 0x3C && b1 == 0x00 && b2 == 0x00 && b3 == 0x00) ||
					// UCS-4, little-endian machine (4321 order)
					(b0 == 0x00 && b1 == 0x00 && b2 == 0x3C && b3 == 0x00) ||
					// UCS-4, unusual octet order (2143)
					(b0 == 0x00 && b1 == 0x3C && b2 == 0x00 && b3 == 0x00)) { // UCS-4,
																				// unusual
																				// octet
																				// order
																				// (3412)
				return "ISO-10646-UCS-4"; //$NON-NLS-1$
			}

			if (b0 == 0x00 && b1 == 0x3C && b2 == 0x00 && b3 == 0x3F) {
				// UTF-16, big-endian, no BOM
				// (or could turn out to be UCS-2...
				return "UTF-16BE"; //$NON-NLS-1$
			}
			if (b0 == 0x3C && b1 == 0x00 && b2 == 0x3F && b3 == 0x00) {
				// UTF-16, little-endian, no BOM
				// (or could turn out to be UCS-2...
				return "UTF-16LE"; //$NON-NLS-1$
			}
			if (b0 == 0x4C && b1 == 0x6F && b2 == 0xA7 && b3 == 0x94) {
				// EBCDIC
				// a la xerces1, return CP037 instead of EBCDIC here
				return "CP037"; //$NON-NLS-1$
			}
			if (b0 == 0x3C && b1 == 0x3F && b2 == 0x78 && b3 == 0x6D) {
				// some sort of ASCII derivative
				return "US-ASCII"; //$NON-NLS-1$
			}
		}

		return null;
	}

	protected static final String _NULL_ = "NULL";

	public static String toSQLSyntax(Connection conn, FormatType formatType, Object obj) {
		return toSQLSyntax(conn, formatType, obj, obj);
	}

	public static String toSQLSyntax(Connection conn, FormatType formatType, Object obj, Object preFormattedObj) {
		int sqlType = getSQLType(obj);

		String str = toSQLStringValue(conn, sqlType, formatType, obj, preFormattedObj);
		String literal = toSQLLiteral(str, sqlType);

		String wrapper = getSQLLiteralWrapper(conn, sqlType, formatType);

		return String.format(wrapper, literal);
	}

	public static String toSQLStringValue(Connection conn, FormatType formatType, Object obj, Object preFormattedObj) {
		return toSQLStringValue(conn, getSQLType(obj), formatType, obj, preFormattedObj);
	}

	public static String toSQLStringValue(Connection conn, int sqlType, FormatType formatType, Object obj,
			Object preFormattedObj) {
		if (obj != null) {
			Format formatter = getSQLFormatter(conn, sqlType, formatType);

			// Format as String
			if (formatter != null) {
				return formatter.format(obj);
			} else if (preFormattedObj == null) {
				return obj.toString();
			}
		}

		if (preFormattedObj != null) {
			return preFormattedObj.toString();
		} else {
			return null;
		}
	}

	public static String getSQLLiteralWrapper(Connection conn, int sqlType, FormatType formatType) {
		StringBuffer wrapper = new StringBuffer();

		boolean closeBracket = true;
		switch (sqlType) {
		case OracleTypes.DATE:
			wrapper.append("TO_DATE(");
			break;
		case OracleTypes.TIMESTAMP:
		case OracleTypes.TIMESTAMPLTZ:
			wrapper.append("TO_TIMESTAMP(");
			break;
		case OracleTypes.TIMESTAMPTZ:
			wrapper.append("TO_TIMESTAMP_TZ(");
			break;
		default:
			closeBracket = false;
		}

		wrapper.append("%s");

		if (closeBracket) {
			String pattern = getSQLFormatPattern(conn, sqlType, formatType);

			if (pattern != null) {
				wrapper.append(", ").append(toSQLLiteral(pattern, Types.CHAR));
			}

			wrapper.append(")");
		}

		return wrapper.toString();
	}

	public static Format getSQLFormatter(Connection conn, int sqlType, FormatType formatType) {
		return NLSUtils.getFormat(conn, sqlType, formatType);
	}

	public static String getSQLFormatPattern(Connection conn, int sqlType, FormatType formatType) {
		switch (sqlType) {
		case OracleTypes.DATE:
			return NLSUtils.getDateFormat(conn, formatType);
		case OracleTypes.TIMESTAMP:
		case OracleTypes.TIMESTAMPLTZ:
			return NLSUtils.getTimeStampFormat(conn, formatType);
		case OracleTypes.TIMESTAMPTZ:
			return NLSUtils.getTimeStampWithTimeZoneFormat(conn, formatType);
		default:
			return null;
		}
	}

	public static int getSQLType(Object obj) {
		return (obj != null) ? getSQLType(obj.getClass()) : -1;
	}

	/**
	 * Convert Datum Class to Oracle SQL Type
	 *
	 * @param clazz
	 * @return
	 */
	public static int getSQLType(Class<?> clazz) {
		if (oracle.sql.DATE.class.isAssignableFrom(clazz)) {
			return OracleTypes.DATE;
		} else if (oracle.sql.TIMESTAMP.class.isAssignableFrom(clazz)) {
			return OracleTypes.TIMESTAMP;
		} else if (oracle.sql.TIMESTAMPLTZ.class.isAssignableFrom(clazz)) {
			return OracleTypes.TIMESTAMPLTZ;
		} else if (oracle.sql.TIMESTAMPTZ.class.isAssignableFrom(clazz)) {
			return OracleTypes.TIMESTAMPTZ;
		} else if (oracle.sql.INTERVALDS.class.isAssignableFrom(clazz)) {
			return OracleTypes.INTERVALDS;
		} else if (oracle.sql.INTERVALYM.class.isAssignableFrom(clazz)) {
			return OracleTypes.INTERVALYM;
		} else if (oracle.sql.NUMBER.class.isAssignableFrom(clazz)) {
			return OracleTypes.NUMBER;
		} else if (oracle.sql.BINARY_DOUBLE.class.isAssignableFrom(clazz)) {
			return OracleTypes.BINARY_DOUBLE;
		} else if (oracle.sql.BINARY_FLOAT.class.isAssignableFrom(clazz)) {
			return OracleTypes.BINARY_FLOAT;
		} else if (Number.class.isAssignableFrom(clazz)) {
			return Types.NUMERIC;
		} else {
			return -1;
		}
	}

	public static String toSQLLiteral(String str, int sqlType) {
		return toSQLLiteral(str, sqlType, true);
	}

	public static String toSQLLiteral(String str, int sqlType, boolean treatEmptyAsNull) {
		if (str == null) {
			return "NULL";
		} else if (str.length() == 0) {
			if (treatEmptyAsNull) {
				return "NULL";
			} else {
				return "'''";
			}
		} else if (getDataTypeKind(sqlType) == DataTypeKind.NUMERIC) {
			return str;
		} else {
			return "'" + str.replaceAll("'", "''") + "'";
		}
	}

	public static void copyCharacters(Readable reader, Appendable writer) throws IOException {
		copyCharacters(reader, writer, BUFFER_SIZE);
	}

	public static void copyCharacters(Readable reader, Appendable writer, int bufferSize) throws IOException {
		// Read in 256 byte blocks
		char[] b = new char[bufferSize];

		// Wrap with buffer
		CharBuffer buf = CharBuffer.wrap(b);

		// Read chunk
		int len = reader.read(buf);

		// Read and Write to the end
		while (len != -1) {
                        // Rewind buffer prior to write
                        buf.rewind();

			// Write Buffer
			writer.append(buf, 0, len);

			// Read next chunk
			len = reader.read(buf);
		}
	}

	public static void copyCharacters(Readable reader, StringBuffer buffer) throws IOException {
		copyCharacters(reader, buffer, BUFFER_SIZE);
	}

	public static void copyCharacters(Readable reader, StringBuffer buffer, int bufferSize) throws IOException {
		// Read in 256 byte blocks
		char[] b = new char[bufferSize];

		// Wrap with buffer
		CharBuffer buf = CharBuffer.wrap(b);

		// Read chunk
		int len = reader.read(buf);

		// Read and Write to the end
		while (len != -1) {
			// Write Buffer
			buffer.append(buf, 0, len);

			// Read next chunk
			len = reader.read(buf);
		}
	}

	public static void copyCharacters(Reader reader, StringBuilder buffer) throws IOException {
		copyCharacters(reader, buffer, BUFFER_SIZE);
	}

	public static void copyCharacters(Readable reader, StringBuilder buffer, int bufferSize) throws IOException {
		// Read in 256 byte blocks
		char[] b = new char[bufferSize];

		// Wrap with buffer
		CharBuffer buf = CharBuffer.wrap(b);

		// Read chunk
		int len = reader.read(buf);

		// Read and Write to the end
		while (len != -1) {
			// Write Buffer
			buffer.append(buf, 0, len);

			// Read next chunk
			len = reader.read(buf);
		}
	}

	public static boolean compareData(Readable r1, Readable r2) throws IOException {
		// Read in 256 byte blocks
		char[] b1 = new char[BUFFER_SIZE];
		char[] b2 = new char[BUFFER_SIZE];

		// Fill Arrays
		Arrays.fill(b1, ' ');
		Arrays.fill(b2, ' ');

		// Wrap with buffers
		CharBuffer buf1 = CharBuffer.wrap(b1);
		CharBuffer buf2 = CharBuffer.wrap(b2);

		// Read chunk
		int l1 = r1.read(buf1);
		int l2 = r2.read(buf2);

		// Read and Write to the end
		while (l1 != -1 && l2 != -1 && l1 == l2) {
			// Write Buffer

			if (!Arrays.equals(b1, b2)) {
				return false;
			}

			l1 = r1.read(buf1);
			l2 = r2.read(buf2);
		}

		return (l1 == l2);
	}
}
