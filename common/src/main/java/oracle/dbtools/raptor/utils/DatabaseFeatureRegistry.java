/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.db.ConnectionIdentifier;
import oracle.dbtools.db.ConnectionIdentifier.Key;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.query.ObjectQueries;
import oracle.dbtools.raptor.query.Query;
import oracle.dbtools.util.Logger;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLNode;

import org.w3c.dom.Node;
import org.w3c.dom.Text;

/**
 * Registry of Database features. The Feature registry can be used to check whether a particular feature is enabled for a specific connection. 
 * Most features are database level features, like APEX or Java support, and are checked at connect time. In some cases, a feature might be
 * enabled or disabled for a session via user interaction.
 * 
 * @author jmcginni
 *
 */
public abstract class DatabaseFeatureRegistry {
    private static final String CLASSNAME_ELEMENT = "classname"; //$NON-NLS-1$
    private static final String QUERIES_ELEMENT = "queries"; //$NON-NLS-1$
    private static final String ID_ATTR = "id"; //$NON-NLS-1$
    private static final String FEATURE_ELEMENT = "feature"; //$NON-NLS-1$
        
    public static final DatabaseFeatureRegistry DEFAULT_REGISTRY = new DatabaseFeatureRegistry() {
		@Override
		protected void loadFeaturesImpl() {
		}
	};
	
	private static DatabaseFeatureRegistry sInstance = DEFAULT_REGISTRY;
	
	public static void setFeatureRegistry(DatabaseFeatureRegistry registry) {
		sInstance = registry;
		sInstance.initialize();
	}
	
	public static boolean isFeatureEnabled(ConnectionIdentifier connID, String featureID) {
		return sInstance.isFeatureEnabledImpl(connID, featureID);
	}

    private Map<String, DatabaseFeature> classnameMap = new HashMap<String, DatabaseFeature>();

    private Map<String, DatabaseFeature> knownFeatures;
	
    private static class QueryFeature implements DatabaseFeature {
        private ObjectQueries m_queries;
        
        QueryFeature(XMLNode elem) {
            m_queries = new ObjectQueries(elem);
        }
        
        @Override
        public boolean isFeatureEnabled(ConnectionIdentifier connID, String featureID) {
            Query q = m_queries.getQuery(connID);
            if ( q != null ) {
                String sql = q.getSql();
                DBUtil util = DBUtil.getInstance(connID);
                try {
                    return util.executeReturnOneCol(sql) != null;
                } finally {
                    SQLException ex = util.getLastException();
                    if ( ex != null ) {
                        // Log the exception with a fine granularity. In almost all cases, the exception is a valid result, as we are checking for the presence of a table, view, or feature and
                        // the lack with result in an exception. Logging the exception helps to catch the other cases, and fine logs shouldn't be visible in a production build.
                        Logger.fine(DatabaseFeatureRegistry.class, ex);
                    }
                }
            }
            return false;
        }
        
    }
    
    private Map<ConnectionIdentifier.Key, Map<String, Boolean>> m_features = new HashMap<ConnectionIdentifier.Key, Map<String,Boolean>>();
	
    protected abstract void loadFeaturesImpl();
    
    protected final void loadFeatures() {
    	if ( knownFeatures == null ) {
    		knownFeatures = new HashMap<String, DatabaseFeature>();

        	loadFeaturesImpl();
    	}
    }
    
    /**
     * Called when the DatabaseFeatureRegistry is first made active.
     */
    protected void initialize() {
    	
    }
	
	/**
	 * Whether the feature with the specified ID is enabled for the given connection. An unknown or uninitialized feature is always disabled.
	 * 
	 * @param conn the Connection 
	 * @param id the feature ID
	 * @return whether the feature is enabled.
	 */
	protected boolean isFeatureEnabledImpl(ConnectionIdentifier connID, String featureID) {
    	boolean enabled = false;
    	
        Map<String, Boolean> features = m_features.get(connID.getIdentifierKey());
        if ( features != null ) {
        	Boolean val = features.get(featureID);
        	enabled = val != null && val.booleanValue();
        }
        return enabled;
	}

	private Map<String, Boolean> getFeatures(ConnectionIdentifier connID) {
		Key key = connID.getIdentifierKey();
		Map<String, Boolean> features = m_features.get(key);
        if ( features == null ) {
        	features = new HashMap<String, Boolean>();
        	m_features.put(key, features);
        }
		return features;
	}
    
    protected void initializeFeatures(ConnectionIdentifier connID) {
        Map<String, Boolean> features = getFeatures(connID);
        for ( Entry<String, DatabaseFeature> entry : getKnownFeatures().entrySet()) {
            String id = entry.getKey();
            DatabaseFeature df = entry.getValue();
            features.put(id, df.isFeatureEnabled(connID, id));
        }
    }

    private Map<String, DatabaseFeature> getKnownFeatures() {
    	loadFeatures();
        return Collections.unmodifiableMap(knownFeatures);
    }
    
    protected void cleanupFeatures(ConnectionIdentifier connID) {
        m_features.remove(connID.getIdentifierKey());
    }
    
	protected void processURL(URL url,
			ClassLoader cl) {
		Map<String, DatabaseFeature> localFeatures = new HashMap<String, DatabaseFeature>();
        InputStream in = null;
        try {
            DOMParser parser = new DOMParser();
            parser.setPreserveWhitespace(false);
            in = url.openStream();
            parser.parse(in);
            XMLDocument doc = parser.getDocument();
            // Get the features element
            Node root = doc.getFirstChild();

            // Get all the feature elements
            XMLNode[] featureNodes = XMLHelper.getChildNodes(root, FEATURE_ELEMENT);
            for (XMLNode node: featureNodes) {
                // A feature element always has an id
                String id = XMLHelper.getAttributeNode(node, ID_ATTR);

                // A feature either has a query or a classname
                Node child = XMLHelper.getChildNode(node, QUERIES_ELEMENT);
                if (child != null) {
                    if (child instanceof XMLNode) {
                        localFeatures.put(id, new QueryFeature((XMLNode) child));
                    }
                } else {
                    child = XMLHelper.getChildNode(node, CLASSNAME_ELEMENT);
                    if (child != null) {
                        String clsName = null;
                        Node txt = child.getFirstChild();
                        if (txt instanceof Text) {
                            clsName = txt.getTextContent();
                        }

                        if (ModelUtil.hasLength(clsName)) {
                            DatabaseFeature feature = classnameMap.get(clsName);
                            if (feature == null) {
                                // create the class
								Class<? extends DatabaseFeature> cls = cl.loadClass(clsName).asSubclass(DatabaseFeature.class);
                                feature = cls.newInstance();
                                classnameMap.put(clsName, feature);
                            }
                            localFeatures.put(id, feature);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.severe(DatabaseFeatureRegistry.class, "Error parsing file " + url.toString(), e); //$NON-NLS-1$
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (in != null) try {
                in.close();
            } catch (Exception ex) {
            }
        }
        knownFeatures.putAll(localFeatures);
	}
}