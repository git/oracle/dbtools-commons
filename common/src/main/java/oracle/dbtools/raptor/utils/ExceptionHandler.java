/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.sql.SQLException;

/**
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=ExceptionHandler.java"
 *         >Barry McGillin</a>
 * 
 */
public final class ExceptionHandler {
	//can be overridden later (it was not set for sdcli->null pointer exception)
	//this is a static - if you have a background task - do you want it to have the same handler as the UI worksheet?
	//i.e. should it be per script runner context?
	private static IExceptionHandler handler=new BasicExceptionHandler();
	public static void setHandler(IExceptionHandler my_handler) {
		handler = my_handler;
	}
	private static Throwable hitException = null;

	public static Throwable wasException() {
		return hitException;
	}

	public static void reset() {
		hitException = null;
	}

	private ExceptionHandler() {
	}

	public static void handleException(final Throwable e) {
		handler.handleException(null, e);
	}

	public static void handleException(final String connName, final Throwable e) {
		handler.handleException(connName, e);
	}

	public static void handleException(Exception e, String sql, int offset) {
		hitException = e;
		handler.handleException(e, sql, offset);
	}

	public static void handleException(final Exception e, final String sql, final int offset, final int line,
	        final int col) {
		hitException = e;
		handler.handleException(e, sql, offset, line, col);
	}

	public static boolean handleExceptionWithAction(final Exception e, String title, String okText, String cancelText) {
		hitException = e;
		return handler.handleExceptionWithAction(e, title, okText, cancelText);
	}

	public static void handleException(Exception e, String connName, String sql, int offset, int line, int col) {
		hitException = e;
		handler.handleException(e, connName, sql, offset, line, col);
	}

	public static void handleException(final SQLException e, final String connName, final String sql, final int offset) {
		handler.handleException(e, connName, sql, offset);
	}

	public static void handleException(final Exception e, final String connName) {
		handler.handleException(e, connName);
	}

	public static void handleException(final Exception e, final String connName, boolean silent, boolean log) {
		handler.handleException(e, connName, silent, log);
	}
}
