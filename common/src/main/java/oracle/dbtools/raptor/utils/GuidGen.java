/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.io.ByteArrayOutputStream;
import java.net.InetAddress;
import java.util.Random;


/**
 * This static class provided utilties for generation and manipulation
 * of DFC style Globally Unique Identifiers,
 * using the original HP/OSF UUID reference implemenation as a basis.
 * Since the Java runtime does not allow access to the full IEEE 802
 * 48-bit ethernet address of a host (if indeed there is one)
 * this implementation attempts to use the 32-bit IP number
 * in the first instance, and defaults to the sample
 * pseudo-unique algorithm if the IP address is not available.<BR>
 * A native method-based extension will be provided in due course
 * to provide true GUID values.


 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=GuidGen.java">Barry McGillin</a> 
 *
 */
public class GuidGen {


//
// Cached pseudo-IEEE ethernet address
//
private static byte m_arrIeee[] = null;

//
// A pseudo clock sequence - guarantees at least that for a
// given JVM invocation on a given machine, these GUIDs are unique.
//
private static int  m_nClockSeq = 0;

//
// No core package hex formatting
//
private static char m_arrHex[] = {'0','1','2','3','4','5','6','7','8','9',
                                  'a','b','c','d','e','f'};

/**
 * The length of a raw GUID byte array
 */
public static final int GUID_LENGTH = 16;

/**
 * remember the last generated GUID
 */
private static String m_lastUuid = null;

///////////////////////////////////////////////////////////////////////////////
// GuidGen.uuidCreate()
//
/**
 * Generate a raw 128-bit (16-byte) UUID.
 * @return a raw 16-byte UUID
 */
public static synchronized byte[] uuidCreate()
{
   // use the private implementation to create the guid and remeber the guid for
   // next time
   byte uuid[] = uuidCreateImpl().toByteArray();

   m_lastUuid = toString(uuid);

   return uuid;
}

private static synchronized ByteArrayOutputStream uuidCreateImpl()
{
    long    nTime = System.currentTimeMillis();
    byte[]  ieeeAddr = getIeeeNodeIdentifier();

    if (ieeeAddr == null)
        return null;

    ByteArrayOutputStream   s = new ByteArrayOutputStream(16);
    //
    // Since all of these values are 2's complement, I
    // don't think we are actually generating pukka DFC GUIDs -
    // but they should be statistically unique, all the same.
    //
    int   nTimeLow = (int)(nTime & 0xFFFFFFFF);
    short nTimeMid = (short)((nTime >> 32) & 0x0000FFFF);
    short nTimeHiVersion = (short)(((nTime >> 32) & 0x0FFF0000) >> 16);
    nTimeHiVersion |= (1 << 12);
    byte  nClockSeqLow = (byte)(m_nClockSeq & 0xFF);
    byte  nClockSeqHiReserved = (byte)((m_nClockSeq & 0x3F00) >> 8);
    nClockSeqHiReserved |= 0x80;

    s.write((nTimeLow >> 24) & 0xFF);
    s.write((nTimeLow >> 16) & 0xFF);
    s.write((nTimeLow >> 8) & 0xFF);
    s.write(nTimeLow  & 0xFF);

    s.write((nTimeMid >> 8) & 0xFF);
    s.write(nTimeMid & 0xFF);

    s.write((nTimeHiVersion >> 8) & 0xFF);
    s.write(nTimeHiVersion & 0xFF);

    s.write(nClockSeqHiReserved);
    s.write(nClockSeqLow);
    s.write(ieeeAddr, 0, 6);

    m_nClockSeq++;

    if (m_lastUuid != null && m_lastUuid.equals(toString(s.toByteArray())))
    {
       s = uuidCreateImpl(); // redo the generation
    }

    return s;
}

///////////////////////////////////////////////////////////////////////////////
// GuidGen.toString()
//
/**
 * Convert a raw UUID to a standard string representation.
 * @param uuidRaw A raw 16-byte UUID
 * @return String representation of the UUID.  Null on failure.
 */
public static String toString(byte[] uuidRaw)
{
    if (uuidRaw.length != 16)
        return null;

    StringBuffer    strBuffer = new StringBuffer();

    for (int nIdx = 0; nIdx < 16; nIdx++)
    {
        byteToHex(strBuffer, uuidRaw[nIdx]);
        if (nIdx == 3 || nIdx == 5 || nIdx == 7 || nIdx == 9)
            strBuffer.append('-');
    }

    return strBuffer.toString();
}

///////////////////////////////////////////////////////////////////////////////
// GuidGen.toRaw()
//
/**
 * Convert a standard UUID string representation to a raw 16-byte UUID.
 * @param strUUID A standard string representation of a UUID
 * @return a raw 16-byte UUID. Null on failure
 */
public static byte[] toRaw(String strUUID)
{
    //
    // String must always be of the format;
    // xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
    //
    if (strUUID == null || strUUID.length() != 36 ||
        strUUID.charAt(8) != '-' || strUUID.charAt(13) != '-' ||
        strUUID.charAt(18) != '-' || strUUID.charAt(23) != '-')
        return null;

    byte[] arrUuid = new byte[16];
    int  nArr = 0;
    for (int nIdx = 0; nIdx < strUUID.length(); nIdx += 2)
    {
        // Skip the separators;
        if (nIdx == 8 || nIdx == 13 || nIdx == 18 || nIdx == 23)
            nIdx++;

        String strByte = strUUID.substring(nIdx, nIdx+2);
        arrUuid[nArr++] = (byte)Short.parseShort(strByte, 16);
    }
    return arrUuid;
}

///////////////////////////////////////////////////////////////////////////////
// GuidGen.byteToHex
/**
 * Coverts the specified byte value to a hexadecimal representaion,
 * and appends this to the specified String buffer.
 * @param buff the StringBuffer
 * @param byteVal the byte value to be converted.
 */
public static void byteToHex(StringBuffer buff, byte byteVal)
{
    buff.append(m_arrHex[((byteVal >> 4) & 0x0F)]);
    buff.append(m_arrHex[(byteVal & 0x0F)]);
}

///////////////////////////////////////////////////////////////////////////////
// GuidGen.getIeeeNodeIdentifier()
//
// For a true DFC UUID, this method should return the 6-byte IEEE 802
// ethernet address of the current host.  However, the best we can hope for
// from the Java runtime is the local IP number.  In order try and maintain
// a unique value, we will use the raw IP number as the high four bytes
// of the IEEE address, and two random bytes for the remaining space.
//
// If the Java SecurityManager denies access to the IP number (for example
// when running in a applet) then we resort to generating a pseudo-unique
// identifier.
private static byte[] getIeeeNodeIdentifier()
{
    if (m_arrIeee != null)
      return m_arrIeee;

    Random rndTmp = new Random(System.currentTimeMillis() + (m_nClockSeq++));

    m_arrIeee = new byte[6];

    byte[]  arrInet = null;
    try
    {
        arrInet = InetAddress.getLocalHost().getAddress();
        if (arrInet.length != 4)
            throw new Exception("IP address != 4 bytes!!"); //NOTRANS

        // Fill the final two bytes
        byte[]    arrRand = new byte[2];
        rndTmp.nextBytes(arrRand);
        m_arrIeee[0] = arrInet[0];
        m_arrIeee[1] = arrInet[1];
        m_arrIeee[2] = arrInet[2];
        m_arrIeee[3] = arrInet[3];
        m_arrIeee[4] = arrRand[0];
        m_arrIeee[5] = arrRand[1];
    }
    catch(Exception e)
    {
        //
        // Assume this is a security exception - generate a pseudo-unique ID
        //
        TRACE("getIeeeNodeIdentifier() - " + e); //NOTRANS

        //
        // Use a full pseudo-random sequence
        //
        rndTmp.nextBytes(m_arrIeee);
    }

    //TRACE("getIeeeNodeIdentifier() - " + m_arrIeee); //NOTRANS
    return m_arrIeee;
}
///////////////////////////////////////////////////////////////////////////////
// GuidGen.TRACE()
private static void TRACE(String strMessage)
{
    System.out.println("GuidGen." + strMessage); //NOTRANS
}

///////////////////////////////////////////////////////////////////////////////
// GuidGen.main()
//
// Test harness
static void main(String args[])
{
    byte[]  uuid = null;
    String  strUuid = null;

    try
    {
        for (int n = 0; n < 100; n++)
        {
            uuid = GuidGen.uuidCreate();
            strUuid = GuidGen.toString(uuid);

            TRACE("UUID = " + strUuid); //NOTRANS
        }

        uuid = GuidGen.uuidCreate();
        strUuid = GuidGen.toString(uuid);

        byte[] uuidRaw = GuidGen.toRaw(strUuid);
        String strUuidRaw = GuidGen.toString(uuidRaw);

        TRACE("----------------------------------------------------------"); //NOTRANS
        TRACE("SRC - " + strUuid); //NOTRANS
        TRACE("DST - " + strUuidRaw); //NOTRANS
        /*
        byte b = Short.parseShort("80", 16); //NOTRANS
        TRACE("b = " + b); //NOTRANS
        */
    }
    catch (Exception e)
    {
        TRACE(e.toString());
    }
}

} // End of class GuidGen.//

