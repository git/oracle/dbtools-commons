/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.lang.reflect.Method;

import java.sql.Connection;

import java.util.Properties;

/**
 * JDBC Proxy Utility
 */
public final class JDBCProxyUtil {
    private static final String PROXY_REGISTRY_CLASS = "oracle.dbtools.raptor.proxy.ProxyRegistry"; //$NON-NLS-1$
    private static final String PROXY_REGISTRY_UNWRAP = "unwrap"; //$NON-NLS-1$
    private static final String PROXY_REGISTRY_PROXY_FOR = "jdbcProxyFor"; //$NON-NLS-1$

    private static JDBCProxyUtil INSTANCE = null;
    
    private ClassLoader classLoader;
    private Class<?> proxyRegistryClass;
    private Method unwrapMethod;
    private Method jdbcProxyForMethod;
    
    private ClassLoader classLoaderHint;

    /**
     * Create Singleton Instance
     */
    private JDBCProxyUtil() {
        super();
        
        this.classLoader = null;
        this.proxyRegistryClass = null;
        this.unwrapMethod = null;
        this.jdbcProxyForMethod = null;
        this.classLoaderHint = null;
    }

    /**
     * Get Singleton Instance 
     * 
     * @param classLoaderHint
     * @return
     */
    public static JDBCProxyUtil getInstance(ClassLoader classLoaderHint) {
        if (INSTANCE == null) {
            synchronized (JDBCProxyUtil.class) {
                if (INSTANCE == null) {
                    INSTANCE = new JDBCProxyUtil();
                }
            }
        }
        
        INSTANCE.suggestClassLoader(classLoaderHint);
        
        return INSTANCE;
    }

    /**
     * Get Singleton Instance 
     * 
     * @return
     */
    public static JDBCProxyUtil getInstance() {
        return getInstance(null);
    }
    
    /**
     * Static Wrap Connection with Proxy
     *
     * @return
     */
    public Connection jdbcProxyFor(Connection conn, Properties p) {
        if (conn != null) {
            // Get Proxy
            try {
                Method jdbcProxy = jdbcProxyForMethod;
                
                if (jdbcProxy == null) {
                    Class<?> registryClass = (proxyRegistryClass != null) ? proxyRegistryClass : loadClass(PROXY_REGISTRY_CLASS, conn.getClass().getClassLoader());
                    proxyRegistryClass = registryClass;
                    jdbcProxy = registryClass.getMethod(PROXY_REGISTRY_PROXY_FOR, new Class[] {Connection.class, Properties.class});
                    jdbcProxyForMethod = jdbcProxy;
                }
                
                conn = (Connection)jdbcProxy.invoke(null, conn, p);
            } catch (Exception e) {
                ;
            }
        }
        
        return conn;
    }
    
    /**
     * Unwrap Connection Proxy
     *
     * @return
     */
    @SuppressWarnings("oracle.jdeveloper.java.unchecked-cast")
    public <T> T unwrap(T obj) {
        if (obj != null) {
            // Get Delegate
            try {
                Method unwrap = unwrapMethod;
                
                if (unwrap == null) {
                    Class<?> registryClass = (proxyRegistryClass != null) ? proxyRegistryClass : loadClass(PROXY_REGISTRY_CLASS, obj.getClass().getClassLoader());
                    proxyRegistryClass = registryClass;
                    unwrap = registryClass.getMethod(PROXY_REGISTRY_UNWRAP, Object.class);
                    unwrapMethod = unwrap;
                }
                
                obj = (T)unwrap.invoke(null, obj);
            } catch (Exception e) {
                ;
            }
        }
        
        return obj;
    }
    
    private Class<?> loadClass(String className, ClassLoader contextClassLoader) throws ClassNotFoundException {
        Class<?> clazz = null;
        
        if (classLoader != null) {
            clazz = classLoader.loadClass(className);
        }
        
        if (clazz == null && classLoaderHint != null) {
            try {
                clazz = classLoaderHint.loadClass(className);
                setClassLoader(classLoaderHint);
            } catch (Exception e) {
                suggestClassLoader(null);
            }
        }
        
        if (clazz == null && contextClassLoader != null) {
            clazz = contextClassLoader.loadClass(className);
            setClassLoader(contextClassLoader);
        }
        
        return clazz;
    }

    /**
     * Set Class Loader
     * 
     * @param classLoader
     */
    public synchronized void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
        suggestClassLoader(null);
    }

    /**
     * Set Class Loader Hint
     * 
     * @param classLoaderHint
     */
    public synchronized void suggestClassLoader(ClassLoader classLoaderHint) {
        this.classLoaderHint = (classLoaderHint != null && this.classLoader == null) ? classLoaderHint : null;
    }

    /**
     * Get Class Loader
     * 
     * @return
     */
    public ClassLoader getClassLoader() {
        return classLoader;
    }
}
