/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

public class ListPrinter implements IListPrinter {

  
  /**
   *   Since the dbutil returns a list<map<string,object>>  this simple gets the object and does a toString or for null returns ""
   * @param m
   * @param key
   * @return
   */
	@Override
  public String getValue(Map<String, ?> m, String key) {
    if (m.get(key) == null) {
      return "";
    } else {
      return m.get(key).toString();
    }
  }

  /**
   * This method walks over all the rows for the specified column name and returns the width of the longest
   * 
   * @param cols
   * @param string
   * @return
   */
	@Override
  public int getWidest(List<Map<String, ?>> cols, String string) {
    // default to the column name
    int ret = string.length();
    for (Map<String, ?> m : cols) {
      if (m.get(string) != null) {
        int l = m.get(string).toString().length();
        ret = l > ret ? ret = l : ret;
      }
    }
    return ret;
  }

  /**
   * Meant to be used with DBUtils.executeReturnList( final String sql, final Map<String, ?> binds )
   * However the columns will not always be in the order of the select from the hashmap

   * 
   * @param ctx
   * @param rows
   */
	@Override
  public void print(ScriptRunnerContext ctx, List<Map<String, ?>> rows) {
    if (rows.size() > 0) {
      int[] widths = new int[rows.get(0).size()];
      String[] colName = new String[rows.get(0).size()];
      int i = 0;
      // get the widths
      for (String col : rows.get(0).keySet()) {
        colName[i] = col;
        widths[i] = getWidest(rows, col) + 2;
        i++;
      }

      // print the headers
      i = 0;

      for (String col : rows.get(0).keySet()) {
      	writeBoldUnderlineFormat(ctx,col,widths[i]);
        i++;
      }
      ctx.write("\n");

      // print the rows
      for (Map<String, ?> row : rows) {
        i = 0;
        for (String col : colName) {
          ctx.write(String.format("%-" + widths[i] + "s", getValue(row, col)));
          i++;
        }
        ctx.write("\n");
      }
      ctx.write("\n");

    } else {
      ctx.write("No Data Found.");
    }
  }

  

	/**
   * Meant to be used with DBUtil.executeReturnListofList( final String sql, final Map<String, ?> binds )
   * 
   * @param ctx
   * @param rows
   */
	@Override
  public void printListofList(ScriptRunnerContext ctx, List<List<?>> rows) {
    try {
      print(ctx.getOutputStream(),ctx.getCurrentConnection(),rows);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

	@Override
  public  void printListofList(ScriptRunnerContext ctx, String title, List<List<?>> rows) {
    if ( title != null ){
    	ctx.write(title+"\n");
    }
    printListofList(ctx,rows);
  }

	@Override
  public void print(OutputStream outStream, Connection conn,List<List<?>> rows) throws IOException {
      print(outStream, conn, rows, 0);
  }
    
	@Override
  public void print(OutputStream outStream, Connection conn,List<List<?>> rows,int pageSize) throws IOException {
      print(outStream, conn, rows, pageSize, "",null);
  }
  
	@Override
  public void print(OutputStream outStream, Connection conn,List<List<?>> rows,int pageSize,String nullDisplay,List<Boolean> isNumber) throws IOException {
    OutputStreamWriter out = new OutputStreamWriter(outStream);
    if (rows!= null && rows.size() > 0) {
      int[] widths = new int[rows.get(0).size()];
      // get the widths
      for (List<?> row: rows) {
        for(int i=0;i<row.size();i++){
          if (row.get(i) != null && widths[i] <  row.get(i).toString().length() + 2 ){
            widths[i] =row.get(i).toString().length() +2;
          } 
        }
      }

      // print the headers
      int j=0;
      List<?> headers = rows.get(0);
      rows.remove(0);
      int i=0;
      for (List row:rows) {
        int ii=0;
        if ( i== 0 || ( pageSize != 0 && i % pageSize == 0 ) ){
          printHeaders(out, widths, j, headers,isNumber);          
        }
        for(Object col:row){
          String val = NLSUtils.format(conn, col);
          if (val == null) val = (nullDisplay != null) ? nullDisplay : "";
          if (col != null &&  col.toString().indexOf("@|") >= 0 && col.toString().indexOf("|@") > 0 ){
            try { 
            	printValue(out,widths[ii],val,isNumber!=null?isNumber.get(ii):false);
            } catch (IllegalArgumentException e ){
              out.write(e.getLocalizedMessage());
            }
          } else if ( isNumber != null && isNumber.get(ii) && val != null){
            String s = String.format("%" + widths[ii] + "s", val );
            out.write(s);
          } else {
            String s = String.format("%-" + widths[ii] + "s", val); 
            out.write(s);
          }
          out.write(" ");
          ii++;
        }
        out.write("\n");
        i++;
      }
      out.write("\n");
    } else {
      out.write("No Data Found.");
    }    
    out.flush();
  }

	@Override
  public void printHeaders(OutputStreamWriter out, int[] widths, int j, List<?> headers,List<Boolean> isNumber) throws IOException {
      for (Object col:headers) {
            String align =  "-";
          if ( col.toString().indexOf("@|") >= 0 && col.toString().indexOf("|@") > 0 ){
          	out.write(String.format("%" +align + widths[j] + "s",col.toString()));
          } else {
          	out.write(String.format("%"+align + widths[j] + "s",col.toString()));
          }
          out.write(" ");
          j++;
    }
    out.write("\n");
  }
	
	public void writeBoldUnderlineFormat(ScriptRunnerContext ctx,String col, int i) {
  	ctx.write(String.format("%-" + i + "s", col));
  	ctx.write(" ");

	}
	public void printValue(OutputStreamWriter out, int i, String val,boolean isNumber) throws IOException {
      String align = isNumber ? "" : "-";    
	    out.write(String.format("%"+align + i + "s", val));
      out.write(" ");

	}
	
}
