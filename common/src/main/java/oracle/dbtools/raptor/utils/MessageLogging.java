/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A basic interface for logging messages and reporting violations of API contracts. By using MessageLogging, utility classes
 * can separate out the need to report a message from the underlying mechanism for displaying the message to the user.
 * <p>
 * @author jmcginni
 *
 */
public class MessageLogging {
    private static final Logger LOGGER = Logger.getLogger(MessageLogging.class.getName());
    
    private static MessageLogging s_instance = new MessageLogging();
    
    public static MessageLogging getInstance() {
        return s_instance;
    }
    
    public static void setMessageLogger( MessageLogging logger ) {
        s_instance = logger;
    }
    
    protected MessageLogging() {
        
    }
    
    /**
     * Logs a simple text message.
     * @param txt the String to log
     */
    public void log(String txt) {
        LOGGER.log(Level.INFO, txt);
    }
    
    /**
     * Reports a violation of an API contract to the user. API Exceptions are non-fatal messages to be used for debug purposes and generally
     * indicate some failure in implementation.
     * @param message a String with the message to display
     * @param exception A Throwable whose stack represents the point at which the incorrect API usage occurred
     * @param apiName a String containing the name of the API call in question
     */
    public void reportAPIException(String message, Throwable exception, String apiName) {
        // TODO Auto-generated method stub

    }
}
