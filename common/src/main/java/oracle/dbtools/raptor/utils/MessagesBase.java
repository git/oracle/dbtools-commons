/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.awt.Image;
import java.net.URL;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.raptor.LazyIcon;

/**
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=MessagesBase.java"
 *         >Barry McGillin</a>
 * 
 */
public abstract class MessagesBase {
	private static class IconKey {
		private String m_cls;
		private String m_key;

		private IconKey(String cls, String key) {
			m_cls = cls;
			m_key = key;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}

			if (obj instanceof IconKey) {
				return ModelUtil.areEqual(((IconKey) obj).m_cls, m_cls)
				        && ModelUtil.areEqual(((IconKey) obj).m_key, m_key);
			}
			return false;
		}

		@Override
		public int hashCode() {
			return m_cls.hashCode() + m_key.hashCode();
		}

		@Override
		public String toString() {
			return m_cls + ":" + m_key; //$NON-NLS-1$
		}
	}

	private static Map<IconKey, ImageIcon> s_icons = new HashMap<IconKey, ImageIcon>();

	private final ResourceBundle m_bundle;

	protected MessagesBase(String bundleName, ClassLoader cl) {
		m_bundle = ResourceBundle.getBundle(bundleName, Locale.getDefault(), cl, RaptorResourceBundleControl.INSTANCE);
	}

	protected ResourceBundle getResourceBundle() {
		return m_bundle;
	}

	protected String getStringImpl(String key) {
		try {
			return m_bundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	protected Image getImageImpl(String key) {
		ImageIcon imageIcon = getImageIconImpl(key);
		return imageIcon != null ? imageIcon.getImage() : null;
	}

	private ImageIcon getImageIconImpl(String key) {
		String path = m_bundle.getString(key);
		if (path == null || path.length() == 0) {
			return null;
		}
		Class<? extends MessagesBase> cls = getClass();
		IconKey cacheKey = new IconKey(cls.getName(), key);

		// Look up the icon in the cache
		synchronized (s_icons) {
			ImageIcon icon = s_icons.get(cacheKey);
			if (icon != null) {
				return icon;
			}
		}

		URL url = cls.getResource(path);
		ImageIcon icon = null;
		if (url != null) {
			icon = new LazyIcon(url);
			synchronized (s_icons) {
				s_icons.put(cacheKey, icon);
			}
		}

		return icon;
	}

	protected Icon getIconImpl(String key) {
		return getImageIconImpl(key);
	}

	protected String formatImpl(String key, Object... arguments) {
		try {
			return MessageFormat.format(m_bundle.getString(key), arguments);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	public final Integer getIntegerImpl(String key) {
		final String s = getStringImpl(key);
		if ((s.length() == 1) && Character.isLetter(s.charAt(0))) {
			return new Integer(s.charAt(0));
		}
		return Integer.valueOf(s);
	}

}
