/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.Format;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import oracle.dbtools.raptor.nls.FormatType;
import oracle.dbtools.raptor.nls.NLSProvider;
import oracle.jdbc.OracleConnection;

/**
 * @author jscapicc
 *
 */
public class NLSUtils {

  // NLS
  public static final String NLS_LANG = "NLS_LANG"; //$NON-NLS-1$

  public static final String NLS_TERR = "NLS_TERR"; //$NON-NLS-1$

  public static final String NLS_SORT = "NLS_SORT"; //$NON-NLS-1$

  public static final String NLS_COMP = "NLS_COMP"; //$NON-NLS-1$

  public static final String NLS_DATE_LANG = "NLS_DATE_LANG"; //$NON-NLS-1$
  // public static final String NLS_CALENDAR = "NLS_CALENDAR";

  public static final String NLS_DATE_FORM = "NLS_DATE_FORM"; //$NON-NLS-1$

  public static final String NLS_TS_FORM = "NLS_TS_FORM"; //$NON-NLS-1$

  public static final String NLS_TS_TZ_FORM = "NLS_TS_TZ_FORM"; //$NON-NLS-1$

  public static final String NLS_DEC_SEP = "NLS_DEC_SEP"; //$NON-NLS-1$

  public static final String NLS_GRP_SEP = "NLS_GRP_SEP"; //$NON-NLS-1$

  public static final String NLS_CURR = "NLS_CURR"; //$NON-NLS-1$

  public static final String NLS_ISO_CURR = "NLS_ISO_CURR"; //$NON-NLS-1$

  public static final String NLS_LENGTH = "NLS_LENGTH"; //$NON-NLS-1$

  // private static String sep;
  public static void setNlsFromPrefs(Connection dbCon) {
    NLSProvider.getProvider(dbCon).setNlsFromPrefs();
  }

  /**
   * @param conn
   * @param obj
   * @return
   */
  public static Object getValue(Connection conn, Object obj) {
    return NLSProvider.getProvider(conn).getValue(obj);
  }

  /**
   * @param conn
   * @param obj
   * @param formatType
   * @return
   */
  public static Object getValue(Connection conn, Object obj, FormatType formatType) {
    if (formatType == null) {
      return NLSProvider.getProvider(conn).getValue(obj);
    } else {
      return NLSProvider.getProvider(conn).getValue(obj, formatType);
    }
  }

  /**
   * @param conn
   * @param map
   */
  public static void updateDefaults(Connection conn, Map<String, String> map) {
    NLSProvider.getProvider(conn).updateDefaults(map);
  }

  /**
   * @param conn
   */
  public static void populateNLS(Connection conn) {
    NLSProvider.getProvider(conn).populateNLS();
  }

  /**
   * @param conn
   * @param sqlType
   * @param formatType
   * @return
   */
  public static Format getFormat(Connection conn, int sqlType, FormatType formatType) {
    return NLSProvider.getProvider(conn).getFormat(sqlType, formatType);
  }

  /**
   * getDateFormat - allows specification of the formatType on the request.
   * 
   * @param conn
   * @param formatType
   * @return
   */
  public static String getDateFormat(Connection conn, FormatType formatType) {
    return NLSProvider.getProvider(conn).getDateFormat(formatType);
  }

  /**
   * @param conn
   * @param formatType
   * @return
   */
  public static String getTimeStampWithTimeZoneFormat(Connection conn, FormatType formatType) {
    return NLSProvider.getProvider(conn).getTimeStampWithTimeZoneFormat(formatType);
  }

  /**
   * @param conn
   * @param formatType
   * @return
   */
  public static String getTimeStampFormat(Connection conn, FormatType formatType) {
    return NLSProvider.getProvider(conn).getTimeStampFormat(formatType);
  }

  /**
   * @param conn
   * @param sqlType
   * @return
   */
  public static Format getFormat(Connection conn, int sqlType) {
    return NLSProvider.getProvider(conn).getFormat(sqlType);
  }

  /**
   * @param conn
   * @return
   */
  public static String getDateFormat(Connection conn) {
    return getDateFormat(conn, FormatType.PREFERRED);
  }

  /**
   * @param conn
   * @return
   */
  public static String getTimeStampWithTimeZoneFormat(Connection conn) {
    return getTimeStampWithTimeZoneFormat(conn, FormatType.PREFERRED);
  }

  /**
   * @param conn
   * @return
   */
  public static String getTimeStampFormat(Connection conn) {
    return getTimeStampFormat(conn, FormatType.PREFERRED);
  }

  /**
   * @param conn
   * @return
   */
  public static char getDecimalSeparator(Connection conn) {
    return NLSProvider.getProvider(conn).getDecimalSeparator();
  }

  /**
   * @param conn
   * @param obj
   * @return
   */
  public static String toSql(Connection conn, Object obj) {
    return NLSProvider.getProvider(conn).getSQL(obj);
  }

  /**
   * @param conn
   * @param obj
   * @return
   */
  public static String format(Connection conn, Object obj) {
    return NLSProvider.getProvider(conn).format(obj);
  }

  /**
   * @param conn
   * @return
   */
  public static String getDBCharset(Connection conn) {
    return NLSProvider.getProvider(conn).getDBCharset();
  }

  /**
   * @param conn
   * @return
   */
  public static TimeZone getDatabaseTimeZone(Connection conn) {
    return NLSProvider.getProvider(conn).getDatabaseTimeZone();
  }

  /**
   * @param conn
   * @return
   */
  public static TimeZone getSessionTimeZone(Connection conn) {
    return NLSProvider.getProvider(conn).getSessionTimeZone();
  }

  /**
   * @param conn
   * @return
   */
  public static String getNullDisplay(Connection conn) {
    return NLSProvider.getProvider(conn).getNullDisplay();
  }

  /**
   * @param conn
   * @param obj
   * @return
   */
  public static String yieldNullDisplay(Connection conn, Object obj) {
    return NLSProvider.getProvider(conn).yieldNullDisplay(obj);
  }

  /**
   * @param conn
   * @param obj
   * @return
   */
  public static boolean isNullOrEquiv(Connection conn, Object obj) {
    return NLSProvider.getProvider(conn).isNullOrEquiv(obj);
  }

  /**
   * @param conn
   * @param obj
   * @return
   */
  public static boolean isNullDisplay(Connection conn, Object obj) {
    return NLSProvider.getProvider(conn).isNullDisplay(obj);
  }

  /**
   * @param conn
   * @param nullDisplay
   * @return
   */
  public static void setNullDisplay(Connection conn, String nullDisplay) {
    NLSProvider.getProvider(conn).setNullDisplay(nullDisplay);
  }
  
	/**
	 * 
	 * @param conn
	 * @return
	 */
	public static Locale getNLSSessionLocale(Connection conn) {
		Properties ssProp;
		Locale locale = Locale.getDefault();
		//RESTJDBC driver is not an instance of oracle.jdbc.internal.OracleConnection so return the default locale
    if(!(conn instanceof oracle.jdbc.internal.OracleConnection)){
      return locale;
    }
		try {
			ssProp = ((oracle.jdbc.internal.OracleConnection) conn).getServerSessionInfo();
			String nlsLang = (String) ssProp.get("AUTH_NLS_LXLAN");
			if (nlsLang != null) {
				if (nlsLang.equalsIgnoreCase("GERMAN")) {
					// _de
					locale = Locale.GERMAN;
				} else if (nlsLang.equalsIgnoreCase("SPANISH")) {
					// _es
					locale = new Locale("es", "ES");
				} else if (nlsLang.equalsIgnoreCase("FRENCH")) {
					// _fr
					locale = Locale.FRENCH;
				} else if (nlsLang.equalsIgnoreCase("ITALIAN")) {
					// _it
					locale = Locale.ITALIAN;
				} else if (nlsLang.equalsIgnoreCase("JAPANESE")) {
					// _ja
					locale = Locale.JAPANESE;
				} else if (nlsLang.equalsIgnoreCase("KOREAN")) {
					// _ko
					locale = Locale.KOREAN;
				} else if (nlsLang.equalsIgnoreCase("BRAZILIAN PORTUGUESE")) {
					// _pt_BR
					locale = new Locale("pt", "BR");
				} else if (nlsLang.equalsIgnoreCase("SIMPLIFIED CHINESE")) {
					// _zh_CN
					locale = Locale.CHINESE;
				} else if (nlsLang.equalsIgnoreCase("TRADITIONAL CHINESE")) {
					// _zh_TW
					locale = Locale.TAIWAN;
				} else if (nlsLang.equalsIgnoreCase("AMERICAN")) {
					// _us
					locale = Locale.US;
				} else if (nlsLang.equalsIgnoreCase("ENGLISH")) {
					// _uk
					locale = Locale.UK;
				}
			}
		} catch (SQLException e) {
			locale = Locale.getDefault();
		}
		return locale;
	}
}
