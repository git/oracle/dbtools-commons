/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.HashMap;

import oracle.dbtools.util.Closeables;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;


public final class ResolvedDBObject {

  private String _owner;

  private String _part1;

  private String _part2;

  private String _dblink;

  private int _type;

  private String _filter;

  private BigDecimal _id;

  private boolean bad = false;
  private static String error = "";
  
  private ResolvedDBObject(String owner, String part1, String part2, String dblink, int type, String filter, BigDecimal objId) {
    _owner = owner;
    _part1 = part1;
    _part2 = part2;
    _dblink = dblink;
    _type = type;
    _filter = filter;
    _id = objId;
  }
  
  private ResolvedDBObject(String errorMessage) {
	  bad=true;
	  error= errorMessage;
  }

  public BigDecimal getId(){
    return _id;
  }
  
  public String getName() {
    if (_type == 2 || _type == 4) {
      // table/view
      return _part1;
    } else if (_type == 7 || _type == 8) {
      // proc/function
      return _part2;
    } else if (_type == 9) {
      return _part1;
    } 
    return _part1;
  }

  public int getType() {
    return _type;
  }

  public String getTypeName(){
    switch (_type) {
    case 1:
      return "TABLE";
    case 2:
      return "TABLE";
    case 4:
      return "VIEW";
    case 5:
      return "SYNONYM";
    case 7:
      return "PROCEDURE";
    case 8:
      return "FUNCTION";
    case 9:
      return "PACKAGE";

    }
    return null;
  }
  public String getFilter() {
    return _filter;
  }

  public String getOwner() {
    return _owner;
  }

  public String getPart1() {
    return _part1;
  }

  public String getPart2() {
    return _part2;
  }

  public String getDBlink() {
    return _dblink;
  }

  public HashMap<String, ?> getBinds() {
    HashMap<String, String> binds = new HashMap<String, String>();
    binds.put("OBJECT_OWNER", _owner);
    binds.put("FILTER", _filter);
    if (_type == 2 || _type == 4) {
      // table/view
      binds.put("OBJECT_NAME", _part1);
    } else if (_type == 7 || _type == 8) {
      // proc/function
      binds.put("OBJECT_NAME", _part2);
    } else if (_type == 9) {
      binds.put("OBJECT_NAME", _part1);
      binds.put("FILTER", _part2);
    }
    return binds;
  }
  
  
  
  /**
   * 
   * 
   * 
   */
  

  /*
   * This is easier than querying the DD for name resolution
   */
  private static final String PLSQL_DBMS_UTILITY_RESOLVE = "begin " + 
      " DBMS_UTILITY.NAME_RESOLVE (" + 
      "   name          => :NAME, " + 
      "   context       => :CTX," + 
      "   schema        => :SCHEMA, " + 
      "   part1         => :PART1, " + 
      "   part2         => :PART2," + 
      "   dblink        => :DBLINK, " + 
      "   part1_type    => :PART1_TYPE, " + 
      "   object_number => :NUM);" + 
      "end;";

  private static final String QUERY_OWNED_SYNONYM = "select owner,table_name,table_owner from all_synonyms where synonym_name = :NAME and owner = :OWNER";
  
  private static final String QUERY_SYNONYM = "select owner,table_name,table_owner from all_synonyms where synonym_name = :NAME";
  
  private static final String PLSQL_DBMS_UTILITY_TOKENIZE = "begin " + 
      " DBMS_UTILITY.NAME_TOKENIZE (" + 
      "   name   => :NAME, " + 
      "   a      => :A, " + 
      "   b      => :B, " + 
      "   c      => :C, " + 
      "   dblink => :DBLINK, " + 
      "   nextpos => :NEXTPOS);" +
      "end;";

  public static ResolvedDBObject resolveDBObject(Connection conn,String rawName,String filter){
    if (conn == null) {
        return new ResolvedDBObject("no connection");
    }
    ResolvedDBObject dbObj = null;
        if (conn != null) {
            for (int i = 0; i < 11 && dbObj == null ; i++) {
              if (dbObj == null) {
                dbObj = getResolvedObject(conn, rawName, i, filter);
              }
              if (i == 9 && rawName.contains(".")) {
                filter = rawName.substring(rawName.lastIndexOf(".") + 1);
                rawName = rawName.substring(0, rawName.lastIndexOf("."));
                i = -1;
              }
            }
        }

    if ( dbObj == null ){
      dbObj = getSynonymnReference(conn, rawName);
    }
    if (dbObj == null && rawName.contains("@")) {
    	//We are looking at a database link
    	dbObj = getResolvedObjectlink(conn, rawName);
    }
    return dbObj;
  }
  
  private static ResolvedDBObject getSynonymnReference(Connection conn, String rawName) {
      ResolvedDBObject dbObj = null;
      
      String synonymName = null;
      String synonymOwner = null;
      
      OracleCallableStatement call = null;
      try {
          call = (OracleCallableStatement) conn.prepareCall(PLSQL_DBMS_UTILITY_TOKENIZE);
          call.registerOutParameter(2, Types.VARCHAR);
          call.registerOutParameter(3, Types.VARCHAR);
          call.registerOutParameter(4, Types.VARCHAR);
          call.registerOutParameter(5, Types.VARCHAR);
          call.registerOutParameter(6, Types.NUMERIC);

          call.setString(1, rawName);
          call.execute();

          String a = call.getString(2);
          String b = call.getString(3);
          String c = call.getString(4);
          String dblink = call.getString(5);
          int nextpos = call.getInt(6);
          if (b == null) {
            synonymName = a;
          } else {
            synonymOwner = a;
            synonymName = b;
          }
      } catch (SQLException e) {
          synonymName = rawName;
      } finally{
          if (call != null) {
            try {
              call.close();
            } catch (SQLException e) {
            }
          }
      }
      try {
        OraclePreparedStatement stmt = null;
        OracleResultSet rset = null;
        try {
          if (synonymOwner != null) {
              stmt = (OraclePreparedStatement)conn.prepareStatement(QUERY_OWNED_SYNONYM);
              stmt.setStringAtName("OWNER", synonymOwner);
          } else {
              stmt = (OraclePreparedStatement)conn.prepareStatement(QUERY_SYNONYM);
          }
          stmt.setStringAtName("NAME", synonymName);
          rset = (OracleResultSet)stmt.executeQuery();
          if (rset.next()) {
                synonymOwner = rset.getString(1);
                String objectName = rset.getString(2);
                String objectOwner = rset.getString(3);
                dbObj = new ResolvedDBObject(synonymOwner, synonymName, "\"" + objectOwner + "\".\"" + objectName + "\"", null, 5, null, null);
          }
        } finally {          
            if (rset != null) {
                rset.close();
            }
            stmt.close();
        }
      } catch (SQLException ex) {
          ex.printStackTrace();
      }
      return dbObj;
  }
  private static ResolvedDBObject getResolvedObjectlink(Connection conn, String rawName) {
	    OracleCallableStatement call = null;
	    try {
	    	String link=rawName;
			String dblink = "";
			String object = "";
			String schema = "";
	    	
			if (link.indexOf('@') > -1) {
				object = link.substring(0, link.indexOf("@"));
			} else
				object = "";
			if (object.indexOf('.') > -1) {
				schema = object.substring(0, object.indexOf('.'));
				object = object.substring(object.indexOf('.') + 1, object.length()); 
			} else {
				schema = "";
			}
			if (link.indexOf("@")>-1) {
			dblink = link.substring(link.indexOf("@")+1,link.length());
			} else {
				dblink="";
			}

  		  OraclePreparedStatement checkStmt = (OraclePreparedStatement)conn.prepareStatement("select username,owner from all_db_links where UPPER(db_link) like UPPER(:LINK)||'%'");
	      checkStmt.setStringAtName("LINK", dblink.toUpperCase());
	      ResultSet rset = checkStmt.executeQuery();
	      rset.next();
	      if (rset.getString(1)!=null && !rset.getString(1).trim().equals("")) {
	    	  schema=rset.getString(1);
	      }
	      rset.close();
	      checkStmt.close();


	      checkStmt = (OraclePreparedStatement)conn.prepareStatement(MessageFormat.format("select object_type from all_objects{0} where UPPER(object_name) = UPPER(:NAME) and UPPER(owner) = UPPER(:OWNER)", new Object[] {"@"+dblink.toUpperCase()}));
	      checkStmt.setStringAtName("NAME", object);
	      checkStmt.setStringAtName("OWNER", schema);
	      rset = checkStmt.executeQuery();
	      rset.next();
	      if (rset.getString(1).length()== 0) {
	    	  return null;
		} else {
			final String type = rset.getString(1);
			int context=0;
			switch (type) {
			case "TABLE":
				context=1;
				break;
			case "VIEW":
				context=4;
				break;
			case "FUNCTION":
				context=8;
				break;
			case "PROCEDURE":
				context=7;
				break;
			case "PACKAGE":
				context=9;
			break;
			case "SYNONYM":
				context=5;
			break;
			default:
				context=1;
				break;
			}
			return new ResolvedDBObject(schema, object, "", dblink, context, "",null);			
		}
	    } catch (SQLException e) {
	      // this means not a table or not a proc
	      if (e.getErrorCode() == 6563) {
	          return new ResolvedDBObject("Object Not Found: " + rawName);
	      }
	      if (e.getErrorCode() == 931) { // missing identifier
	          return new ResolvedDBObject("invalid object name");
	      }
	      if (e.getErrorCode() != 4047 && e.getErrorCode() != 6564) {
	          ResolvedDBObject dbObj = new ResolvedDBObject(e.getMessage());
	          return dbObj;
	      }
	    } finally{
	      if (call != null) {
	        try {
	          call.close();
	        } catch (SQLException e) {
	        }
	      }
	    }

	    return null;
	  }

  private static ResolvedDBObject getResolvedObject(Connection conn, String rawName, int ctx, String filter) {
    OracleCallableStatement call = null;
    PreparedStatement checkStmt = null;
    ResultSet rset = null;
    try {
      call = (OracleCallableStatement) conn.prepareCall(PLSQL_DBMS_UTILITY_RESOLVE);
      call.registerOutParameter(3, Types.VARCHAR);
      call.registerOutParameter(4, Types.VARCHAR);
      call.registerOutParameter(5, Types.VARCHAR);
      call.registerOutParameter(6, Types.VARCHAR);
      call.registerOutParameter(7, Types.NUMERIC);
      call.registerOutParameter(8, Types.NUMERIC);
      
      call.setString(1, rawName);
      call.setInt(2, ctx);// table
      call.execute();

      String schema = call.getString(3);
      String part1 = call.getString(4); // package if a package
      String part2 = call.getString(5); // package proc or plain proc
      String dblink = call.getString(6); // package proc or plain proc
      int type = call.getInt(7); // package proc or plain proc
      BigDecimal objId = call.getBigDecimal(8); // objectid

      ResolvedDBObject dbObj = new ResolvedDBObject(schema, part1, part2, dblink, type, filter,objId);

      checkStmt = conn.prepareStatement("select count(*) from all_objects where object_name = :NAME and owner = :OWNER");
      checkStmt.setString(1, dbObj.getName());
      checkStmt.setString(2, dbObj.getOwner());
      rset = checkStmt.executeQuery();
      rset.next();
      if (rset.getInt(1) == 0) {
          dbObj = null;
      } 

      return dbObj;
    } catch (SQLException e) {
      // this means not a table or not a proc
      if (e.getErrorCode() == 6563) {
          return new ResolvedDBObject("Object Not Found: " + rawName);
      }
      if (e.getErrorCode() == 931) { // missing identifier
          return new ResolvedDBObject("invalid object name");
      }
      if (e.getErrorCode() != 4047 && e.getErrorCode() != 6564) {
          ResolvedDBObject dbObj = new ResolvedDBObject(e.getMessage());
          return dbObj;
      }
    } finally{
      Closeables.close(call,checkStmt,rset);
    }

    return null;
  }

/**
 * @return the bad
 */
public final boolean isBad() {
	return bad;
}

/**
 * @return the error
 */
public final String getError() {
	return error;
}
}