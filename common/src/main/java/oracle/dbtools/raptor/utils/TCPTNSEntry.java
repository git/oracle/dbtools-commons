/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=TCPTNSEntry.java">Barry McGillin</a> 
 *
 */
public class TCPTNSEntry 
{
    private static final Logger LOGGER = Logger.getLogger( TCPTNSEntry.class.getName() );
    
  //Eg. jdbc:oracle:thin:@<mc-name>:<port-no>:<sid>
  public static int SPEC_SHORT_FORMAT = 1;
//Eg. jdbc:oracle:thin:@(description=(address=(host=<mc-name>)(protocol=tcp)(port=<port-no>))(connect_data=(sid=<sid>)))
  public static int SPEC_LONG_FORMAT = 2;
  
  private int spec_format = 0;
  
  /** url parts for short format - START **/
  private String name = null;

  private String hostname = null;

  private String portno = null;

  private String sid = null;
  
  private String servicename = null;
  /** url parts for short format - END **/
  private String desc = null;

  /*
   * Item format is entryname=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=hostname)(PORT=portno))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sid)))
   */
  public TCPTNSEntry(String tnsName,String descriptor){
      spec_format = SPEC_LONG_FORMAT;
      name = tnsName;
      desc = descriptor;
      parseDescriptor();
  }
  
  TCPTNSEntry(String item, int specFormat)
  {
	this.spec_format = specFormat;
    //if (copy.indexOf(SPEC_PROTOCOL) < 0)
      //throw new IllegalArgumentException("Only TCP protocol supported by this class");
    populate( item );
  }
  
  protected void populate(String item){
    int end = item.indexOf( "=" ); //$NON-NLS-1$
    name = item.substring(0, end);
    
    desc = item.substring( end + 1 );
    
    parseDescriptor();
  }

    private static final Pattern PORT_PATTERN = Pattern.compile( "\\(\\s*PORT\\s*=\\s*([^\\s)]+)\\s*\\)" ); //$NON-NLS-1$
    private static final Pattern HOST_PATTERN = Pattern.compile( "\\(\\s*HOST\\s*=\\s*([^\\s)]+)\\s*\\)" ); //$NON-NLS-1$
    private static final Pattern SID_PATTERN = Pattern.compile( "\\(\\s*SID\\s*=\\s*([^\\s)]+)\\s*\\)" ); //$NON-NLS-1$
    private static final Pattern SVCNAME_PATTERN = Pattern.compile( "\\(\\s*SERVICE_NAME\\s*=\\s*([^\\s)]+)\\s*\\)" ); //$NON-NLS-1$

    private void parseDescriptor()
    {
      String copy = desc.toUpperCase();

        Matcher match = PORT_PATTERN.matcher( copy );
        if ( match.find() )
        {
            portno = desc.substring( match.start( 1 ), match.end( 1 ) );
        }

        match = HOST_PATTERN.matcher( copy );
        if ( match.find() )
        {
            hostname = desc.substring( match.start( 1 ), match.end( 1 ) );
        }

        match = SID_PATTERN.matcher( copy );
        if ( match.find() )
        {
            sid = desc.substring( match.start( 1 ), match.end( 1 ) );
        }

        match = SVCNAME_PATTERN.matcher( copy );
        if ( match.find() )
        {
            servicename = desc.substring( match.start( 1 ), match.end( 1 ) );
        }
    }

  public String getJDBCUrl()
  {
	String url = null;
	if(spec_format == SPEC_SHORT_FORMAT)
	{
    if(servicename != null)
      url = "jdbc:oracle:thin:@\\\\" + hostname + ":" + portno + "\\" + servicename; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    else if(sid != null)
      url = "jdbc:oracle:thin:@" + hostname + ":" + portno + ":" + sid; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
	else if(spec_format == SPEC_LONG_FORMAT)
	{
		url = "jdbc:oracle:thin:@" + desc; //$NON-NLS-1$
	}
    return url;
  }
  
  public String getSpecUrl()
  {
  String specUrl = null;
  if(spec_format == SPEC_SHORT_FORMAT)
  {
    if(servicename != null)
      specUrl = "\\\\" + hostname + ":" + portno + "\\" + servicename; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    else if(sid != null)
      specUrl = hostname + ":" + portno + ":" + sid; //$NON-NLS-1$ //$NON-NLS-2$
  }
  else if(spec_format == SPEC_LONG_FORMAT)
  {
    specUrl = desc;
  }
    return specUrl;
  }

  public static void printUrl(String[] items)
  {
    for(int i = 0; i < items.length; i++)
    {
      TCPTNSEntry te = new TCPTNSEntry(
    		  items[i].replaceAll("\\s", ""),  //$NON-NLS-1$ //$NON-NLS-2$
    		  TCPTNSEntry.SPEC_LONG_FORMAT
    		  );
      LOGGER.info(te.getJDBCUrl());
    }
  }

  	public static void main(String[] args)
  	{
  	    String[] items = {
  	    	    "ORCL=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=orcl)))", //$NON-NLS-1$
  	    	    "spx2tcp  = (description= (address_list=(address=(protocol=spx)(service=orasrvc1))(address=(protocol=tcp)(port=1580)(host=spcstn)))(connect_data=(sid=cman))(source_route=yes))", //$NON-NLS-1$
  	    	    "tcp_mv713 =\n" +  //$NON-NLS-1$
  	    	    "  (DESCRIPTION =\n" +  //$NON-NLS-1$
  	    	    "    (ADDRESS=      (PROTOCOL=TCP) (HOST=hostname) (PORT=1521))\n" +  //$NON-NLS-1$
  	    	    "    (CONNECT_DATA= (SID=MV713))\n" +  //$NON-NLS-1$
  	    	    "  )", //$NON-NLS-1$
  	    	    "mv713 = \n" +  //$NON-NLS-1$
  	    	    "  (DESCRIPTION =\n" +  //$NON-NLS-1$
  	    	    "    (ADDRESS=      (PROTOCOL=IPC) (KEY=700))\n" +  //$NON-NLS-1$
  	    	    "    (CONNECT_DATA= (SID=MV713)) \n" +  //$NON-NLS-1$
  	    	    "  )", //$NON-NLS-1$
  	    	    "EXTPROC_CONNECTION_DATA=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=IPC)(KEY=EXTPROC1)))", //$NON-NLS-1$

  	    	    };
  	    
  	    try
  	    {
  	    	printUrl(items);
  	    }
  	    catch(Exception e)
  	    {
  	    	LOGGER.severe(e.getMessage());
  	    }
  	    
  	    
  	}
  public String getName()
  {
    return name;
  }

  public String getHostname()
  {
    return hostname;
  }

  public String getPortno()
  {
    return portno;
  }

  public String getSid()
  {
    return sid;
  }

  public String getServicename()
  {
    return servicename;
  }
  
  public String toString(){
	  return name + "=" + getJDBCUrl(); //$NON-NLS-1$
  }

  public void setName(String name){
	  this.name =name; 
  }
  public String getDescriptor(){
	  return desc;
  }  
}
