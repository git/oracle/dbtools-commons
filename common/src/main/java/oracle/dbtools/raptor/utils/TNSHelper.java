/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.PlatformUtils;
import oracle.dbtools.common.utils.UtilResources;
import oracle.dbtools.logging.Timer;

/**
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=TNSHelper.java"
 *         >Barry McGillin</a>
 * 
 */
public class TNSHelper {
	public static final String ENV_OH = "ORACLE_HOME"; //$NON-NLS-1$
	public static final String ENV_TA = "TNS_ADMIN"; //$NON-NLS-1$
	public static final String ENV_LA = "LDAP_ADMIN"; //$NON-NLS-1$
	private static Map<String, Long> lastModified = new HashMap<String, Long>();
	private static Object LOCK = new Object();
	private static final String NETWORK_ADMIN_PATH = File.separator + "network" //$NON-NLS-1$
	        + File.separator + "admin"; //$NON-NLS-1$
	public static final String FILE_TNS = NETWORK_ADMIN_PATH + File.separator + "tnsnames.ora"; //$NON-NLS-1$ 
	private static ArrayList<TCPTNSEntry> s_entries = null;
	private static Logger logger = Logger.getLogger("oracle.dbtools.raptor.utils"); //$NON-NLS-1$
	private static LinkedHashMap<String, String> s_paths = new LinkedHashMap<String,String>();
	public static LinkedHashMap<String, String> getTNSLookupLocations() {
		return s_paths;
	}
	public static void forceDirty() {
		lastModified = new HashMap<String, Long>();
	}

	private static FilenameFilter filter = new FilenameFilter() {
		public boolean accept(File dir, String name) {
			return name.equalsIgnoreCase(".tnsnames") || name.toLowerCase().startsWith("tnsnames."); //$NON-NLS-1$ //$NON-NLS-2$
		}
	};

	private static class NotDirtyException extends Exception {
	}

	private static boolean checkForTns(String dir) {
		logger.info(UtilResources.getString(UtilResources.TNSHelper_12) + dir);
		boolean ret = false;
		File d = new File(dir);
		if (d.exists() && d.listFiles(filter).length > 0)
			ret = true;
		return ret;
	}


	private static File getTNSFileContent(boolean forceReload) throws IOException, NotDirtyException {
		logger.finest("getTNSFileContent - START"); //$NON-NLS-1$
		String oh = System.getenv().get(ENV_OH);
		String ta = System.getenv().get(ENV_TA);
		String driverHome = System.getProperty("jdbc.driver.home"); // check for //$NON-NLS-1$
		// our driver home set by them driver locator
		String user = System.getProperty("user.home"); //$NON-NLS-1$
		String dir = null;
		ProvideTnsnamesDir pFile = getProvideTnsnamesDir();
		if (pFile != null) {
			File possibleTnsnames = pFile.getTnsnamesDir();
			if ((possibleTnsnames != null)) {
				try {
					s_paths.put("Third Party Path",possibleTnsnames.getAbsolutePath());
					String checkThePref = possibleTnsnames.getAbsolutePath();
					if (checkForTns(checkThePref)) {
						dir = checkThePref;
					}
				} catch (Throwable e) {
					dir = null;// keep trying other options if tnsnames.ora
					           // selected by pref does not exist?
				}
			}
		}
		// Get $USER/.tnsnames.ora
		s_paths.put("USER Home dir",user);
		if ((dir == null) && checkForTns(user)) {
			// Get $TNS_ADMIN/tnsnames.ora
			dir = user;
		}
		boolean isWindows = PlatformUtils.isWindows();
		if (!isWindows && dir == null && checkForTns("/etc")) { //$NON-NLS-1$
			// Get /etc/tnsnames.ora
			s_paths.put("/etc","/etc");
			dir = "/etc"; //$NON-NLS-1$
		}
		if (ta != null) {
			s_paths.put("TNS_ADMIN",ta);
			if (dir == null && checkForTns(ta))
				dir = ta;
		}
		String filePath = NETWORK_ADMIN_PATH;
		if (driverHome != null) {
			s_paths.put("Driver path",driverHome + filePath);
			if (dir == null && checkForTns(driverHome + filePath))
				dir = driverHome + filePath;
		}
		if (oh != null) {
			s_paths.put("ORACLE_HOME",oh + filePath);
			if (dir == null && checkForTns(oh + filePath))
				dir = oh + filePath;
		}
		if (dir == null && isWindows) {
			// Get Registry Key for OH
			oh = null;
			String tnsKey = null;
			// ORACLE_HOME_KEY
			logger.info(UtilResources.getString(UtilResources.TNSHelper_20));
			String key = "SOFTWARE\\ORACLE"; //$NON-NLS-1$
			try {
				List<String> oracle_keys = WindowsUtility.readStringSubKeys(WindowsUtility.HKEY_LOCAL_MACHINE, key);
				String homeKey = null;
				for (String oracle_key : oracle_keys) {
					String temp = key + "\\" + oracle_key; //$NON-NLS-1$
					homeKey = WindowsUtility.readString(WindowsUtility.HKEY_LOCAL_MACHINE, temp, "ORACLE_HOME_KEY"); //$NON-NLS-1$
					if (homeKey != null) {
						logger.info(UtilResources.getString(UtilResources.TNSHelper_24) + homeKey);
						tnsKey = WindowsUtility.readString(WindowsUtility.HKEY_LOCAL_MACHINE, temp, "TNS_ADMIN"); //$NON-NLS-1$
						logger.info(UtilResources.getString(UtilResources.TNSHelper_26) + tnsKey);
						break;
					}
				}
				if (homeKey != null) {
					oh = WindowsUtility.readString(WindowsUtility.HKEY_LOCAL_MACHINE, homeKey, "ORACLE_HOME"); //$NON-NLS-1$
					logger.info(UtilResources.getString(UtilResources.TNSHelper_29) + oh);
				}
			} catch (Exception e) {
				logger.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
			}
			if (oh != null && tnsKey == null) {
				s_paths.put("ORACLE_HOME",oh+filePath);
				dir = oh + filePath;
			}
			if (dir == null && tnsKey != null) {
				s_paths.put("TNS_ADMIN",tnsKey);
				dir = tnsKey;
			}
		}
		if (dir == null)
			return null;
		if (isDirty(dir)) {
			// clear the entries as we will be re reading the tns file to get
			// them and dont want duplicates
			s_entries = new ArrayList<TCPTNSEntry>();
		} else {
			throw new NotDirtyException();
		}
		logger.finest("getTNSFileContent - END"); //$NON-NLS-1$
		s_paths.put("Actual",dir);
		return new File(dir);
	}

	/**
	 * 
	 * getOracleHome
	 * 
	 * @return TNSHelper
	 */
	public static String getOracleHome() {
		String oh = System.getenv().get(ENV_OH);
		if (oh == null && PlatformUtils.isWindows()) {
			String key = "SOFTWARE\\ORACLE"; //$NON-NLS-1$
			try {
				List<String> oracle_keys = WindowsUtility.readStringSubKeys(WindowsUtility.HKEY_LOCAL_MACHINE, key);
				String homeKey = null;
				if (oracle_keys != null && !oracle_keys.isEmpty()) {
					for (String oracle_key : oracle_keys) {
						String temp = key + "\\" + oracle_key; //$NON-NLS-1$
						homeKey = WindowsUtility.readString(WindowsUtility.HKEY_LOCAL_MACHINE, temp, "ORACLE_HOME_KEY"); //$NON-NLS-1$
						logger.info(UtilResources.getString(UtilResources.TNSHelper_24) + homeKey);
					}
					if (homeKey != null) {
						oh = WindowsUtility.readString(WindowsUtility.HKEY_LOCAL_MACHINE, homeKey, "ORACLE_HOME"); //$NON-NLS-1$
						logger.info(UtilResources.getString(UtilResources.TNSHelper_29) + oh);
					}
				}
			} catch (Exception e) {
				// doneill
				e.printStackTrace();
			}
		}
		return oh;
	}

	/**
	 * 
	 * isDirty
	 * 
	 * @param dir
	 * @return TNSHelper
	 */
	public static boolean isDirty(String dir) {
		boolean ret = false;
		File d = new File(dir);
		for (File f : d.listFiles(filter)) {
			if (lastModified.get(f.toString()) == null
			        || (lastModified.get(f.toString())).longValue() != f.lastModified())
				ret = true;
		}
		return ret;
	}

	/**
	 * 
	 * updateTimeStamps
	 * 
	 * @param dir
	 *            TNSHelper
	 */
	public static void updateTimeStamps(String dir) {
		File d = new File(dir);
		for (File f : d.listFiles(filter)) {
			lastModified.put(f.toString(), new Long(f.lastModified()));
		}
	}

	/**
	 * 
	 * getEntry
	 * 
	 * @param name
	 * @return TNSHelper
	 */
	public static TCPTNSEntry getEntry(String name) {
		ArrayList<TCPTNSEntry> entries = TNSHelper.getTNSEntries();
		for (TCPTNSEntry entry : entries) {
			if (entry.getName().equalsIgnoreCase(name)) {
				return entry;
			}
		}
		return null;
	}

	private static ArrayList<TCPTNSEntry> getTNSEntries(File fileContent) throws IOException {
		logger.finest("getTNSItems - START"); //$NON-NLS-1$
		ArrayList<TCPTNSEntry> result = new ArrayList<TCPTNSEntry>();
		if (fileContent != null && fileContent.exists() && fileContent.isDirectory()) {
			for (File f : fileContent.listFiles(filter)) {
				result.addAll(getTNSEntries(f));
			}
			updateTimeStamps(fileContent.toString());
		} else if (fileContent != null && fileContent.exists()) {
			List<TCPTNSEntry> lstItems = parseTnsFile(fileContent);
			updateTimeStamps(fileContent.getParent());
			result.addAll(lstItems);
		}
		return result;
	}

	public static List<TCPTNSEntry> parseTnsFile(File fileContent)
			throws FileNotFoundException, IOException {
		ArrayList<TCPTNSEntry> lstItems = new ArrayList<TCPTNSEntry>();
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileContent)));
		logger.finest("getTNSItems - OPEN: " + fileContent.getPath()); //$NON-NLS-1$
		/*
		 * while (true) { int endOfItem = fileContent.indexOf(ITEM_END,
		 * startOfItem) + ITEM_END.length(); if (endOfItem >=
		 * fileContent.length()) break;
		 * lstItems.add(fileContent.substring(startOfItem, endOfItem));
		 * startOfItem = endOfItem; }
		 */
		// TODO: need to optimize this loop
		int bracketCount = 0;
		String buffer = null;
		StringBuilder currentEntry = new StringBuilder();
		TCPTNSEntry entry = null;
		String name = null;
		String desc = null;
		while ((buffer = in.readLine()) != null) {
			// remove trailing comments
			if (buffer.indexOf("#") > 0) //$NON-NLS-1$
				buffer = buffer.substring(0, buffer.indexOf("#")); //$NON-NLS-1$
			if (buffer.indexOf('#') != 0) {
				// count open vs. close.probably count change this to do a
				// mod
				for (int i = 0; i < buffer.length(); i++) {
					if (buffer.charAt(i) == '(')
						bracketCount++;
					else if (buffer.charAt(i) == ')')
						bracketCount--;
				}
				currentEntry.append(buffer);
				if (currentEntry.indexOf("=") > 0) { //$NON-NLS-1$
					String maybeName = currentEntry.substring(0, currentEntry.indexOf("=")).trim(); //$NON-NLS-1$
					int binName = 0;
					for (int i = 0; i < maybeName.length(); i++) {
						if (maybeName.charAt(i) == '(')
							binName++;
						else if (maybeName.charAt(i) == ')')
							binName--;
					}
					if (bracketCount == binName) {
						try {
							name = currentEntry.substring(0, currentEntry.indexOf("=")).trim(); //$NON-NLS-1$
							desc = currentEntry.substring(currentEntry.indexOf("=") + 1).trim(); //$NON-NLS-1$
							if (name.trim().toLowerCase().equals("ifile")) { //$NON-NLS-1$
								try {
									String cwd = fileContent.getParent() + File.separator;
									File iFile = new File(cwd + desc);
									if (!iFile.exists())
										iFile = new File(desc);
									if (iFile.exists())
										lstItems.addAll(getTNSEntries(iFile));
									else {
										logger.severe(UtilResources.getString(UtilResources.TNSHelper_38) + desc);
									}
								} catch (Exception e) {
								}
								currentEntry.setLength(0);
							} else if (name.length() > 0 && desc.trim().length() > 0) {
								TCPTNSEntry aliasedAlias = null;
								for (TCPTNSEntry tns : lstItems) {
									if (tns.getName().equalsIgnoreCase(desc)) {
										aliasedAlias = tns;
									}
								}
								if (aliasedAlias != null) {
									entry = new TCPTNSEntry(name, aliasedAlias.getDescriptor());
								} else {
									entry = new TCPTNSEntry(name, desc);
								}
								lstItems.add(entry);
								currentEntry.setLength(0);
								logger.fine(UtilResources.getString(UtilResources.TNSHelper_39) + name);
								bracketCount = 0;
							}
						} catch (Exception e) {

						}
					}
				}
			}
		}
		logger.finest("getTNSItems - CLOSE: " + fileContent.getPath()); //$NON-NLS-1$
		in.close();
		// entries = lstItems;
		logger.finest("getTNSItems - END"); //$NON-NLS-1$
		return lstItems;
	}

	/**
	 * 
	 * getTNSEntries
	 * 
	 * @return TNSHelper
	 */
	public static ArrayList<TCPTNSEntry> getTNSEntries() {
		synchronized (LOCK) {
			logger.finest("getTNSEntries - START"); //$NON-NLS-1$
			Timer timer = new Timer();
			timer.start();
			if (s_entries == null) {
				s_entries = new ArrayList<TCPTNSEntry>();
			}
			ArrayList<TCPTNSEntry> ret = s_entries;
			try {
				File contents = getTNSFileContent(false);
				if (contents != null) {
					logger.info(UtilResources.getString(UtilResources.TNSHelper_43) + contents.toString());
					ret = getTNSEntries(contents);
				}
			} catch (Exception e) {
				// expect an exception thrown if the tnsfile hasnt change. This
				// is crappy code. look to refactor, shouldnt use an exception
				// to return an expected state.
			}
			Comparator<TCPTNSEntry> comp = new Comparator<TCPTNSEntry>() {
				public int compare(TCPTNSEntry e1, TCPTNSEntry e2) {
					return Collator.getInstance().compare(e1.toString(), e2.toString());
				}
			};
			Collections.sort(ret, comp);
			// doneill
			logger.finest("Finished Parsing TNS in" + timer.durationInSecs()); //$NON-NLS-1$
			logger.finest("getTNSEntries - END"); //$NON-NLS-1$
			if (s_entries != ret) {
				s_entries.addAll(ret);
			}
			// entries.addAll(ret);
			return ret;
		}
	}

	/**
	 * 
	 * getProvideTnsnamesDir
	 * 
	 * @return TNSHelper
	 */
	public static ProvideTnsnamesDir getProvideTnsnamesDir() {
		return getProvideTnsnamesDir;
	}

	/**
	 * 
	 * setProvideTnsnamesDir
	 * 
	 * @param gettnsnamesDir
	 *            TNSHelper
	 */
	public static void setProvideTnsnamesDir(ProvideTnsnamesDir gettnsnamesDir) {
		TNSHelper.getProvideTnsnamesDir = gettnsnamesDir;
	}

	private static ProvideTnsnamesDir getProvideTnsnamesDir = null;

	public static void main(String[] args) throws Exception {
		// http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4199068
		// SimpleIdeCore.initIde();
		logger.info("OH=" + System.getenv(ENV_OH)); //$NON-NLS-1$
		logger.info("TA=" + System.getenv(ENV_TA)); //$NON-NLS-1$
		ArrayList<TCPTNSEntry> entries = getTNSEntries();
		for (TCPTNSEntry entry : entries) {
			logger.info(entry.toString());
		}
		System.exit(0);
	}
}
