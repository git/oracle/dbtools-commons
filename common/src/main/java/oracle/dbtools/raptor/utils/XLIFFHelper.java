/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLNode;
import oracle.xml.parser.v2.XMLParseException;
import oracle.xml.parser.v2.XSLException;

import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

/**
 * Helper class for getting translations from XML files.
 * Oracle uses the XLiff format for translations.
 * base file which is being read and use the xliff
 * to present translated strings.This class will set the
 */
public class XLIFFHelper {
    private static final Logger LOGGER = Logger.getLogger(XLIFFHelper.class.getName());

    private ClassLoader m_cl;
  private XMLDocument _document;
  private DOMParser _parser;
  private String _name;

    public XLIFFHelper(ClassLoader cl, String name) {
        m_cl = cl;
        setBaseDocName(name);
    }

  /**
   * Get the base doc name for the translated file using the locale
   * This gives us the appropriate set of strings for the locale which
   * we will use to substitute from the english ones.
   * @param name of base file.
   */
    private void setBaseDocName(final String name) {
	  _name = name;
	  _document = null;
	  _parser = null;
  }
  
  private void open() {
	InputStream in =null;
	if ( _name != null ) {
	    try {
	        String localeCodeXliff = Locale.getDefault() + ".xliff"; //$NON-NLS-1$
	        final String baseName = _name.substring(0, _name.lastIndexOf(".")); //$NON-NLS-1$
	        String trName = baseName + "_" + localeCodeXliff; //$NON-NLS-1$ 
	        in = m_cl.getResourceAsStream(trName);
	        if (in == null && localeCodeXliff.lastIndexOf("_") > 0) { //If we cant read this, then we try just the language //$NON-NLS-1$
	            String langCodeXliff = localeCodeXliff.substring(0, localeCodeXliff.lastIndexOf("_")) + ".xliff"; //$NON-NLS-1$ //$NON-NLS-2$
	            trName = baseName + "_" + langCodeXliff; //$NON-NLS-1$ 
	            in = m_cl.getResourceAsStream(trName);
	        }
	        if (in == null) { //If we cant read this, then we revert to english.
	            return;
	        }
	        if (_parser == null) {
	            _parser = new DOMParser();
	        } else {
	            _parser.reset();
	        }
	        _parser.setPreserveWhitespace(false);
	        _parser.parse(in);
	        _document = _parser.getDocument();
	    } catch (final XMLParseException e) {
	        LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
	    } catch (final SAXException e) {
	        LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
	    } catch (final IOException e) {
	        Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);    
	    } finally { 
	        if ( in != null ) try { in.close(); } catch (Exception e){}
	    }
	}
  }


    /**
     * Get the translated String for the string entered by taking the xpath
     * and returning the target 'translated' string back to the caller.
     * @param key to look for
     * @return the translated strings
     */
    public String getTranslation(final String key) {
      // ///xliff/file/body/trans-unit[source = 'Drop']/target
    	
    	// no need for english
    	if ( ! System.getProperty("user.language").equalsIgnoreCase("en") ) { //$NON-NLS-1$ //$NON-NLS-2$
		    	if (_document == null && _name != null) {
		    		open();
		    	}
		      try {
		        final String path = "file/body/trans-unit[source = '" + key + "']/target"; //$NON-NLS-1$ //$NON-NLS-2$
		        if  (_document != null && key != null && key.length() > 0) {
		            final XMLNode node1 = (XMLNode) _document.getFirstChild();
		         final XMLNode node = (XMLNode) node1.selectSingleNode(path);
		         if (node != null) {
		           return node.getTextContent();
		         } else {
		             return key;
		         }
		        } else {
		            return key;
		        }
		
		      } catch (final XSLException e) {
            LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		      } catch (final DOMException e) {
            LOGGER.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
		      }
    	}
      return key;
  }
}
