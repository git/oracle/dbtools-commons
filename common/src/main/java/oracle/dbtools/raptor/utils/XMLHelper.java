/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import oracle.xml.parser.v2.XMLNode;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class XMLHelper {

    private static DocumentBuilderFactory factory = null;
    private static DocumentBuilder builder = null;
    private static Document document = null;
    private static Element root = null;    
    
    public static Node getChildNode(Node node,String s){
        try
        {
          if (node == null || s == null)
              return null;
          NodeList nl = ( ( XMLNode ) node ).getChildNodes();
          Node ret = null;
          String name = s.indexOf("/") == 0?s.substring(1):s; //$NON-NLS-1$
          for(int i=0;ret==null&&i<nl.getLength();i++){
              if (nl.item(i).getNodeName().equals(name) ) {
        	  ret = nl.item(i);
              }
          }
          return ret;// ( ( XMLNode ) node ).selectSingleNode( s );
        }
        catch ( Exception e )
        {
            Logger.getLogger(XMLHelper.class.getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
        }
        return null;
    }
    
    public static XMLNode[] getChildNodes( Node node, String s )
    {
      XMLNode[] result = null;
      try
      {
    	NodeList nl = ((XMLNode)node).getChildNodes();
    	ArrayList<XMLNode> l = new ArrayList<XMLNode>();
    	for ( int i = 0; i < nl.getLength(); i++ ){
    		if (nl.item(i).getNodeName().equalsIgnoreCase(s)) {
    			l.add((XMLNode) nl.item(i));
    		}
    	}
    	if (l.size() > 0) {
    		result = new XMLNode[l.size()];
    		l.toArray(result);
    	}

      }
      catch ( Exception ex )
      {
        
      }
      
      return result != null ? result : new XMLNode[ 0 ];
    }
    
    public static String getNodeValue(Node node,String name){
        String value = null;
        if( node != null ){
          Node child = getChildNode( node, name );
	        if (child == null)
	          return ""; //$NON-NLS-1$
                  
	        Node n1 = child.getFirstChild();
                if ( n1 instanceof Text )
                {
                    value = ( ( Text ) n1 ).getTextContent();
                }
                value = n1 != null ? n1.getNodeValue() : child.getNodeValue();
        }
        return value;
    }
    
    public static String getAttributeNode(Node node,String s){
          String ret=null;
          try
          {
            if ( node != null )
            { 
                String path = s.startsWith("@") ? s.substring(1) :  s; //$NON-NLS-1$
                if ( node.getAttributes() != null ) {
                	Node n = node.getAttributes().getNamedItem(path);//( ( XMLNode ) node ).selectSingleNode( path );
	                if ( n != null ) {
	                  ret = n.getNodeValue();
	                }
                }
            }
          }
          catch ( Exception e )
          {
              Logger.getLogger(XMLHelper.class.getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
          }
          return ret;
    }
    
    public static Element NewDocument(){
        try {
                factory = DocumentBuilderFactory.newInstance();
                factory.setCoalescing(true);
                builder = factory.newDocumentBuilder();
                document = builder.newDocument();  // Create from whole cloth
                root = document.createElement("reports");  //$NON-NLS-1$
                document.appendChild(root);
                return root;
            } catch (ParserConfigurationException pce) {
                // Parser with specified options can't be built
                Logger.getLogger(XMLHelper.class.getName()).info(pce.toString());
            }
        return null;
    }
    
    public static Element addFolder(Element parent, String iName,String iDesc, String iTip){
        Element folder = document.createElement("folder");  //$NON-NLS-1$
        Element name = document.createElement("name");  //$NON-NLS-1$
        name.appendChild(document.createCDATASection(iName.trim()));
        folder.appendChild(name);
        Element desc = document.createElement("description");  //$NON-NLS-1$
        desc.appendChild(document.createCDATASection(iDesc.trim()));
        folder.appendChild(desc);
        Element tip = document.createElement("tooltip");  //$NON-NLS-1$
        tip.appendChild(document.createCDATASection(iTip.trim()));
        folder.appendChild(tip);
        parent.appendChild(folder);
        return folder;
    }
    
    public static void addReport(Element parent,String iName,String iDesc, String iTip, String iSql){
        Element report = document.createElement("report");  //$NON-NLS-1$
        report.setAttribute("type","report"); //$NON-NLS-1$ //$NON-NLS-2$
        report.setAttribute("enabled","true"); //$NON-NLS-1$ //$NON-NLS-2$
        
        Element name = document.createElement("name");  //$NON-NLS-1$
        name.appendChild(document.createCDATASection(iName.trim()));
        report.appendChild(name);
        
        Element desc = document.createElement("description");  //$NON-NLS-1$
        desc.appendChild(document.createCDATASection(iDesc.trim()));
        report.appendChild(desc);
        
        Element tip = document.createElement("tooltip");  //$NON-NLS-1$
        tip.appendChild(document.createCDATASection(iTip.trim()));
        report.appendChild(tip);
        
        Element query = document.createElement("query");  //$NON-NLS-1$
        report.appendChild(query);
        
        Element sql = document.createElement("sql");  //$NON-NLS-1$
        sql.appendChild(document.createCDATASection(iSql.trim()));
        query.appendChild(sql);
        
        parent.appendChild(report);
    }
    
    public static Document getDocument() {
        return document;
    }
    
}
