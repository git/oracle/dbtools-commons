/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils.oerr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;


public class DBMessage2XML {
  public static int SPLITINTO = 10;

  WriteXML wXML = null;


  public static void main(String[] args) throws Exception {
    System.out.println("Set the input (.msg) and output (.xml) diectories run " + // (authorized) //$NON-NLS-1$
                       "the code and copy over and checkin the new files"); //$NON-NLS-1$
    Oerr x = new Oerr();
    x.oerr("tns", "0"); //$NON-NLS-1$ //$NON-NLS-2$
    System.exit(0);
    DBMessage2XML d2x = new DBMessage2XML();
    d2x.process("/home/turloch/separation/permanent_tmp/bmcgillimesgfiles", //$NON-NLS-1$
                "/home/turloch/separation/permanent_tmp/xmlmessagefiles"); //$NON-NLS-1$
  }

  private void process(String dir, String results) throws Exception {
      URL d = new URL( dir );
    // OutputStream out = new FileOutputStream(results);
    System.out.print("Processing:"); // (authorized) //$NON-NLS-1$
    wXML = new WriteXML();
    wXML.setupIndexFile(results);
    
    File folder = new File(d.toURI());
    File[] list = folder.listFiles();
    for (int i=0;i<list.length;i++) {
    	if (list[i].getName().toLowerCase().endsWith("msg")) {
    		processFile(list[i].toURI().toURL(), results);
    	}
    }
    wXML.finishIndexFile();
    WriteXML wXMLg = new WriteXML();
    long start = System.currentTimeMillis();
    /*
     * //wXML.getByIndex(results,"ora","44004"); wXMLg.getByIndex(results,
     * "ora", "02082"); System.out.println(System.currentTimeMillis() -
     * start); start = System.currentTimeMillis(); wXMLg.getByIndex(results,
     * "ora", "44004"); //wXML.getByIndex(results,"ora","2082");
     * System.out.println(System.currentTimeMillis() - start);
     */
    start = System.currentTimeMillis();
    // wXML.getByIndex(results,"ora","44004");
    wXMLg.getByIndex(results, "tns", "100"); //$NON-NLS-1$ //$NON-NLS-2$
    System.out.println(System.currentTimeMillis() - start); // (authorized)
    start = System.currentTimeMillis();
    wXMLg.getByIndex(results, "tns", "0"); //$NON-NLS-1$ //$NON-NLS-2$
    // wXML.getByIndex(results,"ora","2082");
    System.out.println(System.currentTimeMillis() - start);// (authorized)
    // for(DBMessage2XML.Error e:errList){
    // System.out.println("--------");
    // System.out.println(e.toString());
    // }

  }

  private void processFile(URL url, String results) throws Exception {
    BufferedReader in = new BufferedReader( new InputStreamReader( new FileInputStream(new File( url.toURI()) ) ) );
    String buffer = null;
    DBMessage2XML.Error err = null;
    ArrayList<DBMessage2XML.Error> errList = new ArrayList<DBMessage2XML.Error>();
    while ((buffer = in.readLine()) != null) {
      if (buffer.length() > 0 && Character.isDigit(buffer.charAt(0))) {
        if (err != null)
          errList.add(err);
        // System.out.println("N:" + buffer);
        err = new DBMessage2XML.Error(buffer);
      } else if (err != null && (buffer.indexOf("//") == 0) //$NON-NLS-1$
                 && (buffer.indexOf("///") != 0) //$NON-NLS-1$
                 && (!buffer.matches("^\\/\\/[ \t]*$"))) { //$NON-NLS-1$
        err.addText(buffer);
      } else {
        // end grab
        if (err != null) {
          errList.add(err);
          // System.out.println(err.toString());
          err = null;
        }
        // System.out.println("--"+buffer);
      }
    }
    if (err != null) {
      errList.add(err);
      // System.out.println(err.toString());
      err = null;
    }
    int interval;
    if (SPLITINTO < 2) {
      interval = errList.size();
    } else {
      interval = errList.size() / (SPLITINTO - 1);
    }
    int filesequence = 1;

    int errorNumber = 1;
    int min = 0;
    int max = 0;
    String language = "us"; //$NON-NLS-1$
    String fName = url.getFile();
    for (DBMessage2XML.Error e : errList) {
      if ((errorNumber - 1) % interval == 0) {
        min = new Integer(e.getErrorNum());
        wXML.startSequenceFile(fName.substring(0,(fName.indexOf(language + ".msg"))), //$NON-NLS-1$
                               (new Integer(filesequence)).toString(), language);
      }
      wXML.writeOneSequence(e.getErrorNum(), e.toString());
      if (((errorNumber % interval == 0))
          || (errorNumber == errList.size())) {
        max = new Integer(e.getErrorNum());
        wXML.addIndexLine(fName.substring(0,(fName.indexOf(language + ".msg"))), //$NON-NLS-1$
                          new Integer(filesequence).toString(), language,
                          new Integer(min).toString(), new Integer(max)
                          .toString());
        wXML.finishSequenceFile();
        filesequence++;
      }
      errorNumber++;
    }

  }

  class Error {

    String errNum;

    String _errSubNum;

    String _errMsg;

    // HashMap<String, StringBuilder> _msgs = new HashMap<String,
    // StringBuilder>();
    String _lastGrp;

    StringBuffer _payload = new StringBuffer();

    Error(String firstLine) {
      String[] parts = firstLine.split(",", 3); //$NON-NLS-1$
      errNum = parts[0];
      _errSubNum = parts[1];
      _errMsg = parts[2];
    }

    public String getErrorNum() {
      return errNum;
    }

    public void addText(String text) {
      String raw = text.indexOf(" ") > 0 ? text.substring( //$NON-NLS-1$
        text.indexOf(" ")).trim() : ""; //$NON-NLS-1$ //$NON-NLS-2$
      if (raw.matches("^\\*[A-Za-z ]+:.*")) { //$NON-NLS-1$
        String[] parts = raw.split(":"); //$NON-NLS-1$
        _payload.append((parts[0].trim() + ":             ").substring( //$NON-NLS-1$
                          0, 11)
                        + (new StringBuilder((parts.length > 1 ? parts[1]
                                              .trim()
                                              + "\n" : "\n")))); //$NON-NLS-1$ //$NON-NLS-2$
        // _msgs.put(parts[0], new StringBuilder((parts.length > 1 ?
        // parts[1]+"\n" : "\n")));
        _lastGrp = parts[0];
      } else if (_lastGrp != null) {
        _payload.append(("                       ").substring(0, 11) //$NON-NLS-1$
                        + raw + "\n"); //$NON-NLS-1$
        // _msgs.get(_lastGrp).append("\t" + raw + "\n");
      } else {
        // punt
      }
    }

    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(errNum);
      sb.append("."); //$NON-NLS-1$
      sb.append(_errSubNum);
      sb.append(" - "); //$NON-NLS-1$
      sb.append(_errMsg);
      sb.append("\n"); //$NON-NLS-1$
      // Iterator<String> keys = _msgs.keySet().iterator();
      // String key=null;
      // while(keys.hasNext()){
      // key = keys.next();
      // sb.append(key);
      // sb.append(":");
      // sb.append(_msgs.get(key).toString());
      // }
      sb.append(_payload);

      return sb.toString();
    }
  }
}
