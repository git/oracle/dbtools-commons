/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils.oerr;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=GetFile.java">Barry McGillin</a> 
 *
 */
class GetFile extends DefaultHandler {
  public boolean match = false;

  public String lookfor = null;

  // public String ami="";
  private String _baseName = null;

  private int _errNo = 0;

  boolean matchFound = false;

  public GetFile(String baseName, String errNo) {
    super();
    _baseName = baseName;
    _errNo = new Integer(errNo).intValue();
  }

  public void startElement(String uri, String localName, String qName,
                           Attributes attributes) throws SAXException {
    if (matchFound == false) {
      if ("fileIndex".equals(localName)) { //$NON-NLS-1$
        if (_baseName.equals(attributes.getValue("", "basename"))) { //$NON-NLS-1$ //$NON-NLS-2$

          int min = new Integer(attributes.getValue("", "min")) //$NON-NLS-1$ //$NON-NLS-2$
            .intValue();
          int max = new Integer(attributes.getValue("", "max")) //$NON-NLS-1$ //$NON-NLS-2$
            .intValue();
          if ((_errNo >= min) && (_errNo <= max)) {
            lookfor = attributes.getValue("", "basename") //$NON-NLS-1$ //$NON-NLS-2$
              + attributes.getValue("", "sequence") //$NON-NLS-1$ //$NON-NLS-2$
              + attributes.getValue("", "language") + ".xml"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            matchFound = true;
          }
        }
      }
    }
  }
}
