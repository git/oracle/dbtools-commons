/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils.oerr;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Oerr.java">Barry McGillin</a> 
 *
 */
public class Oerr {
    /*
     * public static void main(String[] args) throws Exception { System.out.println("XX"); }
     */

    private SAXParserFactory _factory = null;

    public static void main(String[] args) throws Exception {
        Oerr oerr = new Oerr();
        try {
            oerr.oerr("ORA-0100"); //$NON-NLS-1$
        } catch (OerrException oe) {
            System.out.println(oe.getMessage());
        }
        try {
            System.out.println(oerr.oerr("ora", "2082")); //$NON-NLS-1$ //$NON-NLS-2$
            System.out.println("-ora 2082"); //$NON-NLS-1$
        } catch (OerrException e) {
            System.out.println("Oerr" + e.getMessage()); //$NON-NLS-1$
        }
        try {
            System.out.println(oerr.oerr("ora", "44004")); //$NON-NLS-1$ //$NON-NLS-2$
            System.out.println("-ora 44004"); //$NON-NLS-1$
        } catch (OerrException e) {
            System.out.println("Oerr" + e.getMessage()); //$NON-NLS-1$
        }
        try {
            System.out.println(oerr.oerr("tns", "100")); //$NON-NLS-1$ //$NON-NLS-2$
            System.out.println("-tns 100"); //$NON-NLS-1$
        } catch (OerrException e) {
            System.out.println("Oerr" + e.getMessage()); //$NON-NLS-1$
        }

        try {
            System.out.println(oerr.oerr("tns", "100")); //$NON-NLS-1$ //$NON-NLS-2$
            System.out.println("-tns 100"); //$NON-NLS-1$
        } catch (OerrException e) {
            System.out.println("Oerr" + e.getMessage()); //$NON-NLS-1$
        }
        try {
            System.out.println(oerr.oerr("xyz", "100")); //$NON-NLS-1$ //$NON-NLS-2$
        } catch (OerrException e) {
            System.out.println("Oerr" + e.getMessage()); //$NON-NLS-1$
        }
        System.out.println("-xyz 100"); //$NON-NLS-1$

        try {
            System.out.println(oerr.oerr("tns", "error")); //$NON-NLS-1$ //$NON-NLS-2$
        } catch (OerrException e) {
            System.out.println("Oerr" + e.getMessage()); //$NON-NLS-1$
        }
        System.out.println("-tns error"); //$NON-NLS-1$
        try {
            System.out.println(oerr.oerr("ORA-2082")); //$NON-NLS-1$
            System.out.println("-ORA-2082"); //$NON-NLS-1$
        } catch (OerrException e) {
            System.out.println("Oerr" + e.getMessage()); //$NON-NLS-1$
        }

        try {
            System.out.println(oerr.oerr("ORA-44004")); //$NON-NLS-1$
            System.out.println("-ORA-44004"); //$NON-NLS-1$
        } catch (OerrException e) {
            System.out.println("Oerr" + e.getMessage()); //$NON-NLS-1$
        }

        try {
            System.out.println(oerr.oerr("TNS-0100:")); //$NON-NLS-1$
            System.out.println("-TNS-0100:"); //$NON-NLS-1$
        } catch (OerrException e) {
            System.out.println("Oerr" + e.getMessage()); //$NON-NLS-1$
        }

        try {
            System.out.println(oerr.oerr("TNS-0999999")); //$NON-NLS-1$
            System.out.println("-TNS-0999999"); //$NON-NLS-1$
        } catch (OerrException e) {
            System.out.println("Oerr" + e.getMessage()); //$NON-NLS-1$
        }

    }

    public String oerr(String typeAndNum) throws OerrException {
        String retVal = ""; //$NON-NLS-1$
        String baseName = ""; //$NON-NLS-1$
        String errNo = ""; //$NON-NLS-1$
        if (typeAndNum == null) {
            throw new OerrException(OerrArb.getString(OerrArb.OERR_NULL));
        }
        typeAndNum = typeAndNum.trim();
        if (typeAndNum.indexOf("\n") != -1) { //$NON-NLS-1$
            typeAndNum = typeAndNum.substring(0, typeAndNum.indexOf("\n")); //$NON-NLS-1$
        }
        if (typeAndNum.indexOf("\r") != -1) { //$NON-NLS-1$
            typeAndNum = typeAndNum.substring(0, typeAndNum.indexOf("\r")); //$NON-NLS-1$
        }
        int minus = typeAndNum.indexOf('-');
        if (minus != -1) {
            baseName = typeAndNum.substring(0, minus);
            String rest = typeAndNum.substring(minus);
            if (rest.matches("^-[0-9]+.*")) { //$NON-NLS-1$
                int i = 0;
                for (i = 1; i < rest.length(); i++) {
                    if (!(Character.isDigit(rest.charAt(i)))) {
                        errNo = rest.substring(1, i);
                        break;
                    }
                }
                if (i == rest.length()) {
                    errNo = rest.substring(1);
                }
                retVal = oerr(baseName, errNo);
            }
        }
        if ((retVal == null) || (retVal.equals(""))) { //$NON-NLS-1$
            throw new OerrException(OerrArb.getString(OerrArb.NO_VALUE_OERR_STRING));
        }
        return retVal;
    }

    public String oerr(String baseName, String errNo) throws OerrException {
        String retVal = ""; //$NON-NLS-1$
        try {
            if ((baseName == null) || (errNo == null)) {
                throw new OerrException(OerrArb.getString(OerrArb.OERR_NULL));
            }
            baseName = baseName.toLowerCase();
            if (!((baseName.equals("tns") || (baseName.equals("ora"))))) { //$NON-NLS-1$ //$NON-NLS-2$
                throw new OerrException(OerrArb.getString(OerrArb.ONLY_ORA_TNS));
            }

            String sequenceFile = getIndexFile(baseName, errNo);
            if ((sequenceFile != null) && (sequenceFile != "")) { //$NON-NLS-1$
                retVal = outputMatch(sequenceFile, errNo);
            }
            if ((retVal == null) || (retVal.equals(""))) { //$NON-NLS-1$
                throw new OerrException(OerrArb.getString(OerrArb.NO_VALUE_OERR_STRING_STRING));
            }
        } catch (NumberFormatException nfe) {
            throw new OerrException(OerrArb.getString(OerrArb.OERR_NUMBER));
        }
        return retVal.trim();
    }

    private String getIndexFile(String baseName, String errNo) throws OerrException {
        try {
            InputStream in = null;

            InputStream nobuf = getClass().getResourceAsStream("index.xml"); //$NON-NLS-1$
            if (nobuf == null) {
                throw new OerrException(OerrArb.format(OerrArb.GET_RESOURCE_AS_STREAM, "index.xml")); //$NON-NLS-1$
            }
            in = new BufferedInputStream(nobuf);

            if (_factory == null) {
                _factory = SAXParserFactory.newInstance();
                _factory.setValidating(false);
            }
            Oerr.GetFile getoutput = new Oerr.GetFile(baseName, errNo);
            DefaultHandler handler = getoutput;
            _factory.newSAXParser().parse(in, handler);
            if ((getoutput.lookfor == null) || (getoutput.lookfor.equals(""))) { //$NON-NLS-1$
                throw new OerrException(OerrArb.format(OerrArb.GET_FILE_ERROR, baseName, errNo));
                // "Oerr GetFile("+baseName+","+errNo+") failed");
            }

            return getoutput.lookfor;
        } catch (IOException e) {
            throw new OerrException(e);
        } catch (SAXException e) {
            throw new OerrException(e);
        } catch (ParserConfigurationException e) {
            throw new OerrException(e);
        }
    }

    private String outputMatch(String sequenceFile, String errNo) throws OerrException {
        String retVal = ""; //$NON-NLS-1$
        try {
            InputStream in = null;
            InputStream nobuf = getClass().getResourceAsStream(sequenceFile);
            if (nobuf == null) {
                throw new OerrException(OerrArb.format(OerrArb.GET_RESOURCE_AS_STREAM, sequenceFile));
            }
            in = new BufferedInputStream(nobuf);
            if (_factory == null) {
                _factory = SAXParserFactory.newInstance();
                _factory.setValidating(false);
            }
            Oerr.SAXHandler getoutput = new Oerr.SAXHandler(errNo);
            DefaultHandler handler = getoutput;
            InputSource is = new InputSource(in);
            is.setEncoding("UTF-8"); //$NON-NLS-1$
            _factory.newSAXParser().parse(is, handler);

            if ((getoutput.lookfor.toString() != null) && (!getoutput.lookfor.toString().equals(""))) { //$NON-NLS-1$
                retVal = getoutput.lookfor.toString();
            }
            if ((getoutput.lookfor == null) || (getoutput.lookfor.equals(""))) { //$NON-NLS-1$
                throw new OerrException(OerrArb.getString(OerrArb.NO_VALUE_OERR_STRING));
            }
        } catch (IOException e) {
            throw new OerrException(e);
        } catch (SAXException e) {
            throw new OerrException(e);
        } catch (ParserConfigurationException e) {
            throw new OerrException(e);
        }
        return retVal;
    }

    private class SAXHandler extends DefaultHandler {
        public boolean match = false;

        public StringBuffer lookfor = new StringBuffer();

        public int ami = 0;

        public SAXHandler(String getme) {
            super();
            ami = new Integer(getme).intValue();
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

            if ("payload".equals(localName)) { //$NON-NLS-1$
                if (ami == new Integer(attributes.getValue("", "number")) //$NON-NLS-1$ //$NON-NLS-2$
                .intValue()) {
                    match = true;
                    lookfor = new StringBuffer();
                }
            }
        }

        public void characters(char[] chars, int start, int length) {
            if (match == true) {
                lookfor.append(chars, start, length);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            match = false;
        }
    }

    private class GetFile extends DefaultHandler {
        public boolean match = false;

        public String lookfor = null;

        // public String ami="";
        private String _baseName = null;

        private int _errNo = 0;

        boolean matchFound = false;

        public GetFile(String baseName, String errNo) {
            super();
            _baseName = baseName;
            _errNo = new Integer(errNo).intValue();
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (matchFound == false) {
                if ("fileIndex".equals(localName)) { //$NON-NLS-1$
                    if (_baseName.equals(attributes.getValue("", "basename"))) { //$NON-NLS-1$ //$NON-NLS-2$

                        int min = new Integer(attributes.getValue("", "min")) //$NON-NLS-1$ //$NON-NLS-2$
                        .intValue();
                        int max = new Integer(attributes.getValue("", "max")) //$NON-NLS-1$ //$NON-NLS-2$
                        .intValue();
                        if ((_errNo >= min) && (_errNo <= max)) {
                            lookfor = attributes.getValue("", "basename") //$NON-NLS-1$ //$NON-NLS-2$
                                    + attributes.getValue("", "sequence") //$NON-NLS-1$ //$NON-NLS-2$
                                    + attributes.getValue("", "language") + ".xml"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                            matchFound = true;
                        }
                    }
                }
            }
        }
    }
}
