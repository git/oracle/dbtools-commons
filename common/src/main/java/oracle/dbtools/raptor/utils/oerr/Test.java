/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils.oerr;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Test {
	  String myarg=""; //$NON-NLS-1$
	  public static void main(String[] args){
		 //Test f2=new Test();
		 //f2.doit3();
	  java.lang.Runtime.getRuntime().gc();
      long initial=java.lang.Runtime.getRuntime().freeMemory();
      for (int i=0;i<10000;i++){
    	  java.lang.Runtime.getRuntime().gc();
        System.out.println(java.lang.Runtime.getRuntime().freeMemory()+":"+initial); //$NON-NLS-1$
        try{
          new Oerr().oerr("ORA-"+new Integer(i)); //$NON-NLS-1$
        }
        catch (Exception e)
        {
          System.out.println(e.getMessage()+"exception occurred"); //$NON-NLS-1$
        }
      }
		System.exit(1);
	    //f2=new Test();
	    //f2.doit("\"fred\"","fred","");
	    //f2.doit("\"fred\"flint stone","fred","flint stone");
	    //f2.doit("\'fred\'","fred","");
	    //f2.doit("\'fred\'flint stone","fred","flint stone");
	    //f2.doit("\'fred\' flint stone ","fred","flint stone");
	  }
  void doit3(){
    Oerr oerr=new Oerr();
    try{
      oerr.oerr("tns-100xx"); //$NON-NLS-1$
    } catch(OerrException e){
      e.getMessage();
    }
  }
	  void doit2(){
			InputStream in=Test.class.getResourceAsStream("fred.xml"); //$NON-NLS-1$
			if (in==null){
				System.out.println("NULL InputStream1"); //$NON-NLS-1$
			}
			//works in=Fred2.class.getResourceAsStream("/oracle/dbtools/db/misc/index.xml");
			/*also works*/in=Oerr.class.getResourceAsStream("index.xml" ); //$NON-NLS-1$
			if (in==null){
				System.out.println("NULL InputStream2"); //$NON-NLS-1$
			}
			else{
				BufferedInputStream buf=new BufferedInputStream(in);
				int c;
				try{
				while((c=buf.read())!=-1)
					System.out.print((char) c);
				}catch(IOException ioe){
				    Logger.getLogger(getClass().getName()).log(Level.WARNING, ioe.getStackTrace()[0].toString(), ioe);
				}
		    }
			System.out.println("XXX"); //$NON-NLS-1$
			System.exit(0);
	  }
	  void doit(String in, String expected, String argout){
	    String out=reallydoit(in);
	    if ((!(out.equals(expected)))||(!(myarg.equals(argout)))){
	      System.out.println("ERROR:"+in+":"+out+":"+myarg); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	    }
	  }
	  String reallydoit(String in){
	    String localFile=null;
	    String endString=""; //$NON-NLS-1$
	    myarg=""; //$NON-NLS-1$
	    if ((in.length()>1)&&(in.startsWith("\"")||in.startsWith("\'"))){ //$NON-NLS-1$ //$NON-NLS-2$
	      endString=new Character(in.charAt(0)).toString();
	      System.out.println(endString.substring(1));
	      int indexOfSecond=in.substring(1).indexOf(endString);
	      if (indexOfSecond==-1){
	        return "ERROR"; //$NON-NLS-1$
	      }
	      localFile=in.substring(1,indexOfSecond+1);
	      if (in.length()>indexOfSecond+2){
	        myarg=in.substring(indexOfSecond+2).trim();
	      }
	      else{
	        myarg=""; //$NON-NLS-1$
	      }
	    }
	    else{
	      localFile=in;
	      myarg=""; //$NON-NLS-1$
	    }
	    return localFile;
	  }
}
