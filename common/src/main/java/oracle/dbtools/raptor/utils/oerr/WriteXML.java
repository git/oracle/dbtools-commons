/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils.oerr;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;


public class WriteXML {
  /*
   * public static void main(String[] args) throws Exception {
   * System.out.println("XX"); }
   */
  private DocumentBuilderFactory factory = null;

  private DocumentBuilder builder = null;

  private Document document = null;

  private Document indexDocument = null;

  private Document sequenceDocument = null;

  private Element root = null;

  private Element indexRoot = null;

  private Element sequenceRoot = null;

  private SAXParserFactory _factory;

  private URL indexDirectory;

  private String localBaseName = null;

  private String localFileSequence = null;

  private String localLanguage = null;

  public static void main(String[] args) throws Exception {
    WriteXML wxml = new WriteXML();
    wxml.doit();
    wxml.getora("002"); //$NON-NLS-1$
  }

  public void setupIndexFile(String results) {
    try {
      indexDirectory = new URL( results );
      if (factory == null) {
        factory = DocumentBuilderFactory.newInstance();
        factory.setCoalescing(true);
        // factory.setCoalescing(false);
        builder = factory.newDocumentBuilder();
      }
      indexDocument = builder.newDocument(); // Create from whole cloth
      indexRoot = indexDocument.createElement("index"); //$NON-NLS-1$
    } catch (Exception e) {
	Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    }
  }

  public void startSequenceFile(String baseName, String fileSequence,
                                String language) {
    try {
      localBaseName = baseName;
      localFileSequence = fileSequence;
      localLanguage = language;
      if (factory == null) {
        factory = DocumentBuilderFactory.newInstance();
        factory.setCoalescing(true);
        // factory.setCoalescing(false);
        builder = factory.newDocumentBuilder();
      }
      sequenceDocument = builder.newDocument(); // Create from whole
      // cloth
      sequenceRoot = sequenceDocument.createElement(baseName);
    } catch (Exception e) {
	Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    }
  }

  public void addIndexLine(String baseName, String fileSequence,
                           String language, String min, String max) {
    Element fileIndex = indexDocument.createElement("fileIndex"); //$NON-NLS-1$
    fileIndex.setAttribute("basename", baseName); //$NON-NLS-1$
    fileIndex.setAttribute("sequence", fileSequence); //$NON-NLS-1$
    fileIndex.setAttribute("language", language); //$NON-NLS-1$
    fileIndex.setAttribute("min", min); //$NON-NLS-1$
    fileIndex.setAttribute("max", max); //$NON-NLS-1$
    indexRoot.appendChild(fileIndex);
  }

  void getora(String getme) {
    setupSax(getme);
  }

  void getByIndex(String results, String baseName, String errNo) {
    try {
	    indexDirectory = new URL( results);
    } catch (MalformedURLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
    }
    long start = System.currentTimeMillis();

    String sequenceFile = getIndexFile(false, baseName, errNo);
    System.out.println(Messages.getString("WriteXML.9") //$NON-NLS-1$
                       + (System.currentTimeMillis() - start));

    start = System.currentTimeMillis();
    outputMatch(false, sequenceFile, errNo);
    System.out.println(Messages.getString("WriteXML.8") //$NON-NLS-1$
                       + (System.currentTimeMillis() - start));
  }

  public String oerr(String typeAndNum) {
    String retVal = ""; //$NON-NLS-1$
    String baseName = ""; //$NON-NLS-1$
    String errNo = ""; //$NON-NLS-1$
    int minus = typeAndNum.indexOf('-');
    if (minus != -1) {
      baseName = typeAndNum.substring(0, minus);
      String rest = typeAndNum.substring(minus);
      if (rest.matches("^[0-9]+")) { //$NON-NLS-1$
        int i = 0;
        for (i = 0; i < rest.length(); i++) {
          if (!(Character.isDigit(rest.charAt(i)))) {
            errNo = rest.substring(0, i);
            break;
          }
        }
        if (i == rest.length()) {
          errNo = rest;
        }
        retVal = oerr(baseName, errNo);
      }
    }
    return retVal;
  }

  public String oerr(String baseName, String errNo) {
    String retVal = ""; //$NON-NLS-1$
    baseName = baseName.toLowerCase();
    if (!((baseName.equals("tns") || (baseName.equals("ora"))))) { //$NON-NLS-1$ //$NON-NLS-2$
      return (Messages.getString("WriteXML.17")); //$NON-NLS-1$
    }
    try {
      long start = System.currentTimeMillis();

      String sequenceFile = getIndexFile(true, baseName, errNo);
      System.out.println(Messages.getString("WriteXML.18") //$NON-NLS-1$
                         + (System.currentTimeMillis() - start));

      if ((sequenceFile != null) && (sequenceFile != "")) { //$NON-NLS-1$
        start = System.currentTimeMillis();
        retVal = outputMatch(true, sequenceFile, errNo);
        System.out.println(Messages.getString("WriteXML.20") //$NON-NLS-1$
                           + (System.currentTimeMillis() - start));
      }
    } catch (Exception e) {
      retVal = ""; //$NON-NLS-1$
    }
    return retVal;
  }

  private String getIndexFile(boolean byResource, String baseName,
                              String errNo) {
    try {
      InputStream in = null;
      if (byResource == false) {
          URL file = new URL (indexDirectory+File.separator+ "index.xml" ); //$NON-NLS-1$
          in = new BufferedInputStream( new FileInputStream(new File( file.toURI() ))) ;
      } else {
        // InputStream
        // nobuf=getClass().getClassLoader().getResourceAsStream("/oracle/dbtools/db/misc/index.xml");
        InputStream nobuf = getClass().getResourceAsStream("index.xml"); //$NON-NLS-1$
        // WriteXML.class.getResourceAsStream(/*"/oracle/dbtools/db/misc/"*/"index.xml");

        in = new BufferedInputStream(nobuf);
        // Resource.getResourceAsStream(getClass(), "index.xml" ));
      }

      if (_factory == null) {
        _factory = SAXParserFactory.newInstance();
        _factory.setValidating(false);
      }
      GetFile getoutput = new GetFile(baseName, errNo);
      DefaultHandler handler = getoutput;
      long start = System.currentTimeMillis();
      _factory.newSAXParser().parse(in, handler);
      System.out.println("do parse:" //$NON-NLS-1$
                         + (System.currentTimeMillis() - start));

      return getoutput.lookfor;
    } catch (Exception e) {
	Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
      return ""; //$NON-NLS-1$
    }
  }

  private String outputMatch(boolean byresource, String sequenceFile,
                             String errNo) {
    String retVal = ""; //$NON-NLS-1$
    try {
      InputStream in = null;
      if (byresource ) {
        System.out.println(sequenceFile);
        InputStream nobuf = getClass().getResourceAsStream(sequenceFile);
        // in = new BufferedInputStream(
        // getClass().getClassLoader().getResourceAsStream("/oracle/dbtools/db/misc/"+sequenceFile));
        // Resource.getResourceAsStream(getClass(),sequenceFile));
        in = new BufferedInputStream(nobuf);
      }
      if (_factory == null) {
        _factory = SAXParserFactory.newInstance();
        _factory.setValidating(false);
      }
      SAXHandler getoutput = new SAXHandler(errNo);
      DefaultHandler handler = getoutput;
      InputSource is= new InputSource(in);
      is.setEncoding("UTF-8"); //$NON-NLS-1$
      _factory.newSAXParser().parse(is, handler);
      System.out.println(getoutput.lookfor.toString());
      if ((getoutput.lookfor.toString() != null)
          && (!getoutput.lookfor.toString().equals(""))) { //$NON-NLS-1$
        retVal = getoutput.lookfor.toString();
      }
    } catch (Exception e) {
      retVal=""; //$NON-NLS-1$
    }
    return retVal;
  }

  void setupSax(String getme) {
    try {
        URL file = new URL( "/tmp/reallyworks_" ); //$NON-NLS-1$
        
      InputStream in = new BufferedInputStream( new FileInputStream(new File(file.toURI())) );
      if (_factory == null)
        _factory = SAXParserFactory.newInstance();
      _factory.setValidating(false);
      SAXHandler getoutput = new SAXHandler(getme);
      DefaultHandler handler = getoutput;
      _factory.newSAXParser().parse(in, handler);
      System.out.println(getoutput.lookfor.toString());
    } catch (Exception e) {
    }
  }

  void setup() throws Exception {
    // file handling open
    factory = DocumentBuilderFactory.newInstance();
    factory.setCoalescing(true);
    // factory.setCoalescing(false);
    builder = factory.newDocumentBuilder();
    document = builder.newDocument(); // Create from whole cloth
    root = document.createElement("errors"); //$NON-NLS-1$
  }

  void writeOneSequence(String numberString, String payloadString) {
    Element payload = sequenceDocument.createElement("payload"); //$NON-NLS-1$
    payload.setAttribute("number", numberString); //$NON-NLS-1$
    // payload.appendChild(document.createCDATASection(payloadString));
    payload.appendChild(sequenceDocument.createTextNode(payloadString));
    sequenceRoot.appendChild(payload);
  }

  void writeOne(String numberString, String payloadString) {
    /*
     * Element cover = document.createElement("cover"); Element number =
     * document.createElement("number");
     * number.appendChild(document.createTextNode(numberString));
     * cover.appendChild(number);
     */

    Element payload = document.createElement("payload"); //$NON-NLS-1$
    payload.setAttribute("number", numberString); //$NON-NLS-1$
    // payload.appendChild(document.createCDATASection(payloadString));
    payload.appendChild(document.createTextNode(payloadString));
    root.appendChild(payload);
  }

  void writeOnemanyelements(String numberString, String payloadString) {
    Element cover = document.createElement("cover"); //$NON-NLS-1$
    Element number = document.createElement("number"); //$NON-NLS-1$
    number.appendChild(document.createTextNode(numberString));
    cover.appendChild(number);

    Element payload = document.createElement("payload"); //$NON-NLS-1$
    // payload.appendChild(document.createCDATASection("Groovy Payload"));
    payload.appendChild(document.createTextNode(payloadString));
    cover.appendChild(payload);
    root.appendChild(cover);

  }

  void doit() {
    try {
      setup();
      writeOne("001", "Payload1"); //$NON-NLS-1$ //$NON-NLS-2$
      writeOne("002", "& <! Payload2 & <! "); //$NON-NLS-1$ //$NON-NLS-2$

      document.appendChild(root);
      // write a node number CDATA payload

      // file handling close
      writeXmlFile(document, "/tmp/reallyworks_"); //$NON-NLS-1$
    } catch (Exception e) {
        Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
      System.exit(1);
    }
  }

  void finishSequenceFile() {
    try {
      sequenceDocument.appendChild(sequenceRoot);
      // write a node number CDATA payload

      // file handling close
      writeXmlFile(sequenceDocument, indexDirectory + File.separator
                   + localBaseName + localFileSequence + localLanguage
                   + ".xml"); //$NON-NLS-1$

    } catch (Exception e) {
	Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
      System.exit(1);
    }

  }

  void finishIndexFile() {
    try {
      indexDocument.appendChild(indexRoot);
      // write a node number CDATA payload

      // file handling close
      writeXmlFile(indexDocument, indexDirectory + File.separator
                   + "index.xml"); //$NON-NLS-1$

    } catch (Exception e) {
	Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
      System.exit(1);
    }

  }

  void finish() {
    try {
      document.appendChild(root);
      // write a node number CDATA payload

      // file handling close
      writeXmlFile(sequenceDocument, "/tmp/reallyworks_"); //$NON-NLS-1$
    } catch (Exception e) {
	Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
      System.exit(1);
    }
  }

  public static void writeXmlFile(Document doc, String filename) {
    //write document to a file
	try {
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer;
      transformer = factory.newTransformer();
      //write to index or oerr subset file
      Source source = new DOMSource( doc );
            URL url = new URL( filename );
      //indent makes the code more reasonable
      transformer.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$
      transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8"); //$NON-NLS-1$
      StringWriter outWriter = new StringWriter();
      StreamResult toStream = new StreamResult( outWriter );
      transformer.transform(source, toStream);
      outWriter.close();
      OutputStream out=null;
	    try {
		    out = new FileOutputStream(new File(url.toURI()));
	    } catch (URISyntaxException e) {
		    e.printStackTrace();
	    }
      OutputStreamWriter osw=new OutputStreamWriter(out,"UTF-8"); //$NON-NLS-1$
      osw.write(outWriter.toString());
      osw.close();
      out.close();
      //should be buffered
      //posible problems with strams not closing worked around.
    } catch (TransformerConfigurationException e) {
	Logger.getLogger(WriteXML.class.getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    } catch (TransformerException e) {
	Logger.getLogger(WriteXML.class.getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    } catch (java.io.IOException e) {
	Logger.getLogger(WriteXML.class.getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
     }
  }
}


