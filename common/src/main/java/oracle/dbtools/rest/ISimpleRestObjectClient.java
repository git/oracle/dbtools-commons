/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.rest;

import java.io.IOException;
import java.net.URI;

import oracle.dbtools.common.utils.ModelUtil;

/**
 * ISimpleRestObjectAccessor
 * 
 * Utility class to allow retrieval and update of objects over REST using json. 
 *
 * CONSTRAINTS:
 *  1. You must know the object type(s) to be accessed for the end points
 *  2. The end point services must have access to the classes used 
 *  3. Any constraints added by the implementor
 *     For oracle.common.http, this means the classes used must be able to
 *     be mapped via com.fasterxml.jackson.databind.ObjectMapper.
 *  
 * NOTES:
 *  - Assuming ORDS, it has access to classes in oracle.dbtools.common
 *  - Implementors MAY use the type information to handle multiple types through a single end point     
 *
 * @author <a href="mailto:brian.jeffries@oracle.com?subject=oracle.dbtools.rest.ISimpleRestObjectAccessor">Brian Jeffries</a>
 * @since SQL Developer 4.2
 */
public interface ISimpleRestObjectClient {

    /**
     * Connect accessor to rest service 
     * @param info 
     * @throws IOException
     */
    public void connect(ConnectionInfo info) throws IOException;
    
    /**
     * Release connection. It cannot be re-used.
     */
    public void close();
    
    /**
     * Read object from rest end point. (Retrieve server state)
     * 
     * The GET method means retrieve whatever information (in the form of an 
     * entity) is identified by the Request-URI. If the Request-URI refers 
     * to a data-producing process, it is the produced data which shall be 
     * returned as the entity in the response and not the source text of the 
     * process, unless that text happens to be the output of the process. 
     * 
     * @param path relative path from service root for desired end point
     * @param type the expected type of the returned object
     * @return the requested object
     * @throws IOException
     */
    public <T> T get(String path, T object) throws IOException;
    
    /**
     * Create child object on rest end point.
     * 
     * The POST method is used to request that the origin server accept the
     * entity enclosed in the request as a new subordinate of the resource 
     * identified by the Request-URI in the Request-Line. POST is designed to 
     * allow a uniform method to cover the following functions: 
     *   * Annotation of existing resources
     *   * Posting a message to a bulletin board, newsgroup, mailing list, or 
     *     similar group of articles
     *   * Providing a block of data, such as the result of submitting a form, 
     *     to a data-handling process
     *   * Extending a database through an append operation
     * 
     * @param path relative path from service root for desired end point
     * @param type the expected type of the returned object
     * @return the requested object
     * @throws IOException
     */
    public <T> T post(String path, T object) throws IOException;

    /**
     * Send object to rest end point for update (update server state)
     * 
     * The PUT method requests that the enclosed entity be stored under the 
     * supplied Request-URI. If the Request-URI refers to an already existing
     * resource, the enclosed entity SHOULD be considered as a modified version
     * of the one residing on the origin server.
     *  
     * @param path relative path from service root for desired end point
     * @param object the object to update
     * @return the updated object
     * @throws IOException
     */
    public <T> T put(String path, T object) throws IOException;

    /**
     * Delete object from rest end point.
     * 
     * The DELETE method requests that the origin server delete the resource 
     * identified by the Request-URI. [...] The client cannot be guaranteed 
     * that the operation has been carried out, even if the status code 
     * returned from the origin server indicates that the action has been 
     * completed successfully. 
     * 
     * @param path relative path from service root for desired end point
     * @param type the expected type of the returned object
     * @return the requested object
     * @throws IOException
     */
    public boolean delete(String path) throws IOException;
    
    /**
     * A descriptor for cloud connections.
     * <p>
     * @author jmcginni
     * @since 3.1
     */
    public static class ConnectionInfo {
        // Ripped off from [common/http] oracle.dbtools.http.SessionInfo (minus sftp info)
        private final String m_name;
        private final URI m_root;
        private final String m_user;
        private final char[] m_pwd;
        private final URI m_serviceRoot;
        private final String m_clientId;
        private final String m_clientSecret;

        public ConnectionInfo(String name, URI root, URI serviceRoot, String username, char[] pwd) {
            this(name, root, serviceRoot, username, pwd, null, null);
        }
        
        public ConnectionInfo(String name, URI root, URI serviceRoot, String username, char[] pwd, String clientId, String clientSecret) {
            m_name = name;
            m_root = root;
            m_serviceRoot = serviceRoot;
            m_user = username;
            m_pwd = pwd;
            m_clientId = clientId;
            m_clientSecret = clientSecret;
        }
        
        /**
         * Retrieves the name of the connection. The connection name uniquely identifies 
         * the connection.
         * @return the name of the connection
         */
        public String getName() {
            return m_name;
        }

        /**
         * Retrieves the root of the connection. The connection root is a URI under which all the
         * connection resources can be found.
         * @return the URI root 
         */
        public URI getServerRoot() {
            return m_root;
        }
        
        /**
         * Retrieves the service root for the connection. The service root is a URI under which all the 
         * SQL Developer specific resources can be found.
         * @return the URI for the service root
         */
        public URI getServiceRoot() {
            return m_serviceRoot;
        }
        
        /** Retrieves the username of the connection.
         * 
         * @return the username
         */
        public String getUsername() {
            return m_user;
        }
        
        public char[] getPassword() {
            return m_pwd;
        }
        
        /**
         * Retrieve the Client ID. The Client ID is used to identify an application to the server as the originator of any HTTP requests.
         * @return the client ID.
         */
        public String getClientID() {
            return m_clientId; //TODO: Burn in to implementor?
        };
        
        /**
         * Retrieve the Client Secret. The Client Secret is a key to authenticate the application as a client.
         * @return the client secret.
         */
        public String getClientSecret() {
            return m_clientSecret; //TODO: Burn in to implementor?
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this ||
                    ( ( obj instanceof ConnectionInfo ) &&
                      ModelUtil.areEqual(m_name, ((ConnectionInfo) obj).m_name) &&
                      ModelUtil.areEqual(m_root, ((ConnectionInfo) obj).m_root) &&
                      ModelUtil.areEqual(m_serviceRoot, ((ConnectionInfo) obj).m_serviceRoot) &&
                      ModelUtil.areEqual(m_user, ((ConnectionInfo) obj).m_user) &&
                      ModelUtil.areEqual(m_pwd, ((ConnectionInfo) obj).m_pwd));
        }
        
        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @SuppressWarnings("nls")
        @Override
        public String toString() {
            return ""; // m_ftpUser + "#" + m_ftpHost + "#" + m_ftpPort;
        }
        
    }
    
}
