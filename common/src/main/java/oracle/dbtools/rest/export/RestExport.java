/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.rest.export;

import java.util.List;

import oracle.dbtools.data.common.NullOrEmpty;
import oracle.dbtools.rest.generators.RestCodeGenResults;
import oracle.dbtools.rest.model.RestModule;
import oracle.dbtools.rest.model.RestPrivilege;
//import oracle.dbtools.rest.model.RestRole;
import oracle.dbtools.rest.model.RestSchema;

/**
 * This class is responsible for exporting the RESTful Services definitions.
 * 
 * Exports the modules including its templates, handlers and parameters.
 * Exports the privileges and its associated roles, protected modules and resource patterns.
 * 
 * @author Elizabeth Saunders
 * @since  4.2
 *
 */
public class RestExport {
    private static final String NEWLINE = RestCodeGenResults.NEWLINE;

    private RestCodeGenResults m_codeGen;
    private StringBuilder m_codeSB;
    
    private String m_schema;
    private String m_ordsSchemaVer;

   
    public RestExport(String productName, String productVersion, String schema, String ordsSchemaVer) {
        this (productName, productVersion, schema, ordsSchemaVer, 0);
    }
    
    public RestExport(String productName, String productVersion,
                      String schema, String ordsSchemaVer,  int dbVersion) {
        m_schema = schema;
        m_ordsSchemaVer = ordsSchemaVer;
        
        if (dbVersion != 0) {
           m_codeGen = new RestCodeGenResults(productName, productVersion, dbVersion);
        } else {
            m_codeGen = new RestCodeGenResults(productName, productVersion);            
        }
    }
      
    /**
     * Set the backwards compatible flag to support 11g format.
     * If a REST resource handler source code is greater than 4K, then it will split up the
     * rest handler source code using the concatenation operator.
     * 
     * @param indicator
     */
    public void setBackwardsCompatible(boolean indicator) {
        getCodeGen().setBackwardsCompatible(indicator);        
    }
    
    public String exportModulesRolesPrivileges (RestSchema schema, List<RestModule> modules, 
                                                List<String> roles, List<RestPrivilege> privileges) {
        if (schema != null) {
            // export the enable schema information
            addCode(getCodeGen().generateEnableSchema(schema));            
        }
        
        if (modules != null && !modules.isEmpty()) {
            // export all the modules, templates, handlers and parameters
            for (RestModule module : modules) {
                addCode(getCodeGen().generateModuleTemplatesHandlers(module, false, false), false);
            }
        }
        
        if (roles != null && !roles.isEmpty()) {
            // export all the non-internal roles that are associated with this privilege
            addCode(getCodeGen().getCreateRoles(roles, false, false));
        }
        
        boolean hasPrivileges = false;
        if (privileges != null && !privileges.isEmpty()) {
            // export all the privileges including the associated roles, protected modules and resource patterns 
            String privs = getCodeGen().getDefinePrivileges(privileges, false, false);
            if (privs != null) {
                hasPrivileges = true;
                addCode(privs, false);
            }
        }
        
        String code = getCode();
        if (!NullOrEmpty.nullOrEmpty(code)) {
            String beginEndStmt = null;
            if (hasPrivileges) {
                // For privileges we need to include the declare/begin/end block
                beginEndStmt = getCodeGen().getPrivilegeBeginEnd(code, true);                
            } else {
                // Otherwise, we include only the begin/end block
                beginEndStmt = getCodeGen().getBeginStatementsEnd(code, true);                
            }
            
            if (!NullOrEmpty.nullOrEmpty(beginEndStmt)) {
                StringBuilder fullCode = new StringBuilder();
                
                String commentHdr = getCodeGen().getCommentHeader(m_schema, m_ordsSchemaVer);
                // Add the comment header
                if (commentHdr != null) {
                   fullCode.append(commentHdr).append(NEWLINE).append(beginEndStmt);
                } else {
                   fullCode.append(beginEndStmt);                        
                }

                return fullCode.toString();
            }
        }        
        return null;
    }
    
    /**
     * Get the code generator.
     * 
     * @return RestCodeGenResults
     */
    private RestCodeGenResults getCodeGen() {
        if (m_codeGen == null) {
            m_codeGen = new RestCodeGenResults();
        }
        return m_codeGen;        
    }
   
    /**
     * Convenient method to add the generated code and include an newline.
     * 
     * @param code
     */
    private void addCode(String code) {
        addCode(code, true);
    }

    /**
     * Add the generated code.
     * 
     * @param code
     * @param includeNewLine
     */
    private void addCode(String code, boolean includeNewLine) {
        if (code != null) {
            if (m_codeSB == null) {
                m_codeSB = new StringBuilder();
            }
            
            m_codeSB.append(code);            
            if (includeNewLine) {
                m_codeSB.append(NEWLINE);
            }
        }
    }
    
    /**
     * Get the generated code.
     * 
     * @return generated code
     */
    private String getCode() {
        if (m_codeSB == null) {
            return "";
        }
        return m_codeSB.toString();
    }
    
}
