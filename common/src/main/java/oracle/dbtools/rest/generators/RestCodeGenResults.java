/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.rest.generators;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.dbtools.data.common.NullOrEmpty;
import oracle.dbtools.rest.model.RestHandlerParameter;
import oracle.dbtools.rest.model.RestModule;
import oracle.dbtools.rest.model.RestPrivilege;
import oracle.dbtools.rest.model.RestResourceHandler;
import oracle.dbtools.rest.model.RestSchema;
import oracle.dbtools.rest.model.RestTemplate;
import oracle.dbtools.rest.model.RestResourceHandler.HTTP_METHOD_TYPE;
import oracle.dbtools.rest.model.RestTemplate.ENTITY_TAG_TYPE;

/**
 * @author Elizabeth Saunders
 * @since  4.2
 *
 */
public class RestCodeGenResults {
   
   public static final String NEWLINE = System.getProperty("line.separator"); //$NON-NLS-1$

   private static final int DEFAULT_DB_VERSION   = 12;
   
   // Handling 11g for source code > 4K
   private static final int MAX_STRING_LIMIT     = 4000;
   private static final int MAX_STRING_SIZE      = 3800;
   private static final int MAX_BUF_LIMIT        = 200;
   private static final String SINGLE_QUOTE      = "'";   //$NON-NLS-1$
   private static final String CONCAT_OPERATOR   = "|| "; //$NON-NLS-1$
   
   // Properties
   private static final String AUTO_REST_AUTH    = "autoRestAuth"; //$NON-NLS-1$
   private static final String ACCESS_METHOD     = "accessMethod"; //$NON-NLS-1$
   private static final String BASEPATH          = "basePath"; //$NON-NLS-1$
   private static final String BIND_VAR_NAME     = "bindVarName"; //$NON-NLS-1$
   private static final String COMMENTS          = "comments"; //$NON-NLS-1$
   private static final String COMMIT            = "commit"; //$NON-NLS-1$
   private static final String DATE_TIME         = "dateTime"; //$NON-NLS-1$      
   private static final String DESCRIPTION       = "description"; //$NON-NLS-1$
   private static final String ETAG_QUERY        = "etagQuery"; //$NON-NLS-1$
   private static final String ETAG_TYPE         = "etagType"; //$NON-NLS-1$
   private static final String ITEMS_PER_PAGE    = "itemsPerPage"; //$NON-NLS-1$
   private static final String LABEL             = "label"; //$NON-NLS-1$
   private static final String METHOD            = "method"; //$NON-NLS-1$
   private static final String MIMES_ALLOWED     = "mimesAllowed"; //$NON-NLS-1$
   private static final String MODULE_NAME       = "moduleName"; //$NON-NLS-1$
   private static final String NAME              = "name"; //$NON-NLS-1$
   private static final String NEW_NAME          = "newName"; //$NON-NLS-1$
   private static final String ORDS_SCHEMA_VER   = "ordsSchemaVer"; //$NON-NLS-1$
   private static final String ORIGINS_ALLOWED   = "originsAllowed"; //$NON-NLS-1$
   private static final String PARAM_TYPE        = "paramType"; //$NON-NLS-1$
   private static final String PATTERN           = "pattern"; //$NON-NLS-1$
   //private static final String PATTERNS          = "patterns"; //$NON-NLS-1$
   private static final String PRIORITY          = "priority"; //$NON-NLS-1$   
   //private static final String ROLES             = "roles"; //$NON-NLS-1$
   private static final String SCHEMA_NAME       = "schemaName"; //$NON-NLS-1$
   private static final String SCHEMA_STATUS     = "schemaStatus"; //$NON-NLS-1$
   private static final String SOURCE            = "source"; //$NON-NLS-1$
   private static final String SOURCE_TYPE       = "sourceType"; //$NON-NLS-1$
   private static final String SQLDEV_PROD_NAME  = "sqldevProdName"; //$NON-NLS-1$
   private static final String SQLDEV_PROD_VER   = "sqldevProdVer"; //$NON-NLS-1$
   private static final String STATEMENTS        = "statements"; //$NON-NLS-1$
   private static final String STATUS            = "status"; //$NON-NLS-1$
   private static final String URL_MAPPING_PATTERN = "urlMappingPattern"; //$NON-NLS-1$
   private static final String URL_MAPPING_TYPE    = "urlMappingType"; //$NON-NLS-1$
   
   // Templates that are specified in the ordsgen*.stg files
   
   private static final String EXPORT_COMMENT_HEADER = "export_comment_header"; //$NON-NLS-1$
   // Enable Schema
   private static final String ENABLE_SCHEMA     = "enable_schema"; //$NON-NLS-1$
   
   // Rest Modules
   private static final String DEFINE_MODULE     = "define_module"; //$NON-NLS-1$
   private static final String DELETE_MODULE     = "delete_module"; //$NON-NLS-1$
   private static final String RENAME_MODULE     = "rename_module"; //$NON-NLS-1$
   private static final String SET_MODULE_ORIGINS_ALLOWED = "set_module_origins_allowed"; //$NON-NLS-1$
   private static final String SET_PUBLISH_MODULE = "set_publish_module"; //$NON-NLS-1$

   // Rest Templates, Handlers and Parameters
   private static final String DEFINE_TEMPLATE   = "define_template"; //$NON-NLS-1$
   private static final String DEFINE_HANDLER    = "define_handler"; //$NON-NLS-1$
   private static final String DEFINE_PARAMETER  = "define_parameter"; //$NON-NLS-1$
   private static final String BEGIN_STATEMENTS_END  = "begin_statements_end"; //$NON-NLS-1$
   
   // Rest Privileges and Roles
   private static final String DEFINE_PRIVILEGE  = "define_privilege"; //$NON-NLS-1$
   private static final String RENAME_PRIVILEGE  = "rename_privilege"; //$NON-NLS-1$
   private static final String DELETE_PRIVILEGE  = "delete_privilege"; //$NON-NLS-1$
   private static final String CREATE_ROLE       = "create_role"; //$NON-NLS-1$
   private static final String RENAME_ROLE       = "rename_role"; //$NON-NLS-1$
   private static final String DELETE_ROLE       = "delete_role"; //$NON-NLS-1$
   private static final String ROLE_ARRAY        = "privilege_role_array"; //$NON-NLS-1$
   private static final String MODULE_ARRAY      = "privilege_module_array"; //$NON-NLS-1$
   private static final String PATTERN_ARRAY     = "privilege_pattern_array"; //$NON-NLS-1$
   private static final String DELETE_ELEMENTS   = "privilege_delete_array_elements";
   private static final String PRIV_BEGIN_END    = "privilege_begin_statements_end"; //$NON-NLS-1$
   
   // Values
   private static final String ENABLED       = "ENABLED"; //$NON-NLS-1$
   private static final String EMPTY         = ""; //$NON-NLS-1$
   private static final String DATA_TYPE_INT = "INT"; //$NON-NLS-1$
   //private static final String DISABLED      = "DISABLED"; //$NON-NLS-1$
   private static final String NOT_PUBLISHED = "NOT_PUBLISHED"; //$NON-NLS-1$
   private static final String PUBLISHED     = "PUBLISHED"; //$NON-NLS-1$
   
   // Boolean values
   private static final String TRUE          = "TRUE"; //$NON-NLS-1$
   private static final String FALSE         = "FALSE"; //$NON-NLS-1$
   
   private static final String INDEX         = "index"; //$NON-NLS-1$
   private static final String VALUE         = "value"; //$NON-NLS-1$
         
   private String m_productName = "";
   private String m_productVersion = "";
   private int    m_dbVersion = DEFAULT_DB_VERSION;  // defaults to 12

   private boolean m_backwardsCompatible;
   
   
   public RestCodeGenResults() {	   
   }
   
   public RestCodeGenResults(String dbVer) {
       if (!NullOrEmpty.nullOrEmpty(dbVer)) {
           if (dbVer.startsWith("11")) {
               m_dbVersion = 11;
           } else if (dbVer.startsWith("12")) {
               m_dbVersion = 12;           
           }
       }
   }
   
   public RestCodeGenResults(int dbVer) {
       m_dbVersion = dbVer;
   }
   
   public RestCodeGenResults(String productName, String productVersion) {
       this(productName, productVersion, DEFAULT_DB_VERSION);
   }
   
   public RestCodeGenResults(String productName, String productVersion, int dbVersion) {
	   m_productName = productName;
	   m_productVersion = productVersion;
	   m_dbVersion = dbVersion;
   }
      
   /**
    * Generated code for the resouce module, templates and its handlers (including parameters).
    * 
    * @param module
    * @param template
    * @param handler
    * @param commit
    * @return
    */
   public String generateModuleTemplateHandler(RestModule module, RestTemplate template, RestResourceHandler handler, boolean commit) {
      StringBuilder sb = new StringBuilder();
      String moduleGen = getDefineModule(module);
      if (moduleGen != null) {
         sb.append(moduleGen).append(NEWLINE);
         String templateGen = getDefineTemplate(module.getName(), template);
         if (templateGen != null) {
            sb.append(templateGen).append(NEWLINE);
            String handlerGen = getDefineHandler(module.getName(), template.getURIPattern(), handler);
            if (handlerGen != null) {
               sb.append(handlerGen).append(NEWLINE);
            }
         }
         if (sb.length() > 0)
            return getBeginStatementsEnd(sb.toString(), commit);
      }
      return null;
   }
   
   public String generateModuleTemplatesHandlers(RestModule module, boolean commit) {
       return generateModuleTemplatesHandlers(module, commit, true);
   }
   
   public String generateModuleTemplatesHandlers(RestModule module, boolean commit, boolean includeBeginEnd) {
      StringBuilder sb = new StringBuilder();
      String moduleGen = getDefineModule(module);
      if (moduleGen != null) {
         sb.append(moduleGen).append(NEWLINE);
         // Include all templates, handlers and parameters
         String templateGen = getDefineTemplates(module, true); 
         if (templateGen != null) {
            sb.append(templateGen).append(NEWLINE);
         }
         if (sb.length() > 0) {
             if (includeBeginEnd) {
                 return getBeginStatementsEnd(sb.toString(), commit);                 
             }
             return sb.toString();             
         }
      }
      return null;
   }
   
   public String generateTemplateHandlers(String moduleName, RestTemplate template, boolean commit) {
      StringBuilder sb = new StringBuilder();
      String templateGen = getDefineTemplate(moduleName, template);
      if (templateGen != null) {
         sb.append(templateGen).append(NEWLINE);
         // Include all handlers and parameters
         String handlerGen = getDefineHandlers(moduleName, template, true); 
         if (handlerGen != null) {
            sb.append(handlerGen).append(NEWLINE);
         }
         if (sb.length() > 0)
            return getBeginStatementsEnd(sb.toString(), commit);
      }
      return null;
   }
   
   /**
    * Generated code for the resource module and its template.
    * 
    * @param module
    * @param template
    * @param commit
    * @return
    */
   public String generateModuleTemplate(RestModule module, RestTemplate template, boolean commit) {
      return generateModuleTemplateHandler(module, template, null, commit);
   }

   /**
    * Generated code for the resource template and its owning module.
    * 
    * @param moduleName
    * @param template
    * @param commit
    * @return
    */
   public String generateTemplate(String moduleName, RestTemplate template, boolean commit) {
      String templateGen = getDefineTemplate(moduleName, template);
      if (templateGen != null) {
         StringBuilder sb = new StringBuilder();
         sb.append(templateGen).append(NEWLINE);
         return getBeginStatementsEnd(sb.toString(), commit);
      }
      return null;
   }
   
   /*
    * Generated code for the resource handler and its parameters.
    * 
    * @param moduleName
    * @param uriTemplate
    * @param handler
    * @param commit
    * @return
    */
   public String generateHandler(String moduleName, String uriTemplate, RestResourceHandler handler, boolean commit) {
      String handlerGen = getDefineHandler(moduleName, uriTemplate, handler);
      if (handlerGen != null) {
         StringBuilder codeGen = new StringBuilder();
         codeGen.append(handlerGen).append(NEWLINE);
         String paramsGen = getDefineParameters(moduleName, uriTemplate, handler);
         if (paramsGen != null) {
            codeGen.append(paramsGen).append(NEWLINE);
         }
         return getBeginStatementsEnd(codeGen.toString(), commit);
      }
      return null;
   }
   
   /**
    * Generated code for the resource template and its owning module.
    * 
    * @param moduleName
    * @param template
    * @param commit
    * @return
    */
   public String generatePrivilege(RestPrivilege privilege, boolean commit) {
      String privilegeGen = getDefinePrivilege(privilege,commit,true);
      return privilegeGen;
   }

   /**
    * Generated code for enabling or disabling a schema.
    * 
    * @param schema
    */
   public String generateEnableSchema(RestSchema schema) {
       String schemaName = schema.getSchemaName();
       String schemaStatus = schema.getSchemaStatus();
       String urlMappingType = schema.getUrlMappingType();
       String urlMappingPattern = schema.getPattern();
       String autoRestAuth = schema.getAutoRestAuth();
       return getEnableSchema(schemaName, schemaStatus, urlMappingType, urlMappingPattern, autoRestAuth)+NEWLINE;
   }
   
   /**
    * Generated code to rest enable or disable a schema.
    * 
    * @param schemaName
    * @param schemaStatus
    * @param urlMappingType
    * @param urlMappingPattern
    * @param autoRestAuth
    * @return generated code
    */
   public String getEnableSchema(String schemaName, String schemaStatus, 
                                 String urlMappingType, String urlMappingPattern, String autoRestAuth) {
      if (!NullOrEmpty.nullOrEmpty(schemaName) && !NullOrEmpty.nullOrEmpty(schemaStatus)) {
         String schemaEnabled = schemaStatus.equalsIgnoreCase(ENABLED) ? TRUE : FALSE;
         String autoRestStatus = (autoRestAuth != null && autoRestAuth.equalsIgnoreCase(ENABLED)) ? TRUE : FALSE;

         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(SCHEMA_NAME, schemaName);
         prop.put(SCHEMA_STATUS, schemaEnabled);
         prop.put(URL_MAPPING_TYPE, urlMappingType);
         prop.put(URL_MAPPING_PATTERN, urlMappingPattern);
         prop.put(AUTO_REST_AUTH, autoRestStatus);

         return RestCodeGenerator.getInstance().getTemplate(ENABLE_SCHEMA, prop);
      }
      return null;
   }
   
   /**
    * Generated code to create or edit the module.
    * 
    * @param module
    * @return generated code
    */
   public String getDefineModule(RestModule module) {
      if (module != null) {
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, module.getName());
         prop.put(BASEPATH, module.getURIPrefix());
         prop.put(ITEMS_PER_PAGE, Integer.valueOf((int) module.getPaginationSize()));
         prop.put(STATUS, getPublished(module.isPublished()));
         prop.put(COMMENTS, module.getComments());
         String moduleGen = RestCodeGenerator.getInstance().getTemplate(DEFINE_MODULE, prop);
         
         StringBuilder sb = new StringBuilder();
         if (moduleGen != null) {
             sb.append(moduleGen);
             if (module.getOrigins() != null && module.getOrigins().size() > 0) {
                 String originsGen = getModuleOriginsAllowed(module);
                 if (originsGen != null) {
                     sb.append(NEWLINE).append(originsGen);
                 }
             }
             if (sb.length() > 0) {
                 return sb.toString();
             }
         }
      }
      return null;
   }
   
   /**
    * Convenience method to generate code to delete a module.
    * 
    * @param module
    * @return
    */
   public String getDeleteModule(RestModule module) {
      if (module != null) {
         return getDeleteModule(module.getName());
      }
      return null;
   }   

   public String getDeleteModule(String moduleName, boolean commit) {
      if (!NullOrEmpty.nullOrEmpty(moduleName)) {
         String modGen = getDeleteModule(moduleName);
         StringBuilder sb = new StringBuilder();
         sb.append(modGen).append(NEWLINE);
         return getBeginStatementsEnd(sb.toString(), commit);
      }
      return null;
   }
   
   public String getRenameModule(String moduleName, String newName, String newUriPrefix, boolean commit) {
       if (!NullOrEmpty.nullOrEmpty(moduleName)) {
          String modGen = getRenameModule(moduleName, newName, newUriPrefix);
          StringBuilder sb = new StringBuilder();
          sb.append(modGen).append(NEWLINE);
          return getBeginStatementsEnd(sb.toString(), commit);
       }
       return null;
    }
   

   public String getRenamePrivilege(String oldName, String newName, boolean commit) {
       if (!NullOrEmpty.nullOrEmpty(oldName) && !NullOrEmpty.nullOrEmpty(newName)) {
          String privGen = getRenamePrivilege(oldName, newName);
          StringBuilder sb = new StringBuilder();
          sb.append(privGen).append(NEWLINE);
          return getBeginStatementsEnd(sb.toString(), commit);
       }
       return null;
    }
   
   public String getDeletePrivilege(String privilegeName, boolean commit) {
      if (!NullOrEmpty.nullOrEmpty(privilegeName)) {
         String privGen = getDeletePrivilege(privilegeName);
         StringBuilder sb = new StringBuilder();
         sb.append(privGen).append(NEWLINE);
         return getBeginStatementsEnd(sb.toString(), commit);
      }
      return null;
   }
   
   public String getCreateRole(String roleName, boolean commit) {
       if (!NullOrEmpty.nullOrEmpty(roleName)) {
          String roleGen = getCreateRole(roleName);
          StringBuilder sb = new StringBuilder();
          sb.append(roleGen).append(NEWLINE);
          return getBeginStatementsEnd(sb.toString(), commit);
       }
       return null;
    }
   
   public String getCreateRoles(List<String> roles, boolean commit) {
       return getCreateRoles(roles, true, true);
   }
      
   public String getCreateRoles(List<String> roles, boolean commit, boolean includeBeginEnd) {
       if (roles != null && !roles.isEmpty()) {
           StringBuilder sb = new StringBuilder();
           for (String roleName : roles) {
               String roleGen = getCreateRole(roleName);
               sb.append(roleGen).append(NEWLINE);
           }
           if (includeBeginEnd) {
              return getBeginStatementsEnd(sb.toString(), commit);
           }
           return sb.toString();
       }
       return null;
   }
   
   public String getRenameRole(String oldName, String newName, boolean commit) {
       if (!NullOrEmpty.nullOrEmpty(oldName) && !NullOrEmpty.nullOrEmpty(newName)) {
          String roleGen = getRenameRole(oldName, newName);
          StringBuilder sb = new StringBuilder();
          sb.append(roleGen).append(NEWLINE);
          return getBeginStatementsEnd(sb.toString(), commit);
       }
       return null;
    }
   
      
   public String getDeleteRole(String roleName, boolean commit) {
       if (!NullOrEmpty.nullOrEmpty(roleName)) {
          String roleGen = getDeleteRole(roleName);
          StringBuilder sb = new StringBuilder();
          sb.append(roleGen).append(NEWLINE);
          return getBeginStatementsEnd(sb.toString(), commit);
       }
       return null;
   }
   
   /**
    * Generated code to delete the module.
    * 
    * @param module
    * @return generated code
    */
   public String getDeleteModule(String moduleName) {
      if (!NullOrEmpty.nullOrEmpty(moduleName)) {
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, moduleName);
         return RestCodeGenerator.getInstance().getTemplate(DELETE_MODULE, prop);
      }
      return null;
   }
   
   /**
    * Generated code to rename the module or its base path.
    * 
    * @param module
    * @param newName
    * @return generated code
    */
   public String getRenameModule(String moduleName, String newName, String newUriPrefix) {
      if (!NullOrEmpty.nullOrEmpty(moduleName)) {
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, moduleName);
         prop.put(NEW_NAME, newName);
         prop.put(BASEPATH, newUriPrefix);

         return RestCodeGenerator.getInstance().getTemplate(RENAME_MODULE, prop);
      }
      return null;
   }
   
   /**
    * Generated code to set the origins that are allowed to access this module.
    * 
    * @param module
    * @return generated code
    */
   public String getModuleOriginsAllowed(RestModule module) {
      if ((module != null)) {
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, module.getName());
         prop.put(ORIGINS_ALLOWED, formatCommaDelimited(module.getOrigins()));

         return RestCodeGenerator.getInstance().getTemplate(SET_MODULE_ORIGINS_ALLOWED, prop);
      }
      return null;
   }   

   /**
    * Generated code to set the module's status if it is published or not.
    * 
    * @param module
    * @return generated code
    */
   public String getPublishedModule(RestModule module) {
      if (module != null) {
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, module.getName());
         prop.put(STATUS, getPublished(module.isPublished()));

         return RestCodeGenerator.getInstance().getTemplate(SET_PUBLISH_MODULE, prop);
      }
      return null;
   }

   /**
    * Generated code to create or edit multiple templates.
    * 
    * @param module
    * @return generated code
    */
   public String getDefineTemplates(RestModule module, boolean includeHandlers) {
      if (module != null && module.getTemplates() != null && !module.getTemplates().isEmpty()) {
         StringBuilder sb = new StringBuilder();
         for (RestTemplate template: module.getTemplates()) {
            String templateGen = getDefineTemplate(module.getName(), template);
            if (templateGen != null) {
               sb.append(templateGen).append(NEWLINE);
               if (includeHandlers) {
                  String handlerGen = getDefineHandlers(module.getName(), template, true);
                  if (handlerGen != null) {
                     sb.append(handlerGen);
                  }
               }
            }
         }
         return sb.toString();
      }
      return null;
   }
   
   /**
    * Generated code to create or edit a template.
    * 
    * @param moduleName
    * @param template
    * @return generated code
    */
   public String getDefineTemplate(String moduleName, RestTemplate template) {
      if (!NullOrEmpty.nullOrEmpty(moduleName) && template != null) {

         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, moduleName);
         prop.put(PATTERN, template.getURIPattern());
         prop.put(PRIORITY, Integer.valueOf(template.getPriority()));
         prop.put(ETAG_TYPE, getEntityTag(template.getEntityTag()));
         prop.put(ETAG_QUERY, template.getEntityTagQuery());
         prop.put(COMMENTS, template.getComments());

         return RestCodeGenerator.getInstance().getTemplate(DEFINE_TEMPLATE, prop);
      }
      return null;
   }
   
   /**
    * Generated code to create or edit multiple handlers.
    * 
    * @param moduleName
    * @param template
    * @return generated code
    */
   public String getDefineHandlers(String moduleName, RestTemplate template, boolean includeParams) {
      if (!NullOrEmpty.nullOrEmpty(moduleName) && template != null && !NullOrEmpty.nullOrEmpty(template.getURIPattern())) {
         Map<HTTP_METHOD_TYPE, RestResourceHandler> handlers = template.getResourceHandlers();
         if (handlers != null && !handlers.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (HTTP_METHOD_TYPE key: handlers.keySet()) {
               RestResourceHandler handler = handlers.get(key);
               String handlerGen = getDefineHandler(moduleName, template.getURIPattern(), handler);
               if (handlerGen != null) {
                  sb.append(handlerGen).append(NEWLINE);
                  if (includeParams) {
                     String paramGen = getDefineParameters(moduleName, template.getURIPattern(), handler);
                     if (paramGen != null) {
                        sb.append(paramGen);                        
                     }
                  }
               }
            }
            return sb.toString();
         }
      }
      return null;
   }
   
   /**
    * Generated code to create or edit a handler.
    * 
    * @param moduleName
    * @param templatePattern
    * @param handler
    * @return generated code
    */
   public String getDefineHandler(String moduleName, String templatePattern, RestResourceHandler handler) {
      if (!NullOrEmpty.nullOrEmpty(moduleName) && !NullOrEmpty.nullOrEmpty(templatePattern) && handler != null) {
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, moduleName);
         prop.put(PATTERN, templatePattern);
         prop.put(METHOD, handler.getMethodType().getType().toUpperCase());
         prop.put(SOURCE_TYPE, handler.getSourceTypeGenerator());
         // Update the source if it contains single quote or source greater than 4K for 11g
         String source = getSqlSource(handler.getSQL());
         prop.put(SOURCE, source);
         prop.put(ITEMS_PER_PAGE, Integer.valueOf((int) handler.getPaginationSize()));
         prop.put(MIMES_ALLOWED, formatCommaDelimited(handler.getMimeTypes()));
         prop.put(COMMENTS, handler.getComments());
         
         return RestCodeGenerator.getInstance().getTemplate(DEFINE_HANDLER, prop);
      }
      return null;
   }
   
   private String getSqlSource(String sql) {
       if ((sql == null) || sql.isEmpty() || sql.trim().isEmpty()) return EMPTY;
       
       String source = sql;       
       // Replace a single quote with 2 single quotes
       source = source.replace("'", "''");
       StringBuilder sb = new StringBuilder();

       if ((source.length() <= MAX_STRING_LIMIT) || 
           (m_dbVersion >= 12 && !m_backwardsCompatible)) {
           sb.append(SINGLE_QUOTE).append(source).append(SINGLE_QUOTE);
           return sb.toString();
       } else {
           // 11g and String size > 4K
           // Split the string using concat operator
           int strSize = source.length();
           int loopNum = strSize / MAX_STRING_SIZE;
                      
           int begNdx = 0;
           int endNdx = 0;
           for (int i=0; i<loopNum; i++) {
               endNdx = endNdx + MAX_STRING_SIZE;
               // Only split at a non-single quote
               int cnt = 0;
               int tmpNdx = 0;
               boolean found = false;
               String val = "";
               while (!found && (cnt<=MAX_BUF_LIMIT) && ((endNdx+cnt) < strSize)) {
                   tmpNdx = endNdx+cnt;
                   val = source.substring(tmpNdx,tmpNdx+1);
                   if (val.equals(SINGLE_QUOTE)) {
                       // increment
                       ++cnt;
                   } else {
                       found = true;
                   }
               }
               if (found) {
                  endNdx = endNdx+cnt;
               } else {
                  // Hit the limit of 4K.  Cannot find a non-single quote. 
                  // User will need to fix the source code in the export file.
               }
               
               sb.append(SINGLE_QUOTE).append(source.substring(begNdx, endNdx)).append(SINGLE_QUOTE);
               if (endNdx < strSize) {
                   sb.append(NEWLINE).append(CONCAT_OPERATOR);
               }
               begNdx = endNdx;
           } // for
           
           // Remaining string
           if (endNdx < strSize) {
               sb.append(SINGLE_QUOTE).append(source.substring(begNdx, strSize)).append(SINGLE_QUOTE);               
           }
           
           return sb.toString();
       }       
   }
   
   /**
    * Generated code to create or edit multiple parameters.
    * 
    * @param moduleName
    * @param templatePattern
    * @param handler
    * @return generated code
    */
   public String getDefineParameters(String moduleName, String templatePattern, RestResourceHandler handler) {
      
      if (!NullOrEmpty.nullOrEmpty(moduleName) && !NullOrEmpty.nullOrEmpty(templatePattern) && handler != null) {

         String method = handler.getMethodType().getType();
         List<RestHandlerParameter> parameters = handler.getParameters();
         
         if (method != null && parameters != null && !parameters.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (RestHandlerParameter param: parameters) {
               sb.append(getDefineParameter(moduleName, templatePattern, method, param)).append(NEWLINE);
            }
            return sb.toString();
         }
      }
      return null;
   }
      
   /**
    * Generated code to create or edit a parameter.
    * 
    * @param moduleName
    * @param templatePattern
    * @param method
    * @param parameter
    * @return
    */
   public String getDefineParameter(String moduleName, String templatePattern, 
                                    String method, RestHandlerParameter parameter) {
      if (!NullOrEmpty.nullOrEmpty(moduleName) && !NullOrEmpty.nullOrEmpty(templatePattern) && !NullOrEmpty.nullOrEmpty(method) && parameter != null) {

         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(MODULE_NAME, moduleName);
         prop.put(PATTERN, templatePattern);
         prop.put(METHOD, method.toUpperCase()); // Possible values: GET, POST, PUT, DELETE
         prop.put(NAME, parameter.getName());
         prop.put(BIND_VAR_NAME, parameter.getBindVariable());
         prop.put(SOURCE_TYPE, getSourceType(parameter.getSourceType()));
         prop.put(PARAM_TYPE, getDataType(parameter.getDataType()));
         prop.put(ACCESS_METHOD, getAccessMethod(parameter.getAccessMethod()));
         prop.put(COMMENTS, parameter.getComments());

         return RestCodeGenerator.getInstance().getTemplate(DEFINE_PARAMETER, prop);
      }
      return null;
   }
   
   public String getDefinePrivilege(RestPrivilege privilege, boolean commit, boolean includeBeginEnd) {
       if (privilege != null) {
           List<RestPrivilege> privs = new ArrayList<RestPrivilege>();
           privs.add(privilege);
           return getDefinePrivileges(privs, commit, includeBeginEnd);
       }
       return null;     
    }
   
   public String getDefinePrivileges(List<RestPrivilege> privileges, boolean commit) {
       return getDefinePrivileges(privileges, commit, true);
   }
   
   public String getDefinePrivileges(List<RestPrivilege> privileges, boolean commit, boolean includeBeginEnd) {
       if (privileges != null && !privileges.isEmpty()) {
           StringBuilder sbArray = new StringBuilder();
           //Map<String,Object> arrayProp = new HashMap<String, Object>();
           HashMap<String, Object> prop = new HashMap<String, Object>();
           String code = "";

           for (int p=0; p<privileges.size(); p++) {
               RestPrivilege privilege = privileges.get(p);

               if (p != 0 && privileges.size() > 1) {
                   // Add delete here to initialize the array
                   prop.clear();
                   code = RestCodeGenerator.getInstance().getTemplate(DELETE_ELEMENTS, prop);
                   sbArray.append(code).append(NEWLINE);                              
               }               
               // Roles, Modules and Patterns use the owa.var_arr for their parameter data type

               if (privilege.getRoles() != null && !privilege.getRoles().isEmpty()) {
                   prop.clear();
                   List<String> roles = privilege.getRoles();
                   for (int i=0; i<roles.size(); i++) {
                       prop.put(INDEX,Integer.valueOf(i+1));
                       prop.put(VALUE,roles.get(i));
                       code = RestCodeGenerator.getInstance().getTemplate(ROLE_ARRAY, prop);
                       sbArray.append(code).append(NEWLINE);                              
                   }
               }
               if (privilege.getModules() != null && !privilege.getModules().isEmpty()) {
                   prop.clear();
                   List<String> modules = privilege.getModules();
                   for (int i=0; i<modules.size(); i++) {
                       prop.put(INDEX,Integer.valueOf(i+1));
                       prop.put(VALUE,modules.get(i));
                       code = RestCodeGenerator.getInstance().getTemplate(MODULE_ARRAY, prop);
                       sbArray.append(code).append(NEWLINE);                              
                   }            
               }
               if (privilege.getURIPatterns() != null && !privilege.getURIPatterns().isEmpty()) {
                   prop.clear();
                   List<String> patterns = privilege.getURIPatterns();
                   for (int i=0; i<patterns.size(); i++) {
                       prop.put(INDEX,Integer.valueOf(i+1));
                       prop.put(VALUE,patterns.get(i));
                       code = RestCodeGenerator.getInstance().getTemplate(PATTERN_ARRAY, prop);
                       sbArray.append(code).append(NEWLINE);                              
                   }            
               }

               prop.clear();
               prop.put(NAME, privilege.getName());
               prop.put(LABEL, privilege.getTitle());
               prop.put(DESCRIPTION, privilege.getDescription());
               prop.put(COMMENTS, privilege.getComments());
               code = RestCodeGenerator.getInstance().getTemplate(DEFINE_PRIVILEGE, prop);
               sbArray.append(code).append(NEWLINE);
               
           } // for

           if (includeBeginEnd) {
               return getPrivilegeBeginEnd(sbArray.toString(), commit) + NEWLINE;             
           } else {
               return sbArray.toString();
           }
       }
       return null;     
   }
   
   public String getPrivilegeBeginEnd(String statements, boolean commit) {
       if (!NullOrEmpty.nullOrEmpty(statements)) {
           HashMap<String, Object> prop = new HashMap<String, Object>();
           prop.put(STATEMENTS, statements);
           prop.put(COMMIT, Boolean.valueOf(commit));
           return RestCodeGenerator.getInstance().getTemplate(PRIV_BEGIN_END, prop);           
       }
       return null;
   }
   
   public String getRenamePrivilege(String name, String newName) {
      if (!NullOrEmpty.nullOrEmpty(name) && !NullOrEmpty.nullOrEmpty(newName)) {
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, name);
         prop.put(NEW_NAME, newName);

         return RestCodeGenerator.getInstance().getTemplate(RENAME_PRIVILEGE, prop);
      }
      return null;
   }
   
   public String getDeletePrivilege(String privilegeName) {
      if (!NullOrEmpty.nullOrEmpty(privilegeName)) {
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, privilegeName);
         return RestCodeGenerator.getInstance().getTemplate(DELETE_PRIVILEGE, prop);
      }
      return null;
   }
   
   
   public String getCreateRole(String roleName) {
      if (!NullOrEmpty.nullOrEmpty(roleName)) {         
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, roleName);

         return RestCodeGenerator.getInstance().getTemplate(CREATE_ROLE, prop);
      }
      return null;
     
   }

   public String getRenameRole(String name, String newName) {
      if (!NullOrEmpty.nullOrEmpty(name) && !NullOrEmpty.nullOrEmpty(newName)) {         
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, name);
         prop.put(NEW_NAME, newName);

         return RestCodeGenerator.getInstance().getTemplate(RENAME_ROLE, prop);
      }
      return null;
     
   }
   
   public String getDeleteRole(String roleName) {
      if (!NullOrEmpty.nullOrEmpty(roleName)) {         
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(NAME, roleName);

         return RestCodeGenerator.getInstance().getTemplate(DELETE_ROLE, prop);
      }
      return null;
     
   }
   
   public String getBeginStatementsEnd(String code, boolean commit) {
      if (code != null) {
         HashMap<String, Object> prop = new HashMap<String, Object>();
         prop.put(STATEMENTS, code);
         prop.put(COMMIT, Boolean.valueOf(commit));
         return RestCodeGenerator.getInstance().getTemplate(BEGIN_STATEMENTS_END, prop);
      }
      return null;
   }

    public String getCommentHeader(String pSchema, String pOrdsSchemaVer) {
        String schema = !NullOrEmpty.nullOrEmpty(pSchema) ? pSchema : "";
        String ordsSchemaVer = !NullOrEmpty.nullOrEmpty(pOrdsSchemaVer) ? pOrdsSchemaVer : "";
        HashMap<String, Object> prop = new HashMap<String, Object>();
        prop.put(SQLDEV_PROD_NAME, getProductName());
        prop.put(SQLDEV_PROD_VER, getProductVersion());
        prop.put(DATE_TIME, Calendar.getInstance().getTime());
        prop.put(SCHEMA_NAME, schema);
        prop.put(ORDS_SCHEMA_VER, ordsSchemaVer);
        return RestCodeGenerator.getInstance().getTemplate(EXPORT_COMMENT_HEADER, prop);
    }
   
   public void setProductName(String productName) {
	   m_productName = productName;
   }

   public String getProductName() {
	   return !NullOrEmpty.nullOrEmpty(m_productName) ? m_productName : "";
   }
   
   public void setProductVersion(String productVersion) {
	   m_productVersion = productVersion;
   }

   public String getProductVersion() {
	   return !NullOrEmpty.nullOrEmpty(m_productVersion) ? m_productVersion : "";
   }
      
   private String getEntityTag(RestTemplate.ENTITY_TAG_TYPE entityTag) {
      String value = RestTemplate.ENTITY_TAG_HASH;
      if (entityTag != null && entityTag != ENTITY_TAG_TYPE.SECURE_HASH) {
         value = entityTag.getType();
      }
      return value.toUpperCase();
   }

   private String getSourceType(RestHandlerParameter.SOURCE_TYPE sourceType) {
      String value = RestHandlerParameter.SOURCE_TYPE_HEADER;
      if (sourceType != null && sourceType != RestHandlerParameter.SOURCE_TYPE.HTTP_HEADER) {
         value = sourceType.getType();
      }
      return value.toUpperCase();
   }
   
   private String getDataType(RestHandlerParameter.DATA_TYPE dataType) {
      String value = RestHandlerParameter.DATA_TYPE_STRING;
      if (dataType != null && dataType != RestHandlerParameter.DATA_TYPE.STRING) {
         if (dataType == RestHandlerParameter.DATA_TYPE.INTEGER) {
            value = DATA_TYPE_INT;
         } else {
            value = dataType.getType();
         }
      }
      return value.toUpperCase();
   }

   private String getAccessMethod(RestHandlerParameter.ACCESS_METHOD_TYPE accessMethod) {
      String value = RestHandlerParameter.ACCESS_METHOD_IN;
      if (accessMethod != null && accessMethod != RestHandlerParameter.ACCESS_METHOD_TYPE.IN) {
         value = accessMethod.getType();
      }
      return value.toUpperCase();
   }
   
   private String getPublished(boolean published) {
      if (published) {
         return PUBLISHED;
      }
      return NOT_PUBLISHED;
   }

   private String formatCommaDelimited(List<String> values) {
      if (values != null && !values.isEmpty()) {
         StringBuilder sb = new StringBuilder();
         for (int i = 0; i < values.size(); i++) {
            if (i == 0) {
               sb.append(values.get(i));
            } else {
               sb.append(",").append(values.get(i));
            }
         }
         return sb.toString();
      }
      return EMPTY;
   }

   public void setBackwardsCompatible(boolean indicator) {
       m_backwardsCompatible = indicator;
   }
}
