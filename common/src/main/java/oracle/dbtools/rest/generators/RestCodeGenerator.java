/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.rest.generators;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.stringtemplate.v4.NoIndentWriter;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import oracle.dbtools.rest.RestMessages;

/**
 * @author Elizabeth Saunders
 * @since  4.2
 *
 */
public class RestCodeGenerator {

   private volatile static RestCodeGenerator _instance;

   // Support ORDS 3.0.5 and later
   private static final String ORDS_GROUP_FILE = "oracle/dbtools/rest/generators/templates/ordsgen.stg"; //$NON-NLS-1$

   private STGroup m_templateGroup;

   public static RestCodeGenerator getInstance() {
      if (_instance == null) {
         synchronized (RestCodeGenerator.class) {
            if (_instance == null) {
               _instance = new RestCodeGenerator();
            }
         }
      }
      return _instance;
   }

   private RestCodeGenerator() {
   }

   protected STGroup getTemplateGroup() {
      if (m_templateGroup == null) {
          ClassLoader ctxLoader = Thread.currentThread().getContextClassLoader();
          Thread.currentThread().setContextClassLoader(RestCodeGenerator.class.getClassLoader());
          try {         
              m_templateGroup = new STGroupFile(ORDS_GROUP_FILE);
          } catch (Throwable e) {
              //e.printStackTrace();
              Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage(),e);
          } finally {
              Thread.currentThread().setContextClassLoader(ctxLoader);
          }
      }
      return m_templateGroup;
   }

   protected String getTemplate(String templateName, Map<String, Object> properties) {
      if (getTemplateGroup() != null) {
          ST st = getTemplateGroup().getInstanceOf(templateName);
          if (st != null) {
              Set<String> keys = properties.keySet();
              for (String k : keys) {
                  st.add(k, properties.get(k));
              }

              // Do not allow StringTemplate to auto-indent 
              NoIndentWriter out = new NoIndentWriter(new StringWriter());
              try {
                  st.write(out);
              } catch (IOException e) {
                  Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage(),e);
              }

              return st.render();
          } else {
              Logger.getLogger(getClass().getName()).log(Level.WARNING, RestMessages.format(RestMessages.GEN_TEMPLATE_NOT_FOUND, templateName));              
          }
          
      } else {
          Logger.getLogger(getClass().getName()).log(Level.WARNING, RestMessages.format(RestMessages.GEN_GROUP_FILE_NOT_FOUND, ORDS_GROUP_FILE));                        
      }
      return null;
   }
}
