/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.rest.model;

import oracle.dbtools.data.common.NullOrEmpty;
import oracle.dbtools.rest.RestMessages;

/**
 * @author Elizabeth Saunders
 * @since  4.0
 *
 */
public class RestHandlerParameter {

    public static final String ACCESS_METHOD_IN    = "IN";       //$NON-NLS-1$
    public static final String ACCESS_METHOD_INOUT = "INOUT";    //$NON-NLS-1$
    public static final String ACCESS_METHOD_OUT   = "OUT";      //$NON-NLS-1$
    
    // If the parameter source type is HTTP Header, then the access type indicates the following:
    //    IN - value will be present in the HTTP request
    //    IN/OUT - value will be present in the HTTP request and HTTP response
    //    OUT - value will be present in the HTTP response
    public static enum ACCESS_METHOD_TYPE {
        IN(ACCESS_METHOD_IN, RestMessages.getString(RestMessages.ACCESS_METHOD_IN)),
        OUT(ACCESS_METHOD_OUT, RestMessages.getString(RestMessages.ACCESS_METHOD_OUT)),
        IN_OUT(ACCESS_METHOD_INOUT, RestMessages.getString(RestMessages.ACCESS_METHOD_INOUT));

        private String type;
        private String nlsName;

        ACCESS_METHOD_TYPE(String type, String nlsName) {
            this.type    = type;
            this.nlsName = nlsName; 
        }

        public String getType() {
            return type;
        }

        public String getNLS() {
            return nlsName;        
        }
    }

    public static final String SOURCE_TYPE_HEADER  = "header";  //$NON-NLS-1$
    public static final String SOURCE_TYPE_URI     = "uri"; //$NON-NLS-1$
    public static final String SOURCE_TYPE_RESPONSE= "response"; //$NON-NLS-1$

    // If the parameter source type is URI, parameter will be present in the URI template;
    // otherwise, it will be present in the request header.
    public static enum SOURCE_TYPE {
        HTTP_HEADER(SOURCE_TYPE_HEADER, RestMessages.getString(RestMessages.SOURCE_TYPE_HEADER)),
        URI_TEMPLATE(SOURCE_TYPE_URI, RestMessages.getString(RestMessages.SOURCE_TYPE_URI)),
        RESPONSE(SOURCE_TYPE_RESPONSE, RestMessages.getString(RestMessages.SOURCE_TYPE_RESPONSE));

        private String type;
        private String nlsName;

        SOURCE_TYPE(String type, String nlsName) {
            this.type    = type;
            this.nlsName = nlsName; 
        }

        public String getType() {
            return type;
        }

        public String getNLS() {
            return nlsName;        
        }
    }

    public static final String DATA_TYPE_STRING    = "String";    //$NON-NLS-1$
    public static final String DATA_TYPE_INTEGER   = "Integer";   //$NON-NLS-1$
    public static final String DATA_TYPE_INT       = "Int";   //$NON-NLS-1$
    public static final String DATA_TYPE_LONG      = "Long";      //$NON-NLS-1$
    public static final String DATA_TYPE_DOUBLE    = "Double";    //$NON-NLS-1$
    public static final String DATA_TYPE_BOOLEAN   = "Boolean";   //$NON-NLS-1$
    public static final String DATA_TYPE_TIMESTAMP = "Timestamp"; //$NON-NLS-1$
    public static final String DATA_TYPE_RESULTSET = "Resultset"; //$NON-NLS-1$
    
    public static enum DATA_TYPE {
        STRING(DATA_TYPE_STRING, RestMessages.getString(RestMessages.DATA_TYPE_STRING)),
        INTEGER(DATA_TYPE_INTEGER, RestMessages.getString(RestMessages.DATA_TYPE_INTEGER)),
        LONG(DATA_TYPE_LONG, RestMessages.getString(RestMessages.DATA_TYPE_LONG)),
        DOUBLE(DATA_TYPE_DOUBLE, RestMessages.getString(RestMessages.DATA_TYPE_DOUBLE)),
        BOOLEAN(DATA_TYPE_BOOLEAN, RestMessages.getString(RestMessages.DATA_TYPE_BOOLEAN)),
        TIMESTAMP(DATA_TYPE_TIMESTAMP, RestMessages.getString(RestMessages.DATA_TYPE_TIMESTAMP)),
        RESULTSET(DATA_TYPE_RESULTSET, RestMessages.getString(RestMessages.DATA_TYPE_RESULTSET));

        private String type;
        private String nlsName;

        DATA_TYPE(String type, String nlsName) {
            this.type    = type;
            this.nlsName = nlsName; 
        }

        public String getType() {
            return type;
        }

        public String getNLS() {
            return nlsName;        
        }
    }
            
    private String _name;
    private String _bindVariable;
    private ACCESS_METHOD_TYPE _accessMethod;
    private SOURCE_TYPE _sourceType;
    private DATA_TYPE _dataType;
    private String _comments;

    
    /**
     * Get the parameter name.
     * 
     * @return the parameter name
     */
    public String getName() {
        return _name;
    }
    
    /**
     * Set the parameter name.
     * 
     * @param name the parameter name
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Get the parameter bind variable.
     * 
     * @return the bind variable.
     */
    public String getBindVariable() {
        return _bindVariable;
    }

    /**
     * Set the parameter bind variable.
     * 
     * @param bindVariable the bind variable
     */
    public void setBindVariable(String bindVariable) {
        _bindVariable = bindVariable;
    }

    /**
     * Get the parameter data type.
     * 
     * @return the data type.  Possible enum values are STRING, INTEGER, DOUBLE, LONG, TIMESTAMP
     */
    public DATA_TYPE getDataType() {
        return _dataType;
    }

    /**
     * Set the parameter data type.
     * 
     * @param dataType the enum DATA_TYPE
     */
    public void setDataType(DATA_TYPE dataType) {
        _dataType = dataType;
    }

    public void setDataType(String dataType) {
       if (!NullOrEmpty.nullOrEmpty(dataType)) {
             if (dataType.equalsIgnoreCase(DATA_TYPE_STRING)) {
                _dataType = DATA_TYPE.STRING;
             } else if (dataType.equalsIgnoreCase(DATA_TYPE_INTEGER) ||
                     dataType.equalsIgnoreCase(DATA_TYPE_INT)) {
                _dataType = DATA_TYPE.INTEGER;
             } else if (dataType.equalsIgnoreCase(DATA_TYPE_LONG)) {
                _dataType = DATA_TYPE.LONG;
             } else if (dataType.equalsIgnoreCase(DATA_TYPE_DOUBLE)) {
                _dataType = DATA_TYPE.DOUBLE;
             } else if (dataType.equalsIgnoreCase(DATA_TYPE_BOOLEAN)) {
                _dataType = DATA_TYPE.BOOLEAN;
             } else if (dataType.equalsIgnoreCase(DATA_TYPE_TIMESTAMP)) {
                _dataType = DATA_TYPE.TIMESTAMP;
             } else if (dataType.equalsIgnoreCase(DATA_TYPE_RESULTSET)) {
                _dataType = DATA_TYPE.RESULTSET;
             } else  {
                _dataType = DATA_TYPE.STRING;                
             }
       }
   }
    

    /** 
     * Get the parameter access method.
     * 
     * @return the access method type.  Possible enum values are IN, IN_OUT, OUT
     */
    public ACCESS_METHOD_TYPE getAccessMethod() {
        return _accessMethod;
    }

    /**
     * Set the parameter access method.
     * 
     * @param accessMethod
     */
    public void setAccessMethod(ACCESS_METHOD_TYPE accessMethod) {
        _accessMethod = accessMethod;
    }


   /**
    * Set the parameter access method.
    * 
    * @param p_accessMethod
    */
   public void setAccessMethod(String p_accessMethod) {
      if (!NullOrEmpty.nullOrEmpty(p_accessMethod)) {
         String accessMethod = p_accessMethod.trim().toUpperCase();
         switch (accessMethod) {
            case ACCESS_METHOD_IN:
               _accessMethod = ACCESS_METHOD_TYPE.IN;
               break;
            case ACCESS_METHOD_INOUT:
               _accessMethod = ACCESS_METHOD_TYPE.IN_OUT;
               break;
            case ACCESS_METHOD_OUT:
               _accessMethod = ACCESS_METHOD_TYPE.OUT;
               break;
            default:
               _accessMethod = ACCESS_METHOD_TYPE.IN;
         }
      }
   }
    
    /**
     * Get the parameter source type.
     * 
     * @return the source type.  Possible enum values are HTTP_HEADER and URI
     */
    public SOURCE_TYPE getSourceType() {
        return _sourceType;
    }

    /**
     * Set the parameter source type.
     * 
     * @param sourceType
     */
    public void setSourceType(SOURCE_TYPE sourceType) {
        _sourceType = sourceType;
    }               

    /**
    * Set the parameter source type.
    * 
    * @param p_sourceType
    */
   public void setSourceType(String p_sourceType) {
       if (!NullOrEmpty.nullOrEmpty(p_sourceType)) {
          String sourceType = p_sourceType.trim().toLowerCase();
          switch (sourceType) {
             case SOURCE_TYPE_HEADER:
                _sourceType = SOURCE_TYPE.HTTP_HEADER;
                break;
             case SOURCE_TYPE_RESPONSE:
                _sourceType = SOURCE_TYPE.RESPONSE;
                break;
             case SOURCE_TYPE_URI:
                _sourceType = SOURCE_TYPE.URI_TEMPLATE;
                break;
             default:
                _sourceType = SOURCE_TYPE.HTTP_HEADER;
          }
       }
   }               

   /**
    * @return comments
    */
   public String getComments() {
       return _comments;
   }

   /**
    * @param comments
    */
   public void setComments(String comments) {
       _comments = comments;
   }        
}
