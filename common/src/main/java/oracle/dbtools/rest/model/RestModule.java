/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.rest.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Elizabeth Saunders
 * @since  4.0
 *
 */
public class RestModule {

    private static final String PUBLISHED = "PUBLISHED";
    private String _name;
    private String _uriPrefix;
    private String _privilege;
    private String _schema;
    private String _URL;
    private String _eTag;

    private boolean _published;
    private long _paginationSize;
    private List<String> _origins;
    private List<RestTemplate> _templates;
    private String _comments;

    
    
    /**
     * Get the resource module name.
     * 
     * @return resource module name
     */
    public String getName() {
        return _name;
    }
    
    /**
     * Set the resource module name.
     * 
     * @param name
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Get the URI prefix
     * 
     * @return the URI prefix
     */
    public String getURIPrefix() {
        return _uriPrefix;
    }

    /**
     * Set the URI prefix
     * 
     * @param uriPrefix
     */
    public void setURIPrefix(String uriPrefix) {
        _uriPrefix = uriPrefix;
    }

    /**
     * Get the security privilege.
     * 
     * @return the security privilege
     */
    public String getPrivilege() {
        return _privilege;
    }

    /**
     * Set the security privilege.
     * 
     * @param privilege
     */
    public void setPrivilege(String privilege) {
        _privilege = privilege;
    }

    /**
     * Indicator if the resource module can be available for use.
     * 
     * @return true if published; otherwise, return false.
     */
    public boolean isPublished() {
        return _published;
    }

    /**
     * Set if the resource module can be available for use.
     * 
     * @param published true published, false do not published.
     */
    public void setPublished(boolean published) {
        _published = published;
    }

    public void setPublished(String published) {
       if ((published != null) && !published.isEmpty() && published.toUpperCase().equals(PUBLISHED)) {
          _published = true;
       } else {
          _published = false;
       }
   }
    
    /**
     * Provides the default value for the Resource Handler's pagination size.
     * 
     * @return pagination size.
     */
    public long getPaginationSize() {
        return _paginationSize;
    }

    /**
     * The default pagination size that will be used by the Resource Handler.
     * 
     * @param paginationSize
     */
    public void setPaginationSize(long paginationSize) {
        _paginationSize = paginationSize;
    }

    /**
     * The list of origins allowed to access the resource templates
     * 
     * @return
     */
    public List<String> getOrigins() {
        return _origins;
    }

    /**
     * Set the list of origins allowed to access the resource templates
     * 
     * @param origins
     */
    public void setOrigins(List<String> origins) {
        _origins = origins;
    }

    /**
     * Get a list of the resource templates that is owned by this module.
     * 
     * @return
     */
    public List<RestTemplate> getTemplates() {
        return _templates;
    }

    /**
     * Set a list of the resource templates.
     * 
     * @param templates
     */
    public void setTemplates(List<RestTemplate> templates) {
        _templates = templates;
    }
    
    /**
     * Add a resource template to this resource module.
     * 
     * @param template
     */
    public void addTemplate(RestTemplate template) {
        if (_templates == null) {
            _templates = new ArrayList<RestTemplate>();
        }
        _templates.add(template);
    }
    
    /**
     * Remove the resource template from this module.
     * 
     * @param template
     * @return
     */
    public boolean removeTemplate(RestTemplate template) {
        if ( (template != null) && (_templates != null) ) {
            boolean found = _templates.remove(template);
            return found;
        }
        return false;
    }

    /**
     * The database schema that the module uses for executing the services.
     * 
     * @return the schema name
     */
    public String getSchema() {
        return _schema;
    }

    /**
     * Set the database schema that the module uses for executing the services.
     * 
     * @param schema
     */
    public void setSchema(String schema) {
        _schema = schema;
    }

    /**
     * @return the modules URL; a null value indicates this is a new module
     */
    public String getURL() {
        return _URL;
    }

    /**
     * @param URL the module URL
     */
    public void setURL(String URL) {
        _URL = URL;
    }

    /**
     * @return the entity tag of a resource module
     */
    public String getEntityTag() {
        return _eTag;
    }

    /**
     * @param _eTag the entity tag
     */
    public void setEntityTag(String eTag) {
        _eTag = eTag;
    }

    /**
     * @return comments
     */
    public String getComments() {
        return _comments;
    }

    /**
     * @param comments
     */
    public void setComments(String comments) {
        _comments = comments;
    }    
}
