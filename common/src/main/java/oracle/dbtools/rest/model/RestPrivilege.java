/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.rest.model;

import java.util.List;

/**
 * @author Elizabeth Saunders
 * @since  4.0
 *
 */
public class RestPrivilege {

    private String _name;
    private String _description;
    private String _title;
    private String _URL;
    private String _entityTag;
    private List<String> _uriPatterns;

    private List<String> _roles;
    private List<String> _modules;
    private String _comments;

    
    /**
     * Get the privilege name
     * 
     * @return privilege name
     */
    public String getName() {
        return _name;
    }
    
    /**
     * Set the privilege name
     * 
     * @param name
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Get the description
     * 
     * @return
     */
    public String getDescription() {
        return _description;
    }

    /**
     * Set the description
     * 
     * @param description
     */
    public void setDescription(String description) {
        _description = description;
    }

    /**
     * Get the privilege title
     * 
     * @return
     */
    public String getTitle() {
        return _title;
    }

    /**
     * Set the privilege title
     * 
     * @param title
     */
    public void setTitle(String title) {
        _title = title;
    }

    /**
     * Get a list of assigned roles.
     * 
     * @return
     */
    public List<String> getRoles() {
        return _roles;
    }

    /**
     * Set the list of assigned roles.
     * 
     * @param roles
     */
    public void setRoles(List<String> roles) {
        _roles = roles;
    }

    /**
     * Get a list of the protected modules.
     * 
     * @return
     */
    public List<String> getModules() {
        return _modules;
    }

    /**
     * Set a list of protected modules.
     * 
     * @param modules
     */
    public void setModules(List<String> modules) {
        _modules = modules;
    }

    /**
     * @return the url of the resource privilege
     */
    public String getURL() {
        return _URL;
    }

    /**
     * @param URL set the privilege URL
     */
    public void setURL(String URL) {
        this._URL = URL;
    }

    /**
     * @return the entity tag
     */
    public String getEntityTag() {
        return _entityTag;
    }

    /**
     * @param entityTag 
     */
    public void setEntityTag(String entityTag) {
        this._entityTag = entityTag;
    }
    
    /**
     * Get a list of the uri patterns used for protecting the resources 
     * using this privilege.
     * 
     * @return the URI patterns for the privileges
     */
    public List<String> getURIPatterns() {
        return _uriPatterns;
    }

    /**
     * Set the URI patterns which are used for protecting the resources using this privilege.
     * 
     * @param patterns
     */
    public void setURIPatterns(List<String> patterns) {
        _uriPatterns = patterns;
    }

    /**
     * @return comments
     */
    public String getComments() {
        return _comments;
    }

    /**
     * @param comments
     */
    public void setComments(String comments) {
        _comments = comments;
    }        
}
