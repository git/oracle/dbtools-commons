/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.rest.model;

import java.util.ArrayList;
import java.util.List;

import oracle.dbtools.data.common.NullOrEmpty;
import oracle.dbtools.rest.RestMessages;

/**
 * @author Elizabeth Saunders
 * @since  4.0
 *
 */
public class RestResourceHandler {
   
    public static final int DEFAULT_ITEMS_PER_PAGE = 25;
   
    // For ords via http using zip file
    public static final String GENERATOR_COLLECTION_JSON = "json/collection";    //$NON-NLS-1$
    public static final String GENERATOR_COLLECTION_ITEM_JSON = "json/item";    //$NON-NLS-1$
    public static final String GENERATOR_QUERY_JSON = "json/query";    //$NON-NLS-1$
    public static final String GENERATOR_QUERY_CSV  = "csv/query";     //$NON-NLS-1$
    public static final String GENERATOR_QUERY_ONE_ROW  = "json/query;type=single";  //$NON-NLS-1$
    public static final String GENERATOR_FEED       = "json/query;type=feed";        //$NON-NLS-1$
    public static final String GENERATOR_MEDIA      = "resource/lob";  //$NON-NLS-1$
    public static final String GENERATOR_PLSQL      = "plsql/block";   //$NON-NLS-1$
    
            
    public static enum SOURCE_TYPE {
        COLLECTION_QUERY(GENERATOR_COLLECTION_JSON,RestMessages.getString(RestMessages.SOURCE_COLLECTION_QUERY)),
        COLLECTION_QUERY_ITEM(GENERATOR_COLLECTION_ITEM_JSON, RestMessages.getString(RestMessages.SOURCE_COLLECTION_QUERY_ITEM)),
        QUERY(GENERATOR_QUERY_JSON, RestMessages.getString(RestMessages.SOURCE_QUERY)),
        QUERY_ONE_ROW(GENERATOR_QUERY_ONE_ROW, RestMessages.getString(RestMessages.SOURCE_QUERY_ONE_ROW)),
        FEED(GENERATOR_FEED, RestMessages.getString(RestMessages.SOURCE_FEED)),
        PLSQL(GENERATOR_PLSQL, RestMessages.getString(RestMessages.SOURCE_PLSQL)),
        MEDIA_RESOURCE(GENERATOR_MEDIA, RestMessages.getString(RestMessages.SOURCE_MEDIA));

        private String generator;  // source for zip file
        private String nlsName;

        SOURCE_TYPE(String generator, String nlsName) {
            this.generator = generator;
            this.nlsName   = nlsName; 
        }

        public String getGenerator() {
            return generator;
        }
        
        public String getNLS() {
            return nlsName;        
        }
    }
    
    public static final String METHOD_GET    = "get";     //$NON-NLS-1$
    public static final String METHOD_POST   = "post";    //$NON-NLS-1$
    public static final String METHOD_PUT    = "put";     //$NON-NLS-1$
    public static final String METHOD_DELETE = "delete";  //$NON-NLS-1$
    
    public static enum HTTP_METHOD_TYPE {
        GET(METHOD_GET, RestMessages.getString(RestMessages.METHOD_GET)),
        POST(METHOD_POST, RestMessages.getString(RestMessages.METHOD_POST)),
        PUT(METHOD_PUT, RestMessages.getString(RestMessages.METHOD_PUT)),
        DELETE(METHOD_DELETE, RestMessages.getString(RestMessages.METHOD_DELETE));

        private String type;
        private String nlsName;

        HTTP_METHOD_TYPE(String type, String nlsName) {
            this.type    = type;
            this.nlsName = nlsName; 
        }

        public String getType() {
            return type;
        }

        public String getNLS() {
            return nlsName;        
        }
    }
    
    public static final String FORMAT_JSON  = "JSON";   //$NON-NLS-1$
    public static final String FORMAT_CSV   = "CSV";    //$NON-NLS-1$

    public static enum FORMAT_TYPE {
        JSON(FORMAT_JSON, RestMessages.getString(RestMessages.FORMAT_JSON)),
        CSV(FORMAT_CSV, RestMessages.getString(RestMessages.FORMAT_CSV));

        private String type;
        private String nlsName;

        FORMAT_TYPE(String type, String nlsName) {
            this.type    = type;
            this.nlsName = nlsName; 
        }

        public String getType() {
            return type;
        }

        public String getNLS() {
            return nlsName;        
        }
    }
    
    private HTTP_METHOD_TYPE _httpMethodType;
    
    private String _sql;
    private SOURCE_TYPE _sourceType;
    private FORMAT_TYPE _formatType;
    
    private boolean _securedAccess;
    private long _paginationSize;
    private List<String> _mimeTypes;
    private String _qualifiedConnName;
    
    private List<RestHandlerParameter> _parameters;

    private String _comments;
   
    public RestResourceHandler() {        
    }
    
    public RestResourceHandler(RestResourceHandler copyHandler) {
        if (copyHandler != null) {
            setConnectionName(copyHandler.getConnectionName());
            setFormatType(copyHandler.getFormatType());
            setMethodType(copyHandler.getMethodType());
            setPaginationSize(copyHandler.getPaginationSize());
            setSecuredAccess(copyHandler.isSecuredAccess());
            setSourceType(copyHandler.getSourceType());
            setSQL(copyHandler.getSQL());
            if (copyHandler.getMimeTypes() != null)
                setMimeTypes(new ArrayList<String>(copyHandler.getMimeTypes()));
            if (copyHandler.getParameters() != null)            
                setParameters(new ArrayList<RestHandlerParameter>(copyHandler.getParameters()));
        }
    }
    
   /**
     * Get the HTTP method type for this handler.
     * 
     * @return HTTP method type.  Possible enum values are GET, PUT, POST, DELETE.
     */
    public HTTP_METHOD_TYPE getMethodType() {
        return _httpMethodType;
    }

    /**
     * Set the HTTP method type for this handler.
     * 
     * @param httpMethodType
     */
    public void setMethodType(HTTP_METHOD_TYPE httpMethodType) {
        _httpMethodType = httpMethodType;
    }

    public void setMethodType(String p_methodType) {
       if (!NullOrEmpty.nullOrEmpty(p_methodType)) {
          String methodType = p_methodType.trim().toLowerCase();
          switch (methodType) {
             case METHOD_GET:
                 _httpMethodType = HTTP_METHOD_TYPE.GET;
                break;
             case METHOD_POST:
                _httpMethodType = HTTP_METHOD_TYPE.POST;
                break;
             case METHOD_PUT:
                _httpMethodType = HTTP_METHOD_TYPE.PUT;
                break;
             case METHOD_DELETE:
                _httpMethodType = HTTP_METHOD_TYPE.DELETE;
                break;
             default:
                _httpMethodType = HTTP_METHOD_TYPE.GET;
          }
       }
     }

    /**
     * Get the SQL.
     * 
     * @return the sql
     */
    public String getSQL() {
        return _sql;
    }

    /**
     * Set the SQL.
     * 
     * @param sql
     */
    public void setSQL(String sql) {
        _sql = sql;
    }
    
    /**
     * The resource handler source type
     * 
     * @return source type.  The possible enum values are QUERY, QUERY_ONE_ROW, FEED, PL_SQL, MEDIA_RESOURCE
     */
    public SOURCE_TYPE getSourceType() {
        return _sourceType;
    }
    
    /**
     * Set the source type.
     * 
     * @param sourceType The possible enum values are QUERY, QUERY_ONE_ROW, FEED, PL_SQL, MEDIA_RESOURCE.
     */
    public void setSourceType(SOURCE_TYPE sourceType) {
        _sourceType = sourceType;
    }
    
    public void setSourceType(String sourceType) {
        if (NullOrEmpty.nullOrEmpty(sourceType)) {
            _sourceType = SOURCE_TYPE.COLLECTION_QUERY;            
        } else {            
            switch (sourceType) {
                case GENERATOR_COLLECTION_JSON:
                    _sourceType = SOURCE_TYPE.COLLECTION_QUERY;
                    break;
                case GENERATOR_COLLECTION_ITEM_JSON:
                    _sourceType = SOURCE_TYPE.COLLECTION_QUERY_ITEM;
                    break;
                case GENERATOR_PLSQL:
                    _sourceType = SOURCE_TYPE.PLSQL;
                    break;
                case GENERATOR_FEED:
                    _sourceType = SOURCE_TYPE.FEED;
                    break;
                case GENERATOR_MEDIA:
                    _sourceType = SOURCE_TYPE.MEDIA_RESOURCE;
                    break;
                case GENERATOR_QUERY_JSON:
                    _sourceType = SOURCE_TYPE.QUERY;
                    break;
                case GENERATOR_QUERY_CSV:
                    _sourceType = SOURCE_TYPE.QUERY;
                    break;
                case GENERATOR_QUERY_ONE_ROW:
                    _sourceType = SOURCE_TYPE.QUERY_ONE_ROW;
                    break;
                default:
                    _sourceType = SOURCE_TYPE.COLLECTION_QUERY;
                    break;             
            }
        }
        
        if (sourceType != null && (sourceType.equals(GENERATOR_QUERY_CSV))) {
           setFormatType(FORMAT_TYPE.CSV);
        } else {
           setFormatType(FORMAT_TYPE.JSON);          
        }
     }
    
    
    /**
     * The resulting format type returned for a GET Handler.
     * 
     * @return format type.  The possible enum values are JSON or CSV.
     */
    public FORMAT_TYPE getFormatType() {
        return _formatType;
    }

    /**
     * Set the format type.
     * 
     * @param formatType
     */
    public void setFormatType(FORMAT_TYPE formatType) {
        _formatType = formatType;
    }

    /**
     * Indicator if the Resource Handler should be using a secure access such as https.
     * 
     * @return true if it should use a secured access; otherwise, false.
     */
    public boolean isSecuredAccess() {
        return _securedAccess;
    }

    /**
     * Set the secure access indicator for the handler.
     * 
     * @param securedAccess true if it should use a secured access such as https; otherwise, false.
     */
    public void setSecuredAccess(boolean securedAccess) {
        _securedAccess = securedAccess;
    }

    /**
     * @return
     */
    public long getPaginationSize() {
        return _paginationSize;
    }

    /**
     * @param paginationSize
     */
    public void setPaginationSize(long paginationSize) {
        _paginationSize = paginationSize;
    }

    /**
     * @return
     */
    public List<String> getMimeTypes() {
        return _mimeTypes;
    }

    /**
     * @param mimeTypes
     */
    public void setMimeTypes(List<String> mimeTypes) {
        _mimeTypes = mimeTypes;
    }

    /**
     * The connection name used in SQL Worksheet.
     * 
     * @return the qualified connection name
     */
    public String getConnectionName() {
        return _qualifiedConnName;
    }

    /**
     * Keeps track of the connection name used in SQL Worksheet.
     * 
     * @param qualifiedConnName the qualified connection name
     */
    public void setConnectionName(String _qualifiedConnName) {
        this._qualifiedConnName = _qualifiedConnName;
    }

    
    /**
     * @return
     */
    public List<RestHandlerParameter> getParameters() {
        return _parameters;
    }

    /**
     * @param parameters
     */
    public void setParameters(List<RestHandlerParameter> parameters) {
        _parameters = parameters;
    }

    /**
     * @param parameter
     */
    public void addParameter(RestHandlerParameter parameter) {
        if (_parameters == null) {
            _parameters = new ArrayList<RestHandlerParameter>();
        }
        _parameters.add(parameter);
    }

   /**
    * Get the generator to use to transform the results into a JSON or CSV format. 
    * The PUT, POST and DELETE methods default to pl/sql generator type. 
    * 
    * @param isAPI -indicator if using for pl/sql api
    * @return the generator type
    */
   public String getSourceTypeGenerator() {
      if (_httpMethodType != null) {

         if (_httpMethodType == HTTP_METHOD_TYPE.GET) {
            if (_sourceType != null) {
               if ((_sourceType == SOURCE_TYPE.QUERY) && (_formatType == FORMAT_TYPE.CSV)) {
                  return GENERATOR_QUERY_CSV;
               } else {
                  return _sourceType.getGenerator();
               }
            }
         } else {
            return GENERATOR_PLSQL;
         }
      }      
      return GENERATOR_COLLECTION_JSON; 
   }      

   /**
    * @return comments
    */
   public String getComments() {
       return _comments;
   }

   /**
    * @param comments
    */
   public void setComments(String comments) {
       _comments = comments;
   }        
}
