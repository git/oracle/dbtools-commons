/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.rest.model;

import java.util.HashMap;
import java.util.List;

import oracle.dbtools.data.common.NullOrEmpty;
import oracle.dbtools.rest.RestMessages;

/**
 * @author Elizabeth Saunders
 * @since  4.0
 *
 */
public class RestTemplate {

    public static final String ENTITY_TAG_HASH  = "HASH";  //$NON-NLS-1$
    public static final String ENTITY_TAG_SECURE_HASH = "SECURE_HASH";  //$NON-NLS-1$
    public static final String ENTITY_TAG_QUERY = "QUERY";        //$NON-NLS-1$
    public static final String ENTITY_TAG_NONE  = "NONE";         //$NON-NLS-1$
    
    public static enum ENTITY_TAG_TYPE {
        SECURE_HASH(ENTITY_TAG_SECURE_HASH, RestMessages.getString(RestMessages.ENTITY_TAG_HASH)),
        QUERY(ENTITY_TAG_QUERY, RestMessages.getString(RestMessages.ENTITY_TAG_QUERY)),
        NONE(ENTITY_TAG_NONE, RestMessages.getString(RestMessages.ENTITY_TAG_NONE));

        private String type;
        private String nlsName;

        ENTITY_TAG_TYPE(String type, String nlsName) {
            this.type    = type;
            this.nlsName = nlsName; 
        }

        public String getType() {
            return type;
        }

        public String getNLS() {
            return nlsName;        
        }
    }
    
    private String _uriPattern;
    private ENTITY_TAG_TYPE _entityTag;
    private String _entityTagQuery;
    private int   _priority;
    private String _qualifiedConnName;
    
    private HashMap<RestResourceHandler.HTTP_METHOD_TYPE,RestResourceHandler> _resourceHandlers;
    private String _comments;    

    public RestTemplate() {
        _resourceHandlers = new HashMap<RestResourceHandler.HTTP_METHOD_TYPE,RestResourceHandler>();
    }
    
    /**
     * @return the URI pattern for the template
     */
    public String getURIPattern() {
        return _uriPattern;
    }

    /**
     * Set the URI pattern for the template
     * 
     * @param uriPattern
     */
    public void setURIPattern(String pattern) {
        _uriPattern = pattern;
    }

    /**
     * Get the HTTP Entity Tag: (SECURE_HASH, QUERY, NONE)
     * 
     * @return the HTTP Entity Tag
     */
    public ENTITY_TAG_TYPE getEntityTag() {
        return _entityTag;
    }

    /**
     * Set the HTTP Entity Tag.
     * 
     * @param entityTag
     */
    public void setEntityTag(ENTITY_TAG_TYPE entityTag) {
        _entityTag = entityTag;
    }
    
    /**
     * @param p_entityTag
     */
    public void setEntityTag(String p_entityTag) {
      if (!NullOrEmpty.nullOrEmpty(p_entityTag)) {
         String entityTag = p_entityTag.trim().toUpperCase();
         switch (entityTag) {
            case ENTITY_TAG_HASH:
            case ENTITY_TAG_SECURE_HASH:
               _entityTag = ENTITY_TAG_TYPE.SECURE_HASH;
               break;
            case ENTITY_TAG_QUERY:
               _entityTag = ENTITY_TAG_TYPE.QUERY;
               break;
            case ENTITY_TAG_NONE:
               _entityTag = ENTITY_TAG_TYPE.NONE;
               break;
            default:
               _entityTag = ENTITY_TAG_TYPE.SECURE_HASH;
         }
      }
    }
    
    /**
     * @return the query for an Entity Tag of type QUERY
     */
    public String getEntityTagQuery() {
        return _entityTagQuery;
    }

    /**
     * Set the query for an Entity Tag of type QUERY
     * @param query
     */
    public void setEntityTagQuery(String query) {
        _entityTagQuery = query;
    }
    
    /**
     * Get the priority number for a template. 
     * Priority value is from 0 to 9 where 0 is a low priority and 9 is a high priority.
     * 
     * @return priority value from 0 to 9.
     */
    public int getPriority() {
        return _priority;
    }

    /**
     * Set the priority number for a template.
     * Priority value is from 0 to 9 where 0 is a low priority and 9 is a high priority.
     * 
     * @param priority
     */
    public void setPriority(int priority) {
        _priority = priority;
    }

    /**
     * The connection name used for an Entity Tag of type query.
     * 
     * @return the qualified connection name
     */
    public String getConnectionName() {
        return _qualifiedConnName;
    }

    /**
     * Keeps track of the connection name used for an Entity Tag of type query.
     * 
     * @param qualifiedConnName the qualified connection name
     */
    public void setConnectionName(String qualifiedConnName) {
        _qualifiedConnName = qualifiedConnName;
    }

    /**
     * @return a HashMap. The key is the HTTP method type (GET, PUT, POST, DELETE) and the value is the
     * resource handler.
     */
    public HashMap<RestResourceHandler.HTTP_METHOD_TYPE,RestResourceHandler> getResourceHandlers() {
        return _resourceHandlers;
    }

    /**
     * Set the templates resource handlers.
     * 
     * @param resourceHandlers a HashMap. The key is the HTTP method type (GET, PUT, POST, DELETE) and the value is the
     * resource handler.
     */
    public void setResourceHandlers(HashMap<RestResourceHandler.HTTP_METHOD_TYPE, RestResourceHandler> resourceHandlers) {
        _resourceHandlers = resourceHandlers;
    }
        
    /**
    * @param handlers
    */
   public void setResourceHandlers(List<RestResourceHandler> handlers) {
       for (RestResourceHandler h : handlers) {
          addResourceHandler(h.getMethodType(), h);
       }
   }
            
    /**
     * @param handlerType
     * @param handler
     */
    public void addResourceHandler(RestResourceHandler.HTTP_METHOD_TYPE handlerType, RestResourceHandler handler) {
        _resourceHandlers.put(handlerType, handler);
    }

    /**
     * @param handlerType
     * @return
     */
    public RestResourceHandler getResourceHandler(RestResourceHandler.HTTP_METHOD_TYPE handlerType) {
        return _resourceHandlers.get(handlerType);
    }

    /**
     * @param handlerType
     * @return
     */
    public RestResourceHandler removeResourceHandler(RestResourceHandler.HTTP_METHOD_TYPE handlerType) {
        return _resourceHandlers.remove(handlerType);
    }
    
    /**
     * @param handlerType
     * @return
     */
    public boolean handlerExists(RestResourceHandler.HTTP_METHOD_TYPE handlerType) {
        return _resourceHandlers.containsKey(handlerType);
    }
    
    /**
     * @return comments
     */
    public String getComments() {
        return _comments;
    }

    /**
     * @param comments
     */
    public void setComments(String comments) {
        _comments = comments;
    }        
}
