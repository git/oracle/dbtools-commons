/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.scripting;

import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.utils.ExceptionHandler;

public class ScriptingUtils {
  private static Logger LOGGER = Logger.getLogger(ScriptingUtils.class.getName());

  public static Object runScript(Script script,Map<String,Object> map) throws ScriptException{
    return runScript(null,script,map);
  }
	public static Object runScript(ScriptRunnerContext ctx, Script script,Map<String,Object> map) throws ScriptException{
        ScriptEngineManager mgr = new ScriptEngineManager();
        
        String language = script.getExtension() == null ?script.getLanguage(): script.getExtension();

        ScriptEngine engine = script.getExtension() == null ? mgr.getEngineByExtension(script.getLanguage()):
                                                              mgr.getEngineByExtension(script.getExtension());
        
        // if one was used already it could be saved for later
        if ( ctx.getProperty("SCRIPT_ENGINE:" + language ) != null ) {
            LOGGER.info("\nReUsing  Engine: " +language + "\n");
            engine =  (ScriptEngine)ctx.getProperty("SCRIPT_ENGINE:" + language );
        }
        for(String key:map.keySet()){
            engine.put(key, map.get(key));
            Logger.getLogger(ScriptingUtils.class.getClass().getName()).finer("Adding:" + key);
        }
        Object ret = null;
        try {
         ret = engine.eval(script.getScript());
        } catch (Exception sqlex) {
        	if (sqlex instanceof SQLException || sqlex.getCause() instanceof SQLException)
        	  ExceptionHandler.handleException(sqlex);
        	else throw sqlex;
        }
        if ( ret != null ){
          Logger.getLogger(ScriptingUtils.class.getClass().getName()).finer("Returned:" + ret.toString());
        }
        
        // if ctx was passed in stash the engine for later use
        if ( ctx != null  ) {
          LOGGER.info("\nSaving  Engine: " +language + "\n");
          ctx.putProperty("SCRIPT_ENGINE:" + language , engine);
        }
        return ret;
	}
}
