/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;


/**
 * Ordered array of integers/longs
 * @author Dim
 *
 */
public class Array {
	
	public static void main( String[] args ) throws Exception {
		/*int[] a = {2,3,6,10};
		System.out.println(indexOf(a, 0, a.length-1, -1));
		System.out.println(indexOf(a, 2));
		int[] b = insert(a,22);
		for( int i = 0; i < b.length; i++ ) {
			System.out.print(","+b[i]);
		}*/
		//Service.profile(10000, 50);
		
        /*int[] indexes = new int[4];
        for( int i = 0; i < indexes.length; i++ )
            indexes[i] = 0;
    	do {
    		System.out.println(""+indexes[3]+indexes[2]+indexes[1]+indexes[0]);
    		if( indexes[2]==1 ) {
    			skipPos(2, indexes, 3);
    			continue;
    		}
    	} while( Array.next(indexes,3) );
		*/
	    
	    int size = 1000000;
	    int[] a = new int[size/2];
        int[] b = new int[size/2];
        for( int i = 0; i < a.length; i++ ) 
            a[i] = 2*i;
        for( int i = 0; i < b.length; i++ ) 
            a[i] = 2*i+1;
        
        long t1 = System.nanoTime();        
        Array.merge(a, b);	            
	    System.out.println(System.nanoTime() - t1);
	}

	/**
	 * Find insertion point within an array
	 * @param array
	 * @param value
	 * @return
	 */
	public static int indexOf( int[] array, int value ) {
		return indexOf(array, 0, array.length-1, value);
	}
	public static int indexOf( long[] array, long value ) {
		return indexOf(array, 0, array.length-1, value);
	}
	
	/**
	 * Find insertion point within an array between values x and y
	 * @param array
	 * @param x
	 * @param y
	 * @param value
	 * @return
	 */
	private static int indexOf( int[] array, int x, int y, int value ) {
		if( x+1 == y || x == y )
			return array[x] < value ? y : x;
		int mid = (x+y)/2;
		if( value < array[mid] )
			return indexOf(array, x, mid, value);
		else
			return indexOf(array, mid, y, value);
	}
	private static int indexOf( long[] array, int x, int y, long value ) {
		if( x+1 == y || x == y )
			return array[x] < value ? y : x;
		int mid = (x+y)/2;
		if( value < array[mid] )
			return indexOf(array, x, mid, value);
		else
			return indexOf(array, mid, y, value);
	}
	
	public static int[] insert( int[] array, int value ) {
		if( array == null || array.length == 0 ) {
			array = new int[1];
			array[0] = value;
			return array;
		}
		
		int index = indexOf(array, 0, array.length, value);
		if( index < array.length && array[index] == value )
			return array;
		
		int[] ret = new int[array.length+1];
		for( int i = 0; i < index; i++ ) {
			ret[i] = array[i];
		}
		ret[index] = value; 
		for( int i = index+1; i < ret.length; i++ ) {
			ret[i] = array[i-1];
		}
		return ret;
	}
	public static long[] insert( long[] array, long value ) {
		if( array == null || array.length == 0 ) {
			array = new long[1];
			array[0] = value;
			return array;
		}
		
		int index = indexOf(array, 0, array.length, value);
		if( index < array.length && array[index] == value )
			return array;
		
		long[] ret = new long[array.length+1];
		for( int i = 0; i < index; i++ ) {
			ret[i] = array[i];
		}
		ret[index] = value; 
		for( int i = index+1; i < ret.length; i++ ) {
			ret[i] = array[i-1];
		}
		return ret;
	}
	
    public static int[] delete( int[] array, int value ) {
        int index = indexOf(array, 0, array.length, value);
        if( index == array.length || array[index] != value )
            return array;
        
        int[] ret = new int[array.length-1];
        for( int i = 0; i < index; i++ ) {
            ret[i] = array[i];
        }
        for( int i = index; i < ret.length; i++ ) {
            ret[i] = array[i+1];
        }
        return ret;
    }
    
	public static int[] merge( int[] x, int[] y ) {
		if( x == null )
			return y;
		if( y == null )
			return x;
		
		int m = x.length;
		int n = y.length;
		int[] tmp = new int[m+n];

		int i = 0;
		int j = 0;
		int k = 0;

		while( i < m && j < n ) 
			if( x[i] == y[j] ) {
				tmp[k++] = x[i++];
				j++;
			} else if (x[i] < y[j]) 
				tmp[k++] = x[i++];
			else 
				tmp[k++] = y[j++];
					

		if( i < m ) 
			for( int p = i; p < m; p++ ) 
				tmp[k++] = x[p];
		else
			for( int p = j; p < n; p++ ) 
				tmp[k++] = y[p];
		
		int[] ret = new int[k];
		for( int ii = 0; ii < k; ii++ ) 
			ret[ii] = tmp[ii];		

		return ret;
	}

    public static long[] merge( long[] x, long[] y ) {
        if( x == null )
            return y;
        if( y == null )
            return x;
        
        int m = x.length;
        int n = y.length;
        long[] tmp = new long[m+n];

        int i = 0;
        int j = 0;
        int k = 0;

        while( i < m && j < n ) 
            if( x[i] == y[j] ) {
                tmp[k++] = x[i++];
                j++;
            } else if (x[i] < y[j]) 
                tmp[k++] = x[i++];
            else 
                tmp[k++] = y[j++];
                    

        if( i < m ) 
            for( int p = i; p < m; p++ ) 
                tmp[k++] = x[p];
        else
            for( int p = j; p < n; p++ ) 
                tmp[k++] = y[p];
        
        long[] ret = new long[k];
        for( int ii = 0; ii < k; ii++ ) 
            ret[ii] = tmp[ii];      

        return ret;
    }

	
    /*
     * State vector iterator
     */
    public static boolean next( int[] state, int[] limit ) {
        return next(state, limit, 0);
    }
    public static boolean next( int[] state, int[] limit, int init ) {
        for( int pos = 0; pos < state.length; pos++ ) {
            if( state[pos] < limit[pos]-1 ) {
                state[pos]++;
                return true;
            }
            state[pos] = init;                             
        }
        return false;
    }

}
