/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.io.Closeable;
import java.io.FilterInputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import oracle.sql.BFILE;
import oracle.sql.BLOB;

/**
 * Helper class for managing {@link Closeable} resources. When acquiring
 * resources the following pattern should be used to avoid resource leaks:
 * 
 * <pre>
 * Connection conn = null;
 * Statement stmt = null;
 * ResultSet rs = null;
 * InputStream in = null;
 * try {
 *  conn = ...;
 *  stmt = ...;
 *  rs = ...;
 *  in = ...;
 * } finally {
 *  Closeables.close(rs,stmt,conn,in);
 * }
 * </pre>
 * <ul>
 * <li>Always initiate the resource reference to null (outside of any
 * <code>try</code> block)</li>
 * <li>Surround assignment of the actual resource reference within a
 * <code>try...finally</code> block</li>
 * <li>You can pass multiple arguments to {@link Closeables#close(Object...)},
 * no need to invoke the method more than once in the <code>finally</code> block
 * </ul>
 * 
 * @author cdivilly
 */
@SuppressWarnings("deprecation")
public abstract class Closeables {

  /**
   * Close an object if it is an instance of {@link Closeable},
   * {@link ResultSet}, {@link Statement} or {@link Connection}. If the object
   * is an instance of {@link Flushable} then flush it before closing.Any
   * exceptions occurring during the flush or close operation are swallowed.
   * 
   * @param <T>
   *          The type of the resource
   * @param closeable
   *          The resource reference, may be null
   */
  public static <T> void close(final T closeable) {
    if (closeable instanceof Flushable) {
      try {
        ((Flushable) closeable).flush();
      } catch (final IOException e) {
        // swallow close exceptions
      }
    }
    if (closeable instanceof Closeable) {
      try {
        ((Closeable) closeable).close();
      } catch (final Throwable e) {
        // swallow close exceptions
      }
    } else if (closeable instanceof ResultSet) {
      try {
        final ResultSet target = (ResultSet) closeable;
        target.close();
      } catch (final SQLException e) {
      }
    } else if (closeable instanceof Statement) {
      try {
        final Statement target = (Statement) closeable;
        target.close();
      } catch (final SQLException e) {
      }
    } else if (closeable instanceof Connection) {
      try {
        final Connection target = (Connection) closeable;
        target.close();
      } catch (final SQLException e) {
      }
    } else if (closeable instanceof BLOB) {
      try {
        final BLOB target = (BLOB) closeable;
        target.close();
      } catch (final SQLException e) {
      }
    } else if (closeable instanceof BFILE) {
      try {
        final BFILE target = (BFILE) closeable;
        target.close();
      } catch (final SQLException e) {
      }
    }
  }

  /**
   * Close zero or more resources.
   * 
   * @see #close(Object)
   * @param <T>
   *          the Type of the resources
   * @param closeables
   *          The resources to close, may include null values
   */
  public static <T> void close(final T... closeables) {
    for (final T closeable : closeables) {
      close(closeable);
    }
  }

  /**
   * When the stream is closed, also close the resources backing the stream
   * 
   * @param inputStream
   * @param dependents
   *          resources backing the stream that can be discarded when the stream
   *          is closed
   * @return
   */
  public static InputStream alsoClose(final InputStream inputStream,
      final Object... dependents) {
    return new FilterInputStream(inputStream) {

      @Override
      public void close() throws IOException {
        Closeables.close(dependents);
      }
    };
  }
}
