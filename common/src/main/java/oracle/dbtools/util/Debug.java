/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

/* 
 * Version: $Id: Debug.java,v 1.2 2006/03/24 18:59:38 kris.rice Exp $
 * 
 */
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Debug {
	static private int debugLevel = 0;

	public static boolean DEBUG = false;

	synchronized static private void init() {
	}

	public static void setDebug(boolean flag) {
		DEBUG = flag;
	}

	static private void print(Object obj, String sep) {
		if (obj instanceof String) {
			// just print the string
			System.err.print(obj);// (authorized)
		} else if (obj instanceof String[]) {
			for (int i = 0, ii = ((String[]) obj).length; i < ii; i++) {
				print(((String[]) obj)[i], ","); //$NON-NLS-1$
			}
		} else if (obj instanceof HashMap) {

			// print a hashmap as name=value
			Set keys = ((HashMap) obj).keySet();
			String key;
			for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
				key = (String) iterator.next();
				System.err.print(key + "="); // (authorized) $NON-NLS-1$ //$NON-NLS-1$
				print(((HashMap) obj).get(key), ","); //$NON-NLS-1$
			}
		} else if (obj instanceof List) {
			// iterate and print the list
			for (int i = 0, ii = ((List) obj).size(); i < ii; i++) {
				print(((List) obj).get(i), ","); //$NON-NLS-1$
			}
		} else {
			// if not handled above just try and print
			System.err.print(obj);// (authorized)
		}
		System.err.print(sep); // (authorized)
	}

	static public void debug(Object s) {
		init();
		if (debugLevel >= 3)
			print(s, "\n"); //$NON-NLS-1$
	}

	static public void info(Object s) {
		init();
		if (debugLevel >= 2)
			print(s, "\n"); //$NON-NLS-1$

	}

	static public void warn(Object s) {
		init();
		if (debugLevel >= 1)
			print(s, "\n"); //$NON-NLS-1$

	}

	static public void error(Object s) {
		init();
		print(s, "\n"); //$NON-NLS-1$
	}

	public static boolean isDebugBuild() {
		// Set debug based based on ide version
		// Debug.setDebug(oracle.ide.Version.DEBUG_BUILD == 0);
		return DEBUG;
	}
}
