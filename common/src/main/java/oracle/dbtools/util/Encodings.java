/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;


import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeSet;

/**
 *  Taken from javatools-nodeps.jar. Stripped out the UI elements to just what's required by SQLcl SET ENCODING command
 *  
 *  
 *  Utility class for dealing with character encodings. The determination of
 *  what encodings are available is based on the information supplied by
 *  {@link Charset}. Mapping from an alias to a canonical (IANA) name is
 *  provided.
 *
 *  @author jdijamco
 *  @author John McGinnis
 */
public final class Encodings
{
  /**
   * The name of the 'UTF-8' encoding.
   */
  public static final String UTF8_ENCODING = "UTF-8"; //NOTRANS
  private static String[] _encodings;
  private static String[] _encodingsWithoutAliases;
  private static String _defaultEncoding;

  //private static EncodingsData _data;

  private Encodings()
  {
    //  Prevents instantiation.
  }

  /**
   *  Returns <CODE>true</CODE> if the specified encoding is supported
   *  by the Java implementation.  Returns <CODE>false</CODE> if the
   *  encoding is not supported.
   */
  public static boolean isSupported( String encoding )
  {
    boolean supported = false;
    try
    {
      supported = Charset.isSupported( encoding );
      if ( !supported )
      {
        // Check to see if perhaps the encoding is only supported by the old
        // java.io API
        String s = new String( new byte[ 0 ], encoding );
        supported = true;
      }
    }
    catch ( IllegalCharsetNameException e )
    {
    }
    catch ( UnsupportedEncodingException e )
    {
    }
    return supported;
  }

  /**
   *  Returns the default character encoding being used by the Java
   *  implementation.  This method will only determine the default
   *  encoding once.  After that the result is cached.
   */
  public static String getDefaultEncoding()
  {
    if ( _defaultEncoding == null )
    {
      final InputStreamReader isr = new InputStreamReader( System.in );
      _defaultEncoding = isr.getEncoding().intern();
    }
    return _defaultEncoding;
  }

  /**
   *  Returns a string array of encodings that are supported by the
   *  Java implementation.  This method will only search for encodings
   *  once.  After that the result is cached.
   */
  public static synchronized String[] getEncodings()
  {
    ensureInit();
    return (String[]) _encodings.clone();
  }

  /**
   *  Returns a string array of encodings that are supported by the
   *  Java implementation.  This method will only search for encodings
   *  once.  After that the result is cached.
   */
  public static synchronized String[] getEncodingsWithoutAlias()
  {
    ensureInit();
    return (String[]) _encodingsWithoutAliases.clone();
  }
  
  /**
   *  Certain optimizations can be performed when the running JDK's
   *  default character encoding is "Cp1252".  This method provides a
   *  standard way of detecting that the encoding is Cp1252.
   *
   *  @returns  <CODE>true</CODE> if the default character encoding
   *  for the currently running JDK is Cp1252; <CODE>false</CODE>
   *  otherwise.
   */
  public static boolean isDefaultCp1252()
  {
    //  The "==" operator can be used because the return value of
    //  getDefaultEncoding() is intern()'ed.
    return "Cp1252".equalsIgnoreCase( getDefaultEncoding() ); //NOTRANS
  }

  //--------------------------------------------------------------------------
  //  implementation details...
  //--------------------------------------------------------------------------

  // NOTE: This method needs to be synchronized because it initializes the
  // Encodings state and can be called by multiple threads.
  private static synchronized void ensureInit()
  {
    if ( _encodings == null )
    {
      SortedMap encMap = Charset.availableCharsets();
      final Set encodings = new TreeSet( new CharsetComparator() );
      final Set encodingsWithOutAliases = new TreeSet( new CharsetComparator() );

      for ( Iterator iter = encMap.values().iterator(); iter.hasNext(); )
      {
        Charset ch = ( Charset ) iter.next();
        String chName = ch.name();
        // bug 5458111, exclude x-oracle- encodings
        if (!(chName.length() >= "X-ORACLE-".length() &&
              "X-ORACLE-".equalsIgnoreCase(chName.substring(0, "X-ORACLE-".length()))))
        {
          encodings.add( ch.name() );
          encodingsWithOutAliases.add( ch.name() );
          Set aliases = ch.aliases();
          encodings.addAll( aliases );
        }
      }
      normalizeDefaultEncoding(encodings);
      normalizeDefaultEncoding(encodingsWithOutAliases);
      _encodings = ( String[] ) encodings.toArray( new String[ encodings.size() ] );
      _encodingsWithoutAliases = ( String[] ) encodingsWithOutAliases.toArray( new String[ encodingsWithOutAliases.size() ] );
    }
  }

 

  private static void normalizeDefaultEncoding(final Set encodings)
  {
    String defaultEncoding = getDefaultEncoding();
    if ( encodings.contains( defaultEncoding ) )
    {
      // Okay, we need to make sure that the case is the same by removing
      // it and then re-adding it
       encodings.remove( defaultEncoding );
    }
    encodings.add( defaultEncoding );
  }

 

  /**
   * Comparator implementation that ignores case
   */
  private static class CharsetComparator implements Comparator
  {
    public int compare( Object o1, Object o2 )
    {
      String s1 = ( String ) o1;
      String s2 = ( String ) o2;

      return s1.compareToIgnoreCase( s2 );
    }
  }

}
