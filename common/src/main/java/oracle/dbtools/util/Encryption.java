/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/**
 * <strong>Encryption</strong> - an encryption utility for encrypting sensitive data.
 * <p/>
 * Usage:
 * <p/>
 *      Sender and receiver must agree on a list of seed values to generate a <i>salt</i>
 *      value for the encryption. A common usage pattern is to use selected non-sensitive
 *      <i>variable</i> values that are also being transmitted or otherwise known to both
 *      parties. (The values should be variable so that the same <i>salt</i> is not used
 *      for every encryption.)
 * <p/>     
 *      These will be processed with an internally defined initial seed to create the 
 *      <i>salt</i> value. See {@link #generateSalt(String... seedValues)}
 * <p/>
 *      A utility {@link #getRandomUUID()} method is provided to create a key if the caller
 *      does not already have an appropriate one.
 * <p/>
 *      Sender uses {@link #encrypt(char[] value, String key, byte[] salt)} then transfers
 *      the key and encryptedValue along with any other data.
 * <p/>                    
 *      Receiver generates the <i>salt</i> value the same way the sender does then uses
 *      {@link #decrypt(String encryptedValue, String key, byte[] salt)} to get the original value.
 * <p/> 
 * 
 * NOTE: We are passing <i>char[]</i> instead of <i>String</i> to/from encrypt/decrypt as <i>String</i>
 *       is immutable while the <i>char[]</i> can be overwritten when no longer needed.     
 *     
 * @since SQLDeveloper 4.1.4
 * 
 * @author <a href="mailto:brian.jeffries@oracle.com?subject=oracle.dbtools.transfer.utility.Encryption">Brian Jeffries</a>
 */
public class Encryption {
    
    /**
     * Construct the salt array used for password encryption/decryption.
     * @parameter seedValues One or more 
     */
    public static byte[] generateSalt(String... seedValues) {
        if (seedValues == null ||seedValues.length == 0) {
            throw new IllegalArgumentException("Null or empty seed values"); //$NON-NLS-1$
        }
        assert(seedValues != null);
                
        // The default salt array is initialized with constants and then
        // xor'ed with seed value bytes (from Hadoop team)
        byte[] salt = {  (byte) 0x8C, (byte) 0x4D,
                         (byte) 0x65, (byte) 0xA8,
                         (byte) 0x9F, (byte) 0x50,
                         (byte) 0x52, (byte) 0x33
                      };                
        final int SALT_LEN = salt.length;
        
        for (String seed : seedValues) {
            if (seed != null) {
                int i = 0;
                for (byte b : seed.getBytes()) {
                    salt[i%SALT_LEN] = (byte) (b ^ salt[i%SALT_LEN]);
                    i++;
                }
            } else {
                throw new IllegalArgumentException("Null seed value"); //$NON-NLS-1$
            }
        }
        
        return salt;
    }

    /**
     * @return a 128-bit random UUID
     */
    public static String getRandomUUID() {
       String formattedUUID = UUID.randomUUID().toString();
       String rawUUID = formattedUUID.replaceAll("-", "");
       return rawUUID;     
    }

    /**
     * encrypt a value
     * @param value the value to encrypt
     * @param key the key for the encryption (e.g., a UUID)
     * @param salt the salt for the encryption
     * @return the encrypted value
     */
    public static String encrypt(char[] value, String key, byte[] salt) {
        if (value == null || value.length == 0 || isNullOrEmpty(key) || salt == null || salt.length == 0) {
            throw new IllegalArgumentException();
        }
        try {
            Wrapper wrapper = Wrapper.newInstance(key, salt);    
            return wrapper.encrypt(value);      
        }
        catch (Throwable t) {
            throw asRuntimeException(t);
        }
    }
    
    /**
     * decrypt a value
     * @param value the value to decrypt
     * @param key the key for the decryption (e.g., a UUID)
     * @param salt the salt for the decryption
     * @return the decrypted value
     */
    public static char[] decrypt(String encryptedValue, String key, byte[] salt) {
        if (isNullOrEmpty(encryptedValue) || isNullOrEmpty(key) || salt == null || salt.length == 0) {
            throw new IllegalArgumentException();
        }
        try {
            Wrapper wrapper = Wrapper.newInstance(key, salt);    
            return wrapper.decrypt(encryptedValue);      
        }
        catch (Throwable t) {
            throw asRuntimeException(t);
        }
    }
    
    
    private static boolean isNullOrEmpty(String val) {
        return ((val == null) || (val.isEmpty()));
    }
    
    private static RuntimeException asRuntimeException(Throwable t) {
        if (t instanceof RuntimeException) {
            return (RuntimeException)t;
        } else {
            return new RuntimeException(t);
        }
    }


    /**
     * <strong>Wrapper</strong> - implementation of PBE encryption
     * <p/>
     * "Password-Based Encryption" (PBE) implemented by this Wrapper takes character passwords 
     * and generates strong binary keys. In order to make the task of getting from password to key 
     * very time-consuming for an attacker (via so-called "dictionary attacks" where common dictionary 
     * word->value mappings are precomputed), most PBE implementations will mix in a random number, 
     * known as a salt, to increase the key randomness. (from https://docs.oracle.com/javase/7/docs/technotes/guides/security/crypto/CryptoSpec.html)
     * <p/>
     * To get a new Wrapper instance call {@link Wrapper#newInstance(String key, byte[] salt)}
     *
     * Code given to SQLDev by Hadoop team
     */
    static final class Wrapper {

        // Iteration count
        private static final int ITERATION_COUNT = 42;

        private Cipher m_encoder;

        private Cipher m_decoder;

        private AlgorithmParameterSpec m_paramSpec;

        private SecretKey m_key;

        private CharsetEncoder m_charEnc;

        private CharsetDecoder m_charDec;

        /**
         *
         * @param key
         *            a random string such as a 128 bit random UUID
         * @param salt
         *            a 8 byte array
         * @return a new Wrapper instance constructed using the given key string and salt byte array
         * @throws InvalidKeySpecException
         * @throws NoSuchAlgorithmException
         */
        public static Wrapper newInstance(String key, byte[] salt) throws InvalidKeySpecException, NoSuchAlgorithmException {
            if ((null == key) || (null == salt)) {
                throw new IllegalArgumentException("null arguments");
            }
            return new Wrapper(key, salt);
        }

        /**
         * Create a new wrapper with the specified pass phrase. The pass phrase is combined with the salt to generate a unique secret key
         *
         * @param passPhrase
         *            is unique string such as randomUUID that is used to construct a secret key for encryption/decryption.
         * @throws InvalidKeySpecException
         * @throws NoSuchAlgorithmException
         */
        private Wrapper(String passPhrase, byte[] salt) throws InvalidKeySpecException, NoSuchAlgorithmException {
            // Create the key
            KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, ITERATION_COUNT);
            m_key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
            // and the paramSpec needed to make the encoder and decoder
            m_paramSpec = new PBEParameterSpec(salt, ITERATION_COUNT);
        }

        /**
         * Encrypt the given character array into a base64 string.
         * 
         * The input char[] is converted into a byte[] using UTF-8 encoding and then encrypted using the Wrapper key algorithn and finally encoded into a base64 string.
         *
         * @param raw
         *            the char array to be encrypted
         * @return the encrypted string encoded as a base64 string
         * 
         * @throws InvalidAlgorithmParameterException
         * @throws InvalidKeyException
         * @throws NoSuchPaddingException
         * @throws NoSuchAlgorithmException
         * @throws CharacterCodingException
         * @throws BadPaddingException
         * @throws IllegalBlockSizeException
         */
        public String encrypt(char[] raw) throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchPaddingException,
                CharacterCodingException, IllegalBlockSizeException, BadPaddingException {
            if ((null == raw) || (raw.length == 0)) {
                throw new IllegalArgumentException("null input value");
            }

            // Encrypt
            byte[] enc = null;
            if (m_encoder == null) {
                m_encoder = Cipher.getInstance(m_key.getAlgorithm());
                m_encoder.init(Cipher.ENCRYPT_MODE, m_key, m_paramSpec);
            }
            // Encode the string into bytes using utf-8
            if (m_charEnc == null) {
                m_charEnc = StandardCharsets.UTF_8.newEncoder();
            }

            ByteBuffer bb = m_charEnc.encode(CharBuffer.wrap(raw));
            byte[] utf8 = new byte[ bb.limit() ];
            bb.get(utf8);

            enc = m_encoder.doFinal(utf8);
            // Encode bytes to base64 to get a string
            return javax.xml.bind.DatatypeConverter.printBase64Binary(enc);
        }

        /**
         * Decrypt the specified string All encodings are in UTF-8
         *
         * @param str
         *            the string to be decrypted
         * @return the decrypted string as a character array.
         *
         * @throws NoSuchPaddingException
         * @throws NoSuchAlgorithmException
         * @throws InvalidAlgorithmParameterException
         * @throws InvalidKeyException
         * @throws BadPaddingException
         * @throws IllegalBlockSizeException
         * @throws CharacterCodingException
         */
        public char[] decrypt(String str) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,
                IllegalBlockSizeException, BadPaddingException, CharacterCodingException {
            if (isNullOrEmpty(str)) {
                throw new IllegalArgumentException("null input string");
            }

            if (m_decoder == null) {
                m_decoder = Cipher.getInstance(m_key.getAlgorithm());
                m_decoder.init(Cipher.DECRYPT_MODE, m_key, m_paramSpec);
            }

            // Decode base64 to get bytes
            byte[] dec = javax.xml.bind.DatatypeConverter.parseBase64Binary(str);

            // Decrypt
            byte[] utf8 = m_decoder.doFinal(dec);

            // Decode using utf-8
            if (m_charDec == null) {
                m_charDec = StandardCharsets.UTF_8.newDecoder();
            }
            CharBuffer charbuf = m_charDec.decode(ByteBuffer.wrap(utf8));
            char[] ca = new char[ charbuf.limit() ];
            charbuf.get(ca);

            return ca;
        }
    }

}
