/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;

import java.util.HashMap;
import java.util.Map;

/**
 * Tracks objects with closeable resource usages.
 */
public final class GCTracker {
    private static GCTracker instance = null;
    
    private final Map<Reference, GCReapHandler> handlerMap;
    private final ReferenceQueue reaped;
    
    private boolean tracking;

    private GCTracker() {
        this.handlerMap = new HashMap();
        this.reaped = new ReferenceQueue();
        this.tracking = true;
    }

    public static GCTracker getInstance() {
        if (instance == null) {
            synchronized (GCTracker.class) {
                if (instance == null) {
                    instance = new GCTracker();
                }
            }
        }

        return instance;
    }

    public void setTracking(boolean setting) {
        if (this.tracking != setting) {
            synchronized (this) {
                this.tracking = setting;
            }
        }
    }

    public boolean isTracking() {
        return this.tracking;
    }

    /**
     * Start tracking a closeable resource owner.
     * @param referent the referent to be tracked
     * @param handler referent reap handler
     */
    public void track(Object referent, GCReapHandler handler) {
        if (tracking) {
            synchronized (this) {
                if (tracking) {
                    handlePendingReap();

                    Reference ref = new PhantomReference(referent, reaped);
                    handlerMap.put(ref, handler);
                }
            }
        }
    }

    public void pollReferentReapQueue() {
        pollReferentReapQueue(false);
    }

    public void pollReferentReapQueue(boolean force) {
        if (force || tracking) {
            synchronized (this) {
                handlePendingReap();
            }
        }
    }

    public void purgeReferentReapQueue() {
        synchronized (this) {
            purgeReferentReapQueue(isTracking());
        }
    }
    
    public void purgeReferentReapQueue(boolean tracking) {
        synchronized (this) {
            setTracking(tracking);
            
            handlerMap.clear();
            Reference ref = reaped.poll();
            while (ref != null) {
                ref = reaped.poll();
            }
        }
    }

    /**
     * Traverse the {@link ReferenceQueue} and clean up any tracked referents that
     * have been marked for garbage collected.
     */
    private void handlePendingReap() {
        Reference ref = reaped.poll();
        while (ref != null) {
            GCReapHandler handler = handlerMap.remove(ref);
            if (handler != null) {
                try {
                    handler.referentDequeued();
                } catch (Exception e) {
                    Logger.finest(GCTracker.class, e);
                }
            }
            ref = reaped.poll();
        }
    }
}
