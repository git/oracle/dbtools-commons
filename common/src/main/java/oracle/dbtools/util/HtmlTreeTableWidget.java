/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.util.ArrayList;
import java.util.List;

/**
 * To render a generic tree table in HTML.
 * <P>
 * 
 * @author vadim tropashko
 */
public class HtmlTreeTableWidget {
    public String[] columnNames;
    // public String[][] data;
    private List<Row> data = new ArrayList<Row>();
    private List<Integer> levels = new ArrayList<Integer>();
    private List<String> images = new ArrayList<String>();

    public String formName;

    private static final String defaultName = "";
    public String widgetName = defaultName; // important if there is more than one widget on a page

    //public boolean isTree = true;

    static private class Row {
        String label = null; // if != null then spanned
        String color = null;
        String[] fields;

        public Row(String longLabel, String[] row, String color) {
            label = longLabel;
            fields = row;
            this.color = color;
        }
    }

    public void append( String longLabel, String[] row, int level, String img ) {
    	append(longLabel, row, level, img, null);
    }
    public void append( String longLabel, String[] row, int level, String img, String color ) {
        data.add(new Row(longLabel, row, color));
        levels.add(level);
        images.add(img);        
    }

    public void concat(String moreLabel, String[] moreRow) {
        int idx = data.size();
        Row last = data.get(idx - 1);
        if (moreLabel != null) last.label += moreLabel;
        for (int i = 0; i < last.fields.length; i++)
            last.fields[ i ] += moreRow[ i ];
    }

    public String dataAt(int row, int col) {
        final Row rowObj = data.get(row);
        if (rowObj.label != null)
            return rowObj.label;
        else
            return rowObj.fields[ col ];
    }

    public String render() {
        StringBuilder ret = new StringBuilder();

        if (columnNames.length == 0) return "";

        ret.append("<TABLE class=\"TreeTableWidget\" border=0 cellpadding=0" + " cellspacing=0 width=100% bgcolor=#efefef><tr><td> \n");

        // ret.append(renderFooter());
        ret.append(renderData());

        ret.append("</td></tr></TABLE> \n");
        return ret.toString();
    }

    final static String tableBgColor = "#ffffff";
    final static String yellowBgColor = "#ffffdf";
    final static String grayBgColor = "#dfdfdf";
    final static String yellowBgColor1 = "#ffffd2";
    final static String grayBgColor1 = "#dfd5d1";

    /**
     *
     */
    /**
     *
     */
    public String renderData() {
        StringBuilder ret = new StringBuilder();
        ret.append("\n <!-- ..........................data begin...........................  -->\n");
        ret.append("\n    <TABLE class=TreeTableWidget border=0 cellspacing=0" + " cellpadding=1 align=center width=100%\"  bgcolor=" + tableBgColor + "> \n");
        // Header
        ret.append("    <tr class=\"TableCell\">\n");
        boolean isEvenCol = true;
        for (int i = 0; i < columnNames.length; i++) {
            ret.append("\n       <td valign=top bgcolor=" + (isEvenCol ? "#bbbbbb" : "#aaaaaa") + ">&nbsp;<font color=#444477>");
            ret.append(columnNames[ i ]);
            ret.append("</font>&nbsp;</td>\n");
            isEvenCol = !isEvenCol;
        }
        ret.append("\n    </tr>\n");

        // Data
        boolean evenRow = true;
        for (int row = 0; row < data.size(); row++) {
            ret.append("\n   <tr>");
            boolean rowSpans = data.get(row).label != null;
            if (!rowSpans) {
                evenRow = !evenRow;
            }
            boolean evenCol = true;
            int width0 = 45;
            // int width1 = (100-width0)/(countDisplayed()); // zero divide: lazy evaluation
            int decrFontSize1 = (columnNames.length > 6) ? -1 : 0;
            boolean firstCol = true;
            for (int col = 0; col < columnNames.length; col++) {
                if (rowSpans && !firstCol) {
                    continue;
                }
                evenCol = !evenCol;
                ret.append("\n      <td width=" + ((firstCol /*&& isTree*/) ? width0 : (100 - width0) / columnNames.length) + "% valign=top bgcolor="
                        + ((firstCol /*&& isTree*/) ? tableBgColor : (evenRow ? (evenCol ? yellowBgColor : yellowBgColor1) : (evenCol ? grayBgColor : grayBgColor1)))
                        + ((rowSpans && firstCol) ? (" colspan=" + columnNames.length) : " ") + ">");
                boolean closeFontTag = false;
                if ( /*(!firstCol || isTree) &&*/ !rowSpans && dataAt(row, col) != null) {
                    int decrFontSize2 = (dataAt(row, col).length() > 9) ? -1 : 0;
                    if (decrFontSize1 + decrFontSize2 < 0) {
                        ret.append("<font size=" + (decrFontSize1 + decrFontSize2) + ">");
                    }
                    closeFontTag = true;
                }
                if (firstCol /*&& isTree*/) {
                    ret.append("<table width=100% border=0 cellspacing=0 cellpadding=0><tr><td width=1%><table CELLSPACING=0 cellpadding=0><tr><td>");
                    for (int i = 0; i < levels.get(row); i++)
                        ret.append("&nbsp;&nbsp;&nbsp;&nbsp;</td><td>");
                    String nodeCtrlImg = "minus";
                    ret.append("<IMG SRC=\"images/" + nodeCtrlImg + ".gif\" wWIDTH=16 hHEIGHT=16 BORDER=0>");
                    if (images.get(row) != null) ret.append("<IMG SRC=\"images/" + images.get(row) + "\" wWIDTH=16 hHEIGHT=16 BORDER=0>");
                    ret.append("</td><tr></table></td>  <td width=99%>");
                    if( data.get(row).color != null ) {
                        ret.append("<font color=" + data.get(row).color + ">");
                        closeFontTag = true;
                    }
                }
                String cell = dataAt(row, rowSpans ? 0 : col);
                if (cell == null) cell = "&nbsp";
                boolean isHtml = cell.startsWith("<");
                ret.append((isHtml ? "" : "&nbsp;") + cell + (isHtml ? "" : "&nbsp;"));
                // String d = Str.nonNull(dataAt(row,col));
                // for( int i = 0; i < 16-d.length();i++ )
                // System.out.print(' ');
                // System.out.print(d.length()>15?d.substring(0,16):d);
                if (firstCol /*&& isTree*/) {
                    ret.append("</td></tr></table>");
                }
                if (closeFontTag) ret.append("</font>");
                ret.append("</td>");
                firstCol = false;
            }

            ret.append("\n   </tr>");
        }
        ret.append("\n</table>\n");
        ret.append("\n <!-- ..........................data end...........................  -->\n\n");

        return ret.toString();
    }

}
