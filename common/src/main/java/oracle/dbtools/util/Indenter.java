/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.util.Hashtable;

// Ripped from (OWB) oracle.wh.tools.XMLParser.XMLIndenter
/**
 * Indenter provides indent "pretty printing" by allowing the set up of an 
 * indent string to be used at "this" indent level plus one to be used for 
 * "previous" levels.</p>
 * 
 * By default, these are both set to two blanks. I.e., each indent level 
 * results in an additional two blank spaces added to the indent string.</p>
 * 
 * For a "pretty printing" example consider setIndentString("|--","+--"), 
 * which would produce indent strings:</p> <code>
 * getIndent(1) = "+--" 
 * getIndent(2) = "|--+--" 
 * getIndent(3) = "|--|--+--"
 * </code>
 */
public class Indenter {
    private Hashtable<Integer, String> indents = new Hashtable<Integer, String>();
    private String INDENT_PREV = "  ";
    private String INDENT_THIS = "  ";

    /**
     * Set the indent string to be repeated for each indent level. 
     * Setting this clears any cached indent strings.
     * 
     * @param indentString
     *            a string containing the characters to be "printed" for 
     *            each indent level.
     */
    public void setIndentString(String indentString) {
        setIndentStrings(indentString, indentString);
    }

    /**
     * Set the indent strings to be repeated for each indent level. 
     * Setting this clears any cached indent strings.
     * 
     * @param indentPrev
     *            a string containing the characters to be "printed" for 
     *            each previous level.
     * @param indentThis
     *            a string containing the characters to be "printed" for 
     *            this level.
     */
    public void setIndentStrings(String indentPrev, String indentThis) {
        indents.clear();
        INDENT_PREV = indentPrev;
        INDENT_THIS = indentThis;
    }

    /**
     * Get an indent string for a given level ("tab" setting is 2). 
     * [Presuming it's cheaper to hold these & lookup than it is to create 
     *  lots of strings.]
     * 
     * @param level
     *            the indent level
     * @return an appropriate indent prefix string
     */
    public String getIndent(int level) {
        Integer iLevel = new Integer(level);
        String indent = indents.get(iLevel);
        if (indent == null) {
            indent = new String();
            // play games here - starting with i=1 deliberately so can set all
            // the "previous" indents and still not have set "this" one yet.
            for (int i = 1; i < level; i++) {
                indent = indent.concat(INDENT_PREV);
            }
            if (level > 0) {
                indent = indent.concat(INDENT_THIS);
            }
            indents.put(iLevel, indent);
        }
        return indent;
    }
}
