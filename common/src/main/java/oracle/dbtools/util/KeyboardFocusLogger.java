/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;

/**
 * Utility class for debugging focus issues. Allows logging globally or for a component or component hierarchy.
 * 
 * NOTE: To catch who is requesting a focus change, set a breakpoint in
 *       Component.requestFocusHelper(boolean temporary,
 *           boolean focusedWindowChangeAllowed,
 *           CausedFocusEvent.Cause cause)
 * 
 * @author bjeffrie
 * 
 */
public class KeyboardFocusLogger implements FocusListener, PropertyChangeListener {
    private static Border DEBUG_BORDER = BorderFactory.createLineBorder(Color.RED);
    private static KeyboardFocusLogger _keyboardFocusLogger;
    private KeyboardFocusManager keyboardFocusManager;
    private boolean decorateBorder = true;
    private static final String FOCUS_OWNER = "focusOwner"; //$NON-NLS-1$
    private static Logger LOGGER = Logger.getLogger(KeyboardFocusLogger.class.getName());

    private KeyboardFocusLogger() {
        // nothing
    }

    public static KeyboardFocusLogger getInstance() {
        if (null == _keyboardFocusLogger) {
            _keyboardFocusLogger = new KeyboardFocusLogger();
        }
        return _keyboardFocusLogger;
    }

    /**
     * Start logging and decorating all keyboard focus changes. Component with focus will be decorated with DEBUG_BORDER
     */
    public static void start() {
        start(true);
    }

    /**
     * Start logging and optionally decorating all keyboard focus changes
     * 
     * @param decorate
     *            If true, component with focus will be decorated with DEBUG_BORDER
     */
    public static void start(boolean decorate) {
        getInstance().startImpl(decorate);
    }

    private void startImpl(boolean decorate) {
        decorateBorder = decorate;
        setKeyboardFocusManager(KeyboardFocusManager.getCurrentKeyboardFocusManager());
    }

    private void setKeyboardFocusManager(KeyboardFocusManager currentKeyboardFocusManager) {
        //assert (null == keyboardFocusManager); // TODO: Need provision for nesting/multiple focusManagers?
        if (keyboardFocusManager != null) {
            keyboardFocusManager.removePropertyChangeListener(FOCUS_OWNER, this);
        }
        keyboardFocusManager = currentKeyboardFocusManager;
        keyboardFocusManager.addPropertyChangeListener(FOCUS_OWNER, this);
    }

    /**
     * Stop logging and decorating all keyboard focus changes
     */
    public static void stop() {
        getInstance().stopImpl();
    }

    private void stopImpl() {
        if (keyboardFocusManager != null) {
            keyboardFocusManager.removePropertyChangeListener(FOCUS_OWNER, _keyboardFocusLogger);
            keyboardFocusManager = null;
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
        if (null == keyboardFocusManager) {
            log(e);
        } // else keyboardFocusManager will log property change
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (null == keyboardFocusManager) {
            log(e);
        } // else keyboardFocusManager will log property change
    }

    /**
     * Flag controlling if a debug border should be painted around the
     * focus owner. 
     * @param decorate if false, log only, else log + decorate
     * @return prior value
     */
    public boolean setDecorateBorder(boolean decorate) {
        boolean oldVal = decorateBorder;
        decorateBorder = decorate;
        return oldVal;
    }
    
    /**
     * Start logging and decorating focus changes
     * 
     * @param comp
     *            The component to watch
     */
    public void watch(JComponent comp) {
        ignore(comp);
        comp.addFocusListener(this);
    }

    /**
     * Stop logging and decorating focus changes
     * 
     * @param comp
     *            The component being watched
     */
    public void ignore(JComponent comp) {
        comp.removeFocusListener(this);
    }

    /**
     * Start logging and decorating focus changes for component and all its children
     * 
     * @param comp
     *            The top of the component hierarchy to watch
     */
    public void watchAll(JComponent comp) {
        watch(comp);
        for (Component scomp: comp.getComponents()) {
            if (scomp instanceof JComponent) {
                watchAll((JComponent) scomp);
            }
        }
    }

    /**
     * Stop logging and decorating focus changes for component and all its children
     * 
     * @param comp
     *            The top of the component hierarchy being watched
     */
    public void ignoreAll(JComponent comp) {
        ignore(comp);
        for (Component scomp: comp.getComponents()) {
            if (scomp instanceof JComponent) {
                ignoreAll((JComponent) scomp);
            }
        }
    }

    protected void log(FocusEvent e) {
        // new Throwable("FocusLogger: " + e.toString()).printStackTrace(System.out);
        // System.out.println("FocusLogger: " + e.toString());
        LOGGER.log(Level.FINER, e.toString());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        // Paranoia check
        if (FOCUS_OWNER.equals(evt.getPropertyName())) {
            log(evt);
            Object oldValue = evt.getOldValue();
            Object newValue = evt.getNewValue();
            if (decorateBorder) {
                if (oldValue instanceof JComponent) {
                    JComponent jcomp = (JComponent) oldValue;
                    resetBorder(jcomp);
                }
                if (newValue instanceof JComponent) {
                    JComponent jcomp = (JComponent) newValue;
                    setBorder(jcomp);
                }
            }
        }
    }

    private Map<JComponent,Border> originalBorderMap = new HashMap<JComponent,Border>();
    private void resetBorder(JComponent jcomponent) {
    	if (DEBUG_BORDER == jcomponent.getBorder()) {
    		Border border = originalBorderMap.remove(jcomponent);
    		jcomponent.setBorder(border);
    	}
    }
    private void setBorder(JComponent jcomponent) {
    	Border currentBorder = jcomponent.getBorder();
    	if (currentBorder != DEBUG_BORDER) {
    		originalBorderMap.put(jcomponent, currentBorder);
    		jcomponent.setBorder(DEBUG_BORDER);
    	}
    }
    
    protected void log(PropertyChangeEvent evt) {
        String name = evt.getPropertyName();
        Object oldValue = evt.getOldValue();
        Object newValue = evt.getNewValue();
        String msg = String.format("%1$10s: %2$s\n%3$10s: %4$s", name, newValue, "old", oldValue); //$NON-NLS-1$ //$NON-NLS-2$
        // new Throwable("FocusLogger: " + e.toString()).printStackTrace(System.out);
        System.out.println("FocusLogger: " + msg);
        //LOGGER.log(Level.INFO, msg);
    }

}
