/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.LogRecord;


/**
 * A utility class with convenience methods for using java.util.logging.Logger
 * @author bjeffrie
 *
 */
public class Logger {

    private static final String NO_THROWABLE_STACK_ELEMENT = "(Internal error) No Throwable Stack Element";
	private static final String NO_THROWABLE_OBJECT_FOUND = "(Internal error) No Throwable object found";
	private static final String NO_THROWABLE_OBJECT_THREAD_DIED = "(Internal error) Thread died because there is no throwable object";

	private Logger() {}; // static methods only 
    
    /** utility method to explicitly state intention to ignore a Throwable */
    public static void ignore(Class<?> clazz, Throwable t) {
        //assert(null == t || t != null); // Trick to avoid parameter not used error in IDEs
        // log as fine but add prefix
        String msg = Messages.format("Logger_ignored", format(t)); //$NON-NLS-1$
        log(clazz, Level.FINE, msg, t);
    }
    
    public static void finest(Class<?> clazz, Throwable t) {
        log(clazz, Level.FINEST, format(t), t);
    }
    
    public static void finer(Class<?> clazz, Throwable t) {
        log(clazz, Level.FINER, format(t), t);
    }
    
    public static void fine(Class<?> clazz, Throwable t) {
        log(clazz, Level.FINE, format(t), t);
    }
    
    public static void info(Class<?> clazz, Throwable t) {
        log(clazz, Level.INFO, format(t), t);
    }
    
    public static void warn(Class<?> clazz, Throwable t) {
        log(clazz, Level.WARNING, format(t), t);
    }
    
    public static void severe(Class<?> clazz, Throwable t) {
        log(clazz, Level.SEVERE, format(t), t);
    }
    
    public static void finest(Class<?> clazz, String msg) {
        log(clazz, Level.FINEST, msg, null);
    }
    
    public static void finer(Class<?> clazz, String msg) {
        log(clazz, Level.FINER, msg, null);
    }
    
    public static void fine(Class<?> clazz, String msg) {
        log(clazz, Level.FINE, msg, null);
    }
    
    public static void info(Class<?> clazz, String msg) {
        log(clazz, Level.INFO, msg, null);
    }
    
    public static void warn(Class<?> clazz, String msg) {
        log(clazz, Level.WARNING, msg, null);
    }
    
    public static void severe(Class<?> clazz, String msg) {
        log(clazz, Level.SEVERE, msg, null);
    }
    
    /*
     * NOTE: These methods are for adding context information such as what
     *       user operation is not going to happen due to the exception. 
     *       The exception message and point of failure will be added by
     *       this class.
     */
    
    public static void finest(Class<?> clazz, String msg, Throwable t) {
        log(clazz, Level.FINEST, format(msg, t), t);
    }
    
    public static void finer(Class<?> clazz, String msg, Throwable t) {
        log(clazz, Level.FINER, format(msg, t), t);
    }
    
    public static void fine(Class<?> clazz, String msg, Throwable t) {
        log(clazz, Level.FINE, format(msg, t), t);
    }
    
    public static void info(Class<?> clazz, String msg, Throwable t) {
        log(clazz, Level.INFO, format(msg, t), t);
    }
    
    public static void warn(Class<?> clazz, String msg, Throwable t) {
        log(clazz, Level.WARNING, format(msg, t), t);
    }
    
    public static void severe(Class<?> clazz, String msg, Throwable t) {
        log(clazz, Level.SEVERE, format(msg, t), t);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        log(clazz, Level.INFO, sw.toString(), t);
    }
    
    /*
     * Private methods that do the actual work ...
     */
    
    private static String format(Throwable t) {
    	
    	// Bug 20424066 - JDEV: 12.2.1: ARRAYINDEXOUTOFBOUNDSEXCEPTION IN RAPTORTASKMANAGER$1.UNCAUGHTEXCE
    	// Bug 20385261 - ARRAYINDEXOUTOFBOUNDSEXCEPTION IN RAPTORTASKMANAGER$1.UNCAUGHTEXCEPTION
    	if (t == null) 
    		return Messages.format("Logger_exception", NO_THROWABLE_OBJECT_FOUND, NO_THROWABLE_OBJECT_THREAD_DIED); //$NON-NLS-1$;
    	    	
    	StackTraceElement[] stes = t.getStackTrace();
    	if (stes.length == 0) { 
        	return Messages.format("Logger_exception", t.getLocalizedMessage(), NO_THROWABLE_STACK_ELEMENT); //$NON-NLS-1$
    	} else {
        	return Messages.format("Logger_exception", t.getLocalizedMessage(), t.getStackTrace()[ 0 ].toString()); //$NON-NLS-1$	
    	}
    }
    
    private static String format(String msg, Throwable t) {
        return Messages.format("Logger_detail", msg, format(t));
    }

    public static void log(Class<?> clazz, Level level, String msg, Throwable t) {
        if (null == clazz) {
            clazz = Logger.class;
        }
        LogRecord lr = new LogRecord(level, msg);
        lr.setThrown(t);
        inferCaller(lr); // get the calling class in the log message, not us
        java.util.logging.Logger.getLogger(clazz.getName()).log(lr);
    }
    
    // Ripped/modified from LogRecord
    private static void inferCaller(LogRecord lr) {
        // Get the stack trace.
        StackTraceElement stack[] = (new Throwable()).getStackTrace();
        // First, search back to a method in the Logger class.
        int ix = 0;
        while (ix < stack.length) {
            StackTraceElement frame = stack[ ix ];
            String cname = frame.getClassName();
            if (cname.equals(Logger.class.getName())) {
                break;
            }
            ix++;
        }
        // Now search for the first frame before the "Logger" class.
        while (ix < stack.length) {
            StackTraceElement frame = stack[ ix ];
            String cname = frame.getClassName();
            if (!cname.equals(Logger.class.getName())) {
                // We've found the relevant frame.
                lr.setSourceClassName(cname);
                lr.setSourceMethodName(frame.getMethodName());
                return;
            }
            ix++;
        }
        // We haven't found a suitable frame, so just punt. This is
        // OK as we are only committed to making a "best effort" here.
    }
}
