/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Token;

public class Service {

    public static void main(String[] args) throws Exception {
        /*
         * Set<RuleTuple> rules = ParserEngine.extractRules(); Set<String>
         * keywords = ParserEngine.getKeywords(rules); for( String s : keywords
         * ) System.out.println("| "+s); // (authorized)
         */
        //int p = decrPair(pair(16, 57));
        //System.out.println("x= " + X(p) + ", y= " + Y(p)); // (authorized) //$NON-NLS-1$ //$NON-NLS-2$
        //long p = lPair(123456, 7890);
        //System.out.println("p= " + p + ", x= " + lX(p) + ", y= " + lY(p)); // (authorized) //$NON-NLS-1$ //$NON-NLS-2$
    	
    	//System.out.println(handleMixedCase("emp"));
    	//System.out.println(handleMixedCase("\"DOT.DOt\""));
    	//System.out.println(handleMixedCase("\"HR\".\"DOT.DOT\""));
        
        String input = "й";
        String coded = Service.into4chars(input);
        System.out.println("'"+input+"'"+ " -> '"+coded+"' -> '"+Service.from4chars(coded)+"'");
        System.out.println("'"+Service.from4chars("JJJF")+"'");
        System.out.println("'"+Service.from4chars("JLFFOIJF")+"'");
    }

    /* Read file routines. 
     * 
     * Read a file via a buffered input stream input stream
     * 
     * Can be called with class, string containing the file name or an
     * input stream.
     * 
     * Returns a string containing all the data in the file so should
     * not be used to read very large files.
     * 
     */
    public static String readFile( Class<?> c, String file ) throws IOException  {
        URL u = c.getResource(file);
        if( u == null ) {
           	u = fileNameGitResource(c, file);
        }
        if( u == null )
            throw new FileNotFoundException(file);
        return readFile(u.openStream());
    }

    /**
     * Locate file in git resource dir
     * @param c
     * @param file
     * @return
     * @throws MalformedURLException
     */
	public static URL fileNameGitResource( Class<?> c, String file ) throws MalformedURLException {
		String tmp = c.getResource("").toString();
		final String pattern = "/classes/";
		int pos = tmp.indexOf(pattern)+pattern.length();
		String pkg = tmp.substring(pos);
		tmp = tmp.substring(0,pos);
		pos = tmp.lastIndexOf("common");
		pos = tmp.indexOf("/", pos);
		tmp = tmp.substring(0,pos);
		tmp = tmp + "/common/src/main/resources";
		if( file.startsWith(pkg) )
			file = file.substring(pkg.length());
		if( file.startsWith("/"+pkg) )
			file = file.substring(pkg.length()+1);
		tmp = tmp + '/'+ pkg+file;
		//System.out.println(tmp);
		return new URL(tmp);
	}

    public static String readFile(String file) throws FileNotFoundException, IOException  {
        return readFile(new FileInputStream(file));
    }

    public static String readFile(InputStream is) throws IOException  {
        byte[] bytes = new byte[ 4096 ];
        int bytesRead = 0;
        BufferedInputStream bin = null;
        StringBuffer ret = new StringBuffer();
        try {
            bin = new BufferedInputStream(is);
            bytesRead = bin.read(bytes, 0, bytes.length);
            while (bytesRead != -1) {
                ret.append(new String(bytes).substring(0, bytesRead));
                bytesRead = bin.read(bytes, 0, bytes.length);
            }
        } finally {
            if (bin != null) bin.close();
        }
        return ret.toString();
    }

    /*
     * cleanFileName - replaces characters that are reserved from file names and replaces them with
     * a @.
     */
    public static String cleanFileName(String fileName){
    	return cleanFileName(fileName,"@");
    }
    /*
     * cleanFileName - replaces characters that are reserved from file names and replaces them with
     * the specified string.
     */
    public static String cleanFileName(String fileName, String replace){
    	String clean = fileName.replaceAll("[<>:/\\|?*]", replace);
    	return clean;
    }
    
    /*
     * cleanScriptFileName - replaces characters that are reserved from file names and replaces them with
     * a @.
     * replaces spaces with "_"
     */
    public static String cleanScriptFileName(String fileName){
    	return cleanScriptFileName(fileName,"_","_");
    }
    /*
     * cleanFileName - replaces characters that are reserved from file names and replaces them with
     * the specified string.
     * replaces spaces with a specified string
     */
    public static String cleanScriptFileName(String fileName, String replace, String replaceSpace){
    	//String clean = fileName.replaceAll("[<>:/\\|?*()]", replace);
    	String clean = fileName.replaceAll("[<>:/\\|?*()%&'$@^~/#]", replace);
    	clean = clean.replaceAll(" ", replaceSpace);
    	return clean;
    }
    
    /*
     * static String convertNonAsciiFileName(Stgring fileName)
     */
    
    public static String convertNonAsciiFileName(String fileName){
    	if (!isAscii(fileName)){
  		  return ((Integer)fileName.hashCode()).toString();
    	} 
    	return fileName;
    }
    
    public static boolean isAscii( String s ){ 
  	  int length = s.length(); 
  	  for( int i = 0; i < length; i++){
  		  final char c = s.charAt( i );
  		  if( c > 'z' ){
  			  return false;
  		  }
  	  }
  	  return true;
    }
    
    /*
     * Copy a file to a destination directory.
     * 
     * Creates the new file in the destination directory with the 
     * name componont extracted from the url or the source file.
     */    
    public static void copy( URL url, String dstdir ) {
        InputStream in = null;
        OutputStream out = null;
        JarFile jar = null;
        
        try {
            String name;
            if( url.toString().startsWith("jar:file:") ) {
                int end = url.toString().indexOf('!'); 
                String jarFile = url.toString().substring("jar:file:".length(),end);
                jar =new JarFile(jarFile);
                JarEntry je = jar.getJarEntry(url.toString().substring(end+2));
                in = jar.getInputStream(je);
                String path = je.getName();
                File file = new File(path); 
                name = file.getName();
            } else {                
                String path = url.getPath();
                File file = new File(path); 
                name = file.getName();
                in = new FileInputStream(path);
            }          
            
            out = new FileOutputStream(new File(dstdir+File.separator+name));
            byte[] buf = new byte[1024];
            int len;
            while( (len = in.read(buf)) > 0 ) {
                out.write(buf, 0, len);
            }
        } catch( Exception e ) {
        } finally {
        	if ( in != null ) {
        		try { in.close(); } catch ( Exception ex ) {}
        	}
        	if ( out != null ) {
        		try { out.close(); } catch ( Exception ex ) {}
        	}
        	if ( jar != null ) {
        		try { jar.close(); } catch ( Exception ex ) {}
        	}
        }
    }
    
    /**
     * Copy character-based copy of file.
     * toFile, and parent directories,  will be created if it does not exist.
     * 
     * @param fromPath - path of the source file
     * @param fromEncoding - encoding of the source file
     * @param toPath - path of the target file
     * @param toEncoding - encoding of the target file
     */
    public static void copy( String fromPath, String fromEncoding, String toPath, String toEncoding ) {
    	InputStreamReader reader = null;
    	OutputStreamWriter writer = null;
    	
    	try {
    		FileInputStream fin = new FileInputStream(fromPath);
    		BufferedInputStream bin = new BufferedInputStream(fin);

    		if (fromEncoding == null) {
    			reader = new InputStreamReader(bin); 

    		} else {
    			reader = new InputStreamReader(bin,fromEncoding); 
    		}

    		File toFile = new File(toPath);
    		File toDir = toFile.getParentFile();
    		if (!toDir.isDirectory()){
    			toDir.mkdirs();
    		}
    		if (!toFile.isFile()){
    			toFile.createNewFile();
    		}
    		
    		FileOutputStream fout = new FileOutputStream(toPath);
    		BufferedOutputStream bout = new BufferedOutputStream(fout);

    		if (toEncoding == null) {
    			writer = new OutputStreamWriter(bout); 

    		} else {
    			writer = new OutputStreamWriter(bout,toEncoding); 
    		}

    		int len = 0;
    		char[] buf = new char[1024];
    		while ((len = reader.read(buf,0,1024)) > 0){
    			writer.write(buf,0,len);
    		}

    	} catch (Exception e) {
    		Logger.getLogger(Service.class.getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    	} finally {
    		try {
    			reader.close();
    		} catch (Exception e){
        		Logger.getLogger(Service.class.getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    		}
    		try {
    			writer.close();
    		} catch (Exception e){
        		Logger.getLogger(Service.class.getClass().getName()).log(Level.WARNING, e.getStackTrace()[0].toString(), e);
    		}
    	}	
    }
    
    public static String toNull(String src) {
        return "".equals(src) ? null : src; //$NON-NLS-1$
    }

    public static String identln( int level, String txt ) {
    	return identln(level,txt, " ");
    }
    public static String identln( int level, String txt, String filling ) {
        StringBuffer b = new StringBuffer();
        for(int i = 0; i< level;i++)
            b.append(filling); 
        b.append(txt); 
        return b.toString();
    }
    
    /**
     * Translate character position into line number
     * @param txt
     * @param pos
     * @return
     */
    public static int charPos2LineNo( String txt, int pos ) {
    	int ret = 1;
    	for( int i = 0; i < pos; i++) {
			if( txt.charAt(i) == '\n' )
				ret++;
		}
    	return ret;
    }


    /*
     * Right Pad string with spaces
     * 	
     */
    public static String padln( String txt, int newsize ) {
        StringBuffer b = new StringBuffer(txt);
        for(int i = txt.length(); i < newsize; i++) {
            b.append(" "); 
        }
        return b.toString();
    }

    // interval indexes
    public static int pair(int x, int y) {
        return (y << 16) | x;
    }

    public static int Y( int p ) {
        return p >> 16;
    }

    public static int X( int p ) {
        return p & 0xffff;
    }

    //-----------------------------------------
    /**
     * Bundle two ints into a long
     * @param x
     * @param y
     * @return
     */
    public static long lPair( int x, int y ) {
        return ((long)y << 32) | (long)x;
    }
    /**
     * Extract second component of a pair
     * @param p
     * @return
     */
    public static int lY( long pair ) {
        return (int) (pair >> 32);
    }
    public static int lX( long pair ) {
        return (int)pair;
    }
    
    // x--; y--
    public static int decrPair(int p) {
        return p - 0x10001;
    }

    public static long addlX( long pair, int x ) {
    	return pair+x; 
    }
    
    public static long addlY( long pair, int y ) {
    	return pair+ (((long)y) << 32); 
    }
	//--------------------------------------------------------------

    public static String handleMixedCase( String name ) {
        return handleMixedCase(name, true);
    }
    /**
     * Convert to upper case unquoted name parts
     * @param name  -- input
     * @param unquote -- remove double quotes if true
     * @return
     */
    public static String handleMixedCase( String name, boolean unquote ) {
        List<LexerToken> out = LexerToken.parse(name,true);
        StringBuilder ret = new StringBuilder();
        for( LexerToken t : out ) {
        	String s = t.content;
        	if( t.type == Token.DQUOTED_STRING ) {
        		if( unquote && 1 < s.length() )
        			s = s.substring(1,s.length()-1);
        	} else
        		s = s.toUpperCase();
            ret.append(s);
        }
        return ret.toString();
        
    }
    
    public static String addDoubleQuote( String s ) {
    	return quoteIdentifier(s, '"');
    }
    
    /**
     * Quotes an identifier based on the following:
     * 1) Any lower/mixed case identifier is quoted
     * 2) Any identifier starting with the quote string is NOT quoted. If it is currently quoted (first/last characters 
     * are the quote character) then no quoting is needed, and if it not quoted but contains a quote character, the name is
     * invalid.
     * 3) The string is checked for invalid characters; any invalid characters require the string be quoted. A valid identifier
     * starts with a letter and contains only letters, digits, or one of _$#. Note that the database allows any valid character 
     * in the database character set. We by policy use the basic ASCII character set to determine letters.
     * 
     * Conflicting requirements:
     * - SYS.DBMS_SQLTUNE.ACCEPT_ALL_SQL_PROFILES composite object names shouldn't be quoted
     * - Bug 20667620: weird column name "NO." should
     * @param s
     * @param quoteChar
     * @return
     */
    public static String quoteIdentifier(String s, char quoteChar ) {
    	String quoteString = String.valueOf(quoteChar);
    	if( s == null )   // to be able to safely wrap any string value
    		return null;    	
    	
        // We want to check 3 things here:
        // 1) The string is a valid identifier (starts with a letter, contains only valid characters,--!!!-- don't care if a reserved word
        // 2) The string is all upper case. By policy, we will quote any lower or mixed cased strings
        // 3) the string is not currently quoted. We will only check the first character for a quote. A quote appearing anywhere
        //    else or a starting quote w/o an ending quote is going to make the string invalid no matter what (quotes cannot
        //    be escaped in Oracle identifiers).
        boolean quote = false;
        // Bug 9215024 column name can have "_" so we can't exclude them.
        // ^^^^^^^^^^
        // Edit Wars: the loop below iterates through list of characters, and as soon  
        // it encounters illegitimate symbol, it sets the "need quotation" flag
        String legitimateChars = "$#_";   // !NO ".": see this function JavaDoc above 
        if ( !s.startsWith(quoteString) && !quote ) {
            char[] chars = new char[s.length()];
            s.getChars(0, chars.length, chars, 0);
            if( chars.length > 0 && '0' <= chars[0] && chars[0] <= '9' )
                quote = true;
            else for ( char c : chars ) {
                if( legitimateChars.indexOf(c) < 0 && (
                    c < '0' || '9' < c && c < 'A' || 'Z' < c
                )) {
                    quote = true;
                    break;
                }
                // wierd case with double quote inside
                // ...
            }
        }
        if( s.startsWith("_") || s.startsWith("$") || s.startsWith("#") )
        	quote = true;
        return  quote ? MessageFormat.format("{0}{1}{0}", quoteString, s) : s;//$NON-NLS-1$
    }
    
    // packaging each byte into 2 bytes
    public static String into2chars( String data ) {
        StringBuffer ret = new StringBuffer();
        for( int i = 0; i < data.length(); i++ ) {
            ret.append( into2chars(data.charAt(i)) );
        }
        return ret.toString();
    }
    public static String from2chars( String data ) {
        if( data == null )
            return null;
        StringBuffer ret = new StringBuffer();
        for( int i = 0; i < data.length(); ) {
            char[] chunk = new char[2];
            chunk[0] = data.charAt(i++);
            if( i >= data.length() )
                return null;
            chunk[1] = data.charAt(i++);
            ret.append( from2chars(chunk) );
        }
        if( ret.length() == 0 )
            return null; // not all callers expect empty string
        return ret.toString();
    }
    static char[] into2chars( char input ) {
        char[] ret = new char[2];
        ret[0] = (char)(periodicRemainder(input,16)+70);
        ret[1] = (char)(periodicDivision(input,16)+70);
        return ret;
    }
    static char from2chars( char[] input ) {
        return (char)((input[1]-70)*16+(input[0]-70));
    }
    static int periodicRemainder( int i, int j ) {
        int ret = i%j;
        if( ret < 0 )
            ret += j;
        return ret;
    }
    static int periodicDivision( int i, int j ) {
        int ret = i/j;
        if( ret < 0 )
            ret -= 1;
        return ret;
    }

    // for multibyte need more room
    public static String into4chars( String data ) {
        StringBuffer ret = new StringBuffer();
        for( int i = 0; i < data.length(); i++ ) {
            ret.append( into4chars(data.charAt(i)) );
        }
        return ret.toString();
    }
    public static String from4chars( String data ) {
        if( data == null )
            return null;
        StringBuffer ret = new StringBuffer();
        for( int i = 0; i < data.length(); ) {
            char[] chunk = new char[4];
            chunk[0] = data.charAt(i++);
            if( i >= data.length() )
                return null;
            chunk[1] = data.charAt(i++);
            if( i >= data.length() )
                return null;
            chunk[2] = data.charAt(i++);
            if( i >= data.length() )
                return null;
            chunk[3] = data.charAt(i++);
            ret.append( from4chars(chunk) );
        }
        if( ret.length() == 0 )
            return null; // not all callers expect empty string
        return ret.toString();
    }
    static char[] into4chars( char input ) {
        char[] ret = new char[4];
        int mod = periodicRemainder(input,16*16*16);
        int div = periodicDivision(input,16*16*16);
        ret[3] = (char)(div+70);
        div = periodicDivision(mod,16*16);
        mod = periodicRemainder(mod,16*16);
        ret[2] = (char)(div+70);
        ret[0] = (char)(periodicRemainder(mod,16)+70);
        ret[1] = (char)(periodicDivision(mod,16)+70);
        return ret;
    }
    static char from4chars( char[] input ) {
        return (char)((input[3]-70)*16*16*16+(input[2]-70)*16*16+(input[1]-70)*16+(input[0]-70));
    }
    
    /**
     * Used to init matrix cells at the bottom with a set of guessed symbols
     * @param s
     * @return
     */
    public static int[] toArray( Set<Integer> s ) {
        int[] ret = new int[s.size()];
        int i = 0;
        for( int ii : s )
            ret[i++] = ii;
        return ret;
    }

    public static void profile( final int duration, final int granularity ) {
    	profile(duration, granularity,5); 
    }
    static boolean isRunning = false;
    public static void profile( final int duration, final int granularity, final int stackDepth ) {
    	if( isRunning ) {
    		System.out.println("********fired more than once********");
    		return;
    	}
    	isRunning = true;
        final String threadName =  Thread.currentThread().getName();
        System.out.println("threadName="+threadName);
        new Thread() {
            public void run() {
            	String threadName1 =  Thread.currentThread().getName();
            	final long start = System.currentTimeMillis();
                while( System.currentTimeMillis() < start + duration ) 
                    try {
                        Thread.sleep(granularity);

                        ThreadMXBean bean = ManagementFactory.getThreadMXBean(); 
                        ThreadInfo[] infos = bean.dumpAllThreads(true, true); 

                        boolean divide = false;
                        boolean divide2 = false;
                        for ( ThreadInfo info : infos ) {
                            if( threadName1.equals(info.getThreadName()))
                                continue;
                        	if( divide )
                        		System.out.println("----------------"); 
                        	divide = false;
                           //System.out.println("----"+info.getThreadName()+"----");        
                            StackTraceElement[] elems = info.getStackTrace();
                            int i = 0;
                            for( StackTraceElement elem : elems ) {
                            	if( elem.toString().startsWith("org") 
                            	 || elem.toString().startsWith("oracle.dbtools.raptor.plsql.BackgroundParser")
                            	 || elem.toString().startsWith("oracle.ide.model.FacadeBufferReference$PollingThread")
                                 || elem.toString().startsWith("oracle.ide.util.WeakDataReference$Cleaner")
                                 || elem.toString().startsWith("oracle.ide.status.StatusExecutor")
                                 || elem.toString().startsWith("oracle.ide.log.QueuedLoggingHandler")
                            	)
                            		break;
                            	if( elem.toString().startsWith("java") 
                            	||	elem.toString().startsWith("sun")
                            	||  elem.toString().startsWith("oracle.net.ns")
                            	||  elem.toString().startsWith("oracle.jdbc")
                            	||  elem.toString().startsWith("oracle.dbtools.raptor.backgroundTask")
                            	||  elem.toString().startsWith("oracle.javatools.db.execute.QueryWrapper")
                            	||  elem.toString().startsWith("oracle.javatools.db.execute.ConnectionWrapper")
                            	)
                            		continue;
                                if( i < stackDepth ) {
                                    System.out.println(elem.toString());
                                    divide = true;
                                    divide2 = true;
                                    i++;
                                }
                            }
                        }
                        if( divide2 )
                            System.out.println("================="+(System.currentTimeMillis()-start)); 
                   } catch ( InterruptedException e ) {
                    }
            }
        }.start();
    }
}
