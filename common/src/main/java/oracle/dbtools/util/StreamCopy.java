/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

/**
 * Feed an output stream from an input stream and vice versa. This allows one to
 * drain a stream of existing characters to go to a system console or a file.
 * 
 * @author cdivilly
 */
public class StreamCopy {

	public static long drain(final InputStream is, final OutputStream os)
			throws IOException {
		if (is != null && os != null) {
			final ReadableByteChannel source = Channels.newChannel(is);
			final WritableByteChannel destination = Channels.newChannel(os);
			return drain(source, destination);
		} else {
			return -1;
		}
	}

	public static long drain(final ReadableByteChannel source,
			final WritableByteChannel destination) throws IOException {
		long length = 0;
		final ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
		while (source.read(buffer) != -1) {
			buffer.flip();
			length += buffer.limit();
			destination.write(buffer);
			buffer.compact();
		}
		buffer.flip();
		while (buffer.hasRemaining()) {
			destination.write(buffer);
		}
		return length;
	}

	public static long drain(final Readable source, final Appendable destination)
			throws IOException {
		long length = 0;
		final CharBuffer buffer = CharBuffer.allocate(BUFFER_SIZE);
		while (source.read(buffer) != -1) {
			buffer.flip();
			length += buffer.limit();
			destination.append(buffer);
			buffer.compact();
		}
		return length;
	}

	public static InputStream emptyStream() {
		return EmptyStream.INSTANCE;
	}

	public static String string(final InputStream is) throws IOException {
		try {
			final ByteArrayOutputStream os = new ByteArrayOutputStream();
			int read = 0;
			while (-1 != (read = is.read())) {
				os.write(read);
			}
			os.close();
			return new String(os.toByteArray(), UTF_ENCODING);
		} finally {
			Closeables.close(is);
		}
	}

	public static String string(final Reader r) throws IOException {
		final StringBuilder b = new StringBuilder();
		try {
			final char[] chars = new char[11];
			int count = -1;
			while (-1 != (count = r.read(chars, 0, chars.length))) {
				b.append(chars, 0, count);
			}
		} finally {
			Closeables.close(r);
		}
		return b.toString();
	}

	public static InputStream toInputStream(final String string)
			throws IOException {
		if (null == string) {
			return EmptyStream.INSTANCE;
		}
		return new ByteArrayInputStream(string.getBytes(UTF_ENCODING));
	}

	private static final int         BUFFER_SIZE  = 65536;
	private static final String UTF_ENCODING = "UTF-8";

	private static final class EmptyStream extends InputStream {
		private static final EmptyStream INSTANCE = new EmptyStream();
		@Override
		public int read() throws IOException {
			return -1;
		}
	}
}
