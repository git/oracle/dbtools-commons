/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Some utilities to provide simple and consistent {@link URI} creation.
 * 
 * @author jmcginni
 *
 */
public final class URIUtils {
	
	private static final String DIR = "/";

	/**
	 * Create a new {@link URI} for a URI spec. Any errors in the spec will result in a logged error and a null URI.
	 * @param spec
	 * @return
	 */
	public static URI newURI(String spec) {
		return newURI(spec, false);
	}
	
	/**
	 * Create a new {@link URI} for a URI spec. Any errors in the spec will result in a logged error and a null URI.
	 * @param spec
	 * @param forceDir whether the spec should be forced to end with a trailing "/" character.
	 * @return
	 */
	public static URI newURI(String spec, boolean forceDir) {
		if ( spec == null ) {
			return null;
		}
		
		if ( forceDir ) {
			spec = ensureDir(spec);
		}
		
		try {
			return new URI(spec);
		} catch (URISyntaxException e) {
			Logger.severe(URIUtils.class, e);
		}
		return null;
	}
	
	/**
	 * Create a new {@link URI}. Any errors in the spec will result in a logged error and a null URI.
	 * @param scheme the URI scheme
	 * @param host the host for the URI
	 * @param port the port for the URI
	 * @param path the path for the URI
	 * @return
	 */
	public static URI newURI(String scheme, String host, int port, String path ) {
		return newURI(scheme, host, port, path);
	}
	
	/**
	 * Create a new {@link URI}. Any errors in the spec will result in a logged error and a null URI.
	 * @param scheme the URI scheme
	 * @param host the host for the URI
	 * @param port the port for the URI
	 * @param path the path for the URI
	 * @param forceDir whether the path should be forced to end with a trailing "/" character
	 * @return
	 */
	public static URI newURI(String scheme, String host, int port, String path, boolean forceDir) {
		if ( forceDir) {
			path = ensureDir(path);
		}
		
		try {
			return new URI(scheme, null, host, port, path, null, null);
		} catch (URISyntaxException e) {
			Logger.severe(URIUtils.class, e);
		}
		return null;
	}
	
	/**
	 * Create a new URI based on an existing URI.
	 * @see URI#resolve(String)
	 * @param base the baseURI
	 * @param path the path for the new URI
	 * @param forceDir whether the path should be force to end with a trailing "/" character
	 * @return
	 */
	public static URI newURI(URI base, String path) {
		return newURI(base, path, false);
	}
	/**
	 * Create a new URI based on an existing URI.
	 * @see URI#resolve(String)
	 * @param base the baseURI
	 * @param path the path for the new URI
	 * @param forceDir whether the path should be force to end with a trailing "/" character
	 * @return
	 */
	public static URI newURI(URI base, String path, boolean forceDir) {
		if ( forceDir ) {
			path = ensureDir(path);
		}
		
		return base.resolve(path);
	}
	
	private static String ensureDir(String path) {
		return path == null ? DIR : (path.endsWith(DIR) ? path : path + DIR);
	}
}
