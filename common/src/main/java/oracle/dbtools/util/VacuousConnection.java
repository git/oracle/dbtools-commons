/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.sql.Connection;
import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.Executor;

import oracle.jdbc.LogicalTransactionId;
import oracle.jdbc.LogicalTransactionIdEventListener;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleOCIFailover;
import oracle.jdbc.OracleSavepoint;
import oracle.jdbc.OracleShardingKey;
import oracle.jdbc.aq.AQDequeueOptions;
import oracle.jdbc.aq.AQEnqueueOptions;
import oracle.jdbc.aq.AQMessage;
import oracle.jdbc.aq.AQNotificationRegistration;
import oracle.jdbc.dcn.DatabaseChangeRegistration;
import oracle.jdbc.pool.OracleConnectionCacheCallback;
import oracle.sql.ARRAY;
import oracle.sql.BINARY_DOUBLE;
import oracle.sql.BINARY_FLOAT;
import oracle.sql.DATE;
import oracle.sql.INTERVALDS;
import oracle.sql.INTERVALYM;
import oracle.sql.NUMBER;
import oracle.sql.TIMESTAMP;
import oracle.sql.TIMESTAMPLTZ;
import oracle.sql.TIMESTAMPTZ;
import oracle.sql.TypeDescriptor;

/*
 * Connection that doesn't do anything. Useful for testing sql script processing.
 */
public class VacuousConnection implements OracleConnection {

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return false;
	}

	@Override
	public Statement createStatement() throws SQLException {
		return new VacuousStatement();
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return new VacuousPreparedStatement();
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		return new VacuousCallableStatement();
	}

	@Override
	public String nativeSQL(String sql) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setAutoCommit(boolean autoCommit) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void commit() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void rollback() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isClosed() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setReadOnly(boolean readOnly) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setCatalog(String catalog) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getCatalog() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTransactionIsolation(int level) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clearWarnings() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
			throws SQLException {
		return new VacuousPreparedStatement();
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency)
			throws SQLException {
		return new VacuousCallableStatement();
	}

	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setHoldability(int holdability) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getHoldability() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Savepoint setSavepoint() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Savepoint setSavepoint(String name) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void rollback(Savepoint savepoint) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		return new VacuousPreparedStatement();
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		return new VacuousCallableStatement();
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
		return new VacuousPreparedStatement();
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
		return new VacuousPreparedStatement();
	}

	@Override
	public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
		return new VacuousPreparedStatement();
	}

	@Override
	public Clob createClob() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Blob createBlob() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NClob createNClob() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isValid(int timeout) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setClientInfo(String name, String value) throws SQLClientInfoException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClientInfo(Properties properties) throws SQLClientInfoException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getClientInfo(String name) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Properties getClientInfo() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSchema(String schema) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getSchema() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void abort(Executor executor) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getNetworkTimeout() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Connection _getPC() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void abort() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addLogicalTransactionIdEventListener(LogicalTransactionIdEventListener arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addLogicalTransactionIdEventListener(LogicalTransactionIdEventListener arg0, Executor arg1)
			throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyConnectionAttributes(Properties arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void archive(int arg0, int arg1, String arg2) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean attachServerConnection() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void beginRequest() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cancel() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAllApplicationContext(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close(Properties arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void commit(EnumSet<CommitOption> arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ARRAY createARRAY(String arg0, Object arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BINARY_DOUBLE createBINARY_DOUBLE(double arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BINARY_FLOAT createBINARY_FLOAT(float arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DATE createDATE(Date arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DATE createDATE(Time arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DATE createDATE(Timestamp arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DATE createDATE(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DATE createDATE(Date arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DATE createDATE(Time arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DATE createDATE(Timestamp arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INTERVALDS createINTERVALDS(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INTERVALYM createINTERVALYM(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NUMBER createNUMBER(boolean arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NUMBER createNUMBER(byte arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NUMBER createNUMBER(short arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NUMBER createNUMBER(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NUMBER createNUMBER(long arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NUMBER createNUMBER(float arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NUMBER createNUMBER(double arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NUMBER createNUMBER(BigDecimal arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NUMBER createNUMBER(BigInteger arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NUMBER createNUMBER(String arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Array createOracleArray(String arg0, Object arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMP createTIMESTAMP(Date arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMP createTIMESTAMP(DATE arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMP createTIMESTAMP(Time arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMP createTIMESTAMP(Timestamp arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMP createTIMESTAMP(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPLTZ createTIMESTAMPLTZ(Date arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPLTZ createTIMESTAMPLTZ(Time arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPLTZ createTIMESTAMPLTZ(Timestamp arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPLTZ createTIMESTAMPLTZ(String arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPLTZ createTIMESTAMPLTZ(DATE arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(Date arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(Time arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(Timestamp arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(DATE arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(Date arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(Time arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(Timestamp arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(String arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AQMessage dequeue(String arg0, AQDequeueOptions arg1, byte[] arg2) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AQMessage dequeue(String arg0, AQDequeueOptions arg1, String arg2) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void detachServerConnection(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endRequest() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enqueue(String arg0, AQEnqueueOptions arg1, AQMessage arg2) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TypeDescriptor[] getAllTypeDescriptorsInCurrentSchema() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAuthenticationAdaptorName() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getAutoClose() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public CallableStatement getCallWithKey(String arg0) throws SQLException {
		return new VacuousCallableStatement();
	}

	@Override
	public Properties getConnectionAttributes() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getConnectionReleasePriority() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean getCreateStatementAsRefCursor() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getCurrentSchema() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDataIntegrityAlgorithmName() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DatabaseChangeRegistration getDatabaseChangeRegistration(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getDefaultExecuteBatch() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getDefaultRowPrefetch() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public TimeZone getDefaultTimeZone() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getDescriptor(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getEncryptionAlgorithmName() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public short getEndToEndECIDSequenceNumber() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String[] getEndToEndMetrics() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getExplicitCachingEnabled() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getImplicitCachingEnabled() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getIncludeSynonyms() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object getJavaObject(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LogicalTransactionId getLogicalTransactionId() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Properties getProperties() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getRemarksReporting() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getRestrictGetTables() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getSQLType(Object arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSessionTimeZone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSessionTimeZoneOffset() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getStatementCacheSize() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public PreparedStatement getStatementWithKey(String arg0) throws SQLException {
		return new VacuousPreparedStatement();
	}

	@Override
	public int getStmtCacheSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public short getStructAttrCsId() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public TypeDescriptor[] getTypeDescriptorsFromList(String[][] arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TypeDescriptor[] getTypeDescriptorsFromListInCurrentSchema(String[] arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Properties getUnMatchedConnectionAttributes() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUserName() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getUsingXAFlag() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getXAErrorFlag() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDRCPEnabled() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isLogicalConnection() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isProxySession() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isUsable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean needToPurgeStatementCache() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void openProxySession(int arg0, Properties arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void oracleReleaseSavepoint(OracleSavepoint arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void oracleRollback(OracleSavepoint arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public OracleSavepoint oracleSetSavepoint() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OracleSavepoint oracleSetSavepoint(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public oracle.jdbc.internal.OracleConnection physicalConnectionWithin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int pingDatabase() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pingDatabase(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public CallableStatement prepareCallWithKey(String arg0) throws SQLException {
		return new VacuousCallableStatement();
	}

	@Override
	public PreparedStatement prepareStatementWithKey(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void purgeExplicitCache() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void purgeImplicitCache() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void putDescriptor(String arg0, Object arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public AQNotificationRegistration[] registerAQNotification(String[] arg0, Properties[] arg1, Properties arg2)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void registerConnectionCacheCallback(OracleConnectionCacheCallback arg0, Object arg1, int arg2)
			throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DatabaseChangeRegistration registerDatabaseChangeNotification(Properties arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void registerSQLType(String arg0, Class arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerSQLType(String arg0, String arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerTAFCallback(OracleOCIFailover arg0, Object arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeLogicalTransactionIdEventListener(LogicalTransactionIdEventListener arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setApplicationContext(String arg0, String arg1, String arg2) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAutoClose(boolean arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setConnectionReleasePriority(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCreateStatementAsRefCursor(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDefaultExecuteBatch(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDefaultRowPrefetch(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDefaultTimeZone(TimeZone arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEndToEndMetrics(String[] arg0, short arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setExplicitCachingEnabled(boolean arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setImplicitCachingEnabled(boolean arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setIncludeSynonyms(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPlsqlWarnings(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRemarksReporting(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRestrictGetTables(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSessionTimeZone(String arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setStatementCacheSize(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setStmtCacheSize(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setStmtCacheSize(int arg0, boolean arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setUsingXAFlag(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setWrapper(OracleConnection arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setXAErrorFlag(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void shutdown(DatabaseShutdownMode arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startup(DatabaseStartupMode arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startup(String arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregisterAQNotification(AQNotificationRegistration arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregisterDatabaseChangeNotification(DatabaseChangeRegistration arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregisterDatabaseChangeNotification(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregisterDatabaseChangeNotification(long arg0, String arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregisterDatabaseChangeNotification(int arg0, String arg1, int arg2) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public OracleConnection unwrap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMP createTIMESTAMP(Timestamp arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String getDRCPPLSQLCallbackName() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDRCPReturnTag() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DRCPState getDRCPState() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isDRCPMultitagEnabled() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setShardingKey(OracleShardingKey arg0, OracleShardingKey arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean setShardingKeyIfValid(OracleShardingKey arg0, OracleShardingKey arg1, int arg2) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void startup(DatabaseStartupMode arg0, String arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(Timestamp arg0, ZoneId arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}

class VacuousStatement implements Statement {

	final class VacuousResultSet implements ResultSet {
		private final class VacuousResultSetMetaData implements ResultSetMetaData {
			@Override
			public <T> T unwrap(Class<T> iface) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isWrapperFor(Class<?> iface) throws SQLException {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public int getColumnCount() throws SQLException {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public boolean isAutoIncrement(int column) throws SQLException {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isCaseSensitive(int column) throws SQLException {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isSearchable(int column) throws SQLException {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isCurrency(int column) throws SQLException {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public int isNullable(int column) throws SQLException {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public boolean isSigned(int column) throws SQLException {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public int getColumnDisplaySize(int column) throws SQLException {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public String getColumnLabel(int column) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getColumnName(int column) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getSchemaName(int column) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getPrecision(int column) throws SQLException {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getScale(int column) throws SQLException {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public String getTableName(int column) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getCatalogName(int column) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getColumnType(int column) throws SQLException {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public String getColumnTypeName(int column) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isReadOnly(int column) throws SQLException {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isWritable(int column) throws SQLException {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isDefinitelyWritable(int column) throws SQLException {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public String getColumnClassName(int column) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}
		}

		@Override
		public <T> T unwrap(Class<T> iface) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean next() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void close() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean wasNull() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public String getString(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean getBoolean(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public byte getByte(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public short getShort(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int getInt(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long getLong(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public float getFloat(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public double getDouble(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public byte[] getBytes(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Date getDate(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Time getTime(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Timestamp getTimestamp(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public InputStream getAsciiStream(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public InputStream getUnicodeStream(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public InputStream getBinaryStream(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getString(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean getBoolean(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public byte getByte(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public short getShort(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int getInt(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long getLong(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public float getFloat(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public double getDouble(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public BigDecimal getBigDecimal(String columnLabel, int scale) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public byte[] getBytes(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Date getDate(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Time getTime(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Timestamp getTimestamp(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public InputStream getAsciiStream(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public InputStream getUnicodeStream(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public InputStream getBinaryStream(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public SQLWarning getWarnings() throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void clearWarnings() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public String getCursorName() throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ResultSetMetaData getMetaData() throws SQLException {
			return new VacuousResultSetMetaData();
		}

		@Override
		public Object getObject(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object getObject(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int findColumn(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Reader getCharacterStream(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Reader getCharacterStream(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public BigDecimal getBigDecimal(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isBeforeFirst() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isAfterLast() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isFirst() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isLast() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void beforeFirst() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void afterLast() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean first() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean last() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public int getRow() throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public boolean absolute(int row) throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean relative(int rows) throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean previous() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void setFetchDirection(int direction) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public int getFetchDirection() throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void setFetchSize(int rows) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public int getFetchSize() throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int getType() throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int getConcurrency() throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public boolean rowUpdated() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean rowInserted() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean rowDeleted() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void updateNull(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBoolean(int columnIndex, boolean x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateByte(int columnIndex, byte x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateShort(int columnIndex, short x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateInt(int columnIndex, int x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateLong(int columnIndex, long x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateFloat(int columnIndex, float x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateDouble(int columnIndex, double x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateString(int columnIndex, String x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBytes(int columnIndex, byte[] x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateDate(int columnIndex, Date x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateTime(int columnIndex, Time x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateTimestamp(int columnIndex, Timestamp x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateCharacterStream(int columnIndex, Reader x, int length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateObject(int columnIndex, Object x, int scaleOrLength) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateObject(int columnIndex, Object x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNull(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBoolean(String columnLabel, boolean x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateByte(String columnLabel, byte x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateShort(String columnLabel, short x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateInt(String columnLabel, int x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateLong(String columnLabel, long x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateFloat(String columnLabel, float x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateDouble(String columnLabel, double x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBigDecimal(String columnLabel, BigDecimal x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateString(String columnLabel, String x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBytes(String columnLabel, byte[] x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateDate(String columnLabel, Date x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateTime(String columnLabel, Time x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateTimestamp(String columnLabel, Timestamp x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateAsciiStream(String columnLabel, InputStream x, int length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBinaryStream(String columnLabel, InputStream x, int length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateCharacterStream(String columnLabel, Reader reader, int length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateObject(String columnLabel, Object x, int scaleOrLength) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateObject(String columnLabel, Object x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void insertRow() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateRow() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void deleteRow() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void refreshRow() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void cancelRowUpdates() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void moveToInsertRow() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void moveToCurrentRow() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Statement getStatement() throws SQLException {
			return new VacuousStatement();
		}

		@Override
		public Object getObject(int columnIndex, Map<String, Class<?>> map) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Ref getRef(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Blob getBlob(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Clob getClob(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Array getArray(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Ref getRef(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Blob getBlob(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Clob getClob(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Array getArray(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Date getDate(int columnIndex, Calendar cal) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Date getDate(String columnLabel, Calendar cal) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Time getTime(int columnIndex, Calendar cal) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Time getTime(String columnLabel, Calendar cal) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Timestamp getTimestamp(String columnLabel, Calendar cal) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public URL getURL(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public URL getURL(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void updateRef(int columnIndex, Ref x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateRef(String columnLabel, Ref x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBlob(int columnIndex, Blob x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBlob(String columnLabel, Blob x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateClob(int columnIndex, Clob x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateClob(String columnLabel, Clob x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateArray(int columnIndex, Array x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateArray(String columnLabel, Array x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public RowId getRowId(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public RowId getRowId(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void updateRowId(int columnIndex, RowId x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateRowId(String columnLabel, RowId x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public int getHoldability() throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public boolean isClosed() throws SQLException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void updateNString(int columnIndex, String nString) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNString(String columnLabel, String nString) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNClob(int columnIndex, NClob nClob) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNClob(String columnLabel, NClob nClob) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public NClob getNClob(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public NClob getNClob(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public SQLXML getSQLXML(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public SQLXML getSQLXML(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public String getNString(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getNString(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Reader getNCharacterStream(int columnIndex) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Reader getNCharacterStream(String columnLabel) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateClob(int columnIndex, Reader reader) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateClob(String columnLabel, Reader reader) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNClob(int columnIndex, Reader reader) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateNClob(String columnLabel, Reader reader) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return false;
	}

	@Override
	public ResultSet executeQuery(String sql) throws SQLException {
		return new VacuousResultSet();
	}

	@Override
	public int executeUpdate(String sql) throws SQLException {
		return 0;
	}

	@Override
	public void close() throws SQLException {
	}

	@Override
	public int getMaxFieldSize() throws SQLException {
		return 0;
	}

	@Override
	public void setMaxFieldSize(int max) throws SQLException {
	}

	@Override
	public int getMaxRows() throws SQLException {
		return 0;
	}

	@Override
	public void setMaxRows(int max) throws SQLException {
	}

	@Override
	public void setEscapeProcessing(boolean enable) throws SQLException {
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		return 0;
	}

	@Override
	public void setQueryTimeout(int seconds) throws SQLException {
	}

	@Override
	public void cancel() throws SQLException {
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return null;
	}

	@Override
	public void clearWarnings() throws SQLException {
	}

	@Override
	public void setCursorName(String name) throws SQLException {
	}

	@Override
	public boolean execute(String sql) throws SQLException {
		return false;
	}

	@Override
	public ResultSet getResultSet() throws SQLException {
		return new VacuousResultSet();
	}

	@Override
	public int getUpdateCount() throws SQLException {
		return 0;
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		return false;
	}

	@Override
	public void setFetchDirection(int direction) throws SQLException {
	}

	@Override
	public int getFetchDirection() throws SQLException {
		return 0;
	}

	@Override
	public void setFetchSize(int rows) throws SQLException {
	}

	@Override
	public int getFetchSize() throws SQLException {
		return 0;
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		return 0;
	}

	@Override
	public int getResultSetType() throws SQLException {
		return 0;
	}

	@Override
	public void addBatch(String sql) throws SQLException {
	}

	@Override
	public void clearBatch() throws SQLException {
	}

	@Override
	public int[] executeBatch() throws SQLException {
		return null;
	}

	@Override
	public Connection getConnection() throws SQLException {
		return null;
	}

	@Override
	public boolean getMoreResults(int current) throws SQLException {
		return false;
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		return new VacuousResultSet();
	}

	@Override
	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		return 0;
	}

	@Override
	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		return 0;
	}

	@Override
	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		return 0;
	}

	@Override
	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		return false;
	}

	@Override
	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		return false;
	}

	@Override
	public boolean execute(String sql, String[] columnNames) throws SQLException {
		return false;
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		return 0;
	}

	@Override
	public boolean isClosed() throws SQLException {
		return false;
	}

	@Override
	public void setPoolable(boolean poolable) throws SQLException {
	}

	@Override
	public boolean isPoolable() throws SQLException {
		return false;
	}

	@Override
	public void closeOnCompletion() throws SQLException {
	}

	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		return false;
	}
};

class VacuousPreparedStatement extends VacuousStatement implements PreparedStatement {

	@Override
	public ResultSet executeQuery() throws SQLException {
		return new VacuousResultSet();
	}

	@Override
	public int executeUpdate() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setByte(int parameterIndex, byte x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setShort(int parameterIndex, short x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInt(int parameterIndex, int x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setLong(int parameterIndex, long x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFloat(int parameterIndex, float x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDouble(int parameterIndex, double x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setString(int parameterIndex, String x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDate(int parameterIndex, Date x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTime(int parameterIndex, Time x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearParameters() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setObject(int parameterIndex, Object x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean execute() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addBatch() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRef(int parameterIndex, Ref x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClob(int parameterIndex, Clob x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setArray(int parameterIndex, Array x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setURL(int parameterIndex, URL x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		return new VacuousParameterMetaData();
	}

	@Override
	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNString(int parameterIndex, String value) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		// TODO Auto-generated method stub
		
	}
	
}

class VacuousCallableStatement extends VacuousStatement implements CallableStatement {

	@Override
	public ResultSet executeQuery() throws SQLException {
		return new VacuousResultSet();
	}

	@Override
	public int executeUpdate() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setByte(int parameterIndex, byte x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setShort(int parameterIndex, short x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInt(int parameterIndex, int x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setLong(int parameterIndex, long x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFloat(int parameterIndex, float x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDouble(int parameterIndex, double x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setString(int parameterIndex, String x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDate(int parameterIndex, Date x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTime(int parameterIndex, Time x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearParameters() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setObject(int parameterIndex, Object x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean execute() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addBatch() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRef(int parameterIndex, Ref x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClob(int parameterIndex, Clob x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setArray(int parameterIndex, Array x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setURL(int parameterIndex, URL x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		return new VacuousParameterMetaData();
	}

	@Override
	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNString(int parameterIndex, String value) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerOutParameter(int parameterIndex, int sqlType) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerOutParameter(int parameterIndex, int sqlType, int scale) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean wasNull() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getString(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getBoolean(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public byte getByte(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public short getShort(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getInt(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getLong(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getFloat(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getDouble(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getBigDecimal(int parameterIndex, int scale) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getBytes(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDate(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Time getTime(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp getTimestamp(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getObject(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getBigDecimal(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getObject(int parameterIndex, Map<String, Class<?>> map) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ref getRef(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Blob getBlob(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Clob getClob(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Array getArray(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDate(int parameterIndex, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Time getTime(int parameterIndex, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp getTimestamp(int parameterIndex, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void registerOutParameter(int parameterIndex, int sqlType, String typeName) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerOutParameter(String parameterName, int sqlType) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerOutParameter(String parameterName, int sqlType, int scale) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerOutParameter(String parameterName, int sqlType, String typeName) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public URL getURL(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setURL(String parameterName, URL val) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNull(String parameterName, int sqlType) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBoolean(String parameterName, boolean x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setByte(String parameterName, byte x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setShort(String parameterName, short x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInt(String parameterName, int x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setLong(String parameterName, long x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFloat(String parameterName, float x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDouble(String parameterName, double x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBigDecimal(String parameterName, BigDecimal x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setString(String parameterName, String x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBytes(String parameterName, byte[] x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDate(String parameterName, Date x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTime(String parameterName, Time x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTimestamp(String parameterName, Timestamp x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAsciiStream(String parameterName, InputStream x, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBinaryStream(String parameterName, InputStream x, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setObject(String parameterName, Object x, int targetSqlType, int scale) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setObject(String parameterName, Object x, int targetSqlType) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setObject(String parameterName, Object x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCharacterStream(String parameterName, Reader reader, int length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDate(String parameterName, Date x, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTime(String parameterName, Time x, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTimestamp(String parameterName, Timestamp x, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNull(String parameterName, int sqlType, String typeName) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getString(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getBoolean(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public byte getByte(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public short getShort(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getInt(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getLong(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getFloat(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getDouble(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public byte[] getBytes(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDate(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Time getTime(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp getTimestamp(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getObject(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getBigDecimal(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getObject(String parameterName, Map<String, Class<?>> map) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ref getRef(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Blob getBlob(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Clob getClob(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Array getArray(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDate(String parameterName, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Time getTime(String parameterName, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp getTimestamp(String parameterName, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL getURL(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RowId getRowId(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RowId getRowId(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRowId(String parameterName, RowId x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNString(String parameterName, String value) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNCharacterStream(String parameterName, Reader value, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNClob(String parameterName, NClob value) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClob(String parameterName, Reader reader, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBlob(String parameterName, InputStream inputStream, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNClob(String parameterName, Reader reader, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public NClob getNClob(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NClob getNClob(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSQLXML(String parameterName, SQLXML xmlObject) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SQLXML getSQLXML(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SQLXML getSQLXML(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNString(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNString(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Reader getNCharacterStream(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Reader getNCharacterStream(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Reader getCharacterStream(int parameterIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Reader getCharacterStream(String parameterName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBlob(String parameterName, Blob x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClob(String parameterName, Clob x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAsciiStream(String parameterName, InputStream x, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBinaryStream(String parameterName, InputStream x, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCharacterStream(String parameterName, Reader reader, long length) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAsciiStream(String parameterName, InputStream x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBinaryStream(String parameterName, InputStream x) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCharacterStream(String parameterName, Reader reader) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNCharacterStream(String parameterName, Reader value) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClob(String parameterName, Reader reader) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBlob(String parameterName, InputStream inputStream) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNClob(String parameterName, Reader reader) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <T> T getObject(int parameterIndex, Class<T> type) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T getObject(String parameterName, Class<T> type) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}

class VacuousParameterMetaData implements ParameterMetaData {

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getParameterCount() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int isNullable(int param) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isSigned(int param) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getPrecision(int param) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getScale(int param) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getParameterType(int param) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getParameterTypeName(int param) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getParameterClassName(int param) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getParameterMode(int param) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}
	

}

