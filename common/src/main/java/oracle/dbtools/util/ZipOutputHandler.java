/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;

import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;



/**
 * A utility class with convenience methods for using java.util.zip.ZipOutputStream
 * @author jscapicc
 */
public class ZipOutputHandler {
	ZipOutputStream _out;
	ZipEntry _entry;
	
	FileOutputStream _fout;
	ZipOutputStream _zout;
	OutputStreamWriter _stringWriter;
	
	String _encoding;
	
	String _fileName;
	boolean _entryOpened =false;
	
	/**
	 * An array list of the file I have created.  I'll use this for the controlling script
	 */
	private ArrayList<String> _createdEntries;

	public void openZip(String zipfile) throws FileNotFoundException {
		_out = new ZipOutputStream(new FileOutputStream(zipfile));
		_createdEntries = new ArrayList<String>();
	}

	public void closeZip() throws IOException {
		_out.close();
	}
	
	public void setEncoding(String encoding){
		_encoding = encoding;
	}

	//
	// Open entry just saves the filename and the request to open the entry.
	// The actual open is deferred to allow for excluding zero lenght
	// files from the zip.
	public void openEntry(String fileName) throws IOException {
		_fileName=fileName;
		_entryOpened=false;
	}

	// Do the actual openEntry for the last object requested.
	public void openEntry() throws IOException {
		ZipEntry entry = new ZipEntry(_fileName);
	    _createdEntries.add(_fileName.toString());
		_out.putNextEntry(entry);
		_entryOpened=true;
	}
	
	public void writeEntryText(String text) throws IOException{
		if (!_entryOpened){
			openEntry();
		}
		
		ByteArrayInputStream in;
		//in = new ByteArrayInputStream(text.getBytes("UTF8")); // Stream
		in = new ByteArrayInputStream(text.getBytes(Charset.forName("UTF-8"))); // Stream

		int bytesRead;
		byte[] buffer = new byte[4096]; // Create a buffer for copying
		while ((bytesRead = in.read(buffer)) != -1)
			_out.write(buffer, 0, bytesRead);
	}
	
	public void writeEntryBinaryStream(InputStream reader) throws IOException{
		if (!_entryOpened){
			openEntry();
		}
		
		int bytesRead;
		byte[] buffer = new byte[4096]; // Create a buffer for copying
		while ((bytesRead = reader.read(buffer)) != -1)
			_out.write(buffer, 0, bytesRead);
	}
	
	public void writeEntryCharStream(Reader reader) throws IOException{
		if (!_entryOpened){
			openEntry();
		}
			
		int bytesRead;
		char[] inBuffer = new char[4096];
		String temp=null;
		byte[] buffer = new byte[4096]; // Create a buffer for copying
		while ((bytesRead = reader.read(inBuffer)) != -1){
			temp = new String(inBuffer);
			buffer = temp.getBytes(Charset.forName("UTF-8"));
			_out.write(buffer, 0, bytesRead);
		}	
	}
	
	
	public void writeFileEntry(String filename) throws IOException {
		writeFileEntry(filename, true);
	}
	
	public void writeFileEntry(String filename, boolean skipEmpty) throws IOException {
		File file = new File(filename);
		writeFileEntry(file.toURI().toURL(), skipEmpty);
	}

	public void writeFileEntry(URL url) throws IOException {
		writeFileEntry(url,true);
	}
	
	public void writeFileEntry(URL url, boolean skipEmpty) throws IOException {
		//File f = new File(url.getPath());  // this does not work in all windows environments.
		File f = null;
		try {
			f = new File (url.toURI());
		} catch (URISyntaxException e){
			// if this does happen, we'll get an IOException when we try to use it.  That's ok.
		}
		
		if (f.length() == 0){
			return;
		}
		
		if (f.isDirectory())
			return;
		FileInputStream in = new FileInputStream(f); // Stream to read file
		ZipEntry entry = new ZipEntry(f.getName()); // Make a ZipEntry (relative path)
		_out.putNextEntry(entry); // Store entry
		//_createdEntries.add(f.getPath());
		_createdEntries.add(f.getName());
		int bytesRead;
		byte[] buffer = new byte[4096]; // Create a buffer for copying
		while ((bytesRead = in.read(buffer)) != -1)
			_out.write(buffer, 0, bytesRead);
		in.close();
	}

	public void writeFileEntry(String filePath, String entryPath, boolean skipEmpty) throws IOException {
		//File f = new File(url.getPath());  // this does not work in all windows environments.
		File f = null;
			f = new File (filePath);
		
		if (f.length() == 0){
			return;
		}
		
		if (f.isDirectory())
			return;
		FileInputStream in = new FileInputStream(f); // Stream to read file
		ZipEntry entry = new ZipEntry(entryPath); // Make a ZipEntry (relative path)
		_out.putNextEntry(entry); // Store entry
		//_createdEntries.add(f.getPath());
		_createdEntries.add(entryPath);
		int bytesRead;
		byte[] buffer = new byte[4096]; // Create a buffer for copying
		while ((bytesRead = in.read(buffer)) != -1)
			_out.write(buffer, 0, bytesRead);
		in.close();
	}

	
	public void flush() throws IOException {
		//_out.flush();
	}

	public void closeEntry() throws IOException {
		_out.closeEntry();
	}
	
	/**
	 * 
	 * @param Create a controlling file for the sql files added and include
	 * 		  it in the zip file. 
	 * @param hdrLines
	 * @throws IOException
	 */
	public void addSQLControllingFile(String controllingFile, String hdrLines, boolean isExecutableControllingFile) throws IOException {
		openEntry(controllingFile);
		if (hdrLines != null){
			writeEntryText(hdrLines);
		}
		_createdEntries.remove(_createdEntries.size() - 1);
		if (isExecutableControllingFile){
			for (String s : _createdEntries) {
				if (s.endsWith(".sql")){
					writeEntryText("@" + s + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					writeEntryText("--@" +s + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				}	  
			} 
		} else {
			for (String s : _createdEntries) {
				if (s.endsWith(".sql")){
					writeEntryText("SQL " + s + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				} else if (s.endsWith(".ctl")){
					writeEntryText("LDR " +s + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				}	  
			} 
		}
		closeEntry();
	}
	
	/**
	 * Create a unique entry name by appending a unique number if necessary
	 * @param candidateEntryName
	 * @return uniqueEntryName
	 */
	public String getUniqueEntryName(String candidateFileName, String extension){
		String fileName = candidateFileName+extension;
		int suffix = 0;
		while (_createdEntries.contains(fileName)){
			++suffix;
			fileName = candidateFileName+"_"+suffix+ extension; //$NON-NLS-1$
		}
		return fileName;
	}
	
}