/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util.encoding;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

public class BASE64Encoding extends Encoding implements Decoder, Encoder {
    public BASE64Encoding(MimeType mimeType) {
        super(EncodingType.ENCODING_BASE64, mimeType);
    }

    // Mapping table from 6-bit nibbles to Base64 characters.
    private static char[] map1 = new char[64];
    static {
        int i = 0;
        for (char c = 'A'; c <= 'Z'; c++) {
            map1[i++] = c;
        }
        for (char c = 'a'; c <= 'z'; c++) {
            map1[i++] = c;
        }
        for (char c = '0'; c <= '9'; c++) {
            map1[i++] = c;
        }
        map1[i++] = '+';
        map1[i++] = '/';
    }

    // Mapping table from Base64 characters to 6-bit nibbles.
    private static byte[] map2 = new byte[128];
    static {
        for (int i = 0; i < map2.length; i++) {
            map2[i] = -1;
        }
        for (int i = 0; i < 64; i++) {
            map2[map1[i]] = (byte)i;
        }
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted.
     * @param in  an array containing the data bytes to be encoded.
     * @return    A character array with the Base64 encoded data.
     */
    public char[] encodeToCharArray(byte[] in) throws IOException, EncodingException {
        return encodeToCharArray(in, -1, null);
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted.
     * @param in  an array containing the data bytes to be encoded.
     * @param breakAt   number of character to break at - use -1 if no break required.
     * @param breakChars sequence of break whitespace characters..
     * @return    A character array with the Base64 encoded data.
     */
    public char[] encodeToCharArray(byte[] in, int breakAt,
                                    String breakChars) throws IOException, EncodingException {
        return encodeToString(in, breakAt, breakChars).toCharArray();
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted.
     * @param in  an array containing the data bytes to be encoded.
     * @return    A character array with the Base64 encoded data.
     */
    public String encodeToString(byte[] in) throws IOException, EncodingException {
        return encodeToString(in, -1, null);
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted.
     * @param in  an array containing the data bytes to be encoded.
     * @param breakAt   number of character to break at - use -1 if no break required.
     * @param breakChars sequence of break whitespace characters..
     * @return    A character array with the Base64 encoded data.
     */
    public String encodeToString(byte[] in, int breakAt,
                                 String breakChars) throws IOException, EncodingException {
        StringWriter os = new StringWriter();

        encode(new ByteArrayInputStream(in), os, breakAt, breakChars);

        return os.toString();
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Base64 encoded data.
     * @param in  a string containing the Base64 encoded data.
     * @param breakAt   number of character to break at - use -1 if no break required.
     * @param breakChars sequence of break whitespace characters..
     * @return    The integer value of the required buffer size.
     */
    public long getEncodeLength(byte[] in, int breakAt, String breakChars) {

        return getEncodeLength(in.length, breakAt, breakChars);
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Base64 encoded data.
     * @param inLen  the length of the Base64 encoded data.
     * @param breakAt   number of character to break at - use -1 if no break required.
     * @param breakChars sequence of break whitespace characters..
     * @return    The integer value of the required buffer size.
     */
    public long getEncodeLength(long inLen, int breakAt, String breakChars) {

        long oLen = ((inLen + 2) / 3) * 4; // output length including padding
        long breakLen = 0;

        if (breakAt > 0 && breakChars != null) {
            int breakSeg = breakChars.length();
            breakLen = (oLen / breakAt) * breakSeg;

            if (oLen % breakAt == 0) {
                breakLen = breakLen - breakSeg;
            }
        }

        return oLen + breakLen;
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Base64 encoded data.
     * @param inLen  the length of the Base64 encoded data.
     * @return    The integer value of the required buffer size.
     */
    public long getEncodeLength(long inLen) {

        return getEncodeLength(inLen, -1, null);
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted.
     * @param in   an array containing the data bytes to be encoded.
     * @param out number of bytes to process in <code>in</code>.
     * @return     A character array with the Base64 encoded data.
     */
    public void encode(InputStream in, Writer out) throws IOException, EncodingException {
        encode(in, out, -1, null);
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted.
     * @param in   an array containing the data bytes to be encoded.
     * @param out number of bytes to process in <code>in</code>.
     * @param breakAt   number of character to break at - use -1 if no break required.
     * @param breakChars sequence of break whitespace characters..
     * @return     A character array with the Base64 encoded data.
     */
    public void encode(InputStream in, Writer out, int breakAt,
                       String breakChars) throws IOException, EncodingException {
        long ip = 0;
        long op = 0;
        for (int i0 = in.read(); i0 != -1; i0 = in.read()) {
            ip++;
            int i1 = in.read();
            int i2 = in.read();

            if (i1 == -1) {
                i1 = 0;
            } else {
                i1 = i1 & 0xff;
                ip++;
            }

            if (i2 == -1) {
                i2 = 0;
            } else {
                i2 = i2 & 0xff;
                ip++;
            }

            int o0 = i0 >>> 2;
            int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
            int o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
            int o3 = i2 & 0x3F;

            writeChar(out, map1[o0], op++, breakAt, breakChars);
            writeChar(out, map1[o1], op++, breakAt, breakChars);

            long oDataLen = (ip * 4 + 2) / 3;

            if (op < oDataLen) {
                writeChar(out, map1[o2], op++, breakAt, breakChars);
            }
            if (op < oDataLen) {
                writeChar(out, map1[o3], op++, breakAt, breakChars);
            }
        }

        long oLen = ((ip + 2) / 3) * 4; // output length including padding
        long pCount = oLen - op;
        for (long i = 0; i < pCount; i++) {
            writeChar(out, '=', op++, breakAt, breakChars);
        }
        
        out.flush();
    }

    private static void writeChar(Writer out, int c, long count, int breakAt,
                           String breakChars) throws IOException {
        if (breakAt > 0 && count > 0 && breakChars != null) {
            if (count % breakAt == 0) {
                out.write(breakChars);
            }
        }

        out.write(c);
    }

    /**
     * Decodes a byte array from Base64 format.
     * @param in a Base64 String to be decoded.
     * @return   An array containing the decoded data bytes.
     * @throws   IllegalArgumentException if the input is not valid Base64 encoded data.
     */
    public byte[] decodeToByteArray(char[] in) throws EncodingException, IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        decode(new CharArrayReader(in), os);

        return os.toByteArray();
    }

    /**
     * Decodes a byte array from Base64 format.
     * @param in a Base64 String to be decoded.
     * @return   An array containing the decoded data bytes.
     * @throws   IllegalArgumentException if the input is not valid Base64 encoded data.
     */
    public byte[] decodeToByteArray(String in) throws EncodingException, IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        decode(new StringReader(in), os);

        return os.toByteArray();
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Base64 encoded data.
     * @param in  a string containing the Base64 encoded data.
     * @return    The integer value of the required buffer size.
     * @throws    EncodingException if the input is not valid Base64 encoded data.
     */
    public long getDecodeLength(String in) throws EncodingException {
        long iLen = 0;
        long pCount = 0;

        for (int i = 0; i < in.length(); i++) {
            if (!Character.isWhitespace(in.charAt(i))) {
                iLen++;

                if (in.charAt(i) == '=') {
                    pCount++;
                }
            }
        }

        return getDecodeLength(iLen) - pCount;
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Base64 encoded data.
     * @param inLen  a string containing the Base64 encoded data.
     * @return    The integer value of the required buffer size.
     * @throws    EncodingException if the length is not in blocks of 4.
     */
    public long getDecodeLength(long inLen) throws EncodingException{
        
        if (inLen % 4 != 0) {
            throw new EncodingException("Length of Base64 encoded input string is not a multiple of 4."); //$NON-NLS-1$
        }

        return ((inLen * 3) / 4);
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Base64 encoded data.
     * @param in  a character array containing the Base64 encoded data.
     * @return    The integer value of the required buffer size.
     * @throws    EncodingException if the input is not valid Base64 encoded data.
     */
    public long getDecodeLength(char[] in) throws EncodingException {
        long iLen = 0;
        long pCount = 0;

        for (int i = 0; i < in.length; i++) {
            if (!Character.isWhitespace(in[i])) {
                iLen++;

                if (in[i] == '=') {
                    pCount++;
                }
            }
        }

        return getDecodeLength(iLen) - pCount;
    }

    /**
     * Return the size of the decode block
     * @return size of decode block
     */
    public int getDecodeBlocksize() {
        return 4;
    }

    /**
     * Return the size of the encode block
     * @return size of encode block
     */
    public int getEncodeBlocksize() {
        return 3;
    }

    /**
     * Decodes a byte array from Base64 format.
     * @param in  a stream containing the Base64 encoded data.
     * @param out a stream receiving the decoded data.
     * @return    none.
     * @throws    EncodingException if the input is not valid Base64 encoded data.
     */
    public void decode(Reader in, OutputStream out) throws EncodingException, IOException {
        for (int i0 = readChar(in); i0 != -1; i0 = readChar(in)) {
            int i1 = readChar(in);
            int i2 = readChar(in);
            int i3 = readChar(in);

            if (i3 == -1) {
                throw new EncodingException("Length of Base64 encoded input is not a multiple of 4."); //$NON-NLS-1$
            }

            int x2 = i2;
            int x3 = i3;

            if ((char)i2 == '=') {
                i2 = 'A';
            }
            if ((char)i3 == '=') {
                i3 = 'A';
            }

            if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127) {
                throw new EncodingException("Illegal character in Base64 encoded input."); //$NON-NLS-1$
            }

            int b0 = map2[i0];
            int b1 = map2[i1];
            int b2 = map2[i2];
            int b3 = map2[i3];

            if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0) {
                throw new EncodingException("Illegal character in Base64 encoded input."); //$NON-NLS-1$
            }

            int o0 = (b0 << 2) | (b1 >>> 4);
            int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
            int o2 = ((b2 & 3) << 6) | b3;

            out.write(o0);
            if ((char)x2 != '=') {
                out.write(o1);
            }
            if ((char)x3 != '=') {
                out.write(o2);
            }
        }

        out.flush();
    }

    private static int readChar(Reader in) throws IOException {
        int i = in.read();

        while (i != -1 && Character.isWhitespace((char)i)) {
            i = in.read();
        }

        return i;
    }

    public Object decodeBuffer(Object in) throws EncodingException, IOException {
        if (in == null) {
            return null;
        } else {
            return decodeToByteArray((String)in);
        }
    }

    public void decodeStream(Object in, Object out) throws EncodingException, IOException {
        decode((Reader)in, (OutputStream)out);
    }

    public Object encodeBuffer(Object in) throws EncodingException, IOException {
        return encodeToString((byte[])in);
    }

    public void encodeStream(Object in, Object out) throws EncodingException, IOException {
        encode((InputStream)in, (Writer)out);
    }
}
