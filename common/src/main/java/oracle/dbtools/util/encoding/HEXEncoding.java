/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util.encoding;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=HEXEncoding.java">Barry McGillin</a> 
 *
 */
public class HEXEncoding extends Encoding implements Decoder, Encoder {
    public HEXEncoding(MimeType mimeType) {
        super(EncodingType.ENCODING_HEX, mimeType);
    }

    // Mapping table from 4-bit nibbles to Hex characters.
    private static final char[] map0 =
    { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    private static final char[] map1 =
    { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    // Mapping table from Hex characters to 4-bit nibbles.
    private static byte[] map2 = new byte[128];
    static {
        for (int i = 0; i < map2.length; i++) {
            map2[i] = -1;
        }
        for (int i = 0; i < map0.length; i++) {
            map2[map0[i]] = (byte)i;
        }
        for (int i = 0; i < map1.length; i++) {
            map2[map1[i]] = (byte)i;
        }
    }

    /**
     * Encodes a byte array into Hex format.
     * No blanks or line breaks are inserted.
     * @param in  an array containing the data bytes to be encoded.
     * @return    A character array with the Hex encoded data.
     */
    public char[] encodeToCharArray(byte[] in) throws EncodingException, IOException {
        return encodeToCharArray(in, -1, null);
    }

    /**
     * Encodes a byte array into Hex format.
     * No blanks or line breaks are inserted.
     * @param in  an array containing the data bytes to be encoded.
     * @param breakAt   number of character to break at - use -1 if no break required.
     * @param breakChars sequence of break whitespace characters..
     * @return    A character array with the Hex encoded data.
     */
    public char[] encodeToCharArray(byte[] in, int breakAt,
                                    String breakChars) throws EncodingException, IOException {
        return encodeToString(in, breakAt, breakChars).toCharArray();
    }

    /**
     * Encodes a byte array into Hex format.
     * No blanks or line breaks are inserted.
     * @param in  an array containing the data bytes to be encoded.
     * @return    A character array with the Hex encoded data.
     */
    public String encodeToString(byte[] in) throws EncodingException, IOException {
        return encodeToString(in, -1, null);
    }

    /**
     * Encodes a byte array into Hex format.
     * No blanks or line breaks are inserted.
     * @param in  an array containing the data bytes to be encoded.
     * @param breakAt   number of character to break at - use -1 if no break required.
     * @param breakChars sequence of break whitespace characters..
     * @return    A character array with the Hex encoded data.
     */
    public String encodeToString(byte[] in, int breakAt,
                                 String breakChars) throws EncodingException, IOException {
        StringWriter os = new StringWriter();

        encode(new ByteArrayInputStream(in), os, breakAt, breakChars);

        return os.toString();
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Hex encoded data.
     * @param inLen  length of the data to encode.
     * @return    The integer value of the required buffer size.
     */
    public long getEncodeLength(long inLen) {

        return getEncodeLength(inLen, -1, null);
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Hex encoded data.
     * @param in  a string containing the Hex encoded data.
     * @param breakAt   number of character to break at - use -1 if no break required.
     * @param breakChars sequence of break whitespace characters..
     * @return    The integer value of the required buffer size.
     */
    public long getEncodeLength(byte[] in, int breakAt, String breakChars) {

        return getEncodeLength(in.length, breakAt, breakChars);
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Hex encoded data.
     * @param inLen  length of the data to encode.
     * @param breakAt   number of character to break at - use -1 if no break required.
     * @param breakChars sequence of break whitespace characters..
     * @return    The integer value of the required buffer size.
     */
    public long getEncodeLength(long inLen, int breakAt, String breakChars) {

        long iLen = inLen;
        long oLen = iLen * 2; // output length
        long breakLen = 0;

        if (breakAt > 0 && breakChars != null) {
            int breakSeg = breakChars.length();
            breakLen = (oLen / breakAt) * breakSeg;

            if (oLen % breakAt == 0) {
                breakLen = breakLen - breakSeg;
            }
        }

        return oLen + breakLen;
    }

    /**
     * Encodes a byte array into Hex format.
     * No blanks or line breaks are inserted.
     * @param in   an array containing the data bytes to be encoded.
     * @param out number of bytes to process in <code>in</code>.
     * @return     A character array with the Hex encoded data.
     */
    public void encode(InputStream in, Writer out) throws EncodingException, IOException {
        encode(in, out, -1, null);
    }

    /**
     * Encodes a byte array into Hex format.
     * No blanks or line breaks are inserted.
     * @param in   an array containing the data bytes to be encoded.
     * @param out number of bytes to process in <code>in</code>.
     * @param breakAt   number of character to break at - use -1 if no break required.
     * @param breakChars sequence of break whitespace characters..
     * @return     A character array with the Hex encoded data.
     */
    public void encode(InputStream in, Writer out, int breakAt,
                       String breakChars) throws EncodingException, IOException {
        long ip = 0;
        long op = 0;
        for (int i0 = in.read(); i0 != -1; i0 = in.read()) {
            ip++;

            int v = i0 & 0xff;

            int o0 = v >> 4;
            int o1 = v & 0xf;

            writeChar(out, map0[o0], op++, breakAt, breakChars);
            writeChar(out, map0[o1], op++, breakAt, breakChars);
        }
        
        out.flush();
    }

    private static void writeChar(Writer out, int c, long count, int breakAt,
                                  String breakChars) throws IOException {
        if (breakAt > 0 && count > 0 && breakChars != null) {
            if (count % breakAt == 0) {
                out.write(breakChars);
            }
        }

        out.write(c);
    }

    /**
     * Decodes a byte array from Hex format.
     * @param in a Hex String to be decoded.
     * @return   An array containing the decoded data bytes.
     * @throws   IllegalArgumentException if the input is not valid Hex encoded data.
     */
    public byte[] decodeToByteArray(char[] in) throws EncodingException, IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        decode(new CharArrayReader(in), os);

        return os.toByteArray();
    }

    /**
     * Decodes a byte array from Hex format.
     * @param in a Hex String to be decoded.
     * @return   An array containing the decoded data bytes.
     * @throws   IllegalArgumentException if the input is not valid Hex encoded data.
     */
    public byte[] decodeToByteArray(String in) throws EncodingException, IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        decode(new StringReader(in), os);

        return os.toByteArray();
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Hex encoded data.
     * @param in  a string containing the Hex encoded data.
     * @return    The integer value of the required buffer size.
     * @throws    EncodingException if the input is not valid Hex encoded data.
     */
    public long getDecodeLength(String in) throws EncodingException {
        long iLen = 0;
        for (int i = 0; i < in.length(); i++) {
            if (!Character.isWhitespace(in.charAt(i))) {
                iLen++;
            }
        }

        return getDecodeLength(iLen);
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Hex encoded data.
     * @param in  a character array containing the Hex encoded data.
     * @return    The integer value of the required buffer size.
     * @throws    EncodingException if the input is not valid Hex encoded data.
     */
    public long getDecodeLength(char[] in) throws EncodingException {
        long iLen = 0;

        for (int i = 0; i < in.length; i++) {
            if (!Character.isWhitespace(in[i])) {
                iLen++;
            }
        }

        return getDecodeLength(iLen);
    }

    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Hex encoded data.
     * @param inLen the length of the input.
     * @return    The integer value of the required buffer size.
     * @throws    EncodingException if the length is not in blocks of 2.
     */
    public long getDecodeLength(long inLen) throws EncodingException {

        if (inLen % 2 != 0) {
            throw new EncodingException("Length of Hex encoded input string is not a multiple of 2."); //$NON-NLS-1$
        }
       return inLen / 2;
    }

    /**
     * Return the size of the decode block
     * @return size of decode block
     */
    public int getDecodeBlocksize() {
        return 2;
    }

    /**
     * Return the size of the encode block
     * @return size of encode block
     */
    public int getEncodeBlocksize() {
        return 1;
    }

    /**
     * Decodes a byte array from Hex format.
     * @param in  a stream containing the Hex encoded data.
     * @param out a stream receiving the decoded data.
     * @return    none.
     * @throws    EncodingException if the input is not valid Hex encoded data.
     */
    public void decode(Reader in, OutputStream out) throws EncodingException, IOException {
        for (int i0 = readChar(in); i0 != -1; i0 = readChar(in)) {
            int i1 = readChar(in);

            if (i1 == -1) {
                throw new EncodingException("Length of Hex encoded input is not a multiple of 2."); //$NON-NLS-1$
            }

            if (i0 > 127 || i1 > 127) {
                throw new EncodingException("Illegal character in Hex encoded input."); //$NON-NLS-1$
            }

            int b0 = map2[i0];
            int b1 = map2[i1];

            if (b0 < 0 || b1 < 0) {
                throw new EncodingException("Illegal character in Hex encoded input."); //$NON-NLS-1$
            }

            int o0 = (b0 << 4) | (b1 & 0xf);

            out.write(o0);
        }
        
        out.flush();
    }

    private static int readChar(Reader in) throws IOException {
        int i = in.read();

        while (i != -1 && Character.isWhitespace((char)i)) {
            i = in.read();
        }

        return i;
    }

    public Object decodeBuffer(Object in) throws EncodingException, IOException {
        if (in == null) {
            return null;
        } else {
            return decodeToByteArray((String)in);
        }
    }

    public void decodeStream(Object in, Object out) throws EncodingException, IOException {
        decode((Reader)in, (OutputStream)out);
    }

    public Object encodeBuffer(Object in) throws EncodingException, IOException {
        if (in == null) {
            return null;
        } else {
            return encodeToString((byte[])in);
        }
    }

    public void encodeStream(Object in, Object out) throws EncodingException, IOException {
        encode((InputStream)in, (Writer)out);
    }
}
