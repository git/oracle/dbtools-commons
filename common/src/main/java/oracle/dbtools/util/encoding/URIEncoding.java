/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.util.encoding;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * 
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=URIEncoding.java">Barry McGillin</a> 
 *
 */
public class URIEncoding extends Encoding implements Decoder {
    public URIEncoding(MimeType mimeType) {
        super(EncodingType.ENCODING_URI, mimeType);
    }

    public void decode(Reader in, Writer out) throws URISyntaxException, IOException {
        boolean throwing = true;
        StringBuffer fileValue = new StringBuffer();

        // Read URI value from stream
        int c = in.read();
        while (c != -1) {
            fileValue.append((char)c);
            c = in.read();
        }

        URI uri = new URI(fileValue.toString());

        File iFile = new File(uri);

        FileReader fr = new FileReader(iFile);

        try {
            BufferedReader ir = new BufferedReader(fr);
            
            int x = ir.read();
            while (x != -1) {
                out.write(x);
                x = ir.read();
            }
            
            out.flush();
            throwing = false;
        } finally {
            try {
                fr.close();
            } catch (IOException e) {
                if (throwing) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage(),
                                                                  e);
                } else {
                    throw e;
                }
            }
        }
    }

    public void decode(Reader in, OutputStream out) throws URISyntaxException, IOException {
        boolean throwing = true;
        StringBuffer fileValue = new StringBuffer();

        Reader is = in;
        OutputStream os = out;

        // Read URI value from stream
        int c = is.read();
        while (c != -1) {
            fileValue.append((char)c);
            c = is.read();
        }

        URI uri = new URI(fileValue.toString());

        File iFile = new File(uri);

        FileInputStream fi = new FileInputStream(iFile);

        try {
            BufferedInputStream ir = new BufferedInputStream(fi);

            int x = ir.read();
            while (x != -1) {
                os.write(x);
                x = ir.read();
            }
            
            os.flush();
            throwing = false;
        } finally {
            try {
                fi.close();
            } catch (IOException e) {
                if (throwing) {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getLocalizedMessage(),
                                                                  e);
                } else {
                    throw e;
                }
            }
        }
    }

    public void decodeStream(Object in, Object out) throws EncodingException, IOException {
        try {
            if (out instanceof OutputStream) {
                decode((Reader)in, (OutputStream)out);
            } else {
                decode((Reader)in, (Writer)out);
            }
        } catch (URISyntaxException e) {
            throw new EncodingException(e);
        }
    }
    
    /**
     * Returns the length of the required decoded buffer.
     * No blanks or line breaks are allowed within the Hex encoded data.
     * @param len the length of the input.
     * @return    The integer value of the required buffer size.
     * @throws    EncodingException always as output length cannot be determined from URI name.
     */
    public long getDecodeLength(long len) throws EncodingException{
        throw new EncodingException("Length of output cannot be reliably determined."); //$NON-NLS-1$
    }

    /**
     * Return the size of the decode block
     * @return size of decode block
     */
    public int getDecodeBlocksize() {
        return 1;
    }
}
