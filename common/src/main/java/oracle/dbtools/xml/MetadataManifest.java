/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import oracle.dbtools.util.Logger;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Representation of a metadata.json manifest file.
 * 
 * @author jmcginni
 *
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY )
public final class MetadataManifest {
	public static final String METADATA_FILE = "META-INF/metadata.json"; //$NON-NLS-1$
	
	/**
	 * Create a new manifest instance
	 * @param in the InputStream to read the manifest from
	 * @return the manifest instance
	 * @throws IOException if a read error occurs
	 */
	public static MetadataManifest createManifest(InputStream in) throws IOException {
		MetadataManifest mf = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			
			mf = mapper.readValue(in, MetadataManifest.class);
		} catch ( JsonProcessingException e) {
			Logger.severe(MetadataManifest.class, e.getLocalizedMessage());
		}
		return mf;
	}
		
	private Collection<String> types;
        private Collection<String> viewers;
        private Collection<String> commonQueries;
        private Collection<DDLInfo> oracleddlinfo;

    /**
     * Retrieve the list of types declared in the manifest
     * @return a collection of type declaration files
     */
    public Collection<String> types() {
            return types;
    }
    
    /**
     * Retrieve the list of viewers declared in the manifest
     * @return a collection of viewer declaration files
     */
    public Collection<String> viewers() {
            return viewers;
    }
    
    
    /**
     * Retrieve the list of commonQueries declared in the manifest
     * @return a collection of viewer declaration files
     */
    public Collection<String> commonQueries() {
            return commonQueries;
    }
    
    /**
     * Retrieve the list of oracledbaddl declared in the manifest
     * @return a collection of DDLInfo objects
     */
    public Collection<MetadataManifest.DDLInfo> getOracleDDLInfo()
    {
        return oracleddlinfo;
    }

    public static class Bind implements Comparable<Bind>
    {
        String name;
        String value;

        public String getName()
        {
            return name;
        }

        public String getValue()
        {
            return value;
        }

        public String toString()
        {
            return getName() + "->" + getValue();
        }
        
        public int hashCode()
        {
            return toString().hashCode();
        }

        @Override
        public int compareTo(Bind bind)
        {
            return this.toString().compareTo(bind.toString());
        }
    };

    public static class DDLInfo implements Comparable<DDLInfo>
    {
        String clazz;
        String objectType;
        String ddlId;
        String queryXML;
        Collection<Bind> binds;

        public String getObjectType()
        {
            return objectType;
        }

        public String getQueryXML()
        {
            return queryXML;
        }

        public String getClazz()
        {
            return clazz;
        }

        public String getDdlId()
        {
            return ddlId;
        }

        public Collection<MetadataManifest.Bind> getBinds()
        {
            return binds;
        }

        public String toString()
        {
            return getObjectType();
        }
        
        public int hashCode()
        {
            return getObjectType().hashCode();
        }

        @Override
        public int compareTo(DDLInfo ddlInfo)
        {
            return this.getObjectType().compareTo(ddlInfo.getObjectType());
        }
    }
}
