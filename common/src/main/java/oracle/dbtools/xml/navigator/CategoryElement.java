/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.xml.navigator;

import java.util.List;
import java.util.Set;

import oracle.dbtools.common.utils.Version;
import oracle.xml.parser.v2.XMLNode;

/**
 * Parse result for a Category element in a navigator descriptor.
 * 
 * @author jmcginni
 *
 */
public final class CategoryElement extends FolderOwner {
    private static final String REQUIRED_FEATURES = "requiredFeatures"; //$NON-NLS-1$
    private static final String REQUIRED_ACCESS_OBJECTS = "requiredAccessObjects"; //$NON-NLS-1$

	private FolderElement folder;
	private Version minimumVersion;
	private Version maximumVersion;
	private Set<String> requiredObjects;
	private Set<String> requiredFeatures;
	private boolean ignoreComparator;
	
	CategoryElement(XMLNode node) {
		super(node);
		
		List<FolderElement> folders = getFolders();
		if ( folders.size() > 0 ) {
			folder = folders.get(0);
		}
		
		minimumVersion = getVersion(CURRENT_ELEMENT, MIN_VERSION_ATTRIBUTE, node);
		maximumVersion = getVersion(CURRENT_ELEMENT, MAX_VERSION_ATTRIBUTE, node);
        
        requiredObjects = parseSet(node, REQUIRED_ACCESS_OBJECTS); 
        requiredFeatures = parseSet(node, REQUIRED_FEATURES); 
        
        ignoreComparator = FolderElement.parseIgnoreComparator(node);
	}
	
	/**
	 * Retrieve the Category type (name).
	 * @return
	 */
	public String getType() {
		return getID();
	}

	/**
	 * Retrieve the folder element for the category
	 * @return
	 */
	public FolderElement getFolder() {
		return folder;
	}

	/**
	 * Retrieve the minimum database version supported by the category.
	 * @return
	 */
	public Version getMinimumVersion() {
		return minimumVersion;
	}

	/**
	 * Retrieve the maximum database version supported by the category.
	 * @return
	 */
	public Version getMaximumVersion() {
		return maximumVersion;
	}

	/**
	 * Retrieve the objects that must be accessible for the category to be valid
	 * @return
	 */
	public Set<String> getRequiredObjects() {
		return wrapSet(requiredObjects);
	}

	/**
	 * Retrieve the features that must be present for the category to be valid
	 * @return
	 */
	public Set<String> getRequiredFeatures() {
		return wrapSet(requiredFeatures);
	}

	/**
	 * Whether the contents of the category should ignore the default comparator.
	 * @return
	 */
	public boolean isIgnoreComparator() {
		return ignoreComparator;
	}
	
}
