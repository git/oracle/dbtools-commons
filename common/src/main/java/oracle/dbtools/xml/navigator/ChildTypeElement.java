/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.xml.navigator;

import oracle.dbtools.raptor.query.ObjectQueries;
import oracle.xml.parser.v2.XMLNode;

/**
 * Parse result for a childType element in a navigator descriptor.
 * 
 * @author jmcginni
 *
 */
public class ChildTypeElement extends ItemDescriptor {
	private static final String FOLDER_ATTRIBUTE = "folder"; //$NON-NLS-1$
	private static final String SHOW_FOLDER = "showFolder"; //$NON-NLS-1$
	private static final String PROV_IMPL_CLASS = "provImplClass"; //$NON-NLS-1$
	private static final String NODE_TYPE = "nodeType"; //$NON-NLS-1$
	
	private ObjectQueries queries;
	private String nodeType;
	private String providerImplementationClass;
	private boolean showFolder;
	private LabelElement folderLabel;
	private IconElement folderIcon;
	
	ChildTypeElement(XMLNode node) {
		super(node);

		queries = new ObjectQueries(getSingleElement(QUERIES_ELEMENT, node));
		nodeType = getAttributeValue(CURRENT_ELEMENT, NODE_TYPE, node);
		providerImplementationClass = getAttributeValue(CURRENT_ELEMENT, PROV_IMPL_CLASS, node);
		showFolder = getBooleanValue(CURRENT_ELEMENT, SHOW_FOLDER, node);
		
		XMLNode folder = getSingleElement(FOLDER_ATTRIBUTE, node);
		if ( folder != null ) {
			folderLabel = new LabelElement(folder);
			folderIcon = new IconElement(folder);
		}
	}
	
	/**
	 * Retrieve the name of the child type
	 * @return
	 */
	public String getName() {
		return getID();
	}
	
	/**
	 * Retrieve the queries for the child type
	 * @return
	 */
	public ObjectQueries getQueries() {
		return queries;
	}
	
	/**
	 * Retrieve the node type for the children.
	 * @return
	 */
	public String getNodeType() {
		return nodeType;
	}
	
	/**
	 * Retrieve the optional implementation class for the child type
	 * @return
	 */
	public String getProviderImplementationClass() {
		return providerImplementationClass;
	}
	
	/**
	 * Whether the child type should be sorted under a folder
	 * @return
	 */
	public boolean isShowFolder() {
		return showFolder;
	}
	
	/**
	 * Retrieve the label for the folder
	 * @return
	 */
	public LabelElement getFolderLabel() {
		return folderLabel;
	}
	
	/**
	 * Retrieve the icon for the folder
	 * @return
	 */
	public IconElement getFolderIcon() {
		return folderIcon;
	}
}
