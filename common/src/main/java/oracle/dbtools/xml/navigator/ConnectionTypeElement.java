/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.xml.navigator;

import oracle.dbtools.common.utils.Version;
import oracle.xml.parser.v2.XMLNode;

/**
 * The parse result for a connectionType element in a navigator descriptor
 * 
 * @author jmcginni
 *
 */
public final class ConnectionTypeElement extends Descriptor {
	private static final String CONNECTION_TAB_NAME = "connectionTabName"; //$NON-NLS-1$
	private static final String CONNECTION_PANEL_CLASS = "connectionPanelClass"; //$NON-NLS-1$
	private static final String PROVIDER_CLASS = "providerClass"; //$NON-NLS-1$
	private static final String IMPL_CLASS = "implClass"; //$NON-NLS-1$
	private static final String SUPPORTS_ROW_ID = "supportsRowId"; //$NON-NLS-1$
	private static final String SUPPORTS_CATALOG = "supportsCatalog"; //$NON-NLS-1$
	private static final String SHOW_ALL_SCHEMAS = "showAllSchemas"; //$NON-NLS-1$
	private static final String SUPPORTS_OTHER_SCHEMAS = "supportsOtherSchemas"; //$NON-NLS-1$
	// Note that these two are not the same as MAX_VERSION_ATTRIBUTE and MIN_VERSION_ATTRIBUTE
	// case is different in the attribute names
	private static final String MAX_VERSION = "maxVersion"; //$NON-NLS-1$
	private static final String MIN_VERSION = "minVersion"; //$NON-NLS-1$

	private IconElement icon;
	
	private Version minimumVersion;
	private Version maximumVersion;
	
	private boolean otherSchemasSupport;
	private boolean showAllSchemas;
	private boolean supportsCatalogs;
	private boolean supportsRowID;

	private String implClass;
	private String providerClass;
	
	private String panelClass;
	private String panelLabel;
	
	ConnectionTypeElement(XMLNode node) {
		super(node);
		icon = new IconElement(node);
		
		minimumVersion = getVersion(CURRENT_ELEMENT, MIN_VERSION, node);
		maximumVersion = getVersion(CURRENT_ELEMENT, MAX_VERSION, node);
		
        otherSchemasSupport = getBooleanValue(CURRENT_ELEMENT, SUPPORTS_OTHER_SCHEMAS, node); 
        showAllSchemas = getBooleanValue(CURRENT_ELEMENT, SHOW_ALL_SCHEMAS, node); 
        supportsCatalogs = getBooleanValue(CURRENT_ELEMENT, SUPPORTS_CATALOG, node); 
        supportsRowID = getBooleanValue(CURRENT_ELEMENT, SUPPORTS_ROW_ID, node); 
		
		implClass = getAttributeValue(CURRENT_ELEMENT, IMPL_CLASS, node);
		providerClass = getAttributeValue(CURRENT_ELEMENT, PROVIDER_CLASS, node);
		
        panelClass = getAttributeValue(CURRENT_ELEMENT, CONNECTION_PANEL_CLASS, node); 
        panelLabel = getAttributeValue(CURRENT_ELEMENT, CONNECTION_TAB_NAME, node); 
	}
	
	/**
	 * Retrieve the connection type.
	 * @return
	 */
	public String getType() {
		return getID();
	}
	
	/**
	 * Retrieve the icon for the type
	 * @return
	 */
	public IconElement getIcon() {
		return icon;
	}
	
	/**
	 * Retrieve the optional minimum database version to support the connection type
	 * @return
	 */
	public Version getMinimumVersion() {
		return minimumVersion;
	}
	
	/**
	 * Retrieve the optional maximum database version to support the connection type
	 * @return
	 */
	public Version getMaximumVersion() {
		return maximumVersion;
	}
	
	/**
	 * Whether the other schemas object is supported
	 * 
	 * @return
	 */
	public boolean isOtherSchemasSupport() {
		return otherSchemasSupport;
	}
	
	/**
	 * Whether all schemas should be shown at the top level
	 * @return
	 */
	public boolean isShowAllSchemas() {
		return showAllSchemas;
	}
	
	/**
	 * Whether the connection type supports catalogs
	 * @return
	 */
	public boolean isSupportsCatalogs() {
		return supportsCatalogs;
	}
	
	/**
	 * Whether the connection type supports using ROWID in updates.
	 * @return
	 */
	public boolean isSupportsRowID() {
		return supportsRowID;
	}
	
	/**
	 * Retrieve the implementation class for the ConnectionType
	 * @return
	 */
	public String getImplClass() {
		return implClass;
	}
	
	/**
	 * Retrieve the provider class to use to generate children of the connection type
	 * @return
	 */
	public String getProviderClass() {
		return providerClass;
	}
	
	/**
	 * Retrieve the panel for the connection dialog to specify details of connections of this type.
	 * @return
	 */
	public String getPanelClass() {
		return panelClass;
	}
	
	/**
	 * Retrieve the label for the connection type panel.
	 * @return
	 */
	public String getPanelLabel() {
		return panelLabel;
	}
}
