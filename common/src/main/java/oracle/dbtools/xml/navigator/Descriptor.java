/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.xml.navigator;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.common.utils.Version;
import oracle.dbtools.raptor.utils.XMLHelper;
import oracle.xml.parser.v2.XMLNode;
import oracle.xml.parser.v2.XSLException;

import org.w3c.dom.NodeList;

/**
 * Basic descriptor support for navigator descriptor elements.
 * 
 * @author jmcginni
 *
 */
public abstract class Descriptor implements NavigatorXmlElements {
	private final String id;
	
	protected Descriptor(XMLNode node) {
		id = getAttributeValue(CURRENT_ELEMENT, ID_ATTRIBUTE, node); 
	}
	
	/**
	 * Retrieve the ID attribute for the element
	 * @return
	 */
	protected final String getID() {
		return id;
	}
	
    protected static String getAttributeValue(String elementPath, String attribute, XMLNode node) {
        String value = XMLHelper.getAttributeNode(node, attribute);
        if ( value == null && elementPath != null) {
        	value =  XMLHelper.getAttributeNode(XMLHelper.getChildNode(node, elementPath), attribute);
        }
        
        return value;
    }
    
    protected static boolean getBooleanValue(String elementPath, String attribute, XMLNode node) {
    	return Boolean.valueOf(getAttributeValue(elementPath, attribute, node));
    }

    protected static XMLNode getSingleElement(String elementPath, XMLNode node) {
        XMLNode element = null;
        try {
            element = (XMLNode) node.selectSingleNode(elementPath);
        } catch (XSLException e) {

        }
        return element;
    }

    protected static XMLNode[] getElements(String elementPath, XMLNode node) {
        XMLNode[] elements = null;
        try {
            NodeList nl = node.selectNodes(elementPath);
            elements = new XMLNode[ nl.getLength() ];
            for (int i = 0; i < elements.length; i++) {
                elements[ i ] = (XMLNode) nl.item(i);
            }
        } catch (XSLException e) {

        }
        return elements;
    }
    
    /**
     * Extract comma delimited set from XML attribute.
     * @param node
     * @param attr
     * @return
     */
    protected static Set<String> parseSet(XMLNode node, String attr) {
        Set<String> result = null;
        String reqs = getAttributeValue(CURRENT_ELEMENT, attr, node);
        if ( reqs != null ) {
            result = new HashSet<String>();
            Collections.addAll(result, reqs.split(",")); //$NON-NLS-1$
        }
        return result;
    }

    protected static Version getVersion(String elementPath, String attribute, XMLNode node) {
    	String versionString = getAttributeValue(elementPath, attribute, node);
    	return ModelUtil.hasLength(versionString) ? new Version(versionString) : null;
    }

    protected static final <T> List<T> wrapList(List<T> list) {
    	if ( list != null ) {
    		return Collections.unmodifiableList(list);
    	}
    	return Collections.emptyList();
    }

    protected static final <K,V> Map<K,V> wrapMap(Map<K,V> map) {
    	if ( map != null ) {
    		return Collections.unmodifiableMap(map);
    	}
    	return Collections.emptyMap();
    }
    
    protected static final <T> Set<T> wrapSet(Set<T> set) {
    	if (set != null) {
    		return Collections.unmodifiableSet(set);
    	}
    	return Collections.emptySet();
    }
}
