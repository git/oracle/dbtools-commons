/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.xml.navigator;

import oracle.dbtools.raptor.query.ObjectQueries;
import oracle.xml.parser.v2.XMLNode;

/**
 * Folder element in a navigator descriptor
 * @author jmcginni
 *
 */
public class FolderElement extends Descriptor {
    private static final String SCHEMA_OVERRIDE = "schemaOverride"; //$NON-NLS-1$
	private static final String RELATIVE_WEIGHT = "relativeWeight"; //$NON-NLS-1$
	private static final String SINGLE_CLICK_OPEN = "singleClickOpen"; //$NON-NLS-1$
	private static final String CURRENT_SCHEMA_ONLY = "currentSchemaOnly"; //$NON-NLS-1$
	private static final String IGNORE_COMPARATOR = "ignoreComparator"; //$NON-NLS-1$

	static boolean parseIgnoreComparator(XMLNode node) {
        return Boolean.parseBoolean(getAttributeValue(CURRENT_ELEMENT, IGNORE_COMPARATOR, node));
    }

	private LabelElement label;
	private IconElement icon;
	private ObjectQueries queries;
	private boolean currentSchemaOnly;
	private boolean singleClickOpen;
	private String schemaOverride;
	private double relativeWeight;
	private boolean ignoreComparator;
	
	FolderElement(XMLNode node) {
		super(node);

		label = new LabelElement(node);
		icon = new IconElement(node);
		
		queries = new ObjectQueries(getSingleElement(QUERIES_ELEMENT, node));

		currentSchemaOnly = getBooleanValue(CURRENT_ELEMENT, CURRENT_SCHEMA_ONLY, node);
		singleClickOpen = getBooleanValue(CURRENT_ELEMENT, SINGLE_CLICK_OPEN, node);
		
        String relWeight = getAttributeValue(null, RELATIVE_WEIGHT, node); 
        if (relWeight != null) {
            try {
            	relativeWeight = Double.parseDouble(relWeight);
            } catch (NumberFormatException nfe) {
            }
        }
        
		schemaOverride = getAttributeValue(null, SCHEMA_OVERRIDE, node);
		ignoreComparator = parseIgnoreComparator(node);
	}
	
	public LabelElement getLabel() {
		return label;
	}
	
	public IconElement getIcon() {
		return icon;
	}

	public ObjectQueries getQueries() {
		return queries;
	}

	public boolean isCurrentSchemaOnly() {
		return currentSchemaOnly;
	}

	public boolean isSingleClickOpen() {
		return singleClickOpen;
	}

	public String getSchemaOverride() {
		return schemaOverride;
	}

	public double getRelativeWeight() {
		return relativeWeight;
	}

	public boolean isIgnoreComparator() {
		return ignoreComparator;
	}
	
}
