/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.xml.navigator;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import oracle.xml.parser.v2.XMLNode;

/**
 * Navigator descriptor element that can contain folder definitions.
 * 
 * @author jmcginni
 *
 */
public abstract class FolderOwner extends Descriptor {
    private static final String CONNECTION_TYPE = "connType"; //$NON-NLS-1$
	private static final String INCLUDES_SYNONYMS = "includesSyns"; //$NON-NLS-1$
	private static final String ELEMENT_FOLDER = "folder"; //$NON-NLS-1$
    private static final String ATTRIBUTE_WEIGHT = "weight"; //$NON-NLS-1$
    private static final String ATTRIBUTE_REQUIRES_PARENT = "requiresParent"; //$NON-NLS-1$
    private static final String ATTRIBUTE_TREEONLY = "treeonly"; //$NON-NLS-1$
	private static final String REQUIRED_LICENSES = "requiredLicenses";

	private double weight = Double.NEGATIVE_INFINITY;
	private boolean includeSynonyms;
	private boolean treeOnly;
	private boolean requiresParent;
	private List<FolderElement> folders;
	private String dbType;
	private String category;
	private Set<String> requiredLicences;
	
	FolderOwner(XMLNode node) {
		super(node);
		treeOnly = getBooleanValue(CURRENT_ELEMENT, ATTRIBUTE_TREEONLY, node);
		requiresParent = getBooleanValue(CURRENT_ELEMENT, ATTRIBUTE_REQUIRES_PARENT, node);
		includeSynonyms = getBooleanValue(CURRENT_ELEMENT, INCLUDES_SYNONYMS, node);
		
		dbType = getAttributeValue(CURRENT_ELEMENT, CONNECTION_TYPE, node);
		category = getAttributeValue(CURRENT_ELEMENT, TYPE_CATEGORY_ATTRIBUTE, node);
        requiredLicences = parseSet(node, REQUIRED_LICENSES); 
		
		String weight = getAttributeValue(CURRENT_ELEMENT, ATTRIBUTE_WEIGHT, node); 
        if (weight != null) {
            try {
                this.weight = Double.parseDouble(weight);
            } catch (NumberFormatException nfe) {

            }
        }

        XMLNode[] folderDescs = getElements(ELEMENT_FOLDER, node);
        if ( folderDescs.length > 0 ) {
        	folders = new ArrayList<FolderElement>();
        	for ( XMLNode folder : folderDescs ) {
        		folders.add(new FolderElement(folder));
        	}
        }
	}
	
	public final double getWeight() {
		return weight;
	}

	public final boolean isIncludeSynonyms() {
		return includeSynonyms;
	}

	public final boolean isTreeOnly() {
		return treeOnly;
	}

	public final boolean isRequiresParent() {
		return requiresParent;
	}

	public final List<FolderElement> getFolders() {
		return wrapList(folders);
	}

	public final String getDatabaseType() {
		return dbType;
	}
	
	public final String getCategory() {
		return category;
	}

	/**
	 * Retrieve the licenses that must be enabled for the category to be valid
	 * @return
	 */
	public Set<String> getRequiredLicences() {
		return wrapSet(requiredLicences);
	}
}