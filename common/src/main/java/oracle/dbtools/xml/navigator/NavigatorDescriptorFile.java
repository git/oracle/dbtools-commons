/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.xml.navigator;

import java.util.ArrayList;
import java.util.List;

import oracle.dbtools.raptor.utils.XMLHelper;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLNode;

/**
 * Representation of a Navigator xml descriptor file.
 * 
 * @author jmcginni
 *
 */
public final class NavigatorDescriptorFile implements NavigatorXmlElements {
	/**
	 * Create a new NavigatorDescriptorFile from an XMLDocument
	 * @param doc the XMLDocument representing the descriptor file
	 * @return the NavigatorDescriptorFile
	 */
	public static NavigatorDescriptorFile createDescriptor(XMLDocument doc) {
		NavigatorDescriptorFile desc = null;
		XMLNode navNode = (XMLNode) XMLHelper.getChildNode(doc, NAVIGATOR_ELEMENT);
		if ( navNode != null ) {
			desc = new NavigatorDescriptorFile(navNode);
		}
		return desc;
	}

	private final String resourceKey;
	
	private final List<ConnectionTypeElement> connectionTypes;

	private final List<ObjectTypeElement> types;
	private final List<CategoryElement> categories;

	private NavigatorDescriptorFile(XMLNode node) {
		// The resfile attribute
		resourceKey = XMLHelper.getAttributeNode(node, RESFILE_ATTRIBUTE); 

		// Connection Types (optional)
		{
			List<ConnectionTypeElement> l = new ArrayList<ConnectionTypeElement>();
			XMLNode[] nl = XMLHelper.getChildNodes(node, CONNECTION_ELEMENT);
			if (nl != null && nl.length > 0) {
				for (int i = 0; i < nl.length ;i++) {
					l.add(new ConnectionTypeElement(nl[i]));
				}
			}
			connectionTypes = l.size() > 0 ? l : null;
		}

		// Object Types (optional)
		{
			List<ObjectTypeElement> l = new ArrayList<ObjectTypeElement>();
			for ( XMLNode n: Descriptor.getElements(TYPE_ELEMENT_PATH, node)) {
				l.add( new ObjectTypeElement(n));
			}
			types = l.size() > 0 ? l : null;
		}

		// Categories (optional)
		{
			List<CategoryElement> l = new ArrayList<CategoryElement>();
			for ( XMLNode n : Descriptor.getElements(CATEGORY_ELEMENT_PATH, node)) {
				l.add(new CategoryElement(n));
			}
			categories = l.size() > 0 ? l : null;
		}
	}

	public List<ObjectTypeElement> getTypes() {
		return Descriptor.wrapList(types);
	}

	public List<CategoryElement> getCategories() {
		return Descriptor.wrapList(categories);
	}

	public String getResourceKey() {
		return resourceKey;
	}

	public List<ConnectionTypeElement> getConnectionTypes() {
		return Descriptor.wrapList(connectionTypes);
	}
}
