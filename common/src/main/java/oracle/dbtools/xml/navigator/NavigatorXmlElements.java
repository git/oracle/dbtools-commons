/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.xml.navigator;

public interface NavigatorXmlElements {
	String CURRENT_ELEMENT = ""; //$NON-NLS-1$
    String TYPE_ELEMENT_PATH = "/navigator/objectType"; //$NON-NLS-1$
    String CATEGORY_ELEMENT_PATH = "/navigator/categoryType"; //$NON-NLS-1$
    String ID_ATTRIBUTE = "id"; //$NON-NLS-1$

    String QUERY_ELEMENT = "query"; //$NON-NLS-1$
    String MIN_VERSION_ATTRIBUTE = "minversion"; //$NON-NLS-1$
    String MAX_VERSION_ATTRIBUTE = "maxversion"; //$NON-NLS-1$
    String CONSTRAINED_ATTRIBUTE = "constrained"; //$NON-NLS-1$

    String ARGUMENTS_ELEMENT = "arguments"; //$NON-NLS-1$
    String REQUIRED_VALUE_ELEMENT = "requiredValue"; //$NON-NLS-1$
    String RESULT_ELEMENT = "result"; //$NON-NLS-1$
    String OPTIONAL_VALUE_ELEMENT = "optional"; //$NON-NLS-1$

    String TYPE_ATTRIBUTE = "type"; //$NON-NLS-1$
    String FILTERABLE_ATTRIBUTE = "filterable"; //$NON-NLS-1$
    String SORTABLE_ATTRIBUTE = "sortable"; //$NON-NLS-1$
    String TYPE_CATEGORY_ATTRIBUTE = "category"; //$NON-NLS-1$
    
    String LABEL_ELEMENT = "label"; //$NON-NLS-1$
    String ICON_ELEMENT = "icon"; //$NON-NLS-1$
    String QUERIES_ELEMENT = "queries"; //$NON-NLS-1$
    String RSKEY_ATTRIBUTE = "RSKEY"; //$NON-NLS-1$
    String PATH_ATTRIBUTE = "PATH";  //$NON-NLS-1$

    String CHILDTYPE_ELEMENT = "childType"; //$NON-NLS-1$
    
    String NAVIGATOR_ELEMENT = "navigator"; //$NON-NLS-1$
    String RESFILE_ATTRIBUTE = "RESOURCE_FILE"; //$NON-NLS-1$
    String CONNECTION_ELEMENT = "connection"; //$NON-NLS-1$"
}
