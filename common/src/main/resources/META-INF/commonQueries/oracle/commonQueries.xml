<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright (c) 2018  Oracle Corporation
 
  The Universal Permissive License (UPL), Version 1.0
  
  Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
  software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
  all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
  hereunder covering either 
  (i) the unmodified Software as contributed to or provided by such licensor, or 
  (ii) the Larger Works (as defined below), to deal in both
 
  (a) the Software, and
  (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
  (each a “Larger Work” to which the Software is contributed by such licensors),
 
  without restriction, including without limitation the rights to copy, create derivative works of, display, 
  perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
  sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 
  This license is subject to the following condition:
  The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
  must be included in all copies or substantial portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
  TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
  DEALINGS IN THE SOFTWARE. 
 
-->

<commonQueries>
	<queries id="columns">
		<query minversion="9">
			<sql>
				<![CDATA[select c.column_name,  case when data_type = 'CHAR'     then      data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							when data_type = 'VARCHAR'  then      data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							when data_type = 'VARCHAR2' then      data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							when data_type = 'NCHAR'    then      data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							when data_type = 'NUMBER' then      
								    	case when c.data_precision is null and c.data_scale is null then          'NUMBER' 
								    	when c.data_precision is null and c.data_scale is not null then          'NUMBER(38,'||c.data_scale||')' 
									    else           data_type||'('||c.data_precision||','||c.data_SCALE||')'      end    
    							when data_type = 'NVARCHAR' then      data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							when data_type = 'NVARCHAR2' then     data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							else      data_type    end data_type,
      decode(nullable,'Y','Yes','No') nullable,  
    c.DATA_DEFAULT,column_id,   com.comments	     
      from sys.all_tab_Columns c, 
           sys.all_col_comments com
      where c.owner      = :OBJECT_OWNER  
      and  c.table_name =  :OBJECT_NAME   
      and c.table_name = com.table_name
      and c.owner = com.owner
      and c.column_name = com.column_name	              
      order by column_id ]]>
			</sql>
		</query>
		<query minversion="12.1.0.2" maxversion="12.2.0.0.0">
			<sql>
				<![CDATA[select c.column_name,  case when data_type = 'CHAR'     then      data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							when data_type = 'VARCHAR'  then      data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							when data_type = 'VARCHAR2' then      data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							when data_type = 'NCHAR'    then      data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							when data_type = 'NUMBER' then      
								    	case when c.data_precision is null and c.data_scale is null then          'NUMBER' 
								    	when c.data_precision is null and c.data_scale is not null then          'NUMBER(38,'||c.data_scale||')' 
									    else           data_type||'('||c.data_precision||','||c.data_SCALE||')'      end    
    							when data_type = 'NVARCHAR' then      data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							when data_type = 'NVARCHAR2' then     data_type||'('||c.char_length||decode(char_used,'B',' BYTE','C',' CHAR',null)||')'    
    							else      data_type    end data_type,
      decode(nullable,'Y','Yes','No') nullable,  
    c.DATA_DEFAULT,column_id,   com.comments	     
      from sys.all_tab_Columns c, 
           sys.all_col_comments com
      where c.owner      = :OBJECT_OWNER  
      and  c.table_name =  :OBJECT_NAME   
      and c.table_name = com.table_name
      and c.owner = com.owner
      and c.column_name = com.column_name
      and com.ORIGIN_CON_ID = sys_context('USERENV', 'CON_ID')	              
      order by column_id ]]>
			</sql>
		</query>
		<query>
			<sql>
				<![CDATA[select c.column_name,  
         decode(data_type,'CHAR'    ,data_type||'('||c.data_length||')',    
                          'VARCHAR' ,data_type||'('||c.data_length||')',    
    	                  'VARCHAR2',data_type||'('||c.data_length||')'    ,
                          'NCHAR'   ,data_type||'('||c.data_length||')'    ,
    			  'NUMBER'  ,decode(c.data_precision,null,decode(c.data_scale,null,'NUMBER','NUMBER(38,'||c.data_scale||')'), data_type||'('||c.data_precision||','||c.data_SCALE||')'),
    			  'NVARCHAR'  ,data_type||'('||c.data_length||')'    ,
                          'NVARCHAR2' ,data_type||'('||c.data_length||')'    ,
                          data_type) data_type,
            decode(nullable,'Y','Yes','No') nullable,  
            c.DATA_DEFAULT,
            column_id,   
            com.comments	     
      from sys.all_tab_Columns c, 
           sys.all_col_comments com
      where c.owner      = :OBJECT_OWNER  
      and   c.table_name =  :OBJECT_NAME   
      and   c.table_name = com.table_name
      and   c.owner = com.owner
      and   c.column_name = com.column_name]]>
			</sql>
		</query>
</queries>
</commonQueries>
