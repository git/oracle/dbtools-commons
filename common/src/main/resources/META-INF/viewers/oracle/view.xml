<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright (c) 2018  Oracle Corporation
 
  The Universal Permissive License (UPL), Version 1.0
  
  Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
  software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
  all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
  hereunder covering either 
  (i) the unmodified Software as contributed to or provided by such licensor, or 
  (ii) the Larger Works (as defined below), to deal in both
 
  (a) the Software, and
  (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
  (each a “Larger Work” to which the Software is contributed by such licensors),
 
  without restriction, including without limitation the rights to copy, create derivative works of, display, 
  perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
  sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 
  This license is subject to the following condition:
  The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
  must be included in all copies or substantial portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
  TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
  DEALINGS IN THE SOFTWARE. 
 
-->

<displays>
	<!-- COLUMNS -->
	<display type="editor" style="null" enable="true" class="Oracle#VIEW" objectType="VIEW">
		<name><![CDATA[Columns]]></name>
		<description><![CDATA[]]></description>
		<tooltip><![CDATA[]]></tooltip>
		<drillclass><![CDATA[null]]></drillclass>
		 <query>
            <sql>
                <![CDATA[select c.column_name,  
				UPPER (c.data_type) || case when (   c.data_type='VARCHAR'
			           OR c.data_type = 'VARCHAR2'
			           --rip this out rather than improving table to support (126 bytes) OR c.data_type ='RAW'
			           OR c.data_type='CHAR') AND (
							           c.data_length <> 0 AND
			           nvl(c.data_length,-1) <> -1)        then
			           case when(c.char_used ='C' and 'BYTE' =(select value from nls_session_parameters where PARAMETER='NLS_LENGTH_SEMANTICS')) then '(' || c.char_length || ' CHAR)'
							                when(c.char_used ='B' and 'CHAR' =(select value from nls_session_parameters where PARAMETER='NLS_LENGTH_SEMANTICS')) then '(' || c.data_length || ' BYTE)'
							                when(c.char_used ='C' and 'CHAR' =(select value from nls_session_parameters where PARAMETER='NLS_LENGTH_SEMANTICS')) then '(' || c.char_length || ')'
							                when(c.char_used ='B' and 'BYTE' =(select value from nls_session_parameters where PARAMETER='NLS_LENGTH_SEMANTICS')) then '(' || c.data_length || ')'
							                else '(' || c.data_length || ' BYTE)'
			           end  
                             when (c.data_type='RAW') then ''
			     when (c.data_type='NVARCHAR2' OR c.data_type='NCHAR') then 
			     '(' || c.data_length/2 || ')'  
			     when (c.data_type like 'TIMESTAMP%' OR c.data_type like 'INTERVAL DAY%' OR c.data_type like 'INTERVAL YEAR%' OR c.data_type = 'DATE' OR
							      (c.data_type = 'NUMBER' AND ((c.data_precision = 0) OR NVL (c.data_precision,-1) = -1) AND  nvl (c.data_scale,-1) = -1)) then
							      ''
			     when ((c.data_type = 'NUMBER' AND NVL (c.data_precision,-1) = -1) AND (c.data_scale = 0)) then
                                  '(38)' 
                                 when ((c.data_type = 'NUMBER' AND NVL (c.data_precision,-1) = -1) AND (nvl (c.data_scale,-1) != -1)) then
                                  '(38,'|| c.data_scale ||')'
			     when (c.data_scale  = 0 OR nvl(c.data_scale,-1) = -1) then
							      '('|| c.data_precision ||')'
							     else
							        '('|| c.data_precision ||',' ||c.data_scale ||')'   
			end data_type,
            decode(nullable,'Y','Yes','No') nullable,  
            c.DATA_DEFAULT,
            column_id,   
            com.comments,
			c_update.insertable,
            c_update.updatable,
            c_update.deletable  
      from sys.all_tab_Columns c, 
           sys.all_col_comments com,
           sys.all_updatable_columns c_update
      where c.owner      = :OBJECT_OWNER  
      and   c.table_name =  :OBJECT_NAME   
      and   c.table_name = com.table_name
      and   c.owner = com.owner
      and   c.column_name = com.column_name
      and c_update.column_name = com.column_name
      and c_update.table_name = com.table_name
      and c_update.owner = com.owner]]>
            </sql>
        </query>
	</display>
	<!-- DATA -->
	<display objectType="VIEW" editorClass="oracle.dbtools.raptor.oviewer.table.GridDataEditor">
		<name>Data</name>
	</display>
	<!-- GRANTS -->
	<display type="editor" style="null" enable="true" class="Oracle#VIEW" objectType="VIEW">
		<name><![CDATA[Grants]]></name>
		<description><![CDATA[]]></description>
		<tooltip><![CDATA[]]></tooltip>
		<drillclass><![CDATA[null]]></drillclass>
		<query requiredAccessObjects="dba_col_privs,dba_tab_privs">
			<sql>
				<![CDATA[Select PRIVILEGE, GRANTEE, GRANTABLE, GRANTOR, COLUMN_NAME object_name 
				from dba_col_privs where owner = :OBJECT_OWNER and TABLE_NAME =  :OBJECT_NAME 
				union all 
				Select PRIVILEGE, GRANTEE, GRANTABLE, GRANTOR, table_NAME object_name 
				from dba_tab_privs where owner = :OBJECT_OWNER and TABLE_NAME = :OBJECT_NAME]]>
			</sql>
		</query>
		<query>
			<sql>
				<![CDATA[Select PRIVILEGE, GRANTEE, GRANTABLE, GRANTOR, COLUMN_NAME object_name 
				from all_col_privs where TABLE_SCHEMA = :OBJECT_OWNER and TABLE_NAME =  :OBJECT_NAME 
				union all 
				Select PRIVILEGE, GRANTEE, GRANTABLE, GRANTOR, table_NAME object_name 
				from all_tab_privs where TABLE_SCHEMA = :OBJECT_OWNER and TABLE_NAME = :OBJECT_NAME]]>
			</sql>
		</query>
	</display>
	<!-- DEPENDENCIES -->
	<display type="editor" style="null" enable="true" class="Oracle#VIEW" objectType="VIEW">
		<name><![CDATA[Dependencies]]></name>
		<description><![CDATA[]]></description>
		<tooltip><![CDATA[]]></tooltip>
		<drillclass><![CDATA[null]]></drillclass>
		<query>
			<sql>
				<![CDATA[select owner, name, type, referenced_owner, 'SQLDEV:LINK:'||referenced_owner||':'||referenced_type||':'||referenced_name||':oracle.dbtools.raptor.controls.grid.DefaultDrillLink' as referenced_name 
				, referenced_type ,owner sdev_link_owner, name sdev_link_name, type sdev_link_type from ALL_DEPENDENCIES where referenced_owner = :OBJECT_OWNER and referenced_name = :OBJECT_NAME]]>
			</sql>
		</query>
		<display type="" style="" enable="true">
			<name><![CDATA[References]]></name>
			<description><![CDATA[]]></description>
			<tooltip><![CDATA[]]></tooltip>
			<drillclass><![CDATA[null]]></drillclass>
			<query>
				<sql>
					<![CDATA[select owner, name, type, referenced_owner, 'SQLDEV:LINK:'||referenced_owner||':'||referenced_type||':'||referenced_name||':oracle.dbtools.raptor.controls.grid.DefaultDrillLink' as referenced_name 
					, referenced_type ,referenced_owner sdev_link_owner, referenced_name sdev_link_name, referenced_type sdev_link_type from ALL_DEPENDENCIES where owner = :OBJECT_OWNER and name = :OBJECT_NAME]]>
				</sql>
			</query>
		</display>
	</display>
	<!-- DETAILS -->
	<display type="" style="Table" enable="true" class="Oracle#VIEW" objectType="VIEW">
		<name><![CDATA[Details]]></name>
		<description><![CDATA[]]></description>
		<tooltip><![CDATA[]]></tooltip>
		<drillclass><![CDATA[null]]></drillclass>
		<CustomValues>
			<TYPE>vertical</TYPE>
		</CustomValues>
		<query minversion="12.2">
			<sql>
				<![CDATA[select o.created,o.last_ddl_time,v.*,o.duplicated,o.sharded from
(select owner,object_name,created,last_ddl_time,duplicated,sharded from sys.all_objects where object_name = :OBJECT_NAME and owner = :OBJECT_OWNER) o,
(select * from all_views where owner = :OBJECT_OWNER and view_name = :OBJECT_NAME) v
where o.owner = v.owner and v.view_name = o.object_name]]>
			</sql>
		</query>
		<query>
			<sql>
				<![CDATA[select o.created,o.last_ddl_time,v.* from
(select owner,object_name,created,last_ddl_time from sys.all_objects where object_name = :OBJECT_NAME and owner = :OBJECT_OWNER) o,
(select * from all_views where owner = :OBJECT_OWNER and view_name = :OBJECT_NAME) v
where o.owner = v.owner and v.view_name = o.object_name]]>
			</sql>
		</query>
	</display>
	<!-- TRIGGERS -->
	<display type="editor" style="null" enable="true" class="Oracle#VIEW"	objectType="VIEW">
		<name><![CDATA[Triggers]]></name>
		<description><![CDATA[]]></description>
		<tooltip><![CDATA[]]></tooltip>
		<drillclass><![CDATA[null]]></drillclass>
		<query>
			<sql>
				<![CDATA[ Select dt.owner, dt.trigger_NAME, dt.trigger_type, dt.triggering_Event, dt.status, do.object_id ,  do.owner sdev_link_owner,
            dt.trigger_NAME sdev_link_name,'TRIGGER' sdev_link_type  
            from   all_triggers dt, sys.all_objects do 
            where do.owner = dt.owner 
			and   dt.table_owner = :OBJECT_OWNER 
			and   dt.trigger_name  = do.object_name  
			and   do.OBJECT_TYPE = 'TRIGGER'  
			and   dt.table_name    = :OBJECT_NAME]]>
			</sql>
		</query>
		<display type="" style="Code" enable="true">
			<name><![CDATA[]]></name>
			<description><![CDATA[]]></description>
			<tooltip><![CDATA[]]></tooltip>
			<drillclass><![CDATA[null]]></drillclass>
			<query>
				<sql>
					<![CDATA[ 
					SELECT 
					   CASE WHEN length(:TRIGGER_NAME)>0 THEN
					      DBMS_METADATA.GET_DDL('TRIGGER',:TRIGGER_NAME,:OWNER)
					   ELSE
					   	  TO_CLOB('N/A')  
					   END
					   SQL FROM SYS.DUAL]]>
				</sql>
			</query>
		</display>
	</display>
	<!-- SQL -->
	<display objectType="VIEW" editorClass="oracle.dbtools.raptor.oviewer.table.DDLViewer">
		<name>SQL</name>
	</display>
    <!-- ERRORS -->
	<display type="editor" style="null" enable="true" class="Oracle#VIEW" objectType="VIEW">
		<name><![CDATA[Errors]]></name>
		<description><![CDATA[]]></description>
		<tooltip><![CDATA[]]></tooltip>
		<drillclass><![CDATA[null]]></drillclass>
		<query>
			<sql>
				<![CDATA[Select ATTRIBUTE, LINE || ':' || POSITION "Line:Position", TEXT 
				from all_errors where type = 'VIEW' and OWNER = :OBJECT_OWNER and NAME =  :OBJECT_NAME 
				order by sequence asc]]> 
			</sql>
		</query>
	</display>
</displays>
