query_blocks_at_offset: [query_block) query_block 
--& [query_block <= :offset & :offset <= query_block)
; -- doesn't work for incomplete sql like this "select * from x where"

"query_table_expression_at_offset": [query_table_expression) query_table_expression &
[query_table_expression <= :offset & :offset <= query_table_expression)
->
; 

--"narrowest qb" : /\query_block(query_blocks_at_offset);
"narrowest qb" : [query_block) query_block;

-- SELECT *
--   FROM 
--       ORAPERF.S_PARTY T1, <-- full table ref
--       S_EVT_ACT T18,      <-- full table ref
--       T20,                 <-- "simple table ref"
--       ORAPERF.S_PARTY,     <-- "simple table ref"
"simple table ref": 
"narrowest qb".query_block <= alias &
[alias) table_reference &
([alias) identifier | [alias) query_table_expression) &
table = alias
;
"full table ref": 
"narrowest qb".query_block <= alias &
[alias-1) query_table_expression &
[alias) identifier &
table = alias-1
;


"table_alias in narrowest qb" : "full table ref" | "simple table ref"
->
; 


