CREATE OR REPLACE    procedure store_values( p_owner in varchar2, 
                           p_tname in varchar2, 
                           p_rowid in rowid )
   is
      l_theQuery    varchar2(4096);
      l_cursor      integer;
      l_variable    varchar2(2000);
      l_status      number;
      l_col_cnt     number default 0;
      p_schema      varchar2(1111) := upper(p_owner);   -- line#10
      p_obj         varchar2(1111) := upper(p_tname);
   begin
      -- Verify that there is no SQL injection
      validate_object_name (p_schema, p_obj);

      l_theQuery := 'select rowid, a.* from ' || DBMS_ASSERT.ENQUOTE_NAME(p_schema) || '.' || p_obj ||  -- line#16
                  ' a where rowid = :x1';

      l_cursor := dbms_sql.open_cursor;

      sys.dbms_sql.parse( l_cursor, l_theQuery, dbms_sql.v7 ); --line#21
      dbms_sql.bind_variable( l_cursor, ':x1', p_rowid );
      for i in 1 .. 255 loop
         begin
            dbms_sql.define_column( l_cursor, i, l_variable, 2000 );
            l_col_cnt := l_col_cnt + 1;
         exception
            when last_column then exit;
         end;
      end loop;

      l_status := dbms_sql.execute(l_cursor);
      l_status := dbms_sql.fetch_rows(l_cursor);

      htp.formHidden( 'old_' || p_tname, htf.escape_sc(p_owner) );
      htp.formHidden( 'old_' || p_tname, htf.escape_sc(p_tname) );
      for i in 1 .. l_col_cnt loop
         dbms_sql.column_value( l_cursor, i, l_variable );
         htp.formHidden( 'old_'||p_tname, htf.escape_sc(l_variable) );
      end loop;

      dbms_sql.close_cursor( l_cursor );
   end;