/*
 * Recognize various nodes on parse tree
 * Simple conditions, like
   "sql_statement": [sql_statement) sql_statement;
   are handled automatically without need to list them explicitly
   (SqlRecognizer sees no sql_statement in this file, so it adds one)
 */

"procedureCall": [procedureCall) function | [procedureCall) procedure_call;

"arg":  "procedureCall".procedureCall < arg 
       & ([arg) string_literal | [arg) numeric_literal )
       & ([arg) expr | [arg) pls_expr )  -- 23246755: 
                                         -- SELECT SYS.XMLTYPE(xmlserialize(DOCUMENT XMLType('<poid>143598</poid>') AS
                                         -- CLOB indent size = 2)) "WORKFLOW_DATA"
                                         --                   ^^^ don't want this 
;

pragmas: [pragma) pragma;