/*
 * Recognize various nodes on parse tree
 * Simple conditions, like
   "sql_statement": [sql_statement) sql_statement;
   are handled automatically without need to list them explicitly
   (SqlRecognizer sees no sql_statement in this file, so it adds one)
 */

"procedureCall": [procedureCall) function | [procedureCall) procedure_call;

"queryBlock": [queryBlock) subquery;

"main QB" : \/queryBlock("queryBlock");  -- auxiliary predicate (which is not listed among recognized symbols)
"nested QB" : "queryBlock" - "main QB";  -- ditto

-- When extracting columns the main challenge is not to look inside nested subqueries, for example 
--
--SELECT
--  DEPARTMENT_ID,
-- (select ignore from T) as scalarSubquery
-- FROM EMP_DETAILS_VIEW
--
-- "columnSelect" lists [DEPARTMENT_ID, COUNT(*), (select ignore from T1 where 1=1)] but not "ignore"
-- Hence query in three steps
-- 1. "all columns" 
-- 2. "columns in nested QB"
-- 3. subtract one from the other (projected to the column of interest, first)
"all columns": --[columnSelect) expr & ([columnSelect) select_term | [columnSelect^) select_term)
       [columnSelect) select_term
     & [select_clause) select_clause
     & select_clause < columnSelect
     & "main QB".queryBlock < select_clause
;

"columns in nested QB": --[columnSelect) expr & ([columnSelect) select_term | [columnSelect^) select_term)
       [columnSelect) select_term
     & [select_clause) select_clause
     & select_clause < columnSelect
     & "nested QB".queryBlock < select_clause
;

"columnSelect": ([columnSelect]|"all columns") - ([columnSelect]|"columns in nested QB")
;

"all tables": [tableFrom) table_reference
     & [from_clause) from_clause
     & from_clause < tableFrom
     & "main QB".queryBlock < from_clause
;

"tables in nested QB": [tableFrom) table_reference
     & [from_clause) from_clause
     & from_clause < tableFrom
     & "nested QB".queryBlock < from_clause
;

"tableFrom":  ([tableFrom]|"all tables") - ([tableFrom]|"tables in nested QB")
;

"all predicates": predicateWhere^ = where_clause
      & [where_clause) where_clause
      & [predicateWhere) condition
      & "main QB".queryBlock < where_clause
;

"predicates in nested QB": predicateWhere^ = where_clause
      & [where_clause) where_clause
      & [predicateWhere) condition
      & "nested QB".queryBlock < where_clause
;


"predicateWhere": ([predicateWhere]|"all predicates") - ([predicateWhere]|"predicates in nested QB")
;

"expr in orderBy": [order_by_clause) order_by_clause 
	  & order_by_clause < orderBy
	  &  [orderBy) expr
	  & [order_by_clause^) subquery
;
"allOrderBys": \/orderBy("expr in orderBy")  -- e.g. order by 1+2 --<-- don't want returning nested 1 and 2 
;
"nestedOrderBys": "nested QB".queryBlock < "allOrderBys".orderBy 
; 
"orderBy": "allOrderBys" - "nestedOrderBys";

"expr in groupBy": [group_by_clause) group_by_clause 
	  & group_by_clause < groupBy
	  &  [groupBy) expr
;
"allGroupBys": \/groupBy("expr in groupBy")  
;
"nestedGroupBys": "nested QB".queryBlock < "allGroupBys".groupBy 
; 
"groupBy": "allGroupBys" - "nestedGroupBys";

"arg":  "procedureCall".procedureCall < arg 
       & ([arg) string_literal | [arg) numeric_literal )
       & ([arg) expr | [arg) pls_expr )  -- 23246755: 
                                         -- SELECT SYS.XMLTYPE(xmlserialize(DOCUMENT XMLType('<poid>143598</poid>') AS
                                         -- CLOB indent size = 2)) "WORKFLOW_DATA"
                                         --                   ^^^ don't want this 
;

"user": [user) user;
"role": [role) revoke_system_privileges[3,18);

"assignedBind": [assignedBind) bind_var & [assignedBind^) assignment_stmt & [assignedBind = [(assignedBind^)
              | [assignedBind) bind_var & [assignedBind^) into_list & [assignedBind-1) 'INTO'
;

