/* Example input:
CREATE OR REPLACE PROCEDURE PROCEDURE2
(
  PARAM1 IN VARCHAR2
, PARAM2 IN VARCHAR2
, PARAM3 IN VARCHAR2
) AS
 VAR1 integer(100) := 'FRED';
 --VAR2 integer(100) := 'FRED';
BEGIN
  VAR1 := 'BILL';
  declare
    VAR1 varchar2(100) := 'FRED';
  begin
    VAR1 := 'BILL';
  end;
END PROCEDURE2; 
*/

"subprg_body in pkg_body": 
 [scope) subprg_body &
 [pkg) pkg_body &
 pkg <= scope
;

"subprg_body in subprg_body": 
 [scope) subprg_body &
 [container) subprg_body &
 container < scope 
;


"subprg_body with forward declaration":
 [scope) subprg_body &
 [forward) basic_d &
 [forward) subprg_spec &
 [decl_id1) decl_id &
 decl_id1^ = forward &
 [decl_id2) decl_id &
 decl_id2^^ = scope &
 ?decl_id1 = ?decl_id2
;
 
"excluded proc scopes": "subprg_body with forward declaration" | ("subprg_body in pkg_body" - "subprg_body in subprg_body");
 

"proc args" : 
[decl) prm_spec &
[id) decl_id &
decl <= id &
scope <= decl &
[scope) subprg_body 
;

/* Example output:
"proc args"=[id         scope         decl]
             [6,7) PARAM1    [3,55) "PP(PIV,P"  [6,9) "PIV"
             [10,11) PARAM2    [3,55) "PP(PIV,P"  [10,13) "PIV"
             [14,15) PARAM3    [3,55) "PP(PIV,P"  [14,17) "PIV"
*/

"proc scope" : 
[decl) basic_d  &
![decl) subprg_spec  &
[id) decl_id &
decl <= id &
scope <= decl &
[scope) subprg_body &
[is_or_as) is_or_as &
is_or_as+1 <= decl &
is_or_as^ = subprg_body
;

/*
"proc scope"=[id         is_or_as         scope         subprg_body         decl]
              [19,20) VAR1      [18,19) AS        [3,55) "PP(PIV,P"  [3,55) "PP(PIV,P"  [19,28) "Vi(1):='"
*/

"block scope" : 
[decl) basic_decl_item &
[id) decl_id &
decl <= id &
scope <= decl &
[scope) block_stmt
;

"loop scope" : 
[decl) iteration_scheme &
[id) identifier &
[id+1) 'IN' &
decl <= id &
scope <= decl &
[scope) loop_stmt
;

"united scopes" : "proc scope" | "block scope" | "loop scope" | ("proc args"-"excluded proc scopes") ;  
/*
"united scopes"=[id         scope         decl]
    [19,20) VAR1      [3,55) "PP(PIV,P"  [19,28) "Vi(1):='"
    [35,36) VAR1      [34,52) "dVv(1):="  [35,44) "Vv(1):='"
    [6,7) PARAM1    [3,55) "PP(PIV,P"  [6,9) "PIV"
    [10,11) PARAM2    [3,55) "PP(PIV,P"  [10,13) "PIV"
    [14,15) PARAM3    [3,55) "PP(PIV,P"  [14,17) "PIV"
*/


"vars" :
[entry) identifier &
?entry = ?"united scopes".id &
"united scopes".scope <= entry 
--!entry = sc.id
;

"[entry scope]" : ?scope='false' & ?entry='false';

"var entry in scope" : /\scope("vars" | "[entry scope]");

/*
"var entry in scope"=[scope         entry]
                      [3,55) "PP(PIV,P"  [6,7) PARAM1  
                      [3,55) "PP(PIV,P"  [10,11) PARAM2  
                      [3,55) "PP(PIV,P"  [14,15) PARAM3  
                      [3,55) "PP(PIV,P"  [19,20) VAR1    
                      [3,55) "PP(PIV,P"  [29,30) VAR1    
                      [34,52) "dVv(1):="  [35,36) VAR1    
                      [34,52) "dVv(1):="  [45,46) VAR1    
*/