"primitive nodes":
   [tuple) node
;

"content nodes":
   [tuple) node
&  [value) contents
&  value^ = tuple
;

tupleNodes: "primitive nodes" - "content nodes"
;

values:
   tupleNodes.tuple < value
&  [value) contents 
&  [value^) node 
&  [value-1) opentag
&  [value+1) closetag
&  [attribute) identifier
&  attribute^ = value-1
->
;
   
  