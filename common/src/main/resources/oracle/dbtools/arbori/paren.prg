-- if node x is expr
-- and first child is '('
-- and last child is ')'
-- and parent x^ is not expr
-- then remove text at lp and rp
--     x
--   / | \
--  lp q rp

"paren": 
[x) expr
& 
lp^ = x
&  
lp+1 = q 
&
q+1 = rp
&
[lp) '('
&
[q) expr
&
[rp) ')'
&
! [x^) expr 
;