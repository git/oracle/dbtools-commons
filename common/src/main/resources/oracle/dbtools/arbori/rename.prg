-- rename local variable (definition and all usages)

( /*[entry) :offset  |*/ [id <= :offset & :offset < id) ) &
([decl) basic_decl_item | [decl) prm_spec) &
[id) decl_id &
decl <= id &
scope <= decl &
scope <= entry &
[scope) subprg_body & 
[entry) identifier &
?entry = ?id
->
id = :newName
&
entry = :newName
.