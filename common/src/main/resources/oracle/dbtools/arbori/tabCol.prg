"column_expr" :
[column) select_term 
;

"select_list with aggregate" : [select_list) select_list 
   &  [select_list^) select_clause
   & ([aggr) aggregate_function | [aggr) analytic_function)
   & select_list <= aggr
;

sla : "select_list with aggregate";

"matching gby": 
   [group_by_list) group_by_list
   & [group_by_list^) group_by_clause
   & sla.select_list^^ = group_by_list^^
;

"matching from": -- insertion point in case of missing gby
   [from_clause) from_clause
   & sla.select_list^^ = from_clause^
;


gby : "matching gby";

"select columns":
   [column) column
   & sla.select_list <= column
   & ([column) select_term | ([column^) select_term & ! [column^) aggregate_function) ) 
;

"gby columns":
   [column) column
   & gby.group_by_list <= column
;
