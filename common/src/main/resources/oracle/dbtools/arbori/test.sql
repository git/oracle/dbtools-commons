BEGIN

    z_error(p1          => w_error_message,

            p2          => w_err_type,

            p6          => nvl(p_message_id,

            error.get_short_err_id(p_type      => 'V',

                                  p_pkg_name  => 'pkg',

                                  p_no        => 1

            )

        )

    );

END;