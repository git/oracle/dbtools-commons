"tables": [tid) identifier 
& [tid-1) '<'
& ?tid = 'TABLE'
& tscope = tid^^ 
; 

t : "tables";

"rows":  ?tr = 'TR' & [tr-1) '<' 
& t.tscope < tr 
;

r: "rows";

"cells": ?td = 'TD' & [td-1) '<' 
& r.tr^^ < td 
& [cell) content & td^^ < cell & ![cell-1) '=' & ![cell^) content
;

/* optimized:

"cells": ?td = 'TD' & [td-1) '<' 
& r.tr^^ < td 
;

c: "cells";

"data":  [cell) content & c.td^^ < cell & ![cell-1) '=' & ![cell^) content
;

*/