#   Copyright (c) 2018  Oracle Corporation
#
#   The Universal Permissive License (UPL), Version 1.0
#
#   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
#   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
#   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
#   hereunder covering either 
#   (i) the unmodified Software as contributed to or provided by such licensor, or 
#   (ii) the Larger Works (as defined below), to deal in both
#
#   (a) the Software, and
#   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
#   (each a ?Larger Work? to which the Software is contributed by such licensors),
#
#   without restriction, including without limitation the rights to copy, create derivative works of, display, 
#   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
#   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
#
#   This license is subject to the following condition:
#   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
#   must be included in all copies or substantial portions of the Software.
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
#   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
#   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
#   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
#   DEALINGS IN THE SOFTWARE. 
INFORMATION= INFORMATION\n\
 --------\n\
\n\
  This command is like describe but with more details about the objects requested.\n\
\n\
 INFO[RMATION] {[schema.]object[@connect_identifier]}\n\
 INFO+ will show column statistics\n\
\n\

ALIAS= ALIAS\n\
------\n\
\n\
alias [<name>=<SQL statement>;| LOAD [<filename>]|SAVE [<filename>] | LIST [<NAME>] | \n\
\                               DROP <name> | DESC <name> <Description String>]\n\
\n\
Alias is a command which allows you to save a sql, plsql or sqlplus script and assign it a shortcut command.\n\
\t\"alias" - print a list of aliases\n\
\t\"alias list <aliasName>" - list the contents of the alias\n\
\t\"alias <aliasName>=select :one from dual;" - Simple alias command\n\
\t\Define an alias simply by using the alias keyword followed by a single identifier\n\
\tname followed by an '='. Anything after the '=' will be used as the alias contents.\n\
\t\For example, if it is SQL, it will be terminated by a ';'. If it is PLSQL, it will\n\
\t\be terminated by a '/'\n\
\n\
Examples:\n\
--------\n\
1. SQL Example with Binds\n\
\n\
\t\SQL> alias fred=select :one from dual;\n\
\t\In this example, a bind variable can be set up in the alias.\n\
\t\Running the alias is done like this below with any parameters\n\
\tto the alias being bound to a bind variable by SQLcl\n\
\n\
\t\SQL> fred 1\n\
\t\Command=fred\n\
\t\:ONE\n\
\t\----\n\
\t\>\n\
\n\
2. PL/SQL example\n\
\t\SQL> alias db= begin dbms_output.put_line('hi'); end;\n\
\t\>  /\n\
\t\Here the block of PLSQL is terminated by the '/' on a separate \n\
\t\line at which point it is accepted as a new alias.\n\
\n\
\t\SQL> db\n\
\t\Command=db\n\
\t\PL/SQL procedure successfully completed.\n\
\t\hi\n\
\n\
Summary\n\
-------\n\
\t\alias ..=.. is terminated by ';'\n\
\t\alias ..=begin or ..=declare is terminated by a / on a newline\n\
\t\alias on its own gives a list of existing aliases.\n\

FORMAT= FORMAT\n\
 ---------\n\
\n\
FORMAT BUFFER - formats the script in the SQLcl Buffer\n\
FORMAT RULES <filename> - Loads SQLDeveloper Formatter rules file to formatter.\n\
FORMAT FILE <input_file> <output_file> \n\
\n\
Format used is default or for SQLcl can be chosen by setting an environmental variable\n\
pointing to a SQLDeveloper export (.xml) of formatter options.\n\
The variable is called SQLFORMATPATH\n\
In SQLDeveloper the format options are the default chosen in the preferences.\n\

BRIDGE= BRIDGE\n\
 ----\n\
\n\
Used mainly to script data move between two connections/schemas \n\
It also includes functionality to dynamically create Oracle tables which "fit" the data being received through JDBC\n\
The following functionality is available\n\
A) query tables in other connections \n\
B) query tables in multiple connections in the same statement\n\
C) insert data from one connection into another\n\
D) create a table and insert data into it from another connection\n\
\n\
Syntax:\n\
BRIDGE <targetTableName> as "<jdbcURL>"(<sqlQuery>);\n\
\n\
Example:\n\
BRIDGE table1 as "jdbc:oracle:thin:scott/tiger@localhost:1521:orcl"(select * from dept);\n\
In the above example table1 is created in the current connection. \n\
Table1 is defined using the metadata from the query run against the database connection defined using a SID.\n\
\n\
Example:\n\
BRIDGE table1 as "jdbc:oracle:thin:scott/tiger@localhost:1521/orcl"(select * from dept);\n\
In the above example table1 is created in the current connection. \n\
Table1 is defined using the metadata from the query run against the database connection defined using a Service Name.\n\
\n\
The JDBC URL specified has to conform to the format defined by the driver.\n\
\n\
BRIDGE new_table   as "jdbc:oracle:thin:[USER/PASSWORD]@[HOST][:PORT]:SID"(select * from scott.emp);\n\
BRIDGE new_table   as "jdbc:oracle:thin:[USER/PASSWORD]@[HOST][:PORT]/SERVICE"(select * from scott.dept);\n\
\n\

APEX= APEX\n\
 ---------\n\
 \n\
 apex - Lists Application Express Applications\n\
 apex export <app id> - Exports the application which could be combined with spool for writing to a file\n\
 \n\
 
HISTORY= HISTORY\n\
 ---------\n\
history [<index> | FULL | USAGE | SCRIPT | TIME | CLEAR (SESSION)?] \n\
\n\
SQL>history full\n\
\  1  select 1 from dual;\n\
\  2  select 2\n\
\  >  from dual;\n\
\  3  select 3 from dual\n\
\  >  where 1=1;\n\
\n\
SQL>history usage\n\
\  1  (2) select 1 from dual; \n\
\  2  (11) select 2 from dual; \n\
\  3  (2) select 3 from dual where 1=1;\n\
\n\
SQL>history script\n\
\ select 1 from dual; \n\
\ select 2 from dual; \n\
\ select 3 from dual where 1=1; \n\
\n\
SQL>history 3\n\
\  1  select 3 from dual\n\
\  2* where 1=1;\n\
\n\
SQL>his time\n\
\  1           clear\n\
\  2           cl bre\n\
\  3  (00.201) select 1 from dual\n\
\n\

EDIT = EDIT\n\
\ ---------\n\
\n\ Invokes an operation system text editor on the contents of the \n\
 specified file or on the contents of the SQL buffer. \n\
\n\ ED[IT] [file_name[.ext]]\n\
\n\
The DEFINE variable _EDITOR can be used to set the editor to use\n\
\n\
In SQLcl, _EDITOR can be set to "inline". This will set the editor to\n\
be the SQLcl editor.  This supports the following shortcuts\n\
\	^R - Run the current buffer\n\
\	^W - Go to top of buffer\n\
\	^S - Go to bottom of buffer\n\
\	^A - Go to start of line\n\
\	^E - Go to end of line\n\
\n\

CD = CD\n\
  ---\n\
  \n\ Changes path to look for script at after startup. \n\
(show SQLPATH shows the full search path currently: \n\
  - CD current directory setting set by last cd command \n\
  - baseURL (url for subscripts) \n\
  - topURL (top most url when starting script) \n\
  - Last Node opened (i.e. file in worksheet) \n\
  - Where last script started \n\
  - Last opened on sqlcl path related file chooser \n\
  - SQLPATH setting \n\
  - "." if in SQLDeveloper UI (included in SQLPATH in command line (sdsql))\n\
). \n\
\n\

CTAS=CTAS\n \
 ctas table new_table \n\
   Uses DBMS_METADATA to extract the DDL for the existing table\n \
   Then modifies that into a create table as select * from \n \
\n\

STORE=STORE\n\
-----\n\
\n\
 Saves attributes of the current SQLcl environment in a script.\n\
\n\
 STORE {SET} file_name[.ext] [CRE[ATE] | REP[LACE] | APP[END]]\n\
\n\

NET=NET\n\
------\n\
\n\
NET is a command which allows you to save a network details and assign it a shortcut command.\n\
Available in SQLcl only - \n\
\n\
net [<name>=<Connection String>;| LOAD [<filename>]|SAVE [<filename>] | LIST [NAME] | DROP  ]\n\
\n\
\  "net" - print a list of net short cuts\n\
\  "net list <aliasName>" - list the contents of the short cut\n\
\  "net name=localhost:1521/XE;" simple net command\n\
\  "net drop name" - delete the short cut called name\n\
\n\
net is single line terminated by newline\n\
\  net ..=.. is terminated by ';'\n\
net on its own gives a list of existing short cuts.\n\
\n\
controlled by\n\
\  set net on|off|readonly - default ON \n\
\     readonly means only do try to enter a net shortcut on successful connect command\n\
\  set noverwrite on|off|warn - default WARN\n\
\     net overwrite: warn prints a warning if an override would otherwise happen.\n\
\n\

LOAD=LOAD\n\
-----\n\
\n\
 Loads a comma separated value (csv) file into a table.\n\
 The first row of the file must be a header row.  The columns in the header row must match the columns defined on the table.\n\
 \n\
 The columns must be delimited by a comma and may optionally be enclosed in double quotes.\n\
 Lines can be terminated with standard line terminators for windows, unix or mac.\n\
 File must be encoded UTF8.\n\
 \n\
 The load is processed with 50 rows per batch.\n\
 If AUTOCOMMIT is set in SQLCL, a commit is done every 10 batches.\n\
 The load is terminated if more than 50 errors are found.\n\
 \n\
LOAD [schema.]table_name[@db_link] file_name\n\
\n\

TNSPING=TNSPING\n\
-------\n\
\n\
\ The TNSPING utility determines whether the listener for a service on an\n\
\ Oracle Net network can be reached successfully.\n\
\ If you can connect successfully from a client to a server (or a server\n\
\ to another server) using\n\
\ the TNSPING utility, then it displays an estimate of the round trip time\n\
\ (in milliseconds) it takes to reach the Oracle Net service.\n\
\n\
\ If it fails, then it displays a message describing the error that occurred.\n\
\ This enables you to see the network error that is occurring without the\n\
\ overhead of a database connection.\n\
\n\
\Use the following command to test connectivity:\n\
\n\
\  tnsping <address> \n\
\n\
\ Where the address is a TNS entry, or a JDBC connection String. For example:\n\
\n\
\  TNSPING DB@ACME.COM\n\
\ or\n\
\  TNSPING localhost:1521/orcl\n\
\n\

SSHTUNNEL=SSHTUNNEL\n\
---------\n\
\n\
Creates a tunnel using standard ssh options \n\
such as port forwarding like option -L of the given port on the local host \n\
will be forwarded to the given remote host and port on the remote side. It also supports\n\
identity files, using the ssh -i option\n\
If passwords are required, they will be prompted for.\n\
\n\
  SSHTUNNEL <username>@<hostname>:port -i <identity_file> [-L localPort:Remotehost:RemotePort]\n\
\n\
Options\n\
\n\
 -L localPort:Remotehost:Remoteport\n\
\n\
   Specifies that the given port (localhost) on the local (client) host is to be forwarded to \n\
   the given remote host (Remotehost) and port (Remoteport) on the remote side.  This works by \n\
   allocating a socket to listen to port on the local side.\n\
   Whenever a connection is made to this port, the connection is forwarded over \n\
   the secure channel, and a connection is made to remote host & remoteport from \n\
   the remote machine.\n\
\n\
 -i identity_file\n\
   Selects a file from which the identity (private key) for public key authentication is read. \n\
   \n\
   
DDL=DDL\n\
---\n\
\n\
DDL generates the code to reconstruct the object listed.  Use the type option\n\
for materialized views. Use the save options to save the DDL to a file.\n\
\n\
DDL [<object_name> [<type>] [SAVE <filename>]] \n\
\n\

OERR=OERR\n\
----\n\
Usage: oerr facility error\n\
\n\
Facility is identified by the prefix string in the error message.\n\
For example, if you get ORA-7300, "ora" is the facility and "7300"\n\
is the error.  So you should type "oerr ora 7300".\n\
\n\
If you get tns-12533, type "oerr tns 12533", and so on.\n\
(ora and tns facilities only)\n\
\n\

REST= REST\n\
 ------\n\
\n\
 REST allows to export ORDS 3.X services.\n\
\n\
\t     REST export                  -\t All modules\n\
\t     REST export <module_name>    -\t Export a specific module\n\
\t     REST export <module_prefix>  -\t Export a specific module related to the given prefix\n\
\t     REST modules                 -\t List the available modules\n\
\t     REST privileges              -\t List the existing privileges\n\
\t     REST schemas                 -\t List the available schemas\n\
\n\




SODA= SODA\n\
 ------\n\
\n\
 SODA allows schemaless application development using the JSON data model.\n\
\t SODA create <collection_name> \n\
\t      Create a new collection \n\n\
\t SODA list \n\
\t      List all the collections \n\n\
\t SODA get <collection_name> [-all | -f | -k | -klist] [{<key> | <k1> <k2> ... > | <qbe>}]\n\
\t      List documents the collection \n\
\t      Optional arguments: \n\
\t\t    -all    list the keys of all docs in the collection \n\
\t\t    -k      list docs matching the specific <key> \n\
\t\t    -klist  list docs matching the list of keys \n\
\t\t    -f      list docs matching the <qbe> \n\n\
\t SODA insert <collection_name> <json_str | filename> \n\
\t      Insert a new document within a collection \n\n\
\t SODA drop <collection_name> \n\
\t      Delete existing collection \n\n\
\t SODA count <collection_name> [<qbe>] \n\
\t      Count # of docs inside collection. \n\
\t      Optional <qbe> returns # of matching docs\n\n\
\t SODA replace <collection_name> <oldkey> <new_{str|doc}> \n\
\t      Replace one doc for another \n\n\
\t SODA remove <collection_name> [-k | -klist |-f] {<key> | <k1> <k2> ... | <qbe>} \n\
\t      Remove doc(s) from collection \n\
\t      Optional arguments: \n\
\t\t    -k     remove doc in collection matching the specific <key> \n\
\t\t    -klist remove doc in collection matching the list <key1> <key2> ... > \n\
\t\t    -f     remove doc in collection matching <qbe> \n\n\  

SQLCL_SHORTCUTS=\n\SQLcl shortcuts:\n\
\t^r run\n\
\t^a start of line\n\
\t^e end of line\n\
\t^w goto top\n\
\t^s goto bottom\n\

FIND=FIND\n\
----\n\
\n\
\ find <filename>\n\
\t\ find will list all the places it finds <filename> on the SQLPATH\n\
\t\ and find all instances within that path structure\n\

WHICH=WHICH\n\
----\n\
\n\
\ which <filename>\n\
\t\ which will list the first <filename> it finds on the SQLPATH\n\
\t\ This will be the file that should used by commands\n\
\t\ and searchs the top level paths on the SQLPATH only.\n\
