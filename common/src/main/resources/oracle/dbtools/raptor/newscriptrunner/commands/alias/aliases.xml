<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright (c) 2018  Oracle Corporation
 
  The Universal Permissive License (UPL), Version 1.0
  
  Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
  software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
  all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
  hereunder covering either 
  (i) the unmodified Software as contributed to or provided by such licensor, or 
  (ii) the Larger Works (as defined below), to deal in both
 
  (a) the Software, and
  (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
  (each a “Larger Work” to which the Software is contributed by such licensors),
 
  without restriction, including without limitation the rights to copy, create derivative works of, display, 
  perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
  sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 
  This license is subject to the following condition:
  The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
  must be included in all copies or substantial portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
  TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
  DEALINGS IN THE SOFTWARE. 
 
-->

<aliases xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:noNamespaceSchemaLocation="aliases.xsd">
	<alias name="tables">
		<description><![CDATA[tables <schema> - show tables from schema]]></description>
		<queries> 
			<query>
				<sql><![CDATA[ select table_name "TABLES" from user_tables]]>
                    </sql>
                 </query>
             </queries>		
		</alias>
		
			<alias name="tables2">
		<description><![CDATA[tables <schema> - show tables from schema]]></description>
		<queries> 
			<query>
				<sql><![CDATA[ col table_name heading "Table" format A28
col num_rows heading "Rows" format 99,999,999,999
col blocks heading "Blocks" format 9,999,999
col index_count heading "Indexes" format 999999
col constraint_count heading "Constraints" format 999999
col part_count heading "Partitions" format 99999999
col last_analyzed heading "Analyzed" format A8
col tablesize heading "Size" format A8
col unformatted_size heading "Size(GB)" format 9,990.999
col compression heading "Compression" format A12
break on report
compute sum label "Total Size" of unformatted_size on report
set linesize 130
prompt
prompt Tables
prompt ======
SELECT table_name,
  num_rows        ,
  blocks          ,
  nvl(ts.unformatted_size/1073741824,0)  unformatted_size,
  CASE
    WHEN t.compression = 'DISABLED'
    THEN 'Disabled'
    WHEN t.compression ='ENABLED'
    THEN 'Compressed'
  END compression,
  (SELECT COUNT(1) FROM user_indexes i WHERE t.table_name = i.table_name
  ) index_count,
  (SELECT COUNT(1) FROM user_constraints uc
   WHERE t.table_name = uc.table_name
   AND uc.constraint_type in ('P','R','U')
  ) constraint_count,
  (SELECT COUNT(1) FROM user_tab_partitions p WHERE t.table_name = p.table_name
  ) part_count,
  CASE
    WHEN (sysdate - last_analyzed) BETWEEN 0 AND 0.1
    THEN '< Hour'
    WHEN (sysdate - last_analyzed) BETWEEN 0.1 AND 1.0
    THEN '< Day'
    WHEN (sysdate - last_analyzed) BETWEEN 1.0 AND 7.0
    THEN '< Week'
    WHEN (sysdate - last_analyzed) BETWEEN 7.0 AND 28.0
    THEN '< Month'
    ELSE '> Month'
  END last_analyzed
   FROM user_tables t ,
  (SELECT segment_name,
    SUM(s.bytes) unformatted_size
     FROM user_segments s
    WHERE s.segment_type IN ('TABLE', 'TABLE PARTITION', 'TABLE SUBPARTITION')
 GROUP BY s.segment_name
  ) ts
  WHERE t.table_name = ts.segment_name (+)
  order by ts.unformatted_size desc]]>
                    </sql>
                 </query>
             </queries>		
		</alias>
		
		<alias name="locks">
		<description><![CDATA[locks <USERNAME>]]></description>
		<queries> 
			<query>
				<sql><![CDATA[ SELECT p.username  username                                      ,
                                                       p.pid pid                                            ,
                                                       s.sid    sid                                        ,
                                                       s.serial# serial ,
                                                       p.spid           spid                                 ,
                                                       s.username ora                                    ,
                                                       DECODE(l2.type,
                                                              'TX','TRANSACTION ROW-LEVEL'     ,
                                                              'RT','REDO-LOG'                  ,
                                                              'TS','TEMPORARY SEGMENT '        ,
                                                              'TD','TABLE LOCK'                ,
                                                              'TM','ROW LOCK'                  ,
                                                                   l2.type                     )   vlock,
                                                       DECODE(l2.type,
                                                              'TX','DML LOCK'                  ,
                                                              'RT','REDO LOG'                  ,
                                                              'TS','TEMPORARY SEGMENT'         ,
                                                              'TD',DECODE(l2.lmode+l2.request  ,
                                                                          4,'PARSE '          ||
                                                                            u.name            ||
                                                                            '.'               ||
                                                                            o.name             ,
                                                                          6,'DDL'              ,
                                                                            l2.lmode+l2.request),
                                                              'TM','DML '                     ||
                                                                   u.name                     ||
                                                                   '.'                        ||
                                                                   o.name                      ,
                                                                   l2.type                     )   type  ,
                                                       DECODE(l2.lmode+l2.request              ,
                                                              2   ,'RS'                        ,
                                                              3   ,'RX'                        ,
                                                              4   ,'S'                         ,
                                                              5   ,'SRX'                       ,
                                                              6   ,'X'                         ,
                                                                   l2.lmode+l2.request         )   lmode ,
                                                       DECODE(l2.request                       ,
                                                              0,NULL                           ,
                                                                'WAIT'                         )   wait
                                                FROM   gv$process                p ,
                                                       gv$_lock                  l1,
                                                       gv$lock                   l2,
                                                       gv$resource               r ,
                                                       sys.obj$                  o ,
                                                       sys.user$                 u ,
                                                       gv$session                s
                                                WHERE  s.paddr    = p.addr
                                                  AND  s.saddr     = l1.saddr
                                                  AND  l1.raddr   = r.addr
                                                  AND  l2.addr    = l1.laddr
                                                  AND  l2.type    <> 'MR'
                                                  AND  r.id1      = o.obj# (+)
                                                  AND  o.owner#   = u.user# (+)
                                                  --AND  u.name = 'GME'
                                                  AND  (:USER_NAME is null or s.username LIKE upper(:USER_NAME))
                                                ORDER BY p.username, p.pid, p.spid, ora, DECODE(l2.type,
                                                              'TX','TRANSACTION ROW-LEVEL'     ,
                                                              'RT','REDO-LOG'                  ,
                                                              'TS','TEMPORARY SEGMENT '        ,
                                                              'TD','TABLE LOCK'                ,
                                                              'TM','ROW LOCK'                  ,
                                                                   l2.type                     )]]>
                    </sql>
                 </query>
             </queries>		
		</alias>
		<alias name="sessions">
			<queries>
				<query> <sql><![CDATA[select vs.sid ,serial# serial, vs.sql_id,
       vs.username "Username",
       case when vs.status = 'ACTIVE' then last_call_et else null end "Seconds in Wait",
       decode(vs.command,  
                         0,null, 
                         1,'CRE TAB', 
                         2,'INSERT', 
                         3,'SELECT', 
                         4,'CRE CLUSTER', 
                         5,'ALT CLUSTER', 
                         6,'UPDATE', 
                         7,'DELETE', 
                         8,'DRP CLUSTER', 
                         9,'CRE INDEX', 
                         10,'DROP INDEX', 
                         11,'ALT INDEX', 
                         12,'DROP TABLE', 
                         13,'CRE SEQ', 
                         14,'ALT SEQ', 
                         15,'ALT TABLE', 
                         16,'DROP SEQ', 
                         17,'GRANT', 
                         18,'REVOKE', 
                         19,'CRE SYN', 
                         20,'DROP SYN', 
                         21,'CRE VIEW', 
                         22,'DROP VIEW', 
                         23,'VAL INDEX', 
                         24,'CRE PROC', 
                         25,'ALT PROC', 
                         26,'LOCK TABLE', 
                         28,'RENAME', 
                         29,'COMMENT', 
                         30,'AUDIT', 
                         31,'NOAUDIT', 
                         32,'CRE DBLINK', 
                         33,'DROP DBLINK', 
                         34,'CRE DB', 
                         35,'ALTER DB', 
                         36,'CRE RBS', 
                         37,'ALT RBS', 
                         38,'DROP RBS', 
                         39,'CRE TBLSPC', 
                         40,'ALT TBLSPC', 
                         41,'DROP TBLSPC', 
                         42,'ALT SESSION', 
                         43,'ALT USER', 
                         44,'COMMIT', 
                         45,'ROLLBACK', 
                         46,'SAVEPOINT', 
                         47,'PL/SQL EXEC', 
                         48,'SET XACTN', 
                         49,'SWITCH LOG', 
                         50,'EXPLAIN', 
                         51,'CRE USER', 
                         52,'CRE ROLE', 
                         53,'DROP USER', 
                         54,'DROP ROLE', 
                         55,'SET ROLE', 
                         56,'CRE SCHEMA', 
                         57,'CRE CTLFILE', 
                         58,'ALTER TRACING', 
                         59,'CRE TRIGGER', 
                         60,'ALT TRIGGER', 
                         61,'DRP TRIGGER', 
                         62,'ANALYZE TAB', 
                         63,'ANALYZE IX', 
                         64,'ANALYZE CLUS', 
                         65,'CRE PROFILE', 
                         66,'DRP PROFILE', 
                         67,'ALT PROFILE', 
                         68,'DRP PROC', 
                         69,'DRP PROC', 
                         70,'ALT RESOURCE', 
                         71,'CRE SNPLOG', 
                         72,'ALT SNPLOG', 
                         73,'DROP SNPLOG', 
                         74,'CREATE SNAP', 
                         75,'ALT SNAP', 
                         76,'DROP SNAP', 
                         79,'ALTER ROLE', 
                         79,'ALTER ROLE', 
                         85,'TRUNC TAB', 
                         86,'TRUNC CLUST', 
                         88,'ALT VIEW', 
                         91,'CRE FUNC', 
                         92,'ALT FUNC', 
                         93,'DROP FUNC', 
                         94,'CRE PKG', 
                         95,'ALT PKG', 
                         96,'DROP PKG', 
                         97,'CRE PKG BODY', 
                         98,'ALT PKG BODY', 
                         99,'DRP PKG BODY', 
                         to_char(vs.command)) "Command",
    vs.machine "Machine",
    vs.osuser "OS User", 
    lower(vs.status) "Status",
    vs.program "Program",
    vs.module "Module",
    vs.action "Action",
    vs.resource_consumer_group,
    vs.client_info,
    vs.client_identifier
from gv$session vs 
where vs.USERNAME is not null
and nvl(vs.osuser,'x') <> 'SYSTEM'
and vs.type <> 'BACKGROUND'
order by 1	]]> </sql>
				</query>
			</queries>
		</alias>
</aliases>