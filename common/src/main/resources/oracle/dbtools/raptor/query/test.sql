select * from (
  SELECT o.OBJECT_NAME, o.OBJECT_ID ,'' short_name, decode(bitand(t.property, 32), 32, 'YES', 'NO') partitioned,
                decode(bitand(t.property, 64), 64, 'IOT',
               decode(bitand(t.property, 512), 512, 'IOT_OVERFLOW',
               decode(bitand(t.flags, 536870912), 536870912, 'IOT_MAPPING', null))) iot_type, 
         o.OWNER OBJECT_OWNER, o.CREATED, o.LAST_DDL_TIME, O.GENERATED, O.TEMPORARY, case when xt.obj# is null then 'N' else 'Y' end EXTERNAL
    FROM SYS.ALL_OBJECTS O ,sys.tab$ t, sys.external_tab$ xt
    WHERE O.OWNER = :SCHEMA
    and   o.object_id = t.obj#(+)
    and   o.object_id = xt.obj#(+)
    AND O.OBJECT_TYPE = 'TABLE' 
union all
SELECT OBJECT_NAME, OBJECT_ID , syn.SYNONYM_NAME short_NAME, decode(bitand(t.property, 32), 32, 'YES', 'NO') partitioned,
                decode(bitand(t.property, 64), 64, 'IOT',
               decode(bitand(t.property, 512), 512, 'IOT_OVERFLOW',
               decode(bitand(t.flags, 536870912), 536870912, 'IOT_MAPPING', null))) iot_type, 
       SYN.TABLE_OWNER OBJECT_OWNER, o.CREATED, o.LAST_DDL_TIME, O.GENERATED, O.TEMPORARY, case when xt.obj# is null then 'N' else 'Y' end EXTERNAL
              FROM SYS.ALL_OBJECTS O, sys.user_synonyms syn,sys.tab$ t, sys.external_tab$ xt
              WHERE  syn.table_owner = o.owner
              and    syn.TABLE_NAME = o.object_NAME
              and    o.object_id = t.obj#
    		  and   o.object_id = xt.obj#(+)
              and    o.object_type = 'TABLE'
              and    :INCLUDE_SYNS = 1
)

