<?xml version = '1.0' encoding = 'UTF-8'?>
<!--
  Copyright (c) 2018  Oracle Corporation
 
  The Universal Permissive License (UPL), Version 1.0
  
  Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
  software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
  all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
  hereunder covering either 
  (i) the unmodified Software as contributed to or provided by such licensor, or 
  (ii) the Larger Works (as defined below), to deal in both
 
  (a) the Software, and
  (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
  (each a “Larger Work” to which the Software is contributed by such licensors),
 
  without restriction, including without limitation the rights to copy, create derivative works of, display, 
  perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
  sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 
  This license is subject to the following condition:
  The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
  must be included in all copies or substantial portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
  TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
  DEALINGS IN THE SOFTWARE. 
 
-->

<tns>
   <payload number="02502">02502. 00000 -  "Authentication: unable to find initialization function"
*Cause:    The native authentication service was unable to call the
           initialization function for the specified service because
           it does not exist.
*Action:   If this service adaptor came directly from Oracle, contact
           Oracle Customer Support, as this error should never
           happen. Otherwise, add an initialization function for the
           service being used.
</payload>
   <payload number="02503">02503. 00000 -  "Parameter retrieval failed"
*Cause:    The native service layer was unable to retrieve a parameter from a
           configuration file.
*Action:   If it is possible, enable tracing and attempt to reproduce the
           problem.
           In any event, contact Oracle Customer Support.
</payload>
   <payload number="02504">02504. 00000 -  "Parameter count retrieval failed"
*Cause:    The native service layer was unable to determine the number of
           arguments given for a configuration parameter.
*Action:   If it is possible, enable tracing and attempt to reproduce the
           problem.
           In any event, contact Oracle Customer Support.
</payload>
   <payload number="02505">02505. 00000 -  "Authentication: null context pointer provided"
*Cause:    The function nau_ini() was passed a null pointer as the pointer to
           the context that it is supposed to use.
*Action:   Call nau_ini() with a pointer to a context structure.
</payload>
   <payload number="02506">02506. 00000 -  "Authentication: no type string"
*Cause:    An authentication context structure does not contain a string that
           describes the authentication service being used.
*Action:   If it is possible, enable tracing and attempt to reproduce the
           problem.
           In any event, contact Oracle Customer Support.
</payload>
   <payload number="02507">02507. 00000 -  "Encryption: algorithm not installed"
*Cause:    After picking an algorithm, the server was unable to find
           an index for it in its table of algorithms.  This should
           be impossible because the algorithm was chosen
           (indirectly) from that list.
*Action:   Not normally visible to the user.  For further details,
           turn on tracing and reexecute the operation.  If error
           persists, contact Oracle Customer Support.
</payload>
   <payload number="02508">02508. 00000 -  "Encryption: server negotiation response in error"
*Cause:    The server's response in negotiation was in error.
*Action:   Not normally visible to the user.  For further details,
           turn on tracing and reexecute the operation.  If error
           persists, contact Oracle Customer Support.
</payload>
   <payload number="02509">02509. 00000 -  "Authentication: invalid process state"
*Cause:    The state in which a process is running does not correspond to any
           of the values which are valid.
*Action:   If it is possible, enable tracing and attempt to reproduce the
           problem.
           In any event, contact Oracle Customer Support.
</payload>
   <payload number="02510">02510. 00000 -  "Invalid numeric data type"
*Cause:    The type of a piece of numeric data that was received does not
           correspond to one of the valid values.
*Action:   If it is possible, enable tracing and attempt to reproduce the
           problem.
           In any event, contact Oracle Customer Support.
</payload>
   <payload number="02511">02511. 00000 -  "Invalid data type"
*Cause:    The type of a piece of data that was received or to be transmitted
           did not correspond to any of the correct values.
*Action:   If it is possible, enable tracing and attempt to reproduce the
           problem.
           In any event, contact Oracle Customer Support.
</payload>
   <payload number="02512">02512. 00000 -  "Invalid status received"
*Cause:    A process received a value as a status flag which was unknown.
*Action:   If it is possible, enable tracing and attempt to reproduce the
           problem.
           In any event, contact Oracle Customer Support.
</payload>
   <payload number="02513">02513. 00000 -  "Requested data type does not match retrieved type"
*Cause:    A service requested data whose type does not match that of the segment
           which was sent from the other process.
*Action:   If it is possible, enable tracing and attempt to reproduce the
           problem.
           In any event, contact Oracle Customer Support.
</payload>
   <payload number="02514">02514. 00000 -  "Invalid packet received"
*Cause:    A process received a data packet which was not meant for the native
           services layer.
*Action:   If it is possible, enable tracing and attempt to reproduce the
           problem.
           In any event, contact Oracle Customer Support.
</payload>
   <payload number="02515">02515. 00000 -  "Encryption/crypto-checksumming: unknown control type"
*Cause:    An encryption or crypto-checksumming algorithm "control"
           function was called, but did not recognize the "type"
           argument it was given.
*Action:   Not normally visible to the user.  For further details,
           turn on tracing and reexecute the operation.  If error
           persists, contact Oracle Customer Support.
</payload>
   <payload number="02516">02516. 00000 -  "No data available"
*Cause:    A native service attempted to retrieve data but no data was
           available to be received.
*Action:   The error is not normally visible as it usually is only used to
           signal the end of a data stream. If the error becomes visible,
           enable tracing to reproduce the problem and contact
           Oracle Customer Support.
</payload>
   <payload number="02517">02517. 00000 -  "key smaller than requested size"
*Cause:    The key returned by negotiation was smaller than
           the size requested by some service (either encryption or
           crypto-checksumming).
*Action:   The error is not normally visible.  If the error persists,
           enable tracing to reproduce the problem and contact
           Oracle Customer Support.
</payload>
   <payload number="02518">02518. 00000 -  "key negotiation error"
*Cause:    An error occurred while the two sides of the connection
           were negotiating an encryption or crypto-checksumming key.
*Action:   The error is not normally visible.  If the error persists,
           enable tracing to reproduce the problem and contact
           Oracle Customer Support.
</payload>
   <payload number="02519">02519. 00000 -  "no appropriate key-negotiation parameters"
*Cause:    No appopriate key-negotiation parameters are available for
           the key size requested either by encryption or by crypto-
           checksumming.
*Action:   The error is not normally visible.  Enable tracing to
           reproduce the problem and contact Oracle Customer
           Support.
</payload>
   <payload number="02520">02520. 00000 -  "encryption/crypto-checksumming: no Diffie-Hellman seed"
*Cause:    The "sqlnet.crypto_seed" parameter is missing from the
           SQLNET.ORA parameters file.
*Action:   Add this line to SQLNET.ORA
           sqlnet.crypto_seed = "randomly-chosen text"
</payload>
   <payload number="02521">02521. 00000 -  "encryption/crypto-checksumming: Diffie-Hellman seed too small"
*Cause:    The "sqlnet.crypto_seed" parameter in the SQLNET.ORA
           parameter file for Oracle Net is too small.
*Action:   Add more randomly-chosen text to it.
</payload>
   <payload number="02524">02524. 00000 -  "Authentication: privilege check failed"
*Cause:    An error occurred when the authentication service attempted
           to verify that a user had a specific database privilege.
*Action:   This error should not happen normally. Enable tracing and attempt
           to repeat the error. Contact Customer Support.
</payload>
   <payload number="02525">02525. 00000 -  "encryption/crypto-checksumming: self test failed"
*Cause:    The encryption/crypto-checksumming service detected an error
           while running tests on the active encryption or checksumming
           algorithm.
*Action:   Contact Customer Support.
</payload>
   <payload number="02526">02526. 00000 -  "server proxy type does not match client type"
*Cause:    The authentication type selected by the server does not match that
           picked by the client.
*Action:   Contact Oracle Customer Support
</payload>
   <payload number="03501">03501. 00000 -  "OK"
*Cause:    The operation succeeded.
*Action:   No action necessary.
</payload>
   <payload number="03502">03502. 00000 -  "Insufficient arguments.  Usage:  tnsping &lt;address> [&lt;count>]"
*Cause:    Some required command-line arguments are missing.
*Action:   Re-enter the command using the correct arguments.
</payload>
   <payload number="03503">03503. 00000 -  "Could not initialize NL"
*Cause:    The network library could not be initialized.
*Action:   This is an internal error which should not normally be visible.
           Ensure that memory is available to run the application and that
           there are no other operating system problems, and then attempt
           the command again.
</payload>
   <payload number="03504">03504. 00000 -  "Service name too long"
*Cause:    The service name you are attempting to ping is too long.
*Action:   Re-enter the command using the correct service name.
</payload>
   <payload number="03505">03505. 00000 -  "Failed to resolve name"
*Cause:    The service name you provided could not be found in TNSNAMES.ORA,
           an Oracle Names server, or a native naming service.
*Action:   Verify that you entered the service name correctly.  You may need
           to ensure that the name was entered correctly into the network
           configuration.
</payload>
   <payload number="03506">03506. 00000 -  "Failed to create address binding"
*Cause:    The TNSPING utility found the requested address or service name,
           but received an internal error when trying to use it.
*Action:   This is an internal error which should not normally be visible.
           Ensure that memory is available to run the application and that
           there are no other operating system problems, and then attempt
           the command again.
</payload>
   <payload number="03507">03507. 00000 -  "Failure looking for ADDRESS keyword"
*Cause:    The TNS address did not contain an ADDRESS keyword.
*Action:   If you entered the TNS address on the command line, be sure that
           the syntax is correct.  If you entered a service name on the
           command line, the address contains the wrong information.  You
           should verify that the information was entered correctly.
</payload>
   <payload number="03508">03508. 00000 -  "Failed to create address string"
*Cause:    The TNSPING utility received an internal error when generating
           an address.
*Action:   This is an internal error which should not normally be visible.
           Ensure that memory is available to run the application and that
           there are no other operating system problems, and then attempt
           the command again.
</payload>
   <payload number="03509">03509. 00000 -  "OK (%d msec)"
*Cause:    The operation succeeded, in this amount of time.
*Action:   No action necessary.
</payload>
   <payload number="03510">03510. 00000 -  "Failed due to I/O error"
*Cause:    An I/O operation failed, perhaps due to a resource failure
           or premature window termination.
*Action:   This is an internal error which should not normally be visible.
           Do not close the TNSPING window before all I/O operations have
           completed.
</payload>
   <payload number="03511">03511. 00000 -  "Used parameter files:\n%s\n"
*Cause:    Prints out the path of the parameter files(sqlnet.ora,tnsnames.ora)
           used in the process of resolving the NAME.
*Action:   NONE
</payload>
   <payload number="03512">03512. 00000 -  "Used %s adapter to resolve the alias\n"
*Cause:    Prints out the name of the adapter which resolved the TNS alias.
*Action:   NONE
</payload>
   <payload number="03513">03513. 00000 -  "Attempting to contact %s\n"
*Cause:    Attempting to contact a given TNS address.
*Action:   NONE
</payload>
   <payload number="03601">03601. 00000 -  "Failed in route information collection"
*Cause:    The route could either not connect, or encountered an
           unsupported version of Oracle Net.
*Action:   Check if Oracle Net along all nodes is version 2.3 or
           greater.
</payload>
   <payload number="03602">03602. 00000 -  "Insufficient arguments.  Usage:  trcroute &lt;address> "
*Cause:    Some required command-line arguments are missing.
*Action:   Re-enter the command using the correct arguments.
</payload>
   <payload number="03603">03603. 00000 -  "Encountered a node with a version eariler than SQL*Net 2.3"
*Cause:    Versions of SQL*Net before 2.3 do not support
           trcroute.
*Action:   Find the node that isn't responding to trcroute.
</payload>
   <payload number="04001">04001. 00000 -  "%s"
*Cause:    CMCTL general message.
*Action:   None.
</payload>
   <payload number="04002">04002. 00000 -  "The command completed successfully."
*Cause:    Not applicable.
*Action:   None
</payload>
   <payload number="04003">04003. 00000 -  "Syntax Error."
*Cause:    The command issued has a wrong syntax.
*Action:   Check for the command issue syntax, and correct the problem.
</payload>
   <payload number="04004">04004. 00000 -  "Unable to encrypt the supplied password."
*Cause:    The password that was supplied to the ADMINISTER command
           could not be encrypted.
*Action:   Change the password to something acceptable. See Oracle Net
           Services documentation for valid password values.
</payload>
   <payload number="04005">04005. 00000 -  "Unable to resolve address for %s."
*Cause:    The alias name supplied with the ADMINISTER command could not
           be resolved to an Oracle Connection Manager address.
*Action:   Check if your an entry for the alias is present in either
           CMAN.ORA file or in TNSNAMES.ORA file.
</payload>
   <payload number="04006">04006. 00000 -  "Invalid password"
*Cause:    The password given with the ADMINISTER command could not
           be verified by the Oracle Connection Manager instance.
*Action:   Retry with the correct password.
</payload>
   <payload number="04007">04007. 00000 -  "Internal error %d."
*Cause:    Not normally visible to the user.
*Action:   If error persists, contact Oracle Support Services.
</payload>
   <payload number="04008">04008. 00000 -  "%-25s | %5s"
*Cause:    String parameter value.
*Action:   None.
</payload>
   <payload number="04009">04009. 00000 -  "%-25s | %5d"
*Cause:    Integer parameter value.
*Action:   None.
</payload>
   <payload number="04010">04010. 00000 -  "Command cannot be issued before the ADMINISTER command."
*Cause:    A command was issued before administering the Oracle
           Connection Manager instance.
*Action:   Enter the ADMINISTER command before retrying this command.
</payload>
   <payload number="04011">04011. 00000 -  "Oracle Connection Manager instance not yet started."
*Cause:    A command was issued when Oracle Connection Manager was not
           yet started or already shutdown.
*Action:   Start Oracle Connection Manager.
</payload>
   <payload number="04012">04012. 00000 -  "Unable to start Oracle Connection Manager instance."
*Cause:    CMCTL was unable to start the Oracle Connection Manager
           instance. Some of the possible reasons include: cmadmin not
           present in ORACLE_HOME/bin, Invalid parameter in configuration
           repository, wrong parameter values, or log directory not present.
*Action:   1. Check whether log directory is present, and is writable.
           Log directory can be found at ORACLE_HOME/network/log, or
           as specified by the LOG_DIRECTORY parameter in CMAN.ORA.
           2. Turn on logging or tracing to get more information
           about this error.
           3. Correct any parameter errors (or mismatched paranthesis),
           and retry starting Oracle Connection Manager.
           4. If the problem persists, contact Oracle Support Services.
</payload>
   <payload number="04013">04013. 00000 -  "CMCTL timed out waiting for Oracle Connection Manager to start"
*Cause:    Oracle Connection Manager internal registrations have not
           been completed.
*Action:   Turn on logging or tracing to get more information about this
           error. If the problem persists, contact Oracle Support
           Services.
</payload>
   <payload number="04014">04014. 00000 -  "Current instance %s is already started"
*Cause:    An ADMINISTER command was issued to administer an Oracle
           Connection Manager instance that is already started.
*Action:   None
</payload>
   <payload number="04015">04015. 00000 -  "Current instance %s is not yet started"
*Cause:    An ADMINISTER command was issued to an instance that has
           not yet been started.
*Action:   None
</payload>
   <payload number="04016">04016. 00000 -  "Connecting to %s"
*Cause:    CMCTL is connecting to the the specified Oracle Connection
           Manager address.
*Action:   None
</payload>
   <payload number="04017">04017. 00000 -  "Please wait. Shutdown in progress."
*Cause:    A command which cannot be used while Oracle Connection Manager
           is shutting down was issued.
*Action:   Wait for some time before retrying the command.
</payload>
   <payload number="04018">04018. 00000 -  "Instance already started"
*Cause:    The STARTUP command was issued to an instance which was already
           started.
*Action:   None
</payload>
   <payload number="04019">04019. 00000 -  "Starting Oracle Connection Manager instance %s. Please wait..."
*Cause:    The STARTUP command was issued for an Oracle Connection
           Manager instance. CMCTL is waiting for the instance to start.
*Action:   None
</payload>
   <payload number="04020">04020. 00000 -  "CMCTL Version: %u.%u.%u.%u.%u"
*Cause:    None.
*Action:   None.
</payload>
   <payload number="04021">04021. 00000 -  "The SET command is unsuccessful for parameter %s."
*Cause:    The value set for the parameter was out of range.
*Action:   See Oracle Net Services documentation for value ranges for
           the parameters.
</payload>
   <payload number="04022">04022. 00000 -  "%s parameter %s set to %s."
*Cause:    Parameter value was successfully set.
*Action:   None
</payload>
   <payload number="04023">04023. 00000 -  "Command failed."
*Cause:    The command issued failed to complete successfully.
*Action:   Retry command. If problem persists, contact Oracle
           Support Services.
</payload>
   <payload number="04024">04024. 00000 -  "Status of the Instance"
</payload>
   <payload number="04025">04025. 00000 -  "----------------------"
</payload>
   <payload number="04026">04026. 00000 -  "Instance name             %s"
</payload>
   <payload number="04027">04027. 00000 -  "Version                   %s"
</payload>
   <payload number="04028">04028. 00000 -  "Start date                %s"
</payload>
   <payload number="04029">04029. 00000 -  "Uptime                    %u days %u hr. %u min. %u sec"
</payload>
   <payload number="04030">04030. 00000 -  "Num of gateways started   %u"
</payload>
   <payload number="04031">04031. 00000 -  "Average Load level        %u"
</payload>
   <payload number="04032">04032. 00000 -  "Log Level                 %s"
</payload>
   <payload number="04033">04033. 00000 -  "Trace Level               %s"
</payload>
   <payload number="04034">04034. 00000 -  "Instance Config file      %s"
</payload>
   <payload number="04035">04035. 00000 -  "Instance Log directory    %s"
</payload>
   <payload number="04036">04036. 00000 -  "Instance Trace directory  %s"
</payload>
   <payload number="04037">04037. 00000 -  "Connections refer to %s."
*Cause:    The ADMINISTER command resolved to the address in the specified
           message string.
*Action:   None
</payload>
   <payload number="04038">04038. 00000 -  "Gateway ID                     %u"
</payload>
   <payload number="04039">04039. 00000 -  "Gateway state                  %s"
</payload>
   <payload number="04040">04040. 00000 -  "Number of active connections   %u"
</payload>
   <payload number="04041">04041. 00000 -  "Peak active connections        %u"
</payload>
   <payload number="04042">04042. 00000 -  "Total connections              %u"
</payload>
   <payload number="04043">04043. 00000 -  "Total connections refused      %u"
</payload>
   <payload number="04044">04044. 00000 -  "Specified gateways do not exist."
*Cause:    One or more specified gateway IDs do not exist.
*Action:   Specify correct gateway IDs.
</payload>
   <payload number="04045">04045. 00000 -  "Invalid specification of time"
*Cause:    The time specified with the GT (greater than) option was invalid.
*Action:   Check Oracle Net Services documentation for valid time
           specification.
</payload>
   <payload number="04046">04046. 00000 -  "Invalid specification for source"
*Cause:    The source specified with the FROM option was invalid.
*Action:   Check Oracle Net Services documentation for valid
           source specification.
</payload>
   <payload number="04047">04047. 00000 -  "Invalid specification for destination"
*Cause:    The destination specified with the TO option was invalid.
*Action:   Check Oracle Net Services documentation for valid destination
           specification.
</payload>
   <payload number="04048">04048. 00000 -  "Specified service does not exist."
*Cause:    The service specified with the FOR option did not exist.
*Action:   None
</payload>
   <payload number="04049">04049. 00000 -  "Specified connections do not exist"
*Cause:    One or more connection IDs do not exist.
*Action:   None
</payload>
   <payload number="04050">04050. 00000 -  "Invalid specification for gateway ID."
*Cause:    The gateway ID specified is invalid.
*Action:   Check Oracle Net Services documentation for a valid gateway
           ID specification.
</payload>
   <payload number="04051">04051. 00000 -  "Connection ID                 %u"
</payload>
   <payload number="04052">04052. 00000 -  "Gateway ID                    %u"
</payload>
   <payload number="04053">04053. 00000 -  "Source                        %s"
</payload>
   <payload number="04054">04054. 00000 -  "Destination                   %s"
</payload>
   <payload number="04055">04055. 00000 -  "Service                       %s"
</payload>
   <payload number="04056">04056. 00000 -  "State                         %s"
</payload>
   <payload number="04057">04057. 00000 -  "Idle time                     %u"
</payload>
   <payload number="04058">04058. 00000 -  "Connected time                %u"
</payload>
   <payload number="04059">04059. 00000 -  "Bytes Received [IN]           %u"
</payload>
   <payload number="04060">04060. 00000 -  "Bytes Sent     [IN]           %u"
</payload>
   <payload number="04061">04061. 00000 -  "Bytes Received [OUT]          %u"
</payload>
</tns>