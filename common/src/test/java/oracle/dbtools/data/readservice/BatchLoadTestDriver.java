/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.DefaultConnectionResolver;
import oracle.dbtools.data.loadservice.LoadParmsAPI.DELETE_ROWS;
import oracle.dbtools.data.loadservice.LoadParmsAPI.SERVICE_METHOD;
import oracle.dbtools.data.loadservice.LoadParmsAPI;
import oracle.dbtools.data.loadservice.LoadAPI;
import oracle.dbtools.data.loadservice.ExitCode;
import oracle.dbtools.data.readservice.ReadParmsDelimitedAPI;
import oracle.dbtools.util.LogHandler;


/**
 * Test Driver for BatchLoader * 
 * @author Joyce Scapicchio
 */

public class BatchLoadTestDriver {
	/** Set these variables appropriately for the test environment  */
	//private static String TEST_CONNECTION ="jdbc:oracle:thin:@yourmachine.yourdomain:1521/yourmachine.yourdomain";
	private static String TEST_CONNECTION ="jdbc:oracle:thin:@gbr30060.uk.oracle.com:1522:DB11GR2";

	//private static String TEST_CONNECTION ="jdbc:oracle:thin:@gbr30060.uk.oracle.com:1522/DB11GR2.uk.oracle.com";
	//private static String TEST_CONNECTION ="jdbc:oracle:thin:@gbr30060.uk.oracle.com:1521/DB12GR1P.uk.oracle.com ";
	//private static String TEST_CONNECTION ="jdbc:oracle:thin:@gbr30025.uk.oracle.com:1523/DB12CR1C";
	//private static String TEST_CONNECTION ="jdbc:oracle:oci:@yourmachine.yourdomain:1521/yourmachine.yourdomain";
	
	private static String FILE_DIR="c:\\temp\\load\\";
	//private static String FILE_DIR="/scratch/jscapicc/MyData/load/";
	private static String TEST_S="hr_load";
	private static String TEST_P="hr_load";
	//private static String TEST_S="jms";
	//private static String TEST_P="jms";
	//private static String TEST_S="system";
	//private static String TEST_P="dbtools";
	
	private static BatchLoadTestDriver m_instance = null;
	
	private String _filename;
	private static String _fileDir = FILE_DIR;
	private Connection _conn;
	private String _schema;
	private String _table;
	private FileInputStream _input;
	private FileOutputStream _output;
	private FileOutputStream _log;
	private String _test;
	private URL _lastURL;


	/**
	 * Dummy main for testing purposes.
	 * Runs the tests for load services.
	 * 
	 * 1.  The test data files should be copied into a working directory 
	 * 2.  Set the value of FILE_DIR to the path for the working directory
	 * 3.  Set the value of TEST_CONNECTION to the connection string
	 * 4.  Create user hr_load/hr_load or set values for TEST_S and TEST_P as user/pass
	 * 5.  Run test/sql/BatchLoadTestInstall.sql
	 * 
	 * We should allow parameters for the above, so feel free to add that capability.
	 * 
	 * Run test/sql/BatchLoadTestClean.sql to truncate tables used in this test.		
	 * 
	 * Response (.rsp) and log (.log) files are created alongside the data files
	 * 
	 * See individual tests for expected results.
	 * 
	 * 
	 * @param args 
	 */
	public static void main(String[] args) {
		// standalone test supporting just logging and exit code
		try {
			DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
			
			// make sure driver is loaded & DBUtil initialized
            String DRIVER_CLASS_NAME = "oracle.jdbc.OracleDriver"; //$NON-NLS-1$
            Class.forName(DRIVER_CLASS_NAME);
                        
			BatchLoadTestDriver driver = getInstance();

			driver.launchTests();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Singleton instance.
	 * 
	 * @return an instance of the test driver
	 */
	public static BatchLoadTestDriver getInstance() {
		if (m_instance == null) {
			m_instance = new BatchLoadTestDriver();
		}
		return m_instance;
	}

	
	private void testV1(){
		/*
		 *  Test Validate One - bad connection
		 *  
		 *  INFO: starting load service: Test V1
		 *	CONNECTION
		 *	Test V1 Completed with exit code: 3 - SEVERE: Severe error, processing terminated
		 */
		try {

			_test="Test V1";
			_filename="vtest01.csv";
			_table = "VTEST01";
			Connection conn = _conn;
			_conn=null;

			ExitCode exitCode = doLoad(null, null);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated());
			_conn=conn;

		} catch (Exception e){
			e.printStackTrace();
		}	
	}
	
	private void testV2(){
		/*
		 *  Test Validate 2 - closed connection
		 *  
		 *  Expected Results:
		 *  SEVERE: Connection error. Closed Connection
		 *  Unable to start load service;
		 *  Test V2 Completed with exit code: 3 - SEVERE: Severe error, processing terminated
		 */
		try {

			_test="Test V2";
			_filename="vtest02.csv";
			_table = "VTEST02";
			_conn.close();

			ExitCode exitCode = doLoad(null, null);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated());

			// restore connection
			_conn = DriverManager.getConnection(TEST_CONNECTION, TEST_S,
			TEST_P);

		} catch (Exception e){
			e.printStackTrace();
		} 
	}
	
	private void testV3(){
		/*
		 *  Test Validate 3 - bad schema
		 *  
		 *  Expected results:  SEVERE: Table SCHEMA_NOT_EXISTS.DEPARTMENTS does not exist or is not available from connection.
		 *  Unable to start load service;
		 *  Test V3 Completed with exit code: 3 - SEVERE: Severe error, processing terminated
		 */
		try {
			
			String schemaSave = _schema;
			_schema="SCHEMA_NOT_EXISTS";
			_test="Test V3";
			_filename="vtest03.csv";
			_table = "DEPARTMENTS";

			ExitCode exitCode = doLoad(null, null);
			_schema=schemaSave;
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated());


		} catch (Exception e){
			e.printStackTrace();
		} 
	}
	
	private void testV4(){
		/*
		 *  Test Validate 4 - bad table
		 *  
		 *  Expected results:  Unable to start load service;
		 *  SEVERE: Table HR_LOAD.TABLE_NOT_EXISTS does not exist or is not available from connection.
		 *  Test V4 Completed with exit code: 3 - SEVERE: Severe error, processing terminated
		 */
		try {
			
			_test="Test V4";
			_filename="vtest04.csv";
			_table = "TABLE_NOT_EXISTS";

			ExitCode exitCode = doLoad(null, null);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated());


		} catch (Exception e){
			e.printStackTrace();
		} 
	}
	
	private void testV5(){
		/*
		 *  Test Validate 5 - empty file (no  header row)
		 *  
		 *  Expected results : SEVERE: Column in header row abc is not defined for table.
		 *     Test V5 Completed with exit code: 3 - SEVERE: Severe error, processing terminated
		 */
		try {
			
			_test="Test V5";
			_filename="vtest05.csv";
			_table = "VTEST05";

			ExitCode exitCode = doLoad(null, null);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated());


		} catch (Exception e){
			e.printStackTrace();
		} 
	}
	
	private void test1(){
		/*
		 *  Test One - simple load of csv with no errors in it using default reader and default loader
		 *  
		 *  Expected Results: Test 1 Completed with exit code: 0 - SUCCESS: Load processed without errors	
		 *  #INFO Number of rows processed: 27
		 *	#INFO Number of rows in error: 0
		 *
		 */
		try {

			_test="Test 1";
			_filename="test01.csv";
			_table = "TEST01";

			ExitCode exitCode = doLoad(null, null);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated());


		} catch (Exception e){
			e.printStackTrace();
		} 
	}
	private void test2(){
		/*
		 *  Test Two - simple load of csv with no errors in it passed reader and passed loader
		 *  All defaults on reader parm
		 *  Delete rows = TRUNCATE
		 *  Date format dd-mon-rr in data and passed as load parm
		 *  ResponseLocales Italian, french, english
		 *  
  		 *  Expected Results: Test 2 Completed with exit code: 0 - SUCCESS: Load processed without errors
 		 *  #INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 *
		 *  Response in Italian
		 *
		 */
		try {

			_test="Test 2";
			_filename="test02.csv";
			_table = "TEST02";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			ArrayList<Locale> prefLocales = new ArrayList<Locale>(3);
			prefLocales.add(Locale.ITALIAN);
			prefLocales.add(Locale.FRENCH);
			prefLocales.add(Locale.ENGLISH);
			
			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.responseLocales(prefLocales)
			.deleteRows(DELETE_ROWS.TRUNCATE)
			.dateFormat("DD-MON-RR")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test2a(){
		/*
		 *  Test Two A - simple load of csv with no errors in it passed reader and passed loader
		 *  All defaults on reader parm
		 *  Delete rows = TRUNCATE
		 *  Date format dd-mon-rr in data and passed as load parm
		 *  ResponseLocales Italian, french, english
		 *  
  		 *  Expected Results: Test 2 Completed with exit code: 0 - SUCCESS: Load processed without errors
 		 *  #INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 *
		 *  Response in Italian
		 *
		 */
		try {

			_test="Test 2A";
			_filename="test02a.csv";
			_table = "TEST02A";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			ArrayList<Locale> prefLocales = new ArrayList<Locale>(3);
			prefLocales.add(Locale.ITALIAN);
			prefLocales.add(Locale.FRENCH);
			prefLocales.add(Locale.ENGLISH);
			
			TreeMap <String,String> aliasMap = new TreeMap<String,String>();
			aliasMap.put("employee_id","EMPLOYEE_ID");
			aliasMap.put("first_name", "FIRST_NAME");
			aliasMap.put("salary", "SALARY");
			aliasMap.put("last_name", "LAST_NAME");
			
			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.responseLocales(prefLocales)
			.mapColumnNames(aliasMap)
			.deleteRows(DELETE_ROWS.TRUNCATE)
			.dateFormat("DD-MON-RR")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test3(){
		/*
		 *  Test Three - simple load of dsv with pipe delimiter
		 * 	Delete rows = TRUNCATE
		 *  Date Format DD-MON-RR
		 *  
		 *  Expected Results: Test 3 Completed with exit code: 0 - SUCCESS: Load processed without errors	
		 *	#INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 */
		try {

			_test="Test 3";
			_filename="test03.dsv";
			_table = "TEST03";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.delimiter("|")
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.TRUE)
			.dateFormat("DD-MON-RR")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test3a(){
		/*
		 *  Test ThreeA - simple load of dsv with pipe delimiter and no enclosure
		 * 	Delete rows = TRUNCATE
		 *  Date Format DD-MON-RR
		 *  
		 *  Expected Results: Test 3 Completed with exit code: 0 - SUCCESS: Load processed without errors	
		 *	#INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 */
		try {

			_test="Test 3a";
			_filename="test03a.dsv";
			_table = "TEST03A";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.delimiter("|")
			.enclosures("")
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.TRUE)
			.dateFormat("DD-MON-RR")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	private void test4(){
		/*
		 *  Test Four - simple load of dsv with multi character delimiter
		 *  
		 *  LoadParms.dateFormat Provided (for hire_date)
		 *  Delete rows = FALSE
		 *  
		 *  Expected Results: Test 4 Completed with exit code: 0 - SUCCESS: Load processed without errors	
		 *	#INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 *
		 */
		try {

			_test="Test 4";
			_filename="test04.dsv";
			_table = "TEST04";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.delimiter("<delim>")
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.FALSE)
			.dateFormat("DD-MON-RR")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));



		} catch (Exception e){
			e.printStackTrace();
		}
	}
	private void test5(){
		/*
		 *  Test 5 - simple load of dsv
		 *  Date format in file is load default yyyy-mm-dd
		 *  LoadParms.dateFormat IS NOT Provided (for hire_date) testing default
		 *  NLS
		 *  
  		 *  Expected Results: 
  		 *  #ERROR Insert failed for row  1
  		 *  #ERROR HIRE_DATE: GDK-05043: not a valid month
  		 *  #ERROR Row 1 data follows:
  		 *  100,Steven,King,SKING,515.123.4567,1987-06-17,AD_PRES,24000,,,90
  		 *  #ERROR Insert failed for row  2
  		 *  #ERROR HIRE_DATE: GDK-05043: not a valid month
  		 *  #ERROR Row 2 data follows:
  		 *  101,Neena,Kochhar,NKOCHHAR,515.123.4568,1989-09-21,AD_VP,17000,,100,90
  		 *  #ERROR Insert failed for row  3
  		 *  #ERROR HIRE_DATE: GDK-05043: not a valid month
  		 *  #ERROR Row 3 data follows:
  		 *  102,Lex,De Haan,LDEHAAN,515.123.4569,1993-01-13,AD_VP,17000,,100,90
  		 *  #ERROR Insert failed for row  4
  		 *  #ERROR HIRE_DATE: GDK-05043: not a valid month
  		 *  #ERROR Row 4 data follows:
		 *   
		 *  All rows as errors above 
		 *   
		 *  #INFO Number of rows processed: 0
		 *	#INFO Number of rows in error: 51
		 *	#INFO Elapsed time: 00:00:01.371 - (1,372 ms)
		 *	2 - ERROR: The Errors Allowed or the Error Limit parameter was exceeded
		 *
		 */
		try {

			_test="Test 5";
			_filename="test05.csv";
			_table = "TEST05";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test5a(){
		/*
		 *  Test 5a - simple load of dsv
		 *  Date format in file is load default yyyy-mm-dd
		 *  LoadParms.dateFormat IS Provided (for hire_date) yyy-mm-dd
		 *  
  		 *  Expected Results: Test 5a Completed with exit code: 0 - SUCCESS: Load processed without errors	
		 *	#INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 */
		try {

			_test="Test 5a";
			_filename="test05a.csv";
			_table = "TEST05A";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.dateFormat("yyyy-mm-dd")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}

	private void test6(){
		/*
		 *  Test 6 - simple load of dsv
		 *  Date format in file is load default dd-mon-rr
		 *	#INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 *	#INFO Elapsed time: 00:00:10.163 - (10,164 ms)
		 *	0 - SUCCESS: Load processed without errors
		 *
		 */
		try {

			_test="Test 6";
			_filename="test06.csv";
			_table = "TEST06";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.errors(5)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test6a(){
		/*
		 *  Test 6a - test response format sql with simple load of dsv
		 *  Date format in file is load default dd-mon-rr
		 *  LoadParms.dateFormat IS specified incorrectly (for hire_date)
		 *  
		 *  Expected Results - 6 errors (non numeric date), then severe exceed errors
		 *  --Insert failed for row  1
		 *  --HIRE_DATE: GDK-05058: non-numeric character found
		 *  --Row 1 data follows:
		 *  INSERT INTO HR_LOAD.TEST06A(EMPLOYEE_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE_NUMBER,HIRE_DATE,JOB_ID,SALARY,COMMISSION_PCT,MANAGER_ID,DEPARTMENT_ID)
		 *  VALUES (100.0,'Steven','King','SKING','515.123.4567',to_date('17-JUN-87', 'DD-MM-YYYY'),'AD_PRES',24000.0,NULL,NULL,90.0);
		 *  --Insert failed for row  2
		 *  --HIRE_DATE: GDK-05058: non-numeric character found
		 *  --Row 2 data follows:
		 *  INSERT INTO HR_LOAD.TEST06A(EMPLOYEE_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE_NUMBER,HIRE_DATE,JOB_ID,SALARY,COMMISSION_PCT,MANAGER_ID,DEPARTMENT_ID)
		 *  VALUES (101.0,'Neena','Kochhar','NKOCHHAR','515.123.4568',to_date('21-SEP-89', 'DD-MM-YYYY'),'AD_VP',17000.0,NULL,100.0,90.0);
		 *  --Insert failed for row  3
		 *  --HIRE_DATE: GDK-05058: non-numeric character found
		 *  --Row 3 data follows:
		 *  INSERT INTO HR_LOAD.TEST06A(EMPLOYEE_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE_NUMBER,HIRE_DATE,JOB_ID,SALARY,COMMISSION_PCT,MANAGER_ID,DEPARTMENT_ID)
		 *  VALUES (102.0,'Lex','De Haan','LDEHAAN','515.123.4569',to_date('13-JAN-93', 'DD-MM-YYYY'),'AD_VP',17000.0,NULL,100.0,90.0);
		 *  --Insert failed for row  4
		 *  --HIRE_DATE: GDK-05058: non-numeric character found
		 *  --Row 4 data follows:
		 *  INSERT INTO HR_LOAD.TEST06A(EMPLOYEE_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE_NUMBER,HIRE_DATE,JOB_ID,SALARY,COMMISSION_PCT,MANAGER_ID,DEPARTMENT_ID)
		 *  --Insert failed for row  5
		 *  --HIRE_DATE: GDK-05058: non-numeric character found
		 *  --Row 5 data follows:
		 *  INSERT INTO HR_LOAD.TEST06A(EMPLOYEE_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE_NUMBER,HIRE_DATE,JOB_ID,SALARY,COMMISSION_PCT,MANAGER_ID,DEPARTMENT_ID)
		 *  VALUES (104.0,'Bruce','Ernst','BERNST','590.423.4568',to_date('21-MAY-91', 'DD-MM-YYYY'),'IT_PROG',6000.0,NULL,103.0,60.0);
		 *  --Insert failed for row  6
		 *  --HIRE_DATE: GDK-05058: non-numeric character found
		 *  --Row 6 data follows:
		 *  INSERT INTO HR_LOAD.TEST06A(EMPLOYEE_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE_NUMBER,HIRE_DATE,JOB_ID,SALARY,COMMISSION_PCT,MANAGER_ID,DEPARTMENT_ID)
		 *  VALUES (105.0,'David','Austin','DAUSTIN','590.423.4569',to_date('25-JUN-97', 'DD-MM-YYYY'),'IT_PROG',4800.0,NULL,103.0,60.0);
		 *  --Number of rows processed: 0
		 *  --Number of rows in error: 6
		 *  --Elapsed time: 00:00:01.542 - (1,542 ms)
		 *  2 - ERROR: The Errors Allowed or the Error Limit parameter was exceeded
		 *	
		 *
		 */
		try {

			_test="Test 6a";
			_filename="test06a.csv";
			_table = "TEST06A";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.dateFormat("DD-MM-YYYY")
			.responseFormat(LoadParmsAPI.RESPONSE_FORMAT.RESPONSE_SQL)
			.errors(5)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test7(){
		/*
		 *  Test 7 - simple load of dsv with error in row 3 and row 23.
		 *  Date format in file is load default yyyy-mm-dd
		 *  LoadParms.dateFormat IS yyyy-mm-dd Provided (for hire_date)
		 *  rowsPerBatch=10
		 *  batchesPerCommit=2
		 *  NLS
		 *  INFO: Insert failed in batch rows  1  through  10 
		 *  INFO: ORA-01438: value larger than specified precision allowed for this column
  		 *  INFO: Insert failed in batch rows  21  through  30 
  		 *  INFO: ORA-01722: invalid number
  		 *  Expected Results: Test 7 Completed with exit code: 1 - WARNING: Load processed with errors
		 *  --INFO--Number of rows processed: 107
		 *	--INFO--Number of rows in error: 69
		 *
		 * Note that the driver is causing incorrect rows to be written to response file.
		 *
		 */
		try {

			_test="Test 7";
			_filename="test07.csv";
			_table = "TEST07";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.dateFormat("yyyy-mm-dd")
			.batchRows(10)
			.batchesPerCommit(2)
			.errors(100)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}

	private void test7a(){
		/*
		 *  Test 7a - simple load of dsv with error in row 3 and row 23.
		 *  Date format in file is rr-mm-dd
		 *  LoadParms.dateFormat IS Provided (for hire_date) testing default
		 *  rowsPerBatch=15
		 *  batchMax=10
		 *  batchesPerCommit=2
		 *  #ERROR Insert failed in batch rows  1  through  10
		 *  #ERROR ORA-01438: value larger than specified precision allowed for this column
		 *  #ERROR Row 2 data follows:
		 *  101,Neena,Kochhar,NKOCHHAR,515.123.4568,89-09-21,AD_VP,17000,,100,90
		 *  #ERROR Row 3 data follows:
		 *  1020000,Lex,De Haan,LDEHAAN,515.123.4569,93-01-13,AD_VP,17000,,100,90
		 *  #ERROR Row 4 data follows:
		 *  103,Alexander,Hunold,AHUNOLD,590.423.4567,90-01-03,IT_PROG,9000,,102,60
		 *  #ERROR Row 5 data follows:
		 *  104,Bruce,Ernst,BERNST,590.423.4568,91-05-21,IT_PROG,6000,,103,60
		 *  #ERROR Row 6 data follows:
		 *  105,David,Austin,DAUSTIN,590.423.4569,97-06-25,IT_PROG,4800,,103,60
		 *  #ERROR Row 7 data follows:
		 *  106,Valli,Pataballa,VPATABAL,590.423.4560,98-02-05,IT_PROG,4800,,103,60
		 *  #ERROR Row 8 data follows:
		 *  107,Diana,Lorentz,DLORENTZ,590.423.5567,99-02-07,IT_PROG,4200,,103,60
		 *  #ERROR Row 9 data follows:
		 *  108,Nancy,Greenberg,NGREENBE,515.124.4569,94-08-17,FI_MGR,12000,,101,100
		 *  #ERROR Row 10 data follows:
		 *  109,Daniel,Faviet,DFAVIET,515.124.4169,94-08-16,FI_ACCOUNT,9000,,108,100
		 *  ...
  		 *  Expected Results: 
  		 *  #INFO Number of rows processed: 107
  		 *  #INFO Number of rows in error: 16
  		 *  #INFO Elapsed time: 00:00:01.487 - (1,487 ms)
  		 *  1 - WARNING: Load processed with errors
		 */
		try {

			_test="Test 7a";
			_filename="test07a.csv";
			_table = "TEST07A";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.dateFormat("rr-mm-dd")
			.batchRows(15)
			.batchMax(10)
			.batchesPerCommit(2)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test7b(){
		/*
		 *  Test 7b - load of dsv with date format and locale specified 
		 *  with error in rows 15 and 16.  First 12 rows have valid italian
		 *  3 character dates.
		 *  Date format in file is rr-mon-dd
		 *  LoadParms.dateFormat IS Provided (for hire_date)
		 *  Locale parm specified as Locale.ITALY
		 *  rowsPerBatch=1
		 *  batchesPerCommit=1
		 *  #ERROR Insert failed for row  15
		 *  #ERROR HIRE_DATE: GDK-05043: not a valid month
		 *  #ERROR Row 15 data follows:
		 *  15,Den,Raphaely,DRAPHEAL,515.127.4561,94-may-07,PU_MAN,11000,,100,30
		 *  #ERROR Insert failed for row  16
		 *  #ERROR HIRE_DATE: GDK-05043: not a valid month
		 *  #ERROR Row 16 data follows:
		 *  16,Alexander,Khoo,AKHOO,515.127.4562,95-jun-18,PU_CLERK,3100,,114,30
		 *  #INFO Number of rows processed: 14
		 *  #INFO Number of rows in error: 2
		 *  #INFO Elapsed time: 00:00:01.558 - (1,558 ms)
		 *  1 - WARNING: Load processed with errors

		 */
		try {

			_test="Test 7b";
			_filename="test07b.csv";
			_table = "TEST07B";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.dateFormat("rr-mon-dd")
			.locale(Locale.ITALY)
			.batchRows(1)
			.batchesPerCommit(1)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));

		} catch (Exception e){
			e.printStackTrace();
		}
	}

	
	private void test8(){
		/*
		 *  Test 8 - load of timestamp
		 *  time formats in file are db nls setting DD-MON-RR HH.MI.SSXFF AM
		 *  Formats not specifed in load parms
  		 *  Expected Results: Test 8 Completed with exit code: 0 - SUCCESS: Load processed without errors
		 *	#INFO Number of rows processed: 30
		 *	#INFO Number of rows in error: 0
		 */
		try {

			_test="Test 8";
			_filename="test08.csv";
			_table = "TEST08";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test8a(){
		/*
		 *  Test 8a - load of timestamp
		 *  time formats in file are not load default
		 *  Format specifed in load parms YY-MM-DD HH24:MI:SS.FF6
  		 *  Expected Results: Test 8 Completed with exit code: 0 - SUCCESS: Load processed without errors
		 *	#INFO Number of rows processed: 10
		 *	#INFO Number of rows in error: 0
		 */
		try {

			_test="Test 8a";
			_filename="test08a.csv";
			_table = "TEST08A";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.timestampFormat("YY-MM-DD HH24:MI:SS.FF6")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test8b(){
		/*
		 *  Test 8b - load of timestamp
		 *  time formats in file are load default
		 *  Formats specified "YYYY-MM-DD HH24:MI:SS.FF"
  		 *  Expected Results: Test 8 Completed with exit code: 0 - SUCCESS: Load processed without errors
		 *	#INFO Number of rows processed: 10
		 *	#INFO Number of rows in error: 0
		 */
		try {

			_test="Test 8b";
			_filename="test08b.csv";
			_table = "TEST08B";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.timestampFormat("YYYY-MM-DD HH24:MI:SS.FF")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test8c(){
		/*
		 *  Test 8c - load of timestamp
		 *  time formats in file are nls timestamp DD-MM-RR HH.MI.SSXFF AM
		 *  Formats specified "DD-MON-RR HH.MI.SSXFF AM"
   		 *  Expected Results: 
		 *  #ERROR Insert failed for row  1
		 *  #ERROR TS: GDK-05043: not a valid month
		 *  #ERROR Row 1 data follows:
		 *  editor,2013-07-12 14:53:10.214000
		 *  #ERROR Insert failed for row  2
		 *  #ERROR TS: GDK-05043: not a valid month
		 *  #ERROR Row 2 data follows:
		 *  editor2,2013-07-12 19:21:52.983000
		 *  #ERROR Insert failed for row  3
		 *  #ERROR TS: GDK-05043: not a valid month
		 *  #ERROR Row 3 data follows:
		 *  insert,2011-02-16 12:40:13.357591
		 *  #ERROR Insert failed for row  4
		 *  #ERROR TS: GDK-05043: not a valid month
		 *  #ERROR Row 4 data follows:
		 *  insert,2011-02-16 12:40:13.357591
		 *  etc...
  		 *  Expected Results: Test 8 Completed with exit code: 0 - SUCCESS: Load processed without errors
  		 *  #INFO Number of rows processed: 0
  		 *  #INFO Number of rows in error: 10
  		 *  #INFO Elapsed time: 00:00:01.364 - (1,365 ms)
  		 *  1 - WARNING: Load processed with errors
		 */
		try {

			_test="Test 8c";
			_filename="test08c.csv";
			_table = "TEST08C";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.timestampFormat("DD-MON-RR HH.MI.SSXFF AM")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	private void test9(){
		/*
		 *  Test 9 - load of timestamp
		 *  batchMax=2, batchRows=3 so there should only be 2 rows in a batch
		 *  time formats in file are load default
		 *  Formats not specifed in load parms
  		 *  Expected Results: 
  		 *  #ERROR Insert failed for row  1
  		 *  #ERROR TS_TZ: GDK-05043: not a valid month
  		 *  #ERROR Row 1 data follows:
  		 *  insert,2011-02-16 12:40:13.357591 -08:00
  		 *  #INFO Number of rows processed: 9
  		 *  #INFO Number of rows in error: 1
  		 *  #INFO Elapsed time: 00:00:01.415 - (1,415 ms)
  		 *  1 - WARNING: Load processed with errors

		 *
		 */
		try {

			_test="Test 9";
			_filename="test09.csv";
			_table = "TEST09";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.batchRows(3)
			.batchMax(2)
			.batchesPerCommit(1)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test9a(){
		/*
		 *  Test 9a - load of timestamptz
		 *  time formats in file are mixed
		 *  Format specifed in load parms is YY-MM-DD HH24:MI:SS.FF TZR
  		 *  Expected Results: Test 9 Completed with exit code: 1 - WARNING: Load processed with errors
  		 *  #ERROR Insert failed for row  5
  		 *  #ERROR TS_TZ: Unable to format column
  		 *  insert,11-02-16 12:40:13.357591 -08:00
  		 *  #ERROR Insert failed for row  6
  		 *  #ERROR TS_TZ: Unable to format column
  		 *  insert,11-02-16 12:40:13.357591 -08:00
  		 *  #ERROR Insert failed for row  7
  		 *  #ERROR TS_TZ: Unable to format column
  		 *  insert,11-02-16 12:40:13.357591 -08:00
  		 *  #ERROR Insert failed for row  8
  		 *  #ERROR TS_TZ: Unable to format column
  		 *  insert,11-02-16 12:40:13.357591 -08:00
  		 *  #ERROR Insert failed for row  9
  		 *  #ERROR TS_TZ: Unable to format column
  		 *  insert,11-02-16 12:40:13.357591 -08:00
  		 *  #ERROR Insert failed for row  10
  		 *  #ERROR TS_TZ: Unable to format column
  		 *  insert,11-02-16 12:40:13.357591 -08:00
  		 *  4 rows processed and successfully loaded (note batchrows 1, batches per commit 1)
  		 *  6 rows in error.
		 *
		 */
		try {

			_test="Test 9a";
			_filename="test09a.csv";
			_table = "TEST09A";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.timestampTZFormat("YY-MM-DD HH24:MI:SS.FF TZR")
			.batchRows(1)
			.batchesPerCommit(1)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test9b(){
		/*
		 *  Test 9b - load of timestamp
		 *  time formats in file are mixed
		 *  Format specifed in load parms is YYYY-MM-DD HH24:MI:SS.FF TZH:TZM
		 *  Response locals specified as english, french italian
		 *  
		 *  NOTE: This test is currently broken because TZH:TZM parsing is disabled and all rows fail to load.
		 *  
  		 *  Expected Results: Test 9b Completed with exit code: 1 - WARNING: Load processed with errors
  		 *  7 rows processed and successfully loaded (note batchrows 1, batches per commit 1)
  		 *  4 rows in error.
  		 *  Response in English
  		 *  
  		 *  ************************
  		 *  NOTE: THIS TEST IS CURRENTLY BROKEN 
  		 *  	BECAUSE TZH:TZM PARSING IS DISABLED AND ALL ROWS FAIL TO LOAD.
		 *
		 */
		try {

			_test="Test 9b";
			_filename="test09b.csv";
			_table = "TEST09B";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			ArrayList<Locale> prefLocales = new ArrayList<Locale>(3);
			prefLocales.add(Locale.ENGLISH);
			prefLocales.add(Locale.FRENCH);
			prefLocales.add(Locale.ITALIAN);
						
			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.timestampTZFormat("YYYY-MM-DD HH24:MI:SS.FF TZH:TZM")
			.responseLocales(prefLocales)
			.batchRows(1)
			.batchesPerCommit(1)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test9c(){
		/*
		 *  Test 9c - load of timestamptz
		 *  time formats in file are mixed
		 *  Format specifed in load parms is YY-MON-DD HH24:MI:SS.FF TZR
		 *  Locale ITALY
		 *  Response locals specified as french, italian, english
  		 *  Expected Results: Test 9c Completed with exit code: 1 - WARNING: Load processed with errors
		 *	--ERROR--Insert failed for row  13
		 *	--ERROR--TS_TZ: GDK-05043: not a valid month
		 *	insert,2011-jan-16 12:40:13.357591 PST
		 *	--ERROR--Insert failed for row  14
		 *	--ERROR--TS_TZ: GDK-05043: not a valid month
		 *	insert,2011-may-16 12:40:13.357591 PST
		 *	--INFO--Number of rows processed: 12
		 *	--INFO--Number of rows in error: 2
-		 *	-INFO--Elapsed time: 00:00:00.650 - (650 ms)
		 *  1 - WARNING: Load processed with errors

		 *	Response in French
		 */
		try {

			_test="Test 9c";
			_filename="test09c.csv";
			_table = "TEST09C";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			ArrayList<Locale> prefLocales = new ArrayList<Locale>(3);
			prefLocales.add(Locale.FRENCH);
			prefLocales.add(Locale.ITALIAN);
			prefLocales.add(Locale.ENGLISH);
			
			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.responseLocales(prefLocales)
			.timestampTZFormat("YYYY-MON-DD HH24:MI:SS.FF TZR")
			.locale(Locale.ITALY)
			.batchRows(1)
			.batchesPerCommit(1)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test9d(){
		/*
		 *  Test 9d - test of response format SQL using load of timestamptz
		 *  time formats in file are mixed
		 *  Format specifed in load parms is YY-MON-DD HH24:MI:SS.FF TZR
		 *  Locale ITALY
  		 *  Expected Results: Test 9d Completed with exit code: 1 - WARNING: Load processed with errors
		 *	--ERROR--Insert failed for row  13
		 *	--ERROR--TS_TZ: GDK-05043: not a valid month
		 *	insert,2011-jan-16 12:40:13.357591 PST
		 *	--ERROR--Insert failed for row  14
		 *	--ERROR--TS_TZ: GDK-05043: not a valid month
		 *	insert,2011-may-16 12:40:13.357591 PST
		 *	--INFO--Number of rows processed: 12
		 *	--INFO--Number of rows in error: 2
-		 *	-INFO--Elapsed time: 00:00:00.650 - (650 ms)
		 *  1 - WARNING: Load processed with errors

		 *
		 */
		try {

			_test="Test 9d";
			_filename="test09d.csv";
			_table = "TEST09D";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.responseFormat(LoadParmsAPI.RESPONSE_FORMAT.RESPONSE_SQL)
			.timestampTZFormat("YYYY-MON-DD HH24:MI:SS.FF TZR")
			.locale(Locale.ITALY)
			.batchRows(1)
			.batchesPerCommit(1)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void test10(){
		/*
		 *  Test 10 - simple load of csv with no errors in it passed reader and passed loader
		 *  All defaults on reader parm
		 *  Date format dd-mon-rr in data and passed as load parm
		 *  
  		 *  Expected Results: Test 10 Completed with exit code: 0 - SUCCESS: Load processed without errors
 		 *  #INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 *
		 *  The first 10 rows should have non-zero scale
		 *
		 */
		try {

			_test="Test 10";
			_filename="test10.csv";
			_table = "TEST10";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.TRUNCATE)
			.dateFormat("DD-MON-RR")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	private void test10a(){
		/*
		 *  Test 10a - simple load of csv with no errors in it passed reader and passed loader
		 *  All defaults on reader parm
		 *  Date format dd-mon-rr in data and passed as load parm
		 *  
  		 *  Expected Results: #INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 *	#INFO Elapsed time: 00:00:01.906 - (1,906 ms)
		 * 	0 - SUCCESS: Load processed without errors

		 *
		 *  The first 10 rows should have non-zero scale in salary
		 *
		 */
		try {

			_test="Test 10a";
			_filename="test10a.csv";
			_table = "TEST10A";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.TRUE)
			.dateFormat("DD-MON-RR")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}		private void test10b(){
		/*
		 *  Test 10b - simple load of csv with no errors in it passed reader and passed loader
		 *  All defaults on reader parm
		 *  Date format dd-mon-rr in data and passed as load parm
		 *  
  		 *  Expected Results: Test 10b Completed with exit code: 0 - SUCCESS: Load processed without errors
 		 *  #INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 *
		 *  The first 10 rows should have non-zero scale in salary
		 *
		 */
		try {

			_test="Test 10b";
			_filename="test10b.csv";
			_table = "TEST10B";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.TRUNCATE)
			.dateFormat("DD-MON-RR")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	private void test10c(){
		/*
		 *  Test 10c - simple load of csv with no errors in it passed reader and passed loader
		 *  All defaults on reader parm
		 *  Date format dd-mon-rr in data and passed as load parm
		 *  
  		 *  Expected Results: Test 10 C Completed with exit code: 0 - SUCCESS: Load processed without errors
 		 *  #INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 *
		 *  The first 3 rows should be 24500000000000000
		 *  The next 7 rows should have non-zero scale
		 *
		 */
		try {

			_test="Test 10c";
			_filename="test10c.csv";
			_table = "TEST10C";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.build();

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.TRUNCATE)
			.dateFormat("DD-MON-RR")
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	private void test11a(){
		/*
		 *  Test 11A - Load of csv with no errors in it null reader and passed loader
		 *  Virtual column on table which is omitted from the load file.
		 *  
  		 *  Expected Results: Test 11A Completed with exit code: 0 - SUCCESS: Load processed without errors
		 *  #INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 *  0 - SUCCESS: Load processed without errors
		 *
		 */
		try {

			_test="Test 11a";
			_filename="test11a.csv";
			_table = "TEST11A";

			ReadParmsDelimitedAPI delimParms = null;

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.TRUNCATE)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	private void test11b(){
		/*
		 *  Test 11B - Load of csv fails in it null reader and passed loader
		 *  Virtual column on table which is included from the load file.
		 *  
		 *
		 *	#ERROR Column in header row  is not defined for table.
		 *	#INFO Number of rows processed: 0
		 *	#INFO Number of rows in error: 0
		 *	3 - SEVERE: Severe error, processing terminated
		 *
		 */
		try {

			_test="Test 11b";
			_filename="test11b.csv";
			_table = "TEST11B";

			ReadParmsDelimitedAPI delimParms = null;

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.TRUNCATE)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	private void test11c(){
		/*
		 *  Test 11C - Load of csv with no errors in it null reader and passed loader
		 *  Nullable column on table which is not included from the load file.
		 *  
  		 *  Expected Results: Test 11C Completed with exit code: 0 - SUCCESS: Load processed without errors
 		 *  #INFO Number of rows processed: 19
		 *	#INFO Number of rows in error: 0
		 *
		 */
		try {

			_test="Test 11c";
			_filename="test11c.csv";
			_table = "TEST11C";

			ReadParmsDelimitedAPI delimParms = null;

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.TRUNCATE)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	private void test11d(){
		/*
		 *  Test 11D - Load of csv with no errors in it null reader and passed loader
		 *  Not Nullable column on table which is not included from the load file.
		 *  
  		 *  Expected Results: Test 11C Completed with exit code: 0 - SUCCESS: Load processed without errors
 		 *  #INFO Number of rows processed: 107
		 *	#INFO Number of rows in error: 0
		 *
		 *  The first 3 rows should be 24500000000000000
		 *  The next 7 rows should have non-zero scale
		 *
		 */
		try {

			_test="Test 11d";
			_filename="test11d.csv";
			_table = "TEST11D";

			ReadParmsDelimitedAPI delimParms = null;

			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.deleteRows(DELETE_ROWS.TRUNCATE)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	private void clob1(){
		/*
		 *  Test clob1 - load of clob data file
		 *  simple load of dsv with no errors in it passed reader and passed loader
		 *  Reader parms specifies custom delimiter <EOD> and line end terminator <EOR>
		 *  (This test has header file in it, but migration does not normally have one)
		 *  
  		 *  Expected Results: 
  		 *  #INFO Number of rows processed: 24
  		 *	#INFO Number of rows in error: 0
		 *	#INFO Elapsed time: 00:00:11.366 - (11,367 ms)
		 *	0 - SUCCESS: Load processed without errors
		 *
		 *
		 */
		try {

			_test="Clob1";
			_filename="clob_tab.dsv";
			_table = "CLOB_TAB";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.delimiter("<EOD>")
			.lineEnd("<EOR>")
			.enclosures("")
			.header(true)
			.build();
			
			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.batchRows(1)
			.deleteRows(DELETE_ROWS.TRUE)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	private void mig1(){
		/*
		 *  Test mig1 - load of SYBASE file
		 *  simple load of dsv with no errors in it passed reader and passed loader
		 *  Reader parms specifies migration custom delimiter and line end terminator
		 *  (This test has header file in it, but migration does not normally have one)
		 *  
  		 *  Expected Results: 
  		 *  #INFO Number of rows processed: 547
  		 *	#INFO Number of rows in error: 0
		 *	#INFO Elapsed time: 00:00:11.366 - (11,367 ms)
		 *	0 - SUCCESS: Load processed without errors
		 *
		 *
		 */
		try {

			_test="Test mig1";
			_filename="SYB15_SYSCOLUMNS.dat";
			_table = "S_SYB15_SYSCOLUMNS";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.delimiter("<EOFD>")
			.lineEnd("<EORD>\r\n")
			.header(true)
			.build();
			
			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.batchRows(1)
			.deleteRows(DELETE_ROWS.TRUE)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	
	private void mig2(){
		/*
		 *  Test mig2 - load of sqlserver file 
		 *  simple load of dsv with no errors in it passed reader and passed loader
		 *  Reader parms specifies migration custom delimiter and line end terminator
		 *  
  		 *  Expected Results: 
  		 *  #INFO Number of rows processed: 793
		 *	#INFO Number of rows in error: 0
		 *	#INFO Elapsed time: 00:00:52.826 - (52,828 ms)
		 *	0 - SUCCESS: Load processed without errors
		 *
		 */
		try {

			_test="Test mig2";
			_filename="STAGE_SS2K5_COLUMNS.dat";
			_table = "S_SS2K5_COLUMNS";

			ReadParmsDelimitedAPI delimParms = new ReadParmsDelimitedAPI.Builder()
			.delimiter("\t<EOFD>\t")
			.lineEnd("<EORD>\r\n")
			.header(false)
			.build();
			
			LoadParmsAPI loadParms = new LoadParmsAPI.Builder(SERVICE_METHOD.JDBC_BATCH)
			.batchRows(1)
			.deleteRows(DELETE_ROWS.TRUE)
			.build();	

			ExitCode exitCode = doLoad(delimParms, loadParms);
			//System.out.println(_test + " Completed with exit code: " + exitCode.toStringTranslated(loadParms.getResponseLocales()));


		} catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	private ExitCode doLoad(ReadParmsDelimitedAPI readParms, LoadParmsAPI loadParms){

		LogHandler handler = null;
		try{

			_lastURL = new File (_fileDir + _filename).toURI().toURL();
			String fileName=_lastURL.getFile();
			int dot = fileName.lastIndexOf(".");

			String responseName = fileName.substring(0,dot)+".rsp";
			String logName = fileName.substring(0,dot)+".log";
			File responseFile = new File(responseName);
			File logFile = new File(logName);

			if (responseFile.isFile()){
				responseFile.delete();
			}
			if (logFile.isFile()){
				logFile.delete();
			}

			_input = new FileInputStream(new File(fileName));
			_output = new FileOutputStream(new File(responseName));

			_log = new FileOutputStream(new File(logName));
			if (_log != null){
				//handler = LogHandler.getInstance();
				handler = new LogHandler();
				handler.setLogFile(_log);
			}
			Logger.getLogger(getClass().getName()).log(Level.INFO,"starting load service: " + _test);

			LoadAPI loader = new LoadAPI.Builder(_conn,_schema,_table,_input,_output)
			.loadParms(loadParms)
			.readParms(readParms)
			.logger(Logger.getLogger(getClass().getName()))
			.build();


			return loader.load();
		} catch (IllegalStateException ise){
			// standard error from constructing/starting load service 
			//System.out.println(ise.getMessage());
			return ExitCode.EXIT_SEVERE;
		} catch (Exception e){
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getStackTrace()[ 0 ].toString(), e);
			return ExitCode.EXIT_SEVERE;
		} finally {
			try {
				_input.close();
				_output.close();
			} catch (Exception e) {  
				Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
			}

			try {	
				if (handler!=null){
					handler.close();
				}	

			} catch (Exception e) {  
				e.printStackTrace();			}

		}
	}
  
	/*
	 * Launcher for predefined tests.  This assumes certain test files and schema.
	 */
	public void launchTests() {
		
		// setup for launching tests
		// First thought is all the tests will use the same connection, schema and table(?), but this is not required
		// The load files need to be in the file directory for the tests.
		// The response and log files will be saved in the same directory as fileName.rsp and fileName.log
		try {
			_conn = DriverManager.getConnection(TEST_CONNECTION, TEST_S,
			TEST_P);
			_fileDir = "/scratch/jscapicc/MyData/load/";
			_schema = "HR_LOAD";
			//_schema = "HR";
			//_schema = "JMS";
			//_schema="C##JMS"; //jdbc:oracle:thin:@gbr30025.uk.oracle.com:1523/DB12CR1C
			_table = "EMPLOYEES";
			
			/*testV1();
			testV2();
			testV3();
			testV4();
			testV5();
			test1();
			test2();
			test2a();
			test3();
			test3a();
			test4();
			test5();
			test5a();
			test6(); 
			test6a();
			 
			test7();
			  
			test7a();
			test7b();
			test8();
			test8a();
			test8b();
			test8c();
			test9();
			test9a();
			test9b();
			test9c();
			test9d();
			test10();
			test10a(); 
			test10b();
			test10c();*/
			test11a();
			test11b(); 
			test11c();
			test11d();
			/*clob1();
			mig1();
			mig2();*/
			
		
		// Can't get the connection	
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			try {
			if (_conn != null){
				_conn.close();
			}	
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		
	}
}
