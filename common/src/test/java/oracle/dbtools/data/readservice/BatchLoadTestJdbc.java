/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.data.test;

/*

To run this test, you need to create a table as below:

  CREATE TABLE "HR_LOAD".test_jdbc
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   );

This test shows getUpdateCount issue against 11.1 and 11.2 databases.
The test runs successfully against 12.1 database.

Run using 12.1 ojdbc 

*/

import oracle.jdbc.*;
import oracle.sql.DATE;

import java.sql.*;

public class BatchLoadTestJdbc {
	/** Set these variables appropriately for the test environment  */
	//private static String TEST_CONNECTION ="jdbc:oracle:thin:@localhost:1521/yourmachine.yourdomain";
	//private static String TEST_S="hr_load";
	//private static String TEST_P="hr_load";
	//private static String _table="TEST_JDBC";
	private static String TEST_CONNECTION ="jdbc:oracle:thin:@gbr30060.uk.oracle.com:1523/DB11GR24"; 
	private static String TEST_S="hr_load";
	private static String TEST_P="hr_load";
	private static String _table="TEST_JDBC";

	/**
	 * Dummy main for testing purposes.
	 * Runs the test for getUpdateCount anomalies
	 * 
	 * 1.  Run the sql above to create the table 
	 * 2.  Set the value of TEST_CONNECTION to the connection string
	 * 3.  Create user hr_load/hr_load or set values for TEST_S and TEST_P as user/pass
	 * 4.  Run the test and examine the output.
	 * 5.  Query the table.
	 * 
	 * We attempt to load 4 rows.  The Third row has a data size error that causes a Batch Update Exception.
	 * The updateCounts indicate that 1 row was loaded, when in fact 2 rows were loaded.
	 * 		
	 */
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());

		String url =     TEST_CONNECTION;

		Connection conn =
		DriverManager.getConnection(url,TEST_S,TEST_P);
		

		conn.setAutoCommit(false);

		String sql = "Insert into " + _table + " (EMPLOYEE_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE_NUMBER,HIRE_DATE,JOB_ID,SALARY,COMMISSION_PCT,MANAGER_ID,DEPARTMENT_ID) values (?,?,?,?,?,?,?,?,?,?,?)";

		OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement(sql);
		
		    addRow(stmt, "100.0","Steven","King","SKING","515.123.4567",new DATE("1987-06-17 00:00:00"),"AD_PRES","24000.0","NULL","NULL","90.0");
		    addRow(stmt, "101.0","Neena","Kochhar","NKOCHHAR","515.123.4568",new DATE("1989-09-21 00:00:00"),"AD_VP","17000.0","NULL","100.0","90.0");
		    addRow(stmt, "1020000.0","Lex","De Haan","LDEHAAN","515.123.4569",new DATE("1993-01-13 00:00:00"),"AD_VP","17000.0","NULL","100.0","90.0");
		    addRow(stmt, "103.0","Alexander","Hunold","AHUNOLD","590.423.4567",new DATE("1990-01-03 00:00:00"),"IT_PROG","9000.0","NULL","102.0","60.0");
						            

			try {
			    stmt.executeBatch();;
			    System.out.println("Batch inserted successcully");
			    System.out.println (stmt.getUpdateCount());
			} catch(BatchUpdateException e) {
			    System.out.println ("Batch Update Exception");
			    System.out.println (e.getMessage());
			    System.out.println (stmt.getUpdateCount());
			    int[] updateCounts = e.getUpdateCounts();
			    for (Integer uc: updateCounts){
			    	System.out.println(uc.toString());
			    }
			}
			       			        
			conn.commit();
			stmt.close();
		
	}	

		

private static void addRow(OraclePreparedStatement stmt,  
		String value1,String value2, String value3,
		String value4, String value5, DATE date,
		String value6, String value7, String value8,
		String value9, String value10){
		
		int col = 0;
	
		setString(stmt, ++col, value1);
		setString(stmt, ++col, value2);
		setString(stmt, ++col, value3);
		setString(stmt, ++col, value4);
		setString(stmt, ++col, value5);
		
		setDate(stmt, ++col, date);
		
		setString(stmt, ++col, value6);
		setString(stmt, ++col, value7);
		setString(stmt, ++col, value8);
		setString(stmt, ++col, value9);
		setString(stmt, ++col, value10);
	
	try {
		stmt.addBatch();
	} catch (Exception e){
		// Pick up the final error for the row.
		System.out.println("Add Batch Exception: " + value1 + " " + e.getMessage());
	}
}

private static void setString(OraclePreparedStatement stmt, int i, String value){
	try{
	if (value != null && value.length()>0 && !value.equals("NULL")){
		stmt.setString(i, value);
	}
	else {
		stmt.setNull(i,java.sql.Types.NUMERIC);
	}
	} catch (Exception e){
		System.out.println("Set String Exception: " + value + " " + e.getMessage());
	}
}

private static void setDate(OraclePreparedStatement stmt, int i, DATE date){
	try{
	if (date != null){
		stmt.setObject(i, date, OracleTypes.DATE);
	}
	else {
		stmt.setNull(i,OracleTypes.DATE);
	}
	} catch (Exception e){
		System.out.println("Set String Exception: " + date.stringValue() + " " + e.getMessage());
	}
}

}
