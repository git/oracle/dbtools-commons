--------------------------------------------------------
--  File created - Wednesday-April-30-2014   
--------------------------------------------------------

--------------------------------------------------------
--  DDL for Table CLOB_TAB
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."CLOB_TAB" 
   (	"COLUMN1" VARCHAR2(20 BYTE), 
	"COLUMN2" CLOB
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" 
 LOB ("COLUMN2") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)) ;

--------------------------------------------------------
--  DDL for Table TEST01
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST01" 
   (	"DEPARTMENT_ID" NUMBER(4,0), 
	"DEPARTMENT_NAME" VARCHAR2(30 BYTE), 
	"MANAGER_ID" NUMBER(6,0), 
	"LOCATION_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST02
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST02" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
   --------------------------------------------------------
--  DDL for Table TEST02A
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST02A" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "EXAMPLE" ;

--------------------------------------------------------
--  DDL for Table TEST03
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST03" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST03A
--------------------------------------------------------

  CREATE TABLE "TEST03A" 
   (	"EMPLOYEE_ID"VARCHAR2(22 BYTE),
	"FIRST_NAME" VARCHAR2(22 BYTE), 
	"LAST_NAME" VARCHAR2(27 BYTE), 
	"EMAIL" VARCHAR2(27 BYTE), 
	"PHONE_NUMBER" VARCHAR2(22 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(12 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "EXAMPLE" ;

--------------------------------------------------------
--  DDL for Table TEST04
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST04" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST05
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST05" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST05A
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST05A" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST06
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST06" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST06A
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST06A" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST07
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST07" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST07A
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST07A" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST07B
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST07B" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST08
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST08" 
   (	"VC2" VARCHAR2(60 BYTE), 
	"TS" TIMESTAMP (6) DEFAULT NULL
   ) ;
--------------------------------------------------------
--  DDL for Table TEST08A
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST08A" 
   (	"VC2" VARCHAR2(60 BYTE), 
	"TS" TIMESTAMP (6) DEFAULT NULL
   ) ;
--------------------------------------------------------
--  DDL for Table TEST08B
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST08B" 
   (	"VC2" VARCHAR2(60 BYTE), 
	"TS" TIMESTAMP (6) DEFAULT NULL
   ) ;
--------------------------------------------------------
--  DDL for Table TEST08C
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST08C" 
   (	"VC2" VARCHAR2(60 BYTE), 
	"TS" TIMESTAMP (6) DEFAULT NULL
   ) ;
--------------------------------------------------------
--  DDL for Table TEST09
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST09" 
   (	"VC2" VARCHAR2(60 BYTE), 
	"TS_TZ" TIMESTAMP (6) WITH TIME ZONE
   ) ;
--------------------------------------------------------
--  DDL for Table TEST09A
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST09A" 
   (	"VC2" VARCHAR2(60 BYTE), 
	"TS_TZ" TIMESTAMP (6) WITH TIME ZONE
   ) ;
--------------------------------------------------------
--  DDL for Table TEST09B
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST09B" 
   (	"VC2" VARCHAR2(60 BYTE), 
	"TS_TZ" TIMESTAMP (6) WITH TIME ZONE
   ) ;
--------------------------------------------------------
--  DDL for Table TEST09C
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST09C" 
   (	"VC2" VARCHAR2(60 BYTE), 
	"TS_TZ" TIMESTAMP (6) WITH TIME ZONE
   ) ;
--------------------------------------------------------
--  DDL for Table TEST09D
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST09D" 
   (	"VC2" VARCHAR2(60 BYTE), 
	"TS_TZ" TIMESTAMP (6) WITH TIME ZONE
   ) ;
--------------------------------------------------------
--  DDL for Table TEST10
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST10" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST10A
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST10A" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(38,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST10B
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST10B" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(8,2), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" LONG, 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST10C
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."TEST10C" 
   (	"EMPLOYEE_ID" NUMBER(6,0), 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(25 BYTE), 
	"PHONE_NUMBER" VARCHAR2(20 BYTE), 
	"HIRE_DATE" DATE, 
	"JOB_ID" VARCHAR2(10 BYTE), 
	"SALARY" FLOAT(126), 
	"COMMISSION_PCT" NUMBER(2,2), 
	"MANAGER_ID" NUMBER(6,0), 
	"DEPARTMENT_ID" NUMBER(4,0)
   ) ;

		--------------------------------------------------------
		--  DDL for Table TEST11A
		--------------------------------------------------------

		  CREATE TABLE "HR_LOAD"."TEST11A" 
		   (	"EMPLOYEE_ID" NUMBER(6,0), 
			"FIRST_NAME" VARCHAR2(20 BYTE), 
			"LAST_NAME" VARCHAR2(25 BYTE), 
			"EMAIL" VARCHAR2(25 BYTE), 
			"PHONE_NUMBER" VARCHAR2(20 BYTE), 
			"HIRE_DATE" DATE, 
			"JOB_ID" VARCHAR2(10 BYTE), 
			"SALARY" NUMBER(8,2),
			"SALARY_COMM" NUMBER GENERATED ALWAYS AS (ROUND(salary*(10/100),2)) VIRTUAL,
			"COMMISSION_PCT" NUMBER(2,2), 
			"MANAGER_ID" NUMBER(6,0), 
			"DEPARTMENT_ID" NUMBER(4,0)
		   ) ;
		--------------------------------------------------------
		--  DDL for Table TEST11B
		--------------------------------------------------------

		  CREATE TABLE "HR_LOAD"."TEST11B" 
		   (	"EMPLOYEE_ID" NUMBER(6,0), 
			"FIRST_NAME" VARCHAR2(20 BYTE), 
			"LAST_NAME" VARCHAR2(25 BYTE), 
			"EMAIL" VARCHAR2(25 BYTE), 
			"PHONE_NUMBER" VARCHAR2(20 BYTE), 
			"HIRE_DATE" DATE, 
			"JOB_ID" VARCHAR2(10 BYTE), 
			"SALARY" NUMBER(8,2), 
			"SALARY_COMM" NUMBER GENERATED ALWAYS AS (ROUND(salary*(10/100),2)) VIRTUAL,
			"COMMISSION_PCT" NUMBER(2,2), 
			"MANAGER_ID" NUMBER(38,0), 
			"DEPARTMENT_ID" NUMBER(4,0)
		   ) ;
		--------------------------------------------------------
		--  DDL for Table TEST11C
		--------------------------------------------------------

		  CREATE TABLE "HR_LOAD"."TEST11C" 
		   (	"EMPLOYEE_ID" NUMBER(6,0), 
			"FIRST_NAME" VARCHAR2(20 BYTE), 
			"LAST_NAME" VARCHAR2(25 BYTE), 
			"EMAIL" VARCHAR2(25 BYTE), 
			"PHONE_NUMBER" VARCHAR2(20 BYTE), 
			"HIRE_DATE" DATE, 
			"JOB_ID" VARCHAR2(10 BYTE), 
			"SALARY" NUMBER(8,2), 
			"COMMISSION_PCT" NUMBER(2,2), 
			"MANAGER_ID" LONG, 
			"DEPARTMENT_ID" NUMBER(4,0)
		   ) ;
		--------------------------------------------------------
		--  DDL for Table TEST11D
		--------------------------------------------------------

		  CREATE TABLE "HR_LOAD"."TEST11D" 
		   (	"EMPLOYEE_ID" NUMBER(6,0) NOT NULL, 
			"FIRST_NAME" VARCHAR2(20 BYTE), 
			"LAST_NAME" VARCHAR2(25 BYTE), 
			"EMAIL" VARCHAR2(25 BYTE), 
			"PHONE_NUMBER" VARCHAR2(20 BYTE), 
			"HIRE_DATE" DATE, 
			"JOB_ID" VARCHAR2(10 BYTE), 
			"SALARY" FLOAT(126), 
			"COMMISSION_PCT" NUMBER(2,2), 
			"MANAGER_ID" NUMBER(6,0), 
			"DEPARTMENT_ID" NUMBER(4,0)
		   ) ;		
--------------------------------------------------------
--  DDL for Table VTEST05
--------------------------------------------------------

  CREATE TABLE "HR_LOAD"."VTEST05" 
   (	"ABC" VARCHAR2(20 BYTE), 
	"DEF" VARCHAR2(20 BYTE), 
	"XXX" VARCHAR2(20 BYTE)
   ) ;
--------------------------------------------------------
--SQL SErver 2005 stage table for the file STAGE_SS2K5_COLMNS.dat
--------------------------------------------------------
CREATE TABLE "HR_LOAD"."S_SS2K5_COLUMNS"
  (
    "DB_NAME_FK"            VARCHAR2(128 CHAR),
    "OBJECT_ID_"            NUMBER(10,0),
    "NAME"                  VARCHAR2(128 CHAR),
    "COLUMN_ID"             NUMBER(10,0),
    "SYSTEM_TYPE_ID"        NUMBER(10,0),
    "USER_TYPE_ID"          NUMBER(10,0),
    "MAX_LENGTH"            NUMBER(10,0),
    "PRECISION"             NUMBER(10,0),
    "SCALE"                 NUMBER(10,0),
    "COLLATION_NAME"        VARCHAR2(128 CHAR),
    "IS_NULLABLE"           NUMBER(10,0),
    "IS_ANSI_PADDED"        NUMBER(10,0),
    "IS_ROWGUIDCOL"         NUMBER(10,0),
    "IS_IDENTITY"           NUMBER(10,0),
    "IS_COMPUTED"           NUMBER(10,0),
    "IS_FILESTREAM"         NUMBER(10,0),
    "IS_REPLICATED"         NUMBER(10,0),
    "IS_NON_SQL_SUBSCRIBED" NUMBER(10,0),
    "IS_MERGE_PUBLISHED"    NUMBER(10,0),
    "IS_DTS_REPLICATED"     NUMBER(10,0),
    "IS_XML_DOCUMENT"       NUMBER(10,0),
    "XML_COLLECTION_ID"     NUMBER(10,0),
    "DEFAULT_OBJECT_ID"     NUMBER(10,0),
    "RULE_OBJECT_ID"        NUMBER(10,0),
    "IS_SPARSE"             NUMBER(10,0),
    "IS_COLUMN_SET"         NUMBER(10,0)
  ) ;

--------------------------------------------------------  
--Sybase stage table for the file SYB15_SYSCOLUMNS.data
--------------------------------------------------------
  CREATE TABLE "HR_LOAD".S_SYB15_SYSCOLUMNS
(
    ID          NUMBER(10),
	DB_NUMBER   NUMBER(10),
	COLID       NUMBER(10),
	STATUS      NUMBER(10),
	DB_TYPE     NUMBER(10),
	LENGTH      NUMBER(10),
	OFFSET      NUMBER(10),
	USERTYPE    NUMBER(10),
	CDEFAULT    NUMBER(10),
	DOMAIN      NUMBER(10),
	NAME        VARCHAR2(255),
	PRINTFMT    VARCHAR2(255),
	PREC        NUMBER(10),
	SCALE       NUMBER(10),
	REMOTE_TYPE NUMBER(10),
	REMOTE_NAME VARCHAR2(255),
	XSTATUS     NUMBER(10),
	XTYPE       NUMBER(10),
	XDBID       NUMBER(10),
	ACESSRULE   NUMBER(10),
	STATUS2     NUMBER(10),
  STATUS3     NUMBER(10),
	COMPUTEDCOL NUMBER(10),
	ENCRTYPE    NUMBER(10),
	ENCRLEN     NUMBER(10),
	ENCRKEYID   NUMBER(10),
	ENCRKEYDB   VARCHAR2(100),
	ENCRDATE    VARCHAR2(100),
	LOBCOMP_LVL NUMBER,
	INROWLEN    NUMBER
);
