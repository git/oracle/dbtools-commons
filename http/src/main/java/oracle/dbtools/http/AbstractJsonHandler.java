/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.util.Logger;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.entity.ContentType;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;

/**
 * ResponseHandler implementation skeleton for use with JSON typed entities
 * @author jmcginni
 *
 * @param <V>
 */
public abstract class AbstractJsonHandler<V> extends AbstractResponseHandler<V> {

	private static class Holder {
		private static final ObjectMapper mapper;
		private static final JsonFactory factory;
		static {
			factory = new JsonFactory();
			mapper = new ObjectMapper(factory);
		}
	}

	public static ObjectMapper getObjectMapper() {
		return Holder.mapper;
	}

	public static JsonParser createParser(Reader r) throws JsonParseException, IOException {
		return Holder.factory.createParser(r);
	}

	private String _entityTag;

	public AbstractJsonHandler() {
		super();
	}

	@Override
	protected final V handleSuccess(HttpResponse resp)
			throws ClientProtocolException, IOException {

		ContentType type = ContentType.getOrDefault(resp.getEntity());
		if ( ModelUtil.areDifferent(ContentType.APPLICATION_JSON.getMimeType(), type.getMimeType())) {
			throw new IOException("Unexpected Content-Type: " + type); //$NON-NLS-1$
		}

		// Get the entity tag (eTag) if it exists
		Header h = resp.getFirstHeader("ETag"); //$NON-NLS-1$
		if (h != null) {
			_entityTag = h.getValue();
		}

		// Now that we've validated the Content-Type, get the HTTP entity and build
		// our JSON document from it
		HttpEntity entity = resp.getEntity();
		Reader r = null;
		try {
			Charset cs = type.getCharset();
			if ( cs == null ) {
				// If either no charset was specified or the one listed is bogus default to UTF-8
				try {
					cs = Charset.forName("UTF-8"); //$NON-NLS-1$
				} catch ( UnsupportedCharsetException e) {
					// Bad voodoo if UTF-8 isn't supported...
					Logger.warn(AbstractJsonHandler.class, "Unsupported charset: UTF-8!"); //$NON-NLS-1$
					cs = Charset.defaultCharset();
				}
			}
			r = new BufferedReader(new InputStreamReader(entity.getContent(), cs));

			return build(r);
		} finally {
			if ( r != null ) {
				r.close();
			}
		}
	}

	@Override
	protected final IOException createInternalServerError(String errorMessage, String errorText,
			String errorTextType) {
		return new JSONProtocolException(errorMessage, errorText, errorTextType);
	}

	@Override
	protected final V handleError(HttpResponse resp) throws ClientProtocolException,
	IOException {
		report500Error(resp);
		return null;
	}

	protected abstract V build(Reader reader) throws IOException;

	protected final void throwJSONFormatException(String expectedValue, String actualValue)
			throws JSONProtocolException {
		throw new JSONProtocolException(expectedValue, actualValue);
	}

	protected void checkJSONElement(Object o, String propName)
			throws JSONProtocolException {
		if ( o == null ) {
			throw new JSONProtocolException(propName);
		}
	}
	
	protected void checkJsonNode(JsonNode node, String propName) throws JSONProtocolException {
		if ( node == null || node.isMissingNode() ) {
			throw new JSONProtocolException(propName);
		}
	}
	
	protected void checkJsonNode(JsonNode node, JsonNodeType type, String propName) throws JSONProtocolException {
		if ( node == null || (node.getNodeType() != type) ) {
			throw new JSONProtocolException(propName);
		}
	}
	
	protected String getAsString(JsonNode node, String fieldName) throws JSONProtocolException {
		JsonNode value = node.path(fieldName);
		if ( value.isMissingNode() ) {
			// If the element is completely missing, just return null;
			return null;
		}
		checkJsonNode(value, JsonNodeType.STRING, fieldName);
		return value.asText();
	}
	
	protected final String getEntityTag() {
		return _entityTag;
	}

}