/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http;

import java.io.IOException;

import oracle.dbtools.util.StreamCopy;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;

/**
 * A {@link ResponseHandler} for handling responses from a cloud or listener connection.
 * @author jmcginni
 *
 * @param <T>
 */
public abstract class AbstractResponseHandler<T> implements ResponseHandler<T> {

	@Override
	public final T handleResponse(HttpResponse resp) throws ClientProtocolException,
			IOException {
        T result = null;
        // Get the status line and check it
        StatusLine status = resp.getStatusLine();
        int code = status.getStatusCode();
        switch (code) {
            case 200:
                // OK
                result = handleSuccess(resp);
                break;
            case 500:
            	// Internal Server error - format a response for this
            	report500Error(resp);
            default:
            	// let our subclasses handle all the other cases
            	result = handleError(resp);
            	
        }
        return result;
	}
	
	protected final void report500Error(HttpResponse resp) throws IOException {
        StatusLine status = resp.getStatusLine();
        Header contentType = resp.getEntity().getContentType();
        // Create an internal server error based on the status line text
        // and and detail message 
    	throw createInternalServerError(status.toString(), StreamCopy.string(Client.createReader(resp)), contentType.getValue());
	}

	/**
	 * Handle the success case (200 OK)
	 * @param resp the HTTP response
	 * @return the result of the request
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	protected abstract T handleSuccess(HttpResponse resp) throws ClientProtocolException, IOException;
	
	/**
	 * Create an error exception for an internal server error (500).
	 * @param errorMessage the error message
	 * @param errorText the detail text
	 * @param errorTextType the MIME type of the detaul text
	 * @return
	 */
	protected abstract IOException createInternalServerError(String errorMessage, String errorText, String errorTextType);
	
	/**
	 * Handle any case other than 200 or 500
	 * @param resp 
	 * @return 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	protected abstract T handleError(HttpResponse resp) throws ClientProtocolException, IOException;
}
