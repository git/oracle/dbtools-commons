/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.ProxySelector;

import oracle.dbtools.http.auth.AuthenticationEngine;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.conn.SystemDefaultRoutePlanner;

public abstract class Client {
    private SessionType m_type;
    private CloseableHttpClient m_client;
    private AuthenticationEngine m_engine;

    protected Client(SessionType type) {
        m_type = type;
        init();
    }
    
    private void init() {
    	// We want to configure a default socket timeout
    	SocketConfig socketConfig = SocketConfig.custom().setSoTimeout(60000).build();
    	
    	// Create the HttpClientConnectionManager
    	PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager();
    	connMgr.setDefaultSocketConfig(socketConfig);
    	connMgr.setDefaultMaxPerRoute(10);
    	
    	// configure the client
    	HttpClientBuilder builder = HttpClients.custom();
    	builder.setConnectionManager(connMgr);
    	builder.setUserAgent(getUserAgent());
    	
    	// configure the proxy
    	initProxy();
    	HttpRoutePlanner routePlanner = new SystemDefaultRoutePlanner(ProxySelector.getDefault());
    	builder.setRoutePlanner(routePlanner);
    	
    	// build the client
    	m_client = builder.build();
    }
    
    protected void initProxy() {}
    
    /**
     * Generate the default user agent string. It should contain the program name and version, OS name and version, 
     * OS architecture, Java version, IDE GUID, and potentially additional custom information.
     * @return
     */
    protected abstract String getUserAgent();

	public AuthenticationEngine getAuthenticationEngine() {
		if ( m_engine == null ) {
			m_engine = createAuthenticationEngine();
            m_engine.init(this);
		}
		return m_engine;
	}
	
	protected final void shutdownClient() {
        m_engine.init(this);
	}
    
    protected abstract  AuthenticationEngine createAuthenticationEngine();

	public CloseableHttpResponse executeRequest(HttpUriRequest request) throws ClientProtocolException, IOException {
        return m_client.execute(request);
    }

    public <T> T executeRequest(HttpUriRequest request, ResponseHandler<? extends T> handler) throws ClientProtocolException, IOException {
        return m_client.execute(request, handler);
    }


    public final SessionType getConnectionType() {
        return m_type;
    }
    
    public static Reader createReader(HttpResponse resp) {
        HttpEntity entity = resp.getEntity();
        Header enc = entity.getContentEncoding();
        try {
            return new BufferedReader(new InputStreamReader(entity.getContent()));
        } catch (Exception e) {
            return new StringReader(""); //$NON-NLS-1$
        }
    }
}
