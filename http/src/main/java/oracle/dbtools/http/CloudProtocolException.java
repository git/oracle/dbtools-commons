/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http;

import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;

/**
 * Base exception for reporting protocol errors with cloud connections
 * @author jmcginni
 *
 */
public abstract class CloudProtocolException extends ClientProtocolException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private StatusLine m_status;
	private String m_additionalDetails;
	private String m_detailType;
	
	/**
	 * Constructs a default CloudProtocolException.
	 */
	protected CloudProtocolException() {
		
	}

	/**
	 * Creates a CloudProtocolException with a specific message.
	 * @param message the exception message
	 */
	protected CloudProtocolException(String message) {
		super(message);
	}

	/**
	 * Creates a CloudProtocolException that wraps another exception.
	 * @param cause the root cause of the exception
	 */
	protected CloudProtocolException(Throwable cause) {
		super(cause);
	}

	/**
	 * Creates a CloudProtocolException with a detailed message.
	 * @param message the exception message
	 * @param additionalDetails additional details about the exception
	 * @param detailsType the MIME type of the detail message
	 */
    protected CloudProtocolException(String message, String additionalDetails, String detailsType) {
        this(message);
        m_additionalDetails = additionalDetails;
        m_detailType = detailsType;
    }
    
    /**
     * Creates a new CloudProtocolException based on an HTTP status line.
     * @param status the HTTP status line
     */
    protected CloudProtocolException(StatusLine status) {
        super(status.toString());
        m_status = status;
    }
    
    /**
     * Retrieves the HTTP status line associated with this exception if there is one.
     * @return the HTTP status line, or <code>null</code> if there was not one associated with the exception.
     */
	public StatusLine getStatusLine() {
	    return m_status;
	}

	/**
	 * Retrieves the additional details message associated with this exception. 
	 * 
	 * @return the additional details message, or <code>null</code> if there was not one associated with the exception.
	 */
	public String getAdditionalDetails() {
	    return m_additionalDetails;
	}

	/**
	 * Retrieves the MIME type of the additional details message associated with this exception. 
	 * 
	 * @return the MIME type of the additional details message, or <code>null</code> if there was not one associated with the exception.
	 */
	public String getDetailsType() {
	    return m_detailType;
	}

	@Override
	public String getMessage() {
		Throwable cause = getCause();
		return cause != null ? cause.getMessage() : super.getMessage();
	}

}