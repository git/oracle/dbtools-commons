/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http;


/**
 * A {@link CloudProtocolException} related to JSON errors encountered.
 * @author jmcginni
 *
 */
@SuppressWarnings("serial")
class JSONProtocolException extends CloudProtocolException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a new JSONProtocolException based on an error message, with additional details of a
	 * specified MIME type
	 * @param message the error message to report
	 * @param additionalDetails any additional details about the error
	 * @param detailsType the MIME type of the additional details
	 */
	JSONProtocolException(String message, String additionalDetails, String detailsType) {
		super(message, additionalDetails, detailsType);
	}
	
	/**
	 * Creates a new JSONProtocolException indicating a mismatch between an expected and actual JSON element.
	 * @param expected the JSON element expected
	 * @param found the actual JSON element found
	 */
	JSONProtocolException(String expected, String found) {
		super(HttpResources.format(HttpResources.MESSAGE_INVALID_JSON, expected, found));
	}
	
	/**
	 * Creates a new JSONProtocolException indicating a missing JSON element.
	 * @param missing the expected JSON element
	 */
	JSONProtocolException(String missing) {
		super(HttpResources.format(HttpResources.MESSAGE_MISSING_JSON_ELEMENT, missing));
	}
}
