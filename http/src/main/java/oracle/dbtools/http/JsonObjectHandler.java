/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http;

import java.io.IOException;
import java.io.Reader;

import org.apache.http.client.ResponseHandler;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * {@link ResponseHandler} implementation for dealing with a single JSON object in a response.
 * 
 * @author jmcginni
 *
 */
public abstract class JsonObjectHandler<V> extends AbstractJsonHandler<V> {
    /* (non-Javadoc)
     * @see oracle.dbtools.http.AbstractJsonHandler#build(java.io.Reader)
     */
    @Override
    protected final V build(Reader reader) throws IOException {
        try {
            return build(getObjectMapper().readTree(reader));
        } catch ( JSONProtocolException e) {
            // Convert any exceptions to IOExceptions
            Throwable t = e.getCause();
            if ( t instanceof IOException ) {
                throw ( IOException ) t;
            } else {
                throw t != null ? new IOException(t) : new IOException(e);
            }
        }
    }

    /**
     * Build an Object from a JSON Object
     * @param obj the JsonObject representing the JSON structure
     * @return the built object
     * @throws JsonException if an error is encountered with the JSON object or other processing issue.
     */
    protected abstract V build(JsonNode obj) throws IOException;
}
