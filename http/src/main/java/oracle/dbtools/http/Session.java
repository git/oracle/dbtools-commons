/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.locks.ReentrantLock;

import oracle.dbtools.http.auth.Authentication;
import oracle.dbtools.http.auth.AuthenticationEngine;
import oracle.dbtools.http.auth.AuthenticationFailedException;
import oracle.dbtools.util.Logger;
import oracle.dbtools.util.StreamCopy;

import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicHeader;

/**
 * A representation of a connection to the Oracle cloud. A Connection defines a root URI and a module ID with which it is associated. All requests
 * made to the connection will be made to locations within the root and module. A Connection manages an authentication state as well. The authentication
 * state defines credentials that will be used with every request.
 * <p>
 * A Connection exists in one of two states: OPEN or CLOSED. A Connection must be in the OPEN state for it to be used. Once a Connection has moved from the OPEN state to the
 * CLOSED state, it cannot be used. CLOSED connections cannot be reopened.
 * <p>
 * @author jmcginni
 *
 */
public class Session {
    private static final String AUTH_HEADER = "Authorization"; //$NON-NLS-1$
    private static final class ExpiredTokenException extends AuthenticationFailedException {
        ExpiredTokenException() {
            super("Expired authentication token"); //$NON-NLS-1$
        }
    }

    private static class ResponseHandlerWrapper<V> implements ResponseHandler<V> {
        private final ResponseHandler<V> m_handler;
        
        ResponseHandlerWrapper(ResponseHandler<V> handler) {
            m_handler = handler;
        }
        @Override
        public V handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
            StatusLine line = response.getStatusLine();
            // Check the status
            int code = line.getStatusCode();
            Logger.info(Session.class, "HTTP Response code: " + code); //$NON-NLS-1$
            switch ( code ) {
                case 401:
                    // Check the headers
                    Header contentType = response.getFirstHeader("Content-Type"); //$NON-NLS-1$
                    if ( contentType != null ) {
                        // We need a content type of "application/json" if we are getting an invalid token response
                    }
                    String data = StreamCopy.string(Client.createReader(response));
                    System.out.println(data);
                    // Authentiation failed problem
                    throw new AuthenticationFailedException(line);
                default:
                    // let the handler decide how to handle it
            }
            
            return m_handler.handleResponse(response);
        }
        
    }
    private static final String PATH_SEPARATOR = "/"; //$NON-NLS-1$

    private static enum State {OPEN, CLOSED};
    private SessionInfo m_info;
    private URI m_root;
    private transient State m_state = State.CLOSED;
    private Authentication m_auth;
    private transient ReentrantLock m_lock;
    private URI m_baseURI;
    
    private Client m_client;
    
    private SessionCloseHandler mCloseHandler;
    
    Session(SessionInfo info, Client client, Authentication auth) {
        m_info = info;
        m_root = info.getServerRoot();
        m_auth = auth;
        m_client = client;
        m_lock = new ReentrantLock();
        m_baseURI = info.getServiceRoot();
    }
    
    public URI getRoot() {
        return m_root;
    }
    
    public URI getDefaultServiceURI() {
        return m_baseURI;
    }
    
    public boolean isOpen() {
        return m_state == State.OPEN;
    }
    
    void setCloseHandler(SessionCloseHandler h) {
    	mCloseHandler = h;
    }
    
    void open() throws SessionException {
        if ( m_state == State.OPEN ) {
            return;
        }
        m_lock.lock();
        try {
            // Ideally, some sort of connection test should happen here to make sure the authentication is correct and the service URI works.
//            // check out authentication
//            executeQueryImpl("tables/", new ResponseHandlerWrapper<Void>(new ResponseHandler<Void>() { //$NON-NLS-1$
//
//                @Override
//                public Void handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
//                    StatusLine statusLine = response.getStatusLine();
//                    int code = statusLine.getStatusCode();
//                    if ( code != 200 ) {
//                        // OK
//                        throw new IOException(statusLine.getReasonPhrase());
//                    }
//                    return null;
//                }
//            }));
            
            // mark ourselves as open
            m_state = State.OPEN;
        } finally {
            m_lock.unlock();            
        }
    }
    
    public void close() {
        if (isOpen()) {
            m_lock.lock();
            try {
                m_auth = null;
                m_state = State.CLOSED;
            } finally {
                m_lock.unlock();
            }
        }
    }
    
    private static void signRequest(HttpRequest request, Authentication auth) {
        // First, remove any existing Authorization header
        request.removeHeaders(AUTH_HEADER);

        String authToken = auth.getToken();
        Header header = new BasicHeader(AUTH_HEADER, "Bearer " + authToken); //$NON-NLS-1$
        request.addHeader(header);
    }
    
    /**
     * Executes a GET to retrieve a resource from the cloud. The HTTP response is passed to a provided
     * handler for processing.
     * @param <V> the formatted resource type
     * @param path the relative URI to the resource
     * @param handler a {#link ResponseHandler} that will format the response and handle any errors.
     * @return the formatted response
     * @throws SessionException
     */
    public <V> V executeQuery(String path, ResponseHandler<V> handler) throws SessionException {
        if ( !isOpen() ) {
            throw new SessionException("connection closed"); //$NON-NLS-1$
        }
        
        return executeQueryImpl(path, handler);
    }
    
    /**
     * Executes a GET to retrieve a resource. The HTTP response is passed to a provided handler for processing.
     * @param <V> the formatted resource type
     * @param uri the URI to the resource
     * @param handler a {#link ResponseHandler} that will format the response and handle any errors.
     * @return the formatted response
     * @throws SessionException
     * @throws SessionException
     */
    public <V> V executeQuery(URI uri, ResponseHandler<V> handler) throws SessionException {
        if ( !isOpen() ) {
            throw new SessionException("connection closed"); //$NON-NLS-1$
        }
        HttpGet request = new HttpGet(uri);
        return executeImpl(handler, request);
    }

    private <V> V executeQueryImpl(String path, ResponseHandler<V> handler) throws SessionException {
        HttpGet request = new HttpGet(getURI(path));
        return executeImpl(handler, request);
    }
    
    /**
     * Executes a GET to retrieve a resource from the cloud. 
     * @param <V> the formatted resource type
     * @param path the relative URI to the resource
     * @return the HTTP response
     * @throws SessionException
     */
    public CloseableHttpResponse executeQuery(String path) throws SessionException {
        if ( !isOpen() ) {
            throw new SessionException("connection closed"); //$NON-NLS-1$
        }
        
        return executeQueryImpl(path);
    }
    
    /**
     * Executes a GET to retrieve a resource.
     * @param <V> the formatted resource type
     * @param uri the URI to the resource
     * @return the HTTP response
     * @throws SessionException
     */
    public CloseableHttpResponse executeQuery(URI uri) throws SessionException {
        if ( !isOpen() ) {
            throw new SessionException("connection closed"); //$NON-NLS-1$
        }
        HttpGet request = new HttpGet(uri);
        return executeImpl(request);
    }

    private CloseableHttpResponse executeQueryImpl(String path) throws SessionException {
        HttpGet request = new HttpGet(getURI(path));
        return executeImpl(request);
    }
    
    private <V> V executeImpl(ResponseHandler<V> handler, HttpUriRequest request) throws SessionException {
        signRequest(request, m_auth);
        try {
            try {
                Logger.info(Session.class, "HTTP Request: " + request.getMethod() + "; URI: " + request.getURI());  //$NON-NLS-1$//$NON-NLS-2$
                return m_client.executeRequest(request, new ResponseHandlerWrapper<V>(handler));
            } catch (AuthenticationFailedException e) {
                StatusLine line = e.getStatusLine();
                int code = line.getStatusCode();
                switch (code) {
                    case 401:
                        // try to reauthenticate
                        AuthenticationEngine engine = m_client.getAuthenticationEngine();
                        try {
                            Authentication newAuth = null;
                            try {
                                newAuth = engine.reauthenticate(m_auth, m_info);
                            } catch (AuthenticationFailedException afe ) {
                                // If the reauthenticate fails, try to prompt the user I guess.
                                // On failure, fall through
                                newAuth = engine.authenticate(m_info);
                            }
                            if ( newAuth != null ) {
                                m_auth = newAuth;
                                signRequest(request, m_auth);
                                V result = m_client.executeRequest(request, new ResponseHandlerWrapper<V>(handler));
                                return result;
                            }
                        } catch ( AuthenticationFailedException afe) {
                            // If we fail to authenticate, we need to close the connection
                            mCloseHandler.connectionClosed(m_info.getName());
                            
                            Throwable cause = afe.getCause();
                            if ( cause instanceof IOException ) {
                               throw (IOException) cause;
                            } else if ( cause instanceof ClientProtocolException ) {
                                throw (ClientProtocolException) cause;
                            }
                            throw afe;
                        }
                        return null;
                    default:
                        throw e;
                }
            }
        } catch (ClientProtocolException e) {
            Logger.warn(Session.class, e);
            throw new SessionException(e);
        } catch (IOException e) {
            Logger.warn(Session.class, e);
            throw new SessionException(e);
        }
    }
    
    private CloseableHttpResponse executeImpl(HttpUriRequest request) throws SessionException {
    	try {
    		CloseableHttpResponse response = null;
    		try {
    			response = executeImplCheckAuthorization(request);
    		} catch (AuthenticationFailedException e ) {
                StatusLine line = e.getStatusLine();
                int code = line.getStatusCode();
                switch (code) {
                    case 401:
                        // try to reauthenticate
                        AuthenticationEngine engine = m_client.getAuthenticationEngine();
                        try {
                            Authentication newAuth = null;
                            try {
                                newAuth = engine.reauthenticate(m_auth, m_info);
                            } catch (AuthenticationFailedException afe ) {
                                // If the reauthenticate fails, try to prompt the user I guess.
                                // On failure, fall through
                                newAuth = engine.authenticate(m_info);
                            }
                            if ( newAuth != null ) {
                                m_auth = newAuth;
                                signRequest(request, m_auth);
                                response = executeImplCheckAuthorization(request); 
                            }
                        } catch ( AuthenticationFailedException afe) {
                            // If we fail to authenticate, we need to close the connection
                            mCloseHandler.connectionClosed(m_info.getName());
                            
                            Throwable cause = afe.getCause();
                            if ( cause instanceof IOException ) {
                               throw (IOException) cause;
                            } else if ( cause instanceof ClientProtocolException ) {
                                throw (ClientProtocolException) cause;
                            }
                            throw afe;
                        }
                    default:
                        throw e;
                }
    		}
    		return response;
		} catch (ClientProtocolException e) {
            Logger.warn(Session.class, e);
            throw new SessionException(e);
		} catch (IOException e) {
            Logger.warn(Session.class, e);
            throw new SessionException(e);
		}
    }
    
    private CloseableHttpResponse executeImplCheckAuthorization(HttpUriRequest request) throws ClientProtocolException, IOException, AuthenticationFailedException {
    	signRequest(request, m_auth);
        Logger.info(Session.class, "HTTP Request: " + request.getMethod() + "; URI: " + request.getURI());  //$NON-NLS-1$//$NON-NLS-2$
        CloseableHttpResponse result = m_client.executeRequest(request);
		// Check to see if we got a 401
		StatusLine line = result.getStatusLine();
		int code = line.getStatusCode();
        Logger.info(Session.class, "HTTP Response code: " + code); //$NON-NLS-1$
        if ( code == 401 ) {
        	// If authentication failed, close the response and return the error
        	try {
        		result.close();
        	} catch ( IOException io ) {
        		Logger.warn(Session.class, io);
        	}
    		throw new AuthenticationFailedException(line);
        }
        return result;
    }
    
    /**
     * Executes a POST to retrieve a resource from the cloud. The HTTP response is passed to a provided
     * handler for processing.
     * @param <V> the formatted resource type
     * @param post the POST request
     * @param handler a {@link ResponseHandler} that will format the response and handle any errors.
     * @return the formatted response
     * @throws SessionException
     */
    public <V> V executeRequest(HttpUriRequest post, ResponseHandler<V> handler) throws SessionException {
        if ( !isOpen() ) {
            throw new SessionException("connection closed"); //$NON-NLS-1$
        }
        
        return executeImpl(handler, post);
    }
    
    /**
     * Executes an arbitrary HTTP Request.
     * @param request the HTTP request being performed
     * @return the HTTP Response
     * @throws SessionException
     */
    public CloseableHttpResponse executeRequest(HttpUriRequest request) throws SessionException {
        if ( !isOpen() ) {
            throw new SessionException("connection closed"); //$NON-NLS-1$
        }
        
        return executeImpl(request);
    }
    
    /**
     * Creates a POST request to the relative URI. After setting the entity on the POST, 
     * {@link executeRequest} can be used to send the request to the server.
     * @param path the relative URI
     * @return the POST request
     */
    public HttpPost createPost(String path) {
        URI uri = getURI(path);
        return createPost(uri);
    }
    
    /**
     * Creates a POST request to the specified URI. After setting the entity on the POST, 
     * {@link executeRequest} can be used to send the request to the server.
     * @param uri the URI for the resource
     * @return the POST request
     */
    public HttpPost createPost(URI uri) {
        return new HttpPost(uri);
    }
    
    /**
     * Creates a PUT request to the specified URI.
     * @param uri the URI for the resource.
     * @return the PUT request
     */
    public HttpPut createPut(URI uri) {
    	return new HttpPut(uri);
    }
    
    /**
     * Creates a PUT request to the relative path.
     * @param String the relative path.
     * @return the PUT request
     */
    public HttpPut createPut(String path) {
    	URI uri = getURI(path);
    	return createPut(uri);
    }
    
    /**
     * Creates a DELETE request for the specified URI.
     * @param uri the URI for resource.
     * @return the DELETE request
     */
    public HttpDelete createDelete(URI uri) {
        return new HttpDelete(uri);
    }
    
    /**
     * Creates a DELETE request for a relative path
     * @param path the relative path
     * @return the DELETE request
     */
    public HttpDelete createDelete(String path) {
        URI uri = getURI(path);
        return createDelete(uri);
    }

    private URI getURI(String path) {
        String realPath = path.startsWith(PATH_SEPARATOR) ? path.substring(1) : path;
    	return m_baseURI.resolve(realPath);
    }
}
