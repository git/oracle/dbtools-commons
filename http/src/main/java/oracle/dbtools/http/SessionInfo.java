/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http;

import java.net.URI;
import java.util.Arrays;

import oracle.dbtools.common.utils.ModelUtil;

/**
 * A descriptor for cloud connections.
 * <p>
 * @author jmcginni
 * @since 3.1
 */
public final class SessionInfo {
	/**
	 * A descriptor for the SFTP information associated with a cloud connection.
	 * @author jmcginni
	 *
	 */
	public final static class SftpInfo {
		public static final int UNSPECIFIED_PORT_VALUE = -1;
		
		private String m_ftpUser;
		private char[] m_ftpPass;
		private String m_ftpHost;
		private int m_ftpPort;
		
		public SftpInfo(String ftpUser, char[] ftpPass, String ftpHost, int ftpPort) {
			m_ftpUser = ftpUser;
			m_ftpPass = ftpPass;
			m_ftpHost = ftpHost;
			m_ftpPort = ftpPort;
		}
		
		public String getSftpUser() {
			return m_ftpUser;
		}
		
		public char[] getSftpPass() {
			return m_ftpPass;
		}
		
		public String getSftpHost() {
			return m_ftpHost;
		}
		
		public int getSftpPort() {
			return m_ftpPort;
		}

		@Override
		public int hashCode() {
			return toString().hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return obj == this || 
					( (obj instanceof SftpInfo ) &&
							ModelUtil.areEqual(m_ftpHost, ((SftpInfo)obj).m_ftpHost) &&
							(m_ftpPort == ((SftpInfo)obj).m_ftpPort) &&
							ModelUtil.areEqual(m_ftpUser, ((SftpInfo)obj).m_ftpUser) &&
							ModelUtil.areEqual(m_ftpPass, ((SftpInfo)obj).m_ftpPass));
		}

	    @SuppressWarnings("nls")
		@Override
		public String toString() {
			return m_ftpUser + "#" + m_ftpHost + "#" + m_ftpPort;
		}
		
		
	}
	
    /**
     * A descriptor for the OSS information associated with a cloud connection.
     * @author jmcginni
     *
     */
    public final static class OssInfo {
        
        private boolean m_ossEnabled;
        private String m_ossServiceName;
        private String m_ossIdentityDomain;
        private String m_ossUserName;
        private char[] m_ossPassword;
        private String m_ossServiceUrl;
        
        public OssInfo(boolean ossEnabled, String ossServiceName, String ossIdentityDomain, String ossUserName, char[] ossPassword, String ossServiceUrl) {
            m_ossEnabled = ossEnabled;
            m_ossServiceName = ossServiceName;
            m_ossIdentityDomain = ossIdentityDomain;
            m_ossUserName = ossUserName;
            // Bug 24827824 - CLOUD CONNECTION: ENABLE OSS OPTION ALWAYS ENABLED
            // need to be able to create 'empty' one for enabled property
            if (ossPassword != null) {
                m_ossPassword = Arrays.copyOf(ossPassword, ossPassword.length);
            }
            m_ossServiceUrl = ossServiceUrl;
        }
        
        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        public boolean isOssEnabled() {
            return m_ossEnabled;
        }

        public void setOssEnabled(boolean ossEnabled) {
            this.m_ossEnabled = ossEnabled;
        }

        public String getOssServiceName() {
            return m_ossServiceName;
        }

        public void setOssServiceName(String ossServiceName) {
            this.m_ossServiceName = ossServiceName;
        }

        public String getOssIdentityDomain() {
            return m_ossIdentityDomain;
        }

        public void setOssIdentityDomain(String ossIdentityDomain) {
            this.m_ossIdentityDomain = ossIdentityDomain;
        }

        public String getOssUserName() {
            return m_ossUserName;
        }

        public void setOssUserName(String ossUserName) {
            this.m_ossUserName = ossUserName;
        }

        public char[] getOssPassword() {
            return m_ossPassword;
        }

        public void setOssPassword(char[] ossPassword) {
            this.m_ossPassword = ossPassword;
        }

        public String getOssServiceUrl() {
            return m_ossServiceUrl;
        }

        public void setOssServiceUrl(String m_ossServiceUrl) {
            this.m_ossServiceUrl = m_ossServiceUrl;
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this || 
                    ( (obj instanceof OssInfo ) &&
                            m_ossEnabled == ((OssInfo)obj).m_ossEnabled &&
                            ModelUtil.areEqual(m_ossServiceName, ((OssInfo)obj).m_ossServiceName) &&
                            ModelUtil.areEqual(m_ossIdentityDomain, ((OssInfo)obj).m_ossIdentityDomain) &&
                            ModelUtil.areEqual(m_ossUserName, ((OssInfo)obj).m_ossUserName) &&
                            ModelUtil.areEqual(m_ossPassword, ((OssInfo)obj).m_ossPassword) &&
                            ModelUtil.areEqual(m_ossServiceUrl, ((OssInfo)obj).m_ossServiceUrl)
                    );
        }

        @SuppressWarnings("nls")
        @Override
        public String toString() {
            return m_ossIdentityDomain + "#" + m_ossUserName + "#" + m_ossServiceName + "@" + m_ossServiceUrl;
        }
        
        
    }
    
    private final String m_name;
    private final URI m_root;
    private final String m_user;
    private final char[] m_pwd;
    private final URI m_serviceRoot;
    private final SftpInfo m_sftpInfo;
    private final OssInfo m_ossInfo;
    
    public SessionInfo(String name, URI root, URI serviceRoot, String username, char[] pwd) {
    	this(name, root, serviceRoot, username, pwd, null);
    }
    
    public SessionInfo(String name, URI root, URI serviceRoot, String username, char[] pwd, SftpInfo sftpInfo) {
        this(name, root, serviceRoot, username, pwd, sftpInfo, null);
    }

    public SessionInfo(String name, URI root, URI serviceRoot, String username, char[] pwd, SftpInfo sftpInfo, OssInfo ossInfo) {
        m_name = name;
        m_root = root;
        m_serviceRoot = serviceRoot;
        m_user = username;
        m_pwd = pwd;
    	m_sftpInfo = sftpInfo;
    	m_ossInfo = ossInfo;
    }
    
    /**
     * Retrieves the name of the connection. The connection name uniquely identifies 
     * the connection.
     * @return the name of the connection
     */
    public String getName() {
        return m_name;
    }

    /**
     * Retrieves the root of the connection. The connection root is a URI under which all the
     * connection resources can be found.
     * @return the URI root 
     */
    public URI getServerRoot() {
        return m_root;
    }
    
    /**
     * Retrieves the service root for the connection. The service root is a URI under which all the 
     * SQL Developer specific resources can be found.
     * @return the URI for the service root
     */
    public URI getServiceRoot() {
        return m_serviceRoot;
    }
    
    /** Retrieves the username of the connection.
     * 
     * @return the username
     */
    public String getUsername() {
        return m_user;
    }
    
    public char[] getPassword() {
    	return m_pwd;
    }
    
    /**
     * Retrieves the SFTP information associated with this connection.
     * @return The SftpInfo 
     */
    public SftpInfo getSftpInfo() {
    	return m_sftpInfo;
    }

    /**
     * Retrieves the OSS information associated with this connection.
     * @return The OssInfo 
     */
    public OssInfo getOssInfo() {
        return m_ossInfo;
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj == this || 
        ( ( obj instanceof SessionInfo ) &&
        ModelUtil.areEqual(m_name, ((SessionInfo) obj).m_name) &&
        ModelUtil.areEqual(m_root, ((SessionInfo) obj).m_root) &&
        ModelUtil.areEqual(m_serviceRoot, ((SessionInfo) obj).m_serviceRoot) &&
        ModelUtil.areEqual(m_user, ((SessionInfo) obj).m_user) &&
        ModelUtil.areEqual(m_pwd, ((SessionInfo) obj).m_pwd) &&
        ModelUtil.areEqual(m_sftpInfo, ((SessionInfo) obj).m_sftpInfo) &&
        ModelUtil.areEqual(m_ossInfo, ((SessionInfo) obj).m_ossInfo));
    }

    @SuppressWarnings("nls")
    @Override
    public String toString() {
        return m_name + "#" + m_root + "#" + m_serviceRoot + "#"+ m_user + "#" + m_sftpInfo != null ? m_sftpInfo.toString() : "" + m_ossInfo != null ? m_ossInfo.toString() : "";
    }
}
