/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http;

import java.util.HashMap;
import java.util.Map;

/**
 * Abstract ConnectionManager for HTTP based connections.
 * @author jmcginni
 *
 */
public abstract class SessionManager implements SessionCloseHandler {

	protected class C {
	        private SessionInfo m_info;
	        private Session m_conn;
	        
	        C(SessionInfo info) {
	            m_info = info;
	        }
	        
	        String getName() {
	            return m_info.getName();
	        }
	        
	        SessionInfo getInfo() {
	            return m_info;
	        }
	        
	        Session getConnection(boolean force) throws SessionException {
	            synchronized(this) {
	                if (m_conn == null && force) {
	                    SessionCreator creator = SessionCreator.createSessionCreator(SessionManager.this);
	                    m_conn = creator.createSession(m_info);
	                    m_conn.setCloseHandler(SessionManager.this);
	                }
	            }
	            
	            return m_conn;
	        }
	        
	        boolean close() {
	            if ( m_conn != null ) {
	                m_conn.close();
	                m_conn = null;
	            }
	            // Currently, closing a connection cannot fail. But we may listeners that allow the vetoing a connection close
	            return true;
	        }
	    }

	private SessionType m_type;
	private Map<String, C> m_conns = new HashMap<String, C>();
	private Client mClient;

	protected SessionManager(SessionType type) {
		m_type = type;
	}
	
	protected SessionType getConnectionType() {
		return m_type;
	}
	
	public synchronized Client getClient() {
		if ( mClient == null ) {
			mClient = createClient();
		}
		
		return mClient;
	}
	
	protected abstract Client createClient();

	/**
	 * @return
	 */
	public abstract String[] listConnections();

	/**
	 * @param name
	 * @return
	 */
	public abstract SessionInfo getConnectionInfo(String name);

	/**
	 * @param name
	 * @param info
	 * @throws SessionException
	 */
	public abstract void addConnection(String name, SessionInfo info)
			throws SessionException;

	/**
	 * @param name
	 * @param info
	 * @throws SessionException
	 */
	public final void modifyConnection(String name, SessionInfo info)
			throws SessionException {
		// First close the connection
		closeConnection(name);
		
		// Let the implementation update the definition
		modifyConnectionImpl(name, info);
	}

	protected abstract void modifyConnectionImpl(String name, SessionInfo info);

	/**
	 * @param name
	 */
	public final void removeConnection(String name) {
		// First close the connection
		closeConnection(name);
	    
		// Now let the implementation remove it from the connection store
		removeConnectionImpl(name);
	}
	
	protected abstract void removeConnectionImpl(String name);
	
	protected abstract void notifyConnectionClosed(String cName);
	
	protected abstract void notifyConnectionOpened(String cName);

	/**
	 * @param conn
	 * @return
	 */
	protected boolean closeConnection(C conn) {
	    if ( conn.close() ) {
	    	notifyConnectionClosed(conn.getName());
	        return true;
	    }
	    return false;
	}
	
	/**
	 * @param name
	 * @param force
	 * @return
	 * @throws SessionException
	 */
	public Session getConnection(String name, boolean force)
			throws SessionException {
			    C conn = m_conns.get(name);
			    if ( conn == null ) {
			    	SessionInfo info = getConnectionInfo(name);
			    	if ( info != null ) {
			        	conn = new C(info);
			        	m_conns.put(name, conn);
			    	}
			    }
			    
			    if ( conn != null ) {
			        boolean wasClosed = conn.m_conn == null;
			        Session cc = conn.getConnection(force);
			        boolean nowOpen = conn.m_conn != null;
			        if (wasClosed && nowOpen) {
			        	notifyConnectionOpened(conn.getName());
			        }
			        return cc;
			    }
			    return null;
			}

	/**
	 * @param name
	 * @return
	 */
	public boolean closeConnection(String name) {
		// Unopened and non-existent connections are always closeable
		boolean result = true;
	    C conn = m_conns.get(name);
	    if ( conn != null ) {
	    	// If the connection is open, try to close it
	    	result = closeConnection(conn);
	    	m_conns.remove(name);
	    }
	    return result;
	}

	public void connectionClosed(String name) {
		// Connection was closed due to an error - make sure to clean it up
		C conn = m_conns.remove(name);
		if ( conn != null ) {
			closeConnection(conn);
		}
	}
}