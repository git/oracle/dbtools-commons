/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http;

import java.net.URI;

import oracle.dbtools.http.auth.AuthenticationFailedException;
import oracle.dbtools.util.URIUtils;

import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.auth.BasicScheme;

/**
 * Specification of a specific type of HTTP connection.
 * 
 * @author jmcginni
 * @since 3.2.2
 *
 */
public abstract class SessionType {
    
    // Relative path to the authentication server
    private static final String AUTH_PATH = "oauth2/token"; //$NON-NLS-1$
    
    /**
     * Retrieve the name of the connection type.
     * @return the type name
     */
    public abstract String getType();
    
    /**
     * Retrieve the Client ID. The Client ID is used to identify SQL Developer to the server as the originator of any HTTP requests.
     * @return the client ID.
     */
    protected abstract String getClientID();
    
    /**
     * Retrieve the Client Secret. The Client Secret is a key to authenticates SQL Developer as a client.
     * @return the client secret.
     */
    protected abstract String getClientSecret();
    
    /**
     * Retrieve the URI used to authenticate the user with the server, based on a specific set of connection details. Different connection types may utilize different URIs
     * for authentication.
     * @param info the connection details.
     * @return the URI used to authenticate the user.
     */
    public URI getAuthenticationURI(SessionInfo info) {
        URI root = info.getServerRoot();
        return URIUtils.newURI(root, getDefaultAuthenticationPath());
    }
    
    /**
     * Retrieve the default path for Authentication.
     * @return
     */
    protected final String getDefaultAuthenticationPath() {
        return AUTH_PATH;
    }
    
    /**
     * Sign an HTTP request with the client credentials.
     * @param request the HTTP request needing signing
     * @throws AuthenticationFailedException
     */
    public final void signRequest(HttpRequest request) throws AuthenticationFailedException {
        Credentials creds = new UsernamePasswordCredentials(getClientID(), getClientSecret());
        BasicScheme scheme = new BasicScheme();
        Header header;
        try {
            header = scheme.authenticate(creds, request);
        } catch (AuthenticationException e) {
            throw new AuthenticationFailedException(e);
        }
        request.addHeader(header);
    }
    
    @Override
    public final int hashCode() {
        return super.hashCode();
    }

    @Override
    public final boolean equals(Object obj) {
        return obj == this;
    }

    @Override
    public String toString() {
        return getType();
    }

    /**
     * Whether connections of this type allow persisting their passwords.
     * @return <code>true</code> if passwords for connections of this type are allowed to be persisted, <code>false</code> otherwise
     */
    public boolean canSavePasswords() {
    	return false;
    }

}
