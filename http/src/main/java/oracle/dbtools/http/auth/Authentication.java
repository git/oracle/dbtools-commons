/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http.auth;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Authentication {
    public static final String TOKEN_KEY = "access_token"; //$NON-NLS-1$
    public  static final String TOKEN_TYPE_KEY = "token_type"; //$NON-NLS-1$
    public  static final String REFRESH_KEY = "refresh_token"; //$NON-NLS-1$
    
    private Map<String, String> m_props = new HashMap<String, String>();
    
    public void setTokenType(String tokenType) {
        m_props.put(TOKEN_TYPE_KEY, tokenType);
    }
    public String getTokenType() {
        return m_props.get(TOKEN_TYPE_KEY);
    }
    public void setToken(String token) {
        m_props.put(TOKEN_KEY, token);
    }
    public String getToken() {
        return m_props.get(TOKEN_KEY);
    }
    public void setRefreshToken(String refreshToken) {
        m_props.put(REFRESH_KEY, refreshToken);
    }
    public String getRefreshToken() {
        return m_props.get(REFRESH_KEY);
    }
    public String getValue(String propName) {
        return m_props.get(propName);
    }
    public void putValue(String propName, String value) {
        m_props.put(propName, value);
    }
    public Set<String> getPropertyNames() {
        return m_props.keySet();
    }
}
