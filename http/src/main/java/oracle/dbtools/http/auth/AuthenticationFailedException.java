/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http.auth;

import oracle.dbtools.http.CloudProtocolException;

import org.apache.http.StatusLine;

/**
 * {@link CloudProtocolException} for indicating that authentication failed.
 * @author jmcginni
 *
 */
public class AuthenticationFailedException extends CloudProtocolException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Create a new AuthenticationFailedException based of a particular message.
	 * @param message the failure message to report.
	 */
    public AuthenticationFailedException(String message) {
        super(message);
    }
    
    /**
     * Create a new AuthenticationFailedException with both a failure message and detail message.
     * @param message the failure message to report
     * @param additionalDetails a text string with additional details about the failure
     */
    public AuthenticationFailedException(String message, String additionalDetails) {
        this(message, additionalDetails, "text"); //$NON-NLS-1$
    }

    /**
     * Create a new AuthenticationFailedException with both a failure message and a detail 
     * message. The detail message MIME type can be specified.
     * @param message the failure message to report
     * @param additionalDetails a string with additional details about the failure
     * @param detailsType the MIME type of the additional details string
     */
    public AuthenticationFailedException(String message, String additionalDetails, String detailsType) {
    	super(message, additionalDetails, detailsType);
    }
    
    /**
     * Create a new AuthenticationFailedException based on an HTTP status line.
     * @param status the HTTP status line
     */
    public AuthenticationFailedException(StatusLine status) {
        super(status);
    }
    
    /**
     * Create a new AuthenticationFailedException wrapping a root cause exception
     * @param t the root cause of the failure
     */
    public AuthenticationFailedException(Throwable t) {
        super(t);
    }
    
    /**
     * Whether this exception represents the user canceling the authentication
     * @return whether the user canceled the authentication
     */
    public boolean isCanceled() {
        return false;
    }
}
