/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http.auth.basic;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import oracle.dbtools.http.SessionInfo;
import oracle.dbtools.http.SessionType;
import oracle.dbtools.http.Client;
import oracle.dbtools.http.HttpResources;
import oracle.dbtools.http.auth.Authentication;
import oracle.dbtools.http.auth.AuthenticationEngine;
import oracle.dbtools.http.auth.AuthenticationFailedException;
import oracle.dbtools.util.Logger;

//import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.params.HttpParams;

/**
 * Implementation of {@link AuthenticationEngine} for use with Basic authentication.
 * @author jmcginni
 *
 */
public abstract class BasicAuthenticationEngine implements AuthenticationEngine {
    private static final String GRANT_TYPE = "grant_type"; //$NON-NLS-1$
    private static final String USERNAME = "username"; //$NON-NLS-1$
    private static final String PASSWORD = "password"; //$NON-NLS-1$
    private static final String ENCODING_UTF8 = "UTF-8"; //$NON-NLS-1$

    private Client m_client;
    
    protected BasicAuthenticationEngine() {
    }
    
    public final void init(Client client) {
        m_client = client;
    }
    
    @Override
    public Authentication authenticate(SessionInfo conn) throws AuthenticationFailedException {
        // First, prompt the user for the password
        
        // Create the post action
        HttpPost post = createPost(conn);
        
        // Get the username/pwd
        HttpEntity content = promptForAuthentication(conn);
        post.setEntity(content);
        
        
        // Now, execute the post
        return doPost(post);
    }

    /**
     * Create the HTTP POST message for the authentication request
     * @param conn the connection information
     * @return the POST message
     * @throws AuthenticationFailedException
     */
    private HttpPost createPost(SessionInfo conn) throws AuthenticationFailedException {
        // Get the Authentication URI from the connection type
        SessionType type = m_client.getConnectionType();
        URI path = type.getAuthenticationURI(conn);
        
        HttpPost post = new HttpPost(path);
        // sign the post
        signRequest(post);
        return post;
    }

    @Override
    public Authentication reauthenticate(Authentication auth, SessionInfo conn) throws AuthenticationFailedException {
        HttpPost post = createPost( conn);
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(GRANT_TYPE, Authentication.REFRESH_KEY));
        params.add(new BasicNameValuePair(Authentication.REFRESH_KEY, auth.getRefreshToken()));
        try {
            post.setEntity(new UrlEncodedFormEntity(params, ENCODING_UTF8));
        } catch (UnsupportedEncodingException e) {
            throw new AuthenticationFailedException(e);
        }

        // Now, execute the post
        return doPost(post);
    }

    /**
     * Prompt the user for the authentication information.
     * @param conn the connection info
     * @return an HTTP entity encoding the authentication details provided by the user
     * @throws AuthenticationFailedException
     */
    protected abstract HttpEntity promptForAuthentication(final SessionInfo conn) throws AuthenticationFailedException;

    /**
     * Sign the request with the client id and password.
     * @param request the HTTP request needing to be signed
     * @throws AuthenticationFailedException
     */
    private void signRequest(HttpRequest request) throws AuthenticationFailedException {
        SessionType type = m_client.getConnectionType();
        type.signRequest(request);
    }
    
    /**
     * Execute a POST to get the authentication token.
     * @param post the POST request
     * @return the resulting Authentication details
     * @throws AuthenticationFailedException
     */
    private Authentication doPost(HttpPost post) throws AuthenticationFailedException {
    	// Log the request line
    	StringBuilder sb = new StringBuilder(post.getRequestLine().toString());
    	
    	// Log the URI
    	sb.append('\n').append(post.getURI().toString());
    	
//    	// Log the Headers
//    	Header[] headers = post.getAllHeaders();
//    	if ( headers != null ) {
//    		for ( Header h : headers ) {
//    			sb.append('\n').append(h.toString());
//    		}
//    	}
//    	
//    	// Log the entity contents
//    	HttpEntity entity = post.getEntity();
//    	if ( URLEncodedUtils.isEncoded(entity) ) {
//    		try {
//    			List<NameValuePair> l = URLEncodedUtils.parse(entity);
//    			sb.append('\n').append("Encoded entity contents: " + URLEncodedUtils.format(l, ENCODING_UTF8));
//    		} catch ( Exception e) {
//    			// do nothing
//    		}
//    	}
    	
    	log(sb.toString());
    	
    	// Execute the post
    	try {
    		return m_client.executeRequest(post, new BasicAuthenticationHandler());
        } catch ( AuthenticationFailedException e ) {
            throw e;
        } catch (ClientProtocolException e) {
            throw new AuthenticationFailedException(e);
        } catch (UnknownHostException e) {
        	// Give a better message for this
        	throw new AuthenticationFailedException(HttpResources.format(HttpResources.MESSAGE_UNKNOWN_HOST, e.getMessage()));
        } catch (IOException e) {
            throw new AuthenticationFailedException(e);
        }
    }
    
    private void log(String s) {
    	Logger.fine(AuthenticationEngine.class, s);
    }

	protected final HttpEntity createEntity(String username, String pwdString)
			throws AuthenticationFailedException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(GRANT_TYPE, PASSWORD));
		params.add(new BasicNameValuePair(USERNAME, username));
		params.add(new BasicNameValuePair(PASSWORD, pwdString));
		try {
		    return new UrlEncodedFormEntity(params, ENCODING_UTF8);
		} catch (UnsupportedEncodingException e) {
		    throw new AuthenticationFailedException(e);
		}
	}
	
	protected final Client getClient() {
		return m_client;
	}

	protected HttpEntity authenticateHeadless(SessionInfo conn)
			throws AuthenticationFailedException {
				String connName = conn.getName();
				String username = conn.getUsername();
				// Make sure the username is specified
				if ( username == null ) {
					throw new AuthenticationFailedException(HttpResources.format(HttpResources.ERROR_NO_USERNAME, connName));
				}
			
				char[] pwd = conn.getPassword();
				
				// Make sure the password has been registered
				if ( pwd == null || pwd.length == 0 ) {
					throw new AuthenticationFailedException(HttpResources.format(HttpResources.ERROR_NO_PWD, connName));
				}
				return createEntity(username, new String(pwd));
			}
}
