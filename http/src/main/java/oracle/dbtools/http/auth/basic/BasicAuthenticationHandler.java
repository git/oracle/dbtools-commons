/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http.auth.basic;

import java.io.IOException;
import java.io.Reader;
import java.text.MessageFormat;

import oracle.dbtools.http.AbstractJsonHandler;
import oracle.dbtools.http.AbstractResponseHandler;
import oracle.dbtools.http.Client;
import oracle.dbtools.http.auth.Authentication;
import oracle.dbtools.http.auth.AuthenticationEngine;
import oracle.dbtools.http.auth.AuthenticationFailedException;
import oracle.dbtools.util.Logger;
import oracle.dbtools.util.Pair;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BasicAuthenticationHandler extends AbstractResponseHandler<Authentication> {
    private static final String RESPONSE_ERROR_DESCRIPTION = "error_description"; //$NON-NLS-1$
    private static final String RESPONSE_ERROR = "error"; //$NON-NLS-1$
//    private static final String MESSAGE = "Expected token {0}, encountered {1}"; //$NON-NLS-1$
//    private static final String MESSAGE2 = "Unexpected token {0} encountered"; //$NON-NLS-1$
    
    /**
     * Parse out the basic error code and error description from an HttpResponse.
     * @param resp
     * @return
     */
    private static Pair<String, String> parseErrorString(HttpResponse resp) {
        String error = null;
        String message = null;
        Reader r = null;
        try {
            r = Client.createReader(resp);
            ObjectMapper mapper = AbstractJsonHandler.getObjectMapper();
        	JsonNode node = mapper.readTree(r);
        	error = node.path(RESPONSE_ERROR).asText();
        	message = node.path(RESPONSE_ERROR_DESCRIPTION).asText();
        } catch ( IOException e ) {
            Logger.severe(AuthenticationEngine.class, e);
        } finally {
            if ( r != null ) {
                try {
                    r.close();
                } catch (IOException e) {
                }
            }
        }
        
        Logger.warn(BasicAuthenticationHandler.class, MessageFormat.format("{0}: {1}", error, message)); //$NON-NLS-1$
        
        return new Pair<String, String>(error, message);
    }
    
    private Authentication parseAuthentication(HttpResponse resp) throws IOException {
        Authentication auth = new Authentication();
        Reader r = null;
        try {
            r = Client.createReader(resp);
            ObjectMapper mapper = AbstractJsonHandler.getObjectMapper();
        	JsonNode node = mapper.readTree(r);
            auth.setToken(node.path(Authentication.TOKEN_KEY).asText());
            auth.setTokenType(node.path(Authentication.TOKEN_TYPE_KEY).asText());
            auth.setRefreshToken(node.path(Authentication.REFRESH_KEY).asText());
        } finally {
            if ( r != null ) {
                r.close();
            }
        }
        return auth;
    }
    
	@Override
	protected Authentication handleSuccess(HttpResponse resp)
			throws ClientProtocolException, IOException {
		return parseAuthentication(resp);
	}

	@Override
	protected IOException createInternalServerError(String errorMessage,
			String errorText, String errorTextType) {
		return new AuthenticationFailedException(errorMessage, errorText, errorTextType);
	}

	@Override
	protected Authentication handleError(HttpResponse resp)
			throws ClientProtocolException, IOException {
        StatusLine status = resp.getStatusLine();
        int code = status.getStatusCode();
        switch (code) {
	        case 400:
	            // OAUTH2 specifies a 400 status code if the authentication fails
	            Pair<String, String> error = parseErrorString(resp);
	            if ( error != null ) {
	                // Throw an authentication failed exception with the error message and details
	                throw new AuthenticationFailedException(error.first(), error.second());
	            }
	            // Fall through to report this as a generic error if we don't get a proper error string
	        case 401:
	            // Authorization Required
	         default:
	             throw new IOException(status.toString());
        }
	}
}
