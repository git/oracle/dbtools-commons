/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.http.rest;

import java.io.IOException;

import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.http.AbstractResponseHandler;
import oracle.dbtools.http.Client;
import oracle.dbtools.http.Session;
import oracle.dbtools.http.SessionCreator;
import oracle.dbtools.http.SessionInfo;
import oracle.dbtools.http.SessionType;
import oracle.dbtools.http.auth.Authentication;
import oracle.dbtools.http.auth.AuthenticationEngine;
import oracle.dbtools.http.auth.AuthenticationFailedException;
import oracle.dbtools.http.auth.basic.HeadlessBasicAuthenticationEngine;
import oracle.dbtools.rest.ISimpleRestObjectClient;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;

/**
 * SimpleRestObjectAccessor
 * 
 * Utility class to allow retrieval and update of objects over REST using json. 
 *
 * CONSTRAINTS:
 *  1. You must know the object type(s) to be accessed for the end points
 *  2. The end point services must have access to the classes used 
 *  3. Any constraints added by the implementor
 *     For oracle.common.http, this means the classes used must be able to
 *     be mapped via com.fasterxml.jackson.databind.ObjectMapper.
 *  
 * NOTES:
 *  - Assuming ORDS, it has access to classes in oracle.dbtools.common
 *  - Implementors MAY use the type information to handle multiple types through a single end point     
 *
 * @author <a href="mailto:brian.jeffries@oracle.com?subject=oracle.dbtools.http.rest.SimpleRestObjectAccessor">Brian Jeffries</a>
 * @since SQL Developer 4.2
 */

public class SimpleRestObjectClient implements ISimpleRestObjectClient {
    public static final String CANNONICAL_OBJECT_CLASS = "X-CannonicalObjectClass";
    
    private ConnectionInfo m_sessionInfo;
    private Session httpSession;

    public SimpleRestObjectClient() {
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.rest.ISimpleRestObjectAccessor#connect(oracle.dbtools.rest.ISimpleRestObjectAccessor.SessionInfo)
     */
    @SuppressWarnings("nls")
    @Override
    public void connect(ConnectionInfo info) throws IOException {
        if (httpSession != null && httpSession.isOpen()) {
            if (ModelUtil.areEqual(info,  m_sessionInfo)) {
                return; // already open
            } else {
                throw new IOException("Cannot change session info for connected client");
            }
        }
        m_sessionInfo = info;
        Client client = new SimpleRestClient(new SimpleRestSessionType(info.getClientID(), info.getClientSecret()));
        SessionCreator sessionCreator =  SessionCreator.createSessionCreator(client);
        SessionInfo sessionInfo = new SessionInfo("SimpleRestObjectAccessor", 
                                                  info.getServerRoot(), info.getServiceRoot(), 
                                                  info.getUsername(), info.getPassword());
        try {
            httpSession = sessionCreator.createSession(sessionInfo);
        }
        catch(Throwable t) {
            throwIOException(t);
        }
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.rest.ISimpleRestObjectAccessor#close()
     */
    @Override
    public void close() {
        if (httpSession != null) {
            httpSession.close();
        }
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.rest.ISimpleRestObjectAccessor#get(java.lang.String, java.lang.Class)
     */
    @Override
    public <T> T get(String path, T object) throws IOException {
        // read
        T result = null;
        try {
            @SuppressWarnings("unchecked")
            SimpleRestObjectHandler<T> handler = new SimpleRestObjectHandler<>((Class<T>) object.getClass());
            result = httpSession.executeQuery(path, handler);
            // Colm: Security issue if allow user specified class construction
            // See https://www.owasp.org/index.php/Deserialization_of_untrusted_data
            // See https://foxglovesecurity.com/2015/11/06/what-do-weblogic-websphere-jboss-jenkins-opennms-and-your-application-have-in-common-this-vulnerability/
            // so don't advertise what class we are sending
            //post.addHeader(CANNONICAL_OBJECT_CLASS, object.getClass().getCanonicalName());
            // DONT_DO: need to get HttpReq to .addHeader(CANNONICAL_OBJECT_CLASS, object.getClass().getCanonicalName());
        }
        catch(Throwable t) {
            throwIOException(t);
        }
        return result;
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.rest.ISimpleRestObjectClient#post(java.lang.String, java.lang.Object)
     */
    @Override
    public <T> T post(String path, T object) throws IOException {
        T result = null;
        try {
            @SuppressWarnings("unchecked")
            SimpleRestObjectHandler<T> handler = new SimpleRestObjectHandler<>((Class<T>) object.getClass());
            HttpPost post = httpSession.createPost(path);
            post.setEntity(handler.asEntity(object));
            // Colm: Security issue if allow user specified class construction
            // See https://www.owasp.org/index.php/Deserialization_of_untrusted_data
            // See https://foxglovesecurity.com/2015/11/06/what-do-weblogic-websphere-jboss-jenkins-opennms-and-your-application-have-in-common-this-vulnerability/
            // so don't advertise what class we are sending
            //post.addHeader(CANNONICAL_OBJECT_CLASS, object.getClass().getCanonicalName());
            result = httpSession.executeRequest(post, handler);
        }
        catch(Throwable t) {
            throwIOException(t);
        }
        return result;
    }


    /* (non-Javadoc)
     * @see oracle.dbtools.rest.ISimpleRestObjectAccessor#update(java.lang.String, java.lang.Object)
     */
    @Override
    public <T> T put(String path, T object) throws IOException {
        // update
        T result = null;
        try {
            @SuppressWarnings("unchecked")
            SimpleRestObjectHandler<T> handler = new SimpleRestObjectHandler<>((Class<T>) object.getClass());
            HttpPut put = httpSession.createPut(path);
            put.setEntity(handler.asEntity(object));
            // Colm: Security issue if allow user specified class construction
            // See https://www.owasp.org/index.php/Deserialization_of_untrusted_data
            // See https://foxglovesecurity.com/2015/11/06/what-do-weblogic-websphere-jboss-jenkins-opennms-and-your-application-have-in-common-this-vulnerability/
            // so don't advertise what class we are sending
            //post.addHeader(CANNONICAL_OBJECT_CLASS, object.getClass().getCanonicalName());
            result = httpSession.executeRequest(put, handler);
        }
        catch(Throwable t) {
            throwIOException(t);
        }
        return result;
    }

    /* (non-Javadoc)
     * @see oracle.dbtools.rest.ISimpleRestObjectClient#delete(java.lang.String, java.lang.Object)
     */
    @Override
    public boolean delete(String path) throws IOException {
        boolean result = false;
        try {
            HttpDelete delete = httpSession.createDelete(path);
            result = httpSession.executeRequest(delete, new AbstractResponseHandler<Boolean>() {
                @Override
                protected Boolean handleSuccess(HttpResponse resp) throws ClientProtocolException, IOException {
                    return true;
                }

                @Override
                protected IOException createInternalServerError(String errorMessage, String errorText, String errorTextType) {
                    return new SimpleRestObjectException(errorMessage, errorText, errorTextType);
                }

                @Override
                protected Boolean handleError(HttpResponse resp) throws ClientProtocolException, IOException {
                    report500Error(resp); // bad name - will throw exception from createInternalServerError
                    return false;
                }
            });
        }
        catch(Throwable t) {
            throwIOException(t);
        }
        return result;
    }

    private void throwIOException(Throwable t) throws IOException {
        if (t instanceof IOException) {
            throw (IOException)t;
        } else {
            Throwable c = t.getCause();
            if (c instanceof IOException) {
                throw (IOException)c;
            } else {
                throw new IOException(t);
            }
        }
    }
    
    
    private static class SimpleRestSessionType extends SessionType {
        private String m_clientId;
        private String m_clientSecret;
        
        public SimpleRestSessionType(String clientId, String clientSecret) {
            super();
            m_clientId = clientId;
            m_clientSecret = clientSecret;
        };
        
        public boolean hasSecret() {
            return getClientSecret() != null && !getClientSecret().isEmpty(); 
        }

        /* (non-Javadoc)
         * @see oracle.dbtools.http.SessionType#getType()
         */
        @SuppressWarnings("nls")
        @Override
        public String getType() {
            return "SimpleRestSessionType";
        }

        /* (non-Javadoc)
         * @see oracle.dbtools.http.SessionType#getClientID()
         */
        @Override
        protected String getClientID() {
            return m_clientId;
        }

        /* (non-Javadoc)
         * @see oracle.dbtools.http.SessionType#getClientSecret()
         */
        @Override
        protected String getClientSecret() {
            return m_clientSecret;
        }
        
    }
    
   
    private static class SimpleRestClient extends Client {

        /**
         * @param type
         */
        protected SimpleRestClient(SimpleRestSessionType type) {
            super(type);
       }

        /* (non-Javadoc)
         * @see oracle.dbtools.http.Client#getUserAgent()
         */
        @SuppressWarnings("nls")
        @Override
        protected String getUserAgent() {
            return "oracle.dbtools.http.rest.SimpleRestObjectClient";
        }

        /* (non-Javadoc)
         * @see oracle.dbtools.http.Client#createAuthenticationEngine()
         */
        @Override
        protected AuthenticationEngine createAuthenticationEngine() {
            if (((SimpleRestSessionType)getConnectionType()).hasSecret()) {
                return new HeadlessBasicAuthenticationEngine();
            } else {
                return new HeadlessBasicAuthenticationEngine() {
                    /* (non-Javadoc)
                     * @see oracle.dbtools.http.auth2.basic.BasicAuthenticationEngine#authenticate(oracle.dbtools.http.SessionInfo)
                     */
                    @Override
                    public Authentication authenticate(SessionInfo conn) throws AuthenticationFailedException {
                        Authentication auth = new Authentication();
                        // Maybe auth.setToken(?) - nope, default seems to work fine
                        return auth;
                    }
                };
            }
        }
        
    }

}
