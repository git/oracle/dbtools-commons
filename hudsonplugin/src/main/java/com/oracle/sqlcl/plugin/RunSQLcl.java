/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package com.oracle.sqlcl.plugin;

import hudson.model.TaskListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.attribute.BasicFileAttributes;
import oracle.dbtools.db.ResultSetFormatter;
import oracle.dbtools.raptor.newscriptrunner.SQLPLUS;
import oracle.dbtools.raptor.newscriptrunner.ScriptExecutor;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.WrapListenBufferOutputStream;
import oracle.dbtools.raptor.newscriptrunner.commands.SetAppinfo;
import org.apache.commons.lang.StringUtils;
import oracle.dbtools.raptor.newscriptrunner.restricted.RunnerRestrictedLevel;


/**
 * @author <a href=
 *         "mailto:ramesh.uppala@oracle.com@oracle.com?subject=RunSQLcl.java">
 * Ramesh Uppala</a>
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=RunSQLcl.java">
 * Barry Mcgillin</a>
 */
public class RunSQLcl {

    private static ScriptExecutor runner;
    private static ScriptRunnerContext ctx;

    public RunSQLcl() {

    }

    public boolean validateConnection() {

      try {
            // returns the Class object for the class with the specified name
            Class cls = Class.forName("oracle.jdbc.OracleDriver");
            Class clsOrest = Class.forName("oracle.dbtools.jdbc.orest.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
          
        //System.out.println("In ValidateConnection =" + getConnectString());
        Connection conn = null;
        boolean returnVal = false;
        try {
            ScriptRunnerContext context = new ScriptRunnerContext();
            ScriptExecutor scriptExec;
            context.putProperty(ScriptRunnerContext.DBCONFIG_GLOGIN, true);// override used for sqldev
            context.putProperty(ScriptRunnerContext.NOLOG, Boolean.TRUE);
            context.putProperty(ScriptRunnerContext.SET_SECUREDCOL, Boolean.FALSE);
            context.consumerRuning(true);

            if (getCredentialsfile() == null) {
                scriptExec = new ScriptExecutor(getConnectString(), null);// does null connection cause a problem..
            } else {
                String fileName = getCredentialsfile();
                File file = new File(fileName);
                if (file.exists()) {
                    returnVal = true;
                } else {
                    return false;
                }
                String setConfigStr = "set cloudconfig " + fileName + System.lineSeparator() + getConnectString();
                scriptExec = new ScriptExecutor(setConfigStr, null);// does null connection cause a problem..
            }

            scriptExec.setScriptRunnerContext(context);
            scriptExec.getScriptRunnerContext().putProperty(ScriptRunnerContext.COMMANDLINECONNECT, Boolean.TRUE);

            scriptExec.run();
            if (scriptExec.getScriptRunnerContext().getCurrentConnection() != null) {
                conn = scriptExec.getScriptRunnerContext().getCurrentConnection();
                if (conn != null) {
                    returnVal = true;
                } else {
                    returnVal = false;
                }
            } else {
                returnVal = false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                }
            } catch (SQLException seq) {
                seq.printStackTrace();
            }
        }
        return returnVal;
    }

    public Connection getConnection() {

        ctx = new ScriptRunnerContext();
        String setConfigStr = null;
        setCtx(ctx);
        setLoggingState(ctx);
        ctx.putProperty(ScriptRunnerContext.DBCONFIG_GLOGIN, true);// override used for sqldev
        ctx.putProperty(ScriptRunnerContext.NOLOG, Boolean.TRUE);
        ctx.putProperty(ScriptRunnerContext.SET_SECUREDCOL, Boolean.FALSE);
        ctx.consumerRuning(true);
        
        ResultSetFormatter.setMaxLines(Integer.MAX_VALUE); // Do not stop at 5k default.Set unlimited, or in this case Integer MAX 

        if (StringUtils.isEmpty(getCredentialsfile())) {
            runner = new ScriptExecutor(getConnectString(), null);// does null connection cause a problem..
        } else {
            setConfigStr = "set cloudconfig " + getCredentialsfile() + System.lineSeparator() + getConnectString();
            runner = new ScriptExecutor(setConfigStr, null);// does null connection cause a problem..
        }

        setRunner(runner);
        runner.setScriptRunnerContext(ctx);
        runner.getScriptRunnerContext().putProperty(ScriptRunnerContext.COMMANDLINECONNECT, Boolean.TRUE);
        runner.getScriptRunnerContext().setEscape(false);
        runner.getScriptRunnerContext().setSubstitutionOn(false);

        // reset these 3 before .login run
        BufferedOutputStream bos = new BufferedOutputStream(getTaskListener().getLogger());

        WrapListenBufferOutputStream wrappedBOS = new WrapListenBufferOutputStream(bos, null);
        wrappedBOS.setRemoveForcePrint(true);

        runner.setOut(wrappedBOS);

        runner.run();
        // do this later 
        if (runner.getScriptRunnerContext().getCurrentConnection() != null) {
            Connection conn = runner.getScriptRunnerContext().getCurrentConnection();
            ctx = runner.getScriptRunnerContext();
            ctx.setBaseConnection(conn);
            ctx.setCloseConnection(false);
            return conn;
        } else {
            if (!StringUtils.isEmpty(getCredentialsfile())) {
                deleteAllCredentialsFilesInTemp();
            }
        }
        return null;
    }

    public void runFile(String name, ScriptRunnerContext context, String inputFile, boolean isSetDirectory) {

        context.putProperty(ScriptRunnerContext.CDPATH, getCurrentDirectory());
        setRestrictLevelContext(context);
        context.consumerRuning(true);
        setCtx(context);
        setLoggingState(context);
        runner.setStmt("@" + inputFile);

        getTaskListener().getLogger().println(name + " " + inputFile);

        context.putProperty(name, runner);
        context.setTopLevel(false);
        @SuppressWarnings("unchecked")
        ArrayList<String> scrArrayList = ((ArrayList<String>) context.getProperty(ScriptRunnerContext.APPINFOARRAYLIST));

        scrArrayList.add(inputFile);

        context.putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
        ResultSetFormatter.setMaxLines(Integer.MAX_VALUE); // Do not stop at 5k default.Set unlimited, or in this case Integer MAX
        ResultSetFormatter.setMaxRows(Integer.MAX_VALUE);; // Do not stop at 5k default.Set unlimited, or in this case Integer MAX
        runner.setScriptRunnerContext(context);
        try {
            runner.run();
        } finally {
            scrArrayList = (ArrayList<String>) context.getProperty(ScriptRunnerContext.APPINFOARRAYLIST);
            if (scrArrayList.size() != 0) {
                scrArrayList.remove(scrArrayList.size() - 1);
            }
            context.putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
            if (scrArrayList.size() != 0) {
                SetAppinfo.setAppinfo(context, context.getCurrentConnection(), scrArrayList.get(scrArrayList.size() - 1), scrArrayList.size());
            } else {
                SetAppinfo.setAppinfo(context, context.getCurrentConnection(), SQLPLUS.PRODUCT_NAME, // $NON-NLS-1$p
                        0);
            }
            disconnect();
        }
        context.setTopLevel(true);
    }

    public void runFile(String name, String inputFile, boolean isSetDirectory) {

        ScriptRunnerContext context = null;
        try {
            context = new ScriptRunnerContext();
            setRestrictLevelContext(context);
            context.putProperty(ScriptRunnerContext.CDPATH, getCurrentDirectory());

            setCtx(context);
            setLoggingState(context);
            context.putProperty(ScriptRunnerContext.DBCONFIG_GLOGIN, true);// override used for sqldev
            context.putProperty(ScriptRunnerContext.NOLOG, Boolean.TRUE);
            context.putProperty(ScriptRunnerContext.SET_SECUREDCOL, Boolean.FALSE);
            context.consumerRuning(true);
            ResultSetFormatter.setMaxLines(Integer.MAX_VALUE); // Do not stop at 5k default.Set unlimited, or in this case Integer MAX 

            runner = new ScriptExecutor("@" + inputFile, null);

            BufferedOutputStream bos = new BufferedOutputStream(getTaskListener().getLogger());

            WrapListenBufferOutputStream wrappedBOS = new WrapListenBufferOutputStream(bos, null);
            wrappedBOS.setRemoveForcePrint(true);

            runner.setOut(wrappedBOS);

            context.consumerRuning(true);

            getTaskListener().getLogger().println(name + " " + inputFile);

            context.putProperty(name, runner);
            context.setTopLevel(false);
            @SuppressWarnings("unchecked")
            ArrayList<String> scrArrayList = ((ArrayList<String>) context.getProperty(ScriptRunnerContext.APPINFOARRAYLIST));

            scrArrayList.add(inputFile);

            context.putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
            ResultSetFormatter.setMaxLines(Integer.MAX_VALUE); // Do not stop at 5k default.Set unlimited, or in this case Integer MAX
            ResultSetFormatter.setMaxRows(Integer.MAX_VALUE);; // Do not stop at 5k default.Set unlimited, or in this case Integer MAX
            runner.setScriptRunnerContext(ctx);

            runner.run();
            context.setTopLevel(true);
        } catch (Exception e) {
           e.printStackTrace(); 
           //getTaskListener().getLogger().println(" File " + inputFile + " not found!");
        } finally {
            ArrayList<String> scrArrayList = (ArrayList<String>) context.getProperty(ScriptRunnerContext.APPINFOARRAYLIST);
            if (scrArrayList.size() != 0) {
                scrArrayList.remove(scrArrayList.size() - 1);
            }
            context.putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
            if (scrArrayList.size() != 0) {
                SetAppinfo.setAppinfo(context, context.getCurrentConnection(), scrArrayList.get(scrArrayList.size() - 1), scrArrayList.size());
            } else {
                SetAppinfo.setAppinfo(context, context.getCurrentConnection(), SQLPLUS.PRODUCT_NAME, // $NON-NLS-1$p
                        0);
            }
            disconnect();
        }
    }

    public void runScript(String name, ScriptRunnerContext context, String inputScript, boolean isSetDirectory) {

        context.consumerRuning(true);
        setCtx(context);
        setLoggingState(context);
        runner.setStmt(inputScript);
        context.putProperty(ScriptRunnerContext.CDPATH, getCurrentDirectory());
        getTaskListener().getLogger().println(name + " " + inputScript);
        setRestrictLevelContext(context);
        context.putProperty(name, runner);
        context.setTopLevel(false);
        @SuppressWarnings("unchecked")
        ArrayList<String> scrArrayList = ((ArrayList<String>) context.getProperty(ScriptRunnerContext.APPINFOARRAYLIST));

        scrArrayList.add(inputScript);

        context.putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
        ResultSetFormatter.setMaxLines(Integer.MAX_VALUE); // Do not stop at 5k default.Set unlimited, or in this case Integer MAX
        ResultSetFormatter.setMaxRows(Integer.MAX_VALUE);; // Do not stop at 5k default.Set unlimited, or in this case Integer MAX
        runner.setScriptRunnerContext(context);
        try {
            runner.run();
        } finally {
            scrArrayList = (ArrayList<String>) context.getProperty(ScriptRunnerContext.APPINFOARRAYLIST);
            if (scrArrayList.size() != 0) {
                scrArrayList.remove(scrArrayList.size() - 1);
            }
            context.putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
            if (scrArrayList.size() != 0) {
                SetAppinfo.setAppinfo(context, context.getCurrentConnection(), scrArrayList.get(scrArrayList.size() - 1), scrArrayList.size());
            } else {
                SetAppinfo.setAppinfo(context, context.getCurrentConnection(), SQLPLUS.PRODUCT_NAME, // $NON-NLS-1$p
                        0);
            }
            disconnect();
        }
        context.setTopLevel(true);
    }

    public void runScript(String name, String inputFile, boolean isSetDirectory) {

        ScriptRunnerContext context = null;
        try {
            context = new ScriptRunnerContext();
            setRestrictLevelContext(context);
            context.putProperty(ScriptRunnerContext.CDPATH, getCurrentDirectory());
            setCtx(context);
            setLoggingState(context);
            context.putProperty(ScriptRunnerContext.DBCONFIG_GLOGIN, true);// override used for sqldev
            context.putProperty(ScriptRunnerContext.NOLOG, Boolean.TRUE);
            context.putProperty(ScriptRunnerContext.SET_SECUREDCOL, Boolean.FALSE);
            context.consumerRuning(true);
            ResultSetFormatter.setMaxLines(Integer.MAX_VALUE); // Do not stop at 5k default.Set unlimited, or in this case Integer MAX 

            runner = new ScriptExecutor(inputFile, null);

            BufferedOutputStream bos = new BufferedOutputStream(getTaskListener().getLogger());

            WrapListenBufferOutputStream wrappedBOS = new WrapListenBufferOutputStream(bos, null);
            wrappedBOS.setRemoveForcePrint(true);

            runner.setOut(wrappedBOS);

            context.consumerRuning(true);

            getTaskListener().getLogger().println(name + " " + inputFile);
            context.putProperty(name, runner);
            context.setTopLevel(false);
            @SuppressWarnings("unchecked")
            ArrayList<String> scrArrayList = ((ArrayList<String>) context.getProperty(ScriptRunnerContext.APPINFOARRAYLIST));

            scrArrayList.add(inputFile);
            context.putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
            ResultSetFormatter.setMaxLines(Integer.MAX_VALUE); // Do not stop at 5k default.Set unlimited, or in this case Integer MAX
            ResultSetFormatter.setMaxRows(Integer.MAX_VALUE);; // Do not stop at 5k default.Set unlimited, or in this case Integer MAX
            runner.setScriptRunnerContext(ctx);

            runner.run();
            context.setTopLevel(true);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ArrayList<String> scrArrayList = (ArrayList<String>) context.getProperty(ScriptRunnerContext.APPINFOARRAYLIST);
            if (scrArrayList.size() != 0) {
                scrArrayList.remove(scrArrayList.size() - 1);
            }
            context.putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
            if (scrArrayList.size() != 0) {
                SetAppinfo.setAppinfo(context, context.getCurrentConnection(), scrArrayList.get(scrArrayList.size() - 1), scrArrayList.size());
            } else {
                SetAppinfo.setAppinfo(context, context.getCurrentConnection(), SQLPLUS.PRODUCT_NAME, // $NON-NLS-1$p
                        0);
            }
            disconnect();
        }
    }

    public void disconnect() {
        try {
            if (runner.getScriptRunnerContext().getCurrentConnection() != null) {
                Connection conn = runner.getScriptRunnerContext().getCurrentConnection();
                // redundant - context & getScriptRunnerContext - should be same object.
                ctx = runner.getScriptRunnerContext();

                // SetCloudConfig.CLOUD_CONFIG -> CLOUD_CONFIG_ZIP
                // SetCloudConfig.CLOUD_CONFIG_PATH -> CLOUD_CONFIG_PATH
                deleteAllCredentialsFilesInTemp();
                ctx.setCloseConnection(true);
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                }
                ctx = null;
                runner = null;
                m_connectString = null;
                m_credentialsFile = null;
            }
        } catch (SQLException seq) {
            seq.printStackTrace();
        }
    }

    private void deleteAllCredentialsFilesInTemp() {
        // SetCloudConfig.CLOUD_CONFIG -> CLOUD_CONFIG_ZIP
        // SetCloudConfig.CLOUD_CONFIG_PATH -> CLOUD_CONFIG_PATH
        ctx = runner.getScriptRunnerContext();
        // if ((!StringUtils.isEmpty(getCredentialsfile())) && (ctx.getProperty("CLOUD_CONFIG_ZIP") != null)) {
        Path configDir = (Path) ctx.getProperty("CLOUD_CONFIG_PATH");
        if (configDir != null) {
            try {
                Files.walkFileTree(configDir, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e) {
                ctx.write(e.getLocalizedMessage());
            }
        }
    }

    public static ScriptExecutor getRunner() {
        return runner;
    }

    public static void setRunner(ScriptExecutor runner) {
        RunSQLcl.runner = runner;
    }

    public static ScriptRunnerContext getCtx() {
        return ctx;
    }

    public static void setCtx(ScriptRunnerContext ctx) {
        RunSQLcl.ctx = ctx;
    }

    public void setTaskListener(TaskListener listener) {
        m_taskListener = listener;
    }

    public TaskListener getTaskListener() {
        return m_taskListener;
    }

    public void setConnectString(String connStr) {
        m_connectString = connStr;
    }

    public String getConnectString() {
        return m_connectString;
    }

    public void setCredentialsfile(String credStr) {
        m_credentialsFile = credStr;
    }

    public String getCredentialsfile() {
        return m_credentialsFile;
    }

    public void setRestrictLevelContext(ScriptRunnerContext context) {
        
      switch (m_restrictLevel) {
         case "0":
             context.setRestrictedLevel(RunnerRestrictedLevel.Level.NONE);
             break;
         case "1":
             context.setRestrictedLevel(RunnerRestrictedLevel.Level.R1);
             break;
         case "2":
             context.setRestrictedLevel(RunnerRestrictedLevel.Level.R2);
             break;
         case "3":
             context.setRestrictedLevel(RunnerRestrictedLevel.Level.R3);
             break;
         case "4":
             context.setRestrictedLevel(RunnerRestrictedLevel.Level.R4);
             break;
         default:
             context.setRestrictedLevel(RunnerRestrictedLevel.Level.NONE);
       
     }

    }

    private void setLoggingState(ScriptRunnerContext context) {
        
        Boolean optlCheck = (Boolean) context.getProperty(ScriptRunnerContext.OPTLFLAG);
 
        if ((optlCheck == null) || optlCheck.equals(Boolean.FALSE)) {
            // -verbose flag off. Just log severe logs
            Enumeration<String> list = LogManager.getLogManager().getLoggerNames();
            while (list.hasMoreElements()) {
                Logger.getLogger(list.nextElement()).setLevel(Level.SEVERE);
            }
        } else {
            // -verbose flag on. Just log info warnings.
            Enumeration<String> list = LogManager.getLogManager().getLoggerNames();
            while (list.hasMoreElements()) {
                Logger.getLogger(list.nextElement()).setLevel(Level.INFO);
            }
        }
    }
    
    public void setRestrictLevel(String rlevel) {

        m_restrictLevel = rlevel;
    }

    public void setCurrentDirectory(String dir) {
        m_currentDirectory = dir;
    }

    public String getCurrentDirectory() {
        return m_currentDirectory;
    }

    private TaskListener m_taskListener;
    private String m_connectString = null;
    private String m_credentialsFile = null;
    private String m_restrictLevel = null;
    private String m_currentDirectory = null;
}
