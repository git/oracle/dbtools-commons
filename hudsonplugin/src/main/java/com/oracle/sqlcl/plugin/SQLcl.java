/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package com.oracle.sqlcl.plugin;

import hudson.Launcher;
import hudson.Extension;
import hudson.util.FormValidation;
import hudson.model.AbstractBuild;
import hudson.model.Result;
import hudson.FilePath;
import hudson.FilePath.FileCallable;
import hudson.XmlFile;
import hudson.model.BuildListener;
import hudson.model.AbstractProject;
import hudson.model.FreeStyleProject;
import hudson.model.Job;
import hudson.model.ParametersAction;
import hudson.model.Saveable;
import hudson.model.listeners.SaveableListener;
import hudson.model.listeners.ItemListener;
import hudson.model.Item;
import hudson.model.Project;
import hudson.tasks.Builder;
import hudson.tasks.BuildStepDescriptor;
import hudson.remoting.Callable;
import hudson.remoting.VirtualChannel;
import net.sf.json.JSONObject;
import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.Connection;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.io.PrintStream;
import java.io.File;
import java.io.Serializable;
import oracle.dbtools.raptor.newscriptrunner.WrapListenBufferOutputStream;

import oracle.security.pki.OracleSecretStore;
import oracle.security.pki.OracleSecretStoreException;
import oracle.security.pki.OracleWallet;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.StaplerRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author <a href=
 *         "mailto:ramesh.uppala@oracle.com@oracle.com?subject=SQLcl.java">
 * Ramesh Uppala</a>
 * @author <a href=
 *         "mailto:roy.varghese@oracle.com@oracle.com?subject=SQLcl.java">
 * Roy Varghese</a>
 *
 */
/**
 * SQLcl {@link Builder}.
 *
 * <p>
 * When the user configures the project and enables this builder,
 * {@link DescriptorImpl#newInstance(StaplerRequest)} is invoked and a new
 * {@link SQLcl} is created. The created instance is persisted to the project
 * configuration XML by using XStream, so this allows you to use instance fields
 * (like {@link #username}) to remember the configuration.
 *
 * <p>
 * When a build is performed, the
 * {@link #perform(AbstractBuild, Launcher, BuildListener)} method will be
 * invoked.
 *
 */
@SuppressWarnings("unused")
public class SQLcl extends Builder {

    private final String username;
    private String password;
    private final String connectstring;
    private final String role;
    private final String sqlfile;
    private final String sqltext;
    private final String restrictionlevel;
    private String workspace;
    private final String sqlfileortext;
    private String configBuilderKey;
    private String credentialsfile;

    //Fields not populated by the JSON binder.
    private static Logger log = LoggerFactory.getLogger(SQLcl.class);

    // Fields in config.jelly must match the parameter names in the "DataBoundConstructor"
    @DataBoundConstructor
    @SuppressWarnings("unused")
    public SQLcl(String configbuilderkey, String username, String password, String connectstring, String credentialsfile, String role,
            String workspace, String sqlfileortext, String sqlfile, String sqltext, String restrictionlevel) {

        this.username = username;
        this.password = password;
        this.connectstring = connectstring;
        this.role = role;
        this.sqlfile = sqlfile;
        this.sqltext = sqltext;
        this.restrictionlevel = restrictionlevel;
        this.workspace = workspace;
        this.sqlfileortext = sqlfileortext;
        this.configBuilderKey = configbuilderkey;
        this.credentialsfile = credentialsfile;

        if (!password.equals(CONFIG_PASSWORD)) {
            setPreWalletPassword(password);
            this.configBuilderKey = generateBuilderKey();
        }
        setPassword();
    }

    /**
     * We'll use this from the <tt>config.jelly</tt>.
     */
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword() {
        if (password != null && !password.isEmpty()) {
            password = CONFIG_PASSWORD;
        } else {
            password = "";
        }
    }

    public String getConnectstring() {
        return connectstring;
    }
    
    public String getCredentialsfile() {
        return credentialsfile;
    }

    public String getSqlfile() {
        return sqlfile;
    }

    public String getSqltext() {
        return sqltext;
    }

    public String getRestrictionlevel() {
        return restrictionlevel;
    }

    public String getRole() {
        return role;
    }

    public boolean getWorkspace() {
        return this.workspace.equalsIgnoreCase("false") ? false : true;
    }

    //@DataBoundSetter
    public void setWorkspace(boolean flag) {
        if (flag) {
            this.workspace = "true";
        } else {
            this.workspace = "false";
        }
    }

    public String getSqlfileortext() {
        return sqlfileortext;
    }

    public String isScriptType(String scriptType) {
        if ( this.sqlfileortext != null)
            return this.sqlfileortext.equalsIgnoreCase(scriptType) ? "true" : "";
        else 
            return "";
    }

    public String getConfigBuilderKey() {
        return configBuilderKey;
    }

    @Override
    public boolean perform(AbstractBuild build, Launcher launcher, BuildListener listener) throws IOException, InterruptedException {

        // Check if the build result set from a previous build step.
        // A null build result indicates that the build is still ongoing and we're
        // likely being run as a build step by the "Any Build Step Plugin".
        // Show only this information on the log files
        Result buildResult = build.getResult();
        ParametersAction parametersAction = build.getAction(ParametersAction.class);
        FilePath sqlFileName = null;
        if (buildResult != null && buildResult.isWorseOrEqualTo(Result.FAILURE)) {
            return false;
        }
        PrintStream logger = listener.getLogger();

        /*
        if (!isUserInputValid(logger)) {
            return false;
        }
         */
        if ((getSqlfileortext() != null) && (getSqlfileortext().equals(SQLFILE_TYPE))) {
            String sqlFile = getSqlfile();
            if (parametersAction != null && !StringUtils.isEmpty(sqlFile)) {
                sqlFile = parametersAction.substitute(build, sqlFile);
            }
            if ("".equals(sqlFile)) {
                writeToConsole(logger, "Error: SQL File is not specified." );
                return false;
            }
            
            if (getWorkspace()) {
                sqlFileName = getFileFromWorkspace(build, sqlFile, listener, logger);
            } else {
                sqlFileName = new FilePath(new File(sqlFile));
                if (!checkIfFileExists(sqlFileName, logger)) {
                    sqlFileName = null;
                }
            }
            
            if (sqlFileName == null) {
                writeToConsole(logger, "Error: The SQL File '" +sqlFile+ "' does not exist in workspace" );
                return false;
            }
        } else if ((getSqlfileortext() != null) && (getSqlfileortext().equals(SQLTEXT_TYPE))) {
            if (getSqltext() == null || getSqltext().equals("")) {
                writeToConsole(logger, "Error: SQL Script Textarea is empty");
                return false;
            }
        } else {
            writeToConsole(logger, "Error: Please select SQL file or add SQL Script to run the job");
            return false;
        }

       try {
            // returns the Class object for the class with the specified name
            Class clsOracleDriver = Class.forName("oracle.jdbc.OracleDriver");
            Class clsRestJdbc = Class.forName("oracle.dbtools.jdbc.orest.Driver");
        } catch (ClassNotFoundException ex) {
            writeToConsole(logger, "Error: "+ex.toString());
        }
       
        String connectString = getConnectstring();
        String password = getWalletPassword(build.getParent(), this, logger);
        String username = getUsername();
        String role = getRole();
        if (parametersAction != null) {
            connectString = parametersAction.substitute(build, connectString);
            password = parametersAction.substitute(build, password);
            username = parametersAction.substitute(build, username);         
        }
        writeToLog(logger, "Connection --- ----------------");
        writeToLog(logger, "Username : " + username);
        writeToLog(logger, "Connect String : " + connectString);
        writeToLog(logger, "Oracle Role : " + role);
        writeToLog(logger, "Workspace : " + getWorkspace());
        writeToLog(logger, "Current Job Directory : " + getJobDirectory(build.getProject()));

        if ((getSqlfileortext() != null) && (getSqlfileortext().equals(SQLFILE_TYPE))) {
            writeToLog(logger, "SQL File Name : " + sqlFileName);
        }
        writeToLog(logger, "Restrict Level : " + getRestrictionlevel());

        writeToLog(logger, "Log file Starts ----------------");

        java.util.logging.Logger l = java.util.logging.Logger.getLogger("oracle.dbtools"); //$NON-NLS-1$
        l.setLevel(Level.parse("SEVERE")); //$NON-NLS-1$

        String sqlText = getSqltext();
        if (parametersAction != null && !StringUtils.isEmpty((sqlText))) {
            sqlText = parametersAction.substitute(build, sqlText);
        }
        
        String credentialsFileName = getCredentialsfile();
        if (parametersAction != null && !StringUtils.isEmpty((credentialsFileName))) {
            credentialsFileName = parametersAction.substitute(build, credentialsFileName);
        }

        String realConnectString = buildConnectString(username, password, connectString, role);

        SqlclCallable callable = null;
        FilePath credentialsZipFile = null;
        if (!StringUtils.isEmpty(credentialsFileName)) {
            credentialsZipFile = getFileFromWorkspace(build, credentialsFileName, listener, logger);
            // must be abosolute path given
            if (credentialsZipFile == null) {
                credentialsZipFile = new FilePath(new File(credentialsFileName));
                if (!checkIfFileExists(credentialsZipFile, logger)) {
                    credentialsZipFile = null;
                    writeToConsole(logger, "Error: Credentials Zip File "+credentialsFileName+" does not exist!!");
                    return false;
                }
            }
        }
        
        String workspaceDirectory = build.getWorkspace().getRemote();
        
        if ( (StringUtils.isEmpty(username)) && (StringUtils.isEmpty(connectString)) ) {
            callable = new SqlclCallable(getSqlfile(), listener, sqlText, null, getRestrictionlevel(), credentialsZipFile, getWorkspace(), workspaceDirectory);
        } else {
            callable = new SqlclCallable(getSqlfile(), listener, sqlText, realConnectString, getRestrictionlevel(), credentialsZipFile, getWorkspace(), workspaceDirectory);
        }

        try {
            FilePath buildWorkspace = build.getWorkspace();
            Boolean exitStatus = buildWorkspace.act(callable);
            return exitStatus;
        } catch (Exception ex) {
            writeToConsole(logger, "Error: "+ex.getMessage());
            ex.printStackTrace(logger);
        }
        return false;
    }

    // Overridden for better type safety.
    // If your plugin doesn't really define any property on Descriptor,
    // you don't have to do this.
    /*  @Override
     public DescriptorImpl getDescriptor() {
     return (DescriptorImpl) super.getDescriptor();
     }
     */
    /**
     * Descriptor for {@link SQLcl}. Used as a singleton. The class is marked as
     * public so that it can be accessed from views.
     *
     * <p>
     * See <tt>src/main/resources/hudson/plugins/SQLcl-plugin/SQLcl/*.jelly</tt>
     * for the actual HTML fragment for the configuration screen.
     */
    @Extension // This indicates to Jenkins that this is an implementation of an extension point.
    // public class DescriptorImpl extends BuildStepDescriptor<Builder> {
    public static final class DescriptorImpl extends BuildStepDescriptor<Builder> {

        public DescriptorImpl() {
            load();
        }

        @Override
        public SQLcl newInstance(StaplerRequest req, JSONObject formData) throws FormException {

            try {
                JSONObject json = req.getSubmittedForm();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            // commented for Hudson
            String sqlfile = null;
            String sqltext = null;
            String sqlType = null;
            String credentialfile = null;
            JSONObject formData1 = formData.getJSONObject("sqlfileortext");
            if (formData1.size() > 0) {
                sqlType = formData1.getString("value");
                if (sqlType.equals("SQLFILE")) {
                    sqlfile = formData1.getString("sqlfile");

                } else if (sqlType.equals("SQLTEXT")) {
                    sqltext = formData1.getString("sqltext");
                }
            }
          
            if (formData.has("credentialsfile")) {
                credentialfile = formData.getString("credentialsfile") ;
            }
            //System.out.println("SQLFile " + sqlfile);
            //System.out.println("sqltext " + sqltext);

            return new SQLcl(formData.getString("configbuilderkey"), formData.getString("username"),
                    formData.getString("password"),
                    formData.getString("connectstring"),
                    credentialfile,
                    formData.getString("role"),
                    formData.getString("workspace"),
                    //  formData.getJSONObject("sqlfileortext").getString("value"),
                    sqlType,
                    sqlfile,
                    sqltext,
                    formData.getString("restrictionlevel"));

            // comment out for Jenkins
            // return req.bindJSON(SQLcl.class, formData);
        }

        /**
         * To persist global configuration information, simply store it in a
         * field and call save().
         *
         * <p>
         * If you don't want fields to be persisted, use <tt>transient</tt>.
         */
        //private boolean useFrench;
        /**
         * Performs on-the-fly validation of the form field 'name'.
         *
         * @param value This parameter receives the value that the user has
         * typed.
         * @return Indicates the outcome of the validation. This is sent to the
         * browser.
         */
        /*
        public FormValidation doCheckUsername(@QueryParameter String value)
                throws IOException, ServletException {
            if (value.length() == 0) {
                return FormValidation.error("Please enter Username");
            }
            
            return FormValidation.ok();
        }
        
        public FormValidation doCheckConnectstring(@QueryParameter String value)
                throws IOException, ServletException {
            if (value.length() == 0) {
                return FormValidation.error("Please enter Connect String");
            }
            
            return FormValidation.ok();
        }
         */
        
        public FormValidation doCheckCredentialsfile(@QueryParameter String value)
                throws IOException, ServletException {
            if (value.length() > 0) {
                if (value.contains(".")) {
                    String extension = value.substring(value.lastIndexOf("."), value.length());
                    if (!extension.toLowerCase().endsWith(".zip")) {
                        return FormValidation.error("Please enter Exadata Express Client Credentials .zip file");
                    }
                } else if ( ! value.startsWith("${")){
                    return FormValidation.error("Please enter Exadata Express Client Credentials .zip file");
                }
            }
            return FormValidation.ok();
        }
        
        
        public boolean isApplicable(Class<? extends AbstractProject> aClass) {
            // Indicates that this builder can be used with all kinds of project types 
            return true;
        }

        public FormValidation doTestConnection(
                @QueryParameter("configBuilderKey") String configBuilderKey,
                @QueryParameter("username") final String username,
                @QueryParameter("password") String password,
                @QueryParameter("connectstring") String connectstring,
                @QueryParameter("credentialsfile") final String credentialsfile,
                @QueryParameter("role") final String role)
                throws IOException, ServletException {
            try {
                //System.out.println("username = " + username);
                //System.out.println("connectstring = " + connectstring);
                //System.out.println("Role = " + role);

                if ((username == null || username.isEmpty()) || (password == null) || (connectstring == null || connectstring.isEmpty())) {
                    return FormValidation.error("Verify user input Username, Password and Connect String");
                }
                if (password.equals(CONFIG_PASSWORD)) { // TODO : Not yet implemented 
                    return FormValidation.error("Please enter password to test connection.");
                    // String jobDirectory = m_ConnectStrJobDir.get(configBuilderKey);
                    // ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    // PrintStream stringLogger = new PrintStream(bytes);
                    // password = SQLcl.getPassword(SQLcl.getOracleWalletStore(jobDirectory, stringLogger), configBuilderKey, username, stringLogger);
                }

                RunSQLcl runSQLcl = new RunSQLcl();
                FilePath credentialsZipFile = null;
                
                // jdbc:oracle:orest@http://ipw00aaj.uk.oracle.com:8085/ords/demo/
                if ((connectstring != null) && ((connectstring.startsWith("http:") || (connectstring.startsWith("https:"))))) { 
                     connectstring = "jdbc:oracle:orest@"+connectstring;
                }
                        
                if (role.equals("")) {
                    runSQLcl.setConnectString("connect " + username + "/" + password + "@" + connectstring + "\n");
                } else {
                    runSQLcl.setConnectString("connect " + username + "/" + password + "@" + connectstring + " as " + role + "\n");
                }
                
                if ( !StringUtils.isEmpty(credentialsfile)) {
                    runSQLcl.setCredentialsfile(credentialsfile);
                }
                
                if (runSQLcl.validateConnection()) {
                    return FormValidation.ok("Connection Successful");
                } else {
                    if (!StringUtils.isEmpty(credentialsfile)) {
                        return FormValidation.error("Connection Failed. Please verify the Credentials Zip file and/or Connect String"); 
                    } else 
                        return FormValidation.error("Connection Failed");
                }

            } catch (Exception e) {
                return FormValidation.error("Client error : " + e.getMessage());
            }
        }

        /**
         * This human readable name is used in the configuration screen.
         */
        public String getDisplayName() {
            return "Oracle SQLcl-plugin";
        }

        @Override
        public boolean configure(StaplerRequest req, JSONObject formData) throws FormException {
            // To persist global configuration information,
            // set that to properties and call save().
            // ^Can also use req.bindJSON(this, formData);
            //  (easier when there are many fields; need set* methods for this, like setUseFrench)
            save();
            return super.configure(req, formData);
        }
    }

    public String buildConnectString(String username, String password, String connectString, String role) {

        // jdbc:oracle:orest@http://ipw00aaj.uk.oracle.com:8085/ords/demo/
        if ((connectString != null) && ((connectString.startsWith("http:") || (connectString.startsWith("https:"))))) { 
             connectString = "jdbc:oracle:orest@"+connectString;
        }
        
        if (StringUtils.isEmpty((role))) {
            connectString = String.format("connect %s/%s@%s\n", username, password, connectString);
        } else {
            connectString = String.format("connect %s/%s@%s as %s\n", username, password, connectString, role);
        }
        return connectString;
    }

    private FilePath getFileFromWorkspace(AbstractBuild build, String file, BuildListener listener, PrintStream logger)
            throws IOException, InterruptedException {

        FilePath fileName = new FilePath(build.getWorkspace(), file);
        if (!checkIfFileExists(fileName, logger)) {
            writeToLog(logger, "Please verify the SQL File exists in workspace directory " + build.getWorkspace());
            return null;
        }
        return fileName;
    }

    private boolean checkIfFileExists(FilePath filename, PrintStream logger) {
        try {
            if (!filename.exists()) {
                writeToLog(logger, "Error : SQLFile '" + filename + "' does not exists!!");
                return false;
            }
        } catch (Exception ex) {
            writeToLog(logger, "Error : Cannot check for file existence");
            return false;
        }
        return true;
    }

    private static class SqlFileCallable implements FileCallable<String> {

        private static final long serialVersionUID = 1L;

        @Override
        public String invoke(File file, VirtualChannel channel) throws IOException, InterruptedException {
            if (file.getAbsoluteFile().exists()) {
                return "'" + file.getName() + "' exists.";
            } else {
                return "'" + file.getName() + "' doesn't exist.";
            }
        }
    }

    private static void setWalletPassword(SQLcl SQLclBuilder, String currentJobDirectory, PrintStream logger) {

        try {
            writeToLog(logger, "<<<Oracle Wallet>>> In setWalletPassword :");

            // returns the Class object for the class with the specified name
            Class cls1 = Class.forName("oracle.jdbc.OracleDriver");
            Class cls = Class.forName("oracle.security.pki.OracleWallet");

            File fCheck = new File(currentJobDirectory);
            if (fCheck.exists() && fCheck.isDirectory() && fCheck.canWrite() && fCheck.canRead()) {
                writeToLog(logger, "Job Directory " + currentJobDirectory + "exists and writable");
            } else {
                writeToLog(logger, "Error : Job Directory " + currentJobDirectory + "does not exists or not read/writable");
            }

            // check if exists
            // if not create it
            OracleWallet tenantWallet = new OracleWallet();
            tenantWallet.setLocation(currentJobDirectory);
            if (!tenantWallet.exists(currentJobDirectory)) {
                tenantWallet.createSSO();
                tenantWallet.saveAs(currentJobDirectory);
            }
            // open it and grab the store
            tenantWallet.open(currentJobDirectory, null);
            OracleSecretStore store = tenantWallet.getSecretStore();
            tenantWallet.setSecretStore(store);
            tenantWallet.save();
            // debug
            // store.listCredential();
            // check for existing password
            String passwd = getPassword(store, SQLclBuilder.getConfigBuilderKey(), SQLclBuilder.getUsername(), logger);
            // have to check for create vs modify or get an exception
            if (passwd == null) {
                store.createCredential((SQLclBuilder.getConfigBuilderKey()).toCharArray(), (SQLclBuilder.getUsername()).toCharArray(), (SQLclBuilder.getPreWalletPassword()).toCharArray());
            } else if (!passwd.equals(SQLclBuilder.getPreWalletPassword())) {
                store.modifyCredential((SQLclBuilder.getConfigBuilderKey()).toCharArray(), (SQLclBuilder.getUsername()).toCharArray(), (SQLclBuilder.getPreWalletPassword()).toCharArray());
            }
            // set the store back
            tenantWallet.setSecretStore(store);
            tenantWallet.save();
        } catch (IOException io) {
            io.printStackTrace();
            writeToLog(logger, io.toString());
        } catch (OracleSecretStoreException os) {
            os.printStackTrace();
            writeToLog(logger, os.toString());
        } catch (ClassNotFoundException ex) {
            writeToLog(logger, "Failed to load : oracle.security.pki.OracleWallet");
        }
    }

    private static String getWalletPassword(Job job, SQLcl SQLclBuilder, PrintStream logger) {

        String passwd = null;
        String jobDir = getJobDirectory(job);
        try {
            OracleWallet tenantWallet = new OracleWallet();
            // check if exists
            // if not create it

            File fCheck = new File(jobDir);
            if (fCheck.exists() && fCheck.isDirectory() && fCheck.canWrite() && fCheck.canRead()) {
                writeToLog(logger, "Job Directory " + jobDir + "exists and writable");
            } else {
                writeToLog(logger, "Error : Job Directory " + jobDir + "does not exists or not read/writable");
            }

            if (!tenantWallet.exists(jobDir)) {
                tenantWallet.createSSO();
                tenantWallet.saveAs(jobDir);
            }
            writeToLog(logger, "<<<Oracle Wallet>>> after tenantWallet.createSSO");
            // open it and grab the store
            tenantWallet.open(jobDir, null);
            OracleSecretStore store = tenantWallet.getSecretStore();
            // debug
            // store.listCredential();
            // check for existing password
            passwd = getPassword(store, SQLclBuilder.getConfigBuilderKey(), SQLclBuilder.getUsername(), logger);
        } catch (IOException io) {
            writeToLog(logger, io.toString());
        } catch (OracleSecretStoreException os) {
            writeToLog(logger, os.toString());
        }
        return passwd;
    }

    private static String getPassword(OracleSecretStore store, String configBuilderKey, String user, PrintStream logger) throws OracleSecretStoreException {

        String connectAlias = configBuilderKey;
        final Enumeration e = store.internalAliases();
        while (e.hasMoreElements()) {
            final String storeAlias = (String) e.nextElement();
            if (storeAlias.startsWith(LOGIN_CONNECT_PREFIX)) {
                if (connectAlias.equals(new String(store.getSecret(storeAlias)))) {
                    String index = storeAlias.substring(LOGIN_CONNECT_PREFIX.length());
                    return new String(store.getSecret(LOGIN_PW_PREFIX + index));
                }
            }
        }
        return null;
    }

    private static OracleSecretStore getOracleWalletStore(String jobDir, PrintStream logger) {
        OracleSecretStore store = null;
        try {
            OracleWallet tenantWallet = new OracleWallet();
            File fCheck = new File(jobDir);
            if (fCheck.exists() && fCheck.isDirectory() && fCheck.canWrite() && fCheck.canRead()) {
                writeToLog(logger, "Job Directory " + jobDir + "exists and writable");
            } else {
                writeToLog(logger, "Error : Job Directory " + jobDir + "does not exists or not read/writable");
            }
            if (!tenantWallet.exists(jobDir)) {
                tenantWallet.createSSO();
                tenantWallet.saveAs(jobDir);
            }
            tenantWallet.open(jobDir, null);
            store = tenantWallet.getSecretStore();
        } catch (IOException io) {
            writeToLog(logger, io.toString());
        } catch (OracleSecretStoreException os) {
            writeToLog(logger, os.toString());
        }
        return store;
    }

    private static void verifyOracleWalletAliasesWithBuilder(ArrayList<SQLcl> sqlclBuilders, String jobDirectory, PrintStream logger) {

        List<String> builderAliases = new ArrayList<String>();
        OracleWallet tenantWallet = new OracleWallet();
        try {
            tenantWallet.setLocation(jobDirectory);
            if (!tenantWallet.exists(jobDirectory)) {
                return;
            }
            for (int i = 0; i < sqlclBuilders.size(); i++) {
                SQLcl sqlclBuilder = (SQLcl) sqlclBuilders.get(i);
                builderAliases.add(sqlclBuilder.getConfigBuilderKey());
            } // Iterator
            // open it and grab the store
            tenantWallet.open(jobDirectory, null);
            OracleSecretStore store = tenantWallet.getSecretStore();

            final Enumeration e = store.internalAliases();
            while (e.hasMoreElements()) {
                final String alias = (String) e.nextElement();
                if (alias.startsWith(LOGIN_CONNECT_PREFIX)) {
                    if (!builderAliases.contains(new String(store.getSecret(alias)))) {
                        writeToLog(logger, "---------- Deleted Aliases in Wallet: " + alias);
                        store.deleteSecret(alias);
                    }
                }
            }
            tenantWallet.setSecretStore(store);
            tenantWallet.save();

        } catch (IOException io) {
            io.printStackTrace();
            writeToLog(logger, io.toString());
        } catch (OracleSecretStoreException os) {
            os.printStackTrace();
            writeToLog(logger, os.toString());
        }
    }

    private static String getJobDirectory(Job job) {
        XmlFile configFile = job.getConfigFile();
        File directory = configFile.getFile().getParentFile();
        String jobDirectory = directory.getPath() + File.separator;
        return jobDirectory;
    }

    static class SqlclCallable implements Callable<Boolean, Exception>, Serializable {

        String sqlFile;
        boolean workspace;
        BuildListener listener;
        String connectString;
        FilePath credentialsZipFile;
        String restrictLevel;
        String sqltext;
        String workspaceDirectory;

        SqlclCallable(String sqlFile, BuildListener listener,
                String sqltext, String connectString, String restrictLevel, FilePath credentialsfile, boolean workspace, String workspaceDir) {
            this.sqlFile = sqlFile;
            this.listener = listener;
            this.sqltext = sqltext;
            this.restrictLevel = restrictLevel;
            this.connectString = connectString;
            this.credentialsZipFile = credentialsfile;
            this.workspace = workspace;
            this.workspaceDirectory = workspaceDir;
        }

        @Override
        public Boolean call() throws Exception {
            RunSQLcl runSQLcl = new RunSQLcl();
            runSQLcl.setTaskListener(listener);
            runSQLcl.setConnectString(connectString);
            runSQLcl.setCurrentDirectory(workspaceDirectory);
            if ( credentialsZipFile != null )
                runSQLcl.setCredentialsfile(this.credentialsZipFile.getRemote());
            else 
                runSQLcl.setCredentialsfile(null);
        
            Connection conn = null;

            try {
                if (restrictLevel == null) {
                    runSQLcl.setRestrictLevel("0");
                } else {
                    runSQLcl.setRestrictLevel(restrictLevel);
                }
                BufferedOutputStream bos = new BufferedOutputStream(listener.getLogger());
                WrapListenBufferOutputStream wrappedBOS = new WrapListenBufferOutputStream(bos, null);
                wrappedBOS.setRemoveForcePrint(true);

                if (connectString != null) {
                    conn = runSQLcl.getConnection();
                    if (conn == null) {
                        if (credentialsZipFile != null) {
                            writeToConsole(listener.getLogger(), " ");
                            writeToConsole(listener.getLogger(), "Please verify the Credentials Zip file and/or Connect String!");
                        } 
                        writeToConsole(listener.getLogger(), "Connection failed!");
                        return false;
                    } else {
                        writeToLog(listener.getLogger(), "Connected");
                    }
                    runSQLcl.getCtx().setOutputStreamWrapper(wrappedBOS);
                    if (this.sqlFile != null) {
                        runSQLcl.runFile("Run File", runSQLcl.getCtx(), sqlFile, workspace);
                    } else {
                        runSQLcl.runScript("Run Script", runSQLcl.getCtx(), sqltext, workspace);
                    }
                } else {

                    if (this.sqlFile != null) {
                        runSQLcl.runFile("Run File", sqlFile, workspace);
                    } else {
                        runSQLcl.runScript("Run Script", sqltext, workspace);
                    }
                }
                // runSQLcl.getCtx().write("Sucessfully Ran SQL File");
                //listener.getLogger().println("END !!!!");
            } catch (Exception ex) {
                writeToLog(listener.getLogger(), ex.toString());
            } finally {
                try {
                    if (conn != null && !conn.isClosed()) {
                        conn.close();
                        writeToLog(listener.getLogger(), " Connection closed!");
                    }
                } catch (SQLException ex) {
                    writeToLog(listener.getLogger(), " Connection close failed!");
                }
            }
            writeToLog(listener.getLogger(), "Log file Ends ----------------");
            return true;
        }
    }

    @Extension
    public static class SQLclSaveListener extends SaveableListener {

        @Override
        public void onChange(Saveable saveable, XmlFile file) {
            if (saveable instanceof FreeStyleProject) {
                FreeStyleProject project = (FreeStyleProject) saveable;
                String jobDirectory = getJobDirectory(project);
                boolean walletFileExists = false;
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                PrintStream stringLogger = new PrintStream(bytes);
                File walletFile = null;

                File fCheck = new File(jobDirectory);
                if (fCheck.exists() && fCheck.isDirectory() && fCheck.canWrite() && fCheck.canRead()) {
                    walletFile = new File(jobDirectory + WALLET_FILE_NAME);
                    if (walletFile.exists()) {
                        walletFileExists = true;
                    } else {
                        walletFileExists = false;
                    }
                    //log.info("SQLclSaveListener-> OnChange : Job Directory " + jobDirectory + "exists and writable");
                } else {

                    //log.error("SQLclSaveListener-> OnChange : Error : Job Directory " + jobDirectory + "does not exists or not read/writable");
                }
                ArrayList<SQLcl> sqlclBuilders = new ArrayList<SQLcl>();

                for (Builder builder : project.getBuilders()) {
                    //log.info("Found builder " + builder);
                    if (builder instanceof SQLcl) {
                        sqlclBuilders.add((SQLcl) builder);
                    }
                }
                if (sqlclBuilders.isEmpty()) {
                    if (walletFileExists) {
                        File walletLockFile = new File(jobDirectory + WALLET_LOCK_FILE_NAME);
                        if (walletLockFile.exists()) {
                            walletLockFile.delete();    
                        }
                        walletFile.delete();        
                    }
                    return;
                }

                for (int i = 0; i < sqlclBuilders.size(); i++) {
                    SQLcl sqlclBuilder = (SQLcl) sqlclBuilders.get(i);
                    String preWalletPassword = sqlclBuilder.getPreWalletPassword();
                    if (preWalletPassword != null) {
                        setWalletPassword(sqlclBuilder, jobDirectory, stringLogger);
                        sqlclBuilder.setPassword();
                        if (bytes.size() != 0) {
                            log.error(bytes.toString());
                        }
                    }
                    // m_ConnectStrJobDir.put(sqlclBuilder.getConfigBuilderKey(), jobDirectory);
                } // Iterator
                verifyOracleWalletAliasesWithBuilder(sqlclBuilders, jobDirectory, stringLogger);
            }
            // log.info(" Out <<<<SQLclSaveListener-> OnChange>>>>");
            super.onChange(saveable, file);
            }
    }
    
    @Extension
    public static class WalletStoreItemListener extends ItemListener {

        @Override
        public void onCopied(Item src, Item target) {
            if (src instanceof Project && target instanceof Project) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                PrintStream stringLogger = new PrintStream(bytes);
                Project srcProject = (Project) src;
                Project targetProject = (Project) target;

                String srcJobDirectory = getJobDirectory(srcProject);
                String tgtJobDirectory = getJobDirectory(targetProject);
                
                boolean srcWalletFileExists = false;
                boolean tgtWalletFileExists = false;
                
                File srcWalletFile = null;
                File tgtWalletFile = null;

                // Not sure if this is the case, if target file already exists, quit
                File tgtFile = new File(tgtJobDirectory);
                if (tgtFile.exists() && tgtFile.isDirectory() && tgtFile.canWrite() && tgtFile.canRead()) {
                    tgtWalletFile = new File(tgtJobDirectory + WALLET_FILE_NAME);
                    if (tgtWalletFile.exists()) {
                        tgtWalletFileExists = true;
                    } else {
                        tgtWalletFileExists = false;
                    }
                }
                
                File srcFile = new File(srcJobDirectory);
                if (srcFile.exists() && srcFile.isDirectory() && srcFile.canWrite() && srcFile.canRead()) {
                    srcWalletFile = new File(srcJobDirectory + WALLET_FILE_NAME);
                    if (srcWalletFile.exists()) {
                        srcWalletFileExists = true;
                    } else {
                        srcWalletFileExists = false;
                    }
                }
                
                if (srcWalletFileExists && (!tgtWalletFileExists) ) {
                    OracleWallet srcTenantWallet = new OracleWallet();
                    OracleWallet tgtTenantWallet = new OracleWallet();
                    try {
                        srcTenantWallet.setLocation(srcJobDirectory);
                        srcTenantWallet.open(srcJobDirectory, null);
                        
                        tgtTenantWallet.createSSO();
                        tgtTenantWallet.saveAs(tgtJobDirectory);
                        tgtTenantWallet.open(tgtJobDirectory, null);

                        OracleSecretStore sourceSstore = srcTenantWallet.getSecretStore();
                        OracleSecretStore targetSstore = tgtTenantWallet.getSecretStore();
                        
                        final Enumeration e = sourceSstore.internalAliases();
                        while (e.hasMoreElements()) {
                            final String alias = (String) e.nextElement();
                            final char[] secret = sourceSstore.getSecret(alias);
                            targetSstore.setSecret(alias, secret);
                        }
                        tgtTenantWallet.setSecretStore(targetSstore);
                        tgtTenantWallet.setLocation(tgtJobDirectory);
                        tgtTenantWallet.save();
                        
                     } catch (IOException io) {
                        writeToLog(stringLogger, io.toString());
                     } catch (OracleSecretStoreException os) {
                        writeToLog(stringLogger, os.toString());
                     }         
                }
            }
            super.onCopied(src, target);
        }
    }

    public boolean isUserInputValid(PrintStream logger) {

        String userName = getUsername();
        String connString = getConnectstring();
        boolean flag = true;
        // User Name validation
        if (userName == null || userName.isEmpty()) {
            writeToLog(logger, "Error : Username is null or empty");
            flag = false;
        }
        // Connect String
        if (connString == null || connString.isEmpty()) {
            writeToLog(logger, "Error : Database connect string is null or empty");
            flag = false;
        }
        return flag;
    }

    public String getPreWalletPassword() {
        return m_preWalletPassword;
    }

    public void setPreWalletPassword(String password) {
        m_preWalletPassword = password;
    }

    private String generateBuilderKey() {
        //generate random UUIDs
        UUID builderKey = UUID.randomUUID();
        return builderKey.toString();
    }

    /**
     * Helper method for writing entries to the Jenkins log.
     *
     * @param msg The message to be written to the Jenkins log.
     */
    private static void writeToLog(PrintStream logger, String msg) {
        if (_debug) {
            logger.println(String.format("[SQLcl] %s", msg));
        }
    }
    
    private static void writeToConsole(PrintStream logger, String msg) {
        logger.println(String.format("%s", msg));
    }

    public static final String SQLFILE_TYPE = "SQLFILE";
    public static final String SQLTEXT_TYPE = "SQLTEXT";
    public static final String SQLFILE_SQLTEXT = "sqlfileortext";
    public static final String CONFIG_PASSWORD = "**********";

    private transient String m_preWalletPassword = null;

    private static final String WALLET_FILE_NAME = "cwallet.sso";
    private static final String WALLET_LOCK_FILE_NAME = "cwallet.sso.lck";

    private static boolean _debug = Boolean.getBoolean(SQLcl.class.getName() + ".debug");

    private static final String LOGIN_PREFIX = "oracle.security.client.";
    private static final String LOGIN_CONNECT_PREFIX = "oracle.security.client.connect_string";
    private static final String LOGIN_USER_PREFIX = "oracle.security.client.username";
    private static final String LOGIN_PW_PREFIX = "oracle.security.client.password";

    //private static HashMap<String, String> m_ConnectStrJobDir = new HashMap<String, String>();
}
