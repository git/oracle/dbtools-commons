/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.examples;

import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author debjanibiswas
 * 
 * This example uses a PreparedStatement to set a URL and 
 * retrieves it from a ResultSet
 *
 */

public class GetURL {
  public static void main(String[] args) {
    // REST JDBC driver name and database URL
    String DRIVER = "oracle.dbtools.jdbc.Driver";  
    String DB_URL = "http://yourmachine.yourdomain:8082/ords/adhocsqltest/";
        
    //  Database credentials
    String USER = "debjani";
    String PASS = "debjani";
        
    Properties cred = new Properties();
    cred.put("user", USER);    
    cred.put("password", PASS);
    
    try {
      Class.forName(DRIVER);
      
      // Open a connection
      Connection conn = DriverManager.getConnection(DB_URL, cred);
      
      String sql = "select ? as col1 from dual";
      String url = "https://www.google.com/";
      
      // Create a statement
      PreparedStatement pstmt = conn.prepareStatement(sql);
      pstmt.setURL(1, new java.net.URL(url));
      
      ResultSet rs = pstmt.executeQuery();
      
      while(rs.next()) {
        java.net.URL rsURL = rs.getURL(1);
        System.out.println(rsURL);
      }
    }
    catch(SQLException | MalformedURLException | ClassNotFoundException e) {
      e.printStackTrace();
    }  
  }
}
