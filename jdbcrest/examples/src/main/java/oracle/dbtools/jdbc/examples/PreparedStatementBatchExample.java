/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.examples;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * @author debjanibiswas
 *
 * This example demonstrates the use of batched parameters with PreparedStatement
 * 
 */
public class PreparedStatementBatchExample {

  public static void main(String[] args) {
    
    // REST JDBC driver name and database URL
    String DRIVER = "oracle.dbtools.jdbc.Driver";  
    String DB_URL = "http://yourmachine.yourdomain:8082/ords/adhocsqltest/";
        
    //  Database credentials
    String USER = "debjani";
    String PASS = "debjani";
        
    Properties cred = new Properties();
    cred.put("user", USER);    
    cred.put("password", PASS);
    
    try {
      Class.forName(DRIVER);
      
      // Open a connection
      Connection conn = DriverManager.getConnection(DB_URL, cred);
      
      String create = "CREATE TABLE T1(col1 number)";
      String insert = "INSERT INTO T1(col1) VALUES (?)";
      String select = "SELECT * FROM T1";
      String drop   = "DROP TABLE T1"; 
      
      Statement stmt = conn.createStatement();
      stmt.execute(create);
      
      PreparedStatement pstmt = conn.prepareStatement(insert);
      
      pstmt.setInt(1, 100);
      pstmt.addBatch();
      pstmt.setInt(1, 101);
      pstmt.addBatch();

      pstmt.setInt(1, 200);
      pstmt.addBatch();

      pstmt.setInt(1, 300);
      pstmt.addBatch();
      pstmt.setInt(1, 305);
      pstmt.addBatch();

      // Overwriting in a batch
      pstmt.setInt(1, 400);
      pstmt.setInt(1, 410);
      pstmt.addBatch();
      
      int[] count = pstmt.executeBatch();  
 
      for(int i =0;i<count.length; i++) {
        System.out.println(count[i]);
      }
      
      // Now select the inserted values using a ResultSet
      ResultSet rs = stmt.executeQuery(select);
      while(rs.next()) {
        System.out.println(rs.getInt(1));
      }
      
      stmt.execute(drop);
    }
    catch(SQLException | ClassNotFoundException e) {
      e.printStackTrace();
  }
}


}
