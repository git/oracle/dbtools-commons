/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.examples;

import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * @author debjanibiswas
 * 
 * This example demonstrates usage of PreparedStatement and ResultSet 
 * with different datatypes in the REST JDBC driver
 *
 */
public class PreparedStatementExample {

  public static void main(String[] args) {
    // REST JDBC driver name and database URL
    String DRIVER = "oracle.dbtools.jdbc.Driver";  
    String DB_URL = "http://yourmachine.yourdomain:8082/ords/adhocsqltest/";
        
    //  Database credentials
    String USER = "debjani";
    String PASS = "debjani";
        
    Properties cred = new Properties();
    cred.put("user", USER);    
    cred.put("password", PASS);
    
    try {
      Class.forName(DRIVER);
      
      // Open a connection
      Connection conn = DriverManager.getConnection(DB_URL, cred);
      String sql = "SELECT  ? as col_NUMBER, ? as col_SHORT, ? as col_DOUBLE, ? as col_VARCHAR, ? as col_FLOAT, ? as col_INTEGER, "
          + "? as col_DATE, ? as col_TIME, ? as col_TIMESTAMP, ? as col_BIGDECIMAL, ? as col_BOOLEANTRUE, ? as col_BOOLEANFALSE, "
          + "? as COL_CLOB FROM DUAL";
      
      PreparedStatement pstmt = conn.prepareStatement(sql);
      
      pstmt.setInt(1, 123);               // col_NUMBER
      pstmt.setShort(2, (short) 1234);    // col_SHORT
      pstmt.setDouble(3, 123456.12312);   // col_DOUBLE
      pstmt.setString(4, "debjani");      // col_VARCHAR
      pstmt.setFloat(5, (float) 1.231);   // col_FLOAT
      pstmt.setInt(6, 6);                 // col_INTEGER

      String testDate = "1970-01-01 02:02:02.561";
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Date utilDate = sdf.parse(testDate);
      java.sql.Date sqlDate             = new java.sql.Date(utilDate.getTime());
      java.sql.Time sqlTime             = new java.sql.Time(utilDate.getTime());
      java.sql.Timestamp sqlTimestamp   = new java.sql.Timestamp(utilDate.getTime());
      
      pstmt.setDate(7, sqlDate);            // col_DATE
      pstmt.setTime(8, sqlTime);            // col_TIME
      pstmt.setTimestamp(9, sqlTimestamp);  // col_TIMESTAMP
      
      BigDecimal pi = new BigDecimal("3.14159265358979323846");
      pstmt.setBigDecimal(10, pi);          // col_BIGDECIMAL
      
      pstmt.setBoolean(11, true);           // col_BOOLEANTRUE
      pstmt.setBoolean(12, false);          // col_BOOLEANFALSE
      
      java.sql.Clob clob = conn.createClob();
      clob.setString(1, "It's a trap!");
      
      pstmt.setClob(13, clob);              // col_CLOB
 
      ResultSet rs = pstmt.executeQuery();
      while(rs.next()) {
        System.out.println("Int: "          +  rs.getInt(1));
        System.out.println("Short: "        +  rs.getShort(2));
        System.out.println("Double: "       +  rs.getDouble(3));
        System.out.println("String: "       +  rs.getString(4));
        System.out.println("Float: "        +  rs.getFloat(5));
        System.out.println("Int: "          +  rs.getInt(6));
        System.out.println("Date: "         +  rs.getDate(7));
        System.out.println("Time: "         +  rs.getTime(8));
        System.out.println("Timestamp: "    +  rs.getTimestamp(9));
        System.out.println("BigDecimal: "   +  rs.getBigDecimal(10));
        System.out.println("BooleanTrue: "  +  rs.getBoolean(11));
        System.out.println("BooleanFalse: " +  rs.getBoolean(12));
        System.out.println("Clob object: "  +  rs.getClob(13));
        Clob clobVal = rs.getClob(13);
        System.out.println("String from Clob: "+ clobVal.getSubString(1, (int) clobVal.length()));
      }
    }
    catch(SQLException | ClassNotFoundException | ParseException e) {
      e.printStackTrace();     
    }
  }
}
