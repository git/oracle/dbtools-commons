/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.orest.examples;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

/**
 * @author totierne
 * 
 * This example demonstrates the basic steps involved in building a JDBC application using the 
 * REST JDBC driver.
 * 
 * It registers the REST JDBC driver using jdbc:oracle:orest:@ for OracleConnection like api,
 * not exercised in this example,
 *  opens a connection with the credentials passed in,
 * executes a query and extracts the data from the ResultSet. 
 * 
 * To run this with the thin driver, you only need to change the driver name 
 * and the database URL
 *
 */

public class CreateStatementExample {

	public static void main(String[] args) {
		
		try {
			Class.forName("oracle.dbtools.jdbc.orest.Driver");
			Properties cred = new Properties();
			cred.put("user", "hr");
			cred.put("password", "hr");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:oracle:orest:http://yourmachine.yourdomain:8082/ords/hr/",cred);
			if (connection != null) {
				Statement stmt = connection.createStatement();
				ResultSet rset = stmt.executeQuery("SELECT first_name FROM employees");
				// Print the name out
				while (rset.next())
					System.out.println(rset.getString(1));
			}
		} catch (ClassNotFoundException ex) {
			System.out.println(ex.getLocalizedMessage());
		} catch (SQLException e) {
			System.out.println(e.getLocalizedMessage());
		}

	}
}
