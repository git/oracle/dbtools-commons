/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.sql.rowset.serial.SerialException;

import oracle.dbtools.jdbc.util.RestJdbcOutputStream;
import oracle.dbtools.jdbc.util.RestJdbcWriter;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.dbtools.jdbc.util.SQLStateMapping;



/**
 * @author debjanibiswas
 *
 */
public class CLOB implements java.sql.Clob {

  /**
   * Global variables
   */
  private long len;

  StringWriter clob = new StringWriter();
  
  /**
   * Constructor for Clob
   */
  public CLOB() {
  }
  
  protected CLOB(String x) {
    if(x!=null) {
      clob.write(x);
      this.len = this.clob.getBuffer().length();
    }
    else {
      clob = null;
    }
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#length()
   */
  @Override
  public long length() throws SQLException {
    return this.len;
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#getSubString(long, int)
   */
  @Override
  public String getSubString(long pos, int length) throws SQLException {
    if(this.length() == 0) {
      return "";
    }
    if (pos < 1 || pos > this.length()) {
      int error_code = 17068;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17068),
                                SQLStateMapping.getSQLState(error_code), error_code);
    }

    if((length > this.length()) || (pos-1) + length > this.length()){
      length = (int) this.length();
    }
    try {
        int start = (int) pos -1;
        int end   = start + length;
        return clob.getBuffer().substring(start , end);
  
    } catch (StringIndexOutOfBoundsException e) {
        throw new SQLException("StringIndexOutOfBoundsException: " +
            e.getMessage());
    }
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#getCharacterStream()
   */
  @Override
  public Reader getCharacterStream() throws SQLException {
    
    return getCharacterStream(1, this.clob.getBuffer().length());
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#getAsciiStream()
   */
  @Override
  public InputStream getAsciiStream() throws SQLException {
    InputStream stream = new ByteArrayInputStream(String.valueOf(clob.getBuffer()).getBytes(StandardCharsets.UTF_8));
    return stream;
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#position(java.lang.String, long)
   */
  @Override
  public long position(String searchstr, long start) throws SQLException {
    
    // Position must not be less than 1 nor greater than the length of the Clob
    if (start < 1 || start > len) {
      return -1;
    }
    
    // returns the first occurrence of the searchstr starting at index start
    // have to add 1 because string index starts with 0
   
    return (clob.getBuffer().indexOf(searchstr, (int) start-1)) + 1;
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#position(java.sql.Clob, long)
   */
  @Override
  public long position(java.sql.Clob searchstr, long start)
      throws SQLException {
  
    return position(searchstr.getSubString(1,(int)searchstr.length()), start);
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#setString(long, java.lang.String)
   */
  @Override
  public int setString(long pos, String str) throws SQLException {
    
    return (setString(pos, str, 0, str.length()));
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#setString(long, java.lang.String, int, int)
   */
  @Override
  public int setString(long pos, String str, int offset, int length)
      throws SQLException {
         
    if (offset < 0 || offset > str.length()) {
      throw new SQLException("Invalid offset in byte array set");
    }
        
    if (pos < 1 || pos > (this.length()+1)) {
      throw new SQLException("Invalid position in CLOB object set");
    }

    if ((offset+length) > str.length()) {
      // need check to ensure length + offset !> bytes.length
      throw new SQLException("Invalid OffSet. Cannot have combined offset " +
          " and length that is greater that the Clob buffer");
    }
    
    // get the characters to be written
    String toWrite = str.substring(offset, offset+length);
    int start = (int) pos-1;
    int end   = start + toWrite.length();
    
    // write the characters to the clob
    clob.getBuffer().replace(start, end, toWrite);
 
    
    // update the length of the new clob
    this.len = clob.getBuffer().length();    
    
    // return the number of characters written
    return (start - end);
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#setAsciiStream(long)
   */
  @Override
  public OutputStream setAsciiStream(long pos) throws SQLException {    
    return new RestJdbcOutputStream(this, pos);
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#setCharacterStream(long)
   */
  @Override
  public Writer setCharacterStream(long pos) throws SQLException {
    return new RestJdbcWriter(this,pos);
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#truncate(long)
   */
  @Override
  public void truncate(long length) throws SQLException {
    
    if (length > this.len) {
      throw new SerialException
         ("Length more than what can be truncated");
   } else {
        this.len = length;        
        // re-size the clob
        if (this.len == 0) {
          // clear contents if len = 0
          clob.getBuffer().setLength(0);
        } else {
          clob.getBuffer().setLength((int) length);        
        }
   }
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#free()
   */
  @Override
  public void free() throws SQLException {
    this.truncate(0);
    this.len = 0;
  }

  /* (non-Javadoc)
   * @see java.sql.Clob#getCharacterStream(long, long)
   */
  @Override
  public Reader getCharacterStream(long pos, long length) throws SQLException {
    int start = (int) pos -1;
    int end   = start + (int) length;
    return new StringReader(clob.getBuffer().substring(start, end));
  }
}
