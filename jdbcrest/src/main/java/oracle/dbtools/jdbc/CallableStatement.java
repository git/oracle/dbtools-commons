/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import oracle.dbtools.jdbc.util.DateTimestampsUtil;
import oracle.dbtools.jdbc.util.OracleTypes;
import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.jdbc.util.RestJdbcUnsupportedException;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.dbtools.jdbc.util.SQLStateMapping;

/**
 * @author <mailto:...> debjani.biswas@oracle.com
 */
public class CallableStatement extends oracle.dbtools.jdbc.PreparedStatement implements java.sql.CallableStatement {
  
  // The logger
  Logger LOGGER = Logger.getLogger(oracle.dbtools.jdbc.CallableStatement.class.getName());
  
  // The current ResultSet
  private ResultSet currentResultSet = null; 
 
  // Flag to indicate if this CallableStatement is open or closed
  private boolean closed = false;
  
  // For out paramters
  private JsonNode node = null;
  
  String timeZoneID = null;
    
  private ObjectNode outResp = JsonNodeFactory.instance.objectNode();
 
  // For ParameterMetaData
  private Map<Integer, Integer> isNullable          = new HashMap<Integer, Integer>();
  private Map<Integer, Boolean> isSigned            = new HashMap<Integer, Boolean>(); 
  private Map<Integer, Integer> precision           = new HashMap<Integer, Integer>();
  private Map<Integer, Integer> scale               = new HashMap<Integer, Integer>();
  private Map<Integer, Integer> parameterType       = new HashMap<Integer, Integer>();
  private Map<Integer, String>  parameterTypeName   = new HashMap<Integer, String>();
  private Map<Integer, String>  parameterClassName  = new HashMap<Integer, String>();
  
  protected CallableStatement(oracle.dbtools.jdbc.Connection conn, final String sql) {
    this.conn = conn;
    this.sql = sql;
  }

   
  /**
   * Helper method to extract value based on the parameterIndex/parameterName
   * This method returns a string to get a proper exception message from getXXX methods 
   * The getXXX methods then parse the String 
   * @return String value
   * @throws SQLException
   */
  private String getVal(Object parameterIndex) throws SQLException {
    String value = null;
    try {
      // If the column is null
      if(extractResult(parameterIndex).findValue("result").isNull()) {
        return null;
      }
      else {        
        value = extractResult(parameterIndex).findValue("result").asText();
      }                          
    }
    // If the column Index is invalid
    catch(NullPointerException ne) {
      // Get the detailed message from the Exception
      if(ne.getMessage()!=null) {
        LOGGER.log(Level.SEVERE, ne.getMessage()); 
      }
      // Invalid column index exception
      int error_code = 17003;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17003), 
                             SQLStateMapping.getSQLState(error_code), error_code);      
    } 
    return value; 
  }

  /**
   * Helper method to extract the JsonNode from the response from ORDS
   * based on parameterIndex or parameterName
   * @return JsonNode at the parameterIndex / parameterName
   * @throws SQLException
   */
  private JsonNode extractResult(Object parameterIndex) throws SQLException{
    ensureOpen();
    int i =0;
    JsonNode result = null;
 
    this.timeZoneID = this.node.get("env").get("defaultTimeZone").asText();
    
    // Iterate through the array to retrieve the result at the given parameterIndex
    while(true) {
      result = this.node.get("items").get(0).get("binds").get(i);
      
      // Reached the end of the results or the parameterIndex is invalid
      if(result == null) {
        handleErrors(this.node); 
        
        // If there isn't a "binds" section in the Json response from ORDS
        if(this.node.get("items").get(0).findValue("binds") == null) {
          // Exception: ORA_17426=Both IN and OUT binds are NULL 
          int errorCode = 17426;
          throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17426), 
              SQLStateMapping.getSQLState(errorCode), errorCode);
        }
        // ORA_17046=Internal error: Invalid index for data access
        else if(outTypes.containsKey(parameterIndex)) {
          int errorCode = 17046;
          throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17046), 
              SQLStateMapping.getSQLState(errorCode), errorCode);
        }
        // If the outparameter hasn't been registered
        // ORA_17041=Missing IN or OUT parameter at index:
        else if(!outTypes.containsKey(parameterIndex)) {
          int errorCode = 17041;
          String errMsg = RestjdbcResources.getString(RestjdbcResources.ORA_17041) + parameterIndex;
          throw new SQLException(errMsg, SQLStateMapping.getSQLState(errorCode), errorCode);    
        }
        
        // We've reached the end of our results and haven't received a response
        // ORA_17001=Internal Error
        else {
          int errorCode = 17001;
          throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17001), 
              SQLStateMapping.getSQLState(errorCode), errorCode);
        }        
      }
      
      // If we have an int index
      if(parameterIndex instanceof Integer && (result.findValue("index")!=null)) {
          if(result.get("index").asInt() == (int) parameterIndex) {       
            return result;
          }
      }
      
      // If we have a String parameterName 
      else if ((parameterIndex instanceof String) && (result.findValue("name")!=null)) {
        if(result.get("name").asText().equals((String) parameterIndex)) {
          return result;
        }
      }          
      // increment the index 
      i++;
    }
    
  }
  
  /* (non-Javadoc)
   * @see java.sql.PreparedStatement#executeQuery()
   * Return a ResultSet
   */
  @Override
  public ResultSet executeQuery() throws SQLException {
    CloseableHttpResponse response = executeInternal(this.sql);
    this.currentResultSet = new oracle.dbtools.jdbc.ResultSet(response, this, this.conn);
    return currentResultSet;
  }
  
  /* (non-Javadoc)
   * @see java.sql.Statement#executeQuery(java.lang.String)
   */
  // TODO: result set for cursor
  @Override
  public ResultSet executeQuery(String sql) throws SQLException {
    CloseableHttpResponse response = executeInternal(sql);
    this.currentResultSet = new oracle.dbtools.jdbc.ResultSet(response, this, this.conn);
    return currentResultSet;
  }
  

  @Override
  public int executeUpdate() throws SQLException {
    int result = 0;
    this.node = null; // re-initialize
    CloseableHttpResponse response = null;
    try {
      response = executeInternal(this.sql); 
      this.node = createJsonNode(response);
      handleErrors(this.node); 
      result= getResult(this.node);
    }
    finally {
      if(response!=null) {
        try {
          response.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
      }         
    }
    return result;
  }
  
  @Override
  public boolean execute(String sql) throws SQLException{
    boolean result = false;
    CloseableHttpResponse response = null;
    
    try {      
      response = executeInternal(sql);
      this.node = createJsonNode(response);
      // Is the result a ResultSet object?
      if(node.findValue("items").get(0).findValue("resultSet")!=null) {
        result = true;
      }      
      // are there any errors?
      handleErrors(this.node); 
    }
    finally {
      if(response!=null) {
        try {
          response.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
        }
      }         
    }   
    return result;
  }
  

  @Override
  public boolean execute() throws SQLException {
    boolean result = false; 
    CloseableHttpResponse response = null;
    try {
      response = executeInternal(this.sql);      
      this.node = createJsonNode(response);
      // Is the result a ResultSet object?
      if(node.findValue("items").get(0).findValue("resultSet")!=null) {
        result = true;
      }
      handleErrors(this.node);
    }
    finally {
      if(response!=null) {
        try {
          response.close();
        } catch (IOException e) {
          LOGGER.log(Level.SEVERE, e.getMessage());
        }
      }
    }
    return result;
  }
  
  
  /**
   * ****************************** All set methods *****************************
   */

  
  
  /* (non-Javadoc)
   * @see java.sql.PreparedStatement#setNull(int, int)
   */
  @Override
  public void setNull(int parameterIndex, int sqlType) throws SQLException {
    super.setNull(parameterIndex,sqlType);
  }

 

  /* (non-Javadoc)
   * @see java.sql.PreparedStatement#setByte(int, byte)
   */
  @Override
  public void setByte(int parameterIndex, byte x) throws SQLException {
    // TODO Auto-generated method stub

  }
  
  /**
   * Posts a pre-formatted date using oracle.dbtools.jdbc.util.DateTimestampsUtil
   */
  @Override
  public void setDate(int parameterIndex, Date x) throws SQLException {
    LOGGER.finest("Setting Date");
   
    DateTimestampsUtil ts = new DateTimestampsUtil();
    // Get a calendar with the client timezone
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    updateList(parameterIndex, ts.setDate(x, this.conn.getTimeZoneID(),cal)); 
    types.put(parameterIndex, "DATE");
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setDate(java.lang.String, java.sql.Date)
   */
  @Override
  public void setDate(String parameterName, Date x) throws SQLException {
    ensureOpen();
    LOGGER.finest("Setting Date with parameterName: " + parameterName);
    
    // Get a calendar with the client timezone
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterName, ts.setDate(x,this.conn.getTimeZoneID() , cal));
    types.put(parameterName, "DATE");
  }

  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setDate(java.lang.String, java.sql.Date, java.util.Calendar)
   */
  @Override
  public void setDate(String parameterName, Date x, Calendar cal)
      throws SQLException {
    LOGGER.finest("Setting Date");
    
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterName, ts.setDate(x,this.conn.getTimeZoneID(),cal));
    types.put(parameterName, "DATE");
  }
  
  
  /**
   * Posts a pre-formatted time using oracle.dbtools.jdbc.util.DateTimestampsUtil 
   */
  @Override
  public void setTime(int parameterIndex, Time x) throws SQLException {
    LOGGER.finest("Setting Time");
    
    // Get the timeZone
    String timeZoneID = this.conn.getTimeZoneID();
    // Get a calendar with the client timezone
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterIndex, ts.setTime(x, timeZoneID,cal));
    types.put(parameterIndex, "DATE");
  }
  

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setTime(java.lang.String, java.sql.Time)
   */
  @Override
  public void setTime(String parameterName, Time x) throws SQLException {
    ensureOpen();
    LOGGER.finest("Setting Time with parameterName: " + parameterName);
    // Get the timeZone
    String timeZoneID = this.conn.getTimeZoneID();
    
    // Get a calendar with the client timezone
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterName, ts.setTime(x, timeZoneID,cal));
    types.put(parameterName, "TIMESTAMP");

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setTime(java.lang.String, java.sql.Time, java.util.Calendar)
   */
  @Override
  public void setTime(String parameterName, Time x, Calendar cal)
      throws SQLException {
    LOGGER.finest("Setting Time");
    
    // Get the timeZone
    String timeZoneID = this.conn.getTimeZoneID();    
    
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterName, ts.setTime(x, timeZoneID,cal));
    types.put(parameterName, "DATE");
  }
  
  
  /**
   * Posts a pre-formatted timestamp using oracle.dbtools.jdbc.util.DateTimestampsUtil
   */
  @Override
  public void setTimestamp(int parameterIndex, Timestamp x)
      throws SQLException {
    
    LOGGER.finest("Setting Timestamp");
    
    // Get the timeZone
    String timeZoneID = this.conn.getTimeZoneID();
    
    // Get a calendar with the client timezone
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterIndex, ts.setTimeStamp(x, timeZoneID,cal));
    types.put(parameterIndex, "TIMESTAMP");
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setTimestamp(java.lang.String, java.sql.Timestamp)
   */
  @Override
  public void setTimestamp(String parameterName, Timestamp x)
      throws SQLException {
    ensureOpen();
    LOGGER.finest("Setting Timestamp with parameterName: " + parameterName);
    // Get a calendar with the client timezone
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    
    // Get the timeZone
    String timeZoneID = this.conn.getTimeZoneID();
    
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterName, ts.setTimeStamp(x, timeZoneID, cal));
    types.put(parameterName, "TIMESTAMP");
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setTimestamp(java.lang.String, java.sql.Timestamp, java.util.Calendar)
   */
  @Override
  public void setTimestamp(String parameterName, Timestamp x, Calendar cal)
      throws SQLException {
    LOGGER.finest("Setting Timestamp");
    
    // Get the timeZone
    String timeZoneID = this.conn.getTimeZoneID();
   
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterName, ts.setTimeStamp(x, timeZoneID,cal));
    types.put(parameterName, "TIMESTAMP");
  }
  

  /* (non-Javadoc)
   * @see java.sql.PreparedStatement#setRowId(int, java.sql.RowId)
   */
  @Override
  public void setRowId(int parameterIndex, RowId x) throws SQLException {
    LOGGER.finest("Setting RowID");    
    updateList(parameterIndex, x.toString());
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setRowId(java.lang.String, java.sql.RowId)
   */
  @Override
  public void setRowId(String parameterName, RowId x) throws SQLException {
    LOGGER.finest("Setting RowID");    
    updateList(parameterName, x.toString());
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setURL(java.lang.String, java.net.URL)
   */
  @Override
  public void setURL(String parameterName, URL val) throws SQLException {
    LOGGER.finest("Setting URL");
    updateList(parameterName, val.toString());
    types.put(parameterName, "VARCHAR2");
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setBoolean(java.lang.String, boolean)
   */
  @Override
  public void setBoolean(String parameterName, boolean x) throws SQLException {
    LOGGER.finest("Setting Boolean with parameterName: " + parameterName);
    updateList(parameterName,x);
    types.put(parameterName, "BOOLEAN");
  }
  
  @Override
  public void setInt(String parameterName, int x) throws SQLException {
    ensureOpen();
    LOGGER.finest("Setting Int for parameterName: " + parameterName );
    updateList(parameterName, x);
    types.put(parameterName, "INTEGER");    
  }

  
  @Override
  public void setShort(String parameterName, short x) throws SQLException {
    ensureOpen();
    LOGGER.finest("Setting Short for parameterName: " + parameterName );
    updateList(parameterName, x);
    types.put(parameterName, "NUMBER");   
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setLong(java.lang.String, long)
   */
  @Override
  public void setLong(String parameterName, long x) throws SQLException {
    ensureOpen();
    LOGGER.finest("Setting Long with parameterName: " + parameterName);
    updateList(parameterName, x);
    types.put(parameterName, "NUMBER");
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setFloat(java.lang.String, float)
   */
  @Override
  public void setFloat(String parameterName, float x) throws SQLException {
    ensureOpen();
    LOGGER.finest("Setting Float with parameterName: " + parameterName);
    updateList(parameterName, x);
    types.put(parameterName, "FLOAT");

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setDouble(java.lang.String, double)
   */
  @Override
  public void setDouble(String parameterName, double x) throws SQLException {
    ensureOpen();
    LOGGER.finest("Setting Double with parameterName: " + parameterName);
    updateList(parameterName, x);
    types.put(parameterName, "NUMBER");
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setBigDecimal(java.lang.String, java.math.BigDecimal)
   */
  @Override
  public void setBigDecimal(String parameterName, BigDecimal x)
      throws SQLException {
    ensureOpen();
    LOGGER.finest("Setting BigDecimal with parameterName: " + parameterName);
    updateList(parameterName, x);
    types.put(parameterName, "BIGDECIMAL");

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setString(java.lang.String, java.lang.String)
   */
  @Override
  public void setString(String parameterName, String x) throws SQLException {
    ensureOpen();
    LOGGER.finest("Setting String with parameterName: " + parameterName);
    updateList(parameterName, x);
    types.put(parameterName, "VARCHAR2");
  }

  /* (non-Javadoc)
   * @see java.sql.PreparedStatement#clearParameters()
   */
  @Override
  public void clearParameters() throws SQLException {
    types.clear();
    data.clear();
    outTypes.clear();
    this.outParams = false;
    this.node = null;
  }

  /*** Unimplemented setXXX methods of CallableStatement ***/
  
  @Override
  public ParameterMetaData getParameterMetaData() throws SQLException {
    int parameterCount = this.types.size() + outTypes.size(); // the number of parameters that have been set

    int[] parameterMode = new int[parameterCount];
    
    for(int i = 0; i < parameterCount; i++)
    {      
      parameterMode[i]      = java.sql.ParameterMetaData.parameterModeIn;
    }
    
    return new oracle.dbtools.jdbc.ParameterMetaData(isNullable, isSigned, precision, scale,
                                       parameterType, parameterTypeName, parameterClassName, 
                                       parameterMode, parameterCount);
  }


  /**
   * Helper method to ensure that a CallableStatement is not closed
   * @throws SQLException
   */
  protected final void ensureOpen() throws SQLException{
     // has the statement been closed?
    if(closed){
      // Throw Exception "Closed Statement"
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17009));
    }
    // If the connection is closed
    if(conn == null) {
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17008));      
    }
  }

  /* (non-Javadoc)
   * @see java.sql.Statement#close()
   * Closes the CallableStatement and relieves associated resources
   */
  @Override
  public void close() throws SQLException {
    synchronized(conn) {
      this.closed = true;    
      if (currentResultSet!=null) {
        currentResultSet = null;
     }
      outTypes = null;
      data = null;
      types = null;
      this.outParams = false;
      this.sql = null;      
    } // end synchronized
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setNull(java.lang.String, int)
   */
  @Override
  public void setNull(String parameterName, int sqlType) throws SQLException {
    LOGGER.finest("Setting Null");
    updateList(parameterName,  null);
    types.put(parameterName, OracleTypes.getSQLTypeName(sqlType));
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setClob(java.lang.String, java.sql.Clob)
   */
  @Override
  public void setClob(String parameterName, Clob x) throws SQLException {
    LOGGER.finest("Setting Clob");
    updateList(parameterName, x.getSubString(1, (int) x.length()));
    types.put(parameterName, "CLOB");
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setByte(java.lang.String, byte)
   */
  @Override
  public void setByte(String parameterName, byte x) throws SQLException {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setBytes(java.lang.String, byte[])
   */
  @Override
  public void setBytes(String parameterName, byte[] x) throws SQLException {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setAsciiStream(java.lang.String, java.io.InputStream, int)
   */
  @Override
  public void setAsciiStream(String parameterName, InputStream x, int length)
      throws SQLException {
   
    setAsciiStream(parameterName, x, (long)length);
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setBinaryStream(java.lang.String, java.io.InputStream, int)
   */
  @Override
  public void setBinaryStream(String parameterName, InputStream x, int length)
      throws SQLException {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object, int, int)
   */
  @Override
  public void setObject(String parameterName, Object x, int targetSqlType,
      int scale) throws SQLException {
    
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object, int)
   */
  @Override
  public void setObject(String parameterName, Object x, int targetSqlType)
      throws SQLException {

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object)
   */
  @Override
  public void setObject(String parameterName, Object x) throws SQLException {
    // The simple data types
    if(x instanceof Boolean) {
      setBoolean(parameterName, (boolean) x);
    }
    else if(x instanceof Integer) {
      setInt(parameterName, (int) x);
    }
    else if(x instanceof Short) {
      setShort(parameterName, (short) x);
    }
    else if(x instanceof Long) {
      setLong(parameterName, (long) x);
    }
    else if(x instanceof Float) {
      setFloat(parameterName, (float) x);
    }
    else if(x instanceof Double) {
      setDouble(parameterName, (double) x);
    }
    else if(x instanceof String) {
      setString(parameterName, (String) x);
    }
    else if(x instanceof java.sql.Date) {
      setDate(parameterName, (Date) x);
    }
    else if(x instanceof java.sql.Time) {
      setTime(parameterName, (Time) x);
    }
    else if(x instanceof java.sql.Timestamp) {
      setTimestamp(parameterName, (Timestamp) x);
    }
    else if(x instanceof BigDecimal) {
      setBigDecimal(parameterName, (BigDecimal) x);
    }
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setCharacterStream(java.lang.String, java.io.Reader, int)
   */
  @Override
  public void setCharacterStream(String parameterName, Reader reader,
      int length) throws SQLException {
    

  }

  

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setNull(java.lang.String, int, java.lang.String)
   */
  @Override
  public void setNull(String parameterName, int sqlType, String typeName)
      throws SQLException {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setBlob(java.lang.String, java.sql.Blob)
   */
  @Override
  public void setBlob(String parameterName, Blob x) throws SQLException {
    // TODO Auto-generated method stub

  }



  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setAsciiStream(java.lang.String, java.io.InputStream, long)
   */
  @Override
  public void setAsciiStream(String parameterName, InputStream x, long length)
      throws SQLException {
    
    try { 
      if(length < 0) {
        int error_code = 17068;
        String message = RestjdbcResources.getString(RestjdbcResources.ORA_17068) + ": " + 
                         RestjdbcResources.getString(RestjdbcResources.RESTJDBC_019);
        throw new SQLException(message, SQLStateMapping.getSQLState(error_code), error_code);  
      }
      StringBuilder sb = new StringBuilder();
      long bytesRead = 0; // keeps count of the number of bytes read
      
      /*
       * A note on performance and memory: One could also use the x.read(byte, int, int) 
       * to read the length of bytes into an array but java.io.InputStream 
       * reads the stream into a byte array in a loop as well. Additionally, we 
       * don't want an extra memory overhead by storing it into a byte[] array
       * as we will ultimately send a string upto adhoc.
       */
      while(bytesRead < length) {
        int bytes = x.read();
        // Reached the end of the stream
        if(bytes < 0) {
          break;
        }
        else {
          sb.append((char)bytes);
        }
        bytesRead ++;
      }
      updateList(parameterName, sb.toString());
      types.put(parameterName, "VARCHAR");
    }
    catch(SQLException e) {
      throw e;
    }
    catch(IOException ioe) {
      LOGGER.log(Level.SEVERE, ioe.getMessage());
    }

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setBinaryStream(java.lang.String, java.io.InputStream, long)
   */
  @Override
  public void setBinaryStream(String parameterName, InputStream x, long length)
      throws SQLException {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setCharacterStream(java.lang.String, java.io.Reader, long)
   */
  @Override
  public void setCharacterStream(String parameterName, Reader reader,
      long length) throws SQLException {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setAsciiStream(java.lang.String, java.io.InputStream)
   */
  @Override
  public void setAsciiStream(String parameterName, InputStream x)
      throws SQLException {
    
    try {      
      StringBuilder sb = new StringBuilder();
      int bytes = 0;
      
      // Read until the end of the Stream
      while(bytes != -1) {
         bytes = x.read();
        // Reached the end of the stream
        if(bytes < 0) {
          break;
        }
        else {
          sb.append((char)bytes);
        }
      }

      updateList(parameterName,  sb.toString());
      types.put(parameterName, "CLOB");
    }   
    catch(IOException ioe) {
      LOGGER.log(Level.SEVERE, ioe.getMessage());
    }
  }


  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setBinaryStream(java.lang.String, java.io.InputStream)
   */
  @Override
  public void setBinaryStream(String parameterName, InputStream x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "setBinaryStream(String parameterName, InputStream x)", 
                                                    this.getClass().getSimpleName()));    

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setCharacterStream(java.lang.String, java.io.Reader)
   */
  @Override
  public void setCharacterStream(String parameterName, Reader reader)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "setCharacterStream(String parameterName, Reader reader)", 
                                                    this.getClass().getSimpleName()));    

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setNCharacterStream(java.lang.String, java.io.Reader)
   */
  @Override
  public void setNCharacterStream(String parameterName, Reader value)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "setNCharacterStream(String parameterName, Reader value)", 
                                                    this.getClass().getSimpleName()));    

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setClob(java.lang.String, java.io.Reader)
   */
  @Override
  public void setClob(String parameterName, Reader reader) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "setClob(String parameterName, Reader reader)", 
                                                    this.getClass().getSimpleName()));    

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setBlob(java.lang.String, java.io.InputStream)
   */
  @Override
  public void setBlob(String parameterName, InputStream inputStream)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "setBlob(String parameterName, InputStream inputStream)", 
                                                    this.getClass().getSimpleName()));    
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setNClob(java.lang.String, java.io.Reader)
   */
  @Override
  public void setNClob(String parameterName, Reader reader)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "setNClob(String parameterName, Reader reader)", 
                                                     this.getClass().getSimpleName()));    
  }

  /**
   * ******************** All the get methods *****************************

  /* (non-Javadoc)
   * @see java.sql.Statement#getConnection()
   */
  @Override
  public Connection getConnection() throws SQLException {   
    return this.conn;
  }


  
  @Override
  public void registerOutParameter(int parameterIndex, int sqlType)
      throws SQLException {
    // ensure the connection and the statement is open
    ensureOpen();
    
    outTypes.put(parameterIndex,sqlType);
    this.outParams = true; // There are out Parameters.
  }
  
  //Internal method to get information of binds
  protected Map<Object,Integer> getOutTypes() {
    return this.outTypes;
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#registerOutParameter(int, int, int)
   */
  @Override
  public void registerOutParameter(int parameterIndex, int sqlType, int scale)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "registerOutParameter(int parameterIndex, int sqlType, int scale)", 
                                                    this.getClass().getSimpleName()));    
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#wasNull()
   */
  @Override
  public boolean wasNull() throws SQLException {
    // TODO Auto-generated method stub
    return false;
  }

  /**
   * Validator for gets
   * input: parameterPosition - could be parameterIndex or a parameterName
   * Checks to see if the parameter index from a get matches a registered one.
   * Checks to see if the parameter name from a get matches a registered one.
   * OUT parameter
   * @throws Exception 
   */
  private void getIndexValidator(Object parameterIndex) throws SQLException {
    
    // Is it an index or a name?
    if(parameterIndex instanceof Integer || parameterIndex instanceof String) {
      // If the parameter Index hasn't been registered
      if(!outTypes.containsKey(parameterIndex)) {
        
        if(parameterIndex instanceof Integer) {
          int errorCode = 17310;
          // ORA_17310=Invalid parameter index
          throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17310), 
              SQLStateMapping.getSQLState(errorCode), errorCode);  
        }
        // RESTJDBC_015=Invalid parameter name
        else if (parameterIndex instanceof String) {
          int errorCode = 17310;
          throw new SQLException(RestjdbcResources.getString(RestjdbcResources.RESTJDBC_015), 
              SQLStateMapping.getSQLState(errorCode), errorCode); 
        }
        
        // ORA_17323=Invalid parameter type
        else {
          int errorCode = 17323;
          throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17323), 
              SQLStateMapping.getSQLState(errorCode), errorCode); 
        }
      }
    }
  }
 
  
  /** (non-Javadoc)
   * @see java.sql.CallableStatement#getString(int)
   * 
   * This method will return a string representation of the object
   */
  @Override
  public String getString(int parameterIndex) throws SQLException {
    int error_code = 17003;
    try {
      JsonNode parameterResult = extractResult(parameterIndex);
      if (parameterResult != null) {
        JsonNode resultJsonNode = parameterResult.findValue("result");
        if (resultJsonNode != null) {
          return resultJsonNode.asText();
        }
      }
      //if we haven't returned by now, then there is a problem
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17003), SQLStateMapping.getSQLState(error_code), error_code);
    } catch (SQLException e) {
      throw e;
    } catch (Exception e) {
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17003), SQLStateMapping.getSQLState(error_code), error_code);
    }
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getBoolean(int)
   */
  @Override
  public boolean getBoolean(int parameterIndex) throws SQLException {
    JsonNode result = extractResult(parameterIndex).findValue("result");
    
    // TODO: throw an exception if it can't be converted to a boolean    
    return result.asBoolean();
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getByte(int)
   */
  @Override
  public byte getByte(int parameterIndex) throws SQLException {
    return 0;
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getShort(int)
   */
  @Override
  public short getShort(int parameterIndex) throws SQLException {
    ensureOpen();
    try { 
      if(getVal(parameterIndex)==null) {
        return 0;
      }
      else {   
        // we just want to truncate / cast double value to integers  
        return (short) Double.parseDouble(getVal(parameterIndex));
      } 
    }
     
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }    
  }


  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getInt(int)
   */
  @Override
  public int getInt(int parameterIndex) throws SQLException {
    try {
      if(getVal(parameterIndex)==null) {
        return 0;
      }
      else {   
        // we just want to truncate / cast double value to integers
        return (int) Double.parseDouble(getVal(parameterIndex));
      }      
    }
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }     
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getLong(int)
   */
  @Override
  public long getLong(int parameterIndex) throws SQLException {
    ensureOpen();
    try {
      if(getVal(parameterIndex)==null) {
        return 0;
      }
      else {   
        // we just want to truncate / cast double value to integers
        return (long) Long.parseLong(getVal(parameterIndex));
      }              
    }    
    
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }         
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getFloat(int)
   */
  @Override
  public float getFloat(int parameterIndex) throws SQLException {
    ensureOpen();
    try {      
      if(getVal(parameterIndex)==null) {
        return (float) 0;
      }
      else {   
        return Float.parseFloat(getVal(parameterIndex));
      }      
    }    
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }   
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getDouble(int)
   */
  @Override
  public double getDouble(int parameterIndex) throws SQLException {
    ensureOpen();
    try {   
      if(getVal(parameterIndex)==null) {
        return (double) 0;
      }
      else {   
        return Double.parseDouble(getVal(parameterIndex));
      } 
    }
    
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }   
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getBigDecimal(int)
   */
  @Override
  public BigDecimal getBigDecimal(int parameterIndex) throws SQLException {
    // If this method is called on a closed CallableStatement
    if(closed) {
      int error_code = 17009;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17009), 
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }
    try {
      return new BigDecimal(extractResult(parameterIndex).findValue("result").asText());
    }
    // Can't be converted to a BigDecimal 
    // Invalid Column Type exception
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }
    catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getCause().toString());
      throw new SQLException(RestjdbcResources.get(RestjdbcResources.RESTJDBC_010));
    }    
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getBigDecimal(int, int)
   */
  @Override
  public BigDecimal getBigDecimal(int parameterIndex, int scale)
      throws SQLException {
    return null;
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getBytes(int)
   */
  @Override
  public byte[] getBytes(int parameterIndex) throws SQLException {
    // TODO Auto-generated method stub
    return null;
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getDate(int)
   */
  @Override
  public Date getDate(int parameterIndex) throws SQLException {
    ensureOpen();
    try {    
      DateTimestampsUtil dtsUtil = new DateTimestampsUtil();
      return dtsUtil.getDate(getVal(parameterIndex), this.timeZoneID);
    }
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
          SQLStateMapping.getSQLState(error_code), error_code);
    }    
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getTime(int)
   */
  @Override
  public Time getTime(int parameterIndex) throws SQLException {
    ensureOpen();
    try {    
      Calendar cal = Calendar.getInstance(TimeZone.getDefault());
      DateTimestampsUtil dtsUtil = new DateTimestampsUtil();      
      return dtsUtil.getTime(getVal(parameterIndex), this.timeZoneID,cal);   
    }
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
          SQLStateMapping.getSQLState(error_code), error_code);
    }
    // SQLException
    catch (SQLException e) {
      if(e.getMessage()!=null){
        throw new SQLException(e.getMessage(), e.getSQLState(), e.getErrorCode());
      }
    }
    // Internal Error  -- should not go here
    throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17001), 
                           SQLStateMapping.getSQLState(17001), 17001);
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getTimestamp(int)
   */
  @Override
  public Timestamp getTimestamp(int parameterIndex) throws SQLException {
    ensureOpen();
    try {
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getTimestamp(getVal(parameterIndex), this.timeZoneID);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
          SQLStateMapping.getSQLState(error_code), error_code);
    }
  }

  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getDate(java.lang.String)
   */
  @Override
  public Date getDate(String parameterName) throws SQLException {
    ensureOpen();
    try {    
      DateTimestampsUtil dtsUtil = new DateTimestampsUtil();
      return dtsUtil.getDate(getVal(parameterName), "PST8PDT");
    }
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
          SQLStateMapping.getSQLState(error_code), error_code);
    }    
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getTime(java.lang.String)
   */
  @Override
  public Time getTime(String parameterName) throws SQLException {
    ensureOpen();
    try {    
      Calendar cal = Calendar.getInstance(TimeZone.getDefault());
      DateTimestampsUtil dtsUtil = new DateTimestampsUtil();      
      return dtsUtil.getTime(getVal(parameterName), this.timeZoneID,cal);   
    }
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
          SQLStateMapping.getSQLState(error_code), error_code);
    }
    // SQLException
    catch (SQLException e) {
      if(e.getMessage()!=null){
        throw new SQLException(e.getMessage(), e.getSQLState(), e.getErrorCode());
      }
    }
    // Internal Error  -- should not go here
    throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17001), 
                           SQLStateMapping.getSQLState(17001), 17001);
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getTimestamp(java.lang.String)
   */
  @Override
  public Timestamp getTimestamp(String parameterName) throws SQLException {
    ensureOpen();
    try {
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getTimestamp(getVal(parameterName), this.timeZoneID);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
          SQLStateMapping.getSQLState(error_code), error_code);
    }
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getDate(int, java.util.Calendar)
   */
  @Override
  public Date getDate(int parameterIndex, Calendar cal) throws SQLException {
    try {
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getDate(getVal(parameterIndex), this.timeZoneID, cal);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException( RestjdbcResources.format(RestjdbcResources.ORA_17004, "getDate"),
                  SQLStateMapping.getSQLState(error_code), error_code);
    } 
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getTime(int, java.util.Calendar)
   */
  @Override
  public Time getTime(int parameterIndex, Calendar cal) throws SQLException {
    try {
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getTime(getVal(parameterIndex), this.timeZoneID, cal);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException( RestjdbcResources.format(RestjdbcResources.ORA_17004, "getTime"),
                  SQLStateMapping.getSQLState(error_code), error_code);
    } 
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getTimestamp(int, java.util.Calendar)
   */
  @Override
  public Timestamp getTimestamp(int parameterIndex, Calendar cal)
      throws SQLException {
    try {
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getTimestamp(getVal(parameterIndex), this.timeZoneID, cal);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException( RestjdbcResources.format(RestjdbcResources.ORA_17004, "getTimestamp"),
                  SQLStateMapping.getSQLState(error_code), error_code); 
    }
  }
  
  @Override
  public Date getDate(String parameterName, Calendar cal) throws SQLException {
    try {
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getDate(getVal(parameterName), this.timeZoneID, cal);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException( RestjdbcResources.format(RestjdbcResources.ORA_17004, "getDate"),
                  SQLStateMapping.getSQLState(error_code), error_code);
    } 
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getTime(java.lang.String, java.util.Calendar)
   */
  @Override
  public Time getTime(String parameterName, Calendar cal) throws SQLException {
    try {
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getTime(getVal(parameterName), this.timeZoneID, cal);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException( RestjdbcResources.format(RestjdbcResources.ORA_17004, "getTime"),
                  SQLStateMapping.getSQLState(error_code), error_code);
    } 
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getTimestamp(java.lang.String, java.util.Calendar)
   */
  @Override
  public Timestamp getTimestamp(String parameterName, Calendar cal)
      throws SQLException {
    try {
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getTimestamp(getVal(parameterName), this.timeZoneID, cal);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException( RestjdbcResources.format(RestjdbcResources.ORA_17004, "getTimestamp"),
                  SQLStateMapping.getSQLState(error_code), error_code); 
    }
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getClob(int)
   */
  @Override
  public Clob getClob(int parameterIndex) throws SQLException {
    String result = extractResult(parameterIndex).findValue("result").asText();
    if(result == null) {
      return null;
    }
    return new oracle.dbtools.jdbc.CLOB(result);
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getObject(int)
   */
  @Override
  public Object getObject(int parameterIndex) throws SQLException {
    try {
      getIndexValidator(parameterIndex);
      Object obj = getObjectHelper(parameterIndex);
      return obj;
    } 
    catch (Exception e) {
        throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17005), 
          SQLStateMapping.getSQLState(17005), 17005); 
    }
  }
  
  /**
   * Helper method for getObject
   * Object returned in the appropriate datatype
   * @throws SQLException
   */
  private Object getObjectHelper(Object parameterIndex) throws SQLException {
    try {
      if(closed) {
        int error_code = 17010;
        throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17010), 
                               SQLStateMapping.getSQLState(error_code), error_code); 
      }
      // get column-type info from the ResultSet metadata
      String type = extractResult(parameterIndex).findValue("data_type").asText();
      
      if(type.contains("NUMBER")) {
        Number number = extractResult(parameterIndex).findValue("result").numberValue();
        return number;
      }
      else if(type.contains("BINARY_DOUBLE")) {
        double binary_double = Double.parseDouble(extractResult(parameterIndex).findValue("result").asText());
        return binary_double;
      }
      else if(type.contains("BINARY_FLOAT")) {
        float binary_float = Float.parseFloat(extractResult(parameterIndex).findValue("result").asText());
        return binary_float;
      }
      
      else if(type.contains("LONG")) {
        long long_value = Long.parseLong(extractResult(parameterIndex).findValue("result").asText());
        return long_value;
      }
      
      else if(type.contains("CHAR")) {
        String char_value = extractResult(parameterIndex).findValue("result").asText();
        return char_value;
      }
    
      else if(type.contains("VARCHAR")) {
        String varchar_value = extractResult(parameterIndex).findValue("result").asText();
        return varchar_value;
      }
      
      else if(type.contains("VARCHAR2")) {      
        String varchar2_value = extractResult(parameterIndex).findValue("result").asText();
        return varchar2_value;
      }
      else if(type.contains("DATE")) {
        if(parameterIndex instanceof Integer) {
          java.sql.Date date_value = getDate((int) parameterIndex);
          return date_value;
        }
        else if(parameterIndex instanceof String) {
          java.sql.Date date_value = getDate((String) parameterIndex);
          return date_value;
        }
        
      }
      else if(type.contains("TIMESTAMP")) {
        if(parameterIndex instanceof Integer) {
          java.sql.Timestamp timestamp_value = getTimestamp((int) parameterIndex);
          return timestamp_value;
        }
        else if(parameterIndex instanceof String) {
          java.sql.Timestamp timestamp_value = getTimestamp((String) parameterIndex);
          return timestamp_value;
        }
      }
      else if(type.contains("TIME")) {
        if(parameterIndex instanceof Integer) {
          java.sql.Time time_value = getTime((int) parameterIndex);
          return time_value;
        }
        else if(parameterIndex instanceof String) {
          java.sql.Time time_value = getTime((String) parameterIndex);
          return time_value;
        }
      }
    }
    
    // If the column Index is invalid
    catch(NullPointerException ne) {
      // Get the detailed message from the Exception
      if(ne.getMessage()!=null) {
        LOGGER.log(Level.SEVERE, ne.getMessage()); 
      }
      // Invalid column index exception
      int error_code = 17003;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17003), 
                             SQLStateMapping.getSQLState(error_code), error_code);      
    }
    
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }    
    // SQLException
    catch (SQLException e) {
      if(e.getMessage()!=null){
        throw new SQLException(e.getMessage(), e.getSQLState(), e.getErrorCode());
      }
    } 
    // Unsupported column type -- should not reach here
    // TODO: Implement non-primitive data types
    throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17005), 
                  SQLStateMapping.getSQLState(17005), 17005);
  }


  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getRowId(int)
   */
  @Override
  public RowId getRowId(int parameterIndex) throws SQLException {
    //ensureOpen("getRowId(int columnIndex)");
    try {
      getIndexValidator(parameterIndex);
      String rowID = extractResult(parameterIndex).findValue("result").asText();
      
      return new oracle.dbtools.jdbc.RowId(rowID);
    }
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004, "getRowId"),
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }
  }


  /* (non-Javadoc)
   * @see java.sql.CallableStatement#registerOutParameter(int, int, java.lang.String)
   */
  @Override
  public void registerOutParameter(int parameterIndex, int sqlType,
      String typeName) throws SQLException {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int)
   */
  @Override
  public void registerOutParameter(String parameterName, int sqlType)
      throws SQLException {
    
    // ensure the connection and the statement is open
    ensureOpen();
    
    outTypes.put(parameterName,sqlType);
    this.outParams = true; // There are out Parameters.
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int, int)
   * 
   * This method is not supported in the REST JDBC driver.
   */
  @Override
  public void registerOutParameter(String parameterName, int sqlType, int scale)
      throws SQLException {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int, java.lang.String)
   * 
   * This method is not supported in the REST JDBC driver.
   */
  @Override
  public void registerOutParameter(String parameterName, int sqlType,
      String typeName) throws SQLException {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getURL(int)
   */
  @Override
  public URL getURL(int parameterIndex) throws SQLException {
    // If this method is called on a closed CallableStatement
    if(closed) {
      int error_code = 17009;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17009), 
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }   
    try {
      getIndexValidator(parameterIndex);
      return new URL(extractResult(parameterIndex).findValue("result").asText());    
    }   
    
    // Invalid column Type
    catch (MalformedURLException e) {
      int error_code = 17004;
      throw new SQLException( RestjdbcResources.format(RestjdbcResources.ORA_17004, "getURL"),
                  SQLStateMapping.getSQLState(error_code), error_code); 
    }
  }

 
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getString(java.lang.String)
   */
  @Override
  public String getString(String parameterName) throws SQLException {
    String result=null;
    try {
      result = extractResult(parameterName).findValue("result").asText();      
    } 
    catch(SQLException e) {
      if(e.getMessage()!=null){
        throw new SQLException(e.getMessage(), e.getSQLState(), e.getErrorCode());
      }
    }
   
    // It's an internal error if anything else
    catch (Exception e) {
      e.printStackTrace();
      int errorCode = 17001;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17001),
                    SQLStateMapping.getSQLState(errorCode), errorCode);
    }   
    return result;      
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getBoolean(java.lang.String)
   */
  @Override
  public boolean getBoolean(String parameterName) throws SQLException {
    // Check if the parameter Index is valid
    JsonNode result = extractResult(parameterName).findValue("result");
    
    // throw an exception if it can't be converted to a boolean    
    return result.asBoolean();
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getShort(java.lang.String)
   */
  @Override
  public short getShort(String parameterName) throws SQLException {
    ensureOpen();
    try { 
      if(getVal(parameterName)==null) {
        return 0;
      }
      else {   
        // we just want to truncate / cast double value to integers
        return (short) Double.parseDouble(getVal(parameterName));
      } 
    }
     
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }    
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getInt(java.lang.String)
   */
  @Override
  public int getInt(String parameterName) throws SQLException {
    try {
      if(getVal(parameterName)==null) {
        return 0;
      }
      else {   
        // we just want to truncate / cast double value to integers
        return (int) Double.parseDouble(getVal(parameterName));
      }      
    }
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }     
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getLong(java.lang.String)
   */
  @Override
  public long getLong(String parameterName) throws SQLException {
    ensureOpen();
    try {
      if(getVal(parameterName)==null) {
        return 0;
      }
      else {   
        // we just want to truncate / cast double value to integers
        return (long) Long.parseLong(getVal(parameterName));
      }              
    }       
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }   
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getFloat(java.lang.String)
   */
  @Override
  public float getFloat(String parameterName) throws SQLException {
    ensureOpen();
    try {      
      if(getVal(parameterName)==null) {
        return (float) 0;
      }
      else {   
        return Float.parseFloat(getVal(parameterName));
      }      
    }    
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }   
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getDouble(java.lang.String)
   */
  @Override
  public double getDouble(String parameterName) throws SQLException {
    ensureOpen();
    try {   
      if(getVal(parameterName)==null) {
        return (double) 0;
      }
      else {   
        return Double.parseDouble(getVal(parameterName));
      } 
    }
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }   
  }

 
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getObject(java.lang.String)
   */
  @Override
  public Object getObject(String parameterName) throws SQLException {   
    // If this method is called on a closed CallableStatement
    if(closed) {
      int error_code = 17009;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17009), 
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }
    try {
      getIndexValidator(parameterName);
      Object obj = getObjectHelper(parameterName);
      return obj;
    } 
    catch (Exception e) {
        throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17005), 
          SQLStateMapping.getSQLState(17005), 17005); 
    }
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getBigDecimal(java.lang.String)
   */
  @Override
  public BigDecimal getBigDecimal(String parameterName) throws SQLException {
    try {
      if(getVal(parameterName)==null) {
        return (BigDecimal.valueOf(0.0));
      }
      else {   
        return BigDecimal.valueOf(Double.parseDouble(getVal(parameterName)));
      }      
    }
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17004), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }     
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getClob(java.lang.String)
   */
  @Override
  public Clob getClob(String parameterName) throws SQLException {
    String result = extractResult(parameterName).findValue("result").asText();
    if(result == null) {
      return null;
    }
    return new oracle.dbtools.jdbc.CLOB(result);
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getURL(java.lang.String)
   */
  @Override
  public URL getURL(String parameterName) throws SQLException {
    // If this method is called on a closed CallableStatement
    if(closed) {
      int error_code = 17009;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17009), 
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }    
    try {
      getIndexValidator(parameterName);
      return new URL(extractResult(parameterName).findValue("result").asText());    
    }       
    // Invalid column Type
    catch (MalformedURLException e) {
      int error_code = 17004;
      throw new SQLException( RestjdbcResources.format(RestjdbcResources.ORA_17004, "getURL"),
                  SQLStateMapping.getSQLState(error_code), error_code); 
    }
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getRowId(java.lang.String)
   */
  @Override
  public RowId getRowId(String parameterName) throws SQLException {
  //ensureOpen("getRowId(int columnIndex)");
    try {
      getIndexValidator(parameterName);
      String rowID = extractResult(parameterName).findValue("result").asText();
      
      return new oracle.dbtools.jdbc.RowId(rowID);
    }
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004, "getRowId"),
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }
  }
  
  /*** Unsupported execute methods ***/

  /* (non-Javadoc)
   * @see java.sql.Statement#executeUpdate(java.lang.String, int)
   */
  @Override
  public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "executeUpdate(String sql, int autoGeneratedKeys)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.Statement#executeUpdate(java.lang.String, int[])
   */
  @Override
  public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "executeUpdate(String sql, int[] columnIndexes)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.Statement#executeUpdate(java.lang.String, java.lang.String[])
   */
  @Override
  public int executeUpdate(String sql, String[] columnNames) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "executeUpdate(String sql, String[] columnNames)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.Statement#execute(java.lang.String, int)
   */
  @Override
  public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "execute(String sql, int autoGeneratedKeys)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.Statement#execute(java.lang.String, int[])
   */
  @Override
  public boolean execute(String sql, int[] columnIndexes) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "execute(String sql, int[] columnIndexes)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.Statement#execute(java.lang.String, java.lang.String[])
   */
  @Override
  public boolean execute(String sql, String[] columnNames) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "execute(String sql, String[] columnNames) ", this.getClass().getSimpleName()));
  }

  /*** Unimplemented getXXX methods in CallableStatement***/
 
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getObject(int, java.util.Map)
   */
  @Override
  public Object getObject(int parameterIndex, Map<String, Class<?>> map)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getObject(int parameterIndex, Map<String, Class<?>> map)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getRef(int)
   */
  @Override
  public Ref getRef(int parameterIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getRef(int parameterIndex)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getBlob(int)
   */
  @Override
  public Blob getBlob(int parameterIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getBlob(int parameterIndex)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getArray(int)
   */
  @Override
  public Array getArray(int parameterIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getArray(int parameterIndex)", this.getClass().getSimpleName()));
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getBytes(java.lang.String)
   */
  @Override
  public byte[] getBytes(String parameterName) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getBytes(String parameterName)", this.getClass().getSimpleName()));
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getByte(java.lang.String)
   */
  @Override
  public byte getByte(String parameterName) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getByte(String parameterName)", this.getClass().getSimpleName()));
  }
  
  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getObject(java.lang.String, java.util.Map)
   */
  @Override
  public Object getObject(String parameterName, Map<String, Class<?>> map)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getObject(String parameterName, Map<String, Class<?>> map)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getRef(java.lang.String)
   */
  @Override
  public Ref getRef(String parameterName) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getRef(String parameterName)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getBlob(java.lang.String)
   */
  @Override
  public Blob getBlob(String parameterName) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getBlob(String parameterName)", this.getClass().getSimpleName()));
  }


  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getArray(java.lang.String)
   */
  @Override
  public Array getArray(String parameterName) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getArray(String parameterName)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setNString(java.lang.String, java.lang.String)
   */
  @Override
  public void setNString(String parameterName, String value)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "setNString(String parameterName, String value)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setNCharacterStream(java.lang.String, java.io.Reader, long)
   */
  @Override
  public void setNCharacterStream(String parameterName, Reader value,
      long length) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "setNCharacterStream(String parameterName, Reader value,long length)", 
         this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setNClob(java.lang.String, java.sql.NClob)
   */
  @Override
  public void setNClob(String parameterName, NClob value) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "setNClob(String parameterName, NClob value)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setClob(java.lang.String, java.io.Reader, long)
   */
  @Override
  public void setClob(String parameterName, Reader reader, long length)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "setClob(String parameterName, Reader reader, long length)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setBlob(java.lang.String, java.io.InputStream, long)
   */
  @Override
  public void setBlob(String parameterName, InputStream inputStream,
      long length) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "setBlob(String parameterName, InputStream inputStream,long length)", 
         this.getClass().getSimpleName()));

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setNClob(java.lang.String, java.io.Reader, long)
   */
  @Override
  public void setNClob(String parameterName, Reader reader, long length)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "setNClob(String parameterName, Reader reader, long length)", this.getClass().getSimpleName()));

  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getNClob(int)
   */
  @Override
  public NClob getNClob(int parameterIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getNClob(int parameterIndex)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getNClob(java.lang.String)
   */
  @Override
  public NClob getNClob(String parameterName) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getNClob(String parameterName)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#setSQLXML(java.lang.String, java.sql.SQLXML)
   */
  @Override
  public void setSQLXML(String parameterName, SQLXML xmlObject)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "setSQLXML(String parameterName, SQLXML xmlObject)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getSQLXML(int)
   */
  @Override
  public SQLXML getSQLXML(int parameterIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getSQLXML(int parameterIndex)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getSQLXML(java.lang.String)
   */
  @Override
  public SQLXML getSQLXML(String parameterName) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getSQLXML(String parameterName)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getNString(int)
   */
  @Override
  public String getNString(int parameterIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getNString(int parameterIndex)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getNString(java.lang.String)
   */
  @Override
  public String getNString(String parameterName) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getNString(String parameterName)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getNCharacterStream(int)
   */
  @Override
  public Reader getNCharacterStream(int parameterIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getNCharacterStream(int parameterIndex)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getNCharacterStream(java.lang.String)
   */
  @Override
  public Reader getNCharacterStream(String parameterName) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getNCharacterStream(String parameterName)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getCharacterStream(int)
   */
  @Override
  public Reader getCharacterStream(int parameterIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getCharacterStream(int parameterIndex)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getCharacterStream(java.lang.String)
   */
  @Override
  public Reader getCharacterStream(String parameterName) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getCharacterStream(String parameterName)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getObject(int, java.lang.Class)
   */
  @Override
  public <T> T getObject(int parameterIndex, Class<T> type)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getObject(int parameterIndex, Class<T> type)", this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.CallableStatement#getObject(java.lang.String, java.lang.Class)
   */
  @Override
  public <T> T getObject(String parameterName, Class<T> type)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getObject(String parameterName, Class<T> type)", this.getClass().getSimpleName()));
  }
 

  /** Other unsupported methods **/
  /* (non-Javadoc)
   * @see java.sql.Wrapper#unwrap(java.lang.Class)
   */
  @Override
  public <T> T unwrap(Class<T> iface) throws SQLException {
    // TODO Auto-generated method stub
    return null;
  }

  /* (non-Javadoc)
   * @see java.sql.Wrapper#isWrapperFor(java.lang.Class)
   */
  @Override
  public boolean isWrapperFor(Class<?> iface) throws SQLException {
    // TODO Auto-generated method stub
    return false;
  }

  // Utility function
  protected Integer truncateInt(String numString) {
  // need to convert "2.0" to 2
  //Assumption: we do not care about anything after the dot 
  //- i.e. , not used and 1e10 scientific notation not used - do it as a function so it can be updated later? 
  //if parseint fails -> let the exception be registered elsewhere
    String theString=numString;
    if (theString==null) {//GIGO
      return null;
    }
    int   theDot=theString.lastIndexOf('.');
    if (theDot!=-1){
      theString=theString.substring(0,theDot);
    }
    return Integer.parseInt(theString);
  }
}