/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import oracle.dbtools.http.Session;
import oracle.dbtools.http.SessionCreator;
import oracle.dbtools.http.SessionException;
import oracle.dbtools.http.SessionInfo;
import oracle.dbtools.jdbc.util.Defaults;
import oracle.dbtools.jdbc.util.JDBCClient;
import oracle.dbtools.jdbc.util.JDBCSessionType;
import oracle.dbtools.jdbc.util.LogUtil;
import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.jdbc.util.RestJdbcUnsupportedException;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.dbtools.jdbc.util.RestjdbcSqlWarnings;
import oracle.dbtools.jdbc.util.SQLStateMapping;

/**
 * @author debjanibiswas
 */
public class Connection implements java.sql.Connection {

	private static String DRIVER_NAME = "Oracle REST JDBC driver";
	// Logger for Connection
	Logger logger = Logger.getLogger(oracle.dbtools.jdbc.Connection.class.getName());
	
	private String db_url = null;
	
	private String full_url = null;
	
	private Properties info = null;
	
	private Session jdbcSession = null;
	
	private DatabaseMetaData databaseMetaData = null;
	
	// Turn this variable on when debugging 
	public boolean debug 			=  false;
	
	// To get oAuth2 token 
	private String token = null;
	
	// get waring at least for grace period
	private SQLWarning sqlWarn=null;
	
	// clear warning grace period already checked
	private boolean muteWarnings=false;
	
	private static final String ADHOC_ENDPOINT = "_/sql";
	
	// To keep track if the connection is closed or not
	private boolean isClosed = false;
	
	/*
	 * url string http:...?key=value
	 * Properties = including username and password 
	 */
	public Connection(String url, Properties info) throws SQLException {
		
		this.info 	= info;
		full_url=url;
		
		url=LogUtil.getUrlProperties(url,this.info);
		try {
			/* Now get the credentials from the properties */
			String user = null;
			char[] pwd	= new char[0];
			
			// Get the user
			if(info.containsKey("user") && info.get("user")!=null) {
				user = (String) info.get("user");
			}
			
			// If "user" is not present in the property variable
			else {
			  throw new SQLException(RestjdbcResources.getString(RestjdbcResources.RESTJDBC_001));
			}
			
			// Get the password 
			if(info.containsKey("password") && info.get("password")!=null) {
			  String password = (String) info.get("password");
			  pwd  = password.toCharArray();
			}
			
			// If "password" is not present in the property variable
			else {
			  throw new SQLException(RestjdbcResources.getString(RestjdbcResources.RESTJDBC_002));			  
			}	
			// Add ords to the url and the enabled user
			this.db_url  = url;
			
			//	URI service_root = null;
			URI root = URI.create(db_url);
		
			JDBCSessionType jdbcSessionType = new JDBCSessionType();
			
			JDBCClient client = new JDBCClient(jdbcSessionType);
			
			String sessionInfo_name = "jdbc_session";
		
			// Instantiate a session info
			SessionInfo jdbc_sessionInfo = new SessionInfo(sessionInfo_name, root, null, user, pwd);
			
			// Create a new Session
			SessionCreator sessionCreator =  SessionCreator.createSessionCreator(client);
			jdbcSession = sessionCreator.createSession(jdbc_sessionInfo);
		
		} 
		
		catch (SessionException e) {
			if(debug) {
				e.printStackTrace();		
			}
			// TODO: URL enforcement not implemented yet
			if (!url.startsWith("jdbc:oraclerest")) {
				throw new SQLException(RestjdbcResources.get(RestjdbcResources.RESTJDBC_008), e);
			}
			throw new SQLException(RestjdbcResources.get(RestjdbcResources.RESTJDBC_009), e);
		}
		
	}
	
	/*
	 * Helper method for Connection
	 * After a connection is established, a statement is posted up to adhoc to verify that 
	 * adhoc is working properly and get the timezone that ords is running on
	 * 
	 * @return the String of the timeZone that the ORDS is running on
	 * @throws exception if cannot connect to ORDS
	 */
	protected String getTimeZoneID() throws SQLException {
	  String timeZoneID = null;
	  try {
	    
	    String sql = "select 1 from dual";
	    
	    Session session = this.jdbcSession;
	    
	    StringBuilder uri = new StringBuilder();
	    uri.append(this.db_url);
	    uri.append(Connection.ADHOC_ENDPOINT);
	    
	    URI root = URI.create(uri.toString());

	    // create a POST with appropriate headers
	    HttpPost post = session.createPost(root);
	    
	    // Always want to get the verbose results
      post.addHeader("Content-Type", "application/json");
      
      // Generate the json post and set the post entity
      post.setEntity(new StringEntity(generatePost(sql)));
      
      // Execute the post request
      CloseableHttpResponse response = session.executeRequest(post); 
	    
      // create JsonNode from response
      JsonNode node = createJsonNode(response);
      
      // handle errors if any
      handleErrors(node);
      
      // get the timezoneID if no errors
      timeZoneID = node.findValue("env").findValue("defaultTimeZone").asText();
	  }
	  
	  catch ( IllegalStateException | IOException | SessionException e) 
    {
      logger.log(Level.SEVERE, e.getMessage());
    }	  
	  return timeZoneID;
	}
	
	/**
	 * in connection rather that dbmsmetadata as dbmsmetadata is shared between orest and rest drivers.
	 * @return
	 */
	public String getDriverName(){
		return DRIVER_NAME;
	}
	/**
	 * Helper method used by getTimeZoneID() method in Connection
	 * to create the json post to send up to ords 
	 * @throws IOException -- for issues with Json generation
	 */
	protected String generatePost(String sql) throws SQLException, IOException {
	  
	  // To prepare the POST
  	JsonFactory f = new JsonFactory();
  	Writer writer = new StringWriter();
  	  
  	JsonGenerator g = f.createGenerator(writer);
  	  
  	g.writeStartObject();
  	g.writeStringField("statementText", sql);
  	g.writeNumberField("offset", Defaults.DEFAULT_OFFSET);
  	g.writeNumberField("limit",  Defaults.DEFAULT_LIMIT);
  	g.writeEndObject();  
  	g.close();
  	
  	return writer.toString();
	}
	
  /**
   * This method handles errors (if any) from the response that's sent back from ORDS
   * @throws SQLException created from response
   */
  protected void handleErrors(JsonNode node) throws SQLException {
    if (node.findValue("errorCode")!=null) { //throw SQLException
      int error_code = node.findValue("errorCode").asInt();
      int error_line = node.findValue("errorLine").asInt();
      int error_column = node.findValue("errorColumn").asInt();
      String error_details = node.findValue("errorDetails").asText();
      throw new SQLException(error_details, SQLStateMapping.getSQLState(error_code), error_code);
    }
    else if(node.findValue("error")!=null) {
      // we don't exactly know what the error code is here...
      throw new SQLException(node.findValue("error").asText());
    }
  }
  
  /**
   * This method creates a JsonNode from the response that's sent back from ORDS
   * @param response
   * @return JsonNode with the response to DDLs and DMLs
   * @throws SQLException
   */
  protected JsonNode createJsonNode(CloseableHttpResponse response) throws SQLException {
    HttpEntity entity = response.getEntity();
    JsonNode node = null;
    try {
      InputStream is = entity.getContent(); 
      ObjectMapper mapper = new ObjectMapper();
      node = mapper.readTree(is);
    } catch (Exception e) {
      logger.severe(e.getMessage());
      int error_code = 17001;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17001),
                                 SQLStateMapping.getSQLState(error_code), error_code); 
    }
    return node;
  }
	
	
	@Override
	public Statement createStatement() throws SQLException {
	  ensureOpen();
		return new oracle.dbtools.jdbc.Statement(this);
	}
	

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		 return new oracle.dbtools.jdbc.PreparedStatement(this, sql);
	}
	

	public Connection getConnection() {
		return this;
	}
	
	/**
	 * Internal helper method to return the Session associated with a connection
	 */
	protected Session getSession() {
	  return this.jdbcSession;
	}

	// Get the rest uri 
	public String getUri() {
	  return this.db_url;
	}
	
	/**
	 * Internal helper method to return the Oauth2 token of the current session
	 */
	protected String getOauth2Token() {
	  return this.token;
	}
	
	@Override
  public void close() throws SQLException {	  
	  // Release all the resources
	  this.jdbcSession.close();
	  this.info = null;
	  this.isClosed = true;
	}
	
	/**
	 * Helper method to ensure connection is open
	 */
	private void ensureOpen() throws SQLException {
	  if(this.isClosed == true) {
	    int errorCode = 17008;
	    throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17008), 
	                           SQLStateMapping.getSQLState(errorCode), errorCode); 
	  }
	}
	



  @Override
  public CallableStatement prepareCall(String sql) throws SQLException {
    ensureOpen();
    return new oracle.dbtools.jdbc.CallableStatement(this,sql);
  }

  /*
   * Just returns the original SQL statement that was passed into the REST JDBC driver
   */
  @Override
  public String nativeSQL(String sql) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "nativeSQL(String sql)", this.getClass().getSimpleName())); 
  }

  /**
   * This method is a no-op for the REST JDBC driver
   */
  @Override
  public void setAutoCommit(boolean autoCommit) throws SQLException {
    ensureOpen();    
  }

  /**
   * Always true for the REST JDBC Driver.
   */
  @Override
  public boolean getAutoCommit() throws SQLException {
    ensureOpen();
    return true;
  }

  @Override
  public void commit() throws SQLException {
    ensureOpen();
     //decision not to cause an optional exception here  
  }

  /**
   * rollback to last commit an error in rest as it auto commits;
   */
  @Override
  public void rollback() throws SQLException {
    ensureOpen();
    LogUtil.logOrException(this, null, "Rollback not supported (REST connection autocommits) - likely to be an error", null);    
  }

  @Override
  public boolean isClosed() throws SQLException {
    ensureOpen();
    return this.isClosed;
  }



  @Override
  public DatabaseMetaData getMetaData() throws SQLException {  
    ensureOpen();
  	if(databaseMetaData == null){
  		databaseMetaData = new oracle.dbtools.jdbc.DatabaseMetaData(this);
  	} 
  	return databaseMetaData;
  }

  @Override
  public SQLWarning getWarnings() throws SQLException {
    ensureOpen();
    SQLWarning retVal=null;
    if (muteWarnings==false) {
      if (this.sqlWarn!=null) {
        retVal=this.sqlWarn;
      } else {
        Statement st=null;
        java.sql.ResultSet rs=null;
        try {
          st=this.createStatement();
          rs=st.executeQuery("select account_status, case when expiry_date is null then 1 else 0 end ifnull, "+ //$NON-NLS-1$
             " case when expiry_date is null then null else TO_CHAR(ROUND(expiry_date - sysdate)) end daysleft from user_users where username=user");  //$NON-NLS-1$
          //explicitly should throw an exception on failure (rather than log)
          if (rs.next()) {
            String status=rs.getString(1);
            int wasNull=rs.getInt(2);
            String howLong=rs.getString(3);
            if (status.trim().equals("EXPIRED(GRACE)")) {  //$NON-NLS-1$
              if (wasNull==1) {
                //length of expire time unknown
                //ORA-28011: the account will expire soon; change your password now //internationalize
                sqlWarn=new SQLWarning(RestjdbcSqlWarnings.getString(RestjdbcSqlWarnings.WARN_28011),"99999",28011);  //$NON-NLS-1$
              } else {
                //how Long = expire time in days calculated rather than from profile (round so 1.4 days will report 1)
                //ORA-28002: the password will expire within 3 days //internationalise
                if (howLong.equals("0")) {  //$NON-NLS-1$
                  //saying you will expire in 0 days does not really cut it, might become ORA-28011
                  howLong="1"; //$NON-NLS-1$
                }
                sqlWarn=new SQLWarning(RestjdbcSqlWarnings.format(RestjdbcSqlWarnings.WARN_28002, howLong),"99999",28002);  //$NON-NLS-1$
              }
              retVal=sqlWarn;
            } else {
              muteWarnings=true;
            }
          } else {
            //should always be a line will mute for now.
            muteWarnings=true;
          }
        } finally {
          if (rs!=null) {
            try {
              rs.close();
            } catch (SQLException e) {
              logger.log(Level.INFO, e.getCause().toString());
            }
          }
          if (st!=null) {
            try {
              st.close();
            } catch (SQLException e) {
              logger.log(Level.INFO, e.getCause().toString());
            }
          }
        }
      }
    }
    return retVal;
  }

  @Override
  public void clearWarnings() throws SQLException {
    ensureOpen();
    this.muteWarnings=true;
  }

  @Override
  public Clob createClob() throws SQLException {
    ensureOpen();
    return new oracle.dbtools.jdbc.CLOB();
  }
  /**
   * This method is not supported by the REST JDBC driver
   */
  @Override
  public Statement createStatement(int resultSetType, int resultSetConcurrency)
      throws SQLException {
    ensureOpen();
    LogUtil.logOrException(this, Level.INFO, 
        // log message
        exceptionLogMessage(Defaults.UNSUPPORTED, 
            "createStatement(int resultSetType, int resultSetConcurrency)"),
        // exception to be thrown
        new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
            "createStatement(int resultSetType, int resultSetConcurrency)", 
            this.getClass().getSimpleName())));    
    return null;
  }

  /**
   * This method is not supported by the REST JDBC driver
   */
  @Override
  public PreparedStatement prepareStatement(String sql, int resultSetType,
      int resultSetConcurrency) throws SQLException {
    ensureOpen();
    LogUtil.logOrException(this, Level.INFO, 
        // log message
        exceptionLogMessage(Defaults.UNSUPPORTED, 
            "prepareStatement(String sql, int resultSetType, int resultSetConcurrency)"),
        // exception to be thrown
        new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
            "prepareStatement(String sql, int resultSetType, int resultSetConcurrency)", 
            this.getClass().getSimpleName())));    
    return null;
  }

  @Override
  public CallableStatement prepareCall(String sql, int resultSetType,
      int resultSetConcurrency) throws SQLException {
    ensureOpen();
    LogUtil.logOrException(this, Level.INFO, 
        // log message
        exceptionLogMessage(Defaults.UNSUPPORTED, 
             "prepareCall(String sql, int resultSetType, int resultSetConcurrency)"),
        // exception to be thrown
        new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
            "prepareCall(String sql, int resultSetType, int resultSetConcurrency)", 
            this.getClass().getSimpleName())));   
    return null;
  }


  /**
   * The REST JDBC driver does not support cursors and holdability
   */
  @Override
  public void setHoldability(int holdability) throws SQLException {
    ensureOpen();
    logger.info(RestjdbcResources.get(RestjdbcResources.RESTJDBC_024));  // Holdability is not supported in the REST JDBC driver.
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setHoldability(int holdability)",  this.getClass().getSimpleName()));  
  }

  /**
   * The REST JDBC driver does not support cursors and holdability.
   */
  @Override
  public int getHoldability() throws SQLException {
    ensureOpen();
    logger.info(RestjdbcResources.get(RestjdbcResources.RESTJDBC_023));  // Cursors are not supported in the REST JDBC Driver. 
    logger.info(RestjdbcResources.get(RestjdbcResources.RESTJDBC_024));  // Holdability is not supported in the REST JDBC driver.
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getHoldability()",  this.getClass().getSimpleName()));
  }

  /**
   * The REST JDBC driver does not support savepoints and rollbacks.
   * Requests to the database are auto-committed. 
   */
  @Override
  public Savepoint setSavepoint() throws SQLException {
    ensureOpen();
    logger.info(RestjdbcResources.get(RestjdbcResources.RESTJDBC_022)); 
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setSavepoint()",  this.getClass().getSimpleName()));     
  }

  /**
   * The REST JDBC driver does not support savepoints and rollbacks.
   * Requests to the database are auto-committed. 
   */
  @Override
  public Savepoint setSavepoint(String name) throws SQLException {
    ensureOpen();
    logger.info(RestjdbcResources.get(RestjdbcResources.RESTJDBC_022)); 
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setSavepoint(String name)",  this.getClass().getSimpleName()));     
  }

  /**
   * The REST JDBC driver does not support rollbacks. 
   * Requests to the database are auto-committed.
   */
  @Override
  public void rollback(Savepoint savepoint) throws SQLException {
    ensureOpen();
    logger.info(RestjdbcResources.get(RestjdbcResources.RESTJDBC_021));    
  }

  /**
   * The REST JDBC driver does not support savepoints and rollbacks.
   * Requests to the database are auto-committed. 
   */
  @Override
  public void releaseSavepoint(Savepoint savepoint) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "releaseSavepoint(Savepoint savepoint)", 
         this.getClass().getSimpleName()));     
  }

  /**
   * This method is not implemented by the REST JDBC driver
   */
  @Override
  public Statement createStatement(int resultSetType, int resultSetConcurrency,
      int resultSetHoldability) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)", 
         this.getClass().getSimpleName()));  
  }

  /**
   * This method is not implemented by the REST JDBC driver
   */
  @Override
  public PreparedStatement prepareStatement(String sql, int resultSetType,
      int resultSetConcurrency, int resultSetHoldability) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability)", 
         this.getClass().getSimpleName()));  
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public CallableStatement prepareCall(String sql, int resultSetType,
      int resultSetConcurrency, int resultSetHoldability) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "prepareCall(String sql, int resultSetType,int resultSetConcurrency, int resultSetHoldability)", 
         this.getClass().getSimpleName()));  
    }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys)
      throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "prepareStatement(String sql, int autoGeneratedKeys)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public PreparedStatement prepareStatement(String sql, int[] columnIndexes)
      throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "prepareStatement(String sql, int[] columnIndexes)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public PreparedStatement prepareStatement(String sql, String[] columnNames)
      throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "prepareStatement(String sql, String[] columnNames)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public Map<String, Class<?>> getTypeMap() throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
                                              "getTypeMap()", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setTypeMap(Map<String, Class<?>> map)", this.getClass().getSimpleName()));
  }


  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public Blob createBlob() throws SQLException {
    ensureOpen();
    return new oracle.dbtools.jdbc.BLOB();
  }

  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public NClob createNClob() throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "createNClob()", this.getClass().getSimpleName())); 
  }

  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public SQLXML createSQLXML() throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "createSQLXML()", this.getClass().getSimpleName())); 
  }

  /**
   * Helper method to 
   * relies on statement timeout which may not be functional yet, theirfore default http timeout will apply, and false will be returned if query too long.
   */
  @Override
  public boolean isValid(int timeout) throws SQLException {
    ensureOpen();
    long start=Calendar.getInstance().getTimeInMillis();
    Long end=null;
    if (timeout<0) {
        throw new SQLException("timeout is negative"); //put correct string in arb
    }
    if (this.isClosed()) {
        return false;
    }
    Statement stmt=null;
    ResultSet rs=null;
    try{
      stmt=this.createStatement();
      //this query timeout does nothing for now
      //stmt.setQueryTimeout(timeout);
      rs=(ResultSet)stmt.executeQuery("select 1 from dual");  //$NON-NLS-1$
      if (rs.next()) {
        end=Calendar.getInstance().getTimeInMillis();
      }
    } catch (SQLException e) {
      logger.log(Level.SEVERE, e.getLocalizedMessage(),e);
      return false;
    } finally {
      if (rs!=null) {
        try {
          rs.close();
        } catch (SQLException e) {
          logger.log(Level.SEVERE, e.getLocalizedMessage(),e);
        }
      }	
      if (stmt!=null) {
        try {
          stmt.close();
        } catch (SQLException e) {
          logger.log(Level.SEVERE, e.getLocalizedMessage(),e);
        }
      }
      if((timeout==0)&&(end!=null)) {
        return true;
      } else if((end!=null)&&(timeout!=0)&&((timeout*1000)>(end-start))) {
        return true;
      }
    }
    return false;
  }

  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public void setClientInfo(String name, String value) throws SQLClientInfoException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setClientInfo(String name, String value)", this.getClass().getSimpleName())); 
    
  }

  @Override
  public void setClientInfo(Properties properties) throws SQLClientInfoException {
    this.info = properties;
  }

  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public String getClientInfo(String name) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getClientInfo(String name)", this.getClass().getSimpleName())); 
  }
  
  /**
   * getInfo - get properties. May be merged with generic getClientInfo at a later date.
   * @return properties either explicitly passed in or added by ?key=value on url on connection creation
   */
  public Properties getInfo() {
    return this.info; 
  }
  
  @Override
  public Properties getClientInfo() throws SQLException {
    ensureOpen();
    return this.info;
  }

  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public Array createArrayOf(String typeName, Object[] elements)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "createArrayOf(String typeName, Object[] elements)", this.getClass().getSimpleName()));
  }
  
  
  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public Struct createStruct(String typeName, Object[] attributes)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "createStruct(String typeName, Object[] attributes)", this.getClass().getSimpleName())); 
  }

  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public void abort(Executor executor) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "abort(Executor executor)", this.getClass().getSimpleName())); 
    
  }

  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public void setNetworkTimeout(Executor executor, int milliseconds)
      throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setNetworkTimeout(Executor executor, int milliseconds)", this.getClass().getSimpleName())); 
    
  }

  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public int getNetworkTimeout() throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getNetworkTimeout()", this.getClass().getSimpleName())); 
  }

  
  /**
   * full client facing url including any ? properties and including prefix if driver is orest.
   * @param inURL 
   */
  public void setURL(String inURL)  {
    // TODO Auto-generated method stub
    full_url=inURL;
  }
  

  /**
   * url got most notably from dbms metadata
   * @return full url including possible ? properties and correct for rest (with prefix for orest)
   * @throws SQLException
   */
  public String getURL() throws SQLException {
    return full_url;
  }
  
  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public String getSchema() throws SQLException { 
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getSchema()", this.getClass().getSimpleName()));
  }
  
  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public void setSchema(String schema) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setSchema(String schema)", this.getClass().getSimpleName())); 
  }

  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public <T> T unwrap(Class<T> iface) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "unwrap(Class<T> iface)", this.getClass().getSimpleName())); 
  }

  /**
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public boolean isWrapperFor(Class<?> iface) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "isWrapperFor(Class<T> iface)", this.getClass().getSimpleName())); 
  }
  
  @Override
  public void setReadOnly(boolean readOnly) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setReadOnly(boolean readOnly)", this.getClass().getSimpleName())); 
    
  }

  @Override
  public boolean isReadOnly() throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "isReadOnly()", this.getClass().getSimpleName())); 
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setCatalog(String catalog) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setCatalog(String catalog)", this.getClass().getSimpleName()));   
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public String getCatalog() throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getCatalog()", this.getClass().getSimpleName()));  
  }

  /**
   * Transaction isolations are not supported in the REST JDBC driver.
   */
  @Override
  public void setTransactionIsolation(int level) throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setTransactionIsolation(int level)", this.getClass().getSimpleName()));  
  }

  /**
   * Transaction isolations are not supported in the REST JDBC driver.
   */
  @Override
  public int getTransactionIsolation() throws SQLException {
    ensureOpen();
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getTransactionIsolation()", this.getClass().getSimpleName()));  
  }
  /**
   * Helper method to construct messages for unsupported / unimplemented features
   * @param boolean value - unsupport - to check if the feature is unsupported / unimplemented
   * @return String message to log and throw as an exception
   */
  protected String exceptionLogMessage(boolean unsupport, String methodName) {
    // If the feature is not supported
    if(unsupport) {
      return RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, methodName, this.getClass().getSimpleName());
    }
    // If the feature is supported but not yet implemented
    else {
      return RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, methodName, this.getClass().getSimpleName());
    }   
  }
}
