/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.jdbc.util.RestjdbcResources;

/**
 * @author debjanibiswas
 *
 */
public class ParameterMetaData implements java.sql.ParameterMetaData {

  // the parameter metadata variable
  private Map<Integer, Integer> isNullable;
  private Map<Integer, Boolean> isSigned;
  private Map<Integer, Integer> precision;
  private Map<Integer, Integer> scale;
  private Map<Integer, Integer> parameterType;
  private Map<Integer, String>  parameterTypeName;
  private Map<Integer, String>  parameterClassName;
  int[]     parameterMode;
  int parameterCount;
  
  public ParameterMetaData(Map<Integer, Integer> isNullable, Map<Integer, Boolean> isSigned, 
                              Map<Integer, Integer> precision, Map<Integer, Integer> scale,
                              Map<Integer, Integer> parameterType, Map<Integer, String>  parameterTypeName, 
                              Map<Integer, String>  parameterClassName, 
                              int[] parameterMode, int parameterCount) {
    
    this.isNullable         = isNullable;
    this.isSigned           = isSigned;
    this.precision          = precision;
    this.scale              = scale;
    this.parameterType      = this.parameterType;
    this.parameterTypeName  = this.parameterTypeName;
    this.parameterClassName = this.parameterClassName;
    this.parameterMode      = this.parameterMode;
    this.parameterCount     = parameterCount;
      
  }

  /* (non-Javadoc)
   * @see java.sql.Wrapper#unwrap(java.lang.Class)
   */
  @Override
  public <T> T unwrap(Class<T> iface) throws SQLException {
	  throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
		        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
  }

  /* (non-Javadoc)
   * @see java.sql.Wrapper#isWrapperFor(java.lang.Class)
   */
  @Override
  public boolean isWrapperFor(Class<?> iface) throws SQLException {
	  throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
		        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
  }

  /**
   * Retrieves the number of parameters in the <code>PreparedStatement</code>
   * object for which this <code>ParameterMetaData</code> object contains
   * information.
   *
   * @return the number of parameters
   * @exception SQLException if a database access error occurs
   * @since 1.4
   */
  @Override
  public int getParameterCount() throws SQLException {
    
    return this.parameterCount;
  }

  /**
   * Retrieves whether null values are allowed in the designated parameter.
   *
   * @param param the first parameter is 1, the second is 2, ...
   * @return the nullability status of the given parameter; one of
   *        <code>ParameterMetaData.parameterNoNulls</code>,
   *        <code>ParameterMetaData.parameterNullable</code>, or
   *        <code>ParameterMetaData.parameterNullableUnknown</code>
   * @exception SQLException if a database access error occurs
   * @since 1.4
   */
  @Override
  public int isNullable(int param) throws SQLException {
    throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
  }

  /**
   * Retrieves whether values for the designated parameter can be signed numbers.
   *
   * @param param the first parameter is 1, the second is 2, ...
   * @return <code>true</code> if so; <code>false</code> otherwise
   * @exception SQLException if a database access error occurs
   * @since 1.4
   */
  @Override
  public boolean isSigned(int param) throws SQLException {
    throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
  }

  /**
   * Retrieves the designated parameter's specified column size.
   *
   * <P>The returned value represents the maximum column size for the given parameter.
   * For numeric data, this is the maximum precision.  For character data, this is the length in characters.
   * For datetime datatypes, this is the length in characters of the String representation (assuming the
   * maximum allowed precision of the fractional seconds component). For binary data, this is the length in bytes.  For the ROWID datatype,
   * this is the length in bytes. 0 is returned for data types where the
   * column size is not applicable.
   *
   * @param param the first parameter is 1, the second is 2, ...
   * @return precision
   * @exception SQLException if a database access error occurs
   * @since 1.4
   */
  @Override
  public int getPrecision(int param) throws SQLException {
    
    throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
  }

  /**
   * Retrieves the designated parameter's number of digits to right of the decimal point.
   * 0 is returned for data types where the scale is not applicable.
   *
   * @param param the first parameter is 1, the second is 2, ...
   * @return scale
   * @exception SQLException if a database access error occurs
   * @since 1.4
   *//* (non-Javadoc)
   * @see java.sql.ParameterMetaData#getScale(int)
   */
  @Override
  public int getScale(int param) throws SQLException {
    throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
              new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

  }

  /**
   * Retrieves the designated parameter's SQL type.
   *
   * @param param the first parameter is 1, the second is 2, ...
   * @return SQL type from <code>java.sql.Types</code>
   * @exception SQLException if a database access error occurs
   * @since 1.4
   * @see Types
   */
  @Override
  public int getParameterType(int param) throws SQLException {
    throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
  }

  /**
   * Retrieves the designated parameter's database-specific type name.
   *
   * @param param the first parameter is 1, the second is 2, ...
   * @return type the name used by the database. If the parameter type is
   * a user-defined type, then a fully-qualified type name is returned.
   * @exception SQLException if a database access error occurs
   * @since 1.4
   */
  @Override
  public String getParameterTypeName(int param) throws SQLException {
    throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
  }

  /**
   * Retrieves the fully-qualified name of the Java class whose instances
   * should be passed to the method <code>PreparedStatement.setObject</code>.
   *
   * @param param the first parameter is 1, the second is 2, ...
   * @return the fully-qualified name of the class in the Java programming
   *         language that would be used by the method
   *         <code>PreparedStatement.setObject</code> to set the value
   *         in the specified parameter. This is the class name used
   *         for custom mapping.
   * @exception SQLException if a database access error occurs
   * @since 1.4
   */
  @Override
  public String getParameterClassName(int param) throws SQLException {
    throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
  }

  /**
   * Retrieves the designated parameter's mode.
   *
   * @param param the first parameter is 1, the second is 2, ...
   * @return mode of the parameter; one of
   *        <code>ParameterMetaData.parameterModeIn</code>,
   *        <code>ParameterMetaData.parameterModeOut</code>, or
   *        <code>ParameterMetaData.parameterModeInOut</code>
   *        <code>ParameterMetaData.parameterModeUnknown</code>.
   * @exception SQLException if a database access error occurs
   * @since 1.4
   */
  @Override
  public int getParameterMode(int param) throws SQLException {    
    return this.parameterMode[param-1];
  }

}
