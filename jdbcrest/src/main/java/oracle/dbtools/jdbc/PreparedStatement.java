/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TimeZone;
import java.util.AbstractMap.SimpleEntry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;

import oracle.dbtools.http.Session;
import oracle.dbtools.http.SessionException;
import oracle.dbtools.jdbc.util.DateTimestampsUtil;
import oracle.dbtools.jdbc.util.Defaults;
import oracle.dbtools.jdbc.util.ExceptionUtil;
import oracle.dbtools.jdbc.util.LogUtil;
import oracle.dbtools.jdbc.util.OracleTypes;
import oracle.dbtools.jdbc.util.RestJdbcUnsupportedException;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.dbtools.jdbc.util.SQLStateMapping;

/**
 * @author debjanibiswas
 */
public class PreparedStatement extends oracle.dbtools.jdbc.Statement implements java.sql.PreparedStatement{

  Logger LOGGER = Logger.getLogger(oracle.dbtools.jdbc.PreparedStatement.class.getName());
  
  // To prepare the POST
  protected JsonFactory f = new JsonFactory();
  protected Writer writer = new StringWriter();
  protected JsonGenerator g = null;
  
  // Flag to indicate if the PreparedStatement is open or closed
  protected boolean closed = false;
  
  // The current ResultSet object
  private ResultSet currResultSet = null;
  
  protected boolean isCloseOnCompletion = false;
  
  // For batched commands
  protected int batchIndex = 0;
  
  // Flag to indicate if there are any outParameters
  protected boolean outParams = false;
  
  // To get rid of duplicates store data to be set with indexes
  protected Map<Object, LinkedList<Object>> data = new HashMap<Object,  LinkedList<Object>>();

  // Store data types with indexes
  protected Map<Object, String> types = new HashMap<Object, String>();
  protected Map<Object,Integer> outTypes = new HashMap<Object,Integer>();
   
  protected PreparedStatement() { }
    
  protected PreparedStatement(oracle.dbtools.jdbc.Connection conn, String sql) {
    this.conn = conn;
    this.sql = sql;
  }
 
   
  @Override
  public ResultSet executeQuery() throws SQLException {
    CloseableHttpResponse response = executeInternal(this.sql); 
    this.rsCount++;
    this.updateCount = -1;
    this.currResultSet = new oracle.dbtools.jdbc.ResultSet(response, this, this.conn, this.sql);
    return currResultSet; 
  }

  @Override
  public int executeUpdate() throws SQLException {
    int result = 0;
    CloseableHttpResponse response = null;
    try {
      response = executeInternal(this.sql); 
      JsonNode node = createJsonNode(response);
      handleErrors(node); 
      result= getResult(node);
    }
    finally {
      if(response!=null) {
        try {
          response.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            ExceptionUtil.throwSQLException(17001);  // Internal Error while sending POST request
        }
      }         
    }
    this.updateCount = result;
    return result;
  }
  
  @Override
  public boolean execute() throws SQLException {
    boolean result = false; 
    CloseableHttpResponse response = null;
    try {
      response = executeInternal(this.sql);     
      JsonNode node = createJsonNode(response);
      // Is the result a ResultSet object?
      if(node.findValue("items").get(0).findValue("resultSet")!=null) {
        result = true;
      }
      handleErrors(node);
    }
    finally {
      if(response!=null) {
        try {
          response.close();
        } catch (IOException e) {
          LOGGER.log(Level.SEVERE, e.getMessage());
          ExceptionUtil.throwSQLException(17001);  // Internal Error while sending POST request
        }
      }
    }
    this.updateCount = 0;
    return result;
  }

  /**
   * Internal helper method to execute a PreparedStatement
   */
  @Override
  protected CloseableHttpResponse executeInternal(String sql) throws SQLException {
    ensureOpen();
    CloseableHttpResponse response=null;
    Session session = this.conn.getSession();
    
    // We have to add _/sql/test to the end of the rest endpoint for adhoc sql to work
    StringBuilder uri = new StringBuilder();
    uri.append(this.conn.getUri());
    uri.append(Defaults.ADHOC_ENDPOINT);
    
    URI root = URI.create(uri.toString());    
    try {
      // create a POST with appropriate headers
      HttpPost post = session.createPost(root);
      
      // Always want to get the verbose results
      post.addHeader("Content-Type", "application/json");
      
      // If there are no bind variables in the sql statement
      if(types.isEmpty() && outTypes.isEmpty()) {
        LOGGER.fine("No bind variables set");

        SimpleEntry<JsonGenerator, Writer> simpleEntry = generatePost(sql,Defaults.DEFAULT_OFFSET);            
        JsonGenerator g = simpleEntry.getKey();
        Writer writer   = simpleEntry.getValue();
        g.close();

        StringEntity strEntity = new StringEntity(writer.toString());
        post.setEntity(strEntity);          
      }
      // If bind variables have been set 
      else {
        LOGGER.fine("Bind variables are set");
        SimpleEntry<JsonGenerator, Writer> simpleEntry = generatePost(sql, Defaults.DEFAULT_OFFSET);       
        JsonGenerator g = null;
        Writer writer;
        
        // Are there any in parameters?
        if(!types.isEmpty()) {
          // Is this a batched Update?
          if(this.batchIndex==0) {
            g = processSets(simpleEntry.getKey());  
        }
        else {
          g = processBatchSets(simpleEntry.getKey());
        }
        }
        // Are there OUT parameters (for CallableStatements)?
        if(!outTypes.isEmpty()) {
          g = (g==null) ? processOUTs(simpleEntry.getKey()) : processOUTs(g);
        }      
        writer   = simpleEntry.getValue();         
        g.close();          
        StringEntity strEntity = new StringEntity(writer.toString());
        post.setEntity(strEntity);       
      }      
      // Execute post request
      response = session.executeRequest(post); 
    }     
    catch (IllegalStateException | IOException | SessionException e) 
    {
      LOGGER.log(Level.SEVERE, e.getMessage());
      // It's an internal error
      ExceptionUtil.throwSQLException(17001, e);   
    }
    return response;  
  }
  
  
  /**
   * Helper method to generate the POST request to be sent to ORDS 
   * Example POST request sent to ORDS:
   * { 
         "statement-text": "SELECT table_name FROM all_tables WHERE PCT_FREE > ? and PCT_USED < ?",
         "offset": 0,
         "limit": 25,
         "binds":[ {"index":1,"data_type":"int","value":20},
            {"index":2,"data_type":"int","value":40} 
          ]
      }
   * @throws SQLException 
   */  
  @Override
  protected SimpleEntry<JsonGenerator,Writer> generatePost(String sql, int offset) throws SQLException {    
    //To prepare the POST
    JsonFactory f = new JsonFactory();
    Writer writer = new StringWriter();
    JsonGenerator g = null;
    
    String stmtTxt = "statementText";
    String binds   = "binds"; 
    
    try {
      long fetchSize = this.getFetchSize();
      g = f.createGenerator(writer);
      g.writeStartObject();
      g.writeStringField(stmtTxt, sql);
      g.writeStringField("emulate", "jdbc");
      g.writeNumberField("offset", offset);
      if (fetchSize >0) {
        g.writeNumberField("limit",  fetchSize);
      }
      else {
        g.writeNumberField("limit",  Defaults.DEFAULT_LIMIT);
      }
      // If there are no binds
      if(types.isEmpty() && outTypes.isEmpty()) {
        g.writeEndObject();  
      }
      else {
        g.writeFieldName(binds); 
        g.writeStartArray();
      }
    } 
    catch (SQLException e) {
      throw e;
    }
    catch (IOException ioe) {
        LOGGER.log(Level.SEVERE, ioe.getMessage()); 
        // There's an issue with the Json writer... shouldn't go here
        // Throw an IO Error
        ExceptionUtil.throwSQLException(17002);             
    }    
    return new SimpleEntry<JsonGenerator, Writer>(g,writer);
  }
  
  /**
   * Helper method to process OUT binds and INOUT binds
   */
  private JsonGenerator processOUTs(JsonGenerator g) throws SQLException {  
  try {
    // Iterate through the out hashmap
    for(Object i: outTypes.keySet()) {
        // Check if there are "inout" parameters
        if(!types.containsKey(i)) {
        g.writeStartObject();
        
        if(i instanceof Integer) {
          g.writeNumberField("index", (int) i);
        }
        else if(i instanceof String) {
          g.writeStringField("name", (String) i);
        }         
        // data-type
        switch(outTypes.get(i)) {
          case java.sql.Types.TINYINT:
          case java.sql.Types.SMALLINT:
          case java.sql.Types.INTEGER:
          case java.sql.Types.BIGINT:
          case java.sql.Types.REAL:
          case java.sql.Types.NUMERIC:
          case java.sql.Types.DOUBLE:
          case java.sql.Types.DECIMAL:
            g.writeStringField("data_type",  "NUMBER");
            break;
          case java.sql.Types.FLOAT:
            g.writeStringField("data_type", "FLOAT");
            break;
          case java.sql.Types.VARCHAR:
          case java.sql.Types.LONGVARCHAR:
          case java.sql.Types.DATALINK:
          case java.sql.Types.CHAR:
            g.writeStringField("data_type", "VARCHAR");
            break;
          case java.sql.Types.DATE:
            g.writeStringField("data_type", "DATE");
            break;
          case java.sql.Types.TIME:
            g.writeStringField("data_type", "TIME");
            break;
          case java.sql.Types.TIMESTAMP:
            g.writeStringField("data_type", "TIMESTAMP");
            break;
          case java.sql.Types.BLOB:
            g.writeStringField("data_type", "BLOB");
            break;
          case java.sql.Types.CLOB:
            g.writeStringField("data_type", "CLOB");
            break;
          case java.sql.Types.BOOLEAN:
            g.writeStringField("data_type", "BOOLEAN");
            break;  
          case java.sql.Types.BIT:
            g.writeStringField("data_type", "BIT");
        } // end switch  
        g.writeStringField("mode", "out");
        g.writeEndObject();
      } // end if
    }    
  }
  catch (IOException e) {
    LOGGER.severe("Failure while generating POST\n" + e.getMessage());
    // Internal error while generating POST - should not go here
    ExceptionUtil.throwSQLException(17001);  
  } 
  return g;
 }
  
  
  /**
   * Close the PreparedStatement
   */
  @Override
  public void close() throws SQLException {
    // Release resources
    if(this.closed == false) {
      this.closed = true;
    
      // If the statement is closed release the ResultSet object
      this.currResultSet = null;
    }
  }
      
  @Override
  public int getUpdateCount() throws SQLException {
    ensureOpen();
    return this.updateCount;
  }


  /**
   * Returns the current ResultSet
   */
  @Override
  public ResultSet getResultSet() throws SQLException {
    ensureOpen(); // ensure the prepared stmt is open
    return currResultSet;
  }

  
  @Override
  public void setFetchDirection(int direction) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setFetchDirection(int direction)", this.getClass().getSimpleName()));
  }
  
  /**
   * Since Oracle database only supports forward only cursor.
   * This method always return FETCH_FORWARD.
   */  
  @Override
  public int getFetchDirection() throws SQLException {
    ensureOpen();
    return ResultSet.FETCH_FORWARD; 
  }

  /**
   * Returns the current connection object if 
   * the PreparedStatement is still open
   */
  @Override
  public Connection getConnection() throws SQLException {
    ensureOpen();
    return conn;
  }

  @Override
  public boolean isClosed() throws SQLException {   
    if(this.isCloseOnCompletion && this.rsCount==0) {
      this.close();
    }
    return this.closed;
  }

  @Override
  public void closeOnCompletion() throws SQLException {
    ensureOpen();
    if(!this.closed) {isCloseOnCompletion = true;}   
  }

  @Override
  public boolean isCloseOnCompletion() throws SQLException {
    ensureOpen();
    return this.isCloseOnCompletion;
  }

 
   /**
   * Helper method to process Sets
   * Useful for duplicate sets --> only need to record the last one
   * This generates the entire POST
   * @throws SQLException 
   */
  protected JsonGenerator processSets(JsonGenerator g) throws SQLException {
    String value = "value";  
    try {
      // iterate through the hashmap
      for(Object i: types.keySet()) { 
        g.writeStartObject();
        
        if(i instanceof Integer) {
          g.writeNumberField("index", (int) i);
        }
        else if(i instanceof String) {
          g.writeStringField("name", (String) i);
        }
        
        String data_type = types.get(i);
        
        // If it is not a double
        if(!(data_type.equals("DOUBLE") || data_type.equals("BIGDECIMAL"))) {
          g.writeStringField("data_type", types.get(i));
        }
        // Check if "inout" parameter
        if(outTypes.containsKey(i)) {
          g.writeStringField("mode", "inout");
        }
        switch(types.get(i)) {
          case "BOOLEAN" : 
          case "BIT":
            g.writeBooleanField(value, (boolean) data.get(i).getLast());                    
            break;
          case "byte":
            break;
          case "SHORT":
            g.writeNumberField(value, (short) data.get(i).getLast());
            break;
          case "INTEGER":
            g.writeNumberField(value, (int) data.get(i).getLast());
            break;
          case "NUMBER":
            if(data.get(i).getLast() instanceof Integer) {
              g.writeNumberField(value, (int) data.get(i).getLast());
            }
            else if((data.get(i).getLast() instanceof Short)) {
              g.writeNumberField(value, (short) data.get(i).getLast());
            }
            else if((data.get(i).getLast() instanceof Long)) {
              g.writeNumberField(value, (long) data.get(i).getLast());
            }
            else if((data.get(i).getLast() instanceof Double)) {
              g.writeNumberField(value, (double) data.get(i).getLast());
            }
            else {
              g.writeNumberField(value, (BigDecimal) data.get(i).getLast());
            }
            break;
          case "VARCHAR":
          case "DATALINK":
            g.writeStringField(value, (String) data.get(i).getLast());
            break;
          case "VARCHAR2":
            g.writeStringField(value, (String) data.get(i).getLast());
            break;
          case "FLOAT":
            g.writeNumberField(value, (float) data.get(i).getLast());
            break;
          case "DOUBLE":
            g.writeStringField("data_type", "NUMBER");
            g.writeNumberField(value, (double) data.get(i).getLast());
            break;
          case "BIGDECIMAL":
            g.writeStringField("data_type", "NUMBER");
            g.writeNumberField(value, (BigDecimal) (data.get(i).getLast()));
            break;
          case "String":
          case "DATE":
          case "TIMESTAMPTZ":
          case "TIMESTAMP":
            g.writeStringField(value, (String) data.get(i).getLast());
            break;
          case "CLOB":
            g.writeObjectField(value, data.get(i).getLast());
            break;
      } // end switch
        g.writeEndObject();        
    } // end for loop
  } // end try
    catch (IOException e) {
      LOGGER.severe("Failure while generating POST\n" + e.getMessage());
      // Internal error - 
      ExceptionUtil.throwSQLException(17001);
    }   
    return g;
  }   
  
  /**
   * Helper method for processing Batch binds
   * Iterates through the list of values in data and adds it to the Json Generator
   * @param JsonGenerator - the JsonGenerator to add list of values to
   * @return JsonGenerator - the updated JsonGenerator with the added list of values of the batch
   * @throws SQLException 
   */
  protected JsonGenerator batchHelper(String type, JsonGenerator g, LinkedList list) throws SQLException {
    try {
      g.writeArrayFieldStart("value");
      switch(type) {
        case "VARCHAR2":
        case "VARCHAR":
        case "CHAR":
        case "DATE":
          case "TIMESTAMP":
        case "TIMESTAMPTZ":
        case "DATALINK":
          for(int i=0; i <list.size(); i++) {
            g.writeString(list.get(i).toString());
          }
            break;
        case "INTEGER":          
        case "NUMBER":
          for(int i=0; i <list.size(); i++) {
            if(list.get(i) instanceof Integer){
            g.writeNumber((int) list.get(i));
          }
            else if (list.get(i) instanceof Double)
            g.writeNumber((double) list.get(i));
          }
          break;
        case "BOOLEAN":
          for(int i=0; i <list.size(); i++) {
            g.writeBoolean((boolean) list.get(i)); 
          }
          break;
        case "BIGDECIMAL":
          for(int i=0; i <list.size(); i++) {
            g.writeNumber( (BigDecimal) list.get(i));
          }
          break;
        case "FLOAT":
          for(int i=0; i <list.size(); i++) {
            g.writeNumber((float) list.get(i));
          }
          break;
        case "DOUBLE":
          for(int i=0; i <list.size(); i++) {
            g.writeNumber((double) list.get(i));
          }
          break;
        case "CLOB":
          for(int i=0; i <list.size(); i++) {
            g.writeString(list.get(i).toString());
          }
          break;            
      } // end switch
      g.writeEndArray();
    }
    // Throw internal error: This should not happen
    catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage());
      ExceptionUtil.throwSQLException(17001);
    }
    return g;
  }
  
  
  /**
   * Helper method to process OUT binds
   * Useful for duplicate sets --> only need to record the last one
   * This generates the entire POST
   * @throws SQLException 
   */
  protected JsonGenerator processBatchSets(JsonGenerator g) throws SQLException {
    String value = "value";
    try {
      // iterate through the hashmap
      for(Object i: types.keySet()) {             
        g.writeStartObject();
        
        if(i instanceof Integer) {
          g.writeNumberField("index", (int) i);
        }
        else if(i instanceof String) {
          g.writeStringField("name", (String) i);
        }        
        String data_type = types.get(i);
        // If it is not a double
        if(!(data_type.equals("DOUBLE") || data_type.equals("BIGDECIMAL"))) {
          g.writeStringField("data_type", types.get(i));
        }
        else {
          g.writeStringField("data_type", "NUMBER");
        }        
        g.writeBooleanField("batch", true);
        g = batchHelper(data_type, g, data.get(i));
        g.writeEndObject();        
      } // end for loop
    }
    catch (IOException e) {
      LOGGER.severe("Exception while generating Json POST request\n" + e.getMessage());
      // Issue with the JSON writer - internal error
      ExceptionUtil.throwSQLException(17001);         
    }
    return g;
  }
  
  /**
   * Helper method for OUT binds
   * Checks if List is already created for data and adds to the list, if list already present.
   * If not present, a new list is created 
   * @param parameterIndex - the parameterIndex or paramterName for the bind
   * @return returns the list to be inserted
   */

  protected void updateList(Object parameterIndex, Object x) {
    // If there is data for the parameter Index / name, add to existing list
    if(data.containsKey(parameterIndex)) {
      // If there is already an element at the batchIndex
      if(this.batchIndex >= data.get(parameterIndex).size()) {
        data.get(parameterIndex).add(this.batchIndex, x);
      }
      else {
        data.get(parameterIndex).set(this.batchIndex, x);
      }      
    }
    // If no data for the parameterIndex/name, create new list
    else {
      LinkedList<Object> list = new LinkedList<Object>();
      list.add(this.batchIndex, x);
      data.put(parameterIndex, list);
    }
  }
  
  
  @Override
  public void setNull(int parameterIndex, int sqlType) throws SQLException {
    LOGGER.finest("Setting Null");
    updateList(parameterIndex,  null);
    types.put(parameterIndex, OracleTypes.getSQLTypeName(sqlType));
  }

  @Override
  public void setBoolean(int parameterIndex, boolean x) throws SQLException {
    LOGGER.finest("Setting Boolean");
    updateList(parameterIndex,  x);
    types.put(parameterIndex, "BOOLEAN");
  }

  @Override
  public void setByte(int parameterIndex, byte x) throws SQLException {
    LOGGER.finest("Setting byte");
    updateList(parameterIndex,  x);
    types.put(parameterIndex, "NUMBER");  
  }


  @Override
  public void setShort(int parameterIndex, short x) throws SQLException {
    LOGGER.finest("Setting Short");
    updateList(parameterIndex,  x);
    types.put(parameterIndex, "NUMBER");
  }

  @Override
  public void setInt(int parameterIndex, int x) throws SQLException {
    LOGGER.finest("Setting Int");
    updateList(parameterIndex,  x);
    types.put(parameterIndex, "INTEGER");    
  }

  @Override
  public void setLong(int parameterIndex, long x) throws SQLException {
    LOGGER.finest("Setting Long");
    updateList(parameterIndex,  x);
    types.put(parameterIndex, "NUMBER");
  }

  @Override
  public void setFloat(int parameterIndex, float x) throws SQLException {
    LOGGER.finest("Setting Float");
    updateList(parameterIndex,  x);
    types.put(parameterIndex, "FLOAT");    
  }
  
  @Override
  public void setDouble(int parameterIndex, double x) throws SQLException {
    LOGGER.finest("Setting Double");
    updateList(parameterIndex,  x);
    types.put(parameterIndex, "DOUBLE");      
  }

  @Override
  public void setBigDecimal(int parameterIndex, BigDecimal x)
      throws SQLException {
    LOGGER.finest("Setting BigDecimal");
    updateList(parameterIndex,  x);
    types.put(parameterIndex, "BIGDECIMAL");     
  }

  @Override
  public void setString(int parameterIndex, String x) throws SQLException {
    LOGGER.finest("Setting String");
    updateList(parameterIndex,  x);
    types.put(parameterIndex, "VARCHAR");   
  }

  /**
   * Posts a pre-formatted date 
   */
  @Override
  public void setDate(int parameterIndex, Date x) throws SQLException {
    LOGGER.finest("Setting Date");
    // Get a calendar with the client timezone
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterIndex,  ts.setDate(x,this.conn.getTimeZoneID(),cal));
    types.put(parameterIndex, "DATE");
  }
   
  /**
   * Posts a pre-formatted time 
   */
  @Override
  public void setTime(int parameterIndex, Time x) throws SQLException {
    LOGGER.finest("Setting Time");  
    String timeZoneID = this.conn.getTimeZoneID();
    
    // Get a calendar with the client timezone
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterIndex,ts.setTime(x, timeZoneID,cal));
    types.put(parameterIndex, "DATE");
  }

  /**
   * Posts a pre-formatted timestamp 
   */
  @Override
  public void setTimestamp(int parameterIndex, Timestamp x)
      throws SQLException {
    
    LOGGER.finest("Setting Timestamp");
    // Get the timeZone
    String timeZoneID = this.conn.getTimeZoneID();
    
    Calendar defaultCal = Calendar.getInstance(TimeZone.getDefault());    
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterIndex,ts.setTimeStamp(x, timeZoneID,defaultCal));
    types.put(parameterIndex, "TIMESTAMP");
  }
  @Override
  public void setDate(int parameterIndex, Date x, Calendar cal)
      throws SQLException {

    LOGGER.finest("Setting Date");    
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterIndex,ts.setDate(x,this.conn.getTimeZoneID(),cal));
    types.put(parameterIndex, "DATE");
  }

  @Override
  public void setTime(int parameterIndex, Time x, Calendar cal)
      throws SQLException {

    String timeZoneID = this.conn.getTimeZoneID();
    DateTimestampsUtil ts = new DateTimestampsUtil();    
    updateList(parameterIndex,ts.setTime(x, timeZoneID,cal));
    types.put(parameterIndex, "TIMESTAMP");
  }

  @Override
  public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal)
      throws SQLException {
    LOGGER.finest("Setting Timestamp with Calendar");
    
    String timeZoneID = this.conn.getTimeZoneID();    
    DateTimestampsUtil ts = new DateTimestampsUtil();
    updateList(parameterIndex,ts.setTimeStamp(x, timeZoneID, cal));
    types.put(parameterIndex, "TIMESTAMP");    
  }

  @Override
  public void setClob(int parameterIndex, Clob x) throws SQLException {
    LOGGER.finest("Setting Clob");
    updateList(parameterIndex,x.getSubString(1, (int) x.length()));
    types.put(parameterIndex, "CLOB");
  }
  
  @Override
  public void setURL(int parameterIndex, URL x) throws SQLException {
    LOGGER.finest("Setting URL");
    updateList(parameterIndex,x.toString());
    types.put(parameterIndex, "VARCHAR2");
  }
  

  @Override
  public void clearParameters() throws SQLException {
    ensureOpen();
    this.types.clear();
    this.data.clear();
  }

  // Internal method to get information of binds
  protected Map<Object, LinkedList<Object>> getData() {
    return this.data;
  }

  protected Map<Object, String> getTypes() {
    return this.types;
} 
  @Override
  public void setObject(int parameterIndex, Object x) throws SQLException {
    // The simple data types
    if(x instanceof Boolean) {
      setBoolean(parameterIndex, (boolean) x);
    }
    else if(x instanceof Integer) {
      setInt(parameterIndex, (int) x);
    }
    else if(x instanceof Short) {
      setShort(parameterIndex, (short) x);
    }
    else if(x instanceof Long) {
      setLong(parameterIndex, (long) x);
    }
    else if(x instanceof Float) {
      setFloat(parameterIndex, (float) x);
    }
    else if(x instanceof Double) {
      setDouble(parameterIndex, (double) x);
    }
    else if(x instanceof String) {
      setString(parameterIndex, (String) x);
    }
    else if(x instanceof java.sql.Date) {
      setDate(parameterIndex, (Date) x);
    }
    else if(x instanceof java.sql.Time) {
      setTime(parameterIndex, (Time) x);
    }
    else if(x instanceof java.sql.Timestamp) {
      setTimestamp(parameterIndex, (Timestamp) x);
    }
    else if(x instanceof BigDecimal) {
      setBigDecimal(parameterIndex, (BigDecimal) x);
    }
    else if(x instanceof java.io.InputStream) {
      setAsciiStream(parameterIndex, (InputStream) x);
    }
  }


  @Override
  public void addBatch() throws SQLException {
    ensureOpen();
    if(this.outParams == true) {
      int errorCode = 17090;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17090)+
                             " Stored procedure with out or inout parameters cannot be batched", 
                             SQLStateMapping.getSQLState(errorCode), errorCode);      
    }
    this.batch = true;
    this.batchIndex++;   
  }

  public void clearBatch() throws SQLException {
    this.batch = false;
    this.batchIndex = 0;
    cmds.clear();
  }

  
  /**
   * Helper method to ensure that a statement is not closed
   */
  @Override
  protected void ensureOpen() throws SQLException {
    // If the statement is closed
    if (this.closed) {    
      ExceptionUtil.throwSQLException(17009);
    }   
    // If the connection is closed
    if(conn.isClosed()) {
      ExceptionUtil.throwSQLException(17008);      
    }
  }
  
  
 /**** ALL UNSUPPORTED METHODS ****/
  
  /**
   * Cursors are not supported in the REST JDBC driver
   * @throws SQLFeatureNotSupportedException
   */
  @Override
  public void setCursorName(String name) throws SQLException {
    ExceptionUtil.throwSQLException("setCursorName(String name)", "PreparedStatement"); 
  }
  
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public int getResultSetConcurrency() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getResultSetConcurrency()", this.getClass().getSimpleName()));
  }
  
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public boolean getMoreResults(int current) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getMoreResults(int current)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public ResultSet getGeneratedKeys() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getGeneratedKeys()", this.getClass().getSimpleName()));
  }

  /**
   * The REST JDBC driver does not support cursors and holdability
   */
  @Override
  public int getResultSetHoldability() throws SQLException {
    ensureOpen();
    LOGGER.info(RestjdbcResources.get(RestjdbcResources.RESTJDBC_024));  // Holdability is not supported in the REST JDBC driver.
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "setHoldability(int holdability)",  this.getClass().getSimpleName()));
  }
  
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void cancel() throws SQLException {
    ExceptionUtil.throwSQLException("cancel()", "PreparedStatement"); 
  }
  
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void clearWarnings() throws SQLException {
    ExceptionUtil.throwSQLException("clearWarnings()", "PreparedStatement"); 
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public boolean getMoreResults() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getMoreResults()", this.getClass().getSimpleName()));
  }
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setUnicodeStream(int parameterIndex, InputStream x, int length)
      throws SQLException {
    ExceptionUtil.throwSQLException("setUnicodeStream(int parameterIndex, InputStream x, int length)", 
                                    "PreparedStatement"); 
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setBinaryStream(int parameterIndex, InputStream x, int length)
      throws SQLException {
    ExceptionUtil.throwSQLException("setBinaryStream(int parameterIndex, InputStream x, int length)", 
                                    "PreparedStatement");  
  }
  
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setBytes(int parameterIndex, byte[] x) throws SQLException {
    ExceptionUtil.throwSQLException("setBytes(int parameterIndex, byte[] x)", "PreparedStatement");
  }
  
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setObject(int parameterIndex, Object x, int targetSqlType)
      throws SQLException {
    ExceptionUtil.throwSQLException("setObject(int parameterIndex, Object x, int targetSqlType)", 
                                    "PreparedStatement");
  }
  
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setCharacterStream(int parameterIndex, Reader reader, int length)
      throws SQLException {
    ExceptionUtil.throwSQLException("setCharacterStream(int parameterIndex, Reader reader, int length)", 
                                    "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setRef(int parameterIndex, Ref x) throws SQLException {
    ExceptionUtil.throwSQLException("setRef(int parameterIndex, Ref x)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setBlob(int parameterIndex, Blob x) throws SQLException {
    ExceptionUtil.throwSQLException("setBlob(int parameterIndex, Blob x)", "PreparedStatement");
  }


  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setArray(int parameterIndex, Array x) throws SQLException {
    ExceptionUtil.throwSQLException("setArray(int parameterIndex, Array x)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public ResultSetMetaData getMetaData() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getMetaData()", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public int executeUpdate(String sql, int autoGeneratedKeys)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "executeUpdate(String sql, int autoGeneratedKeys)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public int executeUpdate(String sql, int[] columnIndexes)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "executeUpdate(String sql, int[] columnIndexes)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public int executeUpdate(String sql, String[] columnNames)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "executeUpdate(String sql, String[] columnNames)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public boolean execute(String sql, int autoGeneratedKeys)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "execute(String sql, int autoGeneratedKeys)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public boolean execute(String sql, int[] columnIndexes) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "execute(String sql, int[] columnIndexes)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public boolean execute(String sql, String[] columnNames) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "execute(String sql, String[] columnNames)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public int getMaxFieldSize() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getMaxFieldSize()", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setMaxFieldSize(int max) throws SQLException {
    ExceptionUtil.throwSQLException("setMaxFieldSize(int max)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public int getQueryTimeout() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "getQueryTimeout()", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setQueryTimeout(int seconds) throws SQLException {
    ExceptionUtil.throwSQLException("setQueryTimeout(int seconds)", "PreparedStatement");
  }


  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public SQLWarning getWarnings() throws SQLException {
    LogUtil.logOrException(this.getConnection(), null, "PreparedStatement.getWarnings() not supported", null);
    return null;
  }
  
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setNull(int parameterIndex, int sqlType, String typeName)
      throws SQLException {
    ExceptionUtil.throwSQLException("setNull(int parameterIndex, int sqlType, String typeName)", 
                                    "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public ParameterMetaData getParameterMetaData() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
        "getParameterMetaData()", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setRowId(int parameterIndex, RowId x) throws SQLException {
    ExceptionUtil.throwSQLException("setRowId(int parameterIndex, RowId x)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setNString(int parameterIndex, String value) throws SQLException {
    ExceptionUtil.throwSQLException("setNString(int parameterIndex, String value)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setNCharacterStream(int parameterIndex, Reader value, long length)
      throws SQLException {
    ExceptionUtil.throwSQLException("setNCharacterStream(int parameterIndex, Reader value, long length)", 
                                    "PreparedStatement");
  }
  
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setNClob(int parameterIndex, NClob value) throws SQLException {
    ExceptionUtil.throwSQLException("setNClob(int parameterIndex, NClob value)", 
                                    "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
    ExceptionUtil.throwSQLException("setClob(int parameterIndex, Reader reader, long length)", 
                                    "PreparedStatement");  
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setBlob(int parameterIndex, InputStream inputStream, long length)
      throws SQLException {
    ExceptionUtil.throwSQLException("setBlob(int parameterIndex, InputStream inputStream, long length)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setNClob(int parameterIndex, Reader reader, long length)
      throws SQLException {
    ExceptionUtil.throwSQLException("setNClob(int parameterIndex, Reader reader, long length)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setSQLXML(int parameterIndex, SQLXML xmlObject)
      throws SQLException {
    ExceptionUtil.throwSQLException("setSQLXML(int parameterIndex, SQLXML xmlObject)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setObject(int parameterIndex, Object x, int targetSqlType,
      int scaleOrLength) throws SQLException {
    ExceptionUtil.throwSQLException("(int parameterIndex, Object x, int targetSqlType,int scaleOrLength)", 
                                    "PreparedStatement");
  }

  /**
   * This method is beta in the REST JDBC driver
   */
  @Override
  public void setAsciiStream(int parameterIndex, InputStream x, int length)
      throws SQLException {
    LOGGER.finest("Setting AsciiStream");
    setAsciiStream(parameterIndex, x, (long) length);
  }
  
  /**
   * This method is beta in the REST JDBC driver
   */
  @Override
  public void setAsciiStream(int parameterIndex, InputStream x, long length)
      throws SQLException {

    try { 
      if(length < 0) {
        int error_code = 17068;
        String message = RestjdbcResources.getString(RestjdbcResources.ORA_17068) + ": " + 
                         RestjdbcResources.getString(RestjdbcResources.RESTJDBC_019);
        throw new SQLException(message, SQLStateMapping.getSQLState(error_code), error_code);  
       }
      StringBuilder sb = new StringBuilder();
      long bytesRead = 0; // keeps count of the number of bytes read

      /*
       * A note on performance and memory: One could also use the x.read(byte, int, int) 
       * to read the length of bytes into an array but java.io.InputStream 
       * reads the stream into a byte array in a loop as well. Additionally, we 
       * don't want an extra memory overhead by storing it into a byte[] array
       * as we will ultimately send a string upto adhoc.
       */
      while(bytesRead < length) {
        int bytes = x.read();
        // Reached the end of the stream
        if(bytes < 0) {
          break;
        }
        else {
          sb.append((char)bytes);
        }
        bytesRead ++;
      }
      updateList(parameterIndex, sb.toString());
      types.put(parameterIndex, "VARCHAR");
    }
    catch(SQLException e) {
      throw e;
    }
    catch(IOException ioe) {
      LOGGER.log(Level.SEVERE, ioe.getMessage());
      // internal error 
      ExceptionUtil.throwSQLException(17001);
    }
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setBinaryStream(int parameterIndex, InputStream x, long length)
      throws SQLException {
    ExceptionUtil.throwSQLException("setBinaryStream(int parameterIndex, InputStream x, long length)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setCharacterStream(int parameterIndex, Reader reader, long length)
      throws SQLException {
    ExceptionUtil.throwSQLException("setCharacterStream(int parameterIndex, Reader reader, long length)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setAsciiStream(int parameterIndex, InputStream x)
      throws SQLException {

    try {      
      StringBuilder sb = new StringBuilder();
      int bytes = 0;
            
      while(bytes != -1) {
         bytes = x.read();
        // Reached the end of the stream
        if(bytes < 0) {
          break;
  }
        else {
          sb.append((char)bytes);
        }
      }
      updateList(parameterIndex, sb.toString());
      types.put(parameterIndex, "CLOB");
    }    
    catch(IOException ioe) {
      LOGGER.log(Level.SEVERE, ioe.getMessage());
    }
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setBinaryStream(int parameterIndex, InputStream x)
      throws SQLException {
    ExceptionUtil.throwSQLException("setBinaryStream(int parameterIndex, InputStream x)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setCharacterStream(int parameterIndex, Reader reader)
      throws SQLException {
    ExceptionUtil.throwSQLException("setCharacterStream(int parameterIndex, Reader reader)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setNCharacterStream(int parameterIndex, Reader value)
      throws SQLException {
    ExceptionUtil.throwSQLException("setNCharacterStream(int parameterIndex, Reader value)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setClob(int parameterIndex, Reader reader) throws SQLException {
    ExceptionUtil.throwSQLException("setClob(int parameterIndex, Reader reader)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setBlob(int parameterIndex, InputStream inputStream)
      throws SQLException {
    ExceptionUtil.throwSQLException("setBlob(int parameterIndex, InputStream inputStream)", "PreparedStatement");
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public void setNClob(int parameterIndex, Reader reader) throws SQLException {
    ExceptionUtil.throwSQLException("setNClob(int parameterIndex, Reader reader)", "PreparedStatement");
  }
  
  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public <T> T unwrap(Class<T> iface) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "unwrap(Class<T> iface)", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public boolean isWrapperFor(Class<?> iface) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        "isWrapperFor(Class<?> iface)", this.getClass().getSimpleName()));
  }
}
