/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.io.ByteArrayInputStream;
import java.io.IOException;  
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import oracle.dbtools.http.Session;
import oracle.dbtools.http.SessionException;
import oracle.dbtools.jdbc.util.DateTimestampsUtil;
import oracle.dbtools.jdbc.util.Defaults;
import oracle.dbtools.jdbc.util.ExceptionUtil;
import oracle.dbtools.jdbc.util.LogUtil;
import oracle.dbtools.jdbc.util.OracleTypes;
import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.jdbc.util.RestJdbcUnsupportedException;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.dbtools.jdbc.util.SQLStateMapping;


/**
 * @author debjanibiswas
 *
 */
public class ResultSet implements java.sql.ResultSet {
  
  // Logger for ResultSet
  Logger LOGGER = Logger.getLogger(oracle.dbtools.jdbc.ResultSet.class.getName());
 
  // Token to indicate the beginning of results in the Json stream
  private static final String TOKEN_START_STR = "items";
  
  // Token to indicate the start of the metadata section 
  private static final String TOKEN_START_METADATA = "metadata";
  
  // Token to indicate an error
  private final String TOKEN_ERR = "errorCode";
  
  // Token to indicate response section
  private static final String TOKEN_RESPONSE = "response";
  
  // Indicator of start and end of a String - ascii --> "
  private static final int QUOTES = 34;
  
  // Tokens for newlines
  private static final int BACKSLASH = 92;
  private static final int NEWLINE = 110;
  private static final int SPACE = 32;
  
  // Json Start Object - { - ascii
  private static final int START_OBJECT = 123;
  
  // Json End Object - } - ascii
  private static final int END_OBJECT = 125;
  
  // Json Start Array - [ - ascii
  private static final int START_ARRAY = 91;
  
  // Json End array - ] - ascii
  private static final int END_ARRAY = 93;
        
  // Flag to turn on debuging
  private boolean debug = false;
  
  // Flag to indicate the start of the results section has started in the Json stream
  private boolean isResultsStarted = false;
  
  // Flag to indicate the end of the results section in the Json stream
  private boolean isResultsEnded = false;
  
  // the current character we're on while streaming
  private int c;
  
  // HashMap for column Names and their corresponding columnIndex
  protected LinkedHashMap<String, Integer> colNames = new LinkedHashMap<String,Integer>();
  protected boolean colNamesExtracted = false;
  
  // HashMap for columnIndex and column Values 
  private Map<Integer, JsonNode> rowData = new HashMap<Integer,JsonNode>();
  
  // Metadata stored as a JsonTree
  private JsonNode rsMetadataTree = null;
   
  private InputStream stream = null;
  
  // Is there a metadata section ? 
  private boolean metadata = false;
  
  // Is there a result section?
  private boolean emptyResultSet = false;
  
  // To retrive the statement object
  private oracle.dbtools.jdbc.Statement stmt = null;
  
  // Retrieve the PreparedStatement object
  private oracle.dbtools.jdbc.PreparedStatement pstmt = null;
  
  // Retrieve the CallableStatement object 
  private oracle.dbtools.jdbc.CallableStatement cstmt = null;
  
  // Has the result set object been closed?
  private boolean isResultSetClosed = false;
  
  // The max number of rows that any ResultSet object generated
  // by this Statement object can contain to the given number
  // zero means there is no limit
  private long maxRows = 0;
  
  // Is the ResultSet closed?
  protected boolean closed = false;
  
  // Offset for fetchSize
  private int offset = 0;

  // The sql string related to this ResultSet
  private String sql = null;
  
  // Was the last column read a null?
  protected boolean wasNull = false;
    
  // Connection 
  protected oracle.dbtools.jdbc.Connection conn = null;

  // For binds
  // Store data to be set with indexes
  protected Map<Object, LinkedList<Object>> data = new HashMap<Object,  LinkedList<Object>>();
  // Store data types with indexes
  protected Map<Object, String> types = new HashMap<Object, String>();
  //For out indexes
  private Map<Object,Integer> outTypes = new HashMap<Object,Integer>();
  
  // The timezone associated with the resultSet
  private String timeZoneID = null;
  
  // Boolean to indicate if ResultSet.next() has been called
  private boolean next = false;
  
  /** (non-Javadoc)
   * Constructor for Statement with CloseableHttpResponse
   */
  public ResultSet(CloseableHttpResponse response, oracle.dbtools.jdbc.Statement stmt, Connection conn) 
      throws SQLException {
    
    HttpEntity entity = response.getEntity();
    this.conn = conn;
    this.sql = stmt.getSqlString();
    this.fetchSize = stmt.getFetchSize();
    
    this.stmt = stmt;
    this.pstmt = null;
    this.cstmt = null;
    try {
      maxRows = stmt.getMaxRows();
      this.stream = entity.getContent();
            
      // Extract metadata for the ResultSet
      if(metadataCreated == false) {
        // Is there a ResultSet section?
        if(isResultSetSection()) {     
          startMetaData();
          createMetadataTree();        
          metadataCreated = true;
        }    
      }   
    } catch (IllegalStateException | IOException e) {
      LOGGER.log(Level.SEVERE, e.getCause().toString());     
    } 
  }
  
  /** (non-Javadoc)
   * Constructor for PreparedStatement with CloseableHttpResponse
   */
  public ResultSet(CloseableHttpResponse response, oracle.dbtools.jdbc.PreparedStatement stmt, Connection conn, String sql) 
      throws SQLException {
    
    HttpEntity entity = response.getEntity();
    this.sql   = sql;
    this.conn  = conn;
    this.pstmt = stmt;
    this.stmt  = null;
    this.cstmt = null;
    this.fetchSize = stmt.getFetchSize();
   
    // get bind information
    this.types = stmt.getTypes();
    this.data  = stmt.getData();    
    try {
      maxRows = stmt.getMaxRows();
      this.stream = entity.getContent();
      
      // Extract metadata for the ResultSet
      if(metadataCreated == false) {
        // Is there a ResultSet section?
        if(isResultSetSection()) {  
          startMetaData();
          createMetadataTree();        
          metadataCreated = true;
        }    
      }     
    }catch (IllegalStateException | IOException e) {
      LOGGER.log(Level.SEVERE, e.getCause().toString());     
    } 
  }
  
  /** (non-Javadoc)
   * Constructor for CallableStatement with CloseableHttpResponse
   */
  public ResultSet(CloseableHttpResponse response,oracle.dbtools.jdbc.CallableStatement stmt, Connection conn) throws SQLException {
    
    HttpEntity entity = response.getEntity();
    this.conn  = conn;
    this.cstmt = stmt; 
    this.pstmt = null;
    this.stmt  = null;
  
    this.fetchSize = stmt.getFetchSize();
    
    // get bind information for pagination
    this.types    = stmt.getTypes();
    this.data     = stmt.getData();
    this.outTypes = stmt.getOutTypes();
        
    try {
      maxRows = stmt.getMaxRows();
      this.stream = entity.getContent();
      
      // Has the metadata been created?
      if(metadataCreated == false) {
        // First check if there is a resultSet section
        if(isResultSetSection()) {      
          startMetaData();
          createMetadataTree();        
          metadataCreated = true;
        }    
      }
     
    } catch (IllegalStateException | IOException e) {
        LOGGER.log(Level.SEVERE, e.getCause().toString());     
    } 
  }
  
  /** (non-Javadoc)
   * Constructor for CallableStatement with InputStream
   */
  public ResultSet(InputStream is,oracle.dbtools.jdbc.CallableStatement stmt) throws SQLException {
       
    this.cstmt = stmt;    
    this.pstmt = null;
    this.stmt  = null;
  
    this.fetchSize = stmt.getFetchSize();
    
    // get bind information for pagination
    this.types    = stmt.getTypes();
    this.data     = stmt.getData();
    this.outTypes = stmt.getOutTypes();
    
    try {
      maxRows = stmt.getMaxRows();
      this.stream = is;
     
      // Extract metadata for the ResultSet
      if(metadataCreated == false) {
        // Is there a ResultSet section?
        if(isResultSetSection()) {        
          startMetaData();
          createMetadataTree();        
          metadataCreated = true;
        }    
      }
    } catch (IllegalStateException e) {
        LOGGER.log(Level.SEVERE, e.getCause().toString());     
    } 
  }
  
/**
 * Constructor to retrieve a result as a ResultSet object
 * @param metadata 
 * @param items
 * @throws SQLException
 */
  public ResultSet(JsonNode metadata, String items) 
      throws SQLException {
    
    this.rsMetadataTree = metadata; 
  }
  
  
  /*
   * Helper method to create data structure for ResultSetMetadata
   */
  protected void createMetadataTree() throws SQLException {
    
    // If you're trying to create a metadata for an empty ResultSet
    if(this.emptyResultSet == true) {
      throw new SQLException(RestjdbcResources.getString("RESTJDBC_007"));
    }
    
    LOGGER.info("Extracting ResultSet metadata");
    
    // Variable to determine if the metadata section has ended
    boolean metadataEnded = false;
    StringBuilder sb = new StringBuilder();
    
    int innerJsonObj = 0;
    int innerJsonArray = 0;
       
    // Append a start object (removed it when removing space)
    sb.append((char)oracle.dbtools.jdbc.ResultSet.START_OBJECT);
    sb.append("\"");
    sb.append(oracle.dbtools.jdbc.ResultSet.TOKEN_START_METADATA);
    sb.append("\"");
        
    while(metadataEnded == false) {
      try {
        this.c = this.stream.read();

        if(this.c==oracle.dbtools.jdbc.ResultSet.START_OBJECT) {
          innerJsonObj ++;
        }            
        else if(this.c==oracle.dbtools.jdbc.ResultSet.START_ARRAY) {
          innerJsonArray ++;
        }            
        else if(this.c==oracle.dbtools.jdbc.ResultSet.END_OBJECT) {
          innerJsonObj --;
        }            
        else if(this.c==oracle.dbtools.jdbc.ResultSet.END_ARRAY) {
          innerJsonArray --;
        }  
        
        // Condition to check if the row has ended 
        if(innerJsonArray==0 && innerJsonObj==0 && this.c==oracle.dbtools.jdbc.ResultSet.END_ARRAY){
          metadataEnded = true;
          // Read the next byte - the comma at the end of one json object
          this.stream.read();
        }            
        sb.append((char)this.c);      
      } // end try      
      catch (IOException e) {        
        Logger.getLogger(this.getClass().getName()).log(Level.INFO,e.getMessage(),e);
      }
     } // end while
   
    sb.append((char)oracle.dbtools.jdbc.ResultSet.END_OBJECT);
    
    // Now convert to json tree
    ObjectMapper mapper= new ObjectMapper();   
    try {      
      this.rsMetadataTree = mapper.readTree(sb.toString());
      // Don't want to store additional stuff in memory
      sb = null;
    } catch (IOException e) {      
        Logger.getLogger(this.getClass().getName()).log(Level.INFO,e.getMessage(),e);
    }  
  }
  

  private String getTimeZoneID() {
    StringBuilder sb = new StringBuilder();
    String tzToken = "defaultTimeZone";
    String timeZoneID = null;
    boolean getEnv = false;
    int quotes = 0;
    while(getEnv == false && this.c >-1) {
      try {
        this.c = this.stream.read();
        
        // Construct the string within the quotes
        if(this.c == oracle.dbtools.jdbc.ResultSet.QUOTES) {
          quotes ++;
        }
        else if(quotes == 1) {
          sb.append((char) this.c);
        }
        // End of String
         if(quotes==2) {
          quotes=0;
          
          // Either it is the start of the resultSet section -- set variables
          if(sb.toString().equals(tzToken)) {  
              getEnv = true;
              int q = 0;
              sb.setLength(0);
              while(q!=2) {
                this.c = this.stream.read();
                if(this.c == oracle.dbtools.jdbc.ResultSet.QUOTES) {
                  q++;
                }
                else if(q == 1) {
                  sb.append((char) this.c);
                }               
              }
              timeZoneID = sb.toString();             
          }
          else {
            // Reinitialize the stringbuilder
            sb.setLength(0);
          }
        }        
      } catch (IOException e) {
          LOGGER.severe(e.getMessage());
      }      
    }
    return timeZoneID;   
  }
  
  
  /**
   * Helper method to determine if there is a resultSet section
   * in the returned json response
   */
  private boolean isResultSetSection() throws SQLException {
    LOGGER.finer("Checking for a ResultSet section in the response");
    
    this.timeZoneID = getTimeZoneID();
    boolean result = false;
    // To determine the start and end of a string
    int quotes = 0;
    
    // String for indicating start string
    StringBuilder sb = new StringBuilder();
    
    boolean rsSection = false;
    String resultSetToken = "resultSet";

    StringBuilder error   = new StringBuilder();
    error.append('{');
    boolean isError = false;
    try {
      while(rsSection == false && c>-1) {
        this.c = this.stream.read();
        
        if(isError) {
          error.append((char) c);
        }
        // Construct the string within the quotes
        else if(this.c == oracle.dbtools.jdbc.ResultSet.QUOTES) {
          quotes ++;
        }
        else if(quotes == 1) {
          sb.append((char) this.c);
        }
        // End of String
        if(!isError && quotes==2) {
          quotes=0;
          
          // It is the start of the resultSet section -- set variables
          if(sb.toString().equals(resultSetToken)) {  
              rsSection = true;
              this.metadata = true;
              result = true;
          }          
          // else it's an error
          else if(sb.toString().equals(this.TOKEN_ERR)) {
            this.metadata = false;
            error.append("\"");           
            error.append(this.TOKEN_ERR);
            error.append("\"");
            isError = true;
          }
          
          // The resultSet section hasn't reached yet, initialize the stringBuilder
          // Metadata section not reached yet
          else {
            // Reinitialize the stringbuilder
            sb.setLength(0);
          }
        }
       } // end while
      if(isError) {
        error.delete(error.length()-3, error.length());
        ObjectMapper mapper= new ObjectMapper();   
        JsonNode errorNode = mapper.readTree(error.toString());
        handleErrors(errorNode);
      }
      
      // If there is no error and there is no resultSet section
      if (result == false) {
        this.emptyResultSet=true;
        this.metadata = false;
      }
    } // end try      
    catch(IOException e) {
      LOGGER.log(Level.WARNING, "isResultSetSection(): "+e.getMessage());
    }
    return result;
  }
  
  /**
   * Helper method to start metadata section of the json stream
   * Instead of a metadata, there could be a "response" for DDLs
   */
  protected void startMetaData() throws SQLException {
    boolean metadata_started = false; 
    
    LOGGER.finer("Extracting the metadata for the ResultSet");
    // To determine the start and end of a string
    int quotes = 0;
    
    // String for indicating start string
    StringBuilder sb = new StringBuilder();
    
    // Run the loop as long as you're not at the start of the results section
    while(metadata_started==false && c>-1) {
      try{
        this.c=this.stream.read();
        
        // Construct the string within the quotes
        if(this.c==oracle.dbtools.jdbc.ResultSet.QUOTES) {
          quotes++;         
        }        
        else if(quotes==1) {
          sb.append((char)this.c);
        }        
        // End of String
        if(quotes==2) {
          quotes=0;
          // Either it is the start of the metadata section
          if(sb.toString().equals(oracle.dbtools.jdbc.ResultSet.TOKEN_START_METADATA)) {  
              metadata_started = true;
              this.metadata = true;
              break;
          }
                    
          // Or if there is a response --> may or may not be an error
          else if(sb.toString().equals(oracle.dbtools.jdbc.ResultSet.TOKEN_RESPONSE)) {
            // process response
            processResponse();
            return;
          }
          // Or there it's an error
          else if(sb.toString().contains(this.TOKEN_ERR)) {
            // process error
            processError();
            return;
          }         
          // Metadata section not reached yet
          else {
            // Reinitialize the stringbuilder
            sb.setLength(0);
          }
        } // end string comparison
      } // end try
      catch (SQLException se){
      	throw se;
      }
      catch (Exception e) {
        // Get the detailed message from the Exception
        if(e.getMessage()!=null) {
          LOGGER.log(Level.SEVERE, "startMetaData()"+e.getMessage()); 
        }
        else {
          LOGGER.log(Level.SEVERE, "startMetaData(): Error in reading ResultSetMetadata");
        }
      }       
    } // end while   
    sb.setLength(0);
  }
  
  /**
   * If there is an error returned instead of the result set 
   * @throws IOException, SQLException
   */
  private void processError() throws SQLException, IOException {
    StringBuilder sb = new StringBuilder();
    
    // read the next token which is a :
    this.stream.read();
    
    boolean endErr = false;
    int count = 0;
    
    // Get the error message
    while(endErr==false) {
      try {
        int c = this.stream.read();
        // The start of the error message
        if(c == ResultSet.QUOTES) {
          count ++;
        }
        // If end of message hasn't reached yet
        else if(count<2) {
          if(c==ResultSet.BACKSLASH && this.stream.read()==ResultSet.NEWLINE) {
            sb.append(System.lineSeparator());
          }
          else {
            sb.append((char)c);
          }
        }
        else {
           String exception = sb.toString();       
           throw new SQLException(exception);
        }
    } catch (IOException e) {
        LOGGER.log(Level.WARNING, e.getCause().toString());    
      } // end catch
    } // end while 
  } // end method
  
  
  /**
   * If there is a response section
   */
  private void processResponse() throws SQLException {
    
    boolean endErr = false;
    boolean firstwordRead = false; // to indicate if we have read the first word yet
    int array = 0; 
    boolean entered = false;
    String s = null;
    StringBuilder sb = new StringBuilder();
    boolean error = false; // is it an error message?
    
    // keep reading the stream until the beginning of the response
    try {     
      // until we have reached the end of the error / response
      while(endErr!= true) {
        int c = this.stream.read();        
        if(c == ResultSet.START_ARRAY) {
          array ++;
          c = this.stream.read(); // read the "
          c = this.stream.read(); // read the \
          entered = true;
        }
        else if(c==ResultSet.END_ARRAY) {
          array--;          
        }
        
        // if we are reading the response
        else if(array>=1) {         
          // replace the new lines with the line separator
          if(c==ResultSet.BACKSLASH && (c=this.stream.read())==ResultSet.NEWLINE) {            
            sb.append(System.lineSeparator());
          }         
          // Is the first word = "Error" and the first new line has been read
          else if(firstwordRead == false) {
            byte[]b = new byte[5]; // to read the word "Error"
            stream.read(b);
            s = new String(b);
            sb.append(s);
            firstwordRead = true;
          }  
          
          // Get the message anyway
          else {
            if(s.equalsIgnoreCase("Error")) {
              error = true;
            }            
            sb.append((char)c);                   
          }
        } // end if 
        if(array == 0 && entered == true) {
          endErr = true;
          // If there is no error just log the message
          if(!error) {
            LOGGER.info(sb.toString().substring(0,sb.toString().length()-1));
          }
          // If it is an error throw an exception
          // the substring is to remove trailing quotations
          else {
            throw new SQLException(sb.toString().substring(0,sb.toString().length()-1)); }
          }
        } // end while
      } catch (IOException e) { 
        LOGGER.log(Level.WARNING, "processResponse()" + e.getCause().toString());
      }
    }
  
  
  /**
   * Moves the cursor forward one row from its current position.
   * A <code>ResultSet</code> cursor is initially positioned
   * before the first row; the first call to the method
   * <code>next</code> makes the first row the current row; the
   * second call makes the second row the current row, and so on.
   * <p>
   * When a call to the <code>next</code> method returns <code>false</code>,
   * the cursor is positioned after the last row. Any
   * invocation of a <code>ResultSet</code> method which requires a
   * current row will result in a <code>SQLException</code> being thrown.
   *  If the result set type is <code>TYPE_FORWARD_ONLY</code>, it is vendor specified
   * whether their JDBC driver implementation will return <code>false</code> or
   *  throw an <code>SQLException</code> on a
   * subsequent call to <code>next</code>.
   *
   * <P>If an input stream is open for the current row, a call
   * to the method <code>next</code> will
   * implicitly close it. A <code>ResultSet</code> object's
   * warning chain is cleared when a new row is read.
   *
   * @return <code>true</code> if the new current row is valid;
   * <code>false</code> if there are no more rows
   * @exception SQLException if a database access error occurs or this method is
   *            called on a closed result set
   */
  private int rowCount = 0;
  private boolean metadataCreated = false;
  @Override
  public boolean next() throws SQLException {    
    if(this.closed) {
      int errorCode = 17010;
      throw new SQLException((RestjdbcResources.getString(RestjdbcResources.ORA_17010)+": next"), 
                SQLStateMapping.getSQLState(errorCode), errorCode);
    }    
    this.next = true;
    if(metadataCreated == false) {
      return false;
    }         
    // Start the Results section in the Json Stream if not started yet
    if(!this.isResultsStarted) {
      // check first to see if there is a resultSet section
      startResults();
      this.isResultsStarted = true;
    }
    
    // If the result set is empty -- return false
    // Can be caused next is called on an empty/non-existent ResultSet
    // example: ResultSet rs = stmt.executeQuery(sql_insert);
    if(this.emptyResultSet==true) {
      LOGGER.log(Level.INFO, RestjdbcResources.getString(RestjdbcResources.RESTJDBC_014));
      return false;
    }      
    JsonNode node = null;
   
    // After the Results section have been started and there is a next row
    if(this.isResultsStarted && this.isResultsEnded == false) {
      
      // there is another row, increment the row counter
      this.rowCount ++;
     
      // If max rows is zero get all rows
      if((maxRows == 0) || (maxRows > 0 && this.rowCount<= maxRows)) {          
        String row = walkRow();

        // Create the JsonTree here
        ObjectMapper mapper= new ObjectMapper();
        try {
          node = mapper.readTree(row);           
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
        // Free memory
        row = null;
        Iterator<Entry<String, JsonNode>> iterator = node.fields();
        int count = 0;
        while(iterator.hasNext()) {
          count ++;
          
          Entry<String, JsonNode> entry = iterator.next();         
          this.rowData.put(count, entry.getValue());
          this.colNames.put(entry.getKey(), count);

          // Remove the node from memory as soon as you put it into the HashMap
          iterator.remove();
         }          
        // Free memory
        node = null;
        return true;
      }
      // if the current row exceeds the maxRows limit then return false
      else{
        return false;
      }
    }    
      // Is there a next row ?
      else {
        return false;
      }
    }

  
  /** (non-Javadoc)
   * Helper method for pagination
   * Sends the POST request to REST Enabled SQL in ORDS, based on a fetchSize
   */
  private CloseableHttpResponse executeInternal() throws SQLException{
    CloseableHttpResponse response = null;
    Session session = this.conn.getSession();
    StringBuilder uri = new StringBuilder();
    uri.append(this.conn.getUri());
    uri.append(Defaults.ADHOC_ENDPOINT);
    URI root = URI.create(uri.toString());
    
    // increase offset
    this.offset += this.fetchSize;
    
    try {
      // create a POST with appropriate headers
      HttpPost post = session.createPost(root);
      
      post.addHeader("Content-Type", "application/json");
      
      String jsonPost = null;
      if(this.stmt!=null) {
       
        SimpleEntry<JsonGenerator, Writer> simpleEntry = this.stmt.generatePost(sql, this.offset);
        JsonGenerator g = simpleEntry.getKey();
        g.close();
        Writer writer    =  simpleEntry.getValue();
        jsonPost         =  writer.toString();
      }
      
      // If it's a PreparedStatement
      else if(this.pstmt!=null) {
        SimpleEntry<JsonGenerator, Writer> simpleEntry = this.pstmt.generatePost(sql, this.offset); 
        JsonGenerator g;
        
        if(this.pstmt.batchIndex==0) {
          g = this.pstmt.processSets(simpleEntry.getKey());  
        }
        // If there are batched binds
        else { g = this.pstmt.processBatchSets(simpleEntry.getKey()); }
       
        g.close();
        Writer writer   = simpleEntry.getValue();        
        jsonPost         =  writer.toString();
      }
      
      // If it's a CallableStatement
      else if(this.cstmt!=null) {        
      }
      StringEntity strEntity = new StringEntity(jsonPost);
      post.setEntity(strEntity);  
      
      // Execute post request
      response = session.executeRequest(post); 
    }
    catch ( IllegalStateException | IOException | SessionException e) {
      LOGGER.log(Level.SEVERE, e.getMessage());
    } 
    catch (SQLException e) { throw e; }  
    return response;
  }      
  
  /** (non-Javadoc)
   * Helper method to start a new Fetch based on the value from the method setFetchSize 
   * or the default FetchSize (25)
   */
  private void startNewFetch() {
    CloseableHttpResponse response = null;
    try {      
      this.stream.read(); // read one character
      byte[] buffer1=new byte[9];
      this.stream.read(buffer1);
      String hasMore = "";
      for(byte b:buffer1)
      {
        hasMore += (char)b;             
      }
      buffer1 = null;
      
      byte[] buffer2 = new byte[5];
      // read the colon and the space
      this.stream.read(); 
      
      // read the value of "hasMore"
      this.stream.read(buffer2);
      String value = "";
      for(byte b:buffer2)
      {
        value += (char)b;             
      }
      if(value.contains("false")) {
        this.isResultsEnded = true;
      }
      // If there are more results 
      // fetch them based on fetchsize
      else if(value.contains("true") && 
             (this.rowCount <= this.maxRows || this.maxRows ==0)) {
        
        // close the stream before opening a new one from the new response
        this.stream.close(); 
        
        // execute the request with the right offset
        response              = executeInternal();
        HttpEntity entity     = response.getEntity();
        this.stream           = entity.getContent();
        this.isResultsEnded   = false;
        this.isResultsStarted = false;
        this.metadataCreated  = false;
       
        // Has the metadata been created? 
        if(metadataCreated == false) {
          // Is there a ResultSet
          if(isResultSetSection()) {
            // Extract the ResultSet Metadata       
            startMetaData();
            createMetadataTree();        
            metadataCreated = true;
          }    
        }
      }
    }
    catch(Exception e) {
      LOGGER.severe(e.getMessage());
    }
   }
  
  /* (non-Javadoc)
   * Helper Method to walk a single row
   * walkRow() also does pagination based on maxRows and fetchSize
   */
  protected String walkRow() {
    boolean hasRowEnded = false;
    StringBuilder row = new StringBuilder();
    
    int innerJsonObj = 0;
    int innerJsonArray = 0;
    
    // Iterate until the end of the row 
    while(hasRowEnded == false && this.isResultsEnded !=true) {
      try {
        // Just for safety, check if you've reached the end of the stream
        // Unlikely to happen unless there is a bug
        if(this.c == -1) {
          return row.toString();
        }
                      
        // For nested Jsons
        if(this.c==oracle.dbtools.jdbc.ResultSet.START_OBJECT) {
          innerJsonObj ++;
        }
            
        else if(this.c==oracle.dbtools.jdbc.ResultSet.START_ARRAY) {
          innerJsonArray ++;
        }
            
        else if(this.c==oracle.dbtools.jdbc.ResultSet.END_OBJECT) {
          innerJsonObj --;
        }
            
        else if(this.c==oracle.dbtools.jdbc.ResultSet.END_ARRAY) {
          innerJsonArray --;
        }
                       
        // Condition to check if the row has ended 
        if(innerJsonArray==0 && innerJsonObj==0 && this.c==oracle.dbtools.jdbc.ResultSet.END_OBJECT) {
          
          hasRowEnded = true;
          int s = this.stream.read();
          row.append((char)c);

          // If we've reached the end of page
          if(s==oracle.dbtools.jdbc.ResultSet.END_ARRAY) {            
            startNewFetch();
          } 
        }
        // If the row hasn't ended yet
        else {
          row.append((char)this.c);          
        }
        this.c=this.stream.read();        
        } catch (IOException e) {          
            LOGGER.log(Level.INFO, e.getCause().toString());
        }
      }
    return row.toString();
  }
  
  /** (non-Javadoc)
   * Helper method to start of the results section in the Json stream
   * @throws SQLException 
   */
  public void startResults() throws SQLException {
    
    // To determine the start and end of a string
    int quotes = 0;
    
    // String for indicating start string
    StringBuilder sb = new StringBuilder();
    
    // Run the loop as long as you're not at the start of the results section
    while(this.isResultsStarted==false && c>-1) {
      try{        
       
        // Construct the string within the quotes
        if(this.c==oracle.dbtools.jdbc.ResultSet.QUOTES) {
          quotes++;         
        }
        
        else if(quotes==1) {
          sb.append((char)this.c);
        }       
        // End of String
        else if(quotes==2) {
          quotes=0;

          if( sb.toString().equals(oracle.dbtools.jdbc.ResultSet.TOKEN_START_STR)) {  
            // Read until '[' in the "items" section  
            while (this.c!=START_ARRAY && c>-1) {              
                this.c = this.stream.read();
             }
            // If the ResultSet is empty the next character will be ']'
            this.c = this.stream.read();
            if(this.c == END_ARRAY) {
              emptyResultSet = true;
            }
            else {
              // read until you reach the beginning of the result set, i.e. '{'
              while(this.c!=START_OBJECT) {
                this.c = this.stream.read();
              }
            }           
            this.isResultsStarted = true;
            return;
          }
          else {
            // Reinitialize the stringbuilder
            sb.setLength(0);
          }
        }        
        this.c=this.stream.read();
      }
      
      // Log all the other exceptions
      catch (Exception e) {
        // Get the detailed message from the Exception
        if(e.getMessage()!=null) {
          LOGGER.log(Level.SEVERE, e.getMessage()); 
        }
        else {
          LOGGER.log(Level.SEVERE, "Exception: Error reading ResultSet");
        }
      }
    }   
  }

  /** (non-Javadoc) 
   * Helper method to extract the correct columnIndex
   * (Needed for ambiguous column names)
   * @throws SQLException 
   */
  private int extractColIndex(LinkedHashMap<String, Integer> map, String columnLabel, String methodName) throws SQLException {
    ensureOpen(methodName);
    
    // populate colNames if not already done
    if(this.colNamesExtracted == false) {
      for(int i=1; i<=getMetaData().getColumnCount(); i++) {
        this.colNames.put(getMetaData().getColumnName(i), i);
      }
      this.colNamesExtracted = true;
    }
    
    for(Map.Entry<String, Integer> entry: map.entrySet()) {
      if(entry.getKey().equalsIgnoreCase(columnLabel)) {
        return entry.getValue();
      }
    }
    
    // Invalid column Index error
    int errorCode = 17006;
    throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17006), 
        SQLStateMapping.getSQLState(errorCode), errorCode);    
  }
  
  /** (non-Javadoc)
   * Helper method to see if next() has been called
   * Throw ORA_17014=ResultSet if next was not called
   */
  private void nextCalled() throws SQLException {
    if(this.next == false) { 
      int errorCode = 17014;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17014), 
          SQLStateMapping.getSQLState(errorCode), errorCode);    
    }       
  }
  /** (non-Javadoc)
   * Helper method to ensure that a statement is not closed
   */
  private void ensureOpen(String methodName) throws SQLException {
    // If the statement is closed
    if(closed) {
      closedrsException(methodName);
    }
    
    // If the connection is closed
    if(this.conn.isClosed()) {
      ExceptionUtil.throwSQLException(17008);      
    }
  }
    
  /** (non-Javadoc)
   * Helper method for getObject
   * Object returned in the correct datatype
   * @throws SQLException 
   */
  private Object getObjectHelper(int columnIndex) throws SQLException {          
    
    try {
      // get column-type info from the ResultSet metadata
      ResultSetMetaData rsmd  = getMetaData();
      int columnType          = rsmd.getColumnType(columnIndex);
      
      if(rowData.get(columnIndex).isNull()) {
        return null;
      }
      else if(columnType == OracleTypes.NUMBER) {
        return getBigDecimal(columnIndex);
       
      }
      else if(columnType == OracleTypes.BINARY_DOUBLE) {
        return getDouble(columnIndex);
      }
      else if(columnType == OracleTypes.BINARY_FLOAT) {
        return getFloat(columnIndex);
      }
      
      else if(columnType == OracleTypes.LONGVARCHAR || 
              columnType == OracleTypes.CHAR || 
              columnType == OracleTypes.VARCHAR) {
        return getString(columnIndex);
      }

      else if(columnType == OracleTypes.DATE) {
        java.sql.Timestamp date_value = getTimestamp(columnIndex);
        return date_value;
      }
      else if(columnType == OracleTypes.TIMESTAMP) {
        java.sql.Timestamp timestamp_value = getTimestamp(columnIndex);
        return timestamp_value;
      }
      else if(columnType == OracleTypes.TIME) {
        java.sql.Time time_value = getTime(columnIndex);
        return time_value;
      }
      else if(columnType == OracleTypes.TIMESTAMPTZ) {
        return getTIMESTAMPTZ(columnIndex);
      }
      else if(columnType == OracleTypes.CLOB) {
        return getClob(columnIndex);
      }
    }
    
    // If the column Index is invalid
    catch(NullPointerException ne) {
      // Get the detailed message from the Exception
      if(ne.getMessage()!=null) {
        LOGGER.log(Level.SEVERE, ne.getMessage()); 
      }
      // Invalid column index exception
      int error_code = 17003;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17003), 
                             SQLStateMapping.getSQLState(error_code), error_code);      
    }
    
    // SQLException
    catch (SQLException e) {
      if(e.getMessage()!=null){
        throw new SQLException(e.getMessage(), e.getSQLState(), e.getErrorCode());
      }
    } 
    // Unsupported column type -- should not reach here
    // TODO: Implement non-primitive data types
    throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17005), 
                  SQLStateMapping.getSQLState(17005), 17005);
  }
 
  /** (non-Javadoc)
   * Helper method for the get methods in ResultSet 
   * checks for common errors
   * @param Object columnInd is either the String columnLabel or int columnIndex 
   * @returns the String row in that column
   */
  private String getRowStr(int columnInd) throws SQLException {
    if(columnInd <= 0) {
      // ORA_17003=Invalid column index exception
      int errorCode = 17003;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17003), 
                           SQLStateMapping.getSQLState(errorCode), errorCode); 
    }
    // If the columnindex is valid but the ResultSet is empty
    if(this.emptyResultSet == true && columnInd > 0 && columnInd <= getMetaData().getColumnCount()) {
      int errorCode = 17289;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17289), 
                           SQLStateMapping.getSQLState(errorCode), errorCode); 
    }
    
    String row = null;
    try {
      // If the column is null
      if(rowData.get(columnInd).isNull()) {
        wasNull = true;
        return null;
      }
      else {       
        row = rowData.get(columnInd).asText();
      }          
                
    } // end try
    // If the column Index is invalid
    catch(NullPointerException ne) {
      // Get the detailed message from the Exception
      if(ne.getMessage()!=null) {
        LOGGER.log(Level.SEVERE, ne.getMessage()); 
      }
      // Invalid column index exception
      int error_code = 17003;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17003), 
                             SQLStateMapping.getSQLState(error_code), error_code);      
    } 
    return row;
  }
  
  /** (non-Javadoc)
   * getTimestamp with timezone - oracle Driver
   * @param columnIndex
   * @return
   * @throws SQLException
   */
  public oracle.sql.TIMESTAMPTZ getTIMESTAMPTZ(int columnIndex) throws SQLException {
   return null;
  }
  

  /** 
   * @see java.sql.ResultSet#getObject(int)
   */
  @Override
  public Object getObject(int columnIndex) throws SQLException {    
    ensureOpen("getObject(int columnIndex)");
    nextCalled();
    return getObjectHelper(columnIndex);
  }
  
  
  /** 
   * @see java.sql.ResultSet#getObject(java.lang.String)
   */
  @Override
  public Object getObject(String columnLabel) throws SQLException {
    ensureOpen("getObject(String columnLabel)");
    nextCalled();
    
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getObject(String columnLabel)");
    return getObject(columnIndex);
  }
  
  
  /** 
   * @see java.sql.ResultSet#getString(int)
   * 
   * When this method is called on a date/time/timestamp, 
   * the value is returned in iso8601 format. This is because the format for 
   * datetime structures are represented in iso8601 in JSON which is how the  
   * REST JDBC driver communicates with the database. 
   */
  @Override
  public String getString(int columnIndex) throws SQLException {   
    ensureOpen("getString(int columnIndex)");   
    nextCalled();
    ResultSetMetaData rsmd  = getMetaData();
    int columnType          = rsmd.getColumnType(columnIndex);
    
    if(columnType == java.sql.Types.DATE || columnType == java.sql.Types.TIMESTAMP) {
      return getTimestamp(columnIndex).toString();
    }
    
    return getRowStr(columnIndex);        
  }
   
  /** 
  * @see java.sql.ResultSet#getShort(java.lang.String)
  */
  @Override
  public short getShort(int columnIndex) throws SQLException {    
    ensureOpen("getShort(int columnIndex)");
    nextCalled();
    try { 
      if(getRowStr(columnIndex)==null) {
        return 0;
      }
      // Throw ORA_17026=Numeric Overflow if out of range
      double d = Double.parseDouble(getRowStr(columnIndex));
      if((d < Short.MIN_VALUE || d > Short.MAX_VALUE) && getMetaData().getColumnTypeName(columnIndex).equals("NUMBER")) {
        int errorCode = 17026;
        throw new SQLException(RestjdbcResources.get("ORA_17026"),
                              SQLStateMapping.getSQLState(errorCode), errorCode);
      }
      else {   
        // we just want to truncate / cast double value to integers
        return (new Double (d)).shortValue();
      } 
    }     
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,"getShort"), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }    
  }

  
  /**
   * @see java.sql.ResultSet#getInt(int)
   */
  @Override
  public int getInt(int columnIndex) throws SQLException {  
    ensureOpen("getInt(int columnIndex)");
    nextCalled();
    try {
      if(getRowStr(columnIndex)==null) {
        return 0;
      }
      else {   
        // we just want to truncate / cast double value to integers
        return (new Double (Double.parseDouble(getRowStr(columnIndex)))).intValue();
      }      
    }
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,"getInt"), 
                              SQLStateMapping.getSQLState(error_code), error_code); 
    }     
  }

  /** 
   * @see java.sql.ResultSet#getLong(int)
   */
  @Override
  public long getLong(int columnIndex) throws SQLException {
    ensureOpen("getLong(int columnIndex)");
    nextCalled();
    try {
      if(getRowStr(columnIndex)==null) {
        return 0;
      }
      else {  
        BigDecimal bigD = new BigDecimal(getRowStr(columnIndex));
        
        // If the bigdecimal is within range
        if(((bigD.compareTo(new BigDecimal(Long.MAX_VALUE)) <= 0 ) || // If bigDecimal is less than the max long
            (bigD.compareTo(new BigDecimal(Long.MIN_VALUE)) >= 0 )) &&  // If bigDecimal is greater than the min long  
            getMetaData().getColumnTypeName(columnIndex).equals("LONG"))
        {
          return bigD.longValue();
        }
        else {
          return new Double(Double.parseDouble(getRowStr(columnIndex))).longValue();
        }
      }              
    }    
    
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,"getLong"), 
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }         
  }

  /** 
   * @see java.sql.ResultSet#getFloat(int)
   */
  @Override
  public float getFloat(int columnIndex) throws SQLException {
    ensureOpen("getFloat(int columnIndex)");
    nextCalled();
    try {      
      if(getRowStr(columnIndex)==null) {
        return (float) 0;
      }
      else {   
        return Float.parseFloat(getRowStr(columnIndex));
      }      
    }
    
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,"getFloat"), 
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }   
  }

  /** 
   * @see java.sql.ResultSet#getDouble(int)
   */
  @Override
  public double getDouble(int columnIndex) throws SQLException{  
    ensureOpen("getDouble(int columnIndex)");
    nextCalled();
    try {   
      if(getRowStr(columnIndex)==null) {
        return (double) 0;
      }
      else {   
        return Double.parseDouble(getRowStr(columnIndex));
      } 
    }
    
    // Can't convert to the specified type?
    // invalid column type
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,"getDouble"), 
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }
  }
  
  
  /**
   * @see java.sql.ResultSet#getBytes(int)
   */
  @Override
  public byte[] getBytes(int columnIndex) throws SQLException {
    ensureOpen("getBytes(int columnIndex)");
    nextCalled();
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getBytes(int columnIndex)", 
                                                     this.getClass().getSimpleName()));    
  }
  
    /** 
    * @see java.sql.ResultSet#getBytes(java.lang.String)
    * 
    * This method is currently not supported in the REST JDBC driver. 
    */
   @Override
   public byte[] getBytes(String columnLabel) throws SQLException {
     ensureOpen("getBytes(String columnLabel)");
     nextCalled();
     throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                     "getBytes(String columnLabel)", 
                                                      this.getClass().getSimpleName()));    
   }
   
  /** (non-Javadoc)
   * Helper method to throw a SQLException if iso8601 date is not retrieved for getDate methods
   */
   private int checkFormat(String tsStr, String methodName, String[] patterns) throws SQLException {
     // Check to see if it's actually a date/time object 
     // (REGEX can be modified)
     int index = -1;
     for(int i =0; i < patterns.length; i++) {
       // Is there any match?
       if(Pattern.matches(patterns[i], tsStr)) {
         index = i;
       }
     }
     if(index < 0) {
       int error_code = 17004;
       throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,methodName), 
                              SQLStateMapping.getSQLState(error_code), error_code); 
     }
     return index;
   }

   /**
    * @see java.sql.ResultSet#getDate(int)
    */
  @Override
  public Date getDate(int columnIndex) throws SQLException {
    ensureOpen("getDate(int columnIndex)");
    nextCalled();
    try {        
      if(getRowStr(columnIndex) == null) {
        return null;
      }
      // Check date-time format
      String[] patterns = {"^\\d{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-6][0-9]:[0-6][0-9](.\\d{3})?Z",
                           "^\\d{4}-[0-1][0-9]-[0-3][0-9]"};

      int index = checkFormat(getRowStr(columnIndex), "getDate", patterns);  
      DateTimestampsUtil dtsUtil = new DateTimestampsUtil();
      if(index == 1) {
        return dtsUtil.getDate(getRowStr(columnIndex));
      }
      return dtsUtil.getDate(getRowStr(columnIndex), this.timeZoneID);
    }
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,"getDate"), 
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }    
  }


  /**
   * @see java.sql.ResultSet#getTime(int)
   * Notes: The timezone is translated to the local time zone of the client.
   * The REST JDBC driver supports a millisecond precision
   */
  @Override
  public Time getTime(int columnIndex) throws SQLException {
    ensureOpen("getTime(int columnIndex)");
    nextCalled();    
    try {              
      if(getRowStr(columnIndex) == null) {
        return null;
      }
      // For Time: allow: yyyy-mm-ddThh:mi:ss(.sss)Z and 
      String[] patterns = {"^\\d{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-6][0-9]:[0-6][0-9](.\\d{3})?Z",
                           "[0-2][0-9]:[0-6][0-9]:[0-6][0-9](.\\d{3})?"};
            
      int index = checkFormat(getRowStr(columnIndex), "getTime", patterns);  
      Calendar cal = Calendar.getInstance(TimeZone.getDefault());
      DateTimestampsUtil dtsUtil = new DateTimestampsUtil();      
      if(index == 1) {
        return dtsUtil.getTime(getRowStr(columnIndex));
      }
           return dtsUtil.getTime(getRowStr(columnIndex), this.timeZoneID,cal);   
    }
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,"getTime"), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }
    // SQLException
    catch (SQLException e) {
      throw e;
    }
  }

  /**
   * @see java.sql.ResultSet#getTimestamp(int)
   * 
   * Note: The REST JDBC driver supports a millisecond precision
   */
  @Override
  public Timestamp getTimestamp(int columnIndex) throws SQLException {
    ensureOpen("getTimestamp(int columnIndex)");
    nextCalled();
    try {     
      if(getRowStr(columnIndex) == null) {
        return null;
      }
      
      // For timestamps: allow only: yyyy-mm-ddThh:mi:ss(.sss)Z formats
      String[] patterns = {"^\\d{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-6][0-9]:[0-6][0-9](.\\d{3})?Z"};
      checkFormat(getRowStr(columnIndex), "getTimestamp", patterns);
      
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getTimestamp(getRowStr(columnIndex), this.timeZoneID);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,"getTimestamp"), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }
  }
  
   /** 
   * @see java.sql.ResultSet#getAsciiStream(int)
   * 
   * This method is not currently supported in the REST JDBC driver
   */
  @Override
  public InputStream getAsciiStream(int columnIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
                                              "getAsciiStream(int columnIndex)", 
                                               this.getClass().getSimpleName())); 
    }

  /** 
   * @see java.sql.ResultSet#getUnicodeStream(int)
   * 
   * This method is not currently supported in the REST JDBC driver
   */
  @Override
  public InputStream getUnicodeStream(int columnIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
                                              "getUnicodeStream(int columnIndex)", 
                                               this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.ResultSet#getBinaryStream(int)
   * 
   * This method is not currently supported in the REST JDBC driver
   */
  @Override
  public InputStream getBinaryStream(int columnIndex) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
                                                    "getBinaryStream(int columnIndex)", 
                                                     this.getClass().getSimpleName()));  
    }
  
  
  /**
   * @see java.sql.ResultSet#getBlob(int)
   * 
   * This method is not currently supported in the REST JDBC driver
   */
  @Override
  public Blob getBlob(int columnIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
                                              "getBlob(int columnIndex)", 
                                               this.getClass().getSimpleName()));    
  }

  /** 
   * @see java.sql.ResultSet#getClob(int)
   */
  @Override
  public Clob getClob(int columnIndex) throws SQLException {
    ensureOpen("getClob(int columnIndex)");
    nextCalled();
    // Is it a null clob?
    if(getRowStr(columnIndex)==null) {
      return null;
    }
    return new oracle.dbtools.jdbc.CLOB(getRowStr(columnIndex));
  }
  
  /**
   * @see java.sql.ResultSet#getClob(java.lang.String)
   */
  @Override
  public Clob getClob(String columnLabel) throws SQLException {
    ensureOpen("getClob(String columnLabel)");
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getClob(String columnLabel)");
    if(getRowStr(columnIndex)==null) {
      return null;
    }    
    return new oracle.dbtools.jdbc.CLOB(getRowStr(columnIndex));
  }

  /**
   * @see java.sql.ResultSet#getArray(int)
   * 
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public Array getArray(int columnIndex) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getArray(int columnIndex)", this.getClass().getSimpleName()));   
  }
  
  /** 
   * @see java.sql.ResultSet#getCharacterStream(int)
   */
  @Override
  public Reader getCharacterStream(int columnIndex) throws SQLException {
    
    return new StringReader(getRowStr(columnIndex));
  }

  /** 
   * @see java.sql.ResultSet#getCharacterStream(java.lang.String)
   */
  @Override
  public Reader getCharacterStream(String columnLabel) throws SQLException {
    // Extract the proper columnIndex for the data to be retrieved
    int colIndex = extractColIndex(this.colNames, columnLabel,"getCharacterStream(String columnLabel)");
    return new StringReader(getRowStr(colIndex));
  }
  
  /**
   * @see java.sql.ResultSet#getBlob(java.lang.String)
   * 
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public Blob getBlob(String columnLabel) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getBlob(String columnLabel)", this.getClass().getSimpleName()));      
  }


  /** 
   * @see java.sql.ResultSet#getArray(java.lang.String)
   * 
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public Array getArray(String columnLabel) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getArray(String columnLabel)", this.getClass().getSimpleName()));  
  }
  

  /**
   * @see java.sql.ResultSet#getAsciiStream(java.lang.String)
   */
  @Override
  public InputStream getAsciiStream(String columnLabel) throws SQLException {
    // Extract the proper columnIndex for the data to be retrieved
    int colIndex = extractColIndex(this.colNames, columnLabel, "getAsciiStream(String columnLabel)");
    return new ByteArrayInputStream(getRowStr(colIndex).getBytes(StandardCharsets.UTF_8));
 }

  /**
   * @see java.sql.ResultSet#getUnicodeStream(java.lang.String)
   * 
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public InputStream getUnicodeStream(String columnLabel) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getUnicodeStream(String columnLabel)", this.getClass().getSimpleName())); 
  }

  /**
   * @see java.sql.ResultSet#getBinaryStream(java.lang.String)
   * 
   *  This method is not supported in the REST JDBC driver
   */
  @Override
  public InputStream getBinaryStream(String columnLabel) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getBinaryStream(String columnLabel)", this.getClass().getSimpleName()));  
    }
  
  
  /**
   * @see java.sql.ResultSet#getString(java.lang.String)
   * 
   * When this method is called on a date/time/timestamp, 
   * the value is returned in iso8601 format. This is because the format for 
   * datetime structures are represented in iso8601 in JSON which is how the  
   * REST JDBC driver communicates with the database. 
   */
  @Override
  public String getString(String columnLabel) throws SQLException {  
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getString(String columnLabel)");
    return getRowStr(columnIndex); 
  }

  /** 
   * @see java.sql.ResultSet#getShort(java.lang.String)
   */
  @Override
  public short getShort(String columnLabel) throws SQLException {
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel,"getShort(String columnLabel)");
    return getShort(columnIndex);
  }

  /** 
   * @see java.sql.ResultSet#getInt(java.lang.String)
   */
  @Override
  public int getInt(String columnLabel) throws SQLException {
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getInt(String columnLabel)");
    return getInt(columnIndex);
  }

  /** 
   * @see java.sql.ResultSet#getLong(java.lang.String)
   */
  @Override
  public long getLong(String columnLabel) throws SQLException {
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getLong(String columnLabel)");
    return getLong(columnIndex);
  }

  /**
   * @see java.sql.ResultSet#getFloat(java.lang.String)
   */
  @Override
  public float getFloat(String columnLabel) throws SQLException {
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getFloat(String columnLabel)");
    return getFloat(columnIndex);
  }

  /**
   * @see java.sql.ResultSet#getDouble(java.lang.String)
   */
  @Override
  public double getDouble(String columnLabel) throws SQLException {
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getDouble(String columnLabel)");
    return getDouble(columnIndex);
  }
  
  /** 
   * @see java.sql.ResultSet#getBigDecimal(int)
   */
  @Override
  public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
    ensureOpen("getBigDecimal(int columnIndex)");
    nextCalled();
    try {
      return new BigDecimal(rowData.get(columnIndex).toString());
    }
    // Can't be converted to a BigDecimal 
    // Invalid Column Type exception
    catch (NumberFormatException e) {      
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,"getBigDecimal"), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }
    catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getCause().toString());
      throw new SQLException(RestjdbcResources.get(RestjdbcResources.RESTJDBC_010));
    }    
  }

  
  /** 
   * @see java.sql.ResultSet#getBigDecimal(java.lang.String)
   */
  @Override
  public BigDecimal getBigDecimal(String columnLabel) throws SQLException {
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getBigDecimal(String columnLabel)");
    return getBigDecimal(columnIndex);
  }

  /** 
   * @see java.sql.ResultSet#getBoolean(int)
   */
  @Override
  public boolean getBoolean(int columnIndex) throws SQLException {
    ensureOpen("getBoolean(int columnIndex)");
    nextCalled();
    try {     
      boolean value = rowData.get(columnIndex).asBoolean();
      if(value) {
        return value;
      }
      else {
        String type = rsMetadataTree.get("metadata").get(columnIndex-1).get("columnTypeName").toString();
        if(type.contains("NUMBER")) {
          // return true if the number > 0
          if(rowData.get(columnIndex).asLong() >0) {
            return true;
          }
          // return false if the number = 0
          else if(rowData.get(columnIndex).asLong() == 0) {
            return false;
          }
        }
        else if(type.contains("VARCHAR")) {
          // return true if the text is "true"
          if(rowData.get(columnIndex).asText().equalsIgnoreCase("true")) {
            return true;
          }
          else {
            return false;
          }
        } // end type VARCHAR
      }  
      // otherwise throw invalid column type exception
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004,"getBoolean"), 
                             SQLStateMapping.getSQLState(error_code), error_code);
    }
    catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getCause().toString());
      throw new SQLException(RestjdbcResources.get(RestjdbcResources.RESTJDBC_010));
    }
  }
  
  
  /** 
   * @see java.sql.ResultSet#getBoolean(java.lang.String)
   */
  
  @Override
  public boolean getBoolean(String columnLabel) throws SQLException {
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel,"getBoolean(String columnLabel)");
    return getBoolean(columnIndex);
    
  }
  
  
  /** 
   * @see java.sql.ResultSet#getDate(java.lang.String)
   */
  @Override
  public Date getDate(String columnLabel) throws SQLException {
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getDate(String columnLabel)");
    return getDate(columnIndex);
  }

  /** 
   * @see java.sql.ResultSet#getTime(java.lang.String)
   * 
   * Note: The timezone is translated to the local time zone of the client.
   * The REST JDBC driver supports a millisecond precision
   */ 
  @Override
  public Time getTime(String columnLabel) throws SQLException {   
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getTime(String columnLabel)");
    return getTime(columnIndex);
  }

  /** 
   * @see java.sql.ResultSet#getTimestamp(java.lang.String)
   * 
   * Note: The REST JDBC driver supports a millisecond precision
   */
  @Override
  public Timestamp getTimestamp(String columnLabel) throws SQLException {
    nextCalled();
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getTimestamp(String columnLabel)");
    return getTimestamp(columnIndex);
  }
  
  /**
   * @see java.sql.ResultSet#close()
   */
  @Override
  public void close() throws SQLException {
    try {
      if (this.closed == false) { 
        this.closed = true;
        // Free up all the resources
        this.isResultSetClosed = true;
        
        this.stream.close();
        this.rsMetadataTree = null;
        this.colNames = null;
        this.rowData = null;
           
        // Decrease the number of ResultSets open for this Statement object
        if(this.stmt!=null) {
          this.stmt.rsCount--;
        }
        // Decrease number of ResultSets open for the PreparedStatement object
        else if(this.pstmt!=null) {
          this.pstmt.rsCount--;
        }
       }
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getCause().toString());
    }
  }
  
  
  /** (non-Javadoc)
   * Helper method for a DMLs
   * This is not actually executing the POST request. 
   * Gets back the response from adhocsql after a DDL or a DML
   * @throws SQLException 
   */
  protected void handleErrors(JsonNode node) throws SQLException {
    if (node.findValue("errorCode")!=null) { //throw SQLException
      int error_code = node.findValue("errorCode").asInt();
      String error_details = node.findValue("errorDetails").asText();
      //System.out.println(error_details);
      //throw new SQLException(error_code,error_line,error_column,error_details);
      throw new SQLException(error_details, SQLStateMapping.getSQLState(error_code), error_code);
    }
    else if(node.findValue("error")!=null) {
      throw new SQLException(node.findValue("error").asText());
    }
  }
  
  
   /**
   * @see java.sql.ResultSet#getMetaData()
   */
  @Override
  public ResultSetMetaData getMetaData() throws SQLException {
    // If the ResultSet is closed, throw SQLException:
    // Closed Resultset: getMetaData
    if(closed) {
      closedrsException("getMetaData");
    }
    if(metadata == false) {
      int errorCode = 17283;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17283), 
                SQLStateMapping.getSQLState(errorCode), errorCode);
    }
    return new oracle.dbtools.jdbc.ResultSetMetadata(this.rsMetadataTree);
  }
  
  /**
   * @see java.sql.ResultSet#isBeforeFirst()
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public boolean isBeforeFirst() throws SQLException {
    throwUnsupportedException("isBeforeFirst()");
    return false;
  }

  /**
   * @see java.sql.ResultSet#isAfterLast()
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public boolean isAfterLast() throws SQLException {
    throwUnsupportedException("isAfterLast()");
    return false;
  }

  /**
   * @see java.sql.ResultSet#isFirst()
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public boolean isFirst() throws SQLException {
    throwUnsupportedException("isFirst()");
    return false;
  }

  /**
   * @see java.sql.ResultSet#isLast()
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public boolean isLast() throws SQLException {
    throwUnsupportedException("isLast");
    return false;
  }
  
  
  /**
   * @see java.sql.ResultSet#getStatement()
   */
  @Override
  public Statement getStatement() throws SQLException {
    ensureOpen("getStatement");
    try {
      if(this.isResultSetClosed == true) {
        throw new SQLException(RestjdbcResources.getString("RESTJDBC_006"));
      } 
      // Is it a Statement?
      else if (stmt!=null && pstmt==null && cstmt==null) {
        return this.stmt;
      } 
      // Is it a PreparedStatement?
      else if (stmt==null && pstmt!=null && cstmt==null) {
        return this.pstmt;
      }
      // Is it a CallableStatement?
      else if (stmt==null && pstmt==null && cstmt!=null) {
        return this.cstmt;
      }      
    }
    catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getCause().toString());
    }
    // shouldn't reach here ... Internal Error exception
    throw new SQLException(RestjdbcResources.get("ORA_17001"));
  }

  @Override
  public <T> T unwrap(Class<T> iface) throws SQLException {
    throwUnsupportedException("unwrap(Class<T> iface)");
    return null;
  }

  @Override
  public boolean isWrapperFor(Class<?> iface) throws SQLException {
    throwUnsupportedException("isWrapperFor(Class<?> iface)");
    return false;
  }

  /** 
   * @see java.sql.ResultSet#wasNull()
   */
  @Override
  public boolean wasNull() throws SQLException {
    return this.wasNull;
  }

  
  /**
   * @see java.sql.ResultSet#getByte(int)
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public byte getByte(int columnIndex) throws SQLException {
    throw new RestJdbcUnsupportedException(
        RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"getByte(int columnIndex)", 
                                 this.getClass().getSimpleName()));       
  }

  /** 
   * see java.sql.ResultSet#getBigDecimal(int, int)
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public BigDecimal getBigDecimal(int columnIndex, int scale)
      throws SQLException {
    throw new RestJdbcUnsupportedException(
        RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"getBigDecimal(int columnIndex, int scale)", 
                                 this.getClass().getSimpleName()));    
  }

  /**
   * @see java.sql.ResultSet#getBytes(int)
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public byte getByte(String columnLabel) throws SQLException {
    throw new RestJdbcUnsupportedException(
        RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"getByte(String columnLabel)", 
                                 this.getClass().getSimpleName()));    
  }

  /** 
   * @see java.sql.ResultSet#getBigDecimal(java.lang.String, int)
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public BigDecimal getBigDecimal(String columnLabel, int scale)
      throws SQLException {
    throw new RestJdbcUnsupportedException(
        RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"getBigDecimal(String columnLabel, int scale)", 
                                 this.getClass().getSimpleName()));    
  }

  /** 
   * @see java.sql.ResultSet#getWarnings()
   */
  @Override
  public SQLWarning getWarnings() throws SQLException {
    LogUtil.logOrException(this.getStatement().getConnection(), null, "ResultSet.getWarnings() not supported", null);
    return null;
  }

  /** 
   * @see java.sql.ResultSet#clearWarnings()
   */
  @Override
  public void clearWarnings() throws SQLException {
    throw new RestJdbcUnsupportedException(
        RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"clearWarnings()", 
                                 this.getClass().getSimpleName()));    
  }

  /** 
   * @see java.sql.ResultSet#getCursorName()
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public String getCursorName() throws SQLException {
    throwUnsupportedException("getCursorName()");
    return null;
  }

  
  /** 
   * @see java.sql.ResultSet#findColumn(java.lang.String)
   */
  @Override
  public int findColumn(String columnLabel) throws SQLException {
    return extractColIndex(this.colNames, columnLabel, "findColumn(String columnLabel)");
  }

  /** 
   * @see java.sql.ResultSet#beforeFirst()
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public void beforeFirst() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"beforeFirst()", 
                                                    this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.ResultSet#afterLast()
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public void afterLast() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"afterLast()", 
                                                    this.getClass().getSimpleName()));   
  }

  /** 
   * @see java.sql.ResultSet#first()
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public boolean first() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"first()", 
                                                    this.getClass().getSimpleName()));   
  }

  /**
   * @see java.sql.ResultSet#last()
   * 
   * This method is currently not supported in the REST JDBC driver
   */
  @Override
  public boolean last() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"last()", 
                                                    this.getClass().getSimpleName()));   
  }

  /** 
   * @see java.sql.ResultSet#getRow()
   */
  @Override
  public int getRow() throws SQLException {
   return rowCount;
  }

  /**
   * @see java.sql.ResultSet#absolute(int)
   * 
   * The REST JDBC driver supports a FORWARD_ONLY ResultSet
   * This method throws an exception
   */
  @Override
  public boolean absolute(int row) throws SQLException {
    ExceptionUtil.throwSQLException(17075, "absolute");
    return false;
  }

  /** 
   * @see java.sql.ResultSet#relative(int)
   * 
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public boolean relative(int rows) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"last()", 
                                                    this.getClass().getSimpleName()));   
  }

  /** 
   * @see java.sql.ResultSet#previous()
   * 
   * This method is not supported in the REST JDBC driver
   */
  @Override
  public boolean previous() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"previous()", 
                                                    this.getClass().getSimpleName()));   
  }

  private int fetchDirection = ResultSet.FETCH_FORWARD;
  
  /**
   * 
   * @see java.sql.ResultSet#setFetchDirection(int)
   * 
   * The REST JDBC driver supports a forward only ResultSet
   */
  @Override
  public void setFetchDirection(int direction) throws SQLException {
    if(closed) {
      closedrsException("setFetchDirection(int direction)");
    }
    // The ResultSet is  TYPE_FORWARD_ONLY
    if(direction!=ResultSet.FETCH_FORWARD) {
      ExceptionUtil.throwSQLException(17308);
    }
    else {
      fetchDirection = direction;
    }
  }

  /**
   * @see java.sql.ResultSet#getFetchDirection()
   * 
   * The REST JDBC driver supports a forward only ResultSet
   */
  @Override
  public int getFetchDirection() throws SQLException {
    if(closed) {
      closedrsException("getFetchDirection()");
    }    
    // The fetch direction should always be ResultSet.FETCH_FOWARD
    return fetchDirection;
  }


  public int fetchSize = 0;
  
  /**
   * @see java.sql.ResultSet#setFetchSize(int)
   */
  @Override
  public void setFetchSize(int rows) throws SQLException {    
    // If the ResultSet is closed
    if(closed) {
      closedrsException("setFetchSize(int rows)");
    }
    if(rows < 0) {
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17433));
    }    
    fetchSize = rows;    
  }

  /**
   * @see java.sql.ResultSet#getFetchSize()
   */
  @Override
  public int getFetchSize() throws SQLException {
    // If the ResultSet is closed
    if(closed) {
      closedrsException("getFetchSize()");
      return -1; // should not go here
    }
    else {
      return (int) fetchSize;
    }
  }

  /** 
   * @see java.sql.ResultSet#getType()
   * 
   * The REST JDBC driver only supports FORWARD ONLY ResultSets
   */
  @Override
  public int getType() throws SQLException {
    ensureOpen("getType()");
    return ResultSet.TYPE_FORWARD_ONLY;
  }

  
  /**
   * @see java.sql.ResultSet#getConcurrency()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public int getConcurrency() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"getConcurrency()", 
                                                    this.getClass().getSimpleName()));   
  }

  /**
   * @see java.sql.ResultSet#rowUpdated()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public boolean rowUpdated() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"rowUpdated()", 
                                                    this.getClass().getSimpleName())); 
  }

  /** 
   * @see java.sql.ResultSet#rowInserted()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public boolean rowInserted() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"rowInserted()", 
                                                    this.getClass().getSimpleName())); 
  }

  /**
   * @see java.sql.ResultSet#rowDeleted()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public boolean rowDeleted() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"rowDeleted()", 
                                                    this.getClass().getSimpleName())); 
  }


  /** 
   * @see java.sql.ResultSet#insertRow()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public void insertRow() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"insertRow()", 
                                                    this.getClass().getSimpleName()));   
  }


  /** 
   * @see java.sql.ResultSet#deleteRow()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public void deleteRow() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"deleteRow()", 
                                                    this.getClass().getSimpleName())); 
  }

  /** 
   * @see java.sql.ResultSet#refreshRow()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public void refreshRow() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"refreshRow()", 
                                                    this.getClass().getSimpleName()));  
  }

  /** 
   * @see java.sql.ResultSet#cancelRowUpdates()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public void cancelRowUpdates() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"cancelRowUpdates()", 
                                                    this.getClass().getSimpleName()));   
  }

  /** 
   * @see java.sql.ResultSet#moveToInsertRow()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public void moveToInsertRow() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"moveToInsertRow()", 
                                                    this.getClass().getSimpleName())); 
  }

  /** 
   * @see java.sql.ResultSet#moveToCurrentRow()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public void moveToCurrentRow() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"moveToCurrentRow()", 
                                                    this.getClass().getSimpleName()));    
  }

  /**
   * @see java.sql.ResultSet#getObject(int, java.util.Map)
   */
  @Override
  public Object getObject(int columnIndex, Map<String, Class<?>> map)
      throws SQLException {
    ensureOpen("getObject(int columnIndex, Map<String, Class<?>> map)");
    Object obj = getObjectHelper(columnIndex);
    return obj;
  }

  /**
   * @see java.sql.ResultSet#getRef(int)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public Ref getRef(int columnIndex) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"getRef(int columnIndex)", 
                                                    this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.ResultSet#getRef(java.lang.String)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public Ref getRef(String columnLabel) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,"getRef(String columnLabel)", 
                                                    this.getClass().getSimpleName()));
  }
  
  /** 
   * @see java.sql.ResultSet#getObject(java.lang.String, java.util.Map)
   */
  @Override
  public Object getObject(String columnLabel, Map<String, Class<?>> map)
      throws SQLException {
    ensureOpen("getObject(String columnLabel, Map<String, Class<?>> map)");
    
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getObject(String columnLabel)");
    Object obj = getObjectHelper(columnIndex);

    return obj;
  }



  /** 
   * @see java.sql.ResultSet#getDate(int, java.util.Calendar)
   */
  @Override
  public Date getDate(int columnIndex, Calendar cal) throws SQLException {
    try {
      if(getRowStr(columnIndex) == null) {
        return null;
      }
      DateTimestampsUtil ts = new DateTimestampsUtil();  
      return ts.getDate(getRowStr(columnIndex), this.timeZoneID, cal);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException( RestjdbcResources.format(RestjdbcResources.ORA_17004, "getDate"),
                              SQLStateMapping.getSQLState(error_code), error_code);
    } 
  }

  /** 
   * @see java.sql.ResultSet#getDate(java.lang.String, java.util.Calendar)
   */
  @Override
  public Date getDate(String columnLabel, Calendar cal) throws SQLException {
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel,"getDate(String columnLabel, Calendar cal)");
    return getDate(columnIndex,cal); 
  }

  /**
   * @see java.sql.ResultSet#getTime(int, java.util.Calendar)
   * 
   * Note: The REST JDBC driver supports a millisecond precision.
   */
  @Override
  public Time getTime(int columnIndex, Calendar cal) throws SQLException {
    try {
      if(getRowStr(columnIndex) == null) {
        return null;
      }
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getTime(getRowStr(columnIndex), this.timeZoneID, cal);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004, "getTime"),
                             SQLStateMapping.getSQLState(error_code), error_code);
    } 
  }

  /** 
   * @see java.sql.ResultSet#getTime(java.lang.String, java.util.Calendar)
   * 
   * Note: The REST JDBC driver supports a millisecond precision.
   */
  @Override
  public Time getTime(String columnLabel, Calendar cal) throws SQLException {
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel,"getTime(String columnLabel, Calendar cal)");
    return getTime(columnIndex,cal);
  }

  /**
   * @see java.sql.ResultSet#getTimestamp(int, java.util.Calendar)
   * 
   * Note: The REST JDBC driver supports a millisecond precison.
   */
  @Override
  public Timestamp getTimestamp(int columnIndex, Calendar cal)
      throws SQLException {
    try {
      if(getRowStr(columnIndex) == null) {
        return null;
      }
      DateTimestampsUtil ts = new DateTimestampsUtil();
      return ts.getTimestamp(getRowStr(columnIndex), this.timeZoneID, cal);    
    }   
    
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004, "getTimestamp"),
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }
  }

  /** 
   * @see java.sql.ResultSet#getTimestamp(java.lang.String, java.util.Calendar)
   * 
   * Note: The REST JDBC driver supports a millisecond precision.
   */
  @Override
  public Timestamp getTimestamp(String columnLabel, Calendar cal)
      throws SQLException {
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getTimestamp(String columnLabel, Calendar cal)");
    return getTimestamp(columnIndex,cal);
  }

  /** 
   * @see java.sql.ResultSet#getURL(int)
   */
  @Override
  public URL getURL(int columnIndex) throws SQLException {   
    try {
      if(closed) {
        closedrsException("getURL(int columnIndex)");
      }
      return new URL(getRowStr(columnIndex));
    } catch (MalformedURLException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004, "getURL"),
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }
  }

  /** 
   * @see java.sql.ResultSet#getURL(java.lang.String)
   */
  @Override
  public URL getURL(String columnLabel) throws SQLException {
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getURL(String columnLabel)");
    return getURL(columnIndex);
  }


  /** 
   * @see java.sql.ResultSet#getRowId(int)
   */
  @Override
  public RowId getRowId(int columnIndex) throws SQLException {
    ensureOpen("getRowId(int columnIndex)");
    try {
      String columnTypeName = this.rsMetadataTree.get("metadata").get(0).get("columnTypeName").asText();
      String rowID = getRowStr(columnIndex);
      // Throw invalid column type exception 
      if(!columnTypeName.equals("CHAR") || !columnTypeName.equals("VARCHAR2")) {
        int error_code = 17004;
        throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004, "getRowId"),
                               SQLStateMapping.getSQLState(error_code), error_code); 
      }      
      return new oracle.dbtools.jdbc.RowId(rowID);
    }
    // Invalid column type
    catch (IllegalArgumentException e) {
      int error_code = 17004;
      throw new SQLException(RestjdbcResources.format(RestjdbcResources.ORA_17004, "getRowId"),
                             SQLStateMapping.getSQLState(error_code), error_code); 
    }
   
  }

  /** 
   * @see java.sql.ResultSet#getRowId(java.lang.String)
   */
  @Override
  public RowId getRowId(String columnLabel) throws SQLException {
    // Extract the proper columnIndex for the data to be retrieved
    int columnIndex = extractColIndex(this.colNames, columnLabel, "getRowId(String columnLabel)");
    return getRowId(columnIndex); 
  }


  /** 
   * @see java.sql.ResultSet#getHoldability()
   * 
   * This method is not currently supported in the REST JDBC driver
   */
  @Override
  public int getHoldability() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getHoldability()", 
                                                    this.getClass().getSimpleName()));
  }

  /**
   * @see java.sql.ResultSet#isClosed()
   */
  @Override
  public boolean isClosed() throws SQLException {
    return this.closed;
  }

  /**
   * @see java.sql.ResultSet#getNClob(int)
   * 
   * This method is not currently supported in the REST JDBC driver
   */
  @Override
  public NClob getNClob(int columnIndex) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getNClob(int columnIndex)", 
                                                     this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.ResultSet#getNClob(java.lang.String)
   * 
   * This method is not currently supported in the REST JDBC driver
   */
  @Override
  public NClob getNClob(String columnLabel) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getNClob(String columnLabel)", 
                                                    this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.ResultSet#getSQLXML(int)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public SQLXML getSQLXML(int columnIndex) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getSQLXML(int columnIndex)", 
                                                    this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.ResultSet#getSQLXML(java.lang.String)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public SQLXML getSQLXML(String columnLabel) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getSQLXML(String columnLabel)", 
                                                    this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.ResultSet#getNString(int)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public String getNString(int columnIndex) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getNString(int columnIndex)", 
                                                    this.getClass().getSimpleName()));
  }

  /**
   * @see java.sql.ResultSet#getNString(java.lang.String)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public String getNString(String columnLabel) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getNString(String columnLabel)", 
                                                    this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.ResultSet#getNCharacterStream(int)
   * 
   * This feature is not supported in the REST JDBC driver.r
   */
  @Override
  public Reader getNCharacterStream(int columnIndex) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getNCharacterStream(int columnIndex)", 
                                                    this.getClass().getSimpleName()));
  }

  /**
   * @see java.sql.ResultSet#getNCharacterStream(java.lang.String)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public Reader getNCharacterStream(String columnLabel) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getNCharacterStream(String columnLabel)", 
                                                    this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.ResultSet#getObject(int, java.lang.Class)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getObject(int columnIndex, Class<T> type)", 
                                                    this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.ResultSet#getObject(java.lang.String, java.lang.Class)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public <T> T getObject(String columnLabel, Class<T> type)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getObject(String columnLabel, Class<T> type)", 
                                                     this.getClass().getSimpleName()));
  }
  
  /** (non-Javadoc)  
   * Helper method to construct exception for closed ResultSets
   */
  protected void closedrsException(String methodName) throws SQLException {
    int error_code = 17010;
    throw new SQLException(
           RestjdbcResources.format(RestjdbcResources.RESTJDBC_020, methodName),
                SQLStateMapping.getSQLState(error_code), error_code); 
  }
  
  
  /**
   * The following methods are not supported by the REST JDBC driver 
   */
  
  /**********************************************************************************/
  

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateRow() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateRow()", 
                                                     this.getClass().getSimpleName()));
  }
  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNull(int columnIndex) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNull(int columnIndex)", 
                                                     this.getClass().getSimpleName()));
  }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBoolean(int columnIndex, boolean x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBoolean(int columnIndex, boolean x)", 
                                                     this.getClass().getSimpleName()));
  }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateByte(int columnIndex, byte x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateByte(int columnIndex, byte x)", 
                                                     this.getClass().getSimpleName())); 
  }
  
  /**
   * This feature is not supported in the RESTJDBC driver
   */

  @Override
  public void updateShort(int columnIndex, short x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateShort(int columnIndex, short x)", 
                                                     this.getClass().getSimpleName())); 
  }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateInt(int columnIndex, int x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateInt(int columnIndex, int x)", 
                                                     this.getClass().getSimpleName()));
  }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateLong(int columnIndex, long x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateLong(int columnIndex, long x)", 
                                                      this.getClass().getSimpleName()));  
  }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateFloat(int columnIndex, float x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateFloat(int columnIndex, float x)", 
                                                     this.getClass().getSimpleName()));  
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateDouble(int columnIndex, double x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateDouble(int columnIndex, double x)", 
                                                     this.getClass().getSimpleName()));  
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBigDecimal(int columnIndex, BigDecimal x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBigDecimal(int columnIndex, BigDecimal x)", 
                                                     this.getClass().getSimpleName()));  
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateString(int columnIndex, String x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateString(int columnIndex, String x)", 
                                                     this.getClass().getSimpleName()));  
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBytes(int columnIndex, byte[] x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBytes(int columnIndex, byte[] x)", 
                                                     this.getClass().getSimpleName()));  
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateDate(int columnIndex, Date x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateDate(int columnIndex, Date x)", 
                                                    this.getClass().getSimpleName()));  
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateTime(int columnIndex, Time x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateTime(int columnIndex, Time x)", 
                                                     this.getClass().getSimpleName()));  
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateTimestamp(int columnIndex, Timestamp x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateTimestamp(int columnIndex, Timestamp x)", 
                                                     this.getClass().getSimpleName()));  
    }
  
  /**
   * This feature is not supported in the RESTJDBC driver
   */

  @Override
  public void updateAsciiStream(int columnIndex, InputStream x, int length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateAsciiStream(int columnIndex, InputStream x, int length)", 
                                                    this.getClass().getSimpleName()));  
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBinaryStream(int columnIndex, InputStream x, int length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBinaryStream(int columnIndex, InputStream x, int length)", 
                                                     this.getClass().getSimpleName()));  }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateCharacterStream(int columnIndex, Reader x, int length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateCharacterStream(int columnIndex, Reader x, int length)", 
                                                     this.getClass().getSimpleName()));  
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateObject(int columnIndex, Object x, int scaleOrLength)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateObject(int columnIndex, Object x, int scaleOrLength)", 
                                                     this.getClass().getSimpleName()));  
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateObject(int columnIndex, Object x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateObject(int columnIndex, Object x)", 
                                                     this.getClass().getSimpleName()));      
  }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNull(String columnLabel) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
        "updateNull(String columnLabel)", 
         this.getClass().getSimpleName()));    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBoolean(String columnLabel, boolean x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBoolean(String columnLabel, boolean x)", 
                                                     this.getClass().getSimpleName()));   
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateByte(String columnLabel, byte x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateByte(String columnLabel, byte x)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateShort(String columnLabel, short x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateShort(String columnLabel, short x)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateInt(String columnLabel, int x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateInt(String columnLabel, int x)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateLong(String columnLabel, long x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateLong(String columnLabel, long x", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateFloat(String columnLabel, float x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateFloat(String columnLabel, float x", 
                                                     this.getClass().getSimpleName()));   
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateDouble(String columnLabel, double x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateDouble(String columnLabel, double x)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBigDecimal(String columnLabel, BigDecimal x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBigDecimal(String columnLabel, BigDecimal x)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateString(String columnLabel, String x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateString(String columnLabel, String x)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBytes(String columnLabel, byte[] x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBytes(String columnLabel, byte[] x)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateDate(String columnLabel, Date x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateDate(String columnLabel, Date x)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateTime(String columnLabel, Time x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateTime(String columnLabel, Time x)", 
                                                     this.getClass().getSimpleName()));     
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateTimestamp(String columnLabel, Timestamp x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateTimestamp(String columnLabel, Timestamp x)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateAsciiStream(String columnLabel, InputStream x, int length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateAsciiStream(String columnLabel, InputStream x, int length)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBinaryStream(String columnLabel, InputStream x, int length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBinaryStream(String columnLabel, InputStream x, int length)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateCharacterStream(String columnLabel, Reader reader, int length) 
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateCharacterStream(String columnLabel, Reader reader, int length)", 
                                                     this.getClass().getSimpleName()));      
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateObject(String columnLabel, Object x, int scaleOrLength)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateObject(String columnLabel, Object x, int scaleOrLength)", 
                                                    this.getClass().getSimpleName()));      
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateObject(String columnLabel, Object x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateObject(String columnLabel, Object x", 
                                                     this.getClass().getSimpleName()));    
    }
  
  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNCharacterStream(int columnIndex, Reader x, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNCharacterStream(int columnIndex, Reader x, long length)", 
                                                     this.getClass().getSimpleName()));      
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNCharacterStream(String columnLabel, Reader reader, long length) 
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNCharacterStream(String columnLabel, Reader reader, long length)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateAsciiStream(int columnIndex, InputStream x, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateAsciiStream(int columnIndex, InputStream x, long length)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBinaryStream(int columnIndex, InputStream x, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBinaryStream(int columnIndex, InputStream x, long length)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateCharacterStream(int columnIndex, Reader x, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateCharacterStream(int columnIndex, Reader x, long length)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateAsciiStream(String columnLabel, InputStream x, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateAsciiStream(String columnLabel, InputStream x, long length)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBinaryStream(String columnLabel, InputStream x, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
        "updateBinaryStream(String columnLabel, InputStream x, long length)", 
         this.getClass().getSimpleName()));    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateCharacterStream(String columnLabel, Reader reader, long length) 
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateCharacterStream(String columnLabel, Reader reader, long length) ", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBlob(int columnIndex, InputStream inputStream, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBlob(int columnIndex, InputStream inputStream, long length)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBlob(String columnLabel, InputStream inputStream, long length) 
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBlob(String columnLabel, InputStream inputStream, long length) ", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateClob(int columnIndex, Reader reader, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateClob(int columnIndex, Reader reader, long length)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateClob(String columnLabel, Reader reader, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateClob(String columnLabel, Reader reader, long length)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNClob(int columnIndex, Reader reader, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNClob(int columnIndex, Reader reader, long length)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNClob(String columnLabel, Reader reader, long length)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNClob(String columnLabel, Reader reader, long length)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNCharacterStream(int columnIndex, Reader x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNCharacterStream(int columnIndex, Reader x)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNCharacterStream(String columnLabel, Reader reader)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNCharacterStream(String columnLabel, Reader reader)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateAsciiStream(int columnIndex, InputStream x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateAsciiStream(int columnIndex, InputStream x)", 
                                                    this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBinaryStream(int columnIndex, InputStream x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBinaryStream(int columnIndex, InputStream x)", 
                                                    this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateCharacterStream(int columnIndex, Reader x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateCharacterStream(int columnIndex, Reader x)", 
                                                     this.getClass().getSimpleName()));   
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateAsciiStream(String columnLabel, InputStream x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateAsciiStream(String columnLabel, InputStream x)", 
                                                     this.getClass().getSimpleName()));    
     }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBinaryStream(String columnLabel, InputStream x)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
        "updateBinaryStream(String columnLabel, InputStream x)", 
         this.getClass().getSimpleName()));    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateCharacterStream(String columnLabel, Reader reader)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateCharacterStream(String columnLabel, Reader reader)", 
                                                    this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBlob(int columnIndex, InputStream inputStream)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBlob(int columnIndex, InputStream inputStream)", 
                                                     this.getClass().getSimpleName()));   
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBlob(String columnLabel, InputStream inputStream)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBlob(String columnLabel, InputStream inputStream)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateClob(int columnIndex, Reader reader) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateClob(int columnIndex, Reader reader)", 
                                                    this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateClob(String columnLabel, Reader reader)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateClob(String columnLabel, Reader reader)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNClob(int columnIndex, Reader reader) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNClob(int columnIndex, Reader reader)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNClob(String columnLabel, Reader reader)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNClob(String columnLabel, Reader reader)", 
                                                     this.getClass().getSimpleName()));    
    }
  
  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateSQLXML(int columnIndex, SQLXML xmlObject)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateSQLXML(int columnIndex, SQLXML xmlObject)", 
                                                    this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateSQLXML(String columnLabel, SQLXML xmlObject)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
        "", 
         this.getClass().getSimpleName()));    }
  
  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNString(int columnIndex, String nString)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNString(int columnIndex, String nString)", 
                                                    this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNString(String columnLabel, String nString)
      throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNString(String columnLabel, String nString)", 
                                                     this.getClass().getSimpleName()));   
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNClob(int columnIndex, NClob nClob) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNClob(int columnIndex, NClob nClob)", 
                                                    this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateNClob(String columnLabel, NClob nClob) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateNClob(String columnLabel, NClob nClob)", 
                                                    this.getClass().getSimpleName()));    
    }
  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateRowId(int columnIndex, RowId x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateRowId(int columnIndex, RowId x)", 
                                                    this.getClass().getSimpleName()));    
    }
    
  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateRowId(String columnLabel, RowId x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateRowId(String columnLabel, RowId x)", 
                                                     this.getClass().getSimpleName()));    
    }
 
  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateRef(int columnIndex, Ref x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateRef(int columnIndex, Ref x)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateRef(String columnLabel, Ref x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateRef(String columnLabel, Ref x)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBlob(int columnIndex, Blob x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBlob(int columnIndex, Blob x)", 
                                                    this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateBlob(String columnLabel, Blob x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateBlob(String columnLabel, Blob x)", 
                                                    this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateClob(int columnIndex, Clob x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateClob(int columnIndex, Clob x)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateClob(String columnLabel, Clob x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateClob(String columnLabel, Clob x)", 
                                                     this.getClass().getSimpleName()));    
    }

  
  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateArray(int columnIndex, Array x) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateArray(int columnIndex, Array x)", 
                                                     this.getClass().getSimpleName()));    
    }

  /**
   * This feature is not supported in the RESTJDBC driver
   */
  @Override
  public void updateArray(String columnLabel, Array x) throws SQLException {   
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "updateArray(String columnLabel, Array x)", 
                                                     this.getClass().getSimpleName()));    
    }
  
  /*
   * (non-Javadoc)
   * Helper feature to construct messages for unsupported / unimplemented features
   * @param unsupport - variable to check if the feature is unsupported / unimplemented
   * @return String message to log and throw as an exception
   */

  protected String exceptionLogMessage(boolean unsupport, String methodName) {
    // If the feature is not supported
    if(unsupport) {
      return RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, methodName, this.getClass().getSimpleName());
    }
    // If the feature is supported but not yet implemented
    else {
      return RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, methodName, this.getClass().getSimpleName());
    }
  } 
  

  
  private void throwUnsupportedException(String methodName) throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, methodName, "ResultSet");
  }
 
}
