/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.sql.SQLException;

import java.util.logging.Logger;

import com.fasterxml.jackson.databind.JsonNode;

import oracle.dbtools.jdbc.util.Accessor;
import oracle.dbtools.jdbc.util.ExceptionUtil;
import oracle.dbtools.jdbc.util.OracleTypes;
import oracle.dbtools.jdbc.util.OracleTypesSize;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.dbtools.jdbc.util.SQLStateMapping;

public class ResultSetMetadata implements java.sql.ResultSetMetaData {

	// Global variable for metadata
	private JsonNode rsMetadataTree = null;

	// Logger for the ResultSet Metadata 
	Logger LOGGER = Logger.getLogger(oracle.dbtools.jdbc.ResultSetMetadata.class.getName());

	protected ResultSetMetadata(JsonNode metadataTree) {
		this.rsMetadataTree = metadataTree;
	}

	/* Helper method to get metadata value 
	 * If anything changes - only need to change this method
	 * */
	protected JsonNode getMetadataHelper(int column, String metadata_info) throws SQLException {
	  try {
	    return rsMetadataTree.get("metadata").get(column - 1).get(metadata_info);
	  }
		catch(NullPointerException ne) {
		  // Throw ORA_17003=Invalid column index exception
		  int errorCode = 17003;
		  throw new SQLException((RestjdbcResources.getString("ORA_17003")), 
          SQLStateMapping.getSQLState(errorCode), errorCode);
		}
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getColumnCount()
   */
	@Override
	public int getColumnCount() throws SQLException {
		return this.rsMetadataTree.get("metadata").size();
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#isAutoIncrement(int)
   * 
   * The REST JDBC driver always returns false. 
   * Columns do not automatically increment
   */
	@Override
	public boolean isAutoIncrement(int column) throws SQLException {
		// Always returns false
		return false;
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#isCaseSensitive(int)
   */
	@Override
	public boolean isCaseSensitive(int column) throws SQLException {

		// Exactly like the thin driver
		int type = getColumnType(column);

		return (type == OracleTypes.CHAR) ||
		       (type == OracleTypes.VARCHAR) || 
		       (type == OracleTypes.LONGVARCHAR) || 
		       (type == OracleTypes.NCHAR) || 
		       (type == OracleTypes.NVARCHAR) || 
		       (type == OracleTypes.CLOB) || 
		       (type == OracleTypes.NCLOB) || 
		       (type == OracleTypes.SQLXML) ||
		       (getColumnTypeName(column).contains("XMLTYPE"));
	}

  /* (non-Javadoc)
   * @see java.sql.ResultSetMetaData#isSearchable(int)
   */
	@Override
	public boolean isSearchable(int column) throws SQLException {

		// Get this info exactly like the thin driver
		int type = getColumnType(column);

		return !((type == OracleTypes.LONGVARBINARY) || 
		        (type == OracleTypes.LONGVARCHAR) || 
		         (type == OracleTypes.BLOB) || 
		         (type == OracleTypes.CLOB) || 
		         (type == OracleTypes.BFILE) || 
		         (type == OracleTypes.NCLOB) || 
		         (type == OracleTypes.STRUCT) || 
		         (type == OracleTypes.JAVA_STRUCT) || 
		         (type == OracleTypes.OPAQUE) || 
		         (type == OracleTypes.ARRAY) || 
		         (type == OracleTypes.REF) || 
		         (type == OracleTypes.CURSOR));
	}

  /* (non-Javadoc)
   * @see java.sql.ResultSetMetaData#isCurrency(int)
   */
	@Override
	public boolean isCurrency(int column) throws SQLException {
		String column_type_name = getColumnTypeName(column);

		// Return true if it's a number or a float (like the thin driver)
		return (column_type_name.equals("NUMBER") || column_type_name.equals("FLOAT"));
	}

  /* (non-Javadoc)
   * @see java.sql.ResultSetMetaData#isNullable(int)
   */
	@Override
	public int isNullable(int column) throws SQLException {
		return getMetadataHelper(column, "isNullable").asInt();
	}

  /* (non-Javadoc)
   * @see java.sql.ResultSetMetaData#isSigned(int)
   */
	@Override
	public boolean isSigned(int column) throws SQLException {

		// Always returns true
		return true;
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getColumnDisplaySize(int)
   */
	@Override
	public int getColumnDisplaySize(int column) throws SQLException {
		// get the type of the column
		int type = getColumnType(column);
		// Precision + 1 for numeric datatypes
		if ((type == OracleTypes.TINYINT)   || 
		    (type == OracleTypes.SMALLINT)  || 
		    (type == OracleTypes.INTEGER)   || 
		    (type == OracleTypes.BIGINT)    || 
		    (type == OracleTypes.NUMBER)    || 
		    (type == OracleTypes.FLOAT)     || 
		    (type == OracleTypes.DOUBLE)    || 
		    (type == OracleTypes.NUMERIC)   || 
		    (type == OracleTypes.DECIMAL)   || 
		    (type == OracleTypes.REAL)) 
		{	  
			int precision = getPrecision(column);
			int scale = getScale(column);
			if ((precision != 0 && scale == -127)) {
				// float ... 
				// See "Oracle Database SQL Language Reference" -> Datatypes -> Float
				precision = (int) ((double) precision / 3.32193);
				scale = 1;
			} else {
				if (precision == 0)
					precision = 38;
				if (scale == -127)
					scale = 0;
			}
			int width = precision + ((scale != 0) ? 1 : 0) + 1;
			return width;
		}
		// Precision for text based data types
		else if ((type == OracleTypes.CHAR) || (type == OracleTypes.VARCHAR) || (type == OracleTypes.LONGVARCHAR)) {
			return getPrecision(column);
		} else {
			return OracleTypesSize.getMaxSize(type);
		}
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getColumnLabel(int)
   */
	@Override
	public String getColumnLabel(int column) throws SQLException {
		// Same as getColumnName
		return getMetadataHelper(column, "columnName").asText();
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getColumnName(int)
   */
	@Override
	public String getColumnName(int column) throws SQLException {
		return getMetadataHelper(column, "columnName").asText();
	}

	
  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getSchemaName(int)
   */
	@Override
	public String getSchemaName(int column) throws SQLException {
		// Can't get this information from oracle
		return "";
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getPrecision(int)
   */
	@Override
	public int getPrecision(int column) throws SQLException {
		return getMetadataHelper(column, "precision").asInt();
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getScale(int)
   */
	@Override
	public int getScale(int column) throws SQLException {
		return getMetadataHelper(column, "scale").asInt();
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getTableName(int)
   * 
   * The REST JDBC driver returns an empty String for this method.
   * This information cannot be obtained from the database.
   */
	@Override
	public String getTableName(int column) throws SQLException {
		return "";
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getCatalogName(int)
   * The REST JDBC driver returns an empty String for this method.
   * This information cannot be obtained from the database.
   */
	@Override
	public String getCatalogName(int column) throws SQLException {
		// Can't get this from Oracle
		return "";
	}


  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getColumnType(int)
   */
	@Override
	public int getColumnType(int column) throws SQLException {
		return Accessor.getConstant(getColumnTypeName(column));
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getColumnTypeName(int)
   */
	@Override
	public String getColumnTypeName(int column) throws SQLException {
		return getMetadataHelper(column, "columnTypeName").asText();
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#isReadOnly(int)
   */
	@Override
	public boolean isReadOnly(int column) throws SQLException {
		// Always false
		return false;
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#isWritable(int)
   */
	@Override
	public boolean isWritable(int column) throws SQLException {
		// Always returns true
		return true;
	}

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#isDefinitelyWritable(int)
   */
	@Override
	public boolean isDefinitelyWritable(int column) throws SQLException {
		// Always false in Oracle
		return false;
	}
	

  /** (non-Javadoc)
   * @see java.sql.ResultSetMetaData#getColumnClassName(int)
   */
	@Override
	public String getColumnClassName(int column) throws SQLException {

		// Create a mapping from database types to JDBC types
		int type = getColumnType(column);
		switch (type) {
		case OracleTypes.CHAR:
		case OracleTypes.VARCHAR:
		case OracleTypes.LONGVARCHAR:
			//  case Accessor.FIXED_CHAR:
			return java.lang.String.class.getName();

		case OracleTypes.NUMBER:
			//  case Accessor.VARNUM:// 993752: Distinguish between NUMBER & FLOAT
			if (getPrecision(column) != 0 && (getScale(column) == -127))
				return java.lang.Double.class.getName();
			else
				return java.math.BigDecimal.class.getName();

		case OracleTypes.BINARY:
		case OracleTypes.LONGVARBINARY:
			return byte[].class.getName();

		case OracleTypes.DATE:
		case OracleTypes.TIMESTAMP:		  
			return java.sql.Timestamp.class.getName();

			/*    case OracleTypes.TIMESTAMPTZ:
			      return oracle.sql.TIMESTAMPTZ.class.getName();

			    case OracleTypes.TIMESTAMPLTZ:
			      return oracle.sql.TIMESTAMPLTZ.class.getName();

			    case OracleTypes.INTERVALYM:
			      return oracle.sql.INTERVALYM.class.getName();

			    case OracleTypes.INTERVALDS:
			      return oracle.sql.INTERVALDS.class.getName();

			   // case Accessor.UROWID:
			    case OracleTypes.ROWID:
			      return oracle.sql.ROWID.class.getName();

			    case OracleTypes.BLOB:
			      return oracle.jdbc.OracleBlob.class.getName();

			    case OracleTypes.CLOB:
			 //      if((getDescription()[index].formOfUse
			   //        == OraclePreparedStatement.FORM_NCHAR))
			      return oracle.jdbc.OracleNClob.class.getName();
			   //    else
			  //    return oracle.jdbc.OracleClob.class.getName();

			    case OracleTypes.BFILE:
			      return oracle.jdbc.OracleBfile.class.getName();
			*/

			/* Adding missing cases to fix 11659989 */
		case OracleTypes.BINARY_DOUBLE:
			return java.lang.Double.class.getName();

		case OracleTypes.BINARY_FLOAT:
			return java.lang.Float.class.getName();

		default:
			return null;
		}
	}

	public JsonNode getRsMetadataTree() {
		return rsMetadataTree;
	}

	public void setRsMetadataTree(JsonNode rsMetadataTree) {
		this.rsMetadataTree = rsMetadataTree;
	}
}
