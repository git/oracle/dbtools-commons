/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLWarning;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import oracle.dbtools.http.Session;
import oracle.dbtools.http.SessionException;
import oracle.dbtools.jdbc.util.Defaults;
import oracle.dbtools.jdbc.util.LogUtil;
import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.jdbc.util.RestJdbcUnsupportedException;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.dbtools.jdbc.util.SQLStateMapping;

import oracle.dbtools.jdbc.util.ExceptionUtil;


public class Statement implements java.sql.Statement {

  // Logger for Statement
  Logger LOGGER = Logger.getLogger(oracle.dbtools.jdbc.Statement.class.getName());
  
	protected oracle.dbtools.jdbc.Connection conn = null;
	
	/** (non-Javadoc)
	 * Only one ResultSet can be open at a given time for a given statement.
	 * A pointer to the current result set is kept to be able to 
	 * close it if a new query is issued.
	 */
  private ResultSet currentResultSet;
  // Is the statement closed?
  private boolean closed = false;     
  
  SQLWarning sqlWarning;
  
  // The max number of rows that any ResultSet object generated
  // by this Statement object can contain to the given number
  // zero means there is no limit
  private long maxRows = 0;
	
  // Variable for the sql string that's being executed
  protected String sql=null;
  
  private boolean isCloseOnCompletion = false;
  
  //To keep a count of  the number of ResultSets open by this Statement object
  protected long rsCount = 0; 
  
  // For batched commands
  protected boolean batch = false;  // is this a batched command? 
  protected ArrayList<String> cmds = new ArrayList<String>();
  
  protected int updateCount = -1;
  
  public Statement() {
    
  }
	public Statement(oracle.dbtools.jdbc.Connection conn) {
		this.setConn(conn);
		this.closed = false;
	}
	

  /** (non-Javadoc)
   * Helper method to create a POST request based on offset and limit
   * @param sql
   * @return String JsonPost request to send to ORDS for pagination
   */
  protected SimpleEntry<JsonGenerator,Writer> generatePost(String sql, int offset) throws SQLException {
    //To prepare the POST
    JsonFactory f = new JsonFactory();
    Writer writer = new StringWriter();
    JsonGenerator g = null;
    try {
      g = f.createGenerator(writer);
      g.writeStartObject();
      if(this.batch==false) {
        g.writeStringField("statementText", sql);
      }
      else {
        g.writeArrayFieldStart("statementText");
        for(String sqlStmt : this.cmds) {
          g.writeString(sqlStmt);
        }
        g.writeEndArray();
      }
      g.writeStringField("emulate", "jdbc");
      g.writeNumberField("offset", offset);
      if (fetchSize >0) {
        g.writeNumberField("limit",  fetchSize);
      }
      else {
        g.writeNumberField("limit",  Defaults.DEFAULT_LIMIT);
      }
      g.writeEndObject();
      g.close();
    } catch(IOException e) {
        LOGGER.log(Level.SEVERE, e.getMessage());
    }    
    return new SimpleEntry<JsonGenerator,Writer>(g,writer);
  }
  
  /**
   * (non-Javadoc)
   * Internal helper execute method that all other public execute methods use
   * Sends a POST request to ORDS
   * @param sql
   * @return response from ORDS
   * @throws SQLException
   */
  protected CloseableHttpResponse executeInternal(String sql) throws SQLException {
    if(this.batch==false && (sql == null || sql.isEmpty())) {
      int error_code = 17104;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17104), 
          SQLStateMapping.getSQLState(error_code), error_code);            
    }
    this.sql = sql;
    // First validate the sql input
    CloseableHttpResponse response = null;

    synchronized(conn) {
      Session session = this.conn.getSession();  
      
      // We have to add _/sql/test to the end of the rest endpoint for adhoc sql to work
      StringBuilder uri = new StringBuilder();
      uri.append(this.getConn().getUri());
      uri.append(Defaults.ADHOC_ENDPOINT);
      
      URI root = URI.create(uri.toString());

      try {
        
        // create a POST with appropriate headers
        HttpPost post = session.createPost(root);
        
        post.addHeader("Content-Type", "application/json");
       
        SimpleEntry<JsonGenerator, Writer> simpleEntry = generatePost(sql,Defaults.DEFAULT_OFFSET);
      
        JsonGenerator g = simpleEntry.getKey();
        Writer writer   = simpleEntry.getValue();
        g.close();
        
        StringEntity strEntity = new StringEntity(writer.toString());
        post.setEntity(strEntity);

        // Execute post request
        response = session.executeRequest(post);   
        
       } // end try
      
      catch ( IllegalStateException | IOException | SessionException e) {
        LOGGER.log(Level.SEVERE, e.getMessage());
      }
      return response;
    }   
  }
  
  /**
   * (non-Javadoc)
   * Helper method for a DMLs
   * This is not actually executing the POST request. 
   * Gets back the response from ORDS after a DDL or a DML
   * @throws SQLException 
   */
  protected void handleErrors(JsonNode node) throws SQLException {
    node = node.get("items").get(0);
    if (node.findValue("errorCode")!=null) { //throw SQLException      
      int error_code = node.findValue("errorCode").asInt();
      int error_line = node.findValue("errorLine").asInt();
      int error_column = node.findValue("errorColumn").asInt();
      String error_details = node.findValue("errorDetails").asText();
      throw new SQLException(error_details, SQLStateMapping.getSQLState(error_code), error_code);
    }    
    else if(node.findValue("error")!=null) {
      throw new SQLException(node.findValue("error").asText());
    }
  }
  
  /**
   * (non-Javadoc)
   * Internal helper method that returns the Json response 
   * @param response
   * @return JsonNode with the response to DDLs and DMLs
   * @throws SQLException
   */
  protected JsonNode createJsonNode(CloseableHttpResponse response) throws SQLException {
    HttpEntity entity = response.getEntity();
    JsonNode node = null;
    try {
      InputStream is = entity.getContent(); 
      ObjectMapper mapper = new ObjectMapper();
      node = mapper.readTree(is);
    } 
    
    catch (Exception e) {
      // throw an Internal Error if the node cannot be created properly
      int error_code = 17001;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17001), 
          SQLStateMapping.getSQLState(error_code), error_code); 
    }
    return node;
  }
  
  /**
   * (non-Javadoc)
   * Helper method to extract the appropriate node from the Json response
   */
  protected int getResult(JsonNode node) throws SQLException 
  {
    int result = -1;
    // return -1 if there it is a ResultSet object
    if(node.findValue("items").findValue("resultSet")!=null) {
      return result;
    }
    else if (node.findValue("items").findValue("result")!=null) {
      result = node.findValue("result").asInt();
    } 
    return result;
  }
  
  
  
	/**
	 * @see java.sql.Statement#executeQuery(java.lang.String)
	 */
	@Override
	public ResultSet executeQuery(String sql) throws SQLException {	
	  this.rsCount++;
	  CloseableHttpResponse response = executeInternal(sql);
  	
	  oracle.dbtools.jdbc.ResultSet rs = new oracle.dbtools.jdbc.ResultSet(response, this, this.getConn());
	  this.setCurrentResultSet(rs);		
	  this.updateCount = -1;
	  return rs;
	}  // end executeQuery
	

  /** 
   * @see java.sql.Statement#execute(java.lang.String)
   */
  @Override
  public boolean execute(String sql) throws SQLException{
    boolean result = false;
    CloseableHttpResponse response = null;    
    try {
      result = true;
      response = executeInternal(sql);
      JsonNode node = createJsonNode(response);
      handleErrors(node);    
      this.updateCount = getResult(node);
    }
    finally {
      if(response!=null) {
        try {
          response.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
        }
      }         
    }   
    return result;
  }

  /**
   * (non-Javadoc)
   * Internal executeUpdate method that all other public executeUpdate within the Statement class use
   * to send a POST request to ORDS. The "limit" is only set to 1. 
   * @param sql
   * @return response from ORDS 
   * @throws SQLException
   */
  protected CloseableHttpResponse executeUpdateInternal(String sql) throws SQLException {
    if(sql == null || sql.isEmpty()) {
      int error_code = 17104;
      throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17104), 
          SQLStateMapping.getSQLState(error_code), error_code);            
    }
    this.sql = sql;
    // First validate the sql input
    CloseableHttpResponse response = null;

    synchronized(conn) {
      Session session = this.conn.getSession();  
      
      // We have to add _/sql/test to the end of the rest endpoint for adhoc sql to work
      StringBuilder uri = new StringBuilder();
      uri.append(this.getConn().getUri());
      uri.append(Defaults.ADHOC_ENDPOINT);
      
      URI root = URI.create(uri.toString());

      try {        
        // create a POST with appropriate headers
        HttpPost post = session.createPost(root);
        post.addHeader("Content-Type", "application/json");
        
        JsonFactory f = new JsonFactory();
        Writer writer = new StringWriter();
        JsonGenerator g = null;
        g = f.createGenerator(writer);
        g.writeStartObject();
        g.writeStringField("statementText", sql);
        g.writeNumberField("offset", Defaults.DEFAULT_OFFSET);
        // Limit is set to 1 so that the json results isn't more than a single row
        g.writeNumberField("limit",  1);
        g.writeEndObject();
        g.close();
        
        StringEntity strEntity = new StringEntity(writer.toString());
        post.setEntity(strEntity);

        // Execute post request
        response = session.executeRequest(post);           
       } // end try
      
      catch ( IllegalStateException | IOException | SessionException e) {
        LOGGER.log(Level.SEVERE, e.getMessage());
      }
      return response;
    }   
  }
  
  
  
  @Override
  public int executeUpdate(String sql) throws SQLException {
    int result = 0;
    CloseableHttpResponse response = null;
    try {
      response = executeUpdateInternal(sql); 
      JsonNode node = createJsonNode(response);
      handleErrors(node); 
      
      result = getResult(node);
    }
    finally {
      if(response!=null) {
        try {
          response.close();
        } catch (IOException e) {
          LOGGER.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
        }
      }         
    }
    this.updateCount = result;
    return result;
  }

 
  @Override
  public void close() throws SQLException {
    // Release resources
    if(this.closed == false) {
      this.closed = true;
      maxRows = 0;
      //this.currentResultSet.close();
    }    
  }
	
  /** 
   * @see java.sql.Statement#getConnection()
   */
  @Override
  public Connection getConnection()  throws SQLException {
    ensureOpen();
    return this.conn;
  }  
  
  /**
   * (non-Javadoc)
   * Helper method to ensure that a statement is not closed
   */
  protected void ensureOpen() throws SQLException {
    // If the statement is closed
    if (this.closed) {    
      ExceptionUtil.throwSQLException(17009);
    }    
    // If the connection is closed
    if(conn.isClosed()) {
      ExceptionUtil.throwSQLException(17008);      
    }
  }
  
  /** 
   * @see java.sql.Statement#getFetchDirection()
   * 
   * Oracle database only supports forward only cursor.
   * So this method will always return FETCH_FORWARD;
   */
  @Override
  public int getFetchDirection() throws SQLException {
    ensureOpen();
    
    return ResultSet.FETCH_FORWARD;
  }
  
  /** 
   * @see java.sql.Statement#setFetchDirection(int)
   * 
   * Oracle JDBC driver only supports FETCH_FORWARD
   * Trying to set fetch direction to FETCH_REVERSE or FETCH_UNKNOWN is a no-op
   */
  @Override
  public void setFetchDirection(int direction) throws SQLException {
    
    // want to synchronize it on connection
    synchronized(getConn()) {
      
      // ensure that the connection and the statement is open
      ensureOpen();
      
      if(direction == ResultSet.FETCH_FORWARD) {
        // do nothing please
      }
      
      else if (direction == ResultSet.FETCH_REVERSE || 
               direction == ResultSet.FETCH_UNKNOWN) {
        // set a SqlWarning
        sqlWarning = new java.sql.SQLWarning(RestjdbcResources.get(RestjdbcResources.RESTJDBC_007));
      }
      
      // else if nothing matches
      else {
        // Illegal Fetch direction
        throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17308));
      }        
    } // end synchronized
  } // end setFetchDirection 
  
  

  @Override
  public int getMaxRows() throws SQLException {
    ensureOpen();    
    return (int) maxRows;
  }
  

  @Override
  public void setMaxRows(int max) throws SQLException {
      ensureOpen();
      // Invalid max value
      if(max < 0) {
        int errorCode = 17068;
        throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17068),
                               SQLStateMapping.getSQLState(errorCode), errorCode);      
      }      
      maxRows = max;
  } // end setMaxRows()
  
  /**
   * Generate the POST request to be sent based on the FetchSize
   * TODO: $asof not yet implemented in ADHOC
   * 
   * Example:
   * {  
        "statement-text": "select object_name from all_objects",  
        "offset": 25,  
        "limit": 25,  
        "$asof": {"$scn": "1273919"}  
      }  
   */
  
  
  /**
   * @see java.sql.Statement#setFetchSize(int)
   */
  protected long fetchSize=0; // default value of the fetchSize
  public void setFetchSize(int rows) throws SQLException {
      ensureOpen();
      if(rows < 0) {
        throw new SQLException(RestjdbcResources.getString(RestjdbcResources.ORA_17433));
      }
      fetchSize = rows;
  } // end setFetchSize()

  
  /**
   * @see java.sql.Statement#getFetchSize()
   */
  @Override
  public int getFetchSize() throws SQLException {
    ensureOpen();
    if(this.fetchSize == 0) {
      return Defaults.DEFAULT_LIMIT;
    }
    return (int) fetchSize;
  }
  
  /** 
   * @see java.sql.Statement#closeOnCompletion()
   */
  @Override
  public void closeOnCompletion() throws SQLException {
    ensureOpen();
    if(!this.closed) {isCloseOnCompletion = true;}   
  }

  /** 
   * @see java.sql.Statement#isCloseOnCompletion()
   */
  @Override
  public boolean isCloseOnCompletion() throws SQLException {
    ensureOpen();
    return this.isCloseOnCompletion;
  }
  
  /** 
   * @see java.sql.Statement#isClosed()
   */
  @Override
  public boolean isClosed() throws SQLException {     
    if(this.isCloseOnCompletion && this.rsCount==0) {
      this.close();
    }
    return this.closed;
  }
  
  /** 
   * @see java.sql.Statement#getResultSet()
   */
  @Override
  public ResultSet getResultSet() throws SQLException {      
    return this.getCurrentResultSet();
  }
  
  /**
   * @see java.sql.Statement#clearWarnings()
   */
  @Override
  public void clearWarnings() throws SQLException
  {
    ensureOpen();
    sqlWarning = null;
  }
  
  /**
   * @see java.sql.Statement#getWarnings()
   */
  @Override
  public SQLWarning getWarnings() throws SQLException
  {
    ensureOpen();
    LogUtil.logOrException(this.getConnection(), null, "Statement.getWarnings() not supported", null);
    return sqlWarning;
  }
  
  /**
   * @see java.sql.Statement#getQueryTimeout()
   * 
   * This method is not currently implemented in the REST JDBC driver.
   */
  @Override
  public int getQueryTimeout() throws SQLException {
   throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
       "getQueryTimeout()", this.getClass().getSimpleName()));
  
  }

  /**
   * @see java.sql.Statement#setQueryTimeout(int)
   * 
   * This method is not currently implemented in the REST JDBC driver. 
   */
  @Override
  public void setQueryTimeout(int seconds) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "setQueryTimeout(int seconds)", this.getClass().getSimpleName()));

  }

  /**
   * @see java.sql.Wrapper#unwrap(java.lang.Class)
   */ 
  @Override
  public <T> T unwrap(Class<T> iface) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "unwrap(Class<T> iface)", this.getClass().getSimpleName()));
  }

  /**
   * @see java.sql.Wrapper#isWrapperFor(java.lang.Class)
   */
  @Override
  public boolean isWrapperFor(Class<?> iface) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "isWrapperFor(Class<?> iface)", this.getClass().getSimpleName()));
  }

  /** 
   * @see java.sql.Statement#getMaxFieldSize()
   * 
   * This method is not currently implemented in the REST JDBC driver. 
   */
  @Override
  public int getMaxFieldSize() throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "getMaxFieldSize()", 
                                                    this.getClass().getSimpleName()));  
  }

  /**
   * @see java.sql.Statement#setMaxFieldSize(int)
   * 
   * This method is not currently implemented in the REST JDBC driver.
   */
  @Override
  public void setMaxFieldSize(int max) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED,
                                                    "setMaxFieldSize(int max)", 
                                                    this.getClass().getSimpleName()));    
 
  }

  /**
   * @see java.sql.Statement#setEscapeProcessing(boolean)
   * 
   * This method is not supported by the REST JDBC driver. 
   * An Unsupported Exception isn't thrown, so the application can keep running.
   */
  @Override
  public void setEscapeProcessing(boolean enable) throws SQLException {
    ensureOpen();
    LogUtil.log(null, Level.INFO, "setEscapeProcessing does not affect the REST JDBC driver" , null);    
  }

  /**
   * The driver is RESTful, and does not support transactions. Hence, this method is not supported by the REST JDBC driver.
   * @throws SQLFeatureNotSupportedException
   */
  @Override
  public void cancel() throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, "cancel()", this.getClass().getSimpleName());
  }

  /**
   * Cursors are not supported in the REST JDBC driver.
   * @throws SQLFeatureNotSupportedException
   */
  @Override
  public void setCursorName(String name) throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "setCursorName(String name)", this.getClass().getSimpleName()));
    
  }

  /**
   * @see java.sql.Statement#getUpdateCount()
   */
  @Override
  public int getUpdateCount() throws SQLException {
    ensureOpen();
    return this.updateCount;
  }

  /**
   * @see java.sql.Statement#getMoreResults()
   * 
   * This method is not supported in the REST JDBC driver.
   */
  @Override
  public boolean getMoreResults() throws SQLException {
    throw new RestJdbcNotImplementedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, 
        "getMoreResults()", this.getClass().getSimpleName()));
  }

  /**
   * This method is not supported by the REST JDBC driver
   * @throws SQLFeatureNotSupportedException
   */
  @Override
  public int getResultSetConcurrency() throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, "getResultSetConcurrency()", this.getClass().getSimpleName());
    return 0;
  }

  /**
   * @see java.sql.Statement#getResultSetType()
   * 
   * The REST JDBC driver only supports FORWARD ONLY ResultSets
   */
  @Override
  public int getResultSetType() throws SQLException {
    ensureOpen();
    return ResultSet.TYPE_FORWARD_ONLY;
  }

  /**
   *  @see java.sql.Statement#addBatch(java.lang.String)
   */
  @Override
  public void addBatch(String sql) throws SQLException {
    this.batch = true;
    cmds.add(sql);
  }

  /**
   * @see java.sql.Statement#clearBatch()
   */
  @Override
  public void clearBatch() throws SQLException {
    this.batch = false;
    cmds.clear();
  }

  /**
   * (non-Javadoc)
   * Helper method for batch processing
   */
  protected int[] createIntArray(JsonNode node) throws SQLException {
    int i = 0;
    JsonNode resultNode = node.get("items").get(0).get("result");
    int[] result = new int[resultNode.size()];
    while(resultNode.get(i)!=null) { 
      try {
        result[i] = resultNode.get(i).asInt();
        i++;
      }
      // Probably ArrayIndexOutOfBounds exception... shouldn't happen.
      // Throw internal error
      catch (Exception e) {
        ExceptionUtil.throwSQLException(17089);
      }
    }
    return result;
  }
  

  /**
   * @see java.sql.Statement#executeBatch()
   */
  @Override
  public int[] executeBatch() throws SQLException {
    int[] result = new int[0];
    if(this.batch == true) {      
      CloseableHttpResponse response = null;    
      try {
        response = executeInternal(this.sql);
        JsonNode node = createJsonNode(response);
        handleErrors(node); 
        result = createIntArray(node);          
      }
      finally {
        // Clear the Batch
        this.batch = false;
        cmds.clear();
        if(response!=null) {
          try {
            response.close();
          } catch (IOException e) {
              LOGGER.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
          }
        }         
      } 
    }
    this.updateCount = result.length;
    return result;
  }

  /**
   * @see java.sql.Statement#getMoreResults(int)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public boolean getMoreResults(int current) throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, "getMoreResults(int current)", this.getClass().getSimpleName());
    return false;
  }

  /**
   * @see java.sql.Statement#getGeneratedKeys()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public ResultSet getGeneratedKeys() throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, " getGeneratedKeys()", this.getClass().getSimpleName());
    return null;
  }

  /** 
   * @see java.sql.Statement#executeUpdate(java.lang.String, int)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public int executeUpdate(String sql, int autoGeneratedKeys)
      throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, "executeUpdate(String sql, int autoGeneratedKeys)", 
                                          this.getClass().getSimpleName());
    return 0;
  }

  /**
   * @see java.sql.Statement#executeUpdate(java.lang.String, int[])
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public int executeUpdate(String sql, int[] columnIndexes)
      throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, "executeUpdate(String sql, int[] columnIndexes)", 
                                          this.getClass().getSimpleName());
    return 0;
  }


  /**
   * @see java.sql.Statement#executeUpdate(java.lang.String, java.lang.String[])
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public int executeUpdate(String sql, String[] columnNames)
      throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, "executeUpdate(String sql, String[] columnIndexes)", 
                                          this.getClass().getSimpleName());
    return 0;
  }

  /**
   * @see java.sql.Statement#execute(java.lang.String, int)
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public boolean execute(String sql, int autoGeneratedKeys)
      throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, "(String sql, int autoGeneratedKeys)", 
                                          this.getClass().getSimpleName());
    this.updateCount = 0;
    return false;
  }

  /**
   * @see java.sql.Statement#execute(java.lang.String, int[])
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public boolean execute(String sql, int[] columnIndexes) throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, "(String sql, int[] columnIndexes)", 
                                          this.getClass().getSimpleName()); 
        
    return false;
  }

  /**
   * @see java.sql.Statement#execute(java.lang.String, java.lang.String[])
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public boolean execute(String sql, String[] columnNames) throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, "execute(String sql, String[] columnNames)", 
                                          this.getClass().getSimpleName()); 
    return false;
  }

  /**
   * @see java.sql.Statement#getResultSetHoldability()
   * 
   * This feature is not supported in the REST JDBC driver.
   */
  @Override
  public int getResultSetHoldability() throws SQLException {
    ExceptionUtil.logExceptionUnsupported(this.conn, "getResultSetHoldability()", 
                                          this.getClass().getSimpleName()); 
    return 0;
  }

  /**
   * @see java.sql.Statement#setPoolable(boolean)
   * 
   * setPoolable has no effect on the REST JDBC driver. An Unsupported Exception isn't thrown. 
   */
  @Override
  public void setPoolable(boolean poolable) throws SQLException {
    ensureOpen();
    LogUtil.log(null, Level.INFO, "setPoolable does not affect the REST JDBC driver" , null);    
  }

  /**
   * @see java.sql.Statement#isPoolable()
   * 
   * The REST JDBC driver always returns false for this method. setPoolable does not effect the REST JDBC driver.
   */
  @Override
  public boolean isPoolable() throws SQLException {
    ensureOpen();
    return false;
  }
  
  /** (non-Javadoc)
   * Helper method that returns the sql string that was executed
   */
  public String getSqlString() {
    return this.sql;
  }
  
  /** (non-Javadoc)
   * Helper method for the OREST driver.
   * @return
   */
  public ResultSet getCurrentResultSet() {
    return currentResultSet;
  }
  public void setCurrentResultSet(ResultSet currentResultSet) {
    this.currentResultSet = currentResultSet;
  }
  public oracle.dbtools.jdbc.Connection getConn() {
  	return conn;
  }
  public void setConn(oracle.dbtools.jdbc.Connection conn) {
	this.conn = conn;
  }
  public CloseableHttpResponse orestExecuteInternal(String sql) throws SQLException {
    return executeInternal(sql);
  }
}
