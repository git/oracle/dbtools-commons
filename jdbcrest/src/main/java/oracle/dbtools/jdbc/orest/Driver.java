/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.orest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.util.Properties;

import oracle.dbtools.jdbc.util.LogUtil;

/**
 * @author debjani
 *
 */
public class Driver extends oracle.dbtools.jdbc.Driver {
	public static final String PREFIX="jdbc:oracle:orest";

  //public Logger restJDBCLogger = Logger.getLogger("OracleRestJDBCDriverLogger");
  /*
   * To register a Driver
   */
  static{
	  LogUtil.log();
  //  System.out.println("Static block");
    try{
      //not going to work without base driver.
      Class.forName("oracle.dbtools.jdbc.Driver");
      Driver driver = new Driver();
      DriverManager.registerDriver(driver);
      LogUtil.setOn();
    }
    catch(SQLException e)
    {
        throw new RuntimeException(ORestjdbcResources.getString(ORestjdbcResources.ORESTJDBC_CANNOT_REGISTER));
    }
    catch(ClassNotFoundException e)
    {
        throw new RuntimeException(ORestjdbcResources.getString(ORestjdbcResources.ORESTJDBC_CANNOT_REGISTER));
    }
  }
  
  

  /*
   * (non-Javadoc)
   * @see oracle.dbtools.restjdbc.abstractDef.RestJDBCDriver#connect(java.lang.String, java.util.Properties)
   */
  @Override
  public Connection connect(String url, Properties info) throws SQLException {
    if (!acceptsURL(url)) {
    	return null;
    }
    LogUtil.log();
    return new oracle.dbtools.jdbc.orest.ORestConnection(url,info);  
  }
  
  
  /*
   * TODO : implement method to check if the db url inputted by user is good
   * (non-Javadoc)
   * @see oracle.dbtools.restjdbc.abstractDef.RestJDBCDriver#acceptsURL(java.lang.String)
   */
  @Override
  public boolean acceptsURL(String url) throws SQLException {
	LogUtil.log();
    //System.out.println("Here in accepts");
    return ((url!=null)&&(url.startsWith(PREFIX))&& (url.indexOf("@")!=-1));
  }
  
  /*
   * TODO: Implement method to extract properties from property file (now done within connect method)
   * (non-Javadoc) 
   * @see oracle.dbtools.restjdbc.abstractDef.RestJDBCDriver#getPropertyInfo(java.lang.String, java.util.Properties)
   */
  @Override
  public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
	  LogUtil.log();
    return null;
  }

  // TODO: Implement other methods from oracle.dbtools.restjdbc.abstractDef.RestJDBCDriver as and when needed 
}

