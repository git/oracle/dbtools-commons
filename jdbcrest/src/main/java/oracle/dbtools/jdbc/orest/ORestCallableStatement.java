/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.orest;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

import org.apache.http.client.methods.CloseableHttpResponse;

import oracle.dbtools.jdbc.CallableStatement;
import oracle.dbtools.jdbc.Connection;
import oracle.dbtools.jdbc.util.LogUtil;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleDataFactory;
import oracle.jdbc.OracleParameterMetaData;
import oracle.jdbc.dcn.DatabaseChangeRegistration;
import oracle.sql.ARRAY;
import oracle.sql.BFILE;
import oracle.sql.BINARY_DOUBLE;
import oracle.sql.BINARY_FLOAT;
import oracle.sql.BLOB;
import oracle.sql.CHAR;
import oracle.sql.CLOB;
import oracle.sql.CustomDatum;
import oracle.sql.CustomDatumFactory;
import oracle.sql.DATE;
import oracle.sql.Datum;
import oracle.sql.INTERVALDS;
import oracle.sql.INTERVALYM;
import oracle.sql.NUMBER;
import oracle.sql.OPAQUE;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.RAW;
import oracle.sql.REF;
import oracle.sql.ROWID;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import oracle.sql.TIMESTAMP;
import oracle.sql.TIMESTAMPLTZ;
import oracle.sql.TIMESTAMPTZ;

public class ORestCallableStatement extends CallableStatement implements OracleCallableStatement {

	protected final ORestCallableStatementImpl impl;
	public ORestCallableStatement(Connection conn, String sql) {
		super(conn, sql);
		impl= new ORestCallableStatementImpl(this);
		LogUtil.log(); // TODOAuto-generated constructor stub
	}

	@Override
	public OracleParameterMetaData OracleGetParameterMetaData() throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public void defineParameterType(int arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineParameterTypeBytes(int arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineParameterTypeChars(int arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public int getExecuteBatch() {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return 0;
	}

	@Override
	public ResultSet getReturnResultSet() throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public void registerReturnParameter(int arg0, int arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void registerReturnParameter(int arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void registerReturnParameter(int arg0, int arg1, String arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setARRAY(int arg0, ARRAY arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setARRAYAtName(String arg0, ARRAY arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setArrayAtName(String arg0, Array arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setAsciiStreamAtName(String arg0, InputStream arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setAsciiStreamAtName(String arg0, InputStream arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setAsciiStreamAtName(String arg0, InputStream arg1, long arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBFILE(int arg0, BFILE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBFILEAtName(String arg0, BFILE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBLOB(int arg0, BLOB arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBLOBAtName(String arg0, BLOB arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBfile(int arg0, BFILE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBfileAtName(String arg0, BFILE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBigDecimalAtName(String arg0, BigDecimal arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryDouble(int arg0, double arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryDouble(int arg0, BINARY_DOUBLE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryDoubleAtName(String arg0, double arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryDoubleAtName(String arg0, BINARY_DOUBLE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryFloat(int arg0, float arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryFloat(int arg0, BINARY_FLOAT arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryFloatAtName(String arg0, float arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryFloatAtName(String arg0, BINARY_FLOAT arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryStreamAtName(String arg0, InputStream arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryStreamAtName(String arg0, InputStream arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryStreamAtName(String arg0, InputStream arg1, long arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBlobAtName(String arg0, Blob arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBlobAtName(String arg0, InputStream arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBlobAtName(String arg0, InputStream arg1, long arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBooleanAtName(String arg0, boolean arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setByteAtName(String arg0, byte arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBytesAtName(String arg0, byte[] arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBytesForBlob(int arg0, byte[] arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBytesForBlobAtName(String arg0, byte[] arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCHAR(int arg0, CHAR arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCHARAtName(String arg0, CHAR arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCLOB(int arg0, CLOB arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCLOBAtName(String arg0, CLOB arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCharacterStreamAtName(String arg0, Reader arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCharacterStreamAtName(String arg0, Reader arg1, long arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCheckBindTypes(boolean arg0) {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setClobAtName(String arg0, Clob arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setClobAtName(String arg0, Reader arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setClobAtName(String arg0, Reader arg1, long arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCursor(int arg0, ResultSet arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCursorAtName(String arg0, ResultSet arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCustomDatum(int arg0, CustomDatum arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCustomDatumAtName(String arg0, CustomDatum arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setDATE(int arg0, DATE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setDATEAtName(String arg0, DATE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setDateAtName(String arg0, Date arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setDateAtName(String arg0, Date arg1, Calendar arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setDisableStmtCaching(boolean arg0) {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setDoubleAtName(String arg0, double arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setFixedCHAR(int arg0, String arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setFixedCHARAtName(String arg0, String arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setFloatAtName(String arg0, float arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setFormOfUse(int arg0, short arg1) {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setINTERVALDS(int arg0, INTERVALDS arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setINTERVALDSAtName(String arg0, INTERVALDS arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setINTERVALYM(int arg0, INTERVALYM arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setINTERVALYMAtName(String arg0, INTERVALYM arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setIntAtName(String arg0, int arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setLongAtName(String arg0, long arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNCharacterStreamAtName(String arg0, Reader arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNCharacterStreamAtName(String arg0, Reader arg1, long arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNClobAtName(String arg0, NClob arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNClobAtName(String arg0, Reader arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNClobAtName(String arg0, Reader arg1, long arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNStringAtName(String arg0, String arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNUMBER(int arg0, NUMBER arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNUMBERAtName(String arg0, NUMBER arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNullAtName(String arg0, int arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNullAtName(String arg0, int arg1, String arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setOPAQUE(int arg0, OPAQUE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setOPAQUEAtName(String arg0, OPAQUE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setORAData(int arg0, ORAData arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setORADataAtName(String arg0, ORAData arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setObjectAtName(String arg0, Object arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setObjectAtName(String arg0, Object arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setObjectAtName(String arg0, Object arg1, int arg2, int arg3) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setOracleObject(int arg0, Datum arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setOracleObjectAtName(String arg0, Datum arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setPlsqlIndexTable(int arg0, Object arg1, int arg2, int arg3, int arg4, int arg5) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRAW(int arg0, RAW arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRAWAtName(String arg0, RAW arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setREF(int arg0, REF arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setREFAtName(String arg0, REF arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setROWID(int arg0, ROWID arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setROWIDAtName(String arg0, ROWID arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRefAtName(String arg0, Ref arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRefType(int arg0, REF arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRefTypeAtName(String arg0, REF arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRowIdAtName(String arg0, RowId arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setSQLXMLAtName(String arg0, SQLXML arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setSTRUCT(int arg0, STRUCT arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setSTRUCTAtName(String arg0, STRUCT arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setShortAtName(String arg0, short arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setStringAtName(String arg0, String arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setStringForClob(int arg0, String arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setStringForClobAtName(String arg0, String arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setStructDescriptor(int arg0, StructDescriptor arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setStructDescriptorAtName(String arg0, StructDescriptor arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTIMESTAMP(int arg0, TIMESTAMP arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTIMESTAMPAtName(String arg0, TIMESTAMP arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTIMESTAMPLTZ(int arg0, TIMESTAMPLTZ arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTIMESTAMPLTZAtName(String arg0, TIMESTAMPLTZ arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTIMESTAMPTZ(int arg0, TIMESTAMPTZ arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTIMESTAMPTZAtName(String arg0, TIMESTAMPTZ arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTimeAtName(String arg0, Time arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTimeAtName(String arg0, Time arg1, Calendar arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTimestampAtName(String arg0, Timestamp arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTimestampAtName(String arg0, Timestamp arg1, Calendar arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setURLAtName(String arg0, URL arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setUnicodeStreamAtName(String arg0, InputStream arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void clearDefines() throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void closeWithKey(String arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public int creationState() {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return 0;
	}

	@Override
	public void defineColumnType(int arg0, int arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineColumnType(int arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineColumnType(int arg0, int arg1, String arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineColumnType(int arg0, int arg1, int arg2, short arg3) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineColumnTypeBytes(int arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineColumnTypeChars(int arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public int getLobPrefetchSize() throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return 0;
	}

	@Override
	public long getRegisteredQueryId() throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return 0;
	}

	@Override
	public String[] getRegisteredTableNames() throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public int getRowPrefetch() {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return 0;
	}

	@Override
	public boolean isNCHAR(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return false;
	}

	@Override
	public void setDatabaseChangeRegistration(DatabaseChangeRegistration arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setLobPrefetchSize(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRowPrefetch(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public ARRAY getARRAY(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public Object getAnyDataEmbeddedObject(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public InputStream getAsciiStream(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public BFILE getBFILE(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public BLOB getBLOB(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public BFILE getBfile(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public InputStream getBinaryStream(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public InputStream getBinaryStream(String arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public CHAR getCHAR(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public CLOB getCLOB(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public ResultSet getCursor(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public Object getCustomDatum(int arg0, CustomDatumFactory arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public DATE getDATE(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public INTERVALDS getINTERVALDS(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public INTERVALYM getINTERVALYM(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public NUMBER getNUMBER(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public OPAQUE getOPAQUE(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public Object getORAData(int arg0, ORADataFactory arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public Object getObject(int arg0, OracleDataFactory arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public Datum getOracleObject(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public Datum[] getOraclePlsqlIndexTable(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public Object getPlsqlIndexTable(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public Object getPlsqlIndexTable(int arg0, Class arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public RAW getRAW(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public REF getREF(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public ROWID getROWID(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public STRUCT getSTRUCT(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public TIMESTAMP getTIMESTAMP(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public TIMESTAMPLTZ getTIMESTAMPLTZ(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public TIMESTAMPTZ getTIMESTAMPTZ(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public InputStream getUnicodeStream(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public InputStream getUnicodeStream(String arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public void registerIndexTableOutParameter(int arg0, int arg1, int arg2, int arg3) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void registerOutParameter(int arg0, int arg1, int arg2, int arg3) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void registerOutParameter(String arg0, int arg1, int arg2, int arg3) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void registerOutParameterAtName(String arg0, int arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void registerOutParameterAtName(String arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void registerOutParameterAtName(String arg0, int arg1, String arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void registerOutParameterBytes(int arg0, int arg1, int arg2, int arg3) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void registerOutParameterChars(int arg0, int arg1, int arg2, int arg3) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public int sendBatch() throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return 0;
	}

	@Override
	public void setARRAY(String arg0, ARRAY arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setArray(String arg0, Array arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBFILE(String arg0, BFILE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBLOB(String arg0, BLOB arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBfile(String arg0, BFILE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryDouble(String arg0, BINARY_DOUBLE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryDouble(String arg0, double arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryFloat(String arg0, BINARY_FLOAT arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBinaryFloat(String arg0, float arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setBytesForBlob(String arg0, byte[] arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCHAR(String arg0, CHAR arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCLOB(String arg0, CLOB arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCursor(String arg0, ResultSet arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setCustomDatum(String arg0, CustomDatum arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setDATE(String arg0, DATE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setExecuteBatch(int arg0) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setFixedCHAR(String arg0, String arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setINTERVALDS(String arg0, INTERVALDS arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setINTERVALYM(String arg0, INTERVALYM arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setNUMBER(String arg0, NUMBER arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setOPAQUE(String arg0, OPAQUE arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setORAData(String arg0, ORAData arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setOracleObject(String arg0, Datum arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRAW(String arg0, RAW arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setREF(String arg0, REF arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setROWID(String arg0, ROWID arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRef(String arg0, Ref arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRefType(String arg0, REF arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setSTRUCT(String arg0, STRUCT arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setStringForClob(String arg0, String arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setStructDescriptor(String arg0, StructDescriptor arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTIMESTAMP(String arg0, TIMESTAMP arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTIMESTAMPLTZ(String arg0, TIMESTAMPLTZ arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setTIMESTAMPTZ(String arg0, TIMESTAMPTZ arg1) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setUnicodeStream(String arg0, InputStream arg1, int arg2) throws SQLException {
		LogUtil.log(); // TODOAuto-generated method stub
                throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName())); 

	}

	// return orest result set:
	@Override
	public ResultSet executeQuery() throws SQLException {
		CloseableHttpResponse response = orestExecuteInternal(this.sql);

		this.setCurrentResultSet(new oracle.dbtools.jdbc.orest.ORestResultSet(response, (oracle.dbtools.jdbc.CallableStatement)this, conn));
		return this.getCurrentResultSet();
	}
	@Override
	public ResultSet executeQuery(String sql) throws SQLException {
		sql = sqlformatter(sql);
		CloseableHttpResponse response = orestExecuteInternal(sql);

		this.setCurrentResultSet(new oracle.dbtools.jdbc.orest.ORestResultSet(response, (oracle.dbtools.jdbc.CallableStatement) this, conn));
		return this.getCurrentResultSet();
	}

	private String sqlformatter(String sql) {

		// Check if the string is within braces
		if (sql.charAt(0) == '{' && sql.charAt(sql.length() - 1) == '}') {
			sql = sql.substring(1, sql.length() - 1);
		}
		// replace "call" with "exec"
		return sql;
	}
}
