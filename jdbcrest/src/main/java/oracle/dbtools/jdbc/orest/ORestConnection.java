/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.orest;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.Executor;

import oracle.dbtools.http.Session;
import oracle.dbtools.jdbc.util.LogUtil;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.jdbc.LogicalTransactionId;
import oracle.jdbc.LogicalTransactionIdEventListener;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleOCIFailover;
import oracle.jdbc.OracleSavepoint;
import oracle.jdbc.OracleShardingKey;
import oracle.jdbc.aq.AQDequeueOptions;
import oracle.jdbc.aq.AQEnqueueOptions;
import oracle.jdbc.aq.AQMessage;
import oracle.jdbc.aq.AQNotificationRegistration;
import oracle.jdbc.dcn.DatabaseChangeRegistration;
import oracle.jdbc.pool.OracleConnectionCacheCallback;
import oracle.sql.ARRAY;
import oracle.sql.BINARY_DOUBLE;
import oracle.sql.BINARY_FLOAT;
import oracle.sql.DATE;
import oracle.sql.INTERVALDS;
import oracle.sql.INTERVALYM;
import oracle.sql.NUMBER;
import oracle.sql.TIMESTAMP;
import oracle.sql.TIMESTAMPLTZ;
import oracle.sql.TIMESTAMPTZ;
import oracle.sql.TypeDescriptor;


public class ORestConnection extends oracle.dbtools.jdbc.Connection implements OracleConnection {
	private static String DRIVER_NAME = "Oracle REST 'instance of OracleConnection' JDBC driver";
    @Override
    public Connection _getPC() {
        // TODO Auto-generated method stub
	throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
									 new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
	//return null;
    }

    public ORestConnection(String db_url, Properties info) throws SQLException {
        super(stripUrl(db_url), info);
        this.setURL(db_url);
        LogUtil.log();// TODO Auto-generated constructor stu
    }
    private static String stripUrl(String inURL) {
        String retVal=inURL;
        int at=inURL.indexOf("@");
        if ((inURL.startsWith(Driver.PREFIX) && (at!=-1))) {
            //ignore everything between orest and @ could be ":" "" username/password
            retVal=inURL.substring(at+1);
        }
        return retVal;
    }

    @Override
    public String getDriverName(){
        return DRIVER_NAME;
    }

    @Override
    public void abort() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement;
    }

    @Override
    public void addLogicalTransactionIdEventListener(LogicalTransactionIdEventListener arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void addLogicalTransactionIdEventListener(LogicalTransactionIdEventListener arg0, Executor arg1)
            throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void applyConnectionAttributes(Properties arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void archive(int arg0, int arg1, String arg2) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?

    }

    @Override
    public boolean attachServerConnection() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public void beginRequest() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void cancel() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void clearAllApplicationContext(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void close(Properties arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        super.close();
    }

    @Override
    public void close(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        super.close();
    }

    @Override
    public void commit(EnumSet<CommitOption> arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        super.commit();
    }

    @Override
    public ARRAY createARRAY(String arg0, Object arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public BINARY_DOUBLE createBINARY_DOUBLE(double arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public BINARY_FLOAT createBINARY_FLOAT(float arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public DATE createDATE(Date arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public DATE createDATE(Time arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public DATE createDATE(Timestamp arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public DATE createDATE(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public DATE createDATE(Date arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public DATE createDATE(Time arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public DATE createDATE(Timestamp arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement
        //return null;
    }

    @Override
    public INTERVALDS createINTERVALDS(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public INTERVALYM createINTERVALYM(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public NUMBER createNUMBER(boolean arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public NUMBER createNUMBER(byte arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public NUMBER createNUMBER(short arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public NUMBER createNUMBER(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public NUMBER createNUMBER(long arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public NUMBER createNUMBER(float arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public NUMBER createNUMBER(double arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public NUMBER createNUMBER(BigDecimal arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public NUMBER createNUMBER(BigInteger arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public NUMBER createNUMBER(String arg0, int arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public Array createOracleArray(String arg0, Object arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMP createTIMESTAMP(Date arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMP createTIMESTAMP(DATE arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMP createTIMESTAMP(Time arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMP createTIMESTAMP(Timestamp arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMP createTIMESTAMP(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPLTZ createTIMESTAMPLTZ(Date arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPLTZ createTIMESTAMPLTZ(Time arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPLTZ createTIMESTAMPLTZ(Timestamp arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPLTZ createTIMESTAMPLTZ(String arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPLTZ createTIMESTAMPLTZ(DATE arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPTZ createTIMESTAMPTZ(Date arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPTZ createTIMESTAMPTZ(Time arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPTZ createTIMESTAMPTZ(Timestamp arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPTZ createTIMESTAMPTZ(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPTZ createTIMESTAMPTZ(DATE arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPTZ createTIMESTAMPTZ(Date arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPTZ createTIMESTAMPTZ(Time arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPTZ createTIMESTAMPTZ(Timestamp arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TIMESTAMPTZ createTIMESTAMPTZ(String arg0, Calendar arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public AQMessage dequeue(String arg0, AQDequeueOptions arg1, byte[] arg2) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public AQMessage dequeue(String arg0, AQDequeueOptions arg1, String arg2) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public void detachServerConnection(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void endRequest() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void enqueue(String arg0, AQEnqueueOptions arg1, AQMessage arg2) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public TypeDescriptor[] getAllTypeDescriptorsInCurrentSchema() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public String getAuthenticationAdaptorName() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public boolean getAutoClose() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public CallableStatement getCallWithKey(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public Properties getConnectionAttributes() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public int getConnectionReleasePriority() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return 0;
    }

    @Override
    public boolean getCreateStatementAsRefCursor() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public String getCurrentSchema() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public String getDataIntegrityAlgorithmName() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public DatabaseChangeRegistration getDatabaseChangeRegistration(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public int getDefaultExecuteBatch() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return 0;
    }

    @Override
    public int getDefaultRowPrefetch() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return 0;
    }

    @Override
    public TimeZone getDefaultTimeZone() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public Object getDescriptor(String arg0) {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public String getEncryptionAlgorithmName() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public short getEndToEndECIDSequenceNumber() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return 0;
    }

    @Override
    public String[] getEndToEndMetrics() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public boolean getExplicitCachingEnabled() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public boolean getImplicitCachingEnabled() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public boolean getIncludeSynonyms() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public Object getJavaObject(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public LogicalTransactionId getLogicalTransactionId() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public Properties getProperties() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public boolean getRemarksReporting() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public boolean getRestrictGetTables() {
        //implement?
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return false;
    }

    @Override
    public String getSQLType(Object arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public String getSessionTimeZone() {
      String timeZoneID = null;
      try {
        timeZoneID = this.getTimeZoneID();
      }
      catch (SQLException e) {
        LogUtil.log(null, e.getMessage(), e);
      }
      return timeZoneID;
    }

    @Override
    public String getSessionTimeZoneOffset() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public int getStatementCacheSize() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return 0;
    }

    @Override
    public PreparedStatement getStatementWithKey(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public int getStmtCacheSize() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return 0;
    }

    @Override
    public short getStructAttrCsId() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return 0;
    }

    @Override
    public TypeDescriptor[] getTypeDescriptorsFromList(String[][] arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public TypeDescriptor[] getTypeDescriptorsFromListInCurrentSchema(String[] arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public Properties getUnMatchedConnectionAttributes() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public String getUserName() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public boolean getUsingXAFlag() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public boolean getXAErrorFlag() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public boolean isDRCPEnabled() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public boolean isLogicalConnection() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public boolean isProxySession() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public boolean isUsable() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public boolean needToPurgeStatementCache() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return false;
    }

    @Override
    public void openProxySession(int arg0, Properties arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void oracleReleaseSavepoint(OracleSavepoint arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void oracleRollback(OracleSavepoint arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public OracleSavepoint oracleSetSavepoint() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public OracleSavepoint oracleSetSavepoint(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public oracle.jdbc.internal.OracleConnection physicalConnectionWithin() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public int pingDatabase() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return 0;
    }

    @Override
    public int pingDatabase(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return 0;
    }

    @Override
    public CallableStatement prepareCallWithKey(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public PreparedStatement prepareStatementWithKey(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public void purgeExplicitCache() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void purgeImplicitCache() throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void putDescriptor(String arg0, Object arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public AQNotificationRegistration[] registerAQNotification(String[] arg0, Properties[] arg1, Properties arg2)
            throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public void registerConnectionCacheCallback(OracleConnectionCacheCallback arg0, Object arg1, int arg2)
            throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public DatabaseChangeRegistration registerDatabaseChangeNotification(Properties arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    @Override
    public void registerSQLType(String arg0, Class arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void registerSQLType(String arg0, String arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void registerTAFCallback(OracleOCIFailover arg0, Object arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void removeLogicalTransactionIdEventListener(LogicalTransactionIdEventListener arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setApplicationContext(String arg0, String arg1, String arg2) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setAutoClose(boolean arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setConnectionReleasePriority(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setCreateStatementAsRefCursor(boolean arg0) {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setDefaultExecuteBatch(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setDefaultRowPrefetch(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setDefaultTimeZone(TimeZone arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setEndToEndMetrics(String[] arg0, short arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setExplicitCachingEnabled(boolean arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setImplicitCachingEnabled(boolean arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setIncludeSynonyms(boolean arg0) {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setPlsqlWarnings(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setRemarksReporting(boolean arg0) {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setRestrictGetTables(boolean arg0) {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setSessionTimeZone(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setStatementCacheSize(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setStmtCacheSize(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setStmtCacheSize(int arg0, boolean arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setUsingXAFlag(boolean arg0) {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setWrapper(OracleConnection arg0) {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void setXAErrorFlag(boolean arg0) {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void shutdown(DatabaseShutdownMode arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void startup(DatabaseStartupMode arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void startup(String arg0, int arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void unregisterAQNotification(AQNotificationRegistration arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void unregisterDatabaseChangeNotification(DatabaseChangeRegistration arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void unregisterDatabaseChangeNotification(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void unregisterDatabaseChangeNotification(long arg0, String arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public void unregisterDatabaseChangeNotification(int arg0, String arg1, int arg2) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
    }

    @Override
    public OracleConnection unwrap() {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //implement?
        //return null;
    }

    
    
    // explicit deligate to internal debs Oracle Connection - might want to cast - generate an ((OracleConnection) new ORestConnection(DebsConnecion)) to pass up
    

        
        /*
         * (non-Javadoc)
         * @see oracle.dbtools.restjdbc.abstractDef.RestJDBCConnection#createStatement()
         */
        @Override
        public Statement createStatement() throws SQLException {
            LogUtil.log();
            //probably want an orest statement
            return new oracle.dbtools.jdbc.orest.ORestStatement(this);
        }
        
        /*
         * (non-Javadoc)
         * @see oracle.dbtools.restjdbc.abstractDef.RestJDBCConnection#prepareStatement(java.lang.String)
         */
        @Override
        public PreparedStatement prepareStatement(String sql) throws SQLException {
            LogUtil.log(); 
            //probably want an orest prepared statement
            return new ORestPreparedStatement(this, sql);//super.prepareStatement(sql);
        }
        
        /**
       * Method to return a connection
       * @return instance of Connection
       */
        public oracle.dbtools.jdbc.Connection getConnection() {
            LogUtil.log();
            return super.getConnection();
        }
        // TODO Add more methods from oracle.dbtools.restjdbc.abstractDef.RestJDBCConnection as we go

        /* Get the rest uri */
        public String getUri() {
            LogUtil.log();
            return super.getUri();
        }
        
        /*
         * Return the Oauth2 token of the current session
         */
        public String getOauth2Token() {
            LogUtil.log();
            return super.getOauth2Token();
        }
        
        @Override
      public void close() throws SQLException {
          
          // Release all the resources
            LogUtil.log();
            super.close();
        }
        
        /**
         * Gets the connection object's current schema name
         * It's the enabled schema
         */
        @Override
      public String getSchema() throws SQLException {   
            LogUtil.log();
            return super.getSchema();
      }
        
        @Override
      public void setSchema(String schema) throws SQLException {
            LogUtil.log();

      }

      @Override
      public <T> T unwrap(Class<T> iface) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.unwrap(iface);
      }

      @Override
      public boolean isWrapperFor(Class<?> iface) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.isWrapperFor(iface);
      }

      /**
       * Creates a <code>CallableStatement</code> object for calling
       * database stored procedures.
       * The <code>CallableStatement</code> object provides
       * methods for setting up its IN and OUT parameters, and
       * methods for executing the call to a stored procedure.
       *
       * <P><B>Note:</B> This method is optimized for handling stored
       * procedure call statements. Some drivers may send the call
       * statement to the database when the method <code>prepareCall</code>
       * is done; others
       * may wait until the <code>CallableStatement</code> object
       * is executed. This has no
       * direct effect on users; however, it does affect which method
       * throws certain SQLExceptions.
       * <P>
       * Result sets created using the returned <code>CallableStatement</code>
       * object will by default be type <code>TYPE_FORWARD_ONLY</code>
       * and have a concurrency level of <code>CONCUR_READ_ONLY</code>.
       * The holdability of the created result sets can be determined by
       * calling {@link #getHoldability}.
       *
       * @param sql an SQL statement that may contain one or more '?'
       * parameter placeholders. Typically this statement is specified using JDBC
       * call escape syntax.
       * @return a new default <code>CallableStatement</code> object containing the
       * pre-compiled SQL statement
       * @exception SQLException if a database access error occurs
       * or this method is called on a closed connection
       */
      @Override
      public CallableStatement prepareCall(String sql) throws SQLException {
          LogUtil.log();
          //probably want an orestcallablestatement
          return new oracle.dbtools.jdbc.orest.ORestCallableStatement(this,sql);
      }

      
      @Override
      public String nativeSQL(String sql) throws SQLException {
        // TODO Auto-generated method stub
          LogUtil.log();
          return super.nativeSQL(sql);
      }

      @Override
      public void setAutoCommit(boolean autoCommit) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.setAutoCommit(autoCommit);
        
      }

      @Override
      public boolean getAutoCommit() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.getAutoCommit();
      }

      @Override
      public void commit() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.commit();
      }

      @Override
      public void rollback() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.rollback();
        
      }

      @Override
      public boolean isClosed() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.isClosed();
      }


      /**
       * Retrieves a <code>DatabaseMetaData</code> object that contains
       * metadata about the database to which this
       * <code>Connection</code> object represents a connection.
       * The metadata includes information about the database's
       * tables, its supported SQL grammar, its stored
       * procedures, the capabilities of this connection, and so on.
       *
       * @return a <code>DatabaseMetaData</code> object for this
       *         <code>Connection</code> object
       * @exception  SQLException if a database access error occurs
       * or this method is called on a closed connection
       */
      @Override
      public DatabaseMetaData getMetaData() throws SQLException {  
          LogUtil.log();
          //probably want to return ORestDatabaseMetaData - if I need to do more...
          return super.getMetaData();
      }

      @Override
      public void setReadOnly(boolean readOnly) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.setReadOnly(readOnly);
        
      }

      @Override
      public boolean isReadOnly() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.isReadOnly();
      }

      @Override
      public void setCatalog(String catalog) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.setCatalog(catalog);
      }

      @Override
      public String getCatalog() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.getCatalog();
      }

      @Override
      public void setTransactionIsolation(int level) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.setTransactionIsolation(level);
        
      }

      @Override
      public int getTransactionIsolation() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.getTransactionIsolation();
      }

      @Override
      public SQLWarning getWarnings() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.getWarnings();
      }

      @Override
      public void clearWarnings() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.clearWarnings();
        
      }

      @Override
      public Statement createStatement(int resultSetType, int resultSetConcurrency)
          throws SQLException {
          LogUtil.log();
        // TODO Auto-generated method stub
        return super.createStatement(resultSetType, resultSetConcurrency);
      }

      @Override
      public PreparedStatement prepareStatement(String sql, int resultSetType,
          int resultSetConcurrency) throws SQLException {
          LogUtil.log();
        return super.prepareStatement(sql, resultSetType, resultSetConcurrency);
      }

      @Override
      public CallableStatement prepareCall(String sql, int resultSetType,
          int resultSetConcurrency) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.prepareCall(sql, resultSetType, resultSetConcurrency);
      }

      @Override
      public Map<String, Class<?>> getTypeMap() throws SQLException {
          LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
      }

      @Override
      public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.setTypeMap(map);
      }

      @Override
      public void setHoldability(int holdability) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.setHoldability(holdability);
      }

      @Override
      public int getHoldability() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.getHoldability();
      }

      @Override
      public Savepoint setSavepoint() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.setSavepoint();
      }

      @Override
      public Savepoint setSavepoint(String name) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.setSavepoint(name);
      }

      @Override
      public void rollback(Savepoint savepoint) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.rollback();
        
      }

      @Override
      public void releaseSavepoint(Savepoint savepoint) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        //implement?
          super.releaseSavepoint(savepoint);
      }

      @Override
      public Statement createStatement(int resultSetType, int resultSetConcurrency,
          int resultSetHoldability) throws SQLException {
        // TODO Auto-generated method stub
          LogUtil.log();
          return super.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
      }

      @Override
      public PreparedStatement prepareStatement(String sql, int resultSetType,
          int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        // TODO Auto-generated method stub
          LogUtil.log();
          return  super.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
      }

      @Override
      public CallableStatement prepareCall(String sql, int resultSetType,
          int resultSetConcurrency, int resultSetHoldability) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
      }

      @Override
      public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys)
          throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        // TODO2 return ORestPreparedStatement
        return super.prepareStatement(sql, autoGeneratedKeys);
      }

      @Override
      public PreparedStatement prepareStatement(String sql, int[] columnIndexes)
          throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        // TODO2 return ORestPreparedStatement
        return super.prepareStatement(sql, columnIndexes);
      }

      @Override
      public PreparedStatement prepareStatement(String sql, String[] columnNames)
          throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method 
          // TODO2 return ORestPreparedStatementHelper(super.prepareStatement(sql, columnNames));
        return super.prepareStatement(sql, columnNames);
      }

      @Override
      public Clob createClob() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.createClob();
      }

      @Override
      public Blob createBlob() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.createBlob();
      }

      @Override
      public NClob createNClob() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.createNClob();
      }

      @Override
      public SQLXML createSQLXML() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.createSQLXML();
      }

      @Override
      public boolean isValid(int timeout) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.isValid(timeout);
      }

      @Override
      public void setClientInfo(String name, String value)
          throws SQLClientInfoException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.setClientInfo(name, value);
        
      }

      @Override
      public void setClientInfo(Properties properties)
          throws SQLClientInfoException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.setClientInfo(properties);
      }

      @Override
      public String getClientInfo(String name) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.getClientInfo(name);
      }

      @Override
      public Properties getClientInfo() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.getClientInfo();
      }

      @Override
      public Array createArrayOf(String typeName, Object[] elements)
          throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.createArrayOf(typeName, elements);
      }

      @Override
      public Struct createStruct(String typeName, Object[] attributes)
          throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
        return super.createStruct(typeName, attributes);
      }

      @Override
      public void abort(Executor executor) throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.abort(executor);
        
      }

      @Override
      public void setNetworkTimeout(Executor executor, int milliseconds)
          throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          super.setNetworkTimeout(executor, milliseconds);
        
      }

      @Override
      public int getNetworkTimeout() throws SQLException {
          LogUtil.log();
          // TODO Auto-generated method stub
          //should I go new ORestSQLException(getencapsulated.getNetworkTimeout())?
          //see if I can get away without it for now....
        return super.getNetworkTimeout();
      }

	@Override
	public TIMESTAMP createTIMESTAMP(Timestamp arg0, Calendar arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TIMESTAMPTZ createTIMESTAMPTZ(Timestamp arg0, ZoneId arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDRCPPLSQLCallbackName() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDRCPReturnTag() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DRCPState getDRCPState() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isDRCPMultitagEnabled() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setShardingKey(OracleShardingKey arg0, OracleShardingKey arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean setShardingKeyIfValid(OracleShardingKey arg0, OracleShardingKey arg1, int arg2) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void startup(DatabaseStartupMode arg0, String arg1) throws SQLException {
		// TODO Auto-generated method stub
		
	}
}
