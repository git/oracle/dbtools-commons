/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.orest;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Calendar;

import org.apache.http.client.methods.CloseableHttpResponse;

import oracle.dbtools.jdbc.PreparedStatement;
import oracle.dbtools.jdbc.util.ExceptionUtil;
import oracle.dbtools.jdbc.util.LogUtil;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.dbtools.jdbc.util.SQLStateMapping;
import oracle.jdbc.OracleParameterMetaData;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.dcn.DatabaseChangeRegistration;
import oracle.sql.ARRAY;
import oracle.sql.BFILE;
import oracle.sql.BINARY_DOUBLE;
import oracle.sql.BINARY_FLOAT;
import oracle.sql.BLOB;
import oracle.sql.CHAR;
import oracle.sql.CLOB;
import oracle.sql.CustomDatum;
import oracle.sql.DATE;
import oracle.sql.Datum;
import oracle.sql.INTERVALDS;
import oracle.sql.INTERVALYM;
import oracle.sql.NUMBER;
import oracle.sql.OPAQUE;
import oracle.sql.ORAData;
import oracle.sql.RAW;
import oracle.sql.REF;
import oracle.sql.ROWID;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import oracle.sql.TIMESTAMP;
import oracle.sql.TIMESTAMPLTZ;
import oracle.sql.TIMESTAMPTZ;

public class ORestPreparedStatement extends PreparedStatement implements OraclePreparedStatement {
	/* Declare the logger */
	Logger LOGGER = Logger.getLogger(oracle.dbtools.jdbc.orest.ORestPreparedStatement.class.getName());
	protected final ORestPreparedStatementImpl impl;
    
    public ORestPreparedStatement() {
        super();
        LogUtil.log();
        impl= new ORestPreparedStatementImpl(this);

        // TODOAuto-generated constructor stub
    }

    public ORestPreparedStatement(oracle.dbtools.jdbc.Connection conn, String sql) {
        super(conn, sql);
        this.conn = conn;
        LogUtil.log();
        impl= new ORestPreparedStatementImpl(this);
        // TODOAuto-generated constructor stub
    }

    @Override
    public void clearDefines() throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void closeWithKey(String arg0) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public int creationState() {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return 0;
    }

    @Override
    public void defineColumnType(int arg0, int arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void defineColumnType(int arg0, int arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void defineColumnType(int arg0, int arg1, String arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void defineColumnType(int arg0, int arg1, int arg2, short arg3) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void defineColumnTypeBytes(int arg0, int arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void defineColumnTypeChars(int arg0, int arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public int getLobPrefetchSize() throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return 0;
    }

    @Override
    public long getRegisteredQueryId() throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return 0;
    }

    @Override
    public String[] getRegisteredTableNames() throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public int getRowPrefetch() {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return 0;
    }

    @Override
    public boolean isNCHAR(int arg0) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return false;
    }

    @Override
    public void setDatabaseChangeRegistration(DatabaseChangeRegistration arg0) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setLobPrefetchSize(int arg0) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setRowPrefetch(int arg0) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public OracleParameterMetaData OracleGetParameterMetaData() throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public void defineParameterType(int arg0, int arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void defineParameterTypeBytes(int arg0, int arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void defineParameterTypeChars(int arg0, int arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public int getExecuteBatch() {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return 0;
    }

    @Override
    public ResultSet getReturnResultSet() throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public void registerReturnParameter(int arg0, int arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void registerReturnParameter(int arg0, int arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void registerReturnParameter(int arg0, int arg1, String arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public int sendBatch() throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return 0;
    }

    @Override
    public void setARRAY(int arg0, ARRAY arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setARRAYAtName(String arg0, ARRAY arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setArrayAtName(String arg0, Array arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setAsciiStreamAtName(String arg0, InputStream arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setAsciiStreamAtName(String arg0, InputStream arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setAsciiStreamAtName(String arg0, InputStream arg1, long arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBFILE(int arg0, BFILE arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBFILEAtName(String arg0, BFILE arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBLOB(int arg0, BLOB arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBLOBAtName(String arg0, BLOB arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBfile(int arg0, BFILE arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBfileAtName(String arg0, BFILE arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBigDecimalAtName(String arg0, BigDecimal arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryDouble(int arg0, double arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryDouble(int arg0, BINARY_DOUBLE arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryDoubleAtName(String arg0, double arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryDoubleAtName(String arg0, BINARY_DOUBLE arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryFloat(int arg0, float arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryFloat(int arg0, BINARY_FLOAT arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryFloatAtName(String arg0, float arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryFloatAtName(String arg0, BINARY_FLOAT arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryStreamAtName(String arg0, InputStream arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryStreamAtName(String arg0, InputStream arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBinaryStreamAtName(String arg0, InputStream arg1, long arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBlobAtName(String arg0, Blob arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBlobAtName(String arg0, InputStream arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBlobAtName(String arg0, InputStream arg1, long arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBooleanAtName(String parameterName, boolean x) throws SQLException {
      LogUtil.log();// TODOAuto-generated method stub
       
      LOGGER.finest("Setting Boolean");
      updateList(parameterName,  x);
      types.put(parameterName, "BOOLEAN");

    }

    @Override
    public void setByteAtName(String arg0, byte arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBytesAtName(String arg0, byte[] arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBytesForBlob(int arg0, byte[] arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setBytesForBlobAtName(String arg0, byte[] arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCHAR(int arg0, CHAR arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCHARAtName(String arg0, CHAR arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCLOB(int arg0, CLOB arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCLOBAtName(String arg0, CLOB arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCharacterStreamAtName(String arg0, Reader arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCharacterStreamAtName(String arg0, Reader arg1, long arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCheckBindTypes(boolean arg0) {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setClobAtName(String arg0, Clob arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setClobAtName(String arg0, Reader arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setClobAtName(String arg0, Reader arg1, long arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCursor(int arg0, ResultSet arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCursorAtName(String arg0, ResultSet arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCustomDatum(int arg0, CustomDatum arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setCustomDatumAtName(String arg0, CustomDatum arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setDATE(int arg0, DATE arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setDATEAtName(String arg0, DATE arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setDateAtName(String arg0, Date arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setDateAtName(String arg0, Date arg1, Calendar arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setDisableStmtCaching(boolean arg0) {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setDoubleAtName(String parameterName, double x) throws SQLException {
      LogUtil.log();// TODOAuto-generated method stub
      LOGGER.finest("Setting Double");
      updateList(parameterName,  x);
      types.put(parameterName, "DOUBLE");    

    }

    @Override
    public void setExecuteBatch(int arg0) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setFixedCHAR(int arg0, String arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setFixedCHARAtName(String arg0, String arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setFloatAtName(String parameterName, float x) throws SQLException {
      LogUtil.log();// TODOAuto-generated method stub
      
      LOGGER.finest("Setting Float");
      updateList(parameterName,  x);
      types.put(parameterName, "FLOAT");   

    }

    @Override
    public void setFormOfUse(int arg0, short arg1) {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setINTERVALDS(int arg0, INTERVALDS arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setINTERVALDSAtName(String arg0, INTERVALDS arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setINTERVALYM(int arg0, INTERVALYM arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setINTERVALYMAtName(String arg0, INTERVALYM arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setIntAtName(String parameterName, int x) throws SQLException {
      LogUtil.log();// TODOAuto-generated method stub
      LOGGER.finest("Setting Int");
      updateList(parameterName,  x);
      types.put(parameterName, "INTEGER");  
    }

    @Override
    public void setLongAtName(String parameterName, long x) throws SQLException {
      LogUtil.log();// TODOAuto-generated method stub
       
      LOGGER.finest("Setting Long");
      updateList(parameterName,  x);
      types.put(parameterName, "NUMBER");

    }

    @Override
    public void setNCharacterStreamAtName(String arg0, Reader arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setNCharacterStreamAtName(String arg0, Reader arg1, long arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setNClobAtName(String arg0, NClob arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setNClobAtName(String arg0, Reader arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setNClobAtName(String arg0, Reader arg1, long arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setNStringAtName(String arg0, String arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setNUMBER(int arg0, NUMBER arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setNUMBERAtName(String arg0, NUMBER arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setNullAtName(String arg0, int arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setNullAtName(String arg0, int arg1, String arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setOPAQUE(int arg0, OPAQUE arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setOPAQUEAtName(String arg0, OPAQUE arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setORAData(int arg0, ORAData arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setORADataAtName(String arg0, ORAData arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setObjectAtName(String parameterName, Object x) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       
     // The simple data types
        if(x instanceof Boolean) {
          setBooleanAtName(parameterName, (boolean) x);
        }
        else if(x instanceof Integer) {
          setIntAtName(parameterName, (int) x);
        }
        else if(x instanceof Short) {
          setShortAtName(parameterName, (short) x);
        }
        else if(x instanceof Long) {
          setLongAtName(parameterName, (long) x);
        }
        else if(x instanceof Float) {
          setFloatAtName(parameterName, (float) x);
        }
        else if(x instanceof Double) {
          setDoubleAtName(parameterName, (double) x);
        }
        else if(x instanceof String) {
          setStringAtName(parameterName, (String) x);
        }
        else if(x instanceof java.sql.Date) {
          setDateAtName(parameterName, (Date) x);
        }
        else if(x instanceof java.sql.Time) {
          setTimeAtName(parameterName, (Time) x);
        }
        else if(x instanceof java.sql.Timestamp) {
          setTimestampAtName(parameterName, (Timestamp) x);
        }
        else if(x instanceof BigDecimal) {
          setBigDecimalAtName(parameterName, (BigDecimal) x);
        }
    }

    @Override
    public void setObjectAtName(String arg0, Object arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setObjectAtName(String arg0, Object arg1, int arg2, int arg3) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setOracleObject(int arg0, Datum arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setOracleObjectAtName(String arg0, Datum arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setPlsqlIndexTable(int arg0, Object arg1, int arg2, int arg3, int arg4, int arg5) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setRAW(int arg0, RAW arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setRAWAtName(String arg0, RAW arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setREF(int arg0, REF arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setREFAtName(String arg0, REF arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setROWID(int arg0, ROWID arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setROWIDAtName(String arg0, ROWID arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setRefAtName(String arg0, Ref arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setRefType(int arg0, REF arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setRefTypeAtName(String arg0, REF arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setRowIdAtName(String arg0, RowId arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setSQLXMLAtName(String arg0, SQLXML arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setSTRUCT(int arg0, STRUCT arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setSTRUCTAtName(String arg0, STRUCT arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setShortAtName(String parameterName, short x) throws SQLException {
      LogUtil.log();// TODOAuto-generated method stub
       
      LOGGER.finest("Setting Short");
      updateList(parameterName,  x);
      types.put(parameterName, "SHORT");   
    }

    @Override
    public void setStringAtName(String parameterName, String x) throws SQLException {
      LogUtil.log();// TODOAuto-generated method stub
      LOGGER.finest("Setting String");
      updateList(parameterName,  x);
      types.put(parameterName, "VARCHAR2");   
    }

    @Override
    public void setStringForClob(int arg0, String arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setStringForClobAtName(String arg0, String arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setStructDescriptor(int arg0, StructDescriptor arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setStructDescriptorAtName(String arg0, StructDescriptor arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setTIMESTAMP(int arg0, TIMESTAMP arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setTIMESTAMPAtName(String arg0, TIMESTAMP arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setTIMESTAMPLTZ(int arg0, TIMESTAMPLTZ arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setTIMESTAMPLTZAtName(String arg0, TIMESTAMPLTZ arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setTIMESTAMPTZ(int arg0, TIMESTAMPTZ arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setTIMESTAMPTZAtName(String arg0, TIMESTAMPTZ arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setTimeAtName(String arg0, Time arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setTimeAtName(String arg0, Time arg1, Calendar arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setTimestampAtName(String arg0, Timestamp arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setTimestampAtName(String arg0, Timestamp arg1, Calendar arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setURLAtName(String arg0, URL arg1) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    @Override
    public void setUnicodeStreamAtName(String arg0, InputStream arg1, int arg2) throws SQLException {
        LogUtil.log();// TODOAuto-generated method stub
       throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

    }

    // Base implementation replaced by log and super 21 September 2016
    /*
     * LogUtil.log(); super.xxx()
     */

    @Override
    public int[] executeBatch() throws SQLException {
        LogUtil.log();
        return super.executeBatch();
    }

    @Override
    public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
        LogUtil.log();
        return super.executeUpdate(sql, autoGeneratedKeys);
    }

    @Override
    public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
        LogUtil.log();
        return super.executeUpdate(sql, columnIndexes);
    }

    @Override
    public int executeUpdate(String sql, String[] columnNames) throws SQLException {
        LogUtil.log();
        return super.executeUpdate(sql, columnNames);
    }

    @Override
    public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
        LogUtil.log();
        return super.execute(sql, autoGeneratedKeys);
    }

    @Override
    public boolean execute(String sql, int[] columnIndexes) throws SQLException {
        LogUtil.log();
        return super.execute(sql, columnIndexes);
    }

    @Override
    public boolean execute(String sql, String[] columnNames) throws SQLException {
        LogUtil.log();
        return super.execute(sql, columnNames);
    }

    @Override
    public int executeUpdate() throws SQLException {
        LogUtil.log();
        return super.executeUpdate();
    }

    @Override
    public boolean execute() throws SQLException {
        LogUtil.log();
        return super.execute();
    }

    /**
     * Close the PreparedStatement
     */
    @Override
    public void close() throws SQLException {
        LogUtil.log();
        super.close();
        ;
    }

    @Override
    public int getMaxFieldSize() throws SQLException {
        LogUtil.log();
        return super.getMaxFieldSize();
    }

    @Override
    public void setMaxFieldSize(int max) throws SQLException {
        LogUtil.log();
        super.setMaxFieldSize(max);
    }

    @Override
    public void setEscapeProcessing(boolean enable) throws SQLException {
        LogUtil.log();
        super.setEscapeProcessing(enable);
    }

    @Override
    public int getQueryTimeout() throws SQLException {
        LogUtil.log();
        return super.getQueryTimeout();
    }

    @Override
    public void setQueryTimeout(int seconds) throws SQLException {
        LogUtil.log();
        super.setQueryTimeout(seconds);
    }

    @Override
    public void cancel() throws SQLException {
        LogUtil.log();
        super.cancel();
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        LogUtil.log();
        return super.getWarnings();
    }

    @Override
    public void clearWarnings() throws SQLException {
        LogUtil.log();
        super.clearWarnings();
    }

    @Override
    public void setCursorName(String name) throws SQLException {
        LogUtil.log();
        super.setCursorName(name);
    }

    /**
     * Returns the current ResultSet
     */
    @Override
    public ResultSet getResultSet() throws SQLException {
        LogUtil.log();
	ResultSet rs=super.getResultSet();
	if ((rs!=null)&&(!(rs instanceof ORestResultSet))) {
	    LOGGER.log(Level.SEVERE, ORestjdbcResources.format(ORestjdbcResources.ORESTJDBC_TYPEMISMATCH, ORestResultSet.class.getName(), rs.getClass().getName()));
	}
        return rs;
    }

    @Override
    public int getUpdateCount() throws SQLException {
        LogUtil.log();
        return super.getUpdateCount();
    }

    @Override
    public boolean getMoreResults() throws SQLException {
        LogUtil.log();
        return super.getMoreResults();
    }

    @Override
    public void setFetchDirection(int direction) throws SQLException {
        LogUtil.log();
        super.setFetchDirection(direction);
        ;
    }

    /**
     * Retrieves the direction for fetching rows from database tables that is
     * the default for result sets generated from this <code>Statement</code>
     * object.
     * 
     * Since Oracle database only supports forward only cursor. This method
     * always return FETCH_FORWARD.
     * 
     * @return the default fetch direction for result sets generated from this
     *         PreparedStatement object
     * @exception SQLException
     *                if a database access error occurs
     */

    @Override
    public int getFetchDirection() throws SQLException {
        LogUtil.log();
        return super.getFetchDirection();
    }

    @Override
    public int getResultSetConcurrency() throws SQLException {
        LogUtil.log();
        return super.getResultSetConcurrency();
    }

    @Override
    public int getResultSetType() throws SQLException {
        LogUtil.log();
        return super.getResultSetType();
    }

    @Override
    public void addBatch(String sql) throws SQLException {
        LogUtil.log();
        super.addBatch(sql);
    }

    @Override
    public void clearBatch() throws SQLException {
        LogUtil.log();
        super.clearBatch();
    }

    /**
     * Returns the current connection object if the PreparedStatement is still
     * open
     */
    @Override
    public Connection getConnection() throws SQLException {
        LogUtil.log();
        return super.getConnection();
    }

    @Override
    public boolean getMoreResults(int current) throws SQLException {
        LogUtil.log();
        return super.getMoreResults(current);
    }

    @Override
    public ResultSet getGeneratedKeys() throws SQLException {
        LogUtil.log();
        return super.getGeneratedKeys();
    }

    @Override
    public int getResultSetHoldability() throws SQLException {
        LogUtil.log();
        return super.getResultSetHoldability();
    }

    /**
     * Is the PreparedStatement closed?
     */
    @Override
    public boolean isClosed() throws SQLException {
        LogUtil.log();
        return super.isClosed();
    }

    @Override
    public void setPoolable(boolean poolable) throws SQLException {
        LogUtil.log();
        super.setPoolable(poolable);
        ;
    }

    @Override
    public boolean isPoolable() throws SQLException {
        LogUtil.log();
        return super.isPoolable();
    }

    @Override
    public void closeOnCompletion() throws SQLException {
        LogUtil.log();
        super.closeOnCompletion();
        ;
    }

    @Override
    public boolean isCloseOnCompletion() throws SQLException {
        LogUtil.log();
        return super.isCloseOnCompletion();
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        LogUtil.log();
        return super.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        LogUtil.log();
        return super.isWrapperFor(iface);
    }

    /**
     * All the setMethods and helpers
     ***************************************************************/

    /**
     * Helper method to iterate through the Hashmaps
     */

    /**
     * Helper method to process Sets Useful for duplicate sets --> only need to
     * record the last one This generates the entire POST
     * 
     * @throws SQLException
     */
    @Override
    public void setNull(int parameterIndex, int sqlType) throws SQLException {
        LogUtil.log();
        super.setNull(parameterIndex, sqlType);
        ;
    }

    @Override
    public void setBoolean(int parameterIndex, boolean x) throws SQLException {
        LogUtil.log();
        super.setBoolean(parameterIndex, x);
        ;
    }

    @Override
    public void setByte(int parameterIndex, byte x) throws SQLException {
        LogUtil.log();
        super.setByte(parameterIndex, x);
        ;
    }

    @Override
    public void setShort(int parameterIndex, short x) throws SQLException {
        LogUtil.log();
        super.setShort(parameterIndex, x);
        ;
    }

    @Override
    public void setInt(int parameterIndex, int x) throws SQLException {
        LogUtil.log();
        super.setInt(parameterIndex, x);
    }

    @Override
    public void setLong(int parameterIndex, long x) throws SQLException {
        LogUtil.log();
        super.setLong(parameterIndex, x);
    }

    @Override
    public void setFloat(int parameterIndex, float x) throws SQLException {
        LogUtil.log();
        super.setFloat(parameterIndex, x);
    }

    @Override
    public void setDouble(int parameterIndex, double x) throws SQLException {
        LogUtil.log();
        super.setDouble(parameterIndex, x);
    }

    @Override
    public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
        LogUtil.log();
        super.setBigDecimal(parameterIndex, x);
    }

    @Override
    public void setString(int parameterIndex, String x) throws SQLException {
        LogUtil.log();
        super.setString(parameterIndex, x);
    }

    @Override
    public void setBytes(int parameterIndex, byte[] x) throws SQLException {
        LogUtil.log();
        super.setBytes(parameterIndex, x);
    }

    /**
     * Posts a pre-formatted date using dbtools.raptor.datatypes
     */
    @Override
    public void setDate(int parameterIndex, Date x) throws SQLException {
        LogUtil.log();
        super.setDate(parameterIndex, x);
    }

    /**
     * Posts a pre-formatted time using dbtools.raptor.datatypes
     */
    @Override
    public void setTime(int parameterIndex, Time x) throws SQLException {
        LogUtil.log();
        super.setTime(parameterIndex, x);
    }

    /**
     * Posts a pre-formatted timestamp using dbtools.raptor.datatypes
     */
    @Override
    public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
        LogUtil.log();
        super.setTimestamp(parameterIndex, x);
    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {

        LogUtil.log();
        super.setAsciiStream(parameterIndex, x, length);
    }

    @Override
    public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
        LogUtil.log();
        super.setUnicodeStream(parameterIndex, x, length);
    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
        LogUtil.log();
        super.setBinaryStream(parameterIndex, x, length);
    }

    @Override
    public void clearParameters() throws SQLException {
        LogUtil.log();
        super.clearParameters();
    }

    @Override
    public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
        LogUtil.log();
        super.setObject(parameterIndex, x, targetSqlType);
    }

    /**
     * <p>
     * Sets the value of the designated parameter using the given object.
     *
     * <p>
     * The JDBC specification specifies a standard mapping from Java
     * <code>Object</code> types to SQL types. The given argument will be
     * converted to the corresponding SQL type before being sent to the
     * database.
     *
     * <p>
     * Note that this method may be used to pass datatabase- specific abstract
     * data types, by using a driver-specific Java type.
     *
     * If the object is of a class implementing the interface
     * <code>SQLData</code>, the JDBC driver should call the method
     * <code>SQLData.writeSQL</code> to write it to the SQL data stream. If, on
     * the other hand, the object is of a class implementing <code>Ref</code>,
     * <code>Blob</code>, <code>Clob</code>, <code>NClob</code>,
     * <code>Struct</code>, <code>java.net.URL</code>, <code>RowId</code>,
     * <code>SQLXML</code> or <code>Array</code>, the driver should pass it to
     * the database as a value of the corresponding SQL type.
     * <P>
     * <b>Note:</b> Not all databases allow for a non-typed Null to be sent to
     * the backend. For maximum portability, the <code>setNull</code> or the
     * <code>setObject(int parameterIndex, Object x, int sqlType)</code> method
     * should be used instead of
     * <code>setObject(int parameterIndex, Object x)</code>.
     * <p>
     * <b>Note:</b> This method throws an exception if there is an ambiguity,
     * for example, if the object is of a class implementing more than one of
     * the interfaces named above.
     *
     * @param parameterIndex
     *            the first parameter is 1, the second is 2, ...
     * @param x
     *            the object containing the input parameter value
     * @exception SQLException
     *                if parameterIndex does not correspond to a parameter
     *                marker in the SQL statement; if a database access error
     *                occurs; this method is called on a closed
     *                <code>PreparedStatement</code> or the type of the given
     *                object is ambiguous
     */
    @Override
    public void setObject(int parameterIndex, Object x) throws SQLException {
        LogUtil.log();
        super.setObject(parameterIndex, x);
    }

    @Override
    public void addBatch() throws SQLException {
        LogUtil.log();
        super.addBatch();
    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
        LogUtil.log();
        super.setCharacterStream(parameterIndex, reader, length);
    }

    @Override
    public void setRef(int parameterIndex, Ref x) throws SQLException {
        LogUtil.log();
        super.setRef(parameterIndex, x);
    }

    @Override
    public void setBlob(int parameterIndex, Blob x) throws SQLException {
        LogUtil.log();
        super.setBlob(parameterIndex, x);
    }

    @Override
    public void setClob(int parameterIndex, Clob x) throws SQLException {
        LogUtil.log();
        super.setClob(parameterIndex, x);
    }

    @Override
    public void setArray(int parameterIndex, Array x) throws SQLException {
        LogUtil.log();
        super.setArray(parameterIndex, x);
    }

    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        LogUtil.log();
        return super.getMetaData();
    }

    @Override
    public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
        LogUtil.log();
        super.setDate(parameterIndex, x, cal);
    }

    @Override
    public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
        LogUtil.log();
        super.setTime(parameterIndex, x, cal);
    }

    @Override
    public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
        LogUtil.log();
        super.setTimestamp(parameterIndex, x, cal);
    }

    @Override
    public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
        LogUtil.log();
        super.setNull(parameterIndex, sqlType, typeName);
    }

    @Override
    public void setURL(int parameterIndex, URL x) throws SQLException {
        LogUtil.log();
        super.setURL(parameterIndex, x);
    }

    @Override
    public ParameterMetaData getParameterMetaData() throws SQLException {
        LogUtil.log();
        return super.getParameterMetaData();
    }

    @Override
    public void setRowId(int parameterIndex, RowId x) throws SQLException {
        LogUtil.log();
        super.setRowId(parameterIndex, x);
    }

    @Override
    public void setNString(int parameterIndex, String value) throws SQLException {
        LogUtil.log();
        super.setNString(parameterIndex, value);
    }

    @Override
    public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
        LogUtil.log();
        super.setNCharacterStream(parameterIndex, value, length);
    }

    @Override
    public void setNClob(int parameterIndex, NClob value) throws SQLException {
        LogUtil.log();
        super.setNClob(parameterIndex, value);
    }

    @Override
    public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
        LogUtil.log();
        super.setClob(parameterIndex, reader, length);
    }

    @Override
    public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
        LogUtil.log();
        super.setBlob(parameterIndex, inputStream, length);
    }

    @Override
    public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
        LogUtil.log();
        super.setNClob(parameterIndex, reader, length);
    }

    @Override
    public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
        LogUtil.log();
        super.setSQLXML(parameterIndex, xmlObject);
    }

    @Override
    public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {

        LogUtil.log();
        super.setObject(parameterIndex, x, targetSqlType, scaleOrLength);
    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
        LogUtil.log();
        super.setAsciiStream(parameterIndex, x, length);
    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
        LogUtil.log();
        super.setBinaryStream(parameterIndex, x, length);
    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
        LogUtil.log();
        super.setCharacterStream(parameterIndex, reader, length);
    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
        LogUtil.log();
        super.setAsciiStream(parameterIndex, x);
    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
        LogUtil.log();
        super.setBinaryStream(parameterIndex, x);
    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
        LogUtil.log();
        super.setCharacterStream(parameterIndex, reader);
    }

    @Override
    public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
        LogUtil.log();
        super.setNCharacterStream(parameterIndex, value);
    }

    @Override
    public void setClob(int parameterIndex, Reader reader) throws SQLException {
        LogUtil.log();
        super.setClob(parameterIndex, reader);
    }

    @Override
    public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
        LogUtil.log();
        super.setBlob(parameterIndex, inputStream);
    }

    @Override
    public void setNClob(int parameterIndex, Reader reader) throws SQLException {
        LogUtil.log();
        super.setNClob(parameterIndex, reader);
    }
  //return orest result set:
  	@Override
  	public ResultSet executeQuery(String sql) throws SQLException {	
  	    CloseableHttpResponse response = orestExecuteInternal(sql);
  	  	
  		this.setCurrentResultSet(new oracle.dbtools.jdbc.orest.ORestResultSet(response, (oracle.dbtools.jdbc.PreparedStatement)this, this.getConn()));		
  		return this.getCurrentResultSet();
  	}
  	@Override
  	public ResultSet executeQuery() throws SQLException {	
  	    CloseableHttpResponse response = orestExecuteInternal(this.sql);
  	  	
  		this.setCurrentResultSet(new oracle.dbtools.jdbc.orest.ORestResultSet(response, (oracle.dbtools.jdbc.PreparedStatement) this, this.getConn(),this.sql));		
  		return this.getCurrentResultSet();
  	}
}
    
