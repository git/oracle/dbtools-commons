/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.orest;
//imports from base driver
 
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.xml.bind.DatatypeConverter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import oracle.dbtools.http.Session;
import oracle.dbtools.http.SessionException;
import oracle.dbtools.jdbc.util.ExceptionUtil;
import oracle.dbtools.jdbc.util.LogUtil;
import oracle.dbtools.jdbc.util.OracleTypes;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.dbtools.raptor.backgroundTask.Messages;
import oracle.dbtools.raptor.datatypes.DataType;
import oracle.dbtools.raptor.datatypes.DataTypeFactory;
import oracle.dbtools.raptor.datatypes.TypeMetadata;
import oracle.dbtools.raptor.datatypes.TypeMetadata.Attribute;
import oracle.dbtools.raptor.datatypes.ValueType;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialClob;
//end of imports from base driver


import oracle.dbtools.jdbc.CallableStatement;
import oracle.dbtools.jdbc.Connection;
import oracle.dbtools.jdbc.PreparedStatement;
import oracle.dbtools.jdbc.ResultSet;
import oracle.dbtools.jdbc.ResultSetMetadata;
import oracle.jdbc.OracleDataFactory;
import oracle.jdbc.OracleResultSet;
import oracle.sql.ARRAY;
import oracle.sql.BFILE;
import oracle.sql.BLOB;
import oracle.sql.CHAR;
import oracle.sql.CLOB;
import oracle.sql.CustomDatum;
import oracle.sql.CustomDatumFactory;
import oracle.sql.DATE;
import oracle.sql.Datum;
import oracle.sql.INTERVALDS;
import oracle.sql.INTERVALYM;
import oracle.sql.NUMBER;
import oracle.sql.OPAQUE;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.RAW;
import oracle.sql.REF;
import oracle.sql.ROWID;
import oracle.sql.STRUCT;
import oracle.sql.TIMESTAMP;
import oracle.sql.TIMESTAMPLTZ;
import oracle.sql.TIMESTAMPTZ;

public class ORestResultSet extends ResultSet implements OracleResultSet {

    public ORestResultSet(CloseableHttpResponse response, oracle.dbtools.jdbc.Statement stmt, Connection conn) throws SQLException {
        super(response, stmt, conn);
        LogUtil.log();// TODO Auto-generated constructor stub
    }

    public ORestResultSet(CloseableHttpResponse response, oracle.dbtools.jdbc.PreparedStatement stmt, Connection conn, String sql) throws SQLException {
        super(response, stmt, conn, sql);
        LogUtil.log();// TODO Auto-generated constructor stub
    }

    public ORestResultSet(CloseableHttpResponse response,  oracle.dbtools.jdbc.CallableStatement stmt, Connection conn) throws SQLException {
        super(response, stmt, conn);
        LogUtil.log();// TODO Auto-generated constructor stub
    }

    public ORestResultSet(InputStream is,  oracle.dbtools.jdbc.CallableStatement stmt) throws SQLException {
        super(is, stmt);
        LogUtil.log();// TODO Auto-generated constructor stub
    }

    @Override
    public ARRAY getARRAY(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
	throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public ARRAY getARRAY(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
	//return null;
    }

    @Override
    public AuthorizationIndicator getAuthorizationIndicator(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
	throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
								 new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
	//return null;
    }

    @Override
    public AuthorizationIndicator getAuthorizationIndicator(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public BFILE getBFILE(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public BFILE getBFILE(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public BLOB getBLOB(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public BLOB getBLOB(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public BFILE getBfile(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public BFILE getBfile(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public CHAR getCHAR(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        return (CHAR) getDatum(getObject(arg0), arg0);
    }

    @Override
    public CHAR getCHAR(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        return (CHAR) getDatum(getObject(arg0), findColumn(arg0));
    }

    @Override
    public CLOB getCLOB(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        return (CLOB) getDatum(getObject(arg0),arg0);
    }

    @Override
    public CLOB getCLOB(String arg0) throws SQLException {
        LogUtil.log();
        return (CLOB) getDatum(getObject(arg0), this.findColumn(arg0));
    }
    
    @Override
    public java.sql.ResultSet getCursor(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public java.sql.ResultSet getCursor(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public CustomDatum getCustomDatum(int arg0, CustomDatumFactory arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public CustomDatum getCustomDatum(String arg0, CustomDatumFactory arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public DATE getDATE(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public DATE getDATE(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public INTERVALDS getINTERVALDS(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public INTERVALDS getINTERVALDS(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public INTERVALYM getINTERVALYM(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public INTERVALYM getINTERVALYM(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public NUMBER getNUMBER(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        return (NUMBER) getDatum(getObject(arg0),arg0);
    }

    @Override
    public NUMBER getNUMBER(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        return (NUMBER) getDatum(getObject(arg0), this.findColumn(arg0));
    }

    @Override
    public OPAQUE getOPAQUE(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public OPAQUE getOPAQUE(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public ORAData getORAData(int arg0, ORADataFactory arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public ORAData getORAData(String arg0, ORADataFactory arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public Object getObject(int arg0, OracleDataFactory arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public Object getObject(String arg0, OracleDataFactory arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public Datum getOracleObject(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        return getDatum(getObject(arg0),arg0);
    }

    @Override
    public Datum getOracleObject(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        
        return getDatum(getObject(arg0), findColumn(arg0));
    }
    //helper method get datum handle string int number for now log and throw sqlexception otherwise
    private Datum getDatum(Object o, int arg0) throws SQLException {
        LogUtil.log(null,"get datum on:"+(o==null?"null":o.getClass().getName()),null);
    	if (o==null) {
    	    return null;
    	}
    
    	DataTypeFactory dTFact = DataTypeFactory.getInstance();
    	//cach this somewhere?
    	ResultSetMetaData rMeta=this.getMetaData();
    	Map<Attribute,Object> tM=new HashMap<Attribute,Object>();
    	tM.put(Attribute.TYPE_CODE, rMeta.getColumnType(arg0));
    
    	tM.put(Attribute.DATA_TYPE, rMeta.getColumnTypeName(arg0));
    	TypeMetadata tMeta = dTFact.getTypeMetadata(tM);//(rMeta.getColumnType(arg0), rMeta.getColumnTypeName(arg0), rMeta.getPrecision(arg0), rMeta.getScale(arg0));
    	DataType dTObj = dTFact.getDataType(this.getStatement().getConnection(), tMeta);
    	Object maybeDatum= dTObj.getDataValue(o).getTypedValue(ValueType.DATUM);
    	if ((maybeDatum != null ) &&(!(maybeDatum instanceof Datum))) {
    	    String oString="null";  //$NON-NLS-1$ 
    	    if (o!=null) {
    		oString=o.getClass().getName();
    	    }
    	    throw new UnsupportedOperationException
    		(ORestjdbcResources.format
    		 (ORestjdbcResources.ORESTJDBC_DATATYPECONVERSION, 
    		  new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName(), 
    		  oString, Datum.class.getName())); 
    	}
    	return (Datum) maybeDatum;
    }
    
    private boolean matchesNumber(Object o) {
        return ((o instanceof Integer)||
        		(o instanceof Long)||
        		(o instanceof BigDecimal)||
        		(o instanceof Float)||
        		(o instanceof Double));
    }
    
    @Override
    public RAW getRAW(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public RAW getRAW(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public REF getREF(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public REF getREF(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public ROWID getROWID(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public ROWID getROWID(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public STRUCT getSTRUCT(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public STRUCT getSTRUCT(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public TIMESTAMP getTIMESTAMP(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }

    @Override
    public TIMESTAMP getTIMESTAMP(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }


    @Override
    public TIMESTAMPLTZ getTIMESTAMPLTZ(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }
    
    @Override
    public TIMESTAMPTZ getTIMESTAMPTZ(int arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }
    
    @Override
    public TIMESTAMPTZ getTIMESTAMPTZ(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }
    

    @Override
    public TIMESTAMPLTZ getTIMESTAMPLTZ(String arg0) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
        //return null;
    }


    @Override
    public void updateARRAY(int arg0, ARRAY arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateARRAY(String arg0, ARRAY arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateBFILE(int arg0, BFILE arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateBFILE(String arg0, BFILE arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateBLOB(int arg0, BLOB arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateBLOB(String arg0, BLOB arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateBfile(int arg0, BFILE arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateBfile(String arg0, BFILE arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateCHAR(int arg0, CHAR arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateCHAR(String arg0, CHAR arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateCLOB(int arg0, CLOB arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateCLOB(String arg0, CLOB arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateCustomDatum(int arg0, CustomDatum arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateCustomDatum(String arg0, CustomDatum arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateDATE(int arg0, DATE arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateDATE(String arg0, DATE arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateINTERVALDS(int arg0, INTERVALDS arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateINTERVALDS(String arg0, INTERVALDS arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateINTERVALYM(int arg0, INTERVALYM arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateINTERVALYM(String arg0, INTERVALYM arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateNUMBER(int arg0, NUMBER arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateNUMBER(String arg0, NUMBER arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateORAData(int arg0, ORAData arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateORAData(String arg0, ORAData arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateOracleObject(int arg0, Datum arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateOracleObject(String arg0, Datum arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateRAW(int arg0, RAW arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateRAW(String arg0, RAW arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateREF(int arg0, REF arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateREF(String arg0, REF arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateROWID(int arg0, ROWID arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateROWID(String arg0, ROWID arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateSTRUCT(int arg0, STRUCT arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateSTRUCT(String arg0, STRUCT arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateTIMESTAMP(int arg0, TIMESTAMP arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateTIMESTAMP(String arg0, TIMESTAMP arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateTIMESTAMPLTZ(int arg0, TIMESTAMPLTZ arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateTIMESTAMPLTZ(String arg0, TIMESTAMPLTZ arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateTIMESTAMPTZ(int arg0, TIMESTAMPTZ arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    @Override
    public void updateTIMESTAMPTZ(String arg0, TIMESTAMPTZ arg1) throws SQLException {
        LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
    }

    // plus gutted base implementation replaced by log/super 22 sep
    public void createMetadataTree() throws SQLException {
        LogUtil.log();
        super.createMetadataTree();
    }

    /**
     * Helper method to start metadata section of the json stream Instead of a
     * metadata, there could be a "response" for DDLs
     */
    public void startMetaData() throws SQLException {
        LogUtil.log();
        super.startMetaData();
    } // end method

    /**
     * Moves the cursor forward one row from its current position. A
     * <code>ResultSet</code> cursor is initially positioned before the first
     * row; the first call to the method <code>next</code> makes the first row
     * the current row; the second call makes the second row the current row,
     * and so on.
     * <p>
     * When a call to the <code>next</code> method returns <code>false</code>,
     * the cursor is positioned after the last row. Any invocation of a
     * <code>ResultSet</code> method which requires a current row will result in
     * a <code>SQLException</code> being thrown. If the result set type is
     * <code>TYPE_FORWARD_ONLY</code>, it is vendor specified whether their JDBC
     * driver implementation will return <code>false</code> or throw an
     * <code>SQLException</code> on a subsequent call to <code>next</code>.
     *
     * <P>
     * If an input stream is open for the current row, a call to the method
     * <code>next</code> will implicitly close it. A <code>ResultSet</code>
     * object's warning chain is cleared when a new row is read.
     *
     * @return <code>true</code> if the new current row is valid;
     *         <code>false</code> if there are no more rows
     * @exception SQLException
     *                if a database access error occurs or this method is called
     *                on a closed result set
     */
    @Override
    public boolean next() throws SQLException {

        LogUtil.log();
        return super.next();
    }

    /**
     * Helper method to generate Post to send to adhoc based on fetchSize
     */
//    protected String generateJsonPostStmt(String sql) {
//        LogUtil.log();
//        return super.generateJsonPostStmt(sql);
//    }

    /**
     * Helper Method to walk a single row walkRow() also does pagination based
     * on maxRows and fetchSize
     */
    public String walkRow() {
        LogUtil.log();
        return super.walkRow();
    }

    /**
     * Helper method to start of the results section in the Json stream
     */
    public void startResults() throws SQLException{
        LogUtil.log();
        super.startResults();
    }

    /**
     * (non-Javadoc)
     * 
     * @see oracle.dbtools.restjdbc.abstractDef.AbstractRestJDBCResultSet#getObject(int)
     *      Get object at specified column index We need to handle each object
     *      individually
     */
    @Override
    public Object getObject(int columnIndex) throws SQLException {

        LogUtil.log();
        return super.getObject(columnIndex);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getObject(java.lang.String) TODO: Handle
     */
    @Override
    public Object getObject(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getObject(columnLabel);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as a <code>String</code> in the Java
     * programming language.
     *
     * @param columnIndex
     *            the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>null</code>
     * @exception SQLException
     *                if the columnIndex is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     */
    @Override
    public String getString(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getString(columnIndex);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as a <code>short</code> in the Java
     * programming language.
     *
     * @param columnIndex
     *            the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>0</code>
     * @exception SQLException
     *                if the columnIndex is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     */
    @Override
    public short getShort(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getShort(columnIndex);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as an <code>int</code> in the Java
     * programming language.
     *
     * @param columnIndex
     *            the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>0</code>
     * @exception SQLException
     *                if the columnIndex is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     */
    @Override
    public int getInt(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getInt(columnIndex);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as a <code>long</code> in the Java
     * programming language.
     *
     * @param columnIndex
     *            the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>0</code>
     * @exception SQLException
     *                if the columnIndex is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     */
    @Override
    public long getLong(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getLong(columnIndex);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as a <code>float</code> in the Java
     * programming language.
     *
     * @param columnIndex
     *            the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>0</code>
     * @exception SQLException
     *                if the columnIndex is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     */
    @Override
    public float getFloat(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getFloat(columnIndex);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as a <code>double</code> in the Java
     * programming language.
     *
     * @param columnIndex
     *            the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>0</code>
     * @exception SQLException
     *                if the columnIndex is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     */
    @Override
    public double getDouble(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getDouble(columnIndex);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as a <code>byte</code> array in the Java
     * programming language. The bytes represent the raw values returned by the
     * driver.
     *
     * @param columnIndex
     *            the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>null</code>
     * @exception SQLException
     *                if the columnIndex is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     */
    @Override
    public byte[] getBytes(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getBytes(columnIndex);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as a <code>java.sql.Date</code> object in
     * the Java programming language.
     *
     * @param columnIndex
     *            the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>null</code>
     * @exception SQLException
     *                if the columnIndex is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     */
    @Override
    public Date getDate(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getDate(columnIndex);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as a <code>java.sql.Time</code> object in
     * the Java programming language.
     *
     * @param columnIndex
     *            the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>null</code>
     * @exception SQLException
     *                if the columnIndex is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     * 
     *                Note : The timezone is translated to the local time zone
     *                of the client TODO: All Exceptions not properly handled
     *                yet] TODO: Unsure if getTime should return decimal
     *                precision or not
     */
    @Override
    public Time getTime(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getTime(columnIndex);
    }

    /**
       * Retrieves the value of the designated column in the current row
       * of this <code>ResultSet</code> object as
       * a <code>java.sql.Timestamp</code> object in the Java programming language.
       *
       * @param columnIndex the first column is 1, the second is 2, ...
       * @return the column value; if the value is SQL <code>NULL</code>, the
       * value returned is <code>null</code>
       * @exception SQLException if the columnIndex is not valid;
       * if a database access error occurs or this method is
       *            called on a closed result set
       *           
       * TODO: Maybe improve exception handling
       */
      @Override
      public Timestamp getTimestamp(int columnIndex) throws SQLException {
      LogUtil.log();
return super.getTimestamp(columnIndex);
      }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getAsciiStream(int)
     */
    @Override
    public InputStream getAsciiStream(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getAsciiStream(columnIndex);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getUnicodeStream(int)
     */
    @Override
    public InputStream getUnicodeStream(int columnIndex) throws SQLException {

        LogUtil.log();
        return super.getUnicodeStream(columnIndex);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getBinaryStream(int) TODO: Handle Exceptions
     */
    @Override
    public InputStream getBinaryStream(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getBinaryStream(columnIndex);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getBlob(int)
     */
    @Override
    public Blob getBlob(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getBlob(columnIndex);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getClob(int)
     */
    @Override
    public Clob getClob(int columnIndex) throws SQLException {

        LogUtil.log();
        return super.getClob(columnIndex);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getArray(int)
     */
    @Override
    public Array getArray(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getArray(columnIndex);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getCharacterStream(int)
     */
    @Override
    public Reader getCharacterStream(int columnIndex) throws SQLException {

        LogUtil.log();
        return super.getCharacterStream(columnIndex);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getCharacterStream(java.lang.String)
     */
    @Override
    public Reader getCharacterStream(String columnLabel) throws SQLException {

        LogUtil.log();
        return super.getCharacterStream(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getBlob(java.lang.String)
     */
    @Override
    public Blob getBlob(String columnLabel) throws SQLException {

        LogUtil.log();
        return super.getBlob(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getClob(java.lang.String)
     */
    @Override
    public Clob getClob(String columnLabel) throws SQLException {

        LogUtil.log();
        return super.getClob(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getArray(java.lang.String)
     */
    @Override
    public Array getArray(String columnLabel) throws SQLException {
        // TODO Auto-generated method stub

        LogUtil.log();
        return super.getArray(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getAsciiStream(java.lang.String)
     */
    @Override
    public InputStream getAsciiStream(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getAsciiStream(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getUnicodeStream(java.lang.String)
     */
    @Override
    public InputStream getUnicodeStream(String columnLabel) throws SQLException {
        // TODO Auto-generated method stub
        LogUtil.log();
        return super.getUnicodeStream(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getBinaryStream(java.lang.String)
     */
    @Override
    public InputStream getBinaryStream(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getBinaryStream(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getString(java.lang.String)
     * 
     */
    @Override
    public String getString(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getString(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getShort(java.lang.String)
     */
    @Override
    public short getShort(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getShort(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getInt(java.lang.String)
     */
    @Override
    public int getInt(String columnLabel) throws SQLException {

        LogUtil.log();
        return super.getInt(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getLong(java.lang.String)
     */
    @Override
    public long getLong(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getLong(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getFloat(java.lang.String)
     */
    @Override
    public float getFloat(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getFloat(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getDouble(java.lang.String)
     */
    @Override
    public double getDouble(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getDouble(columnLabel);
    }

    @Override
    public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getBigDecimal(columnIndex);
    }

    @Override
    public BigDecimal getBigDecimal(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getBigDecimal(columnLabel);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as a <code>boolean</code> in the Java
     * programming language.
     *
     * <P>
     * If the designated column has a datatype of CHAR or VARCHAR and contains a
     * "0" or has a datatype of BIT, TINYINT, SMALLINT, INTEGER or BIGINT and
     * contains a 0, a value of <code>false</code> is returned. If the
     * designated column has a datatype of CHAR or VARCHAR and contains a "1" or
     * has a datatype of BIT, TINYINT, SMALLINT, INTEGER or BIGINT and contains
     * a 1, a value of <code>true</code> is returned.
     *
     * @param columnIndex
     *            the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>false</code>
     * @exception SQLException
     *                if the columnIndex is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     */
    @Override
    public boolean getBoolean(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getBoolean(columnIndex);
    }

    /**
     * Retrieves the value of the designated column in the current row of this
     * <code>ResultSet</code> object as a <code>boolean</code> in the Java
     * programming language.
     *
     * <P>
     * If the designated column has a datatype of CHAR or VARCHAR and contains a
     * "0" or has a datatype of BIT, TINYINT, SMALLINT, INTEGER or BIGINT and
     * contains a 0, a value of <code>false</code> is returned. If the
     * designated column has a datatype of CHAR or VARCHAR and contains a "1" or
     * has a datatype of BIT, TINYINT, SMALLINT, INTEGER or BIGINT and contains
     * a 1, a value of <code>true</code> is returned.
     *
     * @param columnLabel
     *            the label for the column specified with the SQL AS clause. If
     *            the SQL AS clause was not specified, then the label is the
     *            name of the column
     * @return the column value; if the value is SQL <code>NULL</code>, the
     *         value returned is <code>false</code>
     * @exception SQLException
     *                if the columnLabel is not valid; if a database access
     *                error occurs or this method is called on a closed result
     *                set
     */
    @Override
    public boolean getBoolean(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getBoolean(columnLabel);
    }

    /**
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getBytes(java.lang.String)
     */
    @Override
    public byte[] getBytes(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getBytes(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getDate(java.lang.String) TODO : All Exceptions
     * not properly handled yet
     */
    @Override
    public Date getDate(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getDate(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getTime(java.lang.String) Note : The timezone is
     * translated to the local time zone of the client TODO: All Exceptions not
     * properly handled yet] TODO: Unsure if getTime should return decimal
     * precision or not
     */
    @Override
    public Time getTime(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getTime(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getTimestamp(java.lang.String) TODO : Exception
     * handling and perhaps improve performance
     */
    @Override
    public Timestamp getTimestamp(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getTimestamp(columnLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#close()
     */
    @Override
    public void close() throws SQLException {
        LogUtil.log();
        super.close();
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#getMetaData() TODO: All exceptions not handled
     * yet
     */
    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        LogUtil.log();
        return super.getMetaData();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#isBeforeFirst()
     */
    @Override
    public boolean isBeforeFirst() throws SQLException {
        LogUtil.log();
        return super.isBeforeFirst();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#isAfterLast()
     */
    @Override
    public boolean isAfterLast() throws SQLException {
        LogUtil.log();
        return super.isAfterLast();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#isFirst()
     */
    @Override
    public boolean isFirst() throws SQLException {

        LogUtil.log();
        return super.isFirst();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.sql.ResultSet#isLast()
     */
    @Override
    public boolean isLast() throws SQLException {
        LogUtil.log();
        return super.isLast();
    }

    /* Returns the ResultSet object that produces the ResultSet */
    @Override
    public Statement getStatement() throws SQLException {
        LogUtil.log();
        return super.getStatement();
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        LogUtil.log();
        return super.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        LogUtil.log();
        return super.isWrapperFor(iface);
    }

    @Override
    public boolean wasNull() throws SQLException {
        LogUtil.log();
        return super.wasNull();
    }

    @Override
    public byte getByte(int columnIndex) throws SQLException {

        LogUtil.log();
        return super.getByte(columnIndex);
    }

    @Override
    public BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException {
        LogUtil.log();
        return super.getBigDecimal(columnIndex, scale);
    }

    @Override
    public byte getByte(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getByte(columnLabel);
    }

    @Override
    public BigDecimal getBigDecimal(String columnLabel, int scale) throws SQLException {
        LogUtil.log();
        return super.getBigDecimal(columnLabel, scale);
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        LogUtil.log();
        return super.getWarnings();
    }

    @Override
    public void clearWarnings() throws SQLException {
        LogUtil.log();
        super.clearWarnings();
    }

    @Override
    public String getCursorName() throws SQLException {
        LogUtil.log();
        return super.getCursorName();
    }

    @Override
    public int findColumn(String columnLabel) throws SQLException {

        LogUtil.log();
        //deb does not have this yet
        if (columnLabel==null) {
        	throw new SQLException("invalid argument null");
        }
        //make it equals ignore case  
        ResultSetMetaData rsMeta=this.getMetaData();
        for (int i=1;i<=rsMeta.getColumnCount();i++) {
        	if (rsMeta.getColumnLabel(i).equalsIgnoreCase(columnLabel)) {
        		return i;
        	}
        }
        throw new SQLException("invalid argument no match for: "+columnLabel);
        		//super.findColumn(columnLabel);
    }

    @Override
    public void beforeFirst() throws SQLException {
        LogUtil.log();
        super.beforeFirst();
    }

    @Override
    public void afterLast() throws SQLException {
        LogUtil.log();
        super.afterLast();
    }

    @Override
    public boolean first() throws SQLException {
        LogUtil.log();
        return super.first();
    }

    @Override
    public boolean last() throws SQLException {
        LogUtil.log();
        return super.last();
    }

    @Override
    public int getRow() throws SQLException {
        LogUtil.log();
        return super.getRow();
    }

    @Override
    public boolean absolute(int row) throws SQLException {
        LogUtil.log();
        return super.absolute(row);
    }

    @Override
    public boolean relative(int rows) throws SQLException {
        LogUtil.log();
        return super.relative(rows);
    }

    @Override
    public boolean previous() throws SQLException {
        LogUtil.log();
        return super.previous();
    }

    @Override
    public void setFetchDirection(int direction) throws SQLException {
        LogUtil.log();
        super.setFetchDirection(direction);
    }

    @Override
    public int getFetchDirection() throws SQLException {
        LogUtil.log();
        return super.getFetchDirection();
    }

    /**
     * Gives the JDBC driver a hint as to the number of rows that should be
     * fetched from the database when more rows are needed for this
     * <code>ResultSet</code> object. If the fetch size specified is zero, the
     * JDBC driver ignores the value and is free to make its own best guess as
     * to what the fetch size should be. The default value is set by the
     * <code>Statement</code> object that created the result set. The fetch size
     * may be changed at any time.
     *
     * @param rows
     *            the number of rows to fetch
     * @exception SQLException
     *                if a database access error occurs; this method is called
     *                on a closed result set or the condition {@code rows >= 0 }
     *                is not satisfied
     * @since 1.2
     * @see #getFetchSize
     */
    public void setFetchSize(int rows) throws SQLException {
        LogUtil.log();
        super.setFetchSize(rows);
    }

    /**
     * Retrieves the fetch size for this <code>ResultSet</code> object.
     *
     * @return the current fetch size for this <code>ResultSet</code> object
     * @exception SQLException
     *                if a database access error occurs or this method is called
     *                on a closed result set
     * @since 1.2
     * @see #setFetchSize
     */
    @Override
    public int getFetchSize() throws SQLException {
        LogUtil.log();
        return super.getFetchSize();
    }

    @Override
    public int getType() throws SQLException {
        LogUtil.log();
        return super.getType();
    }

    @Override
    public int getConcurrency() throws SQLException {
        LogUtil.log();
        return super.getConcurrency();
    }

    @Override
    public boolean rowUpdated() throws SQLException {
        LogUtil.log();
        return super.rowUpdated();
    }

    @Override
    public boolean rowInserted() throws SQLException {
        LogUtil.log();
        return super.rowInserted();
    }

    @Override
    public boolean rowDeleted() throws SQLException {
        LogUtil.log();
        return super.rowDeleted();
    }

    @Override
    public void insertRow() throws SQLException {
        LogUtil.log();
        super.insertRow();
    }

    @Override
    public void deleteRow() throws SQLException {
        LogUtil.log();
        super.deleteRow();
    }

    @Override
    public void refreshRow() throws SQLException {
        LogUtil.log();
        super.refreshRow();
    }

    @Override
    public void cancelRowUpdates() throws SQLException {
        LogUtil.log();
        super.cancelRowUpdates();
    }

    @Override
    public void moveToInsertRow() throws SQLException {
        LogUtil.log();
        super.moveToInsertRow();
    }

    @Override
    public void moveToCurrentRow() throws SQLException {
        LogUtil.log();
        super.moveToCurrentRow();
    }

    @Override
    public Object getObject(int columnIndex, Map<String, Class<?>> map) throws SQLException {
        LogUtil.log();
        return super.getObject(columnIndex, map);
    }

    @Override
    public Ref getRef(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getRef(columnIndex);
    }

    @Override
    public Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException {
        LogUtil.log();
        return super.getObject(columnLabel, map);
    }

    @Override
    public Ref getRef(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getRef(columnLabel);
    }

    @Override
    public Date getDate(int columnIndex, Calendar cal) throws SQLException {
        LogUtil.log();
        return super.getDate(columnIndex, cal);
    }

    @Override
    public Date getDate(String columnLabel, Calendar cal) throws SQLException {
        LogUtil.log();
        return super.getDate(columnLabel, cal);
    }

    @Override
    public Time getTime(int columnIndex, Calendar cal) throws SQLException {
        LogUtil.log();
        return super.getTime(columnIndex, cal);
    }

    @Override
    public Time getTime(String columnLabel, Calendar cal) throws SQLException {
        LogUtil.log();
        return super.getTime(columnLabel, cal);
    }

    @Override
    public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException {
        LogUtil.log();
        return super.getTimestamp(columnIndex, cal);
    }

    @Override
    public Timestamp getTimestamp(String columnLabel, Calendar cal) throws SQLException {
        LogUtil.log();
        return super.getTimestamp(columnLabel, cal);
    }

    @Override
    public URL getURL(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getURL(columnIndex);
    }

    @Override
    public URL getURL(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getURL(columnLabel);
    }

    @Override
    public RowId getRowId(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getRowId(columnIndex);
    }

    @Override
    public RowId getRowId(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getRowId(columnLabel);
    }

    @Override
    public int getHoldability() throws SQLException {
        LogUtil.log();
        return super.getHoldability();
    }

    @Override
    public boolean isClosed() throws SQLException {
        LogUtil.log();
        return super.isClosed();
    }

    @Override
    public NClob getNClob(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getNClob(columnIndex);
    }

    @Override
    public NClob getNClob(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getNClob(columnLabel);
    }

    @Override
    public SQLXML getSQLXML(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getSQLXML(columnIndex);
    }

    @Override
    public SQLXML getSQLXML(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getSQLXML(columnLabel);
    }

    @Override
    public String getNString(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getNString(columnIndex);
    }

    @Override
    public String getNString(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getNString(columnLabel);
    }

    @Override
    public Reader getNCharacterStream(int columnIndex) throws SQLException {
        LogUtil.log();
        return super.getNCharacterStream(columnIndex);
    }

    @Override
    public Reader getNCharacterStream(String columnLabel) throws SQLException {
        LogUtil.log();
        return super.getNCharacterStream(columnLabel);
    }

    @Override
    public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
        LogUtil.log();
        return super.getObject(columnIndex, type);
    }

    @Override
    public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
        LogUtil.log();
        return super.getObject(columnLabel, type);
    }

    /**
     * The following methods are not supported by the REST JDBC driver
     */

    /**********************************************************************************/

    @Override
    public void updateRow() throws SQLException {
        LogUtil.log();
        super.updateRow();
    }

    @Override
    public void updateNull(int columnIndex) throws SQLException {
        LogUtil.log();
        super.updateNull(columnIndex);
    }

    @Override
    public void updateBoolean(int columnIndex, boolean x) throws SQLException {
        LogUtil.log();
        super.updateBoolean(columnIndex, x);
    }

    @Override
    public void updateByte(int columnIndex, byte x) throws SQLException {
        LogUtil.log();
        super.updateByte(columnIndex, x);
    }

    @Override
    public void updateShort(int columnIndex, short x) throws SQLException {
        LogUtil.log();
        super.updateShort(columnIndex, x);
    }

    @Override
    public void updateInt(int columnIndex, int x) throws SQLException {
        LogUtil.log();
        super.updateInt(columnIndex, x);
    }

    @Override
    public void updateLong(int columnIndex, long x) throws SQLException {
        LogUtil.log();
        super.updateLong(columnIndex, x);
    }

    @Override
    public void updateFloat(int columnIndex, float x) throws SQLException {
        LogUtil.log();
        super.updateFloat(columnIndex, x);
    }

    @Override
    public void updateDouble(int columnIndex, double x) throws SQLException {
        LogUtil.log();
        super.updateDouble(columnIndex, x);
    }

    @Override
    public void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException {
        LogUtil.log();
        super.updateBigDecimal(columnIndex, x);
    }

    @Override
    public void updateString(int columnIndex, String x) throws SQLException {
        LogUtil.log();
        super.updateString(columnIndex, x);
    }

    @Override
    public void updateBytes(int columnIndex, byte[] x) throws SQLException {
        LogUtil.log();
        super.updateBytes(columnIndex, x);
    }

    @Override
    public void updateDate(int columnIndex, Date x) throws SQLException {
        LogUtil.log();
        super.updateDate(columnIndex, x);
    }

    @Override
    public void updateTime(int columnIndex, Time x) throws SQLException {
        LogUtil.log();
        super.updateTime(columnIndex, x);
    }

    @Override
    public void updateTimestamp(int columnIndex, Timestamp x) throws SQLException {
        LogUtil.log();
        super.updateTimestamp(columnIndex, x);
    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException {
        LogUtil.log();
        super.updateAsciiStream(columnIndex, x, length);
    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException {
        LogUtil.log();
        super.updateBinaryStream(columnIndex, x, length);
    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x, int length) throws SQLException {
        LogUtil.log();
        super.updateCharacterStream(columnIndex, x);
    }

    @Override
    public void updateObject(int columnIndex, Object x, int scaleOrLength) throws SQLException {
        LogUtil.log();
        super.updateObject(columnIndex, x, scaleOrLength);
    }

    @Override
    public void updateObject(int columnIndex, Object x) throws SQLException {
        LogUtil.log();
        super.updateObject(columnIndex, x);
    }

    @Override
    public void updateNull(String columnLabel) throws SQLException {
        LogUtil.log();
        super.updateNull(columnLabel);
    }

    @Override
    public void updateBoolean(String columnLabel, boolean x) throws SQLException {
        LogUtil.log();
        super.updateBoolean(columnLabel, x);
    }

    @Override
    public void updateByte(String columnLabel, byte x) throws SQLException {
        LogUtil.log();
        super.updateByte(columnLabel, x);
    }

    @Override
    public void updateShort(String columnLabel, short x) throws SQLException {
        LogUtil.log();
        super.updateShort(columnLabel, x);
    }

    @Override
    public void updateInt(String columnLabel, int x) throws SQLException {
        LogUtil.log();
        super.updateInt(columnLabel, x);
    }

    @Override
    public void updateLong(String columnLabel, long x) throws SQLException {
        LogUtil.log();
        super.updateLong(columnLabel, x);
    }

    @Override
    public void updateFloat(String columnLabel, float x) throws SQLException {
        LogUtil.log();
        super.updateFloat(columnLabel, x);
    }

    @Override
      public void updateDouble(String columnLabel, double x) throws SQLException {
      LogUtil.log();
super.updateDouble(columnLabel, x);
    }

    @Override
    public void updateBigDecimal(String columnLabel, BigDecimal x) throws SQLException {
        LogUtil.log();
        super.updateBigDecimal(columnLabel, x);
    }

    @Override
    public void updateString(String columnLabel, String x) throws SQLException {
        LogUtil.log();
        super.updateString(columnLabel, x);
    }

    @Override
    public void updateBytes(String columnLabel, byte[] x) throws SQLException {
        LogUtil.log();
        super.updateBytes(columnLabel, x);
    }

    public void updateDate(String columnLabel, Date x) throws SQLException {
        LogUtil.log();
        super.updateDate(columnLabel, x);
    }

    @Override
    public void updateTime(String columnLabel, Time x) throws SQLException {
        LogUtil.log();
        super.updateTime(columnLabel, x);
    }

    @Override
    public void updateTimestamp(String columnLabel, Timestamp x) throws SQLException {
        LogUtil.log();
        super.updateTimestamp(columnLabel, x);
    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x, int length) throws SQLException {
        LogUtil.log();
        super.updateAsciiStream(columnLabel, x, length);
    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x, int length) throws SQLException {
        LogUtil.log();
        super.updateBinaryStream(columnLabel, x, length);
    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader, int length) throws SQLException {
        LogUtil.log();
        super.updateCharacterStream(columnLabel, reader, length);
    }

    @Override
    public void updateObject(String columnLabel, Object x, int scaleOrLength) throws SQLException {
        LogUtil.log();
        super.updateObject(columnLabel, x, scaleOrLength);
    }

    @Override
    public void updateObject(String columnLabel, Object x) throws SQLException {
        LogUtil.log();
        super.updateObject(columnLabel, x);
    }

    @Override
    public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
        LogUtil.log();
        super.updateNCharacterStream(columnIndex, x, length);
    }

    @Override
    public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
        LogUtil.log();
        super.updateNCharacterStream(columnLabel, reader, length);
    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
        LogUtil.log();
        super.updateAsciiStream(columnIndex, x, length);
    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
        LogUtil.log();
        super.updateBinaryStream(columnIndex, x, length);
    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
        LogUtil.log();
        super.updateCharacterStream(columnIndex, x);
    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
        LogUtil.log();
        super.updateAsciiStream(columnLabel, x, length);
    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
        LogUtil.log();
        super.updateBinaryStream(columnLabel, x, length);
    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
        LogUtil.log();
        super.updateCharacterStream(columnLabel, reader, length);
    }

    @Override
    public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
        LogUtil.log();
        super.updateBlob(columnIndex, inputStream, length);
    }

    @Override
    public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
        LogUtil.log();
        super.updateBlob(columnLabel, inputStream, length);
    }

    @Override
    public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
        LogUtil.log();
        super.updateClob(columnIndex, reader, length);
    }

    @Override
    public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
        LogUtil.log();
        super.updateClob(columnLabel, reader, length);
    }

    @Override
    public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
        LogUtil.log();
        super.updateNClob(columnIndex, reader, length);
    }

    @Override
    public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
        LogUtil.log();
        super.updateNClob(columnLabel, reader, length);
    }

    @Override
    public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
        LogUtil.log();
        super.updateNCharacterStream(columnIndex, x);
    }

    @Override
    public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
        LogUtil.log();
        super.updateNCharacterStream(columnLabel, reader);
    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
        LogUtil.log();
        super.updateAsciiStream(columnIndex, x);
    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
        LogUtil.log();
        super.updateBinaryStream(columnIndex, x);
    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
        LogUtil.log();
        super.updateCharacterStream(columnIndex, x);
    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
        LogUtil.log();
        super.updateAsciiStream(columnLabel, x);
    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
        LogUtil.log();
        super.updateBinaryStream(columnLabel, x);
    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
        LogUtil.log();
        super.updateCharacterStream(columnLabel, reader);
    }

    @Override
    public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
        LogUtil.log();
        super.updateBlob(columnIndex, inputStream);
    }

    @Override
    public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
        LogUtil.log();
        super.updateBlob(columnLabel, inputStream);
    }

    @Override
    public void updateClob(int columnIndex, Reader reader) throws SQLException {
        LogUtil.log();
        super.updateClob(columnIndex, reader);
    }

    @Override
    public void updateClob(String columnLabel, Reader reader) throws SQLException {
        LogUtil.log();
        super.updateClob(columnLabel, reader);
    }

    @Override
    public void updateNClob(int columnIndex, Reader reader) throws SQLException {
        LogUtil.log();
        super.updateNClob(columnIndex, reader);
    }

    @Override
    public void updateNClob(String columnLabel, Reader reader) throws SQLException {
        LogUtil.log();
        super.updateNClob(columnLabel, reader);
    }

    @Override
    public void updateSQLXML(int columnIndex, SQLXML xmlObject)
        throws SQLException {
        LogUtil.log();
        super.updateSQLXML(columnIndex, xmlObject);
    }

    @Override
    public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
        LogUtil.log();
        super.updateSQLXML(columnLabel, xmlObject);
    }

    @Override
    public void updateNString(int columnIndex, String nString) throws SQLException {
        LogUtil.log();
        super.updateNString(columnIndex, nString);
    }

    @Override
    public void updateNString(String columnLabel, String nString) throws SQLException {
        LogUtil.log();
        super.updateNString(columnLabel, nString);
    }

    @Override
    public void updateNClob(int columnIndex, NClob nClob) throws SQLException {
        LogUtil.log();
        super.updateNClob(columnIndex, nClob);
    }

    @Override
    public void updateNClob(String columnLabel, NClob nClob) throws SQLException {
        LogUtil.log();
        super.updateNClob(columnLabel, nClob);
    }

    @Override
    public void updateRowId(int columnIndex, RowId x) throws SQLException {
        LogUtil.log();
        super.updateRowId(columnIndex, x);
    }

    @Override
    public void updateRowId(String columnLabel, RowId x) throws SQLException {
        LogUtil.log();
        super.updateRowId(columnLabel, x);
    }

    @Override
    public void updateRef(int columnIndex, Ref x) throws SQLException {
        LogUtil.log();
        super.updateRef(columnIndex, x);
    }

    @Override
    public void updateRef(String columnLabel, Ref x) throws SQLException {
        LogUtil.log();
        super.updateRef(columnLabel, x);
    }

    @Override
    public void updateBlob(int columnIndex, Blob x) throws SQLException {
        LogUtil.log();
        super.updateBlob(columnIndex, x);
    }

    @Override
    public void updateBlob(String columnLabel, Blob x) throws SQLException {
        LogUtil.log();
        super.updateBlob(columnLabel, x);
    }

    @Override
    public void updateClob(int columnIndex, Clob x) throws SQLException {
        LogUtil.log();
        super.updateClob(columnIndex, x);
    }

    @Override
    public void updateClob(String columnLabel, Clob x) throws SQLException {
        LogUtil.log();
        super.updateClob(columnLabel, x);
    }

    @Override
    public void updateArray(int columnIndex, Array x) throws SQLException {
        LogUtil.log();
        super.updateArray(columnIndex, x);
    }

    @Override
    public void updateArray(String columnLabel, Array x) throws SQLException {
        LogUtil.log();
        super.updateArray(columnLabel, x);
    }

}
