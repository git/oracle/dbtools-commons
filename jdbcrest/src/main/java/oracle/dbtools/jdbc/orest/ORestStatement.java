/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.orest;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import oracle.dbtools.http.Session;
import oracle.dbtools.http.SessionException;
import oracle.dbtools.jdbc.Connection;
import oracle.dbtools.jdbc.util.RestjdbcResources;
import oracle.dbtools.jdbc.Statement;
import oracle.dbtools.jdbc.util.Defaults;
import oracle.dbtools.jdbc.util.LogUtil;
import oracle.dbtools.parser.plsql.SyntaxError;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleStatement;
import oracle.jdbc.dcn.DatabaseChangeRegistration;

public class ORestStatement extends Statement implements OracleStatement {

	protected final ORestStatementImpl impl;
	@SuppressWarnings("unused")
	private ORestStatement() {
		impl=null;
		LogUtil.log();// TODO Auto-generated constructor stub
	    throw new IllegalArgumentException();
	}

	public ORestStatement(Connection conn) {
		super(conn);
		LogUtil.log();
		impl = new ORestStatementImpl(this);
		LogUtil.log();// TODO Auto-generated constructor stub
	}

	@Override
	public void clearDefines() throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void closeWithKey(String arg0) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public int creationState() {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return 0;
	}

	@Override
	public void defineColumnType(int arg0, int arg1) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineColumnType(int arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineColumnType(int arg0, int arg1, String arg2) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineColumnType(int arg0, int arg1, int arg2, short arg3) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineColumnTypeBytes(int arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void defineColumnTypeChars(int arg0, int arg1, int arg2) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public int getLobPrefetchSize() throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return 0;
	}

	@Override
	public long getRegisteredQueryId() throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return 0;
	}

	@Override
	public String[] getRegisteredTableNames() throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return null;
	}

	@Override
	public int getRowPrefetch() {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return 0;
	}

	@Override
	public boolean isNCHAR(int arg0) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));
		//return false;
	}

	@Override
	public void setDatabaseChangeRegistration(DatabaseChangeRegistration arg0) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setLobPrefetchSize(int arg0) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName()));

	}

	@Override
	public void setRowPrefetch(int arg0) throws SQLException {
		LogUtil.log();// TODO Auto-generated method stub
        throw new UnsupportedOperationException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        new Exception().getStackTrace()[0].getMethodName(), this.getClass().getSimpleName())); 

	}
//return orest result set:
	@Override
	public ResultSet executeQuery(String sql) throws SQLException {	
	    CloseableHttpResponse response = orestExecuteInternal(sql);
	  	
		this.setCurrentResultSet(new oracle.dbtools.jdbc.orest.ORestResultSet(response, this, this.getConn()));		
		return this.getCurrentResultSet();
	}  

}
