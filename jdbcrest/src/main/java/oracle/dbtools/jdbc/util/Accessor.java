/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.util;

import java.util.HashMap;
import java.util.Map;


public class Accessor {
  
  public static Map<String, Integer> types = new HashMap<String,Integer>();
  
  // populate the map
  static {
    types.put("VARCHAR2", OracleTypes.VARCHAR);
    types.put("NVARCHAR", OracleTypes.NVARCHAR);
    types.put("NUMBER", OracleTypes.NUMBER);
    types.put("FLOAT", OracleTypes.FLOAT);
    types.put("LONG", OracleTypes.LONGVARCHAR);
    types.put("DATE", OracleTypes.TIMESTAMP);
    types.put("BINARY_FLOAT", OracleTypes.BINARY_FLOAT);
    types.put("DOUBLE", OracleTypes.DOUBLE);
    types.put("BINARY_DOUBLE", OracleTypes.BINARY_DOUBLE);
    types.put("TIMESTAMP", OracleTypes.TIMESTAMP);
    types.put("TIMESTAMPTZ", OracleTypes.TIMESTAMPTZ); // Timestamp with Time Zone
    types.put("TIMESTAMP WITH TIME ZONE", OracleTypes.TIMESTAMPTZ);
    types.put("TIMESTAMPLTZ", OracleTypes.TIMESTAMPLTZ); // Timestamp with Local Time Zone
    types.put("INTERVALYM", OracleTypes.INTERVALYM); // Interval Year to Month
    types.put("INTERVALDS", OracleTypes.INTERVALDS); // Interval Day to Second
    
    /**
     * PLSQL_BOOLEAN binds BOOLEAN type for input/output 
     * parameters when executing a PLSQL function/procedure.
     * Only available for thin driver.
     * 
     * @since 12.2
     */
    types.put("PLSQL_BOOLEAN", OracleTypes.PLSQL_BOOLEAN); // Only available for thin driver
    types.put("RAW", OracleTypes.RAW);
    types.put("LONG RAW", OracleTypes.LONGVARBINARY);
    types.put("ROWID", OracleTypes.ROWID);
    types.put("CHAR", OracleTypes.CHAR);
    types.put("NCHAR", OracleTypes.NCHAR); 
    types.put("CLOB", OracleTypes.CLOB);
    types.put("NCLOB", OracleTypes.NCLOB); 
    types.put("BLOB", OracleTypes.BLOB);
    types.put("BFILE", OracleTypes.BFILE);
    types.put("TIMESTAMP WITH LOCAL TIME ZONE", OracleTypes.TIMESTAMPLTZ);
    types.put("TIMESTAMP WITH TIME ZONE", OracleTypes.TIMESTAMPTZ);
    
    // User-Defined Types
    // http://yourmachine.yourdomain/12/121/server.121/e17209/sql_elements001.htm#SQLRF51004
    types.put("OBJECT", OracleTypes.STRUCT);
    types.put("REF", OracleTypes.REF);
    types.put("VARRAY", OracleTypes.ARRAY);
    types.put("NESTED_TABLE", OracleTypes.ARRAY);
    // Any Types
    // http://yourmachine.yourdomain/12/121/server.121/e17209/sql_elements001.htm#SQLRF30023
    types.put("ANYTYPE", OracleTypes.OPAQUE);
    types.put("ANYDATA", OracleTypes.OPAQUE);
    
    // XML Types
    // http://yourmachine.yourdomain/12/121/server.121/e17209/sql_elements001.htm#SQLRF30024
    types.put("XMLTYPE", OracleTypes.SQLXML);
    types.put("LONGRAW", OracleTypes.LONGRAW);
    
    // Spatial Types
    // http://yourmachine.yourdomain/12/121/server.121/e17209/sql_elements001.htm#SQLRF30025
    types.put("SDO_GEOMETRY", OracleTypes.STRUCT);
    types.put("SDO_TOPO_GEOMETRY", OracleTypes.STRUCT);
    types.put("SDO_GEORASTER", OracleTypes.STRUCT);
  }

  /**
   * 
   * @param column_type_name
   * @return 
   */
  
  public static int getConstant(String column_type_name) {
    // if the key exists then return    
    if(types.containsKey(column_type_name)) {
      return types.get(column_type_name);
    }
    else if(column_type_name.contains("XMLTYPE")) {
      return 2009;
    }
    // Otherwise return a Oracle.sql.Struct    
    return OracleTypes.STRUCT;
  }
}
