/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.util;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.bind.DatatypeConverter;


public class DateTimestampsUtil {
 
  /**
   * Convert a string to a java.sql.Timestamp
   * @param tsStr - a string Timestamp in iso 8601 format
   * @return
   */
  public java.sql.Timestamp getTimestamp(String tsStr, String timeZoneID) {
    
    // First convert String timestamp to Calendar with UTC timezone
    // This is for parsing the iso 8601 timestamptz
    Calendar utcCal = new GregorianCalendar();
    utcCal = DatatypeConverter.parseDateTime(tsStr);

    Long ts = utcCal.getTimeInMillis();  
    
    
    // Format timestamp to be in the given timeZone
    DateFormat format = DateFormat.getDateTimeInstance();
    format.setTimeZone(TimeZone.getTimeZone(timeZoneID));
    format.format(ts);
    
    // Get the calendar associated with this formatter and given timeZone
    Calendar cal = format.getCalendar();
    
    // we are getting the timestamp from the ResultSet
    boolean sendToAdhoc = false;
   
    String timestampStr = getTimestampStr(cal, sendToAdhoc);
    // Convert to timestamp object
    java.sql.Timestamp timestamp = Timestamp.valueOf(timestampStr); 
   
    return timestamp;    
  }
  
  /**
   * Convert a string to java.sql.Date
   * @param tsStr in iso8601 format
   * @param timeZoneID
   * @return a java.sql.Date date
   */
  public java.sql.Date getDate(String tsStr, String timeZoneID) {
    java.sql.Timestamp timestamp = getTimestamp(tsStr, timeZoneID);
    java.sql.Date date = new Date(timestamp.getTime());
    
    return date;
  }
 
  /**
   * Convert a string to java.sql.Date
   * @param tsStr in "yyyy-mm-dd" format
   * @param timeZoneID
   * @return a java.sql.Date date
   */
  public java.sql.Date getDate(String tsStr) throws SQLException {
    return java.sql.Date.valueOf(tsStr);    
  }
  
  /**
   * Converts a java.sql.Timestamp to the iso-8601 Json format to post up to ORDS
   * @param java.sql.Timestamp
   * @param timeZoneID - timezone of the ORDS instance
   * @return a String timestamp
   */
  public String setTimeStamp(java.sql.Timestamp ts, String timeZoneID, Calendar cal) {
    
    // Get an instance of a calendar in the client timezone
//    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(ts.getTime());
 
    // Get an instance of a calendar with ORDS timezone
    // We will use this calendar to convert the timestamp to the ords timezone
    // ORDS timezone may not be UTC
    Calendar utcCal = Calendar.getInstance();
    utcCal.setTimeZone(TimeZone.getTimeZone(timeZoneID));
    utcCal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
    utcCal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
    utcCal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
    utcCal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
    utcCal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
    utcCal.set(Calendar.SECOND, cal.get(Calendar.SECOND));
    utcCal.set(Calendar.MILLISECOND, cal.get(Calendar.MILLISECOND));
    
    // Because of the way that java's calendar class works, 
    // the calendar's time value in milliseconds is not recomputed until the next call 
    // to get(), getTime(), getTimeInMillis(), add(), or roll() is made
    utcCal.getTimeInMillis();
  
    // Now convert the timezone to the ORDS timezone
    utcCal.setTimeZone(TimeZone.getTimeZone("UTC"));
    
    String iso_8601Str = getTimestampStr(utcCal, true);
    
    return iso_8601Str;    
  }
  
  /**
   * Converts a java.sql.Date to the iso-8601 Json format to post up to ORDS
   * @param java.sql.Date date
   * @param timeZoneID
   * @return a String timestamp
   */
  public String setDate(java.sql.Date date, String timeZoneID, Calendar cal) {
    
    Timestamp ts = new java.sql.Timestamp(date.getTime());
    return setTimeStamp(ts, timeZoneID, cal);
  }
  
  
  /**
   * Converts a java.sql.Time to the iso-8601 Json format to post up to ORDS
   * @param java.sql.Time time
   * @param timeZoneID
   * @return a String timestamp
   */
  public String setTime(java.sql.Time time, String timeZoneID, Calendar cal) {
    
    Timestamp ts = new java.sql.Timestamp(time.getTime());
    return setTimeStamp(ts, timeZoneID, cal);
  }
  
  /**
   * Helper method to create the timestamp string in the iso-8601 format
   * to post up to ORDS
   * @param Calendar to get timestamp information
   * @param boolean sendToAdhoc- are we going to get or set a Timestamp
   * @return a String timestamp either in the iso 8601 format or in a format that can be easily converted 
   * to java.sql.Timestamp
   */
  protected String getTimestampStr(Calendar cal, boolean sendToAdhoc) {
    // Construct the iso-8601 string to send up to ORDS
    StringBuilder sb = new StringBuilder();
    
    sb.append(cal.get(Calendar.YEAR));
    sb.append('-');
    
    // the month - want to add a '0' if it's a single digit month
    if(cal.get(Calendar.MONTH) < 9) {
      sb.append('0');
    }
    sb.append(cal.get(Calendar.MONTH)+1);
    sb.append('-');
    
    // the date - want to add a '0' if it's a single digit date
    if(cal.get(Calendar.DAY_OF_MONTH) < 10) {
      sb.append('0');
    }
    sb.append(cal.get(Calendar.DAY_OF_MONTH));
    
    // Are we setting a timestamp or getting from a ResultSet?
    if(sendToAdhoc) {
      // we are setting a timestamp
      sb.append('T');
    }
    else {
      // we are getting a timestamp from the ResultSet
      sb.append(' ');
    }
    
    // the hour - want to add a '0' if it's a single digit hour
    if(cal.get(Calendar.HOUR_OF_DAY) < 10) {
      sb.append('0');
    }
    sb.append(cal.get(Calendar.HOUR_OF_DAY));
    sb.append(':');
    
    // the minute - want to add a '0' if it's a single digit minute
    if(cal.get(Calendar.MINUTE) < 10) {
      sb.append('0');
    }
    sb.append(cal.get(Calendar.MINUTE));
    sb.append(':');
    
    // the second - want to add a '0' if it's a single digit second
    if(cal.get(Calendar.SECOND) < 10) {
      sb.append('0');
    }
    sb.append(cal.get(Calendar.SECOND));    

    // Millisecond field
    sb.append('.');
    int millis = cal.get(Calendar.MILLISECOND);
    if(millis<10) { 
      sb.append("00");
    }
    else if (millis < 100) {
      sb.append('0');      
    }
    sb.append(millis);
       
    if(sendToAdhoc) {
      sb.append('Z');
    }
    return sb.toString();
  }
  
  /**
   * Construct a timestamp from an iso-8601 String 
   * using the given <code>java.util.Calendar</code> object
   * @param tsStr
   * @param timeZoneID
   * @param cal the <code>java.util.Calendar</code> object to use in constructing the Timestamp
   * @return
   */
  public java.sql.Timestamp getTimestamp(String tsStr, String timeZoneID, Calendar cal) {
    // Get the local calendar
    Calendar local =  new GregorianCalendar(TimeZone.getDefault());
    // convert String to java.sql.Timestamp
    java.sql.Timestamp rsTimestamp = getTimestamp(tsStr, timeZoneID);
    
    // set local calendar timestamp
    local.setTimeInMillis(rsTimestamp.getTime());
    
    // set fields of the given calendar
    cal.set(Calendar.YEAR, local.get(Calendar.YEAR));
    cal.set(Calendar.MONTH, local.get(Calendar.MONTH));
    cal.set(Calendar.DAY_OF_MONTH, local.get(Calendar.DAY_OF_MONTH));
    cal.set(Calendar.HOUR_OF_DAY, local.get(Calendar.HOUR_OF_DAY));
    cal.set(Calendar.MINUTE, local.get(Calendar.MINUTE));
    cal.set(Calendar.SECOND, local.get(Calendar.SECOND));
    cal.set(Calendar.MILLISECOND, local.get(Calendar.MILLISECOND));
    
    return new Timestamp(cal.getTimeInMillis());
  }
  
  
  /**
   * Construct a java.sql.Date from a iso-8601 String
   * given a <code>java.util.Calendar</code> object
   * @param tsStr
   * @param cal the <code>java.util.Calendar</code> object to use in constructing the Date
   * @return a java.sql.Date date
   */
  public java.sql.Date getDate(String tsStr, String timeZoneID, Calendar cal) {
    java.sql.Timestamp timestamp = getTimestamp(tsStr, timeZoneID, cal);
    java.sql.Date date = new Date(timestamp.getTime());
    
    return date;
  }
  
  /**
   * Construct a java.sql.Date from a "hh:mi:ss" string
   * given a <code>java.util.Calendar</code> object
   * @param tsStr 
   * @param cal the <code>java.util.Calendar</code> object to use in constructing the Time
   * @return a java.sql.Time time
   */
  public java.sql.Time getTime(String tsStr) {
    return java.sql.Time.valueOf(tsStr);
  }
  
  /**
   * Construct a java.sql.Date from a iso-8601 String
   * given a <code>java.util.Calendar</code> object
   * @param tsStr
   * @param timeZoneID
   * @param cal the <code>java.util.Calendar</code> object to use in constructing the Time
   * @return a java.sql.Time time
   */
  public java.sql.Time getTime(String tsStr, String timeZoneID, Calendar cal) {
    // Get the local calendar
    Calendar local =  new GregorianCalendar(TimeZone.getDefault());
    // convert String to java.sql.Timestamp
    java.sql.Timestamp rsTimestamp = getTimestamp(tsStr, timeZoneID);
    
    // set local calendar timestamp
    local.setTimeInMillis(rsTimestamp.getTime());
    
    // set fields of the given calendar
    // Since this is a java.sql.Time object, has to be set to 1970-01-01
    cal.set(Calendar.YEAR, 1970);
    cal.set(Calendar.MONTH, 0);
    cal.set(Calendar.DAY_OF_MONTH, 1);
    cal.set(Calendar.HOUR_OF_DAY, local.get(Calendar.HOUR_OF_DAY));
    cal.set(Calendar.MINUTE, local.get(Calendar.MINUTE));
    cal.set(Calendar.SECOND, local.get(Calendar.SECOND));
    cal.set(Calendar.MILLISECOND, local.get(Calendar.MILLISECOND));
    
    return new Time(cal.getTimeInMillis());
  }
}
