/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.util;

import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Level;
import oracle.dbtools.jdbc.util.LogUtil;
import oracle.dbtools.jdbc.util.RestjdbcResources;

/**
 * @author debjanibiswas
 * This class just contains helper methods for throwing exceptions 
 * that are common across different classes.
 * Uses methods from the LogUtil class.
 */
public class ExceptionUtil {
  /**
   * Helper  method to construct the actual log or exception for each unsupported method
   * @param - String methodName - name of method (with parameters) that's unsupported
   * @throws SQLException 
   */
  public static void logExceptionUnsupported(java.sql.Connection conn, String methodName, String className) throws SQLException {
    LogUtil.logOrException(conn, Level.INFO, 
        // log the message 
        exceptionMessageStr(Defaults.UNSUPPORTED, methodName, className),
        // throw an exception
        new SQLFeatureNotSupportedException(
               exceptionMessageStr(Defaults.UNSUPPORTED, methodName, className), 
               Defaults.UNSUPPORT_SQLSTATE, Defaults.UNSUPPORT_CODE));   
  }
  
  
  /**
   * Returns a string message for unsupported / unimplemented features
   * @param boolean value - unsupport - to check if the feature is unsupported / unimplemented
   * @return String message to log and throw as an exception
   */
  private static String exceptionMessageStr(boolean unsupport, String methodName, String className) {
    // If the feature is not supported
    if(unsupport) {
      return RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, methodName, className);
    }
    // If the feature is supported but not yet implemented
    else {
      return RestjdbcResources.format(RestjdbcResources.RESTJDBC_NOTIMPLEMENTED, methodName, className);
    }   
  }
  
  /**
   * Construct any SQL exception given an errorCode
   * @param - errorCode
   */
  public static void throwSQLException(int errorCode) throws SQLException{
    String ORA_error = "ORA_" + errorCode;
    throw new SQLException((RestjdbcResources.getString(ORA_error)), 
                            SQLStateMapping.getSQLState(errorCode), errorCode);
  }
  
  /**
   * Construct any SQL exception given an errorCode and methodName
   */
  public static void throwSQLException(int errorCode, String methodName) throws SQLException{
    String ORA_error = "ORA_" + errorCode;
    throw new SQLException((RestjdbcResources.getString(ORA_error)+": " + methodName),            
                            SQLStateMapping.getSQLState(errorCode), errorCode);
  }
  
  
  /**
   * SQL Exception for unsupported methods
   */
  public static void throwSQLException(String methodName, String className) throws SQLException {
    throw new RestJdbcUnsupportedException(RestjdbcResources.format(RestjdbcResources.RESTJDBC_UNSUPPORTED, 
        methodName, className));
  }
  /**
   * Construct any SQL exception given an errorCode exception e
   */
  public static void throwSQLException(int errorCode, Exception e) throws SQLException{
    String ORA_error = "ORA_" + errorCode;
    throw new SQLException((RestjdbcResources.getString(ORA_error)), 
              SQLStateMapping.getSQLState(errorCode), errorCode, e); 
  }
}
