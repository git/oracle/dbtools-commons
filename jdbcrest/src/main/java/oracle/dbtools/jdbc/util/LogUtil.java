/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.util;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.Connection;

import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;

public class LogUtil {
    private static final Logger logger=Logger.getLogger(LogUtil.class.getName());
    private static final String myName = LogUtil.class.getName();
    private static final String SUBSEP="\\Q?\\E";  //$NON-NLS-1$
    private static final String SUBEQ="\\Q=\\E";   //$NON-NLS-1$
    private static final String LOGORBOTH="logorboth"; //$NON-NLS-1$
    private static final String LOGORBOTHDEFAULT="log"; //$NON-NLS-1$
    private static final String[] LOGORBOTHOPTS={"log","both"}; // is name for log is name for both //$NON-NLS-1$ //$NON-NLS-2$
    private static final Level LOGORBOTHDEFAULTLOGLEVEL=Level.INFO; //if its just info these 'almost exceptions' will get lost in Turlochs 'info' logs whenever someone turns on verbose.
    /**
     * verb basic log - class:method:line filled in from stack - 3 as 
     * indirect calling requires extra depth in stack lookup.
     */
    public static void log() {// very lazy
        log (3,null,null) ;//go one up the stack trace for caller.
    }
    /**
     * Wrapper function to allow level to be defaulted
     * @param up - how far to look up the stack (optional)
     * @param message - message for log (optional will give class:method:line no:message)
     * @param e - exception for log (optional)
     */
    public static void log(Integer up, String message, Exception e) {// very lazy
        if (up==null){
            up=2;
        }
        up++;
        log (up,Level.INFO,message,e);
    }
    /**
     * 
     * @param up - how far to look up the stack (optional)
     * @param inLevel - will default to info
     * @param message - message for log (optional will give class:method:line no:message)
     * @param e - exception for log (optional)
     */
    public static void log(Integer up, Level inLevel, String message, Exception e) {// very lazy
        String called = null;
        String realMessage="No Message";//need to put in non orest arb ORestjdbcResources.getString(ORestjdbcResources.ORESTJDBC_NOMESSAGE);
        Integer doUp=up; 
        if (up==null) {
            //called direct if one deeper input 3
            doUp=2;
        }
        if (message!=null) {
            realMessage=":"+message; //$NON-NLS-1$
        }
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        if ((stackTraceElements != null) && (stackTraceElements.length > doUp)) {
            StackTraceElement el = stackTraceElements[doUp];
            if (el != null) {
                realMessage = el.getClassName() + ":" + el.getMethodName() + ":" + el.getLineNumber()+":"+realMessage; //$NON-NLS-1$ //$NON-NLS-2$  //$NON-NLS-3$
            }
        }
        if (e!=null) {
            logger.log(inLevel, realMessage, e);
        } else {
            logger.log(inLevel, realMessage);
        }
    }
    /**
     * Log driver loaded. Initially thought to use to initialise LogUtil - now use normal logging. Note LogUtil now used by base driver. 
     * In the future if LogUtil requires initialisation - set on from generic rest driver. 
     */
    public static void setOn() {
        log (3,"ORest driver loaded"/*need to pass in ORestjdbcResources.getString(ORestjdbcResources.ORESTJDBC_DRIVERLOADED)*/,null) ;
    }
    /**
     * 
     * @param url (base rest driver) url with optional ?key=value
     * @param info properties passed in on connection initialisation
     * @return info updated with key=value pair
     * @throws SQLException on url format error
     */
    public static String getUrlProperties(String url, Properties info) throws SQLException{
        // TODO Auto-generated method stub
        String[] afterSplit=url.split(SUBSEP);
        
        for  (int i=1;i<afterSplit.length;i++) {
            //assumpttion no quoting initially url properties do
            //not contain subsep or subeql (initially ? and =)
            String[] subsplit=afterSplit[i].split(SUBEQ);
            if ((subsplit==null)||(subsplit.length!=2)) {
                //value = nothing may error might even want this
                throw new SQLException("URL format error");
            }
            info.put(subsplit[0],subsplit[1]);
            //only one setting so far vlidate on connect
            if (subsplit[0].equals(LOGORBOTH)){
                String setting=subsplit[1];
                if ((setting==null)||!((setting.equals(LOGORBOTHOPTS[0])||(setting.equals(LOGORBOTHOPTS[1]))))) {
                    throw new SQLException("URL format error");
                }    
            }
        }
        return afterSplit[0];
    }
    /**
     * All parameters can be null
     * @param conn the connection (to get the per connection key/value pair)
     * @param level log level
     * @param message string to use (or no "message") - with class:method:linenumber filled in 
     * @param e exception for stack trace on logging
     * @throws SQLException 
     */
    public static void logOrException(Connection conn, Level level, String message, Exception e) throws SQLException {
        if (level==null){
            level=LOGORBOTHDEFAULTLOGLEVEL;//Level.SEVERE;
        }
        String setting = null;
        if ((conn==null)||(conn.getClientInfo()==null)){
            setting=LOGORBOTHDEFAULT;
        } else {
            setting = conn.getClientInfo().getProperty(LOGORBOTH);
            if (setting==null) {
                setting=LOGORBOTHDEFAULT;
            }
        }
        boolean both=true;
        if ((setting.equals(LOGORBOTHOPTS[0]))) {
            both=false;
        }
        //go 3 levels up stack trace for class/method/line number - if called directly default = 2 
       log(3,level,message,null);
        if (both) {
            if (message==null) {
                message = "Logged - unsupported Exception";
            }
            // We might throw a Rest JDBC exception
            if(e instanceof RestJdbcNotImplementedException) {
              throw (RestJdbcNotImplementedException) e;
            }
            else if(e instanceof RestJdbcUnsupportedException) {
              throw (RestJdbcUnsupportedException) e;
            }
            else if(e instanceof java.sql.SQLFeatureNotSupportedException) {
              throw (java.sql.SQLFeatureNotSupportedException) e ;
            }
            else if (e != null) {
                throw new UnsupportedOperationException(e);
            } else {
                throw new UnsupportedOperationException(message);
            }
        }
    }
}
