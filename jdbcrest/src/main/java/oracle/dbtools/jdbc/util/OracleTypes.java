/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Oracle types.
 * <p>
 * This interface defines constants that are used to identify SQL types.
 * The actual type constant values are equivalent to those in XOPEN.
 */
public abstract class OracleTypes
{

  // 
  // IMPORTANT !!!
  // Note that a lot of the constants below are copied from
  // java.sql.Types.  It is not possible to subclass java.sql.Types
  // because java.sql.Types has defined a private constructor to
  // prevent instantiation.
  // 

  public final static int BIT = -7;
  public final static int TINYINT = -6;
  public final static int SMALLINT = 5;
  public final static int INTEGER = 4;
  public final static int BIGINT = -5;

  public final static int FLOAT = 6;
  public final static int REAL = 7;
  public final static int DOUBLE = 8;

  public final static int NUMERIC = 2;
  public final static int DECIMAL = 3;

  public final static int CHAR = 1;
  public final static int VARCHAR = 12;
  public final static int LONGVARCHAR = -1;

  public final static int DATE = 91;
  public final static int TIME = 92;
  public final static int TIMESTAMP = 93;

  /**
   * PLSQL_BOOLEAN binds BOOLEAN type for input/output 
   * parameters when executing a PLSQL function/procedure.
   * Only available for thin driver.
   * 
   * @since 12.2
   */
  public final static int PLSQL_BOOLEAN     = 252;
  
  /**
   * @deprecated since 9.2.0. Use OracleTypes.TIMESTAMP instead.
   */
  public final static int TIMESTAMPNS       = -100;   // Oracle TIMESTAMP
  public final static int TIMESTAMPTZ       = -101;
  public final static int TIMESTAMPLTZ      = -102;  // Oracle TIMESTAMPLTZ

  public final static int INTERVALYM        = -103;    // Oracle SQLT_INTERVAL_YM
  public final static int INTERVALDS        = -104;    // Oracle SQLT_INTERVAL_DS

  public final static int BINARY            = -2;
  public final static int VARBINARY         = -3;
  public final static int LONGVARBINARY     = -4;
  public final static int LONGRAW           = -44;

  public final static int ROWID             = java.sql.Types.ROWID;
  public final static int CURSOR            = -10;
 // public final static int REF_CURSOR        = java.sql.Types.REF_CURSOR;
  public final static int BLOB              = java.sql.Types.BLOB;                          // JDBC 2.0
  public final static int CLOB              = java.sql.Types.CLOB;          // JDBC 2.0
  public final static int BFILE             = -13;

  public final static int STRUCT            = java.sql.Types.STRUCT;        // JDBC 2.0
  public final static int ARRAY             = java.sql.Types.ARRAY;         // JDBC 2.0
  public final static int REF               = 2006;           // JDBC 2.0

  public final static int NCHAR             = java.sql.Types.NCHAR;          // JDBC 4.0
  public final static int NCLOB             = java.sql.Types.NCLOB;         // JDBC 4.0
  public final static int NVARCHAR          = java.sql.Types.NVARCHAR;       // JDBC 4.0
  public final static int LONGNVARCHAR      = java.sql.Types.LONGVARCHAR;   // JDBC 4.0
  public final static int SQLXML            = java.sql.Types.SQLXML;        // JDBC 4.0

  // Oracle specific
  public final static int OPAQUE            = 2007;        // TODO: conflicts?

  // Oracle specific
  public final static int JAVA_STRUCT       = 2008;   // TODO: conflicts?

  public final static int JAVA_OBJECT       = 2000;

  // Oracle specific
  public final static int PLSQL_INDEX_TABLE = -14;


  // Oracle specific
  // See $SRCHOME/rdbms/include/dtydef.h
  // Changes here should also be made in oracle.jdbc.driver.Accessor
  // and in oracle.jdbc.driver.T4C8TTIdty
  public final static int BINARY_FLOAT      = 100;   // DTYIBFLOAT--the cannonical form
  public final static int BINARY_DOUBLE     = 101;  // DTYIBDOUBLE--the canonical form

  public final static int NULL              = 0;

  /**
   * NUMBER shares same value as NUMERIC as it is synonym defined
   * for convenience when using the oracle.sql.NUMBER type.
   * Internally, code need only worry about NUMERIC.
   */
  public final static int NUMBER            = NUMERIC;

  /**
   * RAW shares same value as BINARY as it is synonym defined
   * for convenience when using the oracle.sql.RAW type.
   */
  public final static int RAW               = VARBINARY;

  /**
   * OTHER indicates that the SQL type is database specific and
   * gets mapped to a Java object which can be accessed via
   * getObject and setObject.
   */
  public final static int OTHER             = java.sql.Types.OTHER;

  /**
   * Use this type when binding to a CHAR column in the where
   * clause of a Select statement. A non padded comparision will
   * be done unlike in CHAR and VARCHAR case. Not particularly
   * needed for an insert as the database will pad it.
   * This type is used for bind only. It cannot be used for
   * define and registerOutParameter.
   */
  public final static int FIXED_CHAR        = 999;

  /**
   * The constant in the Java programming language, somtimes referred to
   * as a type code, that identifies the generic SQL type <code>DATALINK</code>.
   * 
   * @since 9.0.2
   */
  public final static int DATALINK          = 70;

  /**
   * The constant in the Java programming language, somtimes referred to
   * as a type code, that identifies the generic SQL type <code>BOOLEAN</code>.
   * 
   * @since 9.0.2
   */
  public final static int BOOLEAN           = java.sql.Types.BOOLEAN;

  /**
   * Map a sqlType to the name used in the JSON bind request
   */
  private final static Map<Integer, String> sqlTypeToName;
  static {
    Map<Integer, String> sqlTypeToName_ = new HashMap<Integer, String>();
    sqlTypeToName_.put(java.sql.Types.TINYINT, "NUMBER");
    sqlTypeToName_.put(java.sql.Types.SMALLINT, "NUMBER");
    sqlTypeToName_.put(java.sql.Types.INTEGER, "NUMBER");
    sqlTypeToName_.put(java.sql.Types.BIGINT, "NUMBER");
    sqlTypeToName_.put(java.sql.Types.REAL, "NUMBER");
    sqlTypeToName_.put(java.sql.Types.NUMERIC, "NUMBER");
    sqlTypeToName_.put(java.sql.Types.DOUBLE, "NUMBER");
    sqlTypeToName_.put(java.sql.Types.DECIMAL, "NUMBER");
    sqlTypeToName_.put(java.sql.Types.FLOAT, "FLOAT");
    sqlTypeToName_.put(java.sql.Types.VARCHAR, "VARCHAR");
    sqlTypeToName_.put(java.sql.Types.LONGVARCHAR, "VARCHAR");
    sqlTypeToName_.put(java.sql.Types.DATALINK, "VARCHAR");
    sqlTypeToName_.put(java.sql.Types.CHAR, "VARCHAR");
    sqlTypeToName_.put(java.sql.Types.DATE, "DATE");
    sqlTypeToName_.put(java.sql.Types.TIME, "TIME");
    sqlTypeToName_.put(java.sql.Types.TIMESTAMP, "TIMESTAMP");
    sqlTypeToName_.put(java.sql.Types.BLOB, "BLOB");
    sqlTypeToName_.put(java.sql.Types.CLOB, "CLOB");
    sqlTypeToName_.put(java.sql.Types.BOOLEAN, "BOOLEAN");
    sqlTypeToName_.put(java.sql.Types.BIT, "BIT");
    sqlTypeToName = sqlTypeToName_;
  }

  /**
   *  Map a sqlType to the name used in the JSON bind request
   */
  public static String getSQLTypeName(final int sqlType) {
    return sqlTypeToName.get(sqlType);
  }


}