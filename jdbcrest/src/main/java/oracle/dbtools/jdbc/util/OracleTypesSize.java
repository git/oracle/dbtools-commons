/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.util;

import java.util.HashMap;
import java.util.Map;

public class OracleTypesSize {
	public static final int SIZE_DATE = 7;
	public static final int SIZE_TIMESTAMP = 11;
	public static final int SIZE_TIMESTAMPTZ = 13;
	public static final int SIZE_TIMESTAMPLTZ = 11;
	
	public static final int MAX_CHUNK_SIZE = 32768;

	public static Map<Integer, Integer> maxSizes = new HashMap<Integer, Integer>();

	// populate the map
	static {
		maxSizes.put(OracleTypes.VARCHAR, 0);
		maxSizes.put(OracleTypes.NVARCHAR, 0);
		maxSizes.put(OracleTypes.BIT, 0);
		maxSizes.put(OracleTypes.TINYINT, 0);
		maxSizes.put(OracleTypes.SMALLINT, 0);
		maxSizes.put(OracleTypes.INTEGER, 0);
		maxSizes.put(OracleTypes.BIGINT, 0);
		maxSizes.put(OracleTypes.FLOAT, 0);
		maxSizes.put(OracleTypes.REAL, 0);
		maxSizes.put(OracleTypes.DOUBLE, 0);
		maxSizes.put(OracleTypes.NUMERIC, 0);
		maxSizes.put(OracleTypes.DECIMAL, 0);
		maxSizes.put(OracleTypes.CHAR, 0);
		maxSizes.put(OracleTypes.VARCHAR, 0);
		maxSizes.put(OracleTypes.LONGVARCHAR, 0);
		
		maxSizes.put(OracleTypes.DATE,SIZE_DATE);
		maxSizes.put(OracleTypes.TIME, SIZE_DATE);
		maxSizes.put(OracleTypes.TIMESTAMP, SIZE_TIMESTAMP);
		maxSizes.put(OracleTypes.PLSQL_BOOLEAN, 0);
		/**
		 * @deprecated since 9.2.0. Use OracleTypes.TIMESTAMP instead.
		 */
		maxSizes.put(OracleTypes.TIMESTAMPNS, SIZE_TIMESTAMP);// Oracle TIMESTAMP
		maxSizes.put(OracleTypes.TIMESTAMPTZ, SIZE_TIMESTAMPTZ); // Oracle TIMESTAMPTZ
		maxSizes.put(OracleTypes.TIMESTAMPLTZ, SIZE_TIMESTAMPLTZ); // Oracle TIMESTAMPLTZ
		maxSizes.put(OracleTypes.INTERVALYM, 0); // Oracle SQLT_INTERVAL_YM
		maxSizes.put(OracleTypes.INTERVALDS, 0); // Oracle SQLT_INTERVAL_DS
		maxSizes.put(OracleTypes.BINARY, 0);
		maxSizes.put(OracleTypes.VARBINARY, 0);
		maxSizes.put(OracleTypes.LONGVARBINARY, 0);
		maxSizes.put(OracleTypes.ROWID, 0);
		maxSizes.put(OracleTypes.CURSOR, 0);
		maxSizes.put(OracleTypes.BLOB, MAX_CHUNK_SIZE); // JDBC 2.0
		maxSizes.put(OracleTypes.CLOB, MAX_CHUNK_SIZE); // JDBC 2.0
		maxSizes.put(OracleTypes.BFILE, MAX_CHUNK_SIZE);
		maxSizes.put(OracleTypes.STRUCT, 0); // JDBC 2.0
		maxSizes.put(OracleTypes.ARRAY, 0); // JDBC 2.0
		maxSizes.put(OracleTypes.REF, 0); // JDBC 2.0
		maxSizes.put(OracleTypes.NCHAR, 0); // JDBC 4.0
		maxSizes.put(OracleTypes.NCLOB, MAX_CHUNK_SIZE); // JDBC 4.0
		maxSizes.put(OracleTypes.NVARCHAR, 0); // JDBC 4.0
		maxSizes.put(OracleTypes.LONGNVARCHAR, 0); // JDBC 4.0
		maxSizes.put(OracleTypes.SQLXML, 0); // JDBC 4.0
		// Oracle specific
		maxSizes.put(OracleTypes.OPAQUE, 0); // TODO: conflicts?
		// Oracle specific
		maxSizes.put(OracleTypes.JAVA_STRUCT, 0); // TODO: conflicts?
		maxSizes.put(OracleTypes.JAVA_OBJECT, 0);
		// Oracle specific
		maxSizes.put(OracleTypes.PLSQL_INDEX_TABLE, 0);
		maxSizes.put(OracleTypes.BINARY_FLOAT, 0); // DTYIBFLOAT--the cannonical form
		maxSizes.put(OracleTypes.BINARY_DOUBLE, 0); // DTYIBDOUBLE--the canonical form
		maxSizes.put(OracleTypes.NULL, 0);
		maxSizes.put(OracleTypes.NUMBER, 0);
		maxSizes.put(OracleTypes.RAW, 0);
		maxSizes.put(OracleTypes.OTHER, 0);
		maxSizes.put(OracleTypes.FIXED_CHAR, 0);
		maxSizes.put(OracleTypes.DATALINK, 0);
		maxSizes.put(OracleTypes.BOOLEAN, 0);
	}
	
	static public int getMaxSize(int oracleType){
		if(maxSizes.containsKey(oracleType)){
			return maxSizes.get(oracleType);
		} else {
			return 0;
		}
	}
}
