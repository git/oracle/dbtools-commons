/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.func;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.FailTest;
import oracle.dbtools.unittest.categories.PassTest;
import oracle.dbtools.unittest.categories.UnsupportedTest;

public class CallableStatementTest extends JDBCTestCase {

	@Category(PassTest.class)
	@Test
	public void testExecute() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		ResultSet oraclers = null, restrs = null;
		String oracle_res = "", rest_res = "";

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "create table ocstmt_execute (col1 varchar2(200) )");
			Util.doSQL(restConn, "create table rcstmt_execute (col1 varchar2(200) )");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}
		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn.prepareCall("declare a integer;begin a := 10;"
							          + "insert into ocstmt_execute values ('SUCCESS');end;");
			oraclecstmt.execute();
			oraclers = oraclecstmt.executeQuery("select * from ocstmt_execute");
			if (oraclers.next()) {
				oracle_res = oraclers.getString(1);
			}
			oraclers.close();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			Util.doSQL(restConn, "create table rcstmt_execute (col1 varchar2(200) )");

			restcstmt = restConn.prepareCall("declare a integer;begin a := 10;"
					                    + "insert into rcstmt_execute values ('SUCCESS');end;");
			restcstmt.execute();
			restrs = restcstmt.executeQuery("select * from rcstmt_execute");
			if (restrs.next()) {
				rest_res = restrs.getString(1);
			}
			restrs.close();
		} catch (SQLException e) {
			fail(e.getMessage());
		}

		finally {
			try {
				Util.doSQL(restConn, "drop table ocstmt_execute purge");
				Util.doSQL(restConn, "drop table rcstmt_execute purge");
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_res, rest_res);
	}

	/*
	 * Test to register out parameters.
	 */

	@Category(PassTest.class)
	@Test
	public void testRegisterParameters() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		int oracle_res_int = 0, rest_res_int = 0;
		String rest_res_strg = "", oracle_res_strg = "";

		// Oracle thin driver

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn.prepareCall(" DECLARE "
					+ " temp VARCHAR2(255); " + " subtype onetype is NUMBER;"
					+ " subtype twotype is temp%TYPE;" + " a1 onetype; "
					+ " b1 twotype; " + " BEGIN " + "   a1 := 0; "
					+ "   ? := a1 + 3; " + "   b1 := ?; " + "   ? := b1; "
					+ " END;");
			oraclecstmt.registerOutParameter(1, Types.INTEGER);
			oraclecstmt.setString(2, "testing subtype");
			oraclecstmt.registerOutParameter(3, Types.VARCHAR);

			oraclecstmt.execute();

			oracle_res_int = oraclecstmt.getInt(1);
			oracle_res_strg = oraclecstmt.getString(3);

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver

		try {
			restConn = getRestConnection();
			restcstmt = restConn.prepareCall(" DECLARE "
					+ " temp VARCHAR2(255); " + " subtype onetype is NUMBER;"
					+ " subtype twotype is temp%TYPE;" + " a1 onetype; "
					+ " b1 twotype; " + " BEGIN " + "   a1 := 0; "
					+ "   ? := a1 + 3; " + "   b1 := ?; " + "   ? := b1; "
					+ " END;");
			restcstmt.registerOutParameter(1, Types.INTEGER);
			restcstmt.setString(2, "testing subtype");
			restcstmt.registerOutParameter(3, Types.VARCHAR);
			restcstmt.execute();
			rest_res_int = restcstmt.getInt(1);
			rest_res_strg = restcstmt.getString(3);

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_res_int, rest_res_int);
		assertEquals(oracle_res_strg, rest_res_strg);

	}

	/*
	 * Test set and get method of int datatype.
	 */

	@Category(UnsupportedTest.class)
	@Test
	public void testSetMethod1_int() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		Statement ostmt = null, rstmt = null;
		ResultSet oraclers = null, restrs = null;
		int oracle_int_result = 0, rest_int_result = 0;
		String oracle_strg_result = "", rest_strg_result = "";
		String oracle_dropsql = "drop table ocstmt_Strg purge";
		String rest_dropsql = "drop table rcstmt_Strg1 purge";
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn,oracle_dropsql);
			Util.doSQL(restConn,rest_dropsql);
			Util.doSQL(restConn,
					"create table rcstmt_Strg1 (id number(10), name varchar2(200))");
			Util.doSQL(restConn,
					"create or replace procedure rcstmt_Strg1_proc "
							+ "(id IN NUMBER, " + "name IN VARCHAR2) " + "is "
							+ "begin "
							+ "insert into rcstmt_Strg1 values(id,name); "
							+ "end;  ");
			Util.doSQL(restConn,
					"create table ocstmt_Strg (id number(10), name varchar2(200))");
			Util.doSQL(restConn,
					"create or replace procedure ocstmt_Strg_proc "
							+ "(id IN NUMBER, " + "name IN VARCHAR2) " + "is "
							+ "begin "
							+ "insert into ocstmt_Strg values(id,name); "
							+ "end;  ");

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}
		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{call ocstmt_Strg_proc(?,?)}");
			oraclecstmt.setInt(1, 200);
			oraclecstmt.setString(2, "Garry");
			oraclecstmt.execute();
			ostmt = oracleConn.createStatement();
			oraclers = ostmt.executeQuery("select * from ocstmt_Strg order by id");
			if (oraclers.next()) {
				oracle_int_result = oraclers.getInt(1);
				oracle_strg_result = oraclers.getString(2);
			}
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{call rcstmt_Strg1_proc(?,?)}");
			restcstmt.setInt(1, 200);
			restcstmt.setString(2, "Garry");
			restcstmt.execute();
			rstmt = restConn.createStatement();
			restrs = rstmt.executeQuery("select * from rcstmt_Strg1 order by id");
			if (restrs.next()) {
				rest_int_result = restrs.getInt(1);
				rest_strg_result = restrs.getString(2);
			}
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(oracleConn, oracle_dropsql);
				Util.doSQL(restConn, rest_dropsql);
				close(oraclers, restrs);
				oraclecstmt.close();
				restcstmt.close();
				close(ostmt, rstmt);
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}

		assertEquals(oracle_int_result, rest_int_result);
		assertEquals(oracle_strg_result, rest_strg_result);
	}

	/*
	 * Register Out parameter with parameterIndex.
	 */
	@Category(PassTest.class)
	@Test
	public void testSetMethod2_int() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		int oracle_int_result = 0, rest_int_result = 0;

		try {
			oracleConn = getOracleConnection();
			Util.doSQL(oracleConn,
					"create or replace function oracle_setmtd_proc"
							+ "(n1 in number,n2 in number)" + "return number "
							+ "is " + "temp number(8); " + "begin "
							+ "temp :=n1+n2; " + "return temp; " + "end; ");
			oraclecstmt = oracleConn
					.prepareCall("{?= call oracle_setmtd_proc(?,?)}");
			oraclecstmt.setInt(2, 10);
			oraclecstmt.setInt(3, 43);
			oraclecstmt.registerOutParameter(1, Types.INTEGER);
			oraclecstmt.execute();
			oracle_int_result = oraclecstmt.getInt(1);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "create or replace function rest_setmtd_proc"
					+ "(n1 in number,n2 in number)" + "return number " + "is "
					+ "temp number(8); " + "begin " + "temp :=n1+n2; "
					+ "return temp; " + "end; ");
			restcstmt = restConn.prepareCall("{?= call rest_setmtd_proc(?,?)}");
			restcstmt.setInt(2, 10);
			restcstmt.setInt(3, 43);
			restcstmt.registerOutParameter(1, Types.INTEGER);
			restcstmt.execute();
			rest_int_result = restcstmt.getInt(1);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_result, rest_int_result);
	}

	/*
	 * Testing set and get method of String datatype.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod3_string() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		String oracle_strg_result1 = "", rest_strg_result1 = "";
		String oracle_strg_result2 = "", rest_strg_result2 = "";
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "create or replace function funcvarchar2"
					+ "  (x in varchar2, y in out varchar2) "
					+ "  return varchar2 as " + "    z varchar2(32767);"
					+ "  begin " + "    z := y; y := x; return (z);" + "  end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{ ? = call funcvarchar2(?, ?) }");
			oraclecstmt.setString(2, "Rest JDBC QA");
			oraclecstmt.setString(3, "Testing Rest JDBC");
			oraclecstmt.registerOutParameter(1, Types.VARCHAR);
			oraclecstmt.registerOutParameter(3, Types.VARCHAR);
			oraclecstmt.execute();
			oracle_strg_result1 = oraclecstmt.getString(3);
			oracle_strg_result2 = oraclecstmt.getString(1);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{ ? = call funcvarchar2(?, ?) }");
			restcstmt.setString(2, "Rest JDBC QA");
			restcstmt.setString(3, "Testing Rest JDBC");
			restcstmt.registerOutParameter(1, Types.VARCHAR);
			restcstmt.registerOutParameter(3, Types.VARCHAR);
			restcstmt.execute();
			rest_strg_result1 = restcstmt.getString(3);
			rest_strg_result2 = restcstmt.getString(1);
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_strg_result1, rest_strg_result1);
		assertEquals(oracle_strg_result2, rest_strg_result2);
	}

	/*
	 * Test set and get methods of short datatype
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod4_short() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		Short oracle_strg_result1 = 0, rest_strg_result1 = 0;
		Short oracle_strg_result2 = 0, rest_strg_result2 = 0;
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "create or replace function funcvarchar2"
					+ "  (x in number, y in out number) "
					+ "  return number as " + "    z number;" + "  begin "
					+ "    z := y; y := x; return (z);" + "  end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{ ? = call funcvarchar2(?, ?) }");
			oraclecstmt.setShort(2, (short) 32767);
			oraclecstmt.setShort(3, (short) -1);
			oraclecstmt.registerOutParameter(1, Types.SMALLINT);
			oraclecstmt.registerOutParameter(3, Types.SMALLINT);
			oraclecstmt.execute();
			oracle_strg_result1 = oraclecstmt.getShort(3);
			oracle_strg_result2 = oraclecstmt.getShort(1);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{ ? = call funcvarchar2(?, ?) }");
			restcstmt.setShort(2, (short) 32767);
			restcstmt.setShort(3, (short) -1);
			restcstmt.registerOutParameter(1, Types.SMALLINT);
			restcstmt.registerOutParameter(3, Types.SMALLINT);
			restcstmt.execute();
			rest_strg_result1 = restcstmt.getShort(3);
			rest_strg_result2 = restcstmt.getShort(1);
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_strg_result1, rest_strg_result1);
		assertEquals(oracle_strg_result2, rest_strg_result2);
	}

	/*
	 * Test set and get method of Long datatype. Assertion Failed Expected is
	 * Test setLong and getLong method using parameterIndex.
	 * Registerout parameter with Type.BIGINT.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod5_Long() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		long oracle_long_result1 = 0, rest_long_result1 = 0;
		long oracle_long_result2 = 0, rest_long_result2 = 0;
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "create or replace function funcvarchar2"
					+ "  (x in number, y in out number) "
					+ "  return number as " + "    z number;" + "  begin "
					+ "    z := y; y := x; return (z);" + "  end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{ ? = call funcvarchar2(?, ?) }");
			oraclecstmt.setLong(2, 9223372036854775807L);
			oraclecstmt.setLong(3, 0);
			oraclecstmt.registerOutParameter(1, Types.BIGINT);
			oraclecstmt.registerOutParameter(3, Types.BIGINT);
			oraclecstmt.execute();
			oracle_long_result1 = oraclecstmt.getLong(3);
			oracle_long_result2 = oraclecstmt.getLong(1);
			/*
			 * System.out.println("getString(3) = " + oraclecstmt.getLong(3));
			 * System.out.println("getString(1) = " + oraclecstmt.getLong(1));
			 */
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{ ? = call funcvarchar2(?, ?) }");
			restcstmt.setLong(2, 9223372036854775807L);
			restcstmt.setLong(3, 0);
			restcstmt.registerOutParameter(1, Types.BIGINT);
			restcstmt.registerOutParameter(3, Types.BIGINT);
			restcstmt.execute();
			rest_long_result1 = restcstmt.getLong(3);
			rest_long_result2 = restcstmt.getLong(1);
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_long_result1, rest_long_result1);
		assertEquals(oracle_long_result2, rest_long_result2);
	}

	/*
	 * Test set/get method of float datatype with parameter Index.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod6_Float() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		float oracle_float_result1 = 0, rest_float_result1 = 0;
		float oracle_float_result2 = 0, rest_float_result2 = 0;
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "create or replace function funcvarchar2"
					+ "  (x in number, y in out number) "
					+ "  return number as " + "    z number;" + "  begin "
					+ "    z := y; y := x; return (z);" + "  end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{ ? = call funcvarchar2(?, ?) }");
			oraclecstmt.setFloat(2, (float) 2001.37);
			oraclecstmt.setFloat(3, 0);
			oraclecstmt.registerOutParameter(1, Types.FLOAT);
			oraclecstmt.registerOutParameter(3, Types.FLOAT);
			oraclecstmt.execute();
			oracle_float_result1 = oraclecstmt.getFloat(3);
			oracle_float_result2 = oraclecstmt.getFloat(1);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{ ? = call funcvarchar2(?, ?) }");
			restcstmt.setFloat(2, (float) 2001.37);
			restcstmt.setFloat(3, 0);
			restcstmt.registerOutParameter(1, Types.FLOAT);
			restcstmt.registerOutParameter(3, Types.FLOAT);
			restcstmt.execute();
			rest_float_result1 = restcstmt.getFloat(3);
			rest_float_result2 = restcstmt.getFloat(1);
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_float_result1, rest_float_result1);
		assertEquals(oracle_float_result2, rest_float_result2);
	}

	/*
	 * setTimestamp method. expected:<2017-05-01 12:31:56.78>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetMethod7_Timestamp() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		Timestamp ts = Timestamp.valueOf("2017-05-01 12:31:56.78"), oracle_ts = null, rest_ts = null;
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn,
					"create or replace procedure test_ts (c1 in out TIMESTAMP) \n"
							+ "is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			// oraclecstmt = oracleConn.prepareCall("{call test_ts (?)}");
			oraclecstmt = oracleConn.prepareCall("{call test_ts (:c1)}");
			oraclecstmt.setTimestamp("c1", ts);
			oraclecstmt.registerOutParameter("c1", Types.TIMESTAMP);

			oraclecstmt.executeUpdate();
			oracle_ts = oraclecstmt.getTimestamp("c1");
			// System.out.println("Timestamp value"
			// + oraclecstmt.getTimestamp("c1").toString());
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{call test_ts (:c1)}");
			restcstmt.setTimestamp("c1", ts);
			restcstmt.registerOutParameter("c1", Types.TIMESTAMP);
			restcstmt.executeUpdate();
			rest_ts = restcstmt.getTimestamp("c1");
			// System.out.println("Timestamp value"
			// + restcstmt.getTimestamp("c1").toString());
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_ts, rest_ts);
	}

	/*
	 * Test setDate method. expected:<2017-07-01>
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod8_Date() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		Date oracle_dt = null, rest_dt = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn,
					"create or replace procedure test_dt (c1 in out DATE) \n"
							+ "is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn.prepareCall("{call test_dt (:c1)}");
			oraclecstmt.setDate("c1", Date.valueOf("2017-07-01"));
			oraclecstmt.registerOutParameter("c1", Types.DATE);

			oraclecstmt.executeUpdate();
			oracle_dt = oraclecstmt.getDate("c1");
			// System.out.println("Date value"
			// + oraclecstmt.getDate("c1").toString());
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{call test_dt (:c1)}");
			restcstmt.setDate("c1", Date.valueOf("2017-07-01"));
			restcstmt.registerOutParameter("c1", Types.DATE);

			restcstmt.executeUpdate();
			rest_dt = restcstmt.getDate("c1");
			// System.out.println("Date value"
			// + restcstmt.getDate("c1").toString());
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_dt, rest_dt);
	}

	/*
	 * Test setTime method. expected:<10:10:10>
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod9_Time() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		Time oracle_time = null, rest_time = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn,
					"create or replace procedure test_time (c1 in out DATE) \n"
							+ "is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn.prepareCall("{call test_time (:c1)}");
			oraclecstmt.setTime("c1", Time.valueOf("10:10:10"));
			oraclecstmt.registerOutParameter("c1", Types.TIME);

			oraclecstmt.executeUpdate();
			oracle_time = oraclecstmt.getTime("c1");
			// System.out.println("Date value"
			// + oraclecstmt.getTime("c1").toString());
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{call test_time (:c1)}");
			restcstmt.setTime("c1", Time.valueOf("10:10:10"));
			restcstmt.registerOutParameter("c1", Types.TIME);
			restcstmt.executeUpdate();
			rest_time = restcstmt.getTime("c1");
			// System.out.println("Time value"
			// + restcstmt.getTime("c1").toString());
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_time, rest_time);
	}

	/*
	 * Test set/get method of Timestamp, date and time using parameter Index.
	 * expected = <2017-05-01 12:31:56.78>
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod10_Time() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		Time oracle_time = null, rest_time = null;

		TimeZone tz = TimeZone.getDefault();
		tz.setID("US/Pacific");
		Calendar gcal = new GregorianCalendar(tz);
		Timestamp ts = Timestamp.valueOf("2017-05-01 12:31:56.78");

		String oracle_res1 = "", oracle_res2 = "", oracle_res3 = "", oracle_res4 = "";
		String rest_res1 = "", rest_res2 = "", rest_res3 = "", rest_res4 = "";
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(
					restConn,
					"create or replace procedure test_alltime (c1 in out TIMESTAMP WITH LOCAL TIME ZONE, \n"
							+ "c2 in out TIMESTAMP WITH TIME ZONE, \n"
							+ "c3 in out TIMESTAMP, \n"
							+ "c4 in out Date) \n"
							+ "is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{call test_alltime (?,?,?,?)}");
			oraclecstmt.setTimestamp(1, ts);
			oraclecstmt.registerOutParameter(1, Types.TIMESTAMP);

			oraclecstmt.setTimestamp(2, ts);
			oraclecstmt.registerOutParameter(2, Types.TIMESTAMP);

			oraclecstmt.setTimestamp(3, ts);
			oraclecstmt.registerOutParameter(3, Types.TIMESTAMP);

			oraclecstmt.setDate(4, Date.valueOf("2017-07-01"));
			oraclecstmt.registerOutParameter(4, Types.DATE);

			oraclecstmt.executeUpdate();

			oracle_res1 = oraclecstmt.getTimestamp(1).toString();
			oracle_res2 = oraclecstmt.getTimestamp(2).toString();
			oracle_res3 = oraclecstmt.getTimestamp(3).toString();
			oracle_res4 = oraclecstmt.getDate(4).toString();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{call test_alltime (?,?,?,?)}");

			restcstmt.setTimestamp(1, ts);
			restcstmt.registerOutParameter(1, Types.TIMESTAMP);

			restcstmt.setTimestamp(2, ts);
			restcstmt.registerOutParameter(2, Types.TIMESTAMP);

			restcstmt.setTimestamp(3, ts);
			restcstmt.registerOutParameter(3, Types.TIMESTAMP);

			restcstmt.setDate(4, Date.valueOf("2017-07-01"));
			restcstmt.registerOutParameter(4, Types.DATE);

			restcstmt.executeUpdate();

			rest_res1 = restcstmt.getTimestamp(1).toString();
			rest_res2 = restcstmt.getTimestamp(2).toString();
			rest_res3 = restcstmt.getTimestamp(3).toString();
			rest_res4 = restcstmt.getDate(4).toString();
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_res1, rest_res1);
		assertEquals(oracle_res2, rest_res2);
		assertEquals(oracle_res3, rest_res3);
		assertEquals(oracle_res4, rest_res4);
	}

	/*
	 * Test set numeric methods like setShort, setInt, setFloat, setDouble,
	 * setBigDecimal. <java.lang.Double cannot be cast to java.lang.Long> error
	 * occurs when run using Rest JDBC driver.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod11_allnumbertypes() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		int oracle_int_result = 0, rest_int_result = 0;
		float oracle_float_result = 0, rest_float_result = 0;
		short oracle_short_result = 0, rest_short_result = 0;
		long oracle_long_result = 0, rest_long_result = 0;
		Double oracle_double_result = (double) 0, rest_double_result = (double) 0;
		double oracle_bigdeci_result = 0, rest_bigdeci_result = 0;

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(
					restconn,
					"create or replace procedure setmtd_proc_allnum "
							+ "(num_short in out number, num_int in out number,"
							+ "num_float in out number, num_long in out number,"
							+ "num_double in out binary_double, num_bigdecimal in out number)"
							+ "is "
							+ "begin "
							+ "num_short := num_short *2; num_int := num_int * 2;"
							+ "num_float := num_float*2; num_long := num_long - 1;"
							+ "end; ");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleconn = getOracleConnection();
			oraclecstmt = oracleconn
					.prepareCall("{call setmtd_proc_allnum(:num_short,:num_int,:num_float,:num_long,:num_double,:num_bigdecimal)}");

			oraclecstmt.setShort("num_short", (short) 100);
			oraclecstmt.registerOutParameter("num_short", Types.SMALLINT);
			oraclecstmt.setInt("num_int", 1000);
			oraclecstmt.registerOutParameter("num_int", Types.INTEGER);
			oraclecstmt.setFloat("num_float", (float) 2001.31);
			oraclecstmt.registerOutParameter("num_float", Types.FLOAT);
			oraclecstmt.setLong("num_long", 9223372036854775807L);
			oraclecstmt.registerOutParameter("num_long", Types.BIGINT);
			oraclecstmt.setDouble("num_double", 12345.12345);
			oraclecstmt.registerOutParameter("num_double", Types.DOUBLE);
			oraclecstmt
					.setBigDecimal("num_bigdecimal", new BigDecimal(123.456));
			oraclecstmt.registerOutParameter("num_bigdecimal", Types.NUMERIC);
			oraclecstmt.execute();

			oracle_short_result = oraclecstmt.getShort("num_short");
			oracle_int_result = oraclecstmt.getInt("num_int");
			oracle_float_result = oraclecstmt.getFloat("num_float");
			oracle_long_result = oraclecstmt.getLong("num_long");
			oracle_double_result = (double) oraclecstmt.getDouble("num_double");
			oracle_bigdeci_result = oraclecstmt.getBigDecimal("num_bigdecimal").doubleValue();

			/*
			 * System.out.println("Test Results" + oracle_short_result);
			 * System.out.println("Test Results" + oracle_int_result);
			 * System.out.println("Test Results" + oracle_float_result);
			 * System.out.println("Test Results" + oracle_long_result);
			 * System.out.println("Test Results" + oracle_double_result);
			 * System.out.println("Test Results" + oracle_bigdeci_result);
			 */

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restconn
					.prepareCall("{call setmtd_proc_allnum(:num_short,:num_int,:num_float,:num_long,:num_double,:num_bigdecimal)}");
			restcstmt.setShort("num_short", (short) 100);
			restcstmt.registerOutParameter("num_short", Types.SMALLINT);
			restcstmt.setInt("num_int", 1000);
			restcstmt.registerOutParameter("num_int", Types.INTEGER);
			restcstmt.setFloat("num_float", (float) 2001.31);
			restcstmt.registerOutParameter("num_float", Types.FLOAT);
			restcstmt.setLong("num_long", 9223372036854775807L);
			restcstmt.registerOutParameter("num_long", Types.BIGINT);
			restcstmt.setDouble("num_double", 12345.12345);
			restcstmt.registerOutParameter("num_double", Types.DOUBLE);
			restcstmt.setBigDecimal("num_bigdecimal", new BigDecimal(123.456));
			restcstmt.registerOutParameter("num_bigdecimal", Types.NUMERIC);

			restcstmt.execute();

			rest_short_result = restcstmt.getShort("num_short");
			rest_int_result = restcstmt.getInt("num_int");
			rest_float_result = restcstmt.getFloat("num_float");
			rest_long_result = restcstmt.getLong("num_long");
			rest_double_result = (double) restcstmt.getDouble("num_double");
			rest_bigdeci_result = restcstmt.getBigDecimal("num_bigdecimal").doubleValue();

			/*
			 * System.out.println("Test Results" + rest_short_result);
			 * System.out.println("Test Results" + rest_int_result);
			 * System.out.println("Test Results" + rest_float_result);
			 * System.out.println("Test Results" + rest_long_result);
			 * System.out.println("Test Results" + rest_double_result);
			 * System.out.println("Test Results" + rest_bigdeci_result);
			 */

		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}

		assertEquals(oracle_short_result, rest_short_result);
		assertEquals(oracle_int_result, rest_int_result);
		assertEquals(oracle_float_result, rest_float_result);
		assertEquals(oracle_long_result, rest_long_result);
		assertEquals(oracle_double_result, rest_double_result);
		assertEquals(oracle_bigdeci_result, rest_bigdeci_result);
	}

	/*
	 * Test SetDouble and getDouble method using parameterIndex.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod13_Double() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		Statement ostmt = null, rstmt = null;
		ResultSet oraclers = null, restrs = null;
		Double fixedDouble = new Double(12345.12345);
		int oracle_int_result = 0, rest_int_result = 0;
		Double oracle_dbl_result = (double) 0, rest_dbl_result = (double) 0;
		String rest_dropsql = "drop table cstmt_dbl purge";

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(restconn,
					"create table cstmt_dbl (id number(10), name binary_double)");
			Util.doSQL(restconn, "create or replace procedure cstmt_dbl_proc "
					+ "(id IN NUMBER, " + "dbl IN binary_double) " + "is "
					+ "begin " + "insert into cstmt_dbl  values(id,dbl); "
					+ "end;  ");

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleconn = getOracleConnection();
			oraclecstmt = oracleconn.prepareCall("{call cstmt_dbl_proc(?,?)}");
			oraclecstmt.setInt(1, 200);
			oraclecstmt.setDouble(2, fixedDouble);
			oraclecstmt.execute();
			ostmt = oracleconn.createStatement();
			oraclers = ostmt.executeQuery("select * from cstmt_dbl");
			if (oraclers.next()) {
				oracle_int_result = oraclers.getInt(1);
				oracle_dbl_result = oraclers.getDouble(2);
			}
			oraclers.close();

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restconn.prepareCall("{call cstmt_dbl_proc(?,?)}");
			restcstmt.setInt(1, 200);
			restcstmt.setDouble(2, fixedDouble);
			restcstmt.execute();
			rstmt = restconn.createStatement();
			restrs = rstmt.executeQuery("select * from cstmt_dbl");
			if (restrs.next()) {
				rest_int_result = restrs.getInt(1);
				rest_dbl_result = restrs.getDouble(2);
			}
			restrs.close();

		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(restconn, rest_dropsql);
				oraclecstmt.close();
				restcstmt.close();
				close(ostmt, rstmt);
				close(oraclers, restrs);
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_result, rest_int_result);
		assertEquals(oracle_dbl_result, rest_dbl_result);
	}

	/*
	 * Test setBigdecimal and getBigdecimal using parameter Index.
	 * Numbers decimal precision should be restricted 3 digits
	 * due to JSON limitations.
	 */
	@Category(PassTest.class)
	@Test
	public void testSetMethod13_BigDecimal() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		String oracle_BigDeci_res1 = "", rest_BigDeci_res1 = "";
//		String oracle_BigDeci_res2 = "", rest_BigDeci_res2 = "";

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(restconn,
					"create or replace procedure procout_Bigdecimal "
							+ " (c1 in out NUMBER) is begin null; end;");

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleconn = getOracleConnection();
			oraclecstmt = oracleconn
					.prepareCall("{call procout_Bigdecimal(?)}");
			oraclecstmt.setBigDecimal(1, new BigDecimal(123.456));
			oraclecstmt.registerOutParameter(1, Types.NUMERIC);
			oraclecstmt.execute();
			oracle_BigDeci_res1 = oraclecstmt.getBigDecimal(1).toString().substring(0, 7);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restconn.prepareCall("{call procout_Bigdecimal(?)}");
			restcstmt.setBigDecimal(1, new BigDecimal(123.456));
			restcstmt.registerOutParameter(1, Types.NUMERIC);
			restcstmt.execute();
			rest_BigDeci_res1 = restcstmt.getBigDecimal(1).toString();
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}

		assertEquals(oracle_BigDeci_res1, rest_BigDeci_res1);
//		assertEquals(oracle_BigDeci_res2, rest_BigDeci_res2);
	}


	@Category(PassTest.class)
	@Test
	public void testSetMethod14_Boolean() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		int oracle_int_res = 0, rest_int_res = 0;
		try {
			oracleconn = getOracleConnection();
			oraclecstmt = oracleconn.prepareCall("BEGIN " + "  IF ? > 0 THEN "
					+ "    ? := 1; " + "  END IF; " + "END;");
			oraclecstmt.setBoolean(1, true);
			oraclecstmt.registerOutParameter(2, Types.INTEGER);
			oraclecstmt.executeUpdate();
			oracle_int_res = oraclecstmt.getInt(2);
			// System.out.println("result: " + oracle_int_res);

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restconn = getRestConnection();
			restcstmt = restconn.prepareCall("BEGIN " + "  IF ? > 0 THEN "
					+ "    ? := 1; " + "  END IF; " + "END;");
			restcstmt.setBoolean(1, true);
			restcstmt.registerOutParameter(2, Types.INTEGER);
			restcstmt.executeUpdate();
			rest_int_res = restcstmt.getInt(2);
			// System.out.println("result: " + rest_int_res);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_res, rest_int_res);
	}


	@Category(PassTest.class)
	@Test
	public void testSetMethod15_Boolean() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		boolean oracle_int_res = false, rest_int_res = false;

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(restconn, "create or replace procedure proc_boolean "
					+ " (c1 in out NUMBER) is begin null; end;");

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleconn = getOracleConnection();
			oraclecstmt = oracleconn
					.prepareCall("begin proc_boolean (:a); end;");
			oraclecstmt.setBoolean("a", true);
			oraclecstmt.registerOutParameter("a", Types.BIT);
			oraclecstmt.executeUpdate();
			oracle_int_res = oraclecstmt.getBoolean("a");
			// System.out.println("result: " + oracle_int_res);

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restconn = getRestConnection();
			restcstmt = restconn.prepareCall("begin proc_boolean (:a); end;");
			restcstmt.setBoolean("a", true);
			restcstmt.registerOutParameter("a", Types.BIT);
			restcstmt.executeUpdate();
			rest_int_res = restcstmt.getBoolean("a");
			// System.out.println("result: " + rest_int_res);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_res, rest_int_res);
	}

	/*
	 * Missing IN and OUT parameter at index::1 when run using JDBC
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod16_Boolean() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		boolean oracle_int_res = false, rest_int_res = false;

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(restconn, "create or replace procedure proc1_boolean "
					+ " (c1 in out NUMBER) is begin null; end;");

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleconn = getOracleConnection();
			oraclecstmt = oracleconn
					.prepareCall("begin proc1_boolean (?); end;");
			oraclecstmt.setBoolean(1, true);
			oraclecstmt.registerOutParameter(1, Types.BIT);
			oraclecstmt.executeUpdate();
			oracle_int_res = oraclecstmt.getBoolean(1);
			// System.out.println("result: " + oracle_int_res);

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restconn = getRestConnection();
			restcstmt = restconn.prepareCall("begin proc1_boolean (?); end;");
			restcstmt.setBoolean(1, true);
			restcstmt.registerOutParameter(1, Types.BIT);
			restcstmt.executeUpdate();
			rest_int_res = restcstmt.getBoolean(1);
			// System.out.println("result: " + rest_int_res);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_res, rest_int_res);
	}

	/*
	 * Method setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength)
	 * in CallableStatement not implemented yet.
	 */
	@Category(UnsupportedTest.class)
	@Test
	public void testSetMethod17_Obj() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		Statement ostmt = null, rstmt = null;
		ResultSet oraclers = null, restrs = null;
		int oracle_int_result = 0, rest_int_result = 0;
		String oracle_strg_result = "", rest_strg_result = "";

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table rcstmt_object purge");
			Util.doSQL(restConn, "drop table ocstmt_object purge");

			Util.doSQL(restConn,
					"create table rcstmt_object (id number(10), name varchar2(200))");
			Util.doSQL(restConn,
					"create or replace procedure rcstmt_object_proc "
							+ " (id IN NUMBER, name IN VARCHAR2)  is "
							+ " begin "
							+ " insert into rcstmt_object values(id,name); "
							+ " end;  ");
			Util.doSQL(restConn,
					"create table ocstmt_object (id number(10), name varchar2(200))");
			Util.doSQL(restConn,
					"create or replace procedure ocstmt_object_proc "
							+ " (id IN NUMBER, name IN VARCHAR2) is "
							+ " begin "
							+ " insert into ocstmt_object values(id,name); "
							+ " end;  ");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{call ocstmt_object_proc(?,?)}");
			oraclecstmt.setInt(1, 200);
			oraclecstmt.setObject(2, "abc", Types.VARCHAR, 32);
			// Uncomment below lines
			/*
			 * oraclecstmt.addBatch(); oraclecstmt.setInt(1, 200);
			 * oraclecstmt.setObject(1, "def", Types.VARCHAR, 32);
			 * oraclecstmt.addBatch();
			 */
			oraclecstmt.execute();
			ostmt = oracleConn.createStatement();
			oraclers = ostmt.executeQuery("select * from ocstmt_object");
			if (oraclers.next()) {
				oracle_int_result = oraclers.getInt(1);
				oracle_strg_result = oraclers.getObject(2).toString();
			}
			// System.out.println("Testing results" + oracle_int_result +
			// "====="
			// + oracle_strg_result);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC

		try {
			restcstmt = restConn.prepareCall("{call rcstmt_object_proc(?,?)}");
			restcstmt.setInt(1, 200);
			restcstmt.setObject(2, "abc", Types.VARCHAR, 32);
			restcstmt.execute();
			rstmt = restConn.createStatement();
			restrs = rstmt.executeQuery("select * from rcstmt_object");
			if (restrs.next()) {
				rest_int_result = restrs.getInt(1);
				rest_strg_result = restrs.getObject(2).toString();
			}
			// System.out.println("Testing results" + rest_int_result + "====="
			// + rest_strg_result);
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				ostmt.close();
				rstmt.close();
			} catch (SQLException e) {
				fail(e.getMessage());
			}

		}
		assertEquals(oracle_int_result, rest_int_result);
		assertEquals(oracle_strg_result, rest_strg_result);
	}

	/*
	 * SetTimestamp using parameterName.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod11_Time() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		TimeZone tz = TimeZone.getDefault();
		tz.setID("US/Pacific");
		Calendar gcal = new GregorianCalendar(tz);
		Timestamp ts = Timestamp.valueOf("2017-05-01 12:31:56.78");

		String oracle_res1 = "", oracle_res2 = "", oracle_res3 = "", oracle_res4 = "", oracle_res5 = "";
		String rest_res1 = "", rest_res2 = "", rest_res3 = "", rest_res4 = "", rest_res5 = "";
		// Setup
		try {
		  oracleConn = getOracleConnection();
			Util.doSQL(
			    oracleConn,
					"create or replace procedure test_alltime2 (c1 in out TIMESTAMP WITH LOCAL TIME ZONE, \n"
							+ "c2 in out TIMESTAMP WITH TIME ZONE, \n"
							+ "c3 in out TIMESTAMP, \n"
							+ "c4 in out Date, C5 in out Date) \n"
							+ "is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{call test_alltime2 (:c1,:c2,:c3,:c4,:c5)}");

			oraclecstmt.setTimestamp("c1", ts);
			oraclecstmt.registerOutParameter("c1", Types.TIMESTAMP);

			oraclecstmt.setTimestamp("c2", ts, gcal);
			oraclecstmt.registerOutParameter("c2", Types.TIMESTAMP);

			oraclecstmt.setTimestamp("c3", ts, gcal);
			oraclecstmt.registerOutParameter("c3", Types.TIMESTAMP);

			oraclecstmt.setDate("c4", Date.valueOf("2017-07-01"), gcal);
			oraclecstmt.registerOutParameter("c4", Types.DATE);

			oraclecstmt.setTime("c5", Time.valueOf("10:10:10"), gcal);
			oraclecstmt.registerOutParameter("c5", Types.TIME);

			oraclecstmt.executeUpdate();

			oracle_res1 = oraclecstmt.getTimestamp("c1").toString();
			oracle_res2 = oraclecstmt.getTimestamp("c2").toString();
			oracle_res3 = oraclecstmt.getTimestamp("c3").toString();
			oracle_res4 = oraclecstmt.getDate("c4").toString();
			oracle_res5 = oraclecstmt.getTime("c5").toString();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}
		try {
		  restConn = getRestConnection();
			restcstmt = restConn
					.prepareCall("{call test_alltime2 (:c1,:c2,:c3,:c4,:c5)}");

			restcstmt.setTimestamp("c1", ts);
			restcstmt.registerOutParameter("c1", Types.TIMESTAMP);

			restcstmt.setTimestamp("c2", ts, gcal);
			restcstmt.registerOutParameter("c2", Types.TIMESTAMP);

			restcstmt.setTimestamp("c3", ts, gcal);
			restcstmt.registerOutParameter("c3", Types.TIMESTAMP);

			restcstmt.setDate("c4", Date.valueOf("2017-07-01"), gcal);
			restcstmt.registerOutParameter("c4", Types.DATE);

			restcstmt.setTime("c5", Time.valueOf("10:10:10"), gcal);
			restcstmt.registerOutParameter("c5", Types.TIME);

			restcstmt.executeUpdate();

			rest_res1 = restcstmt.getTimestamp("c1").toString();
			rest_res2 = restcstmt.getTimestamp("c2").toString();
			rest_res3 = restcstmt.getTimestamp("c3").toString();
			rest_res4 = restcstmt.getDate("c4").toString();
			rest_res5 = restcstmt.getTime("c5").toString();
		} catch (SQLException | NullPointerException | ClassNotFoundException e) {
			fail(e.getMessage());
    } finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
			    fail(se.getMessage());
			}
		}
		assertEquals(oracle_res1, rest_res1);
		assertEquals(oracle_res2, rest_res2);
		assertEquals(oracle_res3, rest_res3);
		assertEquals(oracle_res4, rest_res4);
	}

	/*
	 * SetTimestamp using parameterIndex and calendar.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetMethod12_Time() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		TimeZone tz = TimeZone.getDefault();
		tz.setID("US/Pacific");
		Calendar gcal = new GregorianCalendar(tz);
		Timestamp ts = Timestamp.valueOf("2017-05-01 12:31:56.78");

		String oracle_res1 = "", oracle_res2 = "", oracle_res3 = "", oracle_res4 = "", oracle_res5 = "";
		String rest_res1 = "", rest_res2 = "", rest_res3 = "", rest_res4 = "", rest_res5 = "";
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(
					restConn,
					"create or replace procedure test_alltime1 (c1 in out TIMESTAMP WITH LOCAL TIME ZONE, \n"
							+ "c2 in out TIMESTAMP WITH TIME ZONE, \n"
							+ "c3 in out TIMESTAMP, \n"
							+ "c4 in out Date, c5 in out Date) \n"
							+ "is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{call test_alltime1 (?,?,?,?,?)}");

			oraclecstmt.setTimestamp(1, ts, gcal);
			oraclecstmt.registerOutParameter(1, Types.TIMESTAMP);

			// ((OracleCallableStatement) oraclecstmt).setTIMESTAMPTZ(2, tstz);
			// oraclecstmt.registerOutParameter(2, OracleTypes.TIMESTAMPTZ);
			// System.out.println("Testing");

			oraclecstmt.setTimestamp(2, ts, gcal);
			oraclecstmt.registerOutParameter(2, Types.TIMESTAMP);

			oraclecstmt.setTimestamp(3, ts, gcal);
			oraclecstmt.registerOutParameter(3, Types.TIMESTAMP);

			oraclecstmt.setDate(4, Date.valueOf("2017-07-01"), gcal);
			oraclecstmt.registerOutParameter(4, Types.DATE);

			oraclecstmt.setTime(5, Time.valueOf("10:10:10"), gcal);
			oraclecstmt.registerOutParameter(5, Types.TIME);

			oraclecstmt.executeUpdate();

			oracle_res1 = oraclecstmt.getTimestamp(1).toString();
			oracle_res2 = oraclecstmt.getTimestamp(2).toString();
			oracle_res3 = oraclecstmt.getTimestamp(3).toString();
			oracle_res4 = oraclecstmt.getDate(4).toString();
			oracle_res5 = oraclecstmt.getTime(5).toString();

			/*
			 * System.out.println("Timestampo with timezone value" +
			 * oraclecstmt.getTimestamp(1).toString());
			 * System.out.println("Timestampo with timezone value" +
			 * oraclecstmt.getTimestamp(3).toString());
			 * System.out.println("Timestampoo with timezone value" +
			 * oraclecstmt.getDate(4).toString());
			 * System.out.println("Timestampo with timezone value" +
			 * oraclecstmt.getTimestamp(2).toString());
			 * System.out.println("Timestampo with timezone value" +
			 * oraclecstmt.getTime(5).toString());
			 */

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {

			restcstmt = restConn
					.prepareCall("{call test_alltime1 (?,?,?,?,?)}");

			restcstmt.setTimestamp(1, ts, gcal);
			restcstmt.registerOutParameter(1, Types.TIMESTAMP);
			restcstmt.setTimestamp(2, ts, gcal);
			restcstmt.registerOutParameter(2, Types.TIMESTAMP);

			restcstmt.setTimestamp(3, ts, gcal);
			restcstmt.registerOutParameter(3, Types.TIMESTAMP);

			restcstmt.setDate(4, Date.valueOf("2017-07-01"), gcal);
			restcstmt.registerOutParameter(4, Types.DATE);

			restcstmt.setTime(5, Time.valueOf("10:10:10"), gcal);
			restcstmt.registerOutParameter(5, Types.TIME);

			restcstmt.executeUpdate();

			rest_res1 = restcstmt.getTimestamp(1).toString();
			rest_res2 = restcstmt.getTimestamp(2).toString();
			rest_res3 = restcstmt.getTimestamp(3).toString();
			rest_res4 = restcstmt.getDate(4).toString();
			rest_res5 = restcstmt.getTime(5).toString();

			/*
			 * System.out.println("Timestamp with timezone value" +
			 * restcstmt.getTimestamp(1).toString());
			 * System.out.println("Timestamp with timezone value" +
			 * restcstmt.getTimestamp(3).toString());
			 * System.out.println("Timestamp with timezone value" +
			 * restcstmt.getDate(4).toString());
			 * System.out.println("Timestamp with timezone value" +
			 * restcstmt.getTimestamp(2).toString());
			 * System.out.println("Timestamp with timezone value" +
			 * restcstmt.getTime(5).toString());
			 */

		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_res1, rest_res1);
		assertEquals(oracle_res2, rest_res2);
		assertEquals(oracle_res3, rest_res3);
		assertEquals(oracle_res4, rest_res4);
		assertEquals(oracle_res5, rest_res5);
	}

	/*
	 * Test to execute without registering the out parameter. Expected <Missing
	 * IN or OUT parameter at index:: 1> Actual <Missing IN or OUT parameter at
	 * index:: 3>
	 * Bug in ORDS
	 */
	@Category(UnsupportedTest.class)
	@Test
	public void testSetMethod3_int() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		SQLException oracle_exception = null, rest_exception = null;

		try {
			oracleConn = getOracleConnection();
			Util.doSQL(oracleConn,
					"create or replace function oracle_setmtd_proc"
							+ "(n1 in number,n2 in number)" + "return number "
							+ "is " + "temp number(8); " + "begin "
							+ "temp :=n1+n2; " + "return temp; " + "end; ");
			oraclecstmt = oracleConn
					.prepareCall("{?= call oracle_setmtd_proc(?,?)}");
			oraclecstmt.setInt(2, 10);
			oraclecstmt.setInt(3, 43);
			oraclecstmt.execute();
		} catch (SQLException | ClassNotFoundException e) {
			// fail(e.getMessage());
			oracle_exception = (SQLException) e;
		}

		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "create or replace function rest_setmtd_proc"
					+ "(n1 in number,n2 in number)" + "return number " + "is "
					+ "temp number(8); " + "begin " + "temp :=n1+n2; "
					+ "return temp; " + "end; ");
			restcstmt = restConn.prepareCall("{?= call rest_setmtd_proc(?,?)}");
			restcstmt.setInt(2, 10);
			restcstmt.setInt(3, 43); //

			restcstmt.execute();
		} catch (SQLException | ClassNotFoundException e) {
			// fail(e.getMessage());
			rest_exception = (SQLException) e;
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_exception.getErrorCode(),
				rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),
				rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());

	}

	/*
	 * Test setClob and getClob method using parameter Index.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetClob() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		ResultSet oraclers = null, restrs = null;
		int oracle_res_int = 0, rest_res_int = 0;
		String oracle_res_cl_str = "", rest_res_cl_str = "";

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table Table_OracleClob;");
			Util.doSQL(restConn, "drop table Table_RestClob;");
			Util.doSQL(restConn,
					"create table Table_OracleClob (c1 NUMBER, c2 clob);");
			Util.doSQL(restConn,
					"create table Table_RestClob (c1 NUMBER, c2 clob);");
			Util.doSQL(restConn,
					"create or replace procedure Proc_OracleClob ( p1 in NUMBER,p2 in clob)"
							+ " as" + " begin"
							+ " insert into Table_OracleClob values(p1,p2);"
							+ " end; ");
			Util.doSQL(restConn,
					"create or replace procedure Proc_RestClob ( p1 in NUMBER,p2 in clob)"
							+ " as" + " begin"
							+ " insert into Table_RestClob values(p1,p2);"
							+ " end; ");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver

		try {
			oracleConn = getOracleConnection();
			Clob cl = oracleConn.createClob();
			cl.setString(1, "abcdefg");
			oraclecstmt = oracleConn.prepareCall("{call Proc_OracleClob(?,?)}");
			oraclecstmt.setInt(1, 1);
			oraclecstmt.setClob(2, cl);
			oraclecstmt.execute();
			oraclers = oraclecstmt
					.executeQuery("select c1,c2 from Table_OracleClob");
			Clob oracle_res_cl = null;
			int oracle_res_cl_len = 0;
			if (oraclers.next()) {
				oracle_res_int = oraclers.getInt("c1");
				oracle_res_cl_len = (int) oraclers.getClob("c2").length();
				oracle_res_cl = oraclers.getClob("c2");
				oracle_res_cl_str = oracle_res_cl.getSubString(1,
						(int) oracle_res_cl.length());
			}
			// System.out.println("Output is oracle_res==" + oracle_res_int);
			// System.out.println("Output is oracle_res==" + oracle_res_cl_len);
			// System.out.println("Output is oracle_res==" + oracle_res_cl_str);

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC deriver

		try {
			Clob cl = restConn.createClob();
			cl.setString(1, "abcdefg");
			restcstmt = restConn.prepareCall("{call Proc_RestClob(?,?)}");
			restcstmt.setInt(1, 1);
			restcstmt.setClob(2, cl);
			restcstmt.execute();
			restrs = restcstmt.executeQuery("select c1,c2 from Table_RestClob");
			Clob rest_res_cl = null;
			int rest_res_cl_len = 0;
			if (restrs.next()) {
				rest_res_int = restrs.getInt("c1");
				rest_res_cl = restrs.getClob("c2");
				rest_res_cl_len = (int) restrs.getClob("c2").length();
				rest_res_cl_str = rest_res_cl.getSubString(1,
						(int) rest_res_cl.length());
			}

			// System.out.println("Output is rest_res==" + rest_res_int);
			// System.out.println("Output is rest_res==" + rest_res_cl_len);
			// System.out.println("Output is rest_res==" + rest_res_cl_str);

		} catch (SQLException e) {
			fail(e.getMessage());
		}

		finally {
			try {
				close(oraclers, restrs);
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}

		assertEquals(oracle_res_int, rest_res_int);
		assertEquals(oracle_res_cl_str, rest_res_cl_str);
	}

	/*
	 * Basic test for <SetNClob> method. This method is not implemented.
	 */
	@Category(PassTest.class)
	@Test
	public void testSetNClob() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		ResultSet oraclers = null, restrs = null;
		int oracle_res_int = 0, rest_res_int = 0;
		String rest_res_strg = "", oracle_res_strg = "";

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table Table_OracleNClob;");
			Util.doSQL(restConn, "drop table Table_RestNClob;");
			Util.doSQL(restConn,
					"create table Table_OracleNClob (c1 NUMBER, c2 nclob);");
			Util.doSQL(restConn,
					"create table Table_RestNClob (c1 NUMBER, c2 nclob);");

			Util.doSQL(restConn,
					"create or replace procedure Proc_OracleNClob ( p1 in NUMBER,p2 in nclob)"
							+ " as" + " begin"
							+ " insert into Table_OracleNClob values(p1,p2);"
							+ " end; ");

			Util.doSQL(restConn,
					"create or replace procedure Proc_RestNClob ( p1 in NUMBER,p2 in nclob)"
							+ " as" + " begin"
							+ " insert into Table_RestNClob values(p1,p2);"
							+ " end; ");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver

		try {
			oracleConn = getOracleConnection();
			NClob cl = oracleConn.createNClob();
			cl.setString(1, "abcdefg");

			oraclecstmt = oracleConn
					.prepareCall("{call Proc_OracleNClob(?,?)}");
			oraclecstmt.setInt(1, 1);
			oraclecstmt.setNClob(2, cl);

			oraclecstmt.execute();

			oraclers = oraclecstmt
					.executeQuery("select c1,c2 from Table_OracleNClob");

			NClob oracle_res_ncl = null;
			int oracle_res_ncl_len = 0;

			if (oraclers.next()) {
				oracle_res_int = oraclers.getInt("c1");
				oracle_res_ncl_len = (int) oraclers.getClob("c2").length();
				oracle_res_ncl = oraclers.getNClob("c2");
				oracle_res_strg = oracle_res_ncl.getSubString(1,
						(int) oracle_res_ncl.length());

			}

			// System.out.println("Output is oracle_res==" + oracle_res_int);
			// System.out.println("Output is oracle_res==" +
			// oracle_res_ncl_len);
			// System.out.println("Output is oracle_res==" + oracle_res_strg);

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver

		try {
			NClob cl = restConn.createNClob();
			cl.setString(1, "abcdefg");

			restcstmt = restConn.prepareCall("{call Proc_RestNClob(?,?)}");
			restcstmt.setInt(1, 1);
			restcstmt.setNClob(2, cl);

			restcstmt.execute();

			restrs = restcstmt
					.executeQuery("select c1,c2 from Table_RestNClob");

			NClob rest_res_ncl = null;
			int rest_res_ncl_len = 0;
			if (restrs.next()) {
				rest_res_int = restrs.getInt("c1");
				rest_res_ncl_len = (int) restrs.getClob("c2").length();
				rest_res_ncl = restrs.getNClob("c2");
				rest_res_strg = rest_res_ncl.getSubString(1,
						(int) rest_res_ncl.length());

			}

			// System.out.println("Output is oracle_res==" + rest_res_int);
			// System.out.println("Output is oracle_res==" + rest_res_ncl_len);
			// System.out.println("Output is oracle_res==" + rest_res_strg);

		} catch (RestJdbcNotImplementedException e) {
			assertTrue(
					"SetNClob method in CallableStatement class is not implemented",
					true);
		} catch (SQLException e) {
			fail(e.getMessage());
		}

		finally {
			try {
				close(oraclers, restrs);
				oraclecstmt.close();
				// restcstmt.close(); // Uncomment when method is implemented
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		// Uncomment test when implemented
		// assertEquals(oracle_res_int, rest_res_int);
		// assertEquals(oracle_res_strg, rest_res_strg);
	}

	/*
	 * Test registeroutparameter with TYPE "CHAR".
	 *
	 */
	@Category(PassTest.class)
	@Test
	public void testSetMethodString_1() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		String oracle_res_char = "", rest_res_char = "";
		// Setup
		try {
			restConn = getRestConnection();
			Util.trySQL(restConn, "DROP TABLE testSetMethodString_1");
			Util.doSQL(restConn,
					"CREATE TABLE testSetMethodString_1 (n number,c char(20),r raw(100), cb CLOB)");
			Util.doSQL(restConn, "INSERT INTO testSetMethodString_1 values "
					+ " (1, 'CHARa','010101', 'clobcolumn')");

			Util.doSQL(restConn, "commit");

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("begin SELECT c into ? FROM testSetMethodString_1 WHERE n= 1;  end;");
			oraclecstmt.registerOutParameter(1, Types.CHAR);
			oraclecstmt.execute();
			oracle_res_char = oraclecstmt.getString(1).toString();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {

			restcstmt = restConn
					.prepareCall("begin SELECT c into ? FROM testSetMethodString_1 WHERE n= 1;  end;");

			restcstmt.registerOutParameter(1, Types.CHAR);
			restcstmt.execute();
			restcstmt = restConn.prepareCall("begin SELECT c into ? FROM testSetMethodString_1 WHERE n= 1;  end;");
			restcstmt.registerOutParameter(1, Types.CHAR);
			restcstmt.execute();
			rest_res_char = restcstmt.getString(1).toString();
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_res_char, rest_res_char);
	}

	/*
	 * Test setmethod string with TYPE "CHAR".
	 */
	@Category(PassTest.class)
	@Test
	public void testSetMethodString_2() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		String oracle_res_char = "", rest_res_char = "";
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(
					restConn,
					"create or replace procedure proc_testSetMethodString_2 (c1 in out char) is begin  c1:= 'Testing Char' ; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{call proc_testSetMethodString_2(?)}");
			oraclecstmt.registerOutParameter(1, Types.CHAR);
			oraclecstmt.execute();
			oracle_res_char = oraclecstmt.getString(1);
			// System.out.println(">> CHAR  = " + oracle_res_char);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {

			restcstmt = restConn
					.prepareCall("{call proc_testSetMethodString_2(?)}");
			restcstmt.registerOutParameter(1, Types.CHAR);
			restcstmt.execute();
			rest_res_char = oraclecstmt.getString(1);
			// System.out.println(">> CHAR  = " + rest_res_char);
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_res_char, rest_res_char);
	}

	/*
	 * Test setmethod string with TYPE "CHAR".
	 * Missing IN or OUT parameter when run using Rest JDBC driver
	 * Fix needed in Rest SQL service to recognize bind variables
	 * (that may not have been declared before) in procedures
	 */
	@Category(UnsupportedTest.class)
	@Test
	public void testSetMethodString_3() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		String oracle_res_char = "", rest_res_char = "";
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(
					restConn,
					"create or replace procedure proc_testSetMethodString_3 (c1 in out char) is begin  c1:= 'Testing Char' ; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{call proc_testSetMethodString_3(:1)}");
		//	oraclecstmt.setMaxFieldSize(20);
			oraclecstmt.registerOutParameter("c1", Types.CHAR);
			oraclecstmt.execute();
			oracle_res_char = oraclecstmt.getString("c1");
			// System.out.println(">> CHAR  = " + oracle_res_char);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {

			restcstmt = restConn
					.prepareCall("{call proc_testSetMethodString_3(:1)}");
		//	restcstmt.setMaxFieldSize(20);
			restcstmt.registerOutParameter("c1", Types.CHAR);
			restcstmt.execute();
			rest_res_char = oraclecstmt.getString("c1");
			// System.out.println(">> CHAR  = " + rest_res_char);
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_res_char, rest_res_char);
	}

	/*
	 * Basic test for <setByte> method. This method is not implemented.
	 */
	@Category(PassTest.class)
	@Test
	public void testSetMethodBytes_1() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		String oracle_res_char = "", rest_res_char = "";
		String str = "test string in testSetMethodBytes_1";
		byte[] bt = str.getBytes();
		byte[] oracle_res = null, rest_res = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn,
					"create or replace procedure test9 (c1 in out RAW) is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}
		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn.prepareCall("begin test9 (?); end;");
			oraclecstmt.setBytes(1, bt);
			oraclecstmt.registerOutParameter(1, Types.BINARY);
			oraclecstmt.execute();
			oracle_res = oraclecstmt.getBytes(1);
			// System.out.println("c1 from getBytes(int) is ==== "
			// + new String(oracle_res));
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver

		try {
			restcstmt = restConn.prepareCall("begin test9 (?); end;");
			restcstmt.setBytes(1, bt);
			restcstmt.registerOutParameter(1, Types.BINARY);
			restcstmt.execute();
			rest_res = restcstmt.getBytes(1);
			// System.out.println("c1 from getBytes(int) is ==== "
			// + new String(rest_res));
		} catch (RestJdbcNotImplementedException e) {
			assertTrue(
					"SetByte method in CallableStatement class is not implemented",
					true);
		} catch (SQLException e) {
			fail(e.getMessage());
		}

		finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		// Uncomment Test when implemented
		// assertEquals(oracle_res, rest_res);
	}

	/**
	 * Method registerOutParameter(int parameterIndex, int sqlType, int scale)
	 * in CallableStatement not supported in the REST JDBC driver
	 */
	@Category(UnsupportedTest.class)
	@Test
	public void testSetMethodUrl_1() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		java.net.URL pin = null, pinout = null, oracle_res_url = null, rest_res_url = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn,
					"create or replace procedure proc_SetMethodUrl (pin in VARCHAR2, \n"
							+ "pinout in out VARCHAR2, pout out VARCHAR2 ) \n"
							+ "is begin pout := pin; pinout := pin; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("begin proc_SetMethodUrl(?, ?, ?); end;");
			pin = new java.net.URL("http://www.oracle.com");
			pinout = new java.net.URL("http://www.yahoo.com");
			oraclecstmt.registerOutParameter(3, Types.DATALINK);
			oraclecstmt.registerOutParameter(2, Types.DATALINK);
			oraclecstmt.setURL(2, pin);
			oraclecstmt.setURL(1, pinout);
			oraclecstmt.execute();
			oracle_res_url = oraclecstmt.getURL(3);
			// System.out.println("The result from getURL() is "
			// + oracle_res_url.toString());

		} catch (SQLException | ClassNotFoundException | MalformedURLException e) {
			fail(e.getMessage());
		}

		try {

			restcstmt = restConn
					.prepareCall("begin proc_SetMethodUrl(?, ?, ?); end;");

			pin = new java.net.URL("http://www.oracle.com");
			pinout = new java.net.URL("http://www.yahoo.com");

			restcstmt.registerOutParameter(3, Types.DATALINK);
			restcstmt.registerOutParameter(2, Types.DATALINK, 50);
			restcstmt.setURL(2, pin);
			restcstmt.setURL(1, pinout);
			restcstmt.execute();
			rest_res_url = restcstmt.getURL(3);
			// System.out.println("The result from getURL() is "
			// + rest_res_url.toString());
		} catch (SQLException | NullPointerException | MalformedURLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_res_url, rest_res_url);
	}

	/*
	 * Test setInt with registeroutparameter types Decimal.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetInt_TypesDecimal() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		int oracle_int_res = -1, rest_int_res = -1;

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(
					restconn,
					"create or replace function setint_isLess(par1 in number, par2 in number) return number is  begin    if (par1 < par2) then return 1;   else return 2;   end if; end;");

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {

			oracleconn = getOracleConnection();
			oraclecstmt = oracleconn
					.prepareCall("{? = call setint_isLess(?, ?)}");
			oraclecstmt.setInt(2, 4);
			oraclecstmt.setInt(3, 3);
			oraclecstmt.registerOutParameter(1, Types.DECIMAL);
			oraclecstmt.execute();

			oracle_int_res = oraclecstmt.getInt(1);
			// System.out.println("Testing = " + oracle_int_res);

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {

			restcstmt = restconn.prepareCall("{? = call setint_isLess(?, ?)}");
			restcstmt.setInt(2, 4);
			restcstmt.setInt(3, 3);
			restcstmt.registerOutParameter(1, Types.DECIMAL);
			restcstmt.execute();
			rest_int_res = restcstmt.getInt(1);
			// System.out.println("Testing = " + rest_int_res);

		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_res, rest_int_res);
	}

	/*
	 * Test setInt with registeroutparameter types TinyInt.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetInt_TypesTinyInt() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		int oracle_int_res = -1, rest_int_res = -1;

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(restconn, "create or replace procedure setint_tinyint "
					+ " (c1 in out NUMBER) is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {

			oracleconn = getOracleConnection();
			oraclecstmt = oracleconn
					.prepareCall("begin setint_tinyint (:a); end;");
			oraclecstmt.setInt("a", 4);
			oraclecstmt.registerOutParameter("a", Types.TINYINT);
			oraclecstmt.execute();

			oracle_int_res = oraclecstmt.getInt("a");
			// System.out.println("Testing = " + oracle_int_res);

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {

			restcstmt = restconn.prepareCall("begin setint_tinyint (:a); end;");
			restcstmt.setInt("a", 4);
			restcstmt.registerOutParameter("a", Types.TINYINT);
			restcstmt.execute();
			rest_int_res = restcstmt.getInt("a");
			// System.out.println("Testing = " + rest_int_res);

		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_res, rest_int_res);
	}

	/*
	 * Test setInt with registeroutparameter types Real.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetInt_TypesReal() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		int oracle_int_res = -1, rest_int_res = -1;
		float oracle_float_res = -1, rest_float_res = -1;

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(restconn, "create or replace procedure setint_real "
					+ " (c1 in out NUMBER) is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {

			oracleconn = getOracleConnection();
			oraclecstmt = oracleconn
					.prepareCall("begin setint_real (:a); end;");
			oraclecstmt.setFloat("a", (float) 22.22);
			oraclecstmt.registerOutParameter("a", Types.REAL);
			oraclecstmt.execute();

			oracle_int_res = oraclecstmt.getInt("a");
			oracle_float_res = oraclecstmt.getFloat("a");
			// System.out.println("Testing = " + oracle_int_res + "=="
			// + oracle_float_res);

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {

			restcstmt = restconn.prepareCall("begin setint_real (:a); end;");
			restcstmt.setFloat("a", (float) 22.22);
			restcstmt.registerOutParameter("a", Types.REAL);
			restcstmt.execute();
			rest_int_res = restcstmt.getInt("a");
			rest_float_res = restcstmt.getFloat("a");
			// System.out.println("Testing = " + rest_int_res + "=="
			// + rest_float_res);

		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_res, rest_int_res);
		assertEquals(oracle_float_res, rest_float_res);
	}

	/*
	 * Test setClob method using parameter Name.
	 * registerOutParameter with Types.Clob.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetClob_TypesClob() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		int oracle_int_res = -1, rest_int_res = -1;

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(restconn, "create or replace procedure setclob_procout "
					+ " (c1 in out CLOB) is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {

			oracleconn = getOracleConnection();
			Clob oracle_clb = oracleconn.createClob();
			oracle_clb.setString(1,
					"this is clb1 created by RestConnection.createClob()");
			oraclecstmt = oracleconn
					.prepareCall("begin setclob_procout (:a); end;");

			oraclecstmt.setClob("a", oracle_clb);
			oraclecstmt.registerOutParameter("a", Types.CLOB);
			oraclecstmt.execute();
			Clob c1 = oraclecstmt.getClob("a");
			oracle_int_res = (int) c1.length();
			// System.out.println("Out argument : "
			// + c1.getSubString(1, oracle_int_res));
			c1.free();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			Clob rest_clb = restconn.createClob();
			rest_clb.setString(1,
					"this is clb1 created by RestConnection.createClob()");
			restcstmt = restconn
					.prepareCall("begin setclob_procout (:a); end;");
			restcstmt.setClob("a", rest_clb);
			restcstmt.registerOutParameter("a", Types.CLOB);
			restcstmt.execute();
			Clob c1 = restcstmt.getClob("a");
			rest_int_res = (int) c1.length();
			// System.out.println("Out argument : "
			// + c1.getSubString(1, rest_int_res));
			c1.free();

		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_res, rest_int_res);
	}

	/*
	 * Test setBlob with registeroutparameter types Blob. <NOT Implemented>
	 */

	@Category(UnsupportedTest.class)
	@Test
	public void testSetBytes_TypesBlob() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		int oracle_int_res = -1, rest_int_res = -1;

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(restconn, "create or replace procedure setblob_procout "
					+ " (c1 in out BLOB) is begin null; end;");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {

			oracleconn = getOracleConnection();
			Blob blb = oracleconn.createBlob();
			byte[] data = (new String("abcdefg")).getBytes();
			blb.setBytes(1, data);
			oraclecstmt = oracleconn
					.prepareCall("begin setblob_procout (?); end;");

			oraclecstmt.setBlob(1, blb);
			oraclecstmt.registerOutParameter(1, Types.BLOB);
			oraclecstmt.execute();
			Blob b = oraclecstmt.getBlob(1);
			oracle_int_res = (int) b.length();
			// System.out.println("Out argument : "
			// + new String(b.getBytes(1, oracle_int_res)));
			b.free();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}
		try {
			Blob blb = restconn.createBlob();
			byte[] data = (new String("abcdefg")).getBytes();
			blb.setBytes(1, data);
			restcstmt = restconn.prepareCall("begin setblob_procout (?); end;");
			restcstmt.setBlob(1, blb);
			restcstmt.registerOutParameter(1, Types.BLOB);
			restcstmt.execute();
			restcstmt.setBlob(1, blb);
			restcstmt.registerOutParameter(1, Types.BLOB);
			restcstmt.execute();
			Blob b = restcstmt.getBlob(1);
			rest_int_res = (int) b.length();
			// System.out.println("Out argument : "
			// + new String(b.getBytes(1, rest_int_res)));
			b.free();

		} catch (RestJdbcNotImplementedException f) {
			assertTrue("SetBlob method in Callable Statement not implemented",true);
		}
		catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_res, rest_int_res);
	}

	/*
	 * Test setROWID with registeroutparameter types Rowid. <NOT Yet
	 * Implemented>
	 */

	@Category(PassTest.class)
	@Test
	public void testSetROWID_TypesRowid() {
		Connection oracleconn = null, restconn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		String oracle_strg_res = "", rest_strg_res = "";
		String str = "test";
		RowId r = new oracle.dbtools.jdbc.RowId(str);

		// Setup
		try {
			restconn = getRestConnection();

			Util.doSQL(restconn, "drop table orders purge");
			Util.doSQL(restconn, "create or replace procedure setrowid_proc "
					+ "(c1 in out ROWID, c2 in out NUMBER)"
					+ "is begin null;end;");

			Util.doSQL(restconn, "CREATE TABLE orders "
					+ "(order_id number, isbn number)");
			Util.doSQL(restconn,
					"CREATE SEQUENCE seq01 increment by 1 start with 1000");

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {

			oracleconn = getOracleConnection();
			ResultSet rset = null;
			Statement stmt = oracleconn.createStatement();
			stmt.execute("insert into orders (order_id, isbn) "
					+ "values (seq01.NEXTVAL, 966431502)",
					stmt.RETURN_GENERATED_KEYS);
			rset = stmt.getGeneratedKeys();
			rset.next();
			r = rset.getRowId(1);

			oraclecstmt = oracleconn
					.prepareCall("begin setrowid_proc(?,?);end;");

			oraclecstmt.setRowId(1, r);
			oraclecstmt.setInt(2, 123);
			oraclecstmt.registerOutParameter(1, Types.ROWID);
			oraclecstmt.registerOutParameter(2, Types.INTEGER);
			oraclecstmt.execute();
			RowId rid = oraclecstmt.getRowId(1);

			oracle_strg_res = rid.toString();
			// System.out.println("1.getROWID() returns " + rid.toString());
			int num1 = oraclecstmt.getInt(2);
			// System.out.println("1.getNUMBER() returns " + num1);
			oraclecstmt.close();

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restconn.prepareCall("begin setrowid_proc(?,?);end;");
			restcstmt.setRowId(1, r);
			restcstmt.setInt(2, 123);
			restcstmt.registerOutParameter(1, Types.ROWID);
			restcstmt.registerOutParameter(2, Types.INTEGER);
			restcstmt.execute();
			RowId rid = restcstmt.getRowId(1);
			rest_strg_res = rid.toString();
			// System.out.println("1.getROWID() returns " + rid.toString());
			int num1 = restcstmt.getInt(2);
			// System.out.println("1.getNUMBER() returns " + num1);
			restcstmt.close();
		} catch (RestJdbcNotImplementedException f) {
			assertTrue("SetROWID method in Callable Statement not implemented",true);
		}
		catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclecstmt.close();
				restcstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		// assertEquals(oracle_strg_res, rest_strg_res);
	}

	/*
	 * Test addbatch method
	 */

	@Category(PassTest.class)
	@Test
	public void testAddBatch() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		Statement ostmt = null, rstmt = null;
		ResultSet oraclers = null, restrs = null;
		int oracle_int_result = 0, rest_int_result = 0;
		String oracle_strg_result = "", rest_strg_result = "";
		String oracle_dropsql = "drop table ocstmt_Strg purge";
		String rest_dropsql = "drop table rcstmt_Strg1 purge";
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table rcstmt_Strg1 purge");
			Util.doSQL(restConn,
					"create table rcstmt_Strg1 (id number(10), name varchar2(200))");
			Util.doSQL(restConn,
					"create or replace procedure rcstmt_Strg1_proc "
							+ "(id IN NUMBER, " + "name IN VARCHAR2) " + "is "
							+ "begin "
							+ "insert into rcstmt_Strg1 values(id,name); "
							+ "end;  ");

			Util.doSQL(restConn,
					"create table ocstmt_Strg (id number(10), name varchar2(200))");
			Util.doSQL(restConn,
					"create or replace procedure ocstmt_Strg_proc "
							+ "(id IN NUMBER, " + "name IN VARCHAR2) " + "is "
							+ "begin "
							+ "insert into ocstmt_Strg values(id,name); "
							+ "end;  ");

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}
		// Oracle thin driver

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn
					.prepareCall("{call ocstmt_Strg_proc(?,?)}");
			oraclecstmt.setInt(1, 100);
			oraclecstmt.setString(2, "Garry1");
			oraclecstmt.addBatch();
			oraclecstmt.setInt(1, 200);
			oraclecstmt.setString(2, "Garry2");
			oraclecstmt.addBatch();
			oraclecstmt.setInt(1, 300);
			oraclecstmt.setString(2, "Garry3");
			oraclecstmt.addBatch();
			int[] update_count = oraclecstmt.executeBatch();
			oracle_int_result = update_count.length;
			// System.out.println("count - " + update_count.length);
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{call rcstmt_Strg1_proc(?,?)}");
			restcstmt.setInt(1, 100);
			restcstmt.setString(2, "Garry1");
			restcstmt.addBatch();
			restcstmt.setInt(1, 200);
			restcstmt.setString(2, "Garry2");
			restcstmt.addBatch();
			restcstmt.setInt(1, 300);
			restcstmt.setString(2, "Garry3");
			restcstmt.addBatch();
			int[] update_count = restcstmt.executeBatch();
			// NPE occurs here.
			rest_int_result = update_count.length;
			// ("count - " + update_count.length);
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(restConn, rest_dropsql);
				// restrs.close();
				restcstmt.close();
				// rstmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		assertEquals(oracle_int_result, rest_int_result);
	}

	/*
	 * Test <closeoncompletion> method
	 */

	@Category(PassTest.class)
	@Test
	public void testcloseoncompletion_1() {
		Connection oracleConn = null, restConn = null;
		CallableStatement restcstmt1 = null, restcstmt = null;
		Statement ostmt = null, rstmt = null;
		ResultSet oraclers = null, restrs = null;
		int rest_int_result = 0;
		String rest_strg_result = "";
		String rest_dropsql = "drop table rcstmt_Strg1 purge";
		boolean rest_isclosed = false;
		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, rest_dropsql);
			Util.doSQL(restConn,
					"create table rcstmt_Strg1 (id number(10), name varchar2(200))");
			Util.doSQL(restConn,
					"create or replace procedure rcstmt_Strg1_proc "
							+ "(id IN NUMBER, " + "name IN VARCHAR2) " + "is "
							+ "begin "
							+ "insert into rcstmt_Strg1 values(id,name); "
							+ "end;  ");

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restcstmt = restConn.prepareCall("{call rcstmt_Strg1_proc(?,?)}");
			restcstmt.setInt(1, 200);
			restcstmt.setString(2, "Garry");
			restcstmt.execute();

			restcstmt1 = restConn.prepareCall("select * from rcstmt_Strg1");
			restrs = restcstmt1.executeQuery();
			if (restrs.next()) {
				rest_int_result = restrs.getInt(1);
				rest_strg_result = restrs.getString(2);
			}
			restrs.close();

			restcstmt1.closeOnCompletion();
			if (restcstmt1.isCloseOnCompletion()) {
				rest_isclosed = true;
				// System.out.println("Callable statement is closed");
			} else {
				rest_isclosed = false;
				// System.out.println("Callable statement is not closed");
			}
		} catch (SQLException e) {
			fail(e.getMessage());
		}
		assertEquals(true, rest_isclosed);
	}

	/*
	 * Basic testcase to test <registerOutParameter> method with executeBatch.
	 */

	@Category(PassTest.class)
	@Test
	public void testExecuteBatch_INOUTParameters() {
		Connection oracleConn = null, restConn = null;
		CallableStatement oraclecstmt = null, restcstmt = null;
		int oracle_res_int = 0, rest_res_int = 0;
		SQLException oracle_exception = null, rest_exception = null;

		// Run Oracle thin driver.

		try {
			oracleConn = getOracleConnection();
			oraclecstmt = oracleConn.prepareCall(" DECLARE "
					+ " temp VARCHAR2(255); " + " subtype onetype is NUMBER;"
					+ " subtype twotype is temp%TYPE;" + " a1 onetype; "
					+ " b1 twotype; " + " BEGIN " + "   a1 := 0; "
					+ "   ? := a1 + 3; " + "   b1 := ?; " + "   ? := b1; "
					+ " END;");
			oraclecstmt.registerOutParameter(1, Types.INTEGER);
			oraclecstmt.setString(2, "testing subtype1");
			oraclecstmt.registerOutParameter(3, Types.VARCHAR);
			oraclecstmt.addBatch();

			oraclecstmt.registerOutParameter(1, Types.INTEGER);
			oraclecstmt.setString(2, "testing subtype2");
			oraclecstmt.registerOutParameter(3, Types.VARCHAR);
			oraclecstmt.addBatch();

			oraclecstmt.registerOutParameter(1, Types.INTEGER);
			oraclecstmt.setString(2, "testing subtype3");
			oraclecstmt.registerOutParameter(3, Types.VARCHAR);
			oraclecstmt.addBatch();

			int[] update_count = oraclecstmt.executeBatch();
			oracle_res_int = update_count.length;

		} catch (SQLException e) {
			oracle_exception = e;
		} catch(ClassNotFoundException e) {
			fail(e.getMessage());
		}
		finally {
			close(oraclecstmt,restcstmt);
		}


		// Run Rest JDBC driver.
		try {
			restConn = getRestConnection();
			restcstmt = restConn.prepareCall(" DECLARE "
					+ " temp VARCHAR2(255); " + " subtype onetype is NUMBER;"
					+ " subtype twotype is temp%TYPE;" + " a1 onetype; "
					+ " b1 twotype; " + " BEGIN " + "   a1 := 0; "
					+ "   ? := a1 + 3; " + "   b1 := ?; " + "   ? := b1; "
					+ " END;");
			restcstmt.registerOutParameter(1, Types.INTEGER);
			restcstmt.setString(2, "testing subtype1");
			restcstmt.registerOutParameter(3, Types.VARCHAR);
			restcstmt.addBatch();

			restcstmt.registerOutParameter(1, Types.INTEGER);
			restcstmt.setString(2, "testing subtype2");
			restcstmt.registerOutParameter(3, Types.VARCHAR);
			restcstmt.addBatch();

			restcstmt.registerOutParameter(1, Types.INTEGER);
			restcstmt.setString(2, "testing subtype3");
			restcstmt.registerOutParameter(3, Types.VARCHAR);
			restcstmt.addBatch();

			int[] update_count = restcstmt.executeBatch();
			rest_res_int = update_count.length;

		} catch (SQLException e) {
			rest_exception = e;
		} catch(ClassNotFoundException e) {
			fail(e.getMessage());
		}
		finally {
			close(oraclecstmt,restcstmt);
		}
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());
    assertEquals(oracle_exception.getErrorCode(), rest_exception.getErrorCode());
    assertEquals(oracle_exception.getSQLState(), rest_exception.getSQLState());

	}


}
