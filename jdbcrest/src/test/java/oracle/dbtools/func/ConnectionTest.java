/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.func;

import java.io.IOException;
import java.io.Writer;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.FailTest;
import oracle.dbtools.unittest.categories.PassTest;
import oracle.dbtools.unittest.categories.UnsupportedTest;



public class ConnectionTest extends JDBCTestCase {

	/*
	 * Basic test for createStatement method.
	 */

	@Category(PassTest.class)
	@Test
	public void testCreateStmt() {
		Connection conn = null;
		Statement stmt = null;
		Boolean ret = false;
		try {
			conn = getRestConnection();
			stmt = conn.createStatement();
			ret = stmt.execute("create table CreateStmt ( a number)");
			stmt.execute("drop table CreateStmt");
		} catch (SQLException e) {
		} catch (ClassNotFoundException e) {
		} finally {
			try {
				stmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertTrue("Statement executed successfully", ret);
	}

	/*
	 * Test to createStatement after connection is closed.
	 * Oracle thin driver does not throw any error But
	 * Rest JDBC driver throws NPE.
	 *
	 */

	@Category(PassTest.class)
	@Test(expected = NullPointerException.class)
	public void testCreateStmt_AfterConnClose() {
		Connection oConn = null, rConn = null;
		SQLException oracleException = null, restException = null;
		
		// Oracle Thin Driver
    try {
      oConn = getOracleConnection();
      oConn.close();
      Boolean ret = oConn.createStatement().execute("create table CreateStmt_AfterConnClose ( a number)");
    } catch (ClassNotFoundException | NullPointerException e) {
      fail(e.getMessage());
    }
    catch(SQLException e) {
      oracleException = e;
    }		
		// Rest Driver
		try {
			rConn = getRestConnection();
			rConn.close();
			Boolean ret = rConn.createStatement().execute("create table CreateStmt_AfterConnClose ( a number)");
		} catch (ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}
		catch(SQLException e) {
		  restException = e;
		}		
		assertEquals(oracleException.getMessage(),restException.getMessage());
    assertEquals(oracleException.getErrorCode(),restException.getErrorCode());	}
	

	/*
	 * Test to create prepareStatement using connection.
	 * Run executeUpdate on prepareStatement to insert values into table.
	 */

	@Category(PassTest.class)
	@Test
	public void testPrepareStmt_Insert() {
		Connection conn = null;
		int ret = 0;
		PreparedStatement ps = null;
		String sql = "CREATE TABLE PREPARESTMT_INSERT "
				+ "(DEPTNO NUMBER(2) NOT NULL, " + " DNAME VARCHAR2(14) , "
				+ " LOC VARCHAR2(13))";
		String insertsql = "insert into PREPARESTMT_INSERT values (?,?,?)";
		try {
			conn = getRestConnection();
			Util.trySQL(conn, sql);
			ps = conn.prepareStatement(insertsql);
			ps.setInt(1, 10);
			ps.setString(2, "ACCOUNTING");
			ps.setString(3, "NEW YORK");
			ret = ps.executeUpdate();
			ps.execute("DROP TABLE PREPARESTMT_INSERT");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(1, ret);
	}

	/*
	 * Test to create prepareStatement using connection.
	 * Run executeUpdate on prepareStatement to update values in table.
	 */

	@Category(PassTest.class)
	@Test
	public void testPrepareStmt_Update() {
		Connection conn = null;
		int ret = 0;
		Statement stmt = null;
		PreparedStatement ps_insert = null, ps_update = null;
		String sql = "CREATE TABLE PREPARESTMT_UPDATE "
				+ "(DEPTNO NUMBER(2) NOT NULL, " + " DNAME VARCHAR2(14) , "
				+ " LOC VARCHAR2(13))";
		String insertsql = "insert into PREPARESTMT_UPDATE values (?,?,?)";
		String updatesql = "UPDATE PREPARESTMT_UPDATE SET LOC = ? "
				+ " WHERE DEPTNO = ?";
		try {
			conn = getRestConnection();
			Util.trySQL(conn, sql);
			ps_insert = conn.prepareStatement(insertsql);
			ps_insert.setInt(1, 10);
			ps_insert.setString(2, "ACCOUNTING");
			ps_insert.setString(3, "NEW YORK");
			ps_insert.executeUpdate();
			ps_update = conn.prepareStatement(updatesql);
			ps_update.setString(1, "CANADA");
			ps_update.setInt(2, 10);
			ret = ps_update.executeUpdate();

			stmt = conn.createStatement();
			stmt.execute("DROP TABLE PREPARESTMT_UPDATE");
		} catch (SQLException e) {
		} catch (ClassNotFoundException e) {
		} finally {
			try {
				if (ps_insert != null) {
					ps_insert.close();
				}
				if (ps_update != null) {
					ps_update.close();
				}
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(1, ret);
	}

	/**
	 * Test to create connection. Create a prepareStatement using Connection.
	 * Create a prepare statement on non existing table.
	 */

	@Category(PassTest.class)
	@Test
	public void testPrepareStmt_TableDoesntExist() {
		Connection oracleconn = null, restconn = null;
		SQLException oracle_exception = null, rest_exception = null;

		// Run Oracle thin driver
		try {
			oracleconn = getOracleConnection();
			PreparedStatement pstm1 = oracleconn
					.prepareStatement("insert into table PrepareStmt_TableDoesntExist (?)");
			pstm1.setInt(1, 10);
			pstm1.executeUpdate();
		} catch (SQLException e) {
			oracle_exception = e;
		} catch (ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Run REST JDBC driver
		try {
			restconn = getRestConnection();
			PreparedStatement pstm1 = restconn
					.prepareStatement("insert into table PrepareStmt_TableDoesntExist (?)");
			pstm1.setInt(1, 10);
			pstm1.executeUpdate();
		} catch (SQLException e) {
			rest_exception = e;
		} catch (ClassNotFoundException e) {
			fail(e.getMessage());
		}
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage().trim(), rest_exception.getMessage().trim());
	}

	/*
	 * Test getschema method.
	 * Test fails as getSchema method is not supported.
	 */

	@Category(UnsupportedTest.class)
	@Test
	public void testConn_getSchema() {
		Connection conn = null;
		String actualschema = "";
		try {
			conn = getRestConnection();
			actualschema = conn.getSchema();
		}catch (RestJdbcNotImplementedException f) {
			assertTrue ("getSschema method in Connection class is not supported",true);
		}
		catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}
		// assertEquals("ADHOCSQLTEST", actualschema);
	}

	/*
	 * Test to createClob using connection.
	 */

	@Category(PassTest.class)
	@Test
	public void testCreatClob() throws IOException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		Statement stmt = null, stmt1 = null;
		Boolean ret = false, ret1 = false;

		try {
			conn = getRestConnection();
			stmt = conn.createStatement();
			ret = stmt.execute("create table Conn_Clobtable_X ( a clob)");
			if (ret) {
				String sql = "insert into Conn_Clobtable_X values(?) ";
				pstmt = conn.prepareStatement(sql);
				Clob clob = conn.createClob();
				Writer clobWriter = clob.setCharacterStream(1);
				String text = "basketball, reading, music...";
				clobWriter.write(text);
				clobWriter.close();
				pstmt.setClob(1, clob);
				pstmt.executeUpdate();
				pstmt.close();
			}
			stmt1 = conn.createStatement();
			ret1 = stmt1.execute("select count(*) from Conn_Clobtable_X");
			stmt1.close();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		} finally {
			try {
				stmt.execute("Drop table Conn_Clobtable_X purge");
				stmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertTrue("Statement executed successfully", ret);
	}

	/**
	 * Test to create preparedStatement using connection.
	 * Pass a SQL statement with semicolon to preparedStatement.
	 * Actually, semicolon should not be allowed at the end of the sql passed to
	 * PreparedSatement.
	 * ORA-00933 is expected. 
	 * Not supported in the REST JDBC driver. 
	 * The REST JDBC driver will not throw an exception if there is a semicolon at the end of the sql statement
	 * This is allowed in REST SQL and ORDS too. 
	 */

	@Category(UnsupportedTest.class)
	@Test
	public void testPrepareStmt_withsemicolon() {
		Connection restconn = null, oracleconn = null;
		int ret = 0, errorcode = -1;
		PreparedStatement ps = null;
		String sql = "CREATE TABLE PREPARESTMT_WOSEMICOLON "
				+ "(DEPTNO NUMBER(2) NOT NULL, " + " DNAME VARCHAR2(14) , "
				+ " LOC VARCHAR2(13))";
		SQLException oracle_exception = null, rest_exception = null;
		//Setup
		try {
			restconn = getRestConnection();
			Util.trySQL(restconn, sql);
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		//Run Oracle thin driver
		try {
			oracleconn = getOracleConnection();
			ps = oracleconn
					.prepareStatement("insert into PREPARESTMT_WOSEMICOLON values (?,?,?);");
			ps.setInt(1, 10);
			ps.setString(2, "ACCOUNTING");
			ps.setString(3, "NEW YORK");
			ret = ps.executeUpdate();
			ps.execute("DROP TABLE PREPARESTMT_WOSEMICOLON");
		} catch (SQLException e) {
			oracle_exception = e;

		} catch (NullPointerException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			ps = restconn
					.prepareStatement("insert into PREPARESTMT_WOSEMICOLON values (?,?,?);");
			ps.setInt(1, 10);
			ps.setString(2, "ACCOUNTING");
			ps.setString(3, "NEW YORK");
			ret = ps.executeUpdate();
			ps.execute("DROP TABLE PREPARESTMT_WOSEMICOLON");
		} catch (SQLException e) {
			rest_exception = e;
		} catch (NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}

		// NPE occurs in below line
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());

	}

	/*
	 * Test setClientInfo method after creating connection.
	 * Assertion failed for 'clientInfoValue[2]' value in below testcase.
	 * setClientInfo isn't implemented 
	 */

	@Category(UnsupportedTest.class)
	@Test
	public void testConn_SetclientInfo_afterconn() {
		Connection conn = null;
		Statement stmt = null;
		String[] clientInfoName = { "client_info_1", "client_info_2",
				"client_info_3", "client_info_4", "client_info_5" };
		String[] clientInfoValue = { "client_info_1: this is client info 1",
				"client_info_2: this is loooooooooooong client info 2",
				"client_info_3: ~~~``6^&&&&$$ ?(**&*&^^&%GHKJLK:,m,mj9uew", "",
				"   " };
		String[] actual = { "", "", "", "", "", "" };

		try {
			conn = getRestConnection();
			for (int i = 0; i < clientInfoName.length; i++) {
				conn.setClientInfo(clientInfoName[i], clientInfoValue[i]);
			}

			for (int i = 0; i < clientInfoName.length; i++) {
				actual[i] = conn.getClientInfo(clientInfoName[i]);
			}

		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}
		assertEquals(clientInfoValue[1], actual[1]);
		assertEquals(clientInfoValue[2], actual[2]);
		assertEquals(clientInfoValue[3], actual[3]);
		assertEquals(clientInfoValue[4], actual[4]);
		assertEquals(clientInfoValue[5], actual[5]);

	}

	/*
	 * Test setClientInfo method with properties after statement creation.
	 * Test getClientInfo method with properties.
	 * <Not Implemented>.
	 */

	@Category(PassTest.class)
	@Test
	public void testConn_SetclientInfo_afterstmtcreation() {
		Connection conn = null;
		Statement stmt = null;
		String[] clientInfoName = { "client_info_1", "client_info_2",
				"client_info_3", "client_info_4", "client_info_5" };
		String[] clientInfoValue = { "client_info_1: this is client info 1",
				"client_info_2: this is loooooooooooong client info 2",
				"client_info_3: ~~~``6^&&&&$$ ?(**&*&^^&%GHKJLK:,m,mj9uew", "",
				"   " };
		String[] actual = { "", "", "", "", "", "" };

		try {
			conn = getRestConnection();
			stmt = conn.createStatement();
			Properties properties = new Properties();
			for (int i = 0; i < clientInfoName.length; i++) {
				properties.put(clientInfoName[i], clientInfoValue[i]);
			}
			conn.setClientInfo(properties);

			for (int i = 0; i < clientInfoName.length; i++) {
				actual[i] = conn.getClientInfo(clientInfoName[i]);
			}

		} catch (RestJdbcNotImplementedException f) {
			assertTrue("getClientInfo method (With properties) in connection class not implemented",true);
		}
		catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}
		/*assertEquals(clientInfoValue[1], actual[1]);
		assertEquals(clientInfoValue[2], actual[2]);
		assertEquals(clientInfoValue[3], actual[3]);
		assertEquals(clientInfoValue[4], actual[4]);
		assertEquals(clientInfoValue[5], actual[5]);*/
	}

	/*
	 * Test setClientInfo method using properties after executeQuery.
	 *  Test getClientInfo method with properties.
	 * <Not Implemented>.
	 */

	@Category(PassTest.class)
	@Test
	public void testConn_SetclientInfo_afterquery() {
		Connection conn = null;
		Statement stmt = null;
		String[] clientInfoName = { "client_info_1", "client_info_2",
				"client_info_3", "client_info_4", "client_info_5" };
		String[] clientInfoValue = { "client_info_1: this is client info 1",
				"client_info_2: this is loooooooooooong client info 2",
				"client_info_3: ~~~``6^&&&&$$ ?(**&*&^^&%GHKJLK:,m,mj9uew", "",
				"   " };
		String[] actual = { "", "", "", "", "", "" };
		ResultSet rset = null;

		try {
			conn = getRestConnection();
			stmt = conn.createStatement();
			String query = "select 'Hello JDBC: ' || sysdate from dual";
			rset = stmt.executeQuery(query);
			Properties properties = new Properties();
			for (int i = 0; i < clientInfoName.length; i++) {
				properties.put(clientInfoName[i], clientInfoValue[i]);
			}
			conn.setClientInfo(properties);

			for (int i = 0; i < clientInfoName.length; i++) {
				actual[i] = conn.getClientInfo(clientInfoName[i]);
			}

		} catch (RestJdbcNotImplementedException f) {
			assertTrue("getClientInfo method (With properties) in connection class not implemented",true);
		}
		catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		} finally {
			try {
				if (!rset.isClosed()) {
					rset.close();
				}
				if (!stmt.isClosed()) {
					stmt.close();
				}
			} catch (SQLException e) {
				fail(e.getMessage());
			}
		}
		/* assertEquals(clientInfoValue[1], actual[1]);
		assertEquals(clientInfoValue[2], actual[2]);
		assertEquals(clientInfoValue[3], actual[3]);
		assertEquals(clientInfoValue[4], actual[4]);
		assertEquals(clientInfoValue[5], actual[5]); */
	}

	/*
	 * Test setClientInfo method after connection closed.
	 * <Not Implemented>.
	 */

	@Category(PassTest.class)
	@Test
	public void testConn_SetclientInfo_afterconnclose() {
		Connection conn = null;
		Statement stmt = null;
		String[] clientInfoName = { "client_info_1", "client_info_2",
				"client_info_3", "client_info_4", "client_info_5" };
		String[] clientInfoValue = { "client_info_1: this is client info 1",
				"client_info_2: this is loooooooooooong client info 2",
				"client_info_3: ~~~``6^&&&&$$ ?(**&*&^^&%GHKJLK:,m,mj9uew", "",
				"   " };
		String[] actual = { "", "", "", "", "", "" };
		ResultSet rset = null;

		try {
			conn = getRestConnection();
			stmt = conn.createStatement();
			String query = "select 'Hello JDBC: ' || sysdate from dual";
			rset = stmt.executeQuery(query);
			rset.close();
			stmt.close();
			conn.close();

			for (int i = 0; i < clientInfoName.length; i++) {
				conn.setClientInfo(clientInfoName[i], clientInfoValue[i]);
			}

			for (int i = 0; i < clientInfoName.length; i++) {
				actual[i] = conn.getClientInfo(clientInfoName[i]);
			}

		}catch (RestJdbcNotImplementedException f) {
			assertTrue("setClientInfo(String name, String value) in Connection is not implemented",true);
		}
		catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}
		/*assertEquals(clientInfoValue[1], actual[1]);
		assertEquals(clientInfoValue[2], actual[2]);
		assertEquals(clientInfoValue[3], actual[3]);
		assertEquals(clientInfoValue[4], actual[4]);
		assertEquals(clientInfoValue[5], actual[5]);*/
	}

	/*
	 * Test getAutocommit method after connection is established.
	 */

	@Category(PassTest.class)
	@Test
	public void testConn_getAutoCommit() {
		Connection conn = null;
		boolean actual_getCommit = false;
		try {
			conn = getRestConnection();
			actual_getCommit = conn.getAutoCommit();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}
		assertTrue("Get Auto commit should be true", actual_getCommit);
	}

	/*
	 * Test getAutocommit method after connection closed.
	 * Assertion Failed: Expected exception code is <17008>.
	 * SQLState from the thin driver is not accurate. 
	 */
	@Category(PassTest.class)
	@Test
	public void testConn_getAutoCommit_connclosed() {
		Connection conn = null;
		SQLException oracle_exception = null, rest_exception = null;

		// Run Oracle thin driver
		try {
			conn = getOracleConnection();
			conn.close();
			conn.getAutoCommit();
		} catch (SQLException e) {
			oracle_exception = (SQLException) e;
		} catch (ClassNotFoundException | NullPointerException se) {
			fail(se.getMessage());
		}

		// Run REST JDBC driver

		try {
			conn = getRestConnection();
			conn.close();
			conn.getAutoCommit();
		} catch (SQLException e) {
			rest_exception = (SQLException) e;
		} catch (ClassNotFoundException | NullPointerException se) {
			fail(se.getMessage());
		}
		assertEquals(oracle_exception.getErrorCode(), rest_exception.getErrorCode());
		//assertEquals(oracle_exception.getSQLState(), rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());
	}

	/*
	 * Test Connection getmetadata. Only few methods are tested. This test can
	 * be extended further. Expected dbversion is <12.2.0.1.0> actual is <>
	 */

	@Category(PassTest.class)
	@Test
	public void testConn_GetMetadata() {
		Connection conn = null;
		DatabaseMetaData dbmd = null;
		String oracle_drvname = "", oracle_drvVersion = "", oracle_usrname = "", oracle_dbprodname = "", oracle_dbprodversion = "";
		String rest_drvname = "", rest_drvVersion = "", rest_usrname = "", rest_dbprodname = "", rest_dbprodversion = "";

		// Oracle thin driver

		try {
			conn = getOracleConnection();
			dbmd = conn.getMetaData();
			oracle_drvname = dbmd.getDriverName();
			oracle_drvVersion = dbmd.getDriverVersion();
			oracle_usrname = dbmd.getUserName();
			oracle_dbprodname = dbmd.getDatabaseProductName();
			oracle_dbprodversion = dbmd.getDatabaseProductVersion();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC connection
		try {
			conn = getRestConnection();
			dbmd = conn.getMetaData();
			rest_drvname = dbmd.getDriverName();
			rest_drvVersion = dbmd.getDriverVersion();
			rest_usrname = dbmd.getUserName();
			rest_dbprodname = dbmd.getDatabaseProductName();
			rest_dbprodversion = dbmd.getDatabaseProductVersion();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		//driver versions do not match 12.1.0.2.0 vs 12.2.0.1.0 assertEquals(oracle_drvVersion, rest_drvVersion);
		assertEquals(rest_usrname, rest_usrname);
		assertEquals(rest_dbprodname, rest_dbprodname);
		assertEquals(rest_dbprodversion, rest_dbprodversion);
	}

	/*
	 * Test setSchema method.
	 * Method setSchema(String schema) in Connection not implemented.
	 */

	@Category(PassTest.class)
	@Test
	public void testConn_setSchema() {
		Connection restconn = null, conn = null;
		Statement oracle_stmt = null, oracle_stmtb = null, rest_stmt = null, rest_stmtb = null;
		ResultSet oracle_rsb = null, oracle_rs = null, rest_rsb = null, rest_rs = null;
		String queryb = "select * from employees";
		String query = "select a from testconn";
		String oracle_currentschema_res = "", oracle_newschema_res = "";
		String rest_currentschema_res = "", rest_newschema_res = "";

		// Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(restconn, "drop table testconn");
			Util.doSQL(restconn, "create table testconn ( a number)");
			Util.doSQL(restconn, "insert into testconn values (1)");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver.
		try {
			conn = getOracleConnection();
			oracle_stmt = conn.createStatement();
			conn.setSchema("HR");
			oracle_stmtb = conn.createStatement();
			oracle_rsb = oracle_stmtb.executeQuery(queryb);
			oracle_rsb.next();
			oracle_newschema_res = oracle_rsb.getString("first_name");
			conn.setSchema("ADHOCSQLTEST");
			oracle_rsb = oracle_stmtb.executeQuery(query);
			oracle_rs = oracle_stmt.executeQuery(query);
			oracle_rs.next();
			oracle_currentschema_res = oracle_rs.getString("a");
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run REST JDBC driver.
		try {
			rest_stmt = restconn.createStatement();
			restconn.setSchema("HR");
			rest_stmtb = restconn.createStatement();
			rest_rsb = rest_stmtb.executeQuery(queryb);
			rest_rsb.next();
			rest_newschema_res = rest_rsb.getString("first_name");
			restconn.setSchema("ADHOCSQLTEST");
			rest_rsb = rest_stmtb.executeQuery(query);
			rest_rs = rest_stmt.executeQuery(query);
			rest_rs.next();
			rest_currentschema_res = rest_rs.getString("a");
		} catch (RestJdbcNotImplementedException f) {
			assertTrue("setSchema(String schema) method in Connection not implemented",true);
		}

		catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		}

		//assertEquals(oracle_currentschema_res, rest_currentschema_res);
		//assertEquals(oracle_newschema_res, rest_newschema_res);

	}


}
