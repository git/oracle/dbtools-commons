/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.func;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.FailTest;
import oracle.dbtools.unittest.categories.PassTest;
import oracle.dbtools.unittest.categories.UnsupportedTest;

public class PreparedStatementTest extends JDBCTestCase {

	/*
	 * Basic testcase to test <executeUpdate> method.
	 * Set the IN parameters and run executeUpdate.
	 */

	@Category(PassTest.class)
	@Test
	public void testExecute_1() {
		Connection conn = null;
		PreparedStatement pstmt = null, pstmt1 = null;
		String sql = "CREATE TABLE testPrepareStmt_Execute1 "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPrepareStmt_Execute1 values (?,?,?)";
		String sql_select = "select count(*) as rowcount from testPrepareStmt_Execute1";
		String sql_drop = "drop table testPrepareStmt_Execute1 purge";
		int count = 0;

		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_drop);
			Util.doSQL(conn, sql);
			pstmt = conn.prepareStatement(sql_insert);
			pstmt1 = conn.prepareStatement(sql_select);

			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();

			pstmt.setString(1, "Smith");
			pstmt.setString(2, "Anderson");
			pstmt.setLong(3, 101);
			pstmt.executeUpdate();

			pstmt.setString(1, "Adams");
			pstmt.setString(2, "Paul");
			pstmt.setLong(3, 102);
			pstmt.executeUpdate();

			ResultSet rs = pstmt1.executeQuery();
			rs.next();
			count = rs.getInt("rowcount");
			rs.close();
		} catch (SQLException e) {
			fail(e.getMessage());
		} catch (ClassNotFoundException e) {
			fail(e.getMessage());
		} catch (NullPointerException e) {
			fail(e.getMessage());
		} finally {
			Util.doSQL(conn, sql_drop);
		}
		assertEquals(3, count);
	}

	/*
	 * Test fails due to difference in SQLException getmessage between oracle thin driver and REST Jdbc
	 */

	@Category(PassTest.class)
	@Test
	public void testSetString_Char() {
		Connection oracle_conn = null, rest_conn = null;
		PreparedStatement opstmt = null, rpstmt = null;
		Statement ostmt = null, rstmt = null;
		String sql_createtable = "create table rSetString_CharCol (c char(255))";
		String sql_select = "select * from rSetString_CharCol";
		String sql_insert = "insert into rSetString_CharCol values (?)";
		String sql_drop = "drop table rSetString_CharCol purge";
		SQLException oracle_exception = null, rest_exception = null;
		String char_string = new String("aaaaa");
		int oracle_char_string_length = 0, rest_char_string_length = 0;

		// Setup
		try {
			Connection conn = getRestConnection();
			Util.doSQL(conn, sql_drop);
			Util.doSQL(conn, sql_createtable);
			// create test string of length 255.
			for (int i = 1; i <= 25; i++) {
				char_string = char_string.concat("aaaaaaaaaa");
			}
			rpstmt = conn.prepareStatement(sql_insert);
			rpstmt.setString(1, char_string);
			rpstmt.execute();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle Thin Driver
		try {
			oracle_conn = getOracleConnection();
			ostmt = oracle_conn.createStatement();
			ResultSet rs = ostmt.executeQuery(sql_select);
			while (rs.next()) {
				oracle_char_string_length = rs.getString(1).length();
			}
			// Create test string of length 255+1 to Insert value too large for
			// column.

			String oracle_char_string = char_string.concat("a");
			opstmt = oracle_conn.prepareStatement(sql_insert);
			opstmt.setString(1, oracle_char_string);
			opstmt.execute();
		} catch (SQLException e) {
			oracle_exception = e;
		} catch (NullPointerException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Run REST JDBC Driver
		try {
			rest_conn = getRestConnection();
			rstmt = rest_conn.createStatement();
			ResultSet rs = rstmt.executeQuery(sql_select);
			while (rs.next()) {
				rest_char_string_length = rs.getString(1).length();
			}

			String rest_char_string = char_string.concat("a");
			rpstmt.setString(1, rest_char_string);
			rpstmt.execute();
		} catch (SQLException e) {
			rest_exception = e;
		} catch (NullPointerException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		finally {
			try {
				Connection conn = getRestConnection();
				Util.doSQL(conn, sql_drop);
				close(opstmt, rpstmt);
				close(ostmt, rstmt);
			} catch (SQLException | NullPointerException
					| ClassNotFoundException e) {
				fail(e.getMessage());
			}
		}

		assertEquals(oracle_char_string_length, rest_char_string_length);
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage().trim(), rest_exception.getMessage().trim());
	}

	/*
	 * Test fails due to difference in SQLException getmessage between oracle
	 * thin driver and REST Jdbc
	 */
	@Category(PassTest.class)
	@Test
	public void testSetString_Varchar() {
		Connection conn = null, oracle_conn = null, rest_conn = null;
		PreparedStatement rpstmt = null, opstmt = null;
		Statement ostmt = null, rstmt = null;
		String sql_createtable = "create table SetString_Varcharcol (v varchar2(2000))";
		String sql_select = "select * from SetString_Varcharcol";
		String sql_insert = "insert into SetString_Varcharcol values (?)";
		String sql_drop = "drop table SetString_Varcharcol purge";
		String varchar_string = new String();
		int oracle_varchar_string_length = 0, rest_varchar_string_length = 0;
		SQLException oracle_exception = null, rest_exception = null;
		int oracle_errorcode = -1, rest_errorcode = -1;

		//Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_drop);
			Util.doSQL(conn, sql_createtable);
			// create test string
			for (int i = 1; i <= 100; i++) {
				varchar_string = varchar_string.concat("aaaaaaaaaaaaaaaaaaaa");
			}
			rpstmt = conn.prepareStatement(sql_insert);
			rpstmt.setString(1, varchar_string);
			rpstmt.execute();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			ostmt = oracle_conn.createStatement();
			ResultSet rs = ostmt.executeQuery(sql_select);
			while (rs.next()) {
				oracle_varchar_string_length = rs.getString(1).length();
			}

			// Create test string of length 255+1 to Insert value too large for
			// column.
			String oracle_varchar_string = varchar_string.concat("a");
			opstmt = oracle_conn.prepareStatement(sql_insert);
			opstmt.setString(1, oracle_varchar_string);
			opstmt.execute();
		} catch (SQLException e) {
			oracle_exception = e;
			oracle_errorcode = e.getErrorCode();
		} catch (ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		//Run REST JDBC driver
		try {
			rest_conn = getRestConnection();
			rstmt = rest_conn.createStatement();
			ResultSet rs = rstmt.executeQuery(sql_select);
			while (rs.next()) {
				rest_varchar_string_length = rs.getString(1).length();
			}

			// Create test string of length 255+1 to Insert value too large for
			// column.
			String rest_varchar_string = varchar_string.concat("a");
			rpstmt.setString(1, rest_varchar_string);
			rpstmt.execute();
		}

		catch (SQLException e) {
			rest_exception = e;
			rest_errorcode = e.getErrorCode();
		} catch (ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(conn, sql_drop);
				close(opstmt, rpstmt);
			} catch (NullPointerException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_varchar_string_length, rest_varchar_string_length);
		assertEquals(oracle_errorcode, rest_errorcode);
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage().trim(), rest_exception.getMessage().trim());
	}

	/*
	 * Test to set parameter using setString with NULL value.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetString_InsertNull() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		boolean ret = false;
		String sql_createtable = "create table SetString_InsertNull (n number)";
		String sql_insert = "insert into SetString_InsertNull values (?)";
		String sql_drop = "drop table SetString_InsertNull";
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_createtable);
			pstmt = conn.prepareStatement(sql_insert);
			pstmt.setString(1, "");
			ret = pstmt.execute();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(conn, sql_drop);
				pstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertFalse("Successfully inserted null value", ret);
	}

	/*
	 * Test setBoolean and getBoolean method.
	 * Invalid column type error occurs when run in Rest JDBC driver.
	 * Runs fine in oracle thin driver.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetBoolean() {
		Connection restconn = null, oracleconn = null;
		PreparedStatement rest_pstmt = null, oracle_pstmt = null;
		ResultSet rs = null;

		String oracle_sql_createtable = "create table oracle_SetBoolean (col1 number, col2 number, col3 varchar(20), col4 char(20))";
		String rest_sql_createtable = "create table rest_SetBoolean (col1 number, col2 number, col3 varchar(20), col4 char(20))";
		String oracle_sql_select = "select * from oracle_SetBoolean";
		String rest_sql_select = "select * from rest_SetBoolean";
		String oracle_sql_insert = "insert into oracle_SetBoolean values (?,?,?,?)";
		String rest_sql_insert = "insert into rest_SetBoolean values (?,?,?,?)";
		String oracle_sql_drop = "drop table oracle_SetBoolean purge";
		String rest_sql_drop = "drop table rest_SetBoolean purge";

		String[] mystr = new String[2];
		mystr[0] = new String("value 0");
		mystr[1] = new String("value 1");
		int count = -1, rest_rcol2 = -1, oracle_rcol2 = -1;
		Boolean rest_rcol1 = false, oracle_rcol1 = false;
		String oracle_rcol3 = "", oracle_rcol4 = "", rest_rcol3 = "", rest_rcol4 = "";
		SQLException oracle_exception = null, rest_exception = null;

		//Setup
		try {
			restconn = getRestConnection();
			Util.doSQL(restconn, oracle_sql_drop);
			Util.doSQL(restconn, rest_sql_drop);
			Util.doSQL(restconn, oracle_sql_createtable);
			Util.doSQL(restconn, rest_sql_createtable);
		}catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle driver

		try {
			oracleconn = getOracleConnection();
			oracle_pstmt = oracleconn.prepareStatement(oracle_sql_insert);
			/*
			 * 1. Set col. values.
			 * 2. Clear parameters.
			 * 3. Again set the col. values.
			 * 4. Execute paramters.
			 */
			oracle_pstmt.setBoolean(1, false);
			oracle_pstmt.setInt(2, 0);
			oracle_pstmt.setString(3, mystr[0]);
			oracle_pstmt.setString(4, mystr[0]);

			oracle_pstmt.clearParameters();

			oracle_pstmt.setBoolean(1, true);
			oracle_pstmt.setInt(2, 2);
			oracle_pstmt.setString(3, mystr[1]);
			oracle_pstmt.setString(4, mystr[1]);
			oracle_pstmt.execute();
			rs = oracle_pstmt.executeQuery(oracle_sql_select);
			while (rs.next()) {
				oracle_rcol1 = rs.getBoolean("col1");
				oracle_rcol2 = rs.getInt("col2");
				oracle_rcol3 = rs.getString("col3");
				oracle_rcol4 = rs.getString("col4");
			}
				rs.close();
			} catch (SQLException | ClassNotFoundException | NullPointerException e) {
				fail(e.getMessage());
			}


		// Run Rest JDBC driver

		try {
			rest_pstmt = restconn.prepareStatement(rest_sql_insert);

			/*
			 * 1. Set col. values.
			 * 2. Clear parameters.
			 * 3. Again set the col. values.
			 * 4. Execute paramters.
			 */
			rest_pstmt.setBoolean(1, false);
			rest_pstmt.setInt(2, 0);
			rest_pstmt.setString(3, mystr[0]);
			rest_pstmt.setString(4, mystr[0]);

			rest_pstmt.clearParameters();

			rest_pstmt.setBoolean(1, true);
			rest_pstmt.setInt(2, 2);
			rest_pstmt.setString(3, mystr[1]);
			rest_pstmt.setString(4, mystr[1]);
			rest_pstmt.execute();

			rs = rest_pstmt.executeQuery(rest_sql_select);

			while (rs.next()) {
				rest_rcol1 = rs.getBoolean("col1");
				rest_rcol2 = rs.getInt("col2");
				rest_rcol3 = rs.getString("col3");
				rest_rcol4 = rs.getString("col4");
			}
			rs.close();
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		}finally {
			close(oracle_pstmt,rest_pstmt);
		}

		assertEquals(oracle_rcol2, rest_rcol2);
		assertEquals(oracle_rcol3, rest_rcol3);
		assertEquals(oracle_rcol1,rest_rcol1);
	}

	/*
	 * Test to set parameter using setShort method.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetShort() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String sql_createtable = "create table SetShort (scol1 number, fcol2 number, dcol3 number, icol4 number,lcol5 number)";
		String sql_select = "select * from SetShort";
		String sql_insert = "insert into SetShort values (?,?,?,?,?)";
		String sql_drop = "drop table SetShort purge";

		int int_rcol = -1;
		float float_col = (float) 2001.31, float_rcol = -1;
		short short_col = 32767, short_rcol = -1;
		double double_col = 9223372036854775808.12345, double_rcol = -1;

		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_createtable);
			pstmt = conn.prepareStatement(sql_insert);

			pstmt.setShort(1, short_col);
			pstmt.setFloat(2, float_col);
			pstmt.setDouble(3, double_col);
			pstmt.setInt(4, 123);
			pstmt.setInt(5, 123456789);
			pstmt.execute();
			rs = pstmt.executeQuery(sql_select);
			while (rs.next()) {
				short_rcol = rs.getShort("scol1");
				float_rcol = rs.getFloat("fcol2");
				double_rcol = rs.getDouble("dcol3");
				int_rcol = rs.getInt("icol4");
			}

		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(conn, sql_drop);
				rs.close();
				pstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(short_col, short_rcol);
		assertEquals(float_col, float_rcol, 1);
		assertEquals(double_col, double_rcol);
		assertEquals(123, int_rcol);
	}

	/*
	 * Test where value is not supplied to all column index.
	 * Oracle thin driver throws error code 17041 whereas
	 * Rest JDBC driver does not throw any error.
	 */

	@Category(PassTest.class)
	@Test
	public void testMissingIndex() {
		Connection conn = null, oracle_conn = null, rest_conn = null;
		PreparedStatement pstmt = null;

		String sql_createtable = "create table MissingIndex (col1 number, col2 number)";
		String sql_insert = "insert into MissingIndex values (?,?)";
		String sql_drop = "drop table MissingIndex purge";

		SQLException oracle_exception = null, rest_exception = null;
		int oracle_errorcode = -1, rest_errorcode = -1;

		// Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_createtable);
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			pstmt = oracle_conn.prepareStatement(sql_insert);
			pstmt.setInt(1, 123);
			pstmt.execute();

		} catch (SQLException e) {
			oracle_exception = e;
			oracle_errorcode = e.getErrorCode();
		} catch (ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Rest JDBC driver
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_createtable);
			pstmt = conn.prepareStatement(sql_insert);
			pstmt.setInt(1, 123);
			pstmt.execute();
		}

		catch (SQLException e) {
			rest_exception = e;
			rest_errorcode = e.getErrorCode();
		} catch (ClassNotFoundException | NullPointerException e) {

			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(conn, sql_drop);
				pstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_errorcode, rest_errorcode);
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());
	}

	/*
	 * Test to setTimestamp method using parameter Index.
	 * Timestamp microseconds precision is restricted to 3 digits
	 * due to JSON limitations.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetTimestamp() {
		Connection conn = null, oracle_conn = null, rest_conn = null;
		PreparedStatement opstmt = null, rpstmt = null;
		ResultSet oracle_rs = null, rest_rs = null;
		Timestamp ts_oracle_col = null, ts_rest_col = null;
		String sql_createtable = "create table SetTimestamp (col1 number, col2 timestamp)";
		String sql_insert = "insert into SetTimestamp values (?,?)";
		String sql_drop = "drop table SetTimestamp purge";

		//Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_createtable);
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(sql_insert);
			opstmt.setInt(1, 123);
			opstmt.setTimestamp(2,Timestamp.valueOf("2017-07-01 03:04:05.01234567"));
			opstmt.execute();
			oracle_rs = opstmt.executeQuery("select * from SetTimestamp");
			while (oracle_rs.next()) {
				ts_oracle_col = oracle_rs.getTimestamp("col2");
			}

		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run REST JDBC driver
		try {
			rpstmt = conn.prepareStatement(sql_insert);
			rpstmt.setInt(1, 123);
			rpstmt.setTimestamp(2,Timestamp.valueOf("2017-07-01 03:04:05.01234567"));
			rpstmt.execute();
			rest_rs = rpstmt.executeQuery("select * from SetTimestamp");
			while (rest_rs.next()) {
				ts_rest_col = rest_rs.getTimestamp("col2");
			}
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			Util.doSQL(conn, sql_drop);
			close(opstmt, rpstmt);
			close(oracle_rs, rest_rs);
		}
		assertEquals(ts_oracle_col.toString().substring(0,23), ts_rest_col.toString());
	}

	/*
	 * Test to set parameter using setDate method.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetDate() {
		Connection conn = null, oracle_conn = null;
		PreparedStatement opstmt = null, rpstmt = null;
		ResultSet oracle_rs = null, rest_rs = null;
		Date date_oracle_col = null, date_rest_col = null;
		String sql_createtable = "create table SetDate (col1 number, col2 date)";
		String sql_insert = "insert into SetDate values (?,?)";
		String sql_drop = "drop table SetDate purge";

		// Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_createtable);
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle Thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(sql_insert);
			opstmt.setInt(1, 123);
			opstmt.setDate(2, Date.valueOf("2017-07-01"));
			opstmt.execute();
			oracle_rs = opstmt.executeQuery("select * from SetDate");
			while (oracle_rs.next()) {
				date_oracle_col = oracle_rs.getDate("col2");
			}
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Rest JDBC driver
		try {

			rpstmt = conn.prepareStatement(sql_insert);
			rpstmt.setInt(1, 123);
			rpstmt.setDate(2, Date.valueOf("2017-07-01"));
			rpstmt.execute();

			rest_rs = rpstmt.executeQuery("select * from SetDate");
			while (rest_rs.next()) {
				date_rest_col = rest_rs.getDate("col2");
			}
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			Util.doSQL(conn, sql_drop);
			close(opstmt, rpstmt);
			close(oracle_rs, rest_rs);
		}
		// assertEquals("2017-07-01", date_rcol.toString());
		assertEquals(date_oracle_col.toString(), date_rest_col.toString());
	}

	/*
	 * Test set parameter using setTime method.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetTime() {
		Connection conn = null, oracle_conn = null;
		PreparedStatement pstmt = null, opstmt = null;
		ResultSet oracle_rs = null, rest_rs = null;
		Time time_oracle_col = null, time_rest_col = null;
		String sql_createtable = "create table SetTime (col1 number, col2 date)";
		String sql_insert = "insert into SetTime values (?,?)";
		String sql_drop = "drop table SetTime purge";

		// Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_createtable);
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(sql_insert);
			opstmt.setInt(1, 123);
			opstmt.setTime(2, Time.valueOf("10:10:10"));
			opstmt.execute();
			oracle_rs = opstmt.executeQuery("select * from SetTime");
			while (oracle_rs.next()) {
				time_oracle_col = oracle_rs.getTime("col2");
			}
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Rest JDBC driver
		try {
			pstmt = conn.prepareStatement(sql_insert);
			pstmt.setInt(1, 123);
			pstmt.setTime(2, Time.valueOf("10:10:10"));
			pstmt.execute();
			rest_rs = pstmt.executeQuery("select * from SetTime");
			while (rest_rs.next()) {
				time_rest_col = rest_rs.getTime("col2");
			}
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			Util.doSQL(conn, sql_drop);
			close(opstmt, pstmt);
			close(oracle_rs, rest_rs);
		}
		// assertEquals("10:10:10", time_rcol.toString());
		assertEquals(time_oracle_col.toString(), time_rest_col.toString());

	}

	/*
	 * Test to set parameter using setBigdecimal.
	 * Bigdecimal value from oracle thin driver is 1E-9 but that of REST JDBC driver is
	 * 1.0E-9.
	 * Not considered failure so assert statements are changed to reflect the .0
	 */
	@Category(PassTest.class)
	@Test
	public void testSetBigDecimal() {
		Connection conn = null, oracle_conn = null;
		PreparedStatement pstmt = null, pstmt1 = null, opstmt = null;
		ResultSet oracle_rs = null, rest_rs = null;
		double[] db_numbers = new double[] { 9, 9999, 99999, 999999, 9999999,
				99999999, 999999999, 99.9, 9.99, 0.999, 0.0999, 0.00999,
				0.999999, 0.9999999, 0.99999999, 0.000000001, 0.00000000011,
				-999999999, -0.0000000011 };
		BigDecimal[] expected = new BigDecimal[19];
		BigDecimal[] oracle_actual = new BigDecimal[19];
		BigDecimal[] rest_actual = new BigDecimal[19];
		BigDecimal bd_rcol = null;
		String sql_createtable = "create table SetBigDecimal(c number(18,9))";
		String sql_createtable_1 = "create table SetBigDecimal_1(c number)";
		String sql_insert = "insert into SetBigDecimal values (?)";
		String sql_insert_1 = "insert into SetBigDecimal_1 values (?)";
		String sql_drop = "drop table SetBigDecimal purge";

		// Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_drop);
			Util.doSQL(conn, sql_createtable);
			Util.doSQL(conn, sql_createtable_1);

			// Insert into SetBigDecimal
			for (int i = 0; i < db_numbers.length; i++) {
				BigDecimal qa = new BigDecimal(Double.toString(db_numbers[i]));
				expected[i] = qa;
				pstmt = conn.prepareStatement(sql_insert);
				pstmt.setBigDecimal(1, qa);
				pstmt.execute();
				pstmt.close();
			}

			// Insert into SetBigDecimal_1
			for (int i = 0; i < db_numbers.length; i++) {
				BigDecimal qa = new BigDecimal(Double.toString(db_numbers[i]));
				expected[i] = qa;
				pstmt1 = conn.prepareStatement(sql_insert_1);
				pstmt1.setBigDecimal(1, qa);
				pstmt1.execute();
				pstmt1.close();
			}

		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn
					.prepareStatement("select * from SetBigDecimal order by c");
			oracle_rs = opstmt.executeQuery();
			int j = 0;
			while (oracle_rs.next()) {
				bd_rcol = oracle_rs.getBigDecimal("c");
				oracle_actual[j] = bd_rcol;
				j++;
			}

		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Rest JDBC driver
		try {
			pstmt = conn.prepareStatement("select * from SetBigDecimal order by c");
			rest_rs = pstmt.executeQuery();
			int j = 0;
			while (rest_rs.next()) {
				bd_rcol = rest_rs.getBigDecimal("c");
				rest_actual[j] = bd_rcol;
				j++;
			}
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(conn, sql_drop);
				close(opstmt, pstmt);
				close(oracle_rs, rest_rs);
				if (!pstmt1.isClosed()) {
					pstmt1.close();
				}

			} catch (SQLException | NullPointerException e) {
				fail(e.getMessage());
			}
		}
		assertEquals(oracle_actual[15].toString(), rest_actual[15].toString());
		assertEquals(oracle_actual[18].toString(), rest_actual[18].toString());
	}

	/*
	 * Test set parameter using setLong method.
	 * Long value retrieved should be '-9223372036854775807'
	 */

	@Category(PassTest.class)
	@Test
	public void testSetLong() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		long a = -9223372036854775807L, rcol = -1;

		String sql_createtable = "create table SetLong (col1 long)";
		String sql_insert = "insert into SetLong values (?)";
		String sql_drop = "drop table SetLong purge";

		try {
			conn = getRestConnection();

			Util.doSQL(conn, sql_createtable);
			pstmt = conn.prepareStatement(sql_insert);
			pstmt.setLong(1, a);
			pstmt.execute();
			rs = pstmt.executeQuery("select * from SetLong");
			rs.next();
			rcol = rs.getLong("col1");
			rs.close();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(conn, sql_drop);
				if (!pstmt.isClosed()) {
					pstmt.close();
				}
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(a, rcol);
	}

	/*
	 * Testcase to test getResultset method.
	 * REST JDBC driver throws NPE whereas it is fine with oracle thin driver.
	 */

	@Category(UnsupportedTest.class)
	@Test
	public void testgetResultSet() {
		Connection conn = null, oracle_conn = null, rest_conn = null;
		PreparedStatement pstmt = null, rpstmt = null, opstmt = null;
		ResultSet oracle_rs = null, rest_rs = null;
		String sql = "CREATE TABLE testPStmt_getResultSet "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPStmt_getResultSet values (?,?,?)";
		String sql_select = "select count(*) as rowcount from testPStmt_getResultSet";
		String sql_drop = "drop table testPStmt_getResultSet purge";
		int oracle_count = 0, rest_count = 0;

		// Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_drop);
			Util.doSQL(conn, sql);
			pstmt = conn.prepareStatement(sql_insert);

			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();

			pstmt.setString(1, "Smith");
			pstmt.setString(2, "Anderson");
			pstmt.setLong(3, 101);
			pstmt.executeUpdate();
			pstmt.close();

		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(sql_select);
			opstmt.execute();
			oracle_rs = opstmt.getResultSet();
			oracle_rs.next();
			oracle_count = oracle_rs.getInt("rowcount");
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Rest JDBC driver
		try {
			rest_conn = getRestConnection();
			rpstmt = rest_conn.prepareStatement(sql_select);
			rpstmt.execute();
			rest_rs = rpstmt.getResultSet();
			rest_rs.next();
			rest_count = rest_rs.getInt("rowcount");
		} catch (SQLException | NullPointerException | ClassNotFoundException e) {
			fail(e.getMessage());
		} finally {
			Util.doSQL(conn, sql_drop);
			// Uncomment below line when the NPE is solved.
			// close(oracle_rs, rest_rs);
			close(opstmt, rpstmt);
		}

		assertEquals(oracle_count, rest_count);
	}

	/*
	 * Testcase to test getConnection method.
	 */

	@Category(PassTest.class)
	@Test
	public void testgetConnection_1() {
		Connection conn = null, conn1 = null;
		PreparedStatement pstmt = null;
		String sql = "CREATE TABLE testPStmt_GC "
				+ "(firstname VARCHAR2(20) NOT NULL)";
		String sql_drop = "drop table testPStmt_GC purge";
		boolean isconn_present = false;
		try {
			conn = getRestConnection();
			pstmt = conn.prepareStatement(sql);
			conn1 = pstmt.getConnection();
			if (!conn1.isClosed()) {
				isconn_present = true;
			}
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(conn, sql_drop);
				pstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertTrue("Getconnection from preparestatement is fine",
				isconn_present);
	}

	/*
	 * Test to call getConnection on closed prepared statement.
	 * SQL Exception error 17009 is thrown by REST JDBC driver when getConnection method
	 * is called on closed prepared statement..
	 */

	@Category(PassTest.class)
	@Test
	public void testgetConnection_Afterstmtclose() {
		Connection oracle_conn = null, rest_conn = null;
		PreparedStatement opstmt = null, rpstmt = null;
		String sql = "CREATE TABLE testPStmt_GC_stmtclose "
				+ "(firstname VARCHAR2(20) NOT NULL)";
		String sql_drop = "drop table testPStmt_GC_stmtclose purge";
		SQLException oracle_exception = null, rest_exception = null;
		int oracle_error_code = -1, rest_error_code = -1;

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(sql);
			opstmt.close();
			opstmt.getConnection();
		} catch (SQLException e) {
			oracle_exception = e;
			oracle_error_code = e.getErrorCode();
		} catch (ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run JDBC driver
		try {
			rest_conn = getRestConnection();
			rpstmt = rest_conn.prepareStatement(sql);
			rpstmt.close();
			rpstmt.getConnection();
		} catch (SQLException e) {
			rest_exception = e;
			rest_error_code = e.getErrorCode();
		} catch (ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		finally {
			Util.doSQL(rest_conn, sql_drop);
			close(opstmt, rpstmt);
		}
		assertEquals(oracle_error_code, rest_error_code);
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());

	}

	/*
	 * Testcase to test getFetchDirection method.
	 */

	@Category(PassTest.class)
	@Test
	public void testgetFetchDirection_1() {
		Connection conn = null;
		PreparedStatement pstmt = null, pstmt1 = null;
		String sql = "CREATE TABLE testPStmt_GFD "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPStmt_GFD values (?,?,?)";
		String sql_select = "select count(*) as rowcount from testPStmt_GFD";
		String sql_drop = "drop table testPStmt_GFD purge";
		int get_fetch_direction = 0;

		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql);
			pstmt = conn.prepareStatement(sql_insert);
			pstmt1 = conn.prepareStatement(sql_select);

			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();

			pstmt1.execute();
			get_fetch_direction = pstmt1.getFetchDirection();

		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(conn, sql_drop);
				pstmt.close();
				pstmt1.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(1000, get_fetch_direction);

	}

	/* Test to call getFetchDirection on closed prepared statement.
	 * SQL Exception error 17009 is expected
	 */

	@Category(PassTest.class)
	@Test
	public void testgetFetchDirection_Afterstmtclose() {
		Connection conn = null, oracle_conn = null;
		PreparedStatement pstmt = null, opstmt = null;
		String sql = "CREATE TABLE testPStmt_GFD_stmtclose "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPStmt_GFD_stmtclose values (?,?,?)";
		String sql_select = "select count(*) as rowcount from testPStmt_GFD_stmtclose";
		String sql_drop = "drop table testPStmt_GFD_stmtclose purge";
		SQLException oracle_exception = null, rest_exception = null;
		int oracle_error_code = -1, rest_error_code = -1;

		// Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql);
			pstmt = conn.prepareStatement(sql_insert);

			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(sql_select);
			opstmt.execute();
			opstmt.close();
			opstmt.getFetchDirection();
		} catch (SQLException e) {
			oracle_exception = e;
			oracle_error_code = e.getErrorCode();
		} catch (ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run REST JDBC driver
		try {
			pstmt = conn.prepareStatement(sql_select);
			pstmt.execute();
			pstmt.close();
			pstmt.getFetchDirection();
		} catch (SQLException e) {
			rest_exception = e;
			rest_error_code = e.getErrorCode();
		} catch (NullPointerException e) {
			fail(e.getMessage());
		} finally {
			Util.doSQL(conn, sql_drop);
			close(opstmt, pstmt);
		}
		assertEquals(oracle_error_code, rest_error_code);
		assertEquals(oracle_exception.getErrorCode(),
				rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),
				rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());

	}

	/*
	 * Testcase to test getMaxrows method.
	 */

	@Category(PassTest.class)
	@Test
	public void testgetMaxrows() {
		Connection conn = null;
		PreparedStatement pstmt = null, pstmt1 = null;
		String sql = "CREATE TABLE testPStmt_getmaxrows "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPStmt_getmaxrows values (?,?,?)";
		String sql_select = "select count(*) as rowcount from testPStmt_getmaxrows";
		String sql_drop = "drop table testPStmt_getmaxrows purge";
		int getmaxrows = 0;
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql);
			pstmt = conn.prepareStatement(sql_insert);
			pstmt1 = conn.prepareStatement(sql_select);

			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();

			pstmt1.execute();
			getmaxrows = pstmt1.getMaxRows();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				Util.doSQL(conn, sql_drop);
				pstmt.close();
				pstmt1.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(0, getmaxrows);
	}

	/* Test to call getMaxrows method on closed prepared statement.
	 * SQL Exception error 17009 expected
	 */

	@Category(PassTest.class)
	@Test
	public void testgetMaxrows_Afterstmtclose() {
		Connection conn = null, oracle_conn = null;
		PreparedStatement pstmt = null, opstmt = null;
		String sql = "CREATE TABLE testPStmt_getmaxrows "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPStmt_getmaxrows values (?,?,?)";
		String sql_select = "select count(*) as rowcount from testPStmt_getmaxrows";
		String sql_drop = "drop table testPStmt_getmaxrows purge";
		SQLException oracle_exception = null, rest_exception = null;
		int oracle_error_code = -1, rest_error_code = -1, getmaxrows = 0;

		// Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql);
			pstmt = conn.prepareStatement(sql_insert);

			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver

		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(sql_select);
			opstmt.execute();
			opstmt.close();
			getmaxrows = opstmt.getMaxRows();
		} catch (SQLException e) {
			oracle_exception = e;
			oracle_error_code = e.getErrorCode();
		} catch (ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Rest JDBC driver

		try {
			pstmt = conn.prepareStatement(sql_select);
			pstmt.execute();
			pstmt.close();
			getmaxrows = pstmt.getMaxRows();
		} catch (SQLException e) {
			rest_exception = e;
			rest_error_code = e.getErrorCode();
		} catch (NullPointerException e) {
			fail(e.getMessage());
		}

		finally {
			Util.doSQL(conn, sql_drop);
			close(opstmt, pstmt);
		}
		assertEquals(oracle_error_code, rest_error_code);
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());

	}

	/* Testcase to test setMaxrows method.
	 */

	@Category(PassTest.class)
	@Test
	public void testsetMaxrows() {
		Connection conn = null, oracle_conn = null;
		PreparedStatement pstmt = null, opstmt = null;
		String sql = "CREATE TABLE testPStmt_setmaxrows "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPStmt_setmaxrows values (?,?,?)";
		String sql_select = "select count(*) as rowcount from testPStmt_setmaxrows";
		String sql_drop = "drop table testPStmt_setmaxrows purge";
		int oracle_getmaxrows = 0, rest_getmaxrows = 0;

		// Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql);
			pstmt = conn.prepareStatement(sql_insert);

			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();

		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(sql_select);
			opstmt.setMaxRows(3);
			opstmt.execute();
			oracle_getmaxrows = opstmt.getMaxRows();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run REST JDBC driver
		try {
			pstmt = conn.prepareStatement(sql_select);
			pstmt.setMaxRows(3);
			pstmt.execute();
			rest_getmaxrows = pstmt.getMaxRows();
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		}

		finally {
			Util.doSQL(conn, sql_drop);
			close(opstmt, pstmt);
		}
		assertEquals(oracle_getmaxrows, rest_getmaxrows);
	}

	/* Test to setMaxrows method with negative value.
	 * REST JDBC throws NPE when negative value set in setmaxrows method whereas
	 * the test throws exception 17068 using oracle thin driver.
	 */

	@Category(PassTest.class)
	@Test
	public void testsetMaxrows_negValue() {
		Connection conn = null, oracle_conn;
		PreparedStatement pstmt = null, opstmt = null;
		String sql = "CREATE TABLE testPStmt_setmaxrows "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPStmt_setmaxrows values (?,?,?)";
		String sql_select = "select count(*) as rowcount from testPStmt_setmaxrows";
		String sql_drop = "drop table testPStmt_setmaxrows purge";
		SQLException oracle_exception = null, rest_exception = null;
		int oracle_error_code = -1, rest_error_code = -1;

		// Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql);
			pstmt = conn.prepareStatement(sql_insert);

			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(sql_select);
			opstmt.setMaxRows(-1);
		} catch (SQLException e) {
			oracle_exception = e;
			oracle_error_code = e.getErrorCode();
		} catch (ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run REST JDBC driver

		try {
			pstmt = conn.prepareStatement(sql_select);
			pstmt.setMaxRows(-1);
		} catch (SQLException e) {
			rest_exception = e;
			rest_error_code = e.getErrorCode();
		} catch (NullPointerException e) {
			fail(e.getMessage());
		} finally {
			Util.doSQL(conn, sql_drop);
			close(opstmt, pstmt);
		}
		assertEquals(oracle_error_code, rest_error_code);
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());

	}

	/*
	 * NOOP TESTS for RestJdbcNotImplementedException
	 */

	private PreparedStatement noopSetup() throws SQLException,
			ClassNotFoundException {
		Connection conn = getRestConnection();
		return conn.prepareStatement("select 'BARRY' from dual");
	}

	/**
	 * Noop test for <code> int[] executeBatch() </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testEexecuteBatch() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.executeBatch();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP test for
	 * <code> int executeUpdate(String sql, int autoGeneratedKeys)</code>
	 */
	@Category(PassTest.class)
	@Test
	public void testExecuteUpdate() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.executeUpdate("select 1 from dual", 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code> int executeUpdate(String sql, int[] columnIndexes)</code>
	 */
	@Category(PassTest.class)
	@Test
	public void executeUpdate() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.executeUpdate("select 1 from dual", new int[] { 1, 2 });
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>int executeUpdate(String sql, String[] columnNames)</code>
	 */
	@Category(PassTest.class)
	@Test
	public void testExecuteUpdate2() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.executeUpdate("select 1 from dual", new int[] { 1, 2 });
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code> boolean execute(String sql, int autoGeneratedKeys)</code>
	 */
	@Category(PassTest.class)
	@Test
	public void testExecute2() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.executeUpdate("select 1 from dual", 2);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>boolean execute(String sql, int[] columnIndexes) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testExecute3() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.executeUpdate("select 1 from dual", 2);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>boolean execute(String sql, String[] columnNames) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testExecute4() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.execute("select 1 from dual", new String[] { "DUMMY" });
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>int getMaxFieldSize </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testgetMaxFieldSize() throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getMaxFieldSize();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code> void setMaxFieldSize(int max) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetMaxFieldSize() throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setMaxFieldSize(5);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setEscapeProcessing(boolean enable)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetEscapeProcessing() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setEscapeProcessing(Boolean.TRUE);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>int getQueryTimeout()  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testGetQueryTimeout() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getQueryTimeout();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setQueryTimeout(int nos)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetQueryTimeout() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setQueryTimeout(5);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code> void cancel()  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void cancel() throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.cancel();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code> SQLWarning getWarnings() </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testGetWarnings() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getWarnings();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code> void clearWarnings()   </code>
	 */
	@Category(PassTest.class)
	@Test
	public void clearWarnings() throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.clearWarnings();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>int getUpdateCount() </code>
	 */
	@Category(PassTest.class)
	@Test
	public void getUpdateCount() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getUpdateCount();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code> boolean getMoreResults()  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testGetMoreResults() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getMoreResults();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setFetchDirection(int direction)</code>
	 */
	@Category(PassTest.class)
	@Test
	public void setFetchDirection(int direction) throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setFetchDirection(2);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>int getResultSetConcurrency() </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testGetResultSetConcurrency() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getResultSetConcurrency();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>int getResultSetType()</code>
	 */
	@Category(PassTest.class)
	@Test
	public void testGetResultSetType() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getResultSetType();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void addBatch(String sql) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testAddBatch(String sql) {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.addBatch("SELECT 'BARRY' FROM DUAL");
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);

	}

	/**
	 * NOOP Test for <code>void clearBatch() </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testClearBatch() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.clearBatch();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);

	}

	/**
	 * NOOP Test for <code>boolean getMoreResults(int current)</code>
	 */
	@Category(PassTest.class)
	@Test
	public void testGetMoreResults(int current) {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getMoreResults(5);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>ResultSet getGeneratedKeys() </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testGetGeneratedKeys() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getGeneratedKeys();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code> int getResultSetHoldability()  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testGetResultSetHoldability() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getResultSetHoldability();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setPoolable(boolean poolable) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetPoolable() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setPoolable(Boolean.TRUE);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code> boolean isPoolable() </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testIsPoolable() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.isPoolable();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code> void closeOnCompletion() </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testCloseOnCompletion() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.closeOnCompletion();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>boolean isCloseOnCompletion() </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testIsCloseOnCompletion() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.closeOnCompletion();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code><T> T unwrap(Class<T> iface) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testUnwrap() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.unwrap(this.getClass());
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>boolean isWrapperFor(Class<?> iface) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testIsWrapperFor() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.isWrapperFor(this.getClass());
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setBytes(int parameterIndex, byte[] x) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setBytes(1, new byte[] {});
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setUnicodeStream(int parameterIndex, InputStream x, int length) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void setUnicodeStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setUnicodeStream(1, null, 0);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setBinaryStream(int parameterIndex, InputStream x, int length)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void setBinaryStream(int parameterIndex, InputStream x, int length) {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setBinaryStream(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void clearParameters()</code>
	 */
	@Category(PassTest.class)
	@Test
	public void clearParameters() throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.clearParameters();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setObject(int parameterIndex, Object x, int targetSqlType)</code>
	 */
	@Category(PassTest.class)
	@Test
	public void setObject(int parameterIndex, Object x, int targetSqlType) {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setObject(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void addBatch()  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testAddBatch() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.addBatch();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setCharacterStream(int parameterIndex, Reader reader, int length)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetCharacterStream3() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setCharacterStream(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void addBatch()  </code> xxxxxxxx
	 */
	@Category(PassTest.class)
	@Test
	public void setRef(int parameterIndex, Ref x) throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setCharacterStream(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setEscapeProcessing(boolean enable)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setCharacterStream(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setEscapeProcessing(boolean enable)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void setArray(int parameterIndex, Array x) throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setCharacterStream(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>ResultSetMetaData getMetaData() </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testGetMetaData() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.getMetaData();
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	@Category(PassTest.class)
	@Test
	public void setNull(int parameterIndex, int sqlType, String typeName)
			throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setCharacterStream(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setEscapeProcessing(boolean enable)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void setURL(int parameterIndex, URL x) throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setCharacterStream(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setEscapeProcessing(boolean enable)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setCharacterStream(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setEscapeProcessing(boolean enable)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void setNString(int parameterIndex, String value)
			throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setCharacterStream(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setNCharacterStream(int parameterIndex, Reader value, long length) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetNCharacterStream() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setNCharacterStream(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setNClob(int parameterIndex, NClob value) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetNClob3() {
		// setNClob(int parameterIndex, NClob value) throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setNClob(1, (NClob) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setNClob(int parameterIndex, NClob value) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetNClob2() {
		// setNClob(int parameterIndex, NClob value) throws SQLException {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setNClob(1, (Reader) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setNClob(int parameterIndex, NClob value) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetNClob4() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setNClob(1, (Reader) null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}


	/**
	 * NOOP Test for <code>setClob(int, Reader, int)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetClob2() {
		// setClob(int parameterIndex, Reader reader)
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setClob(1, (Reader) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}


	/**
	 * NOOP Test for
	 * <code>void testSetBlob(int parameterIndex, InputStream inputStream, long length) </code>
	 */
	@Category(UnsupportedTest.class)
	@Test
	public void testSetBlob2() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setBlob(1, (InputStream) null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void testSetBlob(int parameterIndex, InputStream inputStream) </code>
	 */
	@Category(UnsupportedTest.class)
	@Test
	public void testSetBlob3() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setBlob(1, (InputStream) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void testSetBlob(int parameterIndex, Blob blob) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetBlob4() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setBlob(1, (Blob) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for <code>void setEscapeProcessing(boolean enable)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetNClob(int parameterIndex, Reader reader, long length) {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setBlob(1, (Blob) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
		// testSetNClob(int parameterIndex, Reader reader, long length)
	}

	/**
	 * NOOP Test for <code>setSQLXML(int parameterIndex, SQLXML xmlObject)
 </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetSQLXML(int parameterIndex, SQLXML xmlObject) {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setSQLXML(1, (SQLXML) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setObject(int parameterIndex, Object x, int targetSqlType, </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetObject() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setObject(1, null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setBinaryStream(int parameterIndex, InputStream x, long length) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetBinaryStream2() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setBinaryStream(1, (InputStream) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void ssetCharacterStream(int parameterIndex, Reader reader, long length)</code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetCharacterStream() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setCharacterStream(1, (Reader) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
		// setCharacterStream(int parameterIndex, Reader reader, long length)
	}

	/**
	 * NOOP Test for
	 * <code>void ssetCharacterStream(int parameterIndex, Reader reader, long length)</code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetCharacterStream4() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setCharacterStream(1, (Reader) null, 1);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setBinaryStream(int parameterIndex, InputStream x)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetBinaryStream() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setBinaryStream(1, (InputStream) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setNCharacterStream(int parameterIndex, Reader value) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetNCharacterStream2() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setNCharacterStream(1, (Reader) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void void setClob(int parameterIndex, Reader reader)  </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetClob4() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setClob(1, (Reader) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setNCharacterStream(int parameterIndex, Reader value)</code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetNCharacterStream3() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setNCharacterStream(1, (Reader) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/**
	 * NOOP Test for
	 * <code>void setNClob(int parameterIndex, Reader reader) </code>
	 */
	@Category(PassTest.class)
	@Test
	public void testSetNClob() {
		try {
			PreparedStatement pstmt = noopSetup();
			pstmt.setNClob(1, (Reader) null);
		} catch (RestJdbcNotImplementedException rjnie) {
			assert (rjnie instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
	}

	/*
	 * Testcase to test setClob method.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetClob_1() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		ResultSet oraclers = null, restrs = null;
		int oracle_res_int = 0, rest_res_int = 0;
		String oracle_res_cl_str = "", rest_res_cl_str = "";
		String rest_sql_insert = "insert into Table_RestClob values (?,?)";
		String oracle_sql_insert = "insert into Table_OracleClob values (?,?)";

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table Table_OracleClob;");
			Util.doSQL(restConn, "drop table Table_RestClob;");
			Util.doSQL(restConn,
					"create table Table_OracleClob (c1 NUMBER, c2 clob);");
			Util.doSQL(restConn,
					"create table Table_RestClob (c1 NUMBER, c2 clob);");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			Clob cl = oracleConn.createClob();
			cl.setString(1, "abcdefg");
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setInt(1, 1);
			oraclepstmt.setClob(2, cl);
			oraclepstmt.executeUpdate();
			oraclers = oraclepstmt.executeQuery("select c1,c2 from Table_OracleClob");
			Clob oracle_res_cl = null;
			int oracle_res_cl_len = 0;
			if (oraclers.next()) {
				oracle_res_int = oraclers.getInt("c1");
				oracle_res_cl_len = (int) oraclers.getClob("c2").length();
				oracle_res_cl = oraclers.getClob("c2");
				oracle_res_cl_str = oracle_res_cl.getSubString(1,(int) oracle_res_cl.length());
			}
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC deriver
		try {
			Clob cl = restConn.createClob();
			cl.setString(1, "abcdefg");
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setClob(2, cl);
			restpstmt.executeUpdate();
			restrs = restpstmt.executeQuery("select c1,c2 from Table_RestClob");
			Clob rest_res_cl = null;
			int rest_res_cl_len = 0;
			if (restrs.next()) {
				rest_res_int = restrs.getInt("c1");
				rest_res_cl = restrs.getClob("c2");
				rest_res_cl_len = (int) restrs.getClob("c2").length();
				rest_res_cl_str = rest_res_cl.getSubString(1,(int) rest_res_cl.length());
			}
		} catch (SQLException e) {
			fail(e.getMessage());
		}

		finally {
			try {
				close(oraclers, restrs);
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}

		assertEquals(oracle_res_int, rest_res_int);
		assertEquals(oracle_res_cl_str, rest_res_cl_str);
	}

	/*
	 * Test setURL method. Missing IN or OUT parameter at index :: 1
	 */
	@Category(PassTest.class)
	@Test
	public void testSetUrl_1() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		int oracle_res_count = -1, rest_res_count = -1;
		java.net.URL url = null;
		try {
			url = new java.net.URL("http://www.oracle.com");
		} catch (MalformedURLException e1) {
			fail(e1.getMessage());
		}
		ResultSet oraclers = null, restrs = null;
		String oracle_sql_select = "select count(*) as rowcount from test_oracleurl";
		String rest_sql_select = "select count(*) as rowcount from test_resturl";

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracleurl;");
			Util.doSQL(restConn, "drop table test_resturl;");
			Util.doSQL(restConn, "create table test_oracleurl (c1 NUMBER, c2 VARCHAR2(50))");
			Util.doSQL(restConn, "create table test_resturl (c1 NUMBER, c2 VARCHAR2(50))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement("insert into test_oracleurl values(?, ?)");
			oraclepstmt.setInt(1, 1);
			oraclepstmt.setURL(2, url);
			oraclepstmt.execute();
			PreparedStatement oraclepstmt1 = oracleConn.prepareStatement(oracle_sql_select);
			oraclers = oraclepstmt1.executeQuery();
			oraclers.next();
			oracle_res_count = oraclers.getInt("rowcount");
			oraclers.close();
			oraclepstmt1.close();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver
		try {

			restpstmt = restConn.prepareStatement("insert into test_resturl values(?, ?)");
			restpstmt.setInt(1, 1);
			restpstmt.setURL(2, url);
			// oraclepstmt.setString(999, sb.toString());
			restpstmt.execute();
			PreparedStatement restpstmt1 = restConn.prepareStatement(rest_sql_select);
			restrs = restpstmt1.executeQuery();
			restrs.next();
			rest_res_count = restrs.getInt("rowcount");
			restrs.close();
			restpstmt1.close();
		} catch (SQLException e) {
			fail(e.getMessage());
		}

		finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_res_count, rest_res_count);
	}

	/*
	 * Test addBatch and executeBatch method.
	 */
	@Category(PassTest.class)
	@Test
	public void testAddBatch_1() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		int oracle_res_count = -1, rest_res_count = -1;
		String oracle_sql_insert = "INSERT INTO test_oracle_addbatch(c1,c2) values (?,?)";
		String rest_sql_insert = "INSERT INTO test_rest_addbatch(c1,c2) values (?,?)";
		String oracle_sql_select = "select count(*) as rowcount from test_oracle_addbatch";
		String rest_sql_select = "select count(*) as rowcount from test_rest_addbatch";
		ResultSet oraclers = null, restrs = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracle_addbatch;");
			Util.doSQL(restConn, "drop table test_rest_addbatch;");
			Util.doSQL(restConn, "create table test_oracle_addbatch(c1 number, c2 varchar2(500))");
			Util.doSQL(restConn,
					"create table test_rest_addbatch(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setInt(1, 1);
			oraclepstmt.setString(2, "Value 1");
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 2);
			oraclepstmt.setString(2, "Value 2");
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 3);
			oraclepstmt.setString(2, "Value 3");
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 4);
			oraclepstmt.setString(2, "Value 4");
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 5);
			oraclepstmt.setString(2, "Value 5");
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 6);
			oraclepstmt.setString(2, "Value 6");
			oraclepstmt.addBatch();

			oraclepstmt.executeBatch();
			PreparedStatement oraclepstmt1 = oracleConn.prepareStatement(oracle_sql_select);
			oraclers = oraclepstmt1.executeQuery();
			oraclers.next();
			oracle_res_count = oraclers.getInt("rowcount");
			oraclers.close();
			oraclepstmt1.close();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver
		try {

			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setString(2, "Value 1");
			restpstmt.addBatch();

			restpstmt.setInt(1, 2);
			restpstmt.setString(2, "Value 2");
			restpstmt.addBatch();

			restpstmt.setInt(1, 3);
			restpstmt.setString(2, "Value 3");
			restpstmt.addBatch();

			restpstmt.setInt(1, 4);
			restpstmt.setString(2, "Value 4");
			restpstmt.addBatch();

			restpstmt.setInt(1, 5);
			restpstmt.setString(2, "Value 5");
			restpstmt.addBatch();

			restpstmt.setInt(1, 6);
			restpstmt.setString(2, "Value 6");
			restpstmt.addBatch();

			restpstmt.executeBatch();
			PreparedStatement restpstmt1 = restConn.prepareStatement(rest_sql_select);
			restrs = restpstmt1.executeQuery();
			restrs.next();
			rest_res_count = restrs.getInt("rowcount");
			restrs.close();
			restpstmt1.close();
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_res_count, rest_res_count);
	}

	/*
	 * Test to calling executeBatch method without addBatch.
	 */

	@Category(PassTest.class)
	@Test
	public void testAddBatch_2() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		int oracle_res_count = -1, rest_res_count = -1;
		String oracle_sql_insert = "INSERT INTO test_oracle_addbatch(c1,c2) values (?,?)";
		String rest_sql_insert = "INSERT INTO test_rest_addbatch(c1,c2) values (?,?)";

		String oracle_sql_select = "select count(*) as rowcount from test_oracle_addbatch";
		String rest_sql_select = "select count(*) as rowcount from test_rest_addbatch";
		ResultSet oraclers = null, restrs = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracle_addbatch;");
			Util.doSQL(restConn, "drop table test_rest_addbatch;");
			Util.doSQL(restConn, "create table test_oracle_addbatch(c1 number, c2 varchar2(500))");
			Util.doSQL(restConn, "create table test_rest_addbatch(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setInt(1, 1);
			oraclepstmt.setString(2, "Value 1");
			// oraclepstmt.addBatch();

			oraclepstmt.executeBatch();
			PreparedStatement oraclepstmt1 = oracleConn.prepareStatement(oracle_sql_select);
			oraclers = oraclepstmt1.executeQuery();
			oraclers.next();
			oracle_res_count = oraclers.getInt("rowcount");
			oraclers.close();
			oraclepstmt1.close();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver
		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setString(2, "Value 1"); //
			// restpstmt.addBatch();

			restpstmt.executeBatch();
			PreparedStatement restpstmt1 = restConn.prepareStatement(rest_sql_select);
			restrs = restpstmt1.executeQuery();
			restrs.next();
			rest_res_count = restrs.getInt("rowcount");
			restrs.close();
			restpstmt1.close();
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_res_count, rest_res_count);
	}

	/*
	 * Test to call addBatch and executeBatch method without setting the values.
	 * Expected error code <17041> and actual error code is <17041>
	 */

	@Category(PassTest.class)
	@Test
	public void testAddBatch_3() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		SQLException oracle_exception = null, rest_exception = null;
		int oracle_res_count = -1, rest_res_count = -1;
		String oracle_sql_insert = "INSERT INTO test_oracle_addbatch(c1,c2) values (?,?)";
		String rest_sql_insert = "INSERT INTO test_rest_addbatch(c1,c2) values (?,?)";

		String oracle_sql_select = "select count(*) as rowcount from test_oracle_addbatch";
		String rest_sql_select = "select count(*) as rowcount from test_rest_addbatch";
		ResultSet oraclers = null, restrs = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracle_addbatch;");
			Util.doSQL(restConn, "drop table test_rest_addbatch;");
			Util.doSQL(restConn, "create table test_oracle_addbatch(c1 number, c2 varchar2(500))");
			Util.doSQL(restConn, "create table test_rest_addbatch(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);

			oraclepstmt.addBatch();
			oraclepstmt.addBatch();
			oraclepstmt.addBatch();
			oraclepstmt.addBatch();

			oraclepstmt.executeBatch();
			PreparedStatement oraclepstmt1 = oracleConn.prepareStatement(oracle_sql_select);

			oraclers = oraclepstmt1.executeQuery();
			oraclers.next();
			oracle_res_count = oraclers.getInt("rowcount");
			oraclers.close();
			oraclepstmt1.close();
		} catch (SQLException e) {
			oracle_exception = e;
		} catch (ClassNotFoundException se) {
			fail(se.getMessage());
		}

		// Rest JDBC driver
		try {

			restpstmt = restConn.prepareStatement(rest_sql_insert);

			restpstmt.addBatch();
			oraclepstmt.addBatch();
			oraclepstmt.addBatch();
			oraclepstmt.addBatch();

			restpstmt.executeBatch();
			PreparedStatement restpstmt1 = restConn.prepareStatement(rest_sql_select);

			restrs = restpstmt1.executeQuery();
			restrs.next();
			rest_res_count = restrs.getInt("rowcount");
			restrs.close();
			restpstmt1.close();
		} catch (SQLException e) {
			rest_exception = e;
		} finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());
	}

	/*
	 * Test to call GetUpdateCount method after addBatch and executeBatch.
	 * Expected: <2> but actual: <-1>
	 */

	@Category(PassTest.class)
	@Test
	public void testGetUpdateCount_1() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		int oracle_res_count = -1, rest_res_count = -1;
		int oracle_upd_count = -1, rest_upd_count = -1;
		String oracle_sql_insert = "INSERT INTO test_oracle_addbatch(c1,c2) values (?,?)";
		String rest_sql_insert = "INSERT INTO test_rest_addbatch(c1,c2) values (?,?)";

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracle_addbatch;");
			Util.doSQL(restConn, "drop table test_rest_addbatch;");
			Util.doSQL(restConn,
					"create table test_oracle_addbatch(c1 number, c2 varchar2(500))");
			Util.doSQL(restConn,
					"create table test_rest_addbatch(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setInt(1, 1);
			oraclepstmt.setString(2, "Value 1");
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 2);
			oraclepstmt.setString(2, "Value 2");
			oraclepstmt.addBatch();

			oraclepstmt.executeBatch();
			oracle_upd_count = oraclepstmt.getUpdateCount();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver
		try {

			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setString(2, "Value 1");
			restpstmt.addBatch();

			restpstmt.setInt(1, 2);
			restpstmt.setString(2, "Value 2");
			restpstmt.addBatch();
			restpstmt.executeBatch();
			rest_upd_count = restpstmt.getUpdateCount();
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_upd_count, rest_upd_count);
	}

	/*
	 * Testcase to test getResultSetType method.
	 */
	@Category(PassTest.class)
	@Test
	public void testGetResultSetType_1() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		int oracle_rs_type = -1, rest_rs_type = -1;
		String oracle_sql_insert = "INSERT INTO test_oracle_addbatch(c1,c2) values (?,?)";
		String rest_sql_insert = "INSERT INTO test_rest_addbatch(c1,c2) values (?,?)"; // Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracle_addbatch;");
			Util.doSQL(restConn, "drop table test_rest_addbatch;");
			Util.doSQL(restConn, "create table test_oracle_addbatch(c1 number, c2 varchar2(500))");
			Util.doSQL(restConn, "create table test_rest_addbatch(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setInt(1, 1);
			oraclepstmt.setString(2, "Value 1");
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 2);
			oraclepstmt.setString(2, "Value 2");
			oraclepstmt.addBatch();

			oraclepstmt.executeBatch();
			oracle_rs_type = oraclepstmt.getResultSetType();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver
		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setString(2, "Value 1");
			restpstmt.addBatch();

			restpstmt.setInt(1, 2);
			restpstmt.setString(2, "Value 2");
			restpstmt.addBatch();
			restpstmt.executeBatch();
			rest_rs_type = restpstmt.getResultSetType();
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_rs_type, rest_rs_type);
	}

	/*
	 * Testcase to test closeonCompletion method with resultset closed.
	 */

	@Category(PassTest.class)
	@Test
	public void testcloseonCompletion_1() {
		Connection restConn = null;
		PreparedStatement restpstmt = null;
		String rest_sql_insert = "INSERT INTO test_rest_closeoncomp(c1,c2) values (?,?)";
		String rest_sql_select = "select count(*) as rowcount from test_rest_closeoncomp";
		ResultSet restrs = null;
		boolean rest_isclosed = false;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_rest_closeoncomp;");
			Util.doSQL(restConn, "create table test_rest_closeoncomp(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		//Run REST JDBC driver
		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setString(2, "Value 1");
			restpstmt.addBatch();

			restpstmt.setInt(1, 2);
			restpstmt.setString(2, "Value 2");
			restpstmt.addBatch();

			restpstmt.executeBatch();
			PreparedStatement restpstmt1 = restConn.prepareStatement(rest_sql_select);
			restrs = restpstmt1.executeQuery();
			restrs.close();
			restpstmt1.closeOnCompletion();
			if (restpstmt1.isClosed()) {
				rest_isclosed = true;
			} else {
				rest_isclosed = false;
			}
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(true, rest_isclosed);
	}

	/*
	 * Testcase to test closeonCompletion method when resultset is not closed.
	 */

	@Category(PassTest.class)
	@Test
	public void testcloseonCompletion_2() {
		Connection restConn = null;
		PreparedStatement restpstmt = null;
		String rest_sql_insert = "INSERT INTO test_rest_closeoncomp(c1,c2) values (?,?)";
		String rest_sql_select = "select count(*) as rowcount from test_rest_closeoncomp";
		ResultSet restrs = null;
		boolean rest_isclosed = false;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_rest_closeoncomp;");
			Util.doSQL(restConn, "create table test_rest_closeoncomp(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setString(2, "Value 1");
			restpstmt.addBatch();

			restpstmt.setInt(1, 2);
			restpstmt.setString(2, "Value 2");
			restpstmt.addBatch();

			restpstmt.executeBatch();
			PreparedStatement restpstmt1 = restConn.prepareStatement(rest_sql_select);
			restrs = restpstmt1.executeQuery();
			// restrs.close();
			restpstmt1.closeOnCompletion();
			if (restpstmt1.isClosed()) {
				rest_isclosed = true;
			} else {
				rest_isclosed = false;
			}
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(false, rest_isclosed);
	}

	/*
	 * Test to call closeonCompletion method on preparedstatement which is already closed.
	 * Expected : <Closed Statement>
	 */
	@Category(PassTest.class)
	@Test
	public void testcloseonCompletion_3() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		String rest_sql_insert = "INSERT INTO test_rest_closeoncomp(c1,c2) values (?,?)";
		String oracle_sql_insert = "INSERT INTO test_oracle_closeoncomp(c1,c2) values (?,?)";

		String rest_sql_select = "select count(*) as rowcount from test_rest_closeoncomp";
		String oracle_sql_select = "select count(*) as rowcount from test_oracle_closeoncomp";

		ResultSet oraclers = null, restrs = null;
		boolean rest_isclosed = false;
		SQLException oracle_exception = null, rest_exception = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_rest_closeoncomp;");
			Util.doSQL(restConn, "drop table test_oracle_closeoncomp;");
			Util.doSQL(restConn, "create table test_rest_closeoncomp(c1 number, c2 varchar2(500))");
			Util.doSQL(restConn, "create table test_oracle_closeoncomp(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setInt(1, 1);
			oraclepstmt.setString(2, "Value 1");
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 2);
			oraclepstmt.setString(2, "Value 2");
			oraclepstmt.addBatch();

			oraclepstmt.executeBatch();
			PreparedStatement oraclepstmt1 = oracleConn.prepareStatement(oracle_sql_select);
			oraclers = oraclepstmt1.executeQuery();
			oraclers.close();
			oraclepstmt1.close();
			oraclepstmt1.closeOnCompletion();
		} catch (ClassNotFoundException e) {
			fail(e.getMessage());
		} catch (SQLException e) {
			oracle_exception = e;
		}

		// Run REST JDBC driver

		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setString(2, "Value 1");
			restpstmt.addBatch();

			restpstmt.setInt(1, 2);
			restpstmt.setString(2, "Value 2");
			restpstmt.addBatch();

			restpstmt.executeBatch();
			PreparedStatement restpstmt1 = restConn.prepareStatement(rest_sql_select);
			restrs = restpstmt1.executeQuery();
			restrs.close();
			restpstmt1.close();
			restpstmt1.closeOnCompletion();
		} catch (SQLException e) {
			rest_exception = e;
		} finally {
			try {
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());
	}

	/*
	 * Testcase to call iscloseonCompletion method after preparedSatement closeonCompletion.
	 */
	@Category(PassTest.class)
	@Test
	public void testiscloseonCompletion_1() {
		Connection restConn = null;
		PreparedStatement restpstmt = null;
		String rest_sql_insert = "INSERT INTO test_rest_closeoncomp(c1,c2) values (?,?)";
		String rest_sql_select = "select count(*) as rowcount from test_rest_closeoncomp";
		ResultSet restrs = null;
		boolean rest_isclosed = false;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_rest_closeoncomp;");
			Util.doSQL(restConn, "create table test_rest_closeoncomp(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setString(2, "Value 1");
			restpstmt.addBatch();

			restpstmt.setInt(1, 2);
			restpstmt.setString(2, "Value 2");
			restpstmt.addBatch();

			restpstmt.executeBatch();
			PreparedStatement restpstmt1 = restConn.prepareStatement(rest_sql_select);
			restrs = restpstmt1.executeQuery();
			restrs.close();
			restpstmt1.closeOnCompletion();
			if (restpstmt1.isCloseOnCompletion()) {
				rest_isclosed = true;
			} else {
				rest_isclosed = false;
			}
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(true, rest_isclosed);
	}

	/*
	 * Testcase to call iscloseonComplete method when preparedSatement is still open.
	 */
	@Category(PassTest.class)
	@Test
	public void testiscloseonCompletion_2() {
		Connection restConn = null;
		PreparedStatement restpstmt = null;
		String rest_sql_insert = "INSERT INTO test_rest_closeoncomp(c1,c2) values (?,?)";
		String rest_sql_select = "select count(*) as rowcount from test_rest_closeoncomp";
		ResultSet restrs = null;
		boolean rest_isclosed = false;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_rest_closeoncomp;");
			Util.doSQL(restConn, "create table test_rest_closeoncomp(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setString(2, "Value 1");
			restpstmt.addBatch();

			restpstmt.setInt(1, 2);
			restpstmt.setString(2, "Value 2");
			restpstmt.addBatch();

			restpstmt.executeBatch();
			PreparedStatement restpstmt1 = restConn.prepareStatement(rest_sql_select);
			restrs = restpstmt1.executeQuery();
			restrs.close();
			// restpstmt1.closeOnCompletion();
			if (restpstmt1.isCloseOnCompletion()) {
				rest_isclosed = true;
			} else {
				rest_isclosed = false;
			}
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(false, rest_isclosed);
	}

	/*
	 * Test to call iscloseonComplete method when preparedSatement is closed.
	 * Expected : <Closed Statement>
	 */

	@Category(PassTest.class)
	@Test
	public void testiscloseonCompletion_3() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		String rest_sql_insert = "INSERT INTO test_rest_closeoncomp(c1,c2) values (?,?)";
		String rest_sql_select = "select count(*) as rowcount from test_rest_closeoncomp";
		ResultSet oraclers = null, restrs = null;
		boolean rest_isclosed = false;
		SQLException oracle_exception = null, rest_exception = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_rest_closeoncomp;");
			Util.doSQL(restConn, "create table test_rest_closeoncomp(c1 number, c2 varchar2(500))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(rest_sql_insert);
			oraclepstmt.setInt(1, 1);
			oraclepstmt.setString(2, "Value 1");
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 2);
			oraclepstmt.setString(2, "Value 2");
			oraclepstmt.addBatch();

			oraclepstmt.executeBatch();
			PreparedStatement oraclepstmt1 = oracleConn.prepareStatement(rest_sql_select);
			oraclers = oraclepstmt1.executeQuery();
			oraclers.close();
			oraclepstmt1.close();
			oraclepstmt1.isCloseOnCompletion();
		} catch (ClassNotFoundException e) {
			fail(e.getMessage());
		} catch (SQLException e) {
			oracle_exception = e;
		}

		// Run REST JDBC driver
		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 1);
			restpstmt.setString(2, "Value 1");
			restpstmt.addBatch();

			restpstmt.setInt(1, 2);
			restpstmt.setString(2, "Value 2");
			restpstmt.addBatch();

			restpstmt.executeBatch();
			PreparedStatement restpstmt1 = restConn.prepareStatement(rest_sql_select);
			restrs = restpstmt1.executeQuery();
			restrs.close();
			restpstmt1.close();
			if (restpstmt1.isCloseOnCompletion()) {
				rest_isclosed = true;
			} else {
				rest_isclosed = false;
			}
		} catch (SQLException e) {
			rest_exception = e;
		} finally {
			try {
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage(), rest_exception.getMessage());
	}

	/*
	 * Test content of int array returned by executeBatch method.
	 */

	@Category(PassTest.class)
	@Test
	public void testExecuteBatch_1() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		int oracle_res_count[] = null;
		int rest_res_count[] = null;
		String oracle_sql_insert = "INSERT INTO test_oracle_exebatch(c1) values (?)";
		String rest_sql_insert = "INSERT INTO test_rest_exebatch(c1) values (?)";
		ResultSet oraclers = null, restrs = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracle_exebatch;");
			Util.doSQL(restConn, "drop table test_rest_exebatch;");
			Util.doSQL(restConn, "create table test_oracle_exebatch(c1 number)");
			Util.doSQL(restConn, "create table test_rest_exebatch(c1 number)");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setInt(1, 100);
			oraclepstmt.addBatch();
			oraclepstmt.setInt(1, 101);
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 200);
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 300);
			oraclepstmt.addBatch();
			oraclepstmt.setInt(1, 305);
			oraclepstmt.addBatch();

			oraclepstmt.setInt(1, 400);
			oraclepstmt.addBatch();

			oracle_res_count = oraclepstmt.executeBatch();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver
		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 100);
			restpstmt.addBatch();
			restpstmt.setInt(1, 101);
			restpstmt.addBatch();

			restpstmt.setInt(1, 200);
			restpstmt.addBatch();

			restpstmt.setInt(1, 300);
			restpstmt.addBatch();
			restpstmt.setInt(1, 305);
			restpstmt.addBatch();

			restpstmt.setInt(1, 400);
			restpstmt.addBatch();

			rest_res_count = restpstmt.executeBatch();

		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		Assert.assertArrayEquals(oracle_res_count, rest_res_count);
		assertEquals(oracle_res_count.length, rest_res_count.length);
	}

	/*
	 * Test executeBatch with addBatch and setvalue(setInt) called only once. Value of
	 * int array (executeBatch) is different in oracle thin driver and REST JDBC driver
	 * Expected: <1> Actual: <0>
	 */

	@Category(PassTest.class)
	@Test
	public void testExecuteBatch_2() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		int oracle_res_count[] = null;
		int rest_res_count[] = null;
		String oracle_sql_insert = "INSERT INTO test_oracle_exebatch(c1) values (?)";
		String rest_sql_insert = "INSERT INTO test_rest_exebatch(c1) values (?)";

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracle_exebatch;");
			Util.doSQL(restConn, "drop table test_rest_exebatch;");
			Util.doSQL(restConn, "create table test_oracle_exebatch(c1 number)");
			Util.doSQL(restConn, "create table test_rest_exebatch(c1 number)");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setInt(1, 100);
			oraclepstmt.addBatch();
			oracle_res_count = oraclepstmt.executeBatch();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver
		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 100);
			restpstmt.addBatch();
			rest_res_count = restpstmt.executeBatch();
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		Assert.assertArrayEquals(oracle_res_count, rest_res_count);
		assertEquals(oracle_res_count.length, rest_res_count.length);
	}

	/*
	 * Test to call executeBatch and addBatch multiple times with values set only once.
	 * Expected array length as a result of executeBatch is <6> but actual is <0>
	 */

	@Category(FailTest.class)
	@Test
	public void testAddBatch_4() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		int oracle_res_count[] = new int[5];
		int rest_res_count[] = new int[5];
		String oracle_sql_insert = "INSERT INTO test_oracle_exebatch(c1) values (?)";
		String rest_sql_insert = "INSERT INTO test_rest_exebatch(c1) values (?)";
		ResultSet oraclers = null, restrs = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracle_exebatch;");
			Util.doSQL(restConn, "drop table test_rest_exebatch;");
			Util.doSQL(restConn, "create table test_oracle_exebatch(c1 number)");
			Util.doSQL(restConn, "create table test_rest_exebatch(c1 number)");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setInt(1, 100);
			oraclepstmt.addBatch();
			oraclepstmt.addBatch();
			oraclepstmt.addBatch();
			oraclepstmt.addBatch();
			oraclepstmt.addBatch();
			oraclepstmt.addBatch();
			oracle_res_count = oraclepstmt.executeBatch();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver
		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setInt(1, 100);
			restpstmt.addBatch();
			restpstmt.addBatch();
			restpstmt.addBatch();
			restpstmt.addBatch();
			restpstmt.addBatch();
			restpstmt.addBatch();
			rest_res_count = restpstmt.executeBatch();
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		Assert.assertArrayEquals(oracle_res_count, rest_res_count);
		assertEquals(oracle_res_count.length, rest_res_count.length);
	}

	/*
	 * Test by setting wrong value type and call addBatch.
	 */

	@Category(PassTest.class)
	@Test
	public void testAddBatch_5() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		int oracle_res_count[] = null;
		int rest_res_count[] = null;
		String oracle_sql_insert = "INSERT INTO test_oracle_exebatch(c1) values (?)";
		String rest_sql_insert = "INSERT INTO test_rest_exebatch(c1) values (?)";
		SQLException oracle_exception = null, rest_exception = null;

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracle_exebatch;");
			Util.doSQL(restConn, "drop table test_rest_exebatch;");
			Util.doSQL(restConn, "create table test_oracle_exebatch(c1 number(5))");
			Util.doSQL(restConn, "create table test_rest_exebatch(c1 number(5))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setDouble(1, 1234567.98873676);
			oraclepstmt.addBatch();
			oraclepstmt.setInt(1, 12);
			oraclepstmt.addBatch();
			oracle_res_count = oraclepstmt.executeBatch();
		} catch (SQLException e) {
			oracle_exception = e;
		} catch (NullPointerException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver
		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setDouble(1, 1234567.98873676);
			restpstmt.addBatch();
			restpstmt.setInt(1, 12);
			restpstmt.addBatch();
			rest_res_count = restpstmt.executeBatch();
		} catch (SQLException e) {
			rest_exception = e;
		} catch (NullPointerException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		assertEquals(oracle_exception.getErrorCode(),rest_exception.getErrorCode());
		assertEquals(oracle_exception.getSQLState(),rest_exception.getSQLState());
		assertEquals(oracle_exception.getMessage().trim(), rest_exception.getMessage().trim());
	}

	/*
	 * Test addBatch, by setting wrong values, followed by set correct value and then call
	 * addBatch. Oracle thin driver does not throw any error. REST JDBC driver
	 * throws <ORA-12899: value too large for column
	 * "ADHOCSQLTEST"."TEST_REST_EXEBATCH"."C1" (actual: 17, maximum: 5)>
	 */

	@Category(PassTest.class)
	@Test
	public void testAddBatch_6() {
		Connection oracleConn = null, restConn = null;
		PreparedStatement oraclepstmt = null, restpstmt = null;
		int oracle_res_count[] = null;
		int rest_res_count[] = null;
		String oracle_sql_insert = "INSERT INTO test_oracle_exebatch(c1) values (?)";
		String rest_sql_insert = "INSERT INTO test_rest_exebatch(c1) values (?)";

		// Setup
		try {
			restConn = getRestConnection();
			Util.doSQL(restConn, "drop table test_oracle_exebatch;");
			Util.doSQL(restConn, "drop table test_rest_exebatch;");
			Util.doSQL(restConn, "create table test_oracle_exebatch(c1 CHAR(5))");
			Util.doSQL(restConn, "create table test_rest_exebatch(c1 CHAR(5))");
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Oracle thin driver
		try {
			oracleConn = getOracleConnection();
			oraclepstmt = oracleConn.prepareStatement(oracle_sql_insert);
			oraclepstmt.setString(1, "Testing Rest JDBC");
			oraclepstmt.setString(1, "Test");
			oraclepstmt.addBatch();

			oracle_res_count = oraclepstmt.executeBatch();
		} catch (SQLException | ClassNotFoundException e) {
			fail(e.getMessage());
		}

		// Rest JDBC driver
		try {
			restpstmt = restConn.prepareStatement(rest_sql_insert);
			restpstmt.setString(1, "Testing Rest JDBC");
			restpstmt.setString(1, "Test");
			restpstmt.addBatch();
			rest_res_count = restpstmt.executeBatch();
		} catch (SQLException e) {
			fail(e.getMessage());
		} finally {
			try {
				oraclepstmt.close();
				restpstmt.close();
			} catch (SQLException se) {
				fail(se.getMessage());
			}
		}
		Assert.assertArrayEquals(oracle_res_count, rest_res_count);
		assertEquals(oracle_res_count.length, rest_res_count.length);
	}

	/*
	 * Basic test for <SetMaxFieldsize> and <getMaxFieldSize> method. Both these
	 * method are not yet implemented.
	 */
	@Category(PassTest.class)
	@Test
	public void testSetMaxFieldSize_1() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "CREATE TABLE testPrepareStmt_Execute1 "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPrepareStmt_Execute1 values (?,?,?)";
		String sql_drop = "drop table testPrepareStmt_Execute1 purge";
		int count = 0;

		try {
			conn = getOracleConnection();
			Util.doSQL(conn, sql);
			conn.prepareStatement(sql_drop);
			pstmt = conn.prepareStatement(sql_insert);
			pstmt.setMaxFieldSize(5);
			count = pstmt.getMaxFieldSize();
		} catch (RestJdbcNotImplementedException f) {
			assertTrue("SetMaxFieldsize method in Prepared Statement not implemented",true);
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			Util.doSQL(conn, sql_drop);
		}
		assertEquals(count, 5);
	}

	/*
	 * Basic test for <GetMoreresults> method. This method is not yet
	 * implemented.
	 */

	@Category(PassTest.class)
	@Test
	public void testgetMoreResults_1() {
		Connection conn = null;
		PreparedStatement pstmt = null, pstmt1 = null;
		String sql = "CREATE TABLE testPrepareStmt_Execute1 "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPrepareStmt_Execute1 values (?,?,?)";
		String sql_select = "select count(*) as rowcount from testPrepareStmt_Execute1";
		String sql_drop = "drop table testPrepareStmt_Execute1 purge";
		boolean returnflag = false;

		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql);
			conn.prepareStatement(sql_drop);
			pstmt = conn.prepareStatement(sql_insert);
			pstmt1 = conn.prepareStatement(sql_select);

			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();

			pstmt.setString(1, "Smith");
			pstmt.setString(2, "Anderson");
			pstmt.setLong(3, 101);
			pstmt.executeUpdate();

			ResultSet rs = pstmt1.executeQuery();
			pstmt1.getMoreResults();
			/*
			 * returnflag = pstmt1.getMoreResults(); if(returnflag) { rs.next();
			 * count = rs.getInt("rowcount"); } rs.close();
			 */

		} catch (RestJdbcNotImplementedException f) {
			assertTrue("GetMoreResults method in Prepared Statement not implemented",true);
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			Util.doSQL(conn, sql_drop);
		}
		// assertEquals(true, returnflag);
	}

	/*
	 * Basic test for <GetQueryTimeout> method. This method is not yet
	 * implemented.
	 */

	@Category(PassTest.class)
	@Test
	public void testgetQueryTimeout_1() {
		Connection conn = null, oracle_conn = null;
		PreparedStatement pstmt = null, opstmt = null;
		String sql = "CREATE TABLE testPStmt "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPStmt values (?,?,?)";
		String sql_select = "select count(*) as rowcount from testPStmt";
		String sql_drop = "drop table testPStmt purge";
		int oracleMax = -1, restMax = -1;
		int oracleZero = -1, restZero = -1;
		int oracleTimeout = 1, restTimeout = 1;
		SQLException e1 = null, e2 = null;
		int oracle_QueryTimeout = -1, rest_QueryTimeout = -1;

		// Setup
		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql_drop);
			Util.doSQL(conn, sql);
			pstmt = conn.prepareStatement(sql_insert);
			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(sql_select);
			opstmt.execute();
			oracle_QueryTimeout = opstmt.getQueryTimeout();

			// Integer.MAX_VALUE
			opstmt.setQueryTimeout(Integer.MAX_VALUE);
			oracleMax = opstmt.getQueryTimeout();

			// 0
			opstmt.setQueryTimeout(0);
			oracleZero = opstmt.getQueryTimeout();

			// Integer.MIN_VALUE
			try {
				opstmt.setQueryTimeout(Integer.MIN_VALUE);
			} catch (SQLException e) {
				e1 = e;
			}

			// Check the timeout after the exception
			oracleTimeout = opstmt.getQueryTimeout();

		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run REST jdbc driver
		try {
			pstmt = conn.prepareStatement(sql_select);
			pstmt.execute();
			pstmt.getQueryTimeout();
			// rest_QueryTimeout = pstmt.getQueryTimeout();

			// Integer.MAX_VALUE
			pstmt.setQueryTimeout(Integer.MAX_VALUE);
			restMax = pstmt.getQueryTimeout();

			// 0
			pstmt.setQueryTimeout(0);
			restZero = pstmt.getQueryTimeout();

			// Integer.MIN_VALUE
			try {
				pstmt.setQueryTimeout(Integer.MIN_VALUE);
			} catch (SQLException e) {
				e2 = e;
			}

			// Check the timeout after the exception
			restTimeout = pstmt.getQueryTimeout();
		} catch (RestJdbcNotImplementedException f) {
			assertTrue("GetQueryTimeout method in Prepared Statement not implemented",true);
		} catch (SQLException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			Util.doSQL(conn, sql_drop);
			close(opstmt, pstmt);

		}
		// assertEquals(oracle_QueryTimeout, rest_QueryTimeout);
		/*
		 * assertEquals(oracleMax, restMax); assertEquals(oracleZero, restZero);
		 * assertEquals(oracleTimeout, restTimeout);
		 *
		 * assertEquals(e1.getErrorCode(), e2.getErrorCode());
		 * assertEquals(e1.getSQLState(), e2.getSQLState());
		 * assertEquals(e1.getMessage(), e2.getMessage());
		 */
	}

	/*
	 * Basic test for <GetWarnings> method. This method is not yet implemented.
	 */

	@Category(PassTest.class)
	@Test
	public void testGetWarnings1() {
		Connection rest_conn = null, oracle_conn = null;
		PreparedStatement pstmt = null, opstmt = null;
		String sql = "CREATE TABLE testPStmt "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String createproc = "create or replace procedure myproc is" + "begin"
				+ "  raise no_data_found;" + "exception" + "  when others then"
				+ "    null;" + "end myproc;";
		SQLWarning oracleSQLWarning = null, restSQLWarning = null;
		// Used to check sql warning after calling clearWarnings method
		SQLWarning clearedOracleSQLWarning = new SQLWarning("xxx"), clearedRESTSQLWarning = new SQLWarning(
				"yyy");

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(createproc);
			opstmt.execute();
			oracleSQLWarning = opstmt.getWarnings();
			opstmt.clearWarnings();
			clearedOracleSQLWarning = opstmt.getWarnings();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run REST JDBC driver
		try {
			rest_conn = getRestConnection();
			pstmt = rest_conn.prepareStatement(createproc);
			pstmt.execute();
			pstmt.getWarnings();
			// restSQLWarning = rest_conn.getWarnings();
			// rest_conn.clearWarnings();
			// clearedRESTSQLWarning = rest_conn.getWarnings();
		} catch (RestJdbcNotImplementedException f) {
			assertTrue("GetWarnings method in Prepared Statement not implemented",true);
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			close(opstmt, pstmt);

		}
		// assertEquals(oracleSQLWarning, restSQLWarning);
		// assertEquals(clearedOracleSQLWarning, clearedRESTSQLWarning);
	}

	/*
	 * Basic test for <setFetchDirection> method. This method is not yet
	 * implemented.
	 */

	@Category(PassTest.class)
	@Test
	public void testSetFetchDirection_1() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "CREATE TABLE testPrepareStmt_Execute1 "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String sql_insert = "insert into testPrepareStmt_Execute1 values (?,?,?)";
		String sql_drop = "drop table testPrepareStmt_Execute1 purge";
		int fetchDirection = -1;

		try {
			conn = getRestConnection();
			Util.doSQL(conn, sql);
			conn.prepareStatement(sql_drop);
			pstmt = conn.prepareStatement(sql_insert);

			pstmt.setString(1, "Gary");
			pstmt.setString(2, "Larson");
			pstmt.setLong(3, 100);
			pstmt.executeUpdate();

			fetchDirection = pstmt.getFetchDirection();
			assertEquals(ResultSet.FETCH_FORWARD, fetchDirection);

			pstmt.setFetchDirection(ResultSet.FETCH_FORWARD);
			/*
			 * fetchDirection = pstmt.getFetchDirection();
			 * assertEquals(ResultSet.FETCH_FORWARD, fetchDirection);
			 *
			 * pstmt.setFetchDirection(ResultSet.FETCH_REVERSE); fetchDirection
			 * = pstmt.getFetchDirection();
			 * assertEquals(ResultSet.FETCH_FORWARD, fetchDirection);
			 *
			 * pstmt.setFetchDirection(ResultSet.FETCH_UNKNOWN); fetchDirection
			 * = pstmt.getFetchDirection();
			 * assertEquals(ResultSet.FETCH_FORWARD, fetchDirection);
			 */
		} catch (RestJdbcNotImplementedException f) {
			assertTrue("setFetchDirection method in Prepared Statement not implemented",true);
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			close(pstmt);

		}
	}

	/*
	 * Basic test for <ClearWarnings> method. This method is not yet
	 * implemented.
	 */

	@Category(PassTest.class)
	@Test
	public void testClearWarnings1() {
		Connection rest_conn = null, oracle_conn = null;
		PreparedStatement pstmt = null, opstmt = null;
		String sql = "CREATE TABLE testPStmt "
				+ "(firstname VARCHAR2(20) NOT NULL, "
				+ " lastname VARCHAR2(20) , " + " ID NUMBER(4))";
		String createproc = "create or replace procedure myproc is" + "begin"
				+ "  raise no_data_found;" + "exception" + "  when others then"
				+ "    null;" + "end myproc;";
		SQLWarning oracleSQLWarning = null, restSQLWarning = null;
		// Used to check sql warning after calling clearWarnings method
		SQLWarning clearedOracleSQLWarning = new SQLWarning("xxx"), clearedRESTSQLWarning = new SQLWarning(
				"yyy");

		// Run Oracle thin driver
		try {
			oracle_conn = getOracleConnection();
			opstmt = oracle_conn.prepareStatement(createproc);
			opstmt.execute();
			oracleSQLWarning = opstmt.getWarnings();
			opstmt.clearWarnings();
			clearedOracleSQLWarning = opstmt.getWarnings();
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		}

		// Run REST JDBC driver
		try {
			rest_conn = getRestConnection();
			pstmt = rest_conn.prepareStatement(createproc);
			pstmt.execute();
			pstmt.clearWarnings();
			// restSQLWarning = rest_conn.getWarnings();
			// rest_conn.clearWarnings();
			// clearedRESTSQLWarning = rest_conn.getWarnings();
		} catch (RestJdbcNotImplementedException f) {
			assertTrue("ClearWarnings method in Prepared Statement not implemented",true);
		} catch (SQLException | ClassNotFoundException | NullPointerException e) {
			fail(e.getMessage());
		} finally {
			close(opstmt, pstmt);

		}
		// assertEquals(oracleSQLWarning, restSQLWarning);
		// assertEquals(clearedOracleSQLWarning, clearedRESTSQLWarning);
	}

}
