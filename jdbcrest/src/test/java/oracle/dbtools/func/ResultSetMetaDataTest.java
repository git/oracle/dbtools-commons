/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.func;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.FailTest;
import oracle.dbtools.unittest.categories.PassTest;

public class ResultSetMetaDataTest extends JDBCTestCase {
  @Override
  protected void setUp() {
    
    
    
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table resultsetmetadatatest_tab purge");
      Util.trySQL(conn, "drop type msg force");
      Util.trySQL(conn, "create or replace type msg as object (ssn number, id number(10,5)," +
          " income float(10), data varchar2(100))");
      Util.trySQL(conn, "create table resultsetmetadatatest_tab(" +
          "col1 blob not null," +
          "col2 clob," +
          "col3 bfile not null," +
          "col4 nclob not null," +
          "col5 NUMBER(9,2) not null unique," +
          "col6 NUMBER(5) primary key," +
          "col7 FLOAT," +
          "col8 FLOAT(90)," +
          "col9 BINARY_FLOAT," +
          "col10 BINARY_DOUBLE," +
          "col11 DATE," +
          "col12 TIMESTAMP," +
          "col13 TIMESTAMP(5)," +
          "col14 TIMESTAMP WITH TIME ZONE," +
          "col15 TIMESTAMP (2) WITH TIME ZONE," +
          "col16 TIMESTAMP WITH LOCAL TIME ZONE," +
          "col17 TIMESTAMP (9) WITH LOCAL TIME ZONE," +
          "col18 INTERVAL YEAR TO MONTH," +
          "col19 INTERVAL YEAR (3) TO MONTH," +
          "col20 INTERVAL DAY TO SECOND," +
          "col21 INTERVAL DAY (4) TO SECOND (7)," +
          "col22 ROWID," +
          "col23 UROWID," +
          "col24 RAW(1000)," +
          "col25 LONG RAW," +
          "col26 XMLTYPE," +
          "col27 msg)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Check <code>getCatalogName</code> method. Always "" in Oracle.
   */
  @Category(PassTest.class)
  @Test
  public void testGetCatalogName() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      StringBuilder sb = new StringBuilder();
      int columnCount = oracleRsmd.getColumnCount();
      for (int i = 1; i <= columnCount; i++) {
        String s = oracleRsmd.getCatalogName(i);
        sb.append(s).append(" ");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      StringBuilder sb = new StringBuilder();
      int columnCount = restRsmd.getColumnCount();
      for (int i = 1; i <= columnCount; i++) {
        String s = restRsmd.getCatalogName(i);
        sb.append(s).append(" ");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Check <code>getColumnLabel</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetColumnLabel() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col6, " +
          "col6 || '1', " +
          "col6 || '', " +
          "col6 + 1, " +
          "col1 as department_name, " +
          "col2 as location " +
          "from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      StringBuilder sb = new StringBuilder();
      int columnCount = oracleRsmd.getColumnCount();
      for (int i = 1; i <= columnCount; i++) {
        String s = oracleRsmd.getColumnLabel(i);
        sb.append(s).append(" ");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col6, " +
          "col6 || '1', " +
          "col6 || '', " +
          "col6 + 1, " +
          "col1 as department_name, " +
          "col2 as location " +
          "from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      StringBuilder sb = new StringBuilder();
      int columnCount = restRsmd.getColumnCount();
      for (int i = 1; i <= columnCount; i++) {
        String s = restRsmd.getColumnLabel(i);
        sb.append(s).append(" ");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Check <code>getColumnName</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetColumnName() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col6, " +
          "col6 || '1', " +
          "col6 || '', " +
          "col6 + 1, " +
          "col1 as department_name, " +
          "col2 as location " +
          "from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      StringBuilder sb = new StringBuilder();
      int columnCount = oracleRsmd.getColumnCount();
      for (int i = 1; i <= columnCount; i++) {
        String s = oracleRsmd.getColumnName(i);
        sb.append(s).append(" ");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col6, " +
          "col6 || '1', " +
          "col6 || '', " +
          "col6 + 1, " +
          "col1 as department_name, " +
          "col2 as location " +
          "from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      StringBuilder sb = new StringBuilder();
      int columnCount = restRsmd.getColumnCount();
      for (int i = 1; i <= columnCount; i++) {
        String s = restRsmd.getColumnName(i);
        sb.append(s).append(" ");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>getColumnType</code> and <code>getColumnTypeName</code> on various data types.
   * Now the test is failing because <code>getColumnType</code> in REST JDBC returns different
   * values for some data types.
   * Expect:
   * BINARY_FLOAT 100
   * BINARY_DOUBLE 101
   * DATE 93 (But in java.sql.Types it is 91 for DATE)
   * TIMESTAMP WITH TIME ZONE -101
   * TIMESTAMP WITH LOCAL TIME ZONE -102
   * RAW -3
   * SYS.XMLTYPE 2009
   */
  @Category(PassTest.class)
  @Test
  public void testGetColumnTypeAndGetColumnTypeName() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.getColumnTypeName(i)).append(" ").append(oracleRsmd.getColumnType(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.getColumnTypeName(i)).append(" ").append(restRsmd.getColumnType(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>getPrecision</code> oracle data types.
   */
  @Category(PassTest.class)
  @Test
  public void testGetPrecision() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.getPrecision(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.getPrecision(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>getScale</code> on Oracle data types.
   */
  @Category(PassTest.class)
  @Test
  public void testGetScale() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.getScale(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.getScale(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  @Category(PassTest.class)
  @Test
  public void testGetSchemaNameAndGetTableName() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      oracleResult = oracleRsmd.getSchemaName(1) + oracleRsmd.getTableName(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      restResult = restRsmd.getSchemaName(1) + restRsmd.getTableName(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>isAutoIncrement</code> on Oracle data types. Always returns <code>false</code> in Oracle.
   */
  @Category(PassTest.class)
  @Test
  public void testIsAutoIncrement() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.isAutoIncrement(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.isAutoIncrement(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>isCaseSensitive</code> on Oracle data types.
   * Now the test is failing because <code>isCaseSensitive</code> returns <code>true</code> on XMLTYPE column
   * in Oracle Thin Driver while it returns <code>false</code> in REST JDBC Driver.
   * Expected: SYS.XMLTYPE true
   */
  @Category(PassTest.class)
  @Test
  public void testIsCaseSensitive() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.getColumnTypeName(i)).append(" ").append(oracleRsmd.isCaseSensitive(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.getColumnTypeName(i)).append(" ").append(restRsmd.isCaseSensitive(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>isCurrency</code> on Oracle data types.
   */
  @Category(PassTest.class)
  @Test
  public void testIsCurrency() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.isCurrency(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.isCurrency(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>isDefinitelyWritable</code> oracle data types. Always <code>false</code> in Oracle.
   */
  @Category(PassTest.class)
  @Test
  public void testIsDefinitelyWritable() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.isDefinitelyWritable(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.isDefinitelyWritable(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>isNullable</code> on various Oracle data types.
   */
  @Category(PassTest.class)
  @Test
  public void testIsNullable() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.isNullable(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.isNullable(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>isReadOnly</code> on various Oracle data types. Always <code>false</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testIsReadOnly() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.isReadOnly(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.isReadOnly(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>isWritable</code> on various Oracle data types. Always <code>true</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testIsWritable() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.isWritable(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.isWritable(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>isSigned</code> on various Oracle data types. Always <code>true</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testIsSigned() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.isSigned(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.isSigned(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>isSearchable</code> on various Oracle data types.
   * Now the test is failing because <code>isSearchable</code> returns
   * different values in REST JDBC on some types.
   * Expected:
   * BINARY_FLOAT true
   * BINARY_DOUBLE true
   * TIMESTAMP WITH LOCAL TIME ZONE true
   * SYS.XMLTYPE true
   */
  @Category(PassTest.class)
  @Test
  public void testIsSearchable() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      int columnCount = oracleRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsmd.getColumnTypeName(i)).append(" ").append(oracleRsmd.isSearchable(i)).append("\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      int columnCount = restRsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsmd.getColumnTypeName(i)).append(" ").append(restRsmd.isSearchable(i)).append("\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Pass invalid index to <code>getColumnName</code>.
   * Expected SQLException details:
   * detailMessage: Invalid column index
   * SQLState: 99999
   * errorCode: 17003
   */
  @Category(PassTest.class)
  @Test
  public void testInvalidIndexGetColumnName() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      try {
        oracleRsmd.getColumnName(1000);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      try {
        restRsmd.getColumnName(1000);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals("Invalid column index", e2.getMessage());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
  }

  /**
   * Pass invalid index to <code>getColumnLabel</code>.
   * Expected SQLException details:
   * detailMessage: Invalid column index
   * SQLState: 99999
   * errorCode: 17003
   */
  @Category(PassTest.class)
  @Test
  public void testInvalidIndexGetColumnLabel() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      try {
        oracleRsmd.getColumnLabel(1000);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      try {
        restRsmd.getColumnLabel(1000);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals("Invalid column index", e2.getMessage());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
  }

  /**
   * Pass invalid index to <code>getColumnType</code>.
   * Expected SQLException details:
   * detailMessage: Invalid column index
   * SQLState: 99999
   * errorCode: 17003
   */
  @Category(PassTest.class)
  @Test
  public void testInvalidIndexGetColumnType() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      try {
        oracleRsmd.getColumnType(1000);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      try {
        restRsmd.getColumnType(1000);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals("Invalid column index", e2.getMessage());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
  }

  /**
   * Pass invalid index to <code>getColumnTypeName</code>.
   * Expected SQLException details:
   * detailMessage: Invalid column index
   * SQLState: 99999
   * errorCode: 17003
   */
  @Category(PassTest.class)
  @Test
  public void testInvalidIndexGetColumnTypeName() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      try {
        oracleRsmd.getColumnTypeName(1000);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      try {
        restRsmd.getColumnTypeName(1000);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals("Invalid column index", e2.getMessage());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
  }

  /**
   * Pass invalid index to <code>getPrecision</code>.
   * Expected SQLException details:
   * detailMessage: Invalid column index
   * SQLState: 99999
   * errorCode: 17003
   */
  @Category(PassTest.class)
  @Test
  public void testInvalidIndexGetPrecision() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsmd = null, restRsmd = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsetmetadatatest_tab");
      oracleRsmd = oracleRs.getMetaData();
      try {
        oracleRsmd.getPrecision(1000);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsetmetadatatest_tab");
      restRsmd = restRs.getMetaData();
      try {
        restRsmd.getPrecision(1000);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals("Invalid column index", e2.getMessage());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
  }

  @Override
  protected void tearDown() {
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table resultsetmetadatatest_tab purge");
      Util.trySQL(conn, "drop type msg");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }
}
