/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.func;

import java.net.URL;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.Map;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.jdbc.util.RestJdbcUnsupportedException;
import oracle.dbtools.jdbc.util.RestJdbcUnsupportedOperationException;
import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.FailTest;
import oracle.dbtools.unittest.categories.PassTest;
import oracle.dbtools.unittest.categories.UnsupportedTest;

public class ResultSetTest extends JDBCTestCase {
  @Override
  public void setUp() throws Exception {
    Connection restConn = null;
    try {
      restConn = getRestConnection();
      Util.trySQL(restConn, "create table strnumtab (" +
          "  col1 varchar2(2), " +
          "  col2 varchar2(20), " +
          "  col3 varchar2(38), " +
          "  col4 varchar2(1), " +
          "  col5 varchar2(20), " +
          "  col6 varchar2(38), " +
          "  col7 varchar2(50), " +
          "  col8 varchar2(100), " +
          "  col9 varchar2(150), " +
          "  col10 varchar2(200), " +
          "  col11 varchar2(250))");
      Util.trySQL(restConn, "insert into strnumtab values ('1', '127', '32767', " +
          "'0', '2147483647', '9223372036854775807', " +
          "'12345678901234567890123456789012345', " +
          "'32766.33', '2147483646.92313', " +
          "'9223372036854775806.234353', " +
          "'1234567890123456789012345.1234567')");
      Util.trySQL(restConn, "insert into strnumtab values ('-1', '-128', '-32768', " +
          "'0', '-2147483648', '-9223372036854775808', " +
          "'-12345678901234567890123456789012345', '-32766.33', '-2147483646.92313', " +
          "'-9223372036854775806.2343', '-1234567890123456789012345.12345')");
    } catch (SQLException | ClassNotFoundException e) {
    }
  }

  /**
   * Call next method on a closed ResultSet.
   */
  @Category(PassTest.class)
  @Test
  public void testClose1() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected = "Closed Resultset: next";
    String actual = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table sampletab(a number, b varchar(20))");
      Util.doSQL(conn, "insert into sampletab values (1, 'value 1')");
      Util.doSQL(conn, "insert into sampletab values (2, 'value 2')");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select * from sampletab");
      rs.next();
      String s = rs.getString(2);
      rs.close();
      try {
        rs.next();
      } catch (SQLException e) {
        actual = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table sampletab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected, actual);
  }

  /**
   * Close ResultSet after querying on an empty table.
   */
  @Category(PassTest.class)
  @Test
  public void testClose2() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected = "Closed Resultset: next";
    String actual = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table emptytable(a number, b varchar(20))");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select * from emptytable");
      rs.close();
      try {
        rs.next();
      } catch (SQLException e) {
        actual = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table emptytable purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected, actual);
  }

  /**
   * Call getMetaData method on a closed ResultSet.
   * This should not be allowed. A SQLException with message: Closed Resultset: getMetaData is expected.
   */
  @Category(PassTest.class)
  @Test
  public void testClose3() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected = "Closed Resultset: getMetaData";
    String actual = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table sampletab(a number, b varchar(20))");
      Util.doSQL(conn, "insert into sampletab values (1, 'value 1')");
      Util.doSQL(conn, "insert into sampletab values (2, 'value 2')");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select * from sampletab");
      rs.close();
      try {
        rs.getMetaData();
      } catch (SQLException e) {
        actual = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table sampletab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected, actual);
  }

  /**
   * Call findColumn on a closed <code>ResultSet</code>
   *
  @Category(FailTest.class)
  @Test
  public void testClose4() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected = "Closed Resultset";
    String actual = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table sampletab(a number, b varchar(20))");
      Util.doSQL(conn, "insert into sampletab values (1, 'value 1')");
      Util.doSQL(conn, "insert into sampletab values (2, 'value 2')");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select * from sampletab");
      rs.close();
      try {
        rs.findColumn("a");
      } catch (SQLException e) {
        actual = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table sampletab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected, actual);
  }
  **/

  /**
   * Test whether close a <code>Statement</code> will close its current <code>ResultSet<code/>
   * Now the test is failing since RESTJDBC does not close its current <code>ResultSet<code/>
   * when close a <code>Statement</code>.
   * Unsupported right now in the RESTJDBC driver
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testClose5() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    boolean oracleRsClosed = false, restRsClosed = false;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from dual");
      oracleStmt.close();
      oracleRsClosed = oracleRs.isClosed();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from dual");
      restStmt.close();
      restRsClosed = restRs.isClosed();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleRsClosed, restRsClosed);
  }

  /**
   * Test <code>getShort</code> by column index and by column name
   */
  @Category(PassTest.class)
  @Test
  public void testGetShort1() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table test_short_tab(col1 number(20))");
      Util.doSQL(conn, "insert into test_short_tab values (123)");
      Util.doSQL(conn, "insert into test_short_tab values (null)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select col1 from test_short_tab");
      rs.next();
      //getShort by index
      short aShort1 = rs.getShort(1);
      assertEquals(123, aShort1);

      //getShort by column name
      short aShort2 = rs.getShort("col1");
      assertEquals(123, aShort2);

      rs.next();

      //getShort on a SQL null
      short aShort3 = rs.getShort(1);
      assertEquals(0, aShort3);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_short_tab purge");
    }
    close(rs);
    close(stmt);
  }

  /**
   * Negative test for <code>getShort</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetShort2() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected1 = "Invalid column index";
    String actual1 = "";
    String expected2 = "Invalid column index";
    String actual2 = "";
    String expected3 = "Invalid column index";
    String actual3 = "";
    String expected4 = "Invalid column name";
    String actual4 = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table test_short_tab(col1 number(20))");
      Util.doSQL(conn, "insert into test_short_tab values (123)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select col1 from test_short_tab");
      rs.next();

      try {
        rs.getShort(2);
      } catch (SQLException e) {
        actual1 = e.getMessage();
      }

      try {
        rs.getShort(0);
      } catch (SQLException e) {
        actual2 = e.getMessage();
      }

      try {
        rs.getShort(-1);
      } catch (SQLException e) {
        actual3 = e.getMessage();
      }

      try {
        rs.getShort("x");
      } catch (SQLException e) {
        actual4 = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_short_tab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected1, actual1);
    assertEquals(expected2, actual2);
    assertEquals(expected3, actual3);
    assertEquals(expected4, actual4);
  }

  /**
   * Test <code>getShort</code> on a closed <code>ResultSet</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetShort3() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected = "Closed Resultset: getShort(int columnIndex)";
    String actual = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table test_short_tab(col1 number(20))");
      Util.doSQL(conn, "insert into test_short_tab values (123)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select col1 from test_short_tab");
      rs.next();
      rs.close();
      try {
        rs.getShort(1);
      } catch (SQLException e) {
        actual = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_short_tab purge");
    }
    close(stmt);
    assertEquals(expected, actual);
  }

  /**
   * Check the boundary for <code>getShort</code> method.
   * Now the test is failing because jdbcrest does not throw a SQLException
   * when the value from <code>ResultSet</code> exceeds the boundary.
   */
  @Category(PassTest.class)
  @Test
  public void testGetShort4() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    short x1 = 0;
    short x2 = 0;
    String expected1 = "Numeric Overflow";
    String actual1 = "";
    String expected2 = "Numeric Overflow";
    String actual2 = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table test_getshort_tab(x number)");
      Util.doSQL(conn, "insert into test_getshort_tab values (32767)");
      Util.doSQL(conn, "insert into test_getshort_tab values (-32768)");
      Util.doSQL(conn, "insert into test_getshort_tab values (32768)");
      Util.doSQL(conn, "insert into test_getshort_tab values (-32769)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select x from test_getshort_tab");
      rs.next();
      try {
        x1 = rs.getShort(1);
      } catch (SQLException e) {
        fail(e.getMessage());
      }
      rs.next();
      try {
        x2 = rs.getShort(1);
      } catch (SQLException e) {
        fail(e.getMessage());
      }
      rs.next();
      try {
        rs.getShort(1);
      } catch (SQLException e) {
        actual1 = e.getMessage();
      }
      rs.next();
      try {
        rs.getShort(1);
      } catch (SQLException e) {
        actual2 = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_getshort_tab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(32767, x1);
    assertEquals(-32768, x2);
    assertEquals(expected1, actual1);
    assertEquals(expected2, actual2);
  }

  /**
   * Test <code>getShort</code> on an empty <code>ResultSet</code>.
   * The test is failing since the exception message is different.
   *
   * Expected error message: Result set after last row
   */
  @Category(PassTest.class)
  @Test
  public void testGetShort5() {
    Connection restConn = null, oracleConn = null;
    Statement rstmt = null, ostmt = null;
    SQLException oracleException = null, restException = null;
    
    // Run the Oracle Driver
    try {
      oracleConn = getOracleConnection();
      ostmt = oracleConn.createStatement();
      ostmt.execute("create table get_short_tab (x int)");
      ResultSet rs = ostmt.executeQuery("select * from get_short_tab");
      rs.next();
      try {
        rs.getShort(1);
      } catch (SQLException e) {
          oracleException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(oracleConn, "drop table get_short_tab purge");
      close(ostmt);
    }    
    
    // Run the REST Driver
    try {
      restConn = getRestConnection();
      rstmt = restConn.createStatement();
      rstmt.execute("create table get_short_tab (x int)");
      ResultSet rs = rstmt.executeQuery("select * from get_short_tab");
      rs.next();
      try {
        rs.getShort(1);
      } catch (SQLException e) {
          restException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(restConn, "drop table get_short_tab purge");   
    }    
    assertEquals(oracleException.getMessage(), restException.getMessage());
  }

  /**
   * Test <code>getShort</code> on a Blob column, ORA-17004 is expected.
   * Now <code>getShort</code> is getting NPE and the test is failing.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetShort6() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    int errorCode = -1;
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table blobtab(col1 blob)");
      Util.doSQL(conn, "insert into blobtab values ('00010203040506070809')");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select * from blobtab");
      rs.next();
      try {
        rs.getShort(1);
      } catch (SQLException e) {
        errorCode = e.getErrorCode();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table blobtab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(17004, errorCode);
  }

  /**
   * Test <code>getShort</code> on string columns.
   */
  @Category(PassTest.class)
  @Test
  public void testGetShort7() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    //Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from strnumtab");
      oracleRsm = oracleRs.getMetaData();
      int columnNumber = oracleRsm.getColumnCount();
      StringBuilder sb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnNumber; i++) {
          sb.append(oracleRs.getShort(i) + "\n");
        }
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    //Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from strnumtab");
      restRsm = restRs.getMetaData();
      int columnNumber = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnNumber; i++) {
          restSb.append(restRs.getShort(i) + "\n");
        }
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleResult, restResult);
  }

  /**
   * Positive test of <code>getInt</code> method
   */
  @Category(PassTest.class)
  @Test
  public void testGetInt() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    int a11 = -1;
    int a12 = -1;
    try {
      conn = getRestConnection();
      Util.trySQL(conn, "drop table test_getint_tab purge");
      Util.doSQL(conn, "create table test_getint_tab(a1 number)");
      Util.doSQL(conn, "insert into test_getint_tab values (10020)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select a1 from test_getint_tab");
      rs.next();
      //getInt by column index
      a11 = rs.getInt(1);

      //getInt by column name
      a12 = rs.getInt("a1");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_getint_tab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(10020, a11);
    assertEquals(10020, a12);
  }

  @Category(PassTest.class)
  @Test
  public void testGetInt2() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected1 = "Invalid column index";
    String actual1 = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table test_getint_tab(col1 number(20))");
      Util.doSQL(conn, "insert into test_getint_tab values (123)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select col1 from test_getint_tab");
      rs.next();
      try {
        rs.getInt(2);
      } catch (SQLException e) {
        actual1 = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_getint_tab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected1, actual1);
  }

  /**
   * Test passing a non-exist column to <code>getInt<code/> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetInt3() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected1 = "Invalid column name";
    String actual1 = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table test_getint_tab(col1 number(20))");
      Util.doSQL(conn, "insert into test_getint_tab values (123)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select col1 from test_getint_tab");
      rs.next();
      try {
        rs.getInt("x");
      } catch (SQLException e) {
        actual1 = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_getint_tab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected1, actual1);
  }

  /**
   * Test <code>getInt</code> on a closed <code>ResultSet</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetInt4() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected1 = "Closed Resultset: getInt(int columnIndex)";
    String actual1 = "";
    String expected2 = "Closed Resultset: getInt(String columnLabel)";
    String actual2 = "";
    String expected3 = "Closed Resultset: getInt(int columnIndex)";
    String actual3 = "";
    String expected4 = "Closed Resultset: getInt(String columnLabel)";
    String actual4 = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table test_getint_tab(col1 number(20))");
      Util.doSQL(conn, "insert into test_getint_tab values (123)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select col1 from test_getint_tab");
      rs.next();
      rs.close();

      try {
        rs.getInt(1);
      } catch (SQLException e) {
        actual1 = e.getMessage();
      }

      try {
        rs.getInt("col1");
      } catch (SQLException e) {
        actual2 = e.getMessage();
      }

      try {
        rs.getInt(23);
      } catch (SQLException e) {
        actual3 = e.getMessage();
      }

      try {
        rs.getInt("x");
      } catch (SQLException e) {
        actual4 = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_getint_tab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected1, actual1);
    assertEquals(expected2, actual2);
    assertEquals(expected3, actual3);
    assertEquals(expected4, actual4);
  }

  /**
   * Test <code>getInt</code> on a Blob column, ORA-17004 is expected.
   * Now <code>getInt</code> is getting NPE and the test is failing.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetInt5() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    int errorCode = -1;
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table blobtab(col1 blob)");
      Util.doSQL(conn, "insert into blobtab values ('00010203040506070809')");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select * from blobtab");
      rs.next();
      try {
        rs.getInt(1);
      } catch (SQLException e) {
        errorCode = e.getErrorCode();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table blobtab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(17004, errorCode);
  }

  /**
   * Test <code>getInt</code> on an empty <code>ResultSet</code>.
   * The test is failing since the exception message is different.
   *
   * Expected error message: Result set after last row
   */
  @Category(PassTest.class)
  @Test
  public void testGetInt6() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected = "Result set after last row";
    SQLException se = null;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute("create table get_int_tab (x int)");
      rs = stmt.executeQuery("select * from get_int_tab");
      rs.next();
      rs.getInt(1);      
    } catch (SQLException e) {
        se = e;
    }
    catch(ClassNotFoundException e1) {
      fail(e1.getMessage());
    } finally {
      Util.trySQL(conn, "drop table get_int_tab purge");
    }
    assertEquals(expected, se.getMessage());
    close(rs);
    close(stmt);   
  }

  /**
   * Test <code>getInt</code> on string columns.
   */
  @Category(PassTest.class)
  @Test
  public void testGetInt7() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    //Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from strnumtab");
      oracleRsm = oracleRs.getMetaData();
      int columnNumber = oracleRsm.getColumnCount();
      StringBuilder sb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnNumber; i++) {
          sb.append(oracleRs.getInt(i) + "\n");
        }
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    //Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from strnumtab");
      restRsm = restRs.getMetaData();
      int columnNumber = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnNumber; i++) {
          restSb.append(restRs.getInt(i) + "\n");
        }
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>getLong</code> by column index and by column name.
   */
  @Category(PassTest.class)
  @Test
  public void testGetLong1() {
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    long max = -1;
    long min = 0;
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table t_get_long(i number)");
      Util.doSQL(conn, "insert into t_get_long values (9)");
      Util.doSQL(conn, "insert into t_get_long values (2147483648)");
      Util.doSQL(conn, "insert into t_get_long values (-2147483649)");
      String sql = "select max(i) from t_get_long where i > ?";
      ps = conn.prepareStatement(sql);
      ps.setLong(1, -2147483649L);
      rs = ps.executeQuery();
      rs.next();
      max = rs.getLong(1);

      sql = "select min(i) from t_get_long";
      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();
      rs.next();
      min = rs.getLong("min(i)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table t_get_long purge");
    }
    close(rs);
    close(ps);
    assertEquals(2147483648L, max);
    assertEquals(-2147483649L, min);
  }

  /**
   * Test passing invalid column index to <code>getLong</code>, a <code>SQLException</code> with
   * Invalid column index message is expected.
   */
  @Category(PassTest.class)
  @Test
  public void testGetLong2() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected1 = "Invalid column index";
    String actual1 = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table test_getlong_tab(col1 number(20))");
      Util.doSQL(conn, "insert into test_getlong_tab values (123)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select col1 from test_getlong_tab");
      rs.next();
      try {
        rs.getLong(2);
      } catch (SQLException e) {
        actual1 = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_getlong_tab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected1, actual1);
  }

  /**
   * Test passing a non-exist column to <code>getLong<code/> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetLong3() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected1 = "Invalid column name";
    String actual1 = "";
    try {
      conn = getRestConnection();     
      Util.doSQL(conn, "create table test_getlong_tab(col1 number(20))");
      Util.doSQL(conn, "insert into test_getlong_tab values (123)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select col1 from test_getlong_tab");
      rs.next();
      try {
        rs.getLong("x");
      } catch (SQLException e) {
        actual1 = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_getlong_tab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected1, actual1);
  }

  /**
   * Test <code>getLong</code> on a closed <code>ResultSet</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetLong4() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected1 = "Closed Resultset: getLong(int columnIndex)";
    String actual1 = "";
    String expected2 = "Closed Resultset: getLong(String columnLabel)";
    String actual2 = "";
    String expected3 = "Closed Resultset: getLong(int columnIndex)";
    String actual3 = "";
    String expected4 = "Closed Resultset: getLong(String columnLabel)";
    String actual4 = "";
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table test_getlong_tab(col1 number(20))");
      Util.doSQL(conn, "insert testGetColumnCount_duplicateColumnNamesinto test_getlong_tab values (123)");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select col1 from test_getlong_tab");
      rs.next();
      rs.close();

      try {
        rs.getLong(1);
      } catch (SQLException e) {
        actual1 = e.getMessage();
      }

      try {
        rs.getLong("col1");
      } catch (SQLException e) {
        actual2 = e.getMessage();
      }

      try {
        rs.getLong(23);
      } catch (SQLException e) {
        actual3 = e.getMessage();
      }

      try {
        rs.getLong("x");
      } catch (SQLException e) {
        actual4 = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table test_getlong_tab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected1, actual1);
    assertEquals(expected2, actual2);
    assertEquals(expected3, actual3);
    assertEquals(expected4, actual4);
  }

  /**
   * Test <code>getLong</code> on a Blob column, ORA-17004 is expected.
   * Now <code>getLong</code> is getting NPE and the test is failing.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetLong5() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    int errorCode = -1;
    try {
      conn = getRestConnection();
      Util.doSQL(conn, "create table blobtab(col1 blob)");
      Util.doSQL(conn, "insert into blobtab values ('00010203040506070809')");
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select * from blobtab");
      rs.next();
      try {
        rs.getLong(1);
      } catch (SQLException e) {
        errorCode = e.getErrorCode();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table blobtab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(17004, errorCode);
  }

  /**
   * Test <code>getLong</code> on an empty <code>ResultSet</code>.
   * Expected error message: Result set after last row
   */
  @Category(PassTest.class)
  @Test
  public void testGetLong6() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected = "Result set after last row";
    String actual = "";
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute("create table get_long_tab (x int)");
      rs = stmt.executeQuery("select * from get_long_tab");
      rs.next();
      try {
        rs.getLong(1);
      } catch (SQLException e) {
        actual = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table get_long_tab purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected, actual);
  }

  /**
   * Test <code>getLong</code> on string columns.
   */
  @Category(PassTest.class)
  @Test
  public void testGetLong7() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    //Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from strnumtab order by col1");
      oracleRsm = oracleRs.getMetaData();
      int columnNumber = oracleRsm.getColumnCount();
      StringBuilder sb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnNumber; i++) {
          sb.append(oracleRs.getLong(i) + "\n");
        }
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    //Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from strnumtab order by col1");
      restRsm = restRs.getMetaData();
      int columnNumber = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnNumber; i++) {
          restSb.append(restRs.getLong(i) + "\n");
        }
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleResult, restResult);
  }

  /**
   * A positive test for <code>next</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testNext1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      Util.trySQL(oracleConn, "create table samptab (a number, b varchar(20))");
      Util.trySQL(oracleConn, "insert into samptab values (1, 'value 1')");
      Util.trySQL(oracleConn, "insert into samptab values (2, 'value 2')");
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from samptab order by a desc");
      StringBuilder sb = new StringBuilder();
      while (oracleRs.next()) {
        sb.append(oracleRs.getInt(1) + " " + oracleRs.getString(2) + "\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(oracleConn, "drop table samptab purge");
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      Util.trySQL(restConn, "create table samptab (a number, b varchar(20))");
      Util.trySQL(restConn, "insert into samptab values (1, 'value 1')");
      Util.trySQL(restConn, "insert into samptab values (2, 'value 2')");
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from samptab order by a desc");
      StringBuilder sb = new StringBuilder();
      while (restRs.next()) {
        sb.append(restRs.getInt(1) + " " + restRs.getString(2) + "\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(restConn, "drop table samptab purge");
    }
    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test call <code>getInt</code> before calling <code>next</code> on a <code>ResultSet</code>.
   * Now the test is failing because the exception message is not ResultSet.next was not called.
   */
  @Category(PassTest.class)
  @Test
  public void testNext2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 100 as x from dual");
      try {
        oracleRs.getString(1);
      } catch (SQLException e) {
        oracleResult = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    if (oracleRs != null) {
      close(oracleRs);
    }
    if (oracleStmt != null) {
      close(oracleStmt);
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 100 as x from dual");
      try {
        restRs.getString(1);
      } catch (SQLException e) {
        restResult = e.getMessage();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    if (restRs != null) {
      close(restRs);
    }
    if (restStmt != null) {
      close(restStmt);
    }

    assertEquals(oracleResult, restResult);
  }

  /**
   * Checking <code>next</code> to different rows.
   */
  @Category(PassTest.class)
  @Test
  public void testNext3() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      Util.trySQL(oracleConn, "create table samptab (a number, b varchar(20))");
      Util.trySQL(oracleConn, "insert into samptab values (1, 'value 1')");
      Util.trySQL(oracleConn, "insert into samptab values (2, 'value 2')");
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from samptab order by a desc");

      oracleRs.next();
      oracleRs.next();
      oracleResult = oracleRs.getInt(1) + " " + oracleRs.getString(2);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(oracleConn, "drop table samptab purge");
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      Util.trySQL(restConn, "create table samptab (a number, b varchar(20))");
      Util.trySQL(restConn, "insert into samptab values (1, 'value 1')");
      Util.trySQL(restConn, "insert into samptab values (2, 'value 2')");
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from samptab order by a desc");

      restRs.next();
      restRs.next();
      restResult = restRs.getInt(1) + " " + restRs.getString(2);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(restConn, "drop table samptab purge");
    }
    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>next</code> method on a closed <code>ResultSet</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testNext4() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from dual");
      oracleRs.close();
      try {
        oracleRs.next();
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from dual");
      restRs.close();
      try {
        restRs.next();
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(e1.getMessage(), e2.getMessage());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
  }

  /**
   * Checking allowable column names by getting them from <code>getMetaData</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetMetaData1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "", restResult = "";
    String createTableSql = "create table samptab (" +
        "\"lowercase\" varchar2(20), " +
        "\"UPPERCASE\" char(20), " +
        "\"UPPER_CASE\" raw(20), " +
        "UPPER#CASE number(8)," +
        "\"MixedCase\" raw(20), " +
        "\"underscore_name\" raw(20), " +
        "\"MixedCase_Underscore_Name\" raw(20), " +
        "\"Name#6\" raw(20), " +
        "\"7thcol\" raw(20), " +
        "\"special$column\" raw(20), " +
        "\"a\" raw(20), " +
        "\"1\" raw(20), " +
        "\"thirty_characters_012345678901\" raw(20))";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      Util.trySQL(oracleConn, createTableSql);
      oracleStmt = oracleConn.createStatement();
      oracleStmt.execute("insert into samptab values ('000102', '010203', '020304', 30405, '040506', " +
          "'050607', '060708', '070809', '080910', '101112', '111213', '121314', '131415')");
      oracleRs = oracleStmt.executeQuery("select * from samptab");
      ResultSetMetaData rsm = oracleRs.getMetaData();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= rsm.getColumnCount(); i++) {
        sb.append(rsm.getColumnName(i) + "\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(oracleConn, "drop table samptab purge");
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      Util.trySQL(restConn, createTableSql);
      restStmt = restConn.createStatement();
      restStmt.execute("insert into samptab values ('000102', '010203', '020304', 30405, '040506', " +
          "'050607', '060708', '070809', '080910', '101112', '111213', '121314', '131415')");
      restRs = restStmt.executeQuery("select * from samptab");
      ResultSetMetaData rsm = restRs.getMetaData();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= rsm.getColumnCount(); i++) {
        sb.append(rsm.getColumnName(i) + "\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(restConn, "drop table samptab purge");
    }
    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test creating a table with column limit(1000) and using <code>getMetaData</code> method.
   * Now the test is failing because in REST JDBC, the following SQLException is thrown even create
   * a table with exactly 1000 columns:
   * ORA-01792: maximum number of columns in a table or view is 1000
   * This is an ORDS limitation. 
   * ORDS adds a new "hidden" column RN__ which is used to identify the row number and perform pagination.
   * So a limitation of ORDS is to query 999 columns.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetMetaData2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";
    SQLException oException = null, rException = null;
    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();

      StringBuilder tabSb = new StringBuilder("create table maxcoltab1 (");
      for (int i = 1; i < 1000; i++) {
        tabSb.append("col" + i + " number, ");
      }
      tabSb.append("col1000 number)");
      oracleStmt.execute(tabSb.toString());

      StringBuilder insertSb = new StringBuilder("insert into maxcoltab1 values(");
      for (int i = 1; i < 1000; i++) {
        insertSb.append(i + ", ");
      }
      insertSb.append("1000)");
      oracleStmt.execute(insertSb.toString());

      oracleRs = oracleStmt.executeQuery("select * from maxcoltab1");
      oracleRsm = oracleRs.getMetaData();

      int columnCount = oracleRsm.getColumnCount();
      oracleRs.next();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(oracleRsm.getColumnName(i) + " is " + oracleRs.getInt(i) + "\n");
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(oracleConn, "drop table maxcoltab1 purge");
    }

    if (oracleRs != null) {
      close(oracleRs);
    }
    if (oracleStmt != null) {
      close(oracleStmt);
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();

      StringBuilder tabSb = new StringBuilder("create table maxcoltab2 (");
      for (int i = 1; i < 1000; i++) {
        tabSb.append("col" + i + " number, ");
      }
      tabSb.append("col1000 number)");
      restStmt.execute(tabSb.toString());

      StringBuilder insertSb = new StringBuilder("insert into maxcoltab2 values(");
      for (int i = 1; i < 1000; i++) {
        insertSb.append(i + ", ");
      }
      insertSb.append("1000)");
      restStmt.execute(insertSb.toString());

      restRs = restStmt.executeQuery("select * from maxcoltab2");
      restRsm = restRs.getMetaData();

      int columnCount = restRsm.getColumnCount();
      restRs.next();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        sb.append(restRsm.getColumnName(i) + " is " + restRs.getInt(i) + "\n");
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
        Util.trySQL(restConn, "drop table maxcoltab2 purge");
    }

    if (restRs != null) {
      close(restRs);
    }
    if (restStmt != null) {
      close(restStmt);
    }

    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>getObject</code> on nulls and strings.
   */
  @Category(PassTest.class)
  @Test
  public void testGetObject1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
      Util.trySQL(conn, "drop table mixstrtab purge");
      Util.trySQL(conn, "create table nulltab (col1 number, col2 varchar2(5), col3 char(4), col4 raw(10), col5 date, col6 blob)");
      Util.trySQL(conn, "create table mixstrtab (" +
          "col1 char(2), " +
          "col2 char(20), " +
          "col3 char(38), " +
          "col4 varchar2(1), " +
          "col5 varchar2(20), " +
          "col6 varchar2(38), " +
          "col7 long, " +
          "col8 varchar2(50), " +
          "col9 varchar2(100), " +
          "col10 varchar2(150), " +
          "col11 varchar2(200), " +
          "col12 varchar2(250))");
      Util.trySQL(conn, "insert into nulltab values (null, null, null, null, null, null)");
      Util.trySQL(conn, "insert into mixstrtab values ('1', '127', '32767'," +
          "'0', '@#$@$%#$%#$', 'Some long string as you can see', " +
          "'Unprofound but long', 'SUN ULTRA 1', " +
          "'fragment of a string', 'not null at all', " +
          "'aha noon what do those two words have in common', " +
          "'not the answer column')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleNullResult = "", restNullResult = "";
    String oracleStrResult = "", restStrResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();

      // get nulls
      oracleRs = oracleStmt.executeQuery("select * from nulltab");
      oracleRs.next();
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        oracleSb.append(oracleRs.getObject(i));
      }
      oracleNullResult = oracleSb.toString();

      // get oracle string
      oracleRs = oracleStmt.executeQuery("select * from mixstrtab");
      oracleRsm = oracleRs.getMetaData();
      columnCount = oracleRsm.getColumnCount();
      oracleSb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          oracleSb.append(oracleRs.getObject(i) + "\n");
        }
      }
      oracleStrResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getORestConnection();
      restStmt = restConn.createStatement();

      // get nulls
      restRs = restStmt.executeQuery("select * from nulltab");
      restRs.next();
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        restSb.append(restRs.getObject(i));
      }
      restNullResult = restSb.toString();

      // get oracle string
      restRs = restStmt.executeQuery("select * from mixstrtab");
      restRsm = restRs.getMetaData();
      columnCount = restRsm.getColumnCount();
      restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          restSb.append(restRs.getObject(i) + "\n");
        }
      }
      restStrResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab");
      Util.trySQL(conn, "drop table mixstrtab");
      Util.trySQL(conn, "drop table datetab");
      Util.trySQL(conn, "drop table rowidtab");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleNullResult, restNullResult);
    assertEquals(oracleStrResult, restStrResult);
  }

  /**
   * Test <code>getObject</code> on oracle raw columns.
   * Now the test if failing because <code>getObject</code> on a raw column
   * is getting SQLException: Invalid column index
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetObject2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rawtab purge");
      Util.trySQL(conn, "create table rawtab (" +
          "col1 raw(20), " +
          "col2 raw(30))");
      Util.trySQL(conn, "insert into rawtab values ('0102030405', '0607080910')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleRawResult = "", restRawResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from rawtab");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          oracleSb.append(Util.getByteArrayString((byte[])oracleRs.getObject(i)));
        }
      }
      oracleRawResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from rawtab");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          restSb.append(Util.getByteArrayString((byte[])restRs.getObject(i)));
        }
      }
      restRawResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rawtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleRawResult, restRawResult);
  }

    /**
     * Check 0, -1, 100 index on <code>getObject</code> method.
     */
  @Category(PassTest.class)
  @Test
  public void testGetObject3() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;

    // zero index
    SQLException oracleException1 = null, restException1 = null;
    // -1 index
    SQLException oracleException2 = null, restException2 = null;
    // 100 index
    SQLException oracleException3 = null, restException3 = null;

    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
      Util.trySQL(conn, "create table simpletab(col1 number(5), col2 number(6), col3 number(7), col4 number(8))");
      Util.trySQL(conn, "insert into simpletab values (2, 3, 4, 5)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();

      // zero index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getObject(0);
      } catch (SQLException e) {
        oracleException1 = e;
      }

      // -1 index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getObject(-1);
      } catch (SQLException e) {
        oracleException2 = e;
      }

      // 100 index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getObject(100);
      } catch (SQLException e) {
        oracleException3 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run Oracle Thin Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();

      // zero index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getObject(0);
      } catch (SQLException e) {
        restException1 = e;
      }

      // -1 index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getObject(-1);
      } catch (SQLException e) {
        restException2 = e;
      }

      // 100 index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getObject(100);
      } catch (SQLException e) {
        restException3 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Cleanup
    try{
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleException1.getMessage(), restException1.getMessage());
    assertEquals(oracleException1.getErrorCode(), restException1.getErrorCode());
    assertEquals(oracleException1.getSQLState(), restException1.getSQLState());

    assertEquals(oracleException2.getMessage(), restException2.getMessage());
    assertEquals(oracleException2.getErrorCode(), restException2.getErrorCode());
    assertEquals(oracleException2.getSQLState(), restException2.getSQLState());

    assertEquals(oracleException3.getMessage(), restException3.getMessage());
    assertEquals(oracleException3.getErrorCode(), restException3.getErrorCode());
    assertEquals(oracleException3.getSQLState(), restException3.getSQLState());
  }

  /**
   * Test <code>getObject</code> on number columns.
   * Now the test is failing because of the precision on float is different.
   * There is a difference in the precision of floats which is why the test
   * is marked as unsupported. The difference is not significant and it just does E18 
   * instead of printing out 18 digits.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetObject4() {
    // Setup
    try {
      Connection conn = getOracleConnection();
      Util.trySQL(conn, "drop table numtab purge");
      Util.trySQL(conn, "create table numtab (" +
          "col1 number(1), " +
          "col2 number(3), " +
          "col3 number(5), " +
          "col4 number(10), " +
          "col5 number(19), " +
          "col6 number(38), " +
          "col7 number(38,5), " +
          "col8 number(38,4), " +
          "col9 float, " +
          "col10 float(68), " +
          "col11 float(68))");
      Util.trySQL(conn, "insert into numtab values (1, 127, 32767, 2147483647, " +
          "9223372036854775807, 17014118346046923173168730371588410572," +
          "126.435, 32766.3453, 2147483646.34234234234, " +
          "9223372036854775807.23423423, " +
          "17014118346046923173168730371588.1231)");
      Util.trySQL(conn, "insert into numtab values (-1, -127, -32767, " +
          "-2147483647, -9223372036854775807, " +
          "-17014118346046923173168730371588410572, -126.435, " +
          "-32766.3453, -2147483646.34234234234, " +
          "-9223372036854775807.23423423, " +
          "-1701411834604692317316873037158.1231)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleNumResult = "", restNumResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      // get oracle number
      oracleRs = oracleStmt.executeQuery("select * from numtab");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          oracleSb.append(oracleRs.getObject(i) + "\n");
        }
      }
      oracleNumResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDCB Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();

      // get oracle number
      restRs = restStmt.executeQuery("select * from numtab");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          restSb.append(restRs.getObject(i) + "\n");
        }
      }
      restNumResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table numtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleNumResult, restNumResult);
  }

  /**
   * Test <code>getObject</code> on date columns.
   */
  @Category(PassTest.class)
  @Test
  public void testGetObject5() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table datetab purge");
      Util.trySQL(conn, "create table datetab (" +
          "col1 number(3), " +
          "col2 date)");
      Util.trySQL(conn, "insert into datetab values (23, '19-jul-71')");
      Util.trySQL(conn, "insert into datetab values (33, '21-sep-71')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleDateResult = "", restDateResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from datetab");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          oracleSb.append(oracleRs.getObject(i) + "\n");
        }
      }
      oracleDateResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col2 from datetab");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          restSb.append(restRs.getObject(i) + "\n");
        }
      }
      restDateResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table datetab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleDateResult, restDateResult);
  }

  /**
   * Test <code>getObject</code> on rowid columns.
   * Now the test is failing because <code>getObject</code> on a
   * rowid column is getting a null.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetObject6() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowidtab purge");
      Util.trySQL(conn, "create table rowidtab (col1 number(3), col2 rowid)");
      Util.trySQL(conn, "insert into rowidtab values (23, chartorowid('AAABBBCCCAAABBBCC1'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleDateResult = "", restDateResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from rowidtab");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          oracleSb.append(oracleRs.getObject(i));
        }
      }
      oracleDateResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col2 from rowidtab");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          restSb.append(restRs.getObject(i));
        }
      }
      restDateResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowidtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleDateResult, restDateResult);
  }

  /**
   * Test passing column name to <code>getObject</code> method.
   * Now the test is failing because <code>getObject</code> in the REST driver
   * because RAW is not supported in ORDS yet. 
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetObject7() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
      Util.trySQL(conn, "create table weirdtable (" +
          "lowercase char(5)," +
          "\"UPPERCASE\" char(15)," +
          "UPPER_CASE char(100)," +
          "UPPER#CASE char(255)," +
          "\"MixedCase\" varchar2(255)," +
          "\"underscore_name\" varchar2(255)," +
          "\"MixedCase_Underscore_Name\" raw(255)," +
          "\"Name#6\" varchar2(255)," +
          "\"7thcol\" varchar2(38)," +
          "\"special$column\" char(29)," +
          "a char(1)," +
          "\"1\" varchar2(1)," +
          "thirty_characters_012345678901 varchar2(255))");

      Util.trySQL(conn, "insert into weirdtable values ('abcde', 'ERNEST CHEN'," +
          "'BRUCE CHANG', 'ALLEN ZHAO', 'Maria Chien', " +
          "'alan_pthiesen', '000102030405', 'finit'," +
          "'le petite prince', 'tao'," +
          " 't', 'f', 'Oracle JSQL Rules!')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from weirdtable");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          String columnName = oracleRsm.getColumnName(i);
          oracleSb.append(oracleRs.getObject(columnName) + "\n");
        }
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from weirdtable");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          String columnName = restRsm.getColumnName(i);
          restSb.append(restRs.getObject(columnName) + "\n");
        }
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleResult, restResult);
  }

  /**
   * Test <code>getObject</code> method on a closed <code>ResultSet</code>.
   * Now the test is failing because exception message from REST JDBC is not expected.
   *
   * Expected: Closed Resultset: getObject(int columnIndex)
   */
  @Category(PassTest.class)
  @Test
  public void testGetObject8() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_date('2017/02/20 8:30:25', 'YYYY/MM/DD HH:MI:SS') col from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getObject(1);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_date('2017/02/20 8:30:25', 'YYYY/MM/DD HH:MI:SS') col from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getObject(1);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals("Closed Resultset: getObject(int columnIndex)", e2.getMessage());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getSQLState(), e2.getSQLState());
  }

  /**
   * Positive test for <code>getTime</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTime1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Time oracleTime = null, restTime = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_date('2017 01 15 15 24 45', 'YYYY MM DD HH24 MI SS') from dual");
      oracleRs.next();
      oracleTime = oracleRs.getTime(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_date('2017 01 15 15 24 45', 'YYYY MM DD HH24 MI SS') from dual");
      restRs.next();
      restTime = restRs.getTime(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleTime, restTime);
  }

  /**
   * Test the following index combination on <code>getTime</code> method.
   * <p>
   *   <ul>
   *     <li>zero</li>
   *     <li>negative index</li>
   *     <li>value beyond max column allowed</li>
   *   </ul>
   * </p>
   * ErrorCode for restMaxException does not match oracleMaxException
   * This is a bug in the Oracle thin driver
   */
  @Category(PassTest.class)
  @Test
  public void testGetTime2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
      Util.trySQL(conn, "create table simpletab (col1 date, col2 date, col3 date, col4 date)");
      Util.trySQL(conn, "insert into simpletab values (" +
          "TO_DATE('2003/05/03 21:59:41', 'yyyy/mm/dd hh24:mi:ss')," +
          "TO_DATE('2013/12/19 23:02:23', 'yyyy/mm/dd hh24:mi:ss')," +
          "TO_DATE('2016/09/03 17:18:31', 'yyyy/mm/dd hh24:mi:ss')," +
          "TO_DATE('2017/01/15 15:53:59', 'yyyy/mm/dd hh24:mi:ss'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleZeroException = null, restZeroException = null;
    SQLException oracleNegativeException = null, restNegativeException = null;
    SQLException oracleMaxException = null, restMaxException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();

      // zero index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getTime(0);
      } catch (SQLException e) {
        oracleZeroException = e;
      }

      // negative index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getTime(-1);
      } catch (SQLException e) {
        oracleNegativeException = e;
      }

      // max index
      oracleRs = oracleStmt.executeQuery("select * from simpletab");
      oracleRs.next();
      try {
        oracleRs.getTime(1000);
      } catch (SQLException e) {
        oracleMaxException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();

      // zero index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getTime(0);
      } catch (SQLException e) {
        restZeroException = e;
      }

      // negative index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getTime(-1);
      } catch (SQLException e) {
        restNegativeException = e;
      }

      // max index
      restRs = restStmt.executeQuery("select * from simpletab");
      restRs.next();
      try {
        restRs.getTime(1000);
      } catch (SQLException e) {
        restMaxException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleZeroException.getErrorCode(), restZeroException.getErrorCode());
    assertEquals(oracleZeroException.getMessage(), restZeroException.getMessage());
    assertEquals(oracleZeroException.getSQLState(), restZeroException.getSQLState());

    assertEquals(oracleNegativeException.getErrorCode(), restNegativeException.getErrorCode());
    assertEquals(oracleNegativeException.getMessage(), restNegativeException.getMessage());
    assertEquals(oracleNegativeException.getSQLState(), restNegativeException.getSQLState());

    
    //assertEquals(oracleMaxException.getErrorCode(), restMaxException.getErrorCode());
    System.out.println(oracleMaxException.getMessage());
    System.out.println(restMaxException.getMessage());
    assertEquals(oracleMaxException.getMessage(), restMaxException.getMessage());
    assertEquals(oracleMaxException.getSQLState(), restMaxException.getSQLState());
  }

  /**
   * Test converting an invalid string to a <code>java.sql.Time</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTime3() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;
    IllegalArgumentException oracleIllegalArgumentException = null, restIllegalArgumentException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 'non-date column' from dual");
      oracleRs.next();
      try {
        oracleRs.getTime(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      } catch (IllegalArgumentException e) {
        oracleIllegalArgumentException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'non-date column' from dual");
      restRs.next();
      try {
        restRs.getTime(1);
      } catch (SQLException e) {
        restSQLException = e;
      } catch (IllegalArgumentException e) {
        restIllegalArgumentException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(17004, restSQLException.getErrorCode());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
    assertEquals("Invalid column type: getTime", restSQLException.getMessage());
    assertEquals(oracleIllegalArgumentException, restIllegalArgumentException);
  }

  /**
   * Test converting a valid string to a <code>java.sql.Time</code> object.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTime4() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Time oracleTime = null, restTime = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select '10:12:25' from dual");
      oracleRs.next();
      oracleTime = oracleRs.getTime(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select '10:12:25' from dual");
      restRs.next();
      restTime = restRs.getTime(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs);
    close(oracleStmt);

    assertEquals(oracleTime, restTime);
  }

  /**
   * Test converting a raw column to a time object.
   * RAW is not supported in ORDS
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetTime5() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rawtab purge");
      Util.trySQL(conn, "create table rawtab(col1 raw(20), col2 raw(30))");
      Util.trySQL(conn, "insert into rawtab values ('0102030405', '0607080910')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from rawtab");
      oracleRs.next();
      try {
        oracleRs.getTime(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col2 from rawtab");
      restRs.next();
      try {
        restRs.getTime(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
    assertEquals(17004, restSQLException.getErrorCode());
    assertEquals("Invalid column type: getTime", restSQLException.getMessage());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rawtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test converting a number to a time object.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTime6() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_number(2081) from dual");
      oracleRs.next();
      try {
        oracleRs.getTime(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_number(2081) from dual");
      restRs.next();
      try {
        restRs.getTime(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
    assertEquals(17004, restSQLException.getErrorCode());
    assertEquals("Invalid column type: getTime", restSQLException.getMessage());
  }

  /**
   * Test <code>getTime</code> method on a rowid column.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetTime7() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowidtab purge");
      Util.trySQL(conn, "create table rowidtab(col1 number(3), col2 rowid)");
      Util.trySQL(conn, "insert into rowidtab select 23, rowid from dual");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleException = null, restException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from rowidtab");
      oracleRs.next();
      try {
        oracleRs.getTime(1);
      } catch (SQLException e) {
        oracleException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col2 from rowidtab");
      restRs.next();
      try {
        restRs.getTime(1);
      } catch (SQLException e) {
        restException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleException.getSQLState(), restException.getSQLState());
    assertEquals(17004, restException.getErrorCode());
    assertEquals("Invalid column type: getTime", restException.getMessage());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowidtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getTime</code> on null columns.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTime8() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
      Util.trySQL(conn, "create table nulltab (col1 number, col2 varchar2(5), col3 char(4), col4 date, col5 raw(10), col6 clob)");
      Util.trySQL(conn, "insert into nulltab values (null, null, null, null, null, null)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from nulltab");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      oracleRs.next();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        oracleSb.append(oracleRs.getTime(i));
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from nulltab");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      restRs.next();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        restSb.append(restRs.getTime(i));
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test passing allowable column name to <code>getTime</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTime9() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
      Util.trySQL(conn, "create table weirdtable (" +
          "lowercase date, " +
          "\"UPPERCASE\" date, " +
          "UPPER_CASE date, " +
          "UPPER#CASE date, " +
          "\"MixedCase\" date, " +
          "\"underscore_name\" date, " +
          "\"MixedCase_Underscore_Name\" date, " +
          "\"Name#6\" date, " +
          "\"7thcol\" date, " +
          "\"special$column\" date, " +
          "a date, " +
          "\"1\" date, " +
          "thirty_characters_012345678901 date)");
      Util.trySQL(conn, "insert into weirdtable values (to_date('01-jan-2017 18:24:00', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('02-jan-2017 11:37:49', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('03-jan-2017 09:26:11', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('04-jan-2017 15:06:58', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('05-jan-2017 03:02:11', 'dd-mm-yyyy hh:mi:ss'), " +
          "to_date('06-jan-2017 01:09:12', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('07-jan-2017 05:11:38', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('08-jan-2017 19:20:07', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('09-jan-2017 17:59:59', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('10-jan-2017 10:38:09', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('11-jan-2017 09:21:59', 'dd-mm-yyyy hh24:mi:ss'), " +
          "'12-jan-2017', '13-jan-2017')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from weirdtable");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      oracleRs.next();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = oracleRsm.getColumnName(i);
        oracleSb.append(oracleRs.getTime(columnName) + "\n");
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from weirdtable");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      restRs.next();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = restRsm.getColumnName(i);
        restSb.append(restRs.getTime(columnName) + "\n");
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getTime</code> on a closed <code>ResultSet</code>
   * Now the test is failing because the exceptions thrown are not equal.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTime10() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_date('01/15/2017 19:39:59', 'mm/dd/yyyy hh24:mi:ss') from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getTime(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run Oracle Thin Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_date('01/15/2017 19:39:59', 'mm/dd/yyyy hh24:mi:ss') from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getTime(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals("Closed Resultset: getTime(int columnIndex)", restSQLException.getMessage());
    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
  }

  /**
   * Test <code>getTime(int columnIndex, Calendar cal)</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTime11() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
      Util.trySQL(conn, "create table simpletab (col1 date)");
      Util.trySQL(conn, "insert into simpletab values (TO_DATE('2017/01/15 15:53:59', 'yyyy/mm/dd hh24:mi:ss'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Time oracleTime = null, restTime = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      oracleTime = oracleRs.getTime(1, new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      restTime = restRs.getTime(1, new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleTime, restTime);
   }

  /**
   * Test <code>getTime(int columnLabel, Calendar cal)</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTime12() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
      Util.trySQL(conn, "create table simpletab (col1 date)");
      Util.trySQL(conn, "insert into simpletab values (TO_DATE('2017/01/15 15:53:59', 'yyyy/mm/dd hh24:mi:ss'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Time oracleTime = null, restTime = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      oracleTime = oracleRs.getTime("col1", new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      restTime = restRs.getTime("col1", new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleTime, restTime);
  }

  /**
   * A positive test of <code>getTimestamp</code> method. Pass a valid index.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Timestamp oracleTs = null, restTs = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_date('2017-01-17 10:55:59', 'yyyy-mm-dd hh24:mi:ss') from dual");
      oracleRs.next();
      oracleTs = oracleRs.getTimestamp(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_date('2017-01-17 10:55:59', 'yyyy-mm-dd hh24:mi:ss') from dual");
      restRs.next();
      restTs = restRs.getTimestamp(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleTs, restTs);
  }

  /**
   * Test a date before 1970-01-01 on <code>getTimestamp</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Timestamp oracleTs = null, restTs = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_date('1969-01-17 10:55:59', 'yyyy-mm-dd hh24:mi:ss') from dual");
      oracleRs.next();
      oracleTs = oracleRs.getTimestamp(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_date('1969-01-17 10:55:59', 'yyyy-mm-dd hh24:mi:ss') from dual");
      restRs.next();
      restTs = restRs.getTimestamp(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleTs, restTs);
  }

  /**
   * Check the fractional seconds part of a <code>Timestamp</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp3() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
      Util.trySQL(conn, "create table simpletab (col1 timestamp(6))");
      Util.trySQL(conn, "insert into simpletab values (to_timestamp('17-JAN-2017 08.48.42.861031', 'dd-MM-yyyy hh24:mi:ss.FF'));");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Timestamp oracleTs = null, restTs = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      oracleTs = oracleRs.getTimestamp(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      restTs = restRs.getTimestamp(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    String oracleTsString = oracleTs.toString();
    String s1 = oracleTsString.substring(0, oracleTsString.length() - 3);
    String s2 = restTs.toString();
    assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test the following index combination on <code>getTime</code> method.
   * <p>
   *   <ul>
   *     <li>zero</li>
   *     <li>negative index</li>
   *     <li>value beyond max column allowed</li>
   *   </ul>
   * </p>
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp4() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpetab purge");
      Util.trySQL(conn, "create table simpletab (col1 timestamp(6))");
      Util.trySQL(conn, "insert into simpletab values (to_timestamp('17-JAN-2017 08.48.42.861031', 'dd-MM-yyyy hh24:mi:ss.FF'));");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleZeroSQLException = null, restZeroSQLException = null;
    SQLException oracleNegativeSQLException = null, restNegativeSQLException = null;
    SQLException oracleMaxSQLException = null, restMaxSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      // 0 index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getTimestamp(0);
      } catch (SQLException e) {
        oracleZeroSQLException = e;
      }

      // -1 index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getTimestamp(-1);
      } catch (SQLException e) {
        oracleNegativeSQLException = e;
      }

      // max index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getTimestamp(100);
      } catch (SQLException e) {
        oracleMaxSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      // 0 index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getTimestamp(0);
      } catch (SQLException e) {
        restZeroSQLException = e;
      }

      // -1 index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getTimestamp(-1);
      } catch (SQLException e) {
        restNegativeSQLException = e;
      }

      // max index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getTimestamp(100);
      } catch (SQLException e) {
        restMaxSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleZeroSQLException.getErrorCode(), restZeroSQLException.getErrorCode());
    assertEquals(oracleZeroSQLException.getMessage(), restZeroSQLException.getMessage());
    assertEquals(oracleZeroSQLException.getSQLState(), restZeroSQLException.getSQLState());

    assertEquals(oracleNegativeSQLException.getErrorCode(), restNegativeSQLException.getErrorCode());
    assertEquals(oracleNegativeSQLException.getMessage(), restNegativeSQLException.getMessage());
    assertEquals(oracleNegativeSQLException.getSQLState(), restNegativeSQLException.getSQLState());

    assertEquals(oracleMaxSQLException.getErrorCode(), restMaxSQLException.getErrorCode());
    assertEquals(oracleMaxSQLException.getMessage(), restMaxSQLException.getMessage());
    assertEquals(oracleMaxSQLException.getSQLState(), oracleMaxSQLException.getSQLState());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getTimestamp</code> on an invalid date string column.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp5() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleException = null, restException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 'non-date column' from dual");
      oracleRs.next();
      try {
        oracleRs.getTimestamp(1);
      } catch (SQLException e) {
        oracleException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'non-date column' from dual");
      restRs.next();
      try {
        restRs.getTimestamp(1);
      } catch (SQLException e) {
        restException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals("Invalid column type: getTimestamp", restException.getMessage());
  }

  /**
   * Test <code>getTimestamp</code> on a raw column.
   * Now the test is failing because the exception messages are different.
   * Not supported in the REST driver because ORDS does not support RAW types
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetTimestamp6() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rawtab purge");
      Util.trySQL(conn, "create table rawtab (col1 raw(20), col2 raw(30))");
      Util.trySQL(conn, "insert into rawtab values ('0102030405', '0607080910')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleException = null, restException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from rawtab");
      oracleRs.next();
      try {
        oracleRs.getTimestamp(1);
      } catch (SQLException e) {
        oracleException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run Oracle Thin Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col2 from rawtab");
      restRs.next();
      try {
        restRs.getTimestamp(1);
      } catch (SQLException e) {
        restException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
    assertEquals(oracleException, restException);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rawtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getTimestamp</code> on a rowid column.
   * RowID is not supported in ORDS currently
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetTimestamp7() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowidtab purge");
      Util.trySQL(conn, "create table rowidtab as select 123 as col1, rowid as col2 from dual");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from rowidtab");
      oracleRs.next();
      try {
        oracleRs.getTimestamp(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col2 from rowidtab");
      restRs.next();
      try {
        restRs.getTimestamp(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleSQLException, restSQLException);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowidtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getTimestamp</code> on an oracle number.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp8() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_number(200) from dual");
      oracleRs.next();
      try {
        oracleRs.getTimestamp(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_number(200) from dual");
      restRs.next();
      try {
        restRs.getTimestamp(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals("Invalid column type: getTimestamp", restSQLException.getMessage());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
  }

  /**
   * Test <code>getTimestamp</code> on null columns.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp9() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
      Util.trySQL(conn, "create table nulltab(col1 number, col2 varchar2(5), col3 char(4), col4 date)");
      Util.trySQL(conn, "insert into nulltab values (null, null, null, null)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from nulltab");
      oracleRs.next();
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1;i <= columnCount; i++) {
        oracleSb.append(oracleRs.getTimestamp(i));
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from nulltab");
      restRs.next();
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1;i <= columnCount; i++) {
        restSb.append(restRs.getTimestamp(i));
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getTimestamp</code> on allowable column names.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp10() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
      Util.trySQL(conn, "create table weirdtable (" +
          "lowercase date, " +
          "\"UPPERCASE\" date, " +
          "UPPER_CASE date, " +
          "UPPER#CASE date, " +
          "\"MixedCase\" date, " +
          "\"underscore_name\" date, " +
          "\"MixedCase_Underscore_Name\" date, " +
          "\"Name#6\" date, " +
          "\"7thcol\" date, " +
          "\"special$column\" date, " +
          "a date, " +
          "\"1\" date, " +
          "thirty_characters_012345678901 date)");

      Util.trySQL(conn, "insert into weirdtable values (to_date('01-jan-2017 18:24:00', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('02-jan-2017 11:37:49', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('03-jan-2017 09:26:11', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('04-jan-2017 15:06:58', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('05-jan-2017 03:02:11', 'dd-mm-yyyy hh:mi:ss'), " +
          "to_date('06-jan-2017 01:09:12', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('07-jan-2017 05:11:38', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('08-jan-2017 19:20:07', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('09-jan-2017 17:59:59', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('10-jan-2017 10:38:09', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('11-jan-2017 09:21:59', 'dd-mm-yyyy hh24:mi:ss'), " +
          "'12-jan-2017', '13-jan-2017')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from weirdtable");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      oracleRs.next();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = oracleRsm.getColumnName(i);
        oracleSb.append(oracleRs.getTimestamp(columnName)).append("\n");
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from weirdtable");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      restRs.next();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = restRsm.getColumnName(i);
        restSb.append(restRs.getTimestamp(columnName)).append("\n");
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getTimestamp</code> on a closed <code>ResultSet</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp11() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_date('1/10/2017', 'mm/dd/yyyy') from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getTimestamp(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_date('1/10/2017', 'mm/dd/yyyy') from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getTimestamp(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals("Closed Resultset: getTimestamp(int columnIndex)", restSQLException.getMessage());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
  }

  /**
   * Check <code>getTimestamp(int columnIndex, Calendar cal)</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp12() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
      Util.trySQL(conn, "create table simpletab (col1 timestamp)");
      Util.trySQL(conn, "insert into simpletab values (to_timestamp('17-JAN-2017 08.48.42.861031', 'dd-MM-yyyy hh24:mi:ss.FF'));");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Timestamp oracleTs = null, restTs = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      oracleTs = oracleRs.getTimestamp(1, new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      restTs = restRs.getTimestamp(1, new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    String oracleTsString = oracleTs.toString();
    String s1 = oracleTsString.substring(0, oracleTsString.length() - 3);
    String s2 = restTs.toString();
    assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Check <code>getTimestamp(int columnLabel, Calendar cal)</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetTimestamp13() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
      Util.trySQL(conn, "create table simpletab (col1 timestamp)");
      Util.trySQL(conn, "insert into simpletab values (to_timestamp('17-JAN-2017 08.48.42.861031', 'dd-MM-yyyy hh24:mi:ss.FF'));");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Timestamp oracleTs = null, restTs = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      oracleTs = oracleRs.getTimestamp("col1", new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      restTs = restRs.getTimestamp("col1", new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    String oracleTsString = oracleTs.toString();
    String s1 = oracleTsString.substring(0, oracleTsString.length() - 3);
    String s2 = restTs.toString();
    assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Positive test for <code>getTime</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDate1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Date oracleDate = null, restDate = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_date('2017 01 20 07 32 45', 'yyyy mm dd hh24 mi ss') from dual");
      oracleRs.next();
      oracleDate = oracleRs.getDate(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_date('2017 01 20 07 32 45', 'yyyy mm dd hh24 mi ss') from dual");
      restRs.next();
      restDate = restRs.getDate(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleDate, restDate);
  }

  /**
   * Test a date before 1970-01-01
   */
  @Category(PassTest.class)
  @Test
  public void testGetDate2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Date oracleDate = null, restDate = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_date('1969 01 20 07 32 45', 'yyyy mm dd hh24 mi ss') from dual");
      oracleRs.next();
      oracleDate = oracleRs.getDate(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_date('1969 01 20 07 32 45', 'yyyy mm dd hh24 mi ss') from dual");
      restRs.next();
      restDate = restRs.getDate(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleDate, restDate);
  }

  /**
   * Test the following index combination on <code>getDate</code> method.
   * <p>
   *   <ul>
   *     <li>zero</li>
   *     <li>negative index</li>
   *     <li>value beyond max column allowed</li>
   *   </ul>
   * </p>
   */
  @Category(PassTest.class)
  @Test
  public void testGetDate3() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpetab purge");
      Util.trySQL(conn, "create table simpletab (col1 date, col2 date, col3 date,col4 date)");
      Util.trySQL(conn, "insert into simpletab values ('1-jan-17', '2-jan-17', '3-jan-17', '4-jan-17')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleZeroSQLException = null, restZeroSQLException = null;
    SQLException oracleNegativeSQLException = null, restNegativeSQLException = null;
    SQLException oracleMaxSQLException = null, restMaxSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      // 0 index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getDate(0);
      } catch (SQLException e) {
        oracleZeroSQLException = e;
      }

      // -1 index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getDate(-1);
      } catch (SQLException e) {
        oracleNegativeSQLException = e;
      }

      // max index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getDate(100);
      } catch (SQLException e) {
        oracleMaxSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      // 0 index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getDate(0);
      } catch (SQLException e) {
        restZeroSQLException = e;
      }

      // -1 index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getDate(-1);
      } catch (SQLException e) {
        restNegativeSQLException = e;
      }

      // max index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getDate(100);
      } catch (SQLException e) {
        restMaxSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleZeroSQLException.getErrorCode(), restZeroSQLException.getErrorCode());
    assertEquals(oracleZeroSQLException.getMessage(), restZeroSQLException.getMessage());
    assertEquals(oracleZeroSQLException.getSQLState(), restZeroSQLException.getSQLState());

    assertEquals(oracleNegativeSQLException.getErrorCode(), restNegativeSQLException.getErrorCode());
    assertEquals(oracleNegativeSQLException.getMessage(), restNegativeSQLException.getMessage());
    assertEquals(oracleNegativeSQLException.getSQLState(), restNegativeSQLException.getSQLState());

    assertEquals(oracleMaxSQLException.getErrorCode(), restMaxSQLException.getErrorCode());
    assertEquals(oracleMaxSQLException.getMessage(), restMaxSQLException.getMessage());
    assertEquals(oracleMaxSQLException.getSQLState(), oracleMaxSQLException.getSQLState());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getDate</code> on an invalid date string column.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDate4() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleException = null, restException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 'non-date column' from dual");
      oracleRs.next();
      try {
        oracleRs.getDate(1);
      } catch (SQLException e) {
        oracleException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'non-date column' from dual");
      restRs.next();
      try {
        restRs.getDate(1);
      } catch (SQLException e) {
        restException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleException.getSQLState(), restException.getSQLState());
    assertEquals(17004, restException.getErrorCode());
    assertEquals("Invalid column type: getDate", restException.getMessage());
  }

  /**
   * Test <code>getDate</code> on an oracle number.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDate5() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;
    String expectedExceptionMessage = "Invalid column type: getDate";
    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_number(200) from dual");
      oracleRs.next();
      try {
        oracleRs.getDate(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_number(200) from dual");
      restRs.next();
      try {
        restRs.getDate(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals(expectedExceptionMessage, restSQLException.getMessage());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
  }

  /**
   * Test <code>getDate</code> on null columns.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDate6() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
      Util.trySQL(conn, "create table nulltab(col1 number, col2 varchar2(5), col3 char(4), col4 date)");
      Util.trySQL(conn, "insert into nulltab values (null, null, null, null)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from nulltab");
      oracleRs.next();
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1;i <= columnCount; i++) {
        oracleSb.append(oracleRs.getDate(i));
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from nulltab");
      restRs.next();
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1;i <= columnCount; i++) {
        restSb.append(restRs.getDate(i));
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getDate</code> on allowable column names.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDate7() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
      Util.trySQL(conn, "create table weirdtable (" +
          "lowercase date, " +
          "\"UPPERCASE\" date, " +
          "UPPER_CASE date, " +
          "UPPER#CASE date, " +
          "\"MixedCase\" date, " +
          "\"underscore_name\" date, " +
          "\"MixedCase_Underscore_Name\" date, " +
          "\"Name#6\" date, " +
          "\"7thcol\" date, " +
          "\"special$column\" date, " +
          "a date, " +
          "\"1\" date, " +
          "thirty_characters_012345678901 date)");

      Util.trySQL(conn, "insert into weirdtable values (to_date('01-jan-2017 18:24:00', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('02-jan-2017 11:37:49', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('03-jan-2017 09:26:11', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('04-jan-2017 15:06:58', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('05-jan-2017 03:02:11', 'dd-mm-yyyy hh:mi:ss'), " +
          "to_date('06-jan-2017 01:09:12', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('07-jan-2017 05:11:38', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('08-jan-2017 19:20:07', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('09-jan-2017 17:59:59', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('10-jan-2017 10:38:09', 'dd-mm-yyyy hh24:mi:ss'), " +
          "to_date('11-jan-2017 09:21:59', 'dd-mm-yyyy hh24:mi:ss'), " +
          "'12-jan-2017', '13-jan-2017')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from weirdtable");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      oracleRs.next();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = oracleRsm.getColumnName(i);
        oracleSb.append(oracleRs.getDate(columnName)).append("\n");
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from weirdtable");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      restRs.next();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = restRsm.getColumnName(i);
        restSb.append(restRs.getDate(columnName)).append("\n");
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getDate</code> on a closed <code>ResultSet</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDate8() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_date('1/10/2017', 'mm/dd/yyyy') from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getDate(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_date('1/10/2017', 'mm/dd/yyyy') from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getDate(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals("Closed Resultset: getDate(int columnIndex)", restSQLException.getMessage());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
  }

  /**
   * Test <code>getDate(int columnIndex, Calendar cal)</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDate9() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "create table date_tab (col1 date)");
      Util.trySQL(conn, "insert into date_tab values (to_date('January 15, 2017, 11:00 A.M.', 'Month dd, YYYY, HH:MI A.M.'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Date oracleDate = null, restDate = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from date_tab");
      oracleRs.next();
      oracleDate = oracleRs.getDate(1, new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from date_tab");
      restRs.next();
      restDate = restRs.getDate(1, new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleDate, restDate);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table date_tab");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getDate(int columnLabel, Calendar cal)</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDate10() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "create table date_tab (col1 date)");
      Util.trySQL(conn, "insert into date_tab values (to_date('January 15, 2017, 11:00 A.M.', 'Month dd, YYYY, HH:MI A.M.'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Date oracleDate = null, restDate = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from date_tab");
      oracleRs.next();
      oracleDate = oracleRs.getDate("col1", new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from date_tab");
      restRs.next();
      restDate = restRs.getDate("col1", new GregorianCalendar());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleDate, restDate);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table date_tab");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Positive test for <code>getDouble</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetDouble1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table numtab purge");
      Util.trySQL(conn, "create table numtab (" +
          "col1 number(1), " +
          "col2 number(3), " +
          "col3 number(5), " +
          "col4 number(10), " +
          "col5 number(19), " +
          "col6 number(38), " +
          "col7 number(38,5), " +
          "col8 number(38,4), " +
          "col9 float, " +
          "col10 float(68), " +
          "col11 float(68))");
      Util.trySQL(conn, "insert into numtab values (1, 127, 32767, 2147483647, " +
          " 9223372036854775807, 17014118346046923173168730371588410572," +
          " 126.435, 32766.3453, 2147483646.34234234234, " +
          " 9223372036854775807.23423423, " +
          "17014118346046923173168730371588.1231)");
      Util.trySQL(conn, "insert into numtab values (-1, -128, -32768, " +
          " -2147483648, -9223372036854775808, " +
          " -17014118346046923173168730371588410572, -126.435, " +
          " -32766.3453, -2147483646.34234234234, " +
          "-9223372036854775807.23423423, " +
          " -1701411834604692317316873037158.1231)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from numtab");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          oracleSb.append(oracleRs.getDouble(i)).append("\n");
        }
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from numtab");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          restSb.append(restRs.getDouble(i)).append("\n");
        }
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table numtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test the following index combination on <code>getDouble</code> method.
   * <p>
   *   <ul>
   *     <li>zero</li>
   *     <li>negative index</li>
   *     <li>value beyond max column allowed</li>
   *   </ul>
   * </p>
   */
  @Category(PassTest.class)
  @Test
  public void testGetDouble2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpetab purge");
      Util.trySQL(conn, "create table simpletab (col1 number(5), col2 number(6), col3 number(7),col4 number(7))");
      Util.trySQL(conn, "insert into simpletab values (2, 3, 4, 5)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleZeroSQLException = null, restZeroSQLException = null;
    SQLException oracleNegativeSQLException = null, restNegativeSQLException = null;
    SQLException oracleMaxSQLException = null, restMaxSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      // 0 index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getDouble(0);
      } catch (SQLException e) {
        oracleZeroSQLException = e;
      }

      // -1 index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getDouble(-1);
      } catch (SQLException e) {
        oracleNegativeSQLException = e;
      }

      // max index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getDouble(100);
      } catch (SQLException e) {
        oracleMaxSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      // 0 index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getDouble(0);
      } catch (SQLException e) {
        restZeroSQLException = e;
      }

      // -1 index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getDouble(-1);
      } catch (SQLException e) {
        restNegativeSQLException = e;
      }

      // max index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getDouble(100);
      } catch (SQLException e) {
        restMaxSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleZeroSQLException.getErrorCode(), restZeroSQLException.getErrorCode());
    assertEquals(oracleZeroSQLException.getMessage(), restZeroSQLException.getMessage());
    assertEquals(oracleZeroSQLException.getSQLState(), restZeroSQLException.getSQLState());

    assertEquals(oracleNegativeSQLException.getErrorCode(), restNegativeSQLException.getErrorCode());
    assertEquals(oracleNegativeSQLException.getMessage(), restNegativeSQLException.getMessage());
    assertEquals(oracleNegativeSQLException.getSQLState(), restNegativeSQLException.getSQLState());

    assertEquals(oracleMaxSQLException.getErrorCode(), restMaxSQLException.getErrorCode());
    assertEquals(oracleMaxSQLException.getMessage(), restMaxSQLException.getMessage());
    assertEquals(oracleMaxSQLException.getSQLState(), oracleMaxSQLException.getSQLState());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getDouble</code> on date columns.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDouble3() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table datetab purge");
      Util.trySQL(conn, "create table datetab (col1 number, col2 date)");
      Util.trySQL(conn, "insert into datetab values (23, '19-jan-17')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;
    String expectedErrorMessage = "Invalid column type: getDouble";
    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from datetab");
      oracleRs.next();
      try {
        oracleRs.getDouble(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col2 from datetab");
      restRs.next();
      try {
        restRs.getDouble(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(restRs, restRs);

    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals(expectedErrorMessage, restSQLException.getMessage());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table datetab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getDouble</code> on nulls.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDouble4() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
      Util.trySQL(conn, "create table nulltab(col1 number, col2 varchar2(5), col3 char(4), col4 date)");
      Util.trySQL(conn, "insert into nulltab values (null, null, null, null)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from nulltab");
      oracleRs.next();
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1;i <= columnCount; i++) {
        oracleSb.append(oracleRs.getDouble(i));
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from nulltab");
      restRs.next();
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1;i <= columnCount; i++) {
        restSb.append(restRs.getDouble(i));
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getDouble</code> on allowable column names.
   * Now the test is failing because REST JDBC is getting NPE.
   */
  @Category(PassTest.class)
  @Test
  public void testGetDouble5() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
      Util.trySQL(conn, "create table weirdtable (" +
          "lowercase number, " +
          "\"UPPERCASE\" number, " +
          "UPPER_CASE number, " +
          "UPPER#CASE number, " +
          "\"MixedCase\" number, " +
          "\"underscore_name\" number, " +
          "\"MixedCase_Underscore_Name\" number, " +
          "\"Name#6\" number, " +
          "\"7thcol\" number, " +
          "\"special$column\" number, " +
          "a number, " +
          "\"1\" number, " +
          "thirty_characters_012345678901 number)");

      Util.trySQL(conn, "insert into weirdtable values (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from weirdtable");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      oracleRs.next();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = oracleRsm.getColumnName(i);
        oracleSb.append(oracleRs.getDouble(columnName)).append("\n");
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from weirdtable");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      restRs.next();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = restRsm.getColumnName(i);
        restSb.append(restRs.getDouble(columnName)).append("\n");
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getDouble</code> on a closed <code>ResultSet</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetDouble6() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 1 from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getDouble(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 1 from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getDouble(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals("Closed Resultset: getDouble(int columnIndex)", restSQLException.getMessage());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
  }

  /**
   * Positive test for <code>getFloat</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetFloat1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table numtab purge");
      Util.trySQL(conn, "create table numtab (" +
          "col1 number(1), " +
          "col2 number(3), " +
          "col3 number(5), " +
          "col4 number(10), " +
          "col5 number(19), " +
          "col6 number(38), " +
          "col7 number(38,5), " +
          "col8 number(38,4), " +
          "col9 float, " +
          "col10 float(68), " +
          "col11 float(68))");
      Util.trySQL(conn, "insert into numtab values (1, 127, 32767, 2147483647, " +
          " 9223372036854775807, 17014118346046923173168730371588410572," +
          " 126.435, 32766.3453, 2147483646.34234234234, " +
          " 9223372036854775807.23423423, " +
          "17014118346046923173168730371588.1231)");
      Util.trySQL(conn, "insert into numtab values (-1, -128, -32768, " +
          " -2147483648, -9223372036854775808, " +
          " -17014118346046923173168730371588410572, -126.435, " +
          " -32766.3453, -2147483646.34234234234, " +
          "-9223372036854775807.23423423, " +
          " -1701411834604692317316873037158.1231)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from numtab");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      while (oracleRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          oracleSb.append(oracleRs.getFloat(i)).append("\n");
        }
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from numtab");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      while (restRs.next()) {
        for (int i = 1; i <= columnCount; i++) {
          restSb.append(restRs.getFloat(i)).append("\n");
        }
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table numtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test the following index combination on <code>getFloat</code> method.
   * <p>
   *   <ul>
   *     <li>zero</li>
   *     <li>negative index</li>
   *     <li>value beyond max column allowed</li>
   *   </ul>
   * </p>
   */
  @Category(PassTest.class)
  @Test
  public void testGetFloat2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpetab purge");
      Util.trySQL(conn, "create table simpletab (col1 number(5), col2 number(6), col3 number(7),col4 number(7))");
      Util.trySQL(conn, "insert into simpletab values (2, 3, 4, 5)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleZeroSQLException = null, restZeroSQLException = null;
    SQLException oracleNegativeSQLException = null, restNegativeSQLException = null;
    SQLException oracleMaxSQLException = null, restMaxSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      // 0 index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getFloat(0);
      } catch (SQLException e) {
        oracleZeroSQLException = e;
      }

      // -1 index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getFloat(-1);
      } catch (SQLException e) {
        oracleNegativeSQLException = e;
      }

      // max index
      oracleRs = oracleStmt.executeQuery("select col1 from simpletab");
      oracleRs.next();
      try {
        oracleRs.getFloat(100);
      } catch (SQLException e) {
        oracleMaxSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      // 0 index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getFloat(0);
      } catch (SQLException e) {
        restZeroSQLException = e;
      }

      // -1 index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getFloat(-1);
      } catch (SQLException e) {
        restNegativeSQLException = e;
      }

      // max index
      restRs = restStmt.executeQuery("select col1 from simpletab");
      restRs.next();
      try {
        restRs.getFloat(100);
      } catch (SQLException e) {
        restMaxSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleZeroSQLException.getErrorCode(), restZeroSQLException.getErrorCode());
    assertEquals(oracleZeroSQLException.getMessage(), restZeroSQLException.getMessage());
    assertEquals(oracleZeroSQLException.getSQLState(), restZeroSQLException.getSQLState());

    assertEquals(oracleNegativeSQLException.getErrorCode(), restNegativeSQLException.getErrorCode());
    assertEquals(oracleNegativeSQLException.getMessage(), restNegativeSQLException.getMessage());
    assertEquals(oracleNegativeSQLException.getSQLState(), restNegativeSQLException.getSQLState());

    assertEquals(oracleMaxSQLException.getErrorCode(), restMaxSQLException.getErrorCode());
    assertEquals(oracleMaxSQLException.getMessage(), restMaxSQLException.getMessage());
    assertEquals(oracleMaxSQLException.getSQLState(), oracleMaxSQLException.getSQLState());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table simpletab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getFloat</code> on date columns.
   */
  @Category(PassTest.class)
  @Test
  public void testGetFloat3() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table datetab purge");
      Util.trySQL(conn, "create table datetab (col1 number, col2 date)");
      Util.trySQL(conn, "insert into datetab values (23, '19-jan-17')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from datetab");
      oracleRs.next();
      try {
        oracleRs.getFloat(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col2 from datetab");
      restRs.next();
      try {
        restRs.getFloat(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(restRs, restRs);

    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals("Invalid column type: getFloat", restSQLException.getMessage());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table datetab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getFloat</code> on nulls.
   */
  @Category(PassTest.class)
  @Test
  public void testGetFloat4() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
      Util.trySQL(conn, "create table nulltab(col1 number, col2 varchar2(5), col3 char(4), col4 date)");
      Util.trySQL(conn, "insert into nulltab values (null, null, null, null)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from nulltab");
      oracleRs.next();
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1;i <= columnCount; i++) {
        oracleSb.append(oracleRs.getFloat(i));
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from nulltab");
      restRs.next();
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1;i <= columnCount; i++) {
        restSb.append(restRs.getFloat(i));
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nulltab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getFloat</code> on allowable column names.
   */
  @Category(PassTest.class)
  @Test
  public void testGetFloat5() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
      Util.trySQL(conn, "create table weirdtable (" +
          "lowercase number, " +
          "\"UPPERCASE\" number, " +
          "UPPER_CASE number, " +
          "UPPER#CASE number, " +
          "\"MixedCase\" number, " +
          "\"underscore_name\" number, " +
          "\"MixedCase_Underscore_Name\" number, " +
          "\"Name#6\" number, " +
          "\"7thcol\" number, " +
          "\"special$column\" number, " +
          "a number, " +
          "\"1\" number, " +
          "thirty_characters_012345678901 number)");

      Util.trySQL(conn, "insert into weirdtable values (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    ResultSetMetaData oracleRsm = null, restRsm = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from weirdtable");
      oracleRsm = oracleRs.getMetaData();
      int columnCount = oracleRsm.getColumnCount();
      oracleRs.next();
      StringBuilder oracleSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = oracleRsm.getColumnName(i);
        oracleSb.append(oracleRs.getFloat(columnName)).append("\n");
      }
      oracleResult = oracleSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from weirdtable");
      restRsm = restRs.getMetaData();
      int columnCount = restRsm.getColumnCount();
      restRs.next();
      StringBuilder restSb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = restRsm.getColumnName(i);
        restSb.append(restRs.getFloat(columnName)).append("\n");
      }
      restResult = restSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table weirdtable purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getFloat</code> on a closed <code>ResultSet</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetFloat6() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 1 from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getFloat(1);
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 1 from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getFloat(1);
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals("Closed Resultset: getFloat(int columnIndex)", restSQLException.getMessage());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
  }

  /**
   * Test passing column index to <code>getClob</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetClob1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table clobtab purge");
      Util.trySQL(conn, "create table clobtab (col1 clob)");
      Util.trySQL(conn, "insert into clobtab values (to_clob('this is a clob string'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from clobtab");
      oracleRs.next();
      Clob c = oracleRs.getClob(1);
      long len = c.length();
      oracleResult = c.getSubString(1L, (int)len);
      c.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from clobtab");
      restRs.next();
      Clob c = restRs.getClob(1);
      long len = c.length();
      restResult = c.getSubString(1L, (int)len);
      c.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table clobtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test passing column name to <code>getClob</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetClob2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table clobtab purge");
      Util.trySQL(conn, "create table clobtab (col1 clob)");
      Util.trySQL(conn, "insert into clobtab values (to_clob('this is a clob string'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from clobtab");
      oracleRs.next();
      Clob c = oracleRs.getClob("col1");
      long len = c.length();
      oracleResult = c.getSubString(1L, (int)len);
      c.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from clobtab");
      restRs.next();
      Clob c = restRs.getClob(1);
      long len = c.length();
      restResult = c.getSubString(1L, (int)len);
      c.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table clobtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getClob</code> on a null and an empty clob.
   * Now the test is failing because getClob(1, 0) on an empty_clob
   * is getting exception in REST JDBC.
   */
  @Category(PassTest.class)
  @Test
  public void testGetClob3() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nullclob purge");
      Util.trySQL(conn, "create table nullclob (col clob)");
      Util.trySQL(conn, "insert into nullclob values (null)");
      Util.trySQL(conn, "insert into nullclob values (empty_clob())");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    long oracleLen = 0, restLen = 0;
    StringBuilder oracleSb = new StringBuilder();
    StringBuilder restSb = new StringBuilder();
    
    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col from nullclob");
      
      while (oracleRs.next()) {
        Clob c = oracleRs.getClob(1);
        if (c != null) {
          c.getSubString(1L, (int)c.length());
          oracleLen += c.length();
          c.free();
        } else {
          oracleSb.append("null");
        }
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run Oracle Thin Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col from nullclob");
      while (restRs.next()) {
        Clob c = restRs.getClob(1);
        if (c != null) {
          c.getSubString(1L, (int)c.length());
          restLen += c.length();
          c.free();
        } else {
          restSb.append("null");
        }
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleSb.toString(), restSb.toString());
    assertEquals(oracleLen, restLen);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nullclob purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  @Category(PassTest.class)
  @Test
  public void testGetClob4() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table clobtab purge");
      Util.trySQL(conn, "create table clobtab (col clob)");
      PreparedStatement pstmt = conn.prepareStatement("insert into clobtab values (?)");
      char[] chars = new char[512];
      Arrays.fill(chars, 'f');
      String s = new String(chars);
      Clob c = conn.createClob();
      c.setString(1, s);
      pstmt.setClob(1, c);
      pstmt.execute();
      pstmt.close();
      c.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col from clobtab");
      oracleRs.next();
      Clob c = oracleRs.getClob(1);
      oracleResult = c.getSubString(1, 100000);
      c.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col from clobtab");
      restRs.next();
      Clob c = restRs.getClob(1);
      restResult = c.getSubString(1, 100000);
      c.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table clobtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getClob(int columnIndex)</code> on a closed <code>ResultSet</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetClob5() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null;
    String expectedExceptionMessage = "Closed Resultset: getClob(int columnIndex)";
    
    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_clob('abc') col from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getClob(1);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_clob('abc') col from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getClob(1);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(expectedExceptionMessage, e2.getMessage());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getSQLState(), e2.getSQLState());
  }

  /**
   * Test <code>getClob(String columnLabel)</code> on a closed <code>ResultSet</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetClob6() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select to_clob('abc') col from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getClob("abc");
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select to_clob('abc') col from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getClob("abc");
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals("Closed Resultset: getClob(String columnLabel)", e2.getMessage());
    assertEquals(e1.getSQLState(), e2.getSQLState());
  }

  @Category(PassTest.class)
  @Test
  public void testGetStatement1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Statement oracleStmtResult = null, restStmtResult = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from dual");
      oracleStmtResult = oracleRs.getStatement();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run Oracle Thin Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from dual");
      restStmtResult = restRs.getStatement();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(oracleStmt, oracleStmtResult);
    assertEquals(restStmt, restStmtResult);

    close(oracleStmt, restStmt, oracleStmtResult, restStmtResult);
  }

  /**
   * Test closing the statement using <code>getStatement</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetStatement2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    boolean oracleIsClosed = false, restIsClosed = false;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from dual");
      oracleRs.getStatement().close();
      oracleIsClosed = oracleStmt.isClosed();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from dual");
      restRs.getStatement().close();
      restIsClosed = restStmt.isClosed();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleIsClosed, restIsClosed);
  }

  /**
   * Test <code>getStatement</code> on a closed <code>ResultSet</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetStatement3() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from dual");
      oracleRs.close();
      try {
        oracleRs.getStatement();
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from dual");
      restRs.close();
      try {
        restRs.getStatement();
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals(oracleSQLException.getMessage(), restSQLException.getMessage());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
  }

  /**
   * Positive test for <code>getFetchSize</code> method.
   */
  @Category(PassTest.class)
  @Test
  public void testGetFetchSize1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table clobtab purge");
      Util.trySQL(conn, "create table clobtab (col1 number(6), col2 clob)");
      Util.trySQL(conn, "insert into clobtab values (1, to_clob('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'))");
      Util.trySQL(conn, "insert into clobtab values (2, to_clob('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb'))");
      Util.trySQL(conn, "insert into clobtab values (3, to_clob('cccccccccccccccccccccccccccccccccccccccccccccccccc'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    int oracleFetchSize1 = -1, restFetchSize1 = -1;
    int oracleFetchSize2 = -1, restFetchSize2 = -1;
    String oracleResult = "", restResult = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from clobtab");
      oracleFetchSize1 = oracleRs.getFetchSize();
      oracleRs.next();
      Clob c = oracleRs.getClob(1);
      oracleResult = c.getSubString(1, (int)c.length());
      oracleFetchSize2 = oracleRs.getFetchSize();
      c.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = oracleStmt.executeQuery("select col2 from clobtab");
      restFetchSize1 = restRs.getFetchSize();
      restRs.next();
      Clob c = restRs.getClob(1);
      restResult = c.getSubString(1, (int)c.length());
      restFetchSize2 = restRs.getFetchSize();
      c.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleFetchSize1, restFetchSize1);
    assertEquals(oracleFetchSize2, restFetchSize2);
    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table clobtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getFetchSize</code> on a closed <code>ResultSet</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetFetchSize2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleSQLException = null, restSQLException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from dual");
      oracleRs.close();
      try {
        oracleRs.getFetchSize();
      } catch (SQLException e) {
        oracleSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = oracleStmt.executeQuery("select * from dual");
      restRs.close();
      try {
        restRs.getFetchSize();
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    close(oracleRs, restRs);

    assertEquals(oracleSQLException.getErrorCode(), restSQLException.getErrorCode());
    assertEquals(oracleSQLException.getMessage(), restSQLException.getMessage());
    assertEquals(oracleSQLException.getSQLState(), restSQLException.getSQLState());
  }

  /**
   * Test <code>getURL</code> method by valid column index.
   */
  @Category(PassTest.class)
  @Test
  public void testGetURL1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    URL oracleURL = null, restURL = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 'http://www.oracle.com' col1 from dual");
      oracleRs.next();
      oracleURL = oracleRs.getURL(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'http://www.oracle.com' col1 from dual");
      restRs.next();
      restURL = restRs.getURL(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleURL, restURL);
  }

  /**
   * Test <code>getURL</code> method by valid column name.
   */
  @Category(PassTest.class)
  @Test
  public void testGetURL2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    URL oracleURL = null, restURL = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 'http://www.oracle.com' col1 from dual");
      oracleRs.next();
      oracleURL = oracleRs.getURL("col1");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'http://www.oracle.com' col1 from dual");
      restRs.next();
      restURL = restRs.getURL("col1");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleURL, restURL);
  }

  /**
   * Test <code>getURL</code> method by passing invalid column index.
   */
  @Category(PassTest.class)
  @Test
  public void testGetURL3() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null;
    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 'http://www.oracle.com' col1 from dual");
      oracleRs.next();
      try {
        oracleRs.getURL(2);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'http://www.oracle.com' col1 from dual");
      restRs.next();
      try {
        restRs.getURL(2);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(e1.getMessage(), e2.getMessage());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
  }

  /**
   * Test <code>getURL</code> method by passing invalid column name.
   */
  @Category(PassTest.class)
  @Test
  public void testGetURL4() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null;
    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 'http://www.oracle.com' col1 from dual");
      oracleRs.next();
      try {
        oracleRs.getURL("x");
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'http://www.oracle.com' col1 from dual");
      restRs.next();
      try {
        restRs.getURL("x");
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(e1.getMessage(), e2.getMessage());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
  }

  /**
   * Test <code>getURL</code> on the closed <code>ResultSet</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetURL5() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null;
    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 'http://www.oracle.com' col1 from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getURL(1);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'http://www.oracle.com' col1 from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getURL(1);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals("Closed Resultset: getURL(int columnIndex)", e2.getMessage());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
  }

  /**
   * Positive test of <code>getRowId</code> using column index.
   * Now the test is failing because RowID is not supported in ORDS currently
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetRowId1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowid_tab  purge");
      Util.trySQL(conn, "create table rowid_tab (A1 number, A2 char(50), A3 varchar2(50), A4 urowid, A5 long)");
      Util.trySQL(conn, "insert into rowid_tab values (1, 'a1charV1', 'a1vcharV1', chartorowid('AAABBBACCAAABBBCC1'), 'a1long1')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    RowId oracleRowId1 = null, oracleRowId2 = null, restRowId1 = null, restRowId2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select rowid, A4 from ROWID_Tab");
      oracleRs.next();
      oracleRowId1 = oracleRs.getRowId(1);
      oracleRowId2 = oracleRs.getRowId(2);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select rowid, A4 from ROWID_Tab");
      restRs.next();
      restRowId1 = restRs.getRowId(1);
      restRowId2 = restRs.getRowId(2);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleRowId1, restRowId1);
    assertEquals(oracleRowId2, restRowId2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowid_tab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Positive test of <code>getRowId</code> using column label.
   * Now the test is failing because RowID is not supported in ORDS yet.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetRowId2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowid_tab  purge");
      Util.trySQL(conn, "create table rowid_tab (A1 number, A2 char(50), A3 varchar2(50), A4 urowid, A5 long)");
      Util.trySQL(conn, "insert into rowid_tab values (1, 'a1charV1', 'a1vcharV1', chartorowid('AAABBBACCAAABBBCC1'), 'a1long1')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    RowId oracleRowId1 = null, oracleRowId2 = null, restRowId1 = null, restRowId2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select rowid, A4 as col from ROWID_Tab");
      oracleRs.next();
      oracleRowId1 = oracleRs.getRowId("rowid");
      oracleRowId2 = oracleRs.getRowId("col");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select rowid, A4 as col from ROWID_Tab");
      restRs.next();
      restRowId1 = restRs.getRowId("rowid");
      restRowId2 = restRs.getRowId("col");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleRowId1, restRowId1);
    assertEquals(oracleRowId2, restRowId2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowid_tab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Negative test of <code>getRowId</code> using invalid column indexes/labels.
   * Now the test is failing because REST JDBC return different error code.
   */
  @Category(PassTest.class)
  @Test
  public void testGetRowId3() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowid_tab  purge");
      Util.trySQL(conn, "create table rowid_tab (A1 number, A2 char(50), A3 varchar2(50), A4 urowid, A5 long)");
      Util.trySQL(conn, "insert into rowid_tab values (1, 'a1charV1', 'a1vcharV1', chartorowid('AAABBBACCAAABBBCC1'), 'a1long1')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleNegativeException = null, restNegativeException = null;
    SQLException oracleZeroException = null, restZeroException = null;
    SQLException oracle1000Exception = null, rest1000Exception = null;
    SQLException oracleNonExistException = null, restNonExistException = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select rowid, A1, a2, a3, a4, a5 from ROWID_Tab");
      oracleRs.next();
      try {
        oracleRs.getRowId(-1);
      } catch (SQLException e) {
        oracleNegativeException = e;
      }

      try {
        oracleRs.getRowId(0);
      } catch (SQLException e) {
        oracleZeroException = e;
      }

      try {
        oracleRs.getRowId(1000);
      } catch (SQLException e) {
        oracle1000Exception = e;
      }

      try {
        oracleRs.getRowId("B100");
      } catch (SQLException e) {
        oracleNonExistException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select rowid, a1, a2, a3 A4, a5 from ROWID_Tab");
      restRs.next();
      try {
        restRs.getRowId(-1);
      } catch (SQLException e) {
        restNegativeException = e;
      }

      try {
        restRs.getRowId(0);
      } catch (SQLException e) {
        restZeroException = e;
      }

      try {
        restRs.getRowId(1000);
      } catch (SQLException e) {
        rest1000Exception = e;
      }

      try {
        restRs.getRowId("B100");
      } catch (SQLException e) {
        restNonExistException = e;
      }

    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleNegativeException.getSQLState(), restNegativeException.getSQLState());
    assertEquals(oracleNegativeException.getErrorCode(), restNegativeException.getErrorCode());
    assertEquals(oracleNegativeException.getMessage(), restNegativeException.getMessage());

    assertEquals(oracleZeroException.getSQLState(), restZeroException.getSQLState());
    assertEquals(oracleZeroException.getErrorCode(), restZeroException.getErrorCode());
    assertEquals(oracleZeroException.getMessage(), restZeroException.getMessage());

    assertEquals(oracle1000Exception.getSQLState(), rest1000Exception.getSQLState());
    assertEquals(oracle1000Exception.getErrorCode(), rest1000Exception.getErrorCode());
    assertEquals(oracle1000Exception.getMessage(), rest1000Exception.getMessage());

    assertEquals(oracleNonExistException.getSQLState(), restNonExistException.getSQLState());
    assertEquals(oracleNonExistException.getErrorCode(), restNonExistException.getErrorCode());
    assertEquals(oracleNonExistException.getMessage(), restNonExistException.getMessage());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rowid_tab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Check various scenarios on <code>getRowId</code> on a closed <code>ResultSet</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetRowId4() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException oracleException1 = null, oracleException2 = null, oracleException3 = null, oracleException4 = null, oracleException5 = null;
    SQLException restException1 = null, restException2 = null, restException3 = null, restException4 = null, restException5 = null;

    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select chartorowid('AAABBBCCCAAABBBCC1') col1 from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getRowId(1);
      } catch (SQLException e) {
        oracleException1 = e;
      }

      try {
        oracleRs.getRowId("col1");
      } catch (SQLException e) {
        oracleException2 = e;
      }

      try {
        oracleRs.getRowId(-1);
      } catch (SQLException e) {
        oracleException3 = e;
      }

      try {
        oracleRs.getRowId("B");
      } catch (SQLException e) {
        oracleException4 = e;
      }

      try {
        oracleRs.getRowId(1000);
      } catch (SQLException e) {
        oracleException5 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select chartorowid('AAABBBCCCAAABBBCC1') col1 from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getRowId(1);
      } catch (SQLException e) {
        restException1 = e;
      }

      try {
        restRs.getRowId("col1");
      } catch (SQLException e) {
        restException2 = e;
      }

      try {
        restRs.getRowId(-1);
      } catch (SQLException e) {
        restException3 = e;
      }

      try {
        restRs.getRowId("B");
      } catch (SQLException e) {
        restException4 = e;
      }

      try {
        restRs.getRowId(1000);
      } catch (SQLException e) {
        restException5 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleException1.getSQLState(), restException1.getSQLState());
    assertEquals(oracleException1.getErrorCode(), restException1.getErrorCode());
    assertEquals("Closed Resultset: getRowId(int columnIndex)", restException1.getMessage());

    assertEquals(oracleException2.getSQLState(), restException2.getSQLState());
    assertEquals(oracleException2.getErrorCode(), restException2.getErrorCode());
    assertEquals("Closed Resultset: getRowId(String columnLabel)", restException2.getMessage());

    assertEquals(oracleException3.getSQLState(), restException3.getSQLState());
    assertEquals(oracleException3.getErrorCode(), restException3.getErrorCode());
    assertEquals("Closed Resultset: getRowId(int columnIndex)", restException3.getMessage());

    assertEquals(oracleException4.getSQLState(), restException4.getSQLState());
    assertEquals(oracleException4.getErrorCode(), restException4.getErrorCode());
    assertEquals("Closed Resultset: getRowId(String columnLabel)", restException4.getMessage());

    assertEquals(oracleException5.getSQLState(), restException5.getSQLState());
    assertEquals(oracleException5.getErrorCode(), restException5.getErrorCode());
    assertEquals("Closed Resultset: getRowId(int columnIndex)", restException5.getMessage());
  }

  /**
   * Test <code>getType</code> method.
   * REST JDBC driver only support <code>ResultSet.TYPE_FORWARD_ONLY</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetType1() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    int type = -1;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select empty_clob() from dual");
      type = rs.getType();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    assertEquals(ResultSet.TYPE_FORWARD_ONLY, type);
  }

  /**
   * Test <code>getType</code> on a closed <code>ResultSet</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetType2() {
    Connection restConn = null;
    Statement restStmt = null;
    ResultSet restRs = null;
    SQLException restSQLException = null;
    
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'xiaohe' as XIAOHE from dual");
      restRs.close();
      try {
        restRs.getType();
      } catch (SQLException e) {
        restSQLException = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(restStmt);
    assertEquals("Closed Resultset: getType()", restSQLException.getMessage());
  }

  /**
   * Test <code>findColumn</code> by passing valid columnLabel to it.
   * Now the test is failing because REST JDBC driver throws Invalid column name error.
   */
  @Category(PassTest.class)
  @Test
  public void testFindColumn1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table resultsettest_tab purge");
      StringBuilder sb = new StringBuilder("create table resultsettest_tab(");
      for (int i = 1; i <= 998; i++) {
        sb.append("col" + i + " CLOB,");
      }
      sb.append("col999 CLOB)");
      Util.trySQL(conn, sb.toString());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "oracleRs", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from resultsettest_tab");
      ResultSetMetaData rsmd = oracleRs.getMetaData();
      int columnCount = rsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = rsmd.getColumnName(i);
        sb.append(oracleRs.findColumn(columnName));
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from resultsettest_tab");
      ResultSetMetaData rsmd = restRs.getMetaData();
      int columnCount = rsmd.getColumnCount();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= columnCount; i++) {
        String columnName = rsmd.getColumnName(i);
        sb.append(restRs.findColumn(columnName));
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table resultsettest_tab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Pass null and 0-length string to <code>findColumn</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testFindColumn2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null, e3 = null, e4 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from dual");
      try {
        oracleRs.findColumn("");
      } catch (SQLException e) {
        e1 = e;
      }

      try {
        oracleRs.findColumn(null);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from dual");
      try {
        restRs.findColumn("");
      } catch (SQLException e) {
        e3 = e;
      }

      try {
        restRs.findColumn(null);
      } catch (SQLException e) {
        e4 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(e1.getSQLState(), e3.getSQLState());
    assertEquals(e1.getErrorCode(), e3.getErrorCode());
    assertEquals(e1.getMessage(), e3.getMessage());

    assertEquals(e2.getSQLState(), e4.getSQLState());
    assertEquals(e2.getErrorCode(), e4.getErrorCode());
    assertEquals(e2.getMessage(), e4.getMessage());
  }

  /**
   * Find a column not in table.
   */
  @Category(PassTest.class)
  @Test
  public void testFindColumn3() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from dual");
      try {
        oracleRs.findColumn("notintable");
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from dual");
      try {
        restRs.findColumn("notintable");
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getMessage(), e2.getMessage());
  }

  /**
   * Test <code>findColumn</code> on a closed <code>ResultSet</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testFindColumn4() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 1 as col from dual");
      oracleRs.close();
      try {
        oracleRs.findColumn("COL");
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 1 as col from dual");
      restRs.close();
      try {
        restRs.findColumn("COL");
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals("Closed Resultset: findColumn(String columnLabel)", e2.getMessage());
  }

  /**
   * Test <code>setFetchDirection</code> by passing <code>ResultSet.FETCH_FORWARD</code>
   */
  @Category(PassTest.class)
  @Test
  public void testSetFetchDirection1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    int oracleDirection = -1, restDirection = -2;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 111 col from DUAL");
      oracleRs.setFetchDirection(ResultSet.FETCH_FORWARD);
      oracleDirection = oracleRs.getFetchDirection();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 111 col from DUAL");
      restRs.setFetchDirection(ResultSet.FETCH_FORWARD);
      restDirection = restRs.getFetchDirection();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleDirection, restDirection);
  }

  /**
   * Pass some unsupported direction to <code>setFetchDirection</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testSetFetchDirection2() {
    Connection conn = null;;
    Statement stmt = null;
    ResultSet rs = null;
    SQLException e1 = null, e2 = null, e3 = null;
    int direction = -1;
    int one = -1;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select 1 as ONE from dual");

      try {
        rs.setFetchDirection(ResultSet.FETCH_REVERSE);
      } catch (SQLException e) {
        e1 = e;
      }

      try {
        rs.setFetchDirection(ResultSet.FETCH_UNKNOWN);
      } catch (SQLException e) {
        e2 = e;
      }

      try {
        rs.setFetchDirection(0);
      } catch (SQLException e) {
        e3 = e;
      }

      rs.next();
      direction = rs.getFetchDirection();
      one = rs.getInt("ONE");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(17308, e1.getErrorCode());
    assertEquals(17308, e2.getErrorCode());
    assertEquals(17308, e3.getErrorCode());
    assertEquals(ResultSet.FETCH_FORWARD, direction);
    assertEquals(1, one);
    close(rs);
  }

  /**
   * Call <code>setFetchDirection</code> on a closed <code>ResultSet</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testSetFetchDirection3() {
    Connection restConn = null;
    Statement restStmt = null;
    ResultSet restRs = null;
    SQLException e1 = null;

    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 1 as ONE from dual");
      restRs.close();
      try {
        restRs.setFetchDirection(ResultSet.FETCH_FORWARD);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(restStmt);

    assertEquals(17010, e1.getErrorCode());
    assertEquals("Closed Resultset: setFetchDirection(int direction)", e1.getMessage());
  }

  /**
   * Check the default fetch direction is <code>ResultSet.FETCH_FORWARD</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetFetchDirection1() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    int diection = -1;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select * from dual");
      diection = rs.getFetchDirection();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(ResultSet.FETCH_FORWARD, diection);

    close(rs);
    close(stmt);
  }

  /**
   * Check <code>getFetchDirection</code> on a closed <code>ResultSet</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetFetchDirection2() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    SQLException e1 = null;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select * from dual");
      rs.close();
      try {
        rs.getFetchDirection();
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals("Closed Resultset: getFetchDirection()", e1.getMessage());
    assertEquals(17010 ,e1.getErrorCode());

    close(stmt);
  }

  /**
   * Basic test for <code>getObject(int columnIndex, Class<T> type)</code>.
   *
   * Expected :xxxxx
   * Actual   :null
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetObject9() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String s1 = "s1", s2 = "s2";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 'xxxxx' as col from dual");
      oracleRs.next();
      s1 = oracleRs.getObject(1, String.class);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'xxxxx' as col from dual");
      restRs.next();
      s2 = restRs.getObject(1, String.class);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(s1, s2);

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
  }

  /**
   * Basic test for <code>getObject(String columnLabel, Class<T> type)</code>.
   *
   * Expected :xxxxx
   * Actual   :null
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetObject10() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String s1 = "s1", s2 = "s2";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 'xxxxx' as col from dual");
      oracleRs.next();
      s1 = oracleRs.getObject("col", String.class);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 'xxxxx' as col from dual");
      restRs.next();
      s2 = restRs.getObject("col", String.class);
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getObject(String columnLabel, Class<T> type) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // assertEquals(s1, s2);

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
  }

  /**
   * Basic check for <code>getObject(int columnIndex, Map<String,Class<?>> map)</code>
   *
   * Expected: java.math.BigDecimal
   */
  @Category(PassTest.class)
  @Test
  public void testGetObject11() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Object s1 = "s1", s2 = "s2";

    Map<String, Class<?>> typeMap = new java.util.HashMap<>();
    typeMap.put("NUMBER", java.math.BigDecimal.class);

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("Select 123456789 as col from dual");
      oracleRs.next();
      s1 = oracleRs.getObject(1, typeMap);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("Select 123456789 as col from dual");
      restRs.next();
      s2 = restRs.getObject(1, typeMap);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(s1.getClass(), s2.getClass());

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
  }

  /**
   * Basic check for <code>getObject(int columnIndex, Map<String,Class<?>> map)</code>
   *
   * Expected: java.math.BigDecimal
   */
  @Category(PassTest.class)
  @Test
  public void testGetObject12() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Object s1 = "s1", s2 = "s2";

    Map<String, Class<?>> typeMap = new java.util.HashMap<>();
    typeMap.put("NUMBER", java.math.BigDecimal.class);

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("Select 123456789 as col from dual");
      oracleRs.next();
      s1 = oracleRs.getObject("col", typeMap);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("Select 123456789 as col from dual");
      restRs.next();
      s2 = restRs.getObject("col", typeMap);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(s1.getClass(), s2.getClass());

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
  }

  /**
   * Test a SQL NULL returns a Java null.
   */
  @Category(PassTest.class)
  @Test
  public void testGetObject13() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Object s1 = "s1", s2 = "s2";

    Map<String, Class<?>> typeMap = new java.util.HashMap<>();
    typeMap.put("NUMBER", java.math.BigDecimal.class);

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("Select NULL as col from dual");
      oracleRs.next();
      s1 = oracleRs.getObject(1, typeMap);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("Select NULL as col from dual");
      restRs.next();
      s2 = restRs.getObject(1, typeMap);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(s1, s2);

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
  }

  /**
   * Test a SQL NULL returns a Java null.
   */
  @Category(PassTest.class)
  @Test
  public void testGetObject14() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    SQLException e1 = null, e2 = null;

    Map<String, Class<?>> typeMap = new java.util.HashMap<>();
    typeMap.put("NUMBER", String.class);

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("Select 123 as col from dual");
      oracleRs.next();
      oracleRs.close();
      try {
        oracleRs.getObject(1, typeMap);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("Select 123 as col from dual");
      restRs.next();
      restRs.close();
      try {
        restRs.getObject(1, typeMap);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals("Closed Resultset: getObject(int columnIndex, Map<String, Class<?>> map)", e2.getMessage());

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
  }

  @Override
  public void tearDown() throws Exception {
    Connection restConn = null;
    try {
      restConn = getRestConnection();
      Util.trySQL(restConn, "drop table strnumtab purge");
    } catch (SQLException | ClassNotFoundException e) {
    }
  }
}
