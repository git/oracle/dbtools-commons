/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.func;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Statement;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.jdbc.util.RestJdbcUnsupportedException;
import oracle.dbtools.jdbc.util.RestJdbcUnsupportedOperationException;
import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.PassTest;
import oracle.dbtools.unittest.categories.UnsupportedTest;

public class ResultSetTest2 extends JDBCTestCase {
  /**
   * Basic check for <code>getRef(int columnIndex)</code>
   *
   * Expected :oracle.sql.REF@979864d4
   */
  @Category(PassTest.class)
  @Test
  public void testGetRef1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table ref_tab purge");
      Util.trySQL(conn, "drop type mytype");
      Util.trySQL(conn, "create or replace type mytype as object (x int)");
      Util.trySQL(conn, "create table ref_tab of mytype");
      Util.trySQL(conn, "insert into ref_tab values (mytype(100))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Ref oracleRef = null, restRef = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select ref(a) from ref_tab a");
      oracleRs.next();
      oracleRef = oracleRs.getRef(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select ref(a) from ref_tab a");
      restRs.next();
      restRef = restRs.getRef(1);
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getRef(int columnIndex) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(oracleRef, restRef);

    //Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop type mytype");
      Util.trySQL(conn, "drop table ref_tab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Basic check for <code>getRef(String columnIndex)</code>
   *
   * Expected :oracle.sql.REF@979864d4
   */
  @Category(PassTest.class)
  @Test
  public void testGetRef2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table ref_tab purge");
      Util.trySQL(conn, "drop type mytype");
      Util.trySQL(conn, "create or replace type mytype as object (x int)");
      Util.trySQL(conn, "create table ref_tab of mytype");
      Util.trySQL(conn, "insert into ref_tab values (mytype(100))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Ref oracleRef = null, restRef = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select ref(a) as r from ref_tab a");
      oracleRs.next();
      oracleRef = oracleRs.getRef(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select ref(a) as r from ref_tab a");
      restRs.next();
      restRef = restRs.getRef("r");
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getRef(String columnLabel) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(oracleRef, restRef);

    //Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop type mytype");
      Util.trySQL(conn, "drop table ref_tab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Basic check for <code>getSQLXML(int columnIndex)</code>
   *
   * Expected String: <t>This is a XML file</t>
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetSQLXML1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table catalog_tab purge");
      Util.trySQL(conn, "create table catalog_tab(id number, catalog xmltype)");
      Util.trySQL(conn, "insert into catalog_tab values (1, xmltype('<t>This is a XML file</t>'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String xmlString1 = null, xmlString2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select catalog from catalog_tab where id = 1");
      oracleRs.next();
      SQLXML xml = oracleRs.getSQLXML(1);
      xmlString1 = xml.getString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select catalog from catalog_tab where id = 1");
      restRs.next();
      SQLXML xml = restRs.getSQLXML(1);
      xmlString2 = xml.getString();
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getSQLXML(int columnIndex) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(xmlString1, xmlString2);
  }

  /**
   * Basic check for <code>getSQLXML(String columnLabel)</code>
   *
   * Expected String: <t>This is a XML file</t>
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetSQLXML2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table catalog_tab purge");
      Util.trySQL(conn, "create table catalog_tab(id number, catalog xmltype)");
      Util.trySQL(conn, "insert into catalog_tab values (1, xmltype('<t>This is a XML file</t>'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String xmlString1 = null, xmlString2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select catalog from catalog_tab where id = 1");
      oracleRs.next();
      SQLXML xml = oracleRs.getSQLXML("catalog");
      xmlString1 = xml.getString();
      System.out.println(xmlString1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select catalog from catalog_tab where id = 1");
      restRs.next();
      SQLXML xml = restRs.getSQLXML("catalog");
      xmlString2 = xml.getString();
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getSQLXML(String columnLabel) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(xmlString1, xmlString2);
  }

  /**
   * Basic check for <code>getByte(int columnIndex)</code>
   *
   * Expected: 9
   */
  @Category(PassTest.class)
  @Test
  public void testGetByte1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    byte b1 = 0, b2 = 0;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 9 as x from dual");
      oracleRs.next();
      b1 = oracleRs.getByte(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 9 as x from dual");
      restRs.next();
      try {
        b2 = restRs.getByte(1);
      } catch (RestJdbcUnsupportedOperationException e) {
        assertEquals("Method getByte(int columnIndex) in ResultSet not supported.", e.getMessage());
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(b1, b2);
  }

  /**
   * Basic check for <code>getByte(String columnLabel)</code>
   *
   * Expected: 9
   */
  @Category(PassTest.class)
  @Test
  public void testGetByte2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    byte b1 = 0, b2 = 0;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select 9 as x from dual");
      oracleRs.next();
      b1 = oracleRs.getByte("x");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select 9 as x from dual");
      restRs.next();
      b2 = restRs.getByte("x");
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getByte(String columnLabel) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(b1, b2);
  }

  /**
   * Basic check for <code>getBytes(int columnIndex)</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetBytes1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rawtab purge");
      Util.trySQL(conn, "create table rawtab(col1 raw(20), col2 raw(20))");
      Util.trySQL(conn, "insert into rawtab values ('0102030405', '0607080910')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    byte[] bytes1 = null, bytes2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from rawtab");
      oracleRs.next();
      bytes1 = oracleRs.getBytes(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from rawtab");
      restRs.next();
      bytes2 = restRs.getBytes(1);
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getBytes(int columnIndex) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // Assert.assertArrayEquals(bytes1, bytes2);

    // Cleaup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rawtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Basic check for <code>getBytes(String columnLabel)</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetBytes2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rawtab purge");
      Util.trySQL(conn, "create table rawtab(col1 raw(20), col2 raw(20))");
      Util.trySQL(conn, "insert into rawtab values ('0102030405', '0607080910')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    byte[] bytes1 = null, bytes2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col1 from rawtab");
      oracleRs.next();
      bytes1 = oracleRs.getBytes("col1");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col1 from rawtab");
      restRs.next();
      bytes2 = restRs.getBytes("col1");
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getBytes(String columnLabel) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // Assert.assertArrayEquals(bytes1, bytes2);

    // Cleaup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table rawtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Basic check for <code>getBlob(int columnIndex)</code>
   * Now the test is failing because <code>next</code> method is getting NPE on BLOB data type.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetBlob1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table blob_tab purge");
      Util.trySQL(conn, "create table blob_tab (col Blob)");
      Util.trySQL(conn, "insert into blob_tab values ('10110110001110010101')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Blob oracleBlob = null, restBlob = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col from blob_tab");
      oracleRs.next();
      oracleBlob = oracleRs.getBlob(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col from blob_tab");
      restRs.next();
      restBlob = restRs.getBlob(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleBlob, restBlob);
  }

  /**
   * Basic check for <code>getBlob(String columnLabel)</code>.
   * Now the test is failing because <code>next</code> method is getting NPE on BLOB data type.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetBlob2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table blob_tab purge");
      Util.trySQL(conn, "create table blob_tab (col Blob)");
      Util.trySQL(conn, "insert into blob_tab values ('10110110001110010101')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    Blob oracleBlob = null, restBlob = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col from blob_tab");
      oracleRs.next();
      oracleBlob = oracleRs.getBlob("col");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col from blob_tab");
      restRs.next();
      restBlob = restRs.getBlob("col");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleBlob, restBlob);
  }

  /**
   * Read a Clob column from a Reader by using <code>getCharacterStream(int columnIndex)</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetCharacterStream1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table clob_tab purge");
      Util.trySQL(conn, "create table clob_tab(id number, c clob)");
      Util.trySQL(conn, "insert into clob_tab values (1, 'aaaaaaaaaaaaaaabbbbbbbbbbbbbbcccccccccccccc')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    int totalLen1 = 0, totalLen2 = 0;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select c from clob_tab where id = 1");
      oracleRs.next();
      java.io.Reader reader = oracleRs.getCharacterStream(1);
      char[] cbuf = new char[30];
      int len;
      try {
        while((len = reader.read(cbuf)) != -1) {
          totalLen1 += len;
        }
      } catch (IOException e) {
        fail(e.getMessage());
      } finally {
        if (reader != null) {
          try {
            reader.close();
          } catch (IOException e) {
            fail(e.getMessage());
          }
        }
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select c from clob_tab where id = 1");
      restRs.next();
      java.io.Reader reader = restRs.getCharacterStream(1);
      char[] cbuf = new char[30];
      int len;
      try {
        while((len = reader.read(cbuf)) != -1) {
          totalLen2 += len;
        }
      } catch (IOException e) {
        fail(e.getMessage());
      } finally {
        if (reader != null) {
          try {
            reader.close();
          } catch (IOException e) {
            fail(e.getMessage());
          }
        }
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(totalLen1, totalLen2);
  }

  /**
   * Read a Clob column from a Reader by using <code>getCharacterStream(String columnLabel)</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetCharacterStream2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table clob_tab purge");
      Util.trySQL(conn, "create table clob_tab(id number, c clob)");
      Util.trySQL(conn, "insert into clob_tab values (1, 'aaaaaaaaaaaaaaabbbbbbbbbbbbbbcccccccccccccc')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    int totalLen1 = 0, totalLen2 = 0;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select c from clob_tab where id = 1");
      oracleRs.next();
      java.io.Reader reader = oracleRs.getCharacterStream("c");
      char[] cbuf = new char[30];
      int len;
      try {
        while((len = reader.read(cbuf)) != -1) {
          totalLen1 += len;
        }
      } catch (IOException e) {
        fail(e.getMessage());
      } finally {
        if (reader != null) {
          try {
            reader.close();
          } catch (IOException e) {
            fail(e.getMessage());
          }
        }
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select c from clob_tab where id = 1");
      restRs.next();
      java.io.Reader reader = restRs.getCharacterStream("c");
      char[] cbuf = new char[30];
      int len;
      try {
        while((len = reader.read(cbuf)) != -1) {
          totalLen2 += len;
        }
      } catch (IOException e) {
        fail(e.getMessage());
      } finally {
        if (reader != null) {
          try {
            reader.close();
          } catch (IOException e) {
            fail(e.getMessage());
          }
        }
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(totalLen1, totalLen2);
  }

  /**
   * Test retrieving a BLOB column using <code>getBinaryStream(int columnIndex)</code>.
   * Compare the number of bytes returned.
   * Now the test is failing because <code>next</code> method on BLOB data type is getting NPE.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetBinaryString1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table testtab purge");
      Util.trySQL(conn, "create table testtab(col1 number, col2 Blob)");
      Util.doSQL(conn, "insert into testtab values (1, " +
          "'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    int total1 = 0, total2 = 0;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from testtab");
      oracleRs.next();
      InputStream oracleInputStream = oracleRs.getBinaryStream(1);
      byte[] buf = new byte[60];
      int len = -1;
      try {
        while ((len = oracleInputStream.read(buf)) != -1) {
          total1 += len;
        }
      } catch (IOException e) {
        fail(e.getMessage());
      } finally {
        if (oracleInputStream != null) {
          try {
            oracleInputStream.close();
          } catch (IOException e) {
            fail(e.getMessage());
          }
        }
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col2 from testtab");
      restRs.next();
      InputStream restInputStream = restRs.getBinaryStream(1);
      byte[] buf = new byte[60];
      int len = -1;
      try {
        while ((len = restInputStream.read(buf)) != -1) {
          total2 += len;
        }
      } catch (IOException e) {
        fail(e.getMessage());
      } finally {
        if (restInputStream != null) {
          try {
            restInputStream.close();
          } catch (IOException e) {
            fail(e.getMessage());
          }
        }
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(total1, total2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table testtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test retrieving a BLOB column using <code>getBinaryStream(String columnLabel)</code>.
   * Compare number of the bytes returned.
   * Now the test is failing because <code>next</code> method on BLOB data type is getting NPE.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetBinaryString2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table testtab purge");
      Util.trySQL(conn, "create table testtab(col1 number, col2 Blob)");
      Util.doSQL(conn, "insert into testtab values (1, " +
          "'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    int total1 = 0, total2 = 0;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select col2 from testtab");
      oracleRs.next();
      InputStream oracleInputStream = oracleRs.getBinaryStream("col2");
      byte[] buf = new byte[60];
      int len = -1;
      try {
        while ((len = oracleInputStream.read(buf)) != -1) {
          total1 += len;
        }
      } catch (IOException e) {
        fail(e.getMessage());
      } finally {
        if (oracleInputStream != null) {
          try {
            oracleInputStream.close();
          } catch (IOException e) {
            fail(e.getMessage());
          }
        }
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col2 from testtab");
      restRs.next();
      InputStream restInputStream = restRs.getBinaryStream("col2");
      byte[] buf = new byte[60];
      int len = -1;
      try {
        while ((len = restInputStream.read(buf)) != -1) {
          total2 += len;
        }
      } catch (IOException e) {
        fail(e.getMessage());
      } finally {
        if (restInputStream != null) {
          try {
            restInputStream.close();
          } catch (IOException e) {
            fail(e.getMessage());
          }
        }
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(total1, total2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table testtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  @Category(PassTest.class)
  @Test
  public void testGetAsciiStream1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table alltable purge");
      Util.trySQL(conn, "create table alltable(col1 char(5), col2 varchar2(255), col3 CLOB)");
      Util.trySQL(conn, "insert into alltable values('abc', 'Oracle Corp', 'This is a string in a clob column')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String s1 = "s1", s2 = "s2";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from alltable");
      oracleRs.next();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= 3; i++) {
        InputStream inputStream = oracleRs.getAsciiStream(i);
        int c;
        try {
          while ((c = inputStream.read()) != -1) {
            sb.append(c);
          }
        } catch (IOException e) {
          fail(e.getMessage());
        } finally {
          if (inputStream != null) {
            try {
              inputStream.close();
            } catch (IOException e) {
              fail(e.getMessage());
            }
          }
        }
      }
      s1 = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from alltable");
      restRs.next();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= 3; i++) {
        InputStream inputStream = restRs.getAsciiStream(i);
        int c;
        try {
          while ((c = inputStream.read()) != -1) {
            sb.append(c);
          }
        } catch (IOException e) {
          fail(e.getMessage());
        } finally {
          if (inputStream != null) {
            try {
              inputStream.close();
            } catch (IOException e) {
              fail(e.getMessage());
            }
          }
        }
      }
      s2 = sb.toString();
    } catch (RestJdbcNotImplementedException e) {
      assertEquals("Method getAsciiStream(int columnIndex) in ResultSet not implemented yet.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table alltable purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  @Category(PassTest.class)
  @Test
  public void testGetAsciiStream2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table alltable purge");
      Util.trySQL(conn, "create table alltable(col1 char(5), col2 varchar2(255), col3 CLOB)");
      Util.trySQL(conn, "insert into alltable values('abc', 'Oracle Corp', 'This is a string in a clob column')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String s1 = "s1", s2 = "s2";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select * from alltable");
      oracleRs.next();
      StringBuilder sb = new StringBuilder();
      ResultSetMetaData rsmd = oracleRs.getMetaData();
      for (int i = 1; i <= 3; i++) {
        InputStream inputStream = oracleRs.getAsciiStream(rsmd.getColumnName(i));
        int c;
        try {
          while ((c = inputStream.read()) != -1) {
            sb.append(c);
          }
        } catch (IOException e) {
          fail(e.getMessage());
        } finally {
          if (inputStream != null) {
            try {
              inputStream.close();
            } catch (IOException e) {
              fail(e.getMessage());
            }
          }
        }
      }
      s1 = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select * from alltable");
      restRs.next();
      StringBuilder sb = new StringBuilder();
      ResultSetMetaData rsmd = restRs.getMetaData();
      for (int i = 1; i <= 3; i++) {
        InputStream inputStream = restRs.getAsciiStream(rsmd.getColumnName(i));
        int c;
        try {
          while ((c = inputStream.read()) != -1) {
            sb.append(c);
          }
        } catch (IOException e) {
          fail(e.getMessage());
        } finally {
          if (inputStream != null) {
            try {
              inputStream.close();
            } catch (IOException e) {
              fail(e.getMessage());
            }
          }
        }
      }
      s2 = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table alltable purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test retieving nchar, nvarchar2 and nclob using <code>getNCharacterStream(int columnIndex)</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetNCharacterStream1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table ntab purge");
      Util.trySQL(conn, "create table ntab(c1 NChar(80), c2 nvarchar2(80), c3 NCLOB)");
      Util.trySQL(conn, "INSERT INTO ntab VALUES (to_nchar('this is nchar'), " +
          "to_nchar('this is nvarchar'), " +
          "to_nclob('this is nclob') )");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select c1, c2, c3 from ntab");
      oracleRs.next();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= 3; i++) {
        Reader reader = oracleRs.getNCharacterStream(i);
        char[] cbuf = new char[100];
        try {
          int length = reader.read(cbuf);
          sb.append(new String(cbuf, 0, length));
        } catch (IOException e) {
          fail(e.getMessage());
        } finally {
          if (reader != null) {
            try {
              reader.close();
            } catch (IOException e) {
              fail(e.getMessage());
            }
          }
        }
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select c1, c2, c3 from ntab");
      restRs.next();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= 3; i++) {
        Reader reader = restRs.getNCharacterStream(i);
        char[] cbuf = new char[100];
        try {
          int length = reader.read(cbuf);
          sb.append(new String(cbuf, 0, length));
        } catch (IOException e) {
          fail(e.getMessage());
        } finally {
          if (reader != null) {
            try {
              reader.close();
            } catch (IOException e) {
              fail(e.getMessage());
            }
          }
        }
      }
      restResult = sb.toString();
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getNCharacterStream(int columnIndex) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(oracleResult, restResult);

    // Cleaup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table ntab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test retieving nchar, nvarchar2 and nclob using <code>getNCharacterStream(String columnLabel)</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetNCharacterStream2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table ntab purge");
      Util.trySQL(conn, "create table ntab(c1 NChar(80), c2 nvarchar2(80), c3 NCLOB)");
      Util.trySQL(conn, "INSERT INTO ntab VALUES (to_nchar('this is nchar'), " +
          "to_nchar('this is nvarchar'), " +
          "to_nclob('this is nclob') )");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select c1, c2, c3 from ntab");
      oracleRs.next();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= 3; i++) {
        Reader reader = oracleRs.getNCharacterStream("c" + i);
        char[] cbuf = new char[100];
        try {
          int length = reader.read(cbuf);
          sb.append(new String(cbuf, 0, length));
        } catch (IOException e) {
          fail(e.getMessage());
        } finally {
          if (reader != null) {
            try {
              reader.close();
            } catch (IOException e) {
              fail(e.getMessage());
            }
          }
        }
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select c1, c2, c3 from ntab");
      restRs.next();
      StringBuilder sb = new StringBuilder();
      for (int i = 1; i <= 3; i++) {
        Reader reader = restRs.getNCharacterStream("c" + i);
        char[] cbuf = new char[100];
        try {
          int length = reader.read(cbuf);
          sb.append(new String(cbuf, 0, length));
        } catch (IOException e) {
          fail(e.getMessage());
        } finally {
          if (reader != null) {
            try {
              reader.close();
            } catch (IOException e) {
              fail(e.getMessage());
            }
          }
        }
      }
      restResult = sb.toString();
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getNCharacterStream(String columnLabel) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(oracleResult, restResult);

    // Cleaup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table ntab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test retrieving NCLOB column using <code>getNClob(int columnIndex)</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetNClob1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nclobtab purge");
      Util.trySQL(conn, "create table nclobtab(c NCLOB)");
      Util.trySQL(conn, "insert into nclobtab values (to_nclob('this is nclob'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String s1 = "s1", s2 = "s2";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select c from nclobtab");
      oracleRs.next();
      NClob nClob = oracleRs.getNClob(1);
      int len = (int)(nClob.length());
      s1 = nClob.getSubString(1, len);
      nClob.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select c from nclobtab");
      restRs.next();
      NClob nClob = restRs.getNClob(1);
      int len = (int)(nClob.length());
      s2 = nClob.getSubString(1, len);
      nClob.free();
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getNClob(int columnIndex) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(restStmt, oracleStmt);

    // assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nclobtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test retrieving NCLOB column using <code>getNClob(String columnLabel)</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetNClob2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nclobtab purge");
      Util.trySQL(conn, "create table nclobtab(c NCLOB)");
      Util.trySQL(conn, "insert into nclobtab values (to_nclob('this is nclob'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String s1 = "s1", s2 = "s2";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select c from nclobtab");
      oracleRs.next();
      NClob nClob = oracleRs.getNClob("c");
      int len = (int)(nClob.length());
      s1 = nClob.getSubString(1, len);
      nClob.free();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select c from nclobtab");
      restRs.next();
      NClob nClob = restRs.getNClob("c");
      int len = (int)(nClob.length());
      s2 = nClob.getSubString(1, len);
      nClob.free();
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getNClob(String columnLabel) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(restStmt, oracleStmt);

    // assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nclobtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test retrieving NCHAR column using <code>getNString(int columnIndex)</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetNString1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nchartab purge");
      Util.trySQL(conn, "create table nchartab(c NCHAR(80))");
      Util.trySQL(conn, "insert into nchartab values (to_nchar('this is nchar 8888888'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String s1 = "s1", s2 = "s2";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select c from nchartab");
      oracleRs.next();
      s1 = oracleRs.getNString(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select c from nchartab");
      restRs.next();
      s2 = restRs.getNString(1);
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getNString(int columnIndex) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(restStmt, oracleStmt);

    // assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nchartab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test retrieving NCHAR column using <code>getNString(String columnLabel)</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetNString2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nchartab purge");
      Util.trySQL(conn, "create table nchartab(c NCHAR(80))");
      Util.trySQL(conn, "insert into nchartab values (to_nchar('this is nchar 333333'))");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String s1 = "s1", s2 = "s2";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select c from nchartab");
      oracleRs.next();
      s1 = oracleRs.getNString("c");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select c from nchartab");
      restRs.next();
      s2 = restRs.getNString("c");
    } catch (RestJdbcUnsupportedOperationException e) {
      assertEquals("Method getNString(String columnLabel) in ResultSet not supported.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(restStmt, oracleStmt);

    // assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table nchartab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }
}
