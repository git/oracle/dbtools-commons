/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.func;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.FailTest;
import oracle.dbtools.unittest.categories.PassTest;
import oracle.dbtools.unittest.categories.UnsupportedTest;

public class StatementTest extends JDBCTestCase {
  /**
   * Create statementtest_emp and statementtest_dept tables so some tests can just use them without having to create tables.
   * These two tables only used for queries. No update will be performed on them.
   */
  @Override
  protected void setUp() {
    Connection conn = null;
    Statement stmt = null;
    String forError="CreateTable Attempted:"+System.getProperty("num")+"Z:";
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      SQLException throwFirst=null;
      try {
          stmt.execute("CREATE TABLE STATEMENTTEST_DEPT (" +
              "  DEPTNO NUMBER(2) CONSTRAINT PK_DEPT PRIMARY KEY," +
              "  DNAME VARCHAR2(14)," +
              "  LOC VARCHAR2(13)" +
              ")");
      } catch (SQLException e) {
          throwFirst=null;
          e.printStackTrace();
      }
      try {
          stmt.execute("CREATE TABLE STATEMENTTEST_EMP (" +
              "  EMPNO NUMBER(4) CONSTRAINT PK_EMP PRIMARY KEY," +
              "  ENAME VARCHAR2(10)," +
              "  JOB VARCHAR2(9)," +
              "  MGR NUMBER(4)," +
              "  HIREDATE DATE," +
              "  SAL NUMBER(7,2)," +
              "  COMM NUMBER(7,2)," +
              "  DEPTNO NUMBER(2) CONSTRAINT FK_DEPTNO REFERENCES STATEMENTTEST_DEPT" +
              ")");
      } catch(SQLException e) {
          if (throwFirst==null) {
              throwFirst=e;
          }
          e.printStackTrace();
          throw throwFirst;
      }
      forError+=" CreateSucceeded";
      stmt.execute("INSERT INTO STATEMENTTEST_DEPT VALUES (10,'ACCOUNTING','NEW YORK')");
      stmt.execute("INSERT INTO STATEMENTTEST_DEPT VALUES (20,'RESEARCH','DALLAS')");
      stmt.execute("INSERT INTO STATEMENTTEST_DEPT VALUES (30,'SALES','CHICAGO')");
      stmt.execute("INSERT INTO STATEMENTTEST_DEPT VALUES (40,'OPERATIONS','BOSTON')");

      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7369,'SMITH','CLERK',7902,to_date('17-12-1980','dd-mm-yyyy'),800,NULL,20)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7499,'ALLEN','SALESMAN',7698,to_date('20-2-1981','dd-mm-yyyy'),1600,300,30)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7521,'WARD','SALESMAN',7698,to_date('22-2-1981','dd-mm-yyyy'),1250,500,30)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7566,'JONES','MANAGER',7839,to_date('2-4-1981','dd-mm-yyyy'),2975,NULL,20)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7654,'MARTIN','SALESMAN',7698,to_date('28-9-1981','dd-mm-yyyy'),1250,1400,30)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7698,'BLAKE','MANAGER',7839,to_date('1-5-1981','dd-mm-yyyy'),2850,NULL,30)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7782,'CLARK','MANAGER',7839,to_date('9-6-1981','dd-mm-yyyy'),2450,NULL,10)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7788,'SCOTT','ANALYST',7566,to_date('13-JUL-87','dd-mm-rr')-85,3000,NULL,20)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7839,'KING','PRESIDENT',NULL,to_date('17-11-1981','dd-mm-yyyy'),5000,NULL,10)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7844,'TURNER','SALESMAN',7698,to_date('8-9-1981','dd-mm-yyyy'),1500,0,30)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7876,'ADAMS','CLERK',7788,to_date('13-JUL-87', 'dd-mm-rr')-51,1100,NULL,20)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7900,'JAMES','CLERK',7698,to_date('3-12-1981','dd-mm-yyyy'),950,NULL,30)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7902,'FORD','ANALYST',7566,to_date('3-12-1981','dd-mm-yyyy'),3000,NULL,20)");
      stmt.execute("INSERT INTO STATEMENTTEST_EMP VALUES (7934,'MILLER','CLERK',7782,to_date('23-1-1982','dd-mm-yyyy'),1300,NULL,10)");
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
        System.err.println("Debug: "+forError);
    }
    close(stmt);
  }

  /**
   * Test executing a normal statement
   */
  @Category(PassTest.class)
  @Test
  public void testExecuteQuery() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String dummy = "";
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select * from dual");
      rs.next();
      dummy = rs.getString(1);
      close(rs);
      close(stmt);
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    assertEquals("X", dummy);
  }

  /**
   * Test passing a null statement to executeQuery.
   * A 17104 errorCode is expected, now it is failing with a 500 status code.
   */
  @Category(PassTest.class)
  @Test
  public void testExecuteQueryNull() {
    Connection conn = null;
    Statement stmt = null;
    int errorCode = -1;
    int expected = 17104;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.executeQuery(null);
    } catch (SQLException e) {
      errorCode = e.getErrorCode();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    close(stmt);

    assertEquals(expected, errorCode);
  }

  /**
   * Test passing an empty String to executeQuery.
   * A 17104 errorCode is expected. 
   * Error Message expected: SQL statement to execute cannot be empty or null
   */
  @Category(PassTest.class)
  @Test
  public void testExecuteQueryEmpty() {
    Connection conn = null;
    Statement stmt = null;
    int errorCode = -1;
    int expected = 17104;
    String expectedErrorMsg = "SQL statement to execute cannot be empty or null";
    String actualErrorMsg = null;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.executeQuery("");
    } catch (SQLException e) {
      errorCode = e.getErrorCode();
      actualErrorMsg = e.getMessage();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    close(stmt);

    assertEquals(expected, errorCode);
    assertEquals(expectedErrorMsg,actualErrorMsg);
  }

  /**
   * Test executing a null statement
   */
  @Category(PassTest.class)
  @Test
  public void testExecuteNull() {
    Connection conn = null;
    Statement stmt = null;
    int errorCode = -1;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute(null);
    } catch (SQLException e) {
      errorCode = e.getErrorCode();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    close(stmt);

    assertEquals(17104, errorCode);
  }

  /**
   * Test passing an empty String to execute
   * ORA-17104 is expected.
   */
  @Category(PassTest.class)
  @Test
  public void testExecuteEmpty() {
    Connection conn = null;
    Statement stmt = null;
    int errorCode = -1;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute("");
    } catch (SQLException e) {
      errorCode = e.getErrorCode();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }

    close(stmt);

    assertEquals(17104, errorCode);
  }

  /**
   * Create and close a statement 1000 times
   */
  @Category(PassTest.class)
  @Test
  public void testCreateAndCloseStatement() {
    Connection restConnection;
    int count = 0;
    try {
      restConnection = getRestConnection();
      for (int i = 1; i <= 1000; i++) {
        restConnection.createStatement().close();
        if (i % 100 == 0) {
          count++;
        }
      }
    } catch (SQLException ex) {
    } catch (ClassNotFoundException ex) {
    }
    assertEquals(count, 10);
  }

  @Category(PassTest.class)
  @Test
  public void testExecute() {
    Connection restConnection = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected = "SMITH\n" +
        "ALLEN\n" +
        "WARD\n" +
        "JONES\n" +
        "MARTIN\n" +
        "BLAKE\n" +
        "CLARK\n" +
        "SCOTT\n" +
        "KING\n" +
        "TURNER\n" +
        "ADAMS\n" +
        "JAMES\n" +
        "FORD\n" +
        "MILLER\n";
    StringBuilder sb = new StringBuilder();
    try {
      restConnection = getRestConnection();
      stmt = restConnection.createStatement();
      stmt.execute("CREATE TABLE EMP " +
          "(EMPNO NUMBER(4) NOT NULL, " +
          "ENAME VARCHAR2(10), " +
          "JOB VARCHAR2(9), " +
          "MGR NUMBER(4), " +
          "HIREDATE DATE, " +
          "SAL NUMBER(7,2), " +
          "COMM NUMBER(7,2), " +
          "DEPTNO NUMBER(2) NOT NULL)");
      stmt.execute("CREATE TABLE DEPT " +
          "(DEPTNO NUMBER(2) NOT NULL, " +
          " DNAME VARCHAR2(14) , " +
          " LOC VARCHAR2(13))");
      stmt.execute("INSERT INTO EMP VALUES (7369,'SMITH','CLERK',7902," +
          "TO_DATE('17-DEC-1980', 'DD-MON-YYYY'),800,NULL,20)");
      stmt.execute("INSERT INTO EMP VALUES (7499,'ALLEN','SALESMAN',7698, " +
          "TO_DATE('20-FEB-1981', 'DD-MON-YYYY'),1600,300,30)");
      stmt.execute("INSERT INTO EMP VALUES (7521,'WARD','SALESMAN',7698, " +
          "TO_DATE('22-FEB-1981', 'DD-MON-YYYY'),1250,500,30)");
      stmt.execute("INSERT INTO EMP VALUES (7566,'JONES','MANAGER',7839, " +
          "TO_DATE('2-APR-1981', 'DD-MON-YYYY'),2975,NULL,20)");
      stmt.execute("INSERT INTO EMP VALUES (7654,'MARTIN','SALESMAN',7698, " +
          "TO_DATE('28-SEP-1981', 'DD-MON-YYYY'),1250,1400,30)");
      stmt.execute("INSERT INTO EMP VALUES (7698,'BLAKE','MANAGER',7839, " +
          "TO_DATE('1-MAY-1981', 'DD-MON-YYYY'),2850,NULL,30)");
      stmt.execute("INSERT INTO EMP VALUES (7782,'CLARK','MANAGER',7839, " +
          "TO_DATE('9-JUN-1981', 'DD-MON-YYYY'),2450,NULL,10)");
      stmt.execute("INSERT INTO EMP VALUES (7788,'SCOTT','ANALYST',7566, " +
          "TO_DATE('9-DEC-1982', 'DD-MON-YYYY'), 3000,NULL,20)");
      stmt.execute("INSERT INTO EMP VALUES (7839,'KING','PRESIDENT',NULL, " +
          "TO_DATE('17-NOV-1981', 'DD-MON-YYYY'),5000,NULL,10)");
      stmt.execute("INSERT INTO EMP VALUES (7844,'TURNER','SALESMAN',7698, " +
          "TO_DATE('8-SEP-1981', 'DD-MON-YYYY'),1500,0,30)");
      stmt.execute("INSERT INTO EMP VALUES (7876,'ADAMS','CLERK',7788, " +
          "TO_DATE('12-JAN-1983', 'DD-MON-YYYY'), 1100,NULL,20)");
      stmt.execute("INSERT INTO EMP VALUES (7900,'JAMES','CLERK',7698, " +
          "TO_DATE('3-DEC-1981', 'DD-MON-YYYY'),950,NULL,30)");
      stmt.execute("INSERT INTO EMP VALUES (7902,'FORD','ANALYST',7566, " +
          "TO_DATE('3-DEC-1981', 'DD-MON-YYYY'),3000,NULL,20)");
      stmt.execute("INSERT INTO EMP VALUES (7934,'MILLER','CLERK',7782, " +
          "TO_DATE('23-JAN-1982', 'DD-MON-YYYY'),1300,NULL,10)");

      stmt.execute("INSERT INTO DEPT VALUES (10,'ACCOUNTING','NEW YORK')");
      stmt.execute("INSERT INTO DEPT VALUES (20,'RESEARCH','DALLAS')");
      stmt.execute("INSERT INTO DEPT VALUES (30,'SALES','CHICAGO')");
      stmt.execute("INSERT INTO DEPT VALUES (40,'OPERATIONS','BOSTON')");

      rs = stmt.executeQuery("select ename, empno from emp order by empno");
      while (rs.next()) {
        sb.append(rs.getString(1) + "\n");
      }
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      try {
        stmt.execute("drop table emp purge");
        stmt.execute("drop table dept purge");
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

    assertEquals(expected, sb.toString());
    close(rs);
    close(stmt);
  }

  /**
   * Test adding columns to a table.
   * The test is failing since the time part is different from that output from the thin driver: 1997-09-03 00:00:00.0 vs 1997-09-03T07:00:00Z
   * Test passes now. 
   */
  @Category(PassTest.class)
  @Test
  public void testAlterTable() {
    Connection connection = null;
    Statement statement = null;
    ResultSet rs = null;
    StringBuilder sb = new StringBuilder();
    String expected = "COL1 is New string in VARCHAR2 col1\n" +
        "COL2 is 12345.67\n" +
        "COL3 is here is string in VARCHAR col3\n" +
        "COL4 is 1997-09-03 00:00:00.0\n" +
        "COL5 is Here is some text in LONG col";
    try {
      connection = getRestConnection();
      statement = connection.createStatement();
      statement.execute("create table a234567890b234567890c234567890(col1 varchar2(30))");
      statement.execute("ALTER TABLE a234567890b234567890c234567890 " +
          "ADD (col2 NUMBER(7,2), col3 VARCHAR(30), col4 DATE, col5 LONG)");
      statement.execute("insert into a234567890b234567890c234567890 " +
          "values ('New string in VARCHAR2 col1', 12345.67, " +
          "'here is string in VARCHAR col3'," +
          "'03-SEP-97', 'Here is some text in LONG col')");
      rs = statement.executeQuery("select * from a234567890b234567890c234567890");
      ResultSetMetaData rsm = rs.getMetaData();
      while (rs.next()) {
        for (int i = 1; i <= rsm.getColumnCount(); i++) {
          if (i != 5) {
            sb.append(rsm.getColumnName(i) + " is " + rs.getString(i) + "\n");
          } else {
            sb.append(rsm.getColumnName(i) + " is " + rs.getString(i));
          }
        }
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
    } catch (ClassNotFoundException ex) {
      ex.printStackTrace();
    } finally {
      try {
        statement.execute("DROP TABLE a234567890b234567890c234567890 purge");
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

    assertEquals(expected, sb.toString());
  }

  @Category(PassTest.class)
  @Test
  public void testCreateAndDropView() {
    Connection connection = null;
    Statement statement = null;
    ResultSet rs = null;
    ResultSetMetaData rsm = null;

    StringBuilder sb = new StringBuilder();
    String expected = "PLAYER is Michael Jordan\n" +
        "BIRTHDAY is 1973-09-03 00:00:00.0\n";
    int errorCode = -1;

    try {
      connection = getRestConnection();

      //setup
      Util.doSQL(connection, "create table a234567890b234567890c234567890 (player varchar2(30), num_1 varchar(3), height number(5, 2))");
      Util.doSQL(connection, "insert into a234567890b234567890c234567890 values ('Michael Jordan', '023', 6.9)");
      Util.doSQL(connection, "create table a234567890b234567890c234567891 (num_2 varchar(3), birthday date)");
      Util.doSQL(connection, "insert into a234567890b234567890c234567891 values ('023', '03-SEP-73')");
      //create view
      statement = connection.createStatement();
      //sometimes the view exists -> change to delete in advance
      try {
          statement.execute("DROP view view_playersbirthdayfromname");
          System.err.println("Warning DROP view view_playersbirthdayfromname succeeded when no view expected");
      } catch (SQLException e) {
          //ignore so at least create view is tested as before
      }
      
      statement.execute("create view view_playersbirthdayfromname as " +
          "select player, birthday " +
          "from a234567890b234567890c234567890, " +
          "a234567890b234567890c234567891 " +
          "where a234567890b234567890c234567890.num_1" +
          " = a234567890b234567890c234567891.num_2");

      rs = statement.executeQuery("select * from view_playersbirthdayfromname");
      rsm = rs.getMetaData();
      while (rs.next()) {
        for (int i = 1; i <= rsm.getColumnCount(); i++) {
          sb.append(rsm.getColumnName(i) + " is " + rs.getString(i) + "\n");
        }
      }

      //drop view
      statement.execute("DROP view view_playersbirthdayfromname");
      try {
        statement.executeQuery("select * from view_playersbirthdayfromname");
      } catch (SQLException e) {
        errorCode = e.getErrorCode();
      }
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      //cleanup
      Util.trySQL(connection, "drop table a234567890b234567890c234567890");
      Util.trySQL(connection, "drop table a234567890b234567890c234567891");
      close(statement);
    }
    
    assertEquals(expected, sb.toString());
    assertEquals(942, errorCode);
  }

  /**
   * Test setFetchDirection and getFetchDirection
   * setFetchDirection(ResultSet.FETCH_REVERSE) is not working, getFetchDirection is getting ResultSet.FETCH_FORWARD instead.
   * setFetchDirection only sets a hint for the driver
   */
  @Category(PassTest.class)
  @Test
  public void testFetchDirection() {
    Connection conn = null;
    Statement stmt = null;
    //fetch direction
    int fetchDirection = -1;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      fetchDirection = stmt.getFetchDirection();
      assertEquals(ResultSet.FETCH_FORWARD, fetchDirection);

      stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
      fetchDirection = stmt.getFetchDirection();
      assertEquals(ResultSet.FETCH_FORWARD, fetchDirection);

      stmt.setFetchDirection(ResultSet.FETCH_REVERSE);
      fetchDirection = stmt.getFetchDirection();
      assertEquals(ResultSet.FETCH_FORWARD, fetchDirection);

      stmt.setFetchDirection(ResultSet.FETCH_UNKNOWN);
      fetchDirection = stmt.getFetchDirection();
      assertEquals(ResultSet.FETCH_FORWARD, fetchDirection);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(stmt);
  }

  /**
   * Test using execute method to create a synonym and drop a synonym.
   * After dropping the synonym, select * from public_synonym should get ORA-00942,
   * so the the errorCode should be 942. The test is failing since e.getErrorCode == 0
   */
  @Category(PassTest.class)
  @Test
  public void testCreateAndDropSynonym() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    ResultSetMetaData rsm = null;
    String expected = "PLAYER is Mike Tiger\n" +
        "NUM is 023\n" +
        "HEIGHT is 6.3\n";
    StringBuilder actual = new StringBuilder();
    int errorCode = -1;
    try {
      conn = getRestConnection();

      // setup
      Util.doSQL(conn, "create table test_synonym (player varchar2(30), num varchar(3), height number(5,2))");

      stmt = conn.createStatement();
      stmt.execute("create synonym public_synonym for test_synonym");
      stmt.execute("insert into test_synonym (player, num, height) values ('Mike Tiger', '023', 6.3)");

      rs = stmt.executeQuery("select * from public_synonym");
      rsm = rs.getMetaData();
      while (rs.next()) {
        for (int i = 1; i <= rsm.getColumnCount(); i++) {
          actual.append(rsm.getColumnName(i) + " is " + rs.getString(i) + "\n");
        }
      }

      //drop the synonym
      stmt.execute("drop SYNONYM public_synonym");
      stmt.execute("select * from public_synonym");
    } catch (SQLException e) {
      errorCode = e.getErrorCode();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      Util.trySQL(conn, "drop table test_synonym");
      Util.trySQL(conn, "drop SYNONYM public_synonym");
    }
    close(rs);
    close(stmt);

    assertEquals(expected, actual.toString());
    assertEquals(942, errorCode);
  }

  @Category(PassTest.class)
  @Test
  public void testMaxRows() {
    Connection conn = null;
    Statement stmt = null;
    int maxRows = -2;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      maxRows = stmt.getMaxRows();
      System.out.println("Default maxRows: " + maxRows);

      stmt.setMaxRows(1);
      maxRows = stmt.getMaxRows();
      System.out.println("maxRows should be 1: " + maxRows);

      stmt.setMaxRows(0);
      maxRows = stmt.getMaxRows();
      System.out.println("maxRows should be 0: " + maxRows);

      stmt.setMaxRows(-1);
      maxRows = stmt.getMaxRows();
      System.out.println(maxRows);
    } catch (SQLException e) {
      System.out.println(e.getErrorCode());
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }

    close(stmt);
  }

  /**
   * Check the maximum number of rows generated by the Statement object is really limited by setMaxRows method.
   * Insert 2 rows into the table, set the limit to 1, check only 1 row can be fetched.
   */
  @Category(PassTest.class)
  @Test
  public void testMaxRows2() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    int count = 0;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.setMaxRows(1);
      stmt.execute("create table t_testMaxRows2(a int, b varchar2(10))");
      stmt.execute("insert into t_testMaxRows2 values (1, 'a test')");
      stmt.execute("insert into t_testMaxRows2 values (2, 'b test')");
      rs = stmt.executeQuery("select a, b from t_testMaxRows2");
      while (rs.next()) {
        count++;
      }
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      try {
        stmt.execute("drop table t_testMaxRows2 purge");
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    close(rs);
    close(stmt);

    assertEquals(1, count);
  }

  @Category(PassTest.class)
  @Test
  public void testMaxRows3() {
    Connection conn = null;
    Statement stmt = null;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.setMaxRows(2147483647);
      assertEquals(2147483647, stmt.getMaxRows());
      stmt.setMaxRows(-2147483648);
    } catch (SQLException e) {
      assertEquals("Invalid argument(s) in call", e.getMessage());
    } catch (ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(stmt);
  }

  /**
   * Check whether the Statement is closed using isClosed method.
   */
  @Category(PassTest.class)
  @Test
  public void testIsClosed() {
    Connection conn = null;
    Statement stmt = null;
    boolean closed = false;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.close();
      closed = stmt.isClosed();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(stmt);

    assertEquals(true, closed);
  }

  /**
   * Test dropping a non-exist table. The error code 942 is expected.
   */
  @Category(PassTest.class)
  @Test
  public void testDropNonExistTable() {
    Connection conn = null;
    Statement stmt = null;
    int errorCode = -1;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute("drop table non_exist_table");
    } catch (SQLException e) {
      errorCode = e.getErrorCode();
    } catch (ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(stmt);

    assertEquals(942, errorCode);
  }

  @Category(PassTest.class)
  @Test
  public void testDropNonExistUser() {
    Connection conn = null;
    Statement stmt = null;
    int errorCode = -1;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute("drop user non_exist_user");
    } catch (SQLException e) {
      errorCode = e.getErrorCode();
    } catch (ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(stmt);
    assertEquals(1918, errorCode);
  }

  /**
   * The semicolon should not be allowed in the end of the sql passed to executeQuery.
   * ORA-00933 is expected. 
   * JDBC REST does not throw an ORA error because the script parser in ORDS
   * removes terminators (;) as it thinks the text could be a script.
   * [This is a documented difference between the thin driver and the REST JDBC driver]
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testOra00933() {
    Connection conn = null;
    Statement stmt = null;
    int errorCode = -1;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.executeQuery("select * from statementtest_dept;");
    } catch (SQLException e) {
      errorCode = e.getErrorCode();
    } catch (ClassNotFoundException e) {
      fail(e.getMessage());
    }
   
    close(stmt);
    assertEquals(933, errorCode);
  }

  /**
   * Try to create a table with invalid table name, ora-00903 is expected.
   */
  @Category(PassTest.class)
  @Test
  public void testInvalidTableName() {
    Connection conn = null;
    Statement stmt = null;
    int errorCode = -1;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute("create table 123424729498274987298347928734982734872 (a1 number)");
    } catch (SQLException e) {
      errorCode = e.getErrorCode();
    } catch (ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(stmt);
    assertEquals(903, errorCode);
  }

  /**
   * Cursor expression is now not allow in jdbcrest.
   */
  @Category(PassTest.class)
  @Test
  public void testCursorExpression() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
//    String expected = "ADAMS\n" +
//        "ALLEN\n" +
//        "BLAKE\n" +
//        "CLARK\n" +
//        "FORD\n" +
//        "JAMES\n" +
//        "JONES\n" +
//        "KING\n" +
//        "MARTIN\n" +
//        "MILLER\n" +
//        "SCOTT\n" +
//        "SMITH\n" +
//        "TURNER\n" +
//        "WARD\n";
    String expected = "ORA-22902: CURSOR expression not allowed";
    int errorCode = 22902;
    SQLException se = null;
    
    String actual = "";
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select ename, CURSOR(select dname from statementtest_dept " +
          "where deptno = statementtest_emp.deptno) as depts from statementtest_emp order by ename");
      StringBuilder sb = new StringBuilder();
      // Dump the cursor
      while (rs.next()) {
        sb.append(rs.getString(1) + "\n");
      }
      actual = sb.toString();
    } catch (SQLException e) {
        se = e;
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    close(stmt);
    assertEquals(expected, se.getMessage());
    assertEquals(errorCode, se.getErrorCode());
  }

  @Category(PassTest.class)
  @Test
  public void testExecuteUpdate() {
    Connection conn = null;
    Statement stmt = null;
    int rc1 = -1;
    int rc2 = -1;
    int rc3 = -1;
    int rc4 = -1;
    int rc5 = -1;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      rc1 = stmt.executeUpdate("create table tkpjis20 (x number)");
      rc2 = stmt.executeUpdate("insert into tkpjis20 values (100)");
      rc3 = stmt.executeUpdate("insert into tkpjis20 select decode(rownum, 1, 111, 2, -1, 3, 56) from dual" +
          " connect by level <= 3");
      rc4 = stmt.executeUpdate("update tkpjis20 set x = -100 where x = -1");
      rc5 = stmt.executeUpdate("delete from tkpjis20");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table tkpjis20 purge");
    }
    close(stmt);
    assertEquals(0, rc1);
    assertEquals(1, rc2);
    assertEquals(3, rc3);
    assertEquals(1, rc4);
    assertEquals(4, rc5);
  }


  /**
   * Test to check for Update Counts on Select statements
   * (The thin driver does not adhere to the documentation in this case)
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testExecuteUpdateSelect() {
    int oracleUpdateCount = 0, restUpdateCount = 0;
    try {
      Connection conn = getOracleConnection();
      Statement stmt = conn.createStatement();
      oracleUpdateCount = stmt.executeUpdate("select * from statementtest_emp");
      stmt.close();
      conn.close();
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    }
    // Run the REST driver
    try {
      Connection conn = getRestConnection();
      Statement stmt = conn.createStatement();
      restUpdateCount = stmt.executeUpdate("select * from statementtest_emp");
      stmt.close();
      conn.close();
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    }
    assertEquals(oracleUpdateCount, restUpdateCount);    
  }

  /**
   * Insert into the table with date as index.
   */
  @Category(PassTest.class)
  @Test
  public void testInsertDateAsIndex() {
    Connection conn = null;
    Statement stmt = null;
    int rc = -1;
    ResultSet rs = null;
    String expected = "a product name expires on 2017-01-03 23:01:48";
    String actual = "";
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute("create table product893 (name varchar2(100) constraint nn_b NOT NULL, " +
          "context_info varchar2(512), " +
          "expiry_date date)");
      stmt.execute("create index PRODUCT893_EXPIRY on PRODUCT893 (EXPIRY_DATE)");
      rc = stmt.executeUpdate("insert into PRODUCT893 values('a product name', 'test', to_date('TUE, 03 JAN 2017 23:01:48 GMT', 'DY, DD MON YYYY HH24:MI:SS \"GMT\"'))");
      rs = stmt.executeQuery("select name, expiry_date from product893");
      StringBuilder sb = new StringBuilder();
      while (rs.next()) {
        String s = rs.getString("name");
        java.sql.Date d = rs.getDate("expiry_date");
        java.sql.Time t = rs.getTime("expiry_date");
        sb.append(s + " expires on " + d + " " + t);
      }
      actual = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop index PRODUCT893_EXPIRY");
      Util.trySQL(conn, "drop table product893 purge");
    }
    assertEquals(1, rc);
    assertEquals(expected, actual);
    close(rs);
    close(stmt);
  }

  /**
   * Test for selecting similarly (ambiguously) named column names.
   */
  @Category(PassTest.class)
  @Test
  public void testSelectColumn() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    int expected = 1;
    int actual = -1;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select 1 as \"col1\",2 as col1 from dual");
      
      while (rs.next()) {
        actual = rs.getInt("col1");        
      }
     
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(rs);
    close(stmt);
    assertEquals(expected, actual);
  }

  /**
   * Test the getResultSet method
   */
  @Category(PassTest.class)
  @Test
  public void testGetResultSet() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String actual = "";
    String expected = "ACCOUNTING\n" +
        "OPERATIONS\n" +
        "RESEARCH\n" +
        "SALES\n";
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute("create table tkpjis62(x number)");
      rs = stmt.getResultSet();
      try {
        rs.next();
      } catch (NullPointerException e) {
      }

      stmt.executeQuery("select dname from STATEMENTTEST_DEPT order by 1");
      rs = stmt.getResultSet();
      StringBuilder sb = new StringBuilder();
      while (rs.next()) {
        sb.append(rs.getObject(1) + "\n");
      }
      actual = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table tkpjis62 purge");
    }
    close(rs);
    close(stmt);
    assertEquals(expected, actual);
  }



  /**
  @Category(FailTest.class)
  @Test
  public void testSetQueryTimeout() {
    Connection conn = null;
    Statement stmt = null;
    int errorCode = -1;
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();

      stmt.setQueryTimeout(2147483647);
      assertEquals(2147483647, stmt.getQueryTimeout());

      try {
        stmt.setQueryTimeout(-2147483648);
      } catch (SQLException e) {
        System.out.println("Oops..." + e.getMessage());
      }

      stmt.setQueryTimeout(2);
      try {
        stmt.execute("declare excep exception; begin dbms_lock.sleep(10); Raise excep; end;");
      } catch (SQLException e) {
        errorCode = e.getErrorCode();
        e.printStackTrace();
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
    close(stmt);
    assertEquals(1013, errorCode);
  }
  */

  /**
   * Test the limit set by setMaxFieldSize method.
   * Now the test is failing because the {@link ResultSet#getBytes(int)} is getting NPE using JDBCREST.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testMaxFieldSize() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String varchar2Expected = "aaaaaaaabbbbbbbbc";
    String varchar2Actual = "";
    String rawExpected = "01010101010a0a0a0a";
    String rawActual = "";
    String longRawExpected = "01010101010a0a0a0a0a0a0a0b0b0b0b0b0c0c0c0c";
    String longRawActual = "";
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute("create table tkpjis73 (x varchar2(255))");
      stmt.execute("insert into tkpjis73 values ('aaaaaaaabbbbbbbbccccccccdddddddd')");

      stmt.execute("create table tkpjis73a (x raw(255))");
      stmt.execute("insert into tkpjis73a values ('01010101010a0a0a0a0a0a0a0b0b0b0b0b0c0c0c0c')");

      stmt.execute("create table tkpjis73b (x blob)");
      stmt.execute("insert into tkpjis73b values ('01010101010a0a0a0a0a0a0a0b0b0b0b0b0c0c0c0c')");

      stmt.setMaxFieldSize(17);
      rs = stmt.executeQuery("select x from tkpjis73");
      rs.next();
      varchar2Actual = rs.getString(1);

      stmt.setMaxFieldSize(9);
      rs = stmt.executeQuery("select x from tkpjis73a");
      rs.next();
      rawActual = Util.getByteArrayString(rs.getBytes(1));

      stmt.setMaxFieldSize(14);
      rs = stmt.executeQuery("select x from tkpjis73b");
      rs.next();
      longRawActual = Util.getByteArrayString(rs.getBytes(1));
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table tkpjis73 purge");
      Util.trySQL(conn, "drop table tkpjis73a purge");
      Util.trySQL(conn, "drop table tkpjis73b purge");
    }
    close(rs);
    close(stmt);
    assertEquals(varchar2Expected, varchar2Actual);
    assertEquals(rawExpected, rawActual);
    assertEquals(longRawExpected, longRawActual);
  }

  /**
   * Test closeOnCompletion method.
   * Now the test is failing with closed statement error.
   * In JDBCRest, the statement is closed after calling {@link Statement#closeOnCompletion()} method.
   */
  @Category(PassTest.class)
  @Test
  public void testCloseOnCompletion() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    String expected = "Expected SQLException";
    String actual = "";
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.execute("create table tkpjis82(value varchar2(100))");
      assertEquals(false, stmt.isCloseOnCompletion());
      stmt.closeOnCompletion();
      assertEquals(true, stmt.isCloseOnCompletion());
      rs = stmt.executeQuery("select value from tkpjis82");
      rs.close();
      assertEquals(true, stmt.isClosed());

      try {
        stmt.closeOnCompletion();
      } catch (SQLException e) {
        actual = "Expected SQLException";
      }
      assertEquals(expected, actual);
      actual = "";

      try {
        stmt.isCloseOnCompletion();
      } catch (SQLException e) {
        actual = "Expected SQLException";
      }
      assertEquals(expected, actual);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    } finally {
      Util.trySQL(conn, "drop table tkpjis82 purge");
    }
  }

  /**
   * Test drop a type with table dependency.
   * Expect SQLException: ORA-02303: cannot drop or replace a type with type or table dependents
   */
  @Category(PassTest.class)
  @Test
  public void testDropTypeWithTableDependency() {
    String expected = "ORA-02303: cannot drop or replace a type with type or table dependents";
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "create or replace type msg as object (x int)");
      Util.trySQL(conn, "create table xtest (col msg)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      try {
        oracleStmt.executeQuery("drop type msg");
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      try {
        restStmt.executeQuery("drop type msg");
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);
    assertEquals(expected, e2.getMessage());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getSQLState(), e2.getSQLState());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table xtest purge");
      Util.trySQL(conn, "drop type msg");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test inserting a 32k string and fetch it back.
   * Now the test is failing because of Internal Error.
   */
  @Category(FailTest.class)
  @Test
  public void testInsert32KString() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table tab_varchar32k purge");
      Util.trySQL(conn, "create table tab_varchar32k (\"COL\" varchar2(32767 BYTE))");
      String str8B = "a\tS:3?&$";
      StringBuilder sb = new StringBuilder(str8B);
      // 8 * 2^12 = 32K
      for (int i = 0; i < 12; i++) {
        sb.append(sb);
      }
      String s = sb.deleteCharAt(0).toString();
      Util.trySQL(conn, "insert into tab_varchar32k values ('" + s + "')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection restConn = null;
    Statement restStmt = null;
    ResultSet restRs = null;
    String s = "";

    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select col from tab_varchar32k");
      restRs.next();
      s = restRs.getString(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(restRs);
    close(restStmt);

    assertEquals(32767, s.length());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table tab_varchar32k purge");
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }

  /**
   * Test executing a sql with comments.
   * REST JDBC Driver is failing with ORA-00907: missing right parenthesis.
   * It should get 'comments' from the query.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testExceutingSQLWithComments() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("select /*foobar*/ 'comments' as col from /*junk*/ dual --dada-- /*oops*/");
      oracleRs.next();
      oracleResult = oracleRs.getString(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("select /*foobar*/ 'comments' as col from /*junk*/ dual --dada-- /*oops*/");
      restRs.next();
      restResult = restRs.getString(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(oracleResult, restResult);

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
  }

  /**
   * Positive test for quoted identifiers. Use column index to retrieve value from a <code>ResultSet</code> object.
   */
  @Category(PassTest.class)
  @Test
  public void testQuotedIdentifiers1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table \"Tab\" purge");
      Util.trySQL(conn, "create table \"Tab\" (col number, \"col\" varchar2(10))");
      Util.trySQL(conn, "insert into \"Tab\" values (100, 'xilan')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    int i1 = -1, i2 = -2;
    String s1 = "s1", s2 = "s2";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleRs = oracleStmt.executeQuery("SeLeCT Col, \"col\" FROm \"Tab\"");
      oracleRs.next();
      i1 = oracleRs.getInt(1);
      s1 = oracleRs.getString(2);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("SeLeCT Col, \"col\" FROm \"Tab\"");
      restRs.next();
      i2 = restRs.getInt(1);
      s2 = restRs.getString(2);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(i1, i2);
    assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table \"Tab\" purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test quoted identifiers. Get the first column from <code>ResultSet</code> object.
   */
  @Category(PassTest.class)
  @Test
  public void testQuotedIdentifiers2() {
    int expectedErrorCode    = 918;
    String expectedSQLState  = "42000";
    String expectedMsg       = "ORA-00918: column ambiguously defined";
    int actualErrorCode      = -1;
    String actualSQLState    = null;
    String actualMsg         = null;
    
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table \"Tab\" purge");
      Util.trySQL(conn, "create table \"Tab\" (col number, \"col\" varchar2(10))");
      Util.trySQL(conn, "insert into \"Tab\" values (100, 'xilan')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection restConn = null;
    Statement restStmt = null;
    ResultSet restRs = null;
    

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restRs = restStmt.executeQuery("SeLeCT Col, col FROm \"Tab\"");
      restRs.next();
      int i2 = restRs.getInt(1);
      int i4 = restRs.getInt(2);
    } catch (ClassNotFoundException e) {
        fail(e.getMessage());
    } catch(SQLException se) {
        actualErrorCode = se.getErrorCode();
        actualSQLState  = se.getSQLState();
        actualMsg       = se.getMessage();
    }

    close(restRs);
    close(restStmt);

    assertEquals(expectedErrorCode, actualErrorCode);
    assertEquals(expectedSQLState, actualSQLState);
    assertEquals(expectedMsg, actualMsg);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table \"Tab\" purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test the default value of <code>isPoolable</code> method. Always <code>false</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testIsPoolable1() {
    Connection conn = null;
    Statement stmt = null;
    boolean poolable = true;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      poolable = stmt.isPoolable();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(stmt);

    assertEquals(false, poolable);
  }

  /**
   * Test <code>isPoolable</code> on a closed <code>Statement</code>.
   * Expected: Closed Statement
   */
  @Category(PassTest.class)
  @Test
  public void testIsPoolable2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.close();
      try {
        oracleStmt.isPoolable();
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.close();
      try {
        restStmt.isPoolable();
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getMessage(), e2.getMessage());
  }

  /**
   * Check <code>setPoolable</code> has no effect on REST JDBC Driver.
   */
  @Category(PassTest.class)
  @Test
  public void testSetPoolable1() {
    Connection conn = null;
    Statement stmt = null;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      stmt.setPoolable(true);
      boolean poolable = true;
      poolable = stmt.isPoolable();
      assertEquals(false, poolable);
      stmt.setPoolable(false);
      poolable = stmt.isPoolable();
      assertEquals(false, poolable);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Check <code>setPoolable</code> on a closed <code>Statement</code>.
   * Should get a SQLException even this method has no effect on REST JDBC Driver.
   */
  @Category(PassTest.class)
  @Test
  public void testSetPoolable2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.close();
      try {
        oracleStmt.setPoolable(false);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.close();
      try {
        restStmt.setPoolable(false);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getMessage(), e2.getMessage());
  }

  /**
   * Check <code>getResultSetType</code> returns <code>ResultSet.TYPE_FORWARD_ONLY</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetResultSetType1() {
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;

    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      int type = stmt.getResultSetType();
      assertEquals(ResultSet.TYPE_FORWARD_ONLY, type);
      rs = stmt.executeQuery("select -1 from dual");
      rs.setFetchDirection(ResultSet.FETCH_FORWARD);
      type = stmt.getResultSetType();
      assertEquals(ResultSet.TYPE_FORWARD_ONLY, type);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(rs);
    close(stmt);
  }

  /**
   * Test <code>getResultSetType</code> on a closed <code>Statement</code>.
   * Now the test is failing because REST JDBC Driver does not get the correct
   * exception message.
   * Expected :Closed Statement
   */
  @Category(PassTest.class)
  @Test
  public void testGetResultSetType2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.close();
      try {
        oracleStmt.getResultSetType();
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.close();
      try {
        restStmt.getResultSetType();
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getMessage(), e2.getMessage());
  }

  /**
   * Test <code>setEscapeProcessing</code> has no effect on REST JDBC Driver.
   */
  @Category(PassTest.class)
  @Test
  public void testSetEscapeProcessing1() {
    //Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table escppr purge");
      Util.trySQL(conn, "create table escppr (adate date)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection restConn = null;
    Statement restStmt = null;
    SQLException e1 = null, e2 = null;

    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.setEscapeProcessing(false);
      try {
        restStmt.executeUpdate("insert into escppr values({d '2017-03-06'})");
      } catch (SQLException e) {
        e1 = e;
      }

      restStmt.setEscapeProcessing(true);
      try {
        restStmt.executeUpdate("insert into escppr values({d '2017-03-06'})");
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(restStmt);

    assertEquals("ORA-00936: missing expression", e1.getMessage());
    assertEquals("ORA-00936: missing expression", e2.getMessage());

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table escppr purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>setEscapseProcessing</code> on a closed <code>Statement</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testSetEscapeProcessing2() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.close();
      try {
        oracleStmt.setEscapeProcessing(true);
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.close();
      try {
        restStmt.setEscapeProcessing(true);
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getMessage(), e2.getMessage());
  }

  /**
   * Test <code>getUpdateCount</code> after inserting 1 row into a table.
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "create table uc_tab1 (col number)");
      Util.trySQL(conn, "drop table uc_tab2 purge");
      Util.trySQL(conn, "create table uc_tab2 (col number)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount1 = -2, restUpdateCount1 = -2;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      String sql = "insert into uc_tab1 values(100)";
      oracleStmt.execute(sql);
      oracleUpdateCount1 = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      String sql = "insert into uc_tab2 values(100)";
      restStmt.execute(sql);
      restUpdateCount1 = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount1, restUpdateCount1);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getUpdateCount</code> after inserting 3 row into a table.
   * Now the test is failing because REST JDBC does not return the correct
   * update count.
   *
   * Expected: 3
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "create table uc_tab1 (col number)");
      Util.trySQL(conn, "drop table uc_tab2 purge");
      Util.trySQL(conn, "create table uc_tab2 (col number)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount1 = -2, restUpdateCount1 = -2;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      String sql = "insert all into uc_tab1 values (1) into uc_tab1 values(2) into uc_tab1 values(3) select 1 from dual";
      oracleStmt.execute(sql);
      oracleUpdateCount1 = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      String sql = "insert all into uc_tab2 values (1) into uc_tab2 values(2) into uc_tab2 values(3) select 1 from dual";
      restStmt.execute(sql);
      restUpdateCount1 = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount1, restUpdateCount1);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getUpdateCount</code> after executing a DDL.
   * Now the test is failing because REST JDBC returns -1 for
   * a DDL while Oracle Thin Driver returns 0 for a DDL.
   *
   * Expected :0
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount3() {
    // Drop the table first in case they are created before
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount = -2, restUpdateCount = -2;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.execute("create table uc_tab1(x number)");
      oracleUpdateCount = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.execute("create table uc_tab2(x number)");
      restUpdateCount = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount, restUpdateCount);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getUpdateCount</code> after executing a query.
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount4() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
      Util.trySQL(conn, "create table uc_tab1 (x number)");
      Util.trySQL(conn, "insert into uc_tab1 values (1)");
      Util.trySQL(conn, "insert into uc_tab1 values (2)");
      Util.trySQL(conn, "create table uc_tab2 (x number)");
      Util.trySQL(conn, "insert into uc_tab2 values (1)");
      Util.trySQL(conn, "insert into uc_tab2 values (2)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount = -2, restUpdateCount = -2;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.execute("select * from uc_tab1");
      oracleUpdateCount = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.execute("select * from uc_tab2");
      restUpdateCount = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount, restUpdateCount);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getUpdateCount</code> after updating 2 rows.
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount5() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "create table uc_tab1 (x int)");
      Util.trySQL(conn, "insert into uc_tab1 values (1)");
      Util.trySQL(conn, "insert into uc_tab1 values (2)");

      Util.trySQL(conn, "drop table uc_tab2 purge");
      Util.trySQL(conn, "create table uc_tab2 (x int)");
      Util.trySQL(conn, "insert into uc_tab2 values (1)");
      Util.trySQL(conn, "insert into uc_tab2 values (2)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount = -2, restUpdateCount = -2;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.execute("update uc_tab1 set x = 100 where x <= 2");
      oracleUpdateCount = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.execute("update uc_tab2 set x = 200 where x <= 2");
      restUpdateCount = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount, restUpdateCount);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getUpdateCount</code> after deleting 2 rows of a table.
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount6() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "create table uc_tab1 (x int)");
      Util.trySQL(conn, "insert into uc_tab1 values (1)");
      Util.trySQL(conn, "insert into uc_tab1 values (2)");

      Util.trySQL(conn, "drop table uc_tab2 purge");
      Util.trySQL(conn, "create table uc_tab2 (x int)");
      Util.trySQL(conn, "insert into uc_tab2 values (1)");
      Util.trySQL(conn, "insert into uc_tab2 values (2)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount = -20, restUpdateCount = -20;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.execute("delete from uc_tab1");
      oracleUpdateCount = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.execute("delete from uc_tab2");
      restUpdateCount = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount, restUpdateCount);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getUpdateCount</code> on a closed <code>Statement</code>.
   * Now the test is failing because REST JDBC Driver return wrong exception message.
   *
   * Expected: Closed Statement
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount7() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.execute("select 1 from dual");
      oracleStmt.close();
      try {
        oracleStmt.getUpdateCount();
      } catch (SQLException e) {
        e1 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.execute("select 1 from dual");
      restStmt.close();
      try {
        restStmt.getUpdateCount();
      } catch (SQLException e) {
        e2 = e;
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    assertEquals(e1.getSQLState(), e2.getSQLState());
    assertEquals(e1.getErrorCode(), e2.getErrorCode());
    assertEquals(e1.getMessage(), e2.getMessage());
  }

  /**
   * Test <code>getUpdateCount</code> after inserting 3 row into a table using <code>executeUpdate</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount8() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "create table uc_tab1 (col number)");
      Util.trySQL(conn, "drop table uc_tab2 purge");
      Util.trySQL(conn, "create table uc_tab2 (col number)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount1 = -2, restUpdateCount1 = -2;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      String sql = "insert all into uc_tab1 values (1) into uc_tab1 values(2) into uc_tab1 values(3) select 1 from dual";
      oracleStmt.executeUpdate(sql);
      oracleUpdateCount1 = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      String sql = "insert all into uc_tab2 values (1) into uc_tab2 values(2) into uc_tab2 values(3) select 1 from dual";
      restStmt.executeUpdate(sql);
      restUpdateCount1 = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount1, restUpdateCount1);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getUpdateCount</code> after executing a DDL using <code>executeUpdate</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount9() {
    // Drop the table first in case they are created before
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount = -2, restUpdateCount = -2;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.executeUpdate("create table uc_tab1(x number)");
      oracleUpdateCount = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.executeUpdate("create table uc_tab2(x number)");
      restUpdateCount = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount, restUpdateCount);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getUpdateCount</code> after executing a query using <code>executeUpdate</code>.   *
   * Expected :-1
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount10() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
      Util.trySQL(conn, "create table uc_tab1 (x number)");
      Util.trySQL(conn, "insert into uc_tab1 values (1)");
      Util.trySQL(conn, "insert into uc_tab1 values (2)");
      Util.trySQL(conn, "create table uc_tab2 (x number)");
      Util.trySQL(conn, "insert into uc_tab2 values (1)");
      Util.trySQL(conn, "insert into uc_tab2 values (2)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount = -2, restUpdateCount = -2;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.executeUpdate("select * from uc_tab1");
      oracleUpdateCount = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.executeUpdate("select * from uc_tab2");
      restUpdateCount = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount, restUpdateCount);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getUpdateCount</code> after updating 2 rows using <code>executeUpdate</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount11() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "create table uc_tab1 (x int)");
      Util.trySQL(conn, "insert into uc_tab1 values (1)");
      Util.trySQL(conn, "insert into uc_tab1 values (2)");

      Util.trySQL(conn, "drop table uc_tab2 purge");
      Util.trySQL(conn, "create table uc_tab2 (x int)");
      Util.trySQL(conn, "insert into uc_tab2 values (1)");
      Util.trySQL(conn, "insert into uc_tab2 values (2)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount = -2, restUpdateCount = -2;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.executeUpdate("update uc_tab1 set x = 100 where x <= 2");
      oracleUpdateCount = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.executeUpdate("update uc_tab2 set x = 200 where x <= 2");
      restUpdateCount = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount, restUpdateCount);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getUpdateCount</code> after deleting 2 rows of a table using <code>executeUpdate</code>
   */
  @Category(PassTest.class)
  @Test
  public void testGetUpdateCount12() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "create table uc_tab1 (x int)");
      Util.trySQL(conn, "insert into uc_tab1 values (1)");
      Util.trySQL(conn, "insert into uc_tab1 values (2)");

      Util.trySQL(conn, "drop table uc_tab2 purge");
      Util.trySQL(conn, "create table uc_tab2 (x int)");
      Util.trySQL(conn, "insert into uc_tab2 values (1)");
      Util.trySQL(conn, "insert into uc_tab2 values (2)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleUpdateCount = -20, restUpdateCount = -20;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.executeUpdate("delete from uc_tab1");
      oracleUpdateCount = oracleStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.executeUpdate("delete from uc_tab2");
      restUpdateCount = restStmt.getUpdateCount();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleUpdateCount, restUpdateCount);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table uc_tab1 purge");
      Util.trySQL(conn, "drop table uc_tab2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getWarnings</code> and <cdde>clearWarnings</cdde> methods.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testWarning1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    SQLWarning oracleSQLWarning = null, restSQLWarning = null;
    // Used to check sql warning after calling clearWarnings method
    SQLWarning clearedOracleSQLWarning = new SQLWarning("xxx"), clearedRESTSQLWarning = new SQLWarning("yyy");

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.execute(
          "create or replace procedure myproc is" +
          "begin" +
          "  raise no_data_found;" +
          "exception" +
          "  when others then" +
          "    null;" +
          "end myproc;");
      oracleSQLWarning = oracleStmt.getWarnings();
      oracleStmt.clearWarnings();
      clearedOracleSQLWarning = oracleStmt.getWarnings();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.execute(
        "create or replace procedure myproc is" +
        "begin" +
        "  raise no_data_found;" +
        "exception" +
        "  when others then" +
        "    null;" +
        "end myproc;");
      restSQLWarning = restStmt.getWarnings();
      restStmt.clearWarnings();
      clearedRESTSQLWarning = restStmt.getWarnings();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleSQLWarning, restSQLWarning);
    assertEquals(clearedOracleSQLWarning, clearedRESTSQLWarning);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop procedure dead_code");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Basic test for <code>addBatch</code>, <code>executeBatch</code>.
   * Now the test is failing since REST JDBC does not get the correct result.
   */
  @Category(PassTest.class)
  @Test
  public void testBatch1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table batch_tab1 purge");
      Util.trySQL(conn, "create table batch_tab1 (x int)");
      Util.trySQL(conn, "drop table batch_tab2 purge");
      Util.trySQL(conn, "create table batch_tab2 (x int)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String oracleResult = "oracleResult", restResult = "restResult";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.addBatch("insert into batch_tab1 values (1)");
      oracleStmt.addBatch("insert into batch_tab1 values (2)");
      oracleStmt.addBatch("insert into batch_tab1 values (3)");
      oracleStmt.executeBatch();
      oracleRs = oracleStmt.executeQuery("select x from batch_tab1");
      StringBuilder sb = new StringBuilder();
      while (oracleRs.next()) {
        sb.append(oracleRs.getInt(1));
      }
      oracleResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.addBatch("insert into batch_tab2 values (1)");
      restStmt.addBatch("insert into batch_tab2 values (2)");
      restStmt.addBatch("insert into batch_tab2 values (3)");
      restStmt.executeBatch();
      restRs = restStmt.executeQuery("select x from batch_tab2");
      StringBuilder sb = new StringBuilder();
      while (restRs.next()) {
        sb.append(restRs.getInt(1));
      }
      restResult = sb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table batch_tabl1 purge");
      Util.trySQL(conn, "drop table batch_tabl2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Basic test of <code>clearBatch</code>.
   */
  @Category(PassTest.class)
  @Test
  public void testBatch2() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table batch_tab1 purge");
      Util.trySQL(conn, "create table batch_tab1 (x int)");
      Util.trySQL(conn, "drop table batch_tab2 purge");
      Util.trySQL(conn, "create table batch_tab2 (x int)");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    int oracleResult = -1, restResult = -1;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.addBatch("insert into batch_tab1 values (1)");
      oracleStmt.addBatch("insert into batch_tab1 values (2)");
      oracleStmt.addBatch("insert into batch_tab1 values (3)");
      oracleStmt.clearBatch();
      oracleStmt.executeBatch();
      oracleRs = oracleStmt.executeQuery("select count(*) from batch_tab1");
      oracleRs.next();
      oracleResult = oracleRs.getInt(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.addBatch("insert into batch_tab1 values (1)");
      restStmt.addBatch("insert into batch_tab1 values (2)");
      restStmt.addBatch("insert into batch_tab1 values (3)");
      restStmt.clearBatch();
      restStmt.executeBatch();
      restRs = restStmt.executeQuery("select count(*) from batch_tab1");
      restRs.next();
      restResult = restRs.getInt(1);
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    assertEquals(oracleResult, restResult);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table batch_tabl1 purge");
      Util.trySQL(conn, "drop table batch_tabl2 purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Basic test of <code>setMaxFieldSize</code> and <code>getMaxFieldSize</code> for char type.
   * Currently the test case just catch RestJdbcNotImplementedException and passes.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testMaxFieldSize1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table maxfieldtab");
      Util.trySQL(conn, "create table maxfieldtab (a char(255))");
      Util.trySQL(conn, "insert into maxfieldtab values ('abcdefghijklmnopqrstuvwxyz')");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    int oracleDefault = -1, restDefault = -1;
    int oracleChaned = -1, restChanged = -1;
    int oracleLength = -1, restLength = -1;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      // default max field size
      oracleDefault = oracleStmt.getMaxFieldSize();
      oracleStmt.setMaxFieldSize(25);
      oracleChaned = oracleStmt.getMaxFieldSize();
      oracleRs = oracleStmt.executeQuery("select * from maxfieldtab");
      oracleRs.next();
      oracleLength = oracleRs.getString(1).length();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      // default max field size
      restDefault = restStmt.getMaxFieldSize();
      restStmt.setMaxFieldSize(25);
      restChanged = restStmt.getMaxFieldSize();
      restRs = restStmt.executeQuery("select * from maxfieldtab");
      restRs.next();
      restLength = restRs.getString(1).length();
    } catch (RestJdbcNotImplementedException e) {
      assertEquals("Method getMaxFieldSize() in Statement not implemented yet.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(oracleDefault, restDefault);
    // assertEquals(oracleChaned, restChanged);
    // assertEquals(oracleLength, restLength);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop table maxfieldtab purge");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Basic test for <code>setQueryTimeout</code> and <code>getQueryTimeout</code>.
   * Currently the test case just catch RestJdbcNotImplementedException and passes.
   */
  @Category(PassTest.class)
  @Test
  public void testQueryTimeout1() {
    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    int oracleDefault = -1, restDefault = -1;
    int oracleMax = -1, restMax = -1;
    int oracleZero = -1, restZero = -1;
    int oracleTimeout = 1, restTimeout = 1;
    SQLException e1 = null, e2 = null;

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      // Default query timeout.
      oracleDefault = oracleStmt.getQueryTimeout();

      // Integer.MAX_VALUE
      oracleStmt.setQueryTimeout(Integer.MAX_VALUE);
      oracleMax = oracleStmt.getQueryTimeout();

      // 0
      oracleStmt.setQueryTimeout(0);
      oracleZero = oracleStmt.getQueryTimeout();

      // Integer.MIN_VALUE
      try {
        oracleStmt.setQueryTimeout(Integer.MIN_VALUE);
      } catch (SQLException e) {
        e1 = e;
      }

      // Check the timeout after the exception
      oracleTimeout = oracleStmt.getQueryTimeout();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      // Default query timeout.
      restDefault = restStmt.getQueryTimeout();

      // Integer.MAX_VALUE
      restStmt.setQueryTimeout(Integer.MAX_VALUE);
      restMax = restStmt.getQueryTimeout();

      // 0
      restStmt.setQueryTimeout(0);
      restZero = oracleStmt.getQueryTimeout();

      // Integer.MIN_VALUE
      try {
        restStmt.setQueryTimeout(Integer.MIN_VALUE);
      } catch (SQLException e) {
        e2 = e;
      }

      // Check the timeout after the exception
      restTimeout = oracleStmt.getQueryTimeout();
    } catch (RestJdbcNotImplementedException e) {
      assertEquals("Method getQueryTimeout() in Statement not implemented yet.",e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    // assertEquals(oracleDefault, restDefault);
    // assertEquals(oracleMax, restMax);
    // assertEquals(oracleZero, restZero);
    // assertEquals(oracleTimeout, restTimeout);

    // assertEquals(e1.getErrorCode(), e2.getErrorCode());
    // assertEquals(e1.getSQLState(), e2.getSQLState());
    // assertEquals(e1.getMessage(), e2.getMessage());
  }

  /**
   * Retrieving multiple <code>ResultSet</code>.
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetMoreResults1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "create or replace procedure p as " +
          "  c1 sys_refcursor; " +
          "  c2 sys_refcursor; " +
          "begin " +
          "  open c1 for 'select 100 col1 from dual'; " +
          "  dbms_sql.return_result (c1); " +
          "  open c2 for 'select 200 col2 from dual';  " +
          "  dbms_sql.return_result (c2); " +
          "end;");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    String s1 = "", s2 = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      oracleStmt.execute("begin p; end;");
      while (oracleStmt.getMoreResults()) {
        ResultSet rs = oracleStmt.getResultSet();
        rs.next();
        s1 += rs.getString(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      restStmt.execute("begin p; end;");
      while (restStmt.getMoreResults()) {
        ResultSet rs = restStmt.getResultSet();
        rs.next();
        s2 += rs.getString(1);
      }
    } catch (RestJdbcNotImplementedException e) {
      assertEquals("Method getMoreResults() in Statement not implemented yet.", e.getMessage());
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleRs, restRs);
    close(oracleStmt, restStmt);

    // assertEquals(s1, s2);

    // Cleanup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "drop procedure p");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }
  }

  /**
   * Test <code>getMoreResults(int current)</code> method. 
   * Compare the return values and values from getter methods.
   * Statement.getMoreResults is currently not supported in the REST JDBC driver 
   */
  @Category(UnsupportedTest.class)
  @Test
  public void testGetMoreResultsInt1() {
    // Setup
    try {
      Connection conn = getRestConnection();
      Util.trySQL(conn, "create or replace procedure p as " +
          "  c1 sys_refcursor; " +
          "  c2 sys_refcursor; " +
          "  c3 sys_refcursor; " +
          "  c4 sys_refcursor; " +
          "begin " +
          "  open c1 for 'select 100 col1 from dual'; " +
          "  dbms_sql.return_result (c1); " +
          "  open c2 for 'select 200 col2 from dual';  " +
          "  dbms_sql.return_result (c2); " +
          "  open c3 for 'select 300 col3 from dual';  " +
          "  dbms_sql.return_result (c3); " +
          "  open c4 for 'select 400 col4 from dual';  " +
          "  dbms_sql.return_result (c4); " +
          "end;");
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    Connection oracleConn = null, restConn = null;
    Statement oracleStmt = null, restStmt = null;

    // The values returned from ResultSet
    String oracleResult = "", restResult = "";
    // The values returned by getMoreResults
    String oracleReturnValues = "", restReturnValues = "";

    // Run Oracle Thin Driver
    try {
      oracleConn = getOracleConnection();
      oracleStmt = oracleConn.createStatement();
      StringBuilder returnValuesSb = new StringBuilder();
      StringBuilder resultsSb = new StringBuilder();
      oracleStmt.execute("begin p; end;");
      boolean retval = oracleStmt.getMoreResults(Statement.KEEP_CURRENT_RESULT);
      returnValuesSb.append(retval);
      ResultSet rs = oracleStmt.getResultSet();
      rs.next();
      resultsSb.append(rs.getString(1));
      retval = oracleStmt.getMoreResults(Statement.CLOSE_CURRENT_RESULT);
      returnValuesSb.append(retval);
      rs = oracleStmt.getResultSet();
      rs.next();
      resultsSb.append(rs.getString(1));
      retval = oracleStmt.getMoreResults(Statement.CLOSE_ALL_RESULTS);
      returnValuesSb.append(retval);
      rs = oracleStmt.getResultSet();
      rs.next();
      resultsSb.append(rs.getString(1));

      retval = oracleStmt.getMoreResults(Statement.CLOSE_ALL_RESULTS);
      returnValuesSb.append(retval);
      rs = oracleStmt.getResultSet();
      rs.next();
      resultsSb.append(rs.getString(1));

      // No more result, retval should be false
      retval = oracleStmt.getMoreResults(Statement.CLOSE_ALL_RESULTS);
      returnValuesSb.append(retval);

      oracleReturnValues = returnValuesSb.toString();
      oracleResult = resultsSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    // Run REST JDBC Driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();
      StringBuilder returnValuesSb = new StringBuilder();
      StringBuilder resultsSb = new StringBuilder();
      restStmt.execute("begin p; end;");
      boolean retval = restStmt.getMoreResults(Statement.KEEP_CURRENT_RESULT);
      returnValuesSb.append(retval);
      ResultSet rs = restStmt.getResultSet();
      rs.next();
      resultsSb.append(rs.getString(1));
      retval = restStmt.getMoreResults(Statement.CLOSE_CURRENT_RESULT);
      returnValuesSb.append(retval);
      rs = restStmt.getResultSet();
      rs.next();
      resultsSb.append(rs.getString(1));
      retval = restStmt.getMoreResults(Statement.CLOSE_ALL_RESULTS);
      returnValuesSb.append(retval);
      rs = restStmt.getResultSet();
      rs.next();
      resultsSb.append(rs.getString(1));

      retval = restStmt.getMoreResults(Statement.CLOSE_ALL_RESULTS);
      returnValuesSb.append(retval);
      rs = restStmt.getResultSet();
      rs.next();
      resultsSb.append(rs.getString(1));

      // No more result, retval should be false
      retval = restStmt.getMoreResults(Statement.CLOSE_ALL_RESULTS);
      returnValuesSb.append(retval);

      restReturnValues = returnValuesSb.toString();
      restResult = resultsSb.toString();
    } catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
    }

    close(oracleStmt, restStmt);

    assertEquals(oracleReturnValues, restReturnValues);
    assertEquals(oracleResult, restResult);
  }

  @Category(PassTest.class)
  @Test
  public void testUnWrap() {
	try {
		Connection conn = getRestConnection();
		Statement stmt = conn.createStatement();
		stmt.unwrap(this.getClass());
	} catch (RestJdbcNotImplementedException f) {
		assert (f instanceof RestJdbcNotImplementedException);
	} catch (Exception e) {
		fail(e.getLocalizedMessage());
	}
	assert (true);
}

@Category(PassTest.class)
@Test
public void teststmtisWrapperFor() {
	try {
		Connection conn = getRestConnection();
		Statement stmt = conn.createStatement();
		boolean result = stmt.isWrapperFor(this.getClass());
	} catch (RestJdbcNotImplementedException f) {
		assert (f instanceof RestJdbcNotImplementedException);
	} catch (Exception e) {
		fail(e.getLocalizedMessage());
	}
	assert (true);
}

/**
 * Method getMaxFieldSize() in Statement not supported.
 */
@Category(UnsupportedTest.class)
@Test
public void testgetMaxFieldSize() {
	try {
		Connection conn = getRestConnection();
		Statement stmt = conn.createStatement();
		int result = stmt.getMaxFieldSize();
	} catch (RestJdbcNotImplementedException f) {
		assert (f instanceof RestJdbcNotImplementedException);
	} catch (Exception e) {
		fail(e.getLocalizedMessage());
	}
	assert (true);
}

/**
 * stmt.setMaxFieldSize() is currently unsupported in the REST JDBC driver
 */
@Category(UnsupportedTest.class)
@Test
public void testSetMaxFieldSize() {
	try {
		Connection conn = getRestConnection();
		Statement stmt = conn.createStatement();
		stmt.setMaxFieldSize(5);
	} catch (RestJdbcNotImplementedException f) {
		assert (f instanceof RestJdbcNotImplementedException);
	} catch (Exception e) {
		fail(e.getLocalizedMessage());
	}
	assert (true);
}


@Category(PassTest.class)
@Test
public void testgetQueryTimeout()  {
	try {
		Connection conn = getRestConnection();
		Statement stmt = conn.createStatement();
		int o = stmt.getQueryTimeout();
	} catch (RestJdbcNotImplementedException f) {
		assert (f instanceof RestJdbcNotImplementedException);
	} catch (Exception e) {
		fail(e.getLocalizedMessage());
	}
	assert (true);
}

/** (non-Javadoc)
 * @see java.sql.Statement#setQueryTimeout(int)
 */

@Category(PassTest.class)
@Test
public void testSetQueryTimeout() {
	try {
		Connection conn = getRestConnection();
		Statement stmt = conn.createStatement();
		stmt.setQueryTimeout(5);
	} catch (RestJdbcNotImplementedException f) {
		assert (f instanceof RestJdbcNotImplementedException);
	} catch (Exception e) {
		fail(e.getLocalizedMessage());
	}
	assert (true);
}

@Category(PassTest.class)
@Test
public void testunwrap()  {
	try {
		Connection conn = getRestConnection();
		Statement stmt = conn.createStatement();
		stmt.unwrap(this.getClass());
	} catch (RestJdbcNotImplementedException f) {
		assert (f instanceof RestJdbcNotImplementedException);
	} catch (Exception e) {
		fail(e.getLocalizedMessage());
	}
	assert (true);
}

@Category(UnsupportedTest.class)
@Test
public void testsetMaxFieldSize()  {
		try {
			Connection conn = getRestConnection();
			Statement stmt = conn.createStatement();
			stmt.setMaxFieldSize(5);
		} catch (RestJdbcNotImplementedException f) {
			assert (f instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
		assert (true);
}

@Category(PassTest.class)
@Test
public void testcancel()  {
		try {
				Connection conn = getRestConnection();
				Statement stmt = conn.createStatement();
				stmt.cancel();
			} catch (RestJdbcNotImplementedException f) {
				assert (f instanceof RestJdbcNotImplementedException);
			} catch (Exception e) {
				fail(e.getLocalizedMessage());
			}
			assert (true);
}


@Category(PassTest.class)
@Test
public void testsetCursorName()  {
		try {
				Connection conn = getRestConnection();
				Statement stmt = conn.createStatement();
				stmt.setCursorName("BMG");
			} catch (RestJdbcNotImplementedException f) {
				assert (f instanceof RestJdbcNotImplementedException);
			} catch (Exception e) {
				fail(e.getLocalizedMessage());
			}
			assert (true);
}


@Category(PassTest.class)
@Test
public void testgetUpdateCount()  {
		try {
				Connection conn = getRestConnection();
				Statement stmt = conn.createStatement();
				stmt.getUpdateCount();
			} catch (RestJdbcNotImplementedException f) {
				assert (f instanceof RestJdbcNotImplementedException);
			} catch (Exception e) {
				fail(e.getLocalizedMessage());
			}
			assert (true);
}

	@Category(PassTest.class)
	@Test
	public void testGetMoreResults()  {
		try {
				Connection conn = getRestConnection();
				Statement stmt = conn.createStatement();
				stmt.getMoreResults();
			} catch (RestJdbcNotImplementedException f) {
				assert (f instanceof RestJdbcNotImplementedException);
			} catch (Exception e) {
				fail(e.getLocalizedMessage());
			}
			assert (true);
}

	@Category(PassTest.class)
	@Test
	public void testGetMoreResultsInt()  {
		try {
				Connection conn = getRestConnection();
				Statement stmt = conn.createStatement();
				stmt.getMoreResults(1);
			} catch (RestJdbcNotImplementedException f) {
				assert (f instanceof RestJdbcNotImplementedException);
			} catch (Exception e) {
				fail(e.getLocalizedMessage());
			}
			assert (true);
}
/**
 * failed with internal error - internally: 20March 
 */
  @Category(FailTest.class)
  @Test
  public void testExecuteQuery32K() {
    Statement stmt = null;
    boolean wasError=false;
    String theMessage="blank";
      // ORDS credentials
  //    String USER = "debjani";
  //    String PASS ="debjani";    
  //    String driver = "oracle.dbtools.jdbc.Driver"; 
  //    String DB_URL = "http://yourmachine.yourdomain:8082/ords/adhocsqltest/";
      
  //  String USER = "ADHOCSQLTEST";
  //  String PASS = "adhocsqltest";
  //  String driver = "oracle.jdbc.driver.OracleDriver";
  //  String DB_URL = "jdbc:oracle:thin:@yourmachine.yourdomain:1521/orcl";
      
      //Properties cred = new Properties();
      //cred.put("user", USER);    
      //cred.put("password", PASS);
      
      try {
        //Class.forName(driver);
        Connection conn = null;
        try {
          conn = getORestConnection();
        } catch (ClassNotFoundException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          wasError=true;
        }
        //DriverManager.getConnection(DB_URL, cred);
        stmt = conn.createStatement();      

        String sql = "create table tab_varchar32k (\"COL\" varchar2(32767 BYTE))";
        try{
          stmt.execute(sql);
        } catch (Exception e) {
          //e.printStackTrace();
          theMessage+=e.getMessage();;
          
        }
        String str8B = "a\tS:3?&$";
        StringBuilder sb = new StringBuilder(str8B);
        // 8 * 2^12 = 32K
        for (int i = 0; i < 12; i++) {
          sb.append(sb);
        }
        String s = sb.deleteCharAt(0).toString();
        //System.out.println("Debug:"+s.length());
          String sql2 = "insert into tab_varchar32k values ('" + s + "')";
          try {
            stmt.execute(sql2);
            theMessage+="execute insert succeeeded";
          } catch (SQLException e) {
            //e.printStackTrace();
            wasError=true;
            throw e;
          }
        
          String sql_select = "select length(COL) from tab_varchar32k";
          ResultSet rs=null;
          try {
            rs=stmt.executeQuery(sql_select);
            if (rs.next()) {
              //new Exception(rs.getLong(1)+"Debug").printStackTrace();
              theMessage+="Debug Length got:"+rs.getLong(1)+":";
            } else {
              fail("reselect LENGTH(COL) failed - NO ROWS");
            }
          } finally {
            close(rs);
          }
          sql_select = "select COL from tab_varchar32k";
          rs=null;
          try {
            rs=stmt.executeQuery(sql_select);
            if (rs.next()) {
            //new Exception(rs.getString(1).length()+":"+rs.getString(1).substring(0,10)+"...:Debuglengthinjava").printStackTrace();
            theMessage+="Debug Lengthinjava got:"+rs.getString(1).length()+":"+rs.getString(1).substring(0,10)+"...:";
            } else {
              fail("reselect full text getString NO ROWS");
            }
          } finally {
            close(rs);
          }
          
      } 
      catch (Exception e) {
        String debug="";
        if (e instanceof SQLException) {
          debug+=("SQLSTATE: " + ((SQLException)e).getSQLState());
        }
        debug+=("Message:" + e.getMessage());
        fail("In ExceptionCatch:"+debug);
        //e.printStackTrace();
        wasError=true;
        
      } finally {
        String drop = "drop table tab_varchar32k purge";
        try{
          stmt.executeQuery(drop);
        } catch (Exception e){
          wasError=true;
          fail("On Drop:"+e.getMessage());
        }
        close(stmt);
      }
      System.err.println("Debug: StatementTest.executeQueryTest32k:wasError="+wasError+":appendedtothemessage="+theMessage);
     // new Exception(wasError+theMessage).printStackTrace();
  }
  @Override
  protected void tearDown() {
    Connection conn = null;
    Statement stmt = null;
    String forError="Drop tables attempted: "+System.getProperty("num")+"Z: ";
    try {
      conn = getRestConnection();
      stmt = conn.createStatement();
      SQLException throwFirst=null;
      try {
          stmt.execute("drop table STATEMENTTEST_EMP purge");
      } catch (SQLException e) {
          throwFirst=e;
          e.printStackTrace();
      }
      try {
          stmt.execute("drop table STATEMENTTEST_DEPT purge");
      } catch (SQLException e) {
          if (throwFirst==null) {
              throwFirst=e;
          }
          e.printStackTrace();
          throw throwFirst;
      }
      forError+="Succeeded";
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
        System.err.println("Debug: "+forError);
    }
    close(stmt);
  }
}
