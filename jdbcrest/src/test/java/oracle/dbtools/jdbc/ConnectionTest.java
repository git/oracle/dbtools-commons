/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.Properties;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.PassTest;

public class ConnectionTest extends JDBCTestCase {

	public Connection createOracleExpireConnection() throws ClassNotFoundException, SQLException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection connection = null;
		Properties props= new Properties();
		props.put("user", "expiretest");
		props.put("password", "expiretest");
		//really want this setting unless creating java procedures.
		//props.put(OracleConnection.CONNECTION_PROPERTY_PROCESS_ESCAPES, "false");
		connection = DriverManager.getConnection("jdbc:oracle:thin:@yourmachine.yourdomain:1521/orcl", props);
		return connection;
	}
	public Connection createOracleExpireForeverConnection() throws ClassNotFoundException, SQLException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection connection = null;
		Properties props= new Properties();
		props.put("user", "expireforeveruser");
		props.put("password", "expireforeveruser");
		//really want this setting unless creating java procedures.
		//props.put(OracleConnection.CONNECTION_PROPERTY_PROCESS_ESCAPES, "false");
		connection = DriverManager.getConnection("jdbc:oracle:thin:@yourmachine.yourdomain:1521/orcl", props);
		return connection;
	}

	public Connection createRestExpireConnection() throws ClassNotFoundException, SQLException {
		Class.forName("oracle.dbtools.jdbc.Driver");
		Properties cred = new Properties();
		cred.put("user", "debjani");
		cred.put("password", "debjani");
		Connection connection = null;
		connection = DriverManager.getConnection("http://yourmachine.yourdomain:8082/ords/expiretest/", cred);
		return connection;
	}
	public Connection createRestExpireForeverConnection() throws ClassNotFoundException, SQLException {
		Class.forName("oracle.dbtools.jdbc.Driver");
		Properties cred = new Properties();
		cred.put("user", "debjani");
		cred.put("password", "debjani");
		Connection connection = null;
		connection = DriverManager.getConnection("http://yourmachine.yourdomain:8082/ords/expireforeveruser/", cred);
		return connection;
	}
	@Category(PassTest.class)
	@Test
	public void testGetWarnings() {
		Connection oracleConn=null;
		Connection restConn=null;
		try {
			try {
				oracleConn=createOracleExpireForeverConnection();	
			} catch (SQLException|ClassNotFoundException ex) {
				fail("Exception on oracle connection: "+ex.getLocalizedMessage());
			}
			try {
				restConn=createRestExpireForeverConnection();
			} catch (SQLException|ClassNotFoundException ex) {
				fail("Exception on rest connection: "+ex.getLocalizedMessage());
			}	
			if (((oracleConn==null)||(restConn==null))) {
				fail("Expire Connection is null");
			}
			coreGetWarnings(oracleConn, restConn );
		} finally {
			if (oracleConn!=null) {
				try {
					oracleConn.close();
				} catch (SQLException e) {
					fail(e.getMessage());
				}
			}
			if (restConn!=null) {
				try {
					restConn.close();
				} catch (SQLException e) {
					fail(e.getMessage());
				}
			}
		}
	
	}	
	@Category(PassTest.class)
	@Test
	public void testGetWarnings2() {
		Connection oracleConn=null;
		Connection restConn=null;
		try {
			try {
				oracleConn=createOracleExpireConnection();	
			} catch (SQLException|ClassNotFoundException ex) {
				fail("Exception on oracle connection: "+ex.getLocalizedMessage());
			}
			try {
				restConn=createRestExpireConnection();
			} catch (SQLException|ClassNotFoundException ex) {
				fail("Exception on rest connection: "+ex.getLocalizedMessage());
			}	
			if (((oracleConn==null)||(restConn==null))) {
				fail("Expire Connection is null");
			}
			coreGetWarnings(oracleConn, restConn );
		} finally {
			if (oracleConn!=null) {
				try {
					oracleConn.close();
				} catch (SQLException e) {
					fail(e.getMessage());
				}
			}
			if (restConn!=null) {
				try {
					restConn.close();
				} catch (SQLException e) {
					fail(e.getMessage());
				}
			}
		}
	
	}
	public void coreGetWarnings(Connection oracleConnection, Connection restConnection) {
		SQLException restException = null, oracleException = null;
		SQLWarning oracleWarning=null, restWarning=null;
		// run with thin driver
		try {
			oracleWarning=oracleConnection.getWarnings();
		} catch (SQLException e) {
			oracleException = e;
		} 
		// run with rest driver
		try {
			restWarning=restConnection.getWarnings();
		} catch (SQLException e) {
			restException = e;
		} 
		ExceptionDiff("Exceptions are different.", oracleException, restException);
		ExceptionDiff("Returned values are different.", oracleWarning, restWarning); // currently the exception test fails so this wont be hit. but once that is fixed we should test the value

	}
	public void ExceptionDiff(String warn,SQLException one, SQLException two) {
		if ((one==null&&two==null)) {
			return;
		}
		if ((one==null||two==null)) {
			assertEquals(warn,one,two);
		}
		//Note message can end in newline \n \r\n plat dependant: Dermot said ignore for now Dec 2016)
		assertEquals(warn,trimMe(one.getMessage()), trimMe(two.getMessage()));
		assertEquals(warn,trimMe(one.getSQLState()), trimMe(two.getSQLState()));
		assertEquals(warn,one.getErrorCode(), two.getErrorCode());
	}
	public String trimMe(String in) {
		if (in==null) {
			return in;
		}
		return in.trim();
	}
}
