/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.sql.SQLException;

import org.junit.experimental.categories.Category;

import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.FailTest;

public class DatabaseMetaDataTest extends JDBCTestCase {

	@Category(FailTest.class)
	public void testGetURL() throws SQLException {
		String oracleURL = null, restURL = null;
		Exception restException = null, oracleException = null;
		String expectedURL = "http://yourmachine.yourdomain:8082/ords/adhocsqltest/";
		
		// run with thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			java.sql.DatabaseMetaData oracleDatabaseMetaData = oracleConnection.getMetaData();
			oracleURL = oracleDatabaseMetaData.getURL();

		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run with rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			java.sql.DatabaseMetaData restDatabaseMetaData = restConnection.getMetaData();
			restURL = restDatabaseMetaData.getURL();
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals( expectedURL, restURL); // currently the exception test fails so this wont be hit. but once that is fixed we should test the value
	}

	@Category(FailTest.class)
	public void testGetDatabaseProductVersion() throws SQLException {
		String oracleDatabaseProductVersion = null, restDatabaseProductVersion = null;
		Exception restException = null, oracleException = null;
		// run with thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			java.sql.DatabaseMetaData oracleDatabaseMetaData = oracleConnection.getMetaData();
			oracleDatabaseProductVersion = oracleDatabaseMetaData.getDatabaseProductVersion();

		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run with rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			java.sql.DatabaseMetaData restDatabaseMetaData = restConnection.getMetaData();
			restDatabaseProductVersion = restDatabaseMetaData.getDatabaseProductVersion();
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Returned values are different.", oracleDatabaseProductVersion, restDatabaseProductVersion); // currently the exception test fails so this wont be hit. but once that is fixed we should test the value
	}
}
