/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.TimeZone;

import org.junit.experimental.categories.Category;

import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.PassTest;

public class PreparedStatementTest extends JDBCTestCase {

  /*********************  ALL PASS TEST CLASSES *********************/ 
	@Category(PassTest.class)
	public void testSetInt() throws SQLException {
		String sql = "SELECT  ? as col1,  ?  as col2 from dual";
		java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		Exception restException = null, oracleException = null;
		int oracleResult = 0, restResult = 0;
		// run thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			oraclePreparedStatement = oracleConnection.prepareStatement(sql);
			oraclePreparedStatement.setInt(1, 20);
			oraclePreparedStatement.setInt(2, 40);
			oracleRS = oraclePreparedStatement.executeQuery();
			while (oracleRS.next()) {
				oracleResult = oracleRS.getInt(1);
			}
		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restPreparedStatement = restConnection.prepareStatement(sql);
			restPreparedStatement.setInt(1, 20);
			restPreparedStatement.setInt(2, 40);
			restRS = restPreparedStatement.executeQuery();
			while (restRS.next()) {
				restResult = restRS.getInt(1);
			}
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		// compare results
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Next did not move the cursor on.", oracleResult, restResult);
		close(oracleRS, restRS);
		close(oraclePreparedStatement, restPreparedStatement);
	}
	
	
	@Category(PassTest.class)
	public void testSetDate() throws SQLException {
		String sql = "SELECT  ? as col1 from dual";
		java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		Exception restException = null, oracleException = null;
		java.sql.Date oracleResult = null, restResult = null;
		java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
		// run thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			oraclePreparedStatement = oracleConnection.prepareStatement(sql);
			oraclePreparedStatement.setDate(1, sqlDate);
			oracleRS = oraclePreparedStatement.executeQuery();
			while (oracleRS.next()) {
				oracleResult = oracleRS.getDate(1);
			}
		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restPreparedStatement = restConnection.prepareStatement(sql);
			restPreparedStatement.setDate(1, sqlDate);
			restRS = restPreparedStatement.executeQuery();
			while (restRS.next()) {
				restResult = restRS.getDate(1);
			}
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		// compare results
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Next did not move the cursor on.", oracleResult.getTime(), restResult.getTime());
		close(oracleRS, restRS);
		close(oraclePreparedStatement, restPreparedStatement);
	}

	
	@Category(PassTest.class)
	public void testSetObject() throws SQLException {
		String sql = "SELECT  ? as col1 from dual";
		java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		Exception restException = null, oracleException = null;
		Object oracleResult = null, restResult = null;
		java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
		// run thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			oraclePreparedStatement = oracleConnection.prepareStatement(sql);
			oraclePreparedStatement.setObject(1, sqlDate);
			oracleRS = oraclePreparedStatement.executeQuery();
			while (oracleRS.next()) {
				oracleResult = oracleRS.getObject(1);
			}
		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restPreparedStatement = restConnection.prepareStatement(sql);
			restPreparedStatement.setObject(1, sqlDate);
			restRS = restPreparedStatement.executeQuery();
			while (restRS.next()) {
				restResult = restRS.getObject(1);
			}
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		// compare results
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Next did not move the cursor on.", ((java.sql.Timestamp) oracleResult).getTime(), ((java.sql.Timestamp) restResult).getTime());
		close(oracleRS, restRS);
		close(oraclePreparedStatement, restPreparedStatement);
	}

	@Category(PassTest.class)
	public void testSetTime() throws SQLException {
		String sql = "SELECT  ? as col1 from dual";
		java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		Exception restException = null, oracleException = null;
		Object oracleResult = null, restResult = null;
		java.sql.Time sqlTime = new java.sql.Time(2, 2, 2);
		// run thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			oraclePreparedStatement = oracleConnection.prepareStatement(sql);
			oraclePreparedStatement.setTime(1, sqlTime);
			oracleRS = oraclePreparedStatement.executeQuery();
			while (oracleRS.next()) {
				oracleResult = oracleRS.getObject(1);
			}
		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restPreparedStatement = restConnection.prepareStatement(sql);
			restPreparedStatement.setTime(1, sqlTime);
			restRS = restPreparedStatement.executeQuery();
			while (restRS.next()) {
				restResult = restRS.getObject(1);
				System.out.println("Debjani: " + restResult);
			}
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		// compare results
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Next did not move the cursor on.", ((java.sql.Timestamp) oracleResult).getTime(), ((java.sql.Timestamp) restResult).getTime());
		close(oracleRS, restRS);
		close(oraclePreparedStatement, restPreparedStatement);
	}
	
	/**
	 * tests setAsciiStream(int parameterIndex, InputStream x, int length)
	 * 
	 * InputStream is created from a string and PreparedStatement is used to 
	 * set the stream. 
	 * Returns a ResultSet from PreparedStatement executeQuery.
	 * Compare the results of getObject() from ResultSet.
	 * 
	 * @throws SQLException
	 */
	@Category(PassTest.class)
  public void testSetAsciiStreamIntLength() throws SQLException {
    String sql = "SELECT  ? as col_stream from dual";
    
    String phrase = "Let me introduce you to that leg of mutton, said the Red Queen. Alice, Mutton. Mutton, Alice.";

    java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
    java.sql.ResultSet restRS = null, oracleRS = null;
    Exception restException = null, oracleException = null;
    Object oracleResult = null, restResult = null;
    int length = 25;
    // run thin driver
    try {
      java.sql.Connection oracleConnection = getOracleConnection();
      oraclePreparedStatement = oracleConnection.prepareStatement(sql); 
      
      // Create the stream
      byte[] bytes = phrase.getBytes(StandardCharsets.UTF_8);
      InputStream is = (InputStream) new ByteArrayInputStream(bytes);
      
      oraclePreparedStatement.setAsciiStream(1, is, length);
      oracleRS = oraclePreparedStatement.executeQuery();
      while (oracleRS.next()) {
        oracleResult = oracleRS.getObject(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      oracleException = e;
    }
    // run rest driver
    try {
      java.sql.Connection restConnection = getRestConnection();
      restPreparedStatement = restConnection.prepareStatement(sql);
      
      // Create the stream
      byte[] bytes = phrase.getBytes(StandardCharsets.UTF_8);
      InputStream is = (InputStream) new ByteArrayInputStream(bytes);
      
      restPreparedStatement.setAsciiStream(1, is, length);
      restRS = restPreparedStatement.executeQuery();
      while (restRS.next()) {
        restResult = restRS.getObject(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      restException = e;
    }
    // compare results
    assertEquals("Exceptions are different.", oracleException, restException);
    assertEquals("Next did not move the cursor on.", oracleResult, restResult);
    close(oracleRS, restRS);
    close(oraclePreparedStatement, restPreparedStatement);
  }
	
	/**
   * tests setAsciiStream(int parameterIndex, InputStream x, long length)
   * 
   * InputStream is created from a string and PreparedStatement is used to 
   * set the stream. 
   * Returns a ResultSet from PreparedStatement executeQuery.
   * Compare the results of getObject() from ResultSet.
   * 
   * @throws SQLException
   */
  @Category(PassTest.class)
  public void testSetAsciiStreamLongLength() throws SQLException {
    String sql = "SELECT  ? as col_stream from dual";
    
    String phrase = "Let me introduce you to that leg of mutton, said the Red Queen. Alice, Mutton. Mutton, Alice.";

    java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
    java.sql.ResultSet restRS = null, oracleRS = null;
    Exception restException = null, oracleException = null;
    Object oracleResult = null, restResult = null;
    long length = 250000000;
    // run thin driver
    try {
      java.sql.Connection oracleConnection = getOracleConnection();
      oraclePreparedStatement = oracleConnection.prepareStatement(sql); 
      
      // Create the stream
      byte[] bytes = phrase.getBytes(StandardCharsets.UTF_8);
      InputStream is = (InputStream) new ByteArrayInputStream(bytes);
      
      oraclePreparedStatement.setAsciiStream(1, is, length);
      oracleRS = oraclePreparedStatement.executeQuery();
      while (oracleRS.next()) {
        oracleResult = oracleRS.getObject(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      oracleException = e;
    }
    // run rest driver
    try {
      java.sql.Connection restConnection = getRestConnection();
      restPreparedStatement = restConnection.prepareStatement(sql);
      
      // Create the stream
      byte[] bytes = phrase.getBytes(StandardCharsets.UTF_8);
      InputStream is = (InputStream) new ByteArrayInputStream(bytes);
      
      restPreparedStatement.setAsciiStream(1, is, length);
      restRS = restPreparedStatement.executeQuery();
      while (restRS.next()) {
        restResult = restRS.getObject(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      restException = e;
    }
    // compare results
    assertEquals("Exceptions are different.", oracleException, restException);
    assertEquals("Next did not move the cursor on.", oracleResult, restResult);
    close(oracleRS, restRS);
    close(oraclePreparedStatement, restPreparedStatement);
  }
  
  @Category(PassTest.class)
  public void testSetDate_cal() throws SQLException {
    String sql = "SELECT  ? as col1 from dual";
    java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
    java.sql.ResultSet restRS = null, oracleRS = null;
    Exception restException = null, oracleException = null;
    java.sql.Date oracleResult = null, restResult = null;
    java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
    
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    // run thin driver
    try {
      java.sql.Connection oracleConnection = getOracleConnection();
      oraclePreparedStatement = oracleConnection.prepareStatement(sql);
      oraclePreparedStatement.setDate(1, sqlDate, cal);
      oracleRS = oraclePreparedStatement.executeQuery();
      while (oracleRS.next()) {
        oracleResult = oracleRS.getDate(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      oracleException = e;
    }
    // run rest driver
    try {
      java.sql.Connection restConnection = getRestConnection();
      restPreparedStatement = restConnection.prepareStatement(sql);
      restPreparedStatement.setDate(1, sqlDate,cal);
      restRS = restPreparedStatement.executeQuery();
      while (restRS.next()) {
        restResult = restRS.getDate(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      restException = e;
    }
    // compare results
    assertEquals(oracleException, restException);
    assertEquals(oracleResult,restResult);
    close(oracleRS, restRS);
    close(oraclePreparedStatement, restPreparedStatement);
  }

  @Category(PassTest.class)
  public void testSetTime_cal1() throws SQLException {
    String sql = "SELECT  ? as col1 from dual";
    java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
    java.sql.ResultSet restRS = null, oracleRS = null;
    Exception restException = null, oracleException = null;
    java.sql.Time oracleResult = null, restResult = null;
    java.sql.Time sqlTime = java.sql.Time.valueOf("08:48:42");
    
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    // run thin driver
    try {
      java.sql.Connection oracleConnection = getOracleConnection();
      oraclePreparedStatement = oracleConnection.prepareStatement(sql);
      oraclePreparedStatement.setTime(1, sqlTime, cal);
      oracleRS = oraclePreparedStatement.executeQuery();
      while (oracleRS.next()) {
        oracleResult = oracleRS.getTime(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      oracleException = e;
    }
    // run rest driver
    try {
      java.sql.Connection restConnection = getRestConnection();
      restPreparedStatement = restConnection.prepareStatement(sql);
      restPreparedStatement.setTime(1, sqlTime,cal);
      restRS = restPreparedStatement.executeQuery();
      while (restRS.next()) {
        restResult = restRS.getTime(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      restException = e;
    }
    // compare results
    assertEquals(oracleException, restException);
    assertEquals(oracleResult,restResult);
    close(oracleRS, restRS);
    close(oraclePreparedStatement, restPreparedStatement);
  }

  @Category(PassTest.class)
  public void testSetTime_cal2() throws SQLException {
    String sql = "SELECT  ? as col1 from dual";
    java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
    java.sql.ResultSet restRS = null, oracleRS = null;
    Exception restException = null, oracleException = null;
    java.sql.Time oracleResult = null, restResult = null;
    java.sql.Time sqlTime = java.sql.Time.valueOf("19:05:45");
    
    // run thin driver
    try {
      java.sql.Connection oracleConnection = getOracleConnection();
      oraclePreparedStatement = oracleConnection.prepareStatement(sql);
      oraclePreparedStatement.setTime(1, sqlTime);
      oracleRS = oraclePreparedStatement.executeQuery();
      while (oracleRS.next()) {
        oracleResult = oracleRS.getTime(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      oracleException = e;
    }
    // run rest driver
    try {
      java.sql.Connection restConnection = getRestConnection();
      restPreparedStatement = restConnection.prepareStatement(sql);
      restPreparedStatement.setTime(1, sqlTime);
      restRS = restPreparedStatement.executeQuery();
      while (restRS.next()) {
        restResult = restRS.getTime(1);
      }
    } catch (SQLException | ClassNotFoundException e) {
      restException = e;
    }
    // compare results
    assertEquals(oracleException, restException);
    assertEquals(oracleResult,restResult);
    close(oracleRS, restRS);
    close(oraclePreparedStatement, restPreparedStatement);
  }

  
  @Category(PassTest.class)
  public void testSetTimestamp_cal1() throws SQLException {
    String sql = "SELECT  ? as col1 from dual";
    java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
    java.sql.ResultSet restRS = null, oracleRS = null;
    Exception restException = null, oracleException = null;
    long oracleResult = 0, restResult = 0;
    java.sql.Timestamp sqlTimestamp =java.sql.Timestamp.valueOf("2017-01-02 18:06:31");
    
    // run thin driver
    try {
      java.sql.Connection oracleConnection = getOracleConnection();
      oraclePreparedStatement = oracleConnection.prepareStatement(sql);
      oraclePreparedStatement.setTimestamp(1, sqlTimestamp);
      oracleRS = oraclePreparedStatement.executeQuery();
      while (oracleRS.next()) {
        oracleResult = oracleRS.getTimestamp(1).getTime();
      }
    } catch (SQLException | ClassNotFoundException e) {
      oracleException = e;
    }
    // run rest driver
    try {
      java.sql.Connection restConnection = getRestConnection();
      restPreparedStatement = restConnection.prepareStatement(sql);
      restPreparedStatement.setTimestamp(1, sqlTimestamp);
      restRS = restPreparedStatement.executeQuery();
      while (restRS.next()) {
        restResult = restRS.getTimestamp(1).getTime();
      }
    } catch (SQLException | ClassNotFoundException e) {
      restException = e;
    }
    // compare results
    assertEquals(oracleException, restException);
    assertEquals(oracleResult,restResult);
    close(oracleRS, restRS);
    close(oraclePreparedStatement, restPreparedStatement);
  }

	/*********************  ALL FAIL TEST CLASSES *********************/ 
	
	
}