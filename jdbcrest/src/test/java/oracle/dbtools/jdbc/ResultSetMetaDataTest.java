/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.FailTest;
import oracle.dbtools.unittest.categories.PassTest;

public class ResultSetMetaDataTest extends JDBCTestCase {

	@Category(PassTest.class)
	@Test
	public void testGetColumnCount() {
		String sql = "select 1 as col1 from dual";
		java.sql.Statement oracleStatement = null, restStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		ResultSetMetaData oracleResultSetMetaData = null, restResultSetMetaData = null;
		Exception restException = null, oracleException = null;
		int restValue = 0, oracleValue = 0;
		// run thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			oracleStatement = oracleConnection.createStatement();
			oracleRS = oracleStatement.executeQuery(sql);
			oracleResultSetMetaData = oracleRS.getMetaData();
			oracleValue = oracleResultSetMetaData.getColumnCount();
		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restStatement = restConnection.createStatement();
			restRS = restStatement.executeQuery(sql);
			restResultSetMetaData = restRS.getMetaData();
			restValue = restResultSetMetaData.getColumnCount();
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		// compare results
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Next did not move the cursor on.", oracleValue, restValue);
		close(oracleRS, restRS);
		close(oracleStatement, restStatement);
	}

	/**
	 * Rest-Enabled SQL Service does not support duplicate column names
	 */
	@Category(PassTest.class)
	@Test
	public void testGetColumnCount_duplicateColumnNames() {
		String sql = "select 1 as col1, 2 as col1 from dual";
		java.sql.Statement oracleStatement = null, restStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		ResultSetMetaData restResultSetMetaData = null;
		SQLException restException = null;
		String expectedExceptiongMsg = "ORA-00918: column ambiguously defined";
		int expectedErrorCode = 918;
		int expectedSQLState  =  42000;
		// run rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restStatement = restConnection.createStatement();
			restRS = restStatement.executeQuery(sql);
			restResultSetMetaData = restRS.getMetaData();
			int restValue = restResultSetMetaData.getColumnCount();
		} catch (SQLException e) {
			  restException = e;
		} catch (ClassNotFoundException e) {
        fail(e.getMessage());
    }
		// compare results
		assertEquals(expectedExceptiongMsg,  restException.getMessage());
		assertEquals(expectedErrorCode, restException.getErrorCode());
		assertEquals("42000", restException.getSQLState());
		close(oracleRS, restRS);
		close(oracleStatement, restStatement);
	}

	/**
	 * Fails as ColumnTypeName when used with a Time. Oracle Thin returns DATE . REST returns TIMESTAMP
	 */
	@Category(FailTest.class)
	@Test
	public void testGetColumnTypeName() {
		String sql = "select ? as col1, ? as col2 from dual";
		java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		ResultSetMetaData oracleResultSetMetaData = null, restResultSetMetaData = null;
		Exception restException = null, oracleException = null;

		java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Time sqlTime = new java.sql.Time(2, 2, 2);

		ArrayList<String> restColumnNames = new ArrayList<String>(), oracleColumnNames = new ArrayList<String>();
		// run thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			oraclePreparedStatement = oracleConnection.prepareStatement(sql);
			oraclePreparedStatement.setDate(1, sqlDate);
			oraclePreparedStatement.setTime(2, sqlTime);
			oracleRS = oraclePreparedStatement.executeQuery();
			oracleResultSetMetaData = oracleRS.getMetaData();
			oracleColumnNames.add(oracleResultSetMetaData.getColumnTypeName(1));
			oracleColumnNames.add(oracleResultSetMetaData.getColumnTypeName(2));
		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restPreparedStatement = restConnection.prepareStatement(sql);
			restPreparedStatement.setDate(1, sqlDate);
			restPreparedStatement.setTime(2, sqlTime);
			restRS = restPreparedStatement.executeQuery();
			restResultSetMetaData = restRS.getMetaData();
			restColumnNames.add(restResultSetMetaData.getColumnTypeName(1));
			restColumnNames.add(restResultSetMetaData.getColumnTypeName(2));
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		// compare results
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Column Type Names are different", oracleColumnNames, restColumnNames);
		close(oracleRS, restRS);
		close(oraclePreparedStatement, restPreparedStatement);
	}

	@Category(PassTest.class)
	@Test
	public void testGetColumnClassName() {
		String sql = "select ? as col1, ? as col2 from dual";
		java.sql.PreparedStatement oraclePreparedStatement = null, restPreparedStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		ResultSetMetaData oracleResultSetMetaData = null, restResultSetMetaData = null;
		Exception restException = null, oracleException = null;

		java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Time sqlTime = new java.sql.Time(2, 2, 2);

		ArrayList<String> restColumnNames = new ArrayList<String>(), oracleColumnNames = new ArrayList<String>();
		// run thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			oraclePreparedStatement = oracleConnection.prepareStatement(sql);
			oraclePreparedStatement.setDate(1, sqlDate);
			oraclePreparedStatement.setTime(2, sqlTime);
			oracleRS = oraclePreparedStatement.executeQuery();
			oracleResultSetMetaData = oracleRS.getMetaData();
			oracleColumnNames.add(oracleResultSetMetaData.getColumnClassName(1));
			oracleColumnNames.add(oracleResultSetMetaData.getColumnClassName(2));
		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restPreparedStatement = restConnection.prepareStatement(sql);
			restPreparedStatement.setDate(1, sqlDate);
			restPreparedStatement.setTime(2, sqlTime);
			restRS = restPreparedStatement.executeQuery();
			restResultSetMetaData = restRS.getMetaData();
			restColumnNames.add(restResultSetMetaData.getColumnClassName(1));
			restColumnNames.add(restResultSetMetaData.getColumnClassName(2));
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		// compare results
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Column Class Names are different", oracleColumnNames, restColumnNames);
		close(oracleRS, restRS);
		close(oraclePreparedStatement, restPreparedStatement);
	}

}
