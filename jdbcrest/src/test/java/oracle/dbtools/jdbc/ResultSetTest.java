/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.FailTest;
import oracle.dbtools.unittest.categories.PassTest;
import oracle.dbtools.unittest.categories.ScratchTest;
import oracle.jdbc.OracleResultSet;

public class ResultSetTest extends JDBCTestCase {

	//bound to fail clob has issues String theSelect="select v,n, c /*clob*/ from orestResultSet order by ob";
	public String theSelect="select v, n, c /*clob*/, ob from orestResultSet order by ob";
	//null fails {"create table orestresultset( oB int, v varchar2(1000), n number, c clob)","insert into orestresultset values ( 1, null, null, null)","insert into orestresultset values ( 2,'string', 12.34, empty_clob())","insert into orestresultset values ( 3,'string2', 678.00, 'something')"}
	public String[] theSetup={"create table orestresultset( oB int, v varchar2(1000), n number, c clob)","insert into orestresultset values ( 1, null, null, null)","insert into orestresultset values ( 2,'string', 12.34, empty_clob())","insert into orestresultset values ( 3,'string2', 678.00, 'something')"};
	//null fails {"create table orestresultset( oB int, v varchar2(1000), n number, c clob)","insert into orestresultset values ( 1, null, null, null)","insert into orestresultset values ( 2,'string', 12.34, empty_clob())","insert into orestresultset values ( 3,'string2', 678.00, 'something')"}
	public String[] theSetupNoNull={"create table orestresultset( oB int, v varchar2(1000), n number, c clob)"/*,"insert into orestresultset values ( 1, null, null, null)"*/,"insert into orestresultset values ( 2,'string', 12.34, empty_clob())","insert into orestresultset values ( 3,'string2', 678.00, 'something')"};

	
	@Category(ScratchTest.class)
	@Test
	public void testAbsolute() {
		String sql = "select * from dual";
		java.sql.Statement oracleStatement = null, restStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		ResultSetMetaData oracleResultSetMetaData = null, restResultSetMetaData = null;
		SQLException restException = null, oracleException = null;
		boolean restResult = false, oracleResult = false;

		// run with thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			oracleStatement = oracleConnection.createStatement();
			oracleRS = oracleStatement.executeQuery(sql);
      try {
        oracleResult = oracleRS.absolute(1);
      } catch (SQLException e) {
        oracleException = e;
      }
		} catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
		}
		// run with rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restStatement = restConnection.createStatement();
			restRS = restStatement.executeQuery(sql);
      try {
        restResult = restRS.absolute(1);
      } catch (SQLException e) {
        restException = e;
      }
		} catch (SQLException | ClassNotFoundException e) {
      fail(e.getMessage());
		}
		assertEquals(oracleException.getMessage(), restException.getMessage());
    assertEquals(oracleException.getSQLState(), restException.getSQLState());
    assertEquals(oracleException.getErrorCode(), restException.getErrorCode());
		//assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Returned values are different.", oracleResult, restResult); // currently the exception test fails so this wont be hit. but once that is fixed we should test the value
		close(oracleRS, restRS);
		close(oracleStatement, restStatement);
	}

	@Category(PassTest.class)
	@Test
	public void testNext() {
		String sql = "with x as (select 1  as col1 from dual union select 2 as col1 from dual) select col1 from x order by col1 asc";
		java.sql.Statement oracleStatement = null, restStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		ResultSetMetaData oracleResultSetMetaData = null, restResultSetMetaData = null;
		Exception restException = null, oracleException = null;
		int restValue = 0, oracleValue = 0;
		boolean restResult = false, oracleResult = false;
		// run thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			oracleStatement = oracleConnection.createStatement();
			oracleRS = oracleStatement.executeQuery(sql);
			oracleResult = oracleRS.next();
			oracleResult = oracleRS.next();
			oracleValue = oracleRS.getInt(1);// should be 2
		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restStatement = restConnection.createStatement();
			restRS = restStatement.executeQuery(sql);
			restResult = restRS.next();
			restResult = restRS.next();
			restValue = restRS.getInt(1);// should be 2
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		// compare results
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Returned values are different.", oracleResult, restResult);
		assertEquals("Next did not move the cursor on.", oracleValue, restValue);
		close(oracleRS, restRS);
		close(oracleStatement, restStatement);
	}

	@Category(PassTest.class)
	@Test
	public void testNext_tillFalse() {
		String sql = "with x as (select 1  as col1 from dual union select 2 as col1 from dual) select col1 from x order by col1 asc";
		java.sql.Statement oracleStatement = null, restStatement = null;
		java.sql.ResultSet restRS = null, oracleRS = null;
		ResultSetMetaData oracleResultSetMetaData = null, restResultSetMetaData = null;
		Exception restException = null, oracleException = null;
		int restValue = 0, oracleValue = 0;
		boolean restResult = false, oracleResult = false;
		// run thin driver
		try {
			java.sql.Connection oracleConnection = getOracleConnection();
			oracleStatement = oracleConnection.createStatement();
			oracleRS = oracleStatement.executeQuery(sql);
			oracleResult = oracleRS.next();
			oracleResult = oracleRS.next();
			oracleResult = oracleRS.next();// should return false
		} catch (SQLException | ClassNotFoundException e) {
			oracleException = e;
		}
		// run rest driver
		try {
			java.sql.Connection restConnection = getRestConnection();
			restStatement = restConnection.createStatement();
			restRS = restStatement.executeQuery(sql);
			restResult = restRS.next();
			restResult = restRS.next();
			restResult = restRS.next(); // should return false
		} catch (SQLException | ClassNotFoundException e) {
			restException = e;
		}
		// compare results
		assertEquals("Exceptions are different.", oracleException, restException);
		assertEquals("Returned values are different.", oracleResult, restResult);
		close(oracleRS, restRS);
		close(oracleStatement, restStatement);
	}
	
	
	/**
	 * Tests the getTimestamp(int columnIndex, Calendar cal) method in ResultSet
	 * Compares the millisecond value of the timestamps
	 */
	@Category(PassTest.class)
  @Test
  public void testgetTimestamp_cal() {
    String sql = "SELECT TO_DATE('2017/01/25 8:30:25', 'YYYY/MM/DD HH:MI:SS') FROM dual";

    Calendar cal= new GregorianCalendar(TimeZone.getTimeZone("PST8PDT"));
    
    java.sql.Statement oracleStatement = null, restStatement = null;
    java.sql.ResultSet restRS = null, oracleRS = null;
    Exception restException = null, oracleException = null;
    long restValue = 0, oracleValue = 0;
    boolean restResult = false, oracleResult = false;
    // run thin driver
    try {
      java.sql.Connection oracleConnection = getOracleConnection();
      oracleStatement = oracleConnection.createStatement();
      oracleRS = oracleStatement.executeQuery(sql);
      oracleResult = oracleRS.next();
      oracleValue = oracleRS.getTimestamp(1, cal).getTime();
    } catch (SQLException | ClassNotFoundException e) {
      oracleException = e;
    }
    // run rest driver
    try {
      java.sql.Connection restConnection = getRestConnection();
      restStatement = restConnection.createStatement();
      restRS = restStatement.executeQuery(sql);
      restResult = restRS.next();
      restValue = restRS.getTimestamp(1, cal).getTime();
    } catch (SQLException | ClassNotFoundException e) {
      restException = e;
    }
    // compare results
    assertEquals(oracleValue, restValue);
    assertEquals(oracleException, restException);
    assertEquals(oracleResult, restResult);
    
    close(oracleRS, restRS);
    close(oracleStatement, restStatement);
  }
	
	
	/**
   * Tests the getTimestamp(String columnIndex, Calendar cal) method in ResultSet
   * Compares the millisecond value of the timestamps
   */
  @Category(PassTest.class)
  @Test
  public void testgetTimestamp_cal2() {
    String select = "select to_timestamp('17-JAN-2017 08.48.42.861031', 'dd-MM-yyyy hh24:mi:ss.FF') as col1 from dual";
    Calendar cal= new GregorianCalendar(TimeZone.getTimeZone("PST8PDT"));

    Connection restConn = null, orclConn = null;
    Statement restStmt = null, orclStmt = null;
    ResultSet oracleRs = null, restRs = null;
    
    Exception restException = null, oracleException = null;
    long restVal = 0, oracleVal = 0;
    boolean restResult = false, oracleResult = false;
    // run thin driver
    try {
      orclConn = getOracleConnection();
      orclStmt = orclConn.createStatement();
      oracleRs = orclStmt.executeQuery(select);
      oracleResult = oracleRs.next();
      oracleVal = oracleRs.getTimestamp("col1", cal).getTime();
    } catch (SQLException | ClassNotFoundException e) {
      oracleException = e;
    } 
    // run rest driver
    try {
      restConn = getRestConnection();
      restStmt = restConn.createStatement();      
      restRs = restStmt.executeQuery(select);
      restResult = restRs.next();
      restVal = restRs.getTimestamp("col1", cal).getTime();
    } catch (SQLException | ClassNotFoundException e) {
      restException = e;
    }
    // compare results
    assertEquals(oracleVal, restVal); // compare the long value of timestamps
    assertEquals(oracleException, restException);
    assertEquals(oracleResult, restResult); 
    
    close(oracleRs, restRs);
    close(orclStmt, restStmt);
  }
	
  /**
   * Tests the getTime(int columnIndex, Calendar cal) method in ResultSet
   * 
   */
  @Category(PassTest.class)
  @Test
  public void testgetTimes_cal1() {
    
    String select = "select to_date('17-JAN-2017 08.48.42', 'dd-MM-yyyy hh24:mi:ss') from dual";
    Calendar cal= new GregorianCalendar(TimeZone.getTimeZone("PST8PDT"));
 
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    
    Exception restException = null, oracleException = null;
    long restVal = 0, oracleVal = 0;
    boolean restResult = false, oracleResult = false;
    // run thin driver
    try {
      java.sql.Connection oracleConnection = getOracleConnection();
      oracleStmt = oracleConnection.createStatement();
      oracleRs = oracleStmt.executeQuery(select);
      oracleResult = oracleRs.next();
      oracleVal = oracleRs.getTime(1, cal).getTime();
    } catch (SQLException | ClassNotFoundException e) {
      oracleException = e;
    }
    // run rest driver
    try {
      java.sql.Connection restConnection = getRestConnection();
      restStmt = restConnection.createStatement();
      restRs = restStmt.executeQuery(select);
      restResult = restRs.next();
      restVal = restRs.getTime(1, cal).getTime();
    } catch (SQLException | ClassNotFoundException e) {
      restException = e;
    }
    // compare results
    assertEquals(oracleVal, restVal); // compare the long value of timestamps
    assertEquals(oracleException, restException);
    assertEquals(oracleResult, restResult); 
    
    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
  }
  
  
  
  /**
   * Tests the getDate(int columnIndex, Calendar cal) method in ResultSet
   * 
   */
  @Category(PassTest.class)
  @Test
  public void testgetDates_cal1() {
    
    String select = "select to_date('17-JAN-2017 08.48.42', 'dd-MM-yyyy hh24:mi:ss') from dual";
    Calendar cal= new GregorianCalendar(TimeZone.getTimeZone("PST8PDT"));
 
    Statement oracleStmt = null, restStmt = null;
    ResultSet oracleRs = null, restRs = null;
    
    Exception restException = null, oracleException = null;
    long restVal = 0, oracleVal = 0;
    boolean restResult = false, oracleResult = false;
    // run thin driver
    try {
      java.sql.Connection oracleConnection = getOracleConnection();
      oracleStmt = oracleConnection.createStatement();
      oracleRs = oracleStmt.executeQuery(select);
      oracleResult = oracleRs.next();
      oracleVal = oracleRs.getDate(1, cal).getTime();
    } catch (SQLException | ClassNotFoundException e) {
      oracleException = e;
    }
    // run rest driver
    try {
      java.sql.Connection restConnection = getRestConnection();
      restStmt = restConnection.createStatement();
      restRs = restStmt.executeQuery(select);
      restResult = restRs.next();
      restVal = restRs.getDate(1, cal).getTime();
    } catch (SQLException | ClassNotFoundException e) {
      restException = e;
    }
    // compare results
    assertEquals(oracleVal, restVal); // compare the long value of timestamps
    assertEquals(oracleException, restException);
    assertEquals(oracleResult, restResult); 
    
    close(oracleRs, restRs);
    close(oracleStmt, restStmt);
  }
  
  
	@Category(PassTest.class)
	@Test
	public void testGetObject() {
		coreGetObject(theSetup,theSelect);
	}
	
	@Category(PassTest.class)
	@Test
	public void testGetObjectNoNull() {
		coreGetObject(theSetupNoNull, theSelect);
	}
	public void coreGetObject(String[] theSetup, String theSelect){
		Connection oracleConn=null;
		Connection restConn=null;
		Statement oracleStmt=null;
		Statement restStmt=null;
		ResultSet oracleResultSet=null;
		ResultSet restResultSet=null;
		ArrayList<Object> oracleDatum=new ArrayList<Object>();//2x3
		ArrayList<Object> restDatum=new ArrayList<Object>();//2x3
		try {
			String[] toCall1=theSetup;//{"create table orestresultset( oB int, v varchar2(1000), n number, c clob)","insert into orestresultset values ( 1, null, null, null)","insert into orestresultset values ( 2,'string', 12.34, empty_clob())","insert into orestresultset values ( 3,'string2', 678.00, 'something')"};
			//String theSelect="select v,n /*, c clob*/ from orestResultSet order by ob";
			try {
				oracleConn=getOracleConnection();
				createDropCommon(oracleConn, toCall1);
				oracleStmt=oracleConn.createStatement();
				oracleResultSet=(OracleResultSet)oracleStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=oracleResultSet.getMetaData();
				while (oracleResultSet.next()) {
					for (int colcount=1;colcount<(rsmd.getColumnCount()+1);colcount++){
						oracleDatum.add(oracleResultSet.getObject(colcount));
					}
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				fail(e.getMessage());
			}
			try {
				restConn=getRestConnection();
				//oracleConn points to restConn createCommon(restConn, toCall1);
				restStmt=restConn.createStatement();
				restResultSet=restStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=restResultSet.getMetaData();
				while (restResultSet.next()) {
					for (int colcount=1;colcount<(rsmd.getColumnCount()+1);colcount++){
						restDatum.add(restResultSet.getObject(colcount));
					}
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(e.getMessage());
			}
			//check datum class
			assertEquals("differentObject returns", oracleDatum.size(), restDatum.size());
			for (int alcount=0;alcount<oracleDatum.size();alcount++) {
				//compare class returns
				assertEquals("Object null check "+alcount,oracleDatum.get(alcount)==null,restDatum.get(alcount)==null );
				objectCheck("Object check "+alcount,oracleDatum.get(alcount),restDatum.get(alcount));
				//assertEquals("datum equals check"+alcount,oracleDatum.get(alcount).getClass(),restDatum.get(alcount));
			}
		} finally {
			String[] toCall={"drop table orestresultset"};
			try {
				createDropCommon(oracleConn, toCall);
			} catch (Exception e) {
				e.printStackTrace();
			}
			close(oracleResultSet,restResultSet);
			close(oracleStmt,restStmt);
		}
	}
	
	@Category(FailTest.class)
	@Test
	public void testGetClob() {
		coreGetClob(theSetup, theSelect);
	}
	
	@Category(PassTest.class)
	@Test
	public void testGetClobNoNull() {
		coreGetClob(theSetupNoNull, theSelect);
	}
	
	public void coreGetClob(String[] theSetup, String theSelect){
		Connection oracleConn=null;
		Connection restConn=null;
		Statement oracleStmt=null;
		Statement restStmt=null;
		OracleResultSet oracleResultSet=null;
		ResultSet restResultSet=null;
		ArrayList<Object> oracleDatum=new ArrayList<Object>();//2x3
		ArrayList<Object> restDatum=new ArrayList<Object>();//2x3
		try {
			String[] toCall1=theSetup;//{"create table orestresultset( oB int, v varchar2(1000), n number, c clob)","insert into orestresultset values ( 1, null, null, null)","insert into orestresultset values ( 2,'string', 12.34, empty_clob())","insert into orestresultset values ( 3,'string2', 678.00, 'something')"};
			
			try {
				oracleConn=getOracleConnection();
				createDropCommon(oracleConn, toCall1);
				oracleStmt=oracleConn.createStatement();
				oracleResultSet=(OracleResultSet)oracleStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=oracleResultSet.getMetaData();
				while (oracleResultSet.next()) {
					oracleDatum.add(oracleResultSet.getClob(3));
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				fail(e.getMessage());
			}
			try {
				restConn=getRestConnection();
				//oracleConn points to restConn createCommon(restConn, toCall1);
				restStmt=restConn.createStatement();
				restResultSet=restStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=restResultSet.getMetaData();
				while (restResultSet.next()) {
					restDatum.add(restResultSet.getClob(3));
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(e.getMessage());
			}
			//check datum class
			assertEquals("different Object returns", oracleDatum.size(), restDatum.size());
			for (int alcount=0;alcount<oracleDatum.size();alcount++) {
				//compare class returns
				assertEquals("Object null checK"+alcount,oracleDatum.get(alcount)==null,restDatum.get(alcount)==null );
				objectCheck("Object check"+alcount,oracleDatum.get(alcount),restDatum.get(alcount));
				//assertEquals("datum equals check"+alcount,oracleDatum.get(alcount).getClass(),restDatum.get(alcount));
			}
		} finally {
			String[] toCall={"drop table orestresultset"};
			try {
				createDropCommon(oracleConn, toCall);
			} catch (Exception e) {
				e.printStackTrace();
			}
			close(oracleResultSet,restResultSet);
			close(oracleStmt,restStmt);
		}
	}
	public String classNameOrNull(Object o) {
		if (o==null) {
			return "null";
		}
		return o.getClass().getName();
	}
	@SuppressWarnings("deprecation")
	public void objectCheck(String st, Object d, Object d2) {
		if (d instanceof Clob) {
			assertEquals(st, d instanceof Clob, d2 instanceof Clob);
			//actual Classes differ
	//		System.out.println("Clob.getClass - does not match but instanceof OK");
		} else {
			assertEquals(st,classNameOrNull(d),classNameOrNull(d2));
		}
		if (d instanceof Integer) {
			if(d==d2) {
				assertTrue(st,false);
			}
		} else if (d instanceof Clob) {
			ArrayList<String> resS=new ArrayList<String>();
			ArrayList<Clob> clobs=new ArrayList<Clob>();
			clobs.add((Clob) d);
			clobs.add((Clob) d2);
			for (Clob e: clobs) {
				StringBuffer val=new StringBuffer();
				BufferedReader r=null;
				try {
					r=new BufferedReader(e.getCharacterStream());
					int next=';';
					while ((next=r.read())!=-1) {
						val.append((char)next);
					}
					resS.add(val.toString());
					
				} catch (SQLException ee) {
					fail(ee.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					fail(e1.getMessage());
				}
				finally{
					if (r!=null) {
						try {
							r.close();
						} catch (IOException ioe) {
							fail(ioe.getMessage());
						}
					}
				}
			}
			assertEquals(st,resS.get(0), resS.get(1));
		} else if ((d instanceof String)||(d instanceof Long)||(d instanceof BigDecimal)) {
			assertEquals(st,d, d2);
		} else {
			//might be both null
			if(!((d==null)&&(d2==null))){
				fail(st+classNameOrNull(d)+":class not checked, only checking strimg int long clob bigdecimal");
			}
		}
	}
	
	public void createDropCommon(Connection conn, String[] toCall) throws SQLException {
		Statement st=null;
		
		for (String call:toCall) {
			try{
				st=conn.createStatement();
				st.execute(call);
			} finally {
				close(st);
			}
		}
	}
	public void close (Statement...statements) {
		for (Statement s: statements) {
			try {
				if (s!=null) {
					s.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public void close (ResultSet...resultSets) {
		for (ResultSet r: resultSets) {
			try {
				if (r!=null){
				   r.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
