/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.jdbc.orest;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.jdbc.ResultSetTest;
import oracle.dbtools.unittest.categories.FailTest;
import oracle.dbtools.unittest.categories.PassTest;
import oracle.jdbc.OracleResultSet;
import oracle.sql.CHAR;
import oracle.sql.CLOB;
import oracle.sql.Datum;
import oracle.sql.NUMBER;

public class ORestResultSetTest extends ResultSetTest {
	@Override
	public Connection getRestConnection() throws ClassNotFoundException, SQLException {
		return getORestConnection();
	}
	 
	//bound to fail clob has issues String theSelect="select v,n, c /*clob*/ from orestResultSet order by ob";
	String theSelectNoNullNoClob="select v, n/*, c clob*/, ob from orestResultSet order by ob";
	//null fails {"create table orestresultset( oB int, v varchar2(1000), n number, c clob)","insert into orestresultset values ( 1, null, null, null)","insert into orestresultset values ( 2,'string', 12.34, empty_clob())","insert into orestresultset values ( 3,'string2', 678.00, 'something')"}
	String[] theSetupNoNullNoClob={"create table orestresultset( oB int, v varchar2(1000), n number, c clob)"/*,"insert into orestresultset values ( 1, null, null, null)"*/,"insert into orestresultset values ( 2,'string', 12.34, empty_clob())","insert into orestresultset values ( 3,'string2', 678.00, 'something')"};
	
	@Category(FailTest.class)
	@Test
	public void testGetNUMBER() {//also breaks if column name uppercased
		coreGetNUMBER(theSetup, theSelect);
	}
	
	@Category(PassTest.class)
	@Test
	public void testGetNumberNoNull() {
		coreGetNUMBER(theSetupNoNull, theSelect);
	}

	public void coreGetNUMBER(String[] theSetup, String theSelect) {
		Connection oracleConn=null;
		Connection restConn=null;
		Statement oracleStmt=null;
		Statement restStmt=null;
		OracleResultSet oracleResultSet=null;
		OracleResultSet restResultSet=null;
		ArrayList<Datum> oracleDatum=new ArrayList<Datum>();//2x3
		ArrayList<Datum> restDatum=new ArrayList<Datum>();//2x3
		try {
			String[] toCall1=theSetup;
			
			try {
				oracleConn=getOracleConnection();
				createDropCommon(oracleConn, toCall1);
				oracleStmt=oracleConn.createStatement();
				oracleResultSet=(OracleResultSet)oracleStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=oracleResultSet.getMetaData();
				while (oracleResultSet.next()) {
					oracleDatum.add(oracleResultSet.getNUMBER(2));
					//oracleDatum.add(oracleResultSet.getNUMBER("N"));
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				fail(e.getMessage());
			}
			try {
				restConn=getRestConnection();
				//oracleConn points to restConn createCommon(restConn, toCall1);
				restStmt=restConn.createStatement();
				restResultSet=(OracleResultSet)restStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=restResultSet.getMetaData();
				while (restResultSet.next()) {
					restDatum.add(restResultSet.getNUMBER(2));
					//BREAKS if column was defines as n restDatum.add(restResultSet.getNUMBER("N"));
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(e.getMessage());
			}
			//check datum class
			assertEquals("different datum returns", oracleDatum.size(), restDatum.size());
			for (int alcount=0;alcount<oracleDatum.size();alcount++) {
				//compare class returns
				assertEquals("datum null checK"+alcount,oracleDatum.get(alcount)==null,restDatum.get(alcount)==null );
				datumCheck("datum check"+alcount,oracleDatum.get(alcount),restDatum.get(alcount));
				//assertEquals("datum equals check"+alcount,oracleDatum.get(alcount).getClass(),restDatum.get(alcount));
			}
		} finally {
			String[] toCall={"drop table orestresultset"};
			try {
				createDropCommon(oracleConn, toCall);
			} catch (Exception e) {
				e.printStackTrace();
			}
			close(oracleResultSet,restResultSet);
			close(oracleStmt,restStmt);
		}
	}
	@Category(FailTest.class)
	@Test
	public void testGetCHAR() {//also get by uppercase column name fails
		coreGetCHAR(theSetup, theSelect);
	}
	
	@Category(PassTest.class)
	@Test
	public void testGetCHARNoNull() {
		coreGetCHAR(theSetupNoNull, theSelect);
	}

	public void coreGetCHAR(String[] theSetup, String theSelect) {
		Connection oracleConn=null;
		Connection restConn=null;
		Statement oracleStmt=null;
		Statement restStmt=null;
		OracleResultSet oracleResultSet=null;
		OracleResultSet restResultSet=null;
		ArrayList<Datum> oracleDatum=new ArrayList<Datum>();//2x3
		ArrayList<Datum> restDatum=new ArrayList<Datum>();//2x3
		try {
			String[] toCall1=theSetup;//{"create table orestresultset( oB int, v varchar2(1000), n number, c clob)","insert into orestresultset values ( 1, null, null, null)","insert into orestresultset values ( 2,'string', 12.34, empty_clob())","insert into orestresultset values ( 3,'string2', 678.00, 'something')"};
			
			try {
				oracleConn=getOracleConnection();
				createDropCommon(oracleConn, toCall1);
				oracleStmt=oracleConn.createStatement();
				oracleResultSet=(OracleResultSet)oracleStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=oracleResultSet.getMetaData();
				while (oracleResultSet.next()) {
					oracleDatum.add(oracleResultSet.getCHAR(1));
					//oracleDatum.add(oracleResultSet.getCHAR("V"));
				}
			} catch (ClassNotFoundException | SQLException e) {
				fail(e.getMessage());
			}
			try {
				restConn=getRestConnection();
				//oracleConn points to restConn createCommon(restConn, toCall1);
				restStmt=restConn.createStatement();
				restResultSet=(OracleResultSet)restStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=restResultSet.getMetaData();
				while (restResultSet.next()) {
					restDatum.add(restResultSet.getCHAR(1));
					//BREAKSrestDatum.add(restResultSet.getCHAR("V"));
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(e.getMessage());
			}
			//check datum class
			assertEquals("different datum returns", oracleDatum.size(), restDatum.size());
			for (int alcount=0;alcount<oracleDatum.size();alcount++) {
				//compare class returns
				assertEquals("datum null checK"+alcount,oracleDatum.get(alcount)==null,restDatum.get(alcount)==null );
				datumCheck("datum check"+alcount,oracleDatum.get(alcount),restDatum.get(alcount));
				//assertEquals("datum equals check"+alcount,oracleDatum.get(alcount).getClass(),restDatum.get(alcount));
			}
		} finally {
			String[] toCall={"drop table orestresultset"};
			try {
				createDropCommon(oracleConn, toCall);
			} catch (Exception e) {
				e.printStackTrace();
			}
			close(oracleResultSet,restResultSet);
			close(oracleStmt,restStmt);
		}
	}
	/*bound to fail clob has issues*/
	@Category(FailTest.class)
	@Test
	public void testGetCLOB() {
		Connection oracleConn=null;
		Connection restConn=null;
		Statement oracleStmt=null;
		Statement restStmt=null;
		OracleResultSet oracleResultSet=null;
		OracleResultSet restResultSet=null;
		ArrayList<Datum> oracleDatum=new ArrayList<Datum>();//2x3
		ArrayList<Datum> restDatum=new ArrayList<Datum>();//2x3
		try {
			String[] toCall1=theSetup;
			
			try {
				oracleConn=getOracleConnection();
				createDropCommon(oracleConn, toCall1);
				oracleStmt=oracleConn.createStatement();
				oracleResultSet=(OracleResultSet)oracleStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=oracleResultSet.getMetaData();
				while (oracleResultSet.next()) {
					oracleDatum.add(oracleResultSet.getCLOB(3));
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				fail(e.getMessage());
			}
			try {
				restConn=getRestConnection();
				//oracleConn points to restConn createCommon(restConn, toCall1);
				restStmt=restConn.createStatement();
				restResultSet=(OracleResultSet)restStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=restResultSet.getMetaData();
				while (restResultSet.next()) {
					restDatum.add(restResultSet.getCLOB(3));
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(e.getMessage());
			}
			//check datum class
			assertEquals("different datum returns", oracleDatum.size(), restDatum.size());
			for (int alcount=0;alcount<oracleDatum.size();alcount++) {
				//compare class returns
				assertEquals("datum null checK"+alcount,oracleDatum.get(alcount)==null,restDatum.get(alcount)==null );
				datumCheck("datum check"+alcount,oracleDatum.get(alcount),restDatum.get(alcount));
				//assertEquals("datum equals check"+alcount,oracleDatum.get(alcount).getClass(),restDatum.get(alcount));
			}
		} catch (Throwable  t) {
			fail (t.getLocalizedMessage());
		} finally {
			String[] toCall={"drop table orestresultset"};
			try {
				createDropCommon(oracleConn, toCall);
			} catch (Exception e) {
				e.printStackTrace();
			}
			close(oracleResultSet,restResultSet);
			close(oracleStmt,restStmt);
		} 
	}
	@Category(FailTest.class)
	@Test
	public void testGetOracleObject() {
		coreGetOracleObject(theSetup, theSelect);
	}
	
	@Category(PassTest.class)
	@Test
	public void testGetOracleObjectNoNullNoClob() {
		coreGetOracleObject(theSetupNoNullNoClob, theSelectNoNullNoClob);
	}
	public void coreGetOracleObject(String[] theSetup, String theSelect) {
		Connection oracleConn=null;
		Connection restConn=null;
		Statement oracleStmt=null;
		Statement restStmt=null;
		OracleResultSet oracleResultSet=null;
		OracleResultSet restResultSet=null;
		ArrayList<Datum> oracleDatum=new ArrayList<Datum>();//2x3
		ArrayList<Datum> restDatum=new ArrayList<Datum>();//2x3
		try {
			String[] toCall1=theSetup;
			try {
				oracleConn=getOracleConnection();
				createDropCommon(oracleConn, toCall1);
				oracleStmt=oracleConn.createStatement();
				oracleResultSet=(OracleResultSet)oracleStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=oracleResultSet.getMetaData();
				while (oracleResultSet.next()) {
					for (int colcount=1;colcount<(rsmd.getColumnCount()+1);colcount++){
						oracleDatum.add(oracleResultSet.getOracleObject(colcount));
					}
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				fail(e.getMessage());
			}
			try {
				restConn=getRestConnection();
				//oracleConn points to restConn createCommon(restConn, toCall1);
				restStmt=restConn.createStatement();
				restResultSet=(OracleResultSet)restStmt.executeQuery(theSelect);
				ResultSetMetaData rsmd=restResultSet.getMetaData();
				while (restResultSet.next()) {
					for (int colcount=1;colcount<(rsmd.getColumnCount()+1);colcount++){
						restDatum.add(restResultSet.getOracleObject(colcount));
					}
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(e.getMessage());
			}
			//check datum class
			assertEquals("different datum returns", oracleDatum.size(), restDatum.size());
			for (int alcount=0;alcount<oracleDatum.size();alcount++) {
				//compare class returns
				assertEquals("datum null checK"+alcount,oracleDatum.get(alcount)==null,restDatum.get(alcount)==null );
				datumCheck("datum check"+alcount,oracleDatum.get(alcount),restDatum.get(alcount));
				//assertEquals("datum equals check"+alcount,oracleDatum.get(alcount).getClass(),restDatum.get(alcount));
			}
		} catch (Throwable t) {
			fail(t.getMessage());
		}
		finally {
			String[] toCall={"drop table orestresultset"};
			try {
				createDropCommon(oracleConn, toCall);
			} catch (Exception e) {
				e.printStackTrace();
			}
			close(oracleResultSet,restResultSet);
			close(oracleStmt,restStmt);
		}
	}
	public String classNameOrNull(Object o) {
		if (o==null) {
			return "null";
		}
		return o.getClass().getName();
	}
	@SuppressWarnings("deprecation")
	public void datumCheck(String st, Datum d, Datum d2) {
		assertEquals(st,classNameOrNull(d),classNameOrNull(d2));
		if (d instanceof NUMBER) {
			if(((NUMBER)d).compareTo
					((NUMBER)d2)!=0) {
				assertTrue(st,false);
			}
		} else if (d instanceof oracle.sql.CLOB) {
			ArrayList<String> resS=new ArrayList<String>();
			ArrayList<CLOB> clobs=new ArrayList<CLOB>();
			clobs.add((CLOB) d);
			clobs.add((CLOB) d2);
			for (CLOB e: clobs) {
				StringBuffer val=new StringBuffer();
				BufferedReader r=null;
				try {
					r=new BufferedReader(e.getCharacterStream());//assume (will throw null pointer)not null
					int next=';';
					while ((next=r.read())!=-1) {
						val.append((char)next);
					}
					resS.add(val.toString());
					
				} catch (SQLException ee) {
					fail(ee.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					fail(e1.getMessage());
				}
				finally{
					if (r!=null) {
						try {
							r.close();
						} catch (IOException ioe) {
							fail(ioe.getMessage());
						}
					}
				}
			}
			assertEquals(st,resS.get(0), resS.get(1));
		} else if (d instanceof oracle.sql.CHAR) {
			assertEquals(st,((CHAR)d).stringValue(), ((CHAR)d2).stringValue());
		} else {
			//might be both null
			if(!((d==null)&&(d2==null))){
				fail(st+classNameOrNull(d)+":class not checked, only checking CHAR NUMBER CLOB");
			}
		}
	}
	
	public void createDropCommon(Connection conn, String[] toCall) throws SQLException {
		Statement st=null;
		for (String call:toCall) {
			try{
				st=conn.createStatement();
				st.execute(call);
			} finally {
				close(st);
			}
		}
	}
	public void close (Statement...statements) {
		for (Statement s: statements) {
			try {
				if (s!=null) {
					s.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public void close (ResultSet...resultSets) {
		for (ResultSet r: resultSets) {
			try {
				if (r!=null){
				   r.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
