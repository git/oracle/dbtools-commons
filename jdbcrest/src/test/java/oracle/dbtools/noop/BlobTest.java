/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.noop;

import java.sql.Blob;
import java.sql.SQLException;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.jdbc.BLOB;
import oracle.dbtools.jdbc.util.RestJdbcNotImplementedException;
import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.PassTest;

public class BlobTest extends JDBCTestCase {
	@Category(PassTest.class)
	@Test
	public void testLength() {
		try {
			Blob b = new BLOB();
			assert (b.length() == 0);
		} catch (SQLException e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testGetBytes() {
		Blob b = new BLOB();
		try {
			b.getBytes(0, 0);
		} catch (RestJdbcNotImplementedException f) {
			assert (f instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}

	@Category(PassTest.class)
	@Test
	public void testPosition() {
		try {
			Blob b = new BLOB();
			assert (b.position(b, 0) == 0);
		} catch (SQLException e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testPosition2() {
		try {
			Blob b = new BLOB();
			assert (b.position(new byte[] {}, 0) == 0);
		} catch (SQLException e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testSetBytes() {
		try {
			Blob b = new BLOB();
			assert (b.setBytes(0, new byte[] {}) == 0);
		} catch (SQLException e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testSetBytes2() {
		try {
			Blob b = new BLOB();
			assert (b.setBytes(0, new byte[] {},0,0) == 0);
		} catch (SQLException e) {
			fail(e.getMessage());
		}
	}
	@Category(PassTest.class)
	@Test
	public void testSetBinaryStream() {
		Blob b = new BLOB();
		try {
			b.setBinaryStream(0);
		} catch (RestJdbcNotImplementedException f) {
			assert (f instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}
	@Category(PassTest.class)
	@Test
	public void testTruncate() {
		Blob b = new BLOB();
		try {
			b.truncate(0);
		} catch (RestJdbcNotImplementedException f) {
			assert (f instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}
	@Category(PassTest.class)
	@Test
	public void testFree() {
		Blob b = new BLOB();
		try {
			b.free();
		} catch (RestJdbcNotImplementedException f) {
			assert (f instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}
	
	@Category(PassTest.class)
	@Test
	public void testGetBinaryStream2() {
		Blob b = new BLOB();
		try {
			b.getBinaryStream();
		} catch (RestJdbcNotImplementedException f) {
			assert (f instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}
	@Category(PassTest.class)
	@Test
	public void testGetBinaryStream3() {
		Blob b = new BLOB();
		try {
			b.getBinaryStream(0,0);
		} catch (RestJdbcNotImplementedException f) {
			assert (f instanceof RestJdbcNotImplementedException);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}
	@Category(PassTest.class)
	@Test
	public void testGetLength() {
		Blob b = new BLOB();
		try {
			assert(b.length()==0);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}
	@Category(PassTest.class)
	@Test
	public void testPosition3() {
		Blob b = new BLOB();
		try {
			assert(b.position((byte[]) null, 0)==0);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}

}