/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.notsupported;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.jdbc.ParameterMetaData;
import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.PassTest;

/**
 * Testing the unsupported functions so they operate correctly.
 * 
 * @author bamcgill
 *
 */
public class ParameterMetaDataTest extends JDBCTestCase {
	@Category(PassTest.class)
	@Test
	public void testUnwrap() {
		try {
			ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },
					2);
			pm.unwrap(this.getClass());
		} catch (UnsupportedOperationException uo) {
			assert (true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testIsWrapperFor() {
		try {
			ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },
					2);
			pm.isWrapperFor(this.getClass());
		} catch (UnsupportedOperationException uo) {
			assert (true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testGetParameterCount() {
		try {
			ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },
					2);
			assert (pm.getParameterCount() == 2);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testIsNullable() {		
		try {
		ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },2);
			assert (pm.isNullable(0)==0);
		} catch(UnsupportedOperationException e) {
			assert(true);
		}catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testIsSigned() {
		ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },2);
		try {
			assert(pm.isSigned(0));
		} catch(UnsupportedOperationException e) {
			assert(true);
		}catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testGetPrecision() {
		ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },2);
		try {
			assert(pm.getPrecision(0)==0);
		} catch(UnsupportedOperationException e) {
			assert(true);
		}catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testGetScale() {
		ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },2);
		try {
			assert(pm.getScale(0)==0);
		} catch(UnsupportedOperationException e) {
			assert(true);
		}catch (Exception e) {
			fail(e.getMessage());
		}	}

	@Category(PassTest.class)
	@Test
	public void testGetParameterType() {
		ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },2);
		try {
			assert(pm.getParameterType(0)==0);
		} catch(UnsupportedOperationException e) {
			assert(true);
		}catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testGetParameterTypeName() {
		ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },2);
		try {
			assert(pm.getParameterTypeName(0).equals(""));
		} catch(UnsupportedOperationException e) {
			assert(true);
		}catch (Exception e) {
			fail(e.getMessage());
		}	}

	@Category(PassTest.class)
	@Test
	public void testGetParameterClassName() {
		ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },2);
		try {
			assert(pm.getParameterClassName(0).equals(""));
		} catch(UnsupportedOperationException e) {
			assert(true);
		}catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Category(PassTest.class)
	@Test
	public void testGetParameterMode() {
		ParameterMetaData pm = new ParameterMetaData(null, null, null, null, null, null, null, new int[] { 1, 2 },2);
		try {
			assert(pm.getParameterMode(2)==2);
		} catch(UnsupportedOperationException e) {
			assert(true);
		} catch (NullPointerException e) {
			assert(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
