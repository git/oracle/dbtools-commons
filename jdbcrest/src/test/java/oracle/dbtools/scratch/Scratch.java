/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.scratch;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import oracle.dbtools.func.Util;
import oracle.dbtools.unittest.JDBCTestCase;
import oracle.dbtools.unittest.categories.ScratchTest;

public class Scratch extends JDBCTestCase{
  
  @Category(ScratchTest.class)
  @Test
  public void testSetDate() {
    Connection conn = null, oracle_conn = null;
    PreparedStatement opstmt = null, rpstmt = null;
    ResultSet oracle_rs = null, rest_rs = null;
    Date date_oracle_col = null, date_rest_col = null;
    String sql_createtable = "create table SetDate (col1 number, col2 date)";
    String sql_insert = "insert into SetDate values (?,?)";
    String sql_drop = "drop table SetDate purge";

    // Setup
    try {
      conn = getRestConnection();
      Util.doSQL(conn, sql_createtable);
    } catch (SQLException | ClassNotFoundException | NullPointerException e) {
      System.out.println(e.getMessage());
      e.printStackTrace();

      fail(e.getMessage());
    }

    // Run Oracle Thin driver

    try {
      oracle_conn = getOracleConnection();
      opstmt = oracle_conn.prepareStatement(sql_insert);
      opstmt.setInt(1, 123);
      opstmt.setDate(2, Date.valueOf("2017-07-01"));
      opstmt.execute();
      oracle_rs = opstmt.executeQuery("select * from SetDate");
      while (oracle_rs.next()) {
        date_oracle_col = oracle_rs.getDate("col2");
      }
    } catch (SQLException | ClassNotFoundException | NullPointerException e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
      fail(e.getMessage());
    }

    // Run Rest JDBC driver

    try {

      rpstmt = conn.prepareStatement(sql_insert);
      rpstmt.setInt(1, 123);
      rpstmt.setDate(2, Date.valueOf("2017-07-01"));
      rpstmt.execute();

      rest_rs = rpstmt.executeQuery("select * from SetDate");
      while (rest_rs.next()) {
        date_rest_col = rest_rs.getDate("col2");
      }
    } catch (SQLException | NullPointerException e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
      fail(e.getMessage());
    } finally {
      Util.doSQL(conn, sql_drop);
      close(opstmt, rpstmt);
      close(oracle_rs, rest_rs);
    }
    // assertEquals("2017-07-01", date_rcol.toString());
    assertEquals(date_oracle_col.toString(), date_rest_col.toString());
  }

  
}

