/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.unittest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import junit.framework.TestCase;
import oracle.dbtools.unittest.categories.FailTest;
import oracle.dbtools.unittest.categories.PassTest;

public class JDBCTestCase extends TestCase {

	Connection ORestConnection = null;
	Connection RestConnection = null;
	Connection OracleConnection = null;

	@Category(FailTest.class)
	@Ignore
	@Test
	public void testFail() {
		assertTrue(true);
	}

	@Category(PassTest.class)
	@Ignore
	@Test
	public void testPass() {
		assertTrue(true);
	}

	public Connection getRestConnection() throws ClassNotFoundException, SQLException {

		if (RestConnection == null) {
			Class.forName("oracle.dbtools.jdbc.Driver");
			Properties cred = new Properties();
			cred.put("user", getSchemaName().toUpperCase());
			cred.put("password", "adhocsqltest");
			RestConnection = DriverManager.getConnection("http://yourmachine.yourdomain:8082/ords/" + getSchemaName() + "/", cred);
			if (RestConnection == null) {
				System.out.println("Rest connection is null");
			} else {
				System.out.println("Rest connection connection.getClass().getName():" + RestConnection.getClass().getName());
			}
		}
		return RestConnection;
	}

	private String getSchemaName() {
		String schemaName = System.getProperty("schema_name");
		if (schemaName!= null) {
			return schemaName;
		} else {
			return "adhocsqltest";
		}
	}

	public Connection getORestConnection() throws ClassNotFoundException, SQLException {
		String schemaName = getSchemaName();
		System.out.println("INSIDE JDBCTestCase.getORestConnection(): "+schemaName);
		if (ORestConnection == null) {
			Class.forName("oracle.dbtools.jdbc.orest.Driver");
			Properties cred = new Properties();
			cred.put("user", getSchemaName().toUpperCase());
			cred.put("password", "adhocsqltest");
			ORestConnection = DriverManager.getConnection("jdbc:oracle:orest:@http://yourmachine.yourdomain:8082/ords/" + getSchemaName() + "/", cred);
			if (ORestConnection == null) {
				System.out.println("ORest connection is null");
			} else {
				System.out.println("ORest connection connection.getClass().getName():" + ORestConnection.getClass().getName());
			}
		}
		return ORestConnection;
	}

	public Connection getOracleConnection() throws ClassNotFoundException, SQLException {
		String schemaName = getSchemaName();
		System.out.println("INSIDE JDBCTestCase.getOracleConnection():"+schemaName);
		if (OracleConnection == null) {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection = null;
			Properties props = new Properties();
			props.put("user", getSchemaName().toUpperCase());
			props.put("password", "adhocsqltest");
			// really want this setting unless creating java procedures.
			// props.put(OracleConnection.CONNECTION_PROPERTY_PROCESS_ESCAPES,
			// "false");
			OracleConnection = DriverManager.getConnection("jdbc:oracle:thin:@yourmachine.yourdomain:1521/orcl", props);
		}
		return OracleConnection;
	}

  public void close(ResultSet... resultSets) {
    try {
      for (ResultSet rs : resultSets) {
        if (rs != null) {
          if (!rs.isClosed()) {
            rs.close();
          }
        }
      }
    } catch (Throwable t) {
      fail(t.getLocalizedMessage());
    }
  }

  public void close(Statement... stmts) {
    try {
      for (Statement stmt : stmts) {
        if (stmt != null) {
          if (!stmt.isClosed()) {
            stmt.close();
          }
        }
      }
    } catch (Throwable t) {
      fail(t.getLocalizedMessage());
    }
  }

	private void log(String msg) {
		System.out.println(msg);
	}
}
