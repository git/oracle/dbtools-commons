/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.unittest.flex;

import java.lang.annotation.Annotation;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.experimental.categories.Categories.CategoryFilter;
import org.junit.experimental.categories.Categories.ExcludeCategory;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.experimental.categories.Category;
import org.junit.runner.Description;
import org.junit.runner.manipulation.NoTestsRemainException;
import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.RunnerBuilder;

/**
 * This class is based on org.junit.experimental.categories.Categories from JUnit 4.10.
 *
 * All anotations and inner classes from the original class Categories are removed,
 * since they will be re-used.
 * Unfortunately sub-classing Categories did not work.
 */
public class FlexibleCategories extends Suite {

  
  /**
   * Specifies the last class to include when listed alphabetically
   * This annotation is not required required.
   */
  @Retention(RetentionPolicy.RUNTIME)
  @Inherited
  public @interface TestClassNameTo {
   public String value();
  }
  
  /**
   * Specifies the first class to include when listed alphabetically
   * This annotation is not required.
   */
  @Retention(RetentionPolicy.RUNTIME)
  @Inherited
  public @interface TestClassNameFrom {
   public String value();
  }
  
  
 /**
  * Specifies the package which should be scanned for test classes (e.g. @TestScanPackage("my.package")).
  * This annotation is required.
  */
 @Retention(RetentionPolicy.RUNTIME)
 @Inherited
 public @interface TestScanPackage {
  public String[] value();
 }

 /**
  * Specifies the prefix of matching class names (e.g. @TestClassPrefix("Test")).
  * This annotation is optional (default: "").
  */
 @Retention(RetentionPolicy.RUNTIME)
 @Inherited
 public @interface TestClassPrefix {
  public String value();
 }

 /**
  * Specifies the suffix of matching class names (e.g. @TestClassSuffix("Test")).
  * This annotation is optional (default: "Test").
  */
 @Retention(RetentionPolicy.RUNTIME)
 @Inherited
 public @interface TestClassSuffix {
  public String value();
 }

 /**
  * Specifies an annotation for methods which must be present in a matching class (e.g. @TestMethodAnnotationFilter(Test.class)).
  * This annotation is optional (default: org.junit.Test.class).
  */
 @Retention(RetentionPolicy.RUNTIME)
 public @interface TestMethodAnnotation {
  public Class<? extends Annotation> value();
 }

 public FlexibleCategories(Class<?> clazz, RunnerBuilder builder)
   throws InitializationError {
  this(builder, clazz, PatternClasspathClassesFinder.getSuiteClasses(
    getTestScanPackage(clazz), getTestClassPrefix(clazz), getTestClassSuffix(clazz),
    getTestMethodAnnotation(clazz),getTestClassNameFrom(clazz),getTestClassNameTo(clazz)));
  try {
   filter(CategoryFilter.categoryFilter(true,getIncludedCategory(clazz),true,getExcludedCategory(clazz)));
  } catch (NoTestsRemainException e) {
   // Ignore all classes with no matching tests.
  }
  assertNoCategorizedDescendentsOfUncategorizeableParents(getDescription());
 }

 public FlexibleCategories(RunnerBuilder builder, Class<?> clazz,
   Class<?>[] suiteClasses) throws InitializationError {
  super(builder, clazz, suiteClasses);
 }

 private static String[] getTestScanPackage(Class<?> clazz) throws InitializationError {
  TestScanPackage annotation = clazz.getAnnotation(TestScanPackage.class);
  if (annotation == null) {
   throw new InitializationError("No package given to scan for tests!\nUse the annotation @TestScanPackage(\"my.package\") on the test suite " + clazz + ".");
  }
  return annotation.value();
 }

 private static String getTestClassPrefix(Class<?> clazz) {
  TestClassPrefix annotation = clazz.getAnnotation(TestClassPrefix.class);
  return annotation == null ? "" : annotation.value();
 }
 
 private static String getTestClassNameFrom(Class<?> clazz) {
   TestClassNameFrom annotation = clazz.getAnnotation(TestClassNameFrom.class);
   return annotation == null ? null : annotation.value();
  }
 
 private static String getTestClassNameTo(Class<?> clazz) {
   TestClassNameTo annotation = clazz.getAnnotation(TestClassNameTo.class);
   return annotation == null ? null : annotation.value();
  }

 private static String getTestClassSuffix(Class<?> clazz) {
  TestClassSuffix annotation = clazz.getAnnotation(TestClassSuffix.class);
  return annotation == null ? "Test" : annotation.value();
 }

 private static Class<? extends Annotation> getTestMethodAnnotation(Class<?> clazz) {
  TestMethodAnnotation annotation = clazz.getAnnotation(TestMethodAnnotation.class);
  return annotation == null ? Test.class : annotation.value();
 }

 private Set<Class<?>> getIncludedCategory(Class<?> clazz) {
  IncludeCategory annotation= clazz.getAnnotation(IncludeCategory.class);
  Class<?>[] classes =  annotation == null ? null : annotation.value();
  if (classes == null){
    return null;
  } else {
    return new HashSet<Class<?>>(Arrays.asList(classes));
  }
 }

 private Set<Class<?>> getExcludedCategory(Class<?> clazz) {
  ExcludeCategory annotation= clazz.getAnnotation(ExcludeCategory.class);
  Class<?>[] classes = annotation == null ? null : annotation.value();
  if (classes == null){
    return null;
  } else {
    return new HashSet<Class<?>>(Arrays.asList(classes));
  }
 }

 private void assertNoCategorizedDescendentsOfUncategorizeableParents(Description description) throws InitializationError {
  if (!canHaveCategorizedChildren(description))
   assertNoDescendantsHaveCategoryAnnotations(description);
  for (Description each : description.getChildren())
   assertNoCategorizedDescendentsOfUncategorizeableParents(each);
 }

 private void assertNoDescendantsHaveCategoryAnnotations(Description description) throws InitializationError {
  for (Description each : description.getChildren()) {
   if (each.getAnnotation(Category.class) != null)
    throw new InitializationError("Category annotations on Parameterized classes are not supported on individual methods.");
   assertNoDescendantsHaveCategoryAnnotations(each);
  }
 }

 // If children have names like [0], our current magical category code can't determine their
 // parentage.
 private static boolean canHaveCategorizedChildren(Description description) {
  for (Description each : description.getChildren())
   if (each.getTestClass() == null)
    return false;
  return true;
 }
}