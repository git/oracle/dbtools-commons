/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.unittest.flex;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 *
 * Modified version of ClasspathClassesFinder from:
 * http://linsolas.free.fr/wordpress/index.php/2011/02/how-to-categorize-junit-tests-with-maven/
 *
 * The difference is, that it does not search for annotated classes but for classes with a certain
 * class name prefix and suffix.
 */
public final class PatternClasspathClassesFinder {
  /**
  * Get the list of classes of a given package name, and that are annotated
  * by a given annotation.
  *
  * @param packageName
  *            The package name of the classes.
  * @param classPrefix
  *            The prefix of the class name.
  * @param classSuffix
  *            The suffix of the class name.
  * @param methodAnnotation
  *            Only return classes containing methods annotated with methodAnnotation.
   * @param classNameTo 
   * @param classNameFrom 
  * @return The List of classes that matches the requirements.
  */
  public static Class<?>[] getSuiteClasses(String[] packageName, String classPrefix, String classSuffix, Class<? extends Annotation> methodAnnotation, String classNameFrom, String classNameTo) {
    try {
      return getClasses(packageName, classPrefix, classSuffix, methodAnnotation,classNameFrom,classNameTo);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
  * Get the list of classes of a given package name, and that are annotated
  * by a given annotation.
  *
  * @param packageName
  *            The package name of the classes.
  * @param classPrefix
  *            The prefix of the class name.
  * @param classSuffix
  *            The suffix of the class name.
  * @param methodAnnotation
  *            Only return classes containing methods annotated with methodAnnotation.
   * @param classNameTo 
   * @param classNameFrom 
  * @return The List of classes that matches the requirements.
  * @throws ClassNotFoundException
  *             If something goes wrong...
  * @throws IOException
  *             If something goes wrong...
  */
  private static Class<?>[] getClasses(String[] packageNames, String classPrefix, String classSuffix, Class<? extends Annotation> methodAnnotation, String classNameFrom, String classNameTo) throws ClassNotFoundException, IOException {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    // For each classpath, get the classes.
    ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
    for (String packageName : packageNames) {
      String path = packageName.replace('.', '/');
      // Get classpath
      Enumeration<URL> resources = classLoader.getResources(path);
      List<File> dirs = new ArrayList<File>();
      while (resources.hasMoreElements()) {
        URL resource = resources.nextElement();
        dirs.add(new File(resource.getFile()));
      }
      for (File directory : dirs) {
        classes.addAll(findClasses(directory, packageName, classPrefix, classSuffix, methodAnnotation,classNameFrom,classNameTo));
      }
    }
    return classes.toArray(new Class[classes.size()]);
  }

  /**
  * Find classes, in a given directory (recursively), for a given package
  * name, that are annotated by a given annotation.
  *
  * @param directory
  *            The directory where to look for.
  * @param packageName
  *            The package name of the classes.
  * @param classPrefix
  *            The prefix of the class name.
  * @param classSuffix
  *            The suffix of the class name.
  * @param methodAnnotation
  *            Only return classes containing methods annotated with methodAnnotation.
   * @param classNameTo 
   * @param classNameFrom 
  * @return The List of classes that matches the requirements.
  * @throws ClassNotFoundException
  *             If something goes wrong...
  */
  private static List<Class<?>> findClasses(File directory, String packageName, String classPrefix, String classSuffix, Class<? extends Annotation> methodAnnotation, String classNameFrom, String classNameTo) throws ClassNotFoundException {
    List<Class<?>> classes = new ArrayList<Class<?>>();
    if (!directory.exists()) {
      return classes;
    }
    File[] files = directory.listFiles();
    for (File file : files) {
      //do not recursively go down through lower directories
      //if (file.isDirectory()) {
      //  classes.addAll(findClasses(file, packageName + "." + file.getName(), classPrefix, classSuffix, methodAnnotation,classNameFrom,classNameTo));
      //} else 
        if (file.getName().startsWith(classPrefix) && file.getName().endsWith(classSuffix + ".class")) {
        // We remove the .class at the end of the filename to get the
        // class name...
        Class<?> clazz = Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6));
        // Check, if class contains test methods (prevent "No runnable methods" exception):
        boolean classHasTest = false;
        for (Method method : clazz.getMethods()) {
          if (method.getAnnotation(methodAnnotation) != null) {
            classHasTest = true;
            break;
          }
        }
        if (classHasTest) {
          if((classNameFrom == null ||classNameFrom.compareTo(clazz.getSimpleName())<=0) && 
             (classNameTo==null || classNameTo.compareTo(clazz.getSimpleName())>=0) ){
            classes.add(clazz);
          }
        }
      }
    }
    return classes;
  }
}