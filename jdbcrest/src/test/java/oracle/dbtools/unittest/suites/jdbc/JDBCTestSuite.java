/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.unittest.suites.jdbc;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

  @RunWith(Categories.class)
  @Suite.SuiteClasses({
    oracle.dbtools.func.CallableStatementTest.class,
    oracle.dbtools.func.ConnectionTest.class,
    oracle.dbtools.func.DMLOperations.class,
    oracle.dbtools.func.PreparedStatementTest.class,
    oracle.dbtools.func.ResultSetMetaDataTest.class,
    oracle.dbtools.func.ResultSetTest.class,
    oracle.dbtools.func.ResultSetTest2.class,
    oracle.dbtools.func.StatementTest.class,
    oracle.dbtools.jdbc.CallableStatementTest.class,
    oracle.dbtools.jdbc.ClobTest.class,
    oracle.dbtools.jdbc.ConnectionTest.class,
    oracle.dbtools.jdbc.DatabaseMetaData.class,
    oracle.dbtools.jdbc.DriverTest.class,
    oracle.dbtools.jdbc.ParameterMetaDataTest.class,
    oracle.dbtools.jdbc.PreparedStatementTest.class,
    oracle.dbtools.jdbc.ResultSetMetaDataTest.class,
    oracle.dbtools.jdbc.ResultSetTest.class,
    oracle.dbtools.jdbc.StatementTest.class,
    oracle.dbtools.noop.BlobTest.class,
    oracle.dbtools.noop.DriverTest.class,
    oracle.dbtools.noop.RowIdTest.class,
    oracle.dbtools.notsupported.ParameterMetaDataTest.class,
    oracle.dbtools.scratch.Scratch.class
  })
  public abstract class JDBCTestSuite {

  }
