There are two products tested here
REST JDBC which implements the JDBC interface and OREST JDBC which implements the Oracle JDBC interface.

REST JDBC Test cases are defined in the following packages
oracle.dbtools.func
oracle.dbtools.jdbc
oracle.dbtoos.noop
oracle.dbtools.notsupported

OREST JDBC Test cases are defined in the following packages.
oracle.dbtools.jdbc.orest


Suites are defined with 
Categories.class or FlexibleCategories.

All test cases extend oracle.dbtools.unittest.JDBCTestCase.
Test case methods can be categorized into 4 categories defined in oracle.dbtools.unittest.categories
PassTest - expected to pass
FailTest  - expected to fail
UnsupportedTest - unssuported for the current release
ScratchTest - used to pick out an individual or a small subset of tests to run

oracle.dbtools.unittest.suites.jdbc
oracle.dbtools.unittest.suites.ojdbc
define an AllxxxTestSuite to run all the test in the PassTest,FailTest and UnsupportedTest categories in all the testcase packages.

oracle.dbtools.unittest.flex.suites package defined suites using the FlexibleCategories runner.
oracle.dbtools.unittest.flex.suites.jdbc defines suites for the REST JDBC test cases.

Test suites ending with a number are to be run in parallel by ant.
A set of abstract classes(TestSuite1.java, TestSuite2.java, TestSuite3.java, TestSuite4.java, TestSuite5.java) help organize the parallel test suites.
Their job is to define which classes are to be run in which parallel "stream". 
The real suites(PassTestSuite1.java, PassTestSuite2.java,... FailTestSuite1.java, FailTestSuite2.java, ...) extend the abstract Test Suites and filter the test run by categories.
 
Test suites which do not end in a number are designed to be run on their own and not in parallel with any other suite
