/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.resource_gen;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.sonatype.plexus.build.incremental.BuildContext;


@Mojo(name = "generate", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyResolution = ResolutionScope.COMPILE)
public class DbtoolsResgenMojo extends AbstractMojo {

	@Parameter(defaultValue = "${project.basedir}/", readonly = true)
	private File projectDir;

	@Parameter(defaultValue = "${project.build.outputDirectory}", readonly = true)
	private File classesDir;

	@Parameter(defaultValue = "${project.build.directory}/generated-sources", readonly = true)
	private File targetDirectory;

	@Parameter(defaultValue = "${project.basedir}/src/main/resources", readonly = true)
	private File sourceDirectory;

	@Parameter(defaultValue = "resfiles.properties", readonly = true)
	private File resFile;

	@Parameter(defaultValue = "resfiles.properties", readonly = true)
	private String propFileEndsWithRegex;

	@Parameter(readonly = true)
	private File resourceFileCodeTemplate;

	@Component
	private BuildContext buildContext;

	public void execute() throws MojoExecutionException, MojoFailureException {
		Path source = Paths.get(sourceDirectory.getAbsolutePath());
		Path target = Paths.get(targetDirectory.getPath());
		Path resFilePath = Paths.get(projectDir.getAbsolutePath(), resFile.getName());
		Path template = null;
		if (resourceFileCodeTemplate != null) {
			template = Paths.get(resourceFileCodeTemplate.getPath());
			getLog().info("Resource Template:" + resourceFileCodeTemplate);
		}

		getLog().info("----------------------------------------------------------------------------");
		getLog().info(" Generating Resource Bundles");
		getLog().info("----------------------------------------------------------------------------");
		getLog().info("Resource File:" + resFile.getAbsolutePath());
		getLog().info("Generated Source:" + targetDirectory.getAbsolutePath());
		getLog().info("Project Directory: " + projectDir.getAbsolutePath());
		getLog().info("resource filename end:" + propFileEndsWithRegex);

		if (source.toString().length() == 0) {
			getLog().error("No source root specified");
			return;
		}
		if (source.toString().length() == 0) {
			getLog().error("No output root specified");
			return;
		}

		ResGen rg = (new ResGen.Builder()).addSourceDir(source).addTargetDir(target)
				.addCodeResourceTemplate(resourceFileCodeTemplate).addResFilePropertiesPath(resFilePath)
				.addPropertyFileRegex(propFileEndsWithRegex).addLogger(getLog())
				.addBuildContext(buildContext).build();
		rg.execute();
		getLog().debug("Completed Resource File Generation");
	}

}