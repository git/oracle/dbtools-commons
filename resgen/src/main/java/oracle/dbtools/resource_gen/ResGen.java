/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.resource_gen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import org.apache.maven.plugin.logging.Log;
import org.sonatype.plexus.build.incremental.BuildContext;

/**
 * @author bamcgill
 *
 */
public class ResGen {

	// Default code template from the plugin.
	private final String m_template;
	// resfiles.properties path where we get a list of property files to process.
	private final Path resFilePath;
	// Where do we generate these Java files to?
	private final Path target;
	// Where do we look for the source files?
	private final Path source;
	// maven log
	private final Log log;
	// Do we need to convert case, defaults to no.
	private final boolean m_convertCase;
	// What the end of the resource files end with so we can search for them
	private final String endsWithRegex;
	private final ArrayList<Path> foundFiles = new ArrayList<Path>();
	// Custom Resource template so you can control whats generated
	private final Path resfileTemplate;
	// Sonatype Buildcontext for partial rebuilds
	private final BuildContext buildContext;


	private ResGen(Builder builder) {
		resfileTemplate = builder.getResFileTemplate();
		source = builder.source;
		target = builder.target;
		resFilePath = builder.resFilePath;
		buildContext = builder.buildContext;
		log = builder.log;
		endsWithRegex = builder.regex;
		m_convertCase = builder.convertCase;
		m_template = builder.template;
	}

	/**
	 * Ok, We're going to take the source directory and we are going to look for
	 * resfiles in the passed in named file. We'll get a list of paths of these and
	 */
	public void execute() {
		
		List<Path> listPathes = new ArrayList<Path>();
		if (getResFilePath().toFile().exists()) {
			getLog().info(MessageFormat.format("Processing classes listed in {0}", getResFilePath().toString()));
			getResourceFiles(listPathes);
		} else {
			// Oh dear, the resfiles.properties (or the one you sent in) doesn't exist
			getLog().info(MessageFormat.format("{0} does not exist", getResFilePath().toString()));
		}
		getLog().info("Checking Extension clause: currently it is:" + endsWithRegex);
		processExtensionClauses(listPathes);
		getLog().info("Processing the files found.");
		processFileset(listPathes);

	}

	private void processExtensionClauses(List<Path> listPathes) {
		// Ok, if we have defined a property which is like arb.poperties,
		//  then, lets look for them
		if (!endsWithRegex.equals(resFilePath.toString())) {
			walk(getSource(), endsWithRegex);
			listPathes.addAll(foundFiles);
		}
	}

	private Path getResfileTemplate() {
		return resfileTemplate;
	}

	/**
	 * @return the resFilePath
	 */
	public Path getResFilePath() {
		return resFilePath;
	}

	/**
	 * @return the target
	 */
	public Path getTarget() {
		return target;
	}

	/**
	 * @return the log
	 */
	public Log getLog() {
		return log;
	}

	/**
	 * @return the source
	 */
	public Path getSource() {
		return source;
	}

	private void getResourceFiles(List<Path> list) {

		if (resFilePath.toFile().exists()) {
			buildContext.removeMessages(resFilePath.toFile());
			try {
				BufferedReader r = new BufferedReader(new FileReader(getResFilePath().toFile()));
				String line;
				while ((line = r.readLine()) != null) {
					line = line.trim();
					if (line.length() > 0) {
						String path = line.replace('.', File.separatorChar);
						String fName = path + ".properties";
						list.add(Paths.get(getSource().toString(), fName));
					}
				}
			} catch (IOException e) {
				getLog().info("Cannot open file " + getResFilePath());
				buildContext.addMessage(resFilePath.toFile(), 0, 0, e.getLocalizedMessage(),
						BuildContext.SEVERITY_ERROR, null);
			}
		}

	}

	private void walk(Path path, String regexEnding) {

		File root = path.toFile();
		File[] list = root.listFiles();

		if (list == null)
			return;

		for (File f : list) {
			if (f.isDirectory()) {
				walk(Paths.get(f.getAbsolutePath()), regexEnding);
				getLog().debug("Searching dir:" + f.getAbsoluteFile());
			} else {
				if (f.getName().endsWith(regexEnding)) {
					foundFiles.add(f.toPath());
					getLog().debug("found " + f.toString());
				}
			}
		}
	}

	private void processFileset(List<Path> files) {
		if (files!=null && files.size() > 0) {
			String template = getTemplate();// only look for template when there are resource files. Otherwise the top level pom will be processed and its paths to the template will be off.
			for (Path file : files) {

				String pkgPath = file.toString().substring(getSource().toString().length() + 1);
				String path = file.toString();

				int idx = pkgPath.lastIndexOf('.');
				pkgPath = idx > 0 ? pkgPath.substring(0, idx) : pkgPath;

				String pkg;
				String cls;

				idx = pkgPath.lastIndexOf(File.separatorChar);
				if (idx > 0) {
					pkg = pkgPath.substring(0, idx).replace(File.separatorChar, '.');
					cls = pkgPath.substring(idx + 1);
				} else {
					pkg = "";
					cls = path;
				}

				File outFile = Paths.get(Paths.get(getTarget().toString()).toString(), pkgPath + ".java").toFile();
				processFile(file.toFile(), outFile, pkg, cls, template);
			}
		}
	}

	/**
	 * Process a resources file
	 * 
	 * @param propFile
	 * @param outFile
	 * @param pkg
	 * @param cls
	 * @param template2 
	 */
	private void processFile(File propFile, File outFile, String pkg, String cls, String template) {
		buildContext.removeMessages(outFile);
		if (buildContext.hasDelta(propFile)) {// will return true is this is run outside of eclipse
			getLog().debug("Processing properties file " + propFile);

			Properties props = new Properties();
			InputStream is = null;
			try {
				is = new FileInputStream(propFile);
				props.load(is);
			} catch (IOException e) {
				getLog().info("Error processing " + propFile + ": " + e.getLocalizedMessage());
				return;
			} finally {
				if (is != null)
					try {
						is.close();
					} catch (Exception ex) {
					}
			}

			if (props.size() > 0) {
				// Build up our list of property keys
				StringBuilder propDecls = new StringBuilder();
				Set<String> keys = new HashSet<String>();
				for (Object key : props.keySet()) {
					String fieldName = convertKeyToField(key.toString());
					if (m_convertCase) {
						fieldName = fieldName.toUpperCase(Locale.ROOT);
						final String baseName = fieldName;
						int i = 1;
						while (keys.contains(fieldName)) {
							fieldName = baseName + i++;
						}
						keys.add(fieldName);
					}
					propDecls.append(MessageFormat
							.format("    public static final String {0} = \"{1}\"; //$NON-NLS-1$\n", fieldName, key));
				}
				String outText = MessageFormat.format(template, pkg, cls, propDecls);

				// Create the output Dir
				File outDir = outFile.getParentFile();
				if (!outDir.exists()) {
					if (!outDir.mkdirs()) {
						getLog().info("Could not create directory path " + outDir);
						return;
					}
				}

				// Generate our Java class
				OutputStreamWriter out = null;
				try {
					out = new OutputStreamWriter(buildContext.newFileOutputStream(outFile)); // use buildContext as
																								// eclipse will refresh
																								// file
					out = new FileWriter(outFile);
					out.write(outText);
				} catch (IOException e) {
					getLog().info("Error writing file " + outFile + ": " + e.getLocalizedMessage());
					buildContext.addMessage(outFile, 0, 0, e.getLocalizedMessage(), BuildContext.SEVERITY_ERROR, null);
				} finally {
					if (out != null)
						try {
							out.close();
						} catch (Exception e) {
						}
					buildContext.refresh(outFile);
				}
			}
		} else {
			getLog().debug("Skipping properties file " + propFile);
		}
	}

	private String convertKeyToField(final String propKey) {
		char[] chars = propKey.toCharArray();
		if (chars.length == 0) {
			getLog().info("Invalid zero-length key found");
			return "";
		}

		if (!Character.isJavaIdentifierStart(chars[0])) {
			chars[0] = '_';
		}
		for (int i = 1; i < chars.length; i++) {
			if (!Character.isJavaIdentifierPart(chars[i])) {
				chars[i] = '_';
			}
		}

		return new String(chars);
	}

	/**
	 * Load up our template file.
	 * 
	 * @return
	 */
	private String getTemplate() {
		String template = "";
		if (m_template == null) {
			if (getResfileTemplate() != null) {
				// Load the template file
				StringBuilder bldr = new StringBuilder();
				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new FileReader(getResfileTemplate().toString()));
					String s;
					while ((s = reader.readLine()) != null) {
						bldr.append(s).append('\n');
					}
				} catch (Exception ex) {
					getLog().info("Could not load template " + ex.getLocalizedMessage(), ex);
				} finally {
					if (reader != null)
						try {
							reader.close();
						} catch (Exception ex) {
						}
				}
				template = bldr.toString();
			} else {
				template = getDefaultTemplate().toString();
			}
		}
		return template;
	}

	public StringBuilder getDefaultTemplate() {

		StringBuilder sb = new StringBuilder();
		sb.append("// Generated File. Do Not Modify" + "\n");
		sb.append("package {0};" + "\n");
		sb.append("" + "\n");
		sb.append("import java.awt.Image;" + "\n");
		sb.append("import java.util.ResourceBundle;" + "\n");
		sb.append("" + "\n");
		sb.append("import javax.swing.Icon;" + "\n");
		sb.append("" + "\n");
		sb.append("import oracle.dbtools.raptor.utils.MessagesBase;" + "\n");
		sb.append("" + "\n");
		sb.append("public class {1} extends MessagesBase '{'" + "\n");
		sb.append("		    // Generated Resource Keys" + "\n");
		sb.append("		{2}" + "\n");
		sb.append("		    private static final String BUNDLE_NAME = \"{0}.{1}\"; //$NON-NLS-1$" + "\n");
		sb.append("");
		sb.append("		    private static final {1} INSTANCE = new {1}();" + "\n");
		sb.append("" + "\n");
		sb.append("		    private {1}() '{'" + "\n");
		sb.append("		        super(BUNDLE_NAME, {1}.class.getClassLoader());" + "\n");
		sb.append("		    '}'" + "\n");
		sb.append("" + "\n");
		sb.append("	    public static ResourceBundle getBundle() '{' " + "\n");
		sb.append("	        return INSTANCE.getResourceBundle();" + "\n");
		sb.append("	    '}'" + "\n");
		sb.append("" + "\n");
		sb.append("		    " + "\n");
		sb.append("	    public static String getString( String key ) '{'" + "\n");
		sb.append("	        return INSTANCE.getStringImpl(key);" + "\n");
		sb.append("	    '}'" + "\n");
		sb.append("" + "\n");
		sb.append("	    public static String get( String key ) '{'" + "\n");
		sb.append("	        return getString(key);" + "\n");
		sb.append("	    '}'" + "\n");
		sb.append("" + "\n");
		sb.append("	    public static Image getImage( String key ) '{'" + "\n");
		sb.append("	        return INSTANCE.getImageImpl(key);" + "\n");
		sb.append("	    '}'" + "\n");
		sb.append("" + "\n");
		sb.append("	    public static String format(String key, Object ... arguments) '{'" + "\n");
		sb.append("	        return INSTANCE.formatImpl(key, arguments);" + "\n");
		sb.append("	    '}'" + "\n");
		sb.append("" + "\n");
		sb.append("	    public static Icon getIcon(String key) '{'" + "\n");
		sb.append("	        return INSTANCE.getIconImpl(key);" + "\n");
		sb.append("	    '}'" + "\n");
		sb.append("" + "\n");
		sb.append("	    public static Integer getInteger(String key) '{'" + "\n");
		sb.append("	        return INSTANCE.getIntegerImpl(key);" + "\n");
		sb.append("	    '}'" + "\n");
		sb.append("" + "\n");
		sb.append("'}'" + "\n");
		sb.append("" + "\n");
		return sb;
	}
	public static class Builder {
		private String template;
		private Path target;
		private Log log;
		private String regex;
		private File resourceFileCodeTemplate;
		private BuildContext buildContext;
		private Path source;
		private Path resFilePath;
		private boolean convertCase;

		public Builder setTemplate(final String template) {
			this.template = template;
			return this;
		}

		public Path getResFileTemplate() {
			try {
				if (resourceFileCodeTemplate != null) {
					return Paths.get(resourceFileCodeTemplate.getCanonicalPath());
				} else {
					return null;
				}
			} catch (IOException e) {
				log.info(e.getLocalizedMessage());
			}
			return null;
		}

		public Builder addTargetDir(final Path target) {
			this.target = target;
			return this;
		}

		public Builder addLogger(final Log log) {
			this.log = log;
			return this;
		}

		public Builder addPropertyFileRegex(final String propFileRegex) {
			this.regex = propFileRegex;
			return this;
		}

		public Builder addCodeResourceTemplate(final File resourceFileCodeTemplate) {
			if (resourceFileCodeTemplate!=null) {
			  this.resourceFileCodeTemplate = resourceFileCodeTemplate;
			}
			return this;
		}

		public Builder addBuildContext(final BuildContext buildContext) {
			this.buildContext = buildContext;
			return this;
		}

		public Builder addResFilePropertiesPath(final Path resFilePath) {
			this.resFilePath = resFilePath;
			return this;
		}

		public Builder addSourceDir(final Path source) {
			this.source = source;
			return this;
		}

		public ResGen build() {
			ResGen rg = new ResGen(this);
			return rg;
		}
	}
}
