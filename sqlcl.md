  
# SQLcl 
![SQLcl logo](sql-command-line-130.png)
## What is it?
The guts of SQL Developer's sqlplus support married with a command line interface.  

The result is a ~13mb download soon with native installers.  That ~13mb includes everything you need including the jdbc driver to connect and work with your Oracle database.  

Now while replicating sqlplus is interesting and will greatly improve our support for it, it's well boring.  Writing code that does nothing more than mimic other code isn't fun.  So we are adding lots and lots of new things.   The first things you'll notice using sqlcl is the arrow keys work, there's history, there's color these and more are just core things we added.  There's also entirely new commands being added and the easiest way to see some of these new things is to use the help command.  The non-sqlplus command are highlighted.  Give them a try.

![SQLcl screenshot](sql-command-line-screenshot.png) 
## Features
### History
This is awesome. SQLcl has a built in history of all the commands executed. Not just for that particular session but for all previous sessions as well. Its configured to provide the last 100 commands at your fingertips.

You can cycle through them using the <UP> and <DOWN> arrows. 

#### History Options
Type history for a list of all the commands in the history. The commands are formatted so that there is 1 command per line. History also provides the following features.

* history full   : renders each command in the format it was written. Useful when you want to examine multi-line commands.
* history usage  : shows the number of times a command was run.
* history time   : show the length of time a command last took to run.
* history script : shows the last commands in this session in order. As if a script was being written
* history clear  : removes all commands from the history
* history clear session : removes all commands saved in this session from the history

```
SYS@orcl🍺 >his

  1  EXECUTE dbms_output.put_line('select systimestamp from dual')
  2  set server output on
  3  set serveroutput on
  4  EXECUTE dbms_output.put_line('SQLcl is running @'||systimestamp)
  5  clear
  6  clear screen
  7  select 1 as "One", 2 as "Two" from dual

SYS@orcl🍺 >his time

  1  (00.027) EXECUTE dbms_output.put_line('select systimestamp from dual')
  2           set server output on
  3           set serveroutput on
  4  (00.085) EXECUTE dbms_output.put_line('SQLcl is running @'||systimestamp)
  5           clear
  6           clear screen
  7  (00.023) select 1 as "One", 2 as "Two" from dual
  
SYS@orcl🍺 >his full

  1  EXECUTE dbms_output.put_line('select systimestamp from dual')
  2  set server output on
  3  set serveroutput on
  4  EXECUTE dbms_output.put_line('SQLcl is running @'||systimestamp)
  5  clear
  6  clear screen
  7  select 1 as "One",
  >  2 as "Two"
  >  from dual

SYS@orcl🍺 >
```

### Buffer editing
But this is better! What's the point of a history if you can't edit the commands easily. SQLcl provides an intuitive way of working with your command history.

Try this out . Type and run the following
```
SELECT * 
FROM DUAL;
```
* **UP** arrow . The last command run will appear in its original multiline format.
* **LEFT** arrow . You are now editing your chosen command.  Keep <LEFT> arrowing back to the * . 
* Replace * with 'WOW'. 
* **CTRL** **ENTER**  To run the command

####Editing Rules

* **UP** and **DOWN** arrows to choose the command.
* **ENTER** to run the chosen command .
* **LEFT** arrow to start Edit Mode.
 * **LEFT** arrow moves cursor left.
 * **RIGHT** arrow moves cursor right.
 * **UP** arrow moves cursor up one line if the chosen command is multiline.
 * **DOWN** arrow moves cursor down one line if the chosen command is multiline.
 * **CTRL**-**R** to run the chosen command.
 * **ESC** to leave edit mode or place the cursor at the end of the command

Not enough? Well check out HELP EDIT on the command line for a list of shortcuts.

Still not enough ? Well if you would prefer your own text editor to edit commands you can set this up in SQLcl also, the default being **vi** on *Mac* and *UNIX* and **Notepad** default on *Windows*.  Other options for unix include *emacs* and *wordpad* for windows.  

```
define _EDITOR=.....
```  
  
You can even set the editor to the inline editor we use when creating a statement or editing a history item.  You can set this by running:  

```  
define _EDITOR=inline  
```
### Color
What? color?! Feast your eyes.
TODO PIC of query result where dates/numbers are formatted with color and null set to a color

Color used to make your reports pop and increase their readability.


### Insight / Tab Completion


### help
Help on its own gives list of topics, sqlcl only topics are highlighted. Help <command> provides detailed information about that command.

## New Commands

### alias
Alias is similar to bash aliases in that a short user defined name can be used to call one or more sql or plsql commands with arguments converted to binds if there are binds that do not resolve.
######Simple
These can be simple or very complex.  Here is an example of both.  At its simplest, we just define a alias like this  

```
  alias \<name>=\<statement\>;
```  

So a simple example of this would be  

```
  alias tables= select table_name from user_tables;
```

and then running it returns the name of the alias and the results of the statement that you included.  In this case, its a list of the tables in the users schema

```  
HR@orcl🍺 >tables  
Command\=tables

TABLES                                                                                                                         
--------------
REGIONS                                                                                                                         
LOCATIONS                                                                                                                       
JOB_HISTORY                                                                                                                     
JOBS                                                                                                                            
EMPLOYEES                                                                                                                       
DEPARTMENTS                                                                                                                     
COUNTRIES                                                                                                                       

7 rows selected 
HR@orcl🍺 >  

```    
You can see what SQL is being run by the alias.

```
HR@orcl🍻🍺 >alias list tables

tables
------
select table_name "TABLES" from user_tables

HR@orcl🍻🍺 >
```  
######Binds
We can include Binds into the SQL statements or PL/SQL statements.  By automatically using the arguments to the alias as positional binds make very powerful aliases.

```
HR@orcl🍺 >alias bind=select :one from dual;

HR@orcl🍺 >bind demo
Command=bind

:ONE                           
--------------------------------
demo                            

HR@orcl🍺 >
```  

######PL/SQL Aliases
For PLSQL aliases, we support a proper PLSQL block, terminated with a '/' to define the alias.  Here's a simple example below.

```  
HR@orcl🍻🍺 >alias plsql=begin
  2   dbms_output.put_line('Aliases Rule OK');
  3  end;
  4  /

HR@orcl🍺 >plsql
Command=plsql

PL/SQL procedure successfully completed.

Aliases Rule OK

HR@orcl🍺 >

```  





### apex

### bridge

### cd
Changes the current working directory for resolution of scripts, host commands, spooling of files. This will then prepend that directory to the front of the **SQLPATH**

~~~
HR@orcl🍺 >cd examples

HR@orcl🍺 >show SQLPATH
SQLPATH : /Users/username/examples:/Users/bamcgill/:.

HR@orcl🍺 >
~~~

### ctas

### ddl

### format

### history
See features above for a detailed description of history

### information
Information and Information+ is the new and improved describe.  It provides much more detailed information about the object being described including **PRIMARY KEY**, **FOREIGN KEY**

~~~
HR@orcl🍻🍺 >info employees

TABLE: EMPLOYEES 
	 LAST ANALYZED:2015-06-05 22:01:19.0 
	 ROWS         :107 
	 SAMPLE SIZE  :107 
	 INMEMORY     :DISABLED 
	 COMMENTS     :employees table. Contains 107 rows. References with departments,
                       jobs, job_history tables. Contains a self reference. 

Columns
=======
NAME             DATA TYPE           NULL  DEFAULT        COMMENTS
---------------  ------------------  ----  -------  -----------------------------------
*EMPLOYEE_ID     NUMBER(6,0)         No             Primary key of employees table.
 FIRST_NAME      VARCHAR2(20 BYTE)   Yes            First name of the employee. A not null column.
 LAST_NAME       VARCHAR2(25 BYTE)   No             Last name of the employee. A not null column.
 EMAIL           VARCHAR2(25 BYTE)   No             Email id of the employee
 PHONE_NUMBER    VARCHAR2(20 BYTE)   Yes            Phone number of the employee; includes country code and area code
 HIRE_DATE       DATE                No             Date when the employee started on this job. A not null column.
 JOB_ID          VARCHAR2(10 BYTE)   No             Current job of the employee; foreign key to job_id column of the
                                                    jobs table. A not null column.
 SALARY          NUMBER(8,2)         Yes            Monthly salary of the employee. Must be greater
                                                    than zero (enforced by constraint emp_salary_min)
 COMMISSION_PCT  NUMBER(2,2)         Yes            Commission percentage of the employee; Only employees in sales
                                                    department elgible for commission percentage
 MANAGER_ID      NUMBER(6,0)         Yes            Manager id of the employee; has same domain as manager_id in
                                                    departments table. Foreign key to employee_id column of employees table.
                                                    (useful for reflexive joins and CONNECT BY query)
 DEPARTMENT_ID   NUMBER(4,0)         Yes            Department id where employee works; foreign key to department_id
                                                    column of the departments table

Indexes
=======
INDEX_NAME            UNIQUENESS  STATUS  FUNCIDX_STATUS  COLUMNS                COLUMN_EXPRESSION  
--------------------  ----------  ------  --------------  ---------------------  -----------------
HR.EMP_JOB_IX         NONUNIQUE   VALID                   JOB_ID                                    
HR.EMP_NAME_IX        NONUNIQUE   VALID                   LAST_NAME, FIRST_NAME                     
HR.EMP_EMAIL_UK       UNIQUE      VALID                   EMAIL                                     
HR.EMP_EMP_ID_PK      UNIQUE      VALID                   EMPLOYEE_ID                               
HR.EMP_MANAGER_IX     NONUNIQUE   VALID                   MANAGER_ID                                
HR.EMP_DEPARTMENT_IX  NONUNIQUE   VALID                   DEPARTMENT_ID                             


References
==========
TABLE_NAME   CONSTRAINT_NAME  DELETE_RULE  STATUS   DEFERRABLE      VALIDATED  GENERATED
-----------  ---------------  -----------  -------  --------------  ---------  ---------  
DEPARTMENTS  DEPT_MGR_FK      NO ACTION    ENABLED  NOT DEFERRABLE  VALIDATED  USER NAME  
EMPLOYEES    EMP_MANAGER_FK   NO ACTION    ENABLED  NOT DEFERRABLE  VALIDATED  USER NAME  
JOB_HISTORY  JHIST_EMP_FK     NO ACTION    ENABLED  NOT DEFERRABLE  VALIDATED  USER NAME  

HR@orcl🍺 >
~~~

### load

### net

### oerr
Have you got an Oracle error? or a TNS error?  Find out what it is and what to do about it.  

~~~
HR@orcl🍺 >oerr ora 942

00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

HR@orcl🍺 >oerr ora 1031


01031. 00000 -  "insufficient privileges"
*Cause:    An attempt was made to perform a database operation without
           the necessary privileges.
*Action:   Ask your database administrator or designated security
           administrator to grant you the necessary privileges
HR@orcl🍺 >
~~~

### repeat

### rest
List or exports ORDS restful services in the current schema, the output is an anonymous block with ORDS PL/SQL API calls that can be run in a different database with ORDS 3.X installed.

Notes:

* The allowed origins are **not** exported.

* Privileges and Roles are exported in case the services require them.



This is a list of the available options:

**List the schemas**

~~~
rest schemas
~~~

**List the available modules in the current schema**

~~~
rest modules
~~~

**List the privileges visible by the current schema**

~~~
rest privileges
~~~

**Exports all the modules and the related templates, handlers, parameters, privileges and roles**

~~~
rest export
~~~

**Exports a specific module and the related templates, handlers, parameters, privileges and roles**

~~~
rest export <module_name>
~~~

**Exports a specific module that matches a given prefix. It also exports the related templates, handlers, parameters, privileges and roles**

~~~
rest export <module_prefix>
~~~

### script

### sshtunnel

### tnsping

## Enhanced Commands
### connect


As well using **eazyconnect** syntax  

```username/password@host[:port]/service [as role]```  

to connect to the database, we support **TNS** with or without OCI support.
 
```username/password@tnsnamesentry``` 

We also honor **TWO\_TASK**, **LOCAL** and **ORACLE\_SID** in the normal precedence. 
We can also use the full description of a **TNS** connection and user it as the URL for the connect.  

```
connect username/password@(DESCRIPTION =(ADDRESS = (PROTOCOL= TCP)
                      (Host= hostname.network)(Port= 1521))
                         (CONNECT_DATA = (SID = remote_SID)))
```  

username/password@ENTRY where entry is inserted into a template provided by LDAPADMIN environmental or sqlplus style variable: 
```
set LDAPCON jdbc:oracle:thin:@ldap://yourmachine.yourdomain:389/\#ENTRY\#,cn=OracleContext,dc=ldapcdc,dc=lcom
```  
or can be a network alias defined by net command 
```
net ENTRY=localhost:1521/XE;
```
Tab completion works on net entries.

### show
In addition to most sqlplus show features, and show all - show all+ displays additional details beyond the sqlplus show including some multi-line entries.  

#### show tns
Many users have wondered where and how we make a connection in SQLcl when we are using **TNS** aliases.  The biggest question is where is the **tnsnames.ora** that I want to use today?  What location is it in.  Over the years, **Oracle** and **SQL*Plus** users have had several ways to allow connections to be made.  With **show tns** we list out where we are looking for the **tnsnames.ora**, which one we will use *and* a list of all the aliases in that file.  Specifically, the rules are in general  
 
  
```
SYS@orcl🍺 >show tns

TNS Lookup locations
--------------------
1.  USER Home dir
    /Users/username
2.  TNS_ADMIN
    /etc
3.  ORACLE_HOME
    /Users/username/Oracle/network/admin

Location used:
-------------
	/Users/username

Available TNS Entries
---------------------
LOCAL_DB1
LOCAL_DB2
SYS@orcl🍺 >

```

####Show SQLPATH
SQLPATH is used   

```
SYS@orcl🍺 >show sqlpath   
SQLPATH : /Users/username/:.  
SYS@orcl🍺 >cd test  
SYS@orcl🍺 >show sqlpath  
SQLPATH : **/Users/username/test**:/Users/username/:.  
```
