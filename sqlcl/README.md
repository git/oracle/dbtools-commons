![](http://www.oracle.com/technetwork/developer-tools/sqlcl/sqlcl-64-2994744.png)
#Oracle SQL Developer Command Line
Oracle SQL Developer Command Line (SQLcl) is a free command line interface for Oracle Database. 
It allows you to:
 
 - interactively or batch execute SQL and PL/SQL. 
 - provides in-line editing, statement completion, and command recall for a feature-rich experience

All while also supporting your previously written SQL*Plus scripts.
