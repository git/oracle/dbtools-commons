/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.app;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLRecoverableException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import jline.console.completer.Completer;
import oracle.dbtools.arbori.MaterializedPredicate;
import oracle.dbtools.arbori.SqlProgram;
import oracle.dbtools.arbori.Tuple;
import oracle.dbtools.parser.Cell;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Matrix;
import oracle.dbtools.parser.ParseNode;
import oracle.dbtools.parser.Parsed;
import oracle.dbtools.parser.Token;
import oracle.dbtools.parser.plsql.SqlEarley;
import oracle.dbtools.parser.plsql.doc.HarvestDoc;
import oracle.dbtools.raptor.console.HistoryItem;
import oracle.dbtools.raptor.console.MultiLineHistory;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.HiddenParameters;
import oracle.dbtools.raptor.newscriptrunner.commands.net.NetEntries;
import oracle.dbtools.util.Service;

/**
 * Code completion, version 3
 * @author Dim
 */
public class SqlclCompleter extends SqlCompleter implements Completer {
    
    private static final String color32 = "\u001B[32m";

    private static final String color1 = "\u001B[1m";

    private static final String color0 = "\u001B[0m";

    static int approximateIndex = -1; // if no token at cursor
    
    static IProgram programInstance = null;
    
    private ScriptRunnerContext _ctx;

    public SqlclCompleter( ScriptRunnerContext ctx ) {
    	super(null);
        _ctx = ctx;
    }
    
    /**
     * @param candidates true if found expanded command in the history
     * @param sugg
     * @return
     */
    private static void lookupHistory( List<CharSequence> candidates, String prefix ) {
        prefix = prefix.trim();
        ArrayList<HistoryItem> history = MultiLineHistory.getInstance().getHistory();
        int cnt = 0;
        for( HistoryItem item : history ) {
            if( 7 < cnt )
                break;
            List<String> buffer = item.getBuffer();
            StringBuilder suggestion = new StringBuilder();
            for( String line : buffer ) {
                suggestion.append(line);
                if( 150 < suggestion.length() ) {
                    suggestion = null;
                    break;
                } 
            }
            if( suggestion == null )
                continue;
            String newSuggestion = suggestion.toString();
            if( !newSuggestion.toLowerCase().startsWith(prefix.toLowerCase()) )
                continue;
            if( !contains(candidates,newSuggestion) ) {
                candidates.add(newSuggestion);
                cnt++;
            }
            //return true;
        }
    }
    
    private static boolean contains( List<CharSequence> candidates, String entry ) {
        for( CharSequence tmp : candidates ) {
            if( ((String)tmp).toUpperCase().trim().equals(entry.toUpperCase().trim()) )
                return true;
        }
        return false;
    }

    static class IProgram extends SqlProgram {
        public IProgram( String arboriProgram ) throws IOException {
            super(arboriProgram);
        }

        int offset = -1;
    };
    /**
     * Completes the following:
     * 1. Keywords
     * 2. Table list (prefixed and not)
     * 3. Column list in where clause (prefixed and not)
     * @return position in the last line!
     */
    @Override
    public int complete( String lastLine, int cursor, List<CharSequence> candidates ) {
        final String colCompleteKey = "coloredComplete";
        String colComplete = HiddenParameters.parameters.get(colCompleteKey);
        if( colComplete == null && _ctx != null && "ON".equals(_ctx.getProperty(_ctx.SETCOLOR))  ) {
            
            colComplete = "true";
            HiddenParameters.parameters.put(colCompleteKey,colComplete);
        }
        try {
            if( "true".equals(HiddenParameters.parameters.get("debugComplete")) ) {
                System.out.println(color32);
                System.out.println("lastLine="+lastLine+"");
                System.out.println("cursor="+cursor);
            }
        	String line = "";
            int pos = cursor;
            if( _ctx != null ) {
                line =  _ctx.getSQLPlusBuffer().getBufferString(); 
                pos = _ctx.getSQLPlusBuffer().getBufferCursor(_ctx.getSQLPlusBuffer().getCurrentLine(), cursor);
            } else {
                line = lastLine;
                //pos = lastLine.s
            }
            
            if( "true".equals(HiddenParameters.parameters.get("debugComplete")) ) {
                System.out.println("line="+line);
                System.out.println("pos="+pos);
            }
            
            CompletionList insight = complete(line,pos);
			for( CompletionItem item : insight.entries )
            	candidates.add(item.entry);
            
            if( candidates.size() == 1 && 0 < candidates.get(0).toString().indexOf(' ') ) // history completion
                return 0;
            if( candidates.size() == 1 ) {
                String cleaned = null;
                for( CharSequence c: candidates ) {
                    cleaned = c.toString();
                    cleaned = discolor(cleaned);
                }
                candidates.clear();
                candidates.add(cleaned);
            }
            int ret = cursor;
            if( insight.prefix != null )
                ret = ret-insight.prefix.content.length();
            if( "true".equals(HiddenParameters.parameters.get("debugComplete")) ) 
                System.out.println("return="+ret+color0);
            return ret;
        /*} catch (SQLRecoverableException e) {
        	//Eat this if the connection dies.
        	//We wont want to do anything on this thread, rather the amin one
        	return 0;*/
        } catch( Throwable t ) {
            t.printStackTrace();
            return 0;
        }
    }

    public static String discolor( String cleaned ) {
        cleaned = cleaned.replace(color0, "");
        cleaned = cleaned.replace(color1, "");
        cleaned = cleaned.replace(color32, "");
        return cleaned;
    }    
    
    @Override
    public Connection getConnection() throws SQLException {
        Connection conn = null;
        if( _ctx != null )
            conn = _ctx.getBaseConnection();
        else {
            conn = DriverManager.getConnection("jdbc:oracle:thin:@gbr30060.uk.oracle.com:1521/DB12PERF","hr","hr"); 
        }
        return conn;
    }


}
