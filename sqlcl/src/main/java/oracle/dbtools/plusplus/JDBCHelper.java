/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.plusplus;

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.PlatformUtils;
import oracle.dbtools.raptor.utils.WindowsUtility;

/**
 * Helper class to figure out if an ORACLE_HOME exists and some functions on it
 * if it does. Primarily this has been created to identify jdbc locations within
 * an ORACLE_HOME.
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=JDBCHelper.java">
 *         Barry McGillin</a>
 *
 */
public class JDBCHelper {
	public static final String ENV_OH = "ORACLE_HOME"; //$NON-NLS-1$
	private static final String JDBCJAR8 = "ojdbc8.jar"; //$NON-NLS-1$
	private static final String JDBCJAR7 = "ojdbc7.jar"; //$NON-NLS-1$
	private static final String JDBCJAR6 = "ojdbc6.jar"; //$NON-NLS-1$
	private static Logger logger = Logger.getLogger("oracle.dbtools.plusplus"); //$NON-NLS-1$

	/**
	 * Check if ORACLE_HOME is set anywhere on the system.
	 * 
	 * @return true if it is set. This does not prove it is usable though.
	 */
	public static boolean isOHSet() {
		String oh = getOH();
		if (oh != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if an ORACLE_HOME is real.
	 * 
	 * @return true if ORACLE_HOME is set and actually exists.
	 */
	public static boolean isOHReal() {
		String oh = getOH();
		if (oh != null) {
			// OracleHome is set. Is it real?
			File dir = new File(oh);
			if (dir.exists()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get ORACLE_HOME varaible if it is set in Nix or Windows.
	 * 
	 * @return the ORACLE_HOME directory String or null if it is not set
	 */
	public static String getOH() {
		boolean isWindows = PlatformUtils.isWindows();
		String oh = System.getenv().get(ENV_OH);
		if (oh == null && isWindows) {
			// Its not an environment variable. Is it set in Winders?
			try {
				String key = "SOFTWARE\\ORACLE"; //$NON-NLS-1$
				List<String> oracle_keys = WindowsUtility.readStringSubKeys(WindowsUtility.HKEY_LOCAL_MACHINE, key);
				String homeKey = null;
				if (oracle_keys != null) {
					for (String oracle_key : oracle_keys) {
						String temp = key + "\\" + oracle_key; //$NON-NLS-1$
						homeKey = WindowsUtility.readString(WindowsUtility.HKEY_LOCAL_MACHINE, temp, "ORACLE_HOME_KEY"); //$NON-NLS-1$
						if (homeKey != null) {
							oh = WindowsUtility.readString(WindowsUtility.HKEY_LOCAL_MACHINE, homeKey, "ORACLE_HOME"); //$NON-NLS-1$
						}
					}
				}
			} catch (Exception e) {
				logger.log(Level.WARNING, e.getStackTrace()[0].toString(), e);
			}
		}
		return oh;
	}

	/**
	 * There are two places we look for JDBC when we have an ORACLE_HOME. The
	 * first is the usual in an ORACLE install which is
	 * $ORACLE_HOME/jdbc/lib/ojdbc6.jar. If we have an instant client, we use an
	 * ORACLE_HOME to delineate where the instant client is. This is usually a
	 * list of library files only including the jdbc jar so we can also look for
	 * $ORACLE_HOME/ojdbc6.jar
	 * 
	 * @return true if we find one of them.
	 */
	public static boolean doesOHJDBCExist() {
		if (!isOHReal()) {
			return false;
		}
		//Try ojdbc8.jar first, then for older OH, try 6
		File jar = new File(getOHJDBCLib() + File.separator + JDBCJAR8);
		if (jar != null && jar.exists()) {
			return true;
		}
		jar = new File(getOHJDBCLib() + File.separator + JDBCJAR7);
		if (jar != null && jar.exists()) {
			return true;
		}
		jar = new File(getOHJDBCLib() + File.separator + JDBCJAR6);
		if (jar != null && jar.exists()) {
				return true;
		}
		return false;
		
	}

	/**
	 * Get the jdbc lib directory from Oracle home if it exists
	 * 
	 * @return the String for the dir or null if not set.
	 */
	public static String getOHJDBCLib() {
		if (isOHReal()) {
			String lib = getOH() + File.separator + "jdbc" + File.separator + "lib"; //$NON-NLS-1$ //$NON-NLS-2$
			File f = new File(lib);
			if (f.exists()) {
				return lib;
			} else {
				return getOH();
			}
		} else {
			return null;
		}
	}

	/**
	 * Does ORACLE_HOME/jdbc/lib exist?
	 * 
	 * @return true if it exists
	 */
	public static boolean doesOHJDBCLibExist() {
		File jdbclib = new File(getOHJDBCLib());
		if (jdbclib.exists())
			return true;
		return false;
	}
}
