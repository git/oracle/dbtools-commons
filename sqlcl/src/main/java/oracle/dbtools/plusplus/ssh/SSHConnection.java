/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.plusplus.ssh;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SSHConnection.java"
 *         >Barry McGillin</a>
 *
 */
public class SSHConnection {
	ConnType m_type;
	String m_host;
	String m_user;
	String m_identityFile;
	String m_basePort;
	String m_portForward;
	String m_password;
	Properties m_config;
	Session session;
	ScriptRunnerContext m_ctx;

	public enum Result {
		CONNECTED, NOTCONNECTED, ERROR
	}

	public enum ConnType {
		NONE, PASSWORD, KEYFILE
	}

	public SSHConnection(ScriptRunnerContext ctx, ConnType type, String host, String user, String identity,
			String basePort, String portf, String pass) {
		m_ctx = ctx;
		m_type = type;
		m_host = host;
		m_user = user;
		m_identityFile = identity;
		m_basePort = basePort;
		m_portForward = portf;
		m_password = pass;
		m_config = new Properties();
		m_config.put("StrictHostKeyChecking", "no");
	}

	public boolean connect() {
		JSch jsch = new JSch();

		try {
			if (m_identityFile != null && !m_identityFile.equals("")) {
				jsch.addIdentity(m_identityFile);
			} else {
				Path defaultIdFilePath =  Paths.get(System.getProperty("user.home"), ".ssh/id_rsa");
				if (defaultIdFilePath.toFile().exists()) {
					jsch.addIdentity(defaultIdFilePath.toString());
				}
			}
			int port = 22;
			try {
				port = Integer.parseInt(m_basePort);
			} catch (NumberFormatException nfe) {
				// bad number so use the default 22
			}
			System.out.println("Using port:" + port);

			session = jsch.getSession(m_user, m_host, port);
			session.setUserInfo(new SQLUserInfo(m_ctx));
			if (m_password != null && m_password.length() > 0)
				session.setPassword(m_password);
			session.setConfig(m_config);
			if (m_portForward != null && m_portForward.split(":").length == 3) {
				String[] parts = m_portForward.split(":");
				int lport = Integer.parseInt(parts[0]);
				String rhost = parts[1];
				int rport = Integer.parseInt(parts[2]);
				int assignedPort = session.setPortForwardingL(lport, rhost, rport);
			}
			session.connect();

			if (session.isConnected()) {
				return true;
			} else {
				return false;
			}
		} catch (JSchException e) {
			m_ctx.write(e.getLocalizedMessage() + "\n");
			return false;
		}
	}

	public boolean isConnected() {
		return session.isConnected();
	}

	public Session getSession() {
		return session;
	}

	public void disconnect() {
		if (getSession() != null && isConnected()) {
			session.disconnect();
		}

	}

}
