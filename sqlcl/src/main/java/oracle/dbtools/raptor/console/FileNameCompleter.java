/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.console;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=FileNameCompleter.java">Barry McGillin</a> 
 *
 */

import static jline.internal.Preconditions.checkNotNull;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import jline.console.completer.Completer;
import jline.internal.Configuration;

/**
 * A file name completer takes the buffer and issues a list of potential
 * completions.
 * <p/>
 * This completer tries to behave as similar as possible to <i>bash</i>'s file
 * name completion (using GNU readline) with the following exceptions:
 * <p/>
 * <ul>
 * <li>Candidates that are directories will end with "/"</li>
 * <li>Wildcard regular expressions are not evaluated or replaced</li>
 * <li>The "~" character can be used to represent the user's home, but it cannot
 * complete to other users' homes, since java does not provide any way of
 * determining that easily</li>
 * </ul>
 *
 * @author <a href="mailto:mwp1@cornell.edu">Marc Prud'hommeaux</a>
 * @author <a href="mailto:jason@planet57.com">Jason Dillon</a>
 * @since 2.3
 */
public class FileNameCompleter implements Completer {

	private ScriptRunnerContext _ctx;

	public FileNameCompleter(ScriptRunnerContext ctx) {
		_ctx = ctx;
	}

	private static final boolean OS_IS_WINDOWS;

	static {
		String os = Configuration.getOsName();
		OS_IS_WINDOWS = os.contains("windows");
	}

	public int complete(String buffer,  int cursor, final List<CharSequence> candidates) {
		if (buffer == null) {
			buffer = "";
		} else if (!(buffer.toLowerCase().startsWith("@") || buffer.toLowerCase().startsWith("start") || buffer.toLowerCase().startsWith("!")
				|| buffer.toLowerCase().startsWith("host") || buffer.toLowerCase().startsWith("load") || buffer.toLowerCase().startsWith("cd")
				|| buffer.toLowerCase().startsWith("get") || buffer.toLowerCase().startsWith("save") || buffer.toLowerCase().startsWith("store"))) {
			return -1;
		}
		// buffer can be null
		checkNotNull(candidates);
		if (OS_IS_WINDOWS) {
			buffer = buffer.replace('/', '\\');
		}
		String[] tokens = buffer.split("\\s+|@");
		int tokenAtCursor = getTokenAtcursor(tokens, buffer, cursor);
		String translated = "";
		if (tokens.length > 0)
			translated = tokens[tokenAtCursor];
		String token2Complete = translated;
		File homeDir = getUserHome();
		// Special character: ~ maps to the user's home directory
		if (translated.startsWith("~") || translated.startsWith("~" + separator())) {
			try {
				translated = translated.replaceFirst("~", homeDir.getCanonicalPath());
			} catch (IOException e) {

			}
		} else if (!(new File(translated).isAbsolute())) {
			String cwd = getUserDir().getAbsolutePath();
			translated = cwd + separator() + translated;
		} else {
			if (!(new File(translated)).isDirectory()) {
				String tmp = _ctx.prependCD(translated.substring(1));
				if( !(new File(tmp)).isDirectory() ) {
					int pos = translated.lastIndexOf(File.separator)+File.separator.length();
					token2Complete = translated.substring(0, pos);
				} else
					translated = tmp;
			}
		}
		if (translated.endsWith("..")) {
			translated=translated+File.separator;
			cursor=cursor+1;
			buffer=buffer.replace("..", "../");
			token2Complete=token2Complete+File.separator;
			candidates.add("../");
		}
		File file = new File(translated);
		final File dir;
		if (file.isDirectory()) {
			dir = file;
		} else {
			dir = file.getParentFile();
		}
		File[] entries = dir == null ? new File[0] : dir.listFiles();
		return matchFiles(buffer, cursor, token2Complete, translated, entries, candidates);
	}

	private int getTokenAtcursor(String[] tokens, String buffer, int cursor) {
		for (int y = 0; y < tokens.length; y++) {
			String regex = tokens[y];
			if (regex.trim().length() > 0) {
				regex = regex.replace("\\", "\\\\");
				Pattern p = Pattern.compile(regex);
				Matcher m = p.matcher(buffer);
				while (m.find()) {
					if (m.start() <= cursor && cursor <= m.end()) {
						return y;
					}
				}
			}
		}
		return 0;
	}

	protected String separator() {
		return File.separator;
	}

	protected File getUserHome() {
		return Configuration.getUserHome();
	}

	protected File getUserDir() {
		// TODO
		return new File(_ctx.prependCD(""));
	}

	protected int matchFiles(final String buffer, final int cursor, final String token2Complete, final String translated, final File[] files,
			final List<CharSequence> candidates) {
		if (files == null) {
			return -1;
		}

		int matches = 0;

		// first pass: just count the matches
		for (File file : files) {
			if (file.getAbsolutePath().startsWith(translated)) {
				matches++;
			}
		}
		for (File file : files) {
			if (file.getAbsolutePath().startsWith(translated)) {
				CharSequence name = file.getName() + (matches == 1 && file.isDirectory() ? separator() : " ");
				candidates.add(render(file, name).toString());
			}
		}

		// Check if it is a directory. If so, then we need to just refresh to
		// the file separator, else, go back to the start of the token.
		if (token2Complete.contains(File.separator)) {
			return buffer.lastIndexOf(File.separator) + File.separator.length();
		} else {
			return cursor - token2Complete.length();
		}
	}

	protected CharSequence render(final File file, final CharSequence name) {
		return name;
	}
}
