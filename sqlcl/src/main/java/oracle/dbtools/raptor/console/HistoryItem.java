/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.console;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import oracle.dbtools.raptor.newscriptrunner.SqlParserProvider;

/**
 * History Item for SQLPlus items.
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=HistoryItem.java"
 *         >Barry McGillin</a>
 *
 */
public class HistoryItem {
	private String original = "";
	private String timestamp = "";
	private int timesUsed = 0;
	private long timing = 0;
	private boolean inCurrentSession = false;
	private boolean fail = false;

	public HistoryItem(String data) {
		original = data;
		Date tmpDate = new Date(System.currentTimeMillis());
		timestamp = DateFormat.getDateInstance().format(tmpDate);
		timesUsed = 1;
		inCurrentSession = true;
	}

	public HistoryItem(String data, String timeStamp, String times, long timing) {
		this(data,timeStamp,times,timing,false);
	}
	
	public HistoryItem(String data, String timeStamp, String times, long timing, boolean fail) {
		original = data;
		timestamp = timeStamp;
		timesUsed = Integer.parseInt(times);
		this.timing = timing;
		this.setFail(fail);
	}

	public String getData() {
		return original;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setData(String data) {
		original = data.trim();
	}

	public int getTimesUsed() {
		return timesUsed;
	}

	public void dingTimesUsed() {
		timesUsed += 1;
		inCurrentSession = true;
	}

	public List<String> getBuffer() {
		String[] array = original.split("\\r?\\n");
		return Arrays.asList(array);
	}

	/**
	 * @return the timing
	 */
	public long getTiming() {
		return timing;
	}

	/**
	 * @param timing
	 *            the timing to set
	 */
	public void setTiming(long timing) {
		this.timing = timing;
	}
	
	public boolean inCurrentSession(){
		return inCurrentSession;
	}

	public void setIsCurrentSession(boolean isCurrentSession) {
		inCurrentSession = isCurrentSession;
	}

	/**
	 * @return the fail
	 */
	public boolean isFail() {
		return fail;
	}

	/**
	 * @param fail the fail to set
	 */
	public void setFail(boolean fail) {
		this.fail = fail;
	}

}
