/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.console;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import oracle.dbtools.raptor.newscriptrunner.commands.alias.Alias;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=HistoryParser.java"
 *         >Barry McGillin</a>
 *
 */
public class HistoryParser {
	private static final String HISTORY = "history";
	private static final String HISTORYITEM = "historyItem";

	public static String ORIGINAL = "original";
	public static String TIMESTAMP = "timestamp";
	public static String TIMESUSED = "timesUsed";
	public static String TIMING = "timing";
	public static String FAILURE = "failure";
	protected static Logger LOGGER = Logger.getLogger(HistoryParser.class.getName());
	private XMLDocument _document;
	private DOMParser parser;
	ArrayList<HistoryItem> history = new ArrayList<HistoryItem>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public HistoryParser() {
	}

	public HistoryParser(URL u) {
		try {
			parser.parse(u.openStream());
			_document = parser.getDocument();
			// XMLHelper.
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<HistoryItem> processXML(URL u) throws Exception {
		if (parser == null) {
			parser = new DOMParser();
		} else {
			parser.reset();
		}
		parser.setPreserveWhitespace(false);
		parser.parse(u.openStream());
		_document = parser.getDocument();
		NodeList rootNodes = _document.getChildNodes();
		for (int i = 0; i < rootNodes.getLength(); i++) {
			processNodes(rootNodes.item(i));
		}
		return history;
	}

	private void processNodes(Node item) {
		NodeList children = item.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			String childName = child.getNodeName();
			if (childName.equalsIgnoreCase(HISTORYITEM)) { //$NON-NLS-1$
				newItem(child);
			}
		}
	}

	private void newItem(Node child) {
		String original = "";
		String timeStamp = "";
		long timing = 0;
		String timesUsed = "";
		boolean failure=false;
		NodeList items = child.getChildNodes();
		for (int i = 0; i < items.getLength(); i++) {
			Node x = items.item(i);
			if (x.getNodeName().equalsIgnoreCase(ORIGINAL)) {
				Node n1 = x.getFirstChild();
				if (n1 instanceof Text) {
					original = ((Text) n1).getTextContent();
				}
			}
			if (x.getNodeName().equalsIgnoreCase(TIMESTAMP)) {
				Node n1 = x.getFirstChild();
				if (n1 instanceof Text) {
					timeStamp = ((Text) n1).getTextContent();
				}
			}
			if (x.getNodeName().equalsIgnoreCase(TIMESUSED)) {
				Node n1 = x.getFirstChild();
				if (n1 instanceof Text) {
					timesUsed = ((Text) n1).getTextContent();
				}
			}
			if (x.getNodeName().equalsIgnoreCase(TIMING)) {
				Node n1 = x.getFirstChild();
				if (n1 instanceof Text) {
					String t  = ((Text) n1).getTextContent();
					timing= Long.valueOf(t);
				}
			}
			if (x.getNodeName().equalsIgnoreCase(FAILURE)) {
				Node n1 = x.getFirstChild();
				if (n1 instanceof Text) {
					String t  = ((Text) n1).getTextContent();
					failure= Boolean.valueOf(t);
				}
			}
		}
		history.add(new HistoryItem(original, timeStamp, timesUsed, timing, failure));
	}

	public void putXML(File f, ArrayList<HistoryItem> history) throws Exception {
		f.createNewFile();
		Document dom;
		Element e = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		dom = db.newDocument();
		Element rootEle = dom.createElement(HISTORY);
		// create data elements and place them under root
		for (HistoryItem item : history) {
			e = dom.createElement(HISTORYITEM);
			String original = item.getData();
			int timesUsed = item.getTimesUsed();
			String timing = Long.toString(item.getTiming());
			String timeStamp = item.getTimestamp();
			boolean failure = item.isFail();
			Element tElemt = dom.createElement(ORIGINAL);
			tElemt.appendChild(dom.createCDATASection(original));
			e.appendChild(tElemt);
			tElemt = dom.createElement(TIMESTAMP);
			tElemt.appendChild(dom.createCDATASection(timeStamp));
			e.appendChild(tElemt);
			tElemt = dom.createElement(TIMESUSED);
			tElemt.appendChild(dom.createCDATASection(timesUsed+""));
			e.appendChild(tElemt);
			tElemt = dom.createElement(TIMING);
			tElemt.appendChild(dom.createCDATASection(timing));
			e.appendChild(tElemt);
			tElemt = dom.createElement(FAILURE);
			tElemt.appendChild(dom.createCDATASection(String.valueOf(failure)));
			e.appendChild(tElemt);
			rootEle.appendChild(e);
		}
		
		dom.appendChild(rootEle);
		FileOutputStream fos=null;
		try {
			Transformer tr = TransformerFactory.newInstance().newTransformer();
			tr.setOutputProperty(OutputKeys.INDENT, "yes");
			tr.setOutputProperty(OutputKeys.METHOD, "xml");
			tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			 tr.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS,
					 ORIGINAL+" "+TIMESTAMP+" "+TIMESUSED+" "+TIMING);
			tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			fos=new FileOutputStream(f.getAbsolutePath());
			tr.transform(new DOMSource(dom), new StreamResult(fos));
		} finally {
			if (fos!=null) {
				try {
					fos.close();
				} catch (IOException ioe) {
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,ioe.getLocalizedMessage());
				}
			}
		}
	}
}
