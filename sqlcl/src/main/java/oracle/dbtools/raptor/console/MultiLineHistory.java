/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.console;

import static jline.internal.Preconditions.checkNotNull;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Node;

import jline.console.history.History;
import oracle.dbtools.common.utils.PlatformUtils;
import oracle.dbtools.raptor.newscriptrunner.ICommandHistory;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;
import oracle.dbtools.raptor.scriptrunner.cmdline.editor.Buffer;
import oracle.dbtools.raptor.scriptrunner.cmdline.editor.IndexBuilder;

/**
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=History.java"
 *         >Barry McGillin</a>
 *
 */
public class MultiLineHistory implements IHelp, History, ICommandHistory {
	private static final String HISTORY = "history.xml";
	public static final String CONFIGFILE=File.separator+HISTORY;
	public static final String CONFIGBACK=File.separator+HISTORY+"~";

	private static MultiLineHistory INSTANCE;
	private ArrayList<HistoryItem> history = new ArrayList<HistoryItem>();
	public static final int DEFAULT_MAX_ITEMS = 100;
	public static final int DEFAULT_MAX_FAILS = 20;
	private int maxItems = DEFAULT_MAX_ITEMS;
	private int maxFails = DEFAULT_MAX_FAILS;
	private int offset = 0;
	private int index = 0;
	private int maxTimeLength = 0;
	private int maxTimesUsed = 0;
	private int terminalWidth = 80;
	private String noHistory;
	private String baseBlacklist = "";
	private boolean nofails = false;

	public int getSize() {
		return history.size();
	}
	
	/**
	 * If we allow fails, then we shoe all and if not, then just the non fails.
	 * @return
	 */
	public int getVisibileSize() {
		if (showFailures())
		return getSize();
		else {
			int items=0;
			Iterator<HistoryItem> it =history.iterator();
			while (it.hasNext()) {
				HistoryItem item = it.next();
				if (!item.isFail()) {
					items++;
				}
			}
			return items;
		}
	}
	
	
	public List<String> getVisibleItem(int item) {
		if (showFailures()) {
			return getItem(item);
		} else {
			if (item<1 ) {
				item=1;
			}
			if (item>getVisibileSize()) {
				item=getVisibileSize();
			}
			Iterator<HistoryItem> it = history.iterator();
			int i = 0;
			HistoryItem value=null;
			while (it.hasNext()) {
				HistoryItem hi = it.next();
				if (!hi.isFail()) {
				 i++;
				 if (i==item) {
					 value=hi;
				}
				}
				
			}
			return value.getBuffer();
		}
	}
	
	public String getBaseBlackList() {
		return baseBlacklist;
	}
	public void setBaseBlackList(String blacklist) {
		baseBlacklist=blacklist;
	}
	public static String getBase() {
		String storage=System.getProperty("user.home")+File.separator+".sqlcl";
		if (PlatformUtils.isWindows()) {
			String appData=System.getenv("APPDATA");
			if (appData!=null) {
				storage=appData+File.separator+"sqlcl";
			} else {
				storage=null;
			}
		}
		return storage;
	}
	/**
	 * @return the history
	 */
	public ArrayList<HistoryItem> getHistory() {
		return history;
	}

	/**
	 * @param history
	 *            the history to set
	 */
	public void setHistory(ArrayList<HistoryItem> history) {
		this.history = history;
	}

	/**
	 * @return the maxItems
	 */
	public int getMaxItems() {
		return maxItems;
	}

	public MultiLineHistory(String stoargeDir) {
		File f = new File(stoargeDir);
		if (f.exists()) {
			// Check for history.xml
			File h = new File(stoargeDir + File.pathSeparator + HISTORY);
			if (h.exists()) {
				load(h);
			}
		}
		INSTANCE = this;
	}

	private void load(File history) {
		// TODO Auto-generated method stub

	}

	/**
	 * Set the max size of the history
	 * 
	 * @param maxItems
	 *            the maxItems to set
	 */
	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}
	public void addItem(String item, long duration, boolean fail) {
		if (item!=null && item.length()>0) {
			Iterator<HistoryItem> it = history.iterator();
			HistoryItem hi = null;
			boolean found = false;
			while (it.hasNext()) {
				hi = it.next();
				if (hi.getData().replaceAll("\\s|;|/", "").equals(item.replaceAll("\\s|;|/", ""))) {
					hi.dingTimesUsed();
					if (hi.getTimesUsed()>maxTimesUsed) {
						maxTimesUsed=hi.getTimesUsed();
					}
					found = true;
					break;
				}
			}
			if (!found) {
				if (history.size() >= maxItems) {
					history.remove(0);
				}
				if (!nohistory(item)) {
					hi = new HistoryItem(item);
					hi.setTiming(duration);
					hi.setFail(fail);
					if (atMaxFails()) {
						removeOldestFail();
					}
					if (getTimingString(duration).length()>maxTimeLength) {
						maxTimeLength=getTimingString(duration).length();
					}
					history.add(hi);
					maybeResize();
				}
				
			} else {
				//Move current item found to last item.
				if (item!=null) {
					history.remove(hi);
					history.add(hi);
				}
			}
			index=history.size();
		}

	}
	/**
	 * Remove the first failed item from the list.
	 */
	private void removeOldestFail() {
		Iterator<HistoryItem>it = history.iterator();
		while(it.hasNext()) {
			HistoryItem hi = it.next();
			if (hi.isFail()) {
				history.remove(hi);
				return;
			}
		}
	}
	/**
	 * Is there the max amount of fails in the list
	 */
	private boolean atMaxFails() {
		int fails=0;
		Iterator<HistoryItem> it = history.iterator();
		while(it.hasNext()) {
			HistoryItem item = it.next();
			if (item.isFail()) {
				fails++;
			}
		}
		if (fails>=getMaxFails()) {
			return true;
		}
		return false;
	}
	public void addItem(String item, long duration) {
		addItem(item, duration, false);
	}
	private boolean nohistory(String item) {
		if (item.length()>0 ) {
			String command="";
			if (getNoHistory()!=null) {
				String[] cmds = getNoHistory().split(",");
				if ( item.contains(" "))
				 command = item.split("\\s+")[0];
				else 
					command=item;
				for (String cmd: cmds) {
					if (cmd.toLowerCase().startsWith(command.toLowerCase())) {
						return true;
					}
				}
			}
		}
		return false;
	}
	public void addItem(String item) {
		addItem(item, 0);
	}

	public void setTermWidth(int width) {
		terminalWidth = width;
	}
	
	public String listHistory(boolean timesExec, boolean fullList, boolean duration, boolean script, boolean showFails) {
		Iterator<HistoryItem> it = history.iterator();
		int i = 1;
		StringBuilder sb = new StringBuilder();
		while (it.hasNext()) {
			HistoryItem hi = it.next();
			if (!showFails && hi.isFail()) continue;
			if(!script || (script && hi.inCurrentSession())){
				String lineItem = IndexBuilder.getIndex(i, -1);
				if (timesExec) {
					String format="";
					if (maxTimesUsed>=0 && maxTimesUsed<10) {
						format="%d";
					} else if (maxTimesUsed>=10 && maxTimesUsed <100) {
						format="%2d";
					} else if (maxTimesUsed>100) {
						format="%3d";
					}
					lineItem += MessageFormat.format("({0}) ", new Object[] { String.format(format, hi.getTimesUsed()) });
				}
				if (duration) {
					String durationPad = "";
					if (hi.getTiming()>0) {
						String durAct = MessageFormat.format("({0}) ", new Object[] { getTimingString(hi.getTiming()) });
						int padSize = maxTimeLength - durAct.length();
						if (padSize>0) {
							 durationPad = new String(new char[padSize+2]).replace('\0', ' ');
						}
						lineItem += durationPad + durAct;
					}  else {
						durationPad=new String(new char[maxTimeLength+3]).replace('\0', ' ');
						lineItem += durationPad;
					}
				}
				StringBuilder result = new StringBuilder();
				int indexLength = lineItem.length();
				String pad = new String(new char[indexLength]).replace('\0', ' ');
				pad = pad.substring(0, pad.length() - 3) + ">" + pad.substring(pad.length() - 2);
				if (fullList) {
					Iterator<String> hit = hi.getBuffer().iterator();
					boolean firstLine = true;
					while (hit.hasNext()) {
						String historyItem = hit.next();
						if(script){
							sb.append(historyItem+"\n");
						} else{
							if (firstLine) {
								sb.append(lineItem + historyItem + "\n");
								firstLine = false;
							} else {
								sb.append(pad + historyItem + "\n");
							}
						}
					}
					if(script){//put a statement terminator in like ";" or "/"
						sb.deleteCharAt(sb.length()-1);//remove the last \n
						String terminator = sb.toString().endsWith(";")?"\n/":";";
						sb.append(terminator+"\n");
					}
				} else {
					String x = lineItem + hi.getData().replace("\n", " ").replace("\r", " ");
					if (x.length()>=terminalWidth) {
						x=x.substring(0,terminalWidth-1);
					}
					result.append(x);
				}
				sb.append(result.toString() + "\n");
				i++;
			}
		}
		return sb.toString();
	}

	/**
	 * 
	 * @param item
	 * @return
	 */
	public List<String> getItem(int item) {
		if (item<1 ) {
			item=1;
		}
		if (item>history.size()) {
			item=history.size();
		}
		HistoryItem hi = history.get(item - 1);
		return hi.getBuffer();
	}

	public static void main(String[] args) {
		MultiLineHistory history = MultiLineHistory.getInstance();
		history.addItem("select\n1\nfrom\ndual where 1\n=1");
		history.addItem("select\n1\nfrom\ndual where 1\n=1");
		history.addItem("select\n2\nfrom\ndual where 1\n=1");
		history.addItem("select\n3\nfrom\ndual where 1\n=1");
		history.addItem("select\n4\nfrom\ndual where 1\n=1");
		history.addItem("select\n1\nfrom\ndual where 1\n=1");
		history.addItem("select\n1\nfrom\ndual where 1\n=1");
		history.addItem("select\n2\nfrom\ndual where 1\n=1");
		history.addItem("select\n3\nfrom\ndual where 1\n=1");
		history.addItem("select\n4\nfrom\ndual where 1\n=1");
		history.addItem("select\n1\nfrom\ndual where 1\n=1");
		history.addItem("select\n1\nfrom\ndual where 1\n=1");
		history.addItem("select\n2\nfrom\ndual where 1\n=1");
		history.addItem("select\n3\nfrom\ndual where 1\n=1");
		history.addItem("select\n4\nfrom\ndual where 1\n=1");
		System.out.println(history.listHistory(true, true, true, true, true));
	}

	public static MultiLineHistory getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new MultiLineHistory(System.getProperty("user.name") + File.separator + ".sqlplus");
			INSTANCE.load();
		}
		return INSTANCE;
	}

	public void addHistoryItem(Node child) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getCommand() {
		return "HISTORY";
	}

	@Override
	public String getHelp() {
		return CommandsHelp.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return false;
	}

	@Override
	public int size() {
		return history.size();
	}

	@Override
	public boolean isEmpty() {
		if (history == null || (history != null && history.size() == 0))
			return true;
		return false;
	}

	@Override
	public int index() {
		return offset + index;
	}

	@Override
	public void clear() {
		history.clear();
		index=0;
		maxTimeLength=0;
		maxTimesUsed=0;
	}

	@Override
	public CharSequence get(int index) {
		// Force this into a buffer and switch it back to a String which is a
		// charsequence!
		if (history.isEmpty() ) 
			return "";
		Buffer tmp = new Buffer();
		tmp.resetBuffer(getItem(index));
		return tmp.getBuffer();
	}

	public void add(CharSequence item) {
		checkNotNull(item);

		if (isAutoTrim()) {
			item = String.valueOf(item).trim();
		}

		if (isIgnoreDuplicates()) {
			if (!history.isEmpty() && item.equals(history.get(history.size()))) {
				return;
			}
		}

		internalAdd(item);
	}

	protected void internalAdd(CharSequence item) {
		this.addItem(item.toString());
		maybeResize();
	}

	@Override
	public void set(int index, CharSequence item) {
        history.set(index - offset, new HistoryItem(item.toString()));
	}

	@Override
	public CharSequence remove(int i) {
		checkNotNull(history);
		checkNotNull(i);
		HistoryItem item = history.get(i);
		history.remove(i);
		return item.getData();
	}

	
	@Override
	public CharSequence removeFirst() {
		ListIterator<HistoryItem> it = history.listIterator();
		CharSequence seq = it.next().getData();
		it.remove();
		return seq;
	}

	@Override
	public CharSequence removeLast() {
		HistoryItem item = history.get(history.size());
		history.remove(history.size());// remove last
		return item.getData();
	}

	@Override
	public void replace(CharSequence item) {
		removeLast();
		add(item);

	}

	public ListIterator<Entry> entries(final int index) {
		return new EntriesIterator(index - offset);
	}

	public ListIterator<Entry> entries() {
		return entries(offset);
	}

	@Override
	public Iterator<Entry> iterator() {
		return entries();
	}

	/**
	 * Return the content of the current buffer.
	 */
	@Override
	public CharSequence current() {
		if (index >= size()) {
			return "";
		}
		return history.get(index).getData();
	}

	/**
	 * Move the pointer to the previous element in the buffer.
	 *
	 * @return true if we successfully went to the previous element
	 */
	@Override
	public boolean previous() {
		if (index <= 0) {
			return false;
		}
		index--;

		return true;
	}

	/**
	 * Move the pointer to the next element in the buffer.
	 *
	 * @return true if we successfully went to the next element
	 */
	@Override
	public boolean next() {
		if (index >= size()) {
			return false;
		}
		index++;
		return true;
	}

	/**
	 * Moves the history index to the first entry.
	 *
	 * @return Return false if there are no entries in the history or if the
	 *         history is already at the beginning.
	 */
	public boolean moveToFirst() {
		if (size() > 0 && index != 0) {
			index = 0;
			return true;
		}

		return false;
	}

	/**
	 * This moves the history to the last entry. This entry is one position
	 * before the moveToEnd() position.
	 *
	 * @return Returns false if there were no history entries or the history
	 *         index was already at the last entry.
	 */
	@Override
	public boolean moveToLast() {
		int lastEntry = size() - 1;
		if (lastEntry >= 0 && lastEntry != index) {
			index = size() - 1;
			return true;
		}

		return false;
	}

	/**
	 * Move to the specified index in the history
	 * 
	 * @param index
	 * @return
	 */
	@Override
	public boolean moveTo(int index) {
		index -= offset;
		if (index >= 0 && index < size()) {
			this.index = index;
			return true;
		}
		return false;
	}

	@Override
	public void moveToEnd() {
		index = history.size();

	}
	
	public void setMaxFailSize(final int maxSize) {
		this.setMaxFails(maxSize);
		maybeResize();
	}
	public void setMaxSize(final int maxSize) {
		this.maxItems = maxSize;
		maybeResize();
	}

	private void maybeResize() {
		while (size() > getMaxSize()) {
			removeFirst();
			offset++;
		}

		index = size();
	}

	public int getMaxSize() {
		return maxItems;
	}

	public boolean isIgnoreDuplicates() {
		return true;
	}

	public void setIgnoreDuplicates(final boolean flag) {
		// NULLOP ; this is the default
	}

	public boolean isAutoTrim() {
		return false;
	}

	public void setAutoTrim(final boolean flag) {
		// NULLOP this is false
	}

	private class EntriesIterator implements ListIterator<Entry> {
		private final ListIterator<CharSequence> source;
		private LinkedList<CharSequence> ll;

		private EntriesIterator(final int index) {
			ListIterator<HistoryItem> it = history.listIterator(index);
			if (ll == null) {
				ll = new LinkedList<CharSequence>();
				while (it.hasNext()) {
					ll.add(it.next().getData());
				}
			}
			source = ll.listIterator();
		}

		public Entry next() {
			if (!source.hasNext()) {
				throw new NoSuchElementException();
			}
			return new EntryImpl(offset + source.nextIndex(), source.next());
		}

		public Entry previous() {
			if (!source.hasPrevious()) {
				throw new NoSuchElementException();
			}
			return new EntryImpl(offset + source.previousIndex(), source.previous());
		}

		public int nextIndex() {
			return offset + source.nextIndex();
		}

		public int previousIndex() {
			return offset + source.previousIndex();
		}

		public boolean hasNext() {
			return source.hasNext();
		}

		public boolean hasPrevious() {
			return source.hasPrevious();
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public void set(final Entry entry) {
			throw new UnsupportedOperationException();
		}

		public void add(final Entry entry) {
			throw new UnsupportedOperationException();
		}
	}

	private static class EntryImpl implements Entry {
		private final int index;

		private final CharSequence value;

		public EntryImpl(int index, CharSequence value) {
			this.index = index;
			this.value = value;
		}

		public int index() {
			return index;
		}

		public CharSequence value() {
			return value;
		}

		@Override
		public String toString() {
			return String.format("%d: %s", index, value);
		}
	}
	public void save() {
		HistoryParser parser=new HistoryParser();
		try {
			String base=getBase();
			if (base==null) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,"HIST-013 APPDATA is null");
			}
			if (base!=null&&!(new File(base).exists())) {
				if (!(new File(base).mkdirs())) {
					throw new Exception ("HIST-009 "+base+" Directory did not create");
				}
			}
			if (base!=null&new File(base+CONFIGFILE).exists()) {
				if  (new File(base+CONFIGBACK).exists()) {
					if (!new File(base+CONFIGBACK).delete()) {
						throw new Exception ("HIST-011 "+base+CONFIGBACK+" could not be deleted ");
					}
				}
				if(!new File(base+CONFIGFILE).renameTo /*should use move*/
				(new File(base+CONFIGBACK))) {
					throw new Exception ("HIST-010 "+base+CONFIGFILE+" could not be renamed to "+base+CONFIGBACK);
				}
			}
			if (base!=null) {
				parser.putXML(new File(base+CONFIGFILE), history);
			}

		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,e.getLocalizedMessage());
		}
		return;
	}
	public void load() {
		HistoryParser parser=new HistoryParser();
		try {
			String base=getBase();
			if (base==null) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,"HIST-013 APPDATA is null");
			}
			if (base!=null&&new File(base+CONFIGFILE).canRead()) {
				URL u = new File(base+CONFIGFILE).toURI().toURL();
				if (new File(base+CONFIGFILE).exists()) {
					ArrayList<HistoryItem> filealiases = parser.processXML(u);
					Iterator<HistoryItem> it = filealiases.iterator();
					while (it.hasNext()) {
						HistoryItem a = it.next();
						if (getTimingString(a.getTiming()).length()>maxTimeLength) {
							maxTimeLength=getTimingString(a.getTiming()).length();
						}
						if (a.getTimesUsed()>maxTimesUsed) {
							maxTimesUsed = a.getTimesUsed();
						}
						if (!(historyContains(a.getData()))) {
							history.add(a);
						}
					}
					
				} else {
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,"HIST-012 "+base+CONFIGFILE+" could not be read.");
				}
			}
			moveToEnd();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,e.getLocalizedMessage());
		} 
		return;
	}

	private boolean historyContains(String data) {
		for (HistoryItem hi: history) {
			if (hi.getData().contains(data)) {
				return true;
			}
		}
		return false;
	}
	@Override
	public void saveCommand(ISQLCommand cmd) {
		if (!(cmd.getStmtSubType().equals(StmtSubType.G_S_RUN)||cmd.getStmtSubType().equals(StmtSubType.G_S_SLASH)
				||cmd.getStmtSubType().equals(StmtSubType.G_S_DEL_PLUS)||cmd.getStmtSubType().equals(StmtSubType.G_S_CHANGE)||cmd.getStmtSubType().equals(StmtSubType.G_S_HELP)||
				cmd.getStmtSubType().equals(StmtSubType.G_S_SAVE)||cmd.getStmtSubType().equals(StmtSubType.G_S_STORE)
				||cmd.getStmtSubType().equals(StmtSubType.G_S_GET)||cmd.getStmtSubType().equals(StmtSubType.G_S_LIST)
				||cmd.getStmtSubType().equals(StmtSubType.G_S_APPEND)||cmd.getStmtSubType().equals(StmtSubType.G_S_EDIT)
				||cmd.getStmtSubType().equals(StmtSubType.G_S_FORMAT)
				||cmd.getStmtSubType().equals(StmtSubType.G_S_COMMENT_PLUS)||cmd.getStmtSubType().equals(StmtSubType.G_S_HISTORY)
				||cmd.getStmtSubType().equals(StmtSubType.G_S_DOC_PLUS))||cmd.getSQLOrig().trim().toLowerCase().startsWith("repeat")) {
			/*
			 * Right now, I'm adding all commands to the history.  This might change depending on requirements, but for now
			 * lets let this ride.
			 */
			if (cmd.getdepth()==0)
			MultiLineHistory.getInstance().addItem(cmd.getSQLOrigWithTerminator(), cmd.getTiming(), cmd.isFail());
		}
	}
	
	public static String getTimingString(long durationMilliSecs) {
		String x = "";
		long hrs = ((durationMilliSecs / (1000 * 60 * 60)) % 24);
		if (hrs > 0) {
			durationMilliSecs = durationMilliSecs - (hrs * 1000 * 60 * 60);
			x+=String.format("%02d", hrs)+":";
		}
		long mins = ((durationMilliSecs / (1000 * 60)) % 60);
		if (mins > 0) {
			durationMilliSecs = durationMilliSecs - (mins * 1000 * 60);
			x+=String.format("%02d", mins)+":";
		}
		long secs = (durationMilliSecs / 1000) % 60;
		//Always show seconds
		durationMilliSecs = durationMilliSecs - (secs * 1000);
		x+=String.format("%02d", secs)+".";
		if (durationMilliSecs>0) {
			x+=String.format("%03d", durationMilliSecs);
		}
		return x;
	}
	/**
	 * @return the noHistory
	 */
	public final String getNoHistory() {
		return noHistory;
	}
	/**
	 * @param noHistory the noHistory to set
	 */
	public final void setBlackList(String blacklist) {
		this.noHistory = blacklist;
	}
	@Override
	public void allowFailures(boolean value) {
		nofails = value;
	}
		
	public boolean showFailures() {
		return nofails;
	}
	public int getMaxFails() {
		return maxFails;
	}
	public void setMaxFails(int maxFails) {
		this.maxFails = maxFails;
	}
	
	
	}

