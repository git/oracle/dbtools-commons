/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.console;

import java.awt.event.ActionListener;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import jline.console.UserInterruptException;
import oracle.dbtools.raptor.console.clone.DbtoolsConsoleReader;
import oracle.dbtools.raptor.console.clone.DbtoolsNonBlockingInputStream;
import oracle.dbtools.raptor.newscriptrunner.console.IConsoleClearScreen;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SQLPlusconsoleReader.java"
 *         >Barry McGillin</a>
 *
 */
public class SQLPlusConsoleReader extends DbtoolsConsoleReader implements IConsoleClearScreen {

	public static final String CTRLC = "ctrlc";

	public SQLPlusConsoleReader() throws IOException {
		super("SQLPlus", new FileInputStream(FileDescriptor.in), System.out, null);
		// We need to handle this
		setHandleUserInterrupt(true);
	}

	@Override
	public void doClearScreen(String position) throws IOException {
		super.clearScreen(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jline.console.ConsoleReader#readLine()
	 */
	@Override
	public String readLine() throws IOException {
		try {
			return super.readLine();
		} catch (UserInterruptException e) {
			fireListeners(CTRLC);
		}
		return null;
	}

	private ArrayList<ConsoleListener> m_listeners;

	/**
	 * Fire the events at the listeners.
	 * 
	 * @param event
	 *            The event to raise.
	 */
	public void fireListeners(final String event) {
		for (ConsoleListener listener : getListeners()) {
			listener.raiseEvent(event);
		}
	}

	/**
	 * @return Returns the listeners.
	 */
	protected ArrayList<ConsoleListener> getListeners() {
		if (m_listeners == null) {
			m_listeners = new ArrayList<ConsoleListener>();
		}
		return m_listeners;
	}

	/**
	 * Add a listener to the audience.
	 * 
	 * @param listener
	 *            The listener to add.
	 */
	public void addListener(final ConsoleListener listener) {
		getListeners().add(listener);
	}

	@Override
	public void addTriggeredAction(char c, ActionListener al) {
		super.addTriggeredAction(c, al);

	}

	public void removeListener(ConsoleListener task) {
		getListeners().remove(task);
	}

	@Override
	public void setRedit(boolean on) {
		editNow = on;
		super.fullRedraw = true;
		super.historyReturned = false;

	}

	@Override
	public void pauseReader(boolean pause) {
		((DbtoolsNonBlockingInputStream) getInput()).pause(pause);
	}

	public void resetRedraw(String line) {
		if (line != null && (line.toLowerCase().startsWith("edit")||partofinsert(line))) {
			fullRedraw = true;
		} else {
			fullRedraw = false;
		}

	}

	private boolean partofinsert(String line) {
		switch (line.toLowerCase().trim()) {
		case ("i"):
		case ("in"):
		case ("inp"):
		case ("inpu"):
		case ("input"):
			return true;
		}
		return false;
	}

	@Override
	public int getWidth() {
		return getTerminal().getWidth();
	}

	@Override
	public int getHeight() {
		return getTerminal().getHeight();
	}

	@Override
	public void forceRedraw(boolean draw) {
		int cursorPos = getMultilineBuffer().getLine(getMultilineBuffer().getCurrentLine()).length();
		setRedraw(getMultilineBuffer().getCurrentLine(), MoveState.UP, cursorPos, 0, CursorState.NORMAL, true);
		redrawDetails.setDontReDraw(false);
		fullRedraw = true;
	}
}
