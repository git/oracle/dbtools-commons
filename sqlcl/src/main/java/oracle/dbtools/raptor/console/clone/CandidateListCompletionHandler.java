/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.console.clone;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

import jline.console.ConsoleReader;
import jline.console.CursorBuffer;
import oracle.dbtools.app.SqlclCompleter;
import oracle.dbtools.raptor.console.clone.DbtoolsConsoleReader.CursorState;
import oracle.dbtools.raptor.console.clone.DbtoolsConsoleReader.MoveState;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=CandidateListCompletionHandler.java"
 *         >Barry McGillin</a>
 *
 */
public class CandidateListCompletionHandler implements CompletionHandler {

	/*
	 * Copyright (c) 2002-2012, the original author or authors.
	 * 
	 * This software is distributable under the BSD license. See the terms of
	 * the BSD license in the documentation provided with this software.
	 * 
	 * http://www.opensource.org/licenses/bsd-license.php
	 */

	/**
	 * A {@link CompletionHandler} that deals with multiple distinct completions
	 * by outputting the complete list of possibilities to the console. This
	 * mimics the behavior of the <a
	 * href="http://www.gnu.org/directory/readline.html">readline</a> library.
	 */
	// TODO: handle quotes and escaped quotes && enable automatic escaping of
	// whitespace
	public boolean complete(final DbtoolsConsoleReader reader, final List<CharSequence> candidates, final int pos) throws IOException {
		CursorBuffer buf = reader.getCursorBuffer();

		// if there is only one completion, then fill in the buffer
		if (candidates.size() == 1) {
			CharSequence value = candidates.get(0);

			// fail if the only candidate is the same as the current buffer
			if (value.equals(buf.toString())) {
				return false;
			}
			setBuffer(reader, value, pos);
			reader.setRedraw(reader.getMultilineBuffer().getCurrentLine(), MoveState.SAME, reader.getCursorBuffer().cursor, 0, CursorState.NORMAL, false);

			return true;
		} else if (candidates.size() > 1) {
			String value = getUnambiguousCompletions(candidates);
			setBuffer(reader, value, pos);
		}
		printCandidates(reader, candidates);
		return true;
	}

	public static void setBuffer(final DbtoolsConsoleReader reader, final CharSequence value, final int offset) throws IOException {
		while ((reader.getCursorBuffer().cursor > offset) && reader.backspace()) {
			// empty
		}

		reader.putString(value);
		reader.setCursorPosition(offset + value.length());
		reader.getMultilineBuffer().replace(reader.getMultilineBuffer().getCurrentLine(), reader.getCursorBuffer().toString());
	}

	/**
	 * Print out the candidates. If the size of the candidates is greater than
	 * the {@link ConsoleReader#getAutoprintThreshold}, they prompt with a
	 * warning.
	 *
	 * @param candidates
	 *            the list of candidates to print
	 */
	public static void printCandidates(final DbtoolsConsoleReader reader, Collection<CharSequence> candidates) throws IOException {
		Set<CharSequence> distinct = new HashSet<CharSequence>(candidates);

		// copy the values and make them distinct, without otherwise affecting
		// the ordering. Only do it if the sizes differ.
		if (distinct.size() != candidates.size()) {
			Collection<CharSequence> copy = new ArrayList<CharSequence>();

			for (CharSequence next : candidates) {
				if (!copy.contains(next)) {
					copy.add(next);
				}
			}

			candidates = copy;
		}

		reader.printColumns(candidates);
	}

	/**
	 * Returns a root that matches all the {@link String} elements of the
	 * specified {@link List}, or null if there are no commonalities. For
	 * example, if the list contains <i>foobar</i>, <i>foobaz</i>,
	 * <i>foobuz</i>, the method will return <i>foob</i>.
	 */
	private String getUnambiguousCompletions(final List<CharSequence> candidates) {
		if (candidates == null || candidates.isEmpty()) {
			return null;
		}

		// convert to an array for speed
		String[] strings = candidates.toArray(new String[candidates.size()]);
		for( int i = 0; i < strings.length; i++ ) {
            strings[i] = SqlclCompleter.discolor(strings[i]);
        }

		String first = strings[0];
		StringBuilder candidate = new StringBuilder();

		for (int i = 0; i < first.length(); i++) {
			if (startsWith(first.substring(0, i + 1), strings)) {
				candidate.append(first.charAt(i));
			} else {
				break;
			}
		}

		return candidate.toString();
	}

	/**
	 * @return true is all the elements of <i>candidates</i> start with
	 *         <i>starts</i>
	 */
	private boolean startsWith(final String starts, final String[] candidates) {
		for (String candidate : candidates) {
			if (!candidate.startsWith(starts)) {
				return false;
			}
		}

		return true;
	}

	/*@SuppressWarnings("unused")
	private static enum Messages {
		DISPLAY_CANDIDATES, DISPLAY_CANDIDATES_YES, DISPLAY_CANDIDATES_NO, ;

		private static final ResourceBundle bundle = ResourceBundle.getBundle(CandidateListCompletionHandler.class.getName(), Locale.getDefault());

		public String format(final Object... args) {
			if (bundle == null)
				return "";
			else
				return String.format(bundle.getString(name()), args);
		}
	}*/
}
