/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.console.clone;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Iterator;

import org.fusesource.jansi.Ansi;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.scriptrunner.cmdline.editor.Buffer;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=ConsoleBufferPainter.java"
 *         >Barry McGillin</a>
 *
 */
public class ConsoleBufferPainter {

	private static Buffer _buffer;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Buffer buffer = new Buffer();
		buffer.add("select");
		buffer.add("1 from");
		buffer.add("from dual");
		buffer.setCurrentLine(2);
		ConsoleBufferPainter.setBuffer(buffer);
		ConsoleBufferPainter.printBuffer(System.out);

	}

	public static void setBuffer(Buffer buffer) {
		_buffer = buffer;
	}

	public static void printBuffer(PrintStream out) {
		Ansi cursorposition = Ansi.ansi().saveCursorPosition();
		Iterator<String> it = _buffer.getBufferList().iterator();
		while (it.hasNext()) {
			out.append(Ansi.ansi().a(it.next()).toString()+"\n"); out.flush();
		}
		out.append(Ansi.ansi().restorCursorPosition().toString()); out.flush();
	}

	public static int getTerminalWidth() {
		// for non-windows get the columns/lines size from the terminal
		if (!System.getProperty("os.name").toLowerCase().contains("win")) { //$NON-NLS-1$ //$NON-NLS-2$
			try {
				Process p = Runtime.getRuntime().exec(new String[] { "/bin/bash", "-c", "tput cols 2> /dev/tty" }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				byte[] buf = new byte[200];
				p.getInputStream().read(buf);
				String val = new String(buf).trim();
				return Integer.parseInt(val);

			} catch (Exception e) {
				return 0;// something went wrong no col size detected
			}
		}
		return -1;
	}

	public static int getTerminalHeight() {
		// for non-windows get the columns/lines size from the terminal
		if (!System.getProperty("os.name").toLowerCase().contains("win")) { //$NON-NLS-1$ //$NON-NLS-2$

			try {
				Process p = Runtime.getRuntime().exec(new String[] { "/bin/bash", "-c", "tput lines 2> /dev/tty" }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				byte[] buf = new byte[200];
				p.getInputStream().read(buf);
				String val = new String(buf).trim();
				return Integer.parseInt(val);
			} catch (Exception e) {
				return 0; // something went wrong no lines size detected
			}
		}
		return -1;
	}
}
