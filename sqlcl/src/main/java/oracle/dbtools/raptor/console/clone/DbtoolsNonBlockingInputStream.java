/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.console.clone;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import jline.internal.Log;

/**
 * Simpler version of JLines NonBlockingInputStream.
 * All reading from the console takes place on a thread which populates a member ArrayList variable
 * All DbtoolsNonBlockingInputStream.read  references the ArrayList variable. 
 * You can wait some time if you want to see if the ArrayList is populated with something.
 * There is only one instance of this Object.
 * 
 * The thread that is run by this class is the only thread reading the jline.
 * @author dermot.oneill@oracle.com
 */
public class DbtoolsNonBlockingInputStream extends InputStream implements Runnable {
	private InputStream _in = null;
	private ArrayList<Integer> _inputBuffer = new ArrayList<Integer>();
	private boolean _shutdown = false;
	private static DbtoolsNonBlockingInputStream _instance = null;
	private boolean _nonBlockingEnabled = true;
	private boolean _pause = false;
	private KeyMap _keymap = null;
	private boolean _waiting = true;

	public static DbtoolsNonBlockingInputStream getInstance(InputStream in, boolean nonBlockingEnabled) {
		if (_instance == null) {
			_instance = new DbtoolsNonBlockingInputStream(in, nonBlockingEnabled);
		}
		return _instance;
	}

	private DbtoolsNonBlockingInputStream(InputStream in, boolean nonBlockingEnabled) {
		_in = in;
		_nonBlockingEnabled = nonBlockingEnabled;
		if (_nonBlockingEnabled) {
			Thread t = new Thread(this);
			t.setName("DBtoolsNonBlockingInputStreamThread");
			t.setDaemon(true);
			t.start();
		}
	}

	@Override
	public void run() {
		Log.debug("DBtoolsNonBlockingInputStream start");
		int charRead = -2;
		while (!_shutdown && charRead !=-1) {
			if(isNewLine(charRead)){ 
				pauseTillNextRead(); //for each new line, wait until a read is required.
			}
			charRead = -2;
			try {
				charRead = _in.read();
				addToBuffer(charRead);
			} catch (IOException e) {
			}
		}
		Log.debug("DbtoolsNonBlockingInputStream shutdown");
	}

	private boolean isNewLine(int charRead) {
		try {
			if (_keymap != null) {
				try {
					StringBuilder sb = new StringBuilder();
					sb.appendCodePoint(charRead);
					Object o = _keymap.getBound(sb);
					if (o instanceof Operation) {
						if (((Operation) o) == Operation.ACCEPT_LINE) {
							return true;
						}
					}
				} catch (Exception e) {
					if (charRead == 13) {
						return true;
					}
				}
			} else {
				if (charRead == 13) {
					return true;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	private synchronized void pauseTillNextRead() {
			try {
				synchronized (this) {
					_waiting = true;
					wait(0); //wait till notified by a reader... to get next line or too fullpeek
					_waiting = false;
				}
			} catch (InterruptedException e) {
			}
	}

	@Override
	public int read() throws IOException {
		return read(0L, false);
	}

	public int read(long timeout) throws IOException {
		return read(timeout, false);
	}

	private synchronized int read(long timeout, boolean isPeek) throws IOException {
		if (!_nonBlockingEnabled) {
			//read the char yourself..
			return _in.read();
		}
			
		if(_waiting && getBufferSize() == 0 ){
			if(isPeek){
				return -2;//do not allow a peek onto the next line. As peeking onto the next line has ramifications for such commands as HOST
			} else {
				notify();//get the thread to stop waiting and read the next line
			}
		}
		if (getBufferSize() == 0) {
			try {
				if (timeout == 0L) {
					while (getBufferSize() == 0) {
						wait(10);
					}
				} else {
					wait(timeout);
				}
			} catch (InterruptedException e) {
				/* IGNORED */
			}
		}

		if (getBufferSize() == 0) { // doesnt have anything still
			return -2;
		} else {
			int chr = getFirstCharFromBuffer();
			if ((!isPeek)&&(chr!=-1)) {//EOF is forever
				removeFirstCharFromBuffer();
			}
			return chr;
		}
	}

	private void addToBuffer(int charRead) {
		synchronized (_inputBuffer) {
			if (!((charRead==-1)&&
					!(_inputBuffer.isEmpty())
					&&(_inputBuffer.get(_inputBuffer.size()-1)==-1))){
				//do not store duplicate -1 (i.e. end of buffer) => avoid memory overflow.
			 _inputBuffer.add(charRead);
			}
		}
	}

	private void removeFirstCharFromBuffer() {
		synchronized (_inputBuffer) {
			_inputBuffer.remove(0);
		}
	}

	private int getFirstCharFromBuffer() {
		synchronized (_inputBuffer) {
			return _inputBuffer.get(0);
		}
	}

	private int getBufferSize() {
		synchronized (_inputBuffer) {
			return _inputBuffer.size();
		}
	}

	/**
	 * This version of read() is very specific to jline's purposes, it 
	 * will always always return a single byte at a time, rather than filling
	 * the entire buffer. 
	 */
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (b == null) {
			throw new NullPointerException();
		} else if (off < 0 || len < 0 || len > b.length - off) {
			throw new IndexOutOfBoundsException();
		} else if (len == 0) {
			return 0;
		}

		int c;
		c = this.read(0L);

		if (c == -1) {
			return -1;
		}
		b[off] = (byte) c;
		return 1;
	}

	public int peek(long timeout) throws IOException {
		return read(timeout, true);
	}

	/**
	 * peek only allows you to peek at only the first character.
	 * This fullPeek returns evertyhing we have in out buffer
	 * Very handy to see if anyone has typed ^C to exit, as they may type this after messing around with other keys
	 * @return
	 * @throws IOException
	 */
	public  synchronized ArrayList<Integer> fullPeek() throws IOException {
			//with full peek you can peek onto the next line.
		  //full peek wont interfere with things, like the HOST command, as the HOST command sets _pause to true 
	    //wait for a little while before performing a fullpeek. This allows any command like HOST to set _pause to true
			try {
				wait(250);
			} catch (InterruptedException e) {
			}
			ArrayList<Integer> inputBufferClone = null;
		
				if(_waiting &&  !_pause  && getBufferSize() == 0 ){
					notify();
				}
				inputBufferClone = (ArrayList<Integer>) _inputBuffer.clone();
			return inputBufferClone;
	}
	
	
	/**
	 * read only allows you to read  the first character.
	 * readFull returns all the characters in the buffer as a String.
	 * Perform a full peek, wait 1/4 second then perform another full peek. 
	 * If the peeks are the same then  the buffer is "stable" so return the result
	 * @param waitTillStable true/false should you wait until the input buffer is stable from more than 1/4 second before returning the result
	 * @return
	 * @throws IOException
	 */
	public String fullRead(boolean waitTillStable) throws IOException {
		if (!waitTillStable) {
			return fullRead();
		} else {
			boolean stable = false;
			while (!stable) {
				//peek now
				ArrayList<Integer> peek1 = fullPeek();
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
				}
				ArrayList<Integer> peek2 = fullPeek();
				//if both peeks are the same then the input buffer is stable
				if (peek1.equals(peek2)) {
					stable = true;
				}		
			}
			return fullRead();
		}
	}
	/**
	 * read only allows you to read  the first character.
	 * readFull returns all the characters in the buffer as a String.
	 * You should be certain that writing to the buffer is "complete". 
	 * Otherwise this returns whats in the buffer at this moment, but there maybe more to be written too it.
	 * 
	 * @return
	 * @throws IOException
	 */
	private synchronized String fullRead() throws IOException {
		StringBuffer sb = new StringBuffer();
		boolean addMinusOne = false;
		for (Integer currentInt : _inputBuffer) {
			if(currentInt != -1){
				sb.appendCodePoint(currentInt);
			} else {
				addMinusOne = true;
			}
		}
		resetBuffer();
	    if(addMinusOne){
	    	_inputBuffer.add(-1);
	    }
		return sb.toString();
	}

	
	public boolean isNonBlockingEnabled() {
		return _nonBlockingEnabled;
	}

	@Override
	public void close() throws IOException {
		/*
		 * The underlying input stream is closed first. This means that if the
		 * I/O thread was blocked waiting on input, it will be woken for us.
		 */
		_in.close();
		shutdown();
	}

	public synchronized void shutdown() {
		_shutdown = true;
	}

	public void resetBuffer() {
		synchronized (_inputBuffer) {
			_inputBuffer.clear();
		}
	}

	public void pause(boolean pause) {
		_pause = pause;
	}
	
	public void setKeyMap(KeyMap keymap){
		_keymap = keymap;
	}
}
