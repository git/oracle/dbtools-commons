/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.proformatter;

import java.io.IOException;
import java.net.URL;

import oracle.dbtools.app.Format;
import oracle.dbtools.raptor.newscriptrunner.commands.HiddenParameters;


public class SQLPlusFormatter implements ICoreFormatter {

	/*
	 * String retVal = ""; //$NON-NLS-1$ try { InputStream in = null;
	 * InputStream nobuf = new FileInputStream(new File(fname)); if (nobuf ==
	 * null) { throw new NullPointerException(); } in = new
	 * BufferedInputStream(nobuf); SAXParserFactory _factory; if (_factory ==
	 * null) { _factory = SAXParserFactory.newInstance();
	 * _factory.setValidating(false); } SAXHandler getoutput = new
	 * SAXHandler(hm, cb, tag,enclosedBy); DefaultHandler handler = getoutput;
	 * InputSource is = new InputSource(in); is.setEncoding("UTF-8");
	 * //$NON-NLS-1$ _factory.newSAXParser().parse(is, handler);
	 * 
	 * if (!hm.isEmpty()) { Set s=hm.keySet(); Object[] o=s.toArray(); for
	 * (Object m:o) { System.out.println("Not set:"+((Method)m).getName()); } }
	 * } catch (IOException e) { throw new OerrException(e); } catch
	 * (SAXException e) { throw new OerrException(e); } catch
	 * (ParserConfigurationException e) { throw new OerrException(e); } return
	 * retVal; }
	 * 
	 * private class SAXHandler extends DefaultHandler {
	 * 
	 * //look for key 1:SQL //on value store all elements until end of value
	 * public boolean match = false;
	 * 
	 * public StringBuffer lookfor = new StringBuffer();
	 * 
	 * public int ami = 0;
	 * 
	 * public SAXHandler(String getme) { super(); ami = new
	 * Integer(getme).intValue(); }
	 * 
	 * public void startElement(String uri, String localName, String qName,
	 * Attributes attributes) throws SAXException {
	 * 
	 * if ("payload".equals(localName)) { //$NON-NLS-1$ if (ami == new
	 * Integer(attributes.getValue("", "number")) //$NON-NLS-1$ //$NON-NLS-2$
	 * .intValue()) { match = true; lookfor = new StringBuffer(); } } }
	 * 
	 * public void characters(char[] chars, int start, int length) { if (match
	 * == true) { lookfor.append(chars, start, length); } }
	 * 
	 * @Override public void endElement(String uri, String localName, String
	 * qName) throws SAXException { match = false; } }
	 */
	// if more flexability required (multiple formatters? some options not
	// there?) factory could be used - best to default the gets/sets to
	// something
	// load a default and put the setting on top?
	public String format(ICodingStyleSQLOptions co, String start) {
	    try {
	        return new Format().format(start);
	    } catch ( IOException e ) {
	        e.printStackTrace();
	    }
	    return start;
	}


}
