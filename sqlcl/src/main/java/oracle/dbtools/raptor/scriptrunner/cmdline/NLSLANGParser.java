/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline;

import java.text.MessageFormat;

/**
 * Parse and validate NLS_LANG variable attributes and return validated
 * constituents
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com?subject=NLSLANGParser.java">Barry
 *         McGillin</a>
 */
public class NLSLANGParser {
	private static boolean debug = false;
	private static String lang = "";
	private static String country = "";
	private static String encoding = "";
	private static String nls_lang;

	public static enum Languages {
		AMERICAN, // us
		ARABIC, // ar
		BENGALI, // bn
		BRAZILIAN_PORTUGUESE, // ptb
		BULGARIAN, // bg
		CANADIAN_FRENCH, // frc
		CATALAN, // ca
		CROATIAN, // hr
		CZECH, // cs
		DANISH, // dk
		DUTCH, // nl
		EGYPTIAN, // eg
		ENGLISH, // gb
		ESTONIAN, // et
		FINNISH, // sf
		FRENCH, // f
		GERMAN, // d
		GERMAN_DIN, // din
		GREEK, // el
		HEBREW, // iw
		HINDI, // hi
		HUNGARIAN, // hu
		ICELANDIC, // is
		INDONESIAN, // in
		ITALIAN, // i
		JAPANESE, // ja
		KOREAN, // ko
		LATIN_AMERICAN_SPANISH, // esa
		LATVIAN, // lv
		LITHUANIAN, // lt
		MALAY, // ms
		MEXICAN_SPANISH, // esm
		NORWEGIAN, // N
		POLISH, // PL
		PORTUGUESE, // pt
		ROMANIAN, // ro
		RUSSIAN, // ru
		SIMPLIFIED_CHINESE, // ZHS
		SLOVAK, // SK
		SLOVENIAN, // sl
		SPANISH, // E
		SWEDISH, // S
		TAMIL, // ta
		THAI, // th
		TRADITIONAL_CHINESE, // ZHT
		TURKISH, // TR
		UKRAINIAN, // uk
		VIETNAMESE, // vn
	}

	public static enum LangAbbreviation {
		AR, // ARABIC
		BG, // BULGARIAN
		BN, // BENGALI
		CA, // CATALAN,
		CS, // CZECH
		D, // GERMAN
		DIN, // GERMAN_DIN
		DK, // DANISH
		E, // SPANISH
		EG, // EGYPTIAN,
		EL, // GREEK
		ESA, // LATIN_AMERICAN_SPANISH
		ESM, // MEXICAN_SPANISH
		ET, // ESTONIAN,
		F, // FRENCH
		FRC, // CANADIAN_FRENCH
		GB, // ENGLISH,
		HI, // HINDI
		HR, // CROATIAN,
		HU, // HUNGARIAN
		I, // ITALIAN
		IN, // INDONESIAN
		IS, // ICELANDIC
		IW, // HEBREW
		JA, // JAPANESE
		KO, // KOREAN
		LT, // LITHUANIAN
		LV, // LATVIAN
		MS, // MALAY
		N, // NORWEGIAN
		NL, // DUTCH
		PL, // POLISH
		PT, // PTPORTUGUESE
		PTB, // BRAZILIAN_PORTUGUESE
		RO, // ROMANIAN
		RU, // RUSSIAN
		S, // SWEDISH
		SF, // FINNISH,
		SK, // SLOVAK
		SL, // SLOVENIAN
		TA, // TAMIL
		TH, // THAI
		TR, // TURKISH
		uk, // UKRANIAN
		UK, // UKRAINIAN
		us, // AMERICAN
		US, // AMERICAN
		vm, // VIETNAMESE
		VN, // VIETNAMESE
		ZHS, // SIMPLIFIED_CHINESE
		ZHT, // TRADITIONAL_CHINESE
	}

	public static enum Territories {
		ALGERIA, //
		ICELAND, //
		QATAR, //
		AMERICA, //
		INDIA, //
		ROMANIA, //
		AUSTRALIA, //
		INDONESIA, //
		SAUDI_ARABIA, //
		AUSTRIA, //
		IRAQ, //
		SINGAPORE, //
		BAHRAIN, //
		IRELAND, //
		SLOVAKIA, //
		BANGLADESH, //
		ISRAEL, //
		SLOVENIA, //
		BELGIUM, //
		ITALY, //
		SOMALIA, //
		BRAZIL, //
		JAPAN, //
		SOUTH_AFRICA, //
		BULGARIA, //
		JORDAN, //
		SPAIN, //
		CANADA, //
		KAZAKHSTAN, //
		SUDAN, //
		CATALONIA, //
		KOREA, //
		SWEDEN, //
		CHINA, //
		KUWAIT, //
		SWITZERLAND, //
		CIS, //
		LATVIA, //
		SYRIA, //
		CROATIA, //
		LEBANON, //
		TAIWAN, //
		CYPRUS, //
		LIBYA, //
		THAILAND, //
		CZECH_REPUBLIC, //
		LITHUANIA, //
		THE_NETHERLANDS, //
		DENMARK, //
		LUXEMBOURG, //
		TUNISIA, //
		DJIBOUTI, //
		MALAYSIA, //
		TURKEY, //
		EGYPT, //
		MAURITANIA, //
		UKRAINE, //
		ESTONIA, //
		MEXICO, //
		UNITED_ARAB_EMIRATES, //
		FINLAND, //
		MOROCCO, //
		UNITED_KINGDOM, //
		FRANCE, //
		NEW_ZEALAND, //
		UZBEKISTAN, //
		GERMANY, //
		NORWAY, //
		VIETNAM, //
		GREECE, //
		OMAN, //
		YEMEN, //
		HONG_KONG, //
		POLAND, //
		HUNGARY, //
		PORTUGAL //
	}

	public static enum EURO_CHARSET {
		US7ASCII, // ASCII 7-bit American SB, ASCII
		SF7ASCII, // ASCII 7-bit Finnish SB
		YUG7ASCII, // ASCII 7-bit Yugoslavian SB
		RU8BESTA, // BESTA 8-bit Latin/Cyrillic SB, ASCII
		EL8GCOS7, // Bull EBCDIC GCOS7 8-bit Greek SB
		WE8GCOS7, // Bull EBCDIC GCOS7 8-bit West European SB
		EL8DEC, // DEC 8-bit Latin/Greek SB
		TR7DEC, // DEC VT100 7-bit Turkish SB
		TR8DEC, // DEC 8-bit Turkish SB, ASCII
		TR8EBCDIC1026, // EBCDIC Code Page 1026 8-bit Turkish SB
		TR8EBCDIC1026S, // EBCDIC Code Page 1026 Server 8-bit Turkish SB
		TR8PC857, // IBM-PC Code Page 857 8-bit Turkish SB, ASCII
		TR8MACTURKISH, // MAC Client 8-bit Turkish SB
		TR8MACTURKISHS, // MAC Server 8-bit Turkish SB, ASCII
		TR8MSWIN1254, // MS Windows Code Page 1254 8-bit Turkish SB, ASCII, EURO
		WE8BS2000L5, // Siemens EBCDIC.DF.L5 8-bit West European/Turkish SB
		WE8DEC, // DEC 8-bit West European SB, ASCII
		D7DEC, // DEC VT100 7-bit German SB
		F7DEC, // DEC VT100 7-bit French SB
		S7DEC, // DEC VT100 7-bit Swedish SB
		E7DEC, // DEC VT100 7-bit Spanish SB
		NDK7DEC, // DEC VT100 7-bit Norwegian/Danish SB
		I7DEC, // DEC VT100 7-bit Italian SB
		NL7DEC, // DEC VT100 7-bit Dutch SB
		CH7DEC, // DEC VT100 7-bit Swiss (German/French) SB
		SF7DEC, // DEC VT100 7-bit Finnish SB
		WE8DG, // DG 8-bit West European SB, ASCII
		WE8EBCDIC37C, // EBCDIC Code Page 37 8-bit Oracle/c SB
		WE8EBCDIC37, // EBCDIC Code Page 37 8-bit West European SB
		D8EBCDIC273, // EBCDIC Code Page 273/1 8-bit Austrian German SB
		DK8EBCDIC277, // EBCDIC Code Page 277/1 8-bit Danish SB
		S8EBCDIC278, // EBCDIC Code Page 278/1 8-bit Swedish SB
		I8EBCDIC280, // EBCDIC Code Page 280/1 8-bit Italian SB
		WE8EBCDIC284, // EBCDIC Code Page 284 8-bit Latin American/Spanish SB
		WE8EBCDIC285, // EBCDIC Code Page 285 8-bit West European SB
		WE8EBCDIC1047, // EBCDIC Code Page 1047 8-bit West European SB
		WE8EBCDIC1140, // EBCDIC Code Page 1140 8-bit West European SB, EURO
		WE8EBCDIC1140C, // EBCDIC Code Page 1140 Client 8-bit West European SB,
						// EURO
		WE8EBCDIC1145, // EBCDIC Code Page 1145 8-bit West European SB, EURO
		WE8EBCDIC1146, // EBCDIC Code Page 1146 8-bit West European SB, EURO
		WE8EBCDIC1148, // EBCDIC Code Page 1148 8-bit West European SB, EURO
		WE8EBCDIC1148C, // EBCDIC Code Page 1148 Client 8-bit West European SB,
						// EURO
		F8EBCDIC297, // EBCDIC Code Page 297 8-bit French SB
		WE8EBCDIC500C, // EBCDIC Code Page 500 8-bit Oracle/c SB
		WE8EBCDIC500, // EBCDIC Code Page 500 8-bit West European SB
		EE8EBCDIC870, // EBCDIC Code Page 870 8-bit East European SB
		EE8EBCDIC870C, // EBCDIC Code Page 870 Client 8-bit East European SB
		EE8EBCDIC870S, // EBCDIC Code Page 870 Server 8-bit East European SB
		WE8EBCDIC871, // EBCDIC Code Page 871 8-bit Icelandic SB
		EL8EBCDIC875, // EBCDIC Code Page 875 8-bit Greek SB
		EL8EBCDIC875S, // EBCDIC Code Page 875 Server 8-bit Greek SB
		CL8EBCDIC1025, // EBCDIC Code Page 1025 8-bit Cyrillic SB
		CL8EBCDIC1025C, // EBCDIC Code Page 1025 Client 8-bit Cyrillic SB
		CL8EBCDIC1025S, // EBCDIC Code Page 1025 Server 8-bit Cyrillic SB
		CL8EBCDIC1025X, // EBCDIC Code Page 1025 (Modified) 8-bit Cyrillic SB
		BLT8EBCDIC1112, // EBCDIC Code Page 1112 8-bit Baltic Multilingual SB
		BLT8EBCDIC1112S, // EBCDIC Code Page 1112 8-bit Server Baltic
							// Multilingual SB
		D8EBCDIC1141, // EBCDIC Code Page 1141 8-bit Austrian German SB, EURO
		DK8EBCDIC1142, // EBCDIC Code Page 1142 8-bit Danish SB, EURO
		S8EBCDIC1143, // EBCDIC Code Page 1143 8-bit Swedish SB, EURO
		I8EBCDIC1144, // EBCDIC Code Page 1144 8-bit Italian SB, EURO
		F8EBCDIC1147, // EBCDIC Code Page 1147 8-bit French SB, EURO
		EEC8EUROASCI, // EEC Targon 35 ASCI West European/Greek SB
		EEC8EUROPA3, // EEC EUROPA3 8-bit West European/Greek SB
		LA8PASSPORT, // German Government Printer 8-bit All-European Latin SB,
						// ASCII
		WE8HP, // HP LaserJet 8-bit West European SB
		WE8ROMAN8, // HP Roman8 8-bit West European SB, ASCII
		HU8CWI2, // Hungarian 8-bit CWI-2 SB, ASCII
		HU8ABMOD, // Hungarian 8-bit Special AB Mod SB, ASCII
		LV8RST104090, // IBM-PC Alternative Code Page 8-bit Latvian
						// (Latin/Cyrillic) SB, ASCII
		US8PC437, // IBM-PC Code Page 437 8-bit American SB, ASCII
		BG8PC437S, // IBM-PC Code Page 437 8-bit (Bulgarian Modification) SB,
					// ASCII
		EL8PC437S, // IBM-PC Code Page 437 8-bit (Greek modification) SB, ASCII
		EL8PC737, // IBM-PC Code Page 737 8-bit Greek/Latin SB
		LT8PC772, // IBM-PC Code Page 772 8-bit Lithuanian (Latin/Cyrillic) SB,
					// ASCII
		LT8PC774, // IBM-PC Code Page 774 8-bit Lithuanian (Latin) SB, ASCII
		BLT8PC775, // IBM-PC Code Page 775 8-bit Baltic SB, ASCII
		WE8PC850, // IBM-PC Code Page 850 8-bit West European SB, ASCII
		EL8PC851, // IBM-PC Code Page 851 8-bit Greek/Latin SB, ASCII
		EE8PC852, // IBM-PC Code Page 852 8-bit East European SB, ASCII
		RU8PC855, // IBM-PC Code Page 855 8-bit Latin/CyrillicSB, ASCII
		WE8PC858, // IBM-PC Code Page 858 8-bit West European SB, ASCII, EURO
		WE8PC860, // IBM-PC Code Page 860 8-bit West European SB. ASCII
		IS8PC861, // IBM-PC Code Page 861 8-bit Icelandic SB, ASCII
		CDN8PC863, // IBM-PC Code Page 863 8-bit Canadian French SB, ASCII
		N8PC865, // IBM-PC Code Page 865 8-bit Norwegian SB. ASCII
		RU8PC866, // IBM-PC Code Page 866 8-bit Latin/Cyrillic SB, ASCII
		EL8PC869, // IBM-PC Code Page 869 8-bit Greek/Latin SB, ASCII
		LV8PC1117, // IBM-PC Code Page 1117 8-bit Latvian SB, ASCII
		US8ICL, // ICL EBCDIC 8-bit American SB
		WE8ICL, // ICL EBCDIC 8-bit West European SB
		WE8ISOICLUK, // ICL special version ISO8859-1 SB
		WE8ISO8859P1, // ISO 8859-1 West European SB, ASCII
		EE8ISO8859P2, // ISO 8859-2 East European SB, ASCII
		SE8ISO8859P3, // ISO 8859-3 South European SB, ASCII
		NEE8ISO8859P4, // ISO 8859-4 North and North-East European SB, ASCII
		CL8ISO8859P5, // ISO 8859-5 Latin/Cyrillic SB, ASCII
		AR8ISO8859P6, // ISO 8859-6 Latin/Arabic SB, ASCII
		EL8ISO8859P7, // ISO 8859-7 Latin/Greek SB, ASCII, EURO
		IW8ISO8859P8, // ISO 8859-8 Latin/Hebrew SB, ASCII
		NE8ISO8859P10, // ISO 8859-10 North European SB, ASCII
		WE8ISO8859P15, // ISO 8859-15 West European SB, ASCII, EURO
		LA8ISO6937, // ISO 6937 8-bit Coded Character Set for Text Communication
					// SB, ASCII
		IW7IS960, // Israeli Standard 960 7-bit Latin/Hebrew SB
		AR8ARABICMAC, // Mac Client 8-bit Latin/Arabic SB
		EE8MACCE, // Mac Client 8-bit Central European SB
		EE8MACCROATIAN, // Mac Client 8-bit Croatian SB
		WE8MACROMAN8, // Mac Client 8-bit Extended Roman8 West European SB
		EL8MACGREEK, // Mac Client 8-bit Greek SB
		IS8MACICELANDIC, // Mac Client 8-bit Icelandic SB
		CL8MACCYRILLIC, // Mac Client 8-bit Latin/Cyrillic SB
		AR8ARABICMACS, // Mac Server 8-bit Latin/Arabic SB, ASCII
		EE8MACCES, // Mac Server 8-bit Central European SB, ASCII
		EE8MACCROATIANS, // Mac Server 8-bit Croatian SB, ASCII
		WE8MACROMAN8S, // Mac Server 8-bit Extended Roman8 West European SB,
						// ASCII
		CL8MACCYRILLICS, // Mac Server 8-bit Latin/Cyrillic SB, ASCII
		EL8MACGREEKS, // Mac Server 8-bit Greek SB, ASCII
		IS8MACICELANDICS, // Mac Server 8-bit Icelandic SB
		BG8MSWIN, // MS Windows 8-bit Bulgarian Cyrillic SB, ASCII
		LT8MSWIN921, // MS Windows Code Page 921 8-bit Lithuanian SB, ASCII
		ET8MSWIN923, // MS Windows Code Page 923 8-bit Estonian SB, ASCII
		EE8MSWIN1250, // MS Windows Code Page 1250 8-bit East European SB,
						// ASCII, EURO
		CL8MSWIN1251, // MS Windows Code Page 1251 8-bit Latin/Cyrillic SB,
						// ASCII, EURO
		WE8MSWIN1252, // MS Windows Code Page 1252 8-bit West European SB,
						// ASCII, EURO
		EL8MSWIN1253, // MS Windows Code Page 1253 8-bit Latin/Greek SB, ASCII,
						// EURO
		BLT8MSWIN1257, // MS Windows Code Page 1257 8-bit Baltic SB, ASCII, EURO
		BLT8CP921, // Latvian Standard LVS8-92(1) Windows/Unix 8-bit Baltic SB,
					// ASCII
		LV8PC8LR, // Latvian Version IBM-PC Code Page 866 8-bit Latin/Cyrillic
					// SB, ASCII
		WE8NCR4970, // NCR 4970 8-bit West European SB, ASCII
		WE8NEXTSTEP, // NeXTSTEP PostScript 8-bit West European SB, ASCII
		CL8KOI8R, // RELCOM Internet Standard 8-bit Latin/Cyrillic SB, ASCII
		US8BS2000, // Siemens 9750-62 EBCDIC 8-bit American SB
		DK8BS2000, // Siemens 9750-62 EBCDIC 8-bit Danish SB
		F8BS2000, // Siemens 9750-62 EBCDIC 8-bit French SB
		D8BS2000, // Siemens 9750-62 EBCDIC 8-bit German SB
		E8BS2000, // Siemens 9750-62 EBCDIC 8-bit Spanish SB
		S8BS2000, // Siemens 9750-62 EBCDIC 8-bit Swedish SB
		DK7SIEMENS9780X, // Siemens 97801/97808 7-bit Danish SB
		F7SIEMENS9780X, // Siemens 97801/97808 7-bit French SB
		D7SIEMENS9780X, // Siemens 97801/97808 7-bit German SB
		I7SIEMENS9780X, // Siemens 97801/97808 7-bit Italian SB
		N7SIEMENS9780X, // Siemens 97801/97808 7-bit Norwegian SB
		E7SIEMENS9780X, // Siemens 97801/97808 7-bit Spanish SB
		S7SIEMENS9780X, // Siemens 97801/97808 7-bit Swedish SB
		WE8BS2000, // Siemens EBCDIC.DF.04 8-bit West European SB
		CL8BS2000, // Siemens EBCDIC.EHC.LC 8-bit Cyrillic SB
		AL24UTFFSS, // See "Universal Character Sets" for details
		UTF8, // See "Universal Character Sets" for details
		UTFE // See "Universal Character Sets" for details
	}

	public static enum ASIAN_CHARSET {
		BN8BSCII, // Bangladesh National Code 8-bit BSCII SB, ASCII
		ZHT16BIG5, // BIG5 16-bit Traditional Chinese MB, ASCII
		ZHS16CGB231280, // CGB2312-80 16-bit Simplified Chinese MB, ASCII
		JA16EUC, // EUC 24-bit Japanese MB, ASCII
		JA16EUCYEN, // EUC 24-bit Japanese with '\' mapped to the Japanese yen
					// character MB
		JA16EUCFIXED, // EUC 16-bit Japanese. A fixed-width subset of JA16EUC
						// (contains only the 2-byte characters of JA16EUC).
						// Contains no 7- or 8-bit ASCII characters FIXED
		ZHT32EUC, // EUC 32-bit Traditional Chinese MB, ASCII
		ZHT32EUCFIXED, // EUC 32-bit Traditional Chinese (32-bit fixed-width, no
						// single byte) FIXED
		ZHS16GBK, // GBK 16-bit Simplified Chinese MB, ASCII, UDC
		ZHS16GBKFIXED, // GBK 16-bit Simplified Chinese (16-bit fixed-width, no
						// single byte) FIXED, UDC
		ZHT16CCDC, // HP CCDC 16-bit Traditional Chinese MB, ASCII
		JA16DBCS, // IBM EBCDIC 16-bit Japanese MB, UDC
		JA16EBCDIC930, // IBM DBCS Code Page 290 16-bit Japanese MB, UDC
		JA16DBCSFIXED, // IBM EBCDIC 16-bit Japanese (16-bit fixed width, no
						// single byte) FIXED, UDC
		KO16DBCS, // IBM EBCDIC 16-bit Korean MB, UDC
		KO16DBCSFIXED, // IBM EBCDIC 16-bit Korean (16-bit fixed-width, no
						// single byte) FIXED, UDC
		ZHS16DBCS, // IBM EBCDIC 16-bit Simplified Chinese MB, UDC
		ZHS16CGB231280FIXED, // CGB2312-80 16-bit Simplified Chinese (16-bit
								// fixed-width, no single byte) FIXED
		ZHS16DBCSFIXED, // IBM EBCDIC 16-bit Simplified Chinese (16-bit
						// fixed-width, no single byte) FIXED, UDC
		ZHT16DBCS, // IBM EBCDIC 16-bit Traditional Chinese MB, UDC
		ZHT16DBCSFIXED, // IBM EBCDIC 16-bit Traditional Chinese (16-bit
						// fixed-width, no single byte) FIXED
		KO16KSC5601, // KSC5601 16-bit Korean MB, ASCII
		KO16KSCCS, // KSCCS 16-bit Korean MB, ASCII
		KO16KSC5601FIXED, // KSC5601 (16-bit fixed-width, no single byte) FIXED
		JA16VMS, // JVMS 16-bit Japanese MB, ASCII
		ZHS16MACCGB231280, // Mac client CGB2312-80 16-bit Simplified Chinese MB
		JA16MACSJIS, // Mac client Shift-JIS 16-bit Japanese MB
		TH8MACTHAI, // Mac Client 8-bit Latin/Thai SB
		TH8MACTHAIS, // Mac Server 8-bit Latin/Thai SB, ASCII
		TH8TISEBCDICS, // Thai Industrial Standard 620-2533-EBCDIC Server 8-bit
						// SB
		ZHT16MSWIN950, // MS Windows Code Page 950 Traditional Chinese MB,
						// ASCII, UDC
		KO16MSWIN949, // MS Windows Code Page 949 Korean MB, ASCII, UDC
		VN8MSWIN1258, // MS Windows Code Page 1258 8-bit Vietnamese SB, ASCII,
						// EURO
		IN8ISCII, // Multiple-Script Indian Standard 8-bit Latin/Indian
					// Languages SB, ASCII
		JA16SJIS, // Shift-JIS 16-bit Japanese MB, ASCII, UDC
		JA16SJISFIXED, // Shift-JIS 16-bit Japanese. A fixed-width subset of
						// JA16SJIS (contains only the 2-byte characters of
						// JA16JIS). Contains no 7- or 8-bit ASCII characters
						// FIXED, UDC
		JA16SJISYEN, // Shift-JIS 16-bit Japanese with '\' mapped to the
						// Japanese yen character MB, UDC
		ZHT32SOPS, // SOPS 32-bit Traditional Chinese MB, ASCII
		ZHT16DBT, // Taiwan Taxation 16-bit Traditional Chinese MB, ASCII
		ZHT16BIG5FIXED, // BIG5 16-bit Traditional Chinese (16-bit fixed-width,
						// no single byte) FIXED
		TH8TISASCII, // Thai Industrial Standard 620-2533 - ASCII 8-bit SB,
						// ASCII, EURO
		TH8TISEBCDIC, // Thai Industrial Standard 620-2533 - EBCDIC 8-bit SB
		ZHT32TRIS, // TRIS 32-bit Traditional Chinese MB, ASCII
		ZHT32TRISFIXED, // TRIS 32-bit Fixed-width Traditional Chinese FIXED
		AL24UTFFSS, // See "Universal Character Sets" for details
		UTF8, // See "Universal Character Sets" for details
		UTFE, // See "Universal Character Sets" for details
		VN8VN3, // VN3 8-bit Vietnamese SB, ASCII

	}

	public static enum MiddleEasternCharSets {
		AR8APTEC715, // APTEC 715 Server 8-bit Latin/Arabic SB, ASCII
		AR8APTEC715T, // APTEC 715 8-bit Latin/Arabic SB
		AR8ASMO708PLUS, // ASMO 708 Plus 8-bit Latin/Arabic SB, ASCII
		AR8ASMO8X, // ASMO Extended 708 8-bit Latin/Arabic SB, ASCII
		AR8ADOS710, // Arabic MS-DOS 710 Server 8-bit Latin/Arabic SB, ASCII
		AR8ADOS710T, // Arabic MS-DOS 710 8-bit Latin/Arabic SB
		AR8ADOS720, // Arabic MS-DOS 720 Server 8-bit Latin/Arabic SB, ASCII
		AR8ADOS720T, // Arabic MS-DOS 720 8-bit Latin/Arabic SB
		TR7DEC, // DEC VT100 7-bit Turkish SB
		TR8DEC, // DEC 8-bit Turkish SB
		WE8EBCDIC37C, // EBCDIC Code Page 37 8-bit Oracle/c SB
		IW8EBCDIC424, // EBCDIC Code Page 424 8-bit Latin/Hebrew SB
		IW8EBCDIC424S, // EBCDIC Code Page 424 Server 8-bit Latin/Hebrew SB
		WE8EBCDIC500C, // EBCDIC Code Page 500 8-bit Oracle/c SB
		IW8EBCDIC1086, // EBCDIC Code Page 1086 8-bit Hebrew SB
		AR8EBCDIC420S, // EBCDIC Code Page 420 Server 8-bit Latin/Arabic SB
		AR8EBCDICX, // EBCDIC XBASIC Server 8-bit Latin/Arabic SB
		TR8EBCDIC1026, // EBCDIC Code Page 1026 8-bit Turkish SB
		TR8EBCDIC1026S, // EBCDIC Code Page 1026 Server 8-bit Turkish SB
		AR8HPARABIC8T, // HP 8-bit Latin/Arabic SB
		TR8PC857, // IBM-PC Code Page 857 8-bit Turkish SB, ASCII
		IW8PC1507, // IBM-PC Code Page 1507/862 8-bit Latin/Hebrew SB, ASCII
		AR8ISO8859P6, // ISO 8859-6 Latin/Arabic SB, ASCII
		IW8ISO8859P8, // ISO 8859-8 Latin/Hebrew SB, ASCII
		WE8ISO8859P9, // ISO 8859-9 West European & Turkish SB, ASCII
		LA8ISO6937, // ISO 6937 8-bit Coded Character Set for Text Communication
					// SB, ASCII
		IW7IS960, // Israeli Standard 960 7-bit Latin/Hebrew SB
		IW8MACHEBREW, // Mac Client 8-bit Hebrew SB
		AR8ARABICMAC, // Mac Client 8-bit Latin/Arabic SB
		AR8ARABICMACT, // Mac 8-bit Latin/Arabic SB
		TR8MACTURKISH, // Mac Client 8-bit Turkish SB
		IW8MACHEBREWS, // Mac Server 8-bit Hebrew SB, ASCII
		AR8ARABICMACS, // Mac Server 8-bit Latin/Arabic SB, ASCII
		TR8MACTURKISHS, // Mac Server 8-bit Turkish SB, ASCII
		TR8MSWIN1254, // MS Windows Code Page 1254 8-bit Turkish SB, ASCII, EURO
		IW8MSWIN1255, // MS Windows Code Page 1255 8-bit Latin/Hebrew SB, ASCII,
						// EURO
		AR8MSWIN1256, // MS Windows Code Page 1256 8-Bit Latin/Arabic SB. ASCII,
						// EURO
		IN8ISCII, // Multiple-Script Indian Standard 8-bit Latin/Indian
					// Languages SB
		AR8MUSSAD768, // Mussa'd Alarabi/2 768 Server 8-bit Latin/Arabic SB,
						// ASCII
		AR8MUSSAD768T, // Mussa'd Alarabi/2 768 8-bit Latin/Arabic SB
		AR8NAFITHA711, // Nafitha Enhanced 711 Server 8-bit Latin/Arabic SB,
						// ASCII
		AR8NAFITHA711T, // Nafitha Enhanced 711 8-bit Latin/Arabic SB
		AR8NAFITHA721, // Nafitha International 721 Server 8-bit Latin/Arabic
						// SB, ASCII
		AR8NAFITHA721T, // Nafitha International 721 8-bit Latin/Arabic SB
		AR8SAKHR706, // SAKHR 706 Server 8-bit Latin/Arabic SB, ASCII
		AR8SAKHR707, // SAKHR 707 Server 8-bit Latin/Arabic SB, ASCII
		AR8SAKHR707T, // SAKHR 707 8-bit Latin/Arabic SB
		AR8XBASIC, // XBASIC 8-bit Latin/Arabic SB
		WE8BS2000L5, // Siemens EBCDIC.DF.04.L5 8-bit West European/Turkish SB
		AL24UTFFSS, // See "Universal Character Sets" for details
		UTF8, // See "Universal Character Sets" for details
		UTFE,// See "Universal Character Sets" for details
	}

	public static enum CharacterSet {
		AL24UTFFSS, // See "Universal Character Sets" for details
		AR8ADOS710, // Arabic MS-DOS 710 Server 8-bit Latin/Arabic SB, ASCII
		AR8ADOS710T, // Arabic MS-DOS 710 8-bit Latin/Arabic SB
		AR8ADOS720, // Arabic MS-DOS 720 Server 8-bit Latin/Arabic SB, ASCII
		AR8ADOS720T, // Arabic MS-DOS 720 8-bit Latin/Arabic SB
		AR8APTEC715, // APTEC 715 Server 8-bit Latin/Arabic SB, ASCII
		AR8APTEC715T, // APTEC 715 8-bit Latin/Arabic SB
		AR8ARABICMAC, // Mac Client 8-bit Latin/Arabic SB
		AR8ARABICMACS, // Mac Server 8-bit Latin/Arabic SB, ASCII
		AR8ARABICMACT, // Mac 8-bit Latin/Arabic SB
		AR8ASMO708PLUS, // ASMO 708 Plus 8-bit Latin/Arabic SB, ASCII
		AR8ASMO8X, // ASMO Extended 708 8-bit Latin/Arabic SB, ASCII
		AR8EBCDIC420S, // EBCDIC Code Page 420 Server 8-bit Latin/Arabic SB
		AR8EBCDICX, // EBCDIC XBASIC Server 8-bit Latin/Arabic SB
		AR8HPARABIC8T, // HP 8-bit Latin/Arabic SB
		AR8ISO8859P6, // ISO 8859-6 Latin/Arabic SB, ASCII
		AR8MSWIN1256, // MS Windows Code Page 1256 8-Bit Latin/Arabic SB. ASCII,
						// EURO
		AR8MUSSAD768, // Mussa'd Alarabi/2 768 Server 8-bit Latin/Arabic SB,
						// ASCII
		AR8MUSSAD768T, // Mussa'd Alarabi/2 768 8-bit Latin/Arabic SB
		AR8NAFITHA711, // Nafitha Enhanced 711 Server 8-bit Latin/Arabic SB,
						// ASCII
		AR8NAFITHA711T, // Nafitha Enhanced 711 8-bit Latin/Arabic SB
		AR8NAFITHA721, // Nafitha International 721 Server 8-bit Latin/Arabic
						// SB, ASCII
		AR8NAFITHA721T, // Nafitha International 721 8-bit Latin/Arabic SB
		AR8SAKHR706, // SAKHR 706 Server 8-bit Latin/Arabic SB, ASCII
		AR8SAKHR707, // SAKHR 707 Server 8-bit Latin/Arabic SB, ASCII
		AR8SAKHR707T, // SAKHR 707 8-bit Latin/Arabic SB
		AR8XBASIC, // XBASIC 8-bit Latin/Arabic SB
		BG8MSWIN, // MS Windows 8-bit Bulgarian Cyrillic SB, ASCII
		BG8PC437S, // IBM-PC Code Page 437 8-bit (Bulgarian Modification) SB,
					// ASCII
		BLT8CP921, // Latvian Standard LVS8-92(1) Windows/Unix 8-bit Baltic SB,
					// ASCII
		BLT8EBCDIC1112, // EBCDIC Code Page 1112 8-bit Baltic Multilingual SB
		BLT8EBCDIC1112S, // EBCDIC Code Page 1112 8-bit Server Baltic
							// Multilingual SB
		BLT8MSWIN1257, // MS Windows Code Page 1257 8-bit Baltic SB, ASCII, EURO
		BLT8PC775, // IBM-PC Code Page 775 8-bit Baltic SB, ASCII
		BN8BSCII, // Bangladesh National Code 8-bit BSCII SB, ASCII
		CDN8PC863, // IBM-PC Code Page 863 8-bit Canadian French SB, ASCII
		CH7DEC, // DEC VT100 7-bit Swiss (German/French) SB
		CL8BS2000, // Siemens EBCDIC.EHC.LC 8-bit Cyrillic SB
		CL8EBCDIC1025, // EBCDIC Code Page 1025 8-bit Cyrillic SB
		CL8EBCDIC1025C, // EBCDIC Code Page 1025 Client 8-bit Cyrillic SB
		CL8EBCDIC1025S, // EBCDIC Code Page 1025 Server 8-bit Cyrillic SB
		CL8EBCDIC1025X, // EBCDIC Code Page 1025 (Modified) 8-bit Cyrillic SB
		CL8ISO8859P5, // ISO 8859-5 Latin/Cyrillic SB, ASCII
		CL8KOI8R, // RELCOM Internet Standard 8-bit Latin/Cyrillic SB, ASCII
		CL8MACCYRILLIC, // Mac Client 8-bit Latin/Cyrillic SB
		CL8MACCYRILLICS, // Mac Server 8-bit Latin/Cyrillic SB, ASCII
		CL8MSWIN1251, // MS Windows Code Page 1251 8-bit Latin/Cyrillic SB,
						// ASCII, EURO
		D7DEC, // DEC VT100 7-bit German SB
		D7SIEMENS9780X, // Siemens 97801/97808 7-bit German SB
		D8BS2000, // Siemens 9750-62 EBCDIC 8-bit German SB
		D8EBCDIC1141, // EBCDIC Code Page 1141 8-bit Austrian German SB, EURO
		D8EBCDIC273, // EBCDIC Code Page 273/1 8-bit Austrian German SB
		DK7SIEMENS9780X, // Siemens 97801/97808 7-bit Danish SB
		DK8BS2000, // Siemens 9750-62 EBCDIC 8-bit Danish SB
		DK8EBCDIC1142, // EBCDIC Code Page 1142 8-bit Danish SB, EURO
		DK8EBCDIC277, // EBCDIC Code Page 277/1 8-bit Danish SB
		E7DEC, // DEC VT100 7-bit Spanish SB
		E7SIEMENS9780X, // Siemens 97801/97808 7-bit Spanish SB
		E8BS2000, // Siemens 9750-62 EBCDIC 8-bit Spanish SB
		EE8EBCDIC870, // EBCDIC Code Page 870 8-bit East European SB
		EE8EBCDIC870C, // EBCDIC Code Page 870 Client 8-bit East European SB
		EE8EBCDIC870S, // EBCDIC Code Page 870 Server 8-bit East European SB
		EE8ISO8859P2, // ISO 8859-2 East European SB, ASCII
		EE8MACCE, // Mac Client 8-bit Central European SB
		EE8MACCES, // Mac Server 8-bit Central European SB, ASCII
		EE8MACCROATIAN, // Mac Client 8-bit Croatian SB
		EE8MACCROATIANS, // Mac Server 8-bit Croatian SB, ASCII
		EE8MSWIN1250, // MS Windows Code Page 1250 8-bit East European SB,
						// ASCII, EURO
		EE8PC852, // IBM-PC Code Page 852 8-bit East European SB, ASCII
		EEC8EUROASCI, // EEC Targon 35 ASCI West European/Greek SB
		EEC8EUROPA3, // EEC EUROPA3 8-bit West European/Greek SB
		EL8DEC, // DEC 8-bit Latin/Greek SB
		EL8EBCDIC875, // EBCDIC Code Page 875 8-bit Greek SB
		EL8EBCDIC875S, // EBCDIC Code Page 875 Server 8-bit Greek SB
		EL8GCOS7, // Bull EBCDIC GCOS7 8-bit Greek SB
		EL8ISO8859P7, // ISO 8859-7 Latin/Greek SB, ASCII, EURO
		EL8MACGREEK, // Mac Client 8-bit Greek SB
		EL8MACGREEKS, // Mac Server 8-bit Greek SB, ASCII
		EL8MSWIN1253, // MS Windows Code Page 1253 8-bit Latin/Greek SB, ASCII,
						// EURO
		EL8PC437S, // IBM-PC Code Page 437 8-bit (Greek modification) SB, ASCII
		EL8PC737, // IBM-PC Code Page 737 8-bit Greek/Latin SB
		EL8PC851, // IBM-PC Code Page 851 8-bit Greek/Latin SB, ASCII
		EL8PC869, // IBM-PC Code Page 869 8-bit Greek/Latin SB, ASCII
		ET8MSWIN923, // MS Windows Code Page 923 8-bit Estonian SB, ASCII
		F7DEC, // DEC VT100 7-bit French SB
		F7SIEMENS9780X, // Siemens 97801/97808 7-bit French SB
		F8BS2000, // Siemens 9750-62 EBCDIC 8-bit French SB
		F8EBCDIC1147, // EBCDIC Code Page 1147 8-bit French SB, EURO
		F8EBCDIC297, // EBCDIC Code Page 297 8-bit French SB
		HU8ABMOD, // Hungarian 8-bit Special AB Mod SB, ASCII
		HU8CWI2, // Hungarian 8-bit CWI-2 SB, ASCII
		I7DEC, // DEC VT100 7-bit Italian SB
		I7SIEMENS9780X, // Siemens 97801/97808 7-bit Italian SB
		I8EBCDIC1144, // EBCDIC Code Page 1144 8-bit Italian SB, EURO
		I8EBCDIC280, // EBCDIC Code Page 280/1 8-bit Italian SB
		IN8ISCII, // Multiple-Script Indian Standard 8-bit Latin/Indian
					// Languages SB, ASCII
		IS8MACICELANDIC, // Mac Client 8-bit Icelandic SB
		IS8MACICELANDICS, // Mac Server 8-bit Icelandic SB
		IS8PC861, // IBM-PC Code Page 861 8-bit Icelandic SB, ASCII
		IW7IS960, // Israeli Standard 960 7-bit Latin/Hebrew SB
		IW8EBCDIC1086, // EBCDIC Code Page 1086 8-bit Hebrew SB
		IW8EBCDIC424, // EBCDIC Code Page 424 8-bit Latin/Hebrew SB
		IW8EBCDIC424S, // EBCDIC Code Page 424 Server 8-bit Latin/Hebrew SB
		IW8ISO8859P8, // ISO 8859-8 Latin/Hebrew SB, ASCII
		IW8MACHEBREW, // Mac Client 8-bit Hebrew SB
		IW8MACHEBREWS, // Mac Server 8-bit Hebrew SB, ASCII
		IW8MSWIN1255, // MS Windows Code Page 1255 8-bit Latin/Hebrew SB, ASCII,
						// EURO
		IW8PC1507, // IBM-PC Code Page 1507/862 8-bit Latin/Hebrew SB, ASCII
		JA16DBCS, // IBM EBCDIC 16-bit Japanese MB, UDC
		JA16DBCSFIXED, // IBM EBCDIC 16-bit Japanese (16-bit fixed width, no
						// single byte) FIXED, UDC
		JA16EBCDIC930, // IBM DBCS Code Page 290 16-bit Japanese MB, UDC
		JA16EUC, // EUC 24-bit Japanese MB, ASCII
		JA16EUCFIXED, // EUC 16-bit Japanese. A fixed-width subset of JA16EUC
						// (contains only the 2-byte characters of JA16EUC).
						// Contains no 7- or 8-bit ASCII characters FIXED
		JA16EUCYEN, // EUC 24-bit Japanese with '\' mapped to the Japanese yen
					// character MB
		JA16MACSJIS, // Mac client Shift-JIS 16-bit Japanese MB
		JA16SJIS, // Shift-JIS 16-bit Japanese MB, ASCII, UDC
		JA16SJISFIXED, // Shift-JIS 16-bit Japanese. A fixed-width subset of
						// JA16SJIS (contains only the 2-byte characters of
						// JA16JIS). Contains no 7- or 8-bit ASCII characters
						// FIXED, UDC
		JA16SJISYEN, // Shift-JIS 16-bit Japanese with '\' mapped to the
						// Japanese yen character MB, UDC
		JA16VMS, // JVMS 16-bit Japanese MB, ASCII
		KO16DBCS, // IBM EBCDIC 16-bit Korean MB, UDC
		KO16DBCSFIXED, // IBM EBCDIC 16-bit Korean (16-bit fixed-width, no
						// single byte) FIXED, UDC
		KO16KSC5601, // KSC5601 16-bit Korean MB, ASCII
		KO16KSC5601FIXED, // KSC5601 (16-bit fixed-width, no single byte) FIXED
		KO16KSCCS, // KSCCS 16-bit Korean MB, ASCII
		KO16MSWIN949, // MS Windows Code Page 949 Korean MB, ASCII, UDC
		LA8ISO6937, // ISO 6937 8-bit Coded Character Set for Text Communication
					// SB, ASCII
		LA8PASSPORT, // German Government Printer 8-bit All-European Latin SB,
						// ASCII
		LT8MSWIN921, // MS Windows Code Page 921 8-bit Lithuanian SB, ASCII
		LT8PC772, // IBM-PC Code Page 772 8-bit Lithuanian (Latin/Cyrillic) SB,
					// ASCII
		LT8PC774, // IBM-PC Code Page 774 8-bit Lithuanian (Latin) SB, ASCII
		LV8PC1117, // IBM-PC Code Page 1117 8-bit Latvian SB, ASCII
		LV8PC8LR, // Latvian Version IBM-PC Code Page 866 8-bit Latin/Cyrillic
					// SB, ASCII
		LV8RST104090, // IBM-PC Alternative Code Page 8-bit Latvian
						// (Latin/Cyrillic) SB, ASCII
		N7SIEMENS9780X, // Siemens 97801/97808 7-bit Norwegian SB
		N8PC865, // IBM-PC Code Page 865 8-bit Norwegian SB. ASCII
		NDK7DEC, // DEC VT100 7-bit Norwegian/Danish SB
		NE8ISO8859P10, // ISO 8859-10 North European SB, ASCII
		NEE8ISO8859P4, // ISO 8859-4 North and North-East European SB, ASCII
		NL7DEC, // DEC VT100 7-bit Dutch SB
		RU8BESTA, // BESTA 8-bit Latin/Cyrillic SB, ASCII
		RU8PC855, // IBM-PC Code Page 855 8-bit Latin/CyrillicSB, ASCII
		RU8PC866, // IBM-PC Code Page 866 8-bit Latin/Cyrillic SB, ASCII
		S7DEC, // DEC VT100 7-bit Swedish SB
		S7SIEMENS9780X, // Siemens 97801/97808 7-bit Swedish SB
		S8BS2000, // Siemens 9750-62 EBCDIC 8-bit Swedish SB
		S8EBCDIC1143, // EBCDIC Code Page 1143 8-bit Swedish SB, EURO
		S8EBCDIC278, // EBCDIC Code Page 278/1 8-bit Swedish SB
		SE8ISO8859P3, // ISO 8859-3 South European SB, ASCII
		SF7ASCII, // ASCII 7-bit Finnish SB
		SF7DEC, // DEC VT100 7-bit Finnish SB
		TH8MACTHAI, // Mac Client 8-bit Latin/Thai SB
		TH8MACTHAIS, // Mac Server 8-bit Latin/Thai SB, ASCII
		TH8TISASCII, // Thai Industrial Standard 620-2533 - ASCII 8-bit SB,
						// ASCII, EURO
		TH8TISEBCDIC, // Thai Industrial Standard 620-2533 - EBCDIC 8-bit SB
		TH8TISEBCDICS, // Thai Industrial Standard 620-2533-EBCDIC Server 8-bit
						// SB
		TR7DEC, // DEC VT100 7-bit Turkish SB
		TR8DEC, // DEC 8-bit Turkish SB
		TR8EBCDIC1026, // EBCDIC Code Page 1026 8-bit Turkish SB
		TR8EBCDIC1026S, // EBCDIC Code Page 1026 Server 8-bit Turkish SB
		TR8MACTURKISH, // MAC Client 8-bit Turkish SB
		TR8MACTURKISHS, // MAC Server 8-bit Turkish SB, ASCII
		TR8MSWIN1254, // MS Windows Code Page 1254 8-bit Turkish SB, ASCII, EURO
		TR8PC857, // IBM-PC Code Page 857 8-bit Turkish SB, ASCII
		US7ASCII, // ASCII 7-bit American SB, ASCII
		US8BS2000, // Siemens 9750-62 EBCDIC 8-bit American SB
		US8ICL, // ICL EBCDIC 8-bit American SB
		US8PC437, // IBM-PC Code Page 437 8-bit American SB, ASCII
		UTF8, // See "Universal Character Sets" for details
		UTFE, // See "Universal Character Sets" for details
		VN8MSWIN1258, // MS Windows Code Page 1258 8-bit Vietnamese SB, ASCII,
						// EURO
		VN8VN3, // VN3 8-bit Vietnamese SB, ASCII
		WE8BS2000, // Siemens EBCDIC.DF.04 8-bit West European SB
		WE8BS2000L5, // Siemens EBCDIC.DF.04.L5 8-bit West European/Turkish SB
		WE8DEC, // DEC 8-bit West European SB, ASCII
		WE8DG, // DG 8-bit West European SB, ASCII
		WE8EBCDIC1047, // EBCDIC Code Page 1047 8-bit West European SB
		WE8EBCDIC1140, // EBCDIC Code Page 1140 8-bit West European SB, EURO
		WE8EBCDIC1140C, // EBCDIC Code Page 1140 Client 8-bit West European SB,
						// EURO
		WE8EBCDIC1145, // EBCDIC Code Page 1145 8-bit West European SB, EURO
		WE8EBCDIC1146, // EBCDIC Code Page 1146 8-bit West European SB, EURO
		WE8EBCDIC1148, // EBCDIC Code Page 1148 8-bit West European SB, EURO
		WE8EBCDIC1148C, // EBCDIC Code Page 1148 Client 8-bit West European SB,
						// EURO
		WE8EBCDIC284, // EBCDIC Code Page 284 8-bit Latin American/Spanish SB
		WE8EBCDIC285, // EBCDIC Code Page 285 8-bit West European SB
		WE8EBCDIC37, // EBCDIC Code Page 37 8-bit West European SB
		WE8EBCDIC37C, // EBCDIC Code Page 37 8-bit Oracle/c SB
		WE8EBCDIC500, // EBCDIC Code Page 500 8-bit West European SB
		WE8EBCDIC500C, // EBCDIC Code Page 500 8-bit Oracle/c SB
		WE8EBCDIC871, // EBCDIC Code Page 871 8-bit Icelandic SB
		WE8GCOS7, // Bull EBCDIC GCOS7 8-bit West European SB
		WE8HP, // HP LaserJet 8-bit West European SB
		WE8ICL, // ICL EBCDIC 8-bit West European SB
		WE8ISO8859P1, // ISO 8859-1 West European SB, ASCII
		WE8ISO8859P15, // ISO 8859-15 West European SB, ASCII, EURO
		WE8ISO8859P9, // ISO 8859-9 West European & Turkish SB, ASCII
		WE8ISOICLUK, // ICL special version ISO8859-1 SB
		WE8MACROMAN8, // Mac Client 8-bit Extended Roman8 West European SB
		WE8MACROMAN8S, // Mac Server 8-bit Extended Roman8 West European SB,
						// ASCII
		WE8MSWIN1252, // MS Windows Code Page 1252 8-bit West European SB,
						// ASCII, EURO
		WE8NCR4970, // NCR 4970 8-bit West European SB, ASCII
		WE8NEXTSTEP, // NeXTSTEP PostScript 8-bit West European SB, ASCII
		WE8PC850, // IBM-PC Code Page 850 8-bit West European SB, ASCII
		WE8PC858, // IBM-PC Code Page 858 8-bit West European SB, ASCII, EURO
		WE8PC860, // IBM-PC Code Page 860 8-bit West European SB. ASCII
		WE8ROMAN8, // HP Roman8 8-bit West European SB, ASCII
		YUG7ASCII, // ASCII 7-bit Yugoslavian SB
		ZHS16CGB231280, // CGB2312-80 16-bit Simplified Chinese MB, ASCII
		ZHS16CGB231280FIXED, // CGB2312-80 16-bit Simplified Chinese (16-bit
								// fixed-width, no single byte) FIXED
		ZHS16DBCS, // IBM EBCDIC 16-bit Simplified Chinese MB, UDC
		ZHS16DBCSFIXED, // IBM EBCDIC 16-bit Simplified Chinese (16-bit
						// fixed-width, no single byte) FIXED, UDC
		ZHS16GBK, // GBK 16-bit Simplified Chinese MB, ASCII, UDC
		ZHS16GBKFIXED, // GBK 16-bit Simplified Chinese (16-bit fixed-width, no
						// single byte) FIXED, UDC
		ZHS16MACCGB231280, // Mac client CGB2312-80 16-bit Simplified Chinese MB
		ZHT16BIG5, // BIG5 16-bit Traditional Chinese MB, ASCII
		ZHT16BIG5FIXED, // BIG5 16-bit Traditional Chinese (16-bit fixed-width,
						// no single byte) FIXED
		ZHT16CCDC, // HP CCDC 16-bit Traditional Chinese MB, ASCII
		ZHT16DBCS, // IBM EBCDIC 16-bit Traditional Chinese MB, UDC
		ZHT16DBCSFIXED, // IBM EBCDIC 16-bit Traditional Chinese (16-bit
						// fixed-width, no single byte) FIXED
		ZHT16DBT, // Taiwan Taxation 16-bit Traditional Chinese MB, ASCII
		ZHT16MSWIN950, // MS Windows Code Page 950 Traditional Chinese MB,
						// ASCII, UDC
		ZHT32EUC, // EUC 32-bit Traditional Chinese MB, ASCII
		ZHT32EUCFIXED, // EUC 32-bit Traditional Chinese (32-bit fixed-width, no
						// single byte) FIXED
		ZHT32SOPS, // SOPS 32-bit Traditional Chinese MB, ASCII
		ZHT32TRIS, // TRIS 32-bit Traditional Chinese MB, ASCII
		ZHT32TRISFIXED, // TRIS 32-bit Fixed-width Traditional Chinese FIXED
	}

	/**
	 * Test with some standard types.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		debug = true;
		getNLSLang();
		resetNLSLang();
		getNLSLang("french_germany.utf8");
		resetNLSLang();
		getNLSLang("_france.utf8");
		resetNLSLang();
		getNLSLang("french:utf8");
		resetNLSLang();
		getNLSLang(".utf8");
		resetNLSLang();
		getNLSLang("en");
		resetNLSLang();
		getNLSLang("ENGLISH");
		resetNLSLang();
		getNLSLang("BARRY");
		resetNLSLang();
		getNLSLang("ENGLISH_BARRY");
		resetNLSLang();
		getNLSLang("ENGLISH_FRANCE");
		resetNLSLang();
		getNLSLang("ENGLISH_FRANCE.BARRY");
		resetNLSLang();
		getNLSLang("ENGLISH_FRANCE.ZHS16GBK");

	}

	public static String getNLSTerritory() {
		return country;
	}

	public static String getNLSLanguage() {
		return lang;
	}

	public static String getNLSEncoding() {
		return encoding;
	}

	private static void getNLSLang(String string) {
		nls_lang = string;
		if (nls_lang == null) {
			resetNLSLang();
		} else if (nls_lang != null && nls_lang.length() != 0) {
			// Look for encoding
			String[] bits = nls_lang.split("\\.");
			if (bits.length > 1) { // We have encoding.
				encoding = bits[1];
			}
			String[] minibits = bits[0].split("_");
			if (minibits.length > 1) {// we have a territory
				country = minibits[1];
			}
			lang = minibits[0]; // last bit is lang
		}
		if (debug) {
			System.out.println(MessageFormat.format("NLS_LANG={0},lang {1},territory {2}, encoding {3}, isValid {4}",
					nls_lang, lang, country, encoding, isNLSLangValid()));
		}
	}

	private static void getNLSLang() {
		String nlslang = System.getenv("NLS_LANG");
		getNLSLang(nlslang);
	}

	public static void resetNLSLang() {
		lang = "";
		country = "";
		encoding = "";
	}

	/**
	 * Check to see if Oracle NLS_LANG variable is set. Right now, this only
	 * checks linux but should also try windowsUtility to see if it is set in
	 * the Windows registry too.
	 * 
	 * @return true if it is set.
	 */
	public static boolean isNLSLangSet() {
		if (System.getenv("NLS_LANG") != null) {
			getNLSLang();
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param lang
	 * @return
	 */
	public static boolean isValidLanguage(String lang) {
		Languages realLang = getEnumFromString(Languages.class, lang);
		if (realLang != null) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isValidTerritory(String territory) {
		Territories realTerritory = getEnumFromString(Territories.class, territory);
		if (realTerritory != null) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isValidCharacterSet(String charSet) {
		CharacterSet realTerritory = getEnumFromString(CharacterSet.class, charSet);
		if (realTerritory != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * A common method for all enums since they can't have another base class
	 * 
	 * @param <T>
	 *            Enum type
	 * @param c
	 *            enum type. All enums must be all caps.
	 * @param string
	 *            case insensitive
	 * @return corresponding enum, or null
	 */
	public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string) {
		if (c != null && string != null) {
			try {
				return Enum.valueOf(c, string.trim().toUpperCase());
			} catch (IllegalArgumentException ex) {
			}
		}
		return null;
	}

	public static boolean isNLSLangValid() {
		if (!lang.equals("") && !isValidLanguage(lang)) {
			return false;
		}
		if (!country.equals("") && !isValidTerritory(country)) {
			return false;
		}
		if (!encoding.equals("") && !isValidCharacterSet(encoding)) {
			return false;
		}
		if ((encoding + country + lang).length() == 0) {
			return false;
		}
		return true;
	}

	public static String getNLSLANG() {
		return nls_lang;
	}

}
