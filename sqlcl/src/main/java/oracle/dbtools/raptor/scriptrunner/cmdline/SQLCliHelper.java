/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline;

import java.io.File;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.AdvancedConnectionSupport;
import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.ResultSetFormatter;
import oracle.dbtools.raptor.console.clone.DbtoolsConsoleReader;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.proformatter.CodingStyleSQLOptionsBean;
import oracle.dbtools.raptor.proformatter.SQLPlusFormatter;
import oracle.dbtools.raptor.scriptrunner.cmdline.handlers.SQLCliGetSpoolFilterProvider;
import oracle.dbtools.raptor.scriptrunner.cmdline.handlers.SQLCliHandlePauseProvider;
import oracle.dbtools.raptor.scriptrunner.cmdline.handlers.SQLCliHandleSetPauseProvider;
import oracle.dbtools.raptor.scriptrunner.cmdline.handlers.SQLCliPromptedAcceptProvider;
import oracle.dbtools.raptor.scriptrunner.cmdline.handlers.SQLCliPromptedConnectFieldsProvider;
import oracle.dbtools.raptor.scriptrunner.cmdline.handlers.SQLCliPromptedPasswordFieldsProvider;
import oracle.dbtools.raptor.scriptrunner.cmdline.handlers.SQLCliPromptedSubstitutionProvider;
import oracle.dbtools.versions.SQLclVersion;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SQLCliHelper.java"
 *         >Barry McGillin</a>
 *
 */
public class SQLCliHelper {

	private static final String DEFAULT_LANG = "en"; //$NON-NLS-1$
	private static final String DEFAULT_COUNTRY = "US"; //$NON-NLS-1$

	private static Locale getLocale() {
		String lang = DEFAULT_LANG;
		String country = DEFAULT_COUNTRY;
		if (System.getProperty("user.language")!=null && !System.getProperty("user.language").trim().equals("")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			lang=System.getProperty("user.language"); //$NON-NLS-1$
		}
		if (System.getProperty("user.country")!=null && !System.getProperty("user.country").trim().equals("")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			country=System.getProperty("user.country"); //$NON-NLS-1$
		}
		Locale locale = new Locale(lang, country);
	
		return locale;
	}
	
	public static String getBannerTime() {
			Format dateformatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy",getLocale()); //$NON-NLS-1$
		Calendar now = Calendar.getInstance(getLocale());
		return dateformatter.format(now.getTime());
	}

	public static String getBannerYear() {
		Format yearFormatter = new SimpleDateFormat("yyyy",getLocale());// (authorized) //$NON-NLS-1$
		Calendar now = Calendar.getInstance(getLocale());
		return yearFormatter.format(now.getTime());
	}

	public static ScriptRunnerContext getCliScriptRunnerContext(
			DbtoolsConsoleReader reader) {
		ScriptRunnerContext context = new ScriptRunnerContext();
		context.setVersion(new Version(SQLclVersion.getSQLclVersion()));
		context.setSpoolFilterProvider(new SQLCliGetSpoolFilterProvider());
		context.setPromptedFieldProvider(new SQLCliPromptedAcceptProvider());
		context.setSubstitutionFieldProvider(new SQLCliPromptedSubstitutionProvider());
		context.setPasswordFieldsProvider(new SQLCliPromptedPasswordFieldsProvider());
		context.setConnectFieldsProvider(new SQLCliPromptedConnectFieldsProvider());
		context.setHandlePauseProvider(new SQLCliHandlePauseProvider());
		context.setHandleSetPauseProvider(new SQLCliHandleSetPauseProvider());
		context.setSQLPlusFormatter(new SQLPlusFormatter());
		context.setSQLPlusCodingStyleOptions(new CodingStyleSQLOptionsBean());
		context.putProperty(ScriptRunnerContext.JLINE_SETTING, reader);
		context.setTopLevel(true);
		context.putProperty(ScriptRunnerContext.SYSTEM_OUT, true);
		context.putProperty(ScriptRunnerContext.ARRAYSIZE, "15"); //sqlplus default //$NON-NLS-1$
		
		// setting the defaults according the SQL*Plus User Guide and Reference Doc.
		// http://yourmachine.yourdomain/12/121/server.121/e18404/ch_twelve040.htm#BACGAJIC 
		context.putProperty(ScriptRunnerContext.SETPAGESIZE, 14);
		context.putProperty(ScriptRunnerContext.SETLINESIZE, 80);
		context.putProperty(ScriptRunnerContext.SETLONG, 80);
		context.putProperty(ScriptRunnerContext.SETLONGCHUNKSIZE, new Integer(80));
		context.putProperty(ScriptRunnerContext.SETHEADING, "ON"); //$NON-NLS-1$
		context.putProperty(ScriptRunnerContext.SETNUMWIDTH, new Integer(10)); 
		context.putProperty(ScriptRunnerContext.SETNUMFORMAT, ""); //$NON-NLS-1$
		context.setEncoding(Charset.defaultCharset().displayName());
		context.consumerRuning(true);// no consumer running - none needed we are
										// just printing to stdout.
		// NODE_URL is used to get the dir so add a ficticious file on the end
		// for removal
		try {
			context.putProperty(ScriptRunnerContext.NODE_URL,
					new File(System.getProperty("user.dir") + File.separator //$NON-NLS-1$
							+ "filename_will_be_stripped").toURI().toURL()); //$NON-NLS-1$
		} catch (MalformedURLException e) {
			Logger.getLogger("oracle.dbtools").log(Level.SEVERE, e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		} //$NON-NLS-1

		context.putProperty(ScriptRunnerContext.DBCONFIG_GLOGIN, true);
		context.putProperty(ScriptRunnerContext.DBCONFIG_USE_THICK_DRIVER,
				false);

		return context;
	}

	public static void setDefaultScriptRunnerProperies() {
		ResultSetFormatter.setMaxLines(Integer.MAX_VALUE);
		ResultSetFormatter.setMaxRows(Integer.MAX_VALUE);
		ConnectionSupport.setConnectionSupportImplementation(new AdvancedConnectionSupport());
	}

}
