/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.plusplus.JDBCHelper;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.restricted.RunnerRestrictedLevel;
import oracle.dbtools.util.Logger;

/**
 * SQLPlus options are easy to manage. and they are well defined by the
 * documentation for SQLPlus <br>
 * sqlplus [ [options] [logon] [start] <br>
 * where options include help, logon, restrict, silent, and our new lggging one
 * optl now called verbose
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SQLCliOptions.java"
 *         >Barry McGillin</a>
 *
 */
public class SQLCliOptions {
	/**
	 * OPTIONS
	 */
	public static String commandlineArg = ""; //$NON-NLS-1$
	public static ArrayList<String> options = new ArrayList<String>();
	public static Properties properties = new Properties();
	public static final String LOGGING = "-verbose";// do not //$NON-NLS-1$
													// switch off logging at
													// the
													// start
	/**
	 * -H[ELP]
	 * 
	 * Displays the usage and syntax for the SQLPLUS command, and then returns
	 * control to the operating system.
	 */
	public static final String H = "-h"; //$NON-NLS-1$
	public static final String HELP = "-help"; //$NON-NLS-1$
	/**
	 * -L[OGON]
	 * 
	 * Specifies not to reprompt for username or password if the initial
	 * connection does not succeed. This can be useful in operating system
	 * scripts that must either succeed or fail and you don't want to be
	 * reprompted for connection details if the database server is not running.
	 * This option is not available with the Windows graphical user interface
	 * SQL*Plus executable.
	 */
	public static final String L = "-l"; //$NON-NLS-1$
	public static final String LOGON = "-logon"; //$NON-NLS-1$
	/**
	 * -R[ESTRICT] {1|2|3}
	 * 
	 * Allows you to disable certain commands that interact with the operating
	 * system. This is similar to disabling the same commands in the Product
	 * User Profile (PUP) table. However, commands disabled with the -RESTRICT
	 * option are disabled even if there is no connection to a server, and
	 * remain disabled until SQL*Plus terminates.
	 * 
	 * If no -RESTRICT option is active, than all commands can be used, unless
	 * disabled in the PUP table.
	 * 
	 * If -RESTRICT 3 is used, then LOGIN.SQL is not read. GLOGIN.SQL is read
	 * but restricted commands used will fail.
	 * <table>
	 * <tr>
	 * <th>Command</th>
	 * <th>Level 1</th>
	 * <th>Level 2</th>
	 * <th>Level 3</th>
	 * <th>Level 4</th>
	 * </tr>
	 * <tr>
	 * <td>EDIT</td>
	 * <td>disabled</td>
	 * <td>disabled</td>
	 * <td>disabled</td>
	 * </tr>
	 * <tr>
	 * <td>GET</td>
	 * <td></td>
	 * <td></td>
	 * <td>disabled</td>
	 * </tr>
	 * <tr>
	 * <td>HOST</td>
	 * <td>disabled</td>
	 * <td>disabled</td>
	 * <td>disabled</td>
	 * </tr>
	 * <tr>
	 * <td>SAVE</td>
	 * <td></td>
	 * <td>disabled</td>
	 * <td>disabled</td>
	 * </tr>
	 * <tr>
	 * <td>SPOOL</td>
	 * <td></td>
	 * <td>disabled</td>
	 * <td>disabled</td>
	 * </tr>
	 * <tr>
	 * <td>START @ @@</td>
	 * <td></td>
	 * <td></td>
	 * <td>disabled</td>
	 * </tr>
	 * <tr>
	 * <td>STORE</td><td</td>
	 * <td>disabled</td>
	 * <td>disabled</td>
	 * </tr>
	 * </table>
	 */
	public static final String R = "-r"; //$NON-NLS-1$
	public static final String RESTRICT = "-restrict"; //$NON-NLS-1$
	public static final String R1 = "1"; //$NON-NLS-1$
	public static final String R2 = "2"; //$NON-NLS-1$
	public static final String R3 = "3"; //$NON-NLS-1$
	public static final String R4 = "4"; //$NON-NLS-1$
	/**
	 * -S[ILENT]
	 * 
	 * Suppresses all SQL*Plus information and prompt messages, including the
	 * command prompt, the echoing of commands, and the banner normally
	 * displayed when you start SQL*Plus. If you omit username or password,
	 * SQL*Plus prompts for them, but the prompts are not visible. Use SILENT to
	 * invoke SQL*Plus within another program so that the use of SQL*Plus is
	 * invisible to the user.
	 * 
	 * SILENT is a useful mode for creating reports for the web using the
	 * SQLPLUS -MARKUP command inside a CGI script or operating system script.
	 * The SQL*Plus banner and prompts are suppressed and do not appear in
	 * reports created using the SILENT option.
	 */
	public static final String S = "-s"; //$NON-NLS-1$
	public static final String SILENT = "-silent"; //$NON-NLS-1$
	/**
	 * -V[ERSION]
	 * 
	 * Displays the current version and level number for SQL*Plus, and then
	 * returns control to the operating system.
	 */
	public static final String V = "-v"; //$NON-NLS-1$
	public static final String VERSION = "-version"; //$NON-NLS-1$
	/**
	 * -N[oHISTORY]
	 * 
	 * Kill the history for this session.
	 */
	public static final String N = "-n"; //$NON-NLS-1$
	public static final String NOHISTORY = "-nohistory"; //$NON-NLS-1$

	public static final String NOUPDATES = "-noupdates"; //$NON-NLS-1$

	/**
	 * LOGON OPTIONS
	 */

	/**
	 * /NOLOG
	 * 
	 * Establishes no initial connection to Oracle. Before issuing any SQL
	 * commands, you must issue a CONNECT command to establish a valid logon.
	 * Use /NOLOG when you want to have a SQL*Plus script prompt for the
	 * username, password, or database specification. The first line of this
	 * script is not assumed to contain a logon.
	 */
	public static final String NOLOG = "/nolog"; //$NON-NLS-1$
	/*
	 * SQLPlus + Options
	 */
	public static final String OCI = "-oci"; //$NON-NLS-1$
	public static final String THIN = "-thin"; //$NON-NLS-1$
	public static final String NOLOGINFILE = "-nologinfile"; //$NON-NLS-1$
	private static final String AT = "@"; //$NON-NLS-1$
	private static final Object AS = "as"; //$NON-NLS-1$
	private static final String[] ASROLES = { "sysdba", "sysbackup", "sysdg", "syskm", "sysasm", "sysoper" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
	private static final CharSequence SYSBACKUP = "sysbackup"; //$NON-NLS-1$

	// Hidden options
	public static final String XHIDEBANNERS = "-x"; //$NON-NLS-1$
	public static final String COMPATIBILITY = "-compatibility"; //$NON-NLS-1$
	public static final String NOLOGINTIME = "-nologintime"; //$NON-NLS-1$

	private static int atcount = 0;
	private static String connectString;
	private static String filename;
	private static String edition;
	private static ArrayList<String> variables = new ArrayList<String>();
	// Have we figured out if this is a sysdba connect? Lets start off with no
	private static String asRole = ""; //$NON-NLS-1$
	private static boolean assysbackup = false;
	/**
	 * SSH Options
	 */
	public static final String SSHHOST = "-sshhost"; //$NON-NLS-1$
	public static final String SSHKEY = "-sshkey"; //$NON-NLS-1$
	private static final String NLS_LANG = null;
	private static final String EDITION = "edition";
	/**
	 * Grab the args passed in and store them. We can process them when we need
	 * them.
	 * 
	 * @param cmdLineArgs
	 *            {@link String} array
	 */

	static String[] realOption = new String[] { SSHHOST, SSHKEY, OCI, THIN, NOLOGINFILE, NOLOG, VERSION, V, SILENT, S,
			RESTRICT, R, LOGGING, LOGON, L, HELP, H, COMPATIBILITY, XHIDEBANNERS };
	private static String badOption;
	private static boolean finishedOptions = false;
	private static String s_compatability;

	public static void setArgs(String[] cmdLineArgs) {
		StringBuilder sb = new StringBuilder();
		for (int index = 0; index < cmdLineArgs.length; index++) {
			String arg = cmdLineArgs[index] + " "; // Adding a //$NON-NLS-1$
													// space to store
													// these as original. We can
													// pop them back any time
			sb.append(arg);
		}
		commandlineArg = sb.toString();
	}

	public static String getArgsString() {
		return commandlineArg;
	}

	public static String[] getArgsArray() {
		StringTokenizer st = new StringTokenizer(commandlineArg);
		String[] args = new String[st.countTokens()];
		int index = 0;
		while (st.hasMoreElements()) {
			args[index] = st.nextToken();
		}
		return args;
	}

	public static void setAtCount(int i) {
		atcount = i;
	}

	public static int getAtCount() {
		return atcount;
	}

	public static void setConnectString(String connect) {
		if (connect != null && connect.startsWith("\"") //$NON-NLS-1$
				&& (!connect.endsWith("\"")) && connect.length() > 1) { //$NON-NLS-1$
			connectString = connect.substring(1);
		} else {
			connectString = connect;
		}

		String s = connectString.substring(0,
				connectString.indexOf('@') != -1 ? connectString.indexOf('@') : connectString.length());
		if (s.length() > s.replaceAll("\\/", "").length() + 1) { //$NON-NLS-1$ //$NON-NLS-2$
			String msg = Messages.getString("SQLCliOptions.4"); //$NON-NLS-1$
			setBadOption(MessageFormat.format(msg, connectString));
		}
	}

	public static String getConnectString() {
		return connectString;
	}

	public static String getFileName() {
		return filename;
	}

	public static void setFileArguments(ArrayList<String> args) {
		variables = args;
	}

	public static ArrayList<String> getFileArguments() {
		return variables;
	}

	/**
	 * Set the filename if it is set.
	 * 
	 * @param arg
	 */
	public static void setFileName(String arg) {
		// If there is still stuff here, then it must be a file so lets
		// check for @{url|file_name[.ext]} [arg ...]
		String aFileName = ""; //$NON-NLS-1$
		if (arg != null) {
			if (arg.indexOf(AT) == 0) // $NON-NLS-1$
				aFileName = arg.substring(1);
			else
				aFileName = arg;

			//
			//
			// if the passed in name doesn't exists tack on a .sql
			//
			File f = new File(aFileName);
			int lastAnySlash = aFileName.lastIndexOf("/");
			int lastBSlash = aFileName.lastIndexOf("\\");
			if (lastBSlash > lastAnySlash) {
				lastAnySlash = lastBSlash;
			}
			if (!f.exists() && (!(aFileName.lastIndexOf(".") > lastAnySlash))) { //$NON-NLS-1$
				aFileName = aFileName + ".sql"; //$NON-NLS-1$
			}

			// dupe?// no .sql check ~/.sql
			// if (!f.exists() && aFileName.indexOf(".") == -1) { //$NON-NLS-1$
			// aFileName = aFileName + ".sql"; //$NON-NLS-1$
			// }
		}
		filename = aFileName;
	}

	/**
	 * Check that this current argument is valid option
	 * 
	 * @param arguments
	 * @param index
	 * @return number of params to skip 0 not handled 1 skip 1 2 skip 2
	 *         (restrict)
	 */
	private static int processOption(String[] arguments, int index) {
		String currArg = arguments[index];
		String arg = currArg;
		int skip = 0;
		if (currArg.startsWith("@") || currArg.contains("@")) { //$NON-NLS-1$ //$NON-NLS-2$
			finishedOptions = true;
		}
		if (currArg != null) {
			switch (arg.toLowerCase()) {
			/*
			 * Logon means to try loggin in once, not three times
			 */
			case L:
				if (!options.contains(LOGON))
					options.add(LOGON);
				skip = 1;
				break;
			case LOGON:
				if (!options.contains(LOGON))
					options.add(LOGON);
				skip = 1;
				break;
			/*
			 * Print the help only. Dont logon or do any other guff
			 */
			case H:
				if (!options.contains(HELP))
					options.add(HELP);
				skip = 1;
				break;
			case HELP:
				if (!options.contains(HELP))
					options.add(HELP);
				skip = 1;
				break;
			case NOLOGINTIME:
				if (!options.contains(NOLOGINTIME))
					options.add(NOLOGINTIME);
				skip = 1;
				break;
			case NOUPDATES:
				if (!options.contains(NOUPDATES))
					options.add(NOUPDATES);
				skip = 1;
				break;
			/*
			 * Just print the verion number and no banners or anything.
			 */
			case V:
				if (!options.contains(VERSION))
					options.add(VERSION);
				skip = 1;
				break;
			case VERSION:
				if (!options.contains(VERSION))
					options.add(VERSION);
				skip = 1;
				break;
			/*
			 * Just print the verion number and no banners or anything.
			 */
			case N:
			case NOHISTORY:
				if (!options.contains(NOHISTORY))
					options.add(NOHISTORY);
				skip = 1;
				break;
			/*
			 * Shhhh. No banner, no prompt, no feedback
			 */
			case S:
				if (!options.contains(SILENT))
					options.add(SILENT);
				skip = 1;
				break;
			case SILENT:
				if (!options.contains(SILENT))
					options.add(SILENT);
				skip = 1;
				break;
			case XHIDEBANNERS:
				if (!options.contains(XHIDEBANNERS))
					options.add(XHIDEBANNERS);
				skip = 1;
				break;
			case COMPATIBILITY:
			case "-c": //$NON-NLS-1$
				if (!options.contains(COMPATIBILITY))
					options.add(COMPATIBILITY);
				skip = 1;
				if (index < arguments.length - 1) {
					// Check next as a version
					String input = arguments[index + 1];
					String regex = "^(\\d+\\.)?(\\d+\\.)?(\\*|\\d+)$"; //$NON-NLS-1$
					Pattern pattern = Pattern.compile(regex);
					Matcher matcher = pattern.matcher(input);
					boolean fail = false;
					if (matcher.matches()) {
						String[] bits = input.split("\\."); //$NON-NLS-1$
						if (bits.length == 2 || bits.length == 3) { // $NON-NLS-1$
							int majormin = Integer.parseInt(bits[0] + bits[1]);
							if (majormin < 73)
								fail = true;
							if (bits.length > 2) {
								String patch = bits[2];
								if (majormin == 73 && !patch.equals("*") && Integer.parseInt(patch) < 4)
									fail = true;
							}
							if (!fail)
								setCompatability(arguments[index + 1]);
							else {
								String msg = Messages.getString("SQLCliOptions.11"); //$NON-NLS-1$
								setBadOption(msg);
							}
						} else {
							String msg = "";
							if (bits.length == 1) {
								msg = Messages.getString("SQLCliOptions.13"); //$NON-NLS-1$
							} else {
								msg = Messages.getString("SQLCliOptions.11"); //$NON-NLS-1$
							}
							setBadOption(msg);
						}
					} else {
						String msg = Messages.getString("SQLCliOptions.12"); //$NON-NLS-1$
						setBadOption(MessageFormat.format(msg, arguments[index] + " " + input)); //$NON-NLS-1$
					}
					skip = 2;
				}
				break;
			/*
			 * REstricted operations. See table above for details.
			 */
			case R:

			case RESTRICT:
				/*
				 * In the case of restrict, we might be expecting an integer
				 * after
				 */
				if (!options.contains(RESTRICT))
					options.add(RESTRICT);
				skip = 1;
				if (index < arguments.length - 1) {
					try {
						int rlevel = Integer.parseInt(arguments[index + 1]);
						skip = 2;
						switch (rlevel) {
						case 1:
							if (!options.contains(R1))
								options.add(R1);
							break;
						case 2:
							if (!options.contains(R2))
								options.add(R2);
							break;
						case 3:
							if (!options.contains(R3))
								options.add(R3);
							break;
						case 4:
							if (!options.contains(R4))
								options.add(R4);
							break;

						default:
							// if -R value is other that 1,2,3 or 4 then need to
							// display the 'Bad Option -R' message
							String msg = Messages.getString("SQLCliOptions.14"); //$NON-NLS-1$
							setBadOption(MessageFormat.format(msg, arguments[index] + " " + arguments[index + 1])); //$NON-NLS-1$
							skip = -1;
							break;
						}

					} catch (NumberFormatException e) {
						// If this is not a number, move on, we dont care. I
						// dont anyway...
						String msg = Messages.getString("SQLCliOptions.16"); //$NON-NLS-1$
						setBadOption(MessageFormat.format(msg, arguments[index] + " " + arguments[index + 1])); //$NON-NLS-1$
					}
				}
				break;
			case OCI:
				if (!options.contains(OCI))
					options.add(OCI);
				skip = 1;
				break;
			case THIN:
				if (!options.contains(THIN))
					options.add(THIN);
				skip = 1;
				break;
			case NOLOGINFILE:
				if (!options.contains(NOLOGINFILE))
					options.add(NOLOGINFILE);
				skip = 1;
				break;
			case NOLOG:
				if (!options.contains(NOLOG))
					options.add(NOLOG);
				skip = 1;
				break;
			case LOGGING:
				if (!options.contains(LOGGING))
					options.add(LOGGING);
				skip = 1;
				break;
			case SSHHOST:
				if (!options.contains(SSHHOST)) {
					if (arguments.length > index) {
						options.add(SSHHOST);
						getProperties().put(SSHHOST, arguments[index + 1]);
					} else {
						// Need to tell someone the options is crapskis
					}
					skip = 2;
				}
				break;
			case SSHKEY:
				if (!options.contains(SSHKEY)) {
					if (arguments.length > index) {
						options.add(SSHKEY);
						getProperties().put(SSHKEY, arguments[index + 1]);
					} else {
						// Need to tell someone the options is crapskis
					}
					skip = 2;
				}
				break;
			default:
				/*
				 * If we dont recognise it, do nothing with it.
				 */
				if (finishedOptions) {
					skip = -2;
				} else {
					skip = -1;
				}
				break;
			}
		}
		return skip;
	}

	private static void setCompatability(String string) {
		s_compatability = string;
	}

	public static String getCompatability() {
		return s_compatability;
	}

	public static boolean processOptions(String[] arguments) {
		// In procssing options, because its easy to know whats in the
		String msg = Messages.getString("SQLCliOptions.18"); //$NON-NLS-1$
		ArrayList<String> list = new ArrayList<String>(Arrays.asList(arguments));
		int index = 0;
		for (index = 0; index < arguments.length; index++) {
			// REgister and option and remove it from the list.
			int skip = processOption(arguments, index);
			if (skip == -1) {
				if (arguments[index].startsWith("-") //$NON-NLS-1$
						|| (arguments[index].startsWith("/") && (!(arguments[index].trim().equals("/") || //$NON-NLS-1$ //$NON-NLS-2$
								(arguments[index].trim().startsWith("/") && arguments[index] //$NON-NLS-1$
										.trim().contains("@")))))) //$NON-NLS-1$
					setBadOption(MessageFormat.format(msg, arguments[index]));
			}
			for (int i = 0; i < skip && list.size() > 0; i++) {
				list.remove(0);
			}
			if (skip > 1)
				index = index + skip - 1; // We need to skip the number here in
											// the loop to stop going around.
		}

		// At this point all the options for SQL*Plus are removed from the list
		// so the next thing should be the logon options
		if (!list.isEmpty()) { // this means that this is either logon or files
			if (!list.get(0).startsWith(AT)) {
				SQLCliOptions.setConnectString(list.get(0));
				boolean usedtwo=false;
				if (list.size()>=2) {
					String both=list.get(1).toLowerCase(Locale.US).trim().replaceAll("\\s+", " ");  //$NON-NLS-1$  //$NON-NLS-2$
					if (list.size()>=3) {
						if (both.equals(AS)) {
							if (list.get(2) != null) {
								for (String role : ASROLES) {
									if (list.get(2).trim().equalsIgnoreCase(role)) {
										both+=" "+list.get(2).trim();  //$NON-NLS-1$
										usedtwo=true;
									}
								}
							}						
						}
					}
					String theRole=null;
					if (both.toLowerCase(Locale.US).startsWith("as ")) { //$NON-NLS-1$
						String[] two=both.split(" "); //$NON-NLS-1$
						if (two.length==2) {
							theRole=two[1];
						}
					}
					if (theRole!=null) {
						for (String role : ASROLES) {
							if (theRole.equalsIgnoreCase(role)) {
								SQLCliOptions.setAsRole(theRole);
							}
						}
						if (theRole.toLowerCase(Locale.US).contains(SYSBACKUP)) {
							SQLCliOptions.setAsSysBackup(true);
						}
					}
				}
				list.remove(0); // removes connect string
				if (SQLCliOptions.getAsRole() != null && (!SQLCliOptions.getAsRole().equals(""))) { // remove //$NON-NLS-1$
																									// as
																									// sysdba
																									// string
					list.remove(0);
					if (usedtwo) {
						list.remove(0);
					}
				}
			}
		}
		if (!list.isEmpty()) {
			if (list.get(0).toLowerCase().startsWith(EDITION)) {
				SQLCliOptions.setEdition(list);
			}
		}
		// If there is still stuff here, then it must be a file so lets
		// check for @{url|file_name[.ext]} [arg ...]
		if (!list.isEmpty()) {
			if (list.get(0).startsWith(AT) && list.get(0).length() > 1) {
				SQLCliOptions.setFileName(list.get(0));
				list.remove(0);
			} else if (list.get(0).startsWith(AT) && list.get(0).length() == 1) {
				// first is @ on its own and next item has to be filename
				list.remove(0);
				if (!list.isEmpty()) {
					SQLCliOptions.setFileName(list.get(0));
					list.remove(0);
				}
			} else {
				msg = Messages.getString("SQLCliOptions.24"); //$NON-NLS-1$
				setBadOption(MessageFormat.format(msg, list.get(0)));
				list.remove(0);
			}
		}
		if (!list.isEmpty()) {
			// Then we have just a file arg values to go in, what ever is left!
			SQLCliOptions.setFileArguments(list);
		}
		// If this is on and we set compat, then remove if there
		if (s_compatability != null)
			if (options.contains(NOLOGINTIME))
				options.remove(NOLOGINTIME);
		return true;
	}

	private static void setEdition(ArrayList<String> list) {
		// we're looking for "edition=<identifier>
		if (list.get(0).indexOf("=") != -1) {
			// Ok, there is an = sign in here
			edition = list.get(0).split("=")[1];
			list.remove(0);
		} else if (list.size() > 1 && list.get(1).indexOf("=") != 1) {
			edition = list.get(1).substring(1);
			list.remove(0);
			list.remove(0);
		} else {
			if (list.size() > 2 && list.get(1).equals("="))
				edition = list.get(2);
			list.remove(0);
			list.remove(0);
			list.remove(0);
		}
	}

	public static void main(String[] args) {
		String argsString = "-V -SILENT -oci -nologinfile \"repo/repo@orcl as sysdba\""; //$NON-NLS-1$
		String[] arg = argsProcess(argsString);
		if (!SQLCliOptions.processOptions(arg)) {

		}
		printStuff();
		options.removeAll(options);
		SQLCliOptions.setFileName(null);
		SQLCliOptions.getFileArguments().removeAll(SQLCliOptions.getFileArguments());
		SQLCliOptions.setAsRole(""); //$NON-NLS-1$

		argsString = "-oci -nologinfile repo@orcl @file one two three"; //$NON-NLS-1$
		arg = argsProcess(argsString);
		SQLCliOptions.processOptions(arg);
		printStuff();
		options.removeAll(options);
		SQLCliOptions.setFileName(null);
		SQLCliOptions.getFileArguments().removeAll(SQLCliOptions.getFileArguments());
		SQLCliOptions.setAsRole(""); //$NON-NLS-1$
		argsString = "repo/repo@localhost:1521/orcl @t.sql"; //$NON-NLS-1$
		arg = argsProcess(argsString);
		SQLCliOptions.processOptions(arg);
		printStuff();
		options.removeAll(options);
		SQLCliOptions.setFileName(null);
		SQLCliOptions.getFileArguments().removeAll(SQLCliOptions.getFileArguments());
		SQLCliOptions.setAsRole(""); //$NON-NLS-1$
		argsString = "repo/repo@localhost:1521/orcl fakeit.sql"; //$NON-NLS-1$
		arg = argsProcess(argsString);
		SQLCliOptions.processOptions(arg);
		printStuff();

	}

	public static void printStuff() {
		Iterator<String> it = options.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
		System.out.println("ConnectString:" + SQLCliOptions.getConnectString()); //$NON-NLS-1$
		System.out.println("sysdba:" //$NON-NLS-1$
				+ ((SQLCliOptions.getAsRole() != null && SQLCliOptions.getAsRole().equalsIgnoreCase("sysdba")) ? "true" //$NON-NLS-1$ //$NON-NLS-2$
						: "false")); //$NON-NLS-1$
		System.out.println("Filename:" + SQLCliOptions.getFileName()); //$NON-NLS-1$
		System.out.println("Filename args:"); //$NON-NLS-1$
		it = SQLCliOptions.getFileArguments().iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}

	}

	/**
	 * Simple convert string to array
	 * 
	 * @param stringArray
	 * @return
	 */
	private static String[] argsProcess(String stringArray) {
		StringTokenizer st = new StringTokenizer(stringArray);
		String[] array = new String[st.countTokens()];
		int index = 0;
		while (st.hasMoreElements()) {
			array[index] = st.nextToken();
			index++;
		}
		return array;
	}

	/**
	 * Are these optionsrole specified
	 * 
	 * @return sysdba {@link Boolean}
	 */
	public static String getAsRole() {
		return asRole;
	}

	/**
	 * Are these options sysbackup
	 * 
	 * @return sysbackup {@link Boolean}
	 */
	public static boolean isAsSysBackup() {
		return assysbackup;
	}

	/**
	 * If we find role, lets set the options.
	 * 
	 * @param role
	 */
	public static void setAsRole(String role) {
		asRole = role;

	}

	/**
	 * If we find sysbackup, lets set the options.
	 * 
	 * @param amISysdba
	 */
	public static void setAsSysBackup(boolean amISysBackup) {
		assysbackup = amISysBackup;

	}

	/**
	 * Push the options into script runner context so we can use it while
	 * running
	 * 
	 * @param {@link
	 * 			ScriptRunnerContext context} to put the options into
	 * @return the {@link ScriptRunnerContext}
	 */
	public static ScriptRunnerContext populateContextWithOptions(ScriptRunnerContext context) {
		if (options.contains(NOLOGINTIME))
			context.putProperty(ScriptRunnerContext.LASTLOGINTIME, false);
		if (options.contains(OCI))
			context.putProperty(ScriptRunnerContext.DBCONFIG_USE_THICK_DRIVER, true);
		if (options.contains(THIN))
			context.putProperty(ScriptRunnerContext.DBCONFIG_USE_THICK_DRIVER, false);
		if (options.contains(NOLOGINFILE))
			context.putProperty(ScriptRunnerContext.DBCONFIG_GLOGIN, false);
		if (options.contains(LOGGING))
			context.putProperty(ScriptRunnerContext.OPTLFLAG, true);
		if (options.contains(NOLOG))
			context.putProperty(ScriptRunnerContext.NOLOG, true);
		if (options.contains(R1)){
		  context.setRestrictedLevel(RunnerRestrictedLevel.Level.R1);
		}
		if (options.contains(R2)){
		  context.setRestrictedLevel(RunnerRestrictedLevel.Level.R2);
		}
		if (options.contains(R3)){
		  context.setRestrictedLevel(RunnerRestrictedLevel.Level.R3);
		}
		if (options.contains(R4)){
		  context.setRestrictedLevel(RunnerRestrictedLevel.Level.R4);
		}
		if (options.contains(SILENT) || options.contains(S)) {
			context.putProperty(ScriptRunnerContext.SILENT, true);
		}
		if (options.contains(NOHISTORY))
			context.putProperty(ScriptRunnerContext.NOHISTORY, Boolean.TRUE);
		if (options.contains(XHIDEBANNERS)) {
			context.putProperty(ScriptRunnerContext.HIDDENOPTIONX, true);
		}
		if (options.contains(COMPATIBILITY)) {
			context.putProperty(ScriptRunnerContext.HIDDENOPTIONC, true);
			context.putProperty(ScriptRunnerContext.SET_SQLPLUSCOMPATIBILITY, s_compatability);
		}
		if (options.contains(LOGON)) {
			context.putProperty(ScriptRunnerContext.LOGON, true);
		}
		// If we are in an ORACLE_HOME and the jar actually exists, then switch
		// us to OCI by default
		if (JDBCHelper.doesOHJDBCExist()) {
			context.putProperty(ScriptRunnerContext.DBCONFIG_USE_THICK_DRIVER, true);
		}
		processProxy(context);
		processNLSLANG(context);
		return context;
	}

	private static void processNLSLANG(ScriptRunnerContext context) {
		// Check NLS_LANG. If it is set, then we need to set the parameters
		// when we make a connection. Lets mark the context so we know what to
		// do.
		if (NLSLANGParser.isNLSLangSet()) {
			if (NLSLANGParser.isNLSLangValid()) {
				context.putProperty(NLS_LANG, Boolean.TRUE);
			} else {
				context.putProperty(NLS_LANG, Boolean.FALSE);
				Logger.info(SQLCliOptions.class, "NLS_LANG is set, but not valid. Ignoring.\n");
			}
		}

	}

	private static ScriptRunnerContext processProxy(ScriptRunnerContext context) {
		Map<String, String> a = System.getenv();

		if ((System.getenv("HTTP_PROXY") != null) || (System.getenv("http_proxy") != null)) { //$NON-NLS-1$ //$NON-NLS-2$
			String value = System.getenv("HTTP_PROXY"); //$NON-NLS-1$
			if (value != null) {
				processValue(value, context);
			} else {
				value = System.getenv("http_proxy"); //$NON-NLS-1$
				if (value != null) {
					processValue(value, context);
				}
			}
			;
		}
		if ((System.getenv("HTTPS_PROXY") != null) || (System.getenv("https_proxy") != null)) { //$NON-NLS-1$ //$NON-NLS-2$
			String value = System.getenv("HTTPS_PROXY"); //$NON-NLS-1$
			if (value != null) {
				processValue(value, context);
			} else {
				value = System.getenv("https_proxy"); //$NON-NLS-1$
				if (value != null) {
					processValue(value, context);
				}
			}
			;
		}
		return context;
	}

	private static void processValue(String value, ScriptRunnerContext context) {
		value = value.replaceFirst("http://|https://", ""); //$NON-NLS-1$ //$NON-NLS-2$
		if (value.split(":").length == 2) { //$NON-NLS-1$
			context.putProperty(ScriptRunnerContext.HTTP_PROXY, true);
			context.putProperty(ScriptRunnerContext.HTTP_PROXY_HOST, value.split(":")[0]); //$NON-NLS-1$
			context.putProperty(ScriptRunnerContext.HTTP_PROXY_PORT, value.split(":")[1]); //$NON-NLS-1$
		}
	}

	/**
	 * Check to see if an option is used
	 * 
	 * @param option
	 *            to check {@link SQLCliOptions}
	 * @return
	 */
	public static boolean isOptionUsed(String option) {
		if (options.contains(option)) {
			return true;
		} else {
			return false;
		}
	}

	public static Properties getProperties() {
		return properties;
	}

	/**
	 * @return the badOption
	 */
	public static boolean isBadOption() {
		return badOption != null;
	}

	public static String getBadOption() {
		return badOption;
	}

	/**
	 * @param badOption
	 *            the badOption to set
	 */
	public static void setBadOption(String badOption) {
		SQLCliOptions.badOption = badOption;
	}

	private static boolean containsFile(String inFile) {
		// can sometimes be .*file:
		boolean retVal = false;
		if (inFile.indexOf(":") > 1) { //$NON-NLS-1$
			String stub = inFile.substring(0, inFile.indexOf(":")); //$NON-NLS-1$
			if (stub.endsWith("file") && (stub.indexOf("/") == -1) && (stub.indexOf("\\") == -1)) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				retVal = true;
			}
		}
		return retVal;
	}

	/**
	 * try \ in case its a microsoft file:\\
	 * 
	 * @param base
	 * @return
	 */
	static boolean haveIBytesRaw(String base) {
		if (startsWithHttpOrFtp(base) == false) {
			return false;
		}
		InputStream is = null;
		URLConnection c = null;
		try {
			String lower = base.toLowerCase();
			if (containsFile(lower)) { // $NON-NLS-1$
				base = base.substring(lower.indexOf("file:")); //$NON-NLS-1$
			}
			if (base.indexOf("\\") != -1) { //$NON-NLS-1$
				base.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
				c = new URL(base).openConnection(); // $NON-NLS-1$ //$NON-NLS-2$

				is = c.getInputStream();
				if (is.read() != -1) {
					return true;
				}
			}
		} catch (MalformedURLException e1) {
			return false;
		} catch (IOException e1) {
			return false;
		} catch (Exception e2) {// was getting illegal argument exception on
								// !"XYZ$% file input
			return false;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					// ignore.
				}
			}
		}
		return false;
	}

	public static boolean haveIBytes(String base) {
		if (startsWithHttpOrFtp(base) == false) {
			return false;
		}
		InputStream is = null;
		URLConnection c = null;
		try {
			String lower = base.toLowerCase();
			if (containsFile(lower)) { // $NON-NLS-1$
				base = base.substring(lower.indexOf("file:")); //$NON-NLS-1$
			}
			c = new URL(base.replaceAll("\\\\", "/")).openConnection(); //$NON-NLS-1$ //$NON-NLS-2$
			is = c.getInputStream();
			if (is.read() != -1) {
				return true;
			}
		} catch (MalformedURLException e1) {
			return false;
		} catch (IOException e1) {
			return false;
		} catch (Exception e2) {// was getting illegal argument exception on
								// !"XYZ$% file input
			return false;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					// ignore.
				}
			}
		}
		return false;
	}

	public static boolean startsWithHttpOrFtp(String base) {
		if (base == null) {
			return false;
		}
		String lower = base.toLowerCase();
		if (!((lower.startsWith("http://") //$NON-NLS-1$
				|| lower.startsWith("http:\\\\") //$NON-NLS-1$
				|| lower.startsWith("https://") //$NON-NLS-1$
				|| lower.startsWith("https:\\\\") //$NON-NLS-1$
				|| containsFile(lower) || lower.startsWith("ftp:\\\\") //$NON-NLS-1$
				|| lower.startsWith("ftp://")))) { //$NON-NLS-1$
			return false;
		}
		return true;
	}

	public static String getBaseHistoryBlacklist() {
		String list = "show,history,connect,set";
		return list;
	}

	/**
	 * @return the edition
	 */
	public static String getEdition() {
		return edition;
	}

	/**
	 * @param edition
	 *            the edition to set
	 */
	public static void setEdition(String edition) {
		SQLCliOptions.edition = edition;
	}
}
