/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;

import jline.console.UserInterruptException;
import jline.console.completer.ArgumentCompleter;
import jline.console.completer.Completer;
import oracle.dbtools.app.SqlclCompleter;
import oracle.dbtools.common.utils.StringUtils;
import oracle.dbtools.db.ResultSetFormatter;
import oracle.dbtools.parser.plsql.ParsedSql;
import oracle.dbtools.parser.plsql.ParsedSql.Language;
import oracle.dbtools.raptor.console.ConsoleListener;
import oracle.dbtools.raptor.console.FileNameCompleter;
import oracle.dbtools.raptor.console.MultiLineHistory;
import oracle.dbtools.raptor.console.SQLPlusConsoleReader;
import oracle.dbtools.raptor.console.clone.DbtoolsNonBlockingInputStream;
import oracle.dbtools.raptor.console.clone.IAcceptTypeHandler;
import oracle.dbtools.raptor.format.ANSIConsoleFormatter;
import oracle.dbtools.raptor.format.FormatRegistry;
import oracle.dbtools.raptor.newscriptrunner.CommandRegistry;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.raptor.newscriptrunner.SQLPLUS;
import oracle.dbtools.raptor.newscriptrunner.SQLPlusProviderForSQLPATH;
import oracle.dbtools.raptor.newscriptrunner.ScriptExecutor;
import oracle.dbtools.raptor.newscriptrunner.ScriptParser;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SqlParserProvider;
import oracle.dbtools.raptor.newscriptrunner.commands.ApexCmd;
import oracle.dbtools.raptor.newscriptrunner.commands.Info;
import oracle.dbtools.raptor.newscriptrunner.commands.SetAppinfo;
import oracle.dbtools.raptor.newscriptrunner.commands.SetExitCommit;
import oracle.dbtools.raptor.scriptrunner.commands.SetWallet;
import oracle.dbtools.raptor.newscriptrunner.commands.alias.Aliases;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowLogin;
import oracle.dbtools.raptor.scriptrunner.cmdline.editor.Buffer;
import oracle.dbtools.raptor.scriptrunner.cmdline.editor.EditCommand;
import oracle.dbtools.raptor.scriptrunner.cmdline.editor.Messages;
import oracle.dbtools.raptor.scriptrunner.cmdline.editor.SetColorCommand;
import oracle.dbtools.raptor.scriptrunner.commands.HelpCmd;
import oracle.dbtools.raptor.scriptrunner.commands.HistoryCommand;
import oracle.dbtools.raptor.scriptrunner.commands.InfoCmd;
import oracle.dbtools.raptor.scriptrunner.commands.NLSLANGListener;
import oracle.dbtools.raptor.scriptrunner.commands.OraDebug;
import oracle.dbtools.raptor.scriptrunner.commands.PromptCmd;
import oracle.dbtools.raptor.scriptrunner.commands.RepeatSQLBufferCmd;
import oracle.dbtools.raptor.scriptrunner.commands.SODACommand;
import oracle.dbtools.raptor.scriptrunner.commands.SODACmd;
import oracle.dbtools.raptor.scriptrunner.commands.ScriptCommand;
import oracle.dbtools.raptor.scriptrunner.commands.Set121LongIdentifier;
import oracle.dbtools.raptor.scriptrunner.commands.SetClassicMode;
import oracle.dbtools.raptor.scriptrunner.commands.SetClearScreen;
import oracle.dbtools.raptor.scriptrunner.commands.SetCloudConfig;
import oracle.dbtools.raptor.scriptrunner.commands.SetEditFile;
import oracle.dbtools.raptor.scriptrunner.commands.SetEmbedded;
import oracle.dbtools.raptor.scriptrunner.commands.SetEncoding;
import oracle.dbtools.raptor.scriptrunner.commands.SetHistory;
import oracle.dbtools.raptor.scriptrunner.commands.SetSQLPlusCompatability;
import oracle.dbtools.raptor.scriptrunner.commands.SetSQLPrompt;
import oracle.dbtools.raptor.scriptrunner.commands.SetTime;
import oracle.dbtools.raptor.scriptrunner.commands.ShowVersion;
import oracle.dbtools.raptor.scriptrunner.commands.Shutdown;
import oracle.dbtools.raptor.scriptrunner.commands.SshTunnelCommand;
import oracle.dbtools.raptor.scriptrunner.commands.Startup;
import oracle.dbtools.raptor.scriptrunner.commands.editor.AppendCommand;
import oracle.dbtools.raptor.scriptrunner.commands.editor.ChangeCommand;
import oracle.dbtools.raptor.scriptrunner.commands.editor.DelCommand;
import oracle.dbtools.raptor.scriptrunner.commands.editor.GetCommand;
import oracle.dbtools.raptor.scriptrunner.commands.editor.InputCommand;
import oracle.dbtools.raptor.scriptrunner.commands.editor.ListCommand;
import oracle.dbtools.raptor.scriptrunner.commands.editor.Pwd;
import oracle.dbtools.raptor.scriptrunner.commands.editor.SaveCommand;
import oracle.dbtools.raptor.scriptrunner.commands.editor.SetSuffix;
import oracle.dbtools.raptor.scriptrunner.commands.editor.StoreCommand;
import oracle.dbtools.raptor.scriptrunner.commands.rest.RESTCommand;
import oracle.dbtools.raptor.utils.AnsiColorListPrinter;
import oracle.dbtools.raptor.utils.BasicExceptionHandler;
import oracle.dbtools.raptor.utils.ExceptionHandler;
import oracle.dbtools.raptor.utils.TNSHelper;
import oracle.dbtools.versions.CheckForUpdates;
import oracle.dbtools.versions.SQLclVersion;
import static org.fusesource.jansi.internal.CLibrary.STDIN_FILENO;
import static org.fusesource.jansi.internal.CLibrary.isatty;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SqlCli.java"
 *         >Barry McGillin</a>
 * @author klrice
 * 
 *         Command line to run: java -Djava.net.preferIPv4Stack=true -classpath
 *         \raptor_trunk\ide\sqldeveloper\lib\oracle.sqldeveloper.utils-nodeps.
 *         jar;
 *         \raptor_trunk\ide\sqldeveloper\lib\jline-2.10.jar;\raptor_trunk\ide
 *         \jdbc\lib\ojdbc6.jar;\raptor_trunk\ide\rdbms\jlib\xdb6.jar
 *         oracle.dbtools.raptor.scriptrunner.utils.SqlCli
 *         hr/hr@gbr30060.uk.oracle.com:1521:DB12GR1N
 */
public class SqlCli implements ConsoleListener {
	public static String productName = "SQLcl"; //$NON-NLS-1$
	private Connection _conn;
	private ScriptExecutor _runner;
	private ScriptRunnerContext _ctx;
	private SQLPlusConsoleReader reader;
	private Buffer _buffer;
	private boolean processed = false;
	boolean RUNNING = false;
	private static boolean logging = false;
	private static boolean _isCmdLine = false;
	private static boolean isPromptBold = false;
	private ExitThread _exitThread = null;
	private String prompt = "SQL>"; //$NON-NLS-1$

	public static boolean isCmdLine() {
		return _isCmdLine;
	}

	/**
	 * @return the reader
	 */
	public SQLPlusConsoleReader getReader() {
		return reader;
	}

	/**
	 * @return the _ctx
	 */
	protected ScriptRunnerContext getScriptRunnerContext() {
		return _ctx;
	}

	public SqlCli() throws Exception {
		ExceptionHandler.setHandler(new BasicExceptionHandler());
		// setting line terminator here for now this is for /*csv*/ and etc.,
		// used in the Query
		FormatRegistry.setLineTerminator(System.getProperty("line.separator")); //$NON-NLS-1$
		SQLCliHelper.setDefaultScriptRunnerProperies();
		ResultSetFormatter.setMaxLines(Integer.MAX_VALUE); // Do not stop at 5k
															// default.Set
															// unlimited, or in
															// this case Integer
															// MAX
		CommandRegistry.addListener(SetEditFile.class, StmtSubType.G_S_SET_EDITFILE);
		CommandRegistry.addListener(HistoryCommand.class, StmtSubType.G_S_HISTORY);
		CommandRegistry.addListener(GetCommand.class, StmtSubType.G_S_GET);
		CommandRegistry.addListener(SaveCommand.class, StmtSubType.G_S_SAVE);
		CommandRegistry.addListener(StoreCommand.class, StmtSubType.G_S_STORE);
		CommandRegistry.addListener(ListCommand.class, StmtSubType.G_S_LIST);
		CommandRegistry.addListener(AppendCommand.class, StmtSubType.G_S_APPEND);
		CommandRegistry.addListener(ChangeCommand.class, StmtSubType.G_S_CHANGE);
		CommandRegistry.addListener(DelCommand.class, StmtSubType.G_S_DEL_PLUS);
		CommandRegistry.addListener(SetEmbedded.class, StmtSubType.G_S_SET_EMBEDDED);
		CommandRegistry.addListener(SetSuffix.class, StmtSubType.G_S_SET_SUFFIX);
		CommandRegistry.addListener(SetTime.class, StmtSubType.G_S_SET_TIME);
		CommandRegistry.addListener(SetClearScreen.class, StmtSubType.G_S_SET_CLEARSCREEN);
		CommandRegistry.addListener(SetEncoding.class, StmtSubType.G_S_SET_ENCODING);
		CommandRegistry.addListener(EditCommand.class, StmtSubType.G_S_EDIT);
		CommandRegistry.addListener(InputCommand.class, StmtSubType.G_S_INPUT);
		CommandRegistry.addListener(SshTunnelCommand.class, StmtSubType.G_S_SSHTUNNEL);
		CommandRegistry.addListener(Startup.class, StmtSubType.G_S_STARTUP);
		CommandRegistry.addListener(Shutdown.class, StmtSubType.G_S_SHUTDOWN);
		CommandRegistry.addListener(ScriptCommand.class, StmtSubType.G_S_SCRIPT);
		CommandRegistry.addListener(SetColorCommand.class, StmtSubType.G_S_SET_UNKNOWN);
		CommandRegistry.addListener(SetSQLPlusCompatability.class, StmtSubType.G_S_SET_SQLCOMPATABILITY);
		CommandRegistry.addListener(SetHistory.class, StmtSubType.G_S_SET_HISTORY);
		CommandRegistry.addListener(OraDebug.class, StmtSubType.G_S_ORADEBUG);
		CommandRegistry.addForAllStmtsListener(SetSQLPrompt.class, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
		CommandRegistry.addForAllStmtsListener(RepeatSQLBufferCmd.class, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
		CommandRegistry.addForAllStmtsListener(RESTCommand.class, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
		CommandRegistry.addForAllStmtsListener(Pwd.class, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
		CommandRegistry.addListener(SetExitCommit.class, StmtSubType.G_S_SET_EXITCOMMIT);
		CommandRegistry.addListener(SetClassicMode.class, StmtSubType.G_S_SET_SQLPLUS_CLASSIC_MODE);
		CommandRegistry.addListener(Set121LongIdentifier.class, StmtSubType.G_S_SET_IGNORE_SETTING_MODE);
		CommandRegistry.addListener(ShowVersion.class, StmtSubType.G_S_VERSION);
		CommandRegistry.addForAllStmtsListener(NLSLANGListener.class, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
		CommandRegistry.addForAllStmtsListener(SetWallet.class, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);
		CommandRegistry.addForAllStmtsListener(SetCloudConfig.class, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);

		setupJLineInCommon();
		SQLPLUS.setProductName(productName);
		startConsole();

	}

	private void setupJLineInCommon() {
		CommandRegistry.addListener(HelpCmd.class, StmtSubType.G_S_HELP);// override default command
		CommandRegistry.removeListener(StmtSubType.G_S_PROMPT);// get rid of the default PROMPT command
		CommandRegistry.addListener(PromptCmd.class, StmtSubType.G_S_PROMPT);// override default command
		CommandRegistry.addForAllStmtsListener(InfoCmd.class, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);// override the
																										// default
																										// command
		CommandRegistry.addForAllStmtsListener(SODACmd.class, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);// override
																										// default
																										// command
		CommandRegistry.addForAllStmtsListener(SODACommand.class, StmtSubType.G_S_FORALLSTMTS_STMTSUBTYPE);

		ANSIConsoleFormatter.setListPrinter(new AnsiColorListPrinter());
		ApexCmd.setListPrinter(new AnsiColorListPrinter());
		Info.setListPrinter(new AnsiColorListPrinter());
	}

	/**
	 * @throws IOException
	 */
	private void startConsole() throws IOException {
		reader = new SQLPlusConsoleReader();
		reader.setBellEnabled(false);
		// Set the cursor at the bottom of the screen. We need this to manage
		// usage. we can get size from the terminal, but we never know where we
		// are!!!
		// This was making printout look weird. Commenting out for now.
		// System.out.print(Ansi.ansi().cursor(reader.getTerminal().getHeight(),0).toString());
		// System.out.flush();
		reader.setAcceptHandler(new IAcceptTypeHandler() {

			@Override
			public String getSQLType(String input) {
				if (input.length() > 0) {
					Language type = ParsedSql.partialRecognize(input);
					return type.toString();
				}

				return "unknown"; //$NON-NLS-1$
			}
		});
		_buffer = new Buffer();
		reader.setMultilineBuffer(_buffer);
		// push sqlplus history and buffer into the reader so we can do
		// multiline
		reader.setHistory(MultiLineHistory.getInstance());
		MultiLineHistory.getInstance().setTermWidth(reader.getWidth());
		MultiLineHistory.getInstance().setBaseBlackList(SQLCliOptions.getBaseHistoryBlacklist());
		MultiLineHistory.getInstance().setBlackList(SQLCliOptions.getBaseHistoryBlacklist());
		ScriptExecutor.setHistory(MultiLineHistory.getInstance());
		reader.setExpandEvents(false);
	}

	public ScriptRunnerContext processArgs(String[] args) throws Exception {
		// This is the context we'll use to populate the args on what we'll do.
		ScriptRunnerContext context = SQLCliHelper.getCliScriptRunnerContext(reader);
		setLoggingState(context);
		SQLCliOptions.setArgs(args);
		SQLCliOptions.processOptions(args); // Grab all the options from the
											// arguments passed in
		context = SQLCliOptions.populateContextWithOptions(context);
		context.setSQLPlusBuffer(_buffer);
		context.putProperty(ScriptRunnerContext.CLEARSCREEN, "top"); //$NON-NLS-1$
		context.setOutputStreamWrapper(new BufferedOutputStream(System.out));
		// Adding this for rdbms qa which is losing login.sql in places. This
		// will force classic mode with an env variable
		// which we will set in sql script for qa only.
		if (System.getenv("SQLPLUS_CLASSIC") != null) { //$NON-NLS-1$
			context.putProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE, Boolean.TRUE);
		}

		return context;
	}

	/**
	 * banner SqlCli
	 */
	private void banner() {
		String version = SQLclVersion.getSQLclShortVersion();

		String banner = Messages.getString("SqlCli.18"); //$NON-NLS-1$
		banner = MessageFormat.format(banner, new Object[] { SqlCli.productName, version, SQLclVersion.getSQLclState(),
				SQLCliHelper.getBannerTime() });
		getScriptRunnerContext().write("\n" + banner + "\n");//$NON-NLS-1$ //$NON-NLS-2$
		banner = Messages.getString("SqlCli.19"); //$NON-NLS-1$
		banner = MessageFormat.format(banner, new Object[] { SQLCliHelper.getBannerYear() });
		getScriptRunnerContext().write("\n" + banner + "\n"); // (authorized) //$NON-NLS-1$ //$NON-NLS-2$
		try {
			getScriptRunnerContext().getOutputStream().flush();
		} catch (IOException e) {
		}
	}

	private Boolean process(String line) {
		try {// After we have decided to do something, CR before any results.
			if (reader.justDidHistory()) {
				reader.println();
			}
			reader.flush();
		} catch (IOException e) {
			/* Eat me */}
		line = line.replaceAll("\\xA0+", ""); // Remove //$NON-NLS-1$ //$NON-NLS-2$
												// nbsp from string - normally
												// pasting from html
		if (_runner == null) {
			_runner = new ScriptExecutor(_ctx.getCurrentConnection());
			_runner.setScriptRunnerContext(_ctx);
		}
		if (_ctx.getCurrentConnection() != null) {
			// current connection could have been changed by connect

			_runner.setConn(_ctx.getCurrentConnection());
		}
		_buffer.stopEditing();
		_runner.setInterrupted(false);// reset flag, as we reuse the runner.
		_runner.setStmt(line);

		Iterator<ISQLCommand> parser = SqlParserProvider.getScriptParserIterator(getScriptRunnerContext(),
				new StringReader(line));
		while (parser.hasNext()) {
			ISQLCommand cmd = parser.next();
			if (cmd.getStmtType() == StmtType.G_C_SQLPLUS && cmd.getStmtSubType() == StmtSubType.G_S_ACCEPT) {
				_exitThread.stopListening();
			} else {
				_exitThread.listen(); // only bother listening for CTRL C while
										// this is running
			}

			if (cmd.getStmtType().equals(StmtType.G_C_PLSQL) || cmd.getStmtType().equals(StmtType.G_C_SQL)) {
				/*
				 * If we are SQL or PLSQL we want to be able to edit stuff like SQL*Plus. so, we
				 * maintain a buffer safe for that. We also need to add this to the history if,
				 * that is what we were running. However, we dont want to add these sqlplus
				 * editing commands to the buffer safe
				 */
				if (!(cmd.getStmtSubType().equals(StmtSubType.G_S_RUN)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_SLASH)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_DEL_PLUS)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_CHANGE)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_SAVE)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_STORE)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_GET)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_LIST)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_APPEND)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_EDIT)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_FORMAT)
						|| cmd.getStmtSubType().equals(StmtSubType.G_S_INSERT))
						|| cmd.getSql().toLowerCase().startsWith("repeat")) { //$NON-NLS-1$
					reader.getMultilineBuffer().setBufferSafe(reader.getMultilineBuffer().getBufferList());
				}

			} else if (cmd.getStmtType().equals(StmtType.G_C_SQLPLUS)) {
				// do not store SQLPLUS commands in the buffersafe letting / use
				// previous sql and PLSQL commands
				// change recognise sqlplus (newline terminated) commands
				/*
				 * If we are SQL or PLSQL we want to be able to edit stuff like SQL*Plus. so, we
				 * maintain a buffer safe for that. We also need to add this to the history if,
				 * that is what we were running. However, we dont want to add these sqlplus
				 * editing commands to the buffer safe
				 */
			}

			/*
			 * We've processed the line. We can now clear the current working buffer.
			 */
			reader.getMultilineBuffer().clear();
			/*
			 * After processing anything, we need to reset this as this manages the redraw
			 * for editing or history
			 */
			reader.maxBufferDrawn = 0;
		}
		_runner.run(); // run after we know the types of statements, allows us
						// to turn on/off exit thread.

		_exitThread.stopListening(); // stop listening, but this exitThread can
										// stay around until the next time we
										// want it to listen
		return true;
	}

	private void processLine(final String line) {
		reader.resetEditor();
		process(line);
		prompt = getPrompt(isPromptBold);
		reader.setBasePrompt(prompt);
		reader.resetRedraw(line);
	}

	public void usage() {
		getScriptRunnerContext().write(MessageFormat.format(Messages.getString("SqlCli.3"), SQLCliOptions.LOGGING)); // (authorized) //$NON-NLS-1$
		try {
			getScriptRunnerContext().getOutputStream().flush();
		} catch (IOException e) {
		}
	}

	public static void main(String[] args) throws Exception {
		try {
			addShutdownHooks();
			// TODO LOG4j setting setup PropertyConfigurator.configure
			// for others to reference to know if we are in cmd line
			_isCmdLine = true;
			AnsiConsole.systemInstall();
			// Forcing exe to take the startupdir to be where exe was started
			// from, not SQLCL/bin
			if (System.getenv("STARTUP_SQLCL") != null && !System.getenv("STARTUP_SQLCL").equals("")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				System.setProperty("user.dir", System.getenv("STARTUP_SQLCL")); //$NON-NLS-1$ //$NON-NLS-2$
			}
			SqlCli cli = new SqlCli();
			cli.setScriptRunnerContext(cli.processArgs(args));
			if (SQLCliOptions.isBadOption()) {
				if (!cli.getScriptRunnerContext().isSQLPlusClassic()) {
					cli.getScriptRunnerContext().write(SQLCliOptions.getBadOption() + "\n"); //$NON-NLS-1$
				}
				cli.getScriptRunnerContext().setExited(true);
				cli.getScriptRunnerContext().putProperty(ScriptRunnerContext.EXIT_INT, 1);
				cli.handleHelp();
				SqlCli.handleExit(cli);
			}
			cli.setLoggingState(cli.getScriptRunnerContext());
			// If version is specified, there is no banner and no logging on.

			if (SQLCliOptions.isOptionUsed(SQLCliOptions.VERSION)) {
				cli.handleVersion();
			} else if (SQLCliOptions.isOptionUsed(SQLCliOptions.HELP)) {
				cli.handleVersion();
				cli.handleHelp();
			} else {
				if (!(SQLCliOptions.isOptionUsed(SQLCliOptions.SILENT))) {
					try {
						cli.handleBanner();
						cli.handleUpdates();
						cli.getScriptRunnerContext().getOutputStream().flush();
					} catch (Exception e) {
						// Shh We dont want to report any of these as they will
						// not affect users experience.
					}
				}
				// Put a reference of the reader into the context so the script
				// commands can be made aware of them
				cli.getScriptRunnerContext().setSQLPlusConsoleReader(cli.getReader());
				// Set the width and height of the terminal
				if (!cli.getScriptRunnerContext().isSQLPlusClassic()) {
					setTerminalConstraints(cli);
				}
				SQLPLUS.setSqlpathProvider(new SQLPlusProviderForSQLPATH());
				cli.getScriptRunnerContext().putProperty(ScriptRunnerContext.ORACLE_HOME_MAIN_THREAD,
						TNSHelper.getOracleHome());
				cli.getScriptRunnerContext().putProperty(ScriptRunnerContext.SQLPATH_PROVIDER_MAIN_THREAD,
						SQLPLUS.getSqlpathProvider().getSQLPATHsetting());
				cli.handleLoginSql(cli.getScriptRunnerContext());
				cli.handleAtFiles(cli.getScriptRunnerContext());
				cli.handleExitInAtFiles();
				// Flush banners and logs up to startup.
				try {
					cli.getScriptRunnerContext().getOutputStream().flush();
				} catch (IOException e) {
				}
				cli.startSQLPlus();
			}
			handleCloseDown(cli);
			handleExit(cli);
		} catch (FileNotFoundException fnfe) {
			log(Level.SEVERE, fnfe.getMessage());
			// do not log exception as too noisey.
		} catch (Exception e) {
			log(Level.SEVERE, e.getMessage());
			e.printStackTrace();
			ExceptionHandler.handleException(e);
		}
	}

	private void handleUpdates() {
		if (SQLCliOptions.isOptionUsed(SQLCliOptions.NOUPDATES))
			return;
		String banner = CheckForUpdates.getBanner();
		if (banner != null && banner.trim().length() > 0)
			getScriptRunnerContext().write(banner);
		Thread t = new Thread("Checkforupdates") {
			/*
			 * (non-Javadoc)
			 * 
			 * @see java.lang.Thread#run()
			 */
			@Override
			public void run() {
				StringBuilder updateString = new StringBuilder();
				CheckForUpdates cfu = CheckForUpdates.check();
				if (cfu.isLoaded()) {
					// LEts see if a Broadcast message is there.
					if ((SQLclVersion.getSQLclOracleVersion().compareTo(cfu.getLatestOracleVersion()) < 0) // Dont
																											// want
																											// to
																											// show
																											// a
																											// message
																											// if
																											// the
																											// local
																											// build
																											// is
																											// higher
																											// than
																											// system
																											// build.
							&& cfu.getBroadcastMessage() != null && cfu.getBroadcastMessage().length() > 0) {
						if (cfu.isMessageShowable(new Date())) {
							updateString.append("\t" + cfu.getBroadcastMessage() + "\n\n"); //$NON-NLS-1$ //$NON-NLS-2$
						}
					}
					if (SQLclVersion.getSQLclOracleVersion().compareTo(cfu.getLatestOracleVersion()) < 0) {
						String version = MessageFormat.format(Messages.getString("SqlCli.13"), cfu.getLatestVersion()); //$NON-NLS-1$
						updateString.append("\n" + version + "\n\n"); // (authorized) //$NON-NLS-1$ //$NON-NLS-2$
					}
					if (updateString.length() > 0) {
						cfu.save(updateString.toString());
					} else {
						cfu.save("");
					}
				} else {
					cfu.save("");
				}

			}

		};
		t.start();
	}

	/**
	 * @param cli
	 */
	private static void setTerminalConstraints(SqlCli cli) {
		// for non-windows get the columns/lines size from the terminal
		if (!System.getProperty("os.name").toLowerCase().contains("win")) { //$NON-NLS-1$ //$NON-NLS-2$
			try {
				Process p = Runtime.getRuntime().exec(new String[] { "/bin/bash", "-c", "tput cols 2> /dev/tty" }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				byte[] buf = new byte[200];
				p.getInputStream().read(buf);
				String val = new String(buf).trim();
				int cols = Integer.parseInt(val);
				cli.getScriptRunnerContext();
				cli.getScriptRunnerContext().putProperty(ScriptRunnerContext.SETLINESIZE, cols);
			} catch (Exception e) {
				// something went wrong no col size detected
			}
			try {
				Process p = Runtime.getRuntime().exec(new String[] { "/bin/bash", "-c", "tput lines 2> /dev/tty" }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				byte[] buf = new byte[200];
				p.getInputStream().read(buf);
				String val = new String(buf).trim();
				int lines = Integer.parseInt(val);

				cli.getScriptRunnerContext();
				cli.getScriptRunnerContext().putProperty(ScriptRunnerContext.SETPAGESIZE, lines);
			} catch (Exception e) {
				// something went wrong no lines size detected
			}
		}
	}

	private static void addShutdownHooks() {
		// Add the raptor shutdown to the hooks so we always shutdown
		Runtime.getRuntime().addShutdownHook(new Thread("cleanup") { //$NON-NLS-1$
			@Override
			public void run() {
				MultiLineHistory.getInstance().save(); // Save history
				Aliases.getInstance().save(); // Save aliases
			}
		});
	}

	private void handleHelp() {
		try {
			InputStream is = getClass()
					.getResourceAsStream("/oracle/dbtools/raptor/scriptrunner/cmdline/help/help1.txt"); //$NON-NLS-1$
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

			String line;
			// read from the urlconnection via the bufferedreader
			while ((line = bufferedReader.readLine()) != null) {
				getScriptRunnerContext().write(line + "\n"); //$NON-NLS-1$
			}
			getScriptRunnerContext().getOutputStream().flush();
			bufferedReader.close();
		} catch (IOException e) {
			log(Level.SEVERE, e.getMessage());
		}

	}

	private static void log(Level level, String message) {
		if (logging) {
			Logger l = Logger.getLogger("oracle.dbtools"); //$NON-NLS-1$
			l.setLevel(Level.parse("SEVERE")); //$NON-NLS-1$
			l.log(level, message);
		}

	}

	private void handleVersion() {
		String banner = Messages.getString("SqlCli.Version"); //$NON-NLS-1$
		banner = MessageFormat.format(banner,
				new Object[] { SqlCli.productName, SQLclVersion.getSQLclVersion(), SQLclVersion.getSQLclState() });
		getScriptRunnerContext().write(banner + "\n"); //$NON-NLS-1$
		try {
			getScriptRunnerContext().getOutputStream().flush();
		} catch (IOException e) {
		}
	}

	private void handleBanner() {
		if (!(SQLCliOptions.isOptionUsed(SQLCliOptions.SILENT)
				^ SQLCliOptions.isOptionUsed(SQLCliOptions.XHIDEBANNERS))) {
			banner();
		}
	}

	private void setLoggingState(ScriptRunnerContext context) {
		Boolean optlCheck = (Boolean) context.getProperty(ScriptRunnerContext.OPTLFLAG);
		if ((optlCheck == null) || optlCheck.equals(Boolean.FALSE)) {
			// -verbose flag off. Just log severe logs
			logging = true;
			Enumeration<String> list = LogManager.getLogManager().getLoggerNames();
			while (list.hasMoreElements()) {
				Logger.getLogger(list.nextElement()).setLevel(Level.SEVERE);
			}
		} else {
			// -verbose flag on. Just log info warnings.
			logging = false;
			Enumeration<String> list = LogManager.getLogManager().getLoggerNames();
			while (list.hasMoreElements()) {
				Logger.getLogger(list.nextElement()).setLevel(Level.INFO);
			}
		}
	}

	private void handleExitInAtFiles() {
		if (getScriptRunnerContext().getProperty(ScriptRunnerContext.EXIT_INT) != null) {
			handleCloseDown(this);
			handleExit(this);
		}

	}

	private void handleAtFiles(ScriptRunnerContext context) throws Exception {
		boolean runFile = true;
		if (SQLCliOptions.getFileName() != null && SQLCliOptions.getFileName().length() > 0) {
			ArrayList<String> parserArgs = SQLCliOptions.getFileArguments();
			if (parserArgs != null) {
				for (int argCount = 0; argCount < parserArgs.size(); argCount++) {
					Map<String, String> m = context.getMap();
					// should really go through ScriptUtils
					// main checks are - argument quote exception
					// Will do this for now:remove outer "" and ''
					String preArg = parserArgs.get(argCount);
					if (((preArg.startsWith("'") && preArg.endsWith("'")) || //$NON-NLS-1$ //$NON-NLS-2$
							(preArg.startsWith("\"") && preArg.endsWith("\""))) && (preArg.length() > 1)) { //$NON-NLS-1$ //$NON-NLS-2$
						preArg = preArg.substring(1, preArg.length() - 1);
					}
					m.put(Integer.toString(argCount + 1), preArg);
				}

			}
			String toRun = null;
			String pathGiven = null;
			if (SQLPLUS.getSqlpathProvider() != null) {
				pathGiven = SQLPLUS.getSqlpathProvider().getSQLPATHsetting();
			}

			// note putting in sqlpath not doing http of ftp for now
			String inputFile = SQLCliOptions.getFileName();
			if (!(SQLCliOptions.startsWithHttpOrFtp(inputFile) && SQLCliOptions.haveIBytes(inputFile)
					|| SQLCliOptions.haveIBytesRaw(inputFile))) {
				boolean prependDirectory = getPrependDirectory(inputFile);

				toRun = getTheCompleteFilename(inputFile, pathGiven);
				if ((pathGiven != null) && (prependDirectory)) { // $NON-NLS-1$
					for (String possible : pathGiven.split(File.pathSeparator)) {// $NON-NLS-1$
						if (possible == null) {
							continue;
						}

						if ((possible.endsWith("/")) || (possible.endsWith("\\"))) { //$NON-NLS-1$ //$NON-NLS-2$
							possible += inputFile;
						} else {
							possible += File.separator + inputFile;
						}
						// does not check http://xxxx.xxx
						if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
							possible = possible.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
						} else {
							possible = possible.replace("\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$
						}
						Logger.getLogger(this.getClass().getName()).log(Level.INFO,
								MessageFormat.format(Messages.getString("SqlCli.FILECHECK"), possible)); //$NON-NLS-1$
						if ((new File(possible).exists()) && (new File(possible).canRead())) {
							toRun = possible;
							break;
						}
					}
				}

			}
			if (toRun == null) {
				toRun = SQLCliOptions.getFileName();
				Logger.getLogger(this.getClass().getName()).log(Level.INFO,
						MessageFormat.format(Messages.getString("SqlCli.FILECHECK"), toRun)); //$NON-NLS-1$
			}
			if (SQLCliOptions.startsWithHttpOrFtp(inputFile) && SQLCliOptions.haveIBytes(inputFile)
					|| SQLCliOptions.haveIBytesRaw(inputFile)) {
				URLConnection u = new URL(inputFile.replaceAll("\\\\", "/")).openConnection(); //$NON-NLS-1$ //$NON-NLS-2$
				_runner = new ScriptExecutor(u.getInputStream(), _conn);
			} else {
				if ((new File(toRun).exists()) && (new File(toRun).canRead())) {
					// create the script runner with the fileName
					_runner = new ScriptExecutor(new FileInputStream(toRun), _conn);
				} else {
					runFile = false;
					context.write(MessageFormat.format("SP2-0310: unable to open file \"{0}\"\n", toRun));
					_runner = new ScriptExecutor(_conn);
					_runner.setScriptRunnerContext(context);
				}

			}
			if (runFile) {
				_runner.setScriptRunnerContext(context);
				SQLPLUS.setExecutorPath(_runner, inputFile, context);
				try {
					runFile(_runner, "runner", context, inputFile); //$NON-NLS-1$
				} finally {
					context.setSourceRef(""); //$NON-NLS-1$
				}
			}
		} else {
			_runner = new ScriptExecutor(_conn);
			_runner.setScriptRunnerContext(context);
		}
		context.putProperty("runner", _runner); //$NON-NLS-1$
	}

	private void runFile(ScriptExecutor runner, String name, ScriptRunnerContext context, String inputFile) {
		context.putProperty(name, runner);
		context.setTopLevel(false);
		@SuppressWarnings("unchecked")
		ArrayList<String> scrArrayList = ((ArrayList<String>) getScriptRunnerContext()
				.getProperty(ScriptRunnerContext.APPINFOARRAYLIST));
		scrArrayList.add(inputFile);
		getScriptRunnerContext().putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
		_runner.setScriptRunnerContext(context);
		try {
			_runner.run();
		} finally {
			setScriptRunnerContext(_runner.getScriptRunnerContext());
			scrArrayList = (ArrayList<String>) getScriptRunnerContext()
					.getProperty(ScriptRunnerContext.APPINFOARRAYLIST);
			if (scrArrayList.size() != 0) {
				scrArrayList.remove(scrArrayList.size() - 1);
			}
			getScriptRunnerContext().putProperty(ScriptRunnerContext.APPINFOARRAYLIST, scrArrayList);
			if (scrArrayList.size() != 0) {
				SetAppinfo.setAppinfo(getScriptRunnerContext(), getScriptRunnerContext().getCurrentConnection(),
						scrArrayList.get(scrArrayList.size() - 1), scrArrayList.size());
			} else {
				SetAppinfo.setAppinfo(getScriptRunnerContext(), getScriptRunnerContext().getCurrentConnection(),
						SQLPLUS.PRODUCT_NAME, // $NON-NLS-1$p
						0);
			}
		}
		context.setTopLevel(true);
	}

	private boolean getPrependDirectory(String inputFile) {
		// allow windows unc paths starts with \\
		if (!((inputFile.startsWith("/")) || (inputFile.startsWith("\\")))) {
			if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
				if (!((inputFile.length() > 3) && (((inputFile.charAt(1) == ':')
						&& ((inputFile.charAt(2) == '/') || (inputFile.charAt(2) == '\\')))
						|| ((inputFile.charAt(0) == '/') && (inputFile.charAt(2) == ':')
								&& (inputFile.charAt(3) == '/'))))) { // $NON-NLS-1$
					return true;
				}
			} else {
				if (!((inputFile.startsWith("/")) || (inputFile.startsWith("\\")))) { //$NON-NLS-1$ //$NON-NLS-2$
					return true;
				}
			}
		}
		return false;
	}

	private void handleLoginSql(ScriptRunnerContext context) {
		context.putProperty(ScriptRunnerContext.DBCONFIG_GLOGIN_FILE, null);// override
																			// used
																			// for
																			// sqldev
		if (context.getRestrictedLevel().getLevel() <= 3) {
			// This should allow us to connect using internal connect String in
			// an @file
			if (!SQLCliOptions.isOptionUsed(SQLCliOptions.NOLOG)) {
				context.setSQLPlusCmdLineLogin(); // Tell engine this is first
													// login
				String connectS = SQLCliOptions.getConnectString();
				if (connectS == null) {
					// Ok, this is to support the user/password@abc syntax for
					// sqlplus testing. We are only doinf this if
					// we have an @ file and no username password.
					if (SQLCliOptions.getConnectString() == null && SQLCliOptions.getFileName() != null
							&& SQLCliOptions.getFileName().length() > 0) {
						context.putProperty(ScriptRunnerContext.LOGON, Boolean.TRUE);
						// read the first line of the @file
						String file = ""; //$NON-NLS-1$
						String pathGiven = null;
						if (SQLPLUS.getSqlpathProvider() != null) {
							pathGiven = SQLPLUS.getSqlpathProvider().getSQLPATHsetting();
						}
						// note putting in sqlpath not doing http of ftp for now
						String inputFile = SQLCliOptions.getFileName();
						if (!(SQLCliOptions.startsWithHttpOrFtp(inputFile) && SQLCliOptions.haveIBytes(inputFile)
								|| SQLCliOptions.haveIBytesRaw(inputFile))) {
							file = getTheCompleteFilename(inputFile, pathGiven);
							//
							if ((new File(file).exists()) && (new File(file).canRead())) {
								connectS = getFirstLineOfFile(file, context);
							}
						}
					} else
						connectS = ""; // no username or password //$NON-NLS-1$
										// or anything given - should pass
										// through to
					// prompt for them assumption "null" as user was not
					// required.
				}
				// SQLCommand connectSql = new
				// SQLCommand(SQLCommand.StmtType.G_C_SQLPLUS,
				// StmtSubType.G_S_CONNECT,
				// SQLCommand.StmtResultType.G_R_NONE,
				// true);
				if (SQLCliOptions.getAsRole() != null && (!SQLCliOptions.getAsRole().equals(""))) { //$NON-NLS-1$
					connectS += " as " + SQLCliOptions.getAsRole(); //$NON-NLS-1$
				} else if (SQLCliOptions.isAsSysBackup()) {
					connectS += " as sysbackup";//$NON-NLS-1$
				}
				connectS = "connect " + connectS + "\n";// need //$NON-NLS-1$ //$NON-NLS-2$
														// a terminator (no
														// harm)
				context.putProperty(ScriptRunnerContext.DBCONFIG_GLOGIN, true);// override
																				// used
																				// for
																				// sqldev
				context.putProperty(ScriptRunnerContext.NOLOG, Boolean.TRUE);
				ScriptExecutor runner = new ScriptExecutor(connectS, null);// does
																			// null
																			// connection
																			// cause
																			// a
																			// problem..
				// any listener changes ? Do we need a command line connect
				// flag?
				runner.setScriptRunnerContext(context);
				// sp.setScriptRunnerContext(context);
				try {// set special flag that you want the connect repeated 3
						// times.
					runner.getScriptRunnerContext().putProperty(ScriptRunnerContext.IN_INITIAL_SQLCLCONNECT, true);
					Boolean logon = (Boolean) context.getProperty(ScriptRunnerContext.LOGON);
					if ((logon == null) || (logon.equals(Boolean.FALSE))) {
						runner.getScriptRunnerContext().putProperty(ScriptRunnerContext.THREETIMES, true);
					} else {
						runner.getScriptRunnerContext().putProperty(ScriptRunnerContext.THREETIMES, false);
					}
					// do not want substitution or escape on during command line
					runner.getScriptRunnerContext().putProperty(ScriptRunnerContext.COMMANDLINECONNECT, Boolean.TRUE);
					runner.getScriptRunnerContext().setEscape(false);
					runner.getScriptRunnerContext().setSubstitutionOn(false);
					// reset these 3 before .login run
					runner.setOut(new BufferedOutputStream(System.out));
					runner.run();
				} finally {
					runner.getScriptRunnerContext().putProperty(ScriptRunnerContext.THREETIMES, false);
					runner.getScriptRunnerContext().putProperty(ScriptRunnerContext.IN_INITIAL_SQLCLCONNECT, false);
				}
				if (runner.getScriptRunnerContext().getCurrentConnection() != null) {
					_conn = runner.getScriptRunnerContext().getCurrentConnection();
					// redundant - context & getScriptRunnerContext - should be
					// same object.
					context = runner.getScriptRunnerContext();
					context.setBaseConnection(_conn);
					context.setCloseConnection(false);// note does not close on
														// exit
					handleEditions(runner);
				} else {
					handleCloseDown(this);// just prints exit for now. Connect
											// already
											// prints error on failure
					System.exit(1);
				}
			} else {
				// NOLOG is on. We have no connection, but we still need to run
				// glogin
				// and login.sql
				String glogin = ShowLogin.firstOrNull(ShowLogin.getLocations(context, "glogin.sql", false)); //$NON-NLS-1$
				if (glogin != null) {
					File f = new File(glogin);
					if (f.exists()) {
						try {
							_runner = new ScriptExecutor(new FileInputStream(glogin), _conn);
							context.putProperty("runner", _runner); //$NON-NLS-1$
							context.putProperty(ScriptRunnerContext.SQLCLI_GLOGIN_SQL, Boolean.TRUE);
							runFile(_runner, "glogin", context, glogin); //$NON-NLS-1$
							context.putProperty(ScriptRunnerContext.SQLCLI_GLOGIN_SQL, Boolean.FALSE);
							_conn = _runner.getScriptRunnerContext().getCurrentConnection();// some
																							// genius
																							// puts
																							// connect
																							// in
																							// (g)login.sql
						} catch (FileNotFoundException e) {
						}
					}
				}
				String loginFile = getLoginFile(context);
				if (loginFile != null) {
					// We know it exists and is readable
					try {
						_runner = new ScriptExecutor(new FileInputStream(loginFile), _conn);
						context.putProperty("runner", _runner); //$NON-NLS-1$
						context.putProperty(ScriptRunnerContext.SQLCLI_LOGIN_SQL, Boolean.TRUE);
						runFile(_runner, "glogin", context, loginFile); //$NON-NLS-1$
						context.putProperty(ScriptRunnerContext.SQLCLI_LOGIN_SQL, Boolean.FALSE);
						_conn = _runner.getScriptRunnerContext().getCurrentConnection();// some
																						// genius
																						// puts
																						// connect
																						// in
																						// (g)login.sql
					} catch (FileNotFoundException e) {
					}
				}
			}
		}
	}

	private void handleEditions(ScriptExecutor runner) {
		if (SQLCliOptions.getEdition() != null) {
			String sql = MessageFormat.format("alter session set edition={0}", SQLCliOptions.getEdition());
			runner.setStmt(sql);
			runner.run();
		}
	}

	private String getLoginFile(ScriptRunnerContext context) {
		return ShowLogin.firstOrNull(ShowLogin.getLocations(context, "login.sql", true)); //$NON-NLS-1$
	}

	private String getTheCompleteFilename(String inputFile, String pathGiven) {
		String file = inputFile;
		boolean prependDirectory = getPrependDirectory(inputFile);
		if ((pathGiven != null) && (prependDirectory)) { // $NON-NLS-1$
			for (String possible : pathGiven.split(File.pathSeparator)) {// $NON-NLS-1$
				if (possible == null) {
					continue;
				}
				if ((possible.endsWith("/")) || (possible.endsWith("\\"))) { //$NON-NLS-1$ //$NON-NLS-2$
					possible += inputFile;
				} else {
					possible += File.separator + inputFile;
				}
				// does not check http://xxxx.xxx
				if (System.getProperty("os.name").startsWith("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
					possible = possible.replace("/", "\\"); //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					possible = possible.replace("\\", "/"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				Logger.getLogger(this.getClass().getName()).log(Level.INFO,
						MessageFormat.format(Messages.getString("SqlCli.FILECHECK"), possible)); //$NON-NLS-1$
				if ((new File(possible).exists()) && (new File(possible).canRead())) {
					file = possible;
					break;
				}
			}
		} else {
			// just get the file. this must be absolute.
			file = inputFile;
		}
		return file;
	}

	private String getFirstLineOfFile(String inputFile, ScriptRunnerContext context) {
		try {
			@SuppressWarnings("resource")
			BufferedReader getLoginFile = null;
			if (inputFile.indexOf('.') != -1 && inputFile.lastIndexOf('.') > 0)
				getLoginFile = new BufferedReader(new FileReader(new File(inputFile)));
			else if (context.getProperty(ScriptRunnerContext.SUFFIX) != null) {
				getLoginFile = new BufferedReader(
						new FileReader(new File(inputFile + "." + context.getProperty(ScriptRunnerContext.SUFFIX)))); //$NON-NLS-1$
			} else
				getLoginFile = new BufferedReader(new FileReader(new File(inputFile)));
			String connectLine = getLoginFile.readLine();
			String patterns = "(\\w+\\/\\w+@\\w+(\\s+as\\s+(sysdba)|(sysoper))?)|\\w+\\/\\w+@\\w+\\s+as\\s+sysdba|\\w+\\/\\w+@\\w+\\s+as\\s+sysoper|\\w+\\/\\w+\\s+as\\s+sysdba|\\w+\\/\\w+\\s+as\\s+sysoper|\\w+\\/\\w+"; //$NON-NLS-1$
			Pattern pattern = Pattern.compile(patterns, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(connectLine.trim());
			if (matcher.matches()) {
				context.putProperty("SQLCLI_IGNORE_FIRST_LINE", true); //$NON-NLS-1$
				context.putProperty(ScriptRunnerContext.THREETIMES, Boolean.FALSE);
				return connectLine.trim();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getPrompt(boolean bold) {
		if (RUNNING || SQLCliOptions.isOptionUsed(SQLCliOptions.SILENT)) {
			return ""; //$NON-NLS-1$
		} else {
			if (bold)
				return Ansi.ansi().bold().a(_ctx.getPrompt()).reset().toString();
			else
				return Ansi.ansi().render(_ctx.getPrompt()).reset().toString();
		}
	}

	private void startSQLPlus() throws Exception {
		getScriptRunnerContext().removeProperty("SQLCLI_IGNORE_FIRST_LINE"); //$NON-NLS-1$
		// StringBuffer fullLine = new StringBuffer();
		prompt = getPrompt(isPromptBold);
		reader.setHandleUserInterrupt(true);
		reader.setCopyPasteDetection(true);
		reader.setBellEnabled(true);
		reader.setBasePrompt(prompt);// We set this because when we edit, we
										// change the prompt to match the lines
										// we are using.
		String leftOver = "";//$NON-NLS-1$
		// boolean skipNextRead = false;
		/*
		 * Current Line will be returned from the reader as the multiline buffer is in
		 * the reader and that is the source of truth for the editor.
		 */
		String currentLine = "";//$NON-NLS-1$
		Pattern pattern = Pattern.compile("^(-?\\d+.*)"); //$NON-NLS-1$
		/*
		 * We can now clear the current working buffer.Incase values were added during
		 * the startup process.
		 */
		stopEditor();
		reader.getMultilineBuffer().clear();

		_exitThread = startExitListener(reader); // listen (read from) the
													// terminal incase CTRL C is
													// typed while processing a
													// command.
		// Check for text passed in. Take it all and process it if there is.
		try {
		    boolean blockOnly=false;
			String passedText = reader.readNonBlockingText();
			if (passedText.trim().length() > 0) {
				// Here we know we are reading from stdin. Set the flag to alert
				// interested parties
				getScriptRunnerContext().putProperty(ScriptRunnerContext.SQLCLI_STDIN, Boolean.TRUE);
				processLine(passedText);
				// Afterwards, if we are not exiting, then we go back to normal,
				// however we want to make sure
				// we still know we have started with stdin
				getScriptRunnerContext().putProperty(ScriptRunnerContext.SQLCLI_STDIN, Boolean.FALSE);
				if (isatty(STDIN_FILENO)==0) {//1 for tty reportedly does not work for cygwin.
				    blockOnly=true;
				    //do not make any editor clear calls as they add whitespace.
				}
			}
			if (!blockOnly) {
				while (// skipNextRead ||
				(startEditing() && (currentLine = reader.readLine(_ctx.getPrompt())) != null)) { // $NON-NLS-1
					if (!RUNNING) {
						if (_buffer.getBuffer().trim().length() == 0
								|| isBlockTermBLankLines(_buffer.getLine(_buffer.size()).trim())) { // $NON-NLS-1$
							// if buffer line entered, stop editing unless there is
							// a block terminator and its not a doc command
							boolean carryon = false;
							if (_buffer.getBuffer().trim().length() > 0) {
								Iterator<ISQLCommand> line_parser = SqlParserProvider.getScriptParserIterator(
										getScriptRunnerContext(), new StringReader(StringUtils.rtrim(_buffer.getBuffer())));
								if (line_parser.hasNext()) {
									ISQLCommand whatami = line_parser.next();
									if (whatami.getStmtSubType().equals(StmtSubType.G_S_DOC_PLUS))
										carryon = true; // allow the editor to
														// continue.
								}
							}
							if (!carryon) {
								stopEditor();
								processLine(""); //$NON-NLS-1$
							}
						} else if ((_buffer.getBuffer().trim().replaceFirst(";", "").length() == 0)) { //$NON-NLS-1$ //$NON-NLS-2$
							processLine("l"); //$NON-NLS-1$
						} else if (_buffer.size() == 1 && pattern.matcher(currentLine).matches()) {
							// Goto line or change line
							gotoOrChangeLine(currentLine);
						} else if (reader.justDidHistory()) {
							reader.resetHistoryFlag();
							processLine(_buffer.getBuffer());
						} else if (reader.isRunBufferNow()) {
							reader.setRunBufferNow(false);
							processLine(_buffer.getBuffer());
						} else if (isSQL(_buffer.getBuffer()) && reader.getMultilineBuffer().size() > 1
								&& reader.getMultilineBuffer().getCurrentLine() == reader.getMultilineBuffer().size()
								&& lastLineSQLBlankLines(reader.getMultilineBuffer()
										.getLine(reader.getMultilineBuffer().getCurrentLine()).trim())) {
							// Emulating SQLPlus for SQL statements so we jump out
							// of the editor when we hit a blank line.
							// Need to check SQLBlank lines tho now and make sure we
							// respect it.
							if (lastLineSQLBlankLines(reader.getMultilineBuffer()
									.getLine(reader.getMultilineBuffer().getCurrentLine()).trim())) {
								String tmp = reader.getMultilineBuffer().getBuffer().trim().toLowerCase();
								// Very specifically. for plsql, we can do whatever
								// we want. sqlbl is not in action
								if (!(tmp.startsWith("begin") || tmp.startsWith("declare"))) //$NON-NLS-1$ //$NON-NLS-2$
									stopEditor();
							}
							// If sqlbl is on then we go around too.
	
						} else if (reader.getMultilineBuffer().getCurrentLine() < reader.getMultilineBuffer().size()
								|| ((reader.getMultilineBuffer().getCurrentLine() == reader.getMultilineBuffer().size())
										&& reader.getCursorBuffer().cursor < reader.getCursorBuffer().length())) {
							/*
							 * We are still editing so need to go around again.
							 */
						} else {
							// creating initial statements
							String line = _buffer.getBuffer();
							//
							if (line != null) {
								_buffer.startEditing(false);
							}
							String dummyComment = "";//$NON-NLS-1$
	
							// If there is nothing in the buffer, we have no been
							// editing hence this is something like a sqlplus
							// command, so lets use the raw String
							// instead
							// getBuffer adds \n
							String s = _buffer.getBuffer();
	
							for (int i = 0; i < 1000; i++) {
								dummyComment = "/* SQLDevZ " + i + " dummy*/";//$NON-NLS-1$ //$NON-NLS-2$
								if ((s.indexOf(dummyComment) == -1)) {
									break;
								}
								;
							}
							ISQLCommand icmd = null;
							if (s.startsWith("*")) { //$NON-NLS-1$
								processLine(s + ";\n"); //$NON-NLS-1$
							} else {
								// assumption s from getBuffer is \n terminated
								String alt = s + dummyComment;
	
								Iterator<ISQLCommand> m_parser = SqlParserProvider
										.getScriptParserIterator(getScriptRunnerContext(), new StringReader(alt));
								int commandCount = 0;
								while (((!((commandCount > 0) && leftOver.equals(""))) && (m_parser //$NON-NLS-1$
										.hasNext()))) {// normally
														// leftover.trim()="" - i.e.
														// no second statements or
														// part of
														// second statement - so
														// skip a
														// next()
									icmd = m_parser.next();
									String runWith = icmd.getSQLOrig();
	
									commandCount++;
									// if ((commandCount > 1) &&
									// (icmd.getSQLOrig().indexOf(dummyComment) ==
									// -1)) {
									// skipNextRead = true;// skip next read as we
									// have
									// // multiple statements on this
									// // line.
									// }
	
									if (icmd.getSQLOrig().indexOf(dummyComment) == -1) {
										s = s.replaceAll("\\\\r\\\\n", "\n");//$NON-NLS-1$ //$NON-NLS-2$
										// just in case \r s have got in...
										if (icmd.getSQLOrig().replaceAll("\\\\r\\\\n", "\n").trim().length() <= s.length()) //$NON-NLS-1$ //$NON-NLS-2$
											leftOver = StringUtils.rtrim(StringUtils.rtrim(s)
													.substring(icmd.getSQLOrig().replaceAll("\\\\r\\\\n", "\n").length()));//$NON-NLS-1$ //$NON-NLS-2$
										// issue is SQLOrig always SQLOrig eg are
										// --xxx
										// stripped due to concatenation check);
	
										if ((icmd.getStatementTerminator() != null)
												&& (!icmd.getStatementTerminator().equals(""))) {//$NON-NLS-1$
											String termTrim = icmd.getStatementTerminator().trim();
											if (leftOver.startsWith(termTrim)) {
												if ((leftOver.length() == 1) || (leftOver.length() == 0)) {
													leftOver = "";//$NON-NLS-1$
												} else {
													leftOver = leftOver.substring(1).trim();
												}
											}
											if ((!termTrim.equals("")) && (!runWith.trim().endsWith(termTrim))) {//$NON-NLS-1$
												if (termTrim.equals("/")) {//$NON-NLS-1$
													runWith += "\n/\n";//$NON-NLS-1$
												} else {
													if (termTrim.equals(";")) {//$NON-NLS-1$
														runWith += ";\n";//$NON-NLS-1$
													}
												}
											}
										}
	
										processLine(runWith);
										prompt = getPrompt(isPromptBold);
										if (getScriptRunnerContext().getProperty(ScriptRunnerContext.EXIT_INT) != null) {
											break;
										}
										icmd = null;
									}
									// this is dodgy. we are circumventing the
									// script parser by trying to identify the end
									// of the statement here and run it....
									// better to leave this to the script parser.
									// for the moment, make sure the WITH clause
									// does not get processed here
									String terminator = getScriptRunnerContext()
											.getProperty(ScriptRunnerContext.SQLTERMINATOR).toString();
									if (icmd != null && icmd.getProperty(ScriptParser.TERMINATOR) != null) {
										terminator = (String) icmd.getProperty(ScriptParser.TERMINATOR);
									}
									Boolean classic = ((this.getScriptRunnerContext() != null
											&& this.getScriptRunnerContext()
													.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE) != null
											&& Boolean.parseBoolean(this.getScriptRunnerContext()
													.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString())));
									if (icmd != null && icmd.getStmtType().equals(StmtType.G_C_SQL)
											&& (s.trim().endsWith(terminator)) && icmd.getStmtSubType() != null
											&& icmd.getStmtSubType() != StmtSubType.G_S_WITH
											&& (classic /* in script parser it is ||!classic and live terminator */)) {
										Pattern p = Pattern.compile("\\/\\*\\s+SQLDevZ\\s+\\d+\\s+dummy\\*\\/", //$NON-NLS-1$
												Pattern.DOTALL);
										Matcher m = p.matcher(icmd.getSQLOrig());
										if (m.find()) {
											icmd.setSQLOrig(m.replaceFirst("")); //$NON-NLS-1$
										}
										processLine(icmd.getSQLOrig());
										prompt = getPrompt(isPromptBold);
									}
								}
							}
						}
						if (getScriptRunnerContext().getProperty(ScriptRunnerContext.EXIT_INT) != null) {
							break;
						}
	
						if (_buffer.getCurrentLine() > -1
								&& (_buffer.getCurrentLine() < _buffer.size() || _buffer.isEditing())) {
							prompt = _buffer.getPrompt();
						} else if ((_buffer.getCurrentLine() == _buffer.size()
								&& _buffer.getLine(_buffer.getCurrentLine()).trim().equals("")) || processed) { //$NON-NLS-1$
							prompt = getPrompt(isPromptBold);
							_buffer.stopEditing();
							_buffer.clear();
							processed = false;
	
						} else {
							if (!RUNNING && _buffer.isEditing()) {
								prompt = _buffer.getPrompt();
							} else {
								prompt = getPrompt(isPromptBold);
								// skipNextRead = false;
								_buffer.clear();
							}
						}
					}
					// incomplete statements can be found in scripts. they dont get
					// run, and are passed back through the context.
					SQLCommand incompleteCmd = (SQLCommand) getScriptRunnerContext()
							.getProperty(ScriptRunnerContext.INCOMPLETE);
					if (incompleteCmd != null) {
						// add the incomplete statement to the multiline buffer.
						// This allows the user to complete the statement.
						reader.getMultilineBuffer().add(incompleteCmd.getModifiedSQL());
						getScriptRunnerContext().removeProperty(ScriptRunnerContext.INCOMPLETE);
					}
				}
			}
		} catch (UserInterruptException e) {
			getScriptRunnerContext().write("Interupt\n"); //$NON-NLS-1$
			reader.fireListeners(SQLPlusConsoleReader.CTRLC);
		} finally {
			stopExitListener(_exitThread); // kill the exit thread, as
											// processing is complete
		}
	}

	private boolean lastLineSQLBlankLines(String trim) {
		// Jump out if this is not a blank line
		if (!trim.equals("")) //$NON-NLS-1$
			return false;
		else {
			ScriptRunnerContext ctx = getScriptRunnerContext();
			String sqlbl = (String) ctx.getProperty("SQLBLANKLINES"); //$NON-NLS-1$
			// Ok, What is SQLbl set to.
			boolean on = false;
			if (sqlbl == null) {
				on = false;
			} else if (sqlbl.equals("on")) { //$NON-NLS-1$
				on = true;
			} else {
				on = false;
			}

			// We know trim is blank here so, what do we do?
			// if sqlbl is on, then we'll send false as if there was text in
			// here.
			if (!on) {
				return true;
			} else {
				return false;
			}

		}
	}

	private boolean isBlockTermBLankLines(String terminator) {
		String defaultBlo = "."; //$NON-NLS-1$
		String blo = defaultBlo;
		ScriptRunnerContext ctx = getScriptRunnerContext();
		blo = (String) ctx.getProperty("BLOCKTERMINATOR"); //$NON-NLS-1$
		if (blo == null) {
			blo = defaultBlo;
		} else if (blo.equals("off")) { //$NON-NLS-1$
			blo = null;
		} else if (blo.equals("on")) { //$NON-NLS-1$
			blo = defaultBlo;
		}

		// Ok, we have worked out for doing the block terminator
		// Lets do something about it now
		if (blo == null) {
			// no block terminator, continue on.
			return false;
		} else if (terminator.equals(blo)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isSQL(String buffer) {
		Iterator<ISQLCommand> parser = SqlParserProvider.getScriptParserIterator(getScriptRunnerContext(),
				new StringReader(buffer));
		while (parser.hasNext()) {
			ISQLCommand cmd = parser.next();
			if (cmd.getStmtType().equals(SQLCommand.StmtType.G_C_SQL)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @throws IOException
	 */
	private void stopEditor() throws IOException {
		prompt = stopEditing();
		// This will be the last line of the buffer being "." which stops
		// editing.
		_buffer.deleteLast();// remove that .
		if (_buffer.getBuffer().trim().length() > 0) {
			int currentLine = _buffer.getBufferSafe().getCurrentLine();
			getScriptRunnerContext().getSQLPlusBuffer().setBufferSafe(_buffer.getBufferList());
			getScriptRunnerContext().getSQLPlusBuffer().getBufferSafe().setCurrentLine(currentLine);
			getScriptRunnerContext().getSQLPlusBuffer().getBufferSafe().setTermLine(_buffer.getTermLine(),
					reader.getHeight());
		}
		reader.println();
		reader.flush();
	}

	/**
	 * Thread that reads/peaks/listens to input on the terminal while the main
	 * thread is processing a command
	 */
	private ExitThread startExitListener(final SQLPlusConsoleReader reader) {
		ExitThread exitThread = new ExitThread("Exit Thread", reader, _runner); //$NON-NLS-1$
		exitThread.setPriority(Thread.MIN_PRIORITY);
		exitThread.setDaemon(true);
		exitThread.start();
		return exitThread;
	}

	/**
	 * Stop the ExitListener thread as input on the terminal is handled by the main
	 * thread again as it is finished processing
	 */
	private void stopExitListener(ExitThread exitThread) {
		if (exitThread != null) {
			exitThread.interrupt();
		}
	}

	/**
	 * Thread that reads/peaks/listens to input on the terminal while the main
	 * thread is processing a command Constructor takes the reader and the runner.
	 * The Thread keeps reading/peaking on the reader. Eating up every character
	 * typed. If CTRL C is typed, then the runner is interrupted so that control
	 * returns immediately to the PROMPT.
	 */
	public static class ExitThread extends Thread {
		SQLPlusConsoleReader _reader = null;
		ScriptExecutor _runner = null;
		boolean _running = true;
		static boolean _listen = false;
		static boolean _resume = false;

		public ExitThread(String name, SQLPlusConsoleReader reader, ScriptExecutor runner) {
			super(name);
			_reader = reader;
			_runner = runner;
		}

		public static void stopListening() {
			_listen = false;
			_resume = _listen;
		}

		public void listen() {
			_listen = true;
			_resume = _listen;
		}

		public static void resumeListening() {
			_listen = _resume;
		}

		@Override
		public void interrupt() {
			super.interrupt();
			_running = false;
		};

		@Override
		public void run() {
			boolean sameSession = false;
			while (!interrupted()) {
				if (!_running) {
					return;// stop this thread running
				}
				if (_listen) {
					try {
						// start reading the input
						InputStream in = _reader.getInput();
						if (in instanceof DbtoolsNonBlockingInputStream) {
							ArrayList<Integer> chrs = ((DbtoolsNonBlockingInputStream) in).fullPeek();
							if (chrs.contains(3)) {// bingo . CTRL C hit
								if (sameSession) { // second CTRL C, Kill the
													// Program
									System.exit(1);
								} else {
									((DbtoolsNonBlockingInputStream) in).resetBuffer();// eat
																						// the
																						// ^C
									sameSession = true;
									if (_runner != null) {
										// interrupt the runner. this will
										// return the processing to the command
										// prompt
										_runner.interrupt();
									}
								}
							} else if (chrs.contains(20)) {
								((DbtoolsNonBlockingInputStream) in).resetBuffer();// eat
																					// the
																					// ^t
								Map<Thread, StackTraceElement[]> threads = Thread.getAllStackTraces();
								Iterator<Thread> it = threads.keySet().iterator();
								while (it.hasNext()) {
									Thread key = it.next();
									System.out.println(key.getName());
									StackTraceElement[] stack = threads.get(key);
									for (StackTraceElement s : stack) {
										System.out.println("\t" + s); //$NON-NLS-1$
									}
									System.out.println("\n"); //$NON-NLS-1$
								}
							}
						}
					} catch (Exception e) {
						return;
					}
				} else {
					sameSession = false;
				}
				try {
					sleep(200);
				} catch (InterruptedException e) {
					return;
				}
			}
		}
	}

	// Sneaky way to push the editor for start of readline.
	private boolean startEditing() {
		SQLCommand incompleteCmd = (SQLCommand) getScriptRunnerContext().getProperty(ScriptRunnerContext.INCOMPLETE);
		if (incompleteCmd != null) {
			// add the incomplete statement to the multiline buffer. This allows
			// the user to complete the statement.
			reader.getMultilineBuffer().add(incompleteCmd.getModifiedSQL());
			getScriptRunnerContext().removeProperty(ScriptRunnerContext.INCOMPLETE);
		}
		return true;
	}

	/**
	 * @param fullLine
	 * @return
	 */
	private String stopEditing() {
		String prompt;
		_buffer.stopEditing();
		prompt = getPrompt(isPromptBold);
		// Stopped editing and now we wont need full line either.
		// fullLine.setLength(0);
		return prompt;
	}

	/**
	 * @param lineNumber
	 * @throws IOException
	 */
	private void gotoOrChangeLine(String lineNumber) throws IOException {
		try {
			// Check if the first word is the line number. if its not, cleanup
			// and come back.
			int lineNo = Integer.parseInt(lineNumber.split("\\s+")[0]); //$NON-NLS-1$
		} catch (NumberFormatException e) {
			getScriptRunnerContext()
					.write(MessageFormat.format(Messages.getString("SqlCli.39"), new Object[] { lineNumber })); //$NON-NLS-1$
			processed = true;
			reader.resetEditor();
			reader.setBasePrompt(getPrompt(isPromptBold));
			getScriptRunnerContext().getOutputStream().flush();
			return;
		}
		if (_buffer.getBufferSafe().size() > 0) {
			String command = ""; //$NON-NLS-1$
			int gotoLineNumber = 0;
			StringTokenizer st = new StringTokenizer(lineNumber);
			if (st.hasMoreTokens()) {
				String s = st.nextToken();
				try {
					gotoLineNumber = Integer.parseInt(s);
				} catch (Exception e) {
					gotoLineNumber = -1;
				}
			}
			if (st.hasMoreTokens()) {
				while (st.hasMoreTokens()) {
					command = command + st.nextToken() + " "; //$NON-NLS-1$
				}
			}
			if (command.trim().endsWith(";")) { //$NON-NLS-1$
				command = command.trim().substring(0, command.trim().length() - 1);
			}
			if (gotoLineNumber >= 0) {
				// ok we're ready to do something. Go to line
				if (command.length() > 0) {
					// replace the line in the buffer with this.
					if (gotoLineNumber == 0) {
						_buffer.getBufferSafe().addAtIndex(0, command);
					} else if (gotoLineNumber > _buffer.getBufferSafe().linesInBuffer()) {
						_buffer.getBufferSafe().add(command);
					} else {
						_buffer.getBufferSafe().replace(gotoLineNumber, command);
					}
				} else {
					getScriptRunnerContext().write("\n" + _buffer.getBufferSafe().setCurrentLine(gotoLineNumber)); //$NON-NLS-1$
				}
			}
		} else {
			getScriptRunnerContext().write("\n" + Messages.getString("SqlCli.15")); //$NON-NLS-1$ //$NON-NLS-2$
		}
		processed = true;
		reader.resetEditor();
		reader.setBasePrompt(getPrompt(isPromptBold));
		getScriptRunnerContext().getOutputStream().flush();
	}

	protected void setScriptRunnerContext(ScriptRunnerContext newContext) {
		_ctx = newContext;
		_ctx.setSQLPlusBuffer(_buffer);
		List<Completer> completors = new LinkedList<Completer>();

		reader.addCompleter(new FileNameCompleter(_ctx));
		reader.addCompleter(new SqlclCompleter(_ctx));
		reader.addCompleter(new ArgumentCompleter(completors));
	}

	/**
	 * handleCloseDown SqlCli
	 * 
	 * @param cli
	 */
	private static void handleExit(SqlCli cli) {
		Integer exitInt = (Integer) cli.getScriptRunnerContext().getProperty(ScriptRunnerContext.EXIT_INT);
		// sqlcli defers the clear - only real thing is spool is flushed closed
		// nicely.
		cli.getScriptRunnerContext().deferredReInitClear();
		if ((exitInt != null) && (exitInt > 0)) {
			System.exit(exitInt);
		}
		System.exit(0);
	}

	/**
	 * handleCloseDown SqlCli
	 * 
	 * @param cli
	 */
	private static void handleCloseDown(SqlCli cli) {
		Connection current = cli.getScriptRunnerContext().getCurrentConnection();
		try {// clean up initial connection or current connection - result
				// of last connect.
			if ((cli.getScriptRunnerContext().getProperty(ScriptRunnerContext.SPOOLOUTBUFFER)) != null) {
				cli.getScriptRunnerContext().stopSpool();
			}
			if ((current != null) && (!current.isClosed())) {
				if (cli.getScriptRunnerContext().getProperty(ScriptRunnerContext.NOLOG) == null
						|| cli.getScriptRunnerContext().getProperty(ScriptRunnerContext.NOLOG).equals(Boolean.FALSE)) {
					SQLPLUS.disconnectMessage(current, cli.getScriptRunnerContext(),
							cli.getScriptRunnerContext().getOutputStream());
				}
				current.close();
			}
		} catch (SQLException e) {
			ExceptionHandler.handleException(e);
		}
	}

	@Override
	public void raiseEvent(String event) {
		if (event.equals("ctrlc")) { //$NON-NLS-1$
			getScriptRunnerContext();
		}

	}

}
