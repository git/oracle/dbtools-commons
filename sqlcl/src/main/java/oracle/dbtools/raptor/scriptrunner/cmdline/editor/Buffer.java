/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline.editor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import jline.console.CursorBuffer;
import oracle.dbtools.common.utils.StringUtils;
import oracle.dbtools.plusplus.FileSystem;
import oracle.dbtools.plusplus.IBuffer;
import oracle.dbtools.util.BOMSkipper;

/**
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Buffer.java"
 *         >Barry McGillin</a>
 *
 */
public class Buffer implements IBuffer {
	private static final String SLASH = "/"; //$NON-NLS-1$
	private ArrayList<String> buffer = new ArrayList<String>();
	private int currentLine = -1;
	private boolean editing = false;
	private IBuffer bufferSafe;
	private CursorBuffer _readLineBuffer;
	private int termLine;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		testBufferList();
		testBufferEdit();
		testBufferFile();

	}

	private static void testBufferFile() {
		Buffer abuffer = new Buffer();
		abuffer.add("This is first String"); //$NON-NLS-1$
		abuffer.add("This is second String"); //$NON-NLS-1$
		abuffer.add("This is third String"); //$NON-NLS-1$
		abuffer.addAtIndex(2, "This is four string"); //$NON-NLS-1$
		abuffer.setCurrentLine(2);
		System.out.println(abuffer.list(false));
		System.out.println("Saving buffer"); //$NON-NLS-1$
		abuffer.save("/tmp/test.sql", FileSystem.State.REPLACE, "sql"); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("buffer saved"); //$NON-NLS-1$
		System.out.println("Try empty buffer"); //$NON-NLS-1$
		abuffer.clear();
		System.out.println("printing buffer"); //$NON-NLS-1$
		System.out.println(abuffer.list(false));
		System.out.println("finish printing buffer"); //$NON-NLS-1$
		System.out.println("GEt test.sql"); //$NON-NLS-1$
		abuffer.get("/tmp/test.sql", true); //$NON-NLS-1$
		System.out.println("buffer loaded"); //$NON-NLS-1$
		System.out.println("printing buffer"); //$NON-NLS-1$
		System.out.println(abuffer.list(false));
		System.out.println("finish printing buffer"); //$NON-NLS-1$
		System.out.println("getBufferString"); //$NON-NLS-1$
		System.out.println(abuffer.getBuffer());

	}

	private static void testBufferEdit() {
		Buffer abuffer = new Buffer();
		abuffer.add("This is first String"); //$NON-NLS-1$
		abuffer.add("This is second String"); //$NON-NLS-1$
		abuffer.add("This is third String"); //$NON-NLS-1$
		abuffer.addAtIndex(2, "This is four string"); //$NON-NLS-1$
		abuffer.setCurrentLine(2);
		System.out.println(abuffer.list(false));
		abuffer.append("Appending this"); //$NON-NLS-1$
		System.out.println(""); //$NON-NLS-1$
		System.out.println(abuffer.list(false));
		System.out.println("Try deleting 'Appending this' again"); //$NON-NLS-1$
		abuffer.change("Appending this"); //$NON-NLS-1$
		System.out.println(abuffer.list(false));
		System.out.println("Try changing second to 2 "); //$NON-NLS-1$
		abuffer.change("second", "2"); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println(abuffer.list(false));

		System.out.println("Try insert newline"); //$NON-NLS-1$
		abuffer.input("select * from dual"); //$NON-NLS-1$
		System.out.println(abuffer.list(false));
		abuffer.change("from", "select abc"); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println(abuffer.list(false));

		System.out.println("Try empty buffer"); //$NON-NLS-1$
		abuffer.clear();
		System.out.println("printing buffer"); //$NON-NLS-1$
		System.out.println(abuffer.list(false));
		System.out.println("finish printing buffer"); //$NON-NLS-1$
	}

	private static void testBufferList() {
		Buffer buffer = new Buffer();
		buffer.add("This is first String"); //$NON-NLS-1$
		buffer.add("This is second String"); //$NON-NLS-1$
		buffer.add("This is third String"); //$NON-NLS-1$
		System.out.println(buffer.list(false));
		System.out.println("buffer.getBuffer():"); //$NON-NLS-1$
		System.out.println(">>>" + buffer.getBuffer() + "<<<"); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println(""); //$NON-NLS-1$
		buffer.addAtIndex(2, "This is four string"); //$NON-NLS-1$
		buffer.setCurrentLine(2);
		System.out.println(buffer.list(false));
		System.out.println("list last"); //$NON-NLS-1$
		System.out.println(buffer.listLast(false));
		System.out.println("list 1 3"); //$NON-NLS-1$
		System.out.println(buffer.list(1, 3,false));
		System.out.println("list * 4"); //$NON-NLS-1$
		System.out.println(buffer.deleteStarN(4));
		System.out.println("list 2 *"); //$NON-NLS-1$
		System.out.println(buffer.deleteNStar(2));
		System.out.println("list *"); //$NON-NLS-1$
		System.out.println(buffer.listStar(false));
		System.out.println("list 4"); //$NON-NLS-1$
		System.out.println(buffer.list(4,false));

	}

	public void add(String string) {
		buffer.add(string);
		currentLine = buffer.size();
	}

	/**
	 * Add a String at an index. Checking that index is in range tho...
	 * 
	 * @param @link {@link String string} to add to buffer
	 */

	public void addAtIndex(int index, String string) {
		if (buffer.size() == 0) {
			buffer.add(string);
			return;
		}
		int newIndex = index;
		if (index < 0) {
			newIndex = 0;
		}
		if (index > buffer.size()) {
			buffer.add(string);
		} else {
			buffer.add(newIndex, string);
		}
	}

	/**
	 * Return the current line in the buffer, ie the one thats being edited.
	 * 
	 * @return {@link Integer} currentLine
	 */
	public int getCurrentLine() {
		if (currentLine<0) return 0;
		return currentLine;
	}

	/**
	 * 
	 * LIST |;| * or L |lists all lines in the SQL buffer<br>
	 * 
	 * @return {@link String}
	 */
	public String list(boolean trim) {
		if (buffer.size()>0 && buffer.get(buffer.size()-1).trim().equals("/")) {
			setCurrentLine(buffer.size()-1);
			return list (1, buffer.size()-1,trim);
		}
		setCurrentLine(buffer.size());
		Iterator<String> bufferIterator = buffer.iterator();
		int lineNumber = 1;
		if (bufferIterator.hasNext()) {
			StringBuilder sb = new StringBuilder();
			while (bufferIterator.hasNext()) {
				String line = bufferIterator.next();
				if (!bufferIterator.hasNext()) {
					if (line.trim().endsWith(";") && trim)
						line = line.substring(0, line.length()-1);
				}
				sb.append(IndexBuilder.getIndex(lineNumber, getCurrentLine()) + line + "\n"); //$NON-NLS-1$
				lineNumber++;
				
			}
			return sb.toString();
		} else {
			return Messages.getString("Buffer.51"); //$NON-NLS-1$
		}
	}

	/**
	 * 
	 * LIST |;| * or L |lists all lines in the SQL buffer<br>
	 * 
	 * @return {@link String} 
	 */
	public String list() {
		if (buffer.size()>0 && (buffer.get(buffer.size()-1).trim().equals("/")||buffer.get(buffer.size()-1).trim().equals(".")||buffer.get(buffer.size()-1).trim().equals(";"))) {
			setCurrentLine(buffer.size()-1);
			return list (1, buffer.size()-1,false);
		}
		setCurrentLine(buffer.size());
		Iterator<String> bufferIterator = buffer.iterator();
		int lineNumber = 1;
		if (bufferIterator.hasNext()) {
			StringBuilder sb = new StringBuilder();
			while (bufferIterator.hasNext()) {
				sb.append(IndexBuilder.getIndex(lineNumber, getCurrentLine()) + bufferIterator.next() + "\n"); //$NON-NLS-1$
				lineNumber++;
			}
			String listString=sb.toString();
			if (sb.toString().trim().endsWith(";")||sb.toString().trim().endsWith(SLASH))
			 listString =  StringUtils.rtrim(sb.toString()).substring(0,StringUtils.rtrim(sb.toString()).length()-1)+"\n";
			return listString;
		} else {
			return Messages.getString("Buffer.51"); //$NON-NLS-1$
		}
	}

	
	
	public void cleanSlashFromBuffer() {
		if (buffer.size() > 0 && buffer.get(buffer.size() - 1).trim().equals("/")) { //$NON-NLS-1$
			buffer.remove(buffer.size() - 1);
		}
		
	}

	/**
	 * 
	 * LIST n |L n or n |lists line n<br>
	 * 
	 * @param {@link Integer} n
	 * @return {@link String}
	 */

	public String list(int n, boolean trim) {
		if (n > 0 && n <= buffer.size()) {
			currentLine = n;
			return IndexBuilder.getIndex(n, getCurrentLine()) + buffer.get(n - 1) + "\n"; //$NON-NLS-1$
		} else {
			return Messages.getString("Buffer.53"); //$NON-NLS-1$
		}
	}

	/**
	 * LIST * |L * |lists the current line<br>
	 * 
	 * @return {@link String}
	 */

	public String listStar(boolean trim) {
		if (buffer.size() > 0) {
			return IndexBuilder.getIndex(getCurrentLine(), getCurrentLine()) + buffer.get(currentLine - 1) + "\n"; //$NON-NLS-1$
		} else {
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * LIST LAST|L LAST |lists the last line<br>
	 * 
	 * @return {@link String}
	 */

	public String listLast(boolean trim) {
		if (buffer.size() > 0) {
			setCurrentLine(buffer.size());
			return IndexBuilder.getIndex(buffer.size(), getCurrentLine()) + buffer.get(buffer.size() - 1) + "\n"; //$NON-NLS-1$
		} else {
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * LIST m n |L m n |lists a range of lines (m to n)<br>
	 * 
	 * @param {@link Integer} m
	 * @param {@link Integer} n
	 * @return {@link String}
	 */

	public String list(int m, int n, boolean trim) {
		if (m <= n && n <= buffer.size() && m > 0) {
			setCurrentLine(n);
			Iterator<String> bufferIterator = buffer.iterator();
			int lineNumber = 1;
			int index = getCurrentLine();
			StringBuilder sb = new StringBuilder();
			while (bufferIterator.hasNext()) {
				if (lineNumber >= m && lineNumber <= n) {
					if	(trim ) {
						String toPrint = bufferIterator.next();
						if (bufferIterator.hasNext()||!(buffer.get(buffer.size()-1).trim().equals(";")||buffer.get(buffer.size()-1).trim().equals("/"))) { //if there is noting left then we wont get here so the last line wont be printed
							if (lineNumber+1==getCurrentLine() && lineNumber+1==buffer.size() && getBuffer().trim().endsWith(";")) {
								index=lineNumber;
							} else {
								index=getCurrentLine();
							}
							sb.append(IndexBuilder.getIndex(lineNumber, index) + toPrint + "\n"); //$NON-NLS-1$
						} 
					} else{
						sb.append(IndexBuilder.getIndex(lineNumber, getCurrentLine()) + bufferIterator.next() + "\n"); //$NON-NLS-1$
					}
				} else {
					bufferIterator.next();
				}
				lineNumber++;
			}
			return sb.toString();
		} else {
			if (n > m && m>0 && n<= buffer.size()) {
				return Messages.getString("Buffer.59"); //$NON-NLS-1$
			}
			return Messages.getString("Buffer.61"); //$NON-NLS-1$
		}
	}

	/**
	 * DEL|(none)|deletes the current line<br>
	 * 
	 * @return
	 * */

	public String delete() {
		return delete(false);
	}


	@Override
	public String delete(boolean trim) {
		//trim is here but nothing to trim cos we're nuking the buffer
		if (buffer.size()==0)
			return Messages.getString("Buffer.65"); //$NON-NLS-1$
		if (buffer.size() >= 1) {
			buffer.remove(getCurrentLine() - 1);
		}
		return ""; //$NON-NLS-1$
	
	}

	public String delete(int lineNumber) {
		return delete(lineNumber, false);
	}
	
	@Override
	public String delete(int lineNumber, boolean trim) {
		if (lineNumber > 0 && lineNumber <= buffer.size()) {
			int n = 1;
			if (trim && buffer.size()>1 && buffer.get(buffer.size()).trim().equals("/")) {
				n = 2;
			}
			buffer.remove(lineNumber - n);
			return ""; //$NON-NLS-1$
		} else {
			return Messages.getString("Buffer.63"); //$NON-NLS-1$
		}
	}
	
	
	@Override
	public String deleteStar(boolean trim) {

		return delete(trim);
	}

	public String deleteStar() {
		return delete(false);
	}
	@Override
	public String deleteNStar(int n, boolean trim) {
		if(buffer.size()==0)
			return Messages.getString("Buffer.59"); //$NON-NLS-1$
		if (getCurrentLine() >= n) {
			return delete(n, getCurrentLine() , trim);
		}
		return Messages.getString("Buffer.64"); //$NON-NLS-1$
	}

	public String deleteNStar(int n) {
		return deleteNStar(n, false);
	}

	
	@Override
	public String deleteLast(boolean trim) {
		if (buffer.size() > 0) {
			if (trim) {
				if (buffer.get(buffer.size()-1).trim().equals("/") && buffer.size()>1) {
					buffer.remove(buffer.size() - 1);					
				}
			}
			buffer.remove(buffer.size() - 1);
			return "";
		}
		return Messages.getString("Buffer.65"); //$NON-NLS-1$
	}

	public String deleteLast() {
		return deleteLast(false);
	}

	public String delete(int m, int n) {
		return delete(m, n, false);
	}

	@Override
	public String delete(int m, int n, boolean trim) {
		if (buffer.size() > 0) {
			if (m <= n && n <= buffer.size() && m > 0) {
				int num2Delete = n - m+1;
				for (int i = 0; i < num2Delete; i++) {
					buffer.remove(m-1);
				}
				return ""; //$NON-NLS-1$
			} else {
				if (m > n) {
					return Messages.getString("Buffer.67"); //$NON-NLS-1$
				} else {
					return Messages.getString("Buffer.68"); //$NON-NLS-1$
				}
			}
		} else {
			return Messages.getString("Buffer.69"); //$NON-NLS-1$
		}
	}
	
	@Override
	public String deleteStarN(int n, boolean trim) {
		if (getCurrentLine() <= n) {
			return delete(getCurrentLine(), n);
		}
		// Really shouldnt get here...
		return Messages.getString("Buffer.67"); //$NON-NLS-1$
	}
	
	public String deleteStarN(int n) {
		return deleteStarN(n, false);
	}

	public void clear() {
		buffer.clear();
		currentLine = -1;
	}

	public void append(String string) {
		if (getCurrentLine() <= 0) {
			buffer.add(string);
			setCurrentLine(1);
		} else {
			String newline = buffer.get(currentLine - 1);
			if (newline.trim().endsWith(";")) {
				newline=newline.trim().replaceAll(";$", "");
			}
			newline += string;
			buffer.set(currentLine - 1, newline);
		}
	}

	public String change(String oldString, String newString) {
		if (currentLine < 1)
			currentLine = 1;
		if (buffer.size() > 0) {
			String newline = buffer.get(currentLine - 1);
			if (!newline.contains(oldString)) {
				return Messages.getString("Buffer.70"); //$NON-NLS-1$
			}
			String regex = oldString.replaceAll("\\+", "\\\\+").replaceAll("\\?", "\\\\?").replaceAll("\\.", "\\\\.").replaceAll("\\*", "\\\\*"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$
			newline = newline.replaceFirst(regex, newString);
			buffer.set(currentLine - 1, newline);
			return list(false); //$NON-NLS-1$
		} else {
			return Messages.getString("Buffer.80"); //$NON-NLS-1$
		}
	}

	public String change(String string) {
		if (currentLine < 1)
			currentLine = 1;
		if (buffer.size() > 0) {
			String newline = buffer.get(currentLine - 1);
			if (!newline.contains(string)) {
				return Messages.getString("Buffer.81"); //$NON-NLS-1$
			}
			String regex = string.replaceAll("\\+", "\\\\+").replaceAll("\\?", "\\\\?").replaceAll("\\.", "\\\\.").replaceAll("\\*", "\\\\*"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$
			newline = newline.replaceFirst(regex, ""); //$NON-NLS-1$
			buffer.set(currentLine - 1, newline);
			return list(false); //$NON-NLS-1$
		} else {
			return Messages.getString("Buffer.92"); //$NON-NLS-1$
		}
	}

	public String input(String string) {
		if (currentLine < 0)
			currentLine = 0;
		buffer.add(currentLine, string);
		setCurrentLine(currentLine + 1);
		return "";// Nothing for now. //$NON-NLS-1$

	}

	public Results get(String filename, boolean list, String suffix) {
		if (filename.lastIndexOf(".") == -1) { //$NON-NLS-1$
			// no suffix, hence we may need some work here if we cant find it.
			Results results = get(filename, list);
			if (!results.equals(FileSystem.Results.OK)) {
				// ok, add suffix and try again.
				String newFileNameWithSuffix = filename + "." + suffix; //$NON-NLS-1$
				return get(newFileNameWithSuffix, list);
			} else {
				return results;
			}
		} else {
			return get(filename, list);
		}
	}

	public Results get(String filename, boolean list) {
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(filename));
			BOMSkipper.skip(reader);
			return get(reader, list);
		} catch (FileNotFoundException e) {
			return Results.FILE_NOT_FOUND;
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
			}
		}
	}

	public Results get(File file, boolean list) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			BOMSkipper.skip(reader);
			return get(reader, list);
		} catch (FileNotFoundException e) {
			return Results.FILE_NOT_FOUND;
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
			}
		}

	}

	private Results get(BufferedReader reader, boolean list) {
		try {
			String text = null;
			buffer.clear();

			while ((text = reader.readLine()) != null) {
				buffer.add(text);
			}
			if (buffer.size() > 0) {
				setCurrentLine(buffer.size());
				if (buffer.get(getCurrentLine() - 1).trim().equals(SLASH)) {
					delete(getCurrentLine());
				}
			}
			setBufferSafe(buffer);

			return Results.OK;
		} catch (IOException e) {
			return Results.FILE_NOT_ACCESSIBLE;
		}
	}

	public String save(String filename, State state, String suffix) {
		String message = ""; //$NON-NLS-1$

		BufferedWriter out = null;
		FileWriter fstream = null;
		int hasExt = filename.indexOf("."); //$NON-NLS-1$
		String newFilename = filename;

		if (hasExt == -1) {
			newFilename = filename + "." + suffix; //$NON-NLS-1$
		}
		File file = new File(newFilename);
		
		if (bufferSafe ==null ||!(bufferSafe.linesInBuffer() > 0)) {
			return "SP2-0107: Nothing to save.\n";
		}

		
		try {
			switch (state) {
			case UNKNOWN:
			case CREATE:
				if (file.exists()) {
					message = MessageFormat.format(Messages.getString("Buffer.99"), new Object[] { filename }); //$NON-NLS-1$
					return message;
				}
				fstream = new FileWriter(filename, false);
				break;
			case APPEND:
				fstream = new FileWriter(filename, true);
				break;
			case REPLACE:
				if (file.exists())
					file.delete();
				fstream = new FileWriter(filename, false);
				break;
			default:
				break;
			}
			
				
			out = new BufferedWriter(fstream);
			Iterator<String> bufferIterator = bufferSafe.getBufferList().iterator();
			while (bufferIterator.hasNext()) {
				out.write(bufferIterator.next() + "\n"); //$NON-NLS-1$
			}
			out.write(SLASH + "\n");
		} catch (IOException e) {
			message = "Error: " + e.getMessage(); //$NON-NLS-1$
			System.err.println(message);
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (fstream != null) {

					fstream.close();
				}
				if (message.length() == 0) {
					switch (state) {
					case UNKNOWN:
					case CREATE:
						return MessageFormat.format(Messages.getString("Buffer.102"), new Object[] { filename }); //$NON-NLS-1$
					case APPEND:
						return MessageFormat.format(Messages.getString("Buffer.103"), new Object[] { filename }); //$NON-NLS-1$
					case REPLACE:
						return MessageFormat.format(Messages.getString("Buffer.104"), new Object[] { filename }); //$NON-NLS-1$
					}
				}
			} catch (IOException e) {
				System.err.println(Messages.getString("Buffer.105") + e.getMessage()); //$NON-NLS-1$
			}

		}
		return ""; //$NON-NLS-1$
	}

	public String getBuffer() {
		Iterator<String> bufferIterator = buffer.iterator();
		StringBuilder sb = new StringBuilder();
		while (bufferIterator.hasNext()) {
			sb.append(bufferIterator.next() + "\n"); //$NON-NLS-1$
		}
		return sb.toString();
	}

	/**
	 * Set the current line
	 * 
	 * @param lineNumber
	 *            {@link Integer}
	 */

	public String setCurrentLine(int lineNumber) {
		if (lineNumber > 0 && lineNumber <= buffer.size()) {
			currentLine = lineNumber;
		}
		return list(lineNumber,false);
	}

	public String replace(int lineNumber, String string) {
		if (buffer.size() == 0)
			buffer.add(string);
		if (lineNumber < 1) {
			buffer.set(0, string);
		} else {
			if (lineNumber-1<buffer.size())
			buffer.set(lineNumber - 1, string);
		}
		return list(lineNumber, false);
	}

	public int linesInBuffer() {
		return buffer.size();
	}

	public String getPrompt() {
		String prompt = IndexBuilder.getIndex(linesInBuffer() + 1, -1);

		return prompt;
	}

	public String getPrompt(int line) {
		String prompt = IndexBuilder.getIndex(line, -1);

		return prompt;
	}

	public boolean isEditing() {
		return editing;
	}

	public void startEditing(boolean clear) {
		if (!editing) {
			if (clear)
				clear();
		}
		editing = true;
	}

	public void startEditingAfterHistory() {
		editing = true;
	}

	public void stopEditing() {
		editing = false;
	}

	/**
	 * 
	 * LIST n * |L n * |lists line n through the current line<br>
	 * 
	 * @param {@link Integer} n
	 * @return {@link String}
	 */

	public String listNStar(int n, boolean trim) {
		if(buffer.size()==0||n>getCurrentLine())
			return Messages.getString("Buffer.59"); //$NON-NLS-1$
		else if (getCurrentLine() >= n) {
			return list(n, getCurrentLine(), trim);
		} else
		return Messages.getString("Buffer.108"); //$NON-NLS-1$

	}

	/**
	 * LIST * n |L * n |lists the current line through line n<br>
	 * 
	 * @param {@link Integer} n
	 * @return {@link String}
	 */

	public String listStarN(int n, boolean trim) {
		if (getCurrentLine() <= n) {
			return list(getCurrentLine(), n, trim);
		}
		// Really shouldnt get here...
		return ""; //$NON-NLS-1$
	}

	public void resetBuffer(String bufferString) {
		String[] lines = bufferString.split("\n"); //$NON-NLS-1$
		resetBuffer(Arrays.asList(lines));
	}

	public void resetBuffer(List<String> historyItem) {
		buffer.clear();
		buffer.addAll(historyItem);
		setCurrentLine(buffer.size());
	}

	public String getLine(int n) {
		if (n - 1 >= 0) {
			if (buffer.size()>0 && buffer.get(n - 1) != null) {

				return buffer.get(n - 1);
			}
		}
		return "";// If its empty return an empty string. //$NON-NLS-1$
	}

	/**
	 * Move the pointer to the previous element in the buffer.
	 *
	 * @return true if we successfully went to the previous element
	 */

	public boolean previous() {
		if (getCurrentLine() <= 1) {
			return false;
		}
		setCurrentLine(getCurrentLine() - 1);
		return true;
	}

	/**
	 * Move the pointer to the next element in the buffer.
	 *
	 * @return true if we successfully went to the next element
	 */

	public boolean next() {
		if (getCurrentLine() >= buffer.size()) {
			return false;
		}
		setCurrentLine(getCurrentLine() + 1);
		return true;
	}

	public int size() {
		return buffer.size();
	}

	public ArrayList<String> getBufferList() {
		return buffer;
	}

	public boolean isEmpty() {
		return !(buffer.size() > 0);
	}

	public void setBufferSafe(ArrayList<String> bufferList) {
		if (bufferList.size() > 0 && bufferList.get(bufferList.size() - 1).trim().equals("/")) { //$NON-NLS-1$
			bufferList.remove(bufferList.size() - 1);
		}
		getBufferSafe().resetBuffer(bufferList);
	}

	public IBuffer getBufferSafe() {
		if (this.bufferSafe == null) {
			this.bufferSafe = new Buffer();
		}
		return this.bufferSafe;
	}

	@Deprecated
	// using getBuffer() instead
	/*
	 * Grudgingly accepted and workarounded confusing idea that it gives the
	 * content without the last line Example: copy-and-paste
	 * 
	 * first line second line
	 * 
	 * into empty command line. The buffer content is just first line.
	 */
	public String getCurrentBufferState(String line) {
		replace(getCurrentLine(), line);
		return getBuffer().replace("\n", " ").replace("\r", " "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	@Deprecated
	public int getCursorLocation(int cursor) {
		int count = 0;
		for (int i = 0; i < getCurrentLine() - 1; i++) {
			count = count + buffer.get(i).length();
		}
		// Now add cursor locationof Current line
		count = count + cursor;
		return 0;
	}

	/**
	 * @return the _readLineBuffer
	 */
	public int getCursor() {
		return _readLineBuffer.cursor;
	}

	public String getCursorContents() {
		return _readLineBuffer.buffer.toString();
	}

	/**
	 * @param _readLineBuffer
	 *            the _readLineBuffer to set
	 */
	public void setReadLineBuffer(Object _readLineBuffer) {
		this._readLineBuffer = (CursorBuffer) _readLineBuffer;
	}

	@Override
	public String getBufferString() {
		Iterator<String> bufferIterator = buffer.iterator();
		StringBuilder sb = new StringBuilder();
		while (bufferIterator.hasNext()) {
			sb.append(bufferIterator.next());
			if (bufferIterator.hasNext()) {
				sb.append("\n"); //$NON-NLS-1$
			}
		}
		return sb.toString();
	}

	@Override
	public int getBufferCursor(int currentLine, int cursor) {
		int bufPos=0;
		int line=0;
		Iterator<String> bufferIterator = buffer.iterator();
		while (bufferIterator.hasNext()) {
			line++;
			if (line!=getCurrentLine()) {
			bufPos += bufferIterator.next().length();
			} else {
				bufPos += cursor;
				return bufPos+currentLine-1;
			}
		}
		return 0;
	}

	@Override
	public void setTermLine(int line, int termHeight) { 
		if (line<=1) 
			termLine=1;
		else 
			termLine=line;
		if (termLine>termHeight) {
			termLine=termHeight;
		}
		if ((termHeight - termLine) > (buffer.size()-currentLine)) {
			termLine=termHeight;
		} 
	}

	@Override
	public int getTermLine() {
		return termLine;
	}
}
