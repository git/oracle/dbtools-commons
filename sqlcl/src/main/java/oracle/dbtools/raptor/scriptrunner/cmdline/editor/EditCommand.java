/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline.editor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.common.utils.FileUtils;
import oracle.dbtools.plusplus.IBuffer;
import oracle.dbtools.raptor.console.SQLPlusConsoleReader;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * Handle Edit in SQLPLUS commands.
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=EditCommand.java"
 *         >Barry McGillin</a>
 *
 */
//Explicitly turning off this comman for REST Enabled SQL Service.
@Restricted(level=Restricted.Level.R1)
public class EditCommand extends CommandListener implements IHelp {
	private static final String CMD = "edit"; //$NON-NLS-1$
	private static final String AFIEDT = "afiedt.buf"; //$NON-NLS-1$
	private static final String EDITOR = "_EDITOR"; //$NON-NLS-1$
	private static final String INLINE = "inline"; //$NON-NLS-1$
	private static final String DOT = "."; //$NON-NLS-1$
	private ScriptRunnerContext ctx;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java
	 * .sql.Connection,
	 * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override

	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
	   if(ctx.getRestrictedLevel().getLevel()>=1){
	      ctx.write(MessageFormat.format(Messages.getString("EditCommand.4"), CMD)); //$NON-NLS-1$
	      return true;  
	    }
		this.ctx = ctx;
		StringTokenizer st = new StringTokenizer(cmd.getSql());
		// Default AFIEDT
		String filename = ""; //$NON-NLS-1$

		String editfile = (String) ctx
				.getProperty(ScriptRunnerContext.EDITFILE);
		if (editfile != null)
			filename = editfile;
		if (st.countTokens() == 1) {
			if (filename.lastIndexOf(DOT) == -1) {
				filename = filename + DOT
						+ ctx.getProperty(ScriptRunnerContext.SUFFIX);
			}
			File f = new File(ctx.prependCD(filename));
			return processFile(cmd, ctx.prependCD(filename), true);
		} else if (st.countTokens() == 2) {
			st.nextToken(); // Skip edit, take next as filename
			String token2 = st.nextToken();
			if (token2.trim().equalsIgnoreCase(INLINE)) {
				processInternalEditor();
				return true;
			} else if (token2.lastIndexOf(DOT) == -1) {
				filename = token2 + DOT
						+ ctx.getProperty(ScriptRunnerContext.SUFFIX);
			} else {
				filename = token2;
			}
			File f = new File(ctx.prependCD(filename));
			return processFile(cmd, ctx.prependCD(filename));
		} else if (st.countTokens() > 3) {
			ctx.write(Messages.getString("EditCommand.5")); //$NON-NLS-1$
			ctx.write(Messages.getString("EditCommand.6")); //$NON-NLS-1$
			return true;
		}
		return processFile(cmd,
				System.getProperty("user.home") + File.separator + AFIEDT); //$NON-NLS-1$
	}

	private boolean processFile(ISQLCommand cmd, String filename) {
		return processFile(cmd, filename, false);
	}

	private boolean processFile(ISQLCommand cmd, String filename,
			boolean editBuffer) {
		IBuffer buf = ctx.getSQLPlusBuffer().getBufferSafe();
		if (cmd.getSql().trim().equalsIgnoreCase("edit") && buf == null || (buf != null && buf.size() == 0)) { //$NON-NLS-1$
			if (ctx.getTopLevel())
				ctx.write(buf.getBufferSafe().list(false));
			else {
				ctx.write(Messages.getString("EditCommand.2")); //$NON-NLS-1$
			}
			return true;
		}
		try {
			File file = new File(filename);
			if (!file.exists() || editBuffer) {// create a new file and write
												// the contents of the buffer to
												// it
				FileWriter f = new FileWriter(filename);
				f.write(buf.getBuffer());
				f.flush();
				f.close();
			}
			if (ctx.getMap().get(EDITOR).toLowerCase().equals("inline")) { //$NON-NLS-1$
				processInternalEditor();
			} else {
				processExternalEditor(filename);
				if (file.exists()) {
					FileReader fr = new FileReader(filename);
					BufferedReader br = new BufferedReader(fr);
					buf.clear();
					for (String line; (line = br.readLine()) != null;) {
						buf.add(line);
					}
					// Set the safe as any operations will be using this.
					buf.setBufferSafe(buf.getBufferList());
					br.close();
					ctx.write(buf.getBufferSafe().list(false));
				}
			}

		} catch (IOException e) {
			ctx.write(e.getMessage() + "\n"); //$NON-NLS-1$
		}
		return true;
	}

	private void processInternalEditor() {
		SQLPlusConsoleReader reader = (SQLPlusConsoleReader) ctx
				.getSQLPlusConsoleReader();
		reader.setRedit(true);
	}

	private void processExternalEditor(String filename) {
		String editor = ctx.getMap().get(EDITOR);
		ProcessBuilder builder = null;
		if ((editor != null) && (editor.equalsIgnoreCase("wordpad")) && //$NON-NLS-1$ 
				(System.getProperty("os.name").startsWith("Windows"))) { //$NON-NLS-1$  //$NON-NLS-2$ 
			builder = new ProcessBuilder(
					"cmd", "/C", "start", "/WAIT", "wordpad", filename); //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$  
		} else {
			//Use the command and split it as there may be options involved there.
			List<String> list = new ArrayList<String>();
			Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(
					editor);
			while (m.find())
				list.add(m.group(1).replace("\"", ""));
			list.add(filename);

			builder = new ProcessBuilder(list.toArray(new String[list.size()]));
		}
		builder.directory(new File(FileUtils.getCWD(ctx))); //$NON-NLS-1$
		builder.redirectInput(ProcessBuilder.Redirect.INHERIT);
		builder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
		Process process;
		try {
			process = builder.start();
			process.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// Wait for exception
			e.printStackTrace();
		}
		// At this point, file is rewritten and lets return back.
	}

	/**
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java
	 *      .sql.Connection,
	 *      oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 *      oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
		if (ctx.isCommandLine()) {
			ctx.getSQLPlusConsoleReader().pauseReader(true);
		}
	}

	/**
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql
	 *      .Connection,
	 *      oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 *      oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx,
			ISQLCommand cmd) {
		if (ctx.isCommandLine()) {
			ctx.getSQLPlusConsoleReader().pauseReader(false);
		}
	}

	@Override
	public String getCommand() {
		return "EDIT"; //$NON-NLS-1$
	}

	@Override
	public String getHelp() {
		return CommandsHelp.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return true;
	}

}
