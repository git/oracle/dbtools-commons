/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline.editor;

import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.Arrays;
import java.util.Stack;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiOutputStream;

import oracle.dbtools.raptor.console.clone.ConsoleKeys;
import oracle.dbtools.raptor.console.clone.DbtoolsNonBlockingInputStream;
import jline.Terminal;
import jline.TerminalFactory;
import jline.TerminalFactory.Flavor;
import jline.NoInterruptUnixTerminal;
import jline.console.ConsoleReader;
import jline.console.CursorBuffer;
import jline.console.KeyMap;
import jline.console.Operation;
import jline.console.UserInterruptException;
import jline.internal.Configuration;
import jline.internal.InputStreamReader;
import jline.internal.Log;
import jline.internal.Urls;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=FullScreenConsoleReader.java"
 *         >Barry McGillin</a>
 *
 */
public class FullScreenConsoleReader extends ConsoleReader {
	
	  public final static char ESC = '\u001b'; // (char) 27;
	  public static final String ANSI_CLS         = ESC + "[2J";
	  public static final String ANSI_HOME        = ESC + "[H"; // 0,0
	  public static final String ANSI_HEAD        = ESC + "[1G"; // Start of current line, position 1
	  public static final String ANSI_UP = ESC+"[P1A";
	  public static final String ANSI_DOWN = ESC+"[P1B";
	private String appName;
	private String encoding;
	private final Terminal terminal;
	private final Writer out;
	/*
	 * The reader and the nonBlockingInput go hand-in-hand. The reader wraps the
	 * nonBlockingInput, but we have to retain a handle to it so that we can
	 * shut down its blocking read thread when we go away.
	 */
	private DbtoolsNonBlockingInputStream in;
	private long escapeTimeout;
	private Reader reader;
	private URL inputrcUrl;
	private ConsoleKeys consoleKeys;
	private Character mask;
	private static final int KEYMAP_LENGTH = 256;
	private boolean handleUserInterrupt = false;
	private StringBuffer searchTerm = null;
	private String previousSearchTerm = "";
	private int searchIndex = -1;
	private String prompt;
	private final CursorBuffer buf = new CursorBuffer();
	private boolean copyPasteDetection = false;
	private int promptLen;

	public static KeyMap keymap() {
		Object[] map = new Object[KEYMAP_LENGTH];

		return null;

	}

	public FullScreenConsoleReader() throws Exception {
		this(null, new FileInputStream(FileDescriptor.in), System.out, null);
	}

	public FullScreenConsoleReader(InputStream in, OutputStream out) throws Exception {
		this(null, in, out, null);
	}

	public FullScreenConsoleReader(InputStream in, OutputStream out, Terminal term) throws Exception {
		this(null, in, out, term);
	}

	public FullScreenConsoleReader(String appName, InputStream in, OutputStream out, Terminal term) throws Exception {
		this(appName, in, out, term, null);
	}

	public FullScreenConsoleReader(String appName, InputStream in, OutputStream out, Terminal term, String encoding) throws Exception {
		this.appName = appName != null ? appName : "JLine";
		this.encoding = encoding != null ? encoding : Configuration.getEncoding();
		this.terminal = term != null ? term : TerminalFactory.getFlavor(Flavor.UNIX);
		String outEncoding = terminal.getOutputEncoding() != null ? terminal.getOutputEncoding() : this.encoding;
		this.out = new OutputStreamWriter(terminal.wrapOutIfNeeded(out), outEncoding);
		setInput(in);
		this.inputrcUrl = getInputRc();
		consoleKeys = new ConsoleKeys(this.appName, inputrcUrl);
	}

	/**
	 * Read the next line and return the contents of the buffer.
	 */
	public String readLine() throws IOException {
		return readLine((String) null);
	}

	/**
	 * Read the next line with the specified character mask. If null, then
	 * characters will be echoed. If 0, then no characters will be echoed.
	 */
	public String readLine(final Character mask) throws IOException {
		return readLine(null, mask);
	}

	public String readLine(final String prompt) throws IOException {
		return readLine(prompt, null);
	}

	void setInput(final InputStream in) throws IOException {
		this.escapeTimeout = Configuration.getLong(JLINE_ESC_TIMEOUT, 100);
		/*
		 * This is gross and here is how to fix it. In getCurrentPosition() and
		 * getCurrentAnsiRow(), the logic is disabled when running unit tests
		 * and the fact that it is a unit test is determined by knowing if the
		 * original input stream was a ByteArrayInputStream. So, this is our
		 * test to do this. What SHOULD happen is that the unit tests should
		 * pass in a terminal that is appropriately configured such that
		 * whatever behavior they expect to happen (or not happen) happens (or
		 * doesn't).
		 * 
		 * So, TODO, get rid of this and fix the unit tests.
		 */
		boolean nonBlockingEnabled = escapeTimeout > 0L && terminal.isSupported() && in != null;

		/*
		 * If we had a non-blocking thread already going, then shut it down and
		 * start a new one.
		 */
		if (this.in != null) {
			this.in.shutdown();
		}

		final InputStream wrapped = terminal.wrapInIfNeeded(in);

		this.in = DbtoolsNonBlockingInputStream.getInstance(wrapped,nonBlockingEnabled);
		this.reader = new InputStreamReader(this.in, encoding);
	}

	private URL getInputRc() throws IOException {
		String path = Configuration.getString(JLINE_INPUTRC);
		if (path == null) {
			File f = new File(Configuration.getUserHome(), INPUT_RC);
			if (!f.exists()) {
				f = new File(DEFAULT_INPUT_RC);
			}
			return f.toURI().toURL();
		} else {
			return Urls.create(path);
		}
	}

	public String readLine(String prompt, final Character mask) throws IOException {
		// prompt may be null
		// mask may be null

		/*
		 * This is the accumulator for VI-mode repeat count. That is, while in
		 * move mode, if you type 30x it will delete 30 characters. This is
		 * where the "30" is accumulated until the command is struck.
		 */
		int repeatCount = 0;

		// FIXME: This blows, each call to readLine will reset the console's
		// state which doesn't seem very nice.
		this.mask = mask;
		if (prompt != null) {
			setPrompt(prompt);
		} else {
			prompt = getPrompt();
		}

		try {
			// if (!terminal.isSupported()) {
			// beforeReadLine(prompt, mask);
			// }

			if (prompt != null && prompt.length() > 0) {
				out.write(prompt);
				out.flush();
			}


			String originalPrompt = this.prompt;

			boolean success = true;

			StringBuilder sb = new StringBuilder();
			Stack<Character> pushBackChar = new Stack<Character>();
			while (true) {
				int c = pushBackChar.isEmpty() ? readCharacter() : pushBackChar.pop();
				if (c == -1) {
					return null;
				}
				sb.appendCodePoint(c);

				Object o = getKeys().getBound(sb);

				if (o == Operation.DO_LOWERCASE_VERSION) {
					sb.setLength(sb.length() - 1);
					sb.append(Character.toLowerCase((char) c));
					o = getKeys().getBound(sb);
				}

				/*
				 * A KeyMap indicates that the key that was struck has a number
				 * of keys that can follow it as indicated in the map. This is
				 * used primarily for Emacs style ESC-META-x lookups. Since more
				 * keys must follow, go back to waiting for the next key.
				 */
				if (o instanceof KeyMap) {
					/*
					 * The ESC key (#27) is special in that it is ambiguous
					 * until you know what is coming next. The ESC could be a
					 * literal escape, like the user entering vi-move mode, or
					 * it could be part of a terminal control sequence. The
					 * following logic attempts to disambiguate things in the
					 * same fashion as regular vi or readline.
					 * 
					 * When ESC is encountered and there is no other pending
					 * character in the pushback queue, then attempt to peek
					 * into the input stream (if the feature is enabled) for
					 * 150ms. If nothing else is coming, then assume it is not a
					 * terminal control sequence, but a raw escape.
					 */
					if (c == 27 && pushBackChar.isEmpty() && in.isNonBlockingEnabled() && in.peek(escapeTimeout) == -2) {
						o = ((KeyMap) o).getAnotherKey();
						if (o == null || o instanceof KeyMap) {
							continue;
						}
						sb.setLength(0);
					} else {
						continue;
					}
				}

				/*
				 * If we didn't find a binding for the key and there is more
				 * than one character accumulated then start checking the
				 * largest span of characters from the beginning to see if there
				 * is a binding for them.
				 * 
				 * For example if our buffer has ESC,CTRL-M,C the getBound()
				 * called previously indicated that there is no binding for this
				 * sequence, so this then checks ESC,CTRL-M, and failing that,
				 * just ESC. Each keystroke that is pealed off the end during
				 * these tests is stuffed onto the pushback buffer so they won't
				 * be lost.
				 * 
				 * If there is no binding found, then we go back to waiting for
				 * input.
				 */
				while (o == null && sb.length() > 0) {
					c = sb.charAt(sb.length() - 1);
					sb.setLength(sb.length() - 1);
					Object o2 = getKeys().getBound(sb);
					if (o2 instanceof KeyMap) {
						o = ((KeyMap) o2).getAnotherKey();
						if (o == null) {
							continue;
						} else {
							pushBackChar.push((char) c);
						}
					}
				}

				if (o == null) {
					continue;
				}
				Log.trace("Binding: ", o);

				// Handle macros
				if (o instanceof String) {
					String macro = (String) o;
					for (int i = 0; i < macro.length(); i++) {
						pushBackChar.push(macro.charAt(macro.length() - 1 - i));
					}
					sb.setLength(0);
					continue;
				}

				// Handle custom callbacks
				if (o instanceof ActionListener) {
					((ActionListener) o).actionPerformed(null);
					sb.setLength(0);
					continue;
				}

				// Search mode.
				//
				// Note that we have to do this first, because if there is a
				// command
				// not linked to a search command, we leave the search mode and
				// fall
				// through to the normal state.

				if (true) {
					/*
					 * If this is still false at the end of the switch, then we
					 * reset our repeatCount to 0.
					 */
					boolean isArgDigit = false;

					/*
					 * Every command that can be repeated a specified number of
					 * times, needs to know how many times to repeat, so we
					 * figure that out here.
					 */
					int count = (repeatCount == 0) ? 1 : repeatCount;

					/*
					 * Default success to true. You only need to explicitly set
					 * it if something goes wrong.
					 */
					success = true;

					if (o instanceof Operation) {
						Operation op = (Operation) o;
						/*
						 * Current location of the cursor (prior to the
						 * operation). These are used by vi *-to operation (e.g.
						 * delete-to) so we know where we came from.
						 */
						int cursorStart = buf.cursor;

						/*
						 * If we are on a "vi" movement based operation, then we
						 * need to restrict the sets of inputs pretty heavily.
						 */

						switch (op) {
						case COMPLETE: // tab
							// There is an annoyance with tab completion in that
							// sometimes the user is actually pasting input in
							// that
							// has physical tabs in it. This attempts to look at
							// how
							// quickly a character follows the tab, if the
							// character
							// follows *immediately*, we assume it is a tab
							// literal.
							boolean isTabLiteral = false;
							if (copyPasteDetection && c == 9 && (!pushBackChar.isEmpty() || (in.isNonBlockingEnabled() && in.peek(escapeTimeout) != -2))) {
								isTabLiteral = true;
							}

							if (!isTabLiteral) {
								success = complete();
							} else {
								putString(sb);
							}
							break;

						case POSSIBLE_COMPLETIONS:
							printCompletionCandidates();
							break;

						case BEGINNING_OF_LINE:
							success = setCursorPosition(0);
							break;

						case YANK:
							success = yank();
							break;

						case YANK_POP:
							success = yankPop();
							break;

						case KILL_LINE: // CTRL-K
							success = killLine();
							break;

						case KILL_WHOLE_LINE:
							success = setCursorPosition(0) && killLine();
							break;

						case CLEAR_SCREEN: // CTRL-L
							success = clearScreen();
							redrawLine();
							break;

						case OVERWRITE_MODE:
							buf.setOverTyping(!buf.isOverTyping());
							break;

						case SELF_INSERT:
							putString(sb);
							break;

						case ACCEPT_LINE:
							return accept();

						case INTERRUPT:
							if (handleUserInterrupt) {
								println();
								flush();
								String partialLine = buf.buffer.toString();
								buf.clear();
								throw new UserInterruptException(partialLine);
							}
							break;

						case BACKWARD_WORD:
							success = word(false);
							break;

						case FORWARD_WORD:
							success = word(true);
							break;

						case PREVIOUS_HISTORY:
							success = move(false);
							break;

						case NEXT_HISTORY:
							success = move(true);
							break;

						case BACKWARD_DELETE_CHAR: // backspace
							success = backspace();
							break;

						case EXIT_OR_DELETE_CHAR:
							if (buf.buffer.length() == 0) {
								return null;
							}
							success = deleteCurrentCharacter();
							break;

						case DELETE_CHAR: // delete
							success = deleteCurrentCharacter();
							break;

						case BACKWARD_CHAR:
							success = moveCursor(-(count)) != 0;
							break;

						case FORWARD_CHAR:
							success = moveCursor(count) != 0;
							break;

						case UNIX_LINE_DISCARD:
							success = resetLine();
							break;

						case END_OF_LINE:
							success = moveToEnd();
							break;

						case TAB_INSERT:
							putString("\t");
							break;

						default:
							break;
						}
					}
				}
				if (!success) {
					beep();
				}
				sb.setLength(0);
				flush();
			}
		} finally {
			if (!terminal.isSupported()) {
				// afterReadLine();
			}
		}
	}

	private boolean word(boolean b) {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean move(boolean down) throws IOException {
		String DOWN="P1A";
		String UP="P1B";
		if (down) {

			out.write(Ansi.ansi().cursorDown(1).toString());
			out.flush();
		} else {
			out.write(Ansi.ansi().cursorUp(1).toString());
			out.flush();
		}
		return false;
	}

	/*
	 * Enables or disables copy and paste detection. The effect of enabling this
	 * this setting is that when a tab is received immediately followed by
	 * another character, the tab will not be treated as a completion, but as a
	 * tab literal.
	 * 
	 * @param onoff true if detection is enabled
	 */
	public void setCopyPasteDetection(final boolean onoff) {
		copyPasteDetection = onoff;
	}

	/**
	 * @return true if copy and paste detection is enabled.
	 */
	public boolean isCopyPasteDetectionEnabled() {
		return copyPasteDetection;
	}

	private boolean deleteCurrentCharacter() throws IOException {
		if (buf.length() == 0 || buf.cursor == buf.length()) {
			return false;
		}

		buf.buffer.deleteCharAt(buf.cursor);
		drawBuffer(1);
		return true;
	}

	/**
	 * Redraw the rest of the buffer from the cursor onwards. This is necessary
	 * for inserting text into the buffer.
	 *
	 * @param clear
	 *            the number of characters to clear after the end of the buffer
	 */
	private void drawBuffer(final int clear) throws IOException {
		// debug ("drawBuffer: " + clear);
		if (buf.cursor == buf.length() && clear == 0) {
		} else {
			char[] chars = buf.buffer.substring(buf.cursor).toCharArray();
			if (mask != null) {
				Arrays.fill(chars, mask);
			}
			if (terminal.hasWeirdWrap()) {
				// need to determine if wrapping will occur:
				int width = terminal.getWidth();
				int pos = getCursorPosition();
				for (int i = 0; i < chars.length; i++) {
					print(chars[i]);
					if ((pos + i + 1) % width == 0) {
						print(32); // move cursor to next line by printing dummy
									// space
						print(13); // CR / not newline.
					}
				}
			} else {
				print(chars);
			}
			clearAhead(clear, chars.length);
			if (terminal.isAnsiSupported()) {
				if (chars.length > 0) {
					back(chars.length);
				}
			} else {
				back(chars.length);
			}
		}
		if (terminal.hasWeirdWrap()) {
			int width = terminal.getWidth();
			// best guess on whether the cursor is in that weird location...
			// Need to do this without calling ansi cursor location methods
			// otherwise it breaks paste of wrapped lines in xterm.
			if (getCursorPosition() > 0 && (getCursorPosition() % width == 0) && buf.cursor == buf.length() && clear == 0) {
				// the following workaround is reverse-engineered from looking
				// at what bash sent to the terminal in the same situation
				print(32); // move cursor to next line by printing dummy space
				print(13); // CR / not newline.
			}
		}
	}

	/**
	 * Redraw the rest of the buffer from the cursor onwards. This is necessary
	 * for inserting text into the buffer.
	 */
	private void drawBuffer() throws IOException {
		drawBuffer(0);
	}

	int getCursorPosition() {
		// FIXME: does not handle anything but a line with a prompt absolute
		// position
		return promptLen + buf.cursor;
	}

	/**
	 * Output the specified character to the output stream without manipulating
	 * the current buffer.
	 */
	private void print(final int c) throws IOException {
		if (c == '\t') {
			char chars[] = new char[TAB_WIDTH];
			Arrays.fill(chars, ' ');
			out.write(chars);
			return;
		}

		out.write(c);
	}

	/**
	 * Output the specified characters to the output stream without manipulating
	 * the current buffer.
	 */
	private void print(final char... buff) throws IOException {
		int len = 0;
		for (char c : buff) {
			if (c == '\t') {
				len += TAB_WIDTH;
			} else {
				len++;
			}
		}

		char chars[];
		if (len == buff.length) {
			chars = buff;
		} else {
			chars = new char[len];
			int pos = 0;
			for (char c : buff) {
				if (c == '\t') {
					Arrays.fill(chars, pos, pos + TAB_WIDTH, ' ');
					pos += TAB_WIDTH;
				} else {
					chars[pos] = c;
					pos++;
				}
			}
		}

		out.write(chars);
	}

	/**
	 * Clear ahead the specified number of characters without moving the cursor.
	 *
	 * @param num
	 *            the number of characters to clear
	 * @param delta
	 *            the difference between the internal cursor and the screen
	 *            cursor - if > 0, assume some stuff was printed and weird wrap
	 *            has to be checked
	 */
	private void clearAhead(final int num, int delta) throws IOException {
		if (num == 0) {
			return;
		}

		if (terminal.isAnsiSupported()) {
			int width = terminal.getWidth();
			int screenCursorCol = getCursorPosition() + delta;
			// clear current line
			printAnsiSequence("K");
			// if cursor+num wraps, then we need to clear the line(s) below too
			int curCol = screenCursorCol % width;
			int endCol = (screenCursorCol + num - 1) % width;
			int lines = num / width;
			if (endCol < curCol)
				lines++;
			for (int i = 0; i < lines; i++) {
				printAnsiSequence("B");
				printAnsiSequence("2K");
			}
			for (int i = 0; i < lines; i++) {
				printAnsiSequence("A");
			}
			return;
		}

		// print blank extra characters
		print(' ', num);

		// we need to flush here so a "clever" console doesn't just ignore the
		// redundancy
		// of a space followed by a backspace.
		// flush();

		// reset the visual cursor
		back(num);

		// flush();
	}

	private void printAnsiSequence(String sequence) throws IOException {
		print(27);
		print('[');
		print(sequence);
		flush(); // helps with step debugging
	}

	private void print(final char c, final int num) throws IOException {
		if (num == 1) {
			print(c);
		} else {
			char[] chars = new char[num];
			Arrays.fill(chars, c);
			print(chars);
		}
	}

	public void setPrompt(final String prompt) {
		this.prompt = prompt;
		this.promptLen = ((prompt == null) ? 0 : stripAnsi(lastLine(prompt)).length());
	}

	private String stripAnsi(String str) {
		if (str == null)
			return "";
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			AnsiOutputStream aos = new AnsiOutputStream(baos);
			aos.write(str.getBytes());
			aos.flush();
			return baos.toString();
		} catch (IOException e) {
			return str;
		}
	}

	private String lastLine(String str) {
		if (str == null)
			return "";
		int last = str.lastIndexOf("\n");

		if (last >= 0) {
			return str.substring(last + 1, str.length());
		}

		return str;
	}
	
	
}
