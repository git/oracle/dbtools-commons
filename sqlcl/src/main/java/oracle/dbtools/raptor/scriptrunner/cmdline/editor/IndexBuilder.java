/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline.editor;


/**
 * In SQL*Plus we need an index to show the user which line number
 * 
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=IndexBuilder.java"
 *         >Barry McGillin</a>
 *
 */
public class IndexBuilder {

	private static int padding = 1;
	private static String currentLineMarker = "*";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(IndexBuilder.getIndex(1, 10));
		System.out.println(IndexBuilder.getIndex(9, 10));
		System.out.println(IndexBuilder.getIndex(10, 10));
		System.out.println(IndexBuilder.getIndex(100, 10));
		System.out.println(IndexBuilder.getIndex(999, 10));
		System.out.println(IndexBuilder.getIndex(1001, 10));
		System.out.println(IndexBuilder.getIndex(10001, 10));
		System.out.println(IndexBuilder.getIndex(10001, 10001));
	}

	public static String getIndex(int lineNumber, int currentLine) {
		// Do the magic to create a line number.
		return getFiller(lineNumber) + lineNumber
				+ getCurrentLineMarker(lineNumber, currentLine) + getPad();
	}

	public static String getIndex(int lineNumber) {
		return getFiller(lineNumber) + lineNumber + getPad();
	}
	private static String getPad() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < padding; i++) {
			sb.append(" ");
		}
		return sb.toString();
	}

	private static String getCurrentLineMarker(int lineNumber, int currentLine) {
		if (lineNumber == currentLine) {
			return currentLineMarker;
		} else {
			return " ";
		}
	}

	private static String getFiller(int lineNumber) {
		String padding = "";
		int numChars = (lineNumber + padding).length();
		if (numChars < 3) {
			if (numChars == 1) {
				return "  ";
			} else {
				return " ";
			}
		}
		return "";
	}

	public static void setPad(int size) {
		padding = size;
	}
}
