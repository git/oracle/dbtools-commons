/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline.handlers;

import oracle.dbtools.raptor.console.clone.DbtoolsConsoleReader;
import oracle.dbtools.raptor.newscriptrunner.IGetHandlePauseProvider;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.SetSpool;
import oracle.dbtools.raptor.utils.ExceptionHandler;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=StandAloneHandleSetPauseProvider.java"
 *         >Barry McGillin</a>
 *
 */
public class SQLCliHandleSetPauseProvider implements IGetHandlePauseProvider {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.newscriptrunner.IGetHandlePauseProvider#handlePause
	 * (oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handlePause(ScriptRunnerContext ctx, ISQLCommand cmd) {
		Integer cnt = (Integer) ctx.getProperty(ScriptRunnerContext.CURRENT_CMD_COUNT);
		DbtoolsConsoleReader reader = (DbtoolsConsoleReader) ctx.getProperty(ScriptRunnerContext.JLINE_SETTING);
		if (reader != null) {
			String prompt = ""; //$NON-NLS-1$
			if (ctx.getProperty(ScriptRunnerContext.SET_PAUSE_VALUE) != null) {
				prompt = (String) ctx.getProperty(ScriptRunnerContext.SET_PAUSE_VALUE);
			}

			String haveASpace = "";

			if (ctx.getProperty(ScriptRunnerContext.SILENT) == null) {
				haveASpace = prompt;
			}
			String fromConsole = null;
			try {
				if (ctx.getProperty(ScriptRunnerContext.SILENT) == null) {
					fromConsole = reader.readLineSimple(haveASpace);
				} else {
					fromConsole = reader.readLineSimple(prompt);
				}
				// set pause output is put to spool file.
				SetSpool.writeToExisting(haveASpace + "\n", ctx); //$NON-NLS-1$
			} catch (Exception e) {
				ExceptionHandler.handleException(e);
			}
			if (fromConsole == null || fromConsole.equals("N") || fromConsole.equals("n")) {
				ctx.setInterrupted(true);
			}
			return false;
		}
		// will ignore this return.
		return false;
	}

}
