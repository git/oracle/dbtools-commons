/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline.handlers;

import jline.console.ConsoleReader;
import oracle.dbtools.raptor.console.clone.DbtoolsConsoleReader;
import oracle.dbtools.raptor.newscriptrunner.IGetPromptedFieldProvider;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunner;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.WrapListenBufferOutputStream;
import oracle.dbtools.raptor.utils.ExceptionHandler;
import static org.fusesource.jansi.internal.CLibrary.STDIN_FILENO;
import static org.fusesource.jansi.internal.CLibrary.isatty;


/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=StandAlonePromptedAcceptProvider.java">Barry McGillin</a> 
 *
 */
public class SQLCliPromptedAcceptProvider implements IGetPromptedFieldProvider {

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.IGetPromptedFieldProvider#getPromptedField(oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, java.lang.String, boolean)
	 */
	@Override
	public String getPromptedField(ScriptRunnerContext ctx, String prompt, boolean hide) {
		DbtoolsConsoleReader reader=(DbtoolsConsoleReader) ctx.getProperty(ScriptRunnerContext.JLINE_SETTING);
	    if (reader!=null) {
            if (prompt==null) {
                prompt=""; //$NON-NLS-1$
            }
            String haveASpace = "";
            
            if (ctx.getProperty(ScriptRunnerContext.SILENT)==null) {
            	haveASpace=prompt;
	        if ((prompt!=null)&&(prompt.length()>0)&&!prompt.endsWith(" ")) { //$NON-NLS-1$
	            haveASpace+=" "; //$NON-NLS-1$
	        }
            }
	        String fromConsole=null;
	        try{
	        	if (ctx.getProperty(ScriptRunnerContext.SILENT)==null) {
					if (hide) {
						String extra=""; //$NON-NLS-1$ 
				        if (ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE)!=null && Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_CLASSIC_MODE).toString()) ) {
				        	fromConsole = reader.readLineSimple(haveASpace, ' ', isMultiple(true, ctx));
				        } else {
				        	fromConsole = reader.readLineSimple(haveASpace, '*', isMultiple(true, ctx));
				        	if (fromConsole!=null) {
				        		extra=fromConsole.replaceAll(".","*");  //$NON-NLS-1$  //$NON-NLS-2$
				        	}
				        }
				        WrapListenBufferOutputStream existing = (WrapListenBufferOutputStream) ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
				        if ((existing != null)) {
				        	//newline required at end.
				        	ctx.getOutputStream().flush();
				        	existing.write((haveASpace+extra+"\n").getBytes("UTF8"));  //$NON-NLS-1$  //$NON-NLS-2$
				        }
					} else {
						fromConsole = reader.readLineSimple(haveASpace);
				        WrapListenBufferOutputStream existing = (WrapListenBufferOutputStream) ctx.getProperty(ScriptRunnerContext.SPOOLOUTBUFFER);
				        if ((existing != null)) {
				        	//newline required at end.
				        	ctx.getOutputStream().flush();
				        	if (fromConsole==null) {
				        	    existing.write((haveASpace+"\n").getBytes("UTF8"));  //$NON-NLS-1$  //$NON-NLS-2$
				        	} else {
				        		existing.write((haveASpace+fromConsole+"\n").getBytes("UTF8"));  //$NON-NLS-1$  //$NON-NLS-2$
				        	}
				        }
					}
	        	}else {
	        		fromConsole=reader.readLineSimple(haveASpace, ' ',isMultiple(hide, ctx));
            	}
	        } catch (Exception e) {
	            ExceptionHandler.handleException(e);
	        }
	        return fromConsole;
	    }
		return null;
	}
    boolean isMultiple(boolean isHide, ScriptRunnerContext ctx) {
    	if ((isHide)&&(isatty(STDIN_FILENO)==0)
    			&&(ctx.getProperty(ScriptRunnerContext.NOMULTILINEACCEPT)==null)) {//only check isatty when hide is on
    		return true;
    	}
    	return false;
    }
}
