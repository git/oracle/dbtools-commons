/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline.handlers;

import oracle.dbtools.raptor.console.clone.DbtoolsConsoleReader;
import oracle.dbtools.raptor.newscriptrunner.ConnectionDetails;
import oracle.dbtools.raptor.newscriptrunner.IGetPromptedConnectFieldsProvider;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.utils.ExceptionHandler;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=StandAlonePromptedConnectFieldsProvider.java">Barry McGillin</a> 
 *
 */
public class SQLCliPromptedConnectFieldsProvider implements IGetPromptedConnectFieldsProvider {

    boolean ctrld=false;
	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.IGetPromptedConnectFieldsProvider#get3Fields(oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, java.lang.String, java.lang.String, java.lang.String, boolean)
	 */
    @Override
	public ConnectionDetails get3Fields(ScriptRunnerContext scriptRunnerContext, ConnectionDetails inCd, boolean retry) {
		ConnectionDetails retCd = null;

		String connectName=inCd.getConnectName();
		String connectPassword=inCd.getConnectPassword();
		String connectDB=inCd.getConnectDB();
	    if (scriptRunnerContext.getProperty(ScriptRunnerContext.JLINE_SETTING)==null){
	        return null;
	    }
	    ctrld=false;
	    if ((retry==false) 
	        && ((connectName!=null)&&(!connectName.equals(""))) //$NON-NLS-1$
	        && ((connectPassword!=null)&&(!connectPassword.equals(""))) //$NON-NLS-1$
	        && ((connectDB!=null)&&(!connectDB.equals("")))){ //$NON-NLS-1$
	          //first time around and we have all the fields:
	            return (inCd);
	        };
	    String retrying=""; //$NON-NLS-1$
	    if (retry) {
	        retrying = ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CONNECT_RETRYING)+" "/*" (Retrying) "*/; //$NON-NLS-1$
	    }
	    /* when prompting do not want to show password in complete thing */
	    String redacted="";  //$NON-NLS-1$
	    String name=inCd.getConnectName();
	    String password=inCd.getConnectPassword();
	    String db=inCd.getConnectDB();
	    String role=inCd.getRole();
	    if (name!=null&&(!name.equals(""))) {  //$NON-NLS-1$
	    	if ((name.indexOf(" ")!=-1)&&(!(name.startsWith("\"")))) {  //$NON-NLS-1$  //$NON-NLS-2$
	    		name = '"'+name+'"';
	    	}
	    	redacted = name;
	    }
	    if (password!=null&&(!password.equals(""))) {  //$NON-NLS-1$
	    	redacted +="/*********";  //$NON-NLS-1$
	    } else if (inCd.getSlash()!=-1) {
	    	redacted +="/";  //$NON-NLS-1$
	    }
	    if (db!=null&&(!db.equals(""))) {  //$NON-NLS-1$
	    	redacted +="@"+db; //$NON-NLS-1$
	    }
	    if (role!=null&&!(role.equals(""))) { //$NON-NLS-1$
	    	redacted += " "+role.trim(); //$NON-NLS-1$
	    }
	    String namePlus=getOneField(scriptRunnerContext, ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CONNECT_USERNAME)+/*"Username?*/" "+ retrying, inCd.getFullString(), redacted, false, retry); //$NON-NLS-1$
        ConnectionDetails cd=ScriptUtils.getConnectionDetails("connect "+namePlus);
	    if ((cd!=null) && (cd.getCallUsage()==true)) {
	        return cd;
	    }
	    if ((namePlus!=null) &&(!namePlus.equals("")) && (cd.getSlash()!=-1 || cd.getAt()!=-1)){
	        connectName=cd.getConnectName();
	        if (cd.getSlash()!=-1) {
	            connectPassword=cd.getConnectPassword();
	        } else {
	            connectPassword=getOneField(scriptRunnerContext, ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CONNECT_PASSWORD)+/*"Password?*/" "+ retrying, connectPassword, connectPassword, true, retry); //$NON-NLS-1$
	        }
            connectDB=cd.getConnectDB();
	        cd.setConnectPassword(connectPassword);
	        retCd=cd;
	    } else {//duplicate to above unless I am missing something
	        cd.setConnectPassword(getOneField(scriptRunnerContext, ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CONNECT_PASSWORD)+/*"Password?*/" "+ retrying, connectPassword, connectPassword, true, retry)); //$NON-NLS-1
	    	retCd = cd;
	    }

	    
	    if ((System.getenv(ScriptRunnerContext.PROMPTFORDATABASE))!=null&&(!(System.getenv(ScriptRunnerContext.PROMPTFORDATABASE).equals("")))&&(cd!=null)&&(cd.getAt()==-1)) {//go for database prompt if env set //$NON-NLS-1$ 
        	String promptDatabase=getOneField(scriptRunnerContext, ScriptRunnerDbArb.getString(ScriptRunnerDbArb.CONNECT_DATABASE)
		                + /* "Connection? */" " + retrying, connectDB, connectDB, false, retry);//$NON-NLS-1$
        	if (promptDatabase!=null){
        		cd.setConnectDB(promptDatabase);
        	}
        }
	    if (ctrld) {
	        return null;
	    }
	    return retCd;
	    
	}

	String getOneField(ScriptRunnerContext ctx, String prompt, String existing, String redacted, boolean hide, boolean retry) {
        if (ctrld==true) {
            return ""; //$NON-NLS-1$
        }
	    if (retry || existing==null || existing.equals("")) {
            DbtoolsConsoleReader reader=(DbtoolsConsoleReader) ctx.getProperty(ScriptRunnerContext.JLINE_SETTING);
        
            if (reader!=null) {
            	String haveASpace=""; //$NON-NLS-1$
            	if (ctx.getProperty(ScriptRunnerContext.SILENT)==null) {
	                if (hide) {
	                    haveASpace=prompt+ "(**********?) "; //$NON-NLS-1$
	                } else {
	                	if (redacted==null) {
	                		redacted="";//$NON-NLS-1$
	                	}
	                    haveASpace=prompt+ "('"+redacted+"'?) "; //$NON-NLS-1$ //$NON-NLS-2$
	                }
            	}
                String fromConsole=null;
                try{
                	if (ctx.getProperty(ScriptRunnerContext.SILENT)==null) {
	                    if (hide) {
	                        fromConsole=reader.readLineSimple(haveASpace,'*');
	                    } else {
	                        fromConsole=reader.readLineSimple(haveASpace); 
	                    }
                	} else {
                		fromConsole=reader.readLineSimple(haveASpace, ' ');
                	}
                    if (retry&&fromConsole!=null&&fromConsole.equals("")){ //$NON-NLS-1$
                        fromConsole=existing;
                    }
                    if (fromConsole==null) {
                        ctrld=true;
                        fromConsole=""; //$NON-NLS-1$
                    }
                } catch (Exception e) {
                    ExceptionHandler.handleException(e);
                }

                return fromConsole;
            }
        }
        return existing;
	}
}
