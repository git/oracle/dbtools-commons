/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline.handlers;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.dbtools.raptor.console.clone.DbtoolsConsoleReader;
import oracle.dbtools.raptor.newscriptrunner.IGetPromptedPasswordFieldsProvider;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerDbArb;
import oracle.dbtools.raptor.utils.ExceptionHandler;
import oracle.dbtools.util.Pair;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=StandAlonePromptedPasswordFieldsProvider.java">Barry McGillin</a>
 * 
 */
public class SQLCliPromptedPasswordFieldsProvider implements IGetPromptedPasswordFieldsProvider {
    private final String ENCODING = "UTF-8"; //$NON-NLS-1$
    private ScriptRunnerContext ctx = null;

    /*
     * (non-Javadoc)
     * 
     * @see oracle.dbtools.raptor.newscriptrunner.IGetPromptedPasswordFieldsProvider#passwordGetPassword(oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
     * java.lang.String, java.lang.String)
     */


    public String getPromptedField(ScriptRunnerContext ctx, String prompt, boolean hide) {
        DbtoolsConsoleReader reader = (DbtoolsConsoleReader) ctx.getProperty(ScriptRunnerContext.JLINE_SETTING);
        if (reader != null) {
            if (prompt==null) {
                prompt=""; //$NON-NLS-1$
            }
            String haveASpace = "";
            
            if (ctx.getProperty(ScriptRunnerContext.SILENT)==null) {
            	haveASpace=prompt;
	            if ((prompt!=null) && (prompt.length()>0) && !prompt.endsWith(" ")) {
	                haveASpace += " "; //$NON-NLS-1$
	            }
            }
            String fromConsole = null;
            try {
            	if (ctx.getProperty(ScriptRunnerContext.SILENT)==null) {
	                if (hide) {
	                	if (ctx.isSQLPlusClassic()) {
	    					fromConsole = reader.readLineSimple(haveASpace, ' ');
	                	} else {
	                		fromConsole = reader.readLineSimple(haveASpace, '*');
	                	}
	                } else {
	                    fromConsole = reader.readLineSimple(haveASpace);
	                }
				} else {
					fromConsole = reader.readLineSimple(haveASpace, ' ');
				}
            } catch (Exception e) {
                ExceptionHandler.handleException(e);
            }
            return fromConsole;
        }
        return null;
    }
    @SuppressWarnings("unchecked")
	@Override
    public Pair<String, String> passwordGetPassword(ScriptRunnerContext inctx, String currentUserId, String alterUserId) {
        ctx = inctx;
        String[] result = threePasswords((currentUserId.equals(alterUserId) || (alterUserId == null)), currentUserId, alterUserId, ctx.getCurrentConnection());
        if (result == null) {
            return null;
        }
        if (result[ 3 ].equals("CONNECT EXCEPTION")) { //$NON-NLS-1$
            report(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.FAILED_TO_VALIDATE_USER));
            return null;
        } else
            if (result[ 3 ].equals("NO CLICK")) { //$NON-NLS-1$
                return null;
            } else
                if ((result[ 1 ] != null) && (!(result[ 1 ].equals(result[ 2 ])))) {
                    report(ScriptRunnerDbArb.getString(ScriptRunnerDbArb.PASSWORDS_DO_NOT_MATCH));
                    return null;
                }
        return new Pair(result[ 2 ], result[ 0 ]);
    }

    // copied from ScriptRunner.java
    /**
     * note ide status not changed. This may be required.
     */
    protected void report(String s) {
        OutputStream out=ctx.getOutputStream();
        if (out != null) {
            try {
                out.write(s.getBytes(ENCODING));
                out.write('\n');
            } catch (IOException e) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getStackTrace()[ 0 ].toString(), e);
            }
        }
    }

    String[] threePasswords(boolean showUsers, String user, String alterUser, Connection lconn) {
        String localPassword = null, newPass1 = null, newPass2 = null;
        String errorOut = null;

        if (showUsers) {
            localPassword = getPromptedField(ctx, ScriptRunnerDbArb.format(ScriptRunnerDbArb.PASSWORD_FOR, user), true);
            if (localPassword == null) {
                return null;
            }
        }
        newPass1 = getPromptedField(ctx, ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NEW_PASSWORD_FOR), true);
        if (newPass1 == null) {
            return null;
        }
        newPass2 = getPromptedField(ctx, ScriptRunnerDbArb.getString(ScriptRunnerDbArb.NEW_PASSWORD_AGAIN), true);
        if (newPass2 == null) {
            return null;
        }
        errorOut = "NO CLICK"; //$NON-NLS-1$

        if (showUsers) {
            // try a loopback login to confirm password
            Properties props = new Properties();
            props.setProperty("user", '"'+user+'"'); //$NON-NLS-1$
            props.setProperty("password", localPassword); //$NON-NLS-1$
            if (user.equalsIgnoreCase("SYS")) { //$NON-NLS-1$
                props.setProperty("internal_logon", "SYSDBA"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            String url;
            Connection myconn = null;
            try {
                url = lconn.getMetaData().getURL();
                myconn = DriverManager.getConnection(url, props);
                errorOut = "OK";//$NON-NLS-1$
            } catch (Exception ex) {
                myconn = null;
                errorOut = "CONNECT EXCEPTION"; //$NON-NLS-1$
            } finally {
                if (myconn != null) {
                    try {
                        myconn.close();
                    } catch (SQLException ex) {
                    }
                }
            }
        } else {
            errorOut = "OK"; //$NON-NLS-1$
        }

        return new String[] { localPassword, newPass1, newPass2, errorOut };
    }
}