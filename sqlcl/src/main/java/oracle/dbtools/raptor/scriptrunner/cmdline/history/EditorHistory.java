/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.cmdline.history;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.ListIterator;

import jline.console.history.History;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=EditorHistory.java">Barry McGillin</a> 
 *
 */
public class EditorHistory implements History {

	/* (non-Javadoc)
	 * @see jline.console.history.History#size()
	 */
	@Override
	public int size() {
		System.out.println(MessageFormat.format("size:{0}",new Object[] {0}));
		return 0;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		System.out.println("isEmpty");
		return false;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#index()
	 */
	@Override
	public int index() {
		System.out.println("index");
		return 0;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#clear()
	 */
	@Override
	public void clear() {
		
		System.out.println("clear");

	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#get(int)
	 */
	@Override
	public CharSequence get(int index) {
		System.out.println("get");
		return "barry";
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#add(java.lang.CharSequence)
	 */
	@Override
	public void add(CharSequence line) {
		System.out.println("gettngBarryfrom history");
		

	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#set(int, java.lang.CharSequence)
	 */
	@Override
	public void set(int index, CharSequence item) {
		System.out.println("set");

	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#remove(int)
	 */
	@Override
	public CharSequence remove(int i) {
		System.out.println("remove");
		return "Barry removed";
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#removeFirst()
	 */
	@Override
	public CharSequence removeFirst() {
		System.out.println("removeforst");
		return "REmoved first";
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#removeLast()
	 */
	@Override
	public CharSequence removeLast() {
		System.out.println("removelast");
		return "REmoved last";
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#replace(java.lang.CharSequence)
	 */
	@Override
	public void replace(CharSequence item) {
		System.out.println("replace");
		

	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#entries(int)
	 */
	@Override
	public ListIterator<Entry> entries(int index) {
		System.out.println("entries");
		return null;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#entries()
	 */
	@Override
	public ListIterator<Entry> entries() {
		System.out.println("entries noindex");
		return null;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#iterator()
	 */
	@Override
	public Iterator<Entry> iterator() {
		System.out.println("iterator");
		return null;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#current()
	 */
	@Override
	public CharSequence current() {
		System.out.println("currentline");
		return "currentline";
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#previous()
	 */
	@Override
	public boolean previous() {
		System.out.println("previos?");
		return false;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#next()
	 */
	@Override
	public boolean next() {
		System.out.println("next?");
		return false;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#moveToFirst()
	 */
	@Override
	public boolean moveToFirst() {
		System.out.println("movetofirst");
		return false;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#moveToLast()
	 */
	@Override
	public boolean moveToLast() {
		System.out.println("movetolast");
		return false;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#moveTo(int)
	 */
	@Override
	public boolean moveTo(int index) {
		System.out.println("movetoindex");
		
		return false;
	}

	/* (non-Javadoc)
	 * @see jline.console.history.History#moveToEnd()
	 */
	@Override
	public void moveToEnd() {
		System.out.println("movetoend");
	}

}
