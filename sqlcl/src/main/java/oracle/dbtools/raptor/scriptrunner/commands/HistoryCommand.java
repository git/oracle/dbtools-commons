/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import oracle.dbtools.raptor.console.HistoryItem;
import oracle.dbtools.raptor.console.MultiLineHistory;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;
import oracle.dbtools.util.Array;
import sun.security.jca.GetInstance;

/**
 * @author <a href= "mailto:barry.mcgillin@oracle.com@oracle.com?subject=HistoryCommand.java" >Barry McGillin</a>
 *
 */
public class HistoryCommand extends CommandListener implements IHelp {

	private static final String USAGE = Messages.getString("HistoryCommand.0"); //$NON-NLS-1$
	private static final String TIME = "time"; //$NON-NLS-1$
	private static final String FAILS = "fails"; //$NON-NLS-1$
	private static final String FULL = "full"; //$NON-NLS-1$
	private static final String CLEAR = "clear"; //$NON-NLS-1$
	private static final String SESSION = "session"; //$NON-NLS-1$
	private static final String SCRIPT = "script";//$NON-NLS-1$
	private static final String REMOVE = "remove";//$NON-NLS-1$
	private static String SAVE = "save"; //$NON-NLS-1$
	private static String LOAD = "load"; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java .sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String[] cmds = cmd.getSQLOrig().split("\\s+"); //$NON-NLS-1$
		if (cmds.length > 3) {
			ctx.write(Messages.getString("HistoryCommand.8")); //$NON-NLS-1$
			ctx.write(Messages.getString("HistoryCommand.9")); //$NON-NLS-1$
			return true;
		}
		if ((cmd.getSQLOrig().startsWith("his") || cmd.getSQLOrig().startsWith("h")) && (cmds.length == 2 | cmds.length == 3)) { //$NON-NLS-1$ //$NON-NLS-2$
			if (cmds[1].toLowerCase().equals(USAGE)) {
				ctx.write(MultiLineHistory.getInstance().listHistory(true, false, false, false, MultiLineHistory.getInstance().showFailures()));
				return true;
			} else if (cmds[1].toLowerCase().equals(FULL)) {
				ctx.write(MultiLineHistory.getInstance().listHistory(false, true, false, false, MultiLineHistory.getInstance().showFailures()));
				return true;
			} else if (cmds[1].toLowerCase().equals(REMOVE)) {
				String[] lines = cmds[2].split(",");
				ArrayList<Integer> lint = new ArrayList<Integer>();
				for (int i = 0; i < lines.length; i++) {
					try {
						int mint = Integer.parseInt(lines[i]);
						lint.add(mint);
					} catch (NumberFormatException e) {
						// Dont care about none int versions
					}
				}
				Collections.sort(lint);
				for (int i = lint.size(); i >= 0; i--) {
					if (i <= MultiLineHistory.getInstance().getSize() && i >= 0)
						MultiLineHistory.getInstance().remove(i);
				}
			} else if (cmds[1].toLowerCase().equals(SCRIPT)) {
				ctx.write(MultiLineHistory.getInstance().listHistory(false, true, false, true, MultiLineHistory.getInstance().showFailures()));
			} else if (cmds[1].toLowerCase().equals(CLEAR)) {
				if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING) != null) {
					MultiLineHistory history = MultiLineHistory.getInstance();
					if (cmds.length > 2 && cmds[2].toLowerCase().equals(SESSION)) {
						for (HistoryItem hi : history.getHistory()) {
							hi.setIsCurrentSession(false);
						}
					} else {
						history.clear();
					}
					ctx.write(Messages.getString("HistoryCommand.13")); //$NON-NLS-1$
				}
			} else if (cmds[1].toLowerCase().equals(SAVE)) {
				if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING) != null) {
					MultiLineHistory history = MultiLineHistory.getInstance();
					history.save();
				} else {
					ctx.write(Messages.getString("HistoryCommand.14")); //$NON-NLS-1$
				}
				return true;
			} else if (cmds[1].toLowerCase().equals(LOAD)) {
				if (ctx.getProperty(ScriptRunnerContext.JLINE_SETTING) != null) {
					MultiLineHistory history = MultiLineHistory.getInstance();
					history.load();
				} else {
					ctx.write(Messages.getString("HistoryCommand.15")); //$NON-NLS-1$
				}
				return true;
			} else if (cmds[1].toLowerCase().equals(TIME)) {
				ctx.write(MultiLineHistory.getInstance().listHistory(false, false, true, false, MultiLineHistory.getInstance().showFailures()));
				return true;
			} else {
				try {
					int a = Integer.parseInt(cmds[1]);
					if (a > 0 && a <= MultiLineHistory.getInstance().getVisibileSize()) {
						ctx.getSQLPlusBuffer().getBufferSafe().resetBuffer(MultiLineHistory.getInstance().getVisibleItem(a));
						ctx.getSQLPlusBuffer().resetBuffer(MultiLineHistory.getInstance().getVisibleItem(a));
						ctx.write(ctx.getSQLPlusBuffer().list(false));
						return true;
					} else {
						ctx.write(Messages.getString("HistoryCommand.16")); //$NON-NLS-1$
						return false;
					}
				} catch (Exception e) {
					ctx.write(Messages.getString("HistoryCommand.17")); //$NON-NLS-1$
				}
			}
		} else if ((cmd.getSQLOrig().startsWith("his") || cmd.getSQLOrig().startsWith("h")) && cmds.length == 1) { //$NON-NLS-1$ //$NON-NLS-2$
			if (MultiLineHistory.getInstance().getSize() > 0) {
				ctx.write("History: \n");
				ctx.write(MultiLineHistory.getInstance().listHistory(false, false, false, false, MultiLineHistory.getInstance().showFailures()));
			} else {
				ctx.write("History empty\n");
			}
		} else if (cmd.getSQLOrig().startsWith("h") && cmds.length == 1) { //$NON-NLS-1$
			String item = cmd.getSQLOrig().trim().substring(1);
			int a = Integer.parseInt(item);
			if (a > 0 && a <= MultiLineHistory.getInstance().getHistory().size()) {
				ctx.getSQLPlusBuffer().getBufferSafe().resetBuffer(MultiLineHistory.getInstance().getItem(a));
				ctx.getSQLPlusBuffer().resetBuffer(MultiLineHistory.getInstance().getItem(a));
				ctx.write(ctx.getSQLPlusBuffer().list(false));
				return true;
			} else {
				ctx.write(Messages.getString("HistoryCommand.21")); //$NON-NLS-1$
				return false;
			}
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java .sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public String getCommand() {
		return "HISTORY"; //$NON-NLS-1$
	}

	@Override
	public String getHelp() {
		return CommandsHelp.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return false;
	}

}
