/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.AForAllStmtsCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.IOnConnection;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.scriptrunner.cmdline.NLSLANGParser;
import oracle.dbtools.util.Logger;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com?subject=NLSLANGListener.java">Barry
 *         McGillin</a>
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class NLSLANGListener extends AForAllStmtsCommand implements IOnConnection {
	private static String sql = "alter session set {0} = {1}"; //$NON-NLS-1$
	private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_NLSLANG;

	public NLSLANGListener() {
		super(m_cmdStmtSubType);
	}

	@Override
	public boolean runOnConnect(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (NLSLANGParser.isNLSLangSet()) {
			ctx.putProperty("NLS_LANG", NLSLANGParser.getNLSLANG());
			Logger.info(getClass(), Messages.getString("NLSLANGListener.1") + NLSLANGParser.getNLSLANG() + Messages.getString("NLSLANGListener.2")); //$NON-NLS-1$ //$NON-NLS-2$
			Logger.info(getClass(), Messages.getString("NLSLANGListener.3") + NLSLANGParser.getNLSLanguage() + Messages.getString("NLSLANGListener.4")); //$NON-NLS-1$ //$NON-NLS-2$
			Logger.info(getClass(), Messages.getString("NLSLANGListener.5") + NLSLANGParser.getNLSTerritory() + Messages.getString("NLSLANGListener.6") //$NON-NLS-1$ //$NON-NLS-2$
					+ Boolean.toString(NLSLANGParser.isValidTerritory(NLSLANGParser.getNLSTerritory())) + Messages.getString("NLSLANGListener.7")); //$NON-NLS-1$
			Logger.info(getClass(), Messages.getString("NLSLANGListener.8") + NLSLANGParser.getNLSEncoding() + Messages.getString("NLSLANGListener.9")); //$NON-NLS-1$ //$NON-NLS-2$
			if (NLSLANGParser.isNLSLangValid() && NLSLANGParser.isValidLanguage(NLSLANGParser.getNLSLanguage())) {
				setLanguage(conn, ctx, NLSLANGParser.getNLSLanguage());
			}
			if (NLSLANGParser.isNLSLangValid() && NLSLANGParser.isValidTerritory(NLSLANGParser.getNLSTerritory())) {
				setTerritory(conn, ctx, NLSLANGParser.getNLSTerritory());
			}
			if (NLSLANGParser.isNLSLangValid() && NLSLANGParser.isValidCharacterSet(NLSLANGParser.getNLSEncoding())) {
				setEncoding(conn, ctx, NLSLANGParser.getNLSEncoding());
			}
		} else {
			Logger.info(getClass(), Messages.getString("NLSLANGListener.10")); //$NON-NLS-1$

		}
		return false;
	}

	private void setEncoding(Connection conn, ScriptRunnerContext ctx, String nlsEncoding) {
		try {
			ctx.setEncoding(nlsEncoding);
		} catch (Exception e) {
			// do nothing here. if its badly set do nothing with it.
		}

	}

	private void setTerritory(Connection conn, ScriptRunnerContext ctx, String nlsTerritory) {
		try {
			conn.createStatement().execute(MessageFormat.format(sql, Messages.getString("NLSLANGListener.11"), nlsTerritory)); //$NON-NLS-1$
		} catch (SQLException e) {
			Logger.warn(getClass(), e);
		}
	}

	private void setLanguage(Connection conn, ScriptRunnerContext ctx, String nlsLanguage) {
		try {
			conn.createStatement().execute(MessageFormat.format(sql, Messages.getString("NLSLANGListener.12"), nlsLanguage)); //$NON-NLS-1$
		} catch (SQLException e) {
			Logger.warn(getClass(), e);
		}
	}

	@Override
	public boolean runOnDisconnect(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		super.endEvent(conn, ctx, cmd);
		if (this instanceof IOnConnection && cmd.getStmtSubType().equals(StmtSubType.G_S_CONNECT)) {
			runOnConnect(conn, ctx, cmd);
		}
	}

}

////////
