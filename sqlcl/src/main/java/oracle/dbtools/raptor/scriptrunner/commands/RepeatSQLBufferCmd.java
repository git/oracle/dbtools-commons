/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.console.MultiLineHistory;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptExecutor;
import oracle.dbtools.raptor.newscriptrunner.ScriptParser;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.Substitution;
import oracle.dbtools.raptor.newscriptrunner.Substitution.SubstitutionException;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.query.Bind;
import oracle.dbtools.raptor.utils.AnsiColorListPrinter;
import oracle.dbtools.raptor.utils.IListPrinter;

import org.fusesource.jansi.Ansi;

//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class RepeatSQLBufferCmd extends CommandListener implements IHelp {
  private static Logger LOGGER = Logger.getLogger(RepeatSQLBufferCmd.class.getName());
  public static final String ANSI_CLS = "\u001b[2J"; //$NON-NLS-1$
  public static final String ANSI_HOME = "\u001b[H"; //$NON-NLS-1$
  public static final String ANSI_BOLD = "\u001b[1m"; //$NON-NLS-1$
  public static final String ANSI_AT55 = "\u001b[10;10H"; //$NON-NLS-1$
  public static final String ANSI_REVERSEON = "\u001b[7m"; //$NON-NLS-1$
  public static final String ANSI_NORMAL = "\u001b[0m"; //$NON-NLS-1$
  public static final String ANSI_WHITEONBLUE = "\u001b[37;44m"; //$NON-NLS-1$
  public static final IListPrinter _listPrinter = new AnsiColorListPrinter();
  /*
   * HELP DATA
   */
  public String getCommand() {
    return "REPEAT"; //$NON-NLS-1$
  }

  public String getHelp() {
    return Messages.getString(getCommand());
  }

  @Override
  public boolean isSqlPlus() {
    return false;
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    // nothing to do
  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

  }

  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    if (matches("repeat", cmd.getSql())) { //$NON-NLS-1$
      if ( ctx.getProperty("REPEAT_COMMAND") != null  ){ //$NON-NLS-1$
        ctx.write(Messages.getString("RepeatSQLBufferCmd.NO_NESTED")); //$NON-NLS-1$
        return true;
      }
		if (ctx.getSQLPlusBuffer().getBufferSafe().isEmpty()) {
			ctx.write(Messages.getString("RepeatSQLBufferCmd.12")); //$NON-NLS-1$
			return true;
		}
      ctx.putProperty("REPEAT_COMMAND", Boolean.TRUE); //$NON-NLS-1$
      if (Ansi.isEnabled()) {
        printTop(conn, ctx, cmd);
      } else {
        ctx.write(Messages.getString("RepeatSQLBufferCmd.9")); // ansi required //$NON-NLS-1$
      }
      ctx.removeProperty("REPEAT_COMMAND"); //$NON-NLS-1$

      return true;
    } else {
      return false;// nothing to do here.
    }
  }

  private void printTop(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    String[] parts = cmd.getSQLOrig().split(" "); //$NON-NLS-1$
    double sleep = 1;
    int num = -1;
    try {
      num = Integer.parseInt(parts[1]);
      sleep = Double.parseDouble(parts[2]);
    } catch (Exception e){
    	  ctx.write(MessageFormat.format(Messages.getString("RepeatSQLBufferCmd.USAGE"), getCommand(), Integer.MAX_VALUE)); //$NON-NLS-1$
          return;
    }
    if ( num < 0 || sleep < 0 || sleep > 120  ){
      ctx.write(MessageFormat.format(Messages.getString("RepeatSQLBufferCmd.USAGE"), getCommand(), Integer.MAX_VALUE)); //$NON-NLS-1$
      
      return;
    }
    if (conn==null) return;
    String nextCmd = null;
    int check=MultiLineHistory.getInstance().size();
    while(check>-1 && nextCmd==null){
      String next = (String) MultiLineHistory.getInstance().get(check);
      if ( ! next.toLowerCase().startsWith("repeat") ){ //$NON-NLS-1$
        nextCmd = next;
      }
      check--;
    }
    String sql = nextCmd;
    
    ScriptParser sp = new ScriptParser(sql);
      sp.parse();
    
    ISQLCommand[] stmts = sp.getSqlStatements();
    boolean isSQL = false;
    
    NumberFormat df = new DecimalFormat("##"); //$NON-NLS-1$
    
    for ( int i=1;i<num+1;i++){
      if(ctx.isInterrupted()){
        break;
      }
      
      ctx.write(ANSI_CLS);
      ctx.write(ANSI_HOME);
      Calendar calendar = Calendar.getInstance();
      String now = calendar.get(Calendar.HOUR) + ":" +   df.format(calendar.get(Calendar.MINUTE)) + ":" + df.format(calendar.get(Calendar.SECOND)) + "."+ calendar.get(Calendar.MILLISECOND); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      //String header = "Running " + i +" of " + (num) + " @ " + now + " with a delay of " + (sleep) + "s\n";
      String header = MessageFormat.format(Messages.getString("RepeatSQLBufferCmd.16"), i,num,now,sleep); //$NON-NLS-1$
      
      if ( stmts.length == 1 &&  stmts[0].getStmtType() == SQLCommand.StmtType.G_C_SQL) {
    	  Substitution sub = new Substitution(ctx);
    	  try {
			sub.replaceSubstitution(stmts[0]);
		} catch (SubstitutionException e) {		}
        DBUtil dbUtil = DBUtil.getInstance(conn);
        Map<String,Bind> binds = getBinds(stmts[0], ctx);
        List<List<?>> rows = dbUtil.executeReturnListofList(stmts[0].getSql(),binds);
        _listPrinter.printListofList(ctx, header, rows);              
      } else {
        ctx.write(header);        
        ScriptExecutor sqlcl = (ScriptExecutor) ctx.getProperty("runner");   //$NON-NLS-1$
        sqlcl.setStmt(sql);
        sqlcl.run();         
      }
      
      if (i != num) {
        try {
          Thread.sleep(  Math.round(sleep * 1000));
        } catch (InterruptedException e) {
          break;
        }
      }// end sleep
        
    }// end for loop
      
  }

private Map<String, Bind> getBinds(ISQLCommand cmd, ScriptRunnerContext ctx) {
	Iterator<Object> it = cmd.getBinds().iterator();
	Map<String, Bind> binds = new HashMap<String,Bind>();
	Map<String, Bind> externalBinds = ctx.getVarMap();
	while (it.hasNext()) {
		Object name=it.next();
		if (externalBinds.containsKey(name)) {
			binds.put((String) name, externalBinds.get(name));
		}
	}
	if (binds.size()>0) {
		return binds;
	}
	return null;
}


}
