/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Logger;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashSet;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.jdbc.driver.OracleSQLException;
import oracle.soda.OracleCollection;
import oracle.soda.OracleCursor;
import oracle.soda.OracleDatabase;
import oracle.soda.rdbms.OracleRDBMSClient;
import oracle.soda.OracleDocument;
import oracle.soda.OracleException;

/**
 * @author <a href=
 *         "mailto:debjani.biswas@oracle.com?subject=SODACommand.java">Debjani
 *         Biswas</a>
 *
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class SODACommand extends CommandListener implements IHelp {

  private ScriptRunnerContext _ctx;

  private static Logger LOGGER = Logger.getLogger(SODACommand.class.getName());

  /*
   * HELP DATA
   */
  public String getCommand() {
    return "SODA";
  }

  public String getHelp() {
    // return Messages.getString(getCommand());
    return CommandsHelp.getString(getCommand());
  }

  @Override
  public boolean isSqlPlus() {
    return false;
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    // nothing to do
  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

  }

  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    _ctx = ctx;
    if (!cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("soda")) {
      return false;
    }

    // Get the command input from the user
    String soda_cmd = cmd.getSql().trim();
    String[] cmdParts = soda_cmd.split(" ");
    int len = cmdParts.length;
    boolean replace_success = false;
    boolean insert_success = false;

    // List of valid commands that a soda command can expect
    List<String> commands = new ArrayList<String>() {
      {
        add("create");
        add("insert");
        add("list");
        add("get");
        add("drop");
        add("find");
        add("count");
        add("remove");
        add("replace");
        add("help");
      }
    };

    // Check for the validity of the command
    if (matches("soda", cmdParts[0])) {
      if (len < 2) {
        ctx.write(getHelp());
      }

      // If it's a valid soda command ... move along
      else if (commands.contains(cmdParts[1])) {
        try {
          // Get an OracleRDBMSClient - starting point of SODA for Java
          // application.
          OracleRDBMSClient cl = new OracleRDBMSClient();

          // Get a database
          OracleDatabase db = cl.getDatabase(conn);

          if (cmdParts[1].equalsIgnoreCase("help")) {
            if (len <= 2) {
              ctx.write(getHelp());
            }

            else if (len > 2 && cmdParts[2].equalsIgnoreCase("create")) {
              ctx.write(Messages.getString("SODA_HELP_CREATE"));
            }

            else if (len > 2 && cmdParts[2].equalsIgnoreCase("list")) {
              ctx.write(Messages.getString("SODA_HELP_LIST"));
            }

            else if (len > 2 && cmdParts[2].equalsIgnoreCase("find")) {
              ctx.write(Messages.getString("SODA_HELP_FIND"));
            } else if (len > 2 && cmdParts[2].equalsIgnoreCase("get")) {
              ctx.write(Messages.getString("SODA_HELP_GET"));
            }

            else if (len > 2 && cmdParts[2].equalsIgnoreCase("drop")) {
              ctx.write(Messages.getString("SODA_HELP_DROP"));
            }

            else if (len > 2 && cmdParts[2].equalsIgnoreCase("count")) {
              ctx.write(Messages.getString("SODA_HELP_COUNT"));
            } else if (len > 2 && cmdParts[2].equalsIgnoreCase("insert")) {
              ctx.write(Messages.getString("SODA_HELP_INSERT"));
            }

            else if (len > 2 && cmdParts[2].equalsIgnoreCase("replace")) {
              ctx.write(Messages.getString("SODA_HELP_REPLACE"));
            }

            else if (len > 2 && cmdParts[2].equalsIgnoreCase("remove")) {
              ctx.write(Messages.getString("SODA_HELP_REMOVE"));
            }

            else {
              ctx.write(Messages.getString("UNRECOGNIZED_COMMAND"));
              ctx.write(getHelp());
            }
          }

          else if (cmdParts[1].equalsIgnoreCase("create")) {
            try {
              List<String> coll_names = db.admin().getCollectionNames();
              // If the collection already exists display proper message
              if (coll_names.contains(cmdParts[2])) {
                ctx.write(MessageFormat.format(Messages.getString("COLLECTION_EXISTS"), cmdParts[2]) + "\n");
              } else {
                OracleCollection col = db.admin().createCollection(cmdParts[2]);
                ctx.write(MessageFormat.format(Messages.getString("NEW_COLLECTION_SUCCESS"), cmdParts[2]) + "\n\n");

              }
            } catch (Exception e) {
              if (len < 3) {
                ctx.write(Messages.getString("MISSING_COLLECTION_NAME") + "\n");
                ctx.write(Messages.getString("SODA_HELP_CREATE") + "\n\n");
              }

              else {
                ctx.write(MessageFormat.format(Messages.getString("COMMAND_FAILED"), soda_cmd) + "\n");
              }
            }
          }

          else if (cmdParts[1].equalsIgnoreCase("list")) {
            try {
              List<String> coll_names = db.admin().getCollectionNames();
              // if there are no existing collection
              if (coll_names.isEmpty()) {
                ctx.write(Messages.getString("NO_EXISTING_COLLECTIONS") + "\n\n");
              }

              else {
                ctx.write(Messages.getString("COLLECTION_LIST") + "\n\n");
                for (String name : db.admin().getCollectionNames()) {
                  ctx.write(MessageFormat.format(Messages.getString("SIMPLE_OUTPUT"), name) + "\n");
                }
                ctx.write("\n");
              }
            } catch (Exception e) {
              if (e.getCause() instanceof SQLException) {
                ctx.write(e.getCause().getMessage() + "\n");
              }
              ctx.write(MessageFormat.format(Messages.getString("COMMAND_FAILED"), soda_cmd) + "\n");
            }
          }

          else if (cmdParts[1].equalsIgnoreCase("insert") || cmdParts[1].equalsIgnoreCase("replace")) {

            try {
              int arglen = 0;

              // get the json_str
              if (cmdParts[1].equalsIgnoreCase("insert")) {
                arglen = 3;
              }

              else if (cmdParts[1].equalsIgnoreCase("replace")) {
                arglen = 4;
              }

              String json_str = formQBE(cmdParts, soda_cmd, arglen);

              String json_filename = "";
              boolean json_file = false;

              // It's a json file
              if (!(json_str.startsWith("{") && json_str.endsWith("}"))) {
                json_file = true;
                json_filename = json_str;
                json_str = readFile(json_filename);

              }
              OracleCollection ocol = db.openCollection(cmdParts[2]);
              OracleDocument doc = db.createDocumentFromString(json_str);

              // If it's a simple insert
              if (cmdParts[1].equalsIgnoreCase("insert")) {

                // If it's a simple insert (with the default collection)
                OracleDocument insertedDoc = ocol.insertAndGet(doc);
                String key = insertedDoc.getKey();

                ctx.write(Messages.getString("JSON_STR_INSERT_SUCCESS") + "\n\n");
                insert_success = true;
                // Just display an extra message if the input is a file
                if (json_file) {
                  ctx.write(MessageFormat.format(Messages.getString("JSON_DOC_INSERT_SUCCESS"), json_filename) + "\n\n");
                }
              }

              else if (cmdParts[1].equalsIgnoreCase("replace")) {

                OracleDocument resultDoc = ocol.find().key(cmdParts[3]).replaceOneAndGet(doc);
                String key_ex = resultDoc.getKey();
                System.out.println(key_ex);

                if (resultDoc != null) {

                  String key = resultDoc.getKey();
                  String createdOn = resultDoc.getCreatedOn();

                  ctx.write(Messages.getString("JSON_STR_REPLACE_SUCCESS") + "\n\n");
                  replace_success = true;

                  // Just display an extra message if the input is a file
                  if (json_file) {
                    ctx.write(MessageFormat.format(Messages.getString("JSON_DOC_REPLACE_SUCCESS"), json_filename) + "\n\n");
                  }
                  // ctx.write(MessageFormat.format(Messages.getString("GET_KEY"),
                  // key) + "\n");
                  // ctx.write(MessageFormat.format(Messages.getString("GET_DATE"),
                  // createdOn) + "\n\n");
                }
              }
            }

            // If the json string is invalid or if the json file is not found
            catch (NoSuchFileException nofile_e) {
              int cmd_pos = 0;
              if (cmdParts[1].equalsIgnoreCase("insert")) {
                cmd_pos = 3;
              } else if (cmdParts[1].equalsIgnoreCase("replace")) {
                cmd_pos = 4;
              }

              String input_str = soda_cmd.split(" ", 5)[4];
              ctx.write(MessageFormat.format(Messages.getString("INVALID_JSON"), input_str));
            }

            catch (Exception e) {
              String exception_class = e.getClass().toString();
              String exception_type = exception_class.substring(exception_class.lastIndexOf(".") + 1);

              if (cmdParts[1].equalsIgnoreCase("insert") && len < 4) {
                ctx.write(Messages.getString("SODA_INSERT_USAGE") + "\n");
              }

              else if (cmdParts[1].equalsIgnoreCase("replace") && len < 5) {
                ctx.write(Messages.getString("NOT_ENOUGH_ARGS") + "\n");
                ctx.write(Messages.getString("SODA_HELP_REPLACE") + "\n");
              }

              // The collection doesn't exist
              else if (!isExistingColl(cmdParts[2], db)) {
                ctx.write(MessageFormat.format(Messages.getString("COLLECTION_NON_EXISTANT"), cmdParts[2]));
              }

              // If the key given is not a valid key for soda replace
              else if (cmdParts[1].equalsIgnoreCase("replace") && exception_type.equals("NullPointerException")) {
                ctx.write(MessageFormat.format(Messages.getString("INVALID_KEY"), cmdParts[3]));
              }

              // It's not a valid json string if it's unable to insert into the
              // collection
              else {
                String message = null;
                String cause_str = null;
                message = e.getMessage();

                ctx.write(MessageFormat.format(Messages.getString("COMMAND_FAILED"), soda_cmd) + "\n");

                // String exception_class = e.getClass().toString();
                // ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"),exception_class)+"\n");

                if (message != null) {
                  ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"), message) + "\n");
                } else {
                  try {
                    Throwable cause = e.getCause();
                    cause_str = cause.toString();
                    if (cause_str != null) {
                      ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"), cause_str) + "\n");
                    }
                  } catch (Exception el) {
                    ctx.write(MessageFormat.format(Messages.getString("COMMAND_FAILED"), soda_cmd) + "\n");
                  }
                }
              }
            }
          }

          else if (cmdParts[1].equalsIgnoreCase("get") || cmdParts[1].equalsIgnoreCase("find")) {

            OracleCursor c = null;
            try {
              OracleCollection ocol = db.openCollection(cmdParts[2]);
              // If opt is to get all the docs in the collection
              if (cmdParts[3].equalsIgnoreCase("-all")) {
                c = ocol.find().getCursor();

                long cnt = ocol.find().count();

                if (cnt > 0) {
                  String key_header = Messages.getString("HEADER_GET_COLLECTION_KEY");

                  String coll_header = Messages.getString("HEADER_GET_COLLECTION_DATE");

                  key_header = underLine(key_header.replace("", ""));
                  coll_header = underLine(coll_header.replace("", ""));

                  ctx.write(MessageFormat.format(Messages.getString("SIMPLE_OUTPUT"), key_header));
                  ctx.write("\t\t\t\t\t" + MessageFormat.format(Messages.getString("SIMPLE_OUTPUT"), coll_header) + "\n\n");

                  while (c.hasNext()) {

                    OracleDocument resultDoc = c.next();

                    String key = resultDoc.getKey();
                    String createdon = resultDoc.getCreatedOn();

                    ctx.write(MessageFormat.format(Messages.getString("SIMPLE_OUTPUT"), key));
                    ctx.write("\t" + MessageFormat.format(Messages.getString("SIMPLE_OUTPUT"), createdon) + "\n");
                  }
                }
                String msg = "RECORD_COUNT_GET";
                printCountMessage(cnt, msg, ctx);
              }

              // If opt is to get docs in the collection based on a qbe
              else if (cmdParts[3].equalsIgnoreCase("-f")) {

                // qbe = "{\"name\" : { \"$startsWith\" : \"K\" }}";
                // Example1: soda find debjani {"name":{"$startsWith":"K"}}

                // get the QBE
                int arglen = 4; // the filterspec or qbe appears after 4
                                // arguments
                String qbe = formQBE(cmdParts, soda_cmd, arglen);
                OracleDocument doc = db.createDocumentFromString(qbe);

                c = ocol.find().filter(doc).getCursor();
                long cnt = ocol.find().filter(doc).count();

                if (cnt > 0) {
                  while (c.hasNext()) {
                    // Get the next document
                    OracleDocument resultDoc = c.next();

                    // print the information found
                    ctx.write("\n" + MessageFormat.format(Messages.getString("GET_KEY"), resultDoc.getKey()) + "\n");
                    ctx.write(MessageFormat.format(Messages.getString("GET_CONTENT"), resultDoc.getContentAsString()) + "\n");
                    ctx.write(Messages.getString("SEPARATOR"));
                  }
                }
                String msg = "RECORD_COUNT_GET";
                printCountMessage(cnt, msg, ctx);
              }

              // If the opt is to get docs in the collection based on key
              else if (cmdParts[3].equalsIgnoreCase("-k")) {

                OracleDocument resultDoc = ocol.find().key(cmdParts[4]).getOne();

                // print the information returned
                ctx.write("\n" + MessageFormat.format(Messages.getString("GET_KEY"), resultDoc.getKey()) + "\n");
                ctx.write(MessageFormat.format(Messages.getString("GET_CONTENT"), resultDoc.getContentAsString()) + "\n");
                ctx.write(Messages.getString("SEPARATOR"));

                long cnt = ocol.find().key(cmdParts[4]).count();

                String msg = "RECORD_COUNT_GET";
                printCountMessage(cnt, msg, ctx);
              }

              // If the opt is to get docs in the collection based on many keys
              else if (cmdParts[3].equalsIgnoreCase("-klist")) {

                HashSet<String> keySet = new HashSet<String>();
                HashSet<String> existing_keys = new HashSet<String>();
                HashSet<String> invalid_keys = new HashSet<String>();

                existing_keys = findKeys(db, cmdParts, ctx);

                // Add keys to the HashSet
                for (int i = 4; i < len; i++) {
                  keySet.add(cmdParts[i]);
                  if (!existing_keys.contains(cmdParts[i])) {
                    invalid_keys.add(cmdParts[i]);
                  }
                }

                c = ocol.find().keys(keySet).getCursor();

                long cnt = ocol.find().keys(keySet).count();

                while (c.hasNext()) {
                  OracleDocument resultDoc = c.next();

                  // print the information returned
                  ctx.write("\n" + MessageFormat.format(Messages.getString("GET_KEY"), resultDoc.getKey()) + "\n");
                  ctx.write(MessageFormat.format(Messages.getString("GET_CONTENT"), resultDoc.getContentAsString()) + "\n");
                  ctx.write(Messages.getString("SEPARATOR"));
                }

                String msg = "RECORD_COUNT_GET";
                printCountMessage(cnt, msg, ctx);

                // Print the invalid keys
                if (!invalid_keys.isEmpty()) {
                  for (String s : invalid_keys) {
                    ctx.write(MessageFormat.format(Messages.getString("INVALID_KEY"), s));
                  }
                }

              }
            } catch (Exception e) {
              HashSet<String> keys = new HashSet<String>();
              keys = findKeys(db, cmdParts, ctx);

              if (len < 4) {
                if (cmdParts[1].equalsIgnoreCase("get")) {
                  ctx.write(Messages.getString("SODA_HELP_GET") + "\n");
                } else if (cmdParts[1].equalsIgnoreCase("find")) {
                  ctx.write(Messages.getString("SODA_HELP_FIND") + "\n");
                }
              }

              // QBE not given for -filter option
              else if (cmdParts[3].equalsIgnoreCase("-f")) {
                if (len < 5) {
                  ctx.write(Messages.getString("INSUFFICIENT_ARGS"));
                  if (cmdParts[1].equalsIgnoreCase("get")) {
                    ctx.write(Messages.getString("SODA_HELP_GET") + "\n");
                  } else if (cmdParts[1].equalsIgnoreCase("find")) {
                    ctx.write(Messages.getString("SODA_HELP_FIND") + "\n");
                  }
                }

                else {
                  String message = null;
                  String cause_str = null;

                  message = e.getMessage();

                  ctx.write(MessageFormat.format(Messages.getString("COMMAND_FAILED"), soda_cmd) + "\n");

                  if (message != null) {
                    ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"), message) + "\n");
                  } else {
                    try {
                      Throwable cause = e.getCause();
                      cause_str = cause.toString();
                      if (cause_str != null) {
                        ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"), cause_str) + "\n");
                      }
                    } catch (Exception el) {
                      ctx.write(MessageFormat.format(Messages.getString("COMMAND_FAILED"), soda_cmd) + "\n");
                    }
                  }
                }

              }

              // Key not given with -key option
              else if (cmdParts[3].equalsIgnoreCase("-k")) {
                if (len < 5) {
                  if (cmdParts[1].equalsIgnoreCase("get")) {
                    ctx.write(Messages.getString("SODA_HELP_GET") + "\n");
                  } else if (cmdParts[1].equalsIgnoreCase("find")) {
                    ctx.write(Messages.getString("SODA_HELP_FIND") + "\n");
                  }
                }

                // No matching keys
                else if (!keys.contains(cmdParts[4])) {
                  ctx.write(MessageFormat.format(Messages.getString("INVALID_KEY"), cmdParts[4]) + "\n");
                }
              }

              // Key(s) not given with -klist option
              else if (cmdParts[3].equalsIgnoreCase("-klist") && len < 5) {
                ctx.write(Messages.getString("INSUFFICIENT_ARGS"));
                if (cmdParts[1].equalsIgnoreCase("get")) {
                  ctx.write(Messages.getString("SODA_HELP_GET") + "\n");
                } else if (cmdParts[1].equalsIgnoreCase("find")) {
                  ctx.write(Messages.getString("SODA_HELP_FIND") + "\n");
                }
              }

              // The collection doesn't exist
              else if (!isExistingColl(cmdParts[2], db)) {
                ctx.write(MessageFormat.format(Messages.getString("COLLECTION_NON_EXISTANT"), cmdParts[2]) + "\n");
              }

              else {
                ctx.write(MessageFormat.format(Messages.getString("COMMAND_FAILED"), "Deb") + "\n");
                String message = e.getMessage();
                if (message != null) {
                  ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"), message) + "\n\n");
                }
              }
            }

            // Release resources
            finally {
              if (c != null)
                c.close();
            }
          }

          else if (cmdParts[1].equalsIgnoreCase("drop")) {
            try {

              OracleCollection ocol = db.openCollection(cmdParts[2]);
              ocol.admin().drop();

              ctx.write(MessageFormat.format(Messages.getString("DROPPED_COLLECTION"), cmdParts[2]) + "\n\n");
            }

            catch (Exception e) {
              if (len < 3) {
                ctx.write(Messages.getString("SODA_DROP_USAGE") + "\n");
              }

              else {
                ctx.write(Messages.getString("FAILED_DROP") + "\n");
                String message = e.getMessage();
                if (message != null) {
                  ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"), message) + "\n\n");
                }
              }
            }
          }

          else if (cmdParts[1].equalsIgnoreCase("count")) {
            try {
              OracleCollection ocol = db.openCollection(cmdParts[2]);

              // If you want to count a specific collection
              if (len == 3) {
                // If the collection exists
                if (isExistingColl(cmdParts[2], db)) {
                  long cnt = ocol.find().count();

                  String msg = "RECORD_COUNT_GET";
                  printCountMessage(cnt, msg, ctx);

                }

                // Else the collection doesn't exist
                else {
                  ctx.write(MessageFormat.format(Messages.getString("COLLECTION_NON_EXISTANT"), cmdParts[2]) + "\n");
                }

              }

              // If they want the count of a specific QBE within a collection
              else if (len == 4) {

                int arglen = 3; // the filterspec or qbe appears after 4
                                // arguments
                String qbe = formQBE(cmdParts, soda_cmd, arglen);

                // If the collection exists
                if (isExistingColl(cmdParts[2], db)) {
                  OracleDocument doc = db.createDocumentFromString(qbe);
                  long cnt = ocol.find().filter(doc).count();

                  String msg = "RECORD_COUNT_GET";
                  printCountMessage(cnt, msg, ctx);
                }

                // The collection doesn't exist
                else {
                  ctx.write(MessageFormat.format(Messages.getString("COLLECTION_NON_EXISTANT"), cmdParts[2]) + "\n");
                }
              }
            }

            catch (Exception e) {

              if (len < 3) {
                ctx.write(Messages.getString("SODA_COUNT_USAGE") + "\n");
              }

              else {
                ctx.write(Messages.getString("FAILED_COUNT") + "\n");
                String message = e.getMessage();
                if (message != null) {
                  ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"), message) + "\n\n");
                }
              }
            }
          }

          else if (cmdParts[1].equalsIgnoreCase("remove")) {
            try {

              OracleCollection ocol = db.openCollection(cmdParts[2]);

              // For a single key
              if (cmdParts[3].equalsIgnoreCase("-k")) {
                long cnt = ocol.find().key(cmdParts[4]).remove();

                String msg = "SUCCESS_REMOVE";
                printCountMessage(cnt, msg, ctx);
              }

              // For multiple keys
              else if (cmdParts[3].equalsIgnoreCase("-klist")) {
                HashSet<String> keySet = new HashSet<String>();

                HashSet<String> invalid_keys = new HashSet<String>();

                HashSet<String> existing_keys = new HashSet<String>();

                existing_keys = findKeys(db, cmdParts, ctx);

                // Add keys to the HashSet
                for (int i = 4; i < len; i++) {
                  keySet.add(cmdParts[i]);
                  if (!existing_keys.contains(cmdParts[i])) {
                    invalid_keys.add(cmdParts[i]);
                  }
                }

                long cnt = ocol.find().keys(keySet).remove();

                String msg = "SUCCESS_REMOVE";
                printCountMessage(cnt, msg, ctx);

                // Get a message for all invalid keys
                if (!invalid_keys.isEmpty()) {
                  for (String s : invalid_keys) {
                    ctx.write(MessageFormat.format(Messages.getString("INVALID_KEY"), s));
                  }
                }
              }

              // For a filter/qbe
              else if (cmdParts[3].equalsIgnoreCase("-f")) {

                int arglen = 4; // the filterspec or qbe appears after 4
                                // arguments
                String qbe = formQBE(cmdParts, soda_cmd, arglen);

                OracleDocument doc = db.createDocumentFromString(qbe);
                int cnt = ocol.find().filter(doc).remove();

                String msg = "SUCCESS_REMOVE";
                printCountMessage(cnt, msg, ctx);
              }

              else {
                ctx.write(MessageFormat.format(Messages.getString("UNRECOGNIZED_OPT"), cmdParts[3]) + "\n");
              }
            }

            catch (Exception e) {
              // Not enough arguments
              if (len < 4) {
                ctx.write(Messages.getString("SODA_REMOVE_USAGE") + "\n");
              }

              // If the collection doesn't exist
              else if (!isExistingColl(cmdParts[2], db)) {
                ctx.write(MessageFormat.format(Messages.getString("COLLECTION_NON_EXISTANT"), cmdParts[2]) + "\n");
              }

              else {
                ctx.write(Messages.getString("SUCCESS_REMOVE_NONE") + "\n");
                String message = e.getMessage();
                if (message != null) {
                  ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"), message) + "\n\n");
                }
              }
            }
          }
        }

        catch (Exception e) {

          ctx.write(MessageFormat.format(Messages.getString("COMMAND_FAILED"), soda_cmd) + "\n");

          String message = e.getMessage();

          if (message != null) {
            ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"), message) + "\n\n");
          } else {
            try {
              String cause_str = null;
              Throwable cause = e.getCause();
              cause_str = cause.toString();
              if (cause_str != null) {
                ctx.write(MessageFormat.format(Messages.getString("ERR_MSG"), cause_str) + "\n");
              }
            } catch (Exception el) {
              ctx.write(MessageFormat.format(Messages.getString("COMMAND_FAILED"), soda_cmd) + "\n");
            }
          }
        }
      }
      // If the command after "soda" doesn't make sense
      else {
        ctx.write(getHelp());
      }
      return true;
    }
    return false;
  }

  public String underLine(String text) {
    return text; // there is no special formatting by default
  }

  public static String readFile(String inpath) throws IOException {
    byte[] contents = Files.readAllBytes(Paths.get(inpath));
    return new String(contents);
  }

  // TODO : modify
  public static boolean isExistingColl(String coll_name, OracleDatabase db) {
    try {
      List<String> collections = db.admin().getCollectionNames();

      if (collections.contains(coll_name)) {
        return true;
      }

      else {
        return false;
      }
    }

    catch (Exception e) {
      // do nothing
      return false;
    }
  }

  // arglen is the # of arguments after which the qbe appears
  public static String formQBE(String[] cmdParts, String soda_cmd, int arglen) {

    String sub_command = "";

    for (int i = 0; i < arglen; i++) {
      sub_command += cmdParts[i];
    }

    int sub_command_len = sub_command.length() + arglen;
    String qbe = soda_cmd.substring(sub_command_len);

    return qbe;
  }

  // Method to assist in outputting proper count message
  public static void printCountMessage(long cnt, String msg, ScriptRunnerContext ctx) {
    if (cnt < 2 && cnt != 0) {
      msg = msg + "_SING";
      ctx.write(MessageFormat.format(Messages.getString(msg), cnt) + "\n");
    }

    else if (cnt >= 2) {
      msg = msg + "_PLUR";
      ctx.write(MessageFormat.format(Messages.getString(msg), cnt) + "\n");
    } else if (cnt == 0) {
      msg = msg + "_NONE";
      ctx.write(Messages.getString(msg) + "\n");
    }
  }

  // Method to find all the keys in a collection
  public static HashSet findKeys(OracleDatabase db, String[] cmdParts, ScriptRunnerContext ctx) {
    OracleCursor c = null;
    HashSet<String> keys = new HashSet<String>();

    String key = null;

    try {
      OracleCollection col = db.openCollection(cmdParts[2]);
      c = col.find().getCursor();
      OracleDocument resultDoc;

      while (c.hasNext()) {

        // Get the next document
        resultDoc = c.next();
        key = resultDoc.getKey();

        keys.add(key);

      }

      return keys;
    }

    catch (Exception e) {
      ctx.write(Messages.getString("FIND_ALL_KEYS_ERR") + "\n");
      return keys;

    }

  }
}