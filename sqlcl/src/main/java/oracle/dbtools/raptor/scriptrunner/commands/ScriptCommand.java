/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import oracle.dbtools.common.utils.FileUtils;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.raptor.console.MultiLineHistory;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.CommandRegistry;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptExecutor;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.scripting.Script;
import oracle.dbtools.scripting.ScriptingUtils;

public class ScriptCommand extends CommandListener implements IHelp {
	private static Logger LOGGER = Logger.getLogger(ScriptCommand.class.getName());

	/*
	 * HELP DATA
	 */
	public String getCommand() {
		return "SCRIPT"; //$NON-NLS-1$
	}

	public String getHelp() {
		return Messages.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return false;
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// nothing to do
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		int noslines = cmd.getSql().split("\r\n|\r|\n").length;
		if (cmd.getSql().split("\\s+").length == 1) {
			return true;
		}
		if (noslines > 1) {
			// I assumme we are doing inline. We need to
			String code = cmd.getSql().substring(cmd.getSql().indexOf('\n') + 1);
			Script s = new Script();
			s.setExtension("js");
			s.setScript(code);
			runScript(conn, ctx, cmd, s, new String[] {});
		} else {

			Script s = new Script();

			//
			String content;
			String fullCmd = cmd.getSql().substring(cmd.getSql().indexOf(" ")).trim(); //$NON-NLS-1$
			// pick out quoted args... and trim them 
			String regex = "\"?( |$)(?=(([^\"]*\"){2})*[^\"]*$)\"?";
			String[] parts = fullCmd.split(regex); //$NON-NLS-1$
			parts=trimQuotes(parts);
			String location = parts[0];
			String ext = "js"; //$NON-NLS-1$

			if (location.indexOf(".", location.lastIndexOf(File.separator)) > 0) { //$NON-NLS-1$
				ext = location.substring(location.indexOf(".", location.lastIndexOf(File.separator)) + 1); //$NON-NLS-1$
			} else {
				location = location + ".js"; //$NON-NLS-1$
			}
			// try and get the location of the script
			// this should handle http://... , foo.js, or file://...
			try {
				URLConnection uc = FileUtils.getFile(ctx, location);
				if (uc == null) {
					throw new IOException(location);
				}
				InputStream in = uc.getInputStream();
				byte[] contents = new byte[1024];
				int bytesRead = 0;
				StringBuilder strFileContents = new StringBuilder();
				// read it all in
				while ((bytesRead = in.read(contents)) != -1) {
					strFileContents.append(new String(contents, 0, bytesRead));
				}
				content = strFileContents.toString();
				// set the script content
				s.setScript(content);
			} catch (IOException e1) {
				ctx.write(MessageFormat.format(Messages.getString("ScriptCommand.8"), new Object[] { location })); //$NON-NLS-1$
				ctx.write("\n"); //$NON-NLS-1$
 			    return true;
			}

			// set the ext which will get a script engine for that language
			if (ext != null) {
				s.setExtension(ext);
			} else {
				s.setExtension("js"); //$NON-NLS-1$
			}

			ScriptEngineManager mgr = new ScriptEngineManager();

			if (mgr.getEngineByExtension(s.getExtension()) == null) {
				ctx.write(MessageFormat.format(Messages.getString("ScriptCommand.1"), new Object[] { s.getExtension() })); //$NON-NLS-1$
				return true;
			}

			runScript(conn, ctx, cmd, s, parts);

		} // last bit of file based.
		return true;
	}

	private String[] trimQuotes(String[] parts) {
		for (int i=0; i<parts.length; i++) {
			if (parts[i].startsWith("\"")) parts[i]=parts[i].substring(1);
			if (parts[i].endsWith("\"")) parts[i]=parts[i].substring(0,parts[i].length());
		}
		return parts;
	}

	/**
	 * @param conn
	 * @param ctx
	 * @param cmd
	 * @param script
	 * @param scriptArgs
	 */
	private void runScript(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd, Script script, String[] scriptArgs) {
		// push in some globals
		HashMap<String, Object> map = new HashMap<String, Object>();
		//only put it in if it exists
		if ( conn != null ){ 
		  map.put("conn", conn); //$NON-NLS-1$
      map.put("isConnected", true); //$NON-NLS-1$
		} else {
		  map.put("isConnected", false); //$NON-NLS-1$
		}

		map.put("ctx", ctx); //$NON-NLS-1$
		map.put("out", ctx.getOutputStream()); //$NON-NLS-1$
		
		DBUtil dbUtil=null;
		if (conn!=null) {
			dbUtil = DBUtil.getInstance(conn);
		}
		map.put("util", dbUtil); //$NON-NLS-1$
		map.put("commands", CommandRegistry.class);
		//Bug 22171171 - script: commands after script command in a script can not be run
		//creating a new ScriptExecutor to run the script (ex: sqlcl.setStmt('select count(1) from user_objects;');)
		//if you use the current ScriptExecutor you overwrite the reader the ScriptExecutor is using and you get the bug above.
		ScriptExecutor sqlcl = new ScriptExecutor(conn);
		sqlcl.setScriptRunnerContext(ctx);
		map.put("sqlcl", sqlcl); //$NON-NLS-1$

		map.put("args", scriptArgs); //$NON-NLS-1$

		try {
			// call the script only if there is a valid connection, otherwise we are heading south.
				ScriptingUtils.runScript(ctx,script, map); 

				if (!ctx.isLoginSQL())
			MultiLineHistory.getInstance().addItem(cmd.getSqlWithTerminator());
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}

}
