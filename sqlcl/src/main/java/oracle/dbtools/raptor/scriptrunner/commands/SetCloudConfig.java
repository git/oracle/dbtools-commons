/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;
import java.sql.Connection;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import oracle.dbtools.common.utils.FileUtils;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;
import oracle.dbtools.raptor.utils.ProvideTnsnamesDir;
import oracle.dbtools.raptor.utils.TNSHelper;
import oracle.security.pki.OracleWallet;
import oracle.security.pki.resources.OraclePKIMsgID;
import oracle.security.pki.textui.OraclePKIGenFunc;

/**
 * A simple sets the format to use when printing sql
 * 
 * 
 * @author klrice
 * 
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class SetCloudConfig extends CommandListener implements IShowCommand {
  private static final String CLOUD_CONFIG = "CLOUD_CONFIG_ZIP";

  private static final String CLOUD_CONFIG_ORIG = "CLOUD_CONFIG_ORIG";

  private static final String CLOUD_CONFIG_PATH = "CLOUD_CONFIG_PATH";

  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

    // check for our command
    if (cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("setcloudconfig")) {
      String[] parts = cmd.getSQLOrig().split(" ");
      if (parts.length < 3) {
        ctx.write(Messages.getString("CLOUD_USAGE"));
        return true;
      }
      if (testJCE()) {
        // put the path back togeher in case there's spaces in the path
        StringBuilder sb = new StringBuilder();
        for (int i = 2; i < parts.length; i++) {
          sb.append(parts[i]).append(" ");
        }
        String fName = sb.toString().trim();
        if (fName.equalsIgnoreCase("off")) {
          System.setProperty("oracle.net.wallet_location", "");
          System.setProperty("oracle.net.tns_admin", "");
          System.setProperty("oracle.net.ssl_server_dn_match", "");
          if (ctx.getProperty(CLOUD_CONFIG) != null) {
            Path p = (Path) ctx.getProperty(CLOUD_CONFIG_PATH);
            try {
              Files.delete(p);
            } catch (IOException e) {
              ctx.write(e.getLocalizedMessage());
            }
          }
        } else {

          URLConnection fUrl = FileUtils.getFile(ctx, fName);
          URL f = fUrl.getURL();
          if (f.getFile().endsWith(".zip")) {
            if (unzip(ctx, fUrl)) {
            }
          } else {
            ctx.write(MessageFormat.format(Messages.getString("CLOUD_INVALID"), new Object[] { fName }) + "\n");
          }

          if (parts.length < 3) {
            ctx.write(Messages.getString("CLOUD_USAGE"));
            return true;
          }
        }
      } else {
        ctx.write("***** JCE NOT INSTALLED **** \n");
        ctx.write("***** CAN NOT CONNECT TO PDB Service without it **** \n");
        ctx.write("  Current Java: " + System.getProperty("java.home") + "\n");
        ctx.write(" Follow instructions on  http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html\n");
      }
      return true;
    }
    return false;
  }

  private boolean testJCE() {
    int maxKeySize = 0;
    try {
      maxKeySize = Cipher.getMaxAllowedKeyLength("AES");

    } catch (NoSuchAlgorithmException e) {
    }
    return maxKeySize > 128;
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * oracle.dbtools.raptor.newscriptrunner.commands.show.IShow#handle(java.sql.
   * Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
   * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
   */
  @Override
  public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

    showConfig(ctx);

    return true;
  }

  private void showConfig(ScriptRunnerContext ctx) {
    printProp(ctx, "oracle.net.tns_admin");

    printProp(ctx, "javax.net.ssl.trustStore");
    printProp(ctx, "javax.net.ssl.trustStorePassword");

    printProp(ctx, "javax.net.ssl.keyStore");
    printProp(ctx, "javax.net.ssl.keyStorePassword");

    printProp(ctx, "oracle.net.ssl_server_dn_match");
    printProp(ctx, "oracle.net.ssl_version");
  }

  public void printProp(ScriptRunnerContext ctx, String prop) {
    ctx.write(prop + "=" + System.getProperty(prop) + "\n");
  }

  public void setProp(String prop, String value) {
    Properties p = new Properties(System.getProperties());
    p.setProperty(prop, value);
    System.setProperties(p);

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * oracle.dbtools.raptor.newscriptrunner.commands.show.IShow#getShowCommand()
   */
  @Override
  public String[] getShowAliases() {
    return new String[] { "cloudconfig" };
  }

  private boolean unzip(ScriptRunnerContext ctx, URLConnection fUrl) {
    try {
      Path tmp = Files.createTempDirectory("oracle_cloud_config");
      // clean up on exit
      tmp.toFile().deleteOnExit();
      ctx.write("Using temp directory:" + tmp.toAbsolutePath()+ "\n");

      Path pzip = tmp.resolve("temp.zip");
      java.nio.file.Files.copy(fUrl.getInputStream(), pzip);

      ZipFile zf = new ZipFile(pzip.toFile());
      Enumeration<? extends ZipEntry> entities = zf.entries();
      while (entities.hasMoreElements()) {
        ZipEntry entry = entities.nextElement();
        String name = entry.getName();
        Path p = tmp.resolve(name);
        File f = p.toFile();
        java.nio.file.Files.copy(zf.getInputStream(entry), p);
      }

      String path = tmp.toFile().getAbsolutePath();

      setProp("oracle.net.tns_admin", path);

      setStores(path);
      setProp("oracle.net.ssl_server_dn_match", "true");
      setProp("oracle.net.ssl_version", "1.2");

      CloudProvideTnsnamesDir provider = new CloudProvideTnsnamesDir(path);
      TNSHelper.setProvideTnsnamesDir(provider);
      ctx.putProperty(CLOUD_CONFIG, path);
      ctx.putProperty(CLOUD_CONFIG_ORIG, fUrl.getURL());
      ctx.putProperty(CLOUD_CONFIG_PATH, tmp);

    } catch (Exception e) {
      ctx.write(MessageFormat.format(Messages.getString("CLOUD_INVALID"), new Object[] { e.getLocalizedMessage() }) + "\n");
      return false;
    }
    return true;
  }

  private void setStores(String pathToWallet) throws IOException{
            // open the CA's wallet
            OracleWallet caWallet = new OracleWallet();
            caWallet.open(pathToWallet, null);
         
            String passwd = "SQLcl for Oracle Exadata Express";
            char[] keyAndTrustStorePasswd = OraclePKIGenFunc.getCreatePassword(passwd, false);
            
            // certs
            OracleWallet jksK = caWallet.migratePKCS12toJKS(keyAndTrustStorePasswd, OracleWallet.MIGRATE_KEY_ENTIRES_ONLY);


            // migrate (trusted) cert entries from p12 to different jks store
            OracleWallet jksT = caWallet.migratePKCS12toJKS(keyAndTrustStorePasswd, OracleWallet.MIGRATE_TRUSTED_ENTRIES_ONLY);
            String trustPath = pathToWallet+ "/sqlclTrustStore.jks";
            String keyPath = pathToWallet+ "/sqlclKeyStore.jks";
            jksT.saveAs(trustPath);        
            jksK.saveAs(keyPath);
            
            System.setProperty("javax.net.ssl.trustStore",trustPath);
            System.setProperty("javax.net.ssl.trustStorePassword",passwd.toString());
            System.setProperty("javax.net.ssl.keyStore",keyPath);
            System.setProperty("javax.net.ssl.keyStorePassword",passwd.toString());
          
  }
  @Override
  public boolean needsDatabase() {
    return false;
  }

  @Override
  public boolean inShowAll() {
    return true;
  }

  class CloudProvideTnsnamesDir implements ProvideTnsnamesDir {
    File tnsdir = null;

    CloudProvideTnsnamesDir(String file) {
      tnsdir = new File(file);
    }

    @Override
    public File getTnsnamesDir() {
      return tnsdir;
    }

  }
}
