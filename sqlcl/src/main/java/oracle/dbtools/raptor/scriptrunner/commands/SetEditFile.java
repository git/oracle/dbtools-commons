/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.IStoreCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.commands.StoreRegistry;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetEditFile.java">Barry McGillin</a> 
 *
 */
public class SetEditFile extends CommandListener implements IShowCommand, IStoreCommand{
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");//$NON-NLS-1$

	private String[] aliases = new String[] { "editf", "editfile" }; //$NON-NLS-1$ //$NON-NLS-2$
	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// SET EDITF[ILE] file_name[.ext]
		String[] tokens = cmd.getSql().split("\\s+",3); //$NON-NLS-1$
		if (tokens.length!=3) {
			ctx.write(oracle.dbtools.raptor.scriptrunner.commands.Messages.getString("SetEditFile.3")); //$NON-NLS-1$
		} else {
			String token=tokens[2];
			if (token.startsWith("\"")&&!token.endsWith("\"")) { //$NON-NLS-1$ //$NON-NLS-2$
				ctx.write(MessageFormat.format("string \"{0}\" missing terminating quote (\").\n",token)); //$NON-NLS-1$
				ctx.write(oracle.dbtools.raptor.scriptrunner.commands.Messages.getString("SetEditFile.4")); //$NON-NLS-1$
				return true;
			} else if (token.startsWith("\'")&&!token.endsWith("\'")) { //$NON-NLS-1$ //$NON-NLS-2$
				ctx.write(MessageFormat.format("string \"{0}\" missing terminating quote (\').\n",token)); //$NON-NLS-1$
				ctx.write(oracle.dbtools.raptor.scriptrunner.commands.Messages.getString("SetEditFile.5")); //$NON-NLS-1$
				return true;
			} else if (!token.startsWith("\"")&&token.endsWith("\"")|| (!token.startsWith("\'")&&token.endsWith("\'"))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				//String ends with quote but not starts.
				ctx.write(MessageFormat.format("string \"{0}\" has trailing quote and missing starting quote.\n", token)+LINE_SEPARATOR);  //$NON-NLS-1$
				ctx.write(oracle.dbtools.raptor.scriptrunner.commands.Messages.getString("SetEditFile.6")); //$NON-NLS-1$
				return true;
			}
			//Check next as a version
			String input = token;
			if (token.length()>=2 &&((token.startsWith("\"")&&token.endsWith("\""))|| (token.startsWith("\'")&&token.endsWith("\'")))) {//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				//remove quotes if there.
				input=input.substring(1, input.length()-1);
			}
			if (input.trim().length() >0)
				ctx.putProperty(ScriptRunnerContext.EDITFILE, input);
			else
				ctx.write(oracle.dbtools.raptor.scriptrunner.commands.Messages.getString("SetEditFile.7")); //$NON-NLS-1$
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public String[] getShowAliases() {
		return aliases;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		ctx.write(MessageFormat.format("editfile \"{0}\"\n", ctx.getProperty(ScriptRunnerContext.EDITFILE))); //$NON-NLS-1$
		return false;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return true;
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		return StoreRegistry.getCommand("editfile", MessageFormat.format("\"{0}\"", String.valueOf(ctx.getProperty(ScriptRunnerContext.EDITFILE)))); //$NON-NLS-1$ //$NON-NLS-2$
		
	}

}
