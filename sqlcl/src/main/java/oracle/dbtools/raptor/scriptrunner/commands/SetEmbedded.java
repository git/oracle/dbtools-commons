/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.sql.Connection;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.IStoreCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.StoreRegistry;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;

/**
 * @author <a href= "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetClassicMode.java"> Barry McGillin</a>
 *
 */
public class SetEmbedded extends CommandListener implements IShowCommand, IStoreCommand {
	String[] aliases = new String[] { "emb", "embe", "embed", "embedd", "embedde", "embedded" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$

	@Override
	public String[] getShowAliases() {
		return aliases;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

		if (ctx != null && ctx.getProperty(ScriptRunnerContext.SQLPLUS_EMBEDDED_MODE) != null
				&& Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_EMBEDDED_MODE).toString())) {
			ctx.write("embedded ON\n"); //$NON-NLS-1$
		} else if (ctx != null && ctx.getProperty(ScriptRunnerContext.SQLPLUS_EMBEDDED_MODE) != null
				&& !(Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_EMBEDDED_MODE).toString()))) {
			ctx.write("\nembedded OFF\n"); //$NON-NLS-1$
		} else {
			ctx.write("\nembedded OFF\n"); //$NON-NLS-1$
		}
		return true;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return true;
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (cmd.getSql().split("\\s+").length == 3 //$NON-NLS-1$
				&& (cmd.getSql().split("\\s+")[2].toLowerCase().equals("on") || cmd.getSql().split("\\s+")[2].toLowerCase().equals("off"))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			if (cmd.getSql().split("\\s+")[2].toLowerCase().equals("on")) { //$NON-NLS-1$ //$NON-NLS-2$
				ctx.putProperty(ScriptRunnerContext.SQLPLUS_EMBEDDED_MODE, Boolean.TRUE);
			} else {
				ctx.putProperty(ScriptRunnerContext.SQLPLUS_EMBEDDED_MODE, Boolean.FALSE);
			}

		} else {
			ctx.write("SP2-0265: embedded must be set ON or OFF\n"); //$NON-NLS-1$
		}
		return true;
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		if (ctx != null && ctx.getProperty(ScriptRunnerContext.SQLPLUS_EMBEDDED_MODE) != null
				&& Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.SQLPLUS_EMBEDDED_MODE).toString())) {
			return StoreRegistry.getCommand("embedded", "ON"); //$NON-NLS-1$
		}
		return StoreRegistry.getCommand("embedded", "OFF"); //$NON-NLS-1$
	}
}
