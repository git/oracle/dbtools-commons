/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import oracle.dbtools.raptor.console.MultiLineHistory;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLStatementTypes;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.HelpMessages;
import oracle.dbtools.raptor.newscriptrunner.commands.IStoreCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;

/**
 * @author <a href= "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetNoHistory.java"> Barry McGillin</a>
 *
 */
public class SetHistory extends CommandListener implements IHelp, IShowCommand, IStoreCommand {

	private static final String SHOW = "show";
	private static final String FAILS = "fails";
	private static final String NOFAILS = "nofails";
	private static final String LIMIT = "limit";
	private static final String BLACKLIST = "blacklist";
	private static final String DEFAULT = "default";
	private static final String OFF = "off";
	private static final String ON = "on";
	boolean bad = false;

	@Override
	public String getCommand() {
		return "HISTORY"; //$NON-NLS-1$
	}

	@Override
	public String getHelp() {
		return HelpMessages.getString("SETHISTORY");
		// return CommandsHelp.getString("SETHISTORY");
	}

	@Override
	public boolean isSqlPlus() {
		return false;
	}

	/**
	 * set history [show {fails|nofails}] set history [limit [n]|off] default is 100 set history [blacklist [sqlplus]*|default] show history is it on or off (if
	 * off, just show it as off) if on, then show if on, size, size: abc blacklist:connect, show, set, ...... (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 *      oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (cmd.getSql().trim().toLowerCase().replaceAll(" ", "").startsWith("sethistory") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				|| cmd.getSql().trim().toLowerCase().replaceAll(" ", "").startsWith("sethist")) {
			String[] parts = cmd.getSQLOrig().split("\\s+"); //$NON-NLS-1$
			if (parts.length == 3) {
				String[] bits = cmd.getSql().split("\\s+");
				if (bits.length > 2) {
					switch (bits[2].toLowerCase()) {
					case ON:
						ctx.putProperty(ScriptRunnerContext.NOHISTORY, Boolean.FALSE);
						return true;
					case OFF:
						ctx.putProperty(ScriptRunnerContext.NOHISTORY, Boolean.TRUE);
						return true;
					case FAILS:
						MultiLineHistory.getInstance().allowFailures(Boolean.TRUE);
						return true;
					case NOFAILS:
						MultiLineHistory.getInstance().allowFailures(Boolean.FALSE);
						return true;
					default:
						ctx.write("Invalid set history option");
						ctx.write(getHelp());
						return true;
					}
				}
			} else if (parts.length > 3) {
				switch (parts[2].toLowerCase()) {
				case FAILS:
					if (parts.length>4) {
						try {
							int x = Integer.parseInt(parts[4]);
							if (x < Integer.MAX_VALUE && x >= 5) {
								MultiLineHistory.getInstance().setMaxFailSize(x);
							} else {
								ctx.write("History Fails limit is a number >= 5\n");
								ctx.write(getHelp());
							}
						} catch (NumberFormatException e) {
							if (parts[4].equalsIgnoreCase(DEFAULT)) {
								MultiLineHistory.getInstance().setMaxFailSize(MultiLineHistory.DEFAULT_MAX_FAILS);
							} else {
							  ctx.write("History: Invalid number for history fail limit");
							  ctx.write(getHelp());
							}
						}
					} else {
						ctx.write(getHelp());							
					}
					break;
				
				case LIMIT:
					switch (parts[3].toLowerCase()) {
					case OFF:
					case DEFAULT:
						MultiLineHistory.getInstance().setMaxItems(MultiLineHistory.DEFAULT_MAX_ITEMS);
						break;
					default:
						try {
							int x = Integer.parseInt(parts[3]);
							if (x < Integer.MAX_VALUE && x >= 10) {
								MultiLineHistory.getInstance().setMaxSize(x);
							} else {
								ctx.write("History limit is a number >= 10");
								ctx.write(getHelp());
							}
						} catch (NumberFormatException e) {
							ctx.write("History: Invalid number for history limit");
							ctx.write(getHelp());
						}
						break;
					}
					break;
				case BLACKLIST:
					switch (parts[3].toLowerCase()) {
					case DEFAULT:
						if (parts.length == 4) {
							MultiLineHistory.getInstance().setBlackList(MultiLineHistory.getInstance().getBaseBlackList());
						} else {
							// try to set the history base list
							String list = getList(parts, 4, ctx);
							// If commands all ok, then lets add to the black list
							if (!bad) {
								MultiLineHistory.getInstance().setBaseBlackList(list);
							} else {
								ctx.write("Unknown command in history blacklist\n");
								bad = false;
							}
						}
						return true;
					default:
						String list = getList(parts, 3, ctx);
						// anything after the blacklist flag needs built into the list and validated as a real command type.

						// If commands all ok, then lets add to the black list
						if (!bad) {
							ctx.putProperty(getCommand(), list);
							MultiLineHistory.getInstance().setBlackList(list);
						} else {
							ctx.write("Unknown command in history blacklist\n");
							bad = false;
						}
						return true;
					}
				default:
					ctx.write(getHelp());
					break;
				}

				return true;
			} else {
				ctx.write(getHelp());
				return true;
			}

		}
		return false;
	}

	private String getList(String[] parts, int x, ScriptRunnerContext ctx) {
		bad = false;
		ArrayList<String> list = new ArrayList<String>();
		for (int i = x; i < parts.length; i++) {
			String blackList = parts[i];
			String[] cmds = blackList.split(","); //$NON-NLS-1$
			if (cmds.length > 0) {
				for (String acmd : cmds) {
					if (!(SQLStatementTypes.spaceRequiredTokens.containsKey(acmd.toLowerCase().trim()) || matches("set", acmd.toLowerCase()) //$NON-NLS-1$
							|| matches("show", acmd.toLowerCase().trim()))) { //$NON-NLS-1$
						ctx.write(MessageFormat.format(Messages.getString("SetNoHistory.9"), acmd)); //$NON-NLS-1$
						bad = true;
					} else {
						list.add(acmd);
					}
				}
			}
		}
		return csv(list, ',');
	}

	public String csv(final List<?> list, char delimiter) {
		final StringBuilder b = new StringBuilder();
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				b.append(list.get(i).toString());
				if (i != list.size() - 1) {
					b.append(delimiter);
				}
			}
		}
		return b.toString();
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public String[] getShowAliases() {
		return new String[] { "hist", "history" }; //$NON-NLS-1$

	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.NOHISTORY).toString())) {
			ctx.write("HISTORY disabled\n");
			return true;
		} else {
			ctx.write("HISTORY\n\tenabled\n");
			if (ctx.getProperty(ScriptRunnerContext.NOHISTORY) != null) {
				ctx.write("\tblacklist: " + MultiLineHistory.getInstance().getNoHistory() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			} else {
				if (MultiLineHistory.getInstance().getBaseBlackList().length() > 0)
					ctx.write(MessageFormat.format("\tblacklist: default ({0})\n", MultiLineHistory.getInstance().getBaseBlackList())); //$NON-NLS-1$
				else
					ctx.write("\tblacklist: default \n"); //$NON-NLS-1$
			}
			if (MultiLineHistory.getInstance().showFailures()) {
				
				ctx.write(MessageFormat.format("\tShow failed statements:({0} statements)\n",MultiLineHistory.getInstance().getMaxFails()));
			} else {
				ctx.write("\tDo not show failed statements\n");
			}
			if (MultiLineHistory.getInstance().getMaxItems() != MultiLineHistory.DEFAULT_MAX_ITEMS) {
				ctx.write(MessageFormat.format("\tMax Size: {0}\n", MultiLineHistory.getInstance().getMaxItems()));
			}
		}
		return true;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return true;
	}

	@Override
	public String getStoreCommand(ScriptRunnerContext ctx) {
		if (Boolean.parseBoolean(ctx.getProperty(ScriptRunnerContext.NOHISTORY).toString())) {
			return "set history OFF\n";
		} else {
			StringBuilder sb = new StringBuilder();
			if (ctx.getProperty(ScriptRunnerContext.NOHISTORY) != null) {
				sb.append(MessageFormat.format("set history blacklist {0}\n",MultiLineHistory.getInstance().getNoHistory() ));
			} else {
				sb.append(MessageFormat.format("set history blacklist {0}\n",MultiLineHistory.getInstance().getBaseBlackList())); 
			}
			if (MultiLineHistory.getInstance().showFailures()) {
				sb.append("set history fails\n");
			}else {
				sb.append("set history nofails\n");
			}
			if (MultiLineHistory.getInstance().getMaxItems() != MultiLineHistory.DEFAULT_MAX_ITEMS) {
				sb.append(MessageFormat.format("set history limit {0}\n",MultiLineHistory.getInstance().getMaxItems()));
			} else {
				sb.append(MessageFormat.format("set history limit {0}\n", MultiLineHistory.DEFAULT_MAX_ITEMS));
			}
			return sb.toString();
		}

	}

}
