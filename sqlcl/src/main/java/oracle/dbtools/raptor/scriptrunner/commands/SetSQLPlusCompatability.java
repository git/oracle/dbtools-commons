/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.db.VersionTracker;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetSQLPlusCompatability.java">Barry McGillin</a> 
 *
 */
public class SetSQLPlusCompatability  extends CommandListener implements IShowCommand {
	String[] aliases = { "SQLPLUSCOMPAT", "SQLPLUSCOMPATIBILITY","sqlpluscompati","sqlpluscompatib","sqlpluscompatibi",
			"sqlpluscompatibil","sqlpluscompatibili","sqlpluscompatibilit"
			}; //$NON-NLS-1$ //$NON-NLS-2$
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");//$NON-NLS-1$

	@Override
	public String[] getShowAliases() {
		return aliases;
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		if (ctx.getProperty(ScriptRunnerContext.SET_SQLPLUSCOMPATIBILITY) != null) {
			String x = (String) ctx.getProperty(ScriptRunnerContext.SET_SQLPLUSCOMPATIBILITY);
			int numberofDots = x.split("\\.").length - 1;//$NON-NLS-1$
			if ( numberofDots == 1) {
				x += ".0";//$NON-NLS-1$
			}
			if ( numberofDots == 0 || numberofDots > 2 ) {
				// strange : usage string SQLPLUSCOMPATIBILITY_USAGE from Message.property is not taking.
				ctx.write(Messages.getString("SetSQLPlusCompatability.1")+LINE_SEPARATOR);  //$NON-NLS-1$
			} else {
				ctx.write(MessageFormat.format(Messages.getString("SetSQLPlusCompatability.2")+LINE_SEPARATOR, new Object[] { x })); //$NON-NLS-1$
			}
		} else {
			//Bug 22967206 - HAVE TO LOGON WITH -C OPTION TO SEE FEEDBACK FROM SHOW SQLPLUSCOMPATIBILITY 
			String[] ver = (VersionTracker.getDbVersion(DefaultConnectionIdentifier.createIdentifier(conn)).toString()).split("\\."); //$NON-NLS-1$
			ctx.write(MessageFormat.format("sqlpluscompatibility {0}"+LINE_SEPARATOR, new Object[] {ver[0]+"."+ver[1]+"."+ver[2]})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		return true;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return false;
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String sql = cmd.getSql();
		String[] tokens = sql.split("\\s+"); //$NON-NLS-1$
		if (tokens.length != 3) {
			ctx.write(Messages.getString("SetSQLPlusCompatability.6")); //$NON-NLS-1$
			return true;
		} else {
			
			String token = tokens[2];
			
			if (token.startsWith("\"")&&!token.endsWith("\"")) { //$NON-NLS-1$ //$NON-NLS-2$
				ctx.write(MessageFormat.format(Messages.getString("SetSQLPlusCompatability.9"),token, "\"")); //$NON-NLS-1$
				return true;
			} else if (token.startsWith("\'")&&!token.endsWith("\'")) { //$NON-NLS-1$ //$NON-NLS-2$
				ctx.write(MessageFormat.format(Messages.getString("SetSQLPlusCompatability.12"),token,"'")); //$NON-NLS-1$
				return true;
			} else if (!token.startsWith("\"")&&token.endsWith("\"")|| (!token.startsWith("\'")&&token.endsWith("\'"))) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				//String ends with quote but not starts.
				ctx.write(Messages.getString("SetSQLPlusCompatability.17")+LINE_SEPARATOR);  //$NON-NLS-1$
				return true;
			}
			//Check next as a version
			String input = token;
			if (token.length()>2 &&((token.startsWith("\"")&&token.endsWith("\""))|| (token.startsWith("\'")&&token.endsWith("\'")))) {//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				//remove quotes if there.
				input=input.substring(1, input.length()-1);
			}
			
			int bits=input.split("\\.").length;//$NON-NLS-1$
			String regex="^(\\d+\\.)?(\\d+\\.)?(\\*|\\d+)$";//$NON-NLS-1$
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(input);
			if (bits>1 && matcher.matches()) {
				if (checkVersion(input,conn)) {
 				 ctx.putProperty(ScriptRunnerContext.SET_SQLPLUSCOMPATIBILITY,input);
				} else {
					ctx.write(Messages.getString("SetSQLPlusCompatability.0")); //$NON-NLS-1$
				}
			}
			else {
				ctx.write(Messages.getString("SetSQLPlusCompatability.21")+LINE_SEPARATOR);  //$NON-NLS-1$
			}			
		}
		return true;
	}

	private boolean checkVersion(String input, Connection conn) {
		String[] bits = input.split("\\."); //$NON-NLS-1$
		int major=0;
		int minor=0;
		int patch=0;
		if (bits.length>=2) {
			//No try catch as we will only get here with a digit regex above.
			major = Integer.parseInt(bits[0]);
			minor = Integer.parseInt(bits[1]);
			if (bits.length>2) {
				patch=Integer.parseInt(bits[2]);
			}
			try {
				
			Version lowest = new Version("7.3.4"); //$NON-NLS-1$
			Version v = new Version("12.2.0"); //$NON-NLS-1$
				if (conn!=null) {
				int dbMajor = conn.getMetaData().getDatabaseMajorVersion();
				int dbMinor = conn.getMetaData().getDatabaseMinorVersion();
				
				v = new Version(dbMajor+"."+dbMinor+".0"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				Version v2 = new Version(major+"."+minor+".0"); //$NON-NLS-1$ //$NON-NLS-2$
				if (patch>0) {
					v2 = new Version(major+"."+minor+"."+patch); //$NON-NLS-1$ //$NON-NLS-2$
				}
					
				/*
				 * version is less than DB version and higher than 7.3.4
				 */
				if (v2.compareTo(v)>0 ||v2.compareTo(lowest)<0) {
					return false;
				} else  {
					return true;
				}
				
			} catch (SQLException e) {
				return false;			
				}
		}
		return false;
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

}