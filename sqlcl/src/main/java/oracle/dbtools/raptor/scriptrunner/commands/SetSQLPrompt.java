/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.sql.Connection;

import org.fusesource.jansi.Ansi;

import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.AForAllStmtsCommand;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;

/**
 * Setting the prompt for the command line purely means setting it in the
 * context.
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetPrompt.java"
 *         >Barry McGillin</a>
 * 
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.NONE)
public class SetSQLPrompt extends AForAllStmtsCommand {
    private static final SQLCommand.StmtSubType m_cmdStmtSubType = SQLCommand.StmtSubType.G_S_SET_SQLPROMPT;

    public SetSQLPrompt() {
        super(m_cmdStmtSubType);
    }
    
   // public static Set<String> sqlplusVars = null;
    
    @Override
    protected boolean doHandleCmd(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        String newName = ""; //$NON-NLS-1$
        // Check out sub vars and do a quick swap of values as they are
        // already substituted.
        if (!cmd.getSQLOrig().equals(cmd.getModifiedSQL())) {
            newName = diff(cmd.getModifiedSQL().trim().replaceAll("\\s+", " "), //$NON-NLS-1$ //$NON-NLS-2$
                    getBase()); //$NON-NLS-1$
        } else {
            newName = (String) cmd.getProperty(ISQLCommand.PROP_STRING);
        }
        if (newName != null && !newName.trim().equals("")) { //$NON-NLS-1$
            ctx.setPrompt(newName);
            ctx.setBasePrompt(Ansi.ansi().render(ctx.getPrompt()).reset().toString());
        }
        return true;
    }

    private String getBase() {
        return ("set sqlp");  //$NON-NLS-1$
    }
    private static SQLCommand.StmtSubType getType() {
        return SQLCommand.StmtSubType.G_S_SET_SQLPROMPT;
    }
    private String diff(String str1, String str2) {
        int index = str1.indexOf(str2);
        if (index > -1) {
            int index2=str1.substring(index+str2.length()).indexOf(' ');
            if (index2 > -1) {
                return str1.substring(index+str2.length()+index2+1);
            }
        }
        return "";//could not find it. //$NON-NLS-1$
    }
}
