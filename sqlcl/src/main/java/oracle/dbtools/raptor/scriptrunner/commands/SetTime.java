/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;
import oracle.dbtools.raptor.newscriptrunner.commands.Connected;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=SetTime.java">Barry McGillin</a> 
 *
 */
public class SetTime extends CommandListener implements  IShowCommand, Connected {

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.Connected#needsConnection()
	 */
	@Override
	public boolean needsConnection() {
		return false;
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#getShowCommand()
	 */
	@Override
	public String[] getShowAliases() {
		return new String[]{"TIME"}; //$NON-NLS-1$
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand#handleShow(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		ctx.write(MessageFormat.format(Messages.getString("SetTime.1"),ctx.getProperty(ScriptRunnerContext.SQLPROMPTTIME).toString())); //$NON-NLS-1$
		return true;
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        String[] cmds = cmd.getSQLOrig().split("\\s+"); //$NON-NLS-1$
        if (cmds.length==3 && cmds[2].toUpperCase().equals("ON")) { //$NON-NLS-1$
            ctx.getProperties().put(ScriptRunnerContext.SQLPROMPTTIME, "ON"); //$NON-NLS-1$
        }else if (cmds.length==3 && cmds[2].toUpperCase().equals("OFF")) { //$NON-NLS-1$
            ctx.getProperties().put(ScriptRunnerContext.SQLPROMPTTIME, "OFF"); //$NON-NLS-1$
        } else if (cmds.length==3) {
        	ctx.write(Messages.getString("SetTime.7")); //$NON-NLS-1$
        }
		return true;
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/* (non-Javadoc)
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

    @Override
    public boolean needsDatabase() {
        return false;
    }

    @Override
    public boolean inShowAll() {
        return true;
    }

}
