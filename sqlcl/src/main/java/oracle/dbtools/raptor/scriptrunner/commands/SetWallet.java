/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.io.File;
import java.security.Security;
import java.sql.Connection;
import java.text.MessageFormat;

import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.restricted.Restricted;


/**
 * A simple sets the format to use when printing sql
 * 
 * 
 * @author klrice
 * 
 */
//Explicitly set the RESTRICT LEVEL. Otherwise it will be set to R4 by default. This only happens for CommandListeners registered with G_S_FORALLSTMTS
@Restricted(level=Restricted.Level.R4)
public class SetWallet extends CommandListener implements IShowCommand {

  @Override
  public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

    // check for our command
    if (cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("setwallet")) {
      
      
      Security.addProvider(new oracle.security.pki.OraclePKIProvider());
      
      String[] parts = cmd.getSQLOrig().split(" ");
      if (parts.length < 3 ) {
        ctx.write(Messages.getString("WALLET_USAGE"));
        return true;
      }
      if (parts[3].equalsIgnoreCase("off") ){
        System.setProperty("oracle.net.wallet_location", "");
        System.setProperty("oracle.net.tns_admin","");     
        System.setProperty("oracle.net.ssl_server_dn_match","");
      } else {
          // put the path back togeher in case there's spaces in the path
          StringBuilder sb = new StringBuilder();
          for (int i = 2; i <= parts.length; i++) {
            sb.append(parts[i]).append(" ");
          }
          
          File f = new File(sb.toString());
          if ( f.exists() && f.isDirectory() ){
            ctx.write(MessageFormat.format(Messages.getString("WALLET_LOCATION"),new Object[]{sb}) + "\n");
            System.setProperty("oracle.net.wallet_location", "(SOURCE=(METHOD=file)(METHOD_DATA=(DIRECTORY="+sb.toString()+")))");
            System.setProperty("oracle.net.ssl_server_dn_match","true");
          } else {
            ctx.write(MessageFormat.format(Messages.getString("WALLET_INVALID"),new Object[]{f.getAbsolutePath()}) + "\n");
          }
      }
      return true;
    }
    return false;
  }

  @Override
  public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
  }

  @Override
  public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

  }

  /* (non-Javadoc)
   * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShow#handle(java.sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext, oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
   */
  @Override
  public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    String message=null;
    if ( System.getProperty("oracle.net.wallet_location")!=null) {
      message=MessageFormat.format(Messages.getString("WALLET_LOCATION"),new Object[]{System.getProperty("oracle.net.wallet_location")});
    } else {
      message=Messages.getString("WALLET_NOLOCATION");

    }
    ctx.write("");
    ctx.write(message + "\n");
    return true;
  }

  /* (non-Javadoc)
   * @see oracle.dbtools.raptor.newscriptrunner.commands.show.IShow#getShowCommand()
   */
  @Override
  public String[] getShowAliases() {
    return new String[]{"wallet"};
  }
  
@Override
public boolean needsDatabase() {
    // TODO Auto-generated method stub
    return false;
}

@Override
public boolean inShowAll() {
    // TODO Auto-generated method stub
    return true;
}

}
