/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.io.BufferedOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;

import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.db.VersionTracker;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.util.Logger;
import oracle.jdbc.OracleConnection.DatabaseShutdownMode;
import oracle.jdbc.OracleConnection;
/**
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Shutdown.java"
 *         >Barry McGillin</a>
 *
 */
public class Shutdown extends CommandListener {

    /*
     * (non-Javadoc)
     * 
     * @see
     * oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java
     * .sql.Connection,
     * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
     * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
     */
    //private static final String IMMEDIATE = Messages.getString("Shutdown.0"); //$NON-NLS-1$
    //private static final String ABORT = "abort"; //$NON-NLS-1$
    //private static final String TRANSACTIONAL = "transactional"; //$NON-NLS-1$
    //private static final String NORMAL = "normal"; //$NON-NLS-1$
    //private static final String LOCAL = "local"; //$NON-NLS-1$

    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
        boolean nonPlugSuccess=false;
        boolean pluggableAltered=false;
        try {
            boolean wasAlterDatabase = false;
            boolean tryNonPluggable = true;
            String errorString = null;
            if (conn instanceof OracleConnection) {
                OracleConnection ocon = (OracleConnection) conn;
                boolean sysdba = false;
                if (ocon != null) {
                    //should check from prelim??
                    boolean amILocked = LockManager.lock(ocon);
                    try {
                        if (amILocked) {
                            String querySql = null;
                            String sysOperConName=Startup.returnConNameIfPublic(ocon);//gets con_name without dual privileges.
                            DBUtil dbUtil = DBUtil.getInstance(ocon);
                            String s = ocon.getProperties().getProperty("internal_logon");  //$NON-NLS-1$
                            if ((sysOperConName!=null)||(s != null && (s.toLowerCase().equals("sysdba")||s.toLowerCase().equals("sysoper") ||s.toLowerCase().equals("sysasm")))) {  //$NON-NLS-1$   //$NON-NLS-2$  //$NON-NLS-3$
                                sysdba = true;//or sysoper note do not explicitly check for sysoper - work by USER = public/CON_NAME/CON_ID
                            } else {
                                errorString = (Messages.getString("Shutdown.NOTSYSDBA")); //$NON-NLS-1$ 
                                tryNonPluggable = false;
                            }
                            if ((sysOperConName!=null)||((sysdba && (VersionTracker.getDbVersion(DefaultConnectionIdentifier.createIdentifier(ocon)).compareTo(new Version("12.1")) >= 0)))) { //$NON-NLS-1$
                                boolean immediate = false;
                                boolean abort = false;
                                if (true) {
                                    if ((sysOperConName==null)&&((dbUtil.executeOracleReturnOneCol("select NVL(SYS_CONTEXT('USERENV','CDB_NAME'),'1') from dual", null).equals("1")))) { //$NON-NLS-1$  //$NON-NLS-2$  
                                        errorString = Messages.getString("Shutdown.NOTCONSOLIDATED"); //$NON-NLS-1$ 
                                    } else {
                                        if ((sysOperConName==null)&&((dbUtil.executeOracleReturnOneCol("select to_char(SYS_CONTEXT('USERENV','CON_ID')) from dual", null).equals("1")))) { //$NON-NLS-1$  //$NON-NLS-2$  
                                            errorString = Messages.getString("Shutdown.CONID1"); //$NON-NLS-1$ 
                                        } else {
                                            String con_name = null;
                                            if (sysOperConName==null) {
                                                con_name=dbUtil.executeOracleReturnOneCol("select to_char(SYS_CONTEXT ('USERENV', 'CON_NAME')) CON_NAME from dual", null);  //$NON-NLS-1$  //$NON-NLS-2$
                                            } else {
                                                con_name=sysOperConName;
                                            }
                                            Statement st = null;
                                            try {
                                                tryNonPluggable = false;
                                                String invalid = null;
                                                ValidFlags flag=getType(cmd.getSQLOrig());
                                                String toExecute=null;
                                                if (flag==null) {
                                                    ctx.write(Messages.getString("Shutdown.ILLEGAL"));
                                                } else {
                                                    switch (flag) {
                                                    case ABORT:
                                                        toExecute="alter pluggable database \"" + con_name + "\" close abort";  //$NON-NLS-1$  //$NON-NLS-2$
                                                        break;
                                                    case IMMEDIATE:
                                                        toExecute="alter pluggable database \"" + con_name + "\" close immediate";  //$NON-NLS-1$  //$NON-NLS-2$
                                                        break;
                                                    case NORMAL:
                                                        toExecute="alter pluggable database \"" + con_name + "\" close ";  //$NON-NLS-1$  //$NON-NLS-2$
                                                        break;
                                                    case TRANSACTIONAL:
                                                    case TRANSACTIONAL_LOCAL:
                                                        //seems to work in sqlplus keep it (unchanged in sqlcl) as unavailable for the moment
                                                        invalid = flag.toString().toLowerCase(Locale.US) +  Messages.getString("Shutdown.NOTAVAILABLE");   //$NON-NLS-1$;
                                                        break;
                                                    }
                                                    if (invalid == null) {
                                                        st = ocon.createStatement();
                                                        st.execute(toExecute);
                                                        pluggableAltered=true;
                                                        ctx.write(Messages.getString("Shutdown.PLUGGABLE"));  //$NON-NLS-1$
                                                    } else {
                                                        ctx.write(invalid + "\n");  //$NON-NLS-1$
                                                    }
                                                    wasAlterDatabase = true;
                                                }
                                            } finally {
                                                try {
                                                    if (st != null) {
                                                        st.close();
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    } finally {
                        if (amILocked) {
                            LockManager.unlock(ocon);
                        }
                    }
                }
            }
            if (tryNonPluggable) {
                if (conn instanceof OracleConnection) {
                    Boolean isPrelim=(Boolean)(ctx.getProperty(ScriptRunnerContext.PRELIMAUTH));
                    OracleConnection ocon = ((OracleConnection) conn);
                    String s = ocon.getProperties().getProperty("internal_logon");  //$NON-NLS-1$
                    if ((isPrelim==null)||(isPrelim.equals(Boolean.FALSE))) {
                        if ((isPublicNoDualUsed(ocon)==true)||(s != null && (s.toLowerCase().equals("sysdba")||s.toLowerCase().equals("sysoper")||s.toLowerCase().equals("sysasm")))) {  // sysdba or USER=PUBLIC ie sysoper //$NON-NLS-1$  //$NON-NLS-2$ //$NON-NLS-3$
                            ValidFlags flag=getType(cmd.getSQLOrig());
                            if (flag==null) {
                                ctx.write(Messages.getString("Shutdown.ILLEGAL"));
                            } else {
                                switch (flag) {
                                case ABORT:
                                    ocon.shutdown(DatabaseShutdownMode.ABORT);
                                    ctx.write(Messages.getString("Shutdown.DATABASEABORTED")); //$NON-NLS-1$
                                    nonPlugSuccess=switchConnection(ctx,ocon);
                                    break;
                                case IMMEDIATE:
                                    ocon.shutdown(DatabaseShutdownMode.IMMEDIATE);
                                    nonPlugSuccess=cleanShutdown(ctx, ocon);
                                    break;
                                case NORMAL:
                                    ocon.shutdown(DatabaseShutdownMode.CONNECT);
                                    nonPlugSuccess=cleanShutdown(ctx, ocon);
                                    break;
                                case TRANSACTIONAL:
                                    ocon.shutdown(DatabaseShutdownMode.TRANSACTIONAL);
                                    nonPlugSuccess=cleanShutdown(ctx, ocon);
                                    break;
                                case TRANSACTIONAL_LOCAL:
                                    ocon.shutdown(DatabaseShutdownMode.TRANSACTIONAL_LOCAL);
                                    nonPlugSuccess=cleanShutdown(ctx, ocon);
                                    break;
                                }
                            }
                        } else {
                                ctx.write(Messages.getString("Shutdown.INSUFFPRIV")); //$NON-NLS-1$
                        }
                    } else {
                        ctx.write(Messages.getString("Shutdown.IDLE_CONNECTION")); //$NON-NLS-1$
                    }
                } else {
                        ctx.write(Messages.getString("Shutdown.NOTSUPPORTEDONCON")); //$NON-NLS-1$
                }
            } else {
                if (errorString!=null) {
                    ctx.write(errorString + "\n"); //$NON-NLS-1$
                }
            }
        } catch (SQLException e) {
            ctx.write(e.getLocalizedMessage() + "\n"); //$NON-NLS-1$
        }

        //pluggable succeeded -  pluggableAltered==true
        //nonpluggable succeeded - nonPlugSuccess==true
        if (nonPlugSuccess||pluggableAltered) {
            //ctx.write(Messages.getString("Shutdown.SUCCESS")); //$NON-NLS-1$ 
        } else {
            //ctx.write(Messages.getString("Shutdown.FAILED")); //$NON-NLS-1$ 
            ScriptUtils.doWhenever(ctx, cmd, conn, true);
        }
        return true;
    }
    private boolean switchConnection(ScriptRunnerContext ctx, OracleConnection ocon) throws SQLException {
        boolean isLocked=LockManager.lock(ocon);
        try {
            if (isLocked) {
                boolean switchBase=false;
                if (ocon.equals(ctx.getBaseConnection())) {
                    switchBase=true;
                }
                try {
                    if (!(ocon.isClosed())) {
                        ocon.close();
                    }
                } catch (Exception e) {
                    //potential connection leak.
                    Logger.warn(this.getClass(),e.getLocalizedMessage());
                }
                Properties props=(Properties)ctx.getProperty(ScriptRunnerContext.CLI_CONN_PROPS);
                Properties duplicateProperties=new Properties();
                String userName=props.getProperty("user"); //$NON-NLS-1$
                if (userName!=null) {
                    duplicateProperties.setProperty("user", userName); //$NON-NLS-1$
                }
                String password=props.getProperty("password"); //$NON-NLS-1$
                if (password!=null) {
                    duplicateProperties.setProperty("password", password); //$NON-NLS-1$
                }
                duplicateProperties.setProperty("prelim_auth", "true"); //$NON-NLS-1$  //$NON-NLS-2$
                duplicateProperties.setProperty("internal_logon",props.getProperty("internal_logon")); //$NON-NLS-1$  //$NON-NLS-2$
                Connection conn = (OracleConnection) DriverManager.getConnection
                        ((String)ctx.getProperty(ScriptRunnerContext.CLI_CONN_URL),duplicateProperties);
                ctx.putProperty(ScriptRunnerContext.PRELIMAUTH,Boolean.TRUE);
                Boolean getCommit=(Boolean) ctx
                        .getProperty(ScriptRunnerContext.CHECKBOXAUTOCOMMIT);
    
                try {
                    if ((getCommit==null)||(getCommit.equals(Boolean.FALSE))) {
                        conn.setAutoCommit(false);
                    } else {
                        conn.setAutoCommit(true);
                    }
                }catch (SQLException e) {
                    Logger.warn(getClass(), e.getStackTrace()[ 0 ].toString(), e);
                }//i.e. autocommit is usually off for connect.
                if (switchBase) {
                    ctx.setBaseConnection(conn);
                }
                ctx.setCurrentConnection(conn);
                //connAuth has been switched for conn.
                return true;
            }
        } finally {
            if (isLocked) {
                LockManager.unlock(ocon);
            }
        }
        return false;
    }

    /**
     * @param ctx
     * @param ocon
     * @throws SQLException
     */
    private boolean cleanShutdown(ScriptRunnerContext ctx, OracleConnection ocon) throws SQLException {
        //ctx.write(Messages.getString("Shutdown.11")); //$NON-NLS-1$
        Statement stmt = null;
        try {
            try {
            	stmt=ocon.createStatement();
                stmt.execute("alter database close normal"); //$NON-NLS-1$
                ctx.write(Messages.getString("Shutdown.13")); //$NON-NLS-1$
            } catch (SQLException ex) {
                //on ORA-01109: database not open write error and continue
                if (ex.getErrorCode() == 1109) {
                    ctx.write(ex.getLocalizedMessage() + "\n"); //$NON-NLS-1$
                } else {
                    throw ex;
                }
            }finally {
                if (stmt!=null) {
                    try {
                        stmt.close();
                    } catch (SQLException x) {
                        Logger.ignore(this.getClass(),x);
                    }
                };
            }
            stmt = ocon.createStatement();
            stmt.execute("alter database dismount"); //$NON-NLS-1$
            ctx.write(Messages.getString("Shutdown.15")); //$NON-NLS-1$
        } catch (SQLException ex) {
            //on ORA-01507: database not mounted write error and continue
            if (ex.getErrorCode() == 1507) {
                ctx.write(ex.getLocalizedMessage() + "\n"); //$NON-NLS-1$
            } else {
                throw ex;
            }
        }finally {
            if (stmt!=null) {
                try {
                    stmt.close();
                } catch (SQLException x) {
                    Logger.ignore(this.getClass(),x);
                }
            };
        }
        ocon.shutdown(DatabaseShutdownMode.FINAL);
        ctx.write(Messages.getString("Shutdown.16")); //$NON-NLS-1$
        return this.switchConnection(ctx,ocon);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java
     * .sql.Connection,
     * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
     * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
     */
    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql
     * .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
     * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
     */
    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

    }
    public boolean isPublicNoDualUsed(Connection conn) {
        String retVal=null;
        CallableStatement stmt = null;
        if (conn!=null&&(conn instanceof OracleConnection)) {
            boolean amILocked = LockManager.lock(conn);
            
            try {
                if (amILocked) {
                    stmt = conn.prepareCall(new StringBuffer("DECLARE \n").append( //$NON-NLS-1$
                             "CHECKONE VARCHAR2(1000):=NULL; \n").append( //$NON-NLS-1$
                            "BEGIN \n").append( //$NON-NLS-1$
                            " BEGIN \n").append( //$NON-NLS-1$
                            "   CHECKONE:=USER; \n").append( //$NON-NLS-1$
                            " EXCEPTION \n").append( //$NON-NLS-1$
                            " WHEN OTHERS THEN \n").append( //$NON-NLS-1$
                            "  CHECKONE :=NULL; \n").append( //$NON-NLS-1$
                            " END; \n").append( //$NON-NLS-1$
                            " :CHECKFORONE:=CHECKONE; \n").append(  //$NON-NLS-1$
                            "END;").toString()); //$NON-NLS-1$
                            stmt.registerOutParameter(1, java.sql.Types.VARCHAR); //$NON-NLS-1$
                            stmt.executeUpdate(); //$NON-NLS-1$
                            retVal=stmt.getString(1); //$NON-NLS-1$
                            if (stmt.wasNull()==true) { 
                                retVal=null; 
                            }
                }
            } catch (Exception e) {
                retVal=null;
            } finally {
                if (stmt!=null) {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        Logger.ignore(this.getClass(),e);
                    }
                }
                if (amILocked) {
                    LockManager.unlock(conn);
                }
            }
            
        }
        return ((retVal!=null)&&(retVal.equals("PUBLIC")));  //$NON-NLS-1$
    }
    
    public static enum ValidFlags {
        ABORT,
        IMMEDIATE,
        NORMAL,
        TRANSACTIONAL,
        TRANSACTIONAL_LOCAL {
        @Override
        public String toString() {
              return "transactional local";
        }
        }
    };
    private static ValidFlags getType(String cmdText) {
        String[] commandParts = cmdText.trim().split("\\s+");  //$NON-NLS-1$
        if (commandParts.length == 1) {
            // Shutdown normal
            return ValidFlags.NORMAL;
        } else
            if (commandParts.length == 2) {
                if (commandParts[ 1 ].toLowerCase(Locale.US).equals(ValidFlags.IMMEDIATE.toString().toLowerCase(Locale.US))) {
                    return ValidFlags.IMMEDIATE;
                } else
                    if (commandParts[ 1 ].toLowerCase(Locale.US).equals(ValidFlags.NORMAL.toString().toLowerCase(Locale.US))) {
                        return ValidFlags.NORMAL;
                    } else
                        if (commandParts[ 1 ].toLowerCase(Locale.US).equals(ValidFlags.ABORT.toString().toLowerCase(Locale.US))) {
                            return ValidFlags.ABORT;
                        } else
                            if (commandParts[ 1 ].toLowerCase(Locale.US).equals(ValidFlags.TRANSACTIONAL.toString().toLowerCase(Locale.US))) {
                                return ValidFlags.TRANSACTIONAL;
                            } else {
                                return null;
                            }

            } else {
                if (commandParts.length==3&&
                        ((commandParts[ 1 ].toLowerCase(Locale.US) + " "  //$NON-NLS-1$
                    + (commandParts[ 2 ].toLowerCase(Locale.US)))
                    .equals(ValidFlags.TRANSACTIONAL_LOCAL.toString().toLowerCase(Locale.US)))) {
                    return ValidFlags.TRANSACTIONAL_LOCAL;
                } else {
                    return null;
                }
            }
    }
}
