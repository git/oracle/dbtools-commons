/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.sql.Connection;
import java.text.MessageFormat;

import com.jcraft.jsch.JSchException;

import oracle.dbtools.plusplus.ssh.SQLUserInfo;
import oracle.dbtools.plusplus.ssh.SSHConnection;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowCommand;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowNoRows;
import oracle.dbtools.raptor.newscriptrunner.commands.show.IShowPrefixNameNewline;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=SshTunnelCommand.java"
 *         >Barry McGillin</a>
 *
 */
public class SshTunnelCommand extends CommandListener
		implements IHelp, IShowCommand, IShowPrefixNameNewline, IShowNoRows {
	private static final String KEY = "-i";
	private static final String PORTF = "-L";
	private static final String DISCONNECT = "disconnect";
	private static String SSHTUNNEL = "sqlcl.sshtunnel";

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java
	 * .sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// sshtunnel host hostname key keyname
		// 0 1 2 3 4
		String[] parts = cmd.getSql().split("\\s+");
		if (parts.length < 2 || parts.length > 6 || ( parts.length % 2 ) > 0 ) {
			ctx.write(getHelp());
		} else {
			String hostname = "";
			String keyname = "";
			String portf = "";
			if (parts.length == 2) {
				if (parts[1].toLowerCase().equals(DISCONNECT)) {
					SSHConnection tunnel = (SSHConnection) ctx.getProperty(SSHTUNNEL);
					if (tunnel != null && tunnel.isConnected())
						tunnel.disconnect();
					ctx.putProperty(SSHTUNNEL, null);
				} else {
					hostname = parts[1];
				}
			} else if (parts.length == 4 || parts.length == 6) {
				hostname = parts[1];
				keyname = getArgument(KEY, parts);
				portf = getArgument(PORTF, parts);
			}
			String username = System.getProperty("user.name");

			SSHConnection.ConnType type = SSHConnection.ConnType.NONE;
			if (hostname.indexOf("@") != -1) {
				String[] uhost = hostname.split("@");
				username = uhost[0];
				hostname = uhost[1];
			}
			String basePort = "22";

			if (hostname.indexOf(":") != -1) {
				String[] uhost = hostname.split(":");
				hostname = uhost[0];
				basePort = uhost[1];

			}
			SSHConnection sshConn = new SSHConnection(ctx, type, hostname, username, keyname, basePort, portf, "");
			boolean connected = sshConn.connect(); 
			if (connected) {
				ctx.write("SSH Tunnel connected\n");
				ctx.putProperty(SSHTUNNEL, sshConn);
			} else {
				ctx.write("SSH Tunnel connection failed\n");
			}
		}
		return true;
	}

	private String getArgument(String key, String[] parts) {
		for (int i = 0; i < parts.length; i++) {
			if (parts[i].equals(key) && i < parts.length - 1) {
				return parts[i + 1];
			}
		}
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java
	 * .sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql
	 * .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
	 * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
	 */
	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public String getCommand() {
		return "SSHTUNNEL";
	}

	@Override
	public String getHelp() {
		return CommandsHelp.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return false;
	}

	@Override
	public String[] getShowAliases() {
		return new String[] { getCommand() };
	}

	@Override
	public boolean handleShow(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		// ctx.write("SSH Tunnel:\n");
		String optionalHeader = (String) ctx.getProperty(ScriptRunnerContext.OPTIONAL_SHOW_HEADER);
		SSHConnection tunnel = (SSHConnection) ctx.getProperty(SSHTUNNEL);
		if (tunnel != null) {
			if (optionalHeader != null && (!optionalHeader.equals(""))) { //$NON-NLS-1$
				ctx.write(optionalHeader);
			}
			if (tunnel.isConnected()) {
				ctx.write("Tunnel Connected\n");
				ctx.write(MessageFormat.format("\tHost:{0}\n", tunnel.getSession().getHost()));
				ctx.write(MessageFormat.format("\tUsername:{0}\n", tunnel.getSession().getUserName()));
				try {
					if (tunnel.getSession().getPortForwardingL() != null) {
						String[] rules = tunnel.getSession().getPortForwardingL();
						ctx.write("\nPort Forwarding\n");
						for (int i = 0; i < rules.length; i++) {
							String part = rules[i].replace(",", "");
							String[] parts = part.split(":");
							String lport = parts[0];
							String rhost = parts[1];
							String rport = parts[2];
							ctx.write(MessageFormat.format("\nRule {0}\n", i + 1));
							ctx.write(MessageFormat.format("\tlocal port:{0}\n\tRemote Host:{1}\n\tRemote Port:{2}\n",
									new Object[] { lport, rhost, rport }));
						}
					}
				} catch (JSchException e) {
					e.printStackTrace();
				}
			} else {
				ctx.write("No Tunnel Created\n");
			}
		} else {
			if ((optionalHeader != null && (!optionalHeader.equals("")))) { //$NON-NLS-1$
				// we have an optional header i.e. in show info+ do not print anything fot 'no
				// rows'
			} else {
				ctx.write("No SSH Tunnel defined\n");
			}
		}
		return false;
	}

	@Override
	public boolean needsDatabase() {
		return false;
	}

	@Override
	public boolean inShowAll() {
		return false;
	}

}
