/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;

import oracle.dbtools.common.utils.Version;
import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.DefaultConnectionIdentifier;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.db.VersionTracker;
import oracle.dbtools.parser.LexerToken;
import oracle.dbtools.parser.Token;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLPLUS;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.ScriptUtils;
import oracle.dbtools.raptor.newscriptrunner.commands.show.ShowSga;
import oracle.dbtools.util.Pair;
import oracle.dbtools.util.Logger;
import oracle.jdbc.OracleConnection.DatabaseShutdownMode;
import oracle.jdbc.OracleConnection;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=Startup.java" >
 *         Barry McGillin</a>
 *
 */
public class Startup extends CommandListener {

    enum TokenState {
        DEFAULT, USED
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * oracle.dbtools.raptor.newscriptrunner.CommandListener#handleEvent(java
     * .sql.Connection,
     * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
     * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
     */
    private static final String RESTRICT = "restrict"; //$NON-NLS-1$
    private static final String FORCE = "force"; //$NON-NLS-1$
    private static final String PFILEUPPER = "PFILE"; //$NON-NLS-1$
    private static final String ONLYUPPER = "ONLY"; //$NON-NLS-1$
    private static final String READUPPER = "READ"; //$NON-NLS-1$
    private static final String WRITEUPPER = "WRITE"; //$NON-NLS-1$
    private static final String RECOVERUPPER = "RECOVER"; //$NON-NLS-1$
    private static final String OPENUPPER = "OPEN"; //$NON-NLS-1$
    private static final String RETRYUPPER = "RETRY"; //$NON-NLS-1$
    private static final String PARALLELUPPER = "PARALLEL"; //$NON-NLS-1$
    private static final String SHAREDUPPER = "SHARED"; //$NON-NLS-1$
    private static final String MOUNTUPPER = "MOUNT"; //$NON-NLS-1$
    private static final String PLUGGABLEUPPER = "PLUGGABLE"; //$NON-NLS-1$
    private static final String DATABASEUPPER = "DATABASE"; //$NON-NLS-1$
    private static final int NOMATCH = 0;
    private static final int OPENREADONLY = 1;
    private static final int OPENREADWRITE = 2;
    private static final int OPENREADWRITERECOVER = 3;
    private static final int MOUNTMATCH = 1;
    static boolean debug = false;

    @Override
    public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

        // debug
        // System.out.println("DEBUG ME - should be null unless PUBLIC and in
        // pdb:"+returnConNameIfPublic(conn));
        if (!(cmd.getStmtId() != null && cmd.getStmtId() == StmtSubType.G_S_STARTUP)) {
            return false;
        }
        boolean wasOpened = false;
        String alreadyUp = Messages.getString("Startup.ALREADYUP"); //$NON-NLS-1$
        boolean pluggableAltered = false;
        boolean nonPlugSuccess = false;
        boolean pluggableTried = false;
        boolean connIsNull = (conn == null)||
        		((ctx.getProperty(ScriptRunnerContext.NOLOG)!=null)&&((Boolean)(ctx.getProperty(ScriptRunnerContext.NOLOG).equals(Boolean.TRUE))));
        String thepdb = null;
        if ((connIsNull)) { // $NON-NLS-1$
            ctx.write(Messages.getString("Startup.NOCONNECTION")); //$NON-NLS-1$
            return true;
        }
        String prospective = cmd.getSql();
        prospective = SQLPLUS.removeDashNewline(cmd, prospective);
    	//if closed and prelim auth on 'no database (ora 1090)' - try to reconnect
    	Connection reopen=SQLPLUS.prelimAuthTryReopen(conn,ctx);
    	if (reopen!=null) {
    		conn=reopen;
    		ctx.setCurrentConnection(conn);
    	}
        boolean wasQuiet = false;
        boolean wasForce = false;
        boolean wasNoMount = false;
        boolean wasRestrict = false;
        boolean wasUpgrade = false;
        boolean wasDowngrade = false;
        boolean error = false;
        boolean recover = false; //$NON-NLS-1$
        List<LexerToken> src = LexerToken.parse(prospective, false);
        LexState ls = new LexState(src);
        // dump(ctx, src);
        String pfileName = pfile(ctx, src, ls, prospective);
        if (debug)
            System.out.println("STARTUP:103:PFILE:" + pfileName);
        Pair<Integer, String> openState = open(ctx, src, ls);
        if (debug)
            System.out.println("STARTUP:106:OPENSTATE:" + openState.first());

        if (openState.first().equals(OPENREADWRITERECOVER)) {
            openState = new Pair<Integer, String>(OPENREADWRITE, openState.second());
            recover = true; //$NON-NLS-1$
        }
        Pair<Integer, String> mountState = mount(ctx, src, ls);
        String pdb = pluggableDatabase(ctx, src, ls);
        boolean wasShared=eatSharedRetryParallel(ctx, src, ls);
        
        if (debug)
            System.out.println("STARTUP:113:PDB:" + pfileName);
        /*
         * if (pfileName!=null) {
         * ctx.write(MessageFormat.format(Messages.getString("NOT_SUPPORTED_NOW"
         * ),"PFILE=")); //$NON-NLS-1$ //$NON-NLS-2$ error=true; }
         */
        /*
         * for (LexerToken t:src) { if
         * (t!=null&&t.content!=null&&t.type.equals(Token.IDENTIFIER)&&t.content
         * .toUpperCase(Locale.US).equals(PFILEUPPER)) {
         * ctx.write(MessageFormat.format(Messages.getString("NOT_SUPPORTED_NOW"
         * ),"pfile=")); //$NON-NLS-1$ //$NON-NLS-2$ return true; } }
         */
        if ((!error)&&wasShared&&!(ctx.isSQLPlusClassic())) {
        	ctx.write(MessageFormat.format(Messages.getString("NOT_SUPPORTED_NOW_IGNORING"), "(SHARED|PARALLEL) [RETRY]")); //$NON-NLS-1$ //$NON-NLS-2$
        }
        if ((!error) && (mountState.first().equals(MOUNTMATCH) && ((mountState.second() != null)))) {
            //ctx.write(MessageFormat.format(Messages.getString("NOT_SUPPORTED_NOW"), "MOUNT dbname")); //$NON-NLS-1$ //$NON-NLS-2$
            ctx.write(Messages.getString("INVALIDCOMBO"));
            error = true;
        }
        if ((!error) && openState != null && (openState.second() != null)) {
            //ctx.write(MessageFormat.format(Messages.getString("NOT_SUPPORTED_NOW"), "OPEN dbname")); //$NON-NLS-1$ //$NON-NLS-2$
            ctx.write(Messages.getString("INVALIDCOMBO"));
            error = true;
        }
        if ((!error) && !ls.populateLeft(ctx)) {
            ctx.write(MessageFormat.format(Messages.getString("NOT_SUPPORTED_NOW"), prospective));
            
            error = true;
        }
        boolean match = false;

        String toExecute = null;
        String sysOperConName = null;// gets con_name without dual privileges.
        String currentPdb = null;
        if (!error) {
            wasQuiet = ls.leftRemove("QUIET"); //$NON-NLS-1$
            wasForce = ls.leftRemove("FORCE"); //$NON-NLS-1$
            wasNoMount = ls.leftRemove("NOMOUNT"); //$NON-NLS-1$
            wasRestrict = ls.leftRemove("RESTRICT"); //$NON-NLS-1$
            wasUpgrade = ls.leftRemove("UPGRADE"); //$NON-NLS-1$
            wasDowngrade = ls.leftRemove("DOWNGRADE"); //$NON-NLS-1$
            match = false;
            sysOperConName = returnConNameIfPublic(conn);// gets con_name
                                                            // without dual
                                                            // privileges.
            if (debug)
                System.out.println("STARTUP:150:SYSCONNAME:" + sysOperConName);
            if (sysOperConName != null) {
                currentPdb = sysOperConName;
                if (debug)
                    System.out.println("STARTUP:153:CURRENTPDB:" + currentPdb);
            } else {
                currentPdb = getConName(conn, ctx);
                if (debug)
                    System.out.println("STARTUP:156:CURRENTPDB:" + currentPdb);
            }
        }
        if ((!error) && (pdb != null)) { // $NON-NLS-1$
            thepdb = pdb;

            // if pdb has a space and does not end with " probably invalid
            // option;
            if ((thepdb == null) || (thepdb.equals("")) || //$NON-NLS-1$
                    (thepdb.matches("^.*\\s+.*$") && (thepdb.indexOf("\"") == -1 //$NON-NLS-1$ //$NON-NLS-2$
                            || (!thepdb.endsWith("\""))))) { //$NON-NLS-1$
                // restricted is allowed !!!!!!!!!!!!!!!!!!!!
                ctx.write(MessageFormat.format(Messages.getString("Startup.INVALIDPDB"), thepdb)); //$NON-NLS-1$
                error = true;
            }
        }
        String thePdbClause = "";
        if (!error) {
            if (pdb != null) {
                thePdbClause = "PLUGGABLE DATABASE " + pdb + " "; //$NON-NLS-1$ //$NON-NLS-2$
            } else {
                if (currentPdb != null) {
                    // gets restrict open will it bite elsewhere?
                    thePdbClause = "PLUGGABLE DATABASE " + currentPdb + " "; //$NON-NLS-1$ //$NON-NLS-2$
                } else {
                    thePdbClause = "DATABASE "; //$NON-NLS-1$
                }
            }
        }
        if (debug)
            System.out.println("STARTUP:153:PDBCLAUSE:" + thePdbClause);
        if ((!error) && (wasNoMount == false)
                && (((openState.first() == NOMATCH) || (openState.first() == OPENREADWRITE)
                        || (openState.first() == OPENREADONLY)) && (mountState.first() == NOMATCH))) {
            String append = " "; //$NON-NLS-1$
            if (wasRestrict && (!((pdb == null && currentPdb == null))
            /*
             * alter pluggable allows restricted, alter !pdb needs separate
             * handling
             */
            )) {
                append = " RESTRICTED"; //$NON-NLS-1$
            }

            String upgrade = " "; //$NON-NLS-1$
            if (wasUpgrade// not sure if I need pluggable / not pluggable
                            // handlig...&& (!((pdb==null&&currentPdb==null))
            /*
             * alter pluggable allows restricted, alter !pdb needs separate
             * handling
             */
            // )
            ) {
                upgrade = " UPGRADE"; //$NON-NLS-1$
            }
            if (wasDowngrade) {
                upgrade = upgrade + " DOWNGRADE";// error if //$NON-NLS-1$
                                                    // downgrade and upgrade -
                                                    // let jdbc tell um
            }

            if (!error) {
                if (!ls.leftIsEmpty()) {
                    ctx.write(MessageFormat.format(Messages.getString("UNHANDLED_TOKENS"), //$NON-NLS-1$
                            ls.remainingTokens()/* ends in space */)); //$NON-NLS-2$
                    error = true;
                } else {
                    if (openState.first() == OPENREADONLY) {
                        toExecute = "ALTER " + thePdbClause + " OPEN READ ONLY " + append + upgrade; //$NON-NLS-1$ //$NON-NLS-2$
                    } else {
                        toExecute = "ALTER " + thePdbClause + " OPEN READ WRITE " + append + upgrade; //$NON-NLS-1$ //$NON-NLS-2$
                    }
                    match = true;
                }
            }
        } else {
            if (!error) {
                if ((wasNoMount || mountState.first() == this.MOUNTMATCH) && (pdb == null)) {
                    match = true;
                } else {
                    error = true;
                    ctx.write(MessageFormat.format(Messages.getString("NOT_SUPPORTED_NOW"), prospective));
                }
            }
        }

        // prospective = ScriptUtils.stripFirstN(prospective, 500, true, true);

        if ((!error) && !(connIsNull) && !(conn instanceof OracleConnection)) { // $NON-NLS-1$
            ctx.write(Messages.getString("Startup.NOTORACLE")); //$NON-NLS-1$
            error = true;
        }
        if (!error && connIsNull) {
            ctx.write(Messages.getString("Startup.NOCONNECTION")); //$NON-NLS-1$
            error = true;
        }
        OracleConnection ocon = (OracleConnection) conn;
        // need to clean up connection afterwards or reconnect or something for
        // now assume close interim connection omn startup
        boolean doMount = false;
        boolean switchConOnly = false;
        try {
            if ((!error) && (ocon != null) && (thePdbClause.startsWith("PLUGGABLE"))) { //$NON-NLS-1$
                // Should explicitly error out on wasRestrict WasForce
                // ocon.getProperties().getProperty("prelim_auth");
                boolean sysoperconnalive = (sysOperConName != null) || DBUtil.isOracleConnectionAlive(ocon);
                if (debug)
                    System.out.println("STARTUP:250:SYSOPERCONN:" + sysOperConName);
                if (sysoperconnalive) {
                    boolean amILocked = LockManager.lock(ocon);
                    if (debug)
                        System.out.println("STARTUP:253:CONNECTIONLOCKED:" + amILocked);
                    try {
                        if (amILocked) {
                            DBUtil dbUtil = DBUtil.getInstance(ocon);
                            String s = ocon.getProperties().getProperty("internal_logon"); //$NON-NLS-1$
                            if (debug)
                                System.out.println("STARTUP:250:INTERNALLOGIN:" + s);
                            boolean sysdba = false;
                            if ((sysOperConName != null) || (s != null
                                    && (s.toLowerCase().equals("sysdba") || s.toLowerCase().equals("sysoper") || s.toLowerCase().equals("sysasm")))) { //$NON-NLS-1$ //$NON-NLS-2$
                                sysdba = true;// or sysoper and pdb
                            } else {
                                ctx.write(Messages.getString("Shutdown.NOTSYSDBA")); //$NON-NLS-1$
                                error = true;
                            }
                            if ((sysOperConName != null) || (sysdba
                                    && (VersionTracker.getDbVersion(DefaultConnectionIdentifier.createIdentifier(ocon))
                                            .compareTo(new Version("12.1")) >= 0))) { //$NON-NLS-1$
                                if ((sysOperConName == null) && (dbUtil
                                        .executeOracleReturnOneCol(
                                                "select NVL(SYS_CONTEXT('USERENV','CDB_NAME'),'1') from dual", null) //$NON-NLS-1$
                                        .equals("1"))) { //$NON-NLS-1$
                                    ctx.write(Messages.getString("Shutdown.NOTCONSOLIDATED")); //$NON-NLS-1$
                                    error = true;
                                } else {
                                    if ((sysOperConName == null) && ((dbUtil
                                            .executeOracleReturnOneCol(
                                                    "select to_char(SYS_CONTEXT('USERENV','CON_ID')) from dual", null) //$NON-NLS-1$
                                            .equals("1")) && (thepdb == null))) { //$NON-NLS-1$
                                        ctx.write(Messages.getString("Shutdown.CONID1")); //$NON-NLS-1$
                                        error = true;
                                    } else {
                                        // if (truewasForce/*||wasRestrict force
                                        // and restrict allowed*/) { //other non
                                        // pdb options?
                                        // ctx.write(MessageFormat.format(Messages.getString("NOT_SUPPORTED_NOW"),prospective));
                                        // //$NON-NLS-1$
                                        // error=true;
                                        // } else {
                                        // String con_name = thepdb;
                                        // if (con_name==null) {
                                        // con_name=dbUtil.executeOracleReturnOneCol("select
                                        // to_char(SYS_CONTEXT ('USERENV',
                                        // 'CON_NAME')) CON_NAME from dual",
                                        // null); //$NON-NLS-1$
                                        // }
                                        Statement st = null;
                                        try {
                                            pluggableTried = true;
                                            st = ocon.createStatement();
                                            if (pfileName != null) {
                                                if (debug)
                                                    System.out.println("STARTUP:250:PFILENAME:" + pfileName);
                                                ctx.write(MessageFormat.format(Messages.getString("NOT_SUPPORTED_NOW"), //$NON-NLS-1$
                                                        "pfile=")); //$NON-NLS-1$
                                            } else if (recover){ //lots of thinge not available on startup pluggable
                                                ctx.write(MessageFormat.format(Messages.getString("NOT_SUPPORTED_NOW"), //$NON-NLS-1$
                                                        cmd.getSql())); //$NON-NLS-1$
                                            } else if (toExecute != null) {
                                                String toExecuteForce = toExecute;
                                                if (wasForce) {
                                                    toExecuteForce += " FORCE"; //$NON-NLS-1$
                                                }
                                                if (debug)
                                                    ctx.write("about to try: " + toExecuteForce + "\n");
                                                st.execute(toExecuteForce); // $NON-NLS-1$
                                                                            // //$NON-NLS-2$
                                                pluggableAltered = true;
                                                ctx.write(Messages.getString("Startup.PDBOPENED")); //$NON-NLS-1$
                                                wasOpened = true;
                                            }
                                        } finally {
                                            try {
                                                if (st != null) {
                                                    st.close();
                                                }
                                            } catch (Exception e) {
                                                Logger.ignore(this.getClass(), e);
                                            }
                                        }
                                        // }
                                    }
                                }

                            }
                        }
                    } finally {
                        if (amILocked) {
                            LockManager.unlock(ocon);
                        }
                    }
                } else {
                    ctx.write(Messages.getString("Startup.NOTALIVE")); //$NON-NLS-1$
                    error = true;
                }
            }
            // force - on prelim auth ignore
            // on non prelim auth call the shutdown abort/startup
            Boolean prelimAuth = (ctx.getProperty(ScriptRunnerContext.PRELIMAUTH) != null)
                    && ((Boolean) ctx.getProperty(ScriptRunnerContext.PRELIMAUTH).equals(Boolean.TRUE));
            if ((!error) && (pdb == null) && (ocon != null) && (prelimAuth || wasForce)) {
                boolean isLocked = LockManager.lock(ocon);
                try {
                    if (isLocked) {
                        if (ocon instanceof OracleConnection) {
                            String s = ocon.getProperties().getProperty("internal_logon"); //$NON-NLS-1$
                            if (s != null && (s.toLowerCase().equals("sysdba") || s.toLowerCase().equals("sysoper")|| s.toLowerCase().equals("sysasm"))) { //$NON-NLS-1$ //$NON-NLS-2$

                                String[] cmdpartsNoMount = cmd.getSQLOrig().split("\\s+"); //$NON-NLS-1$
                                String[] cmdparts = cmd.getSQLOrig().replaceFirst("(?i)\\s+nomount$", " ") //$NON-NLS-1$ //$NON-NLS-2$
                                        .replaceFirst("(?i)\\s+nomount\\s+", " ").split("\\s+"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                                String forceforce=System.getenv("SQLCL_FORCE_FORCE"); //$NON-NLS-1$
                                Boolean wasForceForce=(forceforce!=null&&(!forceforce.equals(""))); //$NON-NLS-1$ 
                                if (wasRestrict == false && (wasForce == false || (wasForce && prelimAuth && (!wasForceForce)))) {
                                    // startup normal
                                    if (tryPfile(ocon, ctx, OracleConnection.DatabaseStartupMode.NO_RESTRICTION,
                                            pfileName)) {
                                        ctx.write(Messages.getString("Startup.STARTEDUP")); //$NON-NLS-1$
                                        doMount = true;
                                        if (wasNoMount) { // $NON-NLS-1$
                                            doMount = false;
                                            switchConOnly = true;
                                        }
                                    } else {
                                        error = true;
                                    }
                                } else {
                                    if (wasRestrict && (!wasForce)) {
                                        // TODO Startup RESTRICTED
                                        if (tryPfile(ocon, ctx, OracleConnection.DatabaseStartupMode.RESTRICT,
                                                pfileName)) {
                                            doMount = true;
                                        } else {
                                            error = false;
                                        }
                                    } else if (wasForce && (!wasRestrict)) {
                                        // TODO Startup FORCE - do shutdown
                                        // abort, then
                                        // startup - only get here if wasforce
                                        // !wasrestrict and !prelimauth - ie
                                        // there is a connection to shutdown
                                        // abort
                                        // will error out for example if
                                        // database was started since you
                                        // started prelim auth connection
                                        if (tryPfile(ocon, ctx, OracleConnection.DatabaseStartupMode.FORCE,
                                                pfileName)) {
                                            doMount = true;
                                        } else {
                                            error = true;
                                        }
                                    } else {
                                        ctx.write(Messages.getString("Startup.NOTINSQLCL")); //$NON-NLS-1$
                                        error = true;
                                    }
                                    if (wasNoMount) { // $NON-NLS-1$
                                        doMount = false;
                                        switchConOnly = true;
                                    }
                                }
                            } else {
                                ctx.write(Messages.getString("Startup.MUSTBESYSOPERORSYSDBA")); //$NON-NLS-1$
                                error = true;
                            }
                        } else {
                            ctx.write(Messages.getString("Shutdown.NOTSUPPORTEDONCON")); //$NON-NLS-1$
                            error = true;
                        }
                    }
                } finally {
                    if (isLocked) {
                        LockManager.unlock(ocon);
                    }
                }
            } else {
                if (!error && !pluggableTried) {
                    ctx.write(alreadyUp);
                    error = true;
                }
            }
        } catch (SQLException e) {
            ctx.write(e.getLocalizedMessage() + "\n"); //$NON-NLS-1$
            error = true;
        } finally {
            // was close interrim connection
        }
        try {
            if ((!error) && (doMount || switchConOnly)) {
                Statement st = null;
                Connection connAuth = null;
                boolean isLocked = LockManager.lock(ocon);
                try {
                    if (isLocked) {
                        boolean switchBase = false;
                        if (conn.equals(ctx.getBaseConnection())) {
                            switchBase = true;
                            ;
                        }
                        conn.close();
                        connAuth = DriverManager.getConnection(
                                (String) ctx.getProperty(ScriptRunnerContext.CLI_CONN_URL),
                                (Properties) ctx.getProperty(ScriptRunnerContext.CLI_CONN_PROPS));
                        Boolean getCommit = (Boolean) ctx.getProperty(ScriptRunnerContext.CHECKBOXAUTOCOMMIT);

                        try {
                            if ((getCommit == null) || (getCommit.equals(Boolean.FALSE))) {
                                connAuth.setAutoCommit(false);
                            } else {
                                connAuth.setAutoCommit(true);
                            }
                        } catch (SQLException e) {
                            Logger.warn(this.getClass(), e.getStackTrace()[0].toString(), e);
                        } // i.e. autocommit is usually off for connect.

                        String writeLater = ""; //$NON-NLS-1$
                        if (doMount) {
                            st = connAuth.createStatement();
                            st.execute("alter database mount"); //$NON-NLS-1$
                            writeLater += (Messages.getString("Startup.DBMOUNTED")); //$NON-NLS-1$
                            st.close();
                            //if recover is going to fail better not try to fix/continue
                            if (recover) { 
                                st = connAuth.createStatement();
                                try {
                                    st.execute("alter database recover"); //$NON-NLS-1$
                                } catch (Throwable e) {
                                    //ignore if perhaps nested sqlcode=264 ORA-00264: no recovery required
                                    Throwable next=e;
                                    Throwable throwme=e;
                                    while (next!=null) {
                                        if (next instanceof SQLException) {
                                            SQLException se=((SQLException)next);
                                            if (se.getLocalizedMessage().contains("ORA-00264")) {  //$NON-NLS-1$
                                                throwme=null;
                                                break;
                                            }
                                        }
                                        next=next.getCause();
                                    }
                                    if (throwme!=null) {
                                        if (e instanceof SQLException) {
                                            throw (SQLException) e;
                                        } else { //not likely
                                            throw new SQLException(e.getLocalizedMessage());
                                        }
                                    }
                                }
                                st.close();
                            }
                            st = connAuth.createStatement();
                            if (toExecute != null) {
                                if (debug)
                                    ctx.write("about to call " + toExecute + "\n");
                                st.execute(toExecute); // $NON-NLS-1$
                                writeLater += (Messages.getString("Startup.DBOPEN")); //$NON-NLS-1$
                                wasOpened = true;
                            }
                        }
                        ctx.putProperty(ScriptRunnerContext.PRELIMAUTH, Boolean.FALSE);
                        if (switchBase) {
                            ctx.setBaseConnection(connAuth);
                        }
                        ctx.setCurrentConnection(connAuth);
                        // cmd is not referenced
                        if ((doMount || wasNoMount) && (!wasQuiet)) {
                            new ShowSga().doShowSga(connAuth, ctx, cmd);
                        }
                        ctx.write(writeLater);
                        nonPlugSuccess = true;
                        // connAuth has been switched for conn.
                    }
                } finally {
                    try {
                        if (st != null) {
                            st.close();
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    if (isLocked) {
                        LockManager.unlock(ocon);
                    }
                }
            }
        } catch (SQLException e) {
            ctx.write(e.getLocalizedMessage() + "\n"); //$NON-NLS-1$
            error = true;
        }
        // pluggable succeeded - pluggableAltered==true
        // nonpluggable succeeded - nonPlugSuccess==true
        if (nonPlugSuccess || pluggableAltered) {
            if (wasOpened) {
                SQLPLUS.setupNLS(ctx.getCurrentConnection(), ctx);
            }
            // ctx.write(Messages.getString("Startup.SUCCESS")); //$NON-NLS-1$
        } else {
            // ctx.write(Messages.getString("Startup.FAILED")); //$NON-NLS-1$
            ScriptUtils.doWhenever(ctx, cmd, conn, true);
        }
        return true;
    }

    private String getMode(String[] cmdparts) {
        return cmdparts[1].toLowerCase();
    }

    private boolean getGoodMode(String string) {
        if (string.toLowerCase().equals(FORCE) || string.toLowerCase().equals(RESTRICT)) {
            return true;
        }
        return false;
    }

    private boolean tryPfile(Connection conn, ScriptRunnerContext ctx, OracleConnection.DatabaseStartupMode mode,
            String pfile) {
        boolean success = false;
        Throwable nonPfileThrowable = null;
        try {
            if (debug)
                System.out.println("STARTUP:498:TRYPFILE:" + pfile);
            if (pfile == null) {
                try {
                    ((OracleConnection) conn).startup(mode);
                } catch (Throwable t) {
                    nonPfileThrowable = t;
                }
            } else {
                String stripPfile = pfile;
                if ((pfile.startsWith("'") || pfile.startsWith("\"")) && (pfile.endsWith("'") || (pfile.endsWith("\""))) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                        && (pfile.length() > 2)) {
                    stripPfile = stripPfile.substring(1, stripPfile.length() - 1);
                }
                Method m = ((OracleConnection) conn).getClass().getDeclaredMethod("startup", //$NON-NLS-1$
                        OracleConnection.DatabaseStartupMode.class, String.class);
                m.invoke(conn, mode, stripPfile);
            }
            success = true;
        } catch (Throwable t) {
            // PFILEEXCEPTION=Error: startup pfile='"+pfile+"' not available in
            // installed driver\n"
            ctx.write(MessageFormat.format(Messages.getString("PFILEEXCEPTION"), pfile)); //$NON-NLS-1$
            //debug
            t.printStackTrace(new java.io.PrintStream(ctx.getOutputStream()));
            Logger.warn(this.getClass(), t);
        }
        if (nonPfileThrowable != null) {
            ctx.write(nonPfileThrowable.getLocalizedMessage() + "\n"); //$NON-NLS-1$
            success = false;
        }
        return success;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java
     * .sql.Connection,
     * oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
     * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
     */
    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql
     * .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
     * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
     */
    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    }

    static boolean eatSharedRetryParallel(ScriptRunnerContext ctx, List<LexerToken> src, LexState ls) {
        boolean retVal=false;
        for (int i = 0; i < src.size(); i++) {
            LexerToken t = src.get(i);
            if (t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                    && (t.content.toUpperCase(Locale.US).equals(PARALLELUPPER)||t.content.toUpperCase(Locale.US).equals(SHAREDUPPER))) {
                ls.recognised(i);
                retVal=true;
                i++;
                if (debug)
                    ctx.write("parallel|shared=true\n");
                if (i < src.size()) {
                    t = src.get(i);
                } else {
                    break;
                }
                if (t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                        && t.content.toUpperCase(Locale.US).equals(RETRYUPPER)) {
                    ls.recognised(i);
                    i++;
                    if (debug)
                        ctx.write("retry token true\n");
                    if (i < src.size()) {
                        t = src.get(i);
                    } else {
                        break;
                    }

                }
            }
        }
        return retVal;
    }
    static Pair<Integer, String> open(ScriptRunnerContext ctx, List<LexerToken> src, LexState ls) {
        int state = NOMATCH;
        String maybeDbName = null;
        for (int i = 0; i < src.size(); i++) {
            LexerToken t = src.get(i);
            if (t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                    && t.content.toUpperCase(Locale.US).equals(OPENUPPER)) {
                ls.recognised(i);
                i++;
                state = OPENREADWRITE;
                if (debug)
                    ctx.write("open=true\n");
                if (i < src.size()) {
                    t = src.get(i);
                } else {
                    break;
                }
                if (t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                        && t.content.toUpperCase(Locale.US).equals(READUPPER)) {

                    i++;
                    state = OPENREADWRITE;
                    if (debug)
                        ctx.write("read token true\n");
                    if (i < src.size()) {
                        t = src.get(i);
                    } else {
                        break;
                    }
                    if (t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                            && t.content.toUpperCase(Locale.US).equals(ONLYUPPER)) {
                        ls.recognised(i - 1);
                        ls.recognised(i);
                        i++;
                        state = OPENREADONLY;
                        if (debug)
                            ctx.write("readonly=true\n");
                        if (i < src.size()) {
                            t = src.get(i);
                        } else {
                            break;
                        }
                    } else {
                        if (t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                                && t.content.toUpperCase(Locale.US).equals(WRITEUPPER)) {
                            ls.recognised(i - 1);
                            ls.recognised(i);
                            i++;
                            if (debug)
                                ctx.write("readwrite=true\n");
                            if (i < src.size()) {
                                t = src.get(i);
                            } else {
                                break;
                            }
                            if (t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                                    && t.content.toUpperCase(Locale.US).equals(RECOVERUPPER)) {
                                ls.recognised(i);
                                state = OPENREADWRITERECOVER;
                                i++;
                                if (debug)
                                    ctx.write("recover=true\n");
                                if (i < src.size()) {
                                    t = src.get(i);
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    if (t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                            && t.content.toUpperCase(Locale.US).equals(RECOVERUPPER)) {
                        ls.recognised(i);
                        i++;
                        state = OPENREADWRITERECOVER;
                        if (debug)
                            ctx.write("recover=true\n");
                        if (i < src.size()) {
                            t = src.get(i);
                        } else {
                            break;
                        }
                    }
                }

                if (!ls.knownToken(t.content)) {
                    maybeDbName = t.content;// need to check if this is a token
                    ls.recognised(i);
                    if (debug)
                        ctx.write("maybedbname:" + maybeDbName + "\n");
                }
            }
        }
        return new Pair<Integer, String>(state, maybeDbName);
    }

    static Pair<Integer, String> mount(ScriptRunnerContext ctx, List<LexerToken> src, LexState ls) {
        int state = NOMATCH;
        String maybeDbName = null;
        for (int i = 0; i < src.size(); i++) {
            LexerToken t = src.get(i);
            if (t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                    && t.content.toUpperCase(Locale.US).equals(MOUNTUPPER)) {
                ls.recognised(i);
                state = MOUNTMATCH;
                i++;
                if (debug)
                    ctx.write("mount=true\n");
                if (i < src.size()) {
                    t = src.get(i);
                } else {
                    break;
                }

                if (!ls.knownToken(t.content)) {
                    maybeDbName = t.content;// need to check if this is a token
                    ls.recognised(i);
                    if (debug)
                        ctx.write("maybedbname:" + maybeDbName + "\n");
                }
            }
        }
        return new Pair<Integer, String>(state, maybeDbName);
    }

    static String expandEnv(String in) {
        // maybe ignore if quoted
        return ScriptUtils.expandVariables(in);
    }

    static String dotName(ScriptRunnerContext ctx, List<LexerToken> src, LexState ls, int i, String current, int offset,
            String all) {
        int length = src.size();
        // if ! ' or "" go until whitespace \r \n or $ - and mark tokens as
        // called for
        if ((current != null) && ((current.startsWith("'") || current.startsWith("\"")))) { //$NON-NLS-1$ //$NON-NLS-2$
            return "";
        }
        // look for whitespace \r \n $ /
        int start = src.get(i).begin + offset;
        for (; start < all.length(); start++) {
            Character c = all.charAt(start);
            if (c.equals("/") && (start + 1 < all.length() && all.charAt(start + 1) == '*')) { //$NON-NLS-1$
                // start--;
                break;
            }
            if (c.equals("-") && (start + 1 < all.length() && all.charAt(start + 1) == '-')) { //$NON-NLS-1$
                // start--;
                break;
            }
            if (Character.isWhitespace(c)) {
                // start--;
                break;
            }
            if (c.equals('\r') || c.equals('\n')) {
                // start --;
                break;
            }
        }
        start--;// gone off the end or hit something.
        // mark from src.get(i).begin+offset; to [current] start as read.
        for (int curToken = i; curToken < length; curToken++) {
            if (src.get(curToken).begin > start) {
                break;
            }
            ls.recognised(curToken);
        }
        return all.substring(src.get(i).end, start + 1);// what could go wrong?
                                                        // i could be a ' or " -
                                                        // I believe -no . use
                                                        // except in this file
                                                        // name so concat will
                                                        // be valid . extension
                                                        // could be a keyword.
    }

    static String pfile(ScriptRunnerContext ctx, List<LexerToken> src, LexState ls, String all) {
        for (int i = 0; i < src.size(); i++) {
            List<LexerToken> lexOutput = src;
            String upper = lexOutput.get(i).content.toUpperCase(Locale.US);
            if (true) {
                // reminder - pfile can be single quoted [can it be double
                // quoted..]
                // might be aneasier way.. checking explicitly for
                // PFILE="or'a/file" PfIlE = "or'a/file" pfile= "or' a file
                if (debug)
                    ctx.write("unknown token:" + upper + "\n");
                // need to handle pfile =
                boolean itspfile = false;
                if ((upper.startsWith("PFILE=") && (!upper.equals("PFILE=")))) { //$NON-NLS-1$ //$NON-NLS-2$
                    String theToken = lexOutput.get(i).content;
                    String maybePfile = theToken.substring(theToken.indexOf("=") + 1); //$NON-NLS-1$
                    if (maybePfile != null && maybePfile.length() > 0) {
                        ls.recognised(i);
                        return expandEnv(
                                maybePfile + dotName(ctx, src, ls, i, maybePfile, theToken.indexOf("=") + 1, all));// might //$NON-NLS-1$
                                                                                                                    // be
                                                                                                                    // quoted
                                                                                                                    // or
                                                                                                                    // double
                                                                                                                    // quoted..
                    }
                } else {
                    if (true) {
                        if ((i - 2 >= 0) && (lexOutput.get(i - 1).content.equals("=") //$NON-NLS-1$
                                && lexOutput.get(i - 2).content.toUpperCase(Locale.US).equals("PFILE"))) { //$NON-NLS-1$
                            ls.recognised(i);
                            ls.recognised(i - 1);
                            ls.recognised(i - 2);
                            return expandEnv(lexOutput.get(i).content
                                    + dotName(ctx, src, ls, i, lexOutput.get(i).content, 0, all));
                        } else {
                            if ((i - 1 >= 0) && (lexOutput.get(i).content.toUpperCase(Locale.US).equals("PFILE="))) { //$NON-NLS-1$
                                ls.recognised(i - 1);
                                ls.recognised(i);
                                return expandEnv(lexOutput.get(i).content
                                        + dotName(ctx, src, ls, i, lexOutput.get(i).content, 0, all));
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    static String pluggableDatabase(ScriptRunnerContext ctx, List<LexerToken> src, LexState ls) {
        int i = 1;
        ArrayList<Integer> recognised = new ArrayList<Integer>();
        String maybeDbName = null;
        if (src.size() > 3) {
            LexerToken t = src.get(i);
            if (t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                    && t.content.toUpperCase(Locale.US).equals(PLUGGABLEUPPER)) {
                recognised.add(i);
                // ls.recognised(i);
                i++;
                if (debug)
                    ctx.write("pluggable=true\n");
                if (i < src.size()) {
                    t = src.get(i);
                } else {
                    i = 0;
                }
                if (i != 0 && t != null && t.content != null && t.type.equals(Token.IDENTIFIER)
                        && t.content.toUpperCase(Locale.US).equals(DATABASEUPPER)) {
                    recognised.add(i);
                    // ls.recognised(i);
                    i++;
                    if (debug)
                        ctx.write("database=true\n");
                    if (i < src.size()) {
                        t = src.get(i);
                    } else {
                        i = 0;
                    }
                }

                if (i != 0 && !ls.knownToken(t.content)) {
                    maybeDbName = t.content;// need to check if this is a token
                    recognised.add(i);
                    for (Integer marked : recognised) {
                        ls.recognised(marked);
                    }
                    if (debug)
                        ctx.write("maybedbname:" + maybeDbName + "\n");
                }
            }
        }
        return maybeDbName;
    }

    private static class LexState {

        private HashSet<String> left = null;
        private HashSet<String> startupTokens = null;
        private TokenState[] checkUsed;
        private List<LexerToken> lexOutput;

        private LexState() {
        }

        public LexState(List<LexerToken> src) {
            lexOutput = src;
            checkUsed = new TokenState[src.size()];
            for (int i = 0; i < src.size(); i++) {
                checkUsed[i] = TokenState.DEFAULT;
            }
            checkUsed[0] = TokenState.USED;/* startup */
        }

        public TokenState[] getCheckUsed() {
            return checkUsed;
        }

        public void recognised(int i) {
            if (debug)
                System.out.println("Recognised: " + i + " " + lexOutput.get(i).content);
            checkUsed[i] = TokenState.USED;
        }

        public boolean knownToken(String tokenValIn) {
            if (tokenValIn == null) {
                return false;
            }
            if (startupTokens == null) {
                startupTokens = new HashSet<String>();
                for (String tokenVal : new String[] { "STARTUP", "RESTRICT", "PFILE", "QUIET", "MOUNT", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
                        "RECOVER", "NOMOUNT", "PLUGGABLE", "DATABASE", "FORCE", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
                        "UPGRADE", "OPEN", "READ", "WRITE", "READ", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
                        "ONLY", "FORCE", //$NON-NLS-1$ //$NON-NLS-2$
                        "UPGRADE", "RESTRICT", "PFILE", "=", "DOWNGRADE", "QUIET", "RETRY", "PARALLEL", "SHARED" }) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$
                    startupTokens.add(tokenVal);
                }
            }
            return startupTokens.contains(tokenValIn.toUpperCase(Locale.US));
        }

        public boolean allAccountedFor() {
            for (TokenState ts : checkUsed) {
                if (ts.equals(TokenState.DEFAULT)) {
                    return false;
                }
            }
            return true;
        }

        /**
         * left have to be keywords
         * 
         * @param ctx
         * @return
         */
        public boolean populateLeft(ScriptRunnerContext ctx) {
            if (left == null) {
                left = new HashSet<String>();
                for (int i = 0; i < lexOutput.size(); i++) {
                    if (!(checkUsed[i].equals(TokenState.USED))) {
                        String upper = lexOutput.get(i).content.toUpperCase(Locale.US);
                        if (!knownToken(upper)) {
                            if (debug)
                                ctx.write("unknown token:" + upper + "\n");
                            return false;
                        }
                        if (!left.add(upper)) {
                            if (debug)
                                ctx.write(upper + " DUPLICATE \n");
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public boolean leftRemove(String there) {
            return left.remove(there.toUpperCase(Locale.US));
        }

        public boolean leftContains(String there) {
            return left.contains(there.toUpperCase(Locale.US));
        }

        public boolean leftIsEmpty() {
            return left.isEmpty();
        }

        public String remainingTokens() {
            String output = ""; //$NON-NLS-1$
            for (String val : left) {
                output += val + " "; //$NON-NLS-1$
            }
            return output;
        }
    }

    static String getConName(Connection conn, ScriptRunnerContext ctx) {
        String header = "con_name "; //$NON-NLS-1$
        String pdb = null;
        boolean nonPdb = false;
        try {
            String querySql = null;
            Connection currConn = ctx.getCurrentConnection();
            DBUtil dbUtil = DBUtil.getInstance(currConn);
            if (VersionTracker.getDbVersion(DefaultConnectionIdentifier.createIdentifier(currConn))
                    .compareTo(new Version("12.1")) >= 0) { //$NON-NLS-1$
                if (dbUtil
                        .executeOracleReturnOneCol("select NVL(SYS_CONTEXT('USERENV','CDB_NAME'),'1') from dual", null) //$NON-NLS-1$
                        .equals("1")) { //$NON-NLS-1$
                    // Non Consolidated
                    pdb = null;
                    nonPdb = true;
                }
                // above or = 12.1
                querySql = "SELECT substr(SYS_CONTEXT ('USERENV', 'CON_NAME'),1,30) CON_NAME from dual"; //$NON-NLS-1$
                if (debug)
                    ctx.write(querySql + "\n");
            } else {
                pdb = null;
                nonPdb = true;
            }
            if (nonPdb == false) {
                ArrayList<String> localBind = new ArrayList<String>();
                String result = dbUtil.executeReturnOneCol(querySql, localBind);
                if (result != null) {
                    if (!(result.equals("CDB$ROOT"))) { //$NON-NLS-1$
                        pdb = '"' + result + '"';
                    }
                } else {
                    ctx.write(header + Messages.getString("SHOWPARAMETERSNORESULTSET")); //$NON-NLS-1$
                }
            }
        } catch (Exception e) {
            Logger.warn(new Startup().getClass(), e.getStackTrace()[0].toString(), e);
        }
        return pdb;
    }

    public static String returnConNameIfPublic(Connection conn) {
        String retVal = null;
        CallableStatement stmt = null;
        if (conn != null && (conn instanceof OracleConnection)) {
            boolean amILocked = LockManager.lock(conn);

            try {
                if (amILocked) {
                    stmt = conn.prepareCall(new StringBuffer("DECLARE \n") //$NON-NLS-1$
                            .append(
                                    "CHECKONE VARCHAR2(1000):=NULL; \n")
                            .append(
                                    "CON_ID number; \n")
                            .append(
                                    "BEGIN \n")
                            .append(
                                    " BEGIN \n")
                            .append(
                                    "  if (USER='PUBLIC') THEN \n")
                            .append(
                                    "   CON_ID:=TO_NUMBER(sys_context('USERENV' ,'CON_ID')); /* throws exception if <12.1 */ \n")
                            .append(
                                    "   if ((CON_ID IS NOT NULL) AND (CON_ID>1)) \n")
                            .append(
                                    "   THEN \n")
                            .append(
                                    "    CHECKONE:=sys_context('USERENV','CON_NAME'); \n")
                            .append(
                                    "   END IF; \n")
                            .append(
                                    "  END IF; \n")
                            .append(
                                    " EXCEPTION \n")
                            .append(
                                    " WHEN OTHERS THEN \n")
                            .append(
                                    "  CHECKONE :=NULL; \n")
                            .append(
                                    " END; \n")
                            .append(
                                    " :CHECKFORONE:=CHECKONE; \n")
                            .append(
                                    "END;")
                            .toString());
                    stmt.registerOutParameter(1, java.sql.Types.VARCHAR); // $NON-NLS-1$
                    stmt.executeUpdate(); // $NON-NLS-1$
                    retVal = stmt.getString(1); // $NON-NLS-1$
                    if (stmt.wasNull() == true) {
                        retVal = null;
                    }
                }
            } catch (Exception e) {
                retVal = null;
            } finally {
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        Logger.ignore(new Startup().getClass(), e);
                    }
                }
                if (amILocked) {
                    LockManager.unlock(conn);
                }
            }

        }
        return retVal;
    }
}
