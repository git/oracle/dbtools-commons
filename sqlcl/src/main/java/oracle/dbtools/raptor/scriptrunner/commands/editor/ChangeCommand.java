/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands.editor;

import java.sql.Connection;
import java.util.StringTokenizer;

import oracle.dbtools.common.utils.StringUtils;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtSubType;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand.StmtType;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=ListCommand.java"
 *         >Barry McGillin</a>
 *
 */
public class ChangeCommand extends CommandListener {

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		boolean amIChange = cmd.getStmtType().equals(StmtType.G_C_SQLPLUS) && cmd.getStmtSubType().equals(StmtSubType.G_S_CHANGE);
		if (amIChange) {
			try {
				if (ctx.getSQLPlusBuffer() != null) {
					String baseString = cmd.getSQLOrig();
					String findString = null, replaceString = null;
					StringTokenizer st = new StringTokenizer(baseString, StringUtils.getFirstNonAlpha(baseString)!=null?StringUtils.getFirstNonAlpha(baseString):"/"); //$NON-NLS-1$
					if (st.hasMoreTokens()) {
						st.nextToken();
					}
					if (st.hasMoreTokens()) {
						findString = st.nextToken();
					}
					if (st.hasMoreTokens()) {
						replaceString = st.nextToken();
					}
					if (findString != null & replaceString != null) {
						ctx.write(ctx.getSQLPlusBuffer().getBufferSafe().change(findString, replaceString));
						//ctx.write(ctx.getSQLPlusBuffer().getBufferSafe().listStar(false));
						return true;
					} else if (findString != null) {
						ctx.write(ctx.getSQLPlusBuffer().getBufferSafe().change(findString));
						//ctx.write(ctx.getSQLPlusBuffer().getBufferSafe().listStar(false));
						return true;

					} else if (findString == null) {
						ctx.write(Messages.getString("ChangeCommand.1")); //$NON-NLS-1$
						return true;
					}
				}
			} catch (java.util.regex.PatternSyntaxException e) {
				ctx.write(Messages.getString("ChangeCommand.1")); //$NON-NLS-1$
				return true;
			}
		}
		return false;
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}
}