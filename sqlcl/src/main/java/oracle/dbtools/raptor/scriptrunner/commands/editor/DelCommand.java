/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands.editor;

import java.io.StringReader;
import java.sql.Connection;

import oracle.dbtools.plusplus.IBuffer;
import oracle.dbtools.raptor.datatypes.oracle.sql.INTEGER;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SqlParserProvider;
import oracle.dbtools.raptor.newscriptrunner.commands.HelpMessages;

/**
 * <code>DEL [n | n m | n * | n LAST | * | * n | * LAST | LAST]</code>
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=DelCommand.java"
 *         >Barry McGillin</a>
 *
 */
public class DelCommand extends CommandListener implements IHelp{
	private static final String DEL = "DEL"; //$NON-NLS-1$
	private static final String CMD = "del"; //$NON-NLS-1$
	private static final String STAR = "*"; //$NON-NLS-1$
	private static final String LAST = "last"; //$NON-NLS-1$
	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String sqlpcmd = cmd.getSql().trim();
		String[] cmds = sqlpcmd.split("\\s+"); //$NON-NLS-1$
		boolean trim = false;
		if (cmds.length >= 1 && cmds[0].matches(CMD)) {
			if (ctx.getSQLPlusBuffer() != null) {
				IBuffer buffer = ctx.getSQLPlusBuffer();
				if (SqlParserProvider.getScriptParserIterator(ctx, new StringReader(buffer.getBufferSafe().getBufferString())).hasNext()) {
					if (SqlParserProvider.getScriptParserIterator(ctx, new StringReader(buffer.getBufferSafe().getBufferString())).next().getStmtType().equals(SQLCommand.StmtType.G_C_SQL)) {
						trim=true;
					}
				}
				if (cmds.length == 1) {
					ctx.write(buffer.getBufferSafe().delete());
				} else if (cmds.length == 2) {
					String delOption = cmds[1];
					int x = 0;
					if (delOption.equals(STAR)) {
						ctx.write(buffer.getBufferSafe().deleteStar());
					} else if (delOption.toLowerCase().startsWith("la")) { //$NON-NLS-1$
						ctx.write(buffer.getBufferSafe().deleteLast(trim));
					} else {
						try {
							x = Integer.parseInt(delOption);
							ctx.write(buffer.getBufferSafe().delete(x));
						} catch (NumberFormatException e) {
							ctx.write(getHelp());
						}
					}

				} else if (cmds.length==3) {
					String delOption1 = cmds[1];
					String delOption2 = cmds[2];
//					 if (delOption1.equals(STAR)||delOption2.equals(STAR)) {
//						if (delOption1.equals(STAR)) {
//							if (isAnInteger(delOption2)) {
//								ctx.write(buffer.getBufferSafe().deleteStarN(Integer.parseInt(delOption2)));
//							} else if (delOption2.toLowerCase().startsWith("l")) { //$NON-NLS-1$
//								ctx.write(buffer.getBufferSafe().deleteStarN(buffer.getBufferSafe().size()));
//							}
//						} else if (delOption2.equals(STAR)) {
//							if (isAnInteger(delOption1)) {
//								ctx.write(buffer.getBufferSafe().deleteNStar(Integer.parseInt(delOption1)));
//							}
//						}
//					}
					 if (delOption1.equals(STAR) || delOption2.equals(STAR)) {
						if (delOption1.equals(STAR)) {
							if (isAnInteger(delOption2)) {
								int b = Integer.parseInt(delOption2);
								if (b<buffer.getBufferSafe().getCurrentLine()) {
									ctx.write(Messages.getString("ListCommand.0")); //$NON-NLS-1$
								}else {
								  ctx.write(buffer.deleteStarN(Integer.parseInt(delOption2),trim));
								}
							} else if (delOption2.equals(STAR)) {
								ctx.write(buffer.deleteLast(trim));
							} else if (delOption2.toLowerCase().startsWith("l")) { //$NON-NLS-1$
								ctx.write(buffer.deleteStarN(buffer.linesInBuffer(),trim));
							}
						} else if (delOption2.equals(STAR)) {
							if (isAnInteger(delOption1)) {
								ctx.write(buffer.deleteNStar(Integer.parseInt(delOption1),trim));
							} else if (delOption1.equals(STAR)) {
								ctx.write(buffer.deleteLast(trim));
							}
						}
					}   else if (isAnInteger(delOption1)) {
						if (isAnInteger(delOption2)) {
							ctx.write(buffer.getBufferSafe().delete(Integer.parseInt(delOption1),Integer.parseInt(delOption2)));
						} else if (delOption2.toLowerCase().startsWith("l")){ //$NON-NLS-1$
							ctx.write(buffer.getBufferSafe().delete(Integer.parseInt(delOption1), buffer.getBufferSafe().size()));
						} else {
							ctx.write (Messages.getString("ListCommand.1")); //$NON-NLS-1$
						}
					}	else {
						if (delOption1.equalsIgnoreCase("la")||delOption1.equalsIgnoreCase("las")||delOption1.equalsIgnoreCase("last")) {
							int n = buffer.getBufferSafe().size();
							int m = 0;
 							if (isAnInteger(delOption2)) {
								m = Integer.parseInt(delOption2);
							}
							ctx.write(buffer.getBufferSafe().delete(n,m,trim)); 
						} else {
							ctx.write(Messages.getString("DelCommand.2")); //$NON-NLS-1$
						}
					}
				} else {
					//More weirdness. Looking away again!
					return false;
				}
				return true;
			}
		}
		return false;
	}
	private boolean isAnInteger(String delOption) {
		try {
		    Integer.parseInt(delOption);
		    return true;
		} catch (NumberFormatException e) {
		    return false;
		}
	}
	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}
	@Override
	public String getCommand() {
		return DEL;
	}
	@Override
	public String getHelp() {
		return HelpMessages.getString(DEL);
	}
	@Override
	public boolean isSqlPlus() {
		return true;
	}
}