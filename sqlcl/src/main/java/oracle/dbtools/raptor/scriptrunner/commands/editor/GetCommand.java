/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands.editor;

import java.nio.file.Path;
import java.sql.Connection;
import java.text.MessageFormat;

import oracle.dbtools.common.utils.FileUtils;
import oracle.dbtools.plusplus.FileSystem;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.HelpMessages;

/**
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Get.java"
 *         >Barry McGillin</a>
 *
 */
public class GetCommand extends CommandListener implements IHelp{
	private static final String GET = "GET"; //$NON-NLS-1$
	private static final String FILE = "file"; //$NON-NLS-1$
	private static final String LIST = "list"; //$NON-NLS-1$
	private static final String NOLIST = "nolist"; //$NON-NLS-1$

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String sqlpcmd = cmd.getSql().trim();
		String[] cmds = sqlpcmd.split("\\s+"); //$NON-NLS-1$
		boolean list = true;
		String filename = null;
		//Parse command
		switch (cmds.length) {
		case 1:
			ctx.write(Messages.getString("GetCommand.1")); //$NON-NLS-1$
			ctx.write(getHelp());
			return true;
		case 2:
			filename = ctx.prependCD(cmds[1]);
			break;
		case 3:
			if (cmds[1].toLowerCase().equals(FILE)) {
				filename = ctx.prependCD(cmds[2]);
			} else {
				filename = ctx.prependCD(cmds[1]);
				if (cmds[2].toLowerCase().equals(NOLIST)) {
					list = false;
				} else if (cmds[2].toLowerCase().equals(LIST)) {
					list = true;
				} else {
					ctx.write(Messages.getString("GetCommand.2")); //$NON-NLS-1$
					return true;
				}
			}
			break;
		case 4:
			if (!cmds[1].toLowerCase().equals(FILE)) {
				ctx.write(Messages.getString("GetCommand.3")); //$NON-NLS-1$
			}
			if (cmds[3].toLowerCase().equals(NOLIST)) {
				list = false;
			} else if (cmds[3].toLowerCase().equals(LIST)) {
				list = true;
			} else {
				ctx.write(Messages.getString("GetCommand.4")); //$NON-NLS-1$
				return true;
			}
			filename = ctx.prependCD(cmds[2]);
			break;
		default:
			ctx.write(Messages.getString("GetCommand.5")); //$NON-NLS-1$
			return true;
		}

		if (filename.lastIndexOf(".")==-1) { //$NON-NLS-1$
			String suffix = (String) ctx.getProperty(ScriptRunnerContext.SUFFIX);
			if (suffix.length()>0)
			filename = filename + "." + suffix; //$NON-NLS-1$
		}
		Path path = FileUtils.getFileOnSQLPATH(ctx, filename,true);
		FileSystem.Results error=FileSystem.Results.FILE_NOT_FOUND;
		if (path!=null) {
		 error = ctx.getSQLPlusBuffer().getBufferSafe().get(path.toAbsolutePath().toString(), list, (String) ctx.getProperty(ScriptRunnerContext.SUFFIX));
		}
		switch (error) {
		case OK:
			if (list) {
				if ( isSqlPlus() )
					ctx.write(ctx.getSQLPlusBuffer().getBufferSafe().list());
				else 
					ctx.write(ctx.getSQLPlusBuffer().getBufferSafe().list(false));
			}
			break;
		case FILE_NOT_FOUND:
		case FILE_NOT_ACCESSIBLE:
			String realFile = filename.lastIndexOf(".")>-1?filename:filename+"."+ctx.getProperty(ScriptRunnerContext.SUFFIX); //$NON-NLS-1$ //$NON-NLS-2$
			ctx.write(MessageFormat.format(Messages.getString("GetCommand.10"), new Object[] { realFile })); //$NON-NLS-1$
			break;
		default:
			break;
		}
		return true;
	}

	@Override
	public String getCommand() {
		return GET;
	}

	@Override
	public String getHelp() {
		return HelpMessages.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return true;
	}
}
