/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands.editor;

import java.io.StringReader;
import java.sql.Connection;

import oracle.dbtools.plusplus.IBuffer;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.SQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.SqlParserProvider;
import oracle.dbtools.raptor.newscriptrunner.commands.HelpMessages;

/**
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=ListCommand.java"
 *         >Barry McGillin</a>
 *
 */
public class ListCommand extends CommandListener implements IHelp{
	private static final String LIST = "LIST"; //$NON-NLS-1$
	private static final String CMD = "list|lis|li|l"; //$NON-NLS-1$
	private static final String STAR = "*"; //$NON-NLS-1$
	private static final String LAST = "last"; //$NON-NLS-1$

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
		String sqlpcmd = cmd.getSql().trim().toLowerCase();
		String[] cmds = sqlpcmd.split("\\s+"); //$NON-NLS-1$
		if (cmds.length >= 1 && cmds[0].matches(CMD)) {
			boolean trim = false;
			if (ctx.getSQLPlusBuffer() != null) {
				IBuffer buffer = ctx.getSQLPlusBuffer().getBufferSafe();
				if (SqlParserProvider.getScriptParserIterator(ctx, new StringReader(buffer.getBufferString())).hasNext()) {
					if (SqlParserProvider.getScriptParserIterator(ctx, new StringReader(buffer.getBufferString())).next().getStmtType().equals(SQLCommand.StmtType.G_C_SQL)) {
						trim=true;
					}
				}
				if (cmds.length == 1) {
					if ( isSqlPlus() )
					     ctx.write(buffer.list());
					else 
						 ctx.write(buffer.list(trim));
				} else if (cmds.length == 2) {
					String listOption = cmds[1];
					int x = 0;
					if (listOption.equals(STAR)) {
						ctx.write(buffer.listStar(trim));
					} else if(listOption.equals(LAST)) {
						ctx.write(buffer.listLast(trim));
					} else {
						try {
							x = Integer.parseInt(listOption);
							ctx.write(buffer.list(x,trim));
						} catch (NumberFormatException e) {
							ctx.write(getHelp());
						}
					}

				} else if (cmds.length >= 3) {
					String listOption1 = cmds[1];
					String listOption2 = cmds[2];
					if (listOption1.equals(LAST)|| listOption2.equals(LAST)) {
						int last = buffer.getCurrentLine();
						if (isAnInteger(listOption2) && last > Integer.parseInt(listOption2)) {
							ctx.write(Messages.getString("ListCommand.0")); //$NON-NLS-1$
						} else 	if (isAnInteger(listOption1) && listOption2.equals(LAST)) {
								ctx.write(buffer.list(Integer.parseInt(listOption1),last,trim));
						} else if (listOption1.equals(STAR)&& listOption2.equals(LAST)) {
							ctx.write(buffer.listStarN(buffer.linesInBuffer(), trim));
						} else {
						ctx.write(buffer.listLast(trim));
						}
					} else
					if (listOption1.equals(STAR) || listOption2.equals(STAR)) {
						if (listOption1.equals(STAR)) {
							if (isAnInteger(listOption2)) {
								int b = Integer.parseInt(listOption2);
								if (b<buffer.getCurrentLine()) {
									ctx.write(Messages.getString("ListCommand.0")); //$NON-NLS-1$
								}else {
								  ctx.write(buffer.listStarN(Integer.parseInt(listOption2),trim));
								}
							} else if (listOption2.equals(STAR)) {
								ctx.write(buffer.listLast(trim));
							} else if (listOption2.toLowerCase().startsWith("l")) { //$NON-NLS-1$
								ctx.write(buffer.listStarN(buffer.linesInBuffer(),trim));
							}
						} else if (listOption2.equals(STAR)) {
							if (isAnInteger(listOption1)) {
								ctx.write(buffer.listNStar(Integer.parseInt(listOption1),trim));
							} else if (listOption1.equals(STAR)) {
								ctx.write(buffer.listLast(trim));
							}
						}
					} else if (isAnInteger(listOption1) ) {
						if (isAnInteger(listOption2)) {
							ctx.write(buffer.list(Integer.parseInt(listOption1), Integer.parseInt(listOption2),trim));
						} else if (listOption2.toLowerCase().startsWith("l")){ //$NON-NLS-1$
							ctx.write(buffer.list(Integer.parseInt(listOption1), buffer.size(),trim));
						}  else {
							ctx.write (Messages.getString("ListCommand.1")); //$NON-NLS-1$
						}
					}
				} else {
					// More weirdness. Looking away again!
					return false;
				}
				return true;
			}
		}
		return false;
	}

	private boolean isAnInteger(String listOption) {
		try {
			Integer.parseInt(listOption);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public String getCommand() {
		return LIST;
	}

	@Override
	public String getHelp() {
		return HelpMessages.getString(LIST);
	}

	@Override
	public boolean isSqlPlus() {
		return true;
	}
}