/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands.editor;

import java.sql.Connection;
import java.text.MessageFormat;

import oracle.dbtools.plusplus.FileSystem;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.HelpMessages;

/**
 * SA
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Get.java">Barry McGillin</a> 
 *
 */
public class SaveCommand extends CommandListener implements IHelp{
	private static final String CMD = "save"; //$NON-NLS-1$
	private static final String FILE = "file"; //$NON-NLS-1$

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {

		String sqlpcmd = cmd.getSql().trim();
		String[] cmds = sqlpcmd.split("\\s+"); //$NON-NLS-1$
		String filename=""; //$NON-NLS-1$
		FileSystem.State fileCreate=FileSystem.State.UNKNOWN;
		switch (cmds.length) {
		case 1:
			ctx.write(Messages.getString("SaveCommand.6")); //$NON-NLS-1$
			ctx.write(getHelp());
			return true;
		case 2:
			filename=ctx.prependCD(cmds[1]);
			fileCreate=FileSystem.State.CREATE;
			break;
		case 3:
			if (cmds[1].toLowerCase().equals(FILE)) {
				filename = ctx.prependCD(cmds[2]);
				fileCreate=FileSystem.State.CREATE;
			} else {
				filename = ctx.prependCD(cmds[1]);
				fileCreate = getMode(cmds[2]);
			}
			break;
		case 4:
			filename = ctx.prependCD(cmds[2]);
			fileCreate = getMode(cmds[3]);
			break;
		default:
			break;
		}
		if (filename.lastIndexOf(".")==-1) { //$NON-NLS-1$
			String suffix = (String) ctx.getProperty(ScriptRunnerContext.SUFFIX);
			if (suffix.length()>0)
			filename = filename + "." + suffix; //$NON-NLS-1$
		}
		switch (fileCreate) {
		case UNKNOWN:
			ctx.write(Messages.getString("SaveCommand.0")); //$NON-NLS-1$
			return true;
		case CREATE:
			ctx.write(ctx.getSQLPlusBuffer().save(filename, FileSystem.State.CREATE,(String) ctx.getProperty(ScriptRunnerContext.SUFFIX)));
			break;
		case APPEND:
			ctx.write(ctx.getSQLPlusBuffer().save(filename, FileSystem.State.APPEND, (String) ctx.getProperty(ScriptRunnerContext.SUFFIX)));
			break;
		case REPLACE:
			ctx.write(ctx.getSQLPlusBuffer().save(filename, FileSystem.State.REPLACE, (String) ctx.getProperty(ScriptRunnerContext.SUFFIX)));
			break;
		case BADMODE:
			ctx.write(Messages.getString("SaveCommand.0")); //$NON-NLS-1$
			ctx.write(getHelp());
			break;
		default:
			break;
		}
		return true;
	}

	private FileSystem.State getMode(String string) {
		if (string == null)
			return FileSystem.State.UNKNOWN;
		else if (string.toLowerCase().trim().equals("cre")||string.toLowerCase().trim().equals("create")||string.toLowerCase().trim().equals("crea")||string.toLowerCase().trim().equals("creat"))  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		 return FileSystem.State.CREATE;
		else if (string.toLowerCase().trim().equals("app")||string.toLowerCase().trim().equals("append")||string.toLowerCase().trim().equals("appe")||string.toLowerCase().trim().equals("appen")) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		 return FileSystem.State.APPEND;
		else if (string.toLowerCase().trim().equals("rep")||string.toLowerCase().trim().equals("replace")||string.toLowerCase().trim().equals("repl")||string.toLowerCase().trim().equals("repla")||string.toLowerCase().trim().equals("replac"))  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			return FileSystem.State.REPLACE;
		else
		return FileSystem.State.BADMODE;
	}

	@Override
	public String getCommand() {
		return CMD.toUpperCase();
	}

	@Override
	public String getHelp() {
		return HelpMessages.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return true;
	}


}
