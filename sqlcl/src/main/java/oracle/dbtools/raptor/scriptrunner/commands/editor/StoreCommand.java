/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands.editor;

import java.sql.Connection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.dbtools.plusplus.FileSystem;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;

/**
 * 
 * Store set filename[.ext] create|replace|append
 * 
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Get.java"
 *         >Barry McGillin</a>
 *
 */
public class StoreCommand extends CommandListener implements IHelp {
	private static final String SET = "set"; //$NON-NLS-1$
	private static final String CREATE_PATTERN = "cre|create"; //$NON-NLS-1$
	private static final String APPEND_PATTERN = "app|append"; //$NON-NLS-1$
	private static final String REPLACE_PATTERN = "rep|replace"; //$NON-NLS-1$
	private static final String FILENAME_PATTERN = CREATE_PATTERN + "|" + APPEND_PATTERN + "|" + REPLACE_PATTERN; //$NON-NLS-1$ //$NON-NLS-2$

	@Override
	public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	}

	@Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {	
		String sqlpcmd = cmd.getSql().trim();
		//String[] cmds = sqlpcmd.split("\\s+"); //$NON-NLS-1$
		String[] cmds = null;
		if (sqlpcmd.contains("'")) {
			cmds = sqlpcmd.split("\'?( |$)(?=(([^\']*\'){2})*[^\']*$)\'?"); //$NON-NLS-1$
		} else {
			cmds = sqlpcmd.split("\"?( |$)(?=(([^\"]*\"){2})*[^\"]*$)\"?"); //$NON-NLS-1$	
		}
		
		String filename = null;
		FileSystem.State fileCreate = FileSystem.State.CREATE;
		switch (cmds.length) {
		case 1:
			writeMissingSet(ctx);
			ctx.write(Messages.getString("StoreCommand.1")); //$NON-NLS-1$
			return true;
		case 2:
			if (cmds[1].toLowerCase().equals(SET)) {
				ctx.write(Messages.getString("StoreCommand.6")); //$NON-NLS-1$
				ctx.write(Messages.getString("StoreCommand.1")); //$NON-NLS-1$
			} else {
				writeMissingSet(ctx);
				ctx.write(Messages.getString("StoreCommand.1")); //$NON-NLS-1$
			}
			return true;
		case 3:
			if (!cmds[1].toLowerCase().equals(SET)) {
				writeMissingSet(ctx);
				ctx.write(Messages.getString("StoreCommand.1")); //$NON-NLS-1$
				return true;
			} else {
				if (isReservedName(cmds[2])) {
					writeReserved(ctx);
					return true;
				} else {
					filename = ctx.prependCD(cmds[2]);
				}
			}
			break;
		case 4:
			if (!cmds[1].toLowerCase().equals(SET)) {
				writeMissingSet(ctx);
				ctx.write(Messages.getString("StoreCommand.1")); //$NON-NLS-1$
				return true;
			} else if (!isReservedName(cmds[3])) {
				writeIllegalStoreCommand(ctx);
				ctx.write(Messages.getString("StoreCommand.1")); //$NON-NLS-1$
				return true;
			} else if (isReservedName(cmds[2])) {
				writeReserved(ctx);
				return true;
			} else {
				filename = ctx.prependCD(cmds[2]);
				fileCreate = getMode(cmds[3]);
			}
			break;
		default:
			writeIllegalStoreCommand(ctx);
			ctx.write(Messages.getString("StoreCommand.1")); //$NON-NLS-1$
			return true;
		}
		if (filename.lastIndexOf(".") == -1) { //$NON-NLS-1$
			String suffix = (String) ctx.getProperty(ScriptRunnerContext.SUFFIX);
			if (!suffix.equals("")) //$NON-NLS-1$
			filename = filename + "." + suffix; //$NON-NLS-1$

		}
		if (!fileCreate.equals(FileSystem.State.BADMODE)) {
			ctx.write(ctx.save(filename, fileCreate.ordinal()));
		}
		else {
			ctx.write("SP2-0603: Illegal STORE command\n");
		    ctx.write("Usage: STORE {SET} filename[.ext] [CRE[ATE]|REP[LACE]|APP[END]]\n");
		}
		return true;
	}

		private FileSystem.State getMode(String string) {
			if (string == null)
				return FileSystem.State.UNKNOWN;
			if (string.toLowerCase().trim().equals("cre")||string.toLowerCase().trim().equals("create")||string.toLowerCase().trim().equals("crea")||string.toLowerCase().trim().equals("creat"))  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			 return FileSystem.State.CREATE;
			if (string.toLowerCase().trim().equals("app")||string.toLowerCase().trim().equals("append")||string.toLowerCase().trim().equals("appe")||string.toLowerCase().trim().equals("appen")) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			 return FileSystem.State.APPEND;
			if (string.toLowerCase().trim().equals("rep")||string.toLowerCase().trim().equals("replace")||string.toLowerCase().trim().equals("repl")||string.toLowerCase().trim().equals("repla")||string.toLowerCase().trim().equals("replac"))  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
				return FileSystem.State.REPLACE;
			return FileSystem.State.BADMODE;
		}

	private void writeIllegalStoreCommand(ScriptRunnerContext ctx) {
		ctx.write(Messages.getString("StoreCommand.8")); //$NON-NLS-1$
	}

	private void writeReserved(ScriptRunnerContext ctx) {
		ctx.write(Messages.getString("StoreCommand.9")); //$NON-NLS-1$
	}

	private boolean isReservedName(String string) {
		Pattern pattern = Pattern.compile(FILENAME_PATTERN);
		Matcher matcher = pattern.matcher(string);
		if (matcher != null && matcher.find()) {
			return true;
		}
		return false;
	}

	private void writeMissingSet(ScriptRunnerContext ctx) {
		ctx.write(Messages.getString("StoreCommand.10")); //$NON-NLS-1$

	}

	private void writeHelp(ScriptRunnerContext ctx) {
		ctx.write(getHelp());

	}

	@Override
	public String getCommand() {
		return "STORE"; //$NON-NLS-1$
	}

	@Override
	public String getHelp() {
		return CommandsHelp.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return true;
	}

}
