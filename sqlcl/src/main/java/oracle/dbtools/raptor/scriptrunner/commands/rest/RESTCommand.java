/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands.rest;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.dbtools.db.ConnectionSupport;
import oracle.dbtools.db.DBUtil;
import oracle.dbtools.db.LockManager;
import oracle.dbtools.db.ResultSetFormatter;
import oracle.dbtools.db.SQLPLUSCmdFormatter;
import oracle.dbtools.raptor.console.SQLPlusConsoleReader;
import oracle.dbtools.raptor.newscriptrunner.CommandListener;
import oracle.dbtools.raptor.newscriptrunner.IHelp;
import oracle.dbtools.raptor.newscriptrunner.ISQLCommand;
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;
import oracle.dbtools.raptor.newscriptrunner.commands.CommandsHelp;
import oracle.dbtools.raptor.newscriptrunner.commands.Messages;
import oracle.dbtools.raptor.scriptrunner.commands.rest.RESTExtractor.ExportEntity;
import oracle.dbtools.raptor.scriptrunner.commands.rest.RESTExtractor.ModuleNotFoundException;
import oracle.dbtools.raptor.scriptrunner.commands.rest.RESTExtractor.ModuleSearchOrigin;
import oracle.dbtools.rest.export.RestExport;
import oracle.dbtools.rest.model.RestModule;
import oracle.dbtools.rest.model.RestPrivilege;
import oracle.dbtools.rest.model.RestSchema;
import oracle.dbtools.util.Logger;
import oracle.dbtools.versions.SQLclVersion;
import oracle.jdbc.OracleConnection;

/**
 * @author <a href="mailto:rene.mendez@oracle.com@oracle.com?subject=RESTExportCommand.java" >René Méndez</a>
 *
 */
public class RESTCommand extends CommandListener implements IHelp {

	

	/**
	 *  List Queries
	 */
	private static final String ALL_PRIVS_QUERY = "select NAME, LABEL, DESCRIPTION, COMMENTS from USER_ORDS_PRIVILEGES where schema_id > 1000";
	private static final String SYS_SCHEMAS_QUERY = "select s.PARSING_SCHEMA, m.PATTERN, s.STATUS  from ORDS_METADATA.ORDS_SCHEMAS s, ORDS_METADATA.ORDS_URL_MAPPINGS m where s.url_mapping_id = m.id and S.PARSING_SCHEMA <> 'ORDS_METADATA'";
	private static final String ALL_SCHEMAS_QUERY = "select PARSING_SCHEMA, PATTERN, STATUS from USER_ORDS_SCHEMAS";
	private static final String ALL_MODULES_QUERY = "select NAME, URI_PREFIX PREFIX, STATUS, ITEMS_PER_PAGE from USER_ORDS_MODULES";
	
	/**
	 * Command name constant
	 */
	private static final String CMD = "REST";
    
    /**
     *  Tries to handle the rest events.
     */
    @Override
	public boolean handleEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
	    
	    // need to clean up connection afterwards or reconnect or something for now assume close interim connection omn startup        
	    
	    if (!cmd.getLoweredTrimmedNoWhitespaceSQL().startsWith("rest") ){
	      return false;
	    }
      String sql = cmd.getSql();

	    ArrayList<String> cmds = new ArrayList<String>();
	    splitArgs(sql, cmds);
	    
	    // If the command is not rest then skip
	    if (cmds.size()==0 ||(cmds.size()>0 && !"rest".equalsIgnoreCase(cmds.get(0)))) {
	    	return false;
	    }
		if (conn==null) {
			ctx.write(Messages.getString("ApexCmd.7")); //$NON-NLS-1$
			return true;
		} else  if (!((conn!=null)&&(conn instanceof OracleConnection))){
			return false;
    }
    OracleConnection ocon = (OracleConnection) conn;
	    
	    String option = null;
	    String module = null;
	    
	    // If it is call to rest without arguments or too many arguments, then print the help
	    if (cmds.size() < 2 || cmds.size() >= 4) {
	    	ctx.write(getHelp());
	    	return true;
	    }
	    
	    // Get the subcommand
	    if (cmds.size() >= 2) {
	    	option = cmds.get(1);
	    }
	    
	    // Get the option 1
	    if (cmds.size() >= 3) {
	    	module = cmds.get(2);
	    }
	    
	    
	    try {
	        if ((ocon != null) && ((ctx.getProperty(ScriptRunnerContext.PRELIMAUTH) == null 
	                || ((Boolean)ctx.getProperty(ScriptRunnerContext.PRELIMAUTH)
	                        .equals(Boolean.FALSE))))){
	        	
	            if (DBUtil.isOracleConnectionAlive(ocon)) {
	                boolean amILocked = LockManager.lock(ocon);
	                try {
	                    if (amILocked) {
	                    	if ("modules".equalsIgnoreCase(option)) {
	                    		this.listAllModules(ocon, ctx);
	                    	} else if ("export".equalsIgnoreCase(option)) {
	                    		/**
	                    		 * In case it is an export call
	                    		 * We obtain an export model and use the SQLVisitor
	                    		 * to transform it into pl/sql api calls.
	                    		 * 
	                    		 */
	                    		RESTExtractor caller = new RESTExtractor();
	                    		ExportEntity result = caller.getExport(ocon, module);
	                    		String schemaName = "";
	                    		String ordsVersion = "";
	                    		
	                    		if (result.schema != null) {
	                    			schemaName = result.schema.getSchemaName();
	                    			ordsVersion = result.schema.getOrdsSchemaVersion();
	                    		}
	                    		
	                    		RestExport restExport = new RestExport("SQLcl", SQLclVersion.getSQLclVersion(), schemaName, ordsVersion);
	                    		
	                    		RestSchema schema = result.schema;
								List<RestModule> modules = result.modules;
								List<RestPrivilege> privileges = new ArrayList<RestPrivilege>();
								List<String> rolesToCreate = new ArrayList<String>(result.rolesToCreate);
								
								privileges.addAll(result.privileges.values());
								String generated = restExport.exportModulesRolesPrivileges(schema, modules, rolesToCreate, privileges);
								
								// Workaround for a bug with output in production.
								generated+="\n";
								ctx.write(generated);
	                    	} else if ("privileges".equalsIgnoreCase(option)) {
	                    		this.listAllPrivs(ocon, ctx);
	                    	} else if ("schemas".equalsIgnoreCase(option)) {
	                    		this.listAllSchemas(ocon, ctx);
	                    	} else {
	                    		// Unrecognized command
	                    		ctx.write(this.getHelp());
	                    		return true;
	                    	}
	                        
	                    }
	                } finally {	
	                    if (amILocked) {
	                    	LockManager.unlock(ocon);
	                    }
	                }
	            } else {
	            	ctx.write(Messages.getString("ApexCmd.7")); //$NON-NLS-1$
	            }
	        } else {
	        	ctx.write(Messages.getString("ApexCmd.7")); //$NON-NLS-1$
	        }
	        
	
	    } catch (SQLException e) {
	        if (e.getErrorCode() == 942) {
	        	System.err.println("ORDS is not installed in the current schema.");
	        	return true;
	        }
	        
	        Logger.finest(this.getClass(), e.getLocalizedMessage(), e);
	        ctx.write(e.getLocalizedMessage()+"\n"); //$NON-NLS-1$
	        
	        return true;
	    } catch (IOException f) {
	    	Logger.finest(this.getClass(), f.getLocalizedMessage(), f);
	    	return true;
	    } catch (ModuleNotFoundException g) {
	    	ModuleSearchOrigin origin = g.getOrigin();
	    	
	    	if (origin.name().equals(ModuleSearchOrigin.ALL.name())) {
	    		System.err.println("No modules found in the current schema.");
	    	} else if (origin.name().equals(ModuleSearchOrigin.NAME.name())) {
	    		System.err.println("No module named: " + g.getModuleName());
	    	} else if (origin.name().equals(ModuleSearchOrigin.PATH.name())) {
	    		if ("/".equals(g.getModulePath())) {
	    			System.err.println("No module found with an empty prefix");
	    		} else {
	    			System.err.println("No module found with the name or path specified.");
	    		}
	    	} else {
	    		System.err.println("Unknown module origin");
	    	}
	    	
	    	return true;
	    } finally {
	        //was close interrim connection
	    }
	    
	    return true;
	}

    /**
     * Method that prints out the ORDS modules that belong to the current schema.
     * 
     * @param conn Oracle Connection
     * @param ctx Runner context
     * @throws SQLException when the query returns an error
     * @throws IOException when the formatter gets an error.
     */
	private void listAllModules(Connection conn, ScriptRunnerContext ctx) throws SQLException, IOException {
    	OracleConnection ocon = (OracleConnection) conn;
    	
		PreparedStatement stmt = ocon.prepareStatement(ALL_MODULES_QUERY);
		
		executeGenericCall(ctx, stmt);
    }
    
	/**
	 * 
	 * Method that prints out the available schemas (normally it will display the current
	 * schema).
	 * 
	 * @param conn Oracle Connection
	 * @param ctx Runner context.
	 * @throws SQLException when the query returns an error.
	 * @throws IOException when the formatter gets an error.
	 */
    private void listAllSchemas(Connection conn, ScriptRunnerContext ctx) throws SQLException, IOException {
    	OracleConnection ocon = (OracleConnection) conn;
    	
		PreparedStatement stmt = ocon.prepareStatement(SYS_SCHEMAS_QUERY);
		
		try {
			executeGenericCall(ctx, stmt);
		} catch (SQLException e) {
			// Catching ORA-00942 and executing a query on the view in case the connection
			// lacks the permission to get it directly from ORDS_METADATA.ORDS_SCHEMAS;
			if (e.getErrorCode() == 942) {
				stmt = ocon.prepareStatement(ALL_SCHEMAS_QUERY);
				executeGenericCall(ctx, stmt);
			}
		}
    }
    
    /**
	 * 
	 * Method that prints out the privileges for the current schema.
	 * 
	 * @param conn Oracle Connection
	 * @param ctx Runner context.
	 * @throws SQLException when the query returns an error.
	 * @throws IOException when the formatter gets an error.
	 */
    private void listAllPrivs(Connection conn, ScriptRunnerContext ctx) throws SQLException, IOException {
    	OracleConnection ocon = (OracleConnection) conn;
    	
		PreparedStatement stmt = ocon.prepareStatement(ALL_PRIVS_QUERY);
		
		executeGenericCall(ctx, stmt);
    }
    
    /**
     * Helper method that executes and formats sql calls.
     * 
     * @param ctx Runner Context
     * @param statement SQL statement
     * @throws SQLException
     * @throws IOException
     */
    private void executeGenericCall(ScriptRunnerContext ctx, PreparedStatement statement) throws SQLException, IOException {
    	ResultSet result = statement.executeQuery();
		SQLPLUSCmdFormatter formatter = new SQLPLUSCmdFormatter(ctx);
		formatter.rset2sqlplusShrinkToSize(result, ctx.getBaseConnection(), ctx.getOutputStream());	// No need to send the original statement since it got no hints
    }
  
    /**
     * Splits the command line arguments
     * 
     * @param commandline
     * @param args
     * @return
     */
    protected boolean splitArgs(String commandline, ArrayList<String> args) {
	    final int lineLength = commandline.length();
	    final int terminalIndex = lineLength - 1;
	    final char nullChar = '\0';
	
	    StringBuilder arg = new StringBuilder();
	    StringBuilder quoted = new StringBuilder();
	    char quoteChar = nullChar;
	
	    int i = 0;
	    while (i < lineLength) {
	        char ch = commandline.charAt(i);
	
	        if (quoteChar != nullChar) {
	            quoted.append(ch);
	
	            if (ch == quoteChar) {
	                arg.append(quoted);
	                quoted = new StringBuilder();
	                quoteChar = nullChar;
	            }
	        } else if (ch == '"' || ch == '\'') {
	            quoted.append(ch);
	            quoteChar = ch;
	        } else if (Character.isWhitespace(ch)) {
	            if (arg.length() > 0) {
	                args.add(arg.toString());
	                arg = new StringBuilder();
	            }
	        } else {
	            arg.append(ch);
	        }
	
	
	        if (i == terminalIndex && quoteChar != nullChar) {
	            arg.append(quoteChar);
	            i = i - (quoted.length() - 1);
	            quoted = new StringBuilder();
	            quoteChar = nullChar;
	        }
	
	        i++;
	    }
	
	    if (arg.length() > 0) {
	        args.add(arg.toString());
	    }
	
	    return true;
	}

	/*
     * (non-Javadoc)
     * 
     * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#beginEvent(java .sql.Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
     * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
     */
    @Override
    public void beginEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    	//Nothing to be done
    }

    /*
     * (non-Javadoc)
     * 
     * @see oracle.dbtools.raptor.newscriptrunner.CommandListener#endEvent(java.sql .Connection, oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext,
     * oracle.dbtools.raptor.newscriptrunner.ISQLCommand)
     */
    @Override
    public void endEvent(Connection conn, ScriptRunnerContext ctx, ISQLCommand cmd) {
    	//Nothing to be done
    }
	
	@Override
	public String getCommand() {
		return CMD.toUpperCase();
	}

	@Override
	public String getHelp() {
		return CommandsHelp.getString(getCommand());
	}

	@Override
	public boolean isSqlPlus() {
		return false;
	}



	@Override
	public void beginScript(Connection conn, ScriptRunnerContext ctx) {
		// Nothing to be done
	}

}