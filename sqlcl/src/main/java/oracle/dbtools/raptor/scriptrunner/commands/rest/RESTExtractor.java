/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands.rest;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.dbtools.util.StreamCopy;
import oracle.jdbc.OracleConnection;
import oracle.dbtools.rest.model.*;


/**
 * Class that contains utility methods that are able to extract REST objects from ORDS.
 * 
 * @author josmende
 *
 */
public class RESTExtractor {
	
	private static final String PRIVILEGES_QUERY = "select x.*,  (select listagg(y.pattern,'|^|') within group (order by y.pattern) from USER_ORDS_PRIVILEGE_MAPPINGS y where x.name=y.name) as patterns from USER_ORDS_PRIVILEGE_MODULES pm, USER_ORDS_PRIVILEGES x where pm.module_name = ? and pm.privilege_id = x.id";
	private static final String LEGACY_PRIVILEGES_QUERY = PRIVILEGES_QUERY.replace("USER_", "ALL_");
	private static final String EXTRACT_MODULES_QUERY = "select * from USER_ORDS_MODULES ";
	private static final String PRIVILEGE_ROLES_QUERY = "select pr.role_name, r.schema_id role_owner_id from USER_ORDS_PRIVILEGE_ROLES pr, USER_ORDS_ROLES r where pr.privilege_name = ? and pr.role_id = r.id";
	private static final String LEGACY_PRIVILEGE_ROLES_QUERY = PRIVILEGE_ROLES_QUERY.replace("USER_", "ALL_");
	
	private static final String SCHEMA_INFO_SQL = "select parsing_schema, type, pattern, status, auto_rest_auth, ords.installed_version ords_version from user_ords_schemas"; //$NON-NLS-1$  
	
	/**
	 * Given a moduleName/path, it returns an entity containing the module and related privileges.
	 * If the moduleName/path is empty, it will return all the modules in the current schema, and
	 * related privileges.
	 * 
	 * @param conn - Oracle Connection
	 * @param moduleName - moduleName/path or empty (all)
	 * @return an Export Entity that contains all the modules and privileges
	 * @throws SQLException
	 * @throws IOException
	 * @throws ModuleNotFoundException
	 */
	public ExportEntity getExport(Connection conn, String moduleName) throws SQLException, IOException, ModuleNotFoundException {
		// Try to get the modules with matching name
		Set<String> rolesToCreate = new HashSet<String>(); // Set of roles that excludes privileged ones.
		
		List<RestModule> modules = getModules(conn, moduleName, rolesToCreate);
		
		Map<String, RestPrivilege> privileges = getPrivileges(conn, modules, rolesToCreate, false);
		
		boolean isRequestingAllModules = (moduleName == null || "".equals(moduleName) == true);
		boolean isModulePath = false;
		
		if (moduleName != null) {
			isModulePath = moduleName.startsWith("/");
		}
		
		ExportEntity entity = new ExportEntity();
		entity.modules = modules;
		entity.privileges = privileges;
		entity.isRequestionAllModules = isRequestingAllModules;
		entity.isModulePath = isModulePath;
		entity.moduleFilter = moduleName;
		entity.rolesToCreate = rolesToCreate;
		entity.schema = getSchema(conn);
		
		return entity;
	}
	
	private String getCanonicalPath(String modulePath) {
		String res = modulePath;
		
		// Append and prepend / if not empty
		if (modulePath == null || "".equals(modulePath)) {
			return modulePath;
		}
		
		// Empty prefix
		if ("/".equals(modulePath)) {
			return modulePath;
		}
		
		if (!res.startsWith("/")) {
			res = "/" + res; 
		}
		
		if (!res.endsWith("/")) {
			res = res + "/";
		}
		
		return res;
	}
	
	/**
	 * Method that returns the current schema information and the ords version from
	 * the database.
	 * 
	 * @param conn Oracle Connection
	 * @return RestSchema object
	 * @throws SQLException
	 */
	private RestSchema getSchema(Connection conn) throws SQLException {
		RestSchema schema = null;
    	
    	OracleConnection ocon = (OracleConnection) conn;
    	PreparedStatement stmt = null;
    	ResultSet result = null;
    	
    	try {
			stmt = ocon.prepareStatement(SCHEMA_INFO_SQL);
			result = stmt.executeQuery();
			
			
			if (result.next()) {
				// Same tables for user_ords_modules and table ords_modules
				String parsingSchema = result.getString("parsing_schema");
				String type = result.getString("type");
				String pattern = result.getString("pattern");
				String status = result.getString("status");
				String autoRestAuth = result.getString("auto_rest_auth");
				String ordsVersion = result.getString("ords_version");
				
				schema = new RestSchema(parsingSchema, type, pattern, status, autoRestAuth, ordsVersion);
			}
		
		} catch (SQLException f) {
			throw f;
		} finally {
			if (result != null) {
				result.close();
			}
			
			if (stmt != null) {
				stmt.close();
			}
		}
    	
    	return schema;
	}
	
	private List<RestModule> getModules(Connection conn, String moduleName, Set<String> rolesToCreate) throws SQLException, ModuleNotFoundException, IOException {
		List<RestModule> modules = null;
		
		// Attempt to get modules with matching name
		try {
			modules = getModules(conn, moduleName, rolesToCreate, false);
		} catch (ModuleNotFoundException e) {
			//Suppress since we are going to try to get by canonical module path
		}
		
		if (modules == null || modules.isEmpty()) {
			modules = getModules(conn, getCanonicalPath(moduleName), rolesToCreate, true);
		}
		
		return modules;
	}
	
	/**
	 *
	 * @param conn The Oracle Connection
	 * @param moduleName The module name/path or empty (all)
	 * @param isPrefixSearch matching a prefix?
	 * @return a list of modules
	 * @throws SQLException ModuleNotFoundException
	 * @throws IOException 
	 */
	private List<RestModule> getModules(Connection conn, String moduleName, Set<String> rolesToCreate, Boolean isPrefixSearch) throws SQLException, ModuleNotFoundException, IOException {
		OracleConnection ocon = (OracleConnection) conn;
		
		PreparedStatement stmt = null;
		ResultSet result = null;
		
		boolean isRequestingAllModules = (moduleName == null || "".equals(moduleName) == true);
		boolean isModulePath = false;
		
		List<RestModule> modules = new ArrayList<RestModule>();
		
		try {
			if (isRequestingAllModules) {
				stmt = ocon.prepareStatement(EXTRACT_MODULES_QUERY + " order by name");
			} else {
				String baseStatement = EXTRACT_MODULES_QUERY;
				
				isModulePath = isPrefixSearch;
				
				if (isModulePath) {
					// The name is already canonical
					// For backward compatibility we will match 'equivalent' prefixes
					// for rest services created before introducing canonical transformations
					// in ORDS.
					if ("/".equals(moduleName)) {
						// If searching for a generic base path, compare it to null (empty prefix)
						baseStatement += " where uri_prefix is null";
						stmt = ocon.prepareStatement(baseStatement);
					} else {
						// Case where the path is
						baseStatement += " where uri_prefix = ?";
						baseStatement += " or uri_prefix = ?";
						baseStatement += " or uri_prefix = ?";
						baseStatement += " or uri_prefix = ? order by name";
						stmt = ocon.prepareStatement(baseStatement);
						
						stmt.setString(1, moduleName);
						stmt.setString(2, moduleName.substring(1, moduleName.length()));
						stmt.setString(3, moduleName.substring(1, moduleName.length()-1));
						stmt.setString(4, moduleName.substring(0, moduleName.length()-1));
					}
					
					
				} else {
					baseStatement += " where name = ? order by name";
					stmt = ocon.prepareStatement(baseStatement);
					stmt.setString(1, moduleName);
				}
				
			}
			
			result = stmt.executeQuery();
			
			
			
			boolean hasModules = false;
			
			while (result.next()) {
				hasModules = true;
				
				// Same tables for user_ords_modules and table ords_modules
				String name = result.getString("NAME");
				String prefix = result.getString("URI_PREFIX");
				Integer items = result.getInt("ITEMS_PER_PAGE");
				String status = result.getString("STATUS");
				String comments = result.getString("COMMENTS");
				Integer moduleId = result.getInt("ID");
				
				RestModule newModule = new RestModule();
				newModule.setName(name);
				newModule.setURIPrefix(prefix);
				newModule.setPaginationSize(items);
				newModule.setPublished(status);
				newModule.setComments(comments); 
				newModule.setTemplates(getTemplates(conn, moduleId));
				
				modules.add(newModule);
				
			}
		
		
		
			// If the requested module cannot be found
			if (hasModules == false && !isRequestingAllModules) {
				if (isModulePath) {
					throw new ModuleNotFoundException(ModuleSearchOrigin.PATH, moduleName);
				} else {
					throw new ModuleNotFoundException(ModuleSearchOrigin.NAME, moduleName);
				}
			} else if (hasModules == false) {
				throw new ModuleNotFoundException(ModuleSearchOrigin.ALL);
			}
		
		} catch (SQLException f) {
			throw f;
		} finally {
			if (result != null) {
				result.close();
			}
			
			if (stmt != null) {
				stmt.close();
			}
		}
		
		return modules;
		
	}
	
	/**
	 * Given a list of modules, returns a map of non repeated privileges (Key=privilegeName and Value=privilege).
	 * It returns the privileges strictly binded to the given modules with all the patterns related
	 * 
	 * @param conn
	 * @param modules
	 * @param legacy
	 * @return
	 * @throws SQLException 
	 */
	private Map<String, RestPrivilege> getPrivileges(Connection conn, List<RestModule> modules, Set<String> rolesToCreate, boolean legacy) throws SQLException {
		Map<String, RestPrivilege> privs = new HashMap<String, RestPrivilege>();
		
		if (modules == null) {
			return privs;
		}
		
		OracleConnection ocon = (OracleConnection) conn;
		
		for (RestModule module: modules) {
			String moduleName = module.getName();
			
			PreparedStatement stmt =  null;
			
			if (legacy) {
				stmt = ocon.prepareStatement(LEGACY_PRIVILEGES_QUERY);
			} else {
				stmt = ocon.prepareStatement(PRIVILEGES_QUERY);
			}
			
			stmt.setString(1, moduleName);
			ResultSet result = null;
			
			try {
				result = stmt.executeQuery();
				
				while (result.next()) {
					String name = result.getString("name");
					
					if (privs.containsKey(name)) {
						// Append the module to the existing privilege module's list
						privs.get(name).getModules().add(moduleName);
					} else {
						// Create the privilege object, roles and add the moduleName
						String label = result.getString("label");
						String description = result.getString("description");
						String comments = result.getString("comments");
						String unparsedPatterns = result.getString("patterns");
						
						List<String> roles = getPrivilegeRoles(conn, name, rolesToCreate, legacy);
						List<String> privilegeModules = new ArrayList<String>();
						List<String> patterns = new ArrayList<String>();
						
						privilegeModules.add(moduleName);
						
						RestPrivilege privilege = new RestPrivilege();
						privilege.setName(name);
						privilege.setTitle(label);
						privilege.setDescription(description);
						privilege.setComments(comments);
						
						privilege.setRoles(roles);
						privilege.setModules(privilegeModules);
						
						if (unparsedPatterns != null && unparsedPatterns.length() > 0) {
							String tokens[] = unparsedPatterns.split("\\|\\^\\|");
							for (int i = 0; i < tokens.length; i++) {
								patterns.add(tokens[i]);
							}
						}
						
						privilege.setURIPatterns(patterns);
						
						
						//Privilege priv = (new PrivilegesBuilder()).name(name).label(label).description(description).comments(comments).roles(roles).modules(privilegeModules).patterns(patterns).build();
						privs.put(name, privilege);
					}
				}
			}  catch (SQLException e) {
				if (e.getErrorCode() == 942 && legacy == false) {
		        	return getPrivileges(conn, modules, rolesToCreate, true);
		        } else {
		        	throw e;
		        }
			} finally {
				if (result != null) {
					result.close();
				}
				
				if (stmt != null) {
					stmt.close();
				}
			}
		
		}
		
		
		return privs;
	}

	/**
	 * Obtains all the roles associated to a privilege.
	 * It first tries to query USER_* views and if it gets a 942 it will call the 
	 * same function in legacy mode to try to query ALL_* views. If failed again it means
	 * that the current schema does not have access to ORDS object.
	 * 
	 * @param conn Oracle Connection
	 * @param privilegeName
	 * @param legacy if set to false it will try to use USER_* views, set to true if you want to use old ords ALL_ views
	 * @return list of roles (strings)
	 * @throws SQLException
	 */
	private List<String> getPrivilegeRoles(Connection conn, String privilegeName, Set<String> rolesToCreate, boolean legacy) throws SQLException {
		List<String> roles = new ArrayList<String>();
		
		if (privilegeName == null || privilegeName.equals("")) {
			return roles;
		}
		
		OracleConnection ocon = (OracleConnection) conn;
		
		PreparedStatement stmt = null;
		
		if (legacy) {
			stmt = ocon.prepareStatement(LEGACY_PRIVILEGE_ROLES_QUERY);
		} else {
			stmt = ocon.prepareStatement(PRIVILEGE_ROLES_QUERY);
		}
		
		stmt.setString(1, privilegeName);
		ResultSet result = null;
		
		try {
			result = stmt.executeQuery();
			
			while (result.next()) {
				String name = result.getString("role_name");
				Integer roleOwner = result.getInt("role_owner_id");
				
				// Add to the list of referenced roles of that privilege
				roles.add(name);
				
				// Add to the list of roles to be created if it it not privileged
				if (roleOwner > 10) {
					rolesToCreate.add(name);
				}
				
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 942 && legacy == false) {
	        	return getPrivilegeRoles(conn, privilegeName, rolesToCreate, true);
	        } else {
	        	throw e;
	        }
		} finally {
			if (result != null) {
				result.close();
			}
			
			if (stmt != null) {
				stmt.close();
			}
		}
		
		return roles;
	}

	/**
	 * Obtains all the templates associated with a module. The template contains all handlers
	 * and parameters.
	 * 
	 * @param conn Oracle Connection
	 * @param id module ID
	 * @return list of templates
	 * @throws SQLException
	 * @throws IOException
	 */
	private List<RestTemplate> getTemplates(Connection conn, Integer id) throws SQLException, IOException {
		List<RestTemplate> templates = new ArrayList<RestTemplate>();
		
		if (id == null) {
    		return templates;
    	}
    	
    	OracleConnection ocon = (OracleConnection) conn;
    	
		PreparedStatement stmt = null;
		
		ResultSet result = null;
		
		try {
			stmt = ocon.prepareStatement("select * from USER_ORDS_TEMPLATES where module_id = ? order by uri_template");
			stmt.setInt(1, id);
			result = stmt.executeQuery();
			
			while (result.next()) {
				
				// Same tables for user_ords_modules and table ords_modules
				String uriTemplate = result.getString("URI_TEMPLATE");
				Integer priority = result.getInt("PRIORITY");
				String etagType = result.getString("ETAG_TYPE");
				String etagQuery = result.getString("ETAG_QUERY");
				String comments = result.getString("COMMENTS");
				
				Integer templateId = result.getInt("ID");
				
				RestTemplate newTemplate = new RestTemplate();
				newTemplate.setURIPattern(uriTemplate);
				newTemplate.setPriority(priority);
				newTemplate.setEntityTag(etagType);
				newTemplate.setEntityTagQuery(etagQuery);
				newTemplate.setComments(comments);
				
				newTemplate.setResourceHandlers(getHandlers(conn, templateId));
				
				templates.add(newTemplate);
			}
		
		} catch (SQLException f) {
			throw f;
		} finally {
			if (result != null) {
				result.close();
			}
			
			if (stmt != null) {
				stmt.close();
			}
		}
		
		return templates;
	}

	/**
	 * Obtains all the handlers associated with a template. The handlers contain all
	 * the associated parameters.
	 * 
	 * @param conn Oracle Connection
	 * @param id Template ID
	 * @return list of handlers
	 * @throws SQLException
	 * @throws IOException
	 */
	private List<RestResourceHandler> getHandlers(Connection conn, Integer id) throws SQLException, IOException {
		List<RestResourceHandler> handlers = new ArrayList<RestResourceHandler>();
		
		if (id == null) {
    		return handlers;
    	}
    	
    	OracleConnection ocon = (OracleConnection) conn;
    	
    	PreparedStatement stmt = null;
    	ResultSet result = null;
    	
    	try {
			stmt = ocon.prepareStatement("select * from USER_ORDS_HANDLERS where template_id = ? order by method");
			stmt.setInt(1, id);
			result = stmt.executeQuery();
			
			while (result.next()) {
				
				// Same tables for user_ords_modules and table ords_modules
				String sourceType = result.getString("SOURCE_TYPE");
				String method = result.getString("METHOD");
				String mimesAllowed = result.getString("MIMES_ALLOWED");
				Integer itemsPerPage = result.getInt("ITEMS_PER_PAGE");
				Clob source = result.getClob("SOURCE");
				String comments = result.getString("COMMENTS");
				
				StringBuilder ce = new StringBuilder();
				StreamCopy.drain(source.getCharacterStream(), ce);
				InputStream clob = StreamCopy.toInputStream(ce.toString());
				
				
				RestResourceHandler handler = new RestResourceHandler();
				
				List<String> mimeTypes = new ArrayList<String>();
				String mimes[] = null;
				
				if (mimesAllowed != null) {
					mimes = mimesAllowed.split(",");
					if (mimes != null) {
						for (int i = 0; i < mimes.length; i++) {
							String mime = mimes[i];
							if (mime != null) {
								mimeTypes.add(mime.trim());
							}
						}
					}
				}
				
				handler.setSourceType(sourceType);
				handler.setMethodType(method);
				handler.setMimeTypes(mimeTypes);
				handler.setPaginationSize(itemsPerPage);
				handler.setComments(comments);
				handler.setSQL(StreamCopy.string(clob)); // Recommend using clob export generation in RESTSQLVisitor when exporting.
				
				
				
				Integer handlerId = result.getInt("ID");
				handler.setParameters(this.getParameters(conn, handlerId));
					
				handlers.add(handler);
			}
		
		} catch (SQLException f) {
			throw f;
		} finally {
			if (result != null) {
				result.close();
			}
			
			if (stmt != null) {
				stmt.close();
			}
		}
    	
    	return handlers;
	}

	/**
	 * Method that returns all the parameters for particular handler.
	 * 
	 * @param conn Oracle Connection
	 * @param id Handler Id
	 * @return list of parameters
	 * @throws SQLException
	 */
	private List<RestHandlerParameter> getParameters(Connection conn, Integer id) throws SQLException {
		List<RestHandlerParameter> parameters = new ArrayList<RestHandlerParameter>();
		if (id == null) {
    		return parameters;
    	}
    	
    	OracleConnection ocon = (OracleConnection) conn;
    	PreparedStatement stmt = null;
    	ResultSet result = null;
    	
    	try {
			stmt = ocon.prepareStatement("select * from USER_ORDS_PARAMETERS where handler_id = ? order by name");
			stmt.setInt(1, id);
			result = stmt.executeQuery();
			
			
			while (result.next()) {
				// Same tables for user_ords_modules and table ords_modules
				String name = result.getString("NAME");
				String bindName = result.getString("BIND_VARIABLE_NAME");
				String sourceType = result.getString("SOURCE_TYPE");
				String accessMethod = result.getString("ACCESS_METHOD");
				String paramType = result.getString("PARAM_TYPE");
				String comments = result.getString("COMMENTS");
				
				RestHandlerParameter parameter = new RestHandlerParameter();
				
				parameter.setName(name);
				parameter.setBindVariable(bindName);
				parameter.setSourceType(sourceType);
				parameter.setAccessMethod(accessMethod);
				parameter.setDataType(paramType);
				parameter.setComments(comments);
				
				parameters.add(parameter);
			}
		
		} catch (SQLException f) {
			throw f;
		} finally {
			if (result != null) {
				result.close();
			}
			
			if (stmt != null) {
				stmt.close();
			}
		}
    	
    	return parameters;
	}

	/**
	 * Module not found exception for REST modules.
	 * 
	 * Should be thrown when:
	 * 
	 * - There are no modules associated to a path.
	 * - There are no modules with a given name.
	 * - The schema does not contain any module.
	 * 
	 * @author josmende
	 *
	 */
	class ModuleNotFoundException extends Throwable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1548710029851198208L;
		
		ModuleNotFoundException(ModuleSearchOrigin origin) {
			this.origin = origin;
		}
		
		ModuleNotFoundException(ModuleSearchOrigin origin, String originName) {
			this(origin, originName, null);
		}
		
		ModuleNotFoundException(ModuleSearchOrigin origin, String originName, Throwable t) {
			super(t);
			
			this.origin = origin;
			
			if (ModuleSearchOrigin.NAME.equals(origin)) {
				this.moduleName = originName;
			}
			
			if (ModuleSearchOrigin.PATH.equals(origin)) {
				this.modulePath = originName;
			}
		}

		public ModuleSearchOrigin getOrigin() {
			return origin;
		}

		public String getModuleName() {
			return moduleName;
		}
		
		public String getModulePath() {
			return modulePath;
		}

		private ModuleSearchOrigin origin = null;
		private String moduleName = null;
		private String modulePath = null;
		
	}
	
	public enum ModuleSearchOrigin {
		NAME,
		PATH,
		ALL
	}
	
	
	/**
	 * Begin Model classes 
	 * @author josmende
	 *
	 */
	
	
	class ExportEntity implements RESTVisitable {
		public RestSchema schema;
		public Set<String> rolesToCreate; // Roles to be created
		public String moduleFilter;
		public boolean isModulePath;
		public boolean isRequestionAllModules;
		List<RestModule> modules;
		Map<String, RestPrivilege> privileges; // Each privilege has a list of roles that is references.
		
		@Override
		public void accept(RESTVisitor visitor) throws IOException {
			visitor.visit(this);
		}
	}
	
	
	
	

}
