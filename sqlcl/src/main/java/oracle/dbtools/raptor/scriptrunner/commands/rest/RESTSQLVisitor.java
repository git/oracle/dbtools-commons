/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.scriptrunner.commands.rest;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.List;

import oracle.dbtools.raptor.scriptrunner.commands.rest.RESTExtractor.ExportEntity;
import oracle.dbtools.rest.model.*;
import oracle.dbtools.util.StreamCopy;

/**
 * Stateful visitor that transforms a REST data structure into PL/SQL API calls.
 * Usage:
 * 
 * - Instantiate it
 * - Call a visit cascade call
 * - Obtain the results using getContents().
 * 
 * @author josmende
 *
 */
@Deprecated
public class RESTSQLVisitor implements RESTVisitor {
	
	private StringBuilder call;
	
	public RESTSQLVisitor() {
		this.call = new StringBuilder();
	}
	
	public String getContents() {
		return call.toString();
	}

	@Override
	public void visit(ExportEntity entity) throws IOException {
		List<RestModule> modules = entity.modules;
		
		// Handle cases when no modules are found
		// This messages are appended only if the RESTExtractor was not called directly
		// (RESTExtractor throws ModuleNotFoundException)
		if (modules.size() == 0) {
			if (entity.isRequestionAllModules) {
				call.append("No modules found in the current schema.");
			} else {
				if (entity.isModulePath) {
					call.append("No module found for path: " + entity.moduleFilter);
				} else {
					call.append("No module named: " + entity.moduleFilter);
				}
			}
			
			// Halt after printing the message output.
			return;
		}
		
		// Start the wrapper anonymous block
		call.append( "declare\n  l_module_id number;\n  l_template_id number;\n  l_handler_id number;\n  l_parameter_id number;\nbegin\n");
		
		for (RestModule module : modules) {
			this.visit(module);			
		}
		
		
		// Handle privileges
		
		for (String key : entity.privileges.keySet()) {
			call.append("\n    declare\n");
			call.append("      l_modules owa.vc_arr;\n");
			call.append("      l_roles owa.vc_arr;\n");
			call.append("      l_patterns owa.vc_arr;\n");
			call.append("      l_priv_id number;\n");
			call.append("      l_role_id number;\n");
			call.append("    begin\n");
			
			RestPrivilege priv = entity.privileges.get(key);
			
			int roleCounter = 1;
			//create and append roles
			for (String role: priv.getRoles()) {
				call.append("      l_role_id := ORDS_SERVICES.create_role('" + role + "');\n");
				call.append("      l_roles(" + roleCounter++ + ") := '" + role + "';\n");
			}
			
			//append modules to the module array
			int moduleCounter = 1;
			for (String module: priv.getModules()) {
				call.append("      l_modules(" + moduleCounter++ + ") := '" + module + "';\n");
			}
			
			//append patterns to the pattern array
			int patternCounter = 1;
			for (String pattern: priv.getURIPatterns()) {
				call.append("      l_patterns(" + patternCounter++ + ") := '" + pattern + "';\n");
			}
			
			//Create the privilege
			call.append("      l_priv_id := ORDS_SERVICES.create_privilege(\n");
			call.append("        p_name => '" + priv.getName() + "',\n" );
			call.append("        p_label => '" + priv.getTitle() + "',\n" );
			call.append("        p_description => '" + priv.getDescription() + "',\n" );
			call.append("        p_roles => l_roles,\n" );
			call.append("        p_modules => l_modules,\n" );
			call.append("        p_patterns => l_patterns,\n" );
			call.append("        p_comments => '" + priv.getComments() + "');\n" );
			call.append("    end;");
			
    	}
		
		call.append("\ncommit;\n");
		call.append("end;\n/");
		
	}

	@Override
	public void visit(RestModule module) throws IOException {

		
		call.append("\n  ORDS_SERVICES.delete_module(");
		
		if (module.getName() != null) {
			call.append(" p_name => '" + module.getName());
		}
		
		call.append("');");
		
        call.append("\n  l_module_id := ORDS_SERVICES.create_module(");
		
		if (module.getName() != null) {
			call.append(" p_name => '" + module.getName() + "' ,");
		}
		
		if (module.getURIPrefix() != null) {
			call.append(" p_uri_prefix => '" + module.getURIPrefix() + "' ,");
		}
		
		call.append(" p_items_per_page => " + module.getPaginationSize() + " ,");
		
		/*if (module.get.status != null) {
			call.append(" p_status => '" + module.status + "' ,");
		}*/
		
		if (module.getComments() != null) {
			call.append(" p_comments => '" + module.getComments() + "'");
		}
		
		if (call.charAt(call.length() - 1 ) == ',') {
			call.deleteCharAt(call.length() - 1 );
		}
		
		call.append(");");
		
		for (RestTemplate template : module.getTemplates()) {
			this.visit(template);
		}

		call.append("\n");
		
	}

	@Override
	public void visit(RestTemplate template) throws IOException {
		call.append("\n  l_template_id := ORDS_SERVICES.add_template( p_module_id => l_module_id,");
			
		if (template.getURIPattern() != null) {
			call.append(" p_uri_template => '" + template.getURIPattern() + "' ,");
		}
		
		call.append(" p_priority => " + template.getPriority() + " ,");
		
		if (template.getEntityTag().getType() != null) {
			call.append(" p_etag_type => '" + template.getEntityTag().getType() + "' ,");
		}
		
		if (template.getEntityTagQuery() != null) {
			call.append(" p_etag_query => '" + template.getEntityTagQuery() + "' ,");
		}
		
		if (template.getComments() != null) {
			call.append(" p_comments => '" + template.getComments() + "'");
		}
		
		if (call.charAt(call.length() - 1 ) == ',') {
			call.deleteCharAt(call.length() - 1 );
		}
		
		call.append(");");

		for (RestResourceHandler handler : template.getResourceHandlers().values()) {
			this.visit(handler);
		}
	}

	@Override
	public void visit(RestResourceHandler handler) throws IOException {
		call.append("\n");
		call.append("  l_handler_id := ORDS_SERVICES.add_handler( p_template_id => l_template_id,");
		
		if (handler.getSourceType() != null) {
			call.append(" p_source_type => '" + handler.getSourceType() + "' ,");
		}
		
		if (handler.getMethodType().getType() != null) {
			call.append(" p_method => '" + handler.getMethodType().getType() + "' ,");
		}
		
		if (handler.getMimeTypes() != null && handler.getMimeTypes().size() > 0) {
			StringBuilder mimes = new StringBuilder();
			boolean isFirst = true;
			for (String mime: handler.getMimeTypes()) {
				if (isFirst) {
					isFirst = false;
				} else {
					mimes.append(",");
				}
				
				mimes.append(mime);
			}
			
			if (mimes.length() > 0) {
				call.append(" p_mimes_allowed => '" + mimes.toString() + "' ,");
			}
		}
		
		
		call.append(" p_items_per_page => " + handler.getPaginationSize() + " ,");
		
		if (handler.getComments() != null) {
			call.append(" p_comments => '" + handler.getComments() + "'");
		}
		
		if (handler.getSQL() != null) {
			call.append(" p_source => ");
			this.appendClob(StreamCopy.toInputStream(handler.getSQL()));
			call.append(",");
		}
		
		if (call.charAt(call.length() - 1 ) == ',') {
			call.deleteCharAt(call.length() - 1 );
		}
		
		call.append(");");
		
		for (RestHandlerParameter parameter : handler.getParameters()) {
			this.visit(parameter);
		}
	}

	@Override
	public void visit(RestHandlerParameter parameter) {
		call.append("\n");
		call.append("  l_parameter_id := ORDS_SERVICES.add_parameter( p_handler_id => l_handler_id,");
		
		if (parameter.getName() != null) {
			call.append(" p_name => '" + parameter.getName() + "' ,");
		}
		
		if (parameter.getBindVariable() != null) {
			call.append(" p_bind_variable_name => '" + parameter.getBindVariable() + "' ,");
		}
		
		if (parameter.getSourceType() != null) {
			call.append(" p_source_type => '" + parameter.getSourceType() + "' ,");
		}
		
		if (parameter.getAccessMethod() != null) {
			call.append(" p_access_method => '" + parameter.getAccessMethod() + "' ,");
		}
		
		if (parameter.getDataType() != null) {
			call.append(" p_param_type => '" + parameter.getDataType() + "'");
		}
		
		if (parameter.getComments() != null) {
			call.append(" p_comments => '" + parameter.getComments() + "'");
		}
		
		
		if (call.charAt(call.length() - 1 ) == ',') {
			call.deleteCharAt(call.length() - 1 );
		}
		
		call.append(");");
	}
	
	
	/**
     * Buffers chunks of the clob and generates varchar2 appends plus replaces single quotes and line breaks
     * 
     * ' to ''
     * /n to unistr('\000a')
     * 
     * @param clob
     * @param call
     * @throws SQLException 
     * @throws IOException 
     */
    private void appendClob(InputStream in) {
    	if (in == null) {
    		call.append("null");
    	}
    	
    	Integer bufferSize = 200;
    	char[] buffer = new char[bufferSize];
    	String chunk = "";
    	
    	boolean isFirstChunk = true;
    	
    	final String newLineReplacement = "' || unistr('\\000a')\n   || '";
    	
    	Reader clobStream = new InputStreamReader(in);
    	boolean firstRead = true;
    	
    	while (true) {
    		int readResult =  -1;
    		
    		try {
    			//Clear buffer
    			buffer = new char[bufferSize];
				readResult = clobStream.read(buffer);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
    		
    		if (readResult < 0) {   			
    			
    			// Check the last chunk pattern is new line and
    			// remove the last quote and varchar2 string concat.
    			if (chunk.endsWith(newLineReplacement)) {
    				call.replace(call.length() - 5, call.length() - 1, "");
    			}
    			break;
    		}
    		
    		
    		/**
    		 * Grab the null trimmed chunk, escape single quotes, wrap in single quotes (to make it a varchar2) and substitute new lines.
    		 */
    		//Clear nulls from buffer
    		chunk = this.trimNulls(buffer);
    		chunk = chunk.replace("'", "''");
    		chunk = "'" + chunk + "'";
    		
    		if (!firstRead) {
    			chunk = "  ||  " + chunk;
    		} else {
    			firstRead = false;
    		}
    		chunk = chunk.replace("\n", newLineReplacement);
    		
    		
    		// If the whole buffer starts with new line then add the matching single quote 
    		//in order to have a valid concatenation
    		if (isFirstChunk) {
    			if (chunk.startsWith(newLineReplacement)) {
    				chunk = chunk.replaceFirst(newLineReplacement, "'" + newLineReplacement);
    			}
    		}
    		
    		isFirstChunk = false;
    		
    		call.append(chunk);
    	}	
    	
    }
	
	
	/**
     * Buffers chunks of the clob and generates varchar2 appends plus replaces single quotes and line breaks
     * 
     * ' to ''
     * /n to unistr('\000a')
     * 
     * @param clob
     * @param call
     * @throws SQLException 
     * @throws IOException 
     */
    @SuppressWarnings("unused")
	private void appendRawClob(Clob clob, StringBuilder call) throws SQLException {
    	if (clob == null || clob.length() == 0) {
    		call.append("null");
    	}
    	
    	Integer bufferSize = 200;
    	char[] buffer = new char[bufferSize];
    	String chunk = "";
    	
    	boolean isFirstChunk = true;
    	
    	final String newLineReplacement = "' || unistr('\\000a')\n   || '";
    	
    	Reader clobStream = clob.getCharacterStream();
    	boolean firstRead = true;
    	
    	while (true) {
    		int readResult =  -1;
    		
    		try {
    			//Clear buffer
    			buffer = new char[bufferSize];
				readResult = clobStream.read(buffer);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
    		
    		if (readResult < 0) {   			
    			
    			// Check the last chunk pattern is new line and
    			// remove the last quote and varchar2 string concat.
    			if (chunk.endsWith(newLineReplacement)) {
    				call.replace(call.length() - 5, call.length() - 1, "");
    			}
    			break;
    		}
    		
    		
    		/**
    		 * Grab the null trimmed chunk, escape single quotes, wrap in single quotes (to make it a varchar2) and substitute new lines.
    		 */
    		//Clear nulls from buffer
    		chunk = this.trimNulls(buffer);
    		chunk = chunk.replace("'", "''");
    		chunk = "'" + chunk + "'";
    		
    		if (!firstRead) {
    			chunk = "  ||  " + chunk;
    		} else {
    			firstRead = false;
    		}
    		chunk = chunk.replace("\n", newLineReplacement);
    		
    		
    		// If the whole buffer starts with new line then add the matching single quote 
    		//in order to have a valid concatenation
    		if (isFirstChunk) {
    			if (chunk.startsWith(newLineReplacement)) {
    				chunk = chunk.replaceFirst(newLineReplacement, "'" + newLineReplacement);
    			}
    		}
    		
    		isFirstChunk = false;
    		
    		call.append(chunk);
    	}	
    	
    }
    
    /**
	 * Returns a string with the contents of the byte array (without nulls)
	 * @param byteArray
	 * @return
	 */
	private String trimNulls(char[] input) {
		int firstNullPosition = -1;
		
		String result = "";
		
	    for (int i = 0; i < input.length; i++) {
	    	int charValue = new Byte((byte) input[i]).intValue();
	    	if(charValue == 0)	{
	    		firstNullPosition = i;
	    		break;
	        }
	    }
	    
	    if (firstNullPosition > -1 ) {
	    	result = new String(input, 0, firstNullPosition);
	    } else {
	    	result = new String(input);
	    }
	    
	    return result;
	}

}
