/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.tasks;

import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

import oracle.dbtools.raptor.backgroundTask.ITaskDialog;
import oracle.dbtools.raptor.backgroundTask.RaptorTask;
import oracle.dbtools.raptor.backgroundTask.ui.ITaskUI;

/**
 * @author <a href=
 *         "mailto:barry.mcgillin@oracle.com@oracle.com?subject=TaskFeedback.java"
 *         >Barry McGillin</a>
 *
 */
public class ClientTaskFeedback implements ITaskDialog {
	private static ITaskDialog INSTANCE = null;
	ITaskUI ui;
	RaptorTask<?> task;
	boolean check;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ITaskDialog#createNewDialog(oracle
	 * .dbtools.raptor.backgroundTask.ui.ITaskUI,
	 * oracle.dbtools.raptor.backgroundTask.RaptorTask, boolean)
	 */
	public ITaskDialog createNewDialog(ITaskUI arg0, RaptorTask<?> arg1, boolean arg2) {
		if (INSTANCE == null) {
			INSTANCE = new ClientTaskFeedback(arg0, arg1, arg2);
		}
		return INSTANCE;
	}

	public ClientTaskFeedback() {

	}

	public ClientTaskFeedback(ITaskUI arg0, RaptorTask<?> arg1, boolean arg2) {
		ui = arg0;
		arg1 = task;
		check = arg2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ITaskDialog#dispose()
	 */
	public void dispose() {
	
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ITaskDialog#isDisplayable()
	 */
	public boolean isDisplayable() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ITaskDialog#isVisible()
	 */
	public boolean isVisible() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ITaskDialog#setButtonListener(java
	 * .awt.event.ActionListener)
	 */
	public void setButtonListener(ActionListener arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ITaskDialog#setVisible(boolean)
	 */
	public void setVisible(boolean arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ITaskDialog#setWindowCloseListener
	 * (java.awt.event.WindowListener)
	 */
	public void setWindowCloseListener(WindowListener arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ITaskDialog#showDelayedDialog(oracle
	 * .dbtools.raptor.backgroundTask.RaptorTask)
	 */
	public void showDelayedDialog(RaptorTask<?> arg0) {
	}

}
