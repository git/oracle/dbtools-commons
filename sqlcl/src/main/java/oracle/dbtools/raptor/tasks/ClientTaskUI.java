/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.tasks;

import java.awt.Component;

import oracle.dbtools.raptor.backgroundTask.IRaptorTaskStatus;
import oracle.dbtools.raptor.backgroundTask.RaptorTaskDescriptor;
import oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent;
import oracle.dbtools.raptor.backgroundTask.internal.IRaptorTaskUIListener;
import oracle.dbtools.raptor.backgroundTask.ui.ITaskUI;

/**
 * @author <a
 *         href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=TaskUI.java"
 *         >Barry McGillin</a>
 *
 */
public class ClientTaskUI implements ITaskUI {
	private static ClientTaskUI ui = null;
	private RaptorTaskDescriptor td = null;

	public ClientTaskUI(RaptorTaskDescriptor td1) {
		td = td1;
	}

	public ClientTaskUI() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.IRaptorTaskListener#messageChanged
	 * (oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent)
	 */
	public void messageChanged(RaptorTaskEvent arg0) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.IRaptorTaskListener#progressChanged
	 * (oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent)
	 */
	public void progressChanged(RaptorTaskEvent arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.IRaptorTaskListener#stateChanged
	 * (oracle.dbtools.raptor.backgroundTask.RaptorTaskEvent)
	 */
	public void stateChanged(RaptorTaskEvent arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.internal.IRaptorTaskUIListener#
	 * cancelClicked(oracle.dbtools.raptor.backgroundTask.RaptorTaskDescriptor)
	 */
	public void cancelClicked(RaptorTaskDescriptor arg0) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.internal.IRaptorTaskUIListener#
	 * pauseClicked(oracle.dbtools.raptor.backgroundTask.RaptorTaskDescriptor)
	 */
	public void pauseClicked(RaptorTaskDescriptor arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.internal.IRaptorTaskUIListener#
	 * taskClicked(oracle.dbtools.raptor.backgroundTask.RaptorTaskDescriptor)
	 */
	public void taskClicked(RaptorTaskDescriptor arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#addListener(oracle.dbtools
	 * .raptor.backgroundTask.internal.IRaptorTaskUIListener)
	 */
	public void addListener(IRaptorTaskUIListener arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#getMessage()
	 */
	public String getMessage() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#getName()
	 */
	public String getName() {
		if (td != null) {
			return td.getName();
		}
		return "BMG> TaskuiName";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#getNewTaskUI(oracle.dbtools
	 * .raptor.backgroundTask.RaptorTaskDescriptor)
	 */
	public ITaskUI getNewTaskUI(RaptorTaskDescriptor td) {
		if (ui == null) {
			ui = new ClientTaskUI(td);
		}
		return ui;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#getPanel()
	 */
	public Component getPanel() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#getStatus()
	 */
	public IRaptorTaskStatus getStatus() {
		if (td != null)
			return td.getStatus();
		return IRaptorTaskStatus.RUNNABLE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#getTaskDescriptor()
	 */
	public RaptorTaskDescriptor getTaskDescriptor() {
		if (td != null)
			return td;
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#makeProgress(int)
	 */
	public void makeProgress(int arg0) {

		// Here we should be able to do some fancy console bits later like / - \

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#removeListener(oracle
	 * .dbtools.raptor.backgroundTask.internal.IRaptorTaskUIListener)
	 */
	public void removeListener(IRaptorTaskUIListener arg0) {
		// May make the reader a listener so we can fire updates to it
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#setCancellable(boolean)
	 */
	public void setCancellable(boolean arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#setMessage(java.lang.
	 * String)
	 */
	public void setMessage(String arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#setName(java.lang.String)
	 */
	public void setName(String arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#setPausable(boolean)
	 */
	public void setPausable(boolean arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#setProgress(int)
	 */
	public void setProgress(int arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * oracle.dbtools.raptor.backgroundTask.ui.ITaskUI#setStatus(oracle.dbtools
	 * .raptor.backgroundTask.IRaptorTaskStatus)
	 */
	public void setStatus(IRaptorTaskStatus arg0) {
	}

}
