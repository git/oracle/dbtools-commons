/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.raptor.utils;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Attribute;

public class AnsiColorListPrinter extends ListPrinter {

	@Override
  public  void printListofList(ScriptRunnerContext ctx, String title, List<List<?>> rows) {
    if ( title != null ){
      ctx.write(Ansi.ansi().format(title).reset() + "\n");
    }
    printListofList(ctx,rows);
  }

	@Override
  public void printHeaders(OutputStreamWriter out, int[] widths, int j, List<?> headers,List<Boolean> isNumber) throws IOException {
      for (Object col:headers) {
       String align= isNumber != null && isNumber.get(j) ? "" : "-";
       
      if ( col.toString().indexOf("@|") >= 0 && col.toString().indexOf("|@") > 0 ){
        out.write(Ansi.ansi().render("%"+align + widths[j] + "s", Ansi.ansi().render(col.toString())).reset().toString());
      } else {
        out.write(Ansi.ansi().a(Attribute.UNDERLINE).a(Attribute.INTENSITY_BOLD).format("%"+align + widths[j] + "s", col.toString()).reset().toString());
      }
      out.write(" ");
      j++;
    }
    out.write("\n");
  }

	public void writeBoldUnderlineFormat(ScriptRunnerContext ctx,String col, int width) {
		ctx.write(Ansi.ansi().bold().a(Attribute.UNDERLINE).format("%-" + width + "s", col).reset().toString());
		ctx.write(" ");
	}
	
	public void printValue(OutputStreamWriter out, int i, String val) throws IOException {
		out.write(String.format("%-" + i + "s", Ansi.ansi().render(val)));
		out.write(" ");
	}
}
