/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.versions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.impl.conn.SystemDefaultRoutePlanner;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import oracle.dbtools.common.utils.DateUtils;
import oracle.dbtools.common.utils.FileUtils;
import oracle.dbtools.common.utils.ModelUtil;
import oracle.dbtools.common.utils.Version;
import oracle.dbtools.raptor.proxy.ProxySelectorImpl;
import oracle.dbtools.raptor.scriptrunner.cmdline.SqlCli;
import oracle.dbtools.util.Logger;

/**
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=CheckForUpdates.java">Barry McGillin</a>
 *
 */
public class CheckForUpdates {
	private static final String DEFAULT_CONFIGURATION = "META-INF/cfu.json";
	private static final String MIME_TYPE = ContentType.APPLICATION_JSON.getMimeType();
	public static final String BANNERFILE = File.separator + "banner.ser"; //$NON-NLS-1$

	abstract static class RH<T> implements ResponseHandler<T> {
		@Override
		public T handleResponse(HttpResponse response)
				throws ClientProtocolException, IOException {
			T result = null;
			StatusLine status = response.getStatusLine();
			int code = status.getStatusCode();
			switch(code) {
				case 200:
					// OK
					result = parseResponse(response);
					break;
				//TODO: implement checks for redirects, etc.
				default:
					// Log any errors received here
					Logger.info(CheckForUpdates.class, "Error retrieving update" + status.toString());
			}
			return result;
		}

		/**
		 * Parse out the response to the CFU request
		 * @param resp
		 * @return
		 * @throws IOException
		 */
		private T parseResponse(HttpResponse resp) throws IOException {
			// Check to make sure we are getting a JSON document back
			ContentType type = ContentType.getOrDefault(resp.getEntity());
			if ( !MIME_TYPE.equalsIgnoreCase(type.getMimeType())) {
				throw new IOException("Unexpected Content-Type: " + type); //$NON-NLS-1$
			}

			// Now that we've validated the Content-Type, get the HTTP entity and build
			// our JSON document from it
			HttpEntity entity = resp.getEntity();
			Charset cs = type.getCharset();
			if ( cs == null ) {
				// If either no charset was specified or the one listed is bogus default to UTF-8
				try {
					cs = Charset.forName("UTF-8"); //$NON-NLS-1$
				} catch ( UnsupportedCharsetException e) {
					// Bad voodoo if UTF-8 isn't supported...
					Logger.warn(CheckForUpdates.class, "Unsupported charset: UTF-8!"); //$NON-NLS-1$
					cs = Charset.defaultCharset();
				}
			}
			try (Reader r = new BufferedReader(new InputStreamReader(entity.getContent(), cs))) {
				return build(r);
			}
		}
		
		protected abstract T build(Reader r) throws IOException;
	}
	
	/**
	 * {@link ResponseHandler} for processing the CFU request
	 * @author jmcginni
	 *
	 */
	static final class UpdatesResponseHandler extends RH<Updates> {
		/**
		 * Reads from the HttpResponse's entity data and converts it into our Updates model
		 * @param r
		 * @return
		 * @throws IOException
		 */
		protected Updates build(Reader r) throws IOException {
			try {
				return MAPPER.readValue(r, Updates.class);
//				JsonNode root = MAPPER.readTree(r);
//				JsonNode sqlcl = root.get("sqlcl");
//				return MAPPER.convertValue(sqlcl, Updates.class);
			} catch (IllegalArgumentException e) {
				// JSON that has bad format may throw this, so re-throw it as an IOException
				throw new IOException(e);
			}
		}
	}
	
	static final class ListResponseHandler extends RH<List<URI>> {

		@Override
		protected List<URI> build(Reader r) throws IOException {
			List<URI> result = new ArrayList<URI>();
			try {
				// Get the root of the document
				JsonNode root = MAPPER.readTree(r);
				
				// now grab our sqlcl object
				JsonNode sqlcl = root.get("sqlcl"); //$NON-NLS-1$
				if ( sqlcl != null ) {
					
					// And get the list of update centers
					JsonNode cfu = sqlcl.get("cfu"); //$NON-NLS-1$
					
					if ( cfu != null && cfu.isArray() ) {
						for ( JsonNode entry : cfu ) {
							String path = entry.asText();
							if ( ModelUtil.hasLength(path)) {
								// Try to convert to a URI
								try {
									result.add(new URI(path));
								} catch ( URISyntaxException e ) {
									
								}
							}
						}
					}
				}
				return result;
			} catch (IllegalArgumentException e) {
				// JSON that has bad format may throw this, so re-throw it as an IOException
				throw new IOException(e);
			}
		}
		
	}

	/**
	 * The config data specifying where to read the CFU data and the timeout to use
	 * @author jmcginni
	 *
	 */
	public static final class Config {
		private String url;
		private int timeout = 60000; // default to 60s
		
		@JsonSetter("url")
		private void setUpdateUrl(String url) { this.url = url; }
		
		@JsonSetter("timeout")
		private void setTimeout(int timeout) { this.timeout = timeout; }
		
		public String getUpdateURL() {
			return url;
		}
		
		public int getTimeout() {
			return timeout;
		}
	}
	
	private static ObjectMapper MAPPER = new ObjectMapper();
	
	/**
	 * Check for Updates
	 * @return
	 */
	public static CheckForUpdates check() {
		CheckForUpdates cfu = new CheckForUpdates();
		Config config = cfu.loadConfig();
		if ( config != null ) {
			cfu.loadFromConfig(config);
		}
		
		return cfu;
	}
	
	private static CloseableHttpClient getHttpClient(int timeout) {
		// Configure the socket timeout
		SocketConfig socketConfig = SocketConfig.custom().setSoTimeout(timeout).build();
		
		// Create the HttpClientConnectionManager 
		BasicHttpClientConnectionManager connMgr = new BasicHttpClientConnectionManager();
		connMgr.setSocketConfig(socketConfig);
		
		// Configure the client
		HttpClientBuilder builder = HttpClients.custom();
		builder.setConnectionManager(connMgr);
		builder.setUserAgent(getUserAgent());
		
		ProxySelector proxySelector = new ProxySelectorImpl(ProxySelector.getDefault());
		HttpRoutePlanner routePlanner = new SystemDefaultRoutePlanner(proxySelector);
		builder.setRoutePlanner(routePlanner);
		
		return builder.build();
	}
	
	private static String getUserAgent() {
		// The format of this string (particularly where version numbers are) should be synchronized with the server processing
		// so that we can properly log the data
		
        StringBuffer sb = new StringBuffer();
        sb.append( SqlCli.productName );
        sb.append( "/" ); //$NON-NLS-1$
        sb.append( SQLclVersion.getSQLclVersion() );
        sb.append( " (" ); //$NON-NLS-1$
        sb.append( System.getProperty( "os.name" ) ); //$NON-NLS-1$
        sb.append( " " ); //$NON-NLS-1$
        sb.append( System.getProperty( "os.version" ) ); //$NON-NLS-1$
        sb.append( "; " ); //$NON-NLS-1$
        sb.append( System.getProperty( "os.arch" ) ); //$NON-NLS-1$
        sb.append( "; Java " ); //$NON-NLS-1$
        sb.append( System.getProperty( "java.version" ) ); //$NON-NLS-1$
        sb.append( ") " ); //$NON-NLS-1$
        sb.append(SQLclVersion.getFullVersion());
        return sb.toString();
	}
	
	private Updates _update;

	public CheckForUpdates() {

	}
	
	private Config loadConfig() {
		Config config = null;
		
		ClassLoader cl = CheckForUpdates.class.getClassLoader();
		URL url = cl.getResource(DEFAULT_CONFIGURATION);
		if ( url != null ) {
			try (InputStream in = url.openStream() ) {
				config = MAPPER.readValue(in, Config.class);
			} catch (IOException e) {
				Logger.warn(CheckForUpdates.class, e);
			}
		}
		
		return config;
	}
	
	private void loadFromConfig(Config cfg) {
		try (CloseableHttpClient client = getHttpClient(cfg.getTimeout())) {
			// First, fetch t	he list of updates
			HttpGet request = new HttpGet(new URI(cfg.getUpdateURL()));
			if (client !=null) {
				List<URI> updates = client.execute(request, new ListResponseHandler());
				
				if (updates != null) {
					for (URI update : updates) {
						// Iterate over each update center and check for an update
						try {
							request = new HttpGet(update);
							Updates result = client.execute(request,
									new UpdatesResponseHandler());
							if (result != null) {
								_update = result;
								break;
							}
						} catch (UnknownHostException e) {
							// Ignore an unknown host. To be expected if trying to ping the internal update site
							// from outside the firewall
						} catch (IOException e) {
							Logger.warn(CheckForUpdates.class, e);
						}
					}
				}
			}
		} catch (IOException e) {
			Logger.warn(CheckForUpdates.class, e);
		} catch (URISyntaxException e) {
			Logger.warn(CheckForUpdates.class, e);
		}
	}
	
	public boolean isLoaded() {
		return _update != null;
	}

	public Version getLatestOracleVersion() {
		return new Version(getLatestVersion());
	}
	
	public static void main(String[] args) {
		CheckForUpdates cfu = check();
		if (cfu.isLoaded()) {
			System.out.println("Number 5 is ALIVE!");
			System.out.println(cfu.getLatestVersion());
			System.out.println(cfu.getDatePublished());
			System.out.println(cfu.getBroadcastMessage());
			System.out.println(cfu.getBroadcastMessageDate());
		}
	}

	public String getBroadcastMessageDate() {
		if (isLoaded())
			return _update.getMessageDate();
		return "";
	}
	
	public String getBroadcastMessageEndDate() {
		if (isLoaded())
			return _update.getMessageEndDate();
		return ""; //$NON-NLS-1$
	}

	public String getBroadcastMessage() {
		if (isLoaded())
			return _update.getMessage();
		return ""; //$NON-NLS-1$
	}

	public String getDatePublished() {
		if (isLoaded())
			return _update.getPublishedDate();
		return ""; //$NON-NLS-1$
	}

	public String getLatestVersion() {
		if (isLoaded()) {
			return _update.getVersion();
		}
		return "0.0.0.0.1";
	}
	/**
	 * Work out of the message is showable by comparing the start and enddates for the message with the date passed in, usually today!
	 * @param today is the date to check against.
	 * @return boolean.
	 */
	public boolean isMessageShowable(Date today) {
		boolean show = true;
		Date messageStartDate = DateUtils.getDateFromOracleDateFormat(getBroadcastMessageDate());
		Date messageEndDate = DateUtils.getDateFromOracleDateFormat(getBroadcastMessageEndDate());
		if (messageEndDate!=null && messageEndDate.before(today)) {
			show =false;
		}
		if (messageStartDate!=null && messageStartDate.after(today)) {
			show=false;
		}
		return show;
	}

	/**
	 * Serialize this string to the banner so we can grab it next time we start.
	 * @param string to serialize
	 */
	public void save(String string) {
		try {
			String filename = FileUtils.getUserHome()+File.separator+BANNERFILE;			
			if (string.trim().length() > 0) {
				if (FileUtils.getUserHome()!=null) {
					//If the dir does not exist.  make it.
					File dir = new File(FileUtils.getUserHome());
					dir.mkdirs();
				}
				FileOutputStream fileOut = new FileOutputStream(filename);
				ObjectOutputStream out = new ObjectOutputStream(fileOut); 
				out.writeObject(string);
				out.close();
				fileOut.close();
			} else {
				//Nothing to update so we can remove the file
				Files.deleteIfExists(Paths.get( FileUtils.getUserHome(),BANNERFILE));
			}
		 } catch (Exception e) {
			Logger.warn(getClass(), e);
		 }
	}

	/**
	 * Get the SQLcl banner from the previous serialized banner. We try to get a
	 * banner from the CFU and keep it around so we can show it immediately the
	 * next time we start up.
	 * @return
	 */
	public static String getBanner() {
		try {

		String filename = FileUtils.getUserHome()+File.separator+BANNERFILE;
		 FileInputStream fileIn;
			fileIn = new FileInputStream(filename);
         ObjectInputStream in = new ObjectInputStream(fileIn);
         String banner = (String) in.readObject();
         in.close();
         fileIn.close();
         return banner;
		} catch (Exception e) {
			Logger.info(CheckForUpdates.class,e);
		} 
		return null;
	}
}
