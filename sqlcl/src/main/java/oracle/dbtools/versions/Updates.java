/*
 *   Copyright (c) 2018  Oracle Corporation
 *
 *   The Universal Permissive License (UPL), Version 1.0
 *
 *   Subject to the condition set forth below, permission is hereby granted to any person obtaining a copy of this 
 *   software, associated documentation and/or data (collectively the "Software"), free of charge and under any and 
 *   all copyright rights in the Software, and any and all patent rights owned or freely licensable by each licensor 
 *   hereunder covering either 
 *   (i) the unmodified Software as contributed to or provided by such licensor, or 
 *   (ii) the Larger Works (as defined below), to deal in both
 *
 *   (a) the Software, and
 *   (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if one is included with the Software 
 *   (each a “Larger Work” to which the Software is contributed by such licensors),
 *
 *   without restriction, including without limitation the rights to copy, create derivative works of, display, 
 *   perform, and distribute the Software and make, use, sell, offer for sale, import, export, have made, and have 
 *   sold the Software and the Larger Work(s), and to sublicense the foregoing rights on either these or other terms.
 *
 *   This license is subject to the following condition:
 *   The above copyright notice and either this complete permission notice or at a minimum a reference to the UPL 
 *   must be included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 *   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 *   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *   DEALINGS IN THE SOFTWARE. 
 */
package oracle.dbtools.versions;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Simple object to hold parsed update details per startup
 * @author <a href="mailto:barry.mcgillin@oracle.com@oracle.com?subject=Updates.java">Barry McGillin</a>
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public final class Updates {
	private String version;
	private String publishedDate;
	private String message;
	private String messageDate;
	private String messageEndDate;

	/**
	 * 
	 */
	public Updates() {
	}

	/**
	 * @return the version
	 */
	@JsonGetter("version")
	public final String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	@JsonSetter("version")
	public final void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the date_published
	 */
	@JsonGetter("date_published")
	public final String getPublishedDate() {
		return publishedDate;
	}

	/**
	 * @param date_published
	 *            the date_published to set
	 */
	@JsonSetter("date_published")
	public final void setPublishedDate(String date_published) {
		this.publishedDate = date_published;
	}

	/**
	 * @return the message
	 */
	@JsonGetter("message")
	public final String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	@JsonSetter("message")
	public final void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the message_date
	 */
	@JsonGetter("message_date")
	public final String getMessageDate() {
		return messageDate;
	}

	/**
	 * @param message_date
	 *            the message_date to set
	 */
	@JsonSetter("message_date")
	public final void setMessageDate(String message_date) {
		this.messageDate = message_date;
	}

	/**
	 * @return the message_date
	 */
	@JsonGetter("message_end_date")
	public final String getMessageEndDate() {
		return messageEndDate;
	}

	/**
	 * @param message_date
	 *            the message_date to set
	 */
	@JsonSetter("message_end_date")
	public final void setMessageEndDate(String message_end_date) {
		this.messageEndDate = message_end_date;
	}
}